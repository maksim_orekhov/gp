#!/bin/bash

color_output() {
  echo -e "\033[$2m$1\033[0m"
}

title_label() {
  color_output "$1" "$START_END_SCRIPT_COLOR"
}

START_END_SCRIPT_COLOR=37
START_TASK_COLOR=33
END_COLOR=36
INTERACTION=32

################################
# script execution starts here #
################################

color_output "-> This runs migrations, fixtures and dependencies installations (db will be purged). Continue (y/N)?" "$INTERACTION"
read CONTINUE

if [ "$CONTINUE" == "y" ]; then
	
	# Install dependencies
	echo ""
	color_output "-> Installing dependencies..." "$START_TASK_COLOR"
	composer install
	echo ""

	# Migrations
	echo ""
	color_output "-> Migrations..." "$START_TASK_COLOR"
	./vendor/bin/doctrine-module --no-interaction migrations:migrate
	echo ""

	# Fixtures
	echo ""
	color_output "-> Fixtures..." "$START_TASK_COLOR"
	./vendor/bin/doctrine-module --no-interaction orm:fixtures:load
	echo ""
	
	echo ""
	title_label "Deploy finished at: $(date)"
	echo ""
		
	# Front
	color_output "-> Do you want install dependencies and construct front (y/N)?" "$INTERACTION"
	read START_FRONT
	
	if [ "$START_FRONT" == "y" ]; then
		cd public
		echo ""
		color_output "-> Installing front dependencies" "$START_TASK_COLOR"
		# Installing front dependencies
        npm install
		echo ""
		color_output "-> Front construction and listening (to exit push Ctrl+C)" "$START_TASK_COLOR"
		# Front construction
		npm run start
	fi

else
	echo ""
	color_output "Exit" "$END_COLOR"
fi
