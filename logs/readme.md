**error.log** - лог ошибок  
остальные файлы результат работы класса ***LogMessage***
```
use Core\Service\LogMessage;
 
logMessage($message, $file_name=null, $priority=null)
```
var: **$message**:
```
$message = string or array
```
var: **$file_name**:
```
$file_name = string
```  
var: **$priority**:
 ```
EMERG   = 0;  // Emergency: system is unusable
ALERT   = 1;  // Alert: action must be taken immediately
CRIT    = 2;  // Critical: critical conditions
ERR     = 3;  // Error: error conditions
WARN    = 4;  // Warning: warning conditions
NOTICE  = 5;  // Notice: normal but significant condition
INFO    = 6;  // Informational: informational messages
DEBUG   = 7;  // Debug: debug messages
```