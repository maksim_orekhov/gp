#!/bin/bash

color_output() {
  echo -e "\033[$2m$1\033[0m"
}

title_label() {
  color_output "$1" "$START_END_SCRIPT_COLOR"
}

START_END_SCRIPT_COLOR=37
START_TASK_COLOR=33
END_COLOR=36
INTERACTION=32

################################
# script execution starts here #
################################

color_output "-> This runs fixtures installation (db will be purged) and clears the bank-client and deal folders. Continue (y/N)?" "$INTERACTION"
read CONTINUE

if [ "$CONTINUE" == "y" ]; then
	
	# Fixtures
	echo ""
	color_output "-> Fixtures..." "$START_TASK_COLOR"
	./vendor/bin/doctrine-module --no-interaction orm:fixtures:load
	echo ""
	
	# bank-client folder clearing
	echo ""
	color_output "-> bank-client folder clearing..." "$START_TASK_COLOR"

	if [ "$(ls -A upload/bank-client)" ]; then
	     rm upload/bank-client/*
	else
		echo ""
	    echo "bank-client folder is empty"
	fi

	# deal folder clearing
	echo ""
	color_output "-> deal folder clearing..." "$START_TASK_COLOR"

	if [ "$(ls -A upload/deal-contract)" ]; then
	     rm upload/deal-contract/*
	else
		echo ""
	    echo "deal folder is empty"
	fi

	echo ""
	
	echo ""
	title_label "System state rollback finished at: $(date)"
	echo ""

else
	echo ""
	color_output "Exit" "$END_COLOR"
fi
