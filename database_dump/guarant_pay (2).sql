-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Июл 06 2017 г., 15:37
-- Версия сервера: 5.7.14
-- Версия PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `guarant_pay`
--

-- --------------------------------------------------------

--
-- Структура таблицы `code`
--

CREATE TABLE `code` (
  `id` int(11) NOT NULL,
  `value` varchar(10) NOT NULL,
  `count` int(11) DEFAULT NULL,
  `code_type_id` int(11) DEFAULT NULL,
  `code_process_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `code`
--

INSERT INTO `code` (`id`, `value`, `count`, `code_type_id`, `code_process_type_id`, `user_id`, `created`) VALUES
(1, '6001550', NULL, 1, NULL, 43, '2017-07-06 15:06:50'),
(2, '5133090', NULL, 1, NULL, 43, '2017-07-06 15:09:33'),
(3, '0335611', NULL, 1, NULL, 43, '2017-07-06 15:13:03'),
(4, '1511476', NULL, 1, NULL, 43, '2017-07-06 15:14:17'),
(5, '3165501', NULL, 1, NULL, 43, '2017-07-06 15:15:32'),
(6, '2145700', NULL, 1, NULL, 43, '2017-07-06 15:27:04');

-- --------------------------------------------------------

--
-- Структура таблицы `code_process_type`
--

CREATE TABLE `code_process_type` (
  `id` int(11) NOT NULL,
  `value` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `code_type`
--

CREATE TABLE `code_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `code_type`
--

INSERT INTO `code_type` (`id`, `name`, `description`) VALUES
(1, 'confirmation', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(70) NOT NULL,
  `code` varchar(2) NOT NULL,
  `code3` varchar(3) NOT NULL,
  `phone_code` int(7) NOT NULL,
  `postcode_required` tinyint(1) NOT NULL DEFAULT '0',
  `is_eu` tinyint(1) NOT NULL DEFAULT '0',
  `weight` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `country`
--

INSERT INTO `country` (`id`, `name`, `code`, `code3`, `phone_code`, `postcode_required`, `is_eu`, `weight`) VALUES
(1, 'United States', 'US', 'USA', 1, 0, 0, 0),
(2, 'Canada', 'CA', 'CAN', 1, 0, 0, 0),
(3, 'Afghanistan', 'AF', 'AFG', 93, 0, 0, 0),
(4, 'Albania', 'AL', 'ALB', 355, 0, 0, 0),
(5, 'Algeria', 'DZ', 'DZA', 213, 0, 0, 0),
(6, 'American Samoa', 'DS', 'ASM', 44, 0, 0, 0),
(7, 'Andorra', 'AD', 'AND', 376, 0, 0, 0),
(8, 'Angola', 'AO', 'AGO', 244, 0, 0, 0),
(9, 'Anguilla', 'AI', 'AIA', 1264, 0, 0, 0),
(10, 'Antarctica', 'AQ', 'ATA', 672, 0, 0, 0),
(11, 'Antigua and Barbuda', 'AG', 'ATG', 1268, 0, 0, 0),
(12, 'Argentina', 'AR', 'ARG', 54, 0, 0, 0),
(13, 'Armenia', 'AM', 'ARM', 374, 0, 0, 0),
(14, 'Aruba', 'AW', 'ABW', 297, 0, 0, 0),
(15, 'Australia', 'AU', 'AUS', 61, 0, 0, 0),
(16, 'Austria', 'AT', 'AUT', 43, 0, 1, 0),
(17, 'Azerbaijan', 'AZ', 'AZE', 994, 0, 0, 0),
(18, 'Bahamas', 'BS', 'BHS', 1242, 0, 0, 0),
(19, 'Bahrain', 'BH', 'BHR', 973, 0, 0, 0),
(20, 'Bangladesh', 'BD', 'BGD', 880, 0, 0, 0),
(21, 'Barbados', 'BB', 'BRB', 1246, 0, 0, 0),
(22, 'Belarus', 'BY', 'BLR', 375, 0, 0, 0),
(23, 'Belgium', 'BE', 'BEL', 32, 0, 1, 0),
(24, 'Belize', 'BZ', 'BLZ', 501, 0, 0, 0),
(25, 'Benin', 'BJ', 'BEN', 229, 0, 0, 0),
(26, 'Bermuda', 'BM', 'BMU', 1441, 0, 0, 0),
(27, 'Bhutan', 'BT', 'BTN', 975, 0, 0, 0),
(28, 'Bolivia', 'BO', 'BOL', 591, 0, 0, 0),
(29, 'Bosnia and Herzegovina', 'BA', 'BIH', 387, 0, 0, 0),
(30, 'Botswana', 'BW', 'BWA', 267, 0, 0, 0),
(31, 'Bouvet Island', 'BV', '', 44, 0, 0, 0),
(32, 'Brazil', 'BR', 'BRA', 55, 0, 0, 0),
(33, 'British lndian Ocean Territory', 'IO', 'IOT', 0, 0, 0, 0),
(34, 'Brunei Darussalam', 'BN', 'BRN', 673, 0, 0, 0),
(35, 'Bulgaria', 'BG', 'BGR', 359, 0, 1, 0),
(36, 'Burkina Faso', 'BF', 'BFA', 226, 0, 0, 0),
(37, 'Burundi', 'BI', 'BDI', 257, 0, 0, 0),
(38, 'Cambodia', 'KH', 'KHM', 855, 0, 0, 0),
(39, 'Cameroon', 'CM', 'CMR', 237, 0, 0, 0),
(40, 'Cape Verde', 'CV', 'CPV', 238, 0, 0, 0),
(41, 'Cayman Islands', 'KY', 'CYM', 1345, 0, 0, 0),
(42, 'Central African Republic', 'CF', 'CAF', 236, 0, 0, 0),
(43, 'Chad', 'TD', 'TCD', 235, 0, 0, 0),
(44, 'Chile', 'CL', 'CHL', 56, 0, 0, 0),
(45, 'China', 'CN', 'CHN', 86, 0, 0, 0),
(46, 'Christmas Island', 'CX', 'CXR', 61, 0, 0, 0),
(47, 'Cocos (Keeling) Islands', 'CC', 'CCK', 61, 0, 0, 0),
(48, 'Colombia', 'CO', 'COL', 57, 0, 0, 0),
(49, 'Comoros', 'KM', 'COM', 269, 0, 0, 0),
(50, 'Congo', 'CG', 'COG', 242, 0, 0, 0),
(51, 'Cook Islands', 'CK', 'COK', 682, 0, 0, 0),
(52, 'Costa Rica', 'CR', 'CRC', 506, 0, 0, 0),
(53, 'Croatia (Hrvatska)', 'HR', 'HRV', 385, 0, 0, 0),
(54, 'Cuba', 'CU', 'CUB', 53, 0, 0, 0),
(55, 'Cyprus', 'CY', 'CYP', 357, 0, 1, 0),
(56, 'Czech Republic', 'CZ', 'CZE', 420, 0, 1, 0),
(57, 'Denmark', 'DK', 'DNK', 45, 0, 1, 0),
(58, 'Djibouti', 'DJ', 'DJI', 253, 0, 0, 0),
(59, 'Dominica', 'DM', 'DMA', 1767, 0, 0, 0),
(60, 'Dominican Republic', 'DO', 'DOM', 1809, 0, 0, 0),
(61, 'East Timor', 'TP', '', 44, 0, 0, 0),
(62, 'Ecuador', 'EC', 'ECU', 593, 0, 0, 0),
(63, 'Egypt', 'EG', 'EGY', 20, 0, 0, 0),
(64, 'El Salvador', 'SV', 'SLV', 503, 0, 0, 0),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 240, 0, 0, 0),
(66, 'Eritrea', 'ER', 'ERI', 291, 0, 0, 0),
(67, 'Estonia', 'EE', 'EST', 372, 0, 1, 0),
(68, 'Ethiopia', 'ET', 'ETH', 251, 0, 0, 0),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', 500, 0, 0, 0),
(70, 'Faroe Islands', 'FO', 'FRO', 298, 0, 0, 0),
(71, 'Fiji', 'FJ', 'FJI', 679, 0, 0, 0),
(72, 'Finland', 'FI', 'FIN', 358, 0, 1, 0),
(73, 'France', 'FR', 'FRA', 33, 0, 1, 0),
(74, 'France, Metropolitan', 'FX', '', 44, 0, 0, 0),
(75, 'French Guiana', 'GF', '', 44, 0, 0, 0),
(76, 'French Polynesia', 'PF', 'PYF', 689, 0, 0, 0),
(77, 'French Southern Territories', 'TF', '', 44, 0, 0, 0),
(78, 'Gabon', 'GA', 'GAB', 241, 0, 0, 0),
(79, 'Gambia', 'GM', 'GMB', 220, 0, 0, 0),
(80, 'Georgia', 'GE', 'GEO', 995, 0, 0, 0),
(81, 'Germany', 'DE', 'DEU', 49, 0, 1, 0),
(82, 'Ghana', 'GH', 'GHA', 233, 0, 0, 0),
(83, 'Gibraltar', 'GI', 'GIB', 350, 0, 0, 0),
(84, 'Greece', 'GR', 'GRC', 30, 0, 1, 0),
(85, 'Greenland', 'GL', 'GRL', 299, 0, 0, 0),
(86, 'Grenada', 'GD', 'GRD', 1473, 0, 0, 0),
(87, 'Guadeloupe', 'GP', '', 44, 0, 0, 0),
(88, 'Guam', 'GU', 'GUM', 1671, 0, 0, 0),
(89, 'Guatemala', 'GT', 'GTM', 502, 0, 0, 0),
(90, 'Guinea', 'GN', 'GIN', 224, 0, 0, 0),
(91, 'Guinea-Bissau', 'GW', 'GNB', 245, 0, 0, 0),
(92, 'Guyana', 'GY', 'GUY', 592, 0, 0, 0),
(93, 'Haiti', 'HT', 'HTI', 509, 0, 0, 0),
(94, 'Heard and Mc Donald Islands', 'HM', '', 44, 0, 0, 0),
(95, 'Honduras', 'HN', 'HND', 504, 0, 0, 0),
(96, 'Hong Kong', 'HK', 'HKG', 852, 0, 0, 0),
(97, 'Hungary', 'HU', 'HUN', 36, 0, 1, 0),
(98, 'Iceland', 'IS', 'IS', 354, 0, 0, 0),
(99, 'India', 'IN', 'IND', 91, 0, 0, 0),
(100, 'Indonesia', 'ID', 'IDN', 62, 0, 0, 0),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', 98, 0, 0, 0),
(102, 'Iraq', 'IQ', 'IRQ', 964, 0, 0, 0),
(103, 'Ireland', 'IE', 'IRL', 353, 0, 1, 0),
(104, 'Israel', 'IL', 'ISR', 972, 0, 0, 0),
(105, 'Italy', 'IT', 'ITA', 39, 0, 1, 0),
(106, 'Ivory Coast', 'CI', 'CIV', 225, 0, 0, 0),
(107, 'Jamaica', 'JM', 'JAM', 1876, 0, 0, 0),
(108, 'Japan', 'JP', 'JPN', 81, 0, 0, 0),
(109, 'Jordan', 'JO', 'JOR', 962, 0, 0, 0),
(110, 'Kazakhstan', 'KZ', 'KAZ', 7, 0, 0, 0),
(111, 'Kenya', 'KE', 'KEN', 254, 0, 0, 0),
(112, 'Kiribati', 'KI', 'KIR', 686, 0, 0, 0),
(113, 'Korea, Democratic People\'s Republic of', 'KP', 'PRK', 850, 0, 0, 0),
(114, 'Korea, Republic of', 'KR', 'KOR', 82, 0, 0, 0),
(115, 'Kuwait', 'KW', 'KWT', 965, 0, 0, 0),
(116, 'Kyrgyzstan', 'KG', 'KGZ', 996, 0, 0, 0),
(117, 'Lao People\'s Democratic Republic', 'LA', 'LAO', 856, 0, 0, 0),
(118, 'Latvia', 'LV', 'LVA', 371, 0, 1, 0),
(119, 'Lebanon', 'LB', 'LBN', 961, 0, 0, 0),
(120, 'Lesotho', 'LS', 'LSO', 266, 0, 0, 0),
(121, 'Liberia', 'LR', 'LBR', 231, 0, 0, 0),
(122, 'Libyan Arab Jamahiriya', 'LY', 'LBY', 218, 0, 0, 0),
(123, 'Liechtenstein', 'LI', 'LIE', 423, 0, 0, 0),
(124, 'Lithuania', 'LT', 'LTU', 370, 0, 1, 0),
(125, 'Luxembourg', 'LU', 'LUX', 352, 0, 1, 0),
(126, 'Macau', 'MO', 'MAC', 853, 0, 0, 0),
(127, 'Macedonia', 'MK', 'MKD', 389, 0, 0, 0),
(128, 'Madagascar', 'MG', 'MDG', 261, 0, 0, 0),
(129, 'Malawi', 'MW', 'MWI', 265, 0, 0, 0),
(130, 'Malaysia', 'MY', 'MYS', 60, 0, 0, 0),
(131, 'Maldives', 'MV', 'MDV', 960, 0, 0, 0),
(132, 'Mali', 'ML', 'MLI', 223, 0, 0, 0),
(133, 'Malta', 'MT', 'MLT', 356, 0, 1, 0),
(134, 'Marshall Islands', 'MH', 'MHL', 692, 0, 0, 0),
(135, 'Martinique', 'MQ', '', 44, 0, 0, 0),
(136, 'Mauritania', 'MR', 'MRT', 222, 0, 0, 0),
(137, 'Mauritius', 'MU', 'MUS', 230, 0, 0, 0),
(138, 'Mayotte', 'TY', 'MYT', 262, 0, 0, 0),
(139, 'Mexico', 'MX', 'MEX', 52, 0, 0, 0),
(140, 'Micronesia, Federated States of', 'FM', 'FSM', 691, 0, 0, 0),
(141, 'Moldova, Republic of', 'MD', 'MDA', 373, 0, 0, 0),
(142, 'Monaco', 'MC', 'MCO', 377, 0, 0, 0),
(143, 'Mongolia', 'MN', 'MNG', 976, 0, 0, 0),
(144, 'Montserrat', 'MS', 'MSR', 1664, 0, 0, 0),
(145, 'Morocco', 'MA', 'MAR', 212, 0, 0, 0),
(146, 'Mozambique', 'MZ', 'MOZ', 258, 0, 0, 0),
(147, 'Myanmar', 'MM', 'MMR', 95, 0, 0, 0),
(148, 'Namibia', 'NA', 'NAM', 264, 0, 0, 0),
(149, 'Nauru', 'NR', 'NRU', 674, 0, 0, 0),
(150, 'Nepal', 'NP', 'NPL', 977, 0, 0, 0),
(151, 'Netherlands', 'NL', 'NLD', 31, 0, 1, 0),
(152, 'Netherlands Antilles', 'AN', 'ANT', 599, 0, 0, 0),
(153, 'New Caledonia', 'NC', 'NCL', 687, 0, 0, 0),
(154, 'New Zealand', 'NZ', 'NZL', 64, 0, 0, 0),
(155, 'Nicaragua', 'NI', 'NIC', 505, 0, 0, 0),
(156, 'Niger', 'NE', 'NER', 227, 0, 0, 0),
(157, 'Nigeria', 'NG', 'NGA', 234, 0, 0, 0),
(158, 'Niue', 'NU', 'NIU', 683, 0, 0, 0),
(159, 'Norfork Island', 'NF', '', 44, 0, 0, 0),
(160, 'Northern Mariana Islands', 'MP', 'MNP', 1670, 0, 0, 0),
(161, 'Norway', 'NO', 'NOR', 47, 0, 0, 0),
(162, 'Oman', 'OM', 'OMN', 968, 0, 0, 0),
(163, 'Pakistan', 'PK', 'PAK', 92, 0, 0, 0),
(164, 'Palau', 'PW', 'PLW', 680, 0, 0, 0),
(165, 'Panama', 'PA', 'PAN', 507, 0, 0, 0),
(166, 'Papua New Guinea', 'PG', 'PNG', 675, 0, 0, 0),
(167, 'Paraguay', 'PY', 'PRY', 595, 0, 0, 0),
(168, 'Peru', 'PE', 'PER', 51, 0, 0, 0),
(169, 'Philippines', 'PH', 'PHL', 63, 0, 0, 0),
(170, 'Pitcairn', 'PN', 'PCN', 870, 0, 0, 0),
(171, 'Poland', 'PL', 'POL', 48, 0, 1, 0),
(172, 'Portugal', 'PT', 'PRT', 351, 0, 1, 0),
(173, 'Puerto Rico', 'PR', 'PRI', 1, 0, 0, 0),
(174, 'Qatar', 'QA', 'QAT', 974, 0, 0, 0),
(175, 'Reunion', 'RE', '', 44, 0, 0, 0),
(176, 'Romania', 'RO', 'ROU', 40, 0, 1, 0),
(177, 'Russian Federation', 'RU', 'RUS', 7, 0, 0, 99),
(178, 'Rwanda', 'RW', 'RWA', 250, 0, 0, 0),
(179, 'Saint Kitts and Nevis', 'KN', 'KNA', 1869, 0, 0, 0),
(180, 'Saint Lucia', 'LC', 'LCA', 1758, 0, 0, 0),
(181, 'Saint Vincent and the Grenadines', 'VC', 'VCT', 1784, 0, 0, 0),
(182, 'Samoa', 'WS', 'WSM', 685, 0, 0, 0),
(183, 'San Marino', 'SM', 'SMR', 378, 0, 0, 0),
(184, 'Sao Tome and Principe', 'ST', 'STP', 239, 0, 0, 0),
(185, 'Saudi Arabia', 'SA', 'SAU', 966, 0, 0, 0),
(186, 'Senegal', 'SN', 'SEN', 221, 0, 0, 0),
(187, 'Seychelles', 'SC', 'SYC', 248, 0, 0, 0),
(188, 'Sierra Leone', 'SL', 'SLE', 232, 0, 0, 0),
(189, 'Singapore', 'SG', 'SGP', 65, 0, 0, 0),
(190, 'Slovakia', 'SK', 'SVK', 421, 0, 1, 0),
(191, 'Slovenia', 'SI', 'SVN', 386, 0, 1, 0),
(192, 'Solomon Islands', 'SB', 'SLB', 677, 0, 0, 0),
(193, 'Somalia', 'SO', 'SOM', 252, 0, 0, 0),
(194, 'South Africa', 'ZA', 'ZAF', 27, 0, 0, 0),
(195, 'South Georgia South Sandwich Islands', 'GS', '', 44, 0, 0, 0),
(196, 'Spain', 'ES', 'ESP', 34, 0, 1, 0),
(197, 'Sri Lanka', 'LK', 'LKA', 94, 0, 0, 0),
(198, 'St. Helena', 'SH', 'SHN', 290, 0, 0, 0),
(199, 'St. Pierre and Miquelon', 'PM', 'SPM', 508, 0, 0, 0),
(200, 'Sudan', 'SD', 'SDN', 249, 0, 0, 0),
(201, 'Suriname', 'SR', 'SUR', 597, 0, 0, 0),
(202, 'Svalbarn and Jan Mayen Islands', 'SJ', 'SJM', 0, 0, 0, 0),
(203, 'Swaziland', 'SZ', 'SWZ', 268, 0, 0, 0),
(204, 'Sweden', 'SE', 'SWE', 46, 0, 1, 0),
(205, 'Switzerland', 'CH', 'CHE', 41, 0, 0, 0),
(206, 'Syrian Arab Republic', 'SY', 'SYR', 963, 0, 0, 0),
(207, 'Taiwan', 'TW', 'TWN', 886, 0, 0, 0),
(208, 'Tajikistan', 'TJ', 'TJK', 992, 0, 0, 0),
(209, 'Tanzania, United Republic of', 'TZ', 'TZA', 255, 0, 0, 0),
(210, 'Thailand', 'TH', 'THA', 66, 0, 0, 0),
(211, 'Togo', 'TG', 'TGO', 228, 0, 0, 0),
(212, 'Tokelau', 'TK', 'TKL', 690, 0, 0, 0),
(213, 'Tonga', 'TO', 'TON', 676, 0, 0, 0),
(214, 'Trinidad and Tobago', 'TT', 'TTO', 1868, 0, 0, 0),
(215, 'Tunisia', 'TN', 'TUN', 216, 0, 0, 0),
(216, 'Turkey', 'TR', 'TUR', 90, 0, 0, 0),
(217, 'Turkmenistan', 'TM', 'TKM', 993, 0, 0, 0),
(218, 'Turks and Caicos Islands', 'TC', 'TCA', 1649, 0, 0, 0),
(219, 'Tuvalu', 'TV', 'TUV', 688, 0, 0, 0),
(220, 'Uganda', 'UG', 'UGA', 256, 0, 0, 0),
(221, 'Ukraine', 'UA', 'UKR', 380, 0, 0, 70),
(222, 'United Arab Emirates', 'AE', 'ARE', 971, 0, 0, 0),
(223, 'United Kingdom', 'GB', 'GBR', 44, 1, 1, 0),
(224, 'United States minor outlying islands', 'UM', '', 44, 0, 0, 0),
(225, 'Uruguay', 'UY', 'URY', 598, 0, 0, 0),
(226, 'Uzbekistan', 'UZ', 'UZB', 998, 0, 0, 0),
(227, 'Vanuatu', 'VU', 'VUT', 678, 0, 0, 0),
(228, 'Vatican City State', 'VA', 'VAT', 39, 0, 0, 0),
(229, 'Venezuela', 'VE', 'VEN', 58, 0, 0, 0),
(230, 'Vietnam', 'VN', 'VNM', 84, 0, 0, 0),
(231, 'Virigan Islands (British)', 'VG', 'VGB', 1284, 0, 0, 0),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', 1340, 0, 0, 0),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', 681, 0, 0, 0),
(234, 'Western Sahara', 'EH', 'ESH', 0, 0, 0, 0),
(235, 'Yemen', 'YE', 'YEM', 967, 0, 0, 0),
(236, 'Yugoslavia', 'YU', '', 44, 0, 0, 0),
(237, 'Zaire', 'ZR', '', 44, 0, 0, 0),
(238, 'Zambia', 'ZM', 'ZMB', 260, 0, 0, 0),
(239, 'Zimbabwe', 'ZW', 'ZWE', 263, 0, 0, 0),
(240, 'Kosovo', 'XK', '', 381, 0, 0, 0),
(243, 'Montenegro', 'ME', 'MNE', 382, 0, 0, 0),
(386, 'Serbia', 'RS', 'SRB', 381, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `e164`
--

CREATE TABLE `e164` (
  `id` int(11) NOT NULL,
  `country_code` smallint(3) NOT NULL,
  `region_code` smallint(4) NOT NULL,
  `phone_number` bigint(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `e164`
--

INSERT INTO `e164` (`id`, `country_code`, `region_code`, `phone_number`) VALUES
(44, 7, 910, 3300824),
(45, 7, 910, 3300824);

-- --------------------------------------------------------

--
-- Структура таблицы `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `is_verified` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `email`
--

INSERT INTO `email` (`id`, `email`, `is_verified`) VALUES
(44, 'vvzone@yandex.ru', NULL),
(45, 'vvzone@yandex.ru', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `phone`
--

CREATE TABLE `phone` (
  `id` int(11) NOT NULL,
  `user_enter` varchar(50) NOT NULL,
  `comment` tinytext,
  `is_verified` tinyint(1) DEFAULT NULL,
  `e164_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `phone_number` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `phone`
--

INSERT INTO `phone` (`id`, `user_enter`, `comment`, `is_verified`, `e164_id`, `country_id`, `phone_number`) VALUES
(44, '79103300824', NULL, NULL, 44, 177, '9103300824'),
(45, '79103300824', NULL, NULL, 45, 177, '9103300824');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `login` varchar(45) NOT NULL,
  `password` mediumtext NOT NULL,
  `phone_id` int(11) DEFAULT NULL,
  `email_id` int(11) DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `phone_id`, `email_id`, `is_banned`, `created`) VALUES
(43, 'vvzone', 'x12345x', 44, 44, 0, '2017-07-06 00:00:00'),
(44, 'vvzone2', 'x12345x', 45, 45, 0, '2017-07-06 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `user_has_code`
--

CREATE TABLE `user_has_code` (
  `user_id` int(11) NOT NULL,
  `code_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `code`
--
ALTER TABLE `code`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_code_code_type1_idx` (`code_type_id`),
  ADD KEY `fk_code_code_process_type1_idx` (`code_process_type_id`),
  ADD KEY `IDX_77153098A76ED395` (`user_id`);

--
-- Индексы таблицы `code_process_type`
--
ALTER TABLE `code_process_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `code_type`
--
ALTER TABLE `code_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `e164`
--
ALTER TABLE `e164`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `phone`
--
ALTER TABLE `phone`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_phone_e1641_idx` (`e164_id`) USING BTREE,
  ADD KEY `IDX_444F97DDF92F3E70` (`country_id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_phone1_idx` (`phone_id`),
  ADD KEY `fk_user_email1_idx` (`email_id`);

--
-- Индексы таблицы `user_has_code`
--
ALTER TABLE `user_has_code`
  ADD PRIMARY KEY (`user_id`,`code_id`),
  ADD KEY `fk_user_has_code_code1_idx` (`code_id`),
  ADD KEY `fk_user_has_code_user1_idx` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `code`
--
ALTER TABLE `code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `code_process_type`
--
ALTER TABLE `code_process_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `code_type`
--
ALTER TABLE `code_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;
--
-- AUTO_INCREMENT для таблицы `e164`
--
ALTER TABLE `e164`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT для таблицы `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT для таблицы `phone`
--
ALTER TABLE `phone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `code`
--
ALTER TABLE `code`
  ADD CONSTRAINT `FK_77153098A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_code_code_process_type1` FOREIGN KEY (`code_process_type_id`) REFERENCES `code_process_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_code_code_type1` FOREIGN KEY (`code_type_id`) REFERENCES `code_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `phone`
--
ALTER TABLE `phone`
  ADD CONSTRAINT `FK_444F97DD43EC56C6` FOREIGN KEY (`e164_id`) REFERENCES `e164` (`id`),
  ADD CONSTRAINT `FK_444F97DDF92F3E70` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D6493B7323CB` FOREIGN KEY (`phone_id`) REFERENCES `phone` (`id`),
  ADD CONSTRAINT `FK_8D93D649A832C1C9` FOREIGN KEY (`email_id`) REFERENCES `email` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_has_code`
--
ALTER TABLE `user_has_code`
  ADD CONSTRAINT `fk_user_has_code_code1` FOREIGN KEY (`code_id`) REFERENCES `code` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_has_code_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
