#!/bin/bash

color_output() {
  echo -e "\033[$2m$1\033[0m"
}

title_label() {
  color_output "$1" "$START_END_SCRIPT_COLOR"
}

START_END_SCRIPT_COLOR=37
START_TASK_COLOR=33
END_COLOR=36
INTERACTION=32

################################
# script execution starts here #
################################

echo ""
color_output "-> Rollback..." "$START_TASK_COLOR"
./rollback.sh
echo ""

dir=$(cd "./vendor/phpunit/phpunit" && pwd)

"php" "-d" "memory_limit=1G" "${dir}/phpunit" "$@"