<?php
namespace ModuleMessage;

use Zend\Router\Http\Segment;

$message_single = [
    'type' => Segment::class,
    'options' => [
        'route' => '/message/:id',
        'constraints' => [
            'id' => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\MessageController::class,
        ],
    ]
];

return [
    'router' => [
        'routes' => [
            'message-single' => $message_single,
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\MessageController::class => Controller\Factory\MessageControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\MessageManager::class => Service\Factory\MessageManagerFactory::class,
            Service\Rbac\RbacMessageAssertionManager::class => Service\Rbac\Factory\RbacMessageAssertionManagerFactory::class
        ],
    ],
    'controller_plugins' => [
    ],
    // The 'access_filter' key is used by the File module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            'mode' => 'permissive',     // все, что не запрещено, разрешено
            //'mode' => 'restrictive',  // все, что не разрешено, запрещено
        ],
    ],
    'view_manager' => [
        'template_map' => [

        ],
        'template_path_stack' => [
            'ModuleMessage' => __DIR__ . '/../viewTwig',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Entity')
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' =>  __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    // Rbac Assertion Managers
    'rbac_manager' => [
        'assertions' => [
            Service\Rbac\RbacMessageAssertionManager::class,
        ],
    ],
];
