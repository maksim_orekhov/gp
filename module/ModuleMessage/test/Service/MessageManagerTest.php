<?php

namespace ModuleMessageTest\Service;

use Application\Entity\Deal;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use ModuleMessage\Entity\Message;
use ModuleMessage\Service\MessageManager;
use Application\Entity\User;

/**
 * Class MessageManagerTest
 * @package ModuleMessageTest\Service
 */
class MessageManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var MessageManager
     */
    private $messageManager;


    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        //$configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                include __DIR__ . '/../../../../config/application.config.php',
                include __DIR__ . '/../../../../config/autoload/global.php'
            ),
            include __DIR__ . '/../../../../config/autoload/local.php'
        ));

        parent::setUp();

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->messageManager = $serviceManager->get(MessageManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSaveMessage()
    {
        /** @var User $author */ // Получаем тестового пользователя (автора сообщения)
        $author = $this->getTestUserByLogin();

        $text = "Testing save message with test text";

        /** @var Message $result */
        $result = $this->messageManager->saveMessage($text, $author);

        $this->assertNotNull($result->getId());
        $this->assertEquals($text, $result->getText());
        $this->assertEquals($author, $result->getAuthor());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetMessageById()
    {
        /** @var User $author */ // Получаем тестового пользователя (автора сообщения)
        $author = $this->getTestUserByLogin();

        $text = "New message creation test";

        /** @var Message $createdMessage */
        $createdMessage = $this->messageManager->saveMessage($text, $author);
        /** @var Message $selectedMessage */
        $selectedMessage = $this->messageManager->getMessageById($createdMessage->getId());

        $this->assertEquals($text, $selectedMessage->getText());
        $this->assertEquals($author, $selectedMessage->getAuthor());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @expectedException \Exception
     * @expectedExceptionMessage Message not found
     */
    public function testRemoveMessage()
    {
        /** @var User $author */ // Получаем тестового пользователя (автора сообщения)
        $author = $this->getTestUserByLogin();

        $text = "New message creation test";

        /** @var Message $createdMessage */
        $createdMessage = $this->messageManager->saveMessage($text, $author);
        /** @var Message $selectedMessage */
        $selectedMessage = $this->messageManager->getMessageById($createdMessage->getId());

        $message_id = $selectedMessage->getId();

        $this->assertEquals($text, $selectedMessage->getText());
        $this->assertEquals($author, $selectedMessage->getAuthor());

        $this->messageManager->removeMessage($selectedMessage);

        // Try to get removed Message. Must throw Exception.
        $removed = $this->messageManager->getMessageById($message_id);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetMessageOutput()
    {
        /** @var User $author */ // Получаем тестового пользователя (автора сообщения)
        $author = $this->getTestUserByLogin();

        $text = "New message creation test";

        /** @var Message $createdMessage */
        $createdMessage = $this->messageManager->saveMessage($text, $author);
        /** @var array $messageOutput */
        $messageOutput = $this->messageManager->getMessageOutput($createdMessage->getId());

        $this->assertEquals($createdMessage->getId(), $messageOutput['id']);
        $this->assertEquals($text, $messageOutput['text']);
        $this->assertEquals($author->getLogin(), $messageOutput['author']);
        $this->assertNotNull($messageOutput['created']);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSetMessageToDeal()
    {
        $deal_name = 'Сделка Фикстура';
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        /** @var User $author */ // Получаем тестового пользователя (автора сообщения)
        $author = $this->getTestUserByLogin();

        $text = "New message for Deal creation test";

        /** @var Message $createdMessage */
        $createdMessage = $this->messageManager->saveMessage($text, $author);

        // Сообщений нет
        $this->assertNotNull(count($deal->getMessages()));

        $deal->addMessage($createdMessage);
        $this->entityManager->flush();

        /* @var Deal $updatedDeal */
        $updatedDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        // Уже есть одно сообщение
        $this->assertEquals(1, count($updatedDeal->getMessages()));
    }




    /**
     * @param string $login
     * @return null|object
     */
    private function getTestUserByLogin($login = 'test')
    {
        /** @var User $user */
        $user = $this->entityManager
            ->getRepository(User::class)->findOneBy(array('login' => $login));

        return $user;
    }
}
