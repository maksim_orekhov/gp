<?php

namespace ModuleMessage\Entity\Repository;

use Application\Entity\Deal;
use Application\Entity\Dispute;
use Application\Entity\Repository\BaseRepository;
use ModuleMessage\Entity\Message;
use Application\Entity\Role;

/**
 * Class MessageRepository
 * @package ModuleMessage\Entity\Repository
 */
class MessageRepository extends BaseRepository
{

    /**
     * @param Deal $deal
     * @param \Application\Entity\Role $role
     * @return array
     */
    public function getDealOperatorsComments(Deal $deal, Role $role)
    {
        $deal_ids = [];
        /** @var Message $message */
        foreach ($deal->getMessages() as $message) {
            $deal_ids[] = $message->getId();
        }

        if ($deal_ids) {
            $qb = $this->createQueryBuilder('m')
                ->addSelect('author')
                ->leftJoin('m.author', 'author')
                ->andWhere('m.id IN (:deal_ids)')
                ->andWhere(':role MEMBER OF author.roles')
                ->setParameter('deal_ids', $deal_ids)
                ->setParameter('role', $role)
                ->addOrderBy('m.id', 'DESC')
            ;

            return $qb->getQuery()->getResult();
        }

        return null;
    }

    /**
     * @param Dispute $dispute
     * @param Role $role
     * @return array|null
     */
    public function getDisputeOperatorsComments(Dispute $dispute, Role $role)
    {
        $dispute_ids = [];
        /** @var Message $message */
        foreach ($dispute->getMessages() as $message) {
            $dispute_ids[] = $message->getId();
        }

        if ($dispute_ids) {
            $qb = $this->createQueryBuilder('m')
                ->addSelect('author')
                ->leftJoin('m.author', 'author')
                ->andWhere('m.id IN (:dispute_ids)')
                ->andWhere(':role MEMBER OF author.roles')
                ->setParameter('dispute_ids', $dispute_ids)
                ->setParameter('role', $role)
                ->addOrderBy('m.id', 'DESC')
            ;

            return $qb->getQuery()->getResult();
        }

        return null;
    }
}