<?php

namespace ModuleMessage\Controller;

use Core\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use ModuleMessage\Service\MessageManager;

/**
 * Class MessageController
 * @package ModuleMessage\Controller
 */
class MessageController extends AbstractRestfulController
{
    const ERROR_CREATE_MESSAGE    = 'Could not create message object';
    const ERROR_DATA_PROVIDE      = 'Data was not provided';
    const ERROR_NOT_FOUND         = 'Message not found';

    /**
     * @var MessageManager
     */
    private $messageManager;

    /**
     * @var array
     */
    private $config;

    public function __construct(MessageManager $messageManager, array $config)
    {
        $this->messageManager = $messageManager;
        $this->config      = $config;
    }

    /**
     * @param mixed $id
     * @return \Zend\Http\Response|\Zend\Stdlib\ResponseInterface|ViewModel
     *
     * @TODO Не проверен. Теста нет. Возможно, не понадобится.
     */
    public function get($id)
    {
        try {
            $messageOutput = $this->messageManager->getMessageOutput($id);
        }
        catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        // Ajax success
        if ($this->getRequest()->isXmlHttpRequest()) {
            return $this->message()->success('show message',[
                'message' => json_encode($messageOutput)
            ]);
        }
        // Http success
        $view = new ViewModel([
            'message' => $messageOutput,
        ]);
        $template = "message/message/single";
        $view->setTemplate($template);

        return $view;
    }
}
