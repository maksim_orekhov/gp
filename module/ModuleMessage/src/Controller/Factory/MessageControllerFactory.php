<?php
namespace ModuleMessage\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleMessage\Controller\MessageController;
use ModuleMessage\Service\MessageManager;

/**
 * Class MessageControllerFactory
 * @package ModuleFileManager\Controller\Factory
 */
class MessageControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $messageManager = $container->get(MessageManager::class);
        $config = $container->get('Config');

        return new MessageController($messageManager, $config);
    }
}