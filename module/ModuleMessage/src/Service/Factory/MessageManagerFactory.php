<?php
namespace ModuleMessage\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleMessage\Service\MessageManager;

/**
 * Class MessageManagerFactory
 * @package ModuleFileManager\Service\Factory
 */
class MessageManagerFactory implements FactoryInterface
{
    /**
     * This method creates the FileManager service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');


        return new MessageManager(
            $entityManager
        );
    }
}