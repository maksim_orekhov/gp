<?php
namespace ModuleMessage\Service\Rbac;

use Zend\Permissions\Rbac\Rbac;
use Application\Service\UserManager;
use Core\Service\Auth\AuthService;

/**
 * This service is used for invoking user-defined RBAC dynamic assertions
 */
class RbacMessageAssertionManager
{
    /**
     * Entity manager.
     * @var UserManager
     */
    private $userManager;

    /**
     * Auth service.
     * @var AuthService
     */
    private $authService;

    /**
     * RbacMessageAssertionManager constructor.
     * @param UserManager $userManager
     * @param AuthService $authService
     */
    public function __construct(UserManager $userManager, AuthService $authService)
    {
        $this->userManager = $userManager;
        $this->authService = $authService;
    }

    /**
     * For dynamic assertions
     *
     * @param Rbac      $rbac
     * @param string    $permission
     * @param array     $params
     * @return bool
     */
    public function assert(Rbac $rbac, $permission, $params)
    {
        if( !array_key_exists('user', $params) || !array_key_exists('comment', $params)) {

            return false;
        }
        // Редактирование комментария
        if ($permission == 'message.own.edit') {

            if ($params['user'] === $params['comment']->getAuthor()) {

                return true;
            }
        }

        // Удалитение комментария
        if ($permission == 'message.own.delete') {

            if ($params['user'] === $params['comment']->getAuthor()) {

                return true;
            }
        }

        return false;
    }
}