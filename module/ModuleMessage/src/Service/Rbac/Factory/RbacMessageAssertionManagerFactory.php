<?php
namespace ModuleMessage\Service\Rbac\Factory;

use Interop\Container\ContainerInterface;
use ModuleMessage\Service\Rbac\RbacMessageAssertionManager;
use Application\Service\UserManager as UserManager;
use Core\Service\Auth\AuthService;

/**
 * This is the factory class for RbacAssertionManager service
 */
class RbacMessageAssertionManagerFactory
{
    /**
     * This method creates the RbacAssertionManager service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $userManager = $container->get(UserManager::class);
        $authService = $container->get(AuthService::class);

        return new RbacMessageAssertionManager($userManager, $authService);
    }
}