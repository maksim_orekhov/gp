<?php
namespace ModuleMessage\Service;

use Doctrine\Common\Collections\ArrayCollection;
use ModuleFileManager\Entity\File;
use ModuleMessage\Entity\Message;
use Doctrine\ORM\EntityManager;
use Application\Entity\User;

/**
 * Class MessageManager
 * @package ModuleMessage\Service
 */
class MessageManager
{
    const ERROR_NOT_FOUND = 'Message not found';

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;


    /**
     * MessageManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $id
     * @return Message
     * @throws \Exception
     */
    public function getMessageById($id): Message
    {
        /** @var Message $message */
        $message = $this->entityManager->getRepository(Message::class)
            ->findOneById($id);

        if(!$message || !($message instanceof Message)) {

            throw new \Exception(self::ERROR_NOT_FOUND );
        }

        return $message;
    }

    /**
     * @param string $text
     * @param User $author
     * @return Message
     * @throws \Exception
     */
    public function saveMessage(string $text, User $author)
    {
        /** @var Message $message */
        $message = new Message();

        $message->setText($text);
        $message->setAuthor($author);
        $message->setCreated(new \DateTime());

        // Prepare object
        $this->entityManager->persist($message);
        // Save to DB
        $this->entityManager->flush();

        if (!$message->getId()) {

            throw new \Exception('New comment creation failed');
        }

        return $message;
    }

    /**
     * @param Message $message
     * @throws \Exception
     */
    public function removeMessage(Message $message)
    {
        try {
            $this->entityManager->remove($message);

            $this->entityManager->flush();
        }
        catch (\Throwable $t) {

            throw new \Exception('Can not remove message due to: ' . $t->getMessage());
        }
    }

    /**
     * Message object array output
     *
     * @param $id
     * @param null $currentUser
     * @return array
     * @throws \Exception
     */
    public function getMessageOutput($id, $currentUser = null)
    {
        /** @var Message $message */
        $message = $this->getMessageById($id);
        /** @var User $authorUser */
        $authorUser = $message->getAuthor();
        /** @var File $avatar */
        $avatar = $authorUser->getAvatar();

        $messageOutput = [];

        $messageOutput['id']      = $message->getId();
        $messageOutput['text']    = $message->getText();
        $messageOutput['created'] = $message->getCreated()->format('d.m.Y');
        $messageOutput['created_full_time'] = $message->getCreated()->format('d.m.Y H:i');
        $messageOutput['author'] = $authorUser->getLogin();
        $messageOutput['current_user_is_author'] = ($currentUser === $message->getAuthor());

        $messageOutput['avatar'] = null;
        if ($avatar) {
            $messageOutput['avatar'] = [];
            $messageOutput['avatar']['id'] = $avatar->getId();
            $messageOutput['avatar']['name'] = $avatar->getName();
            $messageOutput['avatar']['path'] = $avatar->getPath();
            $messageOutput['avatar']['type'] = $avatar->getType();
            $messageOutput['avatar']['size'] = $avatar->getSize();
            $messageOutput['avatar']['created'] = $avatar->getCreated()->format('d.m.Y');
        }

        return $messageOutput;
    }
}