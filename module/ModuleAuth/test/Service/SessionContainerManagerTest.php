<?php

namespace ModuleAuthTest\Service;

use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class SessionContainerManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \Core\Service\SessionContainerManager
     */
    private $sessionContainerManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        // Add content of global.php and other to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $serviceManager = $this->getApplicationServiceLocator();
        $this->sessionContainerManager = $serviceManager->get(\Core\Service\SessionContainerManager::class);
    }

    protected function tearDown()
    {
        $this->sessionContainerManager->deleteSessionVar('test_var');
        $this->sessionContainerManager->deleteSessionVar('test_array');
        parent::tearDown();
    }

    /**
     * Test setSessionVar and getSessionVar
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSetAndGetSessionVar()
    {
        $this->sessionContainerManager->setSessionVar('test_var', 'test_test');

        $this->assertEquals('test_test', $this->sessionContainerManager->getSessionVar('test_var'));
    }

    /**
     * Test deleteSessionVar
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testDeleteSessionVar()
    {
        $this->sessionContainerManager->setSessionVar('test_var', 'test_test');

        $this->assertEquals('test_test', $this->sessionContainerManager->getSessionVar('test_var'));

        $this->sessionContainerManager->deleteSessionVar('test_var');

        $this->assertEquals(null, $this->sessionContainerManager->getSessionVar('test_var'));
    }

    /**
     * test addElementToSessionArray
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testAddElementToSessionArray()
    {
        if(!$this->sessionContainerManager->getSessionVar('test_array')) {
            $this->sessionContainerManager->setSessionVar('test_array', array());
        }
        // Add elements
        $this->sessionContainerManager->addElementToSessionArray('test_array', 'element_1', 'value_1');
        $this->sessionContainerManager->addElementToSessionArray('test_array', 'element_2', 'value_2');
        $this->sessionContainerManager->addElementToSessionArray('test_array', 'element_3', 'value_3');

        $test_array = $this->sessionContainerManager->getSessionVar('test_array');

        $this->assertEquals('value_1', $test_array['element_1']);
        $this->assertEquals('value_2', $test_array['element_2']);
        $this->assertEquals('value_3', $test_array['element_3']);
        $this->assertCount(3, $test_array);
    }

    /**
     * test removeElementFromSessionArray
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testRemoveElementsFromSessionArray()
    {
        if(!$this->sessionContainerManager->getSessionVar('test_array')) {
            $this->sessionContainerManager->setSessionVar('test_array', array());
        }
        // Add elements
        $this->sessionContainerManager->addElementToSessionArray('test_array', 'element_1', 'value_1');
        $this->sessionContainerManager->addElementToSessionArray('test_array', 'element_2', 'value_2');
        $this->sessionContainerManager->addElementToSessionArray('test_array', 'element_3', 'value_3');
        // Remove one
        $this->sessionContainerManager->removeElementFromSessionArray('test_array', 'element_2');

        $test_array = $this->sessionContainerManager->getSessionVar('test_array');

        $this->assertArrayHasKey('element_1', $test_array);
        $this->assertArrayNotHasKey('element_2', $test_array);
        $this->assertArrayHasKey('element_3', $test_array);
        $this->assertCount(2, $test_array);
    }
}