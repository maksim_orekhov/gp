<?php

namespace ModuleAuthTest\Service;

use PHPUnit\Framework\TestCase;
use Core\Adapter\PhoneNumberUtilAdapter;

class PhoneNumberUtilAdapterTest extends TestCase
{
    // Array of phone numbers [CountryCode, FullNumber, NationalNumber].
    // Addition of new numbers is welcome.
    const PHONE_NUMBERS_FOR_TEST = [
        ['RU', '+79031101518', '9031101518'],
        ['BY', '+375241234567', '241234567'],
        ['UA', '+380991101518', '991101518'],
        ['KZ', '+77751101518', '7751101518'],
        ['US', '+13124123456', '3124123456'],
        ['FR', '+33611971100', '611971100']
    ];

    /**
     * @var PhoneNumberUtilAdapter
     */
    private $phoneNumberAdapter;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        $this->phoneNumberAdapter = new PhoneNumberUtilAdapter();
    }

    public function testGetPhoneNumberObj()
    {
        foreach (self::PHONE_NUMBERS_FOR_TEST as $phoneNumber) {
            $phoneNumberObj = $this->phoneNumberAdapter->getPhoneNumberObj($phoneNumber[1]);
            $this->assertNotEmpty($phoneNumberObj);
        }
    }

    public function testGetCountryCode()
    {
        foreach (self::PHONE_NUMBERS_FOR_TEST as $phoneNumber) {
            $phoneNumberObj = $this->phoneNumberAdapter->getPhoneNumberObj($phoneNumber[1]);
            $countryCode = $this->phoneNumberAdapter->getCountryCode($phoneNumberObj);
            $this->assertEquals($phoneNumber[0], $countryCode);
        }
    }

    public function testGetNationalPhoneNumber()
    {
        foreach (self::PHONE_NUMBERS_FOR_TEST as $phoneNumber) {
            $phoneNumberObj = $this->phoneNumberAdapter->getPhoneNumberObj($phoneNumber[1]);
            $nationalPhoneNumber = $this->phoneNumberAdapter->getNationalPhoneNumber($phoneNumberObj);
            $this->assertEquals($phoneNumber[2], $nationalPhoneNumber);
        }
    }
}