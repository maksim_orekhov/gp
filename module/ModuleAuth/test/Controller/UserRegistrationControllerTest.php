<?php
namespace ModuleAuthTest\Controller;

use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Entity\Interfaces\UserInterface;
use Core\Exception\Code\EmailExceptionCodeInterface;
use Core\Exception\Code\PhoneExceptionCodeInterface;
use Core\Exception\Code\UserExceptionCodeInterface;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseRoleManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\ORMDoctrineUtil;
use Core\Service\SessionContainerManager;
use ModuleAuth\Controller\UserRegistrationController;
use ModuleAuth\Form\UserRegistrationForm;
use Core\Service\Base\BaseAuthManager;
use Application\Entity\Role;
use ModuleAuthTest\Bootstrap;
use Zend\Form\Form;
use Zend\Session\SessionManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use CoreTest\ViewVarsTrait;

class UserRegistrationControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    private $user_login;
    private $user_password;
    private $user_email;
    private $user_phone;

    /**
     * @var
     */
    private $user;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;

    /**
     * @var BasePhoneManager
     */
    private $basePhoneManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var SessionContainerManager
     */
    private $sessionContainerManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;

    /**
     * @var array
     */
    private $config;

    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];
        $this->user_email = $config['tests']['user_email'];
        $this->user_phone = $config['tests']['user_phone'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->baseEmailManager = $serviceManager->get(BaseEmailManager::class);
        $this->basePhoneManager = $serviceManager->get(BasePhoneManager::class);
        $this->sessionContainerManager = $serviceManager->get(SessionContainerManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);
        $this->baseUserManager = $serviceManager->get(BaseUserManager::class);
        $this->config = $serviceManager->get('config');

        $user = $this->baseUserManager->getUserByLogin('test');
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    protected function tearDown()
    {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;
        $this->baseEmailManager = null;
        $this->basePhoneManager = null;
        $this->sessionContainerManager = null;
        $this->baseRoleManager = null;
        $this->baseUserManager = null;

        gc_collect_cycles();
    }

    /**
     * Проверка доступности страницы регистрации и наличия формы
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     *
     */
    public function testUserRegistrationIndexActionCanBeAccessed()
    {
        $this->dispatch('/register', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertMatchedRouteName('register');
        // Что отдается в шаблон
        $this->assertArrayHasKey('userRegistrationForm', $view_vars);
        $this->assertInstanceOf(UserRegistrationForm::class, $view_vars['userRegistrationForm']);
    }

    /**
     * Регистрация с valid Post
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testUserRegistrationWithValidPostAndUppercaseEmail()
    {
        $roleUnverified = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => 'Unverified'));

        $user_login = 'phpunit_test';
        $user_email = 'phpunit_test@gmail.com';
        $user_phone = '+79109109101';

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => $user_phone,
            'email'     => $user_email,
            'password'  => '123456',
            'csrf'      => $csrf
        ];

        $this->dispatch('/register', 'POST', $testUserData );
        /** @var UserInterface $user */
        $user = $this->baseUserManager->getUserByLogin($user_login);

        //// check new user ////
        $this->assertNotNull($user);
        //1. существует email
        $email = $user->getEmail();
        $this->assertNotNull($email);
        //2. существует телефон
        $phone = $user->getPhone();
        $this->assertNotNull($phone);
        //3. существует роль
        $roles = $user->getRoles();
        $this->assertNotEmpty($roles);
        //4. роль должна быть Unverified
        $this->assertContains($roleUnverified, $roles);
        // other checks
        $this->assertSame(strtolower($user_email), $email->getEmail());
        $this->assertSame($user_phone, '+'.$phone->getPhoneNumber());
        $this->assertSame($user_login, $user->getLogin());
        //// end check new user ////

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleAuth');
        /** @noinspection PhpUndefinedMethodInspection */
        $mvcEvent = $this->getApplication()->getMvcEvent();
        /** @noinspection PhpUndefinedMethodInspection */
        $redirect_url = $mvcEvent->getParam('redirect_url', '/login');
        $this->assertRedirectTo($redirect_url);
    }

    /**
     * For Ajax
     * Регистрация с valid Post для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testUserRegistrationWithValidPostForAjax()
    {
        $roleUnverified = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => 'Unverified'));

        $user_login = 'phpunit_test';
        $user_email = 'phpunit_test@gmail.com';
        $user_phone = '+79109109101';

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => $user_phone,
            'email'     => $user_email,
            'password'  => '123456',
            'csrf'      => $csrf
        ];
        $this->getRequest()->setContent(json_encode($testUserData));
        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        /** @var UserInterface $user */
        $user = $this->baseUserManager->getUserByLogin($user_login);

        //// check new user ////
        $this->assertNotNull($user);
        //1. существует email
        $email = $user->getEmail();
        $this->assertNotNull($email);
        //2. существует телефон
        $phone = $user->getPhone();
        $this->assertNotNull($phone);
        //3. существует роль
        $roles = $user->getRoles();
        $this->assertNotEmpty($roles);
        //4. роль должна быть Unverified
        $this->assertContains($roleUnverified, $roles);
        // other checks
        $this->assertSame(strtolower($user_email), $email->getEmail());
        $this->assertSame($user_phone, '+'.$phone->getPhoneNumber());
        $this->assertSame($user_login, $user->getLogin());
        //// end check new user ////

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertSame('SUCCESS', $data['status']);
        $this->assertArrayHasKey('redirect_url', $data['data']);
    }

    /**
     * Попытка зарегистрироваться с неверным csrf
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testUserRegistrationWithInvalidCsrf()
    {
        $user_login = 'phpunit_test';
        $user_email = 'phpunit_test@gmail.com';
        $user_phone = '+79109109101';
        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => $user_phone,
            'email'     => $user_email,
            'password'  => '123456',
            'csrf'      => '12345678901234567890' //неверный csrf
        ];

        $this->dispatch('/register', 'POST', $testUserData );

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertMatchedRouteName('register');
        // Что отдается в шаблон
        $this->assertArrayHasKey('userRegistrationForm', $view_vars);
        $this->assertInstanceOf(UserRegistrationForm::class, $view_vars['userRegistrationForm']);
        /** @var Form $form */
        $form = $view_vars['userRegistrationForm'];
        $message = $form->getMessages();
        $this->assertNotEmpty($message);
        $this->assertArrayHasKey('csrf', $message);
    }

    /**
     * For Ajax
     * Попытка зарегистрироваться с неверным csrf для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testUserRegistrationWithInvalidCsrfForAjax()
    {
        $user_login = 'phpunit_test';
        $user_email = 'phpunit_test@gmail.com';
        $user_phone = '+79109109101';
        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => $user_phone,
            'email'     => $user_email,
            'password'  => '123456',
            'csrf'      => '12345678901234567890' //неверный csrf
        ];

        $this->getRequest()->setContent(json_encode($testUserData));
        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertMatchedRouteName('register');

        $this->assertArrayHasKey('status', $data);
        $this->assertSame('ERROR', $data['status']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    // Variant of invalid data for tests
    public function invalidRegisterData(): array
    {
        return [
            ['te', '79103300811', 'phpunit_test,gmail.com', '123', [
                'login' => 'stringLengthTooShort',
                'phone' => 'invalidPhone',
                'email' => 'regexNotMatch',
                'password' => 'stringLengthTooShort',
            ]],
            ['1test', '+79103300811678956', 'phpunit_test@gmail', 'абвгде', [
                'login' => 'regexNotMatch',
                'phone' => 'invalidPhone',
                'email' => 'regexNotMatch',
                'password' => 'regexNotMatch',
            ]],
            ['12345678901234567', 'yytytytytyt', 'yytytytytyt', '123456789012345678901234567890123456789012345678901234567890123456789', [
                'login' => 'stringLengthTooLong',
                'phone' => 'invalidPhone',
                'email' => 'regexNotMatch',
                'password' => 'stringLengthTooLong',
            ]],
        ];
    }

    /**
     * Попытка регистрации с неверными данными
     *
     * @dataProvider invalidRegisterData
     *
     * @param $user_login
     * @param $user_phone
     * @param $user_email
     * @param $user_pass
     * @param $expected_error
     *
     * @throws \Exception
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserNotRegisteredWithInvalidData($user_login, $user_phone, $user_email, $user_pass, $expected_error)
    {
        $csrfElement = new Element\Csrf('csrf');

        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => $user_phone,
            'email'     => $user_email,
            'password'  => $user_pass,
            'csrf'      => $csrfElement->getValue()
        ];

        $this->dispatch('/register', 'POST', $testUserData );

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertMatchedRouteName('register');
        // Что отдается в шаблон
        $this->assertArrayHasKey('userRegistrationForm', $view_vars);
        $this->assertInstanceOf(UserRegistrationForm::class, $view_vars['userRegistrationForm']);

        /** @var Form $form */
        $form = $view_vars['userRegistrationForm'];
        $message = $form->getMessages();
        $this->assertNotEmpty($message);

        $this->assertArrayHasKey('login', $message);
        $this->assertArrayHasKey('phone', $message);
        $this->assertArrayHasKey('email', $message);

        $this->assertArrayHasKey($expected_error['login'], $message['login']);
        $this->assertArrayHasKey($expected_error['phone'], $message['phone']);
        $this->assertArrayHasKey($expected_error['email'], $message['email']);
        $this->assertArrayHasKey($expected_error['password'], $message['password']);
    }

    /**
     * For Ajax
     * Попытка регистрации с неверными данными для Ajax
     *
     * @dataProvider invalidRegisterData
     *
     * @param $user_login
     * @param $user_phone
     * @param $user_email
     * @param $user_pass
     * @param $expected_error
     *
     * @throws \Exception
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserNotRegisteredWithInvalidDataForAjax($user_login, $user_phone, $user_email, $user_pass, $expected_error)
    {
        $csrfElement = new Element\Csrf('csrf');

        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => $user_phone,
            'email'     => $user_email,
            'password'  => $user_pass,
            'csrf'      => $csrfElement->getValue()
        ];

        $this->getRequest()->setContent(json_encode($testUserData));
        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertMatchedRouteName('register');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('data', $data);

        $this->assertSame('ERROR', $data['status']);
        $this->assertSame(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);

        $this->assertArrayHasKey('login', $data['data']);
        $this->assertArrayHasKey('phone', $data['data']);
        $this->assertArrayHasKey('email', $data['data']);

        $this->assertArrayHasKey($expected_error['login'], $data['data']['login']);
        $this->assertArrayHasKey($expected_error['phone'], $data['data']['phone']);
        $this->assertArrayHasKey($expected_error['email'], $data['data']['email']);
    }

    // Variant of existing data for tests
    public function existingData(): array
    {
        return [
            //1. существует login
            ['test', '+79031111111', 'phpunit_test@gmail.com', UserExceptionCodeInterface::USER_LOGIN_ALREADY_EXISTS_MESSAGE],
            //2. существует phone
            ['phpunit_login', '+79033463906', 'phpunit_test@gmail.com', PhoneExceptionCodeInterface::PHONE_NOT_UNIQUE_MESSAGE],
            //3. существует email
            ['phpunit_login', '+79031111111', 'test@simple-technology.ru', EmailExceptionCodeInterface::EMAIL_ALREADY_EXISTS_MESSAGE],
        ];
    }

    /**
     * Попытка регистрации с валидными данными но с сушествующим phone, email, login
     *
     * @dataProvider existingData
     *
     * @param $user_login
     * @param $user_phone
     * @param $user_email
     * @param $expected_error
     *
     * @throws \Exception
     *
     * @group auth
     */
    public function testUserRegistrationWithValidPostAndExistingPhoneAndEmailAndLogin($user_login, $user_phone, $user_email, $expected_error)
    {
        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => $user_phone,
            'email'     => $user_email,
            'password'  => '123456',
            'csrf'      => $csrfElement->getValue()
        ];

        $this->dispatch('/register', 'POST', $testUserData );

        // проверяем что не создался user, email и phone
        // если user_login === 'test' то не проверяем т.к это существующая сущность
        if ( $user_login !== 'test') {
            $user = $this->baseUserManager->getUserByLogin($user_login);
            $this->assertNull($user);
        }
        // если user_email === 'test@simple-technology.ru' то не проверяем т.к это существующая сущность
        if ( $user_email !== 'test@simple-technology.ru') {
            $email = $this->baseEmailManager->getEmailByValue($user_email);
            $this->assertNull($email);
        }
        // если user_phone === '+79033463906' то не проверяем т.к это существующая сущность
        if ( $user_phone !== '+79033463906') {
            $phone = $this->basePhoneManager->getPhoneByNumber($user_phone);
            $this->assertNull($phone);
        }

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertSame($expected_error, $view_vars['message']);
    }

    /**
     * Проверка не доступности /register/delete неавторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testDeleteActionCanNotBeAccessedByNotAuthorized()
    {
        $this->dispatch('/register/delete', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertRedirectTo('/login?redirectUrl=/register/delete');
    }

    /**
     * Проверка не доступности /register/delete обычному пользователю (Verified)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testDeleteActionCanNotBeAccessedByVerified()
    {
        // Залогиниваем пользователя
        $this->login();

        $this->dispatch('/register/delete', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Проверка недоступности /register/delete Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testDeleteActionCanNotBeAccessedByOperator()
    {
        /** @var UserInterface $user */
        $user = $this->user;
        // Присвоение роли 'Operator' тестовому пользователю
        $this->assignRoleToTestUser('Operator', $this->user);
        // Залогиниваем пользователя
        $this->login($user->getLogin());

        $this->dispatch('/register/delete', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Проверка не доступности /register/delete Администратору если роут выключен в конфиге
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testDeleteActionCanNotBeAccessedByAdministratorIfRouteOffInConfig()
    {
        $this->assertArrayHasKey('delete_user_route', $this->config);
        //проверяем что удаление выключено в конфиге
        $this->assertFalse($this->config['delete_user_route']);

        /** @var UserInterface $user */
        $user = $this->user;
        // Присвоение роли 'Administrator' тестовому пользователю
        $this->assignRoleToTestUser('Administrator', $this->user);
        // Залогиниваем пользователя
        $this->login($user->getLogin());

        $this->dispatch('/register/delete', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param UserInterface|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, UserInterface $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @throws \Exception
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login=null, string $password=null)
    {
        if (!$login) {
            $login = $this->user_login;
        }
        if (!$password) {
            $password = $this->user_password;
        }
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}