<?php
namespace ModuleAuthTest\Controller;

use Application\Entity\Email;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BasePhoneManager;
use Doctrine\ORM\EntityManager;
use ModuleAuth\Controller\EmailController;
use ModuleAuth\Controller\UserRegistrationCheckedController;
use ModuleAuthTest\Bootstrap;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class UserRegistrationCheckedControllerTest
 * @package ModuleAuthTest\Controller
 */
class UserRegistrationCheckedControllerTest extends AbstractHttpControllerTestCase
{
    private $user_phone;

    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var BasePhoneManager
     */
    private $basePhoneManager;

    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_phone = $config['tests']['user_phone'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseEmailManager = $serviceManager->get(BaseEmailManager::class);
        $this->basePhoneManager = $serviceManager->get(BasePhoneManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();

        $this->entityManager = null;
        $this->baseEmailManager = null;
        $this->basePhoneManager = null;

        gc_collect_cycles();
    }

    /**
     * For Ajax
     * Проверка существования email в базе для Ajax
     * Отправляем на проверку данные без email
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testCheckEmailExistsActionWithoutEmailForAjax()
    {
        // Post data
        $postData = [
            'other' => 'without email',
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/check', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertModuleName('moduleAuth');
        $this->assertMatchedRouteName('email/check');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(UserRegistrationCheckedController::ERROR_CHECK_EMAIL_EXISTS , $data['message']);
    }

    /**
     * For Ajax
     * Проверка существования email в базе для Ajax
     * Отправляем на проверку несуществующий email
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testCheckEmailExistsActionWithNonexistentEmailForAjax()
    {
        // Post data
        $postData = [
            'email' => 'nonexistent@email.com',
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/check', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertModuleName('moduleAuth');
        $this->assertMatchedRouteName('email/check');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(UserRegistrationCheckedController::ERROR_CHECK_EMAIL_EXISTS , $data['message']);
    }

    /**
     * For Ajax
     * Проверка существования email в базе для Ajax
     * Отправляем на проверку существующий неподтвержденный email
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testCheckEmailExistsActionWithUnverifiedEmailForAjax()
    {
        // Email value for test
        $email_value = 'Email_check@test.com';

        $email = $this->baseEmailManager->getEmailByValue($email_value);

        if(!$email) {
            // Create Email object
            $email = new Email();
            $email->setEmail($email_value);
            $email->setIsVerified(0);
            $currentDate = new \DateTime();
            $email->setConfirmationTokenCreationDate($currentDate);
            // Save it to DB
            $this->entityManager->persist($email);
            $this->entityManager->flush();
        }

        // Post data
        $postData = [
            'email' => $email_value,
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/check', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertMatchedRouteName('email/check');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(UserRegistrationCheckedController::ERROR_CHECK_EMAIL_EXISTS , $data['message']);
    }

    /**
     * For Ajax
     * Проверка существования email в базе для Ajax
     * Отправляем на проверку существующий подтвержденный email
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testCheckEmailExistsActionForAjax()
    {
        // Email value for test
        $email_value = 'Email_check@test.com';

        $email = $this->baseEmailManager->getEmailByValue($email_value);

        if(!$email) {
            // Create Email object
            $email = new Email();
            $email->setEmail($email_value);
            $email->setIsVerified(1);
            $currentDate = new \DateTime();
            $email->setConfirmationTokenCreationDate($currentDate);
            // Save it to DB
            $this->entityManager->persist($email);
            $this->entityManager->flush();
        }

        // Post data
        $postData = [
            'email' => $email_value,
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/check', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertMatchedRouteName('email/check');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(EmailController::SUCCESS_CHECK_EMAIL_EXISTS , $data['message']);
    }

    /**
     * For Ajax
     * Проверка существования email в базе для Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testCheckEmailExistsActionWithoutDataForAjax()
    {
        $this->dispatch('/email/check', 'GET');

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertMatchedRouteName('email/check');
    }

    /**
     * For Ajax
     * Проверка существования телефона в базе для Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     * @throws \Exception
     */
    public function testCheckIsPhoneUniqueActionWithoutDataForAjax()
    {
        $this->dispatch('/register/check/phone', 'GET');

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertMatchedRouteName('register/check/phone');
    }

    /**
     * For Ajax
     * Проверка существования телефона в базе для Ajax
     * Отправляем на проверку данные без телефона
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testCheckIsPhoneUniqueActionWithoutPhoneForAjax()
    {
        // Post data
        $postData = [
            'other' => 'without phone',
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register/check/phone', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertModuleName('moduleAuth');
        $this->assertMatchedRouteName('register/check/phone');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Phone already taken' , $data['message']);
    }

    /**
     * For Ajax
     * Проверка существования телефона в базе для Ajax
     * Отправляем на проверку несуществующий телефон
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testCheckIsPhoneUniqueActionWithNonexistentPhoneForAjax()
    {
        // Post data
        $postData = [
            'phone' => '+77777777777',
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register/check/phone', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertModuleName('moduleAuth');
        $this->assertMatchedRouteName('register/check/phone');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Phone is available' , $data['message']);
    }

    /**
     * For Ajax
     * Проверка существования телефона в базе для Ajax
     * Отправляем на проверку существующий неподтвержденный телефон
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testCheckIsPhoneUniqueActionWithUnverifiedPhoneForAjax()
    {
        $phone = $this->basePhoneManager->getPhoneByNumber($this->user_phone);
        $this->assertNotNull($phone);
        $phone->setIsVerified(0);
        $this->entityManager->persist($phone);
        $this->entityManager->flush();

        // Post data
        $postData = [
            'phone' => $phone->getPhoneNumber(),
        ];
        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register/check/phone', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertMatchedRouteName('register/check/phone');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Phone is available' , $data['message']);
    }

    /**
     * For Ajax
     * Проверка существования телефона в базе для Ajax
     * Отправляем на проверку существующий подтвержденный телефон
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testCheckIsPhoneUniqueActionForAjax()
    {
        $phone = $this->basePhoneManager->getPhoneByNumber($this->user_phone);
        $this->assertNotNull($phone);
        $this->assertTrue((bool) $phone->getIsVerified());

        // Post data
        $postData = [
            'phone' => $phone->getPhoneNumber(),
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register/check/phone', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertMatchedRouteName('register/check/phone');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Phone already taken' , $data['message']);
    }

    /**
     * For Ajax
     * Проверка существования логина в базе для Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     * @throws \Exception
     */
    public function testCheckIsLoginUniqueActionWithoutDataForAjax()
    {
        $this->dispatch('/register/check/login', 'GET');

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertMatchedRouteName('register/check/login');
    }

    /**
     * For Ajax
     * Проверка существования логина в базе для Ajax
     * Отправляем на проверку данные без логина
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testCheckIsLoginUniqueActionWithoutPhoneForAjax()
    {
        // Post data
        $postData = [
            'other' => 'without login',
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register/check/login', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertModuleName('moduleAuth');
        $this->assertMatchedRouteName('register/check/login');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Login already taken' , $data['message']);
    }

    /**
     * For Ajax
     * Проверка существования логина в базе для Ajax
     * Отправляем на проверку несуществующий логин
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testCheckIsLoginUniqueActionWithNonexistentPhoneForAjax()
    {
        // Post data
        $postData = [
            'login' => 'phpunit_login',
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register/check/login', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(UserRegistrationCheckedController::class);
        $this->assertModuleName('moduleAuth');
        $this->assertMatchedRouteName('register/check/login');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Login is available' , $data['message']);
    }
}