<?php
namespace ModuleAuthTest\Controller;

use Core\Adapter\TokenAdapter;
use Core\Exception\Code\EmailExceptionCodeInterface;
use Core\Exception\Code\TokenExceptionCodeInterface;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\SessionContainerManager;
use ModuleAuth\Controller\EmailController;
use Core\Form\OperationConfirmForm;
use ModuleAuth\Service\RegistrationManager;
use ModuleAuthTest\Bootstrap;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use Application\Entity\Email;
use Zend\Session\Config\SessionConfig;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;

/**
 * Class EmailControllerTest
 * @package ModuleAuthTest\Controller
 */
class EmailControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Core\Adapter\TokenAdapter
     */
    private $tokenAdapter;

    /**
     * $sessionContainerManager
     * @var \Core\Service\SessionContainerManager
     */
    private $sessionContainerManager;

    /**
     * @var integer
     */
    private $confirmation_token_expiration_time;
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var RegistrationManager
     */
    private $registrationManager;
    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;


    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->confirmation_token_expiration_time = $config['tokens']['email_confirmation_token_expiration_time'];
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseEmailManager = $serviceManager->get(BaseEmailManager::class);
        $this->baseUserManager = $serviceManager->get(BaseUserManager::class);
        $this->registrationManager = $serviceManager->get(RegistrationManager::class);
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->tokenAdapter = $serviceManager->get(TokenAdapter::class);
        $this->sessionContainerManager = $serviceManager->get(SessionContainerManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollBack();
        $this->sessionContainerManager->deleteSessionVar('unconfirmed_email');

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Доступность страницы, но недоступность формы без наличия сгенерированного кода(простого токена)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionFormNotDisplayWithoutCode()
    {
        $this->dispatch('/email/confirm', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/confirm');
        // Что отдается в шаблон
        $this->assertArrayHasKey('confirmForm', $view_vars);
        $this->assertInstanceOf(OperationConfirmForm::class, $view_vars['confirmForm']);

        // Проверяем отсутствие формы #email-confirm-form
        $this->assertNotQuery('#email-confirm-form');
    }

    /**
     * For Ajax
     * Доступность страницы и возврат csrf, но с пустыми данными о коде без наличия сгенерированного кода(простого токена)
     * для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionNullDataWithoutCodeForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/email/confirm', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertControllerClass('EmailController');
        $this->assertMatchedRouteName('email/confirm');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(EmailController::SUCCESS_CSRF_TOKEN, $data['message']);
        $this->assertEquals(null, $data['data']['token_created_date']);
        $this->assertEquals(null, $data['data']['is_email_token_expired']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Доступность страницы с формой при наличии токена
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionFormDisplayWithCode()
    {
        $this->login('test');
        // Simple token
        $simple_token = '111111';

        $user = $this->baseUserManager->getUserByLogin('test');
        $this->assertNotNull($user);
        /** @var Email $email */
        $email = $user->getEmail();
        $this->assertNotNull($email);

        // Encoded token
        $this->tokenAdapter->createToken($email->getEmail(), $simple_token);

        $email->setIsVerified(false);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->persist($email);
        $this->entityManager->flush($email);

        $this->dispatch('/email/confirm', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertControllerClass('EmailController');
        $this->assertMatchedRouteName('email/confirm');
        // Что отдается в шаблон
        $this->assertArrayHasKey('confirmForm', $view_vars);
        $this->assertArrayHasKey('token_created_date', $view_vars);
        $this->assertArrayHasKey('email_confirmation_token_request_period', $view_vars);
        $this->assertArrayHasKey('is_email_token_expired', $view_vars);
        $this->assertInstanceOf(OperationConfirmForm::class, $view_vars['confirmForm']);
        // Возвращается из шаблона
        $this->assertQuery('#email-confirm-form');
    }

    /**
     * Подтверждение с валидными Encoded Token методом POST
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionWithValidEncodedTokenByPost()
    {
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '111111';

        // Создаем нового юзера
        $this->createNewUser('testTest', $email_value, '+79103300722', 'asdfgh');
        // Select user
        $user = $this->baseUserManager->getUserByLogin('testTest');
        $this->assertNotNull($user);
        // Email
        $email = $user->getEmail();
        $this->assertNotNull($email);

        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($email->getEmail(), $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'confirmation_token' => $encodedToken,
            'csrf' => $csrf->getValue()
        ];

        $this->dispatch('/email/confirm', 'POST', $postData);

        $savedEmail = $this->baseEmailManager->getEmailByValue($email_value);
        $this->assertNotNull($savedEmail);

        $this->assertTrue((bool) $savedEmail->getIsVerified());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertControllerClass('EmailController');
        $this->assertMatchedRouteName('email/confirm');
        $this->assertRedirectTo('/profile');
    }

    /**
     * For Ajax
     * Подтверждение с валидными Encoded Token методом POST для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionWithValidEncodedTokenByPostForAjax()
    {
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '111111';

        // Создаем нового юзера
        $this->createNewUser('testTest', $email_value, '+79103300722', 'asdfgh');
        // Select user
        $user = $this->baseUserManager->getUserByLogin('testTest');
        $this->assertNotNull($user);
        // Email
        $email = $user->getEmail();
        $this->assertNotNull($email);

        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        $this->entityManager->flush();

        $this->setUnconfirmedEmailToSession('email_controller@test.com');
        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($email->getEmail(), $simple_token);

        /** @var \Application\Entity\Email $savedEmail */
        $savedEmail = $this->baseEmailManager->getEmailByValue($email_value);
        $this->assertNotNull($savedEmail);
        // Проверяем, что Email не подтвержден
        $this->assertFalse((bool) $savedEmail->getIsVerified());
        $this->assertNotNull($savedEmail->getConfirmationToken());

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'confirmation_token' => $encodedToken,
            'csrf' => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var \Application\Entity\Email $savedEmail */
        $savedEmail = $this->baseEmailManager->getEmailByValue($email_value);
        $this->assertNotNull($savedEmail);
        // Проверяем, что Email подтвержден
        $this->assertTrue((bool) $savedEmail->getIsVerified());
        $this->assertNull($savedEmail->getConfirmationToken());

        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);

        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Подтверждение с неверным типом Encoded Token методом POST
     * Подсовываем объект вместо строки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @group auth
     * @throws \Exception
     */
    public function testConfirmActionWithInvalidEncodedTokenTypeByPost()
    {
        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '111111';

        // Create Email object
        $email = new Email();
        $email->setEmail($email_value);
        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        $this->entityManager->persist($email);
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $email;

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'confirmation_token' => $encodedToken,
            'csrf' => $csrf->getValue()
        ];

        $this->dispatch('/email/confirm', 'POST', $postData);

        $savedEmail = $this->baseEmailManager->getEmailByValue($email_value);
        $this->assertNotNull($savedEmail);
        $this->assertFalse((bool) $savedEmail->getIsVerified());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertControllerClass('EmailController');
        $this->assertMatchedRouteName('email/confirm');

        $viewVars = $this->getViewVars();
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame(TokenExceptionCodeInterface::TOKEN_NOT_VALID_MESSAGE, $viewVars['message']);
    }

    /**
     * Валидный Encoded Token методом GET
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionWithValidEncodedTokenByGet()
    {
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '22222222';

        // Создаем нового юзера
        $this->createNewUser('testTest', $email_value, '+79103300722', 'asdfgh');
        // Select user
        $user = $this->baseUserManager->getUserByLogin('testTest');
        $this->assertNotNull($user);
        // Email
        $email = $user->getEmail();
        $this->assertNotNull($email);

        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($email->getEmail(), $simple_token);
        $this->dispatch('/email/confirm?token='.$encodedToken);

        $savedEmail = $this->baseEmailManager->getEmailByValue($email_value);
        $this->assertNotNull($savedEmail);
        $this->assertTrue((bool) $savedEmail->getIsVerified());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/confirm');
        $this->assertRedirectTo('/profile');
    }

    /**
     * For Ajax
     * Валидный Encoded Token методом GET для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testConfirmActionWithValidEncodedTokenByGetForAjax()
    {
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '22222222';

        // Создаем нового юзера
        $this->createNewUser('testTest', $email_value, '+79103300722', 'asdfgh');
        // Select user
        $user = $this->baseUserManager->getUserByLogin('testTest');
        $this->assertNotNull($user);
        // Email
        $email = $user->getEmail();
        $this->assertNotNull($email);

        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($email->getEmail(), $simple_token);

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/confirm?token='.$encodedToken, 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $savedEmail = $this->baseEmailManager->getEmailByValue($email_value);
        $this->assertNotNull($savedEmail);
        $this->assertTrue((bool) $savedEmail->getIsVerified());

        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/confirm');

        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Попытка передать Invalid Encoded Token методом GET
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionWithInvalidEncodedTokenByGet()
    {
        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '11111111';

        // Create Email object
        $email = new Email();
        $email->setEmail($email_value);
        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->persist($email);
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($email->getEmail(), $simple_token);

        $this->dispatch('/email/confirm?token='.$encodedToken.'invalid');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/confirm');

        $viewVars = $this->getViewVars();
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame(TokenExceptionCodeInterface::TOKEN_NOT_VALID_MESSAGE, $viewVars['message']);
    }

    /**
     * For Ajax
     * Попытка передать Invalid Encoded Token методом GET для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionWithInvalidEncodedTokenByGetForAjax()
    {
        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '11111111';

        // Create Email object
        $email = new Email();
        $email->setEmail($email_value);
        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        $this->entityManager->persist($email);
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($email->getEmail(), $simple_token);

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/confirm?token='.$encodedToken.'invalid', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertSame(TokenExceptionCodeInterface::TOKEN_NOT_VALID_MESSAGE, $data['message']);
    }

    /**
     * Попытка передать истекший Encoded Token методом GET
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionWithExpiredEncodedTokenByGet()
    {
        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '11111111';

        // Create Email object
        $email = new Email();
        $email->setEmail($email_value);
        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $currentDate = new \DateTime();
        $expiration_time = $this->confirmation_token_expiration_time + 1;
        $email->setConfirmationTokenCreationDate($currentDate->modify('-'.$expiration_time.' seconds'));
        $this->entityManager->persist($email);
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($email->getEmail(), $simple_token);

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/confirm?token='.$encodedToken, 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(TokenExceptionCodeInterface::TOKEN_IS_EXPIRED_MESSAGE, $data['message']);
    }

    /**
     * Попытка подтверждения токена с заведомо неверным email
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionWithInvalidEmail()
    {
        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '11111111';

        // Create Email object
        $email = new Email();
        $email->setEmail($email_value);
        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        $this->entityManager->persist($email);
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken('invalid_email@test.com', $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'confirmation_token' => $encodedToken,
            'csrf' => $csrf->getValue()
        ];

        $this->dispatch('/email/confirm', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/confirm');

        $viewVars = $this->getViewVars();
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame(EmailExceptionCodeInterface::EMAIL_NOT_MATCHING_SIMPLE_TOKEN_MESSAGE, $viewVars['message']);
    }

    /**
     * For Ajax
     * Попытка подтверждения токена с заведомо неверным email для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     *
     * @group auth
     */
    public function testConfirmActionWithInvalidEmailForAjax()
    {
        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '11111111';

        // Create Email object
        $email = new Email();
        $email->setEmail($email_value);
        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->persist($email);
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken('invalid_email@test.com', $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'confirmation_token' => $encodedToken,
            'csrf' => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/confirm', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertSame(EmailExceptionCodeInterface::EMAIL_NOT_MATCHING_SIMPLE_TOKEN_MESSAGE, $data['message']);
    }

    /**
     * Повторная отправка письма с токеном c использованием email из POST
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResendActionWithValidDataAndEmailFromPost()
    {
        $config = $this->getApplication()
            ->getServiceManager()
            ->get('Configuration');

        // Session start
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config['session_config']);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '22222222';

        // Создаем нового юзера
        $this->createNewUser('testTest', $email_value, '+79103300722', 'asdfgh');
        // Select user
        $user = $this->baseUserManager->getUserByLogin('testTest');
        $this->assertNotNull($user);
        // Email
        $email = $user->getEmail();
        $this->assertNotNull($email);

        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        $this->entityManager->flush();
        $currentDate = new \DateTime();
        $email->setConfirmationTokenCreationDate($currentDate->modify('-120 seconds'));
        // Save it to DB
        $this->entityManager->flush();

        // Post data
        $postData = [
            'email' => $email_value,
        ];

        //авторизуем
        $this->login($user->getLogin(), 'asdfgh');

        $this->dispatch('/email/resend', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/resend');
        $this->assertRedirectTo('/email/confirm');
    }

    /**
     * Повторная отправка письма с токеном c использованием email из сессии
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testResendActionWithValidDataAndEmailFromSession()
    {
        $config = $this->getApplication()
            ->getServiceManager()
            ->get('Configuration');

        // Session start
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config['session_config']);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '22222222';

        // Создаем нового юзера
        $this->createNewUser('testTest', $email_value, '+79103300722', 'asdfgh');
        // Select user
        $user = $this->baseUserManager->getUserByLogin('testTest');
        $this->assertNotNull($user);
        // Email
        $email = $user->getEmail();
        $this->assertNotNull($email);

        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();
        $currentDate = new \DateTime();
        $email->setConfirmationTokenCreationDate($currentDate->modify('-120 seconds'));
        // Save it to DB
        $this->entityManager->flush();

        $this->sessionContainerManager->setSessionVar('unconfirmed_email', $email_value) ;

        //авторизуем
        $this->login($user->getLogin(), 'asdfgh');

        $this->dispatch('/email/resend');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/resend');
        $this->assertRedirectTo('/email/confirm');
    }

    /**
     * For Ajax
     * Повторная отправка письма с токеном методом GET для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResendActionWithValidDataByPostForAjax()
    {
        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '22222222';

        // Создаем нового юзера
        $this->createNewUser('testTest', $email_value, '+79103300722', 'asdfgh');
        // Select user
        $user = $this->baseUserManager->getUserByLogin('testTest');
        $this->assertNotNull($user);
        // Email
        $email = $user->getEmail();
        $this->assertNotNull($email);

        $email->setIsVerified(0);
        $email->setConfirmationToken($simple_token);
        $email->setConfirmationTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();
        $currentDate = new \DateTime();
        $email->setConfirmationTokenCreationDate($currentDate->modify('-120 seconds'));
        // Save it to DB
        $this->entityManager->flush();

        // Post data
        $postData = [
            'email' => $email_value,
        ];


        //авторизуем
        $this->login($user->getLogin(), 'asdfgh');

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/resend', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/resend');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(EmailController::SUCCESS_EMAIL_RESENT , $data['message']);
    }

    /**
     * Повторная отправка письма с токеном c невалидным email из POST
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResendActionWithValidDataAndInvalidEmailFromPost()
    {
        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '22222222';

        $email = $this->baseEmailManager->getEmailByValue($email_value);

        if(!$email) {
            // Create Email object
            $email = new Email();
            $email->setEmail($email_value);
            $email->setIsVerified(0);
            $email->setConfirmationToken($simple_token);
            $currentDate = new \DateTime();
            $email->setConfirmationTokenCreationDate($currentDate);
            // Save it to DB
            $this->entityManager->persist($email);
            $this->entityManager->flush();
        }

        // Post data
        $postData = [
            'email' => 'email@testcom',
        ];

        $this->dispatch('/email/resend', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/resend');

        $viewVars = $this->getViewVars();
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame(EmailController::ERROR_NO_VALID_EMAIL_PROVIDED, $viewVars['message']);
    }

    /**
     * For Ajax
     * Повторная отправка письма с токеном c невалидным email из POST для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     *
     * @group auth
     */
    public function testResendActionWithValidDataAndInvalidEmailFromPostForAjax()
    {
        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '22222222';

        $email = $this->baseEmailManager->getEmailByValue($email_value);

        if(!$email) {
            // Create Email object
            $email = new Email();
            $email->setEmail($email_value);
            $email->setIsVerified(0);
            $email->setConfirmationToken($simple_token);
            $currentDate = new \DateTime();
            $email->setConfirmationTokenCreationDate($currentDate);
            // Save it to DB
            $this->entityManager->persist($email);
            $this->entityManager->flush();
        }

        // Post data
        $postData = [
            'email' => 'email@testcom',
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/resend', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(EmailController::ERROR_NO_VALID_EMAIL_PROVIDED , $data['message']);
    }

    /**
     * Повторная отправка письма с токеном при неистекшем предыдушем токене
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     *
     * @group auth
     */
    public function testResendActionWithValidDataAndNotExpiredPreviousToken()
    {
        $config = $this->getApplication()
            ->getServiceManager()
            ->get('Configuration');

        // Session start
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config['session_config']);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '22222222';

        $email = $this->baseEmailManager->getEmailByValue($email_value);

        if(!$email) {
            // Create Email object
            $email = new Email();
            $email->setEmail($email_value);
            $email->setIsVerified(0);
            $email->setConfirmationToken($simple_token);
            $currentDate = new \DateTime();
            $email->setConfirmationTokenCreationDate($currentDate->modify('-10 seconds'));
            // Save it to DB
            $this->entityManager->persist($email);
            $this->entityManager->flush();
        }

        // Post data
        $postData = [
            'email' => 'email_controller@test.com',
        ];

        $this->dispatch('/email/resend', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(EmailController::class);
        $this->assertMatchedRouteName('email/resend');

        $viewVars = $this->getViewVars();
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame(EmailController::ERROR_NO_VALID_EMAIL_PROVIDED, $viewVars['message']);
    }

    /**
     * For Ajax
     * Повторная отправка письма с токеном при неистекшем предыдушем токене для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResendActionWithValidDataAndNotExpiredPreviousTokenForAjax()
    {
        // Email value for test
        $email_value = 'email_controller@test.com';
        // Simple token
        $simple_token = '22222222';

        $email = $this->baseEmailManager->getEmailByValue($email_value);

        if(!$email) {
            // Create Email object
            $email = new Email();
            $email->setEmail($email_value);
            $email->setIsVerified(0);
            $email->setConfirmationToken($simple_token);
            $currentDate = new \DateTime();
            $email->setConfirmationTokenCreationDate($currentDate->modify('-10 seconds'));
            // Save it to DB
            $this->entityManager->persist($email);
            $this->entityManager->flush();
        }

        // Post data
        $postData = [
            'email' => 'email_controller@test.com',
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/email/resend', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertMatchedRouteName('email/resend');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertSame(EmailController::ERROR_NO_VALID_EMAIL_PROVIDED, $data['message']);
    }

    /**
     * @param string $email_value
     */
    private function setUnconfirmedEmailToSession(string $email_value)
    {
        $config = $this->getApplication()
            ->getServiceManager()
            ->get('Configuration');

        // Session start
        $sessionConfig = new SessionConfig();
        $sessionConfig->setOptions($config['session_config']);
        $sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->sessionContainerManager->setSessionVar('unconfirmed_email', $email_value);
    }

    /**
     * @param $login
     * @param $email
     * @param $phone
     * @param $password
     * @return \Application\Entity\User
     * @throws \Exception
     */
    private function createNewUser($login, $email, $phone, $password)
    {
        $data   = [
            'login'     => $login,
            'phone'     => $phone,
            'email'     => $email,
            'password'  => $password,
        ];

        return $this->registrationManager->addNewUser($data);
    }


    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) {
            $login = $this->user_login;
        }
        if(!$password) {
            $password = $this->user_password;
        }
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}
