<?php

namespace ModuleAuthTest\Controller;

use Core\Adapter\TokenAdapter;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Exception\Code\EmailExceptionCodeInterface;
use Core\Exception\Code\TokenExceptionCodeInterface;
use Core\Exception\Code\UserExceptionCodeInterface;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseUserManager;
use Doctrine\ORM\EntityManager;
use ModuleAuth\Controller\PasswordResetController;
use ModuleAuth\Form\PasswordChangeForm;
use ModuleAuth\Form\PasswordResetForm;
use ModuleAuthTest\Bootstrap;
use Zend\Form\Form;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use CoreTest\ViewVarsTrait;

class PasswordResetControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    private $main_project_url;

    /**
     * @var string
     */
    private $user_email;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var TokenAdapter
     */
    private $tokenAdapter;

    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;


    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->tokenAdapter = $serviceManager->get(TokenAdapter::class);
        $this->baseEmailManager = $serviceManager->get(BaseEmailManager::class);
        $this->baseUserManager = $serviceManager->get(BaseUserManager::class);

        $this->user_email = $config['tests']['user_email'];
        $this->user_login = $config['tests']['user_login'];
        $this->main_project_url = $config['main_project_url'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;
        $this->baseUserManager = null;
        $this->baseEmailManager = null;
        $this->tokenAdapter = null;

        gc_collect_cycles();
    }

    /**
     * Проверка доступности страницы
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResetPasswordActionCanBeAccessed()
    {
        $this->dispatch('/forgot/password/reset');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-reset');
        // Что отдается в шаблон
        $this->assertArrayHasKey('passwordResetForm', $view_vars);
        $this->assertInstanceOf(PasswordResetForm::class, $view_vars['passwordResetForm']);
        // Возвращается из шаблона
        $this->assertQuery('#password-reset-form');
    }

    /**
     * For Ajax
     * Проверка доступности страницы для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResetPasswordActionCanBeAccessedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/reset', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-reset');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Валидный Email и Csrf в Post
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResetPasswordActionWithValidPost()
    {
        $email = $this->baseEmailManager->getEmailByValue($this->user_email);
        $this->assertNotNull($email);

        // If Email is not verified, verify
        if( !$email->getIsVerified() ) {
            $email->setIsVerified(true);
            $this->entityManager->flush();
        }

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'email' => $this->user_email,
            'csrf'  => $csrf->getValue(),
        ];

        $this->dispatch('/forgot/password/reset', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertMatchedRouteName('forgot/password-reset');
        $this->assertControllerName(PasswordResetController::class);

        $user = $this->baseUserManager->getUserByEmail($email);

        $this->assertNotNull($user->getPasswordResetToken());
        $this->assertNotNull($user->getPasswordResetTokenCreationDate());
    }

    /**
     * For Ajax
     * Валидный Email и Csrf в Post для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResetPasswordActionWithValidPostForAjax()
    {
        $email = $this->baseEmailManager->getEmailByValue($this->user_email);
        $this->assertNotNull($email);

        // If Email is not verified, verify
        if( !$email->getIsVerified() ) {
            $email->setIsVerified(true);
            $this->entityManager->flush();
        }

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'email' => $this->user_email,
            'csrf'  => $csrf->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/reset', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');
        $this->assertMatchedRouteName('forgot/password-reset');
        $this->assertControllerName(PasswordResetController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(PasswordResetController::SUCCESS_TOKEN_SENT, $data['message']);
    }

    /**
     * Попытка отправить заведомо неверный email
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResetPasswordActionWithInvalidEmail()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'email' => 'invalid_email@test.net',
            'csrf'  => $csrf->getValue(),
        ];

        $this->dispatch('/forgot/password/reset', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-reset');

        $viewVars = $this->getViewVars();
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame(EmailExceptionCodeInterface::EMAIL_NOT_FOUND_MESSAGE, $viewVars['message']);
    }

    /**
     * For Ajax
     * Попытка отправить заведомо неверный email для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResetPasswordActionWithInvalidEmailForAjax()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'email' => 'invalid_email@test.net',
            'csrf'  => $csrf->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/reset', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertSame(EmailExceptionCodeInterface::EMAIL_NOT_FOUND_MESSAGE, $data['message']);
    }

    /**
     * Попытка отправить заведомо неверный csrf
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testResetPasswordActionWithInvalidCsrf()
    {
        // Post data
        $postData = [
            'email' => $this->user_email,
            'csrf'  => 'e2ec1b3b1ab7407f4cdfb84458addf50-e172c96408bc89412eee5c60a9b71af3',
        ];

        $this->dispatch('/forgot/password/reset', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertMatchedRouteName('forgot/password-reset');

        $viewVars = $this->getViewVars();
        $this->assertArrayHasKey('passwordResetForm', $viewVars);
        $this->assertInstanceOf(PasswordResetForm::class, $viewVars['passwordResetForm']);
        /** @var Form $form */
        $form = $viewVars['passwordResetForm'];
        $formMessage = $form->getMessages();
        $this->assertArrayHasKey('csrf', $formMessage);
        $this->assertArrayHasKey('notSame', $formMessage['csrf']);
        $this->assertSame('The form submitted did not originate from the expected site', $formMessage['csrf']['notSame']);
    }

    /**
     * For Ajax
     * Попытка отправить заведомо неверный csrf для Ajax
     *
     * @group auth
     * @throws \Exception
     */
    public function testResetPasswordActionWithInvalidCsrfForAjax()
    {
        // Post data
        $postData = [
            'email' => $this->user_email,
            'csrf'  => 'e2ec1b3b1ab7407f4cdfb84458addf50-e172c96408bc89412eee5c60a9b71af3',
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/reset', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        $this->assertModuleName('moduleAuth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /**
     * SetPasswordAction без зашифрованного токена отправляент а страницу ошибки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionCanNotBeAccessedWithoutToken()
    {
        $this->dispatch('/forgot/password/set');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');

        $viewVars = $this->getViewVars();
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame('No token provided', $viewVars['message']);
    }

    /**
     * For Ajax
     * SetPasswordAction без зашифрованного токена отправляент а страницу ошибки для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionCanNotBeAccessedWithoutTokenForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/set', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * SetPasswordAction with valid Get
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithValidGet()
    {
        // Simple token
        $simple_token = '333333ab';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        $this->dispatch('/forgot/password/set?token='.$encodedToken);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');
        // Что отдается в шаблон
        $this->assertArrayHasKey('passwordChangeForm', $view_vars);
        $this->assertInstanceOf(PasswordChangeForm::class, $view_vars['passwordChangeForm']);
        // Возвращается из шаблона
        $this->assertQuery('#password-change-form');
    }

    /**
     * For Ajax
     * SetPasswordAction with valid Get для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithValidGetForAjax()
    {
        // Simple token
        $simple_token = '33333333';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
    }

    /**
     * Попытка передать в SetPasswordAction неверный simple token (в составе зашифрованного токена)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidSimpleToken()
    {
        // Simple token
        $simple_token = '33333333';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, '33333332');

        $this->dispatch('/forgot/password/set?token='.$encodedToken);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');

        $viewVars = $this->getViewVars();
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame(UserExceptionCodeInterface::USER_NOT_MATCHING_SIMPLE_TOKEN_MESSAGE, $viewVars['message']);
    }

    /**
     * For Ajax
     * Попытка передать в SetPasswordAction неверный simple token (в составе зашифрованного токена) для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidSimpleTokenForAjax()
    {
        // Simple token
        $simple_token = '33333333';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, '33333332');

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');
        $this->assertEquals('ERROR', $data['status']);
        $this->assertSame(UserExceptionCodeInterface::USER_NOT_MATCHING_SIMPLE_TOKEN_MESSAGE, $data['message']);
    }

    /**
     * Попытка передать в SetPasswordAction неверный зашифрованный токен
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidEncodedToken()
    {
        // Simple token
        $simple_token = '33333333';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        $this->dispatch('/forgot/password/set?token='.$encodedToken.'12');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');

        $viewVars = $this->getViewVars();
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame(TokenExceptionCodeInterface::TOKEN_NOT_VALID_MESSAGE, $viewVars['message']);
    }

    /**
     * For Ajax
     * Попытка передать в SetPasswordAction неверный зашифрованный токен для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidEncodedTokenForAjax()
    {
        // Simple token
        $simple_token = '33333333';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken.'128', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertSame(TokenExceptionCodeInterface::TOKEN_NOT_VALID_MESSAGE, $data['message']);
    }

    /**
     * SetPasswordAction с правильными Post данными
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithValidPost()
    {
        // Simple token
        $simple_token = '44444444';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test_test',
            'csrf'                  => $csrf->getValue()
        ];

        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_new_pass = $user->getPassword();
        // Password has changed
        $this->assertNotEquals($user_old_pass, $user_new_pass);
    }

    /**
     * For Ajax
     * SetPasswordAction с правильными Post данными для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithValidPostForAjax()
    {
        // Simple token
        $simple_token = '44444444';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test_ajax',
            'confirm_new_password'  => 'test_test_ajax',
            'csrf'                  => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Password changed', $data['message']);

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_new_pass = $user->getPassword();
        // Password has changed
        $this->assertNotEquals($user_old_pass, $user_new_pass);
    }

    /**
     * Передаем в SetPasswordAction неверный Csrf
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidCsrf()
    {
        // Simple token
        $simple_token = '44444444';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test_test',
            'csrf'                  => '7779ffebfc787475e9c6fcc1f13191bb-2738e4964056a4b7b2800350670d31e8'
        ];

        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }

    /**
     * For Ajax
     * Передаем в SetPasswordAction неверный Csrf для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidCsrfForAjax()
    {
        // Simple token
        $simple_token = '44444444';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test_test',
            'csrf'                  => '7779ffebfc787475e9c6fcc1f13191bb-2738e4964056a4b7b2800350670d31e8'
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('csrf', $data['data']);

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }

    /**
     * Передаем в SetPasswordAction неверные Post данные (пароли не совпадают)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidPassword()
    {
        // Simple token
        $simple_token = '44444444';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test',
            'csrf'                  => $csrf->getValue()
        ];

        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }

    /**
     * For Ajax
     * Передаем в SetPasswordAction неверные Post данные (пароли не совпадают) для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidPasswordForAjax()
    {
        // Simple token
        $simple_token = '44444444';

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test_ajax',
            'confirm_new_password'  => 'test_test',
            'csrf'                  => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('confirm_new_password', $data['data']);

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }

    /**
     * Попытка смены пароля с просроченным simple token
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithExpiredToken()
    {
        // Simple token
        $simple_token = '44444444';

        $config = $this->getApplication()
            ->getServiceManager()
            ->get('Configuration');
        $token_expiration_time = $config['tokens']['password_resetting_token_expiration_time'];

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $currentDate = new \DateTime();
        $user->setPasswordResetTokenCreationDate($currentDate->modify('-' . ($token_expiration_time*2) . ' seconds'));
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test',
            'csrf'                  => $csrf->getValue()
        ];

        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }

    /**
     * For Ajax
     * Попытка смены пароля с просроченным simple token для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group auth
     */
    public function testSetPasswordActionWithExpiredTokenForAjax()
    {
        // Simple token
        $simple_token = '44444444';

        $config = $this->getApplication()
            ->getServiceManager()
            ->get('Configuration');
        $token_expiration_time = $config['tokens']['password_resetting_token_expiration_time'];

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $currentDate = new \DateTime();
        $user->setPasswordResetTokenCreationDate($currentDate->modify('-' . ($token_expiration_time*2) . ' seconds'));
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test',
            'csrf'                  => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(TokenExceptionCodeInterface::TOKEN_IS_EXPIRED_MESSAGE, $data['message']);

        $user = $this->baseUserManager->getUserByLogin($this->user_login);
        $this->assertNotNull($user);

        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }
}
