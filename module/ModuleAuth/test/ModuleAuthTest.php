<?php

namespace ModuleAuthTest;

use ModuleAuth\Module;
use PHPUnit\Framework\TestCase;

class ModuleAuthTest extends TestCase
{
    const CONFIG_FILES_NAMES = [
        'module.config.php',
        'router.config.php',
        'assets.config.php',
        'controllers.config.php',
        'service.config.php',
        'views.config.php'
    ];

    private $moduleRoot = null;

    protected function setUp()
    {
        $this->moduleRoot = realpath(__DIR__ . '/../');
    }

    /**
     * @group module
     */
    public function testGetConfig()
    {
        $expectedConfig = [];
        foreach(self::CONFIG_FILES_NAMES as $file_name) {
            /** @noinspection PhpIncludeInspection */
            $config = include $this->moduleRoot . '/config/' . $file_name;
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $expectedConfig = array_replace_recursive($expectedConfig, $config);
        }

        $module = new Module();
        $configData = $module->getConfig();

        $this->assertEquals($expectedConfig, $configData);
    }
}