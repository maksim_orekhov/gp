<?php
namespace ModuleAuthTest;

use Zend\Test\Util\ModuleLoader;

include __DIR__.'/../../../vendor/autoload.php';

class Bootstrap extends ModuleLoader
{
    /**
     * @var Bootstrap[]
     */
    protected static $instances;
    protected static $config;

    public function __construct(array $config)
    {
        parent::__construct($config);

        static::$config = $config;
    }

    /**
     * @param array $config
     *
     * @return Bootstrap
     */
    public static function getInstance(array $config = [])
    {
        putenv('AUTH_PROVIDER_ENV=ModuleAuth');

        if (empty($config)) {
            $config = array_replace_recursive(
                include __DIR__ . '/../../../config/application.config.php',
                include __DIR__ . '/../../../config/autoload/global.php'
            );
        }
        $key = md5(serialize($config));
        if (empty(static::$instances[$key])) {
            static::$instances[$key] = new self($config);
        }

        return static::$instances[$key];
    }

    /**
     * Init bootstrap
     *
     * @param array $config
     *
     * @return Bootstrap
     */
    public static function init(array $config = [])
    {
        return static::getInstance($config);
    }

    /**
     * @return mixed
     */
    public static function getConfig()
    {
        return static::$config;
    }
}

Bootstrap::init();
