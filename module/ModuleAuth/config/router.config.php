<?php
namespace ModuleAuth;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'auth-csrf-token' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/auth/csrf-token',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'csrfToken',
                    ],
                ],
            ],
            'login' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/login',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'login',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'confirm' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/confirm',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action'     => 'confirmationCode',
                            ],
                        ],
                    ],
                    'code-resend' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/code-resend',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action'     => 'confirmationCodeResend',
                            ],
                        ],
                    ],
                    'logged-in' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/logged-in',
                            'defaults' => [
                                'controller' => Controller\AuthController::class,
                                'action' => 'loggedIn',
                            ],
                        ],
                    ],
                ]
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],
            'not-authorized' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/not-authorized',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'notAuthorized',
                    ],
                ],
            ],
            'access-denied' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/access-denied',
                    'defaults' => [
                        'controller' => Controller\AuthController::class,
                        'action'     => 'accessDenied',
                    ],
                ],
            ],
            'forgot' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/forgot',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'password-reset' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/password/reset',
                            'defaults' => [
                                'controller' => Controller\PasswordResetController::class,
                                'action'     => 'resetPassword',
                            ],
                        ],
                    ],
                    'reset-confirm' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/password/reset/confirm',
                            'defaults' => [
                                'controller' => Controller\PasswordResetController::class,
                                'action'     => 'confirmationCode',
                            ],
                        ],
                    ],
                    'reset-code-resend' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/password/reset/code-resend',
                            'defaults' => [
                                'controller' => Controller\PasswordResetController::class,
                                'action'     => 'confirmationCodeResend',
                            ],
                        ],
                    ],
                    'password-set' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/password/set',
                            'defaults' => [
                                'controller' => Controller\PasswordResetController::class,
                                'action'     => 'setPassword',
                            ],
                        ],
                    ],
                ],
            ],
            ///////////////////////////////////////
            //////////// registration /////////////
            ///////////////////////////////////////
            'register' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/register',
                    'defaults' => [
                        'controller' => Controller\UserRegistrationController::class,
                        'action' => 'create',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/:action[/id/:id]',
                            'constraints' => [
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[0-9]*',
                            ],
                            'defaults' => [
                                'controller' => Controller\UserRegistrationController::class,
                                'action' => 'delete',
                            ],
                        ],
                    ],
                    'check' => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/check',
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            /// registration checked ///
                            'login' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/login',
                                    'defaults' => [
                                        'controller' => Controller\UserRegistrationCheckedController::class,
                                        'action'     => 'checkIsLoginUnique',
                                    ],
                                    'strategies' => [
                                        'ViewJsonStrategy',
                                    ],
                                ],
                            ],
                            'email' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/email',
                                    'defaults' => [
                                        'controller' => Controller\UserRegistrationCheckedController::class,
                                        'action'     => 'checkIsEmailUnique',
                                    ],
                                    'strategies' => [
                                        'ViewJsonStrategy',
                                    ],
                                ],
                            ],
                            'phone' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/phone',
                                    'defaults' => [
                                        'controller' => Controller\UserRegistrationCheckedController::class,
                                        'action'     => 'checkIsPhoneUnique',
                                    ],
                                    'strategies' => [
                                        'ViewJsonStrategy',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ]
            ],
            ///////////////////////////////////////
            //////////// registration /////////////
            ///////////////////////////////////////
            'email' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/email',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'confirm' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/confirm',
                            'defaults' => array(
                                'controller' => Controller\EmailController::class,
                                'action' => 'confirm',
                            ),
                        ),
                        'strategies' => [
                            'ViewJsonStrategy',
                        ],
                    ),
                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit',
                            'defaults' => array(
                                'controller' => Controller\EmailController::class,
                                'action' => 'edit',
                            ),
                        ),
                        'strategies' => [
                            'ViewJsonStrategy',
                        ],
                    ),
                    'resend' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/resend',
                            'defaults' => array(
                                'controller' => Controller\EmailController::class,
                                'action' => 'resend',
                            ),
                        ),
                    ),
                    'check' => array(
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/check',
                            'defaults' => [
                                'controller' => Controller\UserRegistrationCheckedController::class,
                                'action' => 'checkEmailExists',
                            ],
                            'strategies' => [
                                'ViewJsonStrategy',
                            ],
                        ],
                    ),
                ]
                /* ---=============== </Child ===============--- */
            ],
            /// end registration checked ///
            'phone' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/phone',
                    'defaults' => [
                        'controller' => Controller\PhoneController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'confirm' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/confirm',
                            'defaults' => array(
                                'controller' => Controller\PhoneController::class,
                                'action' => 'confirm',
                            ),
                        ),
                        'strategies' => [
                            'ViewJsonStrategy',
                        ],
                    ),
                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit',
                            'defaults' => array(
                                'controller' => Controller\PhoneController::class,
                                'action' => 'edit',
                            ),
                        ),
                        'strategies' => [
                            'ViewJsonStrategy',
                        ],
                    ),
                    'edit-confirm' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit-confirm',
                            'defaults' => array(
                                'controller' => Controller\PhoneController::class,
                                'action' => 'confirmationEditCode',
                            ),
                        ),
                        'strategies' => [
                            'ViewJsonStrategy',
                        ],
                    ),
                    'code-resend' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/confirm/code-resend',
                            'defaults' => array(
                                'controller' => Controller\PhoneController::class,
                                'action' => 'confirmationCodeResend',
                            ),
                        ),
                    ),
                    'edit-code-resend' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/confirm/edit-code-resend',
                            'defaults' => array(
                                'controller' => Controller\PhoneController::class,
                                'action' => 'confirmationEditCodeResend',
                            ),
                        ),
                    ),
                ]
                /* ---=============== </Child ===============--- */
            ],
        ],
    ],
];