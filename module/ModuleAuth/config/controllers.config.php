<?php
namespace ModuleAuth;

return [
    'controllers' => [
        'factories' => [
            Controller\AuthController::class => Controller\Factory\AuthControllerFactory::class,
            Controller\UserRegistrationController::class => Controller\Factory\UserRegistrationControllerFactory::class,
            Controller\UserRegistrationCheckedController::class => Controller\Factory\UserRegistrationCheckedControllerFactory::class,
            Controller\PhoneController::class => Controller\Factory\PhoneControllerFactory::class,
            Controller\EmailController::class => Controller\Factory\EmailControllerFactory::class,
            Controller\PasswordResetController::class => Controller\Factory\PasswordResetControllerFactory::class,
        ],
    ],
];