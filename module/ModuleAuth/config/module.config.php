<?php
namespace ModuleAuth;

return [
    // The multifactor authorization on/off
    'multifactor_authorization' => false,
    // The multifactor password resetting on/off
    'multifactor_password_reset' => false,
    // роут удаления пользователя on/off
    'delete_user_route' => false,
];
