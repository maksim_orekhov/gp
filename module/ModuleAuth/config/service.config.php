<?php
namespace ModuleAuth;

return [
    'service_manager' => [
        'factories' => [
            //provider
            Provider\AuthServiceProvider::class => Provider\Factory\AuthServiceProviderFactory::class,
            // Adapters
            Adapter\AuthAdapter::class => Adapter\Factory\AuthAdapterFactory::class,
            Adapter\AuthMultifactorAdapter::class => Adapter\Factory\AuthMultifactorAdapterFactory::class,
            // Services
            Service\RegistrationManager::class => Service\Factory\RegistrationManagerFactory::class,
        ],
    ],
];