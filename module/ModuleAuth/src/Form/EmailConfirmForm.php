<?php
namespace ModuleAuth\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class EmailConfirmForm
 * @package Application\Form
 */
class EmailConfirmForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('operation-confirm');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'email-confirm-form');
        $this->setAttribute('class', 'form-signin');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'confirmation_token',
            'options' => [
                'label' => 'Code',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Confirm',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        // Add input for "confirmation_token" field
        $inputFilter->add([
            'name'     => 'confirmation_token',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 50,
                        'max' => 300
                    ],
                ],
            ],
        ]);
    }
}