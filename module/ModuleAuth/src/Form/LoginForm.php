<?php
namespace ModuleAuth\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class LoginForm
 * @package ModuleAuth\Form
 */
class LoginForm extends Form
{
    /**
     * There can be two scenarios - 'with auth confirm code' or 'without auth confirm code'.
     *
     * @var boolean
     */
    #private $authorization_confirm;

    public function __construct()
    {
        // Define form name
        parent::__construct('login-form');

        #$this->authorization_confirm = $authorization_confirm;

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'login-form');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "login" field
        $this->add([
            'type' => 'text',
            'name' => 'login',
            'attributes' => [
                'id' => 'login',
                'class'=>'form-control',
                'placeholder'=>'Логин'
            ],
            'options' => [
                'label' => 'логин',
            ],
        ]);

        // Add "password" field
        $this->add([
            'type'  => 'password',
            'name' => 'password',
            'attributes' => [
                'id' => 'password',
                'class'=>'form-control',
                'placeholder'=>'Введите пароль'
            ],
            'options' => [
                'label' => 'пароль',
            ],
        ]);

        // Add "remember_me" field
        $this->add([
            'type'  => 'checkbox',
            'name' => 'remember_me',
            'options' => [
                'label' => 'Remember me',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Войти',
                'id' => 'submitbutton',
                'class'=>'btn btn-blue'
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        // Add filters and validators for "login" field
        $inputFilter->add([
            'name'     => 'login',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 16]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/^[a-zA-Z][a-zA-Z0-9-_\.]{2,15}$/',
                        'message' => 'Неверный формат ввода']
                ],
            ],
        ]);

        // Add filters and validators for "password" field
        $inputFilter->add([
            'name'     => 'password',
            'required' => true,
            'filters'  => [
            ],
            'validators' => [
                ['name'    => 'StringLength', 'options' => ['min' => 6, 'max' => 64]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[a-zA-Z0-9]$/',
                        'message' => 'Недопустимые символы в пароле']
                ],
            ],
        ]);

        // Add input for "remember_me" field
        $inputFilter->add([
            'name'     => 'remember_me',
            'required' => false,
            'filters'  => [
            ],
            'validators' => [
                [
                    'name'    => 'InArray',
                    'options' => [
                        'haystack' => [0, 1],
                    ]
                ],
            ],
        ]);
    }
}