<?php

namespace ModuleAuth\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class UserRegistrationForm
 * @package ModuleAuth\Form
 */
class UserRegistrationForm extends Form
{
    /**
     * @var mixed
     */
    private $user_email;

    /**
     * @var null
     */
    private $email_pattern;

    /**
     * Конструктор.
     * @param null $user_email
     */
    public function __construct($user_email = null)
    {
        $this->user_email = strtolower($user_email);

        $this->email_pattern = '/^[^#@А-Яа-я,"][\.0-9A-Za-z-_]{0,}\@(([0-9A-Za-z-_]{2,18}\.[A-Za-z-]{2,5})|([0-9A-Za-z-_]{2,18}\.[0-9A-Za-z-_]{2,18}\.[-A-Za-z]{2,5}))/';
        if ( $user_email ) {
            $this->email_pattern = str_replace(['@', '.'], ['\@', '\.'], '/^'.$user_email.'/');
        }

        // Определяем имя формы.
        parent::__construct('user-registration-form');

        // Задает для этой формы метод POST.
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();

    }


    /**
     * Этот метод добавляет элементы к форме (поля ввода и кнопку отправки формы).
     */
    protected function addElements()
    {

        // Добавляем поле "login"
        $this->add([
            'type'  => 'text',
            'name' => 'login',
            'attributes' => [
                'id' => 'login',
                'class'=>'form-control',
                'placeholder'=>'Логин'
            ],
            'options' => [
                'label' => 'логин',
            ],
        ]);

        // Добавляем поле "phone"
        $this->add([
            'type'  => 'text',
            'name' => 'phone',
            'attributes' => [
                'id' => 'phone',
                'class'=>'form-control',
                'placeholder'=>'Телефон',
            ],
            'options' => [
                'label' => 'телефон',
            ],
        ]);

        $readonly = false;
        if ($this->user_email) {
            $readonly = true;
        }

        // Добавляем поле "email"
        $this->add([
            'type'  => 'text',
            'name' => 'email',
            'attributes' => [
                'id' => 'inputEmail',
                'class'=>'form-control',
                'placeholder'=>'',
                'value' => $this->user_email,
                'readonly' => $readonly
            ],
            'options' => [
                'label' => 'email',
            ],
        ]);

        // Добавляем поле "password"
        $this->add([
            'type'  => 'password',
            'name' => 'password',
            'attributes' => [
                'id' => 'password',
                'class'=>'form-control',
                'placeholder'=>'Введите пароль'
            ],
            'options' => [
                'label' => 'пароль',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'hidden',
            'name' => 'token',
            'attributes' => ['value' => '']
        ]);

        // Добавляем кнопку отправки формы
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Зарегистрироваться',
                'id' => 'submitbutton',
                'class'=>'btn btn-blue'
            ],
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'login',
            'required' => true,
            'filters'  => [
                ['name'    =>  'StringTrim'],
                ['name'    =>  'StripTags'],
                ['name'    =>  'StripNewlines'],
            ],
            'validators' => [
                ['name'    => 'StringLength', 'options' => ['min' => 3, 'max' => 16]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/^[a-zA-Z][a-zA-Z0-9-_\.]{2,15}$/',
                        'message' => 'Неверный формат ввода']
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'phone',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name'    => \Core\Form\Validator\PhoneValidator::class,
                    'options' => [
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'email',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => $this->email_pattern,
                        'message' => 'Недопустимый email'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'password',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                ['name'    => 'StringLength', 'options' => ['min' => 6, 'max' => 64]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[a-zA-Z0-9]$/',
                        'message' => 'Недопустимые символы в пароле']
                ],
            ],
        ]);
    }
}
