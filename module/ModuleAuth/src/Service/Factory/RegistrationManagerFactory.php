<?php
namespace ModuleAuth\Service\Factory;

use Core\EventManager\AuthEventProvider;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BasePhoneManager;
use ModuleAuth\Service\RegistrationManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class RegistrationManagerFactory
 * @package ModuleAuth\Service\Factory
 */
class RegistrationManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RegistrationManager|object
     */
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $authEventProvider = $container->get(AuthEventProvider::class);
        $baseEmailManager = $container->get(BaseEmailManager::class);
        $basePhoneManager = $container->get(BasePhoneManager::class);

        return new RegistrationManager(
            $entityManager,
            $authEventProvider->getEventManager(),
            $baseEmailManager,
            $basePhoneManager
        );
    }
}
