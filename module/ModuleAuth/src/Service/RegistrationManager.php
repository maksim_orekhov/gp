<?php
namespace ModuleAuth\Service;

use Application\Entity\User;
use Core\EventManager\AuthEventProvider;
use Core\Exception\LogicException;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseUserManager;
use DateTime;
use Doctrine\ORM\EntityManager;
use Zend\EventManager\EventManager;

class RegistrationManager extends BaseUserManager
{
    /**
     * @var EventManager
     */
    private $eventManager;
    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;
    /**
     * @var BasePhoneManager
     */
    private $basePhoneManager;

    /**
     * RegistrationManager constructor.
     * @param EntityManager $entityManager
     * @param EventManager $eventManager
     * @param BaseEmailManager $baseEmailManager
     * @param BasePhoneManager $basePhoneManager
     */
    public function __construct(EntityManager $entityManager,
                                EventManager $eventManager,
                                BaseEmailManager $baseEmailManager,
                                BasePhoneManager $basePhoneManager)
    {
        parent::__construct($entityManager);

        $this->eventManager = $eventManager;
        $this->baseEmailManager = $baseEmailManager;
        $this->basePhoneManager = $basePhoneManager;
    }

    /**
     * @param $data
     * @return User
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addNewUser($data)
    {
        // Проверка уникальности login на сервере
        if(!$this->checkLoginUnique($data['login'])){

            throw new LogicException(null, LogicException::USER_LOGIN_ALREADY_EXISTS);
        }

        $user = new User();
        $user->setLogin($data['login']);
        $user->setPassword($this->createPassword($data['password']));
        $user->setCreated(new DateTime('now'));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        if (!$user->getId()) {
            throw new LogicException(null, LogicException::USER_CREATED_FAILED);
        }

        // Проверка уникальности email на стороне сервера
        if(!$this->baseEmailManager->checkEmailUnique($data['email'])){

            throw new LogicException(null, LogicException::EMAIL_ALREADY_EXISTS);
        }

        // create new email and set to user
        $email = $this->baseEmailManager->addNewEmail($data['email']);
        $this->setEmailToUser($user, $email);

        // Проверка уникальности phone на стороне сервера
        if(!$this->basePhoneManager->checkPhoneUnique($data['phone'])){

            throw new LogicException(null, LogicException::PHONE_NOT_UNIQUE);
        }

        // create new phone and set to user
        $phone = $this->basePhoneManager->addNewPhone($data['phone']);
        $this->setPhoneToUser($user, $phone);

        $this->eventManager->trigger(AuthEventProvider::EVENT_SET_ROLE_TO_USER, $this, [
            'user' => $user,
            'role' => 'Unverified'
        ]);

        return $user;
    }
}