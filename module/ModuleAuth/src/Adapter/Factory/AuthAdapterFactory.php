<?php
namespace ModuleAuth\Adapter\Factory;

use Core\Service\Base\BaseUserManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleAuth\Adapter\AuthAdapter;

/**
 * This is the factory class for AuthAdapter service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class AuthAdapterFactory implements FactoryInterface
{
    /**
     * This method creates the AuthAdapter service and returns its instance.
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return AuthAdapter|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseUserManager = $container->get(BaseUserManager::class);

        return new AuthAdapter($baseUserManager);
    }
}