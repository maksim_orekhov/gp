<?php
namespace ModuleAuth\Adapter\Factory;

use Core\Service\Base\BaseUserManager;
use ModuleAuth\Adapter\AuthMultifactorAdapter;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * This is the factory class for AuthMultifactorAdapter service
 */
class AuthMultifactorAdapterFactory implements FactoryInterface
{
    /**
     * This method creates the AuthAdapter service and returns its instance.
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return AuthMultifactorAdapter|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseUserManager = $container->get(BaseUserManager::class);

        return new AuthMultifactorAdapter($baseUserManager);
    }
}