<?php
namespace ModuleAuth\Adapter;

use Core\Service\Auth\AuthAdapterInterface;
use Core\Service\Base\BaseUserManager;
use Zend\Authentication\Result;
use Zend\Crypt\Password\Bcrypt;

class AuthAdapter implements AuthAdapterInterface
{
    const SUCCESS_AUTHENTICATION = 'Authenticated successfully';
    const ERROR_AUTHENTICATION = 'Invalid credentials';
    /**
     * User login.
     * @var string
     */
    private $login;
    /**
     * User password.
     * @var string
     */
    private $password;
    /**
     * @var bool
     */
    private $verify_password = true;

    /**
     * @var BaseUserManager
     */
    private $baseUserManager;

    /**
     * AuthAdapter constructor.
     * @param BaseUserManager $baseUserManager
     */
    public function __construct(BaseUserManager $baseUserManager)
    {
        $this->baseUserManager = $baseUserManager;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setVerifyPassword($verify_password = true)
    {
        $this->verify_password = $verify_password;
    }

    /**
     * Performs an authentication attempt
     * Используется в AuthService
     *
     * @return Result
     */
    public function authenticate()
    {
        // Check the database if there is a user with such email.
        $user = $this->baseUserManager->getUserByLogin($this->login);

        // If there is no such user, return 'Identity Not Found' status.
        if ($user === null) {
            return new Result(
                Result::FAILURE_IDENTITY_NOT_FOUND,
                null,
                [self::ERROR_AUTHENTICATION]);
        }

        if ($this->verify_password === false) {
            // не проверяем парол просто логинем
            return new Result(
                Result::SUCCESS,
                $user->getLogin(),
                [self::SUCCESS_AUTHENTICATION]);
        }

        // Now we need to calculate hash based on user-entered password and compare
        // it with the password hash stored in database.
        $bCrypt = new Bcrypt();
        $bCrypt->create('qwerty');
        $passwordHash = $user->getPassword();

        if ($bCrypt->verify($this->password, $passwordHash)) {
            // Great! The password hash matches. Return user identity (login).
            return new Result(
                Result::SUCCESS,
                $user->getLogin(),
                [self::SUCCESS_AUTHENTICATION]);
        }

        // If password check didn't pass return 'Invalid Credential' failure status.
        return new Result(
            Result::FAILURE_CREDENTIAL_INVALID,
            null,
            [self::ERROR_AUTHENTICATION]);
    }

    /**
     * @throws \Error
     */
    public function checkCredentials()
    {
        throw new \Error('Attempt to use checkCredentials with wrong AuthAdapter');
    }

    /**
     * @return bool
     */
    public function checkLoginExist() :bool
    {
        return (bool) $this->baseUserManager->getUserByLogin($this->login);
    }
}