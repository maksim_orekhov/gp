<?php
namespace ModuleAuth\Provider;

use Interop\Container\ContainerInterface;
use ModuleAuth\Adapter\AuthAdapter;
use ModuleAuth\Adapter\AuthMultifactorAdapter;

class AuthServiceProvider
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $adapter;

    /**
     * AuthServiceProvider constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->setAdapter();
    }

    /**
     * @return mixed
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @param null $adapter_name
     * @return mixed
     */
    public function setAdapter($adapter_name = null)
    {
        if ($adapter_name !== null && $this->container->has($adapter_name)) {

            $this->adapter = $this->container->get($adapter_name);

            return $this->adapter;
        }

        $config = $this->container->get('config');

        // For multifactor authentication or automatic authentication after email confirm
        if($config['multifactor_authorization'] === true) {
            $adapter = $this->container->get(AuthMultifactorAdapter::class);
        } else {
            $adapter = $this->container->get(AuthAdapter::class);
        }

        $this->adapter = $adapter;

        return $this->adapter;
    }
}