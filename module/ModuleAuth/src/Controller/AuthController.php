<?php
namespace ModuleAuth\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Entity\Interfaces\UserInterface;
use Core\Controller\Plugin\Message\MessagePlugin;
use Core\Exception\LogicException;
use Core\Service\Base\BaseUserManager;
use Core\Service\CodeOperationInterface;
use Core\Service\SessionContainerManager;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use ModuleAuth\Controller\Traits\AuthControllerTrait;
use Zend\Http\Header\SetCookie;
use Zend\Http\Response;
use Zend\View\Model\ViewModel;
use ModuleAuth\Form\LoginForm;
use Zend\View\Model\JsonModel;
use Core\Service\OperationConfirmManager;
use Core\Form\OperationConfirmForm;

/**
 * This controller is responsible for letting the user to log in and log out.
 */
class AuthController extends AbstractRestfulController implements CodeOperationInterface
{
    use AuthControllerTrait;

    const SUCCESS_USER_LOG_IN           = 'Log in successfully';
    const SUCCESS_USER_AUTHENTICATED    = 'Authenticated successfully';
    const ERROR_INVALID_DATA_PROVIDED   = 'Invalid data provided';
    const ERROR_NO_USER_DEFINED         = 'No user defined';

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var SessionContainerManager
     */
    private $sessionContainerManager;
    /**
     * @var OperationConfirmManager
     */
    private $operationConfirmManager;
    /**
     * @var array
     */
    private $config;

    /**
     * AuthController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param BaseUserManager $baseUserManager
     * @param SessionContainerManager $sessionContainerManager
     * @param OperationConfirmManager $operationConfirmManager
     * @param $config
     */
    public function __construct(BaseAuthManager $baseAuthManager,
                                BaseUserManager $baseUserManager,
                                SessionContainerManager $sessionContainerManager,
                                OperationConfirmManager $operationConfirmManager,
                                $config)
    {
        $this->baseAuthManager         = $baseAuthManager;
        $this->baseUserManager         = $baseUserManager;
        $this->sessionContainerManager = $sessionContainerManager;
        $this->operationConfirmManager = $operationConfirmManager;
        $this->config                  = $config;
    }

    /**
     * @return TwigViewModel|JsonModel|ViewModel
     */
    public function csrfTokenAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        $isGet = $this->getRequest()->isGet();

        if ($isAjax && $isGet) {
            $csrf = new \Zend\Form\Element\Csrf('csrf');
            $csrf->setCsrfValidatorOptions([
                'timeout' => 3600
            ]);
            $data   = ['csrf' => $csrf->getValue()];

            return $this->message()->success('CSRF token return', $data);
        }

        $this->getResponse()->setStatusCode(404);
        return;
    }

    /**
     * @return TwigViewModel|mixed|JsonModel|ViewModel
     * @throws \Exception
     */
    public function loginAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            if ($this->baseAuthManager->getIdentity() !== null) {
                // Auth user redirect
                /** @var UserInterface $user */
                $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
                if (!$user) {

                    throw new LogicException(null, LogicException::USER_NOT_FOUND);
                }
                $redirect_url = $this->getRedirectUrlForLoggedUser($user);

                return $this->redirect()->toUrl($redirect_url);
            }

            $form = new LoginForm();
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);
                if ($form->isValid()) {
                    $data = $form->getData();
                    if ($this->config['multifactor_authorization']) {
                        // MULTIFACTOR AUTHORIZATION
                        $this->operationConfirmManager->checkPassword($data['login'], $data['password']);
                        // Set process initiator login and remember_me flag to session
                        $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
                            self::CODE_OPERATION_TYPE_NAME,
                            $data['login']
                        );
                        // Generate and send confirmation code
                        $this->operationConfirmManager->sendCode($data['login'], self::CODE_OPERATION_TYPE_NAME);
                        // Need for next action
                        $this->sessionContainerManager->setSessionVar('remember_me', $data['remember_me']);
                        if ($isAjax) {
                            return $this->message()->success(OperationConfirmManager::SUCCESS_CONFIRMATION_CODE_SENT);
                        }

                        return $this->forward()->dispatch(self::class, ['action' => 'confirmationCode']);
                        // END OF MULTIFACTOR AUTHORIZATION
                    }
                    // SIMPLE AUTHORIZATION
                    $result = $this->baseAuthManager->login($data['login'], $data['password'], $data['remember_me']);
                    /** @var UserInterface $user */
                    $user = $this->baseUserManager->getUserByLogin($result->getIdentity());

                    $redirect_url = $this->getRedirectUrlForLoggedUser($user);
                    // Ajax success
                    if ($isAjax) {
                        $data = [
                            'login' => $user->getLogin(),
                            'redirect_url' => $redirect_url
                        ];
                        return $this->message()->success(self::SUCCESS_USER_AUTHENTICATED, $data);
                    }
                    // Http success
                    return $this->redirect()->toUrl($redirect_url);
                }
                if($isAjax){

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }

            // set cookie
            $this->cookie()->set('login-form-data', $postData, 15);
            $this->cookie()->set('login-form-message', $form->getMessages(), 15);

            if ($isAjax) {
                $data   = ['csrf' => $form->get('csrf')->getValue()];
                return $this->message()->success('CSRF token return', $data);
            }

        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'loginForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function confirmationCodeAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            // Form creation
            $form = new OperationConfirmForm();
            $form->setAttribute('action', $this->url()->fromRoute('login/confirm'));
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                // Fill form with incoming data
                $form->setData($postData);
                if($form->isValid()) {
                    $data = $form->getData();
                    $codeType = self::CODE_OPERATION_TYPE_NAME;
                    $login = $this->sessionContainerManager->getLoginByOperationType($codeType);
                    /** @var UserInterface $user */
                    $user = $this->baseUserManager->getUserByLogin($login);
                    if ($user === null) {
                        throw new LogicException(null, LogicException::USER_NOT_FOUND);
                    }
                    // Code confirmation
                    $this->operationConfirmManager->checkCode($user, $data['code'], $codeType);
                    // Perform login attempt
                    $result = $this->baseAuthManager->login(
                        $user->getLogin(),
                        'there are no need pass', // User already authenticated before. Second authentication not needed.
                        $this->sessionContainerManager->getSessionVar('remember_me')
                    );
                    // Remove session data
                    $this->sessionContainerManager->deleteProcessInitiatorForCurrentCodeOperationType($codeType);
                    $this->sessionContainerManager->deleteSessionVar('remember_me');
                    // Ajax -
                    if($isAjax){

                        return $this->message()->success(self::SUCCESS_USER_LOG_IN, ['login' => $result->getIdentity()]);
                    }

                    return $this->redirect()->toRoute('profile');
                }
                if($isAjax){

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
            try {
                $codeType = self::CODE_OPERATION_TYPE_NAME;
                $login = $this->sessionContainerManager->getLoginByOperationType($codeType);
                /** @var UserInterface $user */
                $user = $this->baseUserManager->getUserByLogin($login);
                if ($user === null) {
                    throw new LogicException(null, LogicException::USER_NOT_FOUND);
                }
                /** @var array $user_allowed_request_code_status */
                $user_allowed_request_code_status = $this->operationConfirmManager->userAllowedRequestCodeStatus(
                    $login,
                    $codeType
                );
            }
            catch (\Throwable $t) {
                $user = null;
                $user_allowed_request_code_status = null;
            }
            if($isAjax){
                $data   = [
                    'csrf' => $form->get('csrf')->getValue(),
                    'user_allowed_request_code_status' => $user_allowed_request_code_status,
                ];

                return $this->message()->success(OperationConfirmManager::SUCCESS_CSRF_TOKEN, $data);
            }
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $view = new TwigViewModel([
            'operationConfirmForm' => $form,
            'user_allowed_request_code_status' => $user_allowed_request_code_status,
            'resend_url' => $this->url()->fromRoute('login/code-resend'),
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('module-auth/operation-confirm/auth-confirm');

        return $view;
    }

    /**
     * Resend confirmation code
     *
     * @return \Zend\Http\Response|JsonModel|ViewModel
     */
    public function confirmationCodeResendAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            $codeType = self::CODE_OPERATION_TYPE_NAME;
            $login = $this->sessionContainerManager->getLoginByOperationType($codeType);
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($login);
            if ($user === null) {
                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            // Resend confirmation code
            $this->operationConfirmManager->sendCode($login, $codeType);
            if ($isAjax) {

                return $this->message()->success(OperationConfirmManager::SUCCESS_NEW_CONFIRMATION_CODE_SENT);
            }
            // End of SUCCESS Response
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        return $this->redirect()->toRoute('login/confirm');
    }

    /**
     * The "logout" action performs logout operation
     *
     * @return \Zend\Http\Response|JsonModel
     * @throws \Exception
     */
    public function logoutAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $this->baseAuthManager->logout();
            // Response for Ajax request
            if($isAjax) {
                return $this->message()->success('Log out done');
            }

            return $this->redirect()->toRoute('login');
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }
    }

    /**
     * Not Authorized page.
     * @throws \Exception
     */
    public function notAuthorizedAction()
    {
        $this->getResponse()->setStatusCode(403);
        if(! $this->getRequest()->isXmlHttpRequest()){
            try {
                // Get current user
                $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
                $is_unconfirmed_phone = $user && !$user->getPhone()->getIsVerified();
                $is_unconfirmed_email = $user && !$user->getEmail()->getIsVerified();
            }
            catch (\Throwable $t){

                return $this->message()->exception($t);
            }

            $view = new TwigViewModel([
                'is_unconfirmed_phone' => $is_unconfirmed_phone,
                'is_unconfirmed_email' => $is_unconfirmed_email,
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        }else{

            return $this->message()->error(null, ['redirectUrl' => null], MessagePlugin::ERROR_NOT_AUTHORIZED);
        }

        return $view;
    }

    /**
     * @return TwigViewModel|JsonModel|ViewModel
     */
    public function accessDeniedAction()
    {
        if (! $this->getRequest()->isXmlHttpRequest()) {

            $view = new TwigViewModel();
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        } else {

            return $this->message()->error(null, null, MessagePlugin::ERROR_ACCESS_DENIED);
        }

        return $view;
    }

    /**
     * If already logged in
     *
     * @return ViewModel
     */
    public function loggedInAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        return $view;
    }
}