<?php
namespace ModuleAuth\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Entity\Interfaces\UserInterface;
use Core\Exception\LogicException;
use Core\Provider\Token\EncodedTokenProviderInterface;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseTokenManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\SessionContainerManager;
use Core\Service\TwigViewModel;
use Zend\View\Model\ViewModel;
use ModuleAuth\Form\PasswordResetForm;
use ModuleAuth\Form\PasswordChangeForm;
use Zend\View\Model\JsonModel;
use Core\Service\OperationConfirmManager;
use Core\Form\OperationConfirmForm;

/**
 * Class PasswordController
 * @package ModuleAuth\Controller
 */
class PasswordResetController extends AbstractRestfulController
{
    const CODE_OPERATION_TYPE_NAME  = 'PASSWORD_RESET_CONFIRMATION';
    const SUCCESS_TOKEN_SENT        = 'Ссылка для сброса пароля, отправлена на ваш электронный адрес';
    const ERROR_TOKEN_SENT          = 'Token sending fails';
    const ERROR_TOO_FREQUENT_RESET  = 'Not enough time has passed since the date of the last password reset';
    const SUCCESS_PASSWORD_CHANGED  = 'Password changed';
    const ERROR_NO_USER_DEFINED     = 'No user defined';

    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var BaseTokenManager
     */
    private $baseTokenManager;
    /**
     * @var EncodedTokenProviderInterface
     */
    private $encodedTokenProvider;
    /**
     * @var SessionContainerManager
     */
    private $sessionContainerManager;
    /**
     * @var OperationConfirmManager
     */
    private $operationConfirmManager;
    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;
    /**
     * @var array
     */
    private $config;


    /**
     * PasswordResetController constructor.
     * @param BaseUserManager $baseUserManager
     * @param BaseTokenManager $baseTokenManager
     * @param EncodedTokenProviderInterface $encodedTokenProvider
     * @param SessionContainerManager $sessionContainerManager
     * @param OperationConfirmManager $operationConfirmManager
     * @param BaseEmailManager $baseEmailManager
     * @param array $config
     */
    public function __construct(BaseUserManager $baseUserManager,
                                BaseTokenManager $baseTokenManager,
                                EncodedTokenProviderInterface $encodedTokenProvider,
                                SessionContainerManager $sessionContainerManager,
                                OperationConfirmManager $operationConfirmManager,
                                BaseEmailManager $baseEmailManager,
                                array $config)
    {
        $this->baseUserManager          = $baseUserManager;
        $this->baseTokenManager         = $baseTokenManager;
        $this->encodedTokenProvider     = $encodedTokenProvider;
        $this->sessionContainerManager  = $sessionContainerManager;
        $this->operationConfirmManager  = $operationConfirmManager;
        $this->baseEmailManager         = $baseEmailManager;
        $this->config                   = $config;
    }

    /**
     * @return TwigViewModel|JsonModel|ViewModel
     * @throws \Exception
     */
    public function resetPasswordAction()
    {
        $isAjax =  $this->getRequest()->isXmlHttpRequest();
        try {
            $codeType = self::CODE_OPERATION_TYPE_NAME;
            // Create form
            $form = new PasswordResetForm();
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);
                if ( $form->isValid() ) {
                    $data = $form->getData();
                    $email = $this->baseEmailManager->getEmailByValue($data['email']);
                    if ($email === null ) {
                        throw new LogicException(null, LogicException::EMAIL_NOT_FOUND);
                    }
                    $user = $this->baseUserManager->getUserByEmail($email);
                    if ($user === null) {
                        throw new LogicException(null, LogicException::USER_NOT_FOUND);
                    }
                    // Если 'multifactor_password_reset' = true, перехватываем авторизацию, отправляем код и просим подтвердить
                    // Логин - в сессию и, после подтверждения кода, достаем и завершаем процесс авторизации
                    if ($this->config['multifactor_password_reset']) {
                        // MULTIFACTOR PASSWORD RESETTING
                        // Set initiator to session
                        $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType($codeType, $user->getLogin());
                        // Send confirmation code
                        $this->operationConfirmManager->sendCode($user->getLogin(), $codeType);
                        if ($isAjax) {
                            return $this->message()->success(OperationConfirmManager::SUCCESS_CONFIRMATION_CODE_SENT);
                        }
                        return $this->forward()->dispatch(self::class, ['action' => 'confirmationCode']);
                        // END OF MULTIFACTOR PASSWORD RESETTING
                    }

                    // SIMPLE PASSWORD RESETTING
                    // Create token and send mail
                    $this->encodedTokenProvider->provideToken($user);

                    return $this->message()->success(self::SUCCESS_TOKEN_SENT);
                }
                if($isAjax){

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
            if($isAjax){
                $data = ['csrf' => $form->get('csrf')->getValue()];

                return $this->message()->success('CSRF token return', $data);
            }
        } catch (\Throwable $throwable) {

            return $this->message()->exception($throwable);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'passwordResetForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function confirmationCodeAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            $codeType = self::CODE_OPERATION_TYPE_NAME;
            $login = $this->sessionContainerManager->getLoginByOperationType($codeType);
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($login);
            if ($user === null) {
                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }

            $form = new OperationConfirmForm();
            $form->setAttribute('action', $this->url()->fromRoute('forgot/reset-confirm'));
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);
                if($form->isValid()){
                    $data = $form->getData();
                    // Try confirm code
                    $this->operationConfirmManager->checkCode($user, $data['code'], $codeType);
                    // Create token and send mail
                    $this->encodedTokenProvider->provideToken($user);
                    $this->sessionContainerManager->deleteProcessInitiatorForCurrentCodeOperationType($codeType);

                    return $this->message()->success(self::SUCCESS_TOKEN_SENT);
                }
                if($isAjax) {
                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
            /** @var array $user_allowed_request_code_status */
            $user_allowed_request_code_status = $this->operationConfirmManager->userAllowedRequestCodeStatus(
                $user->getLogin(),
                self::CODE_OPERATION_TYPE_NAME
            );
            if($isAjax){
                $data   = [
                    'csrf' => $form->get('csrf')->getValue(),
                    'user_allowed_request_code_status' => $user_allowed_request_code_status
                ];

                return $this->message()->success(OperationConfirmManager::SUCCESS_CSRF_TOKEN, $data);
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'operationConfirmForm' => $form,
            'user_allowed_request_code_status' => $user_allowed_request_code_status,
            'resend_url' => $this->url()->fromRoute('forgot/reset-code-resend')
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('module-auth/operation-confirm/auth-confirm');

        return $view;
    }

    /**
     * Resend confirmation code
     *
     * @return \Zend\Http\Response|JsonModel|TwigViewModel
     * @throws \Exception
     */
    public function confirmationCodeResendAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            $codeType = self::CODE_OPERATION_TYPE_NAME;
            $login = $this->sessionContainerManager->getLoginByOperationType($codeType);
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($login);
            if ($user === null) {
                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            // Resend confirmation code
            $this->operationConfirmManager->sendCode($user->getLogin(), $codeType);

            if($isAjax){

                return $this->message()->success(OperationConfirmManager::SUCCESS_NEW_CONFIRMATION_CODE_SENT);
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->redirect()->toRoute('forgot/reset-confirm');
    }

    /**
     * Set new password
     *
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function setPasswordAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        $isPost = $this->getRequest()->isPost();
        try {
            // Get user provided token from link
            $user_token = trim($this->params()->fromQuery('token', null));
            // If no token provided
            if(!$user_token) {
                return $this->message()->error('No token provided');
            }
            // Decode token
            $decoded_token = $this->baseTokenManager->decodePasswordResetToken($user_token);
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByPasswordResetToken($decoded_token['simple_token']);
            // Create form
            $form = new PasswordChangeForm($user);

            $this->baseTokenManager->validatePasswordResetToken($decoded_token, $user);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);
                if($form->isValid()) {
                    $data = $form->getData();
                    $this->baseUserManager->setUserPasswordByToken($user, $data['new_password']);

                    return $this->message()->success(self::SUCCESS_PASSWORD_CHANGED);
                }
                if($isAjax) {
                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
            if($isAjax) {
                $data = ['csrf' => $form->get('csrf')->getValue()];
                return $this->message()->success('CSRF token return', $data);
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'passwordChangeForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        return $view;
    }
}
