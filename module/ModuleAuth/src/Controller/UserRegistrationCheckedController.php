<?php
namespace ModuleAuth\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseUserManager;
use Zend\View\Model\JsonModel;
use Zend\Json\Json;

/**
 * Class UserRegistrationCheckedController
 * @package ModuleAuth\Controller
 */
class UserRegistrationCheckedController extends AbstractRestfulController
{
    const ERROR_CHECK_EMAIL_EXISTS = 'Email does not exists';
    const SUCCESS_CHECK_EMAIL_EXISTS = 'Email exists';
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var BasePhoneManager
     */
    private $basePhoneManager;
    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;

    /**
     * PhoneController constructor.
     * @param BaseUserManager $baseUserManager
     * @param BasePhoneManager $basePhoneManager
     * @param BaseEmailManager $baseEmailManager
     */
    public function __construct(BaseUserManager $baseUserManager,
                                BasePhoneManager $basePhoneManager,
                                BaseEmailManager $baseEmailManager)
    {
        $this->baseUserManager = $baseUserManager;
        $this->basePhoneManager = $basePhoneManager;
        $this->baseEmailManager = $baseEmailManager;
    }

    /**
     * Точка проверки уникальности login
     * /register/check/login
     * @return array|\Zend\Http\Response|JsonModel
     */
    public function checkIsLoginUniqueAction()
    {
        //only ajax and post
        $this->checkedOnlyAjaxAndPost();
        $content = $this->getRequest()->getContent();
        try {
            $data = Json::decode($content, Json::TYPE_ARRAY);
            if (array_key_exists('login', $data) && $this->baseUserManager->checkLoginUnique($data['login'])) {
                return new JsonModel([
                    'status' => 'SUCCESS',
                    'code' => null,
                    'message' => 'Login is available',
                    'request' => $data,
                ]);
            }

            return new JsonModel([
                'status' => 'ERROR',
                'code' => null,
                'message' => 'Login already taken',
                'request' => $data,
            ]);

        } catch (\Throwable $t) {
            // Ошибки декода и тп
            return new JsonModel([
                'status' => 'ERROR',
                'code' => null,
                'server_error' => true,
                'message' => $t->getMessage(),
                'request' => $content,
            ]);
        }
    }

    /**
     * Точка проверки уникальности Email
     * /register/check/email
     * @return \Zend\Http\Response|JsonModel
     */
    public function checkIsEmailUniqueAction()
    {
        //only ajax and post
        $this->checkedOnlyAjaxAndPost();
        $content = $this->getRequest()->getContent();
        try {
            $data = Json::decode($content, Json::TYPE_ARRAY);

            if(array_key_exists('email', $data)) {
                $data['email'] = strtolower($data['email']);
            }

            if (array_key_exists('email', $data) && $this->baseEmailManager->checkEmailUnique($data['email'])) {
                return new JsonModel([
                    'status' => 'SUCCESS',
                    'code' => null,
                    'message' => 'Email is available',
                    'request' => $data,
                ]);
            }

            return new JsonModel([
                'status' => 'ERROR',
                'code' => null,
                'message' => 'Email already taken',
                'request' => $data,
            ]);

        } catch (\Throwable $t) {

            return new JsonModel([
                'status' => 'ERROR',
                'code' => null,
                'server_error'  => true,
                'message' => $t->getMessage(),
                'request' => $content,
            ]);
        }
    }

    /**
     * Точка проверки уникальности phone
     * /register/check/phone
     * @return array|\Zend\Http\Response|JsonModel
     */
    public function checkIsPhoneUniqueAction(){
        //only ajax and post
        $this->checkedOnlyAjaxAndPost();
        $content = $this->getRequest()->getContent();
        try {
            $data = Json::decode($content, Json::TYPE_ARRAY);
            if (array_key_exists('phone', $data) && $this->basePhoneManager->checkPhoneUnique($data['phone'])) {
                return new JsonModel([
                    'status' => 'SUCCESS',
                    'code' => null,
                    'message' => 'Phone is available',
                    'request' => $data,
                ]);
            }

            return new JsonModel([
                'status' => 'ERROR',
                'code' => null,
                'message' => 'Phone already taken',
                'request' => $data,
            ]);

        } catch (\Throwable $t) {

            return new JsonModel([
                'status' => 'ERROR',
                'code' => null,
                'server_error'  => true,
                'message' => $t->getMessage(),
                'request' => $content,
            ]);
        }
    }

    /**
     * @return bool|\Core\Service\TwigViewModel|JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function checkEmailExistsAction()
    {
        //only ajax and post
        $this->checkedOnlyAjaxAndPost();
        $content = $this->getRequest()->getContent();
        try {
            $data = Json::decode($content, Json::TYPE_ARRAY);

            if(array_key_exists('email', $data) && $this->baseEmailManager->checkEmailExist($data['email'])) {
                // Return Json with SUCCESS status
                return $this->message()->success(self::SUCCESS_CHECK_EMAIL_EXISTS);
            }
        } catch (\Throwable $throwable) {

            return $this->message()->exception($throwable);
        }

        return $this->message()->error(self::ERROR_CHECK_EMAIL_EXISTS);
    }

    /**
     * @return array|void
     */
    private function checkedOnlyAjaxAndPost()
    {
        //only ajax and post
        /** @noinspection PhpUndefinedMethodInspection */
        if (!$this->getRequest()->isXmlHttpRequest() || !$this->getRequest()->isPost()) {
            /** @noinspection PhpUndefinedMethodInspection */
            $this->response->setStatusCode(404);
            /** @noinspection PhpInconsistentReturnPointsInspection */
            return ['content' => 'Method not supported'];
        }
        /** @noinspection UselessReturnInspection */
        return;
    }
}