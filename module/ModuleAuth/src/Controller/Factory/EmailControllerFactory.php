<?php
namespace ModuleAuth\Controller\Factory;

use Core\EventManager\AuthEventProvider;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseTokenManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\TwigRenderer;
use Core\Service\SessionContainerManager;
use Interop\Container\ContainerInterface;
use ModuleAuth\Controller\EmailController;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Provider\Token\EmailConfirmationTokenProvider;
use Core\Provider\Mail\EmailConfirmationTokensSender;
use Core\Service\Base\BaseAuthManager;
use Core\Provider\QrCodeProvider;

/**
 * This is the factory for EmailController
 */
class EmailControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseUserManager = $container->get(BaseUserManager::class);
        $sessionContainerManager = $container->get(SessionContainerManager::class);
        $config = $container->get('Config');
        $encodedTokenProvider = $container->get(EmailConfirmationTokenProvider::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $baseEmailManager = $container->get(BaseEmailManager::class);
        $baseTokenManager = $container->get(BaseTokenManager::class);
        $authEventProvider = $container->get(AuthEventProvider::class);

        return new EmailController(
            $baseUserManager,
            $sessionContainerManager,
            $encodedTokenProvider,
            $baseAuthManager,
            $baseEmailManager,
            $baseTokenManager,
            $authEventProvider->getEventManager(),
            $config
        );
    }
}