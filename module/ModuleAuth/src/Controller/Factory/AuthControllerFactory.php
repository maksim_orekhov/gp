<?php
namespace ModuleAuth\Controller\Factory;

use Core\Service\Base\BaseUserManager;
use Core\Service\OperationConfirmManager;
use Core\Service\SessionContainerManager;
use Interop\Container\ContainerInterface;
use ModuleAuth\Controller\AuthController;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Service\Base\BaseAuthManager;

/**
 * This is the factory for AuthController
 */
class AuthControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $baseUserManager = $container->get(BaseUserManager::class);
        $sessionContainerManager = $container->get(SessionContainerManager::class);
        $operationConfirmManager = $container->get(OperationConfirmManager::class);
        $config = $container->get('Config');

        return new AuthController(
            $baseAuthManager,
            $baseUserManager,
            $sessionContainerManager,
            $operationConfirmManager,
            $config
        );
    }
}