<?php
namespace ModuleAuth\Controller\Factory;

use Core\EventManager\AuthEventProvider;
use Interop\Container\ContainerInterface;
use ModuleAuth\Service\RegistrationManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleAuth\Controller\UserRegistrationController;

/**
 * Class UserRegistrationControllerFactory
 * @package ModuleAuth\Controller\Factory
 */
class UserRegistrationControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserRegistrationController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userRegistrationManager = $container->get(RegistrationManager::class);
        $authEventProvider = $container->get(AuthEventProvider::class);
        $config = $container->get('config');

        return new UserRegistrationController(
            $entityManager,
            $authEventProvider->getEventManager(),
            $userRegistrationManager,
            $config
        );
    }
}
