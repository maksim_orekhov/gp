<?php
namespace ModuleAuth\Controller\Factory;

use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseTokenManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\TwigRenderer;
use Core\Service\OperationConfirmManager;
use Core\Service\SessionContainerManager;
use Interop\Container\ContainerInterface;
use ModuleAuth\Controller\PasswordResetController;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Provider\Token\PasswordResettingTokenProvider;
use Core\Provider\Mail\PasswordResettingTokenSender;

/**
 * This is the factory for PasswordResetController
 */
class PasswordResetControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseUserManager = $container->get(BaseUserManager::class);
        $config = $container->get('Config');
        $twigRenderer = $container->get(TwigRenderer::class);
        $baseTokenManager = $container->get(BaseTokenManager::class);
        $mailSender = new PasswordResettingTokenSender($twigRenderer, $config);
        $encodedTokenProvider = new PasswordResettingTokenProvider($mailSender, $baseTokenManager);
        $sessionContainerManager = $container->get(SessionContainerManager::class);
        $operationConfirmManager = $container->get(OperationConfirmManager::class);
        $baseEmailManager = $container->get(BaseEmailManager::class);

        return new PasswordResetController(
            $baseUserManager,
            $baseTokenManager,
            $encodedTokenProvider,
            $sessionContainerManager,
            $operationConfirmManager,
            $baseEmailManager,
            $config
        );
    }
}