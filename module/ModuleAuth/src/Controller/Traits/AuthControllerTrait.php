<?php
namespace ModuleAuth\Controller\Traits;

use Core\Entity\Interfaces\UserInterface;

trait AuthControllerTrait
{
    /**
     * @param UserInterface|null $user
     * @return string
     */
    public function getRedirectUrlForLoggedUser(UserInterface $user = null)
    {
        /** @var string $redirect_url */
        $redirect_url = $this->params()->fromQuery('redirectUrl', null);

        if ( $redirect_url ) {

            return $redirect_url;
        }

        if ( $user && $this->isUserVerified($user) ) {

            return $this->url()->fromRoute('deal');
        }

        return $this->url()->fromRoute('profile');
    }

    /**
     * @param UserInterface $user
     * @return bool
     */
    public function isUserVerified(UserInterface $user)
    {
        return $user->getPhone()->getIsVerified() && $user->getEmail()->getIsVerified();
    }
}