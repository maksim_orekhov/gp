<?php
namespace ModuleAuth\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Entity\Interfaces\UserInterface;
use Core\EventManager\AuthEventProvider;
use Core\Service\ORMDoctrineUtil;
use Core\Service\TwigViewModel;
use Doctrine\ORM\EntityManager;
use ModuleAuth\Service\RegistrationManager;
use Zend\EventManager\EventManager;
use Zend\Mvc\MvcEvent;
use ModuleAuth\Form\UserRegistrationForm;

/**
 * Class UserRegistrationController
 * @package ModuleAuth\Controller
 */
class UserRegistrationController extends AbstractRestfulController
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var EventManager
     */
    private $eventManager;
    /**
     * @var RegistrationManager
     */
    private $userRegistrationManager;
    /**
     * @var array
     */
    private $config;

    /**
     * UserRegistrationController constructor.
     * @param EntityManager $entityManager
     * @param EventManager $eventManager
     * @param RegistrationManager $userRegistrationManager
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                EventManager $eventManager,
                                RegistrationManager $userRegistrationManager,
                                array $config)
    {
        $this->entityManager           = $entityManager;
        $this->eventManager            = $eventManager;
        $this->userRegistrationManager = $userRegistrationManager;
        $this->config = $config;
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function createAction()
    {
        try {
            // DB Transaction start
            $this->entityManager->getConnection()->beginTransaction();

            $this->eventManager->trigger(AuthEventProvider::EVENT_BEFORE_REGISTRATION, $this);

            /** @var MvcEvent $mvcEvent */
            $mvcEvent = $this->getEvent();
            $form = new UserRegistrationForm($mvcEvent->getParam('user_email', null));

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                if (array_key_exists('email', $postData)) {
                    $postData['email'] = strtolower($postData['email']);
                }
                // Заполняем форму данными.
                $form->setData($postData);

                if ($form->isValid()) {
                    // Получаем валидированные данные формы.
                    $formData = $form->getData();
                    // Add user to DB
                    $user = $this->userRegistrationManager->addNewUser($formData);
                    $this->eventManager->trigger(AuthEventProvider::EVENT_SUCCESS_REGISTRATION, $this, ['user' => $user]);
                    // DB Transaction commit
                    $this->entityManager->getConnection()->commit();

                    $redirect_url = $mvcEvent->getParam('redirect_url', $this->url()->fromRoute('login'));

                    //ajax
                    if ($this->getRequest()->isXmlHttpRequest()) {

                        return $this->message()->success('success create', [
                            'redirect_url' => $redirect_url,
                        ]);
                    }
                    //http
                    return $this->redirect()->toUrl($redirect_url);
                }
                if ($this->getRequest()->isXmlHttpRequest()) {

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
        }catch (\Throwable $t) {
            // DB Transaction rollback
            ORMDoctrineUtil::rollBackTransaction($this->entityManager);

            return $this->message()->exception($t);
        }

        if($this->getRequest()->isXmlHttpRequest()){
            $data = [
                'csrf' => $form->get('csrf')->getValue()
            ];

            return $this->message()->success('CSRF token return', $data);
        }

        // HTTP -
        $form->prepare();
        $view = new TwigViewModel([
            'userRegistrationForm' => $form,
        ]);
        $view->setTemplate('module-auth/user-registration/create');
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        //для переопределения View модели
        $this->eventManager->trigger(AuthEventProvider::EVENT_VIEW_MODEL_REGISTRATION, $this, [
            'view' => $view,
        ]);

        return $view;
    }

    /**
     * Удаление регистрации пользователя;
     * @throws \Exception
     */
    public function deleteAction(){
        //разрешаем роут только через включенную опцию в конфиге
        if (!array_key_exists('delete_user_route', $this->config) || $this->config['delete_user_route'] === false) {

            return $this->redirect()->toRoute('access-denied');
        }

        $accepted_params= $this->params()->fromRoute();
        if (isset($accepted_params['id'])) {

            $this->userRegistrationManager->deleteUserRegistrationByUserId($accepted_params['id']);
            $view = new TwigViewModel([
                'accepted_id' => $accepted_params['id']
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

            return $view;
        }

        $allUsers = $this->userRegistrationManager->getAllUsers();
        $allUsersOutput = [];
        /** @var UserInterface $user */
        foreach ($allUsers as $user){
            $allUsersOutput[] = [
                'id'=> $user->getId(),
                'login'=> $user->getLogin(),
                'phone'=> $user->getPhone()->getUserEnter(),
                'email'=> $user->getEmail()->getEmail(),
                'password'=> $user->getPassword(),
            ];
        }

        $view = new TwigViewModel([
            'allUsers' => $allUsersOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }
}
