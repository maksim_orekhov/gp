<?php
namespace Application;

use Doctrine\Common\EventManager;
use Zend\Mvc\MvcEvent;

/**
 * Class Module
 * @package Application
 */
class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig(): array
    {
        return array_replace_recursive(
            require __DIR__ . '/../config/module.config.php',
            require __DIR__ . '/../config/router.config.php',
            require __DIR__ . '/../config/landing.router.config.php',
            require __DIR__ . '/../config/assets.config.php',
            require __DIR__ . '/../config/rbac.config.php',
            require __DIR__ . '/../config/doctrine.config.php',
            require __DIR__ . '/../config/controllers.config.php',
            require __DIR__ . '/../config/service.config.php',
            require __DIR__ . '/../config/views.config.php'
        );
    }

    /**
     * @param MvcEvent $event
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function onBootstrap(MvcEvent $event)
    {
        $application = $event->getApplication();
        // Get service manager
        $serviceManager = $application->getServiceManager();

        //Set session
        $this->setInitiatorVarToSession($serviceManager, $serviceManager->get('Config'));
        $doctrineEntityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        /** @var EventManager $doctrineEventManager */
        $doctrineEventManager = $doctrineEntityManager->getEventManager();
        $doctrineEventManager
            ->addEventListener(
                array(\Doctrine\ORM\Events::onFlush, \Doctrine\ORM\Events::postFlush),
                $serviceManager->get(\Application\Listener\DeliveryListener::class)
            );
    }

    /**
     * @param $serviceManager
     * @param $config
     * @throws \Exception
     */
    public function setInitiatorVarToSession($serviceManager, $config)
    {
        try {
            $sessionContainer = $serviceManager->get('ContainerNamespace');
            // Check if not exists and set var initiator in session
            if (!$sessionContainer->initiator) {

                $sessionContainer->initiator = array();
            }
        }
        catch (\Throwable $t) {
            // Session validation failed
//            if ($config['hide_exceptions']['session_validation_failed']) {
//                // Instantiate SessionManager as default one
//                /** @var SessionManager $sessionManager */
////                $sessionManager = $serviceManager->get(SessionManager::class);
////                $sessionManager->destroy();
////                $sessionManager->start();
////                $sessionManager->regenerateId();
//                session_unset();
//                $sessionContainer = $serviceManager->get('ContainerNamespace');
//
//            } else { // If $config['hide_exceptions']['session_validation_failed'] == false
//
//                throw new \Exception($t->getMessage());
//            }
        }


    }
}
