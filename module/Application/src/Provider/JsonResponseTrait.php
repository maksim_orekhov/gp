<?php
namespace Application\Provider;

use Zend\View\Model\JsonModel;

/**
 * Trait JsonResponseTrait
 * @package Application\Provider
 */
trait JsonResponseTrait
{
    /**
     * @param null|string   $message
     * @param null|array    $data
     * @return JsonModel
     */
    private function getSuccessJsonResponse($message=null, $data=null)
    {
        return new JsonModel([
            'status'    => 'SUCCESS',
            'message'   => $message,
            'data'      => $data
        ]);
    }

    /**
     * @param null|string $message
     * @param null $data
     * @return JsonModel
     */
    private function getErrorJsonResponse($message=null, $data=null)
    {
        return new JsonModel([
            'status'    => 'ERROR',
            'message'   => $message,
            'data'      => $data
        ]);
    }
}