<?php

namespace Application\Provider\Token;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Service\TokenManager;
use Core\Provider\Mail\MailSenderInterface;
use Core\Entity\Interfaces\UserInterface;
use Core\Provider\Token\EncodedTokenProviderInterface;

/**
 * Class DealInvitationTokenProvider
 * @package Application\Provider\Token
 */
class DealInvitationTokenProvider implements EncodedTokenProviderInterface
{
    /**
     * @var MailSenderInterface
     */
    private $mailSender;
    /**
     * @var TokenManager
     */
    private $tokenManager;

    /**
     * DealInvitationTokenProvider constructor.
     * @param MailSenderInterface|null $mailSender
     * @param TokenManager $tokenManager
     */
    public function __construct(MailSenderInterface $mailSender = null,
                                TokenManager $tokenManager)
    {
        $this->mailSender = $mailSender;
        $this->tokenManager = $tokenManager;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function provideToken($user, array $param = [])
    {
        /** @var DealAgent $counterDealAgent */
        $counterDealAgent = $param['counterDealAgent'];
        /** @var Deal $deal */
        $deal = $param['deal'];
        $email = $counterDealAgent->getEmail();

       // Generate encoded token
        $encoded_token = $this->encodedToken($user, [
            'email' => $email,
            'dealId' => $deal->getId(),
            'dealAgentId' => $counterDealAgent->getId(),
        ]);

        $param['encoded_token'] = $encoded_token;

        $this->send($email, $param);

        return $encoded_token;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function encodedToken($user, array $param = [])
    {
        return $this->tokenManager->createInvitationUserToken($param['email'], $param['dealId'], $param['dealAgentId']);
    }

    /**
     * @param string $email
     * @param array $param
     * @return mixed
     */
    public function send(string $email, array $param = [])
    {
        return $this->mailSender->sendMail($email, $param);
    }
}