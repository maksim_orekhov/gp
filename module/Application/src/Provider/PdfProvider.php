<?php
namespace Application\Provider;

use Zend\Mvc\Controller\AbstractActionController;

class PdfProvider extends AbstractActionController
{
    /**
     * @var \TCPDF
     */
    private $tcpdf;

    /**
     * @var array
     */
    private $config;


    /**
     * PdfProvider constructor.
     * @param \TCPDF $tspdf
     * @param array $config
     */
    public function __construct(\TCPDF $tspdf, array $config)
    {
        $this->tcpdf    = $tspdf;
        $this->config   = $config;
    }

    /**
     * @param string $html
     * @param string $name
     * @param string|null $where
     * @return string
     * @throws \Exception
     */
    public function generatePdfFromHtml(string $html, string $name, string $where=null)
    {
        $pdf = $this->tcpdf;

        $pdf->SetFont('arialnarrow', '', 12, '', false);
        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');

        if($where) {
            return $pdf->Output($name, $where);
        } else {
            $pdf->Output($name);
        }
    }

    /**
     * @param string $html
     * @param string $name
     * @param string|null $where
     * @return string
     */
    public function generateInvoicePdfFromHtml(string $html, string $name, string $where=null)
    {
        $pdf = $this->tcpdf;

        $pdf->SetFont('arialnarrow', '', 10, '', false);
        $pdf->AddPage();

        try {
            $logo = file_get_contents($this->config['pdf']['main_logo']);
            $signature = file_get_contents($this->config['pdf']['signature']);
            $seal = file_get_contents($this->config['pdf']['seal']);
        }
        catch (\Throwable $t) {
            $logo = null;
            $signature = null;
            $seal = null;
        }

        $pdf->Image('@'.$logo,11,14);
        $pdf->Image('@'.$signature,30,203, 35, 35);
        $pdf->Image('@'.$seal,43,205, 35, 35);
        $pdf->Image('@'.$signature,140,203, 35, 35);


        $pdf->writeHTML($html, true, false, true, false, '');


        if($where) {
            return $pdf->Output($name, $where);
        } else {
            $pdf->Output($name);
            return '';
        }
    }

    /**
     * @param string $html
     * @param string $file_name
     * @param string $title
     * @return string
     * @ TODO Генерация PDF этим методом из FileController падала с кучей ошибок. Перенос генерации в DealManager решил проблему.
     */
    public function generateDealContract(string $html, string $file_name, string $title)
    {
        $pdf = $this->tcpdf;

        $pdf->SetFont('arialnarrow', '', 12, '', false);

        $pdf->AddPage();

        // set document information
        #$pdf->SetCreator('GP');
        #$pdf->SetAuthor('GP');
        #$pdf->SetTitle($title);

        #$logo = file_get_contents($_SERVER['DOCUMENT_ROOT'].'img/landing/logo_top.png');
        #$pdf->Image('@'.$logo);

        $pdf->writeHTML($html, true, false, true, false, '');

        // Return PFD as string
        return $pdf->Output($file_name, 'S');
    }
}