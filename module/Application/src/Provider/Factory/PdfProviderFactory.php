<?php
namespace Application\Provider\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Provider\PdfProvider;
use Application\Extension\TcpdfExtension;

class PdfProviderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $tcpdf = $container->get(TcpdfExtension::class);
        $config = $container->get('Config');

        return new PdfProvider($tcpdf, $config);
    }
}