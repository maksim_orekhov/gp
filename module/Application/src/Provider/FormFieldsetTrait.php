<?php
namespace Application\Provider;

/**
 * Trait FormFieldsetTrait
 * @package Application\Provider
 */
trait FormFieldsetTrait
{
    private $fieldsetNames;

    /**
     * @param null $key
     * @return mixed
     */
    public function getFieldsetNames($key = null)
    {
        if ($key === null) {
            return $this->fieldsetNames;
        }

        return (array_key_exists($key, $this->fieldsetNames)) ? $this->fieldsetNames[$key] : [];
    }

    /**
     * @param array $fieldsets[
     *      'my-name-fieldset' => [
     *          'type' => MyClassFieldset::class,
     *          'count' => 3,
     *      ]
     * ]
     * @return array
     */
    public function attachFieldsets(array $fieldsets)
    {
        $list = [];
        foreach ($fieldsets as $name => $fieldsetParams) {
            $list[$name] = [];
            $count = $fieldsetParams['count'];
            $className = $fieldsetParams['type'];

            if ($fieldsetParams['count'] <= 0){
                continue;
            }

            if ($fieldsetParams['count'] > 1) {
                for ($i = 0; $i < $count; $i++) {
                    $newName = $name."_{$i}";
                    $list[$name][] = $newName;
                    $this->add(new $className($newName));
                }
            } else {
                $list[$name][] = $name;
                $this->add(new $className($name));
            }
        }

        $this->fieldsetNames = $list;
    }
}