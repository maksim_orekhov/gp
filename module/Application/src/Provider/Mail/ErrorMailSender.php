<?php
namespace Application\Provider\Mail;
use Core\Service\TwigRenderer;

/**
 * Class ErrorMailSender
 * @package Application\Provider\Mail
 */
class ErrorMailSender extends MailSender
{
    const ERROR_REPORT = 'ERROR_REPORT';
    const ERROR_INVALID_FORM_DATA = 'ERROR_INVALID_FORM_DATA';
    const ERROR_NOT_AUTHORIZED = 'ERROR_NOT_AUTHORIZED';
    const ERROR_ACCESS_DENIED = 'ERROR_ACCESS_DENIED';
    const PHONE_INVALID_CODE = 'PHONE_INVALID_CODE';
    const PHONE_COUNTRY_NOT_DEFINED = 'PHONE_COUNTRY_NOT_DEFINED';

    /**
     * @var TwigRenderer
     */
    private $twigRenderer;

    public function __construct(TwigRenderer $twigRenderer,
                                array $config)
    {
        parent::__construct($config);

        $this->twigRenderer = $twigRenderer;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        switch ($data['code']) {
            case self::ERROR_INVALID_FORM_DATA:
                $this->send($email_value, self::SUBJECT_TITLE.self::ERROR_INVALID_FORM_DATA, $data['message'], null);
                break;
            case self::ERROR_NOT_AUTHORIZED:
                $this->send($email_value, self::SUBJECT_TITLE.self::ERROR_NOT_AUTHORIZED, $data['message'], null);
                break;
            case self::ERROR_ACCESS_DENIED:
                $this->send($email_value, self::SUBJECT_TITLE.self::ERROR_ACCESS_DENIED, $data['message'], null);
                break;
            case self::PHONE_INVALID_CODE:
                $this->send($email_value, self::SUBJECT_TITLE.self::PHONE_INVALID_CODE, $data['message'], null);
                break;
            case self::PHONE_COUNTRY_NOT_DEFINED:
                $this->send($email_value, self::SUBJECT_TITLE.self::PHONE_COUNTRY_NOT_DEFINED, $data['message'], null);
                break;
            case self::ERROR_REPORT:
                $this->sendErrorReport($email_value, $data);
                break;
            default:
                $this->send($email_value, self::SUBJECT_TITLE.$data['code'], $data['message'], null);
        }
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    public function sendErrorReport($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];
        $subject = self::SUBJECT_TITLE.'Error report';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'user_email' => $data['user_email'],
            'error_message' => $data['error_message'],
            'error_tracing' => $data['error_tracing'],
            'error_report' => $data['error_report'],
            'main_project_url'  => $main_project_url,
        ]);
        $twigRenderer->setTemplate('twig/error_report');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }
}