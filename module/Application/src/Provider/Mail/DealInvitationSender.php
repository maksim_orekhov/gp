<?php
namespace Application\Provider\Mail;

use Application\Entity\Deal;
use Core\Service\TwigRenderer;

/**
 * Class DealInvitationSender
 * @package Application\Provider\Mail
 */
class DealInvitationSender extends MailSender
{
    use \Application\Provider\Mail\PrepareOutputForMailTrait;

    const ERROR_TOKEN_IS_ABSENT = 'Trying send email without token data provided';
    const ERROR_DEAL_IS_ABSENT = 'Trying send change notification email without Deal data provided';

    const DEAL_NOTIFICATION = 'notification';
    const DEAL_REGISTRATION = 'registration';
    const DEAL_OWNER_NOTIFICATION = 'owner_notification';
    const DEAL_OWNER_REGISTRATION = 'owner_registration';

    const REGISTRATION_AFTER_CREATE_DEAL_FOR_COUNTER_AGENT = 'registration_after_create_deal_for_counter_agent';
    const REGISTRATION_AFTER_CREATE_DEAL_FOR_OWNER = 'registration_after_create_deal_for_owner';

    /**
     * @var \Core\Service\TwigRenderer $twigRenderer
     */
    private $twigRenderer;

    /**
     * DealInvitationSender constructor.
     * @param \Core\Service\TwigRenderer $twigRenderer
     * @param array $config
     */
    public function __construct(TwigRenderer $twigRenderer,
                                array $config)
    {
        parent::__construct($config);

        $this->twigRenderer = $twigRenderer;
    }

    /**
     * @param string $email_value
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        switch ($data['deal_invitation_type'])
        {
            case self::DEAL_REGISTRATION:
                return $this->sendMailToCounterAgentRegistrationForDeal($email_value, $data);
            case self::DEAL_NOTIFICATION:
                return $this->sendMailToCounterAgentInvitationForDeal($email_value, $data);
            case self::DEAL_OWNER_NOTIFICATION:
                return $this->sendMailAuthorAboutDealCreation($email_value, $data);
            case self::DEAL_OWNER_REGISTRATION:
                return $this->sendMailAuthorRegistrationForDeal($email_value, $data);
            case self::REGISTRATION_AFTER_CREATE_DEAL_FOR_COUNTER_AGENT:
                return $this->sendMailRegistrationCounterAgentAfterCreateDeal($email_value, $data);
            case self::REGISTRATION_AFTER_CREATE_DEAL_FOR_OWNER:
                return $this->sendMailRegistrationOwnerAfterCreateDeal($email_value, $data);
            default:
                return false;
        }
    }

    /**
     * @param $email_value
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function sendMailToCounterAgentRegistrationForDeal($email_value, $data): bool
    {
        $this->validateDealRegistrationData($data);

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutPut = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutPut['author_deal_agent_name'] = $data['author_deal_agent_name'];
        $dealOutPut['counter_deal_agent_role'] = $data['counter_deal_agent_role'];
        $dealOutPut['counter_deal_agent_name'] = $data['counter_deal_agent_name'];
        $dealOutPut['safe_number'] = $this->getSafeNumber($dealOutPut['number']);

        $token = $data['encoded_token'];
        $resend = array_key_exists('is_resend', $data) && $data['is_resend'] ? ' (повторное приглашение)': '';
        $subject = self::SUBJECT_TITLE.'Приглашение на регистрацию для участия в сделке номер '.$dealOutPut['safe_number'].$resend;
        $main_project_url = $this->config['main_project_url'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'main_project_url' => $main_project_url,
            'invitation_url' => $main_project_url . 'register?token=' . $token,
            'invitation_token' => $token,
            'deal' => $dealOutPut,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/registration_for_deal');
        $html = $twigRenderer->getHtml();

        return $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $email_value
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function sendMailToCounterAgentInvitationForDeal($email_value, $data): bool
    {
        //checking data
        $this->validateDealInvitationData($data);

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutPut = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutPut['author_deal_agent_name'] = $data['author_deal_agent_name'];
        $dealOutPut['counter_deal_agent_role'] = $data['counter_deal_agent_role'];
        $dealOutPut['counter_deal_agent_name'] = $data['counter_deal_agent_name'];
        $dealOutPut['safe_number'] = $this->getSafeNumber($dealOutPut['number']);
        $dealOutPut['type_ident'] = $deal->getDealType()->getIdent();

        $resend = array_key_exists('is_resend', $data) && $data['is_resend'] ? ' (повторное приглашение)': '';
        $subject = self::SUBJECT_TITLE.'Приглашение к участию в сделке номер '.$dealOutPut['safe_number'].$resend;
        $main_project_url = $this->config['main_project_url'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutPut,
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/invitation_to_deal');
        $html = $twigRenderer->getHtml();

        return $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailAuthorAboutDealCreation($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $dealOutPut['type_ident'] = $deal->getDealType()->getIdent();
        $resend = array_key_exists('is_resend', $data) && $data['is_resend'] ? ' (повторное приглашение)': '';
        $subject = self::SUBJECT_TITLE.'Сделка создана'.$resend;

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'author_deal_agent_name' => $data['author_deal_agent_name'],
            'counter_deal_agent_name' => $data['counter_deal_agent_name'],
            'counter_deal_agent_role' => $data['counter_deal_agent_role'],
            'main_project_url'  => $main_project_url,
        ]);
        $template = 'twig/invitation_to_deal_for_author';
        $twigRenderer->setTemplate($template);
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailAuthorRegistrationForDeal($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $resend = array_key_exists('is_resend', $data) && $data['is_resend'] ? ' (повторное приглашение)': '';
        $subject = self::SUBJECT_TITLE.'Сделка создана'.$resend;
        $token = $data['encoded_token'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'author_deal_agent_name' => $data['author_deal_agent_name'],
            'counter_deal_agent_name' => $data['counter_deal_agent_name'],
            'counter_deal_agent_role' => $data['counter_deal_agent_role'],
            'invitation_url' => $main_project_url . 'register?token=' . $token,
            'invitation_token' => $token,
            'main_project_url'  => $main_project_url,
        ]);
        $template = 'twig/registration_owner_to_deal';
        $twigRenderer->setTemplate($template);
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    public function sendMailRegistrationCounterAgentAfterCreateDeal($email_value, $data)
    {
        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $main_project_url = $this->config['main_project_url'];
        $token = $data['encoded_token'];
        $email_confirmation_url = $main_project_url . 'email/confirm?token=' . $token;
        $resend = array_key_exists('is_resend', $data) && $data['is_resend'] ? ' (повторное приглашение)': '';
        $subject = self::SUBJECT_TITLE.'Приглашение к участию в сделке номер '.$dealOutput['safe_number'].$resend;

        // Html body
        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'subject' => $subject,
            'login' => $data['login'],
            'email' => $data['email'],
            'password'  => $data['password'],
            'email_confirmation_url' => $email_confirmation_url,
            'deal' => $dealOutput,
            'author_deal_agent_name' => $data['author_deal_agent_name'],
            'counter_deal_agent_name' => $data['counter_deal_agent_name'],
            'counter_deal_agent_role' => $data['counter_deal_agent_role'],
            'main_project_url' => $main_project_url,
        ]);
        $twigRenderer->setTemplate('twig/registration_counter_agent_after_create_deal');
        $html = $twigRenderer->getHtml();

        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    public function sendMailRegistrationOwnerAfterCreateDeal($email_value, $data)
    {
        $resend = array_key_exists('is_resend', $data) && $data['is_resend'] ? ' (повторное приглашение)': '';
        $subject = self::SUBJECT_TITLE.'Сделка создана'.$resend;

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $main_project_url = $this->config['main_project_url'];
        $token = $data['encoded_token'];
        $email_confirmation_url = $main_project_url . 'email/confirm?token=' . $token;
        // Html body
        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'subject' => $subject,
            'login' => $data['login'],
            'email' => $data['email'],
            'password'  => $data['password'],
            'email_confirmation_url' => $email_confirmation_url,
            'deal' => $dealOutput,
            'author_deal_agent_name' => $data['author_deal_agent_name'],
            'counter_deal_agent_name' => $data['counter_deal_agent_name'],
            'counter_deal_agent_role' => $data['counter_deal_agent_role'],
            'main_project_url' => $main_project_url,
        ]);
        $twigRenderer->setTemplate('twig/registration_owner_after_create_deal');
        $html = $twigRenderer->getHtml();

        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $data
     * @throws \Exception
     * @return void
     */
    private function validateDealRegistrationData($data)
    {
        if ( !array_key_exists('encoded_token', $data) ) {

            throw new \Exception(self::ERROR_TOKEN_IS_ABSENT);
        }
    }

    /**
     * @param $data
     * @throws \Exception
     * @return void
     */
    private function validateDealInvitationData($data)
    {
        if(!array_key_exists('deal', $data) || empty($data['deal'])) {

            throw new \Exception(self::ERROR_DEAL_IS_ABSENT);
        }
    }
}