<?php
namespace Application\Provider\Mail;

use Application\Entity\Deal;
use Core\Service\TwigRenderer;

/**
 * Class DealNotificationSender
 * @package Application\Provider\Mail
 */
class DealNotificationSender extends MailSender
{
    use \Application\Provider\Mail\PrepareOutputForMailTrait;

    const ERROR_DEAL_CHANGED_PROPERTIES_ARE_ABSENT = 'Trying send change notification email without changed properties provided';
    const ERROR_DEAL_IS_ABSENT = 'Trying send change notification email without Deal data provided';
    const INVALID_DATA = 'Trying send notification email with invalid data';

    const TYPE_NOTIFY_KEY = 'type_notify';

    const TYPE_NOTIFY_CONFIRMATION = 'confirmation_from_deal_agent';
    const TYPE_NOTIFY_CHANGE = 'change_from_deal_agent';

    const TYPE_NOTIFY_DELIVERY_SERVICE_ORDER_CREATION_CONTRACTOR = 'delivery_service_order_creation_contractor';
    const TYPE_NOTIFY_DELIVERY_SERVICE_ORDER_CREATION_CUSTOMER = 'delivery_service_order_creation_customer';

    const TYPE_NOTIFY_SUCCESSFUL_DELIVERY_CONTRACTOR = 'delivery_successful_contractor';
    const TYPE_NOTIFY_SUCCESSFUL_DELIVERY_CUSTOMER = 'delivery_successful_customer';

    const TYPE_NOTIFY_UNSUCCESSFUL_DELIVERY_CONTRACTOR = 'delivery_unsuccessful_contractor';
    const TYPE_NOTIFY_UNSUCCESSFUL_DELIVERY_CUSTOMER = 'delivery_unsuccessful_customer';

    const TYPE_NOTIFY_DELIVERY_CONFIRM_CONTRACTOR = 'delivery_confirm_contractor';
    const TYPE_NOTIFY_DELIVERY_CONFIRM_CUSTOMER = 'delivery_confirm_customer';

    const DEAL_CLOSED_FOR_CONTRACTOR = 'deal_closed_for_contractor';
    const DEAL_CLOSED_FOR_CUSTOMER = 'deal_closed_for_customer';

    /**
     * @var \Core\Service\TwigRenderer $twigRenderer
     */
    private $twigRenderer;

    /**
     * DealNotificationSender constructor.
     * @param \Core\Service\TwigRenderer $twigRenderer
     * @param array $config
     */
    public function __construct(TwigRenderer $twigRenderer,
                                array $config)
    {
        parent::__construct($config);

        $this->twigRenderer = $twigRenderer;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        //checking data
        $this->checkValidityData($data);

        switch ($data[self::TYPE_NOTIFY_KEY]) {
            case self::TYPE_NOTIFY_CONFIRMATION:
                $this->sendMailWhenDealAgentConfirm($email_value, $data);
                break;
            case self::TYPE_NOTIFY_CHANGE:
                $this->sendMailWhenChangedDeal($email_value, $data);
                break;

            case self::TYPE_NOTIFY_DELIVERY_SERVICE_ORDER_CREATION_CONTRACTOR:
                $this->sendMailContractorAboutDeliveryServiceOrderCreation($email_value, $data);
                break;
            case self::TYPE_NOTIFY_DELIVERY_SERVICE_ORDER_CREATION_CUSTOMER:
                $this->sendMailCustomerAboutDeliveryServiceOrderCreation($email_value, $data);
                break;

            case self::TYPE_NOTIFY_SUCCESSFUL_DELIVERY_CONTRACTOR:
                $this->sendMailContractorAboutSuccessfulDelivery($email_value, $data);
                break;
            case self::TYPE_NOTIFY_SUCCESSFUL_DELIVERY_CUSTOMER:
                $this->sendMailCustomerAboutSuccessfulDelivery($email_value, $data);
                break;

            case self::TYPE_NOTIFY_UNSUCCESSFUL_DELIVERY_CONTRACTOR:
                $this->sendMailContractorAboutUnsuccessfulDelivery($email_value, $data);
                break;
            case self::TYPE_NOTIFY_UNSUCCESSFUL_DELIVERY_CUSTOMER:
                $this->sendMailCustomerAboutUnsuccessfulDelivery($email_value, $data);
                break;

            case self::TYPE_NOTIFY_DELIVERY_CONFIRM_CONTRACTOR:
                $this->sendMailContractorAboutDeliveryConfirmed($email_value, $data);
                break;
            case self::TYPE_NOTIFY_DELIVERY_CONFIRM_CUSTOMER:
                $this->sendMailCustomerAboutDeliveryConfirmed($email_value, $data);
                break;

            case self::DEAL_CLOSED_FOR_CONTRACTOR:
                $this->sendMailContractorAboutDealClosed($email_value, $data);
                break;
            case self::DEAL_CLOSED_FOR_CUSTOMER:
                $this->sendMailCustomerAboutDealClosed($email_value, $data);
                break;
        }
    }

    /**
     * уведомление для продовца о неуспешной доставке
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutUnsuccessfulDelivery($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Неудачная попытка доставки по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'tracking_number' => $data['tracking_number'],
            'trace_page_url' => $data['trace_page_url'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CUSTOMER',
        ]);
        $twigRenderer->setTemplate('twig/delivery_unsuccessful_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для покупателя о неуспешной доставке
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutUnsuccessfulDelivery($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Неудачная попытка доставки по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'tracking_number' => $data['tracking_number'],
            'trace_page_url' => $data['trace_page_url'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/delivery_unsuccessful_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для продовца об успешной доставке
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutSuccessfulDelivery($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Успешная доставка по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CUSTOMER',
        ]);
        $twigRenderer->setTemplate('twig/delivery_successful_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для покупателя об успешной доставке
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutSuccessfulDelivery($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Успешная доставка по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/delivery_successful_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Customer о создании заказа на доставку
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutDeliveryServiceOrderCreation($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Создан заказ на доставку для сделки номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'tracking_number'   => $data['tracking_number'],
            'trace_page_url'    => $data['trace_page_url'],
            'customer_login'    => $data['customer_login'],
            'main_project_url'  => $main_project_url,
        ]);
        $twigRenderer->setTemplate('twig/delivery_service_order_creation_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Contractor о создании заказа на доставку
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutDeliveryServiceOrderCreation($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Создан заказ на доставку для сделки номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'tracking_number'   => $data['tracking_number'],
            'trace_page_url'    => $data['trace_page_url'],
            'contractor_login'  => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
        ]);
        $twigRenderer->setTemplate('twig/delivery_service_order_creation_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Customer о закрытии сделки
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutDealClosed($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Сделка номер '.$dealOutput['safe_number'].' закрыта' ;

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/deal_closed_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Contractor о закрытии сделки
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutDealClosed($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Сделка номер '.$dealOutput['safe_number'].' закрыта' ;

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
        ]);
        $twigRenderer->setTemplate('twig/deal_closed_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для продовца о подтверждении доставки
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutDeliveryConfirmed($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Подтверждение доставки по сделке номер '.$dealOutput['safe_number'];
        if($dealOutput['type_ident'] === 'U') {
            $subject = self::SUBJECT_TITLE.'Подтверждение выполнения услуги по сделке номер '.$dealOutput['safe_number'];
        }

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CUSTOMER',
        ]);
        $twigRenderer->setTemplate('twig/delivery_confirmed_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для покупателя о подтверждении доставки
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutDeliveryConfirmed($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Подтверждение доставки по сделке номер '.$dealOutput['safe_number'];
        if($dealOutput['type_ident'] === 'U') {
            $subject = self::SUBJECT_TITLE.'Подтверждение выполнения услуги по сделке номер '.$dealOutput['safe_number'];
        }

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/delivery_confirmed_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailWhenDealAgentConfirm($email_value, $data)
    {
        //check valid data
        $this->checkValidityDataForDealAgentConfirm($data);

        /** @var Deal $deal */
        $deal = $data['deal'];

        $main_project_url = $this->config['main_project_url'];

        $dealOutPut = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutPut['agent_user_login'] = $data['agent_user_login'];
        $dealOutPut['counteragent_login'] = $data['counteragent_login'];
        $dealOutPut['changed_properties'] = $data['deal_changed_properties'];
        $dealOutPut['civil_law_subject_name'] = $data['civil_law_subject_name'];
        $dealOutPut['civil_law_subject_type'] = $data['civil_law_subject_type'];
        $dealOutPut['safe_number'] = $this->getSafeNumber($dealOutPut['number']);
        $subject = self::SUBJECT_TITLE.'Подтверждение сделки номер '.$dealOutPut['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutPut,
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/deal_agent_confirm');
        $html = $twigRenderer->getHtml();

        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailWhenChangedDeal($email_value, $data)
    {
        //check valid data
        $this->checkValidityDataForChangedDeal($data);

        /** @var Deal $deal */
        $deal = $data['deal'];

        $main_project_url = $this->config['main_project_url'];

        $dealOutPut = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutPut['agent_user_login'] = $data['agent_user_login'];
        $dealOutPut['counteragent_login'] = $data['counteragent_login'];
        $dealOutPut['changed_properties'] = $data['deal_changed_properties'];
        $dealOutPut['safe_number'] = $this->getSafeNumber($dealOutPut['number']);
        $subject = self::SUBJECT_TITLE.'Изменение условий сделки номер '.$dealOutPut['safe_number'];


        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutPut,
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/deal_changed');
        $html = $twigRenderer->getHtml();

        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function checkValidityData($data)
    {
        if(!array_key_exists(self::TYPE_NOTIFY_KEY, $data) || empty($data[self::TYPE_NOTIFY_KEY])) {

            throw new \Exception(self::INVALID_DATA);
        }
        return true;
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function checkValidityDataForDealAgentConfirm($data)
    {
        if(!array_key_exists('deal', $data) || empty($data['deal'])) {

            throw new \Exception(self::ERROR_DEAL_IS_ABSENT);
        }
        if(!array_key_exists('deal_changed_properties', $data) || empty($data['deal_changed_properties'])) {

            throw new \Exception(self::ERROR_DEAL_CHANGED_PROPERTIES_ARE_ABSENT);
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function checkValidityDataForChangedDeal($data)
    {
        return $this->checkValidityDataForDealAgentConfirm($data);
    }
}