<?php

namespace Application\Provider\Mail;

use Application\Entity\Deal;
use Core\Service\TwigRenderer;
use Application\Provider\PdfProvider;

/**
 * Class DealConfirmationSender
 * @package Application\Provider\Mail
 */
class DealConfirmationSender extends MailSender
{
    use PrepareOutputForMailTrait;

    const ERROR_DEAL_IS_ABSENT = 'Trying send change notification email without Deal data provided';
    const TYPE_NOTIFY_CONTRACTOR = 'contractor';
    const TYPE_NOTIFY_CUSTOMER = 'customer';

    /**
     * @var TwigRenderer
     */
    private $twigRenderer;

    /**
     * @var PdfProvider
     */
    private $pdfProvider;

    /**
     * DealConfirmationSender constructor.
     * @param TwigRenderer $twigRenderer
     * @param PdfProvider $pdfProvider
     * @param array $config
     */
    public function __construct(TwigRenderer $twigRenderer,
                                PdfProvider $pdfProvider,
                                array $config)
    {
        parent::__construct($config);

        $this->pdfProvider     = $pdfProvider;
        $this->twigRenderer    = $twigRenderer;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        if ( $data['type_notify'] === self::TYPE_NOTIFY_CUSTOMER ){
            $this->sendNotifyCustomerAboutConfirmedDeal($email_value, $data);
        } else {
            $this->sendNotifyContractorAboutConfirmedDeal($email_value, $data);
        }
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendNotifyCustomerAboutConfirmedDeal($email_value, $data)
    {
        $this->validateMailCustomerData($data);

        /** @var Deal $deal */
        $deal = $data['deal'];

        $main_project_url = $this->config['main_project_url'];

        $dealOutPut = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutPut['agent_user_login'] = $data['agent_user_login'];
        $dealOutPut['counteragent_login'] = $data['counteragent_login'];
        $dealOutPut['safe_number'] = $this->getSafeNumber($dealOutPut['number']);
        $subject = self::SUBJECT_TITLE.'Достигнуто соглашение по сделке номер '.$dealOutPut['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutPut,
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);

        $twigRenderer->setTemplate('twig/deal_confirm_to_customer');

        $html = $twigRenderer->getHtml();

        $invoiceHtml = $this->getInvoiceHtml($deal, $data['invoice_view_vars']);

        // Send
        $this->send($email_value, $subject, $html, null, $invoiceHtml);
    }

    /**
     * contractor
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendNotifyContractorAboutConfirmedDeal($email_value, $data)
    {
        $this->validateMailContractorData($data);

        /** @var Deal $deal */
        $deal = $data['deal'];

        $main_project_url = $this->config['main_project_url'];

        $dealOutPut = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutPut['agent_user_login'] = $data['agent_user_login'];
        $dealOutPut['counteragent_login'] = $data['counteragent_login'];
        $dealOutPut['safe_number'] = $this->getSafeNumber($dealOutPut['number']);
        $subject = self::SUBJECT_TITLE.'Достигнуто соглашение по сделке номер '.$dealOutPut['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutPut,
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CUSTOMER'
        ]);

        $twigRenderer->setTemplate('twig/deal_confirm_to_contractor');

        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function validateMailContractorData($data)
    {
        if(!array_key_exists('deal', $data) || empty($data['deal'])) {

            throw new \Exception(self::ERROR_DEAL_IS_ABSENT);
        }
        return true;
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function validateMailCustomerData($data)
    {
        if(!array_key_exists('deal', $data) || empty($data['deal'])
            || !array_key_exists('invoice_view_vars', $data) || empty($data['invoice_view_vars'])) {

            throw new \Exception(self::ERROR_DEAL_IS_ABSENT);
        }
        return true;
    }

    /**
     * @param Deal $deal
     * @param $invoiceViewVars
     * @return array
     * @throws \Exception
     */
    private function getInvoiceHtml(Deal $deal, $invoiceViewVars)
    {
        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel($invoiceViewVars);
        $twigRenderer->setTemplate('application/invoice/single');
        $invoice_html = $twigRenderer->getHtml();

        // Get Pdf
        $date = new \DateTime();
        $file_name = 'invoice-'.$deal->getId().'-'.$date->format('Ymd').'.pdf';
        return [
            'file_name' => $file_name,
            'file_content' => $this->pdfProvider->generateInvoicePdfFromHtml($invoice_html, $file_name, 'S'),
            'file_type' => 'application/pdf'
        ];
    }
}