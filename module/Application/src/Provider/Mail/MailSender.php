<?php
namespace Application\Provider\Mail;

use Core\Provider\Mail\BaseMailSender;

/**
 * Class MailSender
 * @package Application\Provider\Mail
 */
abstract class MailSender extends BaseMailSender
{
    /**
     * @param $deal_number
     * @return bool|string
     */
    protected function getSafeNumber($deal_number)
    {
        return str_replace('#','', $deal_number);
    }
}