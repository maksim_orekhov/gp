<?php
namespace Application\Provider\Mail;

use Application\Entity\Deal;
use Core\Service\TwigRenderer;

/**
 * Class PaymentReceiptSender
 * @package Application\Provider\Mail
 */
class PaymentReceiptSender extends MailSender
{
    use \Application\Provider\Mail\PrepareOutputForMailTrait;

    const TYPE_NOTIFY_KEY = 'type_notify';

    const PAYMENT_RECEIPT_CUSTOMER = 'payment_receipt_customer';
    const MANDARIN_PAYMENT_RECEIPT_CUSTOMER = 'mandarin_payment_receipt_customer';

    const PAYMENT_RECEIPT_CONTRACTOR = 'payment_receipt_contractor';
    const MANDARIN_PAYMENT_RECEIPT_CONTRACTOR = 'mandarin_payment_receipt_contractor';

    const ERROR_DEAL_IS_ABSENT = 'Trying send change notification email without Deal data provided';

    const TYPE_NOTIFY_CONTRACTOR = 'contractor';
    const TYPE_NOTIFY_CUSTOMER = 'customer';
    const PAYMENT_PART = 'payment_part';
    const PAYMENT_FULL = 'payment_full';


    /**
     * @var TwigRenderer
     */
    private $twigRenderer;

    /**
     * DealConfirmationSender constructor.
     * @param TwigRenderer $twigRenderer
     * @param array $config
     */
    public function __construct(TwigRenderer $twigRenderer,
                                array $config)
    {
        parent::__construct($config);

        $this->twigRenderer    = $twigRenderer;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        switch ($data[self::TYPE_NOTIFY_KEY]) {
            case self::PAYMENT_RECEIPT_CUSTOMER:
                $this->sendNotifyCustomerAboutPaymentReceipt($email_value, $data);
                break;
            case self::PAYMENT_RECEIPT_CONTRACTOR:
                $this->sendNotifyContractorAboutPaymentReceipt($email_value, $data);
                break;
            case self::MANDARIN_PAYMENT_RECEIPT_CUSTOMER:
                $this->sendNotifyCustomerAboutPaymentReceipt($email_value, $data);
                break;
            case self::MANDARIN_PAYMENT_RECEIPT_CONTRACTOR:
                $this->sendNotifyContractorAboutPaymentReceipt($email_value, $data);
                break;
        }
    }

    /**
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendNotifyCustomerAboutPaymentReceipt($email_value, $data)
    {
        $this->validateMailCustomerData($data);

        /** @var Deal $deal */
        $deal = $data['deal'];

        $main_project_url = $this->config['main_project_url'];

        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['agent_user_login'] = $data['agent_user_login'];
        $dealOutput['counteragent_login'] = $data['counteragent_login'];
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Поступление средств по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'payment_receipt_type' => $data['payment_receipt_type'],
            'pay_amount' => $data['pay_amount'],
            'main_project_url'  => $main_project_url,
        ]);

        if ($data['payment_receipt_type'] === self::PAYMENT_FULL) {
            $twigRenderer->setTemplate('twig/payment_receipt_to_customer_full');
        } else {
            $twigRenderer->setTemplate('twig/payment_receipt_to_customer_part');
        }

        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * contractor
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendNotifyContractorAboutPaymentReceipt($email_value, $data)
    {
        $this->validateMailContractorData($data);

        /** @var Deal $deal */
        $deal = $data['deal'];

        $main_project_url = $this->config['main_project_url'];

        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['agent_user_login'] = $data['agent_user_login'];
        $dealOutput['counteragent_login'] = $data['counteragent_login'];
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Поступление средств по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'payment_receipt_type' => $data['payment_receipt_type'],
            'pay_amount' => $data['pay_amount'],
            'main_project_url'  => $main_project_url
        ]);

        if ($data['payment_receipt_type'] === self::PAYMENT_FULL) {
            $twigRenderer->setTemplate('twig/payment_receipt_to_contractor_full');
        } else {
            $twigRenderer->setTemplate('twig/payment_receipt_to_contractor_part');
        }


        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function validateMailContractorData($data): bool
    {
        if (!array_key_exists('deal', $data) || empty($data['deal'])) {

            throw new \Exception(self::ERROR_DEAL_IS_ABSENT);
        }

        return true;
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function validateMailCustomerData($data): bool
    {
        if(!array_key_exists('deal', $data) || empty($data['deal'])) {

            throw new \Exception(self::ERROR_DEAL_IS_ABSENT);
        }

        return true;
    }
}