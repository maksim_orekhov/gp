<?php
namespace Application\Provider\Mail;

use Application\Entity\Deal;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\WarrantyExtension;
use Application\Service\Dispute\DisputeManager;

trait PrepareOutputForMailTrait
{
    public function getDealOutputForEmail(Deal $deal, $config)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $warranty_extension_day = 0;
        $warranty_extension = $dispute ? DisputeManager::getLastWarrantyExtension($dispute->getWarrantyExtensions()) : null;
        if ($warranty_extension) {
            $warranty_extension_day = $warranty_extension->getDays();
        }

        $contractor = $deal->getContractor();
        $customer = $deal->getCustomer();

        $dealOutput = [];
        $dealOutput['id'] = $deal->getId();
        $dealOutput['number'] = $deal->getNumber();
        $dealOutput['name'] = $deal->getName();
        $dealOutput['type'] = $deal->getDealType()->getName();
        $dealOutput['type_ident'] = $deal->getDealType()->getIdent();
        $dealOutput['addon_terms'] = $deal->getAddonTerms();
        $dealOutput['fee'] = $payment->getFee();
        $dealOutput['fee_payer_option'] = $deal->getFeePayerOption()->getName();
        $dealOutput['amount_without_fee'] = $payment->getDealValue();
        $dealOutput['customer_amount'] = $payment->getExpectedValue();
        $dealOutput['contractor_amount'] = $payment->getReleasedValue();
        $dealOutput['delivery_period'] = $deal->getDeliveryPeriod();
        $dealOutput['created'] = $deal->getCreated()->format('d.m.Y');
        $dealOutput['confirm_url'] = $config['main_project_url'] . 'deal/' . $deal->getId() . '/confirm-conditions';
        $dealOutput['deal_url'] = $config['main_project_url'] . 'deal/show/' . $deal->getId();
        $dealOutput['deal_url_edit'] = $config['main_project_url'] . 'deal/edit/' . $deal->getId();
        $dealOutput['deal_close_date'] = null;
        $dealOutput['author_email'] = $customer->getUser() ? $customer->getUser()->getEmail() : null;
        $dealOutput['author_phone'] = $customer->getUser() ? $customer->getUser()->getEmail() : null;
        $dealOutput['dispute'] = $deal->getDispute();

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $dealOutput['payment_expected'] = $payment->getExpectedValue();
        $dealOutput['payment_released'] = $payment->getReleasedValue();
        $dealOutput['contractor_login'] = $contractor->getUser() ? $contractor->getUser()->getLogin() : null;
        $dealOutput['customer_login'] = $customer->getUser() ? $customer->getUser()->getLogin() : null;

        $dealOutput['discount_requests'] = null;
        if (null !== $dispute && $dispute->getDiscountRequests()) {
            $dealOutput['discount_requests'] = $dispute->getDiscountRequests();
        }

        if ($payment->getDeFactoDate()) {
            $deFactoDate = clone $payment->getDeFactoDate();
            $days = $payment->getDeal()->getDeliveryPeriod() + $warranty_extension_day;
            $dealOutput['deal_close_date'] = $deFactoDate->modify('+'.$days.' days')->format('d.m.Y');
        }

        return $dealOutput;
    }
}