<?php
namespace Application\Provider\Mail\Factory;

use Application\Provider\Mail\ErrorMailSender;
use Core\Service\TwigRenderer;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ErrorMailSenderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $twigRenderer = $container->get(TwigRenderer::class);

        return new ErrorMailSender($twigRenderer, $config);
    }
}