<?php
namespace Application\Provider\Mail;

use Application\Entity\Deal;
use Core\Service\TwigRenderer;
/**
 * Class TrackingNumberSender
 * @package Application\Provider\Mail
 */
class TrackingNumberSender extends MailSender
{
    use \Application\Provider\Mail\PrepareOutputForMailTrait;

    const ERROR_DEAL_IS_ABSENT = 'Trying send change notification email without Deal data provided';

    /**
     * @var \Core\Service\TwigRenderer $twigRenderer
     */
    private $twigRenderer;

    /**
     * TrackingNumberSender constructor.
     * @param \Core\Service\TwigRenderer $twigRenderer
     * @param array $config
     */
    public function __construct(TwigRenderer $twigRenderer,
                                array $config)
    {
        parent::__construct($config);

        $this->twigRenderer = $twigRenderer;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        //checking data
        $this->checkValidityData($data);

        /** @var Deal $deal */
        $deal = $data['deal'];

        $main_project_url = $this->config['main_project_url'];

        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['agent_user_login'] = $data['agent_user_login'];
        $dealOutput['counteragent_login'] = $data['counteragent_login'];
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Трек-номер по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'tracking_number' => $data['tracking_number'],
            'main_project_url'  => $main_project_url,
        ]);
        $twigRenderer->setTemplate('twig/tracking_number_customer');
        $html = $twigRenderer->getHtml();

        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function checkValidityData($data)
    {
        if(!array_key_exists('deal', $data) || empty($data['deal'])) {

            throw new \Exception(self::ERROR_DEAL_IS_ABSENT);
        }

        return true;
    }
}