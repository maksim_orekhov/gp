<?php

namespace Application\Provider\Mail;

use Application\Entity\Deal;
use Core\Service\TwigRenderer;

/**
 * Class DealDeadlineNotificationSender
 * @package Application\Provider\Mail
 */
class DealDeadlineNotificationSender extends MailSender
{
    use \Application\Provider\Mail\PrepareOutputForMailTrait;

    const INVALID_DATA = 'Trying send notification email with invalid data';

    const TYPE_NOTIFY_KEY = 'type_notify';

    const TYPE_NOTIFY_NEAR_DEADLINE_FOR_CUSTOMER = 'near_deadline_for_customer';
    const TYPE_NOTIFY_NEAR_DEADLINE_FOR_CONTRACTOR = 'near_deadline_for_contractor';

    /**
     * @var TwigRenderer
     */
    private $twigRenderer;

    /**
     * DealDeadlineNotificationSender constructor.
     * @param TwigRenderer $twigRenderer
     * @param array $config
     */
    public function __construct(TwigRenderer $twigRenderer,
                                array $config)
    {
        parent::__construct($config);

        $this->twigRenderer = $twigRenderer;
    }

    /**
     * {@inheritdoc}
     */
    public function sendMail($email_value, $data)
    {
        $this->validateData($data);

        switch ($data[self::TYPE_NOTIFY_KEY]){
            case self::TYPE_NOTIFY_NEAR_DEADLINE_FOR_CUSTOMER:
                $this->sendMailCustomerAboutNearDeadline($email_value, $data);
                break;
            case self::TYPE_NOTIFY_NEAR_DEADLINE_FOR_CONTRACTOR:
                $this->sendMailContractorAboutNearDeadline($email_value, $data);
                break;
        }
    }

    /**
     * Уведомление о скором окончании срока гарантии для кастомера
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutNearDeadline($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Срок гарантии по сделке номер '.$dealOutput['safe_number'].' заканчивается!';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/near_deadline_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * Уведомление о скором окончании срока гарантии для контрактора
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutNearDeadline($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Срок гарантии по сделке номер '.$dealOutput['safe_number'].' заканчивается!';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/near_deadline_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function validateData($data)
    {
        if(!array_key_exists(self::TYPE_NOTIFY_KEY, $data) || empty($data[self::TYPE_NOTIFY_KEY])) {

            throw new \Exception(self::INVALID_DATA);
        }
        return true;
    }
}