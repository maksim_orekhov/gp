<?php

namespace Application\Provider\Mail;

use Application\Entity\Deal;
use Core\Service\TwigRenderer;

/**
 * Class DisputeNotificationSender
 * @package Application\Provider\Mail
 */
class DisputeNotificationSender extends MailSender
{
    use \Application\Provider\Mail\PrepareOutputForMailTrait;

    const INVALID_DATA = 'Trying send notification email with invalid data';

    const TYPE_NOTIFY_KEY = 'type_notify';

    const OPEN_DISPUTE_FOR_CUSTOMER = 'open_dispute_customer';
    const OPEN_DISPUTE_FOR_CONTRACTOR = 'open_dispute_contractor';

    const CUSTOMER_CLOSED_DISPUTE_FOR_CONTRACTOR = 'customer_closed_dispute_for_contractor';
    const CUSTOMER_CLOSED_DISPUTE_FOR_CUSTOMER = 'customer_closed_dispute_for_customer';

    const CLOSED_DISPUTE_WITH_DAY_FOR_CUSTOMER = 'closed_dispute_with_day_for_customer';
    const CLOSED_DISPUTE_WITH_DAY_FOR_CONTRACTOR = 'closed_dispute_with_day_for_contractor';

    const CLOSED_DISPUTE_WITHOUT_DAY_FOR_CUSTOMER = 'closed_dispute_without_day_for_customer';
    const CLOSED_DISPUTE_WITHOUT_DAY_FOR_CONTRACTOR = 'closed_dispute_without_day_for_contractor';

    // Discount
    const DISCOUNT_REQUEST_FOR_CUSTOMER = 'discount_request_for_customer';
    const DISCOUNT_REQUEST_FOR_CONTRACTOR = 'discount_request_for_contractor';

    const DISCOUNT_REFUSE_FOR_CUSTOMER = 'discount_refuse_for_customer';
    const DISCOUNT_REFUSE_FOR_CONTRACTOR = 'discount_refuse_for_contractor';

    const DISCOUNT_CONFIRM_FOR_AUTHOR_REQUEST = 'discount_confirm_for_author_request';
    const DISCOUNT_CONFIRM_FOR_CONTRACTOR = 'discount_confirm_for_contractor';

    const DISCOUNT_PROVIDED_FOR_CUSTOMER = 'discount_provided_for_customer';
    const DISCOUNT_PROVIDED_FOR_CONTRACTOR = 'discount_provided_for_contractor';

    // Refund
    const REFUND_REQUEST_FOR_CUSTOMER = 'refund_request_for_customer';
    const REFUND_REQUEST_FOR_CONTRACTOR = 'refund_request_for_contractor';

    const REFUND_REFUSE_FOR_AUTHOR_REQUEST = 'refund_refuse_for_author_request';

    const REFUND_REQUEST_ACCEPTED_FOR_CONTRACTOR = 'refund_request_accepted_for_contractor';
    const REFUND_REQUEST_ACCEPTED_FOR_CUSTOMER = 'refund_request_accepted_for_customer';

    const REFUND_PROVIDED_FOR_CUSTOMER = 'refund_provided_for_customer';
    const REFUND_PROVIDED_FOR_CONTRACTOR = 'refund_provided_for_contractor';

    // Arbitrage
    const ARBITRAGE_REQUEST_FOR_CUSTOMER = 'arbitrage_request_for_customer';
    const ARBITRAGE_REQUEST_FOR_CONTRACTOR = 'arbitrage_request_for_contractor';

    const ARBITRAGE_REFUSE_FOR_CUSTOMER = 'arbitrage_refuse_for_customer';
    const ARBITRAGE_REFUSE_FOR_CONTRACTOR = 'arbitrage_refuse_for_contractor';

    const ARBITRAGE_REQUEST_AFTER_DISCOUNT_REQUESTS_FOR_CUSTOMER = 'arbitrage_request_after_discount_requests_for_customer';
    const ARBITRAGE_REQUEST_AFTER_DISCOUNT_REQUESTS_FOR_CONTRACTOR = 'arbitrage_request_after_discount_requests_for_contractor';

    const ARBITRAGE_CONFIRM_FOR_AUTHOR_REQUEST = 'arbitrage_confirm_for_author_request';

    // Tribunal
    const TRIBUNAL_REQUEST_FOR_CUSTOMER = 'tribunal_request_for_customer';
    const TRIBUNAL_REQUEST_FOR_CONTRACTOR = 'tribunal_request_for_contractor';

    const TRIBUNAL_CONFIRM_FOR_AUTHOR_REQUEST = 'tribunal_confirm_for_author_request';
    const TRIBUNAL_REFUSE_FOR_AUTHOR_REQUEST = 'tribunal_refuse_for_author_request';

    // Это для чего?
    const REFERRED_TO_TRIBUNAL_FOR_CUSTOMER = 'referred_to_tribunal_for_customer';
    const REFERRED_TO_TRIBUNAL_FOR_CONTRACTOR = 'referred_to_tribunal_for_contractor';


    /**
     * @var TwigRenderer
     */
    private $twigRenderer;

    /**
     * DisputeNotificationSender constructor.
     * @param TwigRenderer $twigRenderer
     * @param array $config
     */
    public function __construct(TwigRenderer $twigRenderer,
                                array $config)
    {
        parent::__construct($config);

        $this->twigRenderer = $twigRenderer;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        $this->validateData($data);

        switch ($data[self::TYPE_NOTIFY_KEY]){
            case self::OPEN_DISPUTE_FOR_CUSTOMER:
                $this->sendMailCustomerAboutOpenDispute($email_value, $data);
                break;
            case self::OPEN_DISPUTE_FOR_CONTRACTOR:
                $this->sendMailContractorAboutOpenDispute($email_value, $data);
                break;
            case self::CUSTOMER_CLOSED_DISPUTE_FOR_CONTRACTOR:
                $this->sendMailContractorAboutCustomerClosedDispute($email_value, $data);
                break;
            case self::CUSTOMER_CLOSED_DISPUTE_FOR_CUSTOMER:
                $this->sendMailCustomerAboutCustomerClosedDispute($email_value, $data);
                break;
            case self::CLOSED_DISPUTE_WITH_DAY_FOR_CUSTOMER:
                $this->sendMailCustomerAboutOperatorClosedDisputeWithDay($email_value, $data);
                break;
            case self::CLOSED_DISPUTE_WITH_DAY_FOR_CONTRACTOR:
                $this->sendMailContractorAboutOperatorClosedDisputeWithDay($email_value, $data);
                break;
            case self::CLOSED_DISPUTE_WITHOUT_DAY_FOR_CUSTOMER:
                $this->sendMailCustomerAboutOperatorClosedDisputeWithoutDay($email_value, $data);
                break;
            case self::CLOSED_DISPUTE_WITHOUT_DAY_FOR_CONTRACTOR:
                $this->sendMailContractorAboutOperatorClosedDisputeWithoutDay($email_value, $data);
                break;
            case self::DISCOUNT_PROVIDED_FOR_CUSTOMER:
                $this->sendMailCustomerAboutDiscountProvided($email_value, $data);
                break;
            case self::DISCOUNT_PROVIDED_FOR_CONTRACTOR:
                $this->sendMailContractorAboutDiscountProvided($email_value, $data);
                break;
            case self::REFUND_PROVIDED_FOR_CUSTOMER:
                $this->sendMailCustomerAboutRefundProvided($email_value, $data);
                break;
            case self::REFUND_PROVIDED_FOR_CONTRACTOR:
                $this->sendMailContractorAboutRefundProvided($email_value, $data);
                break;
            case self::REFERRED_TO_TRIBUNAL_FOR_CUSTOMER:
                $this->sendMailCustomerAboutReferredToTribunal($email_value, $data);
                break;
            case self::REFERRED_TO_TRIBUNAL_FOR_CONTRACTOR:
                $this->sendMailContractorAboutReferredToTribunal($email_value, $data);
                break;
            case self::DISCOUNT_REQUEST_FOR_CUSTOMER:
                $this->sendMailCustomerAboutDiscountRequest($email_value, $data);
                break;
            case self::DISCOUNT_REQUEST_FOR_CONTRACTOR:
                $this->sendMailContractorAboutDiscountRequest($email_value, $data);
                break;
            case self::DISCOUNT_CONFIRM_FOR_AUTHOR_REQUEST:
                $this->sendMailAuthorRequestAboutDiscountConfirmed($email_value, $data);
                break;
            case self::DISCOUNT_CONFIRM_FOR_CONTRACTOR:
                $this->sendMailContractorAboutDiscountConfirmed($email_value, $data);
                break;
            case self::DISCOUNT_REFUSE_FOR_CUSTOMER:
                $this->sendMailCustomerAboutDiscountRefused($email_value, $data);
                break;
            case self::DISCOUNT_REFUSE_FOR_CONTRACTOR:
                $this->sendMailContractorAboutDiscountRefused($email_value, $data);
                break;
            case self::ARBITRAGE_REQUEST_FOR_CUSTOMER:
                $this->sendMailCustomerAboutArbitrageRequest($email_value, $data);
                break;
            case self::ARBITRAGE_REQUEST_FOR_CONTRACTOR:
                $this->sendMailContractorAboutArbitrageRequest($email_value, $data);
                break;
            case self::ARBITRAGE_CONFIRM_FOR_AUTHOR_REQUEST:
                $this->sendMailAuthorRequestAboutArbitrageConfirmed($email_value, $data);
                break;
            case self::ARBITRAGE_REFUSE_FOR_CUSTOMER:
                $this->sendCustomerRequestAboutArbitrageRefused($email_value, $data);
                break;
            case self::ARBITRAGE_REFUSE_FOR_CONTRACTOR:
                $this->sendContractorRequestAboutArbitrageRefused($email_value, $data);
                break;
            case self::ARBITRAGE_REQUEST_AFTER_DISCOUNT_REQUESTS_FOR_CUSTOMER:
                $this->sendMailCustomerAboutArbitrageRequestOnDiscountRequestOverlimit($email_value, $data);
                break;
            case self::ARBITRAGE_REQUEST_AFTER_DISCOUNT_REQUESTS_FOR_CONTRACTOR:
                $this->sendMailContractorAboutArbitrageRequestOnDiscountRequestOverlimit($email_value, $data);
                break;
            case self::REFUND_REQUEST_FOR_CUSTOMER:
                $this->sendMailCustomerAboutRefundRequest($email_value, $data);
                break;
            case self::REFUND_REQUEST_FOR_CONTRACTOR:
                $this->sendMailContractorAboutRefundRequest($email_value, $data);
                break;
            case self::REFUND_REQUEST_ACCEPTED_FOR_CUSTOMER:
                $this->sendMailCustomerAboutRefundConfirmed($email_value, $data);
                break;
            case self::REFUND_REQUEST_ACCEPTED_FOR_CONTRACTOR:
                $this->sendMailContractorAboutRefundConfirmed($email_value, $data);
                break;
            case self::REFUND_REFUSE_FOR_AUTHOR_REQUEST:
                $this->sendMailAuthorRequestAboutRefundRefused($email_value, $data);
                break;
            case self::TRIBUNAL_REQUEST_FOR_CUSTOMER:
                $this->sendMailCustomerAboutTribunalRequest($email_value, $data);
                break;
            case self::TRIBUNAL_REQUEST_FOR_CONTRACTOR:
                $this->sendMailContractorAboutTribunalRequest($email_value, $data);
                break;
            case self::TRIBUNAL_CONFIRM_FOR_AUTHOR_REQUEST:
                $this->sendMailAuthorRequestAboutTribunalConfirmed($email_value, $data);
                break;
            case self::TRIBUNAL_REFUSE_FOR_AUTHOR_REQUEST:
                $this->sendMailAuthorRequestAboutTribunalRefused($email_value, $data);
                break;
        }
    }

    /**
     * уведомление для Customer о создании запроса на Arbitrage после отказов от запросов на скидку
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutArbitrageRequestOnDiscountRequestOverlimit($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Согласительная комиссия по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'arbitrageRequest' => $data['arbitrageRequestOutput'],
            'contractor_login' => $data['contractor_login'],
            'customer_login' => $data['customer_login'],
            'discount_amount'  => $data['discount_amount'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/arbitrage_request_after_discount_requests_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Contractor о создании запроса на Arbitrage после отказов от запросов на скидку
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutArbitrageRequestOnDiscountRequestOverlimit($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Согласительная комиссия по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'arbitrageRequest' => $data['arbitrageRequestOutput'],
            'contractor_login' => $data['contractor_login'],
            'customer_login' => $data['customer_login'],
            'discount_amount'  => $data['discount_amount'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/arbitrage_request_after_discount_requests_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }


    /**
     * Уведомление о передаче спора в третейский суд для кастомера
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutReferredToTribunal($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Спор по сделке номер '.$dealOutput['safe_number'].' передан в третейский суд';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'tribunal' => $data['tribunalOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/referred_to_tribunal_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * Уведомление о передаче спора в третейский суд для контрактора
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutReferredToTribunal($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Спор по сделке номер '.$dealOutput['safe_number'].' передан в третейский суд';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'tribunal' => $data['tribunalOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CUSTOMER'
        ]);
        $twigRenderer->setTemplate('twig/referred_to_tribunal_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление о возврате средств для кастомера
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutRefundProvided($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Принято решение о возврате средств по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'refund' => $data['refundOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/refund_provided_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление о возврате средств для контрактора
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutRefundProvided($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Принято решение о возврате средств по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'refund' => $data['refundOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/refund_provided_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление о пердоставлении скидки для кастомера
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutDiscountProvided($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Предоставлена скидка по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'discount' => $data['discountOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);

        $twigRenderer->setTemplate('twig/discount_provided_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление о пердоставлении скидки для контрактора
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutDiscountProvided($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Предоставлена скидка по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'discount' => $data['discountOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);

        $twigRenderer->setTemplate('twig/discount_provided_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление о закрытии спора оператором без продления гарантии для кастомера
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutOperatorClosedDisputeWithoutDay($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Спор по сделке номер '.$dealOutput['safe_number'].' закрыт без продления периода гарантии';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/closed_dispute_without_day_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление о закрытии спора оператором без продления гарантии для контрактора
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutOperatorClosedDisputeWithoutDay($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Спор по сделке номер '.$dealOutput['safe_number'].' закрыт без продления периода гарантии';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/closed_dispute_without_day_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление о закрытии спора оператором с продлением дней для кастомера
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutOperatorClosedDisputeWithDay($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Спор по сделке номер '.$dealOutput['safe_number'].' закрыт и продлен период гарантии';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'extension_days' => $data['extension_days'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/closed_dispute_with_day_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление о закрытии спора оператором с продлением дней для контрактора
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutOperatorClosedDisputeWithDay($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Спор по сделке номер '.$dealOutput['safe_number'].' закрыт и продлен период гарантии';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'extension_days' => $data['extension_days'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/closed_dispute_with_day_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление о закрытии спора кастомером для контрактора
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutCustomerClosedDispute($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Спор по сделке номер '.$dealOutput['safe_number'].' отозван';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/customer_closed_dispute_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление о закрытии спора кастомером для кастомера
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutCustomerClosedDispute($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Спор по сделке номер '.$dealOutput['safe_number'].' отозван';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/customer_closed_dispute_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     *  уведомление об открытии спора для кастомера
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutOpenDispute($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Открыт спор по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'contractor_email' => $data['contractor_email'],
            'contractor_phone' => $data['contractor_phone'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/dispute_open_for_customer');
        $html = $twigRenderer->getHtml();

        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление об открытии спора для контрактора
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutOpenDispute($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Открыт спор по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'dispute' => $data['disputeOutput'],
            'deal' => $dealOutput,
            'customer_login' => $data['customer_login'],
            'customer_email' => $data['customer_email'],
            'customer_phone' => $data['customer_phone'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CUSTOMER'
        ]);
        $twigRenderer->setTemplate('twig/dispute_open_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Customer о запросе на скидку
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutDiscountRequest($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на скидку по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'discountRequest' => $data['discountRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/discount_request_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Contractor о запросе на скидку
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutDiscountRequest($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на скидку по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'discountRequest' => $data['discountRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/discount_request_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для автора запроса о подтверждении скидки
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailAuthorRequestAboutDiscountConfirmed($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на скидку по сделке номер '.$dealOutput['safe_number'].' подтвержден';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'discountRequest' => $data['discountRequestOutput'],
            'contractor_login' => $data['contractor_login'],
            'customer_login' => $data['customer_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);

        $twigRenderer->setTemplate('twig/discount_confirm_for_author_request');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для автора запроса о подтверждении скидки
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutDiscountConfirmed($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на скидку по сделке номер '.$dealOutput['safe_number'].' подтвержден';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'discountRequest' => $data['discountRequestOutput'],
            'contractor_login' => $data['contractor_login'],
            'customer_login' => $data['customer_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CUSTOMER'
        ]);

        $twigRenderer->setTemplate('twig/discount_confirm_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Customer об отказе в предоставлении скидки
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutDiscountRefused($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на скидку по сделке номер '.$dealOutput['safe_number'].' отклонен';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'discountRequest' => $data['discountRequestOutput'],
            'contractor_login' => $data['contractor_login'],
            'contractor_email' => $data['contractor_email'],
            'contractor_phone' => $data['contractor_phone'],
            'customer_login' => $data['customer_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/discount_refuse_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Contractor об отказе в предоставлении скидки
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutDiscountRefused($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на скидку по сделке номер '.$dealOutput['safe_number'].' отклонен';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'discountRequest' => $data['discountRequestOutput'],
            'customer_login' => $data['customer_login'],
            'customer_email' => $data['customer_email'],
            'customer_phone' => $data['customer_phone'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/discount_refuse_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Customer о запросе на арбитражную комиссию
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutArbitrageRequest($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на согласительную комиссию по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'arbitrageRequest' => $data['arbitrageRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/arbitrage_request_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Contractor о запросе на арбитражную комиссию
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutArbitrageRequest($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на согласительную комиссию по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'arbitrageRequest' => $data['arbitrageRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/arbitrage_request_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для автора запроса о подтверждении арбитражной комиссии
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailAuthorRequestAboutArbitrageConfirmed($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на согласительную комиссию по сделке номер '.$dealOutput['safe_number'].' подтвержден';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'arbitrageRequest' => $data['arbitrageRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/arbitrage_confirm_for_author_request');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Customer об отказе в предоставлении арбитражной комиссии
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendCustomerRequestAboutArbitrageRefused($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на согласительную комиссию по сделке номер '.$dealOutput['safe_number'].' отклонен';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'arbitrageRequest' => $data['arbitrageRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/arbitrage_refuse_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Contractor об отказе в предоставлении арбитражной комиссии
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendContractorRequestAboutArbitrageRefused($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на согласительную комиссию по сделке номер '.$dealOutput['safe_number'].' отклонен';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'arbitrageRequest' => $data['arbitrageRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CUSTOMER'
        ]);
        $twigRenderer->setTemplate('twig/arbitrage_refuse_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для автора реквеста о запросе на третейский суд
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutTribunalRequest($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на третейский суд по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'tribunalRequest' => $data['tribunalRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/tribunal_request_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для контрагента о запросе на третейский суд
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutTribunalRequest($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на третейский суд по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'tribunalRequest' => $data['tribunalRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/tribunal_request_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для автора запроса о подтверждении запроса на третейский суд
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailAuthorRequestAboutTribunalConfirmed($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на третейский суд по сделке номер '.$dealOutput['safe_number'].' подтвержден';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'tribunalRequest' => $data['tribunalRequestOutput'],
            'agent_user_login' => $data['agent_user_login'],
            'counteragent_login' => $data['counteragent_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/tribunal_confirm_for_author_request');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для автора запроса об отказе в предоставлении третейский суд
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailAuthorRequestAboutTribunalRefused($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на третейский суд по сделке номер '.$dealOutput['safe_number'].' отклонен';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'tribunalRequest' => $data['tribunalRequestOutput'],
            'agent_user_login' => $data['agent_user_login'],
            'counteragent_login' => $data['counteragent_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => 'CONTRACTOR'
        ]);
        $twigRenderer->setTemplate('twig/tribunal_refuse_for_author_request');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Customer о запросе на возврат
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutRefundRequest($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на ' . $this->getRefundName($dealOutput['type_ident']) . ' по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'refundRequest' => $data['refundRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/refund_request_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для Contractor о запросе на возврат
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutRefundRequest($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на ' . $this->getRefundName($dealOutput['type_ident']) . ' по сделке номер '.$dealOutput['safe_number'];

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'refundRequest' => $data['refundRequestOutput'],
            'customer_login' => $data['customer_login'],
            'contractor_login' => $data['contractor_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/refund_request_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для автора запроса о подтверждении запроса на возврат
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailCustomerAboutRefundConfirmed($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на ' . $this->getRefundName($dealOutput['type_ident']) . ' по сделке номер '.$dealOutput['safe_number'].' подтвержден';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'refundRequest' => $data['refundRequestOutput'],
            'contractor_login' => $data['contractor_login'],
            'customer_login' => $data['customer_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/refund_request_accepted_for_customer');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для автора запроса о подтверждении запроса на возврат
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailContractorAboutRefundConfirmed($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на ' . $this->getRefundName($dealOutput['type_ident']) . ' по сделке номер '.$dealOutput['safe_number'].' подтвержден';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'refundRequest' => $data['refundRequestOutput'],
            'contractor_login' => $data['contractor_login'],
            'customer_login' => $data['customer_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/refund_request_accepted_for_contractor');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * уведомление для автора запроса об отказе в предоставлении возврата
     *
     * @param $email_value
     * @param $data
     * @throws \Exception
     */
    private function sendMailAuthorRequestAboutRefundRefused($email_value, $data)
    {
        $main_project_url = $this->config['main_project_url'];

        /** @var Deal $deal */
        $deal = $data['deal'];
        $dealOutput = $this->getDealOutputForEmail($deal, $this->config);
        $dealOutput['safe_number'] = $this->getSafeNumber($dealOutput['number']);
        $subject = self::SUBJECT_TITLE.'Запрос на ' . $this->getRefundName($dealOutput['type_ident']) . ' по сделке номер '.$dealOutput['safe_number'].' отклонен';

        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'deal' => $dealOutput,
            'dispute' => $data['disputeOutput'],
            'refundRequest' => $data['refundRequestOutput'],
            'contractor_login' => $data['contractor_login'],
            'customer_login' => $data['customer_login'],
            'main_project_url'  => $main_project_url,
            'counter_deal_agent_role' => $data['counter_deal_agent_role']
        ]);
        $twigRenderer->setTemplate('twig/refund_refuse_for_author_request');
        $html = $twigRenderer->getHtml();
        // Send
        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function validateData($data)
    {
        if(!array_key_exists(self::TYPE_NOTIFY_KEY, $data) || empty($data[self::TYPE_NOTIFY_KEY])) {

            throw new \Exception(self::INVALID_DATA);
        }
        return true;
    }

    /**
     * @param string $deal_type_ident
     * @return string
     */
    private function getRefundName(string $deal_type_ident)
    {
        return $deal_type_ident === 'U' ? 'отказ от услуги' : 'возврат';
    }
}