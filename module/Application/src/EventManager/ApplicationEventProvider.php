<?php
namespace Application\EventManager;

use Core\EventManager\AbstractEventProvider;

/**
 * Class DeliveryDpdCatalogEventProvider
 * @package ModuleDeliveryDpd\EventManager
 */
class ApplicationEventProvider extends AbstractEventProvider
{
    const EVENT_DEVELOPER_TOOL_LISTENERS_DISABLE = 'EVENT_DEVELOPER_TOOL_LISTENERS_DISABLE';
}