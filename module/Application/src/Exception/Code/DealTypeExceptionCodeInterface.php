<?php
namespace Application\Exception\Code;

interface DealTypeExceptionCodeInterface
{
    /**
     * префикс DEAL_TYPE, для сообшений суфикс _MESSAGE
     */
    const DEAL_TYPE_NOT_FOUND = 'DEAL_TYPE_NOT_FOUND';
    const DEAL_TYPE_NOT_FOUND_MESSAGE = 'Deal type not found';
}
