<?php
namespace Application\Exception\Code;

interface DealTokenExceptionCodeInterface
{
    /**
     * префикс DEAL_TOKEN, для сообшений суфикс _MESSAGE
     */
    const DEAL_TOKEN_CREATION_FAILED = 'DEAL_TOKEN_CREATION_FAILED';
    const DEAL_TOKEN_CREATION_FAILED_MESSAGE = 'Partner button creation failed.';

    const DEAL_TOKEN_UPDATE_FAILED = 'DEAL_TOKEN_UPDATE_FAILED';
    const DEAL_TOKEN_UPDATE_FAILED_MESSAGE = 'Partner button update failed.';

    const DEAL_TOKEN_GENERATION_UNIQUE_KEY_FAILED = 'DEAL_TOKEN_GENERATION_UNIQUE_KEY_FAILED';
    const DEAL_TOKEN_GENERATION_UNIQUE_KEY_FAILED_MESSAGE = 'Unique key for deal token generation failed.';

    const DEAL_TOKEN_CREATION_SUCH_SLUG_ALREADY_USED = 'DEAL_TOKEN_CREATION_SUCH_SLUG_ALREADY_USED';
    const DEAL_TOKEN_CREATION_SUCH_SLUG_ALREADY_USED_MESSAGE = 'Partner button with such slug already exists.';

    const DEAL_TOKEN_BUTTON_AUTHOR_ROLE_NOT_DEFINED = 'DEAL_TOKEN_BUTTON_AUTHOR_ROLE_NOT_DEFINED';
    const DEAL_TOKEN_BUTTON_AUTHOR_ROLE_NOT_DEFINED_MESSAGE = 'Button author role not defined.';
}
