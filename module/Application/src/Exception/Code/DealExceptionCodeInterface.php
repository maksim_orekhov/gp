<?php
namespace Application\Exception\Code;

interface DealExceptionCodeInterface
{
    /**
     * префикс DEAL, для сообшений суфикс _MESSAGE
     */
    const DEAL_CREATE_BAD_DATA_PROVIDED = 'DEAL_CREATE_BAD_DATA_PROVIDED';
    const DEAL_CREATE_BAD_DATA_PROVIDED_MESSAGE = 'An error occurred while processing the provided data';

    const DEAL_EDITING_IS_IMPOSSIBLE = 'DEAL_EDITING_IS_IMPOSSIBLE';
    const DEAL_EDITING_IS_IMPOSSIBLE_MESSAGE = 'The deal is approved by both parties, editing is impossible.';

    const DEAL_BALANCE_DOES_NOT_ALLOW_PAYOFF = 'DEAL_BALANCE_DOES_NOT_ALLOW_PAYOFF';
    const DEAL_BALANCE_DOES_NOT_ALLOW_PAYOFF_MESSAGE = 'The balance of the deal does not allow to payoff.';

    const DEAL_WITH_SUCH_ID_NOT_FOUND = 'DEAL_WITH_SUCH_ID_NOT_FOUND';
    const DEAL_WITH_SUCH_ID_NOT_FOUND_MESSAGE = 'Сделка с такими id не найдена.';

    const DEAL_AGENTS_DOES_NOT_MATCH_EMAIL_PROVIDED = 'DEAL_AGENTS_DOES_NOT_MATCH_EMAIL_PROVIDED';
    const DEAL_AGENTS_DOES_NOT_MATCH_EMAIL_PROVIDED_MESSAGE = 'Deal Agents does not match email provided.';

    const DEAL_AGENT_DOES_NOT_MATCH_TO_DEAL = 'DEAL_AGENT_DOES_NOT_MATCH_TO_DEAL';
    const DEAL_AGENT_DOES_NOT_MATCH_TO_DEAL_MESSAGE = 'The deal agent does not match to deal.';

    const DEAL_INVITATION_NOT_SENT = 'DEAL_INVITATION_NOT_SENT';
    const DEAL_INVITATION_NOT_SENT_MESSAGE = 'Deal invitation not sent.';

    const DEAL_TOKEN_DATA_IS_INVALID = 'DEAL_TOKEN_DATA_IS_INVALID';
    const DEAL_TOKEN_DATA_IS_INVALID_MESSAGE = 'Data provided by token is invalid.';

    const DEAL_TOKEN_POST_DATA_IS_INVALID = 'DEAL_TOKEN_POST_DATA_IS_INVALID';
    const DEAL_TOKEN_POST_DATA_IS_INVALID_MESSAGE = 'Form data is incorrect.';

    const DEAL_TOKEN_NOT_PROVIDED = 'DEAL_TOKEN_NOT_PROVIDED';
    const DEAL_TOKEN_NOT_PROVIDED_MESSAGE = 'Deal token not provided.';

    const DEAL_TOKEN_NOT_FOUND = 'DEAL_TOKEN_NOT_FOUND';
    const DEAL_TOKEN_NOT_FOUND_MESSAGE = 'Deal token not found.';

    const DEAL_TOKEN_IS_NOT_ACTIVE = 'DEAL_TOKEN_IS_NOT_ACTIVE';
    const DEAL_TOKEN_IS_NOT_ACTIVE_MESSAGE = 'Deal token is not active.';

    const DEAL_UNREGISTERED_POST_DATA_IS_INVALID = 'DEAL_UNREGISTERED_POST_DATA_IS_INVALID';
    const DEAL_UNREGISTERED_POST_DATA_IS_INVALID_MESSAGE = 'Form data is incorrect.';

    const DEAL_OBJECT_NOT_PROVIDED = 'DEAL_OBJECT_NOT_PROVIDED';
    const DEAL_OBJECT_NOT_PROVIDED_MESSAGE = 'The object of the deal is not provided.';

    const DEAL_INVITATION_SENDING_FAIL = 'DEAL_INVITATION_SENDING_FAIL';
    const DEAL_INVITATION_SENDING_FAIL_MESSAGE = 'Deal invitation sending fail.';

    const DEAL_OR_DELIVERY_DATA_NOT_PROVIDED = 'DEAL_OR_DELIVERY_DATA_NOT_PROVIDED';
    const DEAL_OR_DELIVERY_DATA_NOT_PROVIDED_MESSAGE = 'Deal or delivery data not provided.';

    const DEAL_TOKEN_SWITCH_ACTIVE_FORBIDDEN_TO_CURRENT_USER = 'DEAL_TOKEN_SWITCH_ACTIVE_FORBIDDEN_TO_CURRENT_USER';
    const DEAL_TOKEN_SWITCH_ACTIVE_FORBIDDEN_TO_CURRENT_USER_MESSAGE = 'Deal token switch active forbidden to current user.';

    const DEAL_TOKEN_DELETE_FORBIDDEN_TO_CURRENT_USER = 'DEAL_TOKEN_DELETE_FORBIDDEN_TO_CURRENT_USER';
    const DEAL_TOKEN_DELETE_FORBIDDEN_TO_CURRENT_USER_MESSAGE = 'Deal token delete forbidden to current user.';

    const DEAL_TOKEN_AGENT_EMAIL_MATCH_COUNTERAGENT_EMAIL = 'DEAL_TOKEN_AGENT_EMAIL_MATCH_COUNTERAGENT_EMAIL';
    const DEAL_TOKEN_AGENT_EMAIL_MATCH_COUNTERAGENT_EMAIL_MESSAGE = 'Запрещено создавать сделку если email агента совпадает с email контрагента.';
}
