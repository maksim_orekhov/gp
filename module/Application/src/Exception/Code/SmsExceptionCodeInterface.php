<?php
namespace Application\Exception\Code;

interface SmsExceptionCodeInterface
{
    /**
     * префикс SMS, для сообшений суфикс _MESSAGE
     */
    const SMS_FORBIDDEN_BY_POLICY_NOTIFICATION = 'SMS_FORBIDDEN_BY_POLICY_NOTIFICATION';
    const SMS_FORBIDDEN_BY_POLICY_NOTIFICATION_MESSAGE = 'Sms forbidden by policy notification.';

    const SMS_PROVIDER_ERROR = 'SMS_PROVIDER_ERROR';
    const SMS_PROVIDER_ERROR_MESSAGE = 'Sms did not send, provider returned error.';
}
