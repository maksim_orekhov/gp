<?php
namespace Application\Exception\Code;

interface NdsTypeExceptionCodeInterface
{
    /**
     * префикс NDS_TYPE, для сообшений суфикс _MESSAGE
     */
    const NDS_TYPE_NOT_SPECIFIED = 'NDS_TYPE_NOT_SPECIFIED';
    const NDS_TYPE_NOT_SPECIFIED_MESSAGE = 'Nds type does not specified';

    const NDS_TYPE_NOT_FOUND = 'NDS_TYPE_NOT_FOUND';
    const NDS_TYPE_NOT_FOUND_MESSAGE = 'Nds type not found';
}
