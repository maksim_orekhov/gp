<?php
namespace Application\Exception\Code;

interface PaymentMethodExceptionCodeInterface
{
    /**
     * префикс PAYMENT_METHOD, для сообшений суфикс _MESSAGE
     */
    const PAYMENT_METHOD_NOT_DEFINED = 'PAYMENT_METHOD_NOT_DEFINED';
    const PAYMENT_METHOD_NOT_DEFINED_MESSAGE = 'Payment method not defined';
    const PAYMENT_METHOD_FORBIDDEN_TO_USE_PATTERN = 'PAYMENT_METHOD_FORBIDDEN_TO_USE_PATTERN';
    const PAYMENT_METHOD_FORBIDDEN_TO_USE_PATTERN_MESSAGE = 'Forbidden to use pattern payment method';
}
