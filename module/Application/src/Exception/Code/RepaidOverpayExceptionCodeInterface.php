<?php
namespace Application\Exception\Code;

interface RepaidOverpayExceptionCodeInterface
{
    /**
     * префикс REPAID_OVERPAY, для сообшений суфикс _MESSAGE
     */
    const REPAID_OVERPAY_CREATE_IS_NOT_ALLOWED_STATUS_DEAL = 'REPAID_OVERPAY_CREATE_IS_NOT_ALLOWED_STATUS_DEAL';
    const REPAID_OVERPAY_CREATE_IS_NOT_ALLOWED_STATUS_DEAL_MESSAGE = 'Deal status is not allowed create repaid overpay.';

    const REPAID_OVERPAY_NON_PERMISSIBLE_AMOUNT = 'REPAID_OVERPAY_NON_PERMISSIBLE_AMOUNT';
    const REPAID_OVERPAY_NON_PERMISSIBLE_AMOUNT_MESSAGE = 'Non-permissible amount repaid overpay.';
}
