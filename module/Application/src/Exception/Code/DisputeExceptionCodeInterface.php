<?php
namespace Application\Exception\Code;

interface DisputeExceptionCodeInterface
{
    /**
     * префикс DISPUTE, для сообшений суфикс _MESSAGE
     */
    const DISPUTE_HISTORY_ROLE_OF_USER_FAILED = 'DISPUTE_HISTORY_ROLE_OF_USER_FAILED';
    const DISPUTE_HISTORY_ROLE_OF_USER_FAILED_MESSAGE = 'Identification failed user role in the processing history of the dispute';
    const DISPUTE_ACTIVE_CYCLE_NOT_FOUND = 'DISPUTE_ACTIVE_CYCLE_NOT_FOUND';
    const DISPUTE_ACTIVE_CYCLE_NOT_FOUND_MESSAGE = 'Active cycle of the dispute is not found';
    const DISPUTE_PREVIOUS_CYCLE_INCORRECT = 'DISPUTE_PREVIOUS_CYCLE_INCORRECT';
    const DISPUTE_PREVIOUS_CYCLE_INCORRECT_MESSAGE = 'The previous cycle of dispute incorrect';
}
