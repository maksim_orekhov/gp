<?php
namespace Application\Exception\Code;

interface UserExceptionCodeInterface
{
    /**
     * префикс USER, для сообшений суфикс _MESSAGE
     */
    const USER_BY_LOGIN_NOT_FOUND = 'USER_BY_LOGIN_NOT_FOUND';
    const USER_BY_LOGIN_NOT_FOUND_MESSAGE = 'User with such login not found';

    const USER_INVITATION_TOKEN_NOT_VALID = 'USER_INVITATION_TOKEN_NOT_VALID';
    const USER_INVITATION_TOKEN_NOT_VALID_MESSAGE = 'User registration token by invitation is not valid';

    const USER_IS_NOT_PARTY_TO_DEAL = 'USER_IS_NOT_PARTY_TO_DEAL';
    const USER_IS_NOT_PARTY_TO_DEAL_MESSAGE = 'The user is not a party to the deal.';

}
