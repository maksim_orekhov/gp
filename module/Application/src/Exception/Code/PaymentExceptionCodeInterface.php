<?php
namespace Application\Exception\Code;

interface PaymentExceptionCodeInterface
{
    /**
     * префикс PAYMENT, для сообшений суфикс _MESSAGE
     */
    const PAYMENT_BY_BANK_TRANSFER_CAN_NOT_BE_MADE = 'PAYMENT_BANK_TRANSFER_CAN_NOT_BE_MADE)';
    const PAYMENT_BANK_TRANSFER_CAN_NOT_BE_MADE_MESSAGE = 'Payment by bank transfer can not be made.';

    const PAYMENT_METHOD_OF_THIS_TYPE_CANT_AVAILABLE = 'PAYMENT_METHOD_OF_THIS_TYPE_CANT_AVAILABLE';
    const PAYMENT_METHOD_OF_THIS_TYPE_CANT_AVAILABLE_MESSAGE = 'Payment method of this type cant availabled';

    const PAYMENT_BY_ACQUIRING_MANDARIN_CAN_NOT_BE_MADE = 'PAYMENT_BY_ACQUIRING_MANDARIN_CAN_NOT_BE_MADE';
    const PAYMENT_BY_ACQUIRING_MANDARIN_CAN_NOT_BE_MADE_MESSAGE = 'Payment by acquiring Mandarin can not be made';

    const PAYMENT_METHOD_TYPE_NOT_FOUND = 'PAYMENT_METHOD_TYPE_NOT_FOUND';
    const PAYMENT_METHOD_TYPE_NOT_FOUNDD_MESSAGE = 'Payment method type not found';
}
