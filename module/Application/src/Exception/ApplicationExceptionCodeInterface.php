<?php
namespace Application\Exception;

use Application\Exception\Code\BankClientExceptionCodeInterface;
use Application\Exception\Code\CivilLawSubjectExceptionCodeInterface;
use Application\Exception\Code\DealExceptionCodeInterface;
use Application\Exception\Code\DisputeExceptionCodeInterface;
use Application\Exception\Code\PaymentExceptionCodeInterface;
use Application\Exception\Code\PaymentMethodExceptionCodeInterface;
use Application\Exception\Code\RepaidOverpayExceptionCodeInterface;
use Application\Exception\Code\UserExceptionCodeInterface;
use Application\Exception\Code\DealTypeExceptionCodeInterface;
use Application\Exception\Code\NdsTypeExceptionCodeInterface;
use Application\Exception\Code\SmsExceptionCodeInterface;
use Application\Exception\Code\DealTokenExceptionCodeInterface;

/**
 * Interface BaseExceptionCodeInterface
 * @package Core\Exception
 */
interface ApplicationExceptionCodeInterface extends
    UserExceptionCodeInterface,
    PaymentMethodExceptionCodeInterface,
    BankClientExceptionCodeInterface,
    DisputeExceptionCodeInterface,
    RepaidOverpayExceptionCodeInterface,
    CivilLawSubjectExceptionCodeInterface,
    DealExceptionCodeInterface,
    PaymentExceptionCodeInterface,
    DealTypeExceptionCodeInterface,
    NdsTypeExceptionCodeInterface,
    SmsExceptionCodeInterface,
    DealTokenExceptionCodeInterface
{
    /**
     * Только для общих кодов, которые могут быть использованы везде!!!
     *
     * без префикса, для сообшений суфикс _MESSAGE
     */
    const INCORRECT_DATA_PROVIDED_TO_LISTENER = 'INCORRECT_DATA_PROVIDED_TO_LISTENER';
    const INCORRECT_DATA_PROVIDED_TO_LISTENER_MESSAGE = 'Incorrect data provided to listener';
}