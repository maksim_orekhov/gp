<?php
namespace Application\Form;

use Application\Controller\DealController;
use Application\Entity\CivilLawSubject;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Entity\PaymentMethod;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Validator\InArray;
use Zend\Form\Element;
use Application\Service\Deal\DealManager;
use Application\Entity\User;
use ModulePaymentOrder\Entity\PaymentMethodType;

class DealForm extends Form
{
    /**
     * Doctrine entity manager.
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var string
     */
    private $deal_role;

    /**
     * @var bool
     */
    private $agent_email;

    /**
     * @var User
     */
    private $user;

    /**
     * @var null|string
     */
    private $minDeliveryPeriod;

    /**
     * DealForm constructor.
     * @param DealManager $dealManager
     * @param User|null $user
     * @param string|null $deal_role
     * @param bool $agent_email
     * @throws \Exception
     */
    public function __construct(DealManager $dealManager, $user, string $deal_role = null, bool $agent_email = false)
    {
        $this->dealManager  = $dealManager;
        $this->user         = $user;
        $this->deal_role    = $deal_role;
        $this->agent_email  = $agent_email;
        $this->minDeliveryPeriod = $dealManager->getMinDeliveryPeriodFromPolitic();

        // Define form name
        parent::__construct('deal-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'deal-form');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return array
     */
    public function getOptionsForDealTypeSelect(): array
    {
        $dealTypes = $this->dealManager->getDealTypes();
        $deal_types_array = [];
        /** @var DealType $dealType */
        foreach ($dealTypes as $dealType) {
            $deal_types_array[$dealType->getId()] = $dealType->getName();
        }

        return $deal_types_array;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getOptionsForCivilLawSubjectsSelect()
    {
        return $this->dealManager->getCivilLawSubjectsAsArray();
    }

    /**
     * @return array
     */
    public function getOptionsForPaymentMethodSelect()
    {
        $payment_methods_array = [];

        /** @var CivilLawSubject $civilLawSubject */
        foreach ($this->user->getCivilLawSubjects() as $civilLawSubject) {
            /** @var PaymentMethod $paymentMethod */
            foreach ($civilLawSubject->getPaymentMethods() as $paymentMethod) {
                // Чтобы методы не повторялись, проверяем на наличие ключа в итоговом массиве
                if( !array_key_exists($paymentMethod->getId(), $payment_methods_array ) && $paymentMethod->getIsPattern() ) {
                    if($paymentMethod->getPaymentMethodType()->getName() === PaymentMethodType::BANK_TRANSFER) {
                        $payment_methods_array[$paymentMethod->getId()] = $paymentMethod->getPaymentMethodBankTransfer()->getName();
                    }
                }
            }
        }

        return $payment_methods_array;
    }

    /**
     * @return array
     */
    public function getOptionsForFeePayerOptionSelect(): array
    {
        $feePayerOptions = $this->dealManager->getFeePayerOptions();

        $fee_payer_options_array = [];
        /** @var FeePayerOption $feePayerOption */
        foreach ($feePayerOptions as $feePayerOption) {
            $fee_payer_options_array[$feePayerOption->getId()]
                = DealManager::FEE_PAYER_OPTION_TRANSLATE[$feePayerOption->getName()];
        }

        return $fee_payer_options_array;
    }

    /**
     * @throws \Exception
     */
    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'placeholder'=>'Название товара/услуги'
            ],
            'options' => [
                'label' => 'Название товара/услуги',
            ],
        ]);

        $this->add([
            'type'  => Element\Number::class,
            'name' => 'amount',
            'attributes' => [
                'id' => 'amount',
                'placeholder'=>'Сумма сделки'
            ],
            'options' => [
                'label' => 'Сумма сделки',
            ],
        ]);

        $this->add([
            'type' => Element\Radio::class,
            'name' => 'deal_role',
            'attributes' => [
                'id' => 'deal_role',
                'placeholder'=>'Ваша роль в сделке:'
            ],
            'options' => [
                'label' => 'Ваша роль в сделке:',
                'value_options' => [
                    'customer' => 'покупатель',
                    'contractor' => 'продавец',
                ],
            ],
        ]);

        $deal_civil_law_subject_options = [
            'label' => 'Вы будете участвовать в сделке как:',
            'empty_option' => 'Ранее зарегистрированные',
            'value_options' => []
        ];
        if (null !== $this->user) {
            $deal_civil_law_subject_options['value_options'] = $this->getOptionsForCivilLawSubjectsSelect();
        }

        $this->add([
            'name' => 'deal_civil_law_subject',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => [
                'id' => 'deal_civil_law_subject',
                'placeholder' => 'Вы будете участвовать в сделке как:'
            ],
            'options' => $deal_civil_law_subject_options
        ]);

        // If deal_role == 'contractor' show select type element. Else - hidden with no value.
        if($this->deal_role === 'contractor') {
            $options = $this->getOptionsForPaymentMethodSelect();
            $empty_option_message = (empty($options)) ? 'Вы должны добавить хотя бы один метод оплаты' : 'Выберите метод оплаты';
            $this->add([
                'name' => 'payment_method',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => [
                    'id' => 'payment_method',
                    'placeholder' => 'Метод оплаты'
                ],
                'options' => array(
                    'label' => 'Метод оплаты',
                    'empty_option' => $empty_option_message,
                    'value_options' => $options
                )
            ]);
        } elseif (false === $this->agent_email) {
            $this->add([
                'name' => 'payment_method',
                'type' => 'Zend\Form\Element\Hidden',
                'attributes' => array(
                    'value' => ''
                )
            ]);
        }

        $this->add([
            'type'  => Element\File::class,
            'name' => 'files',
            'options' => [
                'label' => 'Файл',
            ],
            'attributes' => [
                'id' => 'file',
                'class'=>'form-control',
                'multiple'=> true
            ],
        ]);

        $this->add([
            'type'  => 'Zend\Form\Element\Email',
            'name' => 'counteragent_email',
            'attributes' => [
                'id' => 'counteragent_email',
                'placeholder'=>'Email контрагента'
            ],
            'options' => [
                'label' => 'Email контрагента',
            ],
        ]);

        $this->add([
            'name' => 'deal_type',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => [
                'id' => 'deal_type',
                'placeholder'=>'Тип сделки'
            ],
            'options' => array(
                'label' => 'Тип сделки',
                'empty_option' => 'Выберите тип сделки',
                'value_options' => $this->getOptionsForDealTypeSelect()
            )
        ]);

        $this->add([
            'name' => 'fee_payer_option',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => [
                'id' => 'fee_payer_option',
                'placeholder'=>'Вариант оплаты комиссии'
            ],
            'options' => array(
                'empty_option' => 'Выберите вариант оплаты комиссии',
                'label' => 'Вариант оплаты комиссии',
                'value_options' => $this->getOptionsForFeePayerOptionSelect()
            )
        ]);

        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'addon_terms',
            'attributes' => [
                'id' => 'addon_terms',
                'class'=>'form-control',
                'placeholder'=>'Описание сделки'
            ],
            'options' => [
                'label' => 'Описание сделки',
            ],
        ]);

        $this->add([
            'type'  => Element\Number::class,
            'name' => 'delivery_period',
            'attributes' => [
                'id' => 'delivery_period',
                'class'=>'form-control',
                'placeholder'=>'Срок доставки/исполнения и проверки (минимум 1 сутки)'
            ],
            'options' => [
                'label' => 'Срок доставки/исполнения и проверки (минимум 1 сутки)',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }


    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => ['min' => 2, 'max' => 255]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'amount',
            'required' => true,
            'validators' => [
                ['name'    => \Zend\Validator\LessThan::class,
                    'options' => [
                        'max' => DealController::MAX_DEAL_AMOUNT, // Миллиард
                        'inclusive' => true]
                ],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[1-9][0-9]*/',
                        'message' => 'Неверный формат ввода']
                ],
            ],
        ]);

        // @TODO Убрать условие?
        if (false === $this->agent_email) {
            $inputFilter->add([
                'name'     => 'deal_civil_law_subject',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Digits'
                    ],
                ],
            ]);
            $inputFilter->add([
                'name'     => 'payment_method',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'Digits'
                    ],
                ],
            ]);
        }

        $inputFilter->add([
            'name'     => 'deal_type',
            'required' => true,
            'validators' => [
                [
                    'name' => 'Digits'
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'fee_payer_option',
            'required' => true,
            'validators' => [
                [
                    'name' => 'Digits'
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'delivery_period',
            'required' => true,
            'validators' => [
                [
                    'name'    => \Application\Form\Validator\DeliveryPeriodValidator::class,
                    'options' => [
                        'min' => $this->minDeliveryPeriod,
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'deal_role',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => ['min' => 8, 'max' => 10]
                ],
            ],
        ]);

        $counteragent_email_validators = [
            [
                'name' => 'EmailAddress'
            ],
        ];

        if (null !== $this->user) {
            $counteragent_email_validators[] = [
                'name'    => \Application\Form\Validator\IdenticalToUserEmailValidator::class,
                'options' => [
                    'email' => $this->user->getEmail()->getEmail(),
                    'message' => 'Введенный email используется в Вашем профиле!'
                ]
            ];
        }

        $inputFilter->add([
            'name'     => 'counteragent_email',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => $counteragent_email_validators,
        ]);

        $inputFilter->add([
            'name'     => 'addon_terms',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 5,
                        'max' => 2000
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'files',
            'required' => false,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Zend\Validator\File\Size::class,
                    'options' => [
                        'max' => '10MB'
                    ],
                ]
            ],
        ]);
    }
}