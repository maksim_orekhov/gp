<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * Class DealConditionsConfirmForm
 * @package Application\Form
 */
class DealConditionsConfirmForm extends Form
{
    /**
     * DealConditionsConfirmForm constructor.
     */
    public function __construct()
    {
        parent::__construct('deal-conditions-confirm-form');

        $this->setAttribute('method', 'post');

        $this->addElements();
    }

    protected function addElements()
    {
        $this->add([
            'type' => 'hidden',
            'name' => 'confirm',
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

    }
}