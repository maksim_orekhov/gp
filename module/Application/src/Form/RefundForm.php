<?php
namespace Application\Form;

use Application\Entity\Dispute;
use Application\Entity\PaymentMethodBankTransfer;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class RefundForm
 * @package Application\Form
 */
class RefundForm extends Form
{
    const TYPE_FORM = 'refund_form';

    /**
     * @var Dispute|null
     */
    private $dispute;

    /**
     * @var PaymentMethodBankTransfer|null
     */
    private $paymentMethodBankTransfer;

    /**
     * ReturnPaymentOrderFileForm constructor.
     * @param PaymentMethodBankTransfer|null $paymentMethodBankTransfer
     * @param Dispute|null $dispute
     */
    public function __construct($paymentMethodBankTransfer = null, $dispute = null)
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        $this->paymentMethodBankTransfer = $paymentMethodBankTransfer;
        $this->dispute = $dispute;

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'dispute-refund-create-form');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $dispute_attributes = [];
        if ($this->dispute && $this->dispute instanceof Dispute){
            $dispute_attributes = [
                'value' => $this->dispute->getId(),
            ];
        }
        $this->add([
            'type' => 'hidden',
            'name' => 'dispute_id',
            'attributes' => $dispute_attributes,
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'bank_transfer_id',
            'attributes' => [
                'value' => ($this->paymentMethodBankTransfer) ? $this->paymentMethodBankTransfer->getId() : '',
            ],
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'type_form',
            'attributes' => [
                'value' => self::TYPE_FORM,
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сформировать возврат',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);
        $validators = [];
        if($this->paymentMethodBankTransfer){
            $validators = [
                [
                    'name'    => 'Regex',
                    'options' => [
                        'pattern' => '/'.$this->paymentMethodBankTransfer->getId().'/',
                        'message' => 'Payment method bank transfer id not found'
                    ]
                ],
            ];
        }

        $inputFilter->add([
            'name'     => 'bank_transfer_id',
            'required' => true,
            'filters'  => [
                ['name' =>  'ToInt'],
            ],
            'validators' => $validators,
        ]);

        $inputFilter->add([
            'name' => 'type_form',
            'required' => false,
            'filters' => [],
            'validators' => []
        ]);

        $inputFilter->add([
            'name'     => 'dispute_id',
            'required' => true,
            'validators' => [
                [
                    'name'    => \Application\Form\Validator\ResolutionDisputeValidator::class,
                    'options' => [
                        'dispute' => $this->dispute,
                    ]
                ],
            ],
        ]);
    }
}