<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class PaymentMethodByCivilLawSubjectForm extends Form
{
    use \Application\Provider\FormFieldsetTrait;

    public function __construct($civil_id = null, array $paymentMethodTypes = null, array $fieldsets = [])
    {
        parent::__construct('payment-method-by-civil-law-subject');

        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
        $this->setCivilLawSubject($civil_id);
        $this->setPaymentMethodOptions($paymentMethodTypes);
        $this->attachFieldsets($fieldsets);
    }
    protected  function setCivilLawSubject($civil_id)
    {
        if ($civil_id === null) {
            return;
        }

        $field = $this->get('civil_law_subject_id');
        $field->setValue($civil_id);
    }

    protected function setPaymentMethodOptions($paymentMethodTypes)
    {
        if ($paymentMethodTypes === null) {
            return;
        }

        $field = $this->get('payment_method_type_id');
        $selectOptions = [];
        $selectOptions[0] = 'Выберите метод оплаты';
        foreach ($paymentMethodTypes as $type) {
            $selectOptions[$type->getId()] = $type->getOutputName();
        }
        $field->setValueOptions($selectOptions);
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'hidden',
            'name' => 'method_extend',
            'attributes' => ['value' => false]
        ]);

        $this->add([
            'type'  => 'hidden',
            'name' => 'civil_law_subject_id',
        ]);

        $this->add([
            'type'  => 'select',
            'name' => 'payment_method_type_id',
            'attributes' => [
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'Тип метода оплаты',
                'value_options' => [],
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сохранить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'civil_law_subject_id',
            'required' => true,
            'filters'  => [
                ['name' =>  'ToInt'],
            ],
            'validators' => [],
        ]);

        $inputFilter->add([
            'name'     => 'payment_method_type_id',
            'required' => true,
            'filters'  => [
                ['name' =>  'ToInt'],
            ],
            'validators' => [],
        ]);
    }
}