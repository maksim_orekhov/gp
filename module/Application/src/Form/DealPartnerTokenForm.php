<?php
namespace Application\Form;

use Application\Controller\DealController;
use Application\Entity\CivilLawSubject;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Entity\PaymentMethod;
use Application\Entity\User;
use Application\Service\Deal\DealManager;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;
use Application\Form\Validator\DeliveryPeriodValidator;

/**
 * Class DealPartnerTokenForm
 * @package Application\Form
 */
class DealPartnerTokenForm extends Form
{
    const TYPE_FORM = 'deal-partner-token-form';

    /**
     * Doctrine entity manager.
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    private $is_operator;

    /**
     * @var null|string
     */
    private $minDeliveryPeriod;

    /**
     * @var string
     */
    private $deal_role;

    /**
     * DealPartnerTokenForm constructor.
     * @param DealManager $dealManager
     * @param User $user
     * @param string $type
     * @param bool $is_operator
     * @param string|null $deal_role
     * @throws \Exception
     */
    public function __construct(DealManager $dealManager, User $user, string $type, bool $is_operator, string $deal_role = null)
    {
        $this->dealManager = $dealManager;
        $this->user = $user;
        $this->type = $type;
        $this->is_operator = $is_operator;
        $this->minDeliveryPeriod = $dealManager->getMinDeliveryPeriodFromPolitic();
        $this->deal_role = strtolower($deal_role);

        // Define form name
        parent::__construct(self::TYPE_FORM);

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return array
     */
    public function getOptionsForDealTypeSelect(): array
    {
        $dealTypes = $this->dealManager->getDealTypes();
        $deal_types_array = [];
        /** @var DealType $dealType */
        foreach ($dealTypes as $dealType) {
            $deal_types_array[$dealType->getId()] = $dealType->getName();
        }

        return $deal_types_array;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getOptionsForCivilLawSubjectsSelect()
    {
        return $this->dealManager->getCivilLawSubjectsAsArray();
    }

    /**
     * @return array
     */
    public function getOptionsForPaymentMethodSelect()
    {
        $payment_methods_array = [];

        /** @var CivilLawSubject $civilLawSubject */
        foreach ($this->user->getCivilLawSubjects() as $civilLawSubject) {
            /** @var PaymentMethod $paymentMethod */
            foreach ($civilLawSubject->getPaymentMethods() as $paymentMethod) {
                // Чтобы методы не повторялись, проверяем на наличие ключа в итоговом массиве
                if( !array_key_exists($paymentMethod->getId(), $payment_methods_array ) && $paymentMethod->getIsPattern() ) {
                    if($paymentMethod->getPaymentMethodType()->getName() === PaymentMethodType::BANK_TRANSFER) {
                        $payment_methods_array[$paymentMethod->getId()] = $paymentMethod->getPaymentMethodBankTransfer()->getName();
                    }
                }
            }
        }

        return $payment_methods_array;
    }

    /**
     * @return array
     */
    public function getOptionsForFeePayerOptionSelect(): array
    {
        $feePayerOptions = $this->dealManager->getFeePayerOptions();

        $fee_payer_options_array = [];
        /** @var FeePayerOption $feePayerOption */
        foreach ($feePayerOptions as $feePayerOption) {
            $fee_payer_options_array[$feePayerOption->getId()]
                = DealManager::FEE_PAYER_OPTION_TRANSLATE[$feePayerOption->getName()];
        }

        return $fee_payer_options_array;
    }

    /**
     * @return array
     */
    public function getOptionsForCounteragentRoleSelect(): array
    {
        return [
            'customer' => 'Покупатель',
            'contractor' => 'Продавец'
        ];
    }

    /**
     * @throws \Exception
     */
    protected function addElements()
    {
        $this->add([
            'type' => Element\Text::class,
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'placeholder' => 'Название'
            ],
            'options' => [
                'label' => 'Название',
            ],
        ]);

        $this->add([
            'type' => Element\Radio::class,
            'name' => 'button_author_role',
            'attributes' => [
                'id' => 'button_author_role',
                'placeholder'=>'Вы будете размещать кнопку как:'
            ],
            'options' => [
                'label' => 'Вы будете размещать кнопку как:',
                'value_options' => [
                    'referer' => 'Реферер',
                    'participant' => 'Участник сделки',
                ],
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'slug',
            'attributes' => [
                'id' => 'slug',
                'placeholder' => 'Slug'
            ],
            'options' => [
                'label' => 'Slug',
            ],
        ]);

        $this->add([
            'type'  => 'Zend\Form\Element\Email',
            'name' => 'counteragent_email',
            'attributes' => [
                'id' => 'counteragent_email',
                'placeholder'=>'Email контрагента'
            ],
            'options' => [
                'label' => 'Email контрагента',
            ],
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'deal_role',
            'attributes' => [
                'id' => 'deal_role',
                'placeholder' => 'Роль в сделке'
            ],
            'options' => [
                'label' => 'Роль в сделке',
                'value_options' => $this->getOptionsForCounteragentRoleSelect()
            ],
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'fee_payer_option',
            'attributes' => [
                'id' => 'fee_payer_option',
                'placeholder' => 'Кто платит комиссию'
            ],
            'options' => [
                'label' => 'Кто платит комиссию',
                'value_options' => $this->getOptionsForFeePayerOptionSelect()
            ],
        ]);

        if ($this->type === 'edit' && true === $this->is_operator) {
            $this->add([
                'type' => Element\Text::class,
                'name' => 'partner_ident',
                'attributes' => [
                    'id' => 'partner_ident',
                    'placeholder' => 'Идентификатор партнера'
                ],
                'options' => [
                    'label' => 'Идентификатор партнера',
                ],
            ]);
        }

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'is_active',
            'attributes' => [
                'id' => 'is_active',
                'placeholder'=>'Активность'
            ],
            'options' => [
                'label' => 'Активность',
            ],
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'beneficiar_civil_law_subject',
            'attributes' => [
                'id' => 'beneficiar_civil_law_subject',
                'placeholder' => 'Вы будете участвовать в сделке как:'
            ],
            'options' => [
                'label' => 'Вы будете участвовать в сделке как:',
                'empty_option' => 'Ранее зарегистрированные',
                'value_options' => $this->getOptionsForCivilLawSubjectsSelect()
            ],
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'counteragent_civil_law_subject',
            'attributes' => [
                'id' => 'counteragent_civil_law_subject',
                'placeholder' => 'Вы будете участвовать в сделке как:'
            ],
            'options' => [
                'label' => 'Вы будете участвовать в сделке как:',
                'empty_option' => 'Ранее зарегистрированные',
                'value_options' => $this->getOptionsForCivilLawSubjectsSelect()
            ],
        ]);

        $options = $this->getOptionsForPaymentMethodSelect();
        $empty_option_message = empty($options) ? 'Вы должны добавить хотя бы один метод оплаты' : 'Выберите метод оплаты';
        $this->add([
            'type' => Element\Select::class,
            'name' => 'counteragent_payment_method',
            'attributes' => [
                'id' => 'counteragent_payment_method',
                'placeholder' => 'Метод оплаты'
            ],
            'options' => array(
                'label' => 'Метод оплаты',
                'empty_option' => $empty_option_message,
                'value_options' => $options
            )
        ]);
        $this->add([
            'type' => Element\Select::class,
            'name' => 'beneficiar_payment_method',
            'attributes' => [
                'id' => 'beneficiar_payment_method',
                'placeholder' => 'Выберите способ получения комиссионных'
            ],
            'options' => array(
                'label' => 'Способ получения комиссионных:',
                'empty_option' => $empty_option_message,
                'value_options' => $options
            )
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'deal_type',
            'attributes' => [
                'id' => 'deal_type',
                'placeholder'=>'Тип сделки'
            ],
            'options' => array(
                'label' => 'Тип сделки',
                'empty_option' => 'Выберите тип сделки',
                'value_options' => $this->getOptionsForDealTypeSelect()
            )
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'deal_name',
            'attributes' => [
                'id' => 'deal_name',
                'placeholder' => 'Название сделок'
            ],
            'options' => [
                'label' => 'Название сделок',
            ],
        ]);

        $this->add([
            'type' => Element\Textarea::class,
            'name' => 'deal_description',
            'attributes' => [
                'id' => 'deal_description',
                'placeholder' => 'Описание сделки'
            ],
            'options' => [
                'label' => 'Описание сделки',
            ],
        ]);

        $this->add([
            'type'  => Element\Number::class,
            'name' => 'amount',
            'attributes' => [
                'id' => 'amount',
                'placeholder'=>'Сумма сделки'
            ],
            'options' => [
                'label' => 'Сумма сделки',
            ],
        ]);

        $this->add([
            'type'  => Element\Number::class,
            'name' => 'delivery_period',
            'attributes' => [
                'id' => 'delivery_period',
                'class'=>'form-control',
                'placeholder'=>'Срок доставки/исполнения и проверки (минимум 1 сутки)'
            ],
            'options' => [
                'label' => 'Срок доставки/исполнения и проверки (минимум 1 сутки)',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Расчитать стоимость',
                'class'=>'ButtonApply'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 100
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'slug',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 60
                    ],
                ],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[a-zA-Z][a-zA-Z0-9_-]*/',
                        'message' => 'Неверный формат ввода']
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'deal_role',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 8,
                        'max' => 10
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'fee_payer_option',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name' => 'Digits'
                ],
            ],
        ]);

        if ($this->type === 'edit' && true === $this->is_operator) {
            $inputFilter->add([
                'name'     => 'partner_ident',
                'required' => false,
                'filters'  => [
                    ['name' => \Zend\Filter\StringTrim::class],
                    ['name' => \Zend\Filter\StripTags::class],
                ],
                'validators' => [
                    [
                        'name'    => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 5,
                            'max' => 60
                        ],
                    ],
                ],
            ]);
        }

        $inputFilter->add([
            'name'     => 'beneficiar_civil_law_subject',
            'required' => false,
            'validators' => [
                [
                    'name' => 'Digits'
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'beneficiar_payment_method',
            'required' => false,
            'validators' => [
                [
                    'name' => 'Digits'
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'counteragent_civil_law_subject',
            'required' => false,
            'validators' => [
                [
                    'name' => 'Digits'
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'counteragent_payment_method',
            'required' => false,
            'allow_empty' => $this->deal_role === 'contractor',
            'validators' => [
                [
                    'name' => 'Digits'
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'is_active',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\InArray::class,
                    'options' => [
                        'haystack' => [0,1],
                        'messages' => [
                            'notInArray' => 'Недопустимое значение!',
                        ],
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'deal_type',
            'required' => false,
            'validators' => [
                [
                    'name' => 'Digits'
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'deal_name',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 5,
                        'max' => 100
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'deal_description',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 5,
                        'max' => 2000
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'amount',
            'required' => false,
            'allow_empty' => true,
            'validators' => [
                [
                    'name' => 'Digits'
                ],
                ['name'    => \Zend\Validator\LessThan::class,
                    'options' => [
                        'max' => DealController::MAX_DEAL_AMOUNT, // Миллиард
                        'inclusive' => true]
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'delivery_period',
            'required' => false,
            'validators' => [
                [
                    'name'    => DeliveryPeriodValidator::class,
                    'options' => [
                        'min' => $this->minDeliveryPeriod,
                    ]
                ],
            ],
        ]);

        if (null !== $this->user) {
            $counteragent_email_validators[] = [
                'name'    => \Application\Form\Validator\IdenticalToUserEmailValidator::class,
                'options' => [
                    'email' => $this->user->getEmail()->getEmail(),
                    'message' => 'Введенный email используется в Вашем профиле!'
                ]
            ];
        }

        $inputFilter->add([
            'name'     => 'counteragent_email',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => $counteragent_email_validators,
        ]);
    }
}