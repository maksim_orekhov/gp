<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class DealCommentForm
 * @package Application\Form
 */
class DealCommentForm extends Form
{
    /**
     * @var string
     */
    private $deal_id;

    /**
     * DealForm constructor.
     * @param int $deal_id
     */
    public function __construct($deal_id = null)
    {
        $this->deal_id = $deal_id;

        // Define form name
        parent::__construct('deal-comment-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'deal-comment-form');
        $this->addElements();
        $this->addInputFilter();

        if($this->deal_id) {
            // Set 'deal_id' field value
            $dealIdField = $this->get('deal_id');
            $dealIdField->setValue($this->deal_id);
        }
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'text',
            'attributes' => [
                'id' => 'text',
                'placeholder'=>'Комментарий'
            ],
            'options' => [
                'label' => 'Комментарий',
            ],
        ]);

        $this->add([
            'type'  => 'hidden',
            'name' => 'deal_id',
            'attributes' => [
                'id' => 'deal-id',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'text',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 10,
                        'max' => 20000,
                        'message' => 'Текст сообщения слишком длинный или слишком короткий'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'deal_id',
            'required' => true,
            'filters'  => [
                ['name'    =>  'StringTrim'],
                ['name'    =>  'StripTags'],
                ['name'    =>  'StripNewlines'],
            ],
            'validators' => [
                ['name'    => 'StringLength', 'options' => ['min' => 1, 'max' => 11]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[0-9]$/',
                        'message' => 'Неверный формат ввода'
                    ]
                ],
            ],
        ]);
    }
}