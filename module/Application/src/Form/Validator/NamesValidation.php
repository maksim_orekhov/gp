<?php
namespace Application\Form\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Class NamesValidation
 * @package Application\Form\Validator
 */
class NamesValidation extends AbstractValidator
{
    const REGEXP = "/^[а-яёА-ЯЁ]+(([\'\-\s][а-яёА-ЯЁ])?[а-яёА-ЯЁ]*)*$/u";

    const INVALID_CHARACTERS = 'invalidCharacters';

    protected $messageTemplates = [
        self::INVALID_CHARACTERS  => 'Присутствуют недопустимые символы',
    ];

    /**
     * @param mixed $date_value
     * @return bool
     */
    public function isValid($value)
    {
        if (!$value) {

            return false;
        }

        if (!preg_match(self::REGEXP, $value)) {
            $this->error(self::INVALID_CHARACTERS);

            return false;
        }

        return true;
    }
}