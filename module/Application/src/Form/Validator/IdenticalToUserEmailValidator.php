<?php
namespace Application\Form\Validator;

use Zend\Validator\AbstractValidator;

/**
 * Class IdenticalToUserEmailValidator
 * @package Application\Form\Validator
 */
class IdenticalToUserEmailValidator extends AbstractValidator
{
    const INVALID  = 'email';

    protected $options = [
        'email' => null,
    ];

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::INVALID  => 'Введенный email используется в Вашем профиле!',
    ];

    /**
     * IdenticalToUserEmailValidator constructor.
     * @param array $options
     */
    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param mixed $email
     * @return bool
     */
    public function isValid($email)
    {
        if ($this->options['email'] !== null && strtolower($this->options['email']) === strtolower(trim($email))) {
            $this->error(self::INVALID);

            return false;
        }

        return true;
    }
}