<?php
namespace Application\Form\Validator;

use Application\Entity\Dispute;
use Zend\Validator\AbstractValidator;


class ResolutionDisputeValidator extends AbstractValidator
{
    const INVALID_DISPUTE_ID  = 'dispute_id_invalid';
    const INVALID_DISPUTE  = 'dispute_invalid';
    const INVALID_INSTANCEOF_DISPUTE  = 'instanceof_dispute_invalid';
    const NOT_CORRECT_DISPUTE_ID  = 'not_correct_dispute_id';
    const NOT_CORRECT_STATUS  = 'not_correct_status';

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::INVALID_DISPUTE_ID   => "Dispute ID not valid",
        self::INVALID_DISPUTE   => "Data on the dispute is not provided",
        self::INVALID_INSTANCEOF_DISPUTE   => "Requires an instance of the dispute class",
        self::NOT_CORRECT_DISPUTE_ID   => "Attempt to provide not correct dispute id",
        self::NOT_CORRECT_STATUS   => "The dispute is not in status \"open dispute\"",
    ];

    protected $options = [
        'dispute' => null,
    ];

    public function __construct($options = [])
    {
        if (! is_array($options)) {
            $options     = func_get_args();
            if (! empty($options)) {
                $temp['dispute'] = array_shift($options);
                $options = $temp;
            }
        }

        parent::__construct($options);
    }

    /**
     * @return mixed
     */
    public function getDispute()
    {
        return $this->options['dispute'];
    }

    /**
     * @param $dispute
     * @return $this
     */
    public function setDispute($dispute)
    {
        if (null === $dispute) {
            $this->options['dispute'] = null;
        } else {
            $this->options['dispute'] = $dispute;
        }

        return $this;
    }

    /**
     * @param mixed $dispute_id
     * @return bool
     */
    public function isValid($dispute_id)
    {
        if ((int) $dispute_id <= 0){
            $this->error(self::INVALID_DISPUTE_ID);
            return false;
        }
        /** @var Dispute $dispute */
        $dispute = $this->getDispute();

        if ($dispute === null){
            $this->error(self::INVALID_DISPUTE);
            return false;
        }

        if (!$dispute instanceof Dispute){
            $this->error(self::INVALID_INSTANCEOF_DISPUTE);
            return false;
        }

        if ($dispute->getId() != $dispute_id){
            $this->error(self::NOT_CORRECT_DISPUTE_ID);
            return false;
        }

        if ($dispute->isClosed()){
            $this->error(self::NOT_CORRECT_STATUS);
            return false;
        }

        return true;
    }
}