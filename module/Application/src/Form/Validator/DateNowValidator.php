<?php
namespace Application\Form\Validator;

use Zend\Validator\AbstractValidator;
use DateTime;

/**
 * Class DateNowValidator
 * @package Application\Form\Validator
 */
class DateNowValidator extends AbstractValidator
{
    const INVALID_DATE = 'dateHigherNow';

    protected $messageTemplates = [
        self::INVALID_DATE  => "Value is higher the date now",
    ];

    /**
     * @param mixed $value
     * @return bool
     */
    public function isValid($value)
    {
        if (!strtotime($value)) {
            return false;
        }
        $date = new DateTime(date('Y-m-d H:i:s', strtotime($value)));

        if (new DateTime('now') < $date) {
            $this->error(self::INVALID_DATE);
            return false;
        }

        return true;
    }
}