<?php
namespace Application\Form\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;
use Locale;
use NumberFormatter;
use IntlException;
use Zend\Filter\Digits as DigitsFilter;


class DeliveryPeriodValidator extends AbstractValidator
{
    const INVALID   = 'integerInvalid';
    const NOT_DIGITS   = 'notDigits';
    const STRING_EMPTY = 'digitsStringEmpty';
    const TOO_SMALL = 'integerTooSmall';
    const TOO_LONG  = 'integerTooLarge';

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_DIGITS   => "The input must contain only digits",
        self::STRING_EMPTY => "The input is an empty string",
        self::INVALID   => "Invalid type given. integer expected",
        self::TOO_SMALL => "The minimum delivery period shall not be less than %min% days",
        self::TOO_LONG  => "The maximum delivery period should not be more than %max% days",
    ];

    /**
     * @var array
     */
    protected $messageVariables = [
        'min'    => ['options' => 'min'],
        'max'    => ['options' => 'max'],
    ];

    protected $options = [
        'min'      => 0,       // Minimum value
        'max'      => null,    // Maximum value, null if there is no limitation
    ];

    /**
     * Digits filter used for validation
     *
     * @var \Zend\Filter\Digits
     */
    protected static $filter = null;

    public function __construct($options = [])
    {
        if (! is_array($options)) {
            $options     = func_get_args();
            $temp['min'] = array_shift($options);
            if (! empty($options)) {
                $temp['max'] = array_shift($options);
            }
            $options = $temp;
        }

        parent::__construct($options);
    }

    /**
     * Returns the min option
     *
     * @return int
     */
    public function getMin()
    {
        return $this->options['min'];
    }

    /**
     * Sets the min option
     *
     * @param  int $min
     * @throws Exception\InvalidArgumentException
     * @return DeliveryPeriodValidator
     */
    public function setMin($min)
    {
        if (null !== $this->getMax() && $min > $this->getMax()) {
            throw new Exception\InvalidArgumentException(
                "The minimum must be small than or equal to the maximum value, but {$min} > {$this->getMax()}"
            );
        }

        $this->options['min'] = max(0, (int) $min);
        return $this;
    }

    /**
     * Returns the max option
     *
     * @return int|null
     */
    public function getMax()
    {
        return $this->options['max'];
    }

    /**
     * Sets the max option
     *
     * @param  int|null $max
     * @throws Exception\InvalidArgumentException
     * @return DeliveryPeriodValidator
     */
    public function setMax($max)
    {
        if (null === $max) {
            $this->options['max'] = null;
        } elseif ($max < $this->getMin()) {
            throw new Exception\InvalidArgumentException(
                "The maximum must be large than or equal to the minimum value, but {$max} < {$this->getMin()}"
            );
        } else {
            $this->options['max'] = (int) $max;
        }

        return $this;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function isValid($value)
    {
        if (! is_string($value) && ! is_int($value) && ! is_float($value)) {
            $this->error(self::INVALID);
            return false;
        }

        $this->setValue((string) $value);

        if ('' === $this->getValue()) {
            $this->error(self::STRING_EMPTY);
            return false;
        }

        if (null === static::$filter) {
            static::$filter = new DigitsFilter();
        }

        if ($this->getValue() !== static::$filter->filter($this->getValue())) {
            $this->error(self::NOT_DIGITS);
            return false;
        }

        if ($value < $this->getMin()) {
            $this->error(self::TOO_SMALL);
        }
        if (null !== $this->getMax() && $this->getMax() < $value) {
            $this->error(self::TOO_LONG);
        }
        if (count($this->getMessages())) {
            return false;
        }

        return true;
    }
}