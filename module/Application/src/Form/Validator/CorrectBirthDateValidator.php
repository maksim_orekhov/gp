<?php
namespace Application\Form\Validator;

use Zend\Validator\AbstractValidator;
use DateTime;

/**
 * Class CorrectDateValidator
 * @package Application\Form\Validator
 */
class CorrectBirthDateValidator extends AbstractValidator
{
    const MIN_AGE = 6;
    const MAX_AGE = 130;

    const AGE_IS_TOO_YOUNG = 'ageIsTooYoung';
    const AGE_IS_TOO_BIG = 'ageIsTooBig';

    protected $messageTemplates = [
        self::AGE_IS_TOO_YOUNG  => 'Ваш возраст слишком мал',
        self::AGE_IS_TOO_BIG  => 'Указана некорректная дата рождения',
    ];

    /**
     * @param mixed $date_value
     * @return bool
     */
    public function isValid($date_value)
    {
        if (!strtotime($date_value)) {

           return false;
        }

        /** @var DateTime $date */
        $date = new DateTime(date('Y-m-d H:i:s', strtotime($date_value)));
        /** @var DateTime $currentDate */
        $currentDate = new DateTime('now');

        $difference = $currentDate->diff($date);

        if ($difference->y < self::MIN_AGE) {
            $this->error(self::AGE_IS_TOO_YOUNG);

            return false;
        }

        if ($difference->y > self::MAX_AGE) {
            $this->error(self::AGE_IS_TOO_BIG);

            return false;
        }

        return true;
    }
}