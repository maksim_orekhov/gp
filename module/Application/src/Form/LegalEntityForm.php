<?php
namespace Application\Form;

use Application\Entity\NdsType;
use Zend\Form\Element\Select;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class LegalEntityForm
 * @package Application\Form
 */
class LegalEntityForm extends Form
{
    use \Application\Provider\FormFieldsetTrait;

    public function __construct($ndsTypes = null)
    {
        parent::__construct('legal-entity');

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-default');
        $this->addElements();
        $this->addInputFilter();
        $this->setNdsTypeOptions($ndsTypes);
    }

    /**
     * @param $ndsTypes
     */
    protected function setNdsTypeOptions($ndsTypes)
    {
        /** @var Select $select */
        $select = $this->get('nds_type');
        $selectOptions = [];
        $selectOptions['0'] = 'Выберите НДС';
        if($ndsTypes !== null) {
            /** @var NdsType $ndsType */
            foreach ($ndsTypes as $ndsType) {
                $selectOptions[$ndsType->getType()] = $ndsType->getName();
            }
        }

        $select->setValueOptions($selectOptions);
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'id'=>'name',
                'placeholder'=>'Введите наименование'
            ],
            'options' => [
                'label' => 'Наименование',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'legal_address',
            'attributes' => [
                'id'=>'adress',
                'placeholder'=>'Введите юридический адрес'
            ],
            'options' => [
                'label' => 'Юридический адрес',
            ],
        ]);

//        $this->add([
//            'type'  => 'text',
//            'name' => 'phone',
//            'attributes' => [
//                'id'=>'phone',
//                'class'=>'form-input-element-phone-js',
//                'placeholder'=>'+7 (_ _ _) _ _ _-_ _ -_ _'
//            ],
//            'options' => [
//                'label' => 'Телефон',
//            ],
//        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'inn',
            'attributes' => [
                'id'=>'number-inn',
                'placeholder'=>'Введите ИНН'
            ],
            'options' => [
                'label' => 'ИНН',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'kpp',
            'attributes' => [
                'id'=>'number-kpp',
                'placeholder'=>'Введите КПП'
            ],
            'options' => [
                'label' => 'КПП',
            ],
        ]);

//        $this->add([
//            'type'  => 'text',
//            'name' => 'ogrn',
//            'attributes' => [
//                'id'=>'number-ogrn',
//                'placeholder'=>'Введите ОГРН'
//            ],
//            'options' => [
//                'label' => 'ОГРН',
//            ],
//        ]);

        $this->add([
            'type'  => 'select',
            'name' => 'nds_type',
            'attributes' => [
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'Тип НДС',
                'value_options' => [],
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сохранить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 255]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'legal_address',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 255]],
            ],
        ]);

//        $inputFilter->add([
//            'name'     => 'phone',
//            'required' => false,
//            'filters'  => [
//                ['name' => 'StripTags'],
//                ['name' =>  'StringTrim'],
//                [
//                    'name' =>  'PregReplace',
//                    'options' => [
//                        'pattern' => '/(\()|(\))|(\-)/',
//                        'replacement' => ''
//                    ]
//                ],
//            ],
//            'validators' => [
//                [
//                    'name'    => 'Regex',
//                    'options' => [
//                        'pattern' => '/\d{11}/',
//                        'message' => 'Номер телефона заполнен не полностью'
//                    ]
//                ],
//            ],
//        ]);

        $inputFilter->add([
            'name'     => 'inn',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            //валидный инн для теста юр лиц: 4217030520
            //валидный инн для теста физ лиц: 683103646744
            'validators' => [
                ['name' => \Application\Form\Validator\InnValidator::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'kpp',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 9]],
            ],
        ]);

//        $inputFilter->add([
//            'name'     => 'ogrn',
//            'required' => false,
//            'filters'  => [
//                ['name' =>  'StringTrim'],
//            ],
//            'validators' => [
//                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 15]],
//            ],
//        ]);

        $inputFilter->add([
            'name'     => 'nds_type',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\Digits::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => ['zero'],
                ],
            ]
        ]);
    }
}