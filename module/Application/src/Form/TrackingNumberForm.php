<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class TrackingNumberForm
 * @package Application\Form
 */
class TrackingNumberForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('track-number');
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'tracking-number-form');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'tracking_number',
            'attributes' => [
                'class'=>'form-control',
                'placeholder' => 'Введите трек-номер и название транспортной компании',
            ],
            'options' => [
                'label' => 'Введите трек-номер',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Добавить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'tracking_number',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 5, 'max' => 500
                    ]
                ],
            ],
        ]);
    }
}