<?php
namespace Application\Form\Fieldset;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class LegalEntityTaxInspectionFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct($name)
    {
        parent::__construct($name);

        $this->add([
            'type'  => 'text',
            'name' => 'serial_number',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Серия и номер'
            ],
            'options' => [
                'label' => 'Серия и номер',
            ],
        ]);

        $this->add([
            'type'  => 'date',
            'name' => 'date_issued',
            'options' => [
                'label' => 'Дата выдачи',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'form-control',
                'placeholder'=>'Дата выдачи'
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'issue_organisation',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Кем выдан'
            ],
            'options' => [
                'label' => 'Кем выдан',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'issue_registrar',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Кем зарегистрирован'
            ],
            'options' => [
                'label' => 'Кем зарегистрирован',
            ],
        ]);

        $this->add([
            'type'  => 'date',
            'name' => 'date_registered',
            'options' => [
                'label' => 'Дата регистрации',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'form-control',
                'placeholder'=>'Дата регистрации'
            ],
        ]);

        $this->add([
            'type'  => 'file',
            'name' => 'file',
            'options' => [
                'label' => 'Скан документа',
            ],
            'attributes' => [
                'class'=>'form-control',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            [
                'name'     => 'serial_number',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 45]],
                ],
            ],
            [
                'name'     => 'issue_organisation',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 10, 'max' => 255]],
                ],
            ],
            [
                'name'     => 'issue_registrar',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 10, 'max' => 255]],
                ],
            ],
            [
                'name'     => 'date_issued',
                'required' => true,
                'filters' => [],
                'validators' => [
                    [
                        'name' => \Application\Form\Validator\DateNowValidator::class,
                        'options' => [],
                    ]
                ],
            ],
            [
                'name'     => 'date_registered',
                'required' => true,
                'filters' => [],
                'validators' => [
                    [
                        'name' => \Application\Form\Validator\DateNowValidator::class,
                        'options' => [],
                    ]
                ],
            ],
            [
                'name'     => 'file',
                'required' => true,
                'filters' => [],
                'validators' => [
                    [
                        'name' => \Zend\Validator\File\IsImage::class,
                        'options' => [],
                    ],
                    [
                        'name' => \Zend\Validator\File\Size::class,
                        'options' => [
                            'max' => '10MB'
                        ],
                    ]
                ],
            ],
        ];
    }
}