<?php
namespace Application\Form\Fieldset;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class LegalEntityBankDetailFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct($name)
    {
        parent::__construct($name);

        $this->add([
            'type'  => 'text',
            'name' => 'checking_account',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Расчетный счет'
            ],
            'options' => [
                'label' => 'Расчетный счет',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Наименование банка'
            ],
            'options' => [
                'label' => 'Наименование банка',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'correspondent_account',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Корресподентский счет'
            ],
            'options' => [
                'label' => 'Корресподентский счет',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'bic',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'БИК'
            ],
            'options' => [
                'label' => 'БИК',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            [
                'name'     => 'checking_account',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
                ],
            ],
            [
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 255]],
                ],
            ],
            [
                'name'     => 'correspondent_account',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
                ],
            ],
            [
                'name'     => 'bic',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 9]],
                ],
            ],
        ];
    }
}