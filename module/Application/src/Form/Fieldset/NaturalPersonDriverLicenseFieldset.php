<?php
namespace Application\Form\Fieldset;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class NaturalPersonDriverLicenseFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct($name)
    {
        parent::__construct($name);

        $this->add([
            'type'  => 'date',
            'name' => 'date_issued',
            'options' => [
                'label' => 'Дата выдачи',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'form-control',
                'placeholder'=>'Дата выдачи'
            ],
        ]);

        $this->add([
            'type'  => 'file',
            'name' => 'file',
            'options' => [
                'label' => 'Скан документа',
            ],
            'attributes' => [
                'class'=>'form-control',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            [
                'name'     => 'date_issued',
                'required' => true,
                'filters' => [],
                'validators' => [
                    [
                        'name' => \Application\Form\Validator\DateNowValidator::class,
                        'options' => [],
                    ]
                ],
            ],
            [
                'name'     => 'file',
                'required' => true,
                'filters' => [],
                'validators' => [
                    [
                        'name' => \Zend\Validator\File\IsImage::class,
                        'options' => [],
                    ],
                    [
                        'name' => \Zend\Validator\File\Size::class,
                        'options' => [
                            'max' => '10MB'
                        ],
                    ]
                ],
            ]
        ];
    }
}