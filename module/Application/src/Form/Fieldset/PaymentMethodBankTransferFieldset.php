<?php
namespace Application\Form\Fieldset;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class PaymentMethodBankTransferFieldset extends Fieldset implements InputFilterProviderInterface
{
    public function __construct($name)
    {
        parent::__construct($name);

        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Название'
            ],
            'options' => [
                'label' => 'Название',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'bank_name',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Наименование банка'
            ],
            'options' => [
                'label' => 'Наименование банка',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'bik',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'БИК'
            ],
            'options' => [
                'label' => 'БИК',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'account_number',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Лицевой счет'
            ],
            'options' => [
                'label' => 'Лицевой счет',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'corr_account_number',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Корреспондентский счет'
            ],
            'options' => [
                'label' => 'Корреспондентский счет',
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            [
                'name'     => 'name',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 255]],
                ],
            ],
            [
                'name'     => 'bank_name',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 255]],
                ],
            ],
            [
                'name'     => 'bik',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                    ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 9]],
                ],
            ],
            [
                'name'     => 'account_number',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                    ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
                ]
            ],
            [
                'name'     => 'corr_account_number',
                'required' => true,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'Digits'],
                    ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
                ]
            ],
        ];
    }
}