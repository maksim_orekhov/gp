<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class NaturalPersonPassportForm extends Form
{
    public function __construct($name = 'natural-person-passport')
    {
        parent::__construct($name);

        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();

    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'hidden',
            'name' => 'idNaturalPerson',
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'passport_serial_number',
            'attributes' => [
                'id'=>'passport-number',
                'placeholder'=>'Укажите серию и номер'
            ],
            'options' => [
                'label' => 'Серия и номер',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'date_issued',
            'options' => [
                'label' => 'Дата выдачи',
            ],
            'attributes' => [
                'step' => 'any',
                'id' => 'passport-date-issue',
                'class'=>'form-input-datepicker datepicker-here',
                'placeholder'=>'Выберите дату выдачи'
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'passport_issue_organisation',
            'attributes' => [
                'id'=>'passport-issue',
                'placeholder'=>'Кем выдан'
            ],
            'options' => [
                'label' => 'Кем выдан',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'date_expired',
            'options' => [
                'label' => 'Дата истечения',
            ],
            'attributes' => [
                'step' => 'any',
                'id' => 'passport-date-expired',
                'class'=>'form-input-datepicker datepicker-here',
                'placeholder'=>'Выберите дату истечения'
            ],
        ]);

        $this->add([
            'type'  => 'file',
            'name' => 'file',
            'options' => [
                'label' => 'Скан документа',
            ],
            'attributes' => [
                'class'=>'form-control',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сохранить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'passport_serial_number',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 10, 'max' => 11]],   //два формата подразумевается 1234 567891 или 1234567891
            ],
        ]);
        $inputFilter->add([
            'name'     => 'passport_issue_organisation',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 10, 'max' => 255]],
            ],
        ]);
        $inputFilter->add([
            'name'     => 'date_issued',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Application\Form\Validator\DateNowValidator::class,
                    'options' => [],
                ]
            ],
        ]);
        $inputFilter->add([
            'name'     => 'date_expired',
            'required' => true,
            'filters' => [],
            'validators' => [],
        ]);
        $inputFilter->add([
            'name'     => 'file',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Zend\Validator\File\IsImage::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\File\Size::class,
                    'options' => [
                        'max' => '10MB'
                    ],
                ]
            ],
        ]);
    }
}