<?php
namespace Application\Form;

use Application\Entity\PaymentMethod;
use Application\Entity\PaymentMethodBankTransfer;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element\Select;

/**
 * Class PaymentMethodSelectionForm
 * @package Application\Form
 */
class PaymentMethodSelectionForm extends Form
{
    /**
     * PaymentMethodSelectionForm constructor.
     * @param array|null $paymentMethods
     */
    public function __construct(array $paymentMethods = null)
    {
        // Define form name
        parent::__construct('select-payment-method-form');

        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
        $this->setPaymentMethodOptions($paymentMethods);
    }

    /**
     * @param $paymentMethods
     */
    protected function setPaymentMethodOptions($paymentMethods)
    {
        /** @var Select $select */
        $select = $this->get('payment_method_id');
        $selectOptions = [];
        $selectOptions[0] = 'Выберите способ получения денег';
        if($paymentMethods !== null) {
            foreach ($paymentMethods as $paymentMethodId => $paymentMethod) {
                $selectOptions[$paymentMethodId] = $paymentMethod->getName();
            }
        }

        $select->setValueOptions($selectOptions);
        $this->setDefaultSelectedOption($select, $selectOptions);
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'select',
            'name' => 'payment_method_id',
            'attributes' => [
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'Субъект права',
                'value_options' => [],
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'payment_method_id',
            'required' => true,
            'filters'  => [
                ['name' =>  'ToInt'],
            ],
            'validators' => [  ['name'    => 'Regex',
                'options' => [
                    'pattern' => '/[1-9][0-9]*/',
                    'message' => 'Неверный формат ввода']
            ]]
        ]);
    }

    /**
     * @param Select $select
     * @param null $selectOptions
     */
    private function setDefaultSelectedOption(Select $select, $selectOptions = null)
    {
        $select->setValue('0');

        if($selectOptions && count($selectOptions) > 1){
            reset($selectOptions);      // Place pointer on the first element
            next($selectOptions);       // Advance to the second one
            $select->setValue(key($selectOptions));
        }
    }
}