<?php
namespace Application\Form;

use Application\Entity\NdsType;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element\Select;

/**
 * Class SoleProprietorForm
 * @package Application\Form
 */
class SoleProprietorForm extends Form
{
    use \Application\Provider\FormFieldsetTrait;

    public function __construct($ndsTypes = null)
    {
        parent::__construct('sole-proprietor');

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-default');
        $this->addElements();
        $this->addInputFilter();
        $this->setNdsTypeOptions($ndsTypes);
    }

    /**
     * @param $ndsTypes
     */
    protected function setNdsTypeOptions($ndsTypes)
    {
        /** @var Select $select */
        $select = $this->get('nds_type');
        $selectOptions = [];
        $selectOptions['0'] = 'Выберите НДС';
        if($ndsTypes !== null) {
            /** @var NdsType $ndsType */
            foreach ($ndsTypes as $ndsType) {
                $selectOptions[$ndsType->getType()] = $ndsType->getName();
            }
        }

        $select->setValueOptions($selectOptions);
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'id'=>'name',
                'placeholder'=>'Введите название'
            ],
            'options' => [
                'label' => 'Введите название',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'first_name',
            'attributes' => [
                'id'=>'person-name',
                'placeholder'=>'Введите имя'
            ],
            'options' => [
                'label' => 'Имя',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'secondary_name',
            'attributes' => [
                'id'=>'person-name-two',
                'placeholder'=>'Введите отчество'
            ],
            'options' => [
                'label' => 'Отчество',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'last_name',
            'attributes' => [
                'id'=>'person-surname',
                'placeholder'=>'Введите фамилию'
            ],
            'options' => [
                'label' => 'Фамилия',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'birth_date',
            'attributes' => [
                'step' => 'any',
                'id'=>'birthday',
                'class'=>'InputDate datepicker-here',
                'placeholder'=>'Выберите дату рождения',
            ],
            'options' => [
                'label' => 'Дата рождения',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'inn',
            'attributes' => [
                'id'=>'number-inn',
                'placeholder'=>'Введите ИНН'
            ],
            'options' => [
                'label' => 'ИНН',
            ],
        ]);

        $this->add([
            'type'  => 'select',
            'name' => 'nds_type',
            'attributes' => [
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'Тип НДС',
                'value_options' => [],
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сохранить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 255]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'first_name',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 45]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'secondary_name',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 45]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'last_name',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 45]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'birth_date',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Application\Form\Validator\DateNowValidator::class,
                    'options' => [],
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'inn',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            //валидный инн для теста юр лиц: 4217030520
            //валидный инн для теста физ лиц: 683103646744
            'validators' => [
                ['name' => \Application\Form\Validator\InnValidator::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'nds_type',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\Digits::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => ['zero'],
                ],
            ]
        ]);
    }
}