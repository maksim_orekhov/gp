<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * Class UpdateBankForm
 * @package Application\Form
 */
class UpdateBankForm extends Form
{
    const TYPE_FORM = 'update-bank-form';

    /**
     * UpdateBankForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', self::TYPE_FORM);
        $this->addElements();
    }

    protected function addElements()
    {
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Обновить банковскую базу',
                'class'=>'ButtonApply'
            ],
        ]);
    }
}