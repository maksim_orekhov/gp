<?php
namespace Application\Form;

use Zend\InputFilter\InputFilter;
use Zend\Form\Form;

class SystemPaymentDetailForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('payment-detail');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Наименование'
            ],
            'options' => [
                'label' => 'Наименование',
            ],
        ]);
        $this->add([
            'type'  => 'text',
            'name' => 'bik',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'БИК'
            ],
            'options' => [
                'label' => 'БИК',
            ],
        ]);
        $this->add([
            'type'  => 'text',
            'name' => 'account_number',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Лицевой счет'
            ],
            'options' => [
                'label' => 'Лицевой счет',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'payment_recipient_name',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Имя получателя платежа'
            ],
            'options' => [
                'label' => 'Имя получателя платежа',
            ],
        ]);
        $this->add([
            'type'  => 'text',
            'name' => 'inn',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'ИНН'
            ],
            'options' => [
                'label' => 'ИНН',
            ],
        ]);
        $this->add([
            'type'  => 'text',
            'name' => 'kpp',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'КПП'
            ],
            'options' => [
                'label' => 'КПП',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'bank',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Банк'
            ],
            'options' => [
                'label' => 'Банк',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'corr_account_number',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Номер корреспондентского счета'
            ],
            'options' => [
                'label' => 'Номер корреспондентского счета',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);
        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'class'=>'btn btn-primary',
                'value' => 'Сохранить',
                'id' => 'submit',
            ]
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 255]],
            ]
        ]);
        $inputFilter->add([
            'name'     => 'bik',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 9]],
            ]
        ]);
        $inputFilter->add([
            'name'     => 'account_number',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
            ]
        ]);
        $inputFilter->add([
            'name'     => 'payment_recipient_name',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 45]],
            ]
        ]);
        $inputFilter->add([
            'name'     => 'inn',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
                ['name' => 'StripTags']
            ],
            //валидный инн для теста юр лиц: 4217030520
            //валидный инн для теста физ лиц: 683103646744
            'validators' => [
                ['name' => \Application\Form\Validator\InnValidator::class],
            ],
        ]);
        $inputFilter->add([
            'name'     => 'kpp',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 9]],
            ]
        ]);

        $inputFilter->add([
            'name'     => 'bank',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 255]],
            ]
        ]);

        $inputFilter->add([
            'name'     => 'corr_account_number',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
            ]
        ]);
    }
}