<?php
namespace Application\Form;

use Application\Controller\PayController;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element\Hidden;

class PayVariantForm extends Form
{
    private $deal_id;

    /**
     * PayVariantForm constructor.
     * @param int $deal_id
     */
    public function __construct(int $deal_id)
    {
        $this->deal_id = $deal_id;

        // Define form name
        parent::__construct('pay-variant-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'pay-variant-form');
        #$this->setAttribute('action', '/'.$deal_id.'/pay/options');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return array
     */
    public function getOptionsForPayVariantSelect(): array
    {
        $pay_variants = [];

        foreach (PayController::PAY_VARIANTS as $key=>$value) {
            $pay_variants[$key] = $value;
        }

        return $pay_variants;
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => Hidden::class,
            'name' => 'deal_id',
            'attributes' => array(
                'value' => $this->deal_id
            ),
        ]);

        $this->add([
            'name' => 'pay_variant',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => [
                'id' => 'pay_variant',
                'placeholder'=>'Выберите вариант оплаты'
            ],
            'options' => array(
                'label' => 'Выберите вариант оплаты:',
                #'empty_option' => '---',
                'value_options' => $this->getOptionsForPayVariantSelect()
            )
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'deal_id',
            'required' => true,
            'validators' => [
                [
                    'name' => 'Digits'
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'pay_variant',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => ['min' => 6, 'max' => 40]
                ],
            ],
        ]);
    }
}