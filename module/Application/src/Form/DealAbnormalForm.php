<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Application\Service\Payment\PaymentPolitics;

class DealAbnormalForm extends Form
{
    use \Application\Provider\FormFieldsetTrait;

    /**
     * Doctrine entity manager.
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    public function __construct(PaymentPolitics $paymentPolitics)
    {
        $this->paymentPolitics = $paymentPolitics;
        parent::__construct('deal-abnormal-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return array
     */
    public function getOptionsForAbnormalTypeSelect()
    {
        return $this->paymentPolitics->getPaymentAbnormalTypes();
    }

    protected function addElements()
    {
        $this->add([
            'name' => 'abnormal_type',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => [
                'id' => 'abnormal_type',
                'class'=>'form-control',
            ],
            'options' => array(
                'label' => 'Тип проблемы',
                'value_options' => array_merge(
                    ['' => 'Все проблемы'],
                    $this->getOptionsForAbnormalTypeSelect()
                )
            )
        ]);
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Search',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'abnormal_type',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [],
        ]);
    }
}