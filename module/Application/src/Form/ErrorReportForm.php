<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class ErrorReportForm
 * @package Application\Form
 */
class ErrorReportForm extends Form
{
    const TYPE_FORM = 'error-report-form';
    const REGEXP_EMAIL = '/^[^#@А-Яа-я,"][\.0-9A-Za-z-_]{0,}\@(([0-9A-Za-z-_]{2,18}\.[A-Za-z-]{2,5})|([0-9A-Za-z-_]{2,18}\.[0-9A-Za-z-_]{2,18}\.[-A-Za-z]{2,5}))/';

    public function __construct()
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', self::TYPE_FORM);
        $this->setAttribute('action', '/error-report-message');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'email',
            'attributes' => [
                'id' => 'email',
                'class'=>'form-control',
                'placeholder'=>'Ваш email',
            ],
            'options' => [
                'label' => 'Email',
            ],
        ]);

        $this->add([
            'type'  => 'hidden',
            'name' => 'error_message',
        ]);

        $this->add([
            'type'  => 'hidden',
            'name' => 'error_tracing',
        ]);

        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'error_report',
            'attributes' => [
                'id' => 'error_report',
                'class'=>'form-control',
                'placeholder'=>'Сообщение'
            ],
            'options' => [
                'label' => 'Сообщение',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Отправить',
                'class'=>'ButtonApply'
            ],
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'email',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => self::REGEXP_EMAIL,
                        'message' => 'Недопустимый email'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'error_report',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 5,
                        'max' => 2000
                    ],
                ],
            ],
        ]);
    }
}