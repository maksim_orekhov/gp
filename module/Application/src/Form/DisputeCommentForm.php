<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class DisputeCommentForm
 * @package Application\Form
 */
class DisputeCommentForm extends Form
{
    /**
     * @var string
     */
    private $dispute_id;

    /**
     * DealCommentForm constructor.
     * @param null $dispute_id
     */
    public function __construct($dispute_id = null)
    {
        $this->dispute_id = $dispute_id;

        // Define form name
        parent::__construct('dispute-comment-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'dispute-comment-form');
        $this->addElements();
        $this->addInputFilter();

        if($this->dispute_id) {
            // Set 'deal_id' field value
            $dealIdField = $this->get('dispute_id');
            $dealIdField->setValue($this->dispute_id);
        }
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'text',
            'attributes' => [
                'id' => 'text',
                'placeholder'=>'Комментарий'
            ],
            'options' => [
                'label' => 'Комментарий',
            ],
        ]);

        $this->add([
            'type'  => 'hidden',
            'name' => 'dispute_id',
            'attributes' => [
                'id' => 'dispute-id',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'text',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 10,
                        'max' => 20000,
                        'message' => 'Текст сообщения слишком длинный или слишком короткий'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'dispute_id',
            'required' => true,
            'filters'  => [
                ['name'    =>  'StringTrim'],
                ['name'    =>  'StripTags'],
                ['name'    =>  'StripNewlines'],
            ],
            'validators' => [
                ['name'    => 'StringLength', 'options' => ['min' => 1, 'max' => 11]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[0-9]$/',
                        'message' => 'Неверный формат ввода'
                    ]
                ],
            ],
        ]);
    }
}