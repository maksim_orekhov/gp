<?php
namespace Application\Form\PaymentMethod;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class BankTransferForm
 * @package Application\Form\PaymentMethod
 */
class BankTransferForm extends Form
{
    const TYPE_FORM = 'bank-transfer-payment-method-form';

    public function __construct()
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'hidden',
            'name' => 'civil_law_subject_id',
        ]);

        $this->add([
            'type'  => 'hidden',
            'name' => 'payment_method_type',
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Название'
            ],
            'options' => [
                'label' => 'Название',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'bank_name',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Наименование банка'
            ],
            'options' => [
                'label' => 'Наименование банка',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'bik',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'БИК'
            ],
            'options' => [
                'label' => 'БИК',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'account_number',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Лицевой счет'
            ],
            'options' => [
                'label' => 'Лицевой счет',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'corr_account_number',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Корреспондентский счет'
            ],
            'options' => [
                'label' => 'Корреспондентский счет',
            ],
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'type_form',
            'attributes' => [
                'value' => self::TYPE_FORM,
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сохранить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'civil_law_subject_id',
            'required' => true,
            'filters'  => [
                ['name' =>  'ToInt'],
            ],
            'validators' => [],
        ]);

        $inputFilter->add([
            'name'     => 'payment_method_type',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 4, 'max' => 16]],
            ],
        ]);

        $inputFilter->add([
            'name' => 'name',
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 255]],
            ],
        ]);

        $inputFilter->add([
            'name' => 'bank_name',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 255]],
            ],
        ]);

        $inputFilter->add([
            'name' => 'bik',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 9, 'max' => 9]],
            ],
        ]);

        $inputFilter->add([
            'name' => 'account_number',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 20, 'max' => 20]],
            ]
        ]);

        $inputFilter->add([
            'name' => 'corr_account_number',
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 20, 'max' => 20]],
            ]
        ]);

        $inputFilter->add([
            'name' => 'type_form',
            'required' => false,
            'filters' => [],
            'validators' => []
        ]);
    }
}