<?php
namespace Application\Form;

use Application\Entity\Deal;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class RefundRequestForm
 * @package Application\Form
 */
class RefundRequestForm extends Form
{
    const TYPE_FORM = 'refund-request-form';

    /**
     * @var Deal|null
     */
    private $deal;

    /**
     * DiscountRequestForm constructor.
     * @param Deal|null $deal
     * @param float|int $max_discount
     */
    public function __construct($deal = null, $max_discount = 0)
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        $this->deal = $deal;

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', self::TYPE_FORM);
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $deal_attributes = [];
        if ($this->deal && $this->deal instanceof Deal){
            $deal_attributes = [
                'value' => $this->deal->getId(),
            ];
        }
        $this->add([
            'type' => 'hidden',
            'name' => 'deal',
            'attributes' => $deal_attributes,
        ]);


        $this->add([
            'type' => 'hidden',
            'name' => 'type_form',
            'attributes' => [
                'value' => self::TYPE_FORM,
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Отправить',
                'class'=>'ButtonApply'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'type_form',
            'required' => false,
            'filters' => [],
            'validators' => []
        ]);

        $inputFilter->add([
            'name'     => 'deal',
            'required' => true,
            'validators' => [
                [
                    'name' => \Zend\Validator\Digits::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => ['zero'],
                ],
            ],
        ]);
    }
}