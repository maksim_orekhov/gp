<?php
namespace Application\Form;

use Application\Controller\DealController;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Entity\User;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;
use Application\Service\Deal\DealManager;

/**
 * Class DealPartnerForm
 * @package Application\Form
 */
class DealPartnerForm extends Form
{
    /**
     * Doctrine entity manager.
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var User|null
     */
    private $user;

    /**
     * @var null|string
     */
    private $minDeliveryPeriod;

    /**
     * @var array
     */
    private $partner_token_data;

    /**
     * DealPartnerForm constructor.
     * @param DealManager $dealManager
     * @param User|null $user
     * @param array $partner_token_data
     * @throws \Exception
     */
    public function __construct(DealManager $dealManager, User $user = null, array $partner_token_data = [])
    {
        $this->dealManager = $dealManager;
        $this->user = $user;
        $this->minDeliveryPeriod = $dealManager->getMinDeliveryPeriodFromPolitic();
        $this->partner_token_data = $partner_token_data;

        // Define form name
        parent::__construct('deal-partner-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'deal-partner-form');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return array
     */
    public function getOptionsForDealTypeSelect(): array
    {
        $dealTypes = $this->dealManager->getDealTypes();
        $deal_types_array = [];
        /** @var DealType $dealType */
        foreach ($dealTypes as $dealType) {
            $deal_types_array[$dealType->getId()] = $dealType->getName();
        }

        return $deal_types_array;
    }

    /**
     * @return array
     */
    public function getOptionsForFeePayerOptionSelect(): array
    {
        $feePayerOptions = $this->dealManager->getFeePayerOptions();

        $fee_payer_options_array = [];
        /** @var FeePayerOption $feePayerOption */
        foreach ($feePayerOptions as $feePayerOption) {
            $fee_payer_options_array[$feePayerOption->getId()]
                = DealManager::FEE_PAYER_OPTION_TRANSLATE[$feePayerOption->getName()];
        }

        return $fee_payer_options_array;
    }

    /**
     * @throws \Exception
     */
    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'placeholder'=>'Название товара/услуги'
            ],
            'options' => [
                'label' => 'Название товара/услуги',
            ],
        ]);

        if (array_key_exists('amount', $this->partner_token_data) && null !== $this->partner_token_data['amount']) {
            $amount_attributes = [
                'id' => 'amount',
                'placeholder'=>'Сумма сделки',
                'readonly'=>'true',
                'value'=>$this->partner_token_data['amount'],
            ];
        } else {
            $amount_attributes = [
                'id' => 'amount',
                'placeholder'=>'Сумма сделки'
            ];
        }
        $this->add([
            'type'  => Element\Number::class,
            'name' => 'amount',
            'attributes' => $amount_attributes,
            'options' => [
                'label' => 'Сумма сделки',
            ],
        ]);

        if (null === $this->user) {
            $this->add([
                'type'  => 'Zend\Form\Element\Email',
                'name' => 'agent_email',
                'attributes' => [
                    'id' => 'agent_email',
                    'placeholder'=>'Ваш email'
                ],
                'options' => [
                    'label' => 'Ваш email',
                ],
            ]);
        }


        if (!array_key_exists('deal_type', $this->partner_token_data) || null === $this->partner_token_data['deal_type']) {
            $this->add([
                'name' => 'deal_type',
                'type' => 'Zend\Form\Element\Select',
                'attributes' => [
                    'id' => 'deal_type',
                    'placeholder'=>'Тип сделки'
                ],
                'options' => array(
                    'label' => 'Тип сделки',
                    'empty_option' => 'Выберите тип сделки',
                    'value_options' => $this->getOptionsForDealTypeSelect()
                )
            ]);
        }

        $this->add([
            'name' => 'fee_payer_option',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => [
                'id' => 'fee_payer_option',
                'placeholder'=>'Вариант оплаты комиссии'
            ],
            'options' => array(
                'empty_option' => 'Выберите вариант оплаты комиссии',
                'label' => 'Вариант оплаты комиссии',
                'value_options' => $this->getOptionsForFeePayerOptionSelect()
            )
        ]);

        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'addon_terms',
            'attributes' => [
                'id' => 'addon_terms',
                'class'=>'form-control',
                'placeholder'=>'Описание сделки'
            ],
            'options' => [
                'label' => 'Описание сделки',
            ],
        ]);

        $this->add([
            'type'  => Element\Number::class,
            'name' => 'delivery_period',
            'attributes' => [
                'id' => 'delivery_period',
                'class'=>'form-control',
                'placeholder'=>'Срок доставки/исполнения и проверки (минимум 1 сутки)'
            ],
            'options' => [
                'label' => 'Срок доставки/исполнения и проверки (минимум 1 сутки)',
            ],
        ]);

        $this->add([
            'name' => 'deal_civil_law_subject',
            'type' => 'text',
            'attributes' => [
                'id' => 'deal_civil_law_subject',
                'placeholder' => 'Вы будете участвовать в сделке как:'
            ],
            #'options' => $deal_civil_law_subject_options
        ]);

        $this->add([
            'type'  => Element\File::class,
            'name' => 'files',
            'options' => [
                'label' => 'Файл',
            ],
            'attributes' => [
                'id' => 'file',
                'class'=>'form-control',
                'multiple'=> true
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => ['min' => 2, 'max' => 255]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'amount',
            'required' => true,
            'validators' => [
                ['name'    => \Zend\Validator\LessThan::class,
                    'options' => [
                        'max' => DealController::MAX_DEAL_AMOUNT, // Миллиард
                        'inclusive' => true]
                ],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[1-9][0-9]*/',
                        'message' => 'Неверный формат ввода']
                ],
            ],
        ]);

        if (!array_key_exists('deal_type', $this->partner_token_data) || null === $this->partner_token_data['deal_type']) {
            $inputFilter->add([
                'name' => 'deal_type',
                'required' => true,
                'validators' => [
                    [
                        'name' => 'Digits'
                    ],
                ],
            ]);
        }

        $inputFilter->add([
            'name'     => 'delivery_period',
            'required' => true,
            'validators' => [
                [
                    'name'    => \Application\Form\Validator\DeliveryPeriodValidator::class,
                    'options' => [
                        'min' => $this->minDeliveryPeriod,
                    ]
                ],
            ],
        ]);

        if (null === $this->user) {
            $inputFilter->add([
                'name'     => 'agent_email',
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags']
                ],
                'validators' => [
                    [
                        'name'    => 'EmailAddress'
                    ],
                ],
            ]);
        }

        $inputFilter->add([
            'name'     => 'addon_terms',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 5,
                        'max' => 2000
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'deal_civil_law_subject',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
//            'validators' => [
//                [
//                    'name' => 'StringLength',
//                    'options' => ['min' => 1, 'max' => 500]
//                ],
//            ],
        ]);

        $inputFilter->add([
            'name'     => 'files',
            'required' => false,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Zend\Validator\File\Size::class,
                    'options' => [
                        'max' => '10MB'
                    ],
                ]
            ],
        ]);
    }
}