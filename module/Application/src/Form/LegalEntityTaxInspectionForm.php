<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class LegalEntityTaxInspectionForm extends Form
{
    public function __construct($name = 'legal-entity-tax-inspection')
    {
        parent::__construct($name);

        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'hidden',
            'name' => 'idLegalEntity',
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'serial_number',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Серия и номер'
            ],
            'options' => [
                'label' => 'Серия и номер',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'date_issued',
            'options' => [
                'label' => 'Дата выдачи',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'form-control',
                'placeholder'=>'Дата выдачи'
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'issue_organisation',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Кем выдан'
            ],
            'options' => [
                'label' => 'Кем выдан',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'issue_registrar',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Кем зарегистрирован'
            ],
            'options' => [
                'label' => 'Кем зарегистрирован',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'date_registered',
            'options' => [
                'label' => 'Дата регистрации',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'form-control',
                'placeholder'=>'Дата регистрации'
            ],
        ]);

        $this->add([
            'type'  => 'file',
            'name' => 'file',
            'options' => [
                'label' => 'Скан документа',
            ],
            'attributes' => [
                'class'=>'form-control',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сохранить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'serial_number',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 45]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'issue_organisation',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 10, 'max' => 255]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'issue_registrar',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 10, 'max' => 255]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'date_issued',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Application\Form\Validator\DateNowValidator::class,
                    'options' => [],
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'date_registered',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Application\Form\Validator\DateNowValidator::class,
                    'options' => [],
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'file',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Zend\Validator\File\IsImage::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\File\Size::class,
                    'options' => [
                        'max' => '10MB'
                    ],
                ]
            ],
        ]);
    }
}