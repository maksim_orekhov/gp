<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class NaturalPersonForm extends Form
{
    use \Application\Provider\FormFieldsetTrait;

    public function __construct(array $fieldsets = [])
    {
        parent::__construct('natural-person');

        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-default');

        $this->addElements();
        $this->addInputFilter();
        $this->attachFieldsets($fieldsets);
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'first_name',
            'attributes' => [
                'id'=>'person-name',
                'placeholder'=>'Введите имя'
            ],
            'options' => [
                'label' => 'Имя',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'secondary_name',
            'attributes' => [
                'id'=>'person-name-two',
                'placeholder'=>'Введите отчество'
            ],
            'options' => [
                'label' => 'Отчество',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'last_name',
            'attributes' => [
                'id'=>'person-surname',
                'placeholder'=>'Введите фамилию'
            ],
            'options' => [
                'label' => 'Фамилия',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'birth_date',
            'attributes' => [
                'step' => 'any',
                'id'=>'birthday',
                'class'=>'InputDate datepicker-here',
                'placeholder'=>'Выберите дату рождения',
            ],
            'options' => [
                'label' => 'Дата рождения',
            ],
        ]);

        /*
        $this->add([
            'type'  => 'hidden',
            'name' => 'count_passport',
            'attributes' => ['value' => 0]
        ]);

        $this->add([
            'type'  => 'hidden',
            'name' => 'count_driver_license',
            'attributes' => ['value' => 0]
        ]);
        */

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сохранить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'first_name',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 45]],
                [
                    'name' => \Application\Form\Validator\NamesValidation::class,
                    'options' => [],
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'secondary_name',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 45]],
                [
                    'name' => \Application\Form\Validator\NamesValidation::class,
                    'options' => [],
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'last_name',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 45]],
                [
                    'name' => \Application\Form\Validator\NamesValidation::class,
                    'options' => [],
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'birth_date',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Application\Form\Validator\CorrectBirthDateValidator::class,
                    'options' => [],
                ]
            ],
        ]);
    }
}