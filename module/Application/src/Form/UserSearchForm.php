<?php
namespace Application\Form;

use Zend\InputFilter\InputFilter;

/**
 * Class UserSearchForm
 * @package Application\Form
 */
class UserSearchForm extends FilterForm
{
    use \Application\Provider\FormFieldsetTrait;

    const PROPERTIES_FILTER_RULES = [
        'user_id'           => 'equal',
        'user_login'        => 'like',
        'user_created_from' => 'date_greater',
        'user_created_till' => 'date_smaller',
    ];

    /**
     * @var bool
     */
    private $is_operator;

    /**
     * DealSearchForm constructor.
     */
    public function __construct()
    {

        parent::__construct('user-search-form');

        // Set POST method for this form
        $this->setAttribute('method', 'get');
        $this->setAttribute('class', 'table-site__row table-site__filter');

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return array
     */
    public function getFilterRules()
    {
        return self::PROPERTIES_FILTER_RULES;
    }

    protected function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'filter[user_id]',
            'attributes' => [
                'id' => 'user_id',
                'class'=>'InputRounded',
                'placeholder'=>'Id',
            ],
            'options' => array(
                'label' => 'Id',
            )
        ]);

        $this->add([
            'type'  => 'text',
            'name' => "filter[user_login]",
            'attributes' => [
                'id' => 'user_login',
                'class'=>'filter-input',
                'placeholder'=>'Логин',
            ],
            'options' => [
                'label' => 'Логин',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[user_created_from]',
            'options' => [
                'label' => 'Дата регистрации от',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'filter-date__item datepicker-here',
                'placeholder'=>'Дата регистрации от',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[user_created_till]',
            'options' => [
                'label' => 'Дата регистрации до',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'filter-date__item datepicker-here',
                'placeholder'=>'Дата регистрации до',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Search',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        if ($this->is_operator) {

            $inputFilter->add([
                'name'     => 'filter[deal_id]',
                'required' => false,
                'filters'  => [
                    ['name'    =>  'StringTrim'],
                    ['name'    =>  'StripTags'],
                    ['name'    =>  'StripNewlines'],
                ],
                'validators' => [
                    ['name'    => 'StringLength', 'options' => ['min' => 1, 'max' => 11]],
                    ['name'    => 'Regex',
                        'options' => [
                            'pattern' => '/[0-9]$/',
                            'message' => 'Неверный формат ввода']
                    ],
                ],
            ]);

        }

        $inputFilter->add([
            'name'     => 'filter[user_login]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 16]],
            ],
        ]);

        // @TODO для валидации дат применить регулярное выражение по (цифрам и точке)
        $inputFilter->add([
            'name'     => 'filter[user_created_from]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                #['name' => 'StringLength', 'options' => ['min' => 10, 'max' => 10]],
            ],
        ]);
        $inputFilter->add([
            'name'     => 'filter[user_created_till]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                #['name' => 'StringLength', 'options' => ['min' => 10, 'max' => 10]],
            ],
        ]);
    }
}