<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class LegalEntityBankDetailForm extends Form
{
    public function __construct($name = 'legal-entity-bank-detail')
    {
        parent::__construct($name);

        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();

    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'hidden',
            'name' => 'idLegalEntity',
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'checking_account',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Расчетный счет'
            ],
            'options' => [
                'label' => 'Расчетный счет',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Наименование банка'
            ],
            'options' => [
                'label' => 'Наименование банка',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'correspondent_account',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'Корресподентский счет'
            ],
            'options' => [
                'label' => 'Корресподентский счет',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'bic',
            'attributes' => [
                'class'=>'form-control',
                'placeholder'=>'БИК'
            ],
            'options' => [
                'label' => 'БИК',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сохранить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'checking_account',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 255]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'correspondent_account',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'bic',
            'required' => true,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 9]],
            ],
        ]);
    }
}