<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class NaturalPersonDriverLicenseForm extends Form
{
    public function __construct($name = 'natural-person-driver-license')
    {
        parent::__construct($name);

        $this->setAttribute('method', 'post');

        $this->addElements();
        $this->addInputFilter();

    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'hidden',
            'name' => 'idNaturalPerson',
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'date_issued',
            'options' => [
                'label' => 'Дата выдачи',
            ],
            'attributes' => [
                'step' => 'any',
                'id' => 'passport-date-issue',
                'class'=>'form-input-datepicker datepicker-here',
                'placeholder'=>'Выберите дату выдачи'
            ],
        ]);

        $this->add([
            'type'  => 'file',
            'name' => 'file',
            'options' => [
                'label' => 'Скан документа',
            ],
            'attributes' => [
                'class'=>'form-control',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сохранить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'date_issued',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Application\Form\Validator\DateNowValidator::class,
                    'options' => [],
                ]
            ],
        ]);
        $inputFilter->add([
            'name'     => 'file',
            'required' => true,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Zend\Validator\File\IsImage::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\File\Size::class,
                    'options' => [
                        'max' => '10MB'
                    ],
                ]
            ],
        ]);
    }
}