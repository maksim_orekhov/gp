<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * Class DisputeOpenForm
 * @package Application\Form
 */
class DisputeOpenForm extends Form
{
    /**
     * DisputeOpenForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('dispute-open-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'dispute-open-form');
        $this->addElements();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }
}