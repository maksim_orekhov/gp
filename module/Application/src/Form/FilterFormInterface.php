<?php
namespace Application\Form;


/**
 * Interface FilterFormInterface
 * @package Application\Form
 */
interface FilterFormInterface
{

    public function getFilterRules();
}