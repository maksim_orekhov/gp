<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class DisputeFileUploadForm
 * @package Application\Form
 */
class DisputeFileUploadForm extends Form
{
    /**
     * @var int deal_id
     */
    private $dispute_id;

    /**
     * DisputeFileUploadForm constructor.
     * @param int $dispute_id
     */
    public function __construct(int $dispute_id)
    {
        // Define form name
        parent::__construct('dispute-file-upload');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'dispute-file-upload-form');
        $this->addElements();
        $this->addInputFilter();

        $this->dispute_id = $dispute_id;

        $this->get('dispute_id')->setValue($this->dispute_id);
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type' => 'Zend\Form\Element\File',
            'name' => 'file',
            'options' => [
                'label' => 'File',
                'class'=>'form-control',
            ],
            'attributes' => [
                'id' => 'file',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
            'attributes' => [
                'id' => 'csrf',
            ],
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'dispute_id',
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Upload',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     * The file element’s default input specification will create the correct Input type: Zend\InputFilter\FileInput.
     * The FileInput will automatically prepend an UploadFile Validator, to securely validate that the file is actually an uploaded file,
     * and to report other types of upload errors to the user.
     */
    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'file',
            'required' => true
        ]);

        $inputFilter->add([
            'name'     => 'dispute_id',
            'required' => true,
            'filters'  => [
                ['name' =>  'ToInt'],
            ],
            'validators' => [],
        ]);
    }
}