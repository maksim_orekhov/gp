<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;
use Application\Form\Validator\DeliveryPeriodValidator;

/**
 * Class DisputeCloseForm
 * @package Application\Form
 */
class DisputeCloseForm extends Form
{
    const MIN_DAYS = 0;
    const MAX_DAYS = 100;

    /**
     * @var boolean
     */
    private $is_operator;

    /**
     * DisputeCloseForm constructor.
     * @param bool $isOperator
     */
    public function __construct($isOperator = false)
    {
        $this->is_operator = $isOperator;

        // Define form name
        parent::__construct('dispute-close-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'dispute-close-form');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        if ($this->is_operator) {
            $this->add([
                'type'  => Element\Number::class,
                'name' => 'days',
                'attributes' => [
                    'id' => 'days',
                    'placeholder'=>'Количество дней продления гарантии по сделке'
                ],
                'options' => [
                    'label' => 'Срок продления гарантии по сделке (кол-во дней)',
                ],
            ]);
        }

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        if ($this->is_operator) {
            $inputFilter->add([
                'name' => 'days',
                'required' => true,
                'validators' => [
                    [
                        'name' => DeliveryPeriodValidator::class,
                        'options' => [
                            'min' => self::MIN_DAYS,
                            'max' => self::MAX_DAYS,
                        ]
                    ],
                ],
            ]);
        }
    }
}