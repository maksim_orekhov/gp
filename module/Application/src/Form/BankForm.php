<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * Class BankForm
 * @package Application\Form
 */
class BankForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('bank-search');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'bik',
            'options' => [
                'label' => 'Bik',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'name',
            'options' => [
                'label' => 'Name',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }
}