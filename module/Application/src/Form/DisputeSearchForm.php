<?php
namespace Application\Form;

use Zend\InputFilter\InputFilter;

class DisputeSearchForm extends FilterForm
{
    use \Application\Provider\FormFieldsetTrait;

    const PROPERTIES_FILTER_RULES = [
        'deal_id'               => 'equal',
        'deal_name'             => 'like',
        'deal_number'           => 'like',
        'dispute_created_from'  => 'date_greater',
        'dispute_created_till'  => 'date_smaller',
        'dispute_author'        => 'like',
        'dispute_closed'        => 'equal',
        'deadline_date_from' => 'custom',
        'deadline_date_till' => 'custom',
    ];

    /**
     * DisputeSearchForm constructor.
     */
    public function __construct()
    {
        parent::__construct('dispute-search-form');

        // Set POST method for this form
        $this->setAttribute('method', 'get');

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return array
     */
    public function getFilterRules()
    {
        return self::PROPERTIES_FILTER_RULES;
    }

    protected function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'filter[deal_id]',
            'attributes' => [
                'id' => 'deal_id',
                'placeholder'=>'Id',
            ],
            'options' => array(
                'label' => 'Id',
            )
        ]);

        $this->add([
            'type'  => 'text',
            'name' => "filter[deal_name]",
            'attributes' => [
                'id' => 'deal_name',
                'placeholder'=>'Название сделки',
            ],
            'options' => [
                'label' => 'Название сделки',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[dispute_created_from]',
            'options' => [
                'label' => 'Дата от',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'filter-date__item datepicker-here',
                'placeholder'=>'Дата от',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[dispute_created_till]',
            'options' => [
                'label' => 'Дата до',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'filter-date__item datepicker-here',
                'placeholder'=>'Дата до',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[deadline_date_from]',
            'options' => [
                'label' => 'Дата от',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'InputDate2 datepicker-here',
                'placeholder'=>'Дата от',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[deadline_date_till]',
            'options' => [
                'label' => 'Дата до',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'InputDate2 datepicker-here',
                'placeholder'=>'Дата до',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'filter[dispute_closed]',
            'attributes' => [
                'id' => 'dispute_closed',
                'class'=>'form-control',
            ],
            'options' => array(
                'label' => 'Статус',
                'value_options' => [
                    '' => '',
                    '1' => 'Закрытые',
                    '0' => 'Открытые'
                ]
            )
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'filter[dispute_author]',
            'attributes' => [
                'id' => 'dispute_author',
                'data-placeholder'=>'Инициатор спора',
            ],
            'options' => array(
                '' => '',
                'label' => 'Инициатор спора',
            )
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Search',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'filter[deal_id]',
            'required' => false,
            'filters'  => [
                ['name'    =>  'StringTrim'],
                ['name'    =>  'StripTags'],
                ['name'    =>  'StripNewlines'],
            ],
            'validators' => [
                ['name'    => 'StringLength', 'options' => ['min' => 1, 'max' => 11]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[1-9][0-9]*/',
                        'message' => 'Неверный формат ввода']
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[deal_name]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 100]],
            ],
        ]);

        // @TODO для валидации дат применить регулярное выражение по (цифрам и точке)
        $inputFilter->add([
            'name'     => 'filter[dispute_created_from]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                #['name' => 'StringLength', 'options' => ['min' => 10, 'max' => 10]],
            ],
        ]);
        $inputFilter->add([
            'name'     => 'filter[dispute_created_till]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'Regex',
                    'options' => [
                        'pattern' => '/\d{1}/',
                        #'message' => 'Сообщение'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[dispute_closed]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 1]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[dispute_author]',
            'required' => false,
            'filters'  => [
                ['name'    =>  'StringTrim'],
                ['name'    =>  'StripTags'],
                ['name'    =>  'StripNewlines'],
            ],
            'validators' => [
                ['name'    => 'StringLength', 'options' => ['min' => 3, 'max' => 16]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/^[a-zA-Z][a-zA-Z0-9-_\.]{2,15}$/',
                        'message' => 'Неверный формат ввода']
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[deadline_date_from]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'Regex',
                    'options' => [
                        'pattern' => '/\d{1}/',
                        #'message' => 'Сообщение'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[deadline_date_till]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'Regex',
                    'options' => [
                        'pattern' => '/\d{1}/',
                        #'message' => 'Сообщение'
                    ]
                ],
            ],
        ]);
    }
}