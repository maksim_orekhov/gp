<?php
namespace Application\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class DatePickupForm
 * @package Application\Form
 */
class DatePickupForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('date-pickup');
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'date-pickup-form');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type' => Element\Date::class,
            'name' => 'date_pickup',
            'attributes' => [
                'class'=>'form-control',
                'placeholder' => 'Дата забора',
            ],
            'options' => [
                'label' => 'Дата забора',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Подтвердить',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'date_pickup',
            'required' => true,
            'filters'  => [
                ['name' => \Zend\Filter\DateSelect::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Date::class],
            ],
        ]);
    }
}