<?php
namespace Application\Form;

use Application\Entity\DealType;
use Application\Service\Deal\DealStatus;
use Zend\InputFilter\InputFilter;
use Application\Service\Deal\DealManager;
use Zend\Form\Element\Select;

class DealSearchForm extends FilterForm
{
    use \Application\Provider\FormFieldsetTrait;

    const PROPERTIES_FILTER_RULES = [
        'deal_id'           => 'equal',             // for Operator only
        'deal_number'       => 'like',              // for Operator only
        'deal_name'         => 'like',
        'deal_created_from' => 'date_greater',
        'deal_created_till' => 'date_smaller',
        'deal_amount'       => 'equal',
        'deal_role'         => 'exception_case',    // for User only
        'deal_owner'        => 'equal',             // for Operator only
        'deal_debit_status' => 'exception_case',    // for Operator only
//        'deal_status',
//        'counteragent',
//        'user_login',
//        'customer_login',
//        'contractor_login',
    ];

    /**
     * Doctrine entity manager.
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var bool
     */
    private $is_operator;

    /**
     * DealSearchForm constructor.
     * @param DealManager $dealManager
     * @param bool $is_operator
     */
    public function __construct(DealManager $dealManager, bool $is_operator)
    {
        $this->dealManager = $dealManager;
        $this->is_operator = $is_operator;

        parent::__construct('deal-search-form');

        // Set POST method for this form
        $this->setAttribute('method', 'get');
        $this->setAttribute('class', 'table-site__row table-site__filter');

        $this->addElements();
        $this->addInputFilter();
        $this->setStatusOptions();
    }

    /**
     * @return array
     */
    public function getOptionsForDealTypeSelect()
    {
        $deal_types = $this->dealManager->getDealTypes();

        $deal_types_array = [];
        /** @var DealType $deal_type */
        foreach ($deal_types as $deal_type) {
            $deal_types_array[$deal_type->getId()] = $deal_type->getName();
        }

        return $deal_types_array;
    }

    /**
     *
     */
    protected function setStatusOptions()
    {
        /** @var Select $select */
        $select = $this->get('filter[deal_status]');
        $selectOptions = [];
        $selectOptions[0] = 'Статус';
        foreach (DealStatus::DEAL_STATUSES as $status) {
            $selectOptions[$status['status']] = $status['name'];
        }

        $select->setValueOptions($selectOptions);
        #$this->setDefaultSelectedOption($select, $selectOptions);
    }

    /**
     * @return array
     */
    public function getFilterRules()
    {
        return self::PROPERTIES_FILTER_RULES;
    }

    protected function addElements()
    {
        if ($this->is_operator) {
//            $this->add([
//                'type' => 'text',
//                'name' => 'filter[deal_id]',
//                'attributes' => [
//                    'id' => 'deal_id',
//                    'placeholder'=>'Id',
//                ],
//                'options' => array(
//                    'label' => 'Id',
//                )
//            ]);
            $this->add([
                'type' => 'text',
                'name' => 'filter[deal_number]',
                'attributes' => [
                    'id' => 'deal_number',
                    'class'=>'InputRounded',
                    'placeholder'=>'Номер сделки',
                ],
                'options' => array(
                    'label' => 'Номер сделки',
                )
            ]);
        }

        $this->add([
            'type'  => 'text',
            'name' => "filter[deal_name]",
            'attributes' => [
                'id' => 'deal_name',
                'class'=>'filter-input',
                'placeholder'=>'Название',
            ],
            'options' => [
                'label' => 'Название сделки',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[deal_amount]',
            'attributes' => [
                'id' => 'deal_amount',
                'class'=>'filter-input',
                'placeholder'=>'Сумма',
            ],
            'options' => [
                'label' => 'Сумма',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[deal_created_from]',
            'options' => [
                'label' => 'Дата от',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'filter-date__item datepicker-here',
                'placeholder'=>'Дата от',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[deal_created_till]',
            'options' => [
                'label' => 'Дата до',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'filter-date__item datepicker-here',
                'placeholder'=>'Дата до',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'filter[deal_type]',
            'attributes' => [
                'id' => 'deal_type',
                'class'=>'form-control',
            ],
            'options' => array(
                'label' => 'Тип сделки',
                'value_options' => $this->getOptionsForDealTypeSelect()
            )
        ]);

        if ($this->is_operator) {

            $this->add([
                'type' => 'text',
                'name' => 'filter[deal_owner]',
                'attributes' => [
                    'id' => 'deal_owner',
                    'class'=>'InputSearch',
                    'placeholder'=>'Владелец',
                ],
                'options' => array(
                    '' => '',
                    'label' => 'Deal owner',
                )
            ]);

        } else {

            $this->add([
                'type' => 'Zend\Form\Element\Select',
                'name' => 'filter[deal_role]',
                'attributes' => [
                    'id' => 'deal_role',
                    'class'=>'InputRounded chosen-select',
                    'data-placeholder'=>'Роль',
                ],
                'options' => array(
                    'label' => 'Deal role',
                    'value_options' => [
                        '' => '',
                        'customer' => 'Покупатель',
                        'contractor' => 'Продавец'
                    ]
                )
            ]);
        }

        if ($this->is_operator) {
            $this->add([
                'type' => 'Zend\Form\Element\Select',
                'name' => 'filter[deal_debit_status]',
                'attributes' => [
                    'id' => 'deal_debit_status',
                    'class'=>'chosen-select',
                    'data-placeholder' => 'Дебет',
                ],
                'options' => array(
                    'label' => 'Дебет',
                    'value_options' => [
                        '' => '',
                        '1' => 'Недоплаченные',
                        '2' => 'Переплаченные'
                    ]
                )
            ]);
        }

        // @TODO value_options получать из констант в DealManager
        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'filter[deal_status]',
            'attributes' => [
                'id' => 'deal_status',
                'class' => 'InputRounded chosen-select',
                'data-placeholder' => 'Статус',
            ],
            'options' => array(
                'label' => 'Стату',
                'value_options' => [],
            )
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Search',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);


        if ($this->is_operator) {
//            $inputFilter->add([
//                'name'     => 'filter[deal_id]',
//                'required' => false,
//                'filters'  => [
//                    ['name'    =>  'StringTrim'],
//                    ['name'    =>  'StripTags'],
//                    ['name'    =>  'StripNewlines'],
//                ],
//                'validators' => [
//                    ['name'    => 'StringLength', 'options' => ['min' => 1, 'max' => 11]],
//                    ['name'    => 'Regex',
//                        'options' => [
//                            'pattern' => '/[1-9][0-9]*/',
//                            'message' => 'Неверный формат ввода']
//                    ],
//                ],
//            ]);

            $inputFilter->add([
                'name'     => 'filter[deal_number]',
                'required' => false,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 10]],
                ],
            ]);
        }

        $inputFilter->add([
            'name'     => 'filter[deal_name]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 100]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[deal_amount]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 20]],
            ],
        ]);

        // @TODO для валидации дат применить регулярное выражение по (цифрам и точке)
        $inputFilter->add([
            'name'     => 'filter[date_created_from]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                #['name' => 'StringLength', 'options' => ['min' => 10, 'max' => 10]],
            ],
        ]);
        $inputFilter->add([
            'name'     => 'filter[date_created_till]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'Regex',
                    'options' => [
                        'pattern' => '/\d{1}/',
                        #'message' => 'Сообщение'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[deal_type]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 1]],
            ],
        ]);

        if ($this->is_operator) {

            $inputFilter->add([
                'name'     => 'filter[deal_owner]',
                'required' => false,
                'filters'  => [
                    ['name'    =>  'StringTrim'],
                    ['name'    =>  'StripTags'],
                    ['name'    =>  'StripNewlines'],
                ],
                'validators' => [
                    ['name'    => 'StringLength', 'options' => ['min' => 3, 'max' => 16]],
                    ['name'    => 'Regex',
                        'options' => [
                            'pattern' => '/^[a-zA-Z][a-zA-Z0-9-_\.]{2,15}$/',
                            'message' => 'Неверный формат ввода']
                    ],
                ],
            ]);

        } else {

            $inputFilter->add([
                'name'     => 'filter[deal_role]',
                'required' => false,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 8, 'max' => 10]],
                ],
            ]);

        }

        $inputFilter->add([
            'name'     => 'filter[deal_status]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 1]],
            ],
        ]);

        if ($this->is_operator) {
            $inputFilter->add([
                'name' => 'filter[deal_debit_status]',
                'required' => false,
                'filters'  => [
                    ['name' =>  'StringTrim'],
                ],
                'validators' => [
                ],
            ]);
        }

        $inputFilter->add([
            'name'     => 'filter[counteragent]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 2, 'max' => 50]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[user]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 16]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[customer]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 16]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[contractor]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 16]],
            ],
        ]);
    }
}