<?php
namespace Application\Form;

use ModulePaymentOrder\Entity\PaymentMethodType;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class PaymentMethodTypeSelectionForm
 * @package Application\Form
 */
class PaymentMethodTypeSelectionForm extends Form
{
    private $civil_id;

    /**
     * PaymentMethodTypeSelectionForm constructor.
     * @param array|null $paymentMethodTypes
     * @param null $civil_id
     */
    public function __construct(array $paymentMethodTypes = null, $civil_id = null)
    {
        parent::__construct('select-payment-method-type-form');

        $this->setAttribute('method', 'get');

        $this->civil_id = $civil_id;
        $this->addElements();
        $this->addInputFilter();
        $this->setCivilLawSubject();
        $this->setPaymentMethodOptions($paymentMethodTypes);
    }

    protected function setCivilLawSubject()
    {
        if ($this->civil_id === null) {
            return;
        }

        $field = $this->get('civil_law_subject_id');
        $field->setValue($this->civil_id);
    }

    /**
     * @param $paymentMethodTypes
     */
    protected function setPaymentMethodOptions($paymentMethodTypes)
    {
        if ($paymentMethodTypes === null) {
            return;
        }

        $field = $this->get('payment_method_type');
        $selectOptions = [];
        $selectOptions[0] = 'Выберите метод оплаты';
        /** @var PaymentMethodType $type */
        foreach ($paymentMethodTypes as $type) {
            $selectOptions[$type->getName()] = $type->getOutputName();
        }
        $field->setValueOptions($selectOptions);
    }

    protected function addElements()
    {
        if ($this->civil_id) {
            $this->add([
                'type' => 'hidden',
                'name' => 'civil_law_subject_id',
            ]);
        }

        $this->add([
            'type'  => 'select',
            'name' => 'payment_method_type',
            'attributes' => [
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'Тип метода оплаты',
                'value_options' => [],
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'civil_law_subject_id',
            'required' => true,
            'filters'  => [
                ['name' =>  'ToInt'],
            ],
            'validators' => [],
        ]);

        $inputFilter->add([
            'name'     => 'payment_method_type',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 4, 'max' => 16]],
            ],
        ]);
    }
}