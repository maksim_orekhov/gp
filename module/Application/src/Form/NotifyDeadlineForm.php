<?php
namespace Application\Form;

use Zend\Form\Form;

/**
 * Class NotifyDeadlineForm
 * @package Application\Form
 */
class NotifyDeadlineForm extends Form
{
    const TYPE_FORM = 'notify-deadline-form';

    public function __construct()
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);
        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
    }

    protected function addElements()
    {

        $this->add([
            'type' => 'hidden',
            'name' => 'type_form',
            'attributes' => [
                'value' => self::TYPE_FORM,
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Отправить уведомления',
                'class'=>'ButtonApply'
            ],
        ]);
    }
}