<?php
namespace Application\Form;

use Zend\Form\Element\Select;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class CivilLawSubjectSelectionForm
 * @package Application\Form
 */
class CivilLawSubjectSelectionForm extends Form
{
    private $payment_method_type;

    /**
     * CivilLawSubjectSelectionForm constructor.
     * @param array|null $civilLawSubject
     * @param null $payment_method_type
     */
    public function __construct(array $civilLawSubject = null, $payment_method_type = null)
    {
        // Define form name
        parent::__construct('select-civil-law-subject-form');

        $this->setAttribute('method', 'get');

        $this->payment_method_type = $payment_method_type;
        $this->addElements();
        $this->addInputFilter();
        $this->setPaymentMethodType();
        $this->setCivilLawSubjectOptions($civilLawSubject);
    }

    protected function setPaymentMethodType()
    {
        if ($this->payment_method_type === null) {
            return;
        }

        $field = $this->get('payment_method_type');
        $field->setValue($this->payment_method_type);
    }

    /**
     * @param $civilLawSubjects
     */
    protected function setCivilLawSubjectOptions($civilLawSubjects)
    {
        /** @var Select $select */
        $select = $this->get('civil_law_subject_id');
        $selectOptions = [];
        $selectOptions[0] = 'Выберите субъект права';
        if($civilLawSubjects !== null) {
            foreach ($civilLawSubjects as $civilLawSubject) {
                if ($civilLawSubject->getNaturalPerson()) {
                    $selectOptions[$civilLawSubject->getId()] = $civilLawSubject->getNaturalPerson()->getFirstName() . " " . $civilLawSubject->getNaturalPerson()->getLastName();
                } elseif ($civilLawSubject->getSoleProprietor()) {
                    $selectOptions[$civilLawSubject->getId()] = $civilLawSubject->getSoleProprietor()->getName();
                } elseif ($civilLawSubject->getLegalEntity()) {
                    $selectOptions[$civilLawSubject->getId()] = $civilLawSubject->getLegalEntity()->getName();
                }
            }
        }

        $select->setValueOptions($selectOptions);
        $this->setDefaultSelectedOption($select, $selectOptions);
    }

    protected function addElements()
    {
        if ($this->payment_method_type) {
            $this->add([
                'type' => 'hidden',
                'name' => 'payment_method_type',
            ]);
        }

        $this->add([
            'type'  => 'select',
            'name' => 'civil_law_subject_id',
            'attributes' => [
                'class'=>'form-control',
            ],
            'options' => [
                'label' => 'Субъект права',
                'value_options' => [],
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        if ($this->payment_method_type) {
            $inputFilter->add([
                'name' => 'payment_method_type',
                'required' => true,
                'filters' => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'StringLength', 'options' => ['min' => 4, 'max' => 16]],
                ],
            ]);
        }

        $inputFilter->add([
            'name'     => 'civil_law_subject_id',
            'required' => true,
            'filters'  => [
                ['name' =>  'ToInt'],
            ],
            'validators' => [  ['name'    => 'Regex',
                'options' => [
                    'pattern' => '/[1-9][0-9]*/',
                    'message' => 'Неверный формат ввода']
            ]],
        ]);

    }

    /**
     * @param Select $select
     * @param null $selectOptions
     */
    private function setDefaultSelectedOption(Select $select, $selectOptions = null)
    {
        $select->setValue('0');

        if($selectOptions && count($selectOptions) > 1){
            reset($selectOptions);      // Place pointer on the first element
            next($selectOptions);       // Advance to the second one
            $select->setValue(key($selectOptions));
        }
    }
}