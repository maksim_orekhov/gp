<?php
namespace Application\Form;

use Application\Entity\Deal;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class RepaidOverpayForm
 * @package Application\Form
 */
class RepaidOverpayForm extends Form
{
    /**
     * @var Deal|null
     */
    private $deal;

    /**
     * RepaidOverpayForm constructor.
     * @param null $deal
     */
    public function __construct($deal = null)
    {
        // Define form name
        parent::__construct('repaid-overpay-form');

        $this->deal = $deal;

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $deal_attributes = [];
        if ($this->deal && $this->deal instanceof Deal){
            $deal_attributes = [
                'value' => $this->deal->getId(),
            ];
        }
        $this->add([
            'type' => 'hidden',
            'name' => 'deal_id',
            'attributes' => $deal_attributes,
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Подготовить платежку для возврата',
                'class'=>'ButtonApply'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'deal_id',
            'required' => true,
            'validators' => [
                [
                    'name' => \Zend\Validator\Digits::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\NotEmpty::class,
                    'options' => ['zero'],
                ],
            ],
        ]);
    }
}