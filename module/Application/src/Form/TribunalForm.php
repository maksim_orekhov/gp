<?php
namespace Application\Form;

use Application\Entity\Dispute;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class TribunalForm
 * @package Application\Form
 */
class TribunalForm extends Form
{
    const TYPE_FORM = 'tribunal_form';

    /**
     * @var Dispute|null
     */
    private $dispute;

    /**
     * TribunalForm constructor.
     * @param Dispute|null $dispute
     */
    public function __construct($dispute = null)
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        $this->dispute = $dispute;

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'dispute-tribunal-create-form');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $dispute_attributes = [];
        if ($this->dispute && $this->dispute instanceof Dispute){
            $dispute_attributes = [
                'value' => $this->dispute->getId(),
            ];
        }
        $this->add([
            'type' => 'hidden',
            'name' => 'dispute_id',
            'attributes' => $dispute_attributes,
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'type_form',
            'attributes' => [
                'value' => self::TYPE_FORM,
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Направить в суд',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'type_form',
            'required' => false,
            'filters' => [],
            'validators' => []
        ]);

        $inputFilter->add([
            'name'     => 'dispute_id',
            'required' => true,
            'validators' => [
                [
                    'name'    => \Application\Form\Validator\ResolutionDisputeValidator::class,
                    'options' => [
                        'dispute' => $this->dispute,
                    ]
                ],
            ],
        ]);
    }
}