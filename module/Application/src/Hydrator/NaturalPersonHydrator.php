<?php

namespace Application\Hydrator;

use Application\Entity\NaturalPerson;
use Application\Hydrator\Strategy\CivilLawSubjectMinimumStrategy;
use Application\Hydrator\Strategy\DateStrategy;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

/**
 * Class NaturalPersonHydrator
 * @package Application\Hydrator
 */
class NaturalPersonHydrator
{
    /**
     * Maximally full extraction of entity
     * @param NaturalPerson $naturalPerson
     * @return array
     */
    public function extract(NaturalPerson $naturalPerson)
    {
        $hydrator = new ReflectionHydrator();
        $hydrator->addStrategy('birthDate', new DateStrategy());
        $hydrator->addStrategy('civilLawSubject', new CivilLawSubjectMinimumStrategy());
        // @TODO Добавить стратегии для других свойств

        return $hydrator->extract($naturalPerson);
    }

    /**
     * Extraction of one element of collection (only the most necessary properties)
     *
     * @param NaturalPerson $naturalPerson
     * @return array
     */
    public function extractOneOfCollection(NaturalPerson $naturalPerson)
    {
        $hydrator = new ReflectionHydrator();

        // Исключаем свойство 'birthDate'
        $hydrator->addFilter('birthDate', function ($property) {
            return $property !== 'birthDate';
        }, FilterComposite::CONDITION_AND);
        // Исключаем свойство 'civilLawSubject'
        $hydrator->addFilter('civilLawSubject', function ($property) {
            return $property !== 'civilLawSubject';
        }, FilterComposite::CONDITION_AND);
        // Исключаем свойство 'naturalPersonDocument'
        $hydrator->addFilter('naturalPersonDocument', function ($property) {
            return $property !== 'naturalPersonDocument';
        }, FilterComposite::CONDITION_AND);

        return $hydrator->extract($naturalPerson);
    }

    /**
     * Extraction of collection
     *
     * @param $objects
     * @return array|null
     */
    public function extractCollection($objects)
    {
        $objects_data = null;
        if ($objects && is_array($objects)) {
            $objects_data = [];
            foreach ($objects as $object) {
                $objects_data[] = $this->extractOneOfCollection($object);
            }
        }

        return $objects_data;
    }
}