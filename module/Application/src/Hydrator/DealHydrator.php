<?php

namespace Application\Hydrator;

use Application\Hydrator\Strategy\DateTimeStrategy;
use Application\Entity\Deal;
use Application\Hydrator\Strategy\DealAgentMinimumStrategy;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Application\Hydrator\Strategy\DealAgentStrategy;
use Application\Hydrator\Strategy\DealTypeStrategy;
use Zend\Hydrator\Filter\FilterComposite;

/**
 * Class DealHydrator
 * @package Application\Service\Deal
 */
class DealHydrator
{
    /**
     * @param Deal $deal
     * @return array
     */
    public static function extract(Deal $deal)
    {
        $hydrator = new ReflectionHydrator();
        $hydrator->addStrategy('customer', new DealAgentStrategy());
        $hydrator->addStrategy('contractor', new DealAgentStrategy());
        $hydrator->addStrategy('dealType', new DealTypeStrategy());
        $hydrator->addStrategy('created', new DateTimeStrategy());
        $hydrator->addStrategy('updated', new DateTimeStrategy());

        return $hydrator->extract($deal);
    }

    /**
     * Extraction of one element of collection (only the most necessary properties)
     *
     * @param Deal $deal
     * @return array
     */
    public static function extractOneOfCollection(Deal $deal)
    {
        $hydrator = new ReflectionHydrator();
        $hydrator->addStrategy('customer', new DealAgentMinimumStrategy());
        $hydrator->addStrategy('contractor', new DealAgentMinimumStrategy());
        $hydrator->addStrategy('dealType', new DealTypeStrategy());
        $hydrator->addStrategy('created', new DateTimeStrategy());
        $hydrator->addStrategy('updated', new DateTimeStrategy());
        $hydrator->addFilter('payments', function ($property) {
            return $property !== 'payments';
        }, FilterComposite::CONDITION_AND);
        $hydrator->addFilter('feePayerOption', function ($property) {
            return $property !== 'feePayerOption';
        }, FilterComposite::CONDITION_AND);

        return $hydrator->extract($deal);
    }

    /**
     * Extraction of collection
     *
     * @param $objects
     * @return array|null
     */
    public function extractCollection($objects)
    {
        $objects_data = null;
        if ($objects && is_array($objects)) {
            $objects_data = [];
            foreach ($objects as $object) {
                $objects_data[] = $this->extractOneOfCollection($object);
            }
        }

        return $objects_data;
    }
}