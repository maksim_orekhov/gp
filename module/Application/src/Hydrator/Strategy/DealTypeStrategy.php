<?php

namespace Application\Hydrator\Strategy;

use Zend\Hydrator\Strategy\DefaultStrategy;
use Application\Entity\DealType;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

class DealTypeStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof DealType) {
            $hydrator = new ReflectionHydrator();
            $hydrator->addFilter('id', function ($property) {
                return $property !== 'id';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('description', function ($property) {
                return $property !== 'description';
            }, FilterComposite::CONDITION_AND);

            return $hydrator->extract($value);
        }
    }
}