<?php

namespace Application\Hydrator\Strategy;

use Application\Entity\LegalEntity;
use Zend\Hydrator\Strategy\DefaultStrategy;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

/**
 * Class LegalEntityMinimumStrategy
 * @package Application\Hydrator\Strategy
 */
class LegalEntityMinimumStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof LegalEntity) {
            $hydrator = new ReflectionHydrator();
            $hydrator->addFilter('legalAddress', function ($property) {
                return $property !== 'legalAddress';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('phone', function ($property) {
                return $property !== 'phone';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('inn', function ($property) {
                return $property !== 'inn';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('kpp', function ($property) {
                return $property !== 'kpp';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('ogrn', function ($property) {
                return $property !== 'ogrn';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('civilLawSubject', function ($property) {
                return $property !== 'civilLawSubject';
            }, FilterComposite::CONDITION_AND);

            return $hydrator->extract($value);
        }
    }
}