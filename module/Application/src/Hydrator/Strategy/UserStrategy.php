<?php

namespace Application\Hydrator\Strategy;

use Zend\Hydrator\Strategy\DefaultStrategy;
use Application\Entity\User;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

class UserStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof User) {
            $hydrator = new ReflectionHydrator();
            // Исключаем ненужные свойства:
            $hydrator->addFilter('password', function ($property) {
                return $property !== 'password';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('codes', function ($property) {
                return $property !== 'codes';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('passwordResetToken', function ($property) {
                return $property !== 'passwordResetToken';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('passwordResetTokenCreationDate', function ($property) {
                return $property !== 'passwordResetTokenCreationDate';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('civilLawSubjects', function ($property) {
                return $property !== 'civilLawSubjects';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('paymentMethods', function ($property) {
                return $property !== 'paymentMethods';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('dealAgents', function ($property) {
                return $property !== 'dealAgents';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('roles', function ($property) {
                return $property !== 'roles';
            }, FilterComposite::CONDITION_AND);

            return $hydrator->extract($value);
        }
    }
}