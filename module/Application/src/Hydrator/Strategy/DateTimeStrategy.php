<?php

namespace Application\Hydrator\Strategy;

use DateTime;
use Zend\Hydrator\Strategy\DefaultStrategy;

class DateTimeStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     *
     * Convert a string value into a DateTime object
     */
    public function hydrate($value)
    {
        if (is_string($value)) {
            $value = new DateTime($value);
        }

        return $value;
    }

    public function extract($object)
    {
        if ( !$object instanceof \DateTime ) {
            return array();
        }

        return $object->format('Y-m-d H:i:s');
    }
}