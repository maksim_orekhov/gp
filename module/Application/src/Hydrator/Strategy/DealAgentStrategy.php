<?php

namespace Application\Hydrator\Strategy;

use Zend\Hydrator\Strategy\DefaultStrategy;
use Application\Entity\DealAgent;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

class DealAgentStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof DealAgent) {
            $hydrator = new ReflectionHydrator();
            $hydrator->addStrategy('user', new UserMinimumStrategy());

            return $hydrator->extract($value);
        }
    }
}