<?php

namespace Application\Hydrator\Strategy;

use Zend\Hydrator\Strategy\DefaultStrategy;
use Application\Entity\PaymentMethodBankTransfer;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

/**
 * Class PaymentMethodBankTransferMinimumStrategy
 * @package Application\Hydrator\Strategy
 */
class PaymentMethodBankTransferMinimumStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof PaymentMethodBankTransfer) {
            $hydrator = new ReflectionHydrator();
            /*
            $hydrator->addFilter('bik', function ($property) {
                return $property !== 'bik';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('accountNumber', function ($property) {
                return $property !== 'accountNumber';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('corrAccountNumber', function ($property) {
                return $property !== 'corrAccountNumber';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('paymentRecipientName', function ($property) {
                return $property !== 'paymentRecipientName';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('inn', function ($property) {
                return $property !== 'inn';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('kpp', function ($property) {
                return $property !== 'kpp';
            }, FilterComposite::CONDITION_AND);
            */

            $hydrator->addFilter('paymentMethod', function ($property) {
                return $property !== 'paymentMethod';
            });

            return $hydrator->extract($value);
        }
    }
}