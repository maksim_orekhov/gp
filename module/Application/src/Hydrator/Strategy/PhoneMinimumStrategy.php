<?php

namespace Application\Hydrator\Strategy;

use Zend\Hydrator\Strategy\DefaultStrategy;
use Application\Entity\User;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

class PhoneMinimumStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof User) {
            $hydrator = new ReflectionHydrator();
            $hydrator->addFilter('userEnter', function ($property) {
                return $property !== 'userEnter';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('comment', function ($property) {
                return $property !== 'comment';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('e164', function ($property) {
                return $property !== 'e164';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('country', function ($property) {
                return $property !== 'country';
            }, FilterComposite::CONDITION_AND);

            return $hydrator->extract($value);
        }
    }
}