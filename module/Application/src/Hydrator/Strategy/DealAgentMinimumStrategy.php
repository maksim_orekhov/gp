<?php

namespace Application\Hydrator\Strategy;

use Zend\Hydrator\Strategy\DefaultStrategy;
use Application\Entity\DealAgent;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

class DealAgentMinimumStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof DealAgent) {
            $hydrator = new ReflectionHydrator();
            $hydrator->addStrategy('user', new UserMinimumStrategy());
            $hydrator->addFilter('civilLawSubject', function ($property) {
                return $property !== 'civilLawSubject';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('customerDeals', function ($property) {
                return $property !== 'customerDeals';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('contractorDeals', function ($property) {
                return $property !== 'contractorDeals';
            }, FilterComposite::CONDITION_AND);

            return $hydrator->extract($value);
        }
    }
}