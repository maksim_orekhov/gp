<?php

namespace Application\Hydrator\Strategy;

use Zend\Hydrator\Strategy\DefaultStrategy;
use Application\Entity\CivilLawSubject;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

/**
 * Class CivilLawSubjectMinimumStrategy
 * @package Application\Hydrator\Strategy
 */
class CivilLawSubjectMinimumStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof CivilLawSubject) {
            $hydrator = new ReflectionHydrator();
            $hydrator->addStrategy('user', new UserMinimumStrategy());
            $hydrator->addStrategy('naturalPerson', new NaturalPersonMinimumStrategy());
            $hydrator->addStrategy('legalEntity', new LegalEntityMinimumStrategy());

            $hydrator->addFilter('isPattern', function ($property) {
                return $property !== 'isPattern';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('created', function ($property) {
                return $property !== 'created';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('paymentMethods', function ($property) {
                return $property !== 'paymentMethods';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('dealAgents', function ($property) {
                return $property !== 'dealAgents';
            }, FilterComposite::CONDITION_AND);

            return $hydrator->extract($value);
        }
    }
}