<?php

namespace Application\Hydrator\Strategy;

use Application\Entity\NaturalPerson;
use Zend\Hydrator\Strategy\DefaultStrategy;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

/**
 * Class NaturalPersonMinimumStrategy
 * @package Application\Hydrator\Strategy
 */
class NaturalPersonMinimumStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof NaturalPerson) {
            $hydrator = new ReflectionHydrator();
            $hydrator->addFilter('birthDate', function ($property) {
                return $property !== 'birthDate';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('civilLawSubject', function ($property) {
                return $property !== 'civilLawSubject';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('naturalPersonDocument', function ($property) {
                return $property !== 'naturalPersonDocument';
            }, FilterComposite::CONDITION_AND);

            return $hydrator->extract($value);
        }
    }
}