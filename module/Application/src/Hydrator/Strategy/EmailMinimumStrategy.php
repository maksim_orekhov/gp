<?php

namespace Application\Hydrator\Strategy;

use Zend\Hydrator\Strategy\DefaultStrategy;
use Application\Entity\User;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

class EmailMinimumStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof User) {
            $hydrator = new ReflectionHydrator();
            $hydrator->addFilter('confirmationToken', function ($property) {
                return $property !== 'confirmationToken';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('confirmationTokenCreationDate', function ($property) {
                return $property !== 'confirmationTokenCreationDate';
            }, FilterComposite::CONDITION_AND);

            return $hydrator->extract($value);
        }
    }
}