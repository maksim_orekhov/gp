<?php

namespace Application\Hydrator\Strategy;

use Zend\Hydrator\Strategy\DefaultStrategy;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

/**
 * Class PaymentMethodTypeMinimumStrategy
 * @package Application\Hydrator\Strategy
 */
class PaymentMethodTypeMinimumStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof PaymentMethodType) {
            $hydrator = new ReflectionHydrator();
            $hydrator->addFilter('description', function ($property) {
                return $property !== 'description';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('isActive', function ($property) {
                return $property !== 'isActive';
            }, FilterComposite::CONDITION_AND);

            return $hydrator->extract($value);
        }
    }
}