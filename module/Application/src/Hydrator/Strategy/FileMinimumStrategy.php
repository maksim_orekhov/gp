<?php

namespace Application\Hydrator\Strategy;

use Application\Entity\LegalEntity;
use Zend\Hydrator\Strategy\DefaultStrategy;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

/**
 * Class FileMinimumStrategy
 * @package Application\Hydrator\Strategy
 */
class FileMinimumStrategy extends DefaultStrategy
{
    /**
     * {@inheritdoc}
     */
    public function extract($value)
    {
        if(!is_object($value)) {
            return $value;
        }

        if($value instanceof LegalEntity) {
            $hydrator = new ReflectionHydrator();
            $hydrator->addFilter('originName', function ($property) {
                return $property !== 'originName';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('type', function ($property) {
                return $property !== 'type';
            }, FilterComposite::CONDITION_AND);
            $hydrator->addFilter('path', function ($property) {
                return $property !== 'path';
            }, FilterComposite::CONDITION_AND);

            return $hydrator->extract($value);
        }
    }
}