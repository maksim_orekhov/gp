<?php

namespace Application\Hydrator;

use Application\Entity\PaymentMethod;
use Application\Hydrator\Strategy\CivilLawSubjectMinimumStrategy;
use Application\Hydrator\Strategy\PaymentMethodBankTransferMinimumStrategy;
use Application\Hydrator\Strategy\PaymentMethodTypeMinimumStrategy;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

/**
 * Class PaymentMethodHydrator
 * @package Application\Hydrator
 */
class PaymentMethodHydrator
{
    /**
     * @param PaymentMethod $paymentMethod
     * @return array
     */
    public function extract(PaymentMethod $paymentMethod)
    {
        $hydrator = new ReflectionHydrator();
        $hydrator->addStrategy('civilLawSubject', new CivilLawSubjectMinimumStrategy());
        $hydrator->addStrategy('paymentMethodType', new PaymentMethodTypeMinimumStrategy());
        $hydrator->addStrategy('paymentMethodBankTransfer', new PaymentMethodBankTransferMinimumStrategy());
        // @TODO Добавить стратегии для других свойств

        // Исключаем свойство 'user'
        $hydrator->addFilter('isPattern', function ($property) {
            return $property !== 'isPattern';
        }, FilterComposite::CONDITION_AND);
        // Исключаем свойство 'user'
        $hydrator->addFilter('user', function ($property) {
            return $property !== 'user';
        }, FilterComposite::CONDITION_AND);

        return $hydrator->extract($paymentMethod);
    }

    /**
     * Extraction of collection
     *
     * @param $objects
     * @return array|null
     */
    public function extractCollection($objects)
    {
        $objects_data = null;
        if ($objects && is_array($objects)) {
            $objects_data = [];
            foreach ($objects as $object) {
                $objects_data[] = $this->extract($object);
            }
        }

        return $objects_data;
    }
}