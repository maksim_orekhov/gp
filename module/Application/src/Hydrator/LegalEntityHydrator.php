<?php

namespace Application\Hydrator;

use Application\Entity\LegalEntity;
use Application\Hydrator\Strategy\CivilLawSubjectMinimumStrategy;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

/**
 * Class LegalEntityHydrator
 * @package Application\Hydrator
 */
class LegalEntityHydrator
{
    /**
     * Maximally full extraction of entity
     * @param LegalEntity $legalEntity
     * @return array
     */
    public function extract(LegalEntity $legalEntity)
    {
        $hydrator = new ReflectionHydrator();
        $hydrator->addStrategy('civilLawSubject', new CivilLawSubjectMinimumStrategy());
        #$hydrator->addStrategy('legalEntityDocument', new LegalEntityDocumentStrategy()); // @TODO Создать
        // @TODO Добавить стратегии для других свойств

        return $hydrator->extract($legalEntity);
    }

    /**
     * Extraction of one element of collection (only the most necessary properties)
     *
     * @param LegalEntity $legalEntity
     * @return array
     */
    public function extractOneOfCollection(LegalEntity $legalEntity)
    {
        $hydrator = new ReflectionHydrator();

        // Исключаем свойство 'kpp'
        $hydrator->addFilter('kpp', function ($property) {
            return $property !== 'kpp';
        }, FilterComposite::CONDITION_AND);
        // Исключаем свойство 'ogrn'
        $hydrator->addFilter('ogrn', function ($property) {
            return $property !== 'ogrn';
        }, FilterComposite::CONDITION_AND);
        // Исключаем свойство 'civilLawSubject'
        $hydrator->addFilter('civilLawSubject', function ($property) {
            return $property !== 'civilLawSubject';
        }, FilterComposite::CONDITION_AND);
        // Исключаем свойство 'legalEntityDocument'
        $hydrator->addFilter('legalEntityDocument', function ($property) {
            return $property !== 'legalEntityDocument';
        }, FilterComposite::CONDITION_AND);

        return $hydrator->extract($legalEntity);
    }


    /**
     * Extraction of collection
     *
     * @param $objects
     * @return array|null
     */
    public function extractCollection($objects)
    {
        $objects_data = null;
        if ($objects && is_array($objects)) {
            $objects_data = [];
            foreach ($objects as $object) {
                $objects_data[] = $this->extractOneOfCollection($object);
            }
        }

        return $objects_data;
    }
}