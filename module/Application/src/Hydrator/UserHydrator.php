<?php

namespace Application\Hydrator;

use Application\Hydrator\Strategy\FileMinimumStrategy;
use Application\Entity\User;
use Application\Hydrator\Strategy\EmailMinimumStrategy;
use Application\Hydrator\Strategy\PhoneMinimumStrategy;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Hydrator\Filter\FilterComposite;

class UserHydrator
{
    /**
     * @param User $user
     * @return array
     */
    public function extract(User $user)
    {
        $hydrator = new ReflectionHydrator();
        #$hydrator->addStrategy('user', new UserStrategy());
        $hydrator->addStrategy('phone', new PhoneMinimumStrategy());
        $hydrator->addStrategy('email', new EmailMinimumStrategy());
        $hydrator->addStrategy('avatar', new FileMinimumStrategy());
        // @TODO Добавить стратегии для других свойств

        // Исключаем ненужные свойства:
        $hydrator->addFilter('password', function ($property) {
            return $property !== 'password';
        }, FilterComposite::CONDITION_AND);
        $hydrator->addFilter('passwordResetToken', function ($property) {
            return $property !== 'passwordResetToken';
        }, FilterComposite::CONDITION_AND);
        $hydrator->addFilter('passwordResetTokenCreationDate', function ($property) {
            return $property !== 'passwordResetTokenCreationDate';
        }, FilterComposite::CONDITION_AND);
        $hydrator->addFilter('created', function ($property) {
            return $property !== 'created';
        }, FilterComposite::CONDITION_AND);
        $hydrator->addFilter('codes', function ($property) {
            return $property !== 'codes';
        }, FilterComposite::CONDITION_AND);
        $hydrator->addFilter('civilLawSubjects', function ($property) {
            return $property !== 'civilLawSubjects';
        }, FilterComposite::CONDITION_AND);
        $hydrator->addFilter('paymentMethods', function ($property) {
            return $property !== 'paymentMethods';
        }, FilterComposite::CONDITION_AND);
        $hydrator->addFilter('dealAgents', function ($property) {
            return $property !== 'dealAgents';
        }, FilterComposite::CONDITION_AND);
        $hydrator->addFilter('roles', function ($property) {
            return $property !== 'roles';
        }, FilterComposite::CONDITION_AND);
        $hydrator->addFilter('created', function ($property) {
        return $property !== 'created';
        }, FilterComposite::CONDITION_AND);

        return $hydrator->extract($user);
    }

    /**
     * Extraction of collection
     *
     * @param $objects
     * @return array|null
     */
    public function extractCollection($objects)
    {
        $objects_data = null;
        if ($objects && is_array($objects)) {
            $objects_data = [];
            foreach ($objects as $object) {
                $objects_data[] = $this->extract($object);
            }
        }

        return $objects_data;
    }
}