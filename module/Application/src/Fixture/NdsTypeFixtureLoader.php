<?php

namespace Application\Fixture;

use Application\Entity\NdsType;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;

/**
 * Class DealTypeFixtureLoader
 * @package Application\Fixture
 */
class NdsTypeFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getOrder()
    {
        return 65;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $ndsType = new NdsType();
        $ndsType->setType(1);
        $ndsType->setName('0');
        $ndsType->setIsActive(1);
        $entityManager->persist($ndsType);

        $ndsType2 = new NdsType();
        $ndsType2->setType(2);
        $ndsType2->setName('без НДС');
        $ndsType2->setIsActive(1);
        $entityManager->persist($ndsType2);

        $ndsType3 = new NdsType();
        $ndsType3->setType(3);
        $ndsType3->setName('10%');
        $ndsType3->setIsActive(1);
        $entityManager->persist($ndsType3);

        $ndsType4 = new NdsType();
        $ndsType4->setType(4);
        $ndsType4->setName('18%');
        $ndsType4->setIsActive(1);
        $entityManager->persist($ndsType4);

        $entityManager->flush();
    }
}