<?php

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\DealType;

/**
 * Class DealTypeFixtureLoader
 * @package Application\Fixture
 */
class DealTypeFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $dealType = new DealType();
        $dealType->setName('Услуга');
        $dealType->setIdent('U');

        $entityManager->persist($dealType);

        $dealType2 = new DealType();
        $dealType2->setName('Товар');
        $dealType2->setIdent('T');

        $entityManager->persist($dealType2);

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 90;
    }
}