<?php

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\NaturalPerson;

/**
 * Class NaturalPersonFixtureLoader
 * @package Application\Fixture
 */
class NaturalPersonFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getOrder()
    {
        return 65;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $naturalPerson = new NaturalPerson();
        #$naturalPerson->setCivilLawSubject($setCivilLawSubject1);
        $naturalPerson->setFirstName('Тест');
        $naturalPerson->setSecondaryName('Тестович');
        $naturalPerson->setLastName('Тестов');
        $naturalPerson->setBirthDate(\DateTime::createFromFormat('!Y-m-d', '1980-12-31'));

        $entityManager->persist($naturalPerson);

        $naturalPerson = new NaturalPerson();
        #$naturalPerson->setCivilLawSubject($setCivilLawSubject2);
        $naturalPerson->setFirstName('Контрагент');
        $naturalPerson->setSecondaryName('Контрагентович');
        $naturalPerson->setLastName('Контрагентов');
        $naturalPerson->setBirthDate(\DateTime::createFromFormat('!Y-m-d', '1985-10-11'));

        $entityManager->persist($naturalPerson);

        $entityManager->flush();
    }
}