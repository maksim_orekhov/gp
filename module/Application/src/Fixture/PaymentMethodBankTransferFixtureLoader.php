<?php

namespace Application\Fixture;

use Application\Entity\LegalEntity;
use Application\Entity\NaturalPerson;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\PaymentMethod;
use Application\Entity\CivilLawSubject;
use Doctrine\ORM\EntityManager;
use ModulePaymentOrder\Entity\PaymentMethodType;

class PaymentMethodBankTransferFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    const TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1 = 'Счет ВТБ'; // user 1
    const TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_2 = 'Счет Открытие'; // user 2
    const TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_3 = 'Счет БинБанк'; // user 2
    const TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_4 = 'ВТБ для тестов'; // user 1
    const TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_5 = 'Супер Юридический Банк'; // user 1
    const TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_6 = 'Банк для Кастомеров Юриков'; // user 1

    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var CivilLawSubject
     */
    private $naturalPersonCivilLawSubject_1;

    /**
     * @var CivilLawSubject
     */
    private $naturalPersonCivilLawSubject_2;

    /**
     * @var CivilLawSubject
     */
    private $legalEntityCivilLawSubject_1;

    /**
     * @var CivilLawSubject
     */
    private $legalEntityCivilLawSubject_2;


    public function getOrder()
    {
        return 90;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function load(ObjectManager $manager)
    {
        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        // Set users to class vars $this->civilLawSubject1 и $this->civilLawSubject2
        $this->setCivilLawSubjects();

        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(array('name' => 'bank_transfer'));

        /// 4

        $paymentMethod_4 = new PaymentMethod();
        $paymentMethod_4->setIsPattern(true);
        $paymentMethod_4->setCivilLawSubject($this->naturalPersonCivilLawSubject_1);
        $paymentMethod_4->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentMethod_4);

        $paymentMethodBankTransfer = new PaymentMethodBankTransfer();
        $paymentMethodBankTransfer->setPaymentMethod($paymentMethod_4);
        $paymentMethodBankTransfer->setAccountNumber('40702810090030001128');
        $paymentMethodBankTransfer->setBik('044525201');
        $paymentMethodBankTransfer->setCorrAccountNumber('30101810900000000790');
        $paymentMethodBankTransfer->setName(self::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_4);
        $paymentMethodBankTransfer->setBankName('ВТБ');

        $this->entityManager->persist($paymentMethodBankTransfer);

        /// 1

        $paymentMethod_1 = new PaymentMethod();
        $paymentMethod_1->setIsPattern(true);
        $paymentMethod_1->setCivilLawSubject($this->naturalPersonCivilLawSubject_1);
        $paymentMethod_1->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentMethod_1);

        $paymentMethodBankTransfer = new PaymentMethodBankTransfer();
        $paymentMethodBankTransfer->setPaymentMethod($paymentMethod_1);
        $paymentMethodBankTransfer->setAccountNumber('40702810090030001128');
        $paymentMethodBankTransfer->setBik('044525201');
        $paymentMethodBankTransfer->setCorrAccountNumber('30101810900000000790');
        $paymentMethodBankTransfer->setName(self::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1);
        $paymentMethodBankTransfer->setBankName('ВТБ');

        $this->entityManager->persist($paymentMethodBankTransfer);

        /// 2

        $paymentMethod_2 = new PaymentMethod();
        $paymentMethod_2->setIsPattern(true);
        $paymentMethod_2->setCivilLawSubject($this->naturalPersonCivilLawSubject_2);
        $paymentMethod_2->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentMethod_2);

        $paymentMethodBankTransfer = new PaymentMethodBankTransfer();
        $paymentMethodBankTransfer->setPaymentMethod($paymentMethod_2);
        $paymentMethodBankTransfer->setAccountNumber('40702810090030001139');
        $paymentMethodBankTransfer->setBik('044525212');
        $paymentMethodBankTransfer->setCorrAccountNumber('30101810900000000792');
        $paymentMethodBankTransfer->setName(self::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_2);
        $paymentMethodBankTransfer->setBankName('ПАО Открытие');

        $this->entityManager->persist($paymentMethodBankTransfer);

        /// 3

        $paymentMethod_3 = new PaymentMethod();
        $paymentMethod_3->setIsPattern(true);
        $paymentMethod_3->setCivilLawSubject($this->naturalPersonCivilLawSubject_2);
        $paymentMethod_3->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentMethod_3);

        $paymentMethodBankTransfer = new PaymentMethodBankTransfer();
        $paymentMethodBankTransfer->setPaymentMethod($paymentMethod_3);
        $paymentMethodBankTransfer->setAccountNumber('40702810090030001140');
        $paymentMethodBankTransfer->setBik('044525213');
        $paymentMethodBankTransfer->setCorrAccountNumber('30101810900000000793');
        $paymentMethodBankTransfer->setName(self::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_3);
        $paymentMethodBankTransfer->setBankName('БинБанк');

        $this->entityManager->persist($paymentMethodBankTransfer);

        /// 5

        $paymentMethod_5 = new PaymentMethod();
        $paymentMethod_5->setIsPattern(true);
        $paymentMethod_5->setCivilLawSubject($this->legalEntityCivilLawSubject_1);
        $paymentMethod_5->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentMethod_5);

        $paymentMethodBankTransfer = new PaymentMethodBankTransfer();
        $paymentMethodBankTransfer->setPaymentMethod($paymentMethod_5);
        $paymentMethodBankTransfer->setAccountNumber('40702810090030005555');
        $paymentMethodBankTransfer->setBik('045555213');
        $paymentMethodBankTransfer->setCorrAccountNumber('30101810900000005555');
        $paymentMethodBankTransfer->setName(self::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_5);
        $paymentMethodBankTransfer->setBankName(self::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_5);

        $this->entityManager->persist($paymentMethodBankTransfer);

        /// 6

        $paymentMethod_6 = new PaymentMethod();
        $paymentMethod_6->setIsPattern(true);
        $paymentMethod_6->setCivilLawSubject($this->legalEntityCivilLawSubject_2);
        $paymentMethod_6->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentMethod_6);

        $paymentMethodBankTransfer = new PaymentMethodBankTransfer();
        $paymentMethodBankTransfer->setPaymentMethod($paymentMethod_6);
        $paymentMethodBankTransfer->setAccountNumber('40702810090030006666');
        $paymentMethodBankTransfer->setBik('046666213');
        $paymentMethodBankTransfer->setCorrAccountNumber('30101810900000006666');
        $paymentMethodBankTransfer->setName(self::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_6);
        $paymentMethodBankTransfer->setBankName(self::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_6);

        $this->entityManager->persist($paymentMethodBankTransfer);

        $this->entityManager->flush();
    }

    /**
     * Set civilLawSubjects
     */
    private function setCivilLawSubjects()
    {
        /** @var NaturalPerson $naturalPerson_1 */
        $naturalPerson_1 = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Тест', 'lastName' => 'Тестов'));
        /** @var NaturalPerson $naturalPerson_2 */
        $naturalPerson_2 = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Контрагент', 'lastName' => 'Контрагентов'));

        /** @var LegalEntity $legalEntity_1 */
        $legalEntity_1 = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Колумбийский сахар'));
        /** @var LegalEntity $legalEntity_2 */
        $legalEntity_2 = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Simple Technology'));

        $this->naturalPersonCivilLawSubject_1 = $naturalPerson_1->getCivilLawSubject();
        $this->naturalPersonCivilLawSubject_2 = $naturalPerson_2->getCivilLawSubject();

        $this->legalEntityCivilLawSubject_1 = $legalEntity_1->getCivilLawSubject();
        $this->legalEntityCivilLawSubject_2 = $legalEntity_2->getCivilLawSubject();
    }
}