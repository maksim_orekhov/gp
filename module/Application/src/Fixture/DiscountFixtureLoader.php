<?php

namespace Application\Fixture;

use Application\Entity\BankClientPaymentOrderFile;
use Application\Entity\DealAgent;
use Application\Entity\Discount;
use Application\Entity\Dispute;
use Application\Entity\DisputeCycle;
use Application\Entity\NaturalPerson;
use Application\Entity\PaymentMethodBankTransfer;
use ModuleFileManager\Entity\File;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\GlobalSetting;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\CivilLawSubject;
use Application\Service\BankClient\BankClientManager;
use Doctrine\ORM\EntityManager;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;

/**
 * Class DiscountFixtureLoader
 * @package Application\Fixture
 */
class DiscountFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \DateTime
     */
    private $currentDate;

    /**
     * @var integer
     */
    private $percentOfFee;

    /**
     * @var BankClientPaymentOrderManager
     */
    private $bankClientPaymentOrderManager;


    /**
     * @return int
     */
    public function getOrder()
    {
        return 160;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function load(ObjectManager $manager)
    {
        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        $this->bankClientPaymentOrderManager = $this->container->get(BankClientPaymentOrderManager::class);
        $this->currentDate   = new \DateTime();

        $this->setPercentOfFee();

        foreach (DealFixtureLoader::DEALS_IN_STATUS_DISPUTE as $deal_array) {

            if (array_key_exists('outgoing_discount_payment_order', $deal_array)
                && $deal_array['outgoing_discount_payment_order']) {

                /** @var Deal $deal */
                $deal = $this->entityManager->getRepository(Deal::class)
                    ->findOneBy(array('name' => $deal_array['name']));

                $add_to_file = false;

                if (isset($deal_array['add_outgoing_discount_payment_order_to_file'])) {

                    $add_to_file = $deal_array['add_outgoing_discount_payment_order_to_file'];
                }

                /** @var Payment $payment */
                $discount = $this->createDiscountWithPaymentOrder($deal, $deal_array['outgoing_discount_payment_order'], $add_to_file);

                $this->entityManager->persist($discount);
                $this->entityManager->flush();
            }
        }
    }

    /**
     * @param Deal $deal
     * @param $discount_amount
     * @param bool $add_to_file
     * @return Payment
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createDiscountWithPaymentOrder(Deal $deal, $discount_amount, $add_to_file = false)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $customer->getCivilLawSubject();
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $civilLawSubject->getPaymentMethods()->first();

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();

        $creationDate = clone new \DateTime();
        $date = $creationDate->modify('-3 min');

        $discount = new Discount();
        $discount->setPaymentMethod($paymentMethod);
        $discount->setCreated($date);
        $discount->setDispute($dispute);
        $discount->setAmount($discount_amount);
        $discount->setDisputeCycle($disputeCycle);

        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $this->createPaymentOrder();

        $discount->addPaymentOrder($paymentOrder);

        $this->entityManager->persist($discount);
        $disputeCycle->setDiscount($discount);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush();

        /** @var BankClientPaymentOrder $bankClientPaymentOrder */
        $bankClientPaymentOrder = $this->createBankClientPaymentOrder($deal, $paymentOrder, $discount, $discount_amount);

        if ($add_to_file) {
            /** @var BankClientPaymentOrderFile $bankClientPaymentOrderFile */
            $bankClientPaymentOrderFile = $this->creteBankClientPaymentOrderFile($deal);
            $bankClientPaymentOrder->setBankClientPaymentOrderFile($bankClientPaymentOrderFile);
        }

        $this->entityManager->persist($bankClientPaymentOrder);
        $this->entityManager->flush();

        return $payment;
    }

    /**
     * @param Deal $deal
     * @param PaymentOrder $paymentOrder
     * @param Discount $discount
     * @param $discount_amount
     * @return BankClientPaymentOrder
     */
    private function createBankClientPaymentOrder(Deal $deal, PaymentOrder $paymentOrder, Discount $discount, $discount_amount)
    {
        // Назначение платежа
        $payment_purpose = $this->bankClientPaymentOrderManager->generatePayOffPaymentPurpose(
            $deal,
            PayOffManager::PURPOSE_TYPE_DISCOUNT,
            $discount_amount
        );

        // Создание $bankClientPaymentOrder
        $bankClientPaymentOrder = new BankClientPaymentOrder();
        $bankClientPaymentOrder->setCreated($discount->getCreated());
        $bankClientPaymentOrder->setType(PaymentOrderManager::TYPE_OUTGOING);
        $bankClientPaymentOrder->setPaymentOrder($paymentOrder);

        $bankClientPaymentOrder->setDocumentType('Платежное поручение');
        $bankClientPaymentOrder->setDocumentNumber(4);
        #$bankClientPaymentOrder->setDocumentDate($currentDate->modify('-8 days'));
        $bankClientPaymentOrder->setDocumentDate(date_format($discount->getCreated(), 'd.m.Y'));
        $bankClientPaymentOrder->setAmount($discount_amount);

        // @TODO Данные из Settings
        $bankClientPaymentOrder->setPayerAccount('40702810590030001259');
        $bankClientPaymentOrder->setPayer('ИНН 7813271867 ООО "Гарант Пэй"');
        $bankClientPaymentOrder->setPayerInn('7813271867');
        $bankClientPaymentOrder->setPayer1('ООО "Гарант Пэй"');
        $bankClientPaymentOrder->setPayerKpp('781301001');
        $bankClientPaymentOrder->setPayerBank1('ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" Г. САНКТ-ПЕТЕРБУРГ');
        $bankClientPaymentOrder->setPayerBik('044030790');
        $bankClientPaymentOrder->setPayerCorrAccount('30101810900000000790');

        /** @var PaymentMethod $recipientPaymentMethod */
        $recipientPaymentMethod = $discount->getPaymentMethod();
        /** @var PaymentMethodBankTransfer $recipientPaymentMethodBankTransfer */
        $recipientPaymentMethodBankTransfer = $recipientPaymentMethod->getPaymentMethodBankTransfer();
        /** @var CivilLawSubject $recipientCivilLawSubject */
        $recipientCivilLawSubject = $recipientPaymentMethod->getCivilLawSubject();
        /** @var NaturalPerson $recipientNaturalPerson */
        $recipientNaturalPerson = $recipientCivilLawSubject->getNaturalPerson();

        $bankClientPaymentOrder->setRecipientAccount($recipientPaymentMethodBankTransfer->getAccountNumber());
        $bankClientPaymentOrder->setRecipient($recipientNaturalPerson->getFirstName() . ' ' . $recipientNaturalPerson->getLastName());
        $bankClientPaymentOrder->setRecipientInn(null);
        $bankClientPaymentOrder->setRecipient1($recipientNaturalPerson->getFirstName() . ' ' . $recipientNaturalPerson->getLastName());
        $bankClientPaymentOrder->setRecipientKpp(null);
        $bankClientPaymentOrder->setRecipientBank1($recipientPaymentMethodBankTransfer->getBankName());
        $bankClientPaymentOrder->setRecipientBik($recipientPaymentMethodBankTransfer->getBik());
        $bankClientPaymentOrder->setRecipientCorrAccount($recipientPaymentMethodBankTransfer->getCorrAccountNumber());

        $bankClientPaymentOrder->setPaymentPurpose($payment_purpose);
        $bankClientPaymentOrder->setPaymentType('01');
        $bankClientPaymentOrder->setPriority('5');
        $bankClientPaymentOrder->setDateOfReceipt(date_format($discount->getCreated(), 'd.m.Y'));
        $bankClientPaymentOrder->setDateOfDebit(date_format($discount->getCreated(), 'd.m.Y'));
        $bankClientPaymentOrder->setHash('0faeda34e4c8d8c463d2e4644ba020a4'.$deal->getId());

        return $bankClientPaymentOrder;
    }

    /**
     * @param Deal $deal
     * @return BankClientPaymentOrderFile
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function creteBankClientPaymentOrderFile(Deal $deal)
    {
        $file = new File();
        $file->setCreated($this->currentDate);
        $file->setName('payment_order_for_deal_'.$deal->getId());
        $file->setType('txt');
        $file->setOriginName('payment_order_for_deal_'.$deal->getId());
        $file->setPath('/bank-client');
        $file->setSize(123456);

        $this->entityManager->persist($file);
        $this->entityManager->flush($file);

        $bankClientPaymentOrderFile = new BankClientPaymentOrderFile();
        $bankClientPaymentOrderFile->setFile($file);
        $bankClientPaymentOrderFile->setFileName($file->getName());
        $bankClientPaymentOrderFile->setHash(microtime());

        $this->entityManager->persist($bankClientPaymentOrderFile);
        $this->entityManager->flush($bankClientPaymentOrderFile);

        return $bankClientPaymentOrderFile;
    }

    /**
     * @return PaymentOrder
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createPaymentOrder()
    {
        /** @var PaymentMethodType $paymentMethodType */
        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(array('name' => PaymentMethodType::BANK_TRANSFER));

        $paymentOrderCreateDate = clone $this->currentDate;

        // Создаём PaymentOrder
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = new PaymentOrder();
        $paymentOrder->setCreated($paymentOrderCreateDate->modify('-18 days'));
        $paymentOrder->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentOrder);
        $this->entityManager->flush($paymentOrder);

        return $paymentOrder;
    }

    /**
     * @param float $amount
     * @param string $fee_payer
     * @return array
     */
    private function getDealValues(float $amount, string $fee_payer)
    {
        $fee = ($amount/100)*$this->percentOfFee;
        $values = [];
        if ($fee_payer === 'CUSTOMER') {
            $values['expected_value'] = $amount + $fee;
            $values['released_value'] = $amount;
        } elseif ($fee_payer === 'CONTRACTOR') {
            $values['expected_value'] = $amount;
            $values['released_value'] = $amount - $fee;
        } else {
            $values['expected_value'] = $amount + ($fee/2);
            $values['released_value'] = $amount - ($fee/2);
        }

        return $values;
    }

    private function setPercentOfFee()
    {
        /** @var GlobalSetting $feePercentage */
        $feePercentage = $this->entityManager->getRepository(GlobalSetting::class)
            ->findOneBy(array('name' => 'fee_percentage'));

        $this->percentOfFee = $feePercentage->getValue();
    }
}