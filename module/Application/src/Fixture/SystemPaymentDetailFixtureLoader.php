<?php

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\SystemPaymentDetails;

class SystemPaymentDetailFixtureLoader implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $paymentDetail = new SystemPaymentDetails();
        $paymentDetail->setName('ООО "Гарант Пэй"');
        $paymentDetail->setBank('ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" Г. САНКТ-ПЕТЕРБУРГ');
        $paymentDetail->setBik('044030790');
        $paymentDetail->setAccountNumber('40702810590030001259');
        $paymentDetail->setCorrAccountNumber('30101810900000000790');
        $paymentDetail->setPaymentRecipientName('ООО "Гарант Пэй"');
        $paymentDetail->setInn('7813271867');
        $paymentDetail->setKpp('781301001');
        $entityManager->persist($paymentDetail);

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 50;
    }
}