<?php

namespace Application\Fixture;

use Application\Entity\LegalEntity;
use Application\Entity\NdsType;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\CivilLawSubject;
use Application\Entity\User;
use Application\Entity\NaturalPerson;
use Application\Entity\SoleProprietor;

class CivilLawSubjectFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getOrder()
    {
        return 70;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $user1 = $entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test'));
        $user2 = $entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test2'));

        $ndsTypeWithoutNds = $entityManager->getRepository(NdsType::class)
            ->findOneBy(array('name' => 'без НДС'));

        $ndsType18 = $entityManager->getRepository(NdsType::class)
            ->findOneBy(array('name' => '18%'));

        $ndsType10 = $entityManager->getRepository(NdsType::class)
            ->findOneBy(array('name' => '10%'));

        /** @var NaturalPerson $naturalPerson_1 */
        $naturalPerson_1 = $entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Тест', 'lastName' => 'Тестов'));
        /** @var NaturalPerson $naturalPerson_2 */
        $naturalPerson_2 = $entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Контрагент', 'lastName' => 'Контрагентов'));

        /** @var LegalEntity $legalEntity_1 */
        $legalEntity_1 = $entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Колумбийский сахар'));
        /** @var LegalEntity $legalEntity_2 */
        $legalEntity_2 = $entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Simple Technology'));

        /** @var LegalEntity $legalEntity_3 */
        $legalEntity_3 = $entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Partner'));

        /** @var SoleProprietor $soleProprietor_1 */
        $soleProprietor_1 = $entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => 'ИП Месси Леонель Аргентинович'));
        /** @var SoleProprietor $soleProprietor_2 */
        $soleProprietor_2 = $entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => 'ИП Роналду Криштиану Португалович'));

        // 1
        $civilLawSubject_1 = new CivilLawSubject();
        $civilLawSubject_1->setUser($user1);
        $civilLawSubject_1->setIsPattern(true);
        $civilLawSubject_1->setCreated(new \DateTime());
        $civilLawSubject_1->setNaturalPerson($naturalPerson_1);

        $entityManager->persist($civilLawSubject_1);

        $naturalPerson_1->setCivilLawSubject($civilLawSubject_1);

        $entityManager->persist($naturalPerson_1);

        // 2
        $civilLawSubject_2 = new CivilLawSubject();
        $civilLawSubject_2->setUser($user1);
        $civilLawSubject_2->setIsPattern(true);
        $civilLawSubject_2->setCreated(new \DateTime());
        $civilLawSubject_2->setLegalEntity($legalEntity_1);
        $civilLawSubject_2->setNdsType($ndsTypeWithoutNds);

        $entityManager->persist($civilLawSubject_2);

        $legalEntity_1->setCivilLawSubject($civilLawSubject_2);

        $entityManager->persist($legalEntity_1);

        // 3
        $civilLawSubject_3 = new CivilLawSubject();
        $civilLawSubject_3->setUser($user2);
        $civilLawSubject_3->setIsPattern(true);
        $civilLawSubject_3->setCreated(new \DateTime());
        $civilLawSubject_3->setNaturalPerson($naturalPerson_2);
        #$civilLawSubject->addPaymentMethod($paymentMethod2);

        $entityManager->persist($civilLawSubject_3);

        $naturalPerson_2->setCivilLawSubject($civilLawSubject_3);

        $entityManager->persist($naturalPerson_2);

        // 4
        $civilLawSubject_4 = new CivilLawSubject();
        $civilLawSubject_4->setUser($user2);
        $civilLawSubject_4->setIsPattern(true);
        $civilLawSubject_4->setCreated(new \DateTime());
        $civilLawSubject_4->setLegalEntity($legalEntity_2);
        $civilLawSubject_4->setNdsType($ndsType18);

        $entityManager->persist($civilLawSubject_4);

        $legalEntity_2->setCivilLawSubject($civilLawSubject_4);

        $entityManager->persist($legalEntity_2);

        // 5
        $civilLawSubject_5 = new CivilLawSubject();
        $civilLawSubject_5->setUser($user1);
        $civilLawSubject_5->setIsPattern(true);
        $civilLawSubject_5->setCreated(new \DateTime());
        $civilLawSubject_5->setSoleProprietor($soleProprietor_1);
        $civilLawSubject_5->setNdsType($ndsType18);

        $entityManager->persist($civilLawSubject_5);

        $soleProprietor_1->setCivilLawSubject($civilLawSubject_5);

        $entityManager->persist($soleProprietor_1);

        // 6
        $civilLawSubject_6 = new CivilLawSubject();
        $civilLawSubject_6->setUser($user2);
        $civilLawSubject_6->setIsPattern(true);
        $civilLawSubject_6->setCreated(new \DateTime());
        $civilLawSubject_6->setSoleProprietor($soleProprietor_2);
        $civilLawSubject_6->setNdsType($ndsType10);

        $entityManager->persist($civilLawSubject_6);

        $soleProprietor_2->setCivilLawSubject($civilLawSubject_6);

        $entityManager->persist($soleProprietor_2);

        // 7 юрик партнер
        $civilLawSubject_7 = new CivilLawSubject();
        $civilLawSubject_7->setUser($user2);
        $civilLawSubject_7->setIsPattern(true);
        $civilLawSubject_7->setCreated(new \DateTime());
        $civilLawSubject_7->setLegalEntity($legalEntity_3);
        $civilLawSubject_7->setNdsType($ndsType18);

        $entityManager->persist($civilLawSubject_7);

        $legalEntity_3->setCivilLawSubject($civilLawSubject_7);

        $entityManager->persist($legalEntity_3);


        $entityManager->flush();
    }
}