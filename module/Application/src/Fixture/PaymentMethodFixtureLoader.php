<?php

namespace Application\Fixture;

use Application\Entity\LegalEntity;
use Application\Entity\NaturalPerson;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\PaymentMethod;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Application\Entity\User;
use Application\Entity\CivilLawSubject;
use Doctrine\ORM\EntityManager;

class PaymentMethodFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var CivilLawSubject
     */
    private $naturalPersonCivilLawSubject_1;

    /**
     * @var CivilLawSubject
     */
    private $naturalPersonCivilLawSubject_2;

    /**
     * @var CivilLawSubject
     */
    private $legalEntityCivilLawSubject_1;

    /**
     * @var CivilLawSubject
     */
    private $legalEntityCivilLawSubject_2;


    public function getOrder()
    {
        return 80;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
//        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');
//
//        // Set users to class vars $this->civilLawSubject1 и $this->civilLawSubject2
//        $this->setCivilLawSubjects();
//
//        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
//            ->findOneBy(array('name' => 'bank_transfer'));
//
//        // Для $setCivilLawSubject1 (User1)
//
//        $paymentMethod = new PaymentMethod();
//        $paymentMethod->setIsPattern(true);
//        $paymentMethod->setCivilLawSubject($this->naturalPersonCivilLawSubject_1);
//        $paymentMethod->setPaymentMethodType($paymentMethodType);
//
//        $this->entityManager->persist($paymentMethod);
//
//        $paymentMethod = new PaymentMethod();
//        $paymentMethod->setIsPattern(true);
//        $paymentMethod->setCivilLawSubject($this->naturalPersonCivilLawSubject_1);
//        $paymentMethod->setPaymentMethodType($paymentMethodType);
//
//        $this->entityManager->persist($paymentMethod);
//
//        // Для $setCivilLawSubject2 (User2) - Добавляем два PaymentMethod типа 'bank_transfer'
//
//        $paymentMethod = new PaymentMethod();
//        $paymentMethod->setIsPattern(true);
//        $paymentMethod->setCivilLawSubject($this->naturalPersonCivilLawSubject_2);
//        $paymentMethod->setPaymentMethodType($paymentMethodType);
//
//        $this->entityManager->persist($paymentMethod);
//
//        $paymentMethod = new PaymentMethod();
//        $paymentMethod->setIsPattern(true);
//        $paymentMethod->setCivilLawSubject($this->naturalPersonCivilLawSubject_2);
//        $paymentMethod->setPaymentMethodType($paymentMethodType);
//
//        $this->entityManager->persist($paymentMethod);
//
//        $this->entityManager->flush();
    }

    /**
     * Set civilLawSubjects
     */
    private function setCivilLawSubjects()
    {
        /** @var NaturalPerson $naturalPerson_1 */
        $naturalPerson_1 = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Тест', 'lastName' => 'Тестов'));
        /** @var NaturalPerson $naturalPerson_2 */
        $naturalPerson_2 = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Контрагент', 'lastName' => 'Контрагентов'));

        /** @var LegalEntity $legalEntity_1 */
        $legalEntity_1 = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Колумбийский сахар'));
        /** @var LegalEntity $legalEntity_2 */
        $legalEntity_2 = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Simple Technology'));

        $this->naturalPersonCivilLawSubject_1 = $naturalPerson_1->getCivilLawSubject();
        $this->naturalPersonCivilLawSubject_2 = $naturalPerson_2->getCivilLawSubject();

        $this->legalEntityCivilLawSubject_1 = $legalEntity_1->getCivilLawSubject();
        $this->legalEntityCivilLawSubject_2 = $legalEntity_2->getCivilLawSubject();
    }
}