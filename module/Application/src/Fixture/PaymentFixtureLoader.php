<?php

namespace Application\Fixture;

use Application\Entity\BankClientPaymentOrderFile;
use Application\Entity\DealAgent;
use Application\Service\InvoiceManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use ModuleAcquiringMandarin\Controller\CallbackHandleController;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use ModuleFileManager\Entity\File;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\GlobalSetting;
use Application\Entity\LegalEntity;
use Application\Entity\NaturalPerson;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\User;
use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\CivilLawSubject;
use Application\Entity\SystemPaymentDetails;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Controller\BankClientController;
use Application\Service\Parser\BankClientParser;
use Application\Service\BankClient\BankClientManager;
use Doctrine\ORM\EntityManager;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;

/**
 * Class PaymentFixtureLoader
 * @package Application\Fixture
 */
class PaymentFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var User
     */
    private $user1;

    /**
     * @var User
     */
    private $user2;

    /**
     * @var CivilLawSubject
     */
    private $naturalPersonCivilLawSubject_1;

    /**
     * @var CivilLawSubject
     */
    private $naturalPersonCivilLawSubject_2;

    /**
     * @var CivilLawSubject
     */
    private $legalEntityCivilLawSubject_1;

    /**
     * @var CivilLawSubject
     */
    private $legalEntityCivilLawSubject_2;

    /**
     * @var PaymentMethod
     */
    private $paymentMethod_1;

    /**
     * @var PaymentMethod
     */
    private $paymentMethod_2;

    /**
     * @var \DateTime
     */
    private $currentDate;

    /**
     * @var integer
     */
    private $percentOfFee;

    /**
     * @var BankClientPaymentOrderManager
     */
    private $bankClientPaymentOrderManager;

    /**
     * @var InvoiceManager
     */
    private $invoiceManager;


    /**
     * @return int
     */
    public function getOrder()
    {
        return 120;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function load(ObjectManager $manager)
    {
        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        $this->bankClientPaymentOrderManager = $this->container->get(BankClientPaymentOrderManager::class);
        $this->invoiceManager = $this->container->get(InvoiceManager::class);
        $this->currentDate   = new \DateTime();

        // Set users to class vars $this->user1 и $this->user2
        $this->setUsers();
        // Set users to class vars $this->civilLawSubject1 и $this->civilLawSubject2
        $this->setCivilLawSubjects();

        $this->setPaymentMethods();

        $this->setPercentOfFee();

        #var_dump('Start payment and deal relations creation');

        foreach (DealFixtureLoader::DEALS_IN_STATUS_NEGOTIATION as $deal_array) {
            /** @var Deal $deal */
            $deal = $this->entityManager->getRepository(Deal::class)
                ->findOneBy(array('name' => $deal_array['name']));
            /** @var Payment $payment */
            $payment = $this->createPayment($deal, $deal_array['amount'], $deal_array['fee_payer'], 'negotiation');

            $this->entityManager->persist($payment);
        }

        #var_dump('payment and relations for deals in status NEGOTIATION done');

        foreach (DealFixtureLoader::DEALS_IN_STATUS_CONFIRMED as $deal_array) {
            /** @var Deal $deal */
            $deal = $this->entityManager->getRepository(Deal::class)
                ->findOneBy(array('name' => $deal_array['name']));

            $not_generate_payment_order_file = $deal_array['not_add_incoming_payment_order_to_file'] ?? false;
            $partial_payment_amount = $deal_array['partial_payment_amount'] ?? null;
            $mandarin_payment_status = $deal_array['mandarin_payment_status'] ?? null;

            /** @var Payment $payment */
            $payment = $this->createPayment(
                $deal,
                $deal_array['amount'],
                $deal_array['fee_payer'],
                'confirmed',
                [],
                $not_generate_payment_order_file,
                $partial_payment_amount,
                $mandarin_payment_status
            );

            $this->entityManager->persist($payment);
        }

        #var_dump('payment and relations for deals in status CONFIRMED done');

        foreach (DealFixtureLoader::DEALS_IN_STATUS_DEBIT_MATCHED as $deal_array) {
            /** @var Deal $deal */
            $deal = $this->entityManager->getRepository(Deal::class)
                ->findOneBy(array('name' => $deal_array['name']));

            $not_generate_payment_order_file = $deal_array['not_add_incoming_payment_order_to_file'] ?? false;

            $mandarin_payment_status = $deal_array['mandarin_payment_status'] ?? null;

            /** @var Payment $payment */
            $payment = $this->createPayment(
                $deal,
                $deal_array['amount'],
                $deal_array['fee_payer'],
                'debit_matched',
                [],
                $not_generate_payment_order_file,
                null,
                $mandarin_payment_status
            );

            if (array_key_exists('deadline_days', $deal_array) && null !== $deal_array['deadline_days']) {
                $current_day = new \DateTime();

                $newDeFactoDate = $current_day->modify('-'.$deal->getDeliveryPeriod().' days');
                $newDeFactoDate->modify($deal_array['deadline_days'] . ' days');
                if ($deal_array['deadline_days'] === '+1') {
                    $newDeFactoDate->modify('-2 hours');
                }

                $payment->setDeFactoDate($newDeFactoDate);

                /** @var \DateTime $newCreated */
                $newCreated = clone $newDeFactoDate;

                $deal->setCreated($newCreated->modify('-2 days'));
                $deal->setDateOfConfirm($newCreated->modify('+1 days'));

                $this->entityManager->persist($deal);
            }

            $this->entityManager->persist($payment);
        }

        #var_dump('payment and relations for deals in status DEBIT_MATCHED done');

        foreach (DealFixtureLoader::DEALS_IN_STATUS_DELIVERED as $deal_array) {
            /** @var Deal $deal */
            $deal = $this->entityManager->getRepository(Deal::class)
                ->findOneBy(array('name' => $deal_array['name']));

            $not_generate_payment_order_file = false;

            /** @var Payment $payment */
            $payment = $this->createPayment(
                $deal,
                $deal_array['amount'],
                $deal_array['fee_payer'],
                'debit_matched',
                [],
                $not_generate_payment_order_file
            );

            $this->entityManager->persist($payment);

            $deal->setPayment($payment);

            $this->entityManager->persist($deal);

            $confirmation = false;
            $add_to_file = false;

            $this->createOutgoingPaymentOrder($deal, $payment->getReleasedValue(), $confirmation, $add_to_file);
        }

        #var_dump('payment and relations for deals in status DELIVERED done');

        foreach (DealFixtureLoader::DEALS_IN_STATUS_TROUBLE as $deal_array) {
            /** @var Deal $deal */
            $deal = $this->entityManager->getRepository(Deal::class)
                ->findOneBy(array('name' => $deal_array['name']));
            /** @var Payment $payment */
            $payment = $this->createPayment(
                $deal,
                $deal_array['amount'],
                $deal_array['fee_payer'],
                'trouble',
                $deal_array['trouble']);

            $this->entityManager->persist($payment);
        }

        #var_dump('payment and relations for deals in status TROUBLE done');

        foreach (DealFixtureLoader::DEALS_IN_STATUS_DISPUTE as $deal_array) {
            /** @var Deal $deal */
            $deal = $this->entityManager->getRepository(Deal::class)
                ->findOneBy(array('name' => $deal_array['name']));
            /** @var Payment $payment */
            $payment = $this->createPayment($deal, $deal_array['amount'], $deal_array['fee_payer'], 'debit_matched');

            $this->entityManager->persist($payment);
        }

        #var_dump('payment and relations for deals in status DISPUTE done');

        foreach (DealFixtureLoader::DEALS_IN_STATUS_PENDING_CREDIT_PAYMENT as $deal_array) {
            /** @var Deal $deal */
            $deal = $this->entityManager->getRepository(Deal::class)
                ->findOneBy(array('name' => $deal_array['name']));
            /** @var Payment $payment */
            $payment = $this->createPayment($deal, $deal_array['amount'], $deal_array['fee_payer'], 'debit_matched');

            $this->entityManager->persist($payment);

            $deal->setPayment($payment);

            $this->entityManager->persist($deal);

            $this->createOutgoingPaymentOrder($deal, $payment->getReleasedValue());
        }

        #var_dump('payment and relations for deals in status PENDING_CREDIT_PAYMENT done');

        foreach (DealFixtureLoader::DEALS_IN_STATUS_CLOSED as $deal_array) {
            /** @var Deal $deal */
            $deal = $this->entityManager->getRepository(Deal::class)
                ->findOneBy(array('name' => $deal_array['name']));
            /** @var Payment $payment */
            $payment = $this->createPayment($deal, $deal_array['amount'], $deal_array['fee_payer'], 'debit_matched');

            $this->entityManager->persist($payment);

            $deal->setPayment($payment);

            $this->entityManager->persist($deal);

            $this->createOutgoingPaymentOrder($deal, $payment->getReleasedValue());
            //подтверждаюшия выплату платежка
            $this->createOutgoingPaymentOrder($deal, $payment->getReleasedValue(), true);

        }

        #var_dump('payment and relations for deals in status CLOSED done');

        $this->entityManager->flush();

        // Crete PaymentOrder file
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => 'Сделка Фикстура'));
        $this->cretePaymentOrderFile($deal);
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @param $fee_payer
     * @param null $status
     * @param array $trouble_reasons
     * @param bool $not_generate_payment_order_file
     * @param null $partial_payment_amount
     * @param null $mandarin_payment_status
     * @return Payment
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createPayment(Deal $deal, $amount, $fee_payer, $status = null, $trouble_reasons = [],
                                   $not_generate_payment_order_file = false, $partial_payment_amount = null,
                                   $mandarin_payment_status = null)
    {
        /** @var array $deal_values */
        $deal_values = $this->getDealValues($amount, $fee_payer);

        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = ($deal->getContractor()->getUser() === $this->user2)
            ? $this->paymentMethod_2
            : $this->paymentMethod_1
        ;

        $payment = new Payment();
        $payment->setPaymentMethod($paymentMethod);
        $payment->setDeal($deal);
        $payment->setDealValue($amount);
        $payment->setExpectedValue($deal_values['expected_value']);
        $payment->setReleasedValue($deal_values['released_value']);
        $payment->setFee($this->percentOfFee);

        /** @var \DateTime $dateForPlannedDate */
        $dateForPlannedDate = clone $this->currentDate;
        $dateForPlannedDate->modify('+'.$deal->getDeliveryPeriod().' days');

        $payment->setPlannedDate($dateForPlannedDate);

        $this->entityManager->persist($payment);

        $this->entityManager->flush();

        if ($status === 'negotiation' || ($status === 'trouble' && isset($trouble_reasons['contract_file_missing']))) {

            return $payment;
        }

        // "Клонирование"

        $customer = $deal->getCustomer();
        $contractor = $deal->getContractor();

        // CivilLawSubject
        $customerCivilLawSubject = $customer->getCivilLawSubject();
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();

        // "клонируем" CivilLawSubjects
        /** @var CivilLawSubject $customerCivilLawSubjectClone */
        $customerCivilLawSubjectClone = $this->cloneCivilLawSubject($customerCivilLawSubject);
        /** @var CivilLawSubject $contractorCivilLawSubjectClone */
        $contractorCivilLawSubjectClone = $this->cloneCivilLawSubject($contractorCivilLawSubject);

        if (null !== $customerCivilLawSubject->getLegalEntity()) {
            /** @var LegalEntity $customerLegalEntityClone */
            $customerLegalEntityClone = $this->cloneLegalEntity(
                $customerCivilLawSubject->getLegalEntity(), // pattern
                $customerCivilLawSubjectClone // cloned
            );
            $this->entityManager->persist($customerLegalEntityClone);
        } else {
            /** @var NaturalPerson $customerNaturalPersonClone */
            $customerNaturalPersonClone = $this->cloneNaturalPerson(
                $customerCivilLawSubject->getNaturalPerson(), // pattern
                $customerCivilLawSubjectClone // cloned
            );
            $this->entityManager->persist($customerNaturalPersonClone);
        }
        if (null !== $contractorCivilLawSubject->getLegalEntity()) {
            /** @var LegalEntity $contractorLegalEntityClone */
            $contractorLegalEntityClone = $this->cloneLegalEntity(
                $contractorCivilLawSubject->getLegalEntity(), // pattern
                $contractorCivilLawSubjectClone // cloned
            );
            $this->entityManager->persist($contractorLegalEntityClone);
        } else {
            /** @var NaturalPerson $contractorNaturalPersonClone */
            $contractorNaturalPersonClone = $this->cloneNaturalPerson(
                $contractorCivilLawSubject->getNaturalPerson(), // pattern
                $contractorCivilLawSubjectClone // cloned
            );
            $this->entityManager->persist($contractorNaturalPersonClone);
        }

        $this->entityManager->persist($customerCivilLawSubjectClone);
        $this->entityManager->persist($contractorCivilLawSubjectClone);

        /** @var PaymentMethod $paymentMethod */
        $paymentMethodClone = $this->clonePaymentMethod($paymentMethod, $contractorCivilLawSubjectClone);

        $contractorCivilLawSubjectClone->addPaymentMethod($paymentMethodClone);

        $payment->setPaymentMethod($paymentMethodClone);

        $deal->setPayment($payment);

        $this->entityManager->persist($payment);
        $this->entityManager->persist($deal);
        $this->entityManager->flush();

        $contractor->setCivilLawSubject($contractorCivilLawSubjectClone);
        $customer->setCivilLawSubject($customerCivilLawSubjectClone);

        $this->entityManager->persist($contractor);
        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        if ($status !== 'confirmed' || null !== $partial_payment_amount) {

            if (null === $partial_payment_amount) {

                $payment->setDeFactoDate(new \DateTime());
            }

            if (null !== $mandarin_payment_status) {
                /** @var PaymentOrder $paymentOrder */
                $paymentOrder = $this->createPaymentOrder(PaymentMethodType::ACQUIRING_MANDARIN);
                /** @var MandarinPaymentOrder $mandarinPaymentOrder */
                $mandarinPaymentOrder = $this->createIncomingMandarinPaymentOrder($deal, $mandarin_payment_status, $payment->getExpectedValue());

                $mandarinPaymentOrder->setPaymentOrder($paymentOrder);

                $this->entityManager->persist($mandarinPaymentOrder);

            } else {

                $bankClientPaymentOrder_amount = $partial_payment_amount ?? $payment->getExpectedValue();
                if ($status === 'trouble') {
                    if (isset($trouble_reasons['overpayment'])) {
                        $bankClientPaymentOrder_amount = $deal_values['expected_value'] + $trouble_reasons['overpayment'];
                    }
                }
                /** @var PaymentOrder $paymentOrder */
                $paymentOrder = $this->createPaymentOrder(PaymentMethodType::BANK_TRANSFER);
                /** @var BankClientPaymentOrder $bankClientPaymentOrder */
                $bankClientPaymentOrder = $this->createIncomingBankClientPaymentOrder(
                    $deal,
                    $bankClientPaymentOrder_amount,
                    $not_generate_payment_order_file
                );

                $bankClientPaymentOrder->setPaymentOrder($paymentOrder);

                $this->entityManager->persist($bankClientPaymentOrder);
            }

            $payment->addPaymentOrder($paymentOrder);

            $this->entityManager->persist($payment);
            $this->entityManager->flush();

            if (null === $partial_payment_amount) {
                // Метод оплаты Кастомера
                $customerPaymentMethod = new PaymentMethod;
                $customerPaymentMethod->setCivilLawSubject($customerCivilLawSubjectClone);
                $customerPaymentMethod->setIsPattern(false);
                $customerPaymentMethod->setPaymentMethodType($paymentMethodClone->getPaymentMethodType());

                $paymentMethodBankTransfer = new PaymentMethodBankTransfer();

                $paymentMethodBankTransfer->setName('Счет плательщика по сделке ' . $deal->getNumber());
                $paymentMethodBankTransfer->setBankName('АО "РАЙФФАЙЗЕНБАНК"');
                $paymentMethodBankTransfer->setAccountNumber('40702810900000027373');
                $paymentMethodBankTransfer->setCorrAccountNumber('30101810200000000700');
                $paymentMethodBankTransfer->setBik('044525700');
                $paymentMethodBankTransfer->setPaymentMethod($customerPaymentMethod);

                $customerPaymentMethod->setPaymentMethodBankTransfer($paymentMethodBankTransfer);

                $customerCivilLawSubjectClone->addPaymentMethod($customerPaymentMethod);

                $this->entityManager->persist($customerPaymentMethod);
                $this->entityManager->persist($paymentMethodBankTransfer);
                $this->entityManager->persist($customerCivilLawSubjectClone);

                $this->entityManager->flush();
            }
        }

        return $payment;
    }

    /**
     * @param Deal $deal
     * @param string $mandarin_payment_status
     * @return MandarinPaymentOrder
     *
     * @throws \Exception
     */
    private function createIncomingMandarinPaymentOrder(Deal $deal, string $mandarin_payment_status, $amount): MandarinPaymentOrder
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        $order_id = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZWFsX2lkIjo0OTYG9uI309dCPrc2PMIlxc'.$deal->getId(); // Пока произвольный набор символов

        $payment_purpose = $this->invoiceManager->generateInvoicePaymentPurpose($deal, true);

        $mandarinPaymentOrder = new MandarinPaymentOrder();
        $mandarinPaymentOrder->setType(PaymentOrderManager::TYPE_INCOMING);
        $mandarinPaymentOrder->setOrderId($order_id);
        $mandarinPaymentOrder->setAmount($amount);
        $mandarinPaymentOrder->setAction(CallbackHandleController::ACTION_FOR_PAYMENT);
        $mandarinPaymentOrder->setStatus($mandarin_payment_status);
        $mandarinPaymentOrder->setPaymentPurpose($payment_purpose);
        $mandarinPaymentOrder->setCreated(new \DateTime());

        if ($mandarin_payment_status !== MandarinPaymentOrderManager::STATUS_PROCESSING) {
            $mandarinPaymentOrder->setTransaction('7dsldfn56vglsntk59'); // Произвольный набор
            $mandarinPaymentOrder->setCustomerEmail($customer->getEmail());
            $mandarinPaymentOrder->setCustomerPhone('+'.$customer->getUser()->getPhone()->getPhoneNumber());
            $mandarinPaymentOrder->setCardHolder('CARD HOLDER');
            $mandarinPaymentOrder->setCardNumber('4929509947106878');
            $mandarinPaymentOrder->setCardExpirationYear('25');
            $mandarinPaymentOrder->setCardExpirationMonth('12');
        }

        return $mandarinPaymentOrder;
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @param bool $not_generate_payment_order_file
     * @return BankClientPaymentOrder
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @throws \Exception
     */
    private function createIncomingBankClientPaymentOrder(Deal $deal, $amount, bool $not_generate_payment_order_file): BankClientPaymentOrder
    {
        $payment_purpose = $this->invoiceManager->generateInvoicePaymentPurpose($deal, true);

        // Создание $bankClientPaymentOrder
        $bankClientPaymentOrder = new BankClientPaymentOrder();
        $bankClientPaymentOrder->setCreated($this->currentDate);
        $bankClientPaymentOrder->setType(PaymentOrderManager::TYPE_INCOMING);

        $creationDate = clone $deal->getDateOfConfirm();
        $creationDate->modify('+2 seconds');

        $bankClientPaymentOrder->setDocumentType('Платежное поручение');
        $bankClientPaymentOrder->setDocumentNumber($deal->getId());
        $bankClientPaymentOrder->setDocumentDate(date_format($creationDate, 'd.m.Y'));
        $bankClientPaymentOrder->setAmount((float)$amount);
        $bankClientPaymentOrder->setRecipientAccount('40702810590030001259');
        $bankClientPaymentOrder->setRecipient('ИНН 7813271867 ООО "Гарант Пэй"');
        $bankClientPaymentOrder->setRecipientInn('7813271867');
        $bankClientPaymentOrder->setRecipient1('ООО "Гарант Пэй"');
        $bankClientPaymentOrder->setRecipientKpp('781301001');
        $bankClientPaymentOrder->setRecipientBank1('ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" Г. САНКТ-ПЕТЕРБУРГ');
        $bankClientPaymentOrder->setRecipientBik('044030790');
        $bankClientPaymentOrder->setRecipientCorrAccount('30101810900000000790');
        $bankClientPaymentOrder->setPayerAccount('40702810900000027373');
        $bankClientPaymentOrder->setPayer('ИНН 4217030520 Test');
        $bankClientPaymentOrder->setPayerInn('4217030520');
        $bankClientPaymentOrder->setPayer1('Test');
        #$bankClientPaymentOrder->setPayerKpp('325701001');
        $bankClientPaymentOrder->setPayerKpp(null);
        $bankClientPaymentOrder->setPayerBank1('АО "РАЙФФАЙЗЕНБАНК"');
        $bankClientPaymentOrder->setPayerBik('044525700');
        $bankClientPaymentOrder->setPayerCorrAccount('30101810200000000700');
        $bankClientPaymentOrder->setPaymentPurpose($payment_purpose);
        $bankClientPaymentOrder->setPaymentType('01');
        $bankClientPaymentOrder->setPriority('5');
        $bankClientPaymentOrder->setDateOfReceipt(date_format($creationDate, 'd.m.Y'));
        $bankClientPaymentOrder->setDateOfDebit(date_format($creationDate, 'd.m.Y'));
        $bankClientPaymentOrder->setHash('0faeda34e4c8d8c463d2e4644ba020a4'.$deal->getId());
        /** @var BankClientPaymentOrderFile $bankClientPaymentOrderFile */
        if(!$not_generate_payment_order_file) {
            $bankClientPaymentOrderFile = $this->creteBankClientPaymentOrderFile($deal);
            $bankClientPaymentOrder->setBankClientPaymentOrderFile($bankClientPaymentOrderFile);
        }

        return $bankClientPaymentOrder;
    }

    /**
     * @param PaymentMethod $patternPaymentMethod
     * @param CivilLawSubject $civilLawSubject
     * @return PaymentMethod
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function clonePaymentMethod(PaymentMethod $patternPaymentMethod, CivilLawSubject $civilLawSubject)
    {
        $paymentMethod = new PaymentMethod();
        $paymentMethod->setIsPattern(false);
        $paymentMethod->setCivilLawSubject($civilLawSubject);
        $paymentMethod->setPaymentMethodType($patternPaymentMethod->getPaymentMethodType());

        if (null !== $patternPaymentMethod->getPaymentMethodBankTransfer()) {
            /** @var PaymentMethodBankTransfer $patternPaymentMethodBankTransfer */
            $patternPaymentMethodBankTransfer = $patternPaymentMethod->getPaymentMethodBankTransfer();

            $paymentMethodBankTransfer = new PaymentMethodBankTransfer();
            $paymentMethodBankTransfer->setName($patternPaymentMethodBankTransfer->getName());
            $paymentMethodBankTransfer->setBankName($patternPaymentMethodBankTransfer->getBankName());
            $paymentMethodBankTransfer->setAccountNumber($patternPaymentMethodBankTransfer->getAccountNumber());
            $paymentMethodBankTransfer->setBik($patternPaymentMethodBankTransfer->getBik());
            $paymentMethodBankTransfer->setCorrAccountNumber($patternPaymentMethodBankTransfer->getCorrAccountNumber());

            $this->entityManager->persist($paymentMethodBankTransfer);
            $this->entityManager->persist($paymentMethod);

            $paymentMethod->setPaymentMethodBankTransfer($paymentMethodBankTransfer);
            $paymentMethodBankTransfer->setPaymentMethod($paymentMethod);

            $this->entityManager->persist($paymentMethodBankTransfer);
            $this->entityManager->persist($paymentMethod);

            $this->entityManager->flush();

            return $paymentMethod;
        }

        // @TODO Если другой метод оплаты

        return null;
    }

    /**
     * @param NaturalPerson $patternNaturalPerson
     * @param CivilLawSubject $civilLawSubject
     * @return NaturalPerson
     */
    private function cloneNaturalPerson(NaturalPerson $patternNaturalPerson, CivilLawSubject $civilLawSubject)
    {
        $naturalPerson = new NaturalPerson();
        $naturalPerson->setBirthDate($patternNaturalPerson->getBirthDate());
        $naturalPerson->setCivilLawSubject($civilLawSubject);
        $naturalPerson->setFirstName($patternNaturalPerson->getFirstName());
        $naturalPerson->setSecondaryName($patternNaturalPerson->getSecondaryName());
        $naturalPerson->setLastName($patternNaturalPerson->getLastName());

        return $naturalPerson;
    }

    /**
     * @param LegalEntity $patternLegalEntity
     * @param CivilLawSubject $civilLawSubject
     * @return LegalEntity
     */
    private function cloneLegalEntity(LegalEntity $patternLegalEntity, CivilLawSubject $civilLawSubject)
    {
        $legalEntity = new LegalEntity();
        $legalEntity->setCivilLawSubject($civilLawSubject);
        $legalEntity->setName($patternLegalEntity->getName());
        $legalEntity->setInn($patternLegalEntity->getInn());
        $legalEntity->setLegalAddress($patternLegalEntity->getLegalAddress());
        $legalEntity->setKpp($patternLegalEntity->getKpp());

        return $legalEntity;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @return CivilLawSubject
     */
    private function cloneCivilLawSubject(CivilLawSubject $civilLawSubject)
    {
        $cloneCivilLawSubject = new CivilLawSubject();

        $cloneCivilLawSubject->setIsPattern(false);
        $cloneCivilLawSubject->setUser($civilLawSubject->getUser());
        $cloneCivilLawSubject->setCreated($civilLawSubject->getCreated());
        $cloneCivilLawSubject->setNdsType($civilLawSubject->getNdsType());

        return $cloneCivilLawSubject;
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @param bool $confirmation
     * @param bool $add_to_file
     * @return PaymentOrder
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    private function createOutgoingPaymentOrder(Deal $deal, $amount, $confirmation = false, $add_to_file = true)
    {
        // Назначение платежа
        $payment_purpose = $this->bankClientPaymentOrderManager->generatePayOffPaymentPurpose(
            $deal,
            PayOffManager::PURPOSE_TYPE_PAYOFF,
            $amount
        );

        /** @var \DateTime $dateCreation */
        $dateCreation = clone $deal->getDateOfConfirm();
        $dateCreation->modify('+'.$deal->getDeliveryPeriod().' days');
        $dateCreation->modify('+1 days');

        /**
         * @var Payment $payment
         * @var PaymentMethod $paymentMethod
         * @var PaymentMethodBankTransfer $paymentMethodBankTransfer
         * @var PaymentOrder $paymentOrder
         */
        $payment = $deal->getPayment();
        $paymentMethod = $payment->getPaymentMethod();
        $paymentMethodBankTransfer = $paymentMethod->getPaymentMethodBankTransfer();
        $paymentOrder = $this->createPaymentOrder(PaymentMethodType::BANK_TRANSFER);
        // Создание $bankClientPaymentOrder
        $bankClientPaymentOrder = new BankClientPaymentOrder();
        $bankClientPaymentOrder->setCreated($dateCreation);
        $bankClientPaymentOrder->setType($confirmation ? PaymentOrderManager::TYPE_INCOMING : PaymentOrderManager::TYPE_OUTGOING);
        $bankClientPaymentOrder->setPaymentOrder($paymentOrder);

        $bankClientPaymentOrder->setDocumentType('Платежное поручение');
        $bankClientPaymentOrder->setDocumentNumber(substr(microtime(), -5));
        $bankClientPaymentOrder->setDocumentDate(date_format($dateCreation, 'd.m.Y'));
        $bankClientPaymentOrder->setAmount($amount);
        $bankClientPaymentOrder->setPayerAccount('40702810590030001259');
        $bankClientPaymentOrder->setPayer('ИНН 7813271867 ООО "Гарант Пэй"');
        $bankClientPaymentOrder->setPayerInn('7813271867');
        $bankClientPaymentOrder->setPayer1('ООО "Гарант Пэй"');
        $bankClientPaymentOrder->setPayerKpp('781301001');
        $bankClientPaymentOrder->setPayerBank1('ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" Г. САНКТ-ПЕТЕРБУРГ');
        $bankClientPaymentOrder->setPayerBik('044030790');
        $bankClientPaymentOrder->setPayerCorrAccount('30101810900000000790');

        $bankClientPaymentOrder->setRecipientAccount($paymentMethodBankTransfer->getAccountNumber());
        $bankClientPaymentOrder->setRecipient('Test Testov');
        $bankClientPaymentOrder->setRecipientInn('4217030520');
        $bankClientPaymentOrder->setRecipient1('Test Testov');
        $bankClientPaymentOrder->setRecipientKpp(null);
        $bankClientPaymentOrder->setRecipientBank1($paymentMethodBankTransfer->getBankName());
        $bankClientPaymentOrder->setRecipientBik($paymentMethodBankTransfer->getBik());
        $bankClientPaymentOrder->setRecipientCorrAccount($paymentMethodBankTransfer->getCorrAccountNumber());

        $bankClientPaymentOrder->setPaymentPurpose($payment_purpose);
        $bankClientPaymentOrder->setPaymentType('01');
        $bankClientPaymentOrder->setPriority('5');
        $bankClientPaymentOrder->setDateOfReceipt(date_format($dateCreation, 'd.m.Y'));
        $bankClientPaymentOrder->setDateOfDebit(date_format($dateCreation, 'd.m.Y'));
        $bankClientPaymentOrder->setHash(substr(microtime(), -5));

        if ($add_to_file) {
            /** @var BankClientPaymentOrderFile $bankClientPaymentOrderFile */
            $bankClientPaymentOrderFile = $this->creteBankClientPaymentOrderFile($deal);
            $bankClientPaymentOrder->setBankClientPaymentOrderFile($bankClientPaymentOrderFile);
        }
        $bankClientPaymentOrder->setPaymentOrder($paymentOrder);

        $this->entityManager->persist($bankClientPaymentOrder);

        $paymentOrder->setBankClientPaymentOrder($bankClientPaymentOrder);
        $payment->addPaymentOrder($paymentOrder);

        $this->entityManager->persist($paymentOrder);
        $this->entityManager->persist($payment);

        $this->entityManager->flush();

        return $paymentOrder;
    }

    /**
     * @param Deal $deal
     * @return BankClientPaymentOrderFile
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function creteBankClientPaymentOrderFile(Deal $deal)
    {
        $file = new File();
        $file->setCreated($this->currentDate);
        $file->setName('payment_order_for_deal_'.$deal->getId());
        $file->setType('txt');
        $file->setOriginName('payment_order_for_deal_'.$deal->getId());
        $file->setPath('/bank-client');
        $file->setSize(123456);

        $this->entityManager->persist($file);
        $this->entityManager->flush($file);

        $bankClientPaymentOrderFile = new BankClientPaymentOrderFile();
        $bankClientPaymentOrderFile->setFile($file);
        $bankClientPaymentOrderFile->setFileName($file->getName());
        $bankClientPaymentOrderFile->setHash(microtime());

        $this->entityManager->persist($bankClientPaymentOrderFile);
        $this->entityManager->flush($bankClientPaymentOrderFile);

        return $bankClientPaymentOrderFile;
    }

    /**
     * @param string $payment_method_type
     * @return PaymentOrder
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createPaymentOrder(string $payment_method_type): PaymentOrder
    {
        /** @var PaymentMethodType $paymentMethodType */
        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(array('name' => $payment_method_type));

        $paymentOrderCreateDate = clone $this->currentDate;

        // Создаём PaymentOrder
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = new PaymentOrder();
        $paymentOrder->setCreated($paymentOrderCreateDate->modify('-18 days'));
        $paymentOrder->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentOrder);
        $this->entityManager->flush($paymentOrder);

        return $paymentOrder;
    }

    /**
     * @param float $amount
     * @param string $fee_payer
     * @return array
     */
    private function getDealValues(float $amount, string $fee_payer)
    {
        $fee = ($amount/100)*$this->percentOfFee;
        $values = [];
        if ($fee_payer === 'CUSTOMER') {
            $values['expected_value'] = $amount + $fee;
            $values['released_value'] = $amount;
        } elseif ($fee_payer === 'CONTRACTOR') {
            $values['expected_value'] = $amount;
            $values['released_value'] = $amount - $fee;
        } else {
            $values['expected_value'] = $amount + ($fee/2);
            $values['released_value'] = $amount - ($fee/2);
        }

        return $values;
    }

    /**
     * Set users
     */
    private function setUsers()
    {
        $this->user1 = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test'));

        $this->user2 = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test2'));
    }

    private function setPercentOfFee()
    {
        /** @var GlobalSetting $feePercentage */
        $feePercentage = $this->entityManager->getRepository(GlobalSetting::class)
            ->findOneBy(array('name' => 'fee_percentage'));

        $this->percentOfFee = $feePercentage->getValue();
    }

    /**
     * Set civilLawSubjects
     */
    private function setCivilLawSubjects()
    {
        /** @var NaturalPerson $naturalPerson_1 */
        $naturalPerson_1 = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Тест', 'lastName' => 'Тестов'));
        /** @var NaturalPerson $naturalPerson_2 */
        $naturalPerson_2 = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Контрагент', 'lastName' => 'Контрагентов'));

        /** @var LegalEntity $legalEntity_1 */
        $legalEntity_1 = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Колумбийский сахар'));
        /** @var LegalEntity $legalEntity_2 */
        $legalEntity_2 = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Simple Technology'));

        $this->naturalPersonCivilLawSubject_1 = $naturalPerson_1->getCivilLawSubject();
        $this->naturalPersonCivilLawSubject_2 = $naturalPerson_2->getCivilLawSubject();

        $this->legalEntityCivilLawSubject_1 = $legalEntity_1->getCivilLawSubject();
        $this->legalEntityCivilLawSubject_2 = $legalEntity_2->getCivilLawSubject();
    }

    private function setPaymentMethods()
    {
        /** @var PaymentMethodBankTransfer $paymentMethodBankTransfer_1 */
        $paymentMethodBankTransfer_1 = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1));

        $this->paymentMethod_1 = $paymentMethodBankTransfer_1->getPaymentMethod();

        /** @var PaymentMethodBankTransfer $paymentMethodBankTransfer_2 */
        $paymentMethodBankTransfer_2 = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_2));

        $this->paymentMethod_2 = $paymentMethodBankTransfer_2->getPaymentMethod();
    }

    /**
     * Создание файла платёжки по имеющейся сделке "Сделка Фикстура".
     * Метод в этой фикстуре потому, что Payment по сделке создается в последнюю очередь после всех необходимых объектов.
     * Объект Deal появляется только к моменту создания Payment.
     *
     * @param Deal $deal
     */
    private function cretePaymentOrderFile(Deal $deal)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        /** @var Payment $payment */
        $payment = $entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        /** @var NaturalPerson $customerNaturalPerson */
        $customerNaturalPerson = $entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Тест', 'lastName' => 'Тестов'));

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $customerNaturalPerson->getCivilLawSubject();

        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $civilLawSubject->getPaymentMethods()->first();

        /** @var PaymentMethodBankTransfer $customerBankTransfer */
        $customerBankTransfer = $paymentMethod->getPaymentMethodBankTransfer();

        /** @var SystemPaymentDetails $systemPaymentDetails */ // Реквизиты GP
        $systemPaymentDetails = $entityManager->getRepository(SystemPaymentDetails::class)
            ->findOneBy(array('name' => 'ООО "Гарант Пэй"'));

        // Full path to file
        $path = "./module/Application/test/files/payment_order_from_fixture.txt";

        // Remove if file exists
        if (file_exists($path)) {
            unlink($path);
        }

        // Creates file if not exists
        $file_with_payment_order = fopen($path, "w") or die("Не удается создать файл");
        // Write file header data
        fwrite($file_with_payment_order, mb_convert_encoding(
                BankClientManager::FILE_HEADER,
                BankClientController::ORIGINAL_ENCODING, 'UTF-8')
        );
        // Main body of PaymentOrder

        // @TODO Переделать Плательщик, ПлательщикИНН, Плательщик1

        $currentDate = new \DateTime();
        $txt = "СекцияДокумент=".BankClientManager::DOCUMENT_TYPE.PHP_EOL;
        $txt .= "Номер=".$deal->getId().PHP_EOL;
        $txt .= "Дата=".$currentDate->format('d.m.Y').PHP_EOL;
        $txt .= "ДатаСписано=".$currentDate->format('d.m.Y').PHP_EOL;
        $txt .= "Сумма=".$payment->getExpectedValue().PHP_EOL;
        $txt .= "ПлательщикСчет=".$customerBankTransfer->getAccountNumber().PHP_EOL;
        $txt .= "Плательщик=ИНН ".$customerNaturalPerson->getFirstName(). " ". $customerNaturalPerson->getLastName().PHP_EOL;
        $txt .= "ПлательщикИНН=".PHP_EOL;
        $txt .= "Плательщик1=".$customerNaturalPerson->getFirstName(). " ". $customerNaturalPerson->getLastName().PHP_EOL;
        $txt .= "ПлательщикКПП=".PHP_EOL;
        $txt .= "ПлательщикБанк1=".$customerBankTransfer->getBankName().PHP_EOL; // $customerBankTransfer->getBankName() - когда добавим свойство
        $txt .= "ПлательщикБИК=".$customerBankTransfer->getBik().PHP_EOL;
        $txt .= "ПлательщикКорсчет=".$customerBankTransfer->getCorrAccountNumber().PHP_EOL;
        $txt .= "ПолучательСчет=".$systemPaymentDetails->getAccountNumber().PHP_EOL;
        $txt .= "Получатель=ИНН ".$systemPaymentDetails->getInn()." ".$systemPaymentDetails->getPaymentRecipientName().PHP_EOL;
        $txt .= "ПолучательИНН=".$systemPaymentDetails->getInn().PHP_EOL;
        $txt .= "Получатель1=".$systemPaymentDetails->getPaymentRecipientName().PHP_EOL;
        $txt .= "ПолучательКПП=".$systemPaymentDetails->getKpp().PHP_EOL;
        $txt .= "ПолучательБанк1=".$systemPaymentDetails->getBank().PHP_EOL;
        $txt .= "ПолучательБИК=".$systemPaymentDetails->getBik().PHP_EOL;
        $txt .= "ПолучательКорсчет=".$systemPaymentDetails->getCorrAccountNumber().PHP_EOL;
        $txt .= "НазначениеПлатежа=Оплата по сделке ".$deal->getNumber().PHP_EOL;
        $txt .= "ВидОплаты=01".PHP_EOL;
        $txt .= "Очередность=5".PHP_EOL;
        // Write main body
        fwrite($file_with_payment_order, mb_convert_encoding($txt, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
        // Write SECTION_END_POINT
        fwrite($file_with_payment_order, mb_convert_encoding(BankClientParser::SECTION_END_POINT.PHP_EOL,
            BankClientController::ORIGINAL_ENCODING, 'UTF-8'));

        // Дубликат платёжки в другом файле для тестирования повторной выгрузки
        $path_for_repeat = "./module/Application/test/files/payment_order_from_fixture_repeat.txt";

        // Remove if file exists
        if (file_exists($path_for_repeat)) {
            unlink($path_for_repeat);
        }

        // Creates file if not exists
        $file_with_payment_order_repeat = fopen($path_for_repeat, "w") or die("Не удается создать файл");
        // Write file header data
        fwrite($file_with_payment_order_repeat, mb_convert_encoding(
                BankClientManager::FILE_HEADER,
                BankClientController::ORIGINAL_ENCODING, 'UTF-8')
        );
        // Write main body
        fwrite($file_with_payment_order_repeat, mb_convert_encoding($txt, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
        // Write SECTION_END_POINT
        fwrite($file_with_payment_order_repeat, mb_convert_encoding(BankClientParser::SECTION_END_POINT.PHP_EOL,
            BankClientController::ORIGINAL_ENCODING, 'UTF-8'));

        fclose($file_with_payment_order);
        fclose($file_with_payment_order_repeat);
    }
}