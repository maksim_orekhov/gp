<?php

namespace Application\Fixture;

use Application\Entity\CivilLawSubject;
use Application\Entity\DealContractFile;
use Application\Entity\Delivery;
use Application\Entity\LegalEntity;
use Application\Entity\NaturalPerson;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\User;
use Application\Entity\DealAgent;
use Application\Entity\Deal;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Doctrine\ORM\EntityManager;
use ModuleFileManager\Entity\File;
use ModuleMessage\Entity\Message;

class DealFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const DEAL_DEADLINE_EXPIRED_IN_ONE_DAY_NAME = 'Сделка Deadline one day';
    const DEAL_BETWEEN_LEGAL_ENTITIES_NAME = 'Сделка Между юридическими лицами';
    const DEAL_DISCOUNT_REQUEST_CONFIRMED = 'Сделка Предложение скидки принято';

    const DEALS_IN_STATUS_NEGOTIATION = [ // 'amount' используется классом PaymentFixtureLoader
        ['name' => 'Сделка Фикстура', 'amount' => 100000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => false],
        ['name' => 'Сделка Negotiation', 'amount' => 300000, 'type' => 'U', 'fee_payer' => 'CONTRACTOR',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => false],
        ['name' => 'Сделка Переговорная', 'amount' => 850000, 'type' => 'T', 'fee_payer' => '50/50',
            'main_test_user_is' => 'customer', 'customer_confirm' => false, 'contractor_confirm' => true],
        ['name' => 'Сделка с приглашением', 'amount' => 50000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'contractor', 'customer_confirm' => false, 'contractor_confirm' => false,
            'partner_invited' => true],
        ['name' => 'Сделка с комментариями', 'amount' => 52000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => false,
            'comments' => [
                ['author' => 'operator', 'text' => 'Как теперь быть?'],
                ['author' => 'operator', 'text' => 'Не раскачивайте яхту.'],
                ['author' => 'operator', 'text' => 'Или может я не прав?'],
                ['author' => 'operator', 'text' => 'Хоспади, да когда же все это кончится, ъ?'],
                ['author' => 'operator', 'text' => 'Да, да. При Сталине мобильных телефонов не было.'],
                ['author' => 'operator', 'text' => 'А если это кто-то восприймет всерьез?'],
                ['author' => 'operator', 'text' => 'Куда я попал? И где мои вещи...'],
            ],
        ]
    ];

    const DEALS_IN_STATUS_CONFIRMED = [ // 'amount' используется классом PaymentFixtureLoader
        ['name' => 'Сделка Соглашение достигнуто', 'amount' => 100000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true],
        ['name' => 'Сделка Частично оплачено', 'amount' => 110000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'partial_payment_amount' => 50000
        ],
        ['name' => 'Сделка Оплата Мандарин failed', 'amount' => 230000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'not_add_incoming_payment_order_to_file' => true,
            'mandarin_payment_status' => 'failed'
        ],
        ['name' => 'Сделка Оплата Мандарин processing', 'amount' => 240000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'not_add_incoming_payment_order_to_file' => true,
            'mandarin_payment_status' => 'processing'
        ],
        ['name' => self::DEAL_BETWEEN_LEGAL_ENTITIES_NAME, 'amount' => 250000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'not_add_incoming_payment_order_to_file' => true
        ],
    ];

    const DEALS_IN_STATUS_DEBIT_MATCHED = [ // 'amount' используется классом PaymentFixtureLoader
        ['name' => 'Сделка Оплаченная', 'amount' => 200000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true
        ],
        ['name' => 'Сделка Debit Matched', 'amount' => 46000, 'type' => 'T', 'fee_payer' => 'CONTRACTOR',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true
        ],
        ['name' => 'Сделка "Waiting for delivery confirm"', 'amount' => 980000, 'type' => 'T', 'fee_payer' => '50/50',
            'main_test_user_is' => 'contractor', 'customer_confirm' => true, 'contractor_confirm' => true
        ],
        ['name' => 'Сделка Проблемная платежка', 'amount' => 50000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'not_add_incoming_payment_order_to_file' => true
        ],
        ['name' => self::DEAL_DEADLINE_EXPIRED_IN_ONE_DAY_NAME, 'amount' => 210000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'deadline_days' => '+1' // Добавится к DeFactoDate
        ],
        ['name' => 'Сделка Deadline expired', 'amount' => 215000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'deadline_days' => '-1' // DeFactoDate на день раньше
        ],
        ['name' => 'Сделка Оплата Мандарин success', 'amount' => 220000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'not_add_incoming_payment_order_to_file' => true,
            'mandarin_payment_status' => 'success'
        ],
    ];

    const DEALS_IN_STATUS_DELIVERED = [ // 'amount' используется классом PaymentFixtureLoader
        ['name' => 'Сделка Доставка подтверждена', 'amount' => 560000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'delivered' => true],
    ];

    const DEALS_IN_STATUS_TROUBLE = [ // 'amount' используется классом PaymentFixtureLoader
        ['name' => 'Сделка Переплата', 'amount' => 200000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'trouble' => [
                'overpayment' => 222
            ]
        ],
        ['name' => 'Сделка Без контракта', 'amount' => 250000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'trouble' => [
                'contract_file_missing' => true
            ],
        ]
    ];

    const DEALS_IN_STATUS_DISPUTE = [ // используется также классом DisputeFixtureLoader
        // Открыт спор
        ['name' => 'Сделка Спор открыт', 'amount' => 100000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем наличие спора'
        ],
        // Кастомер открыл спор и отозвал
        ['name' => 'Сделка Спор отозван', 'amount' => 30000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем отзыв созданного спора Заказчиком',
            'dispute_canceled' => true
        ],
        // Оператор закрыл Спор с тикетом продления 0 дней
        ['name' => 'Сделка Спор закрыт Оператором с продлением 0 дней', 'amount' => 35000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем закрытие спора Оператором с тикетом продления 0 дней',
            'warranty_extension_days' => 0,
            'dispute_closed' => true
        ],
        // Оператор закрыл Спор с тикетом продления 3 дня
        ['name' => 'Сделка Спор закрыт Оператор с продлением 3 дня', 'amount' => 40000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем закрытие спора Оператором с тикетом продления 3 дня',
            'warranty_extension_days' => 3,
            'dispute_closed' => true
        ],
        // Покупатель направил предложение о скидке
        ['name' => 'Сделка Покупатель направил запрос на Скидку', 'amount' => 200000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем запрос на Скидку от Покупателя',
            'customer_discount_request' => 20000.00
        ],
        // Покупатель направил предложение о возврате
        ['name' => 'Сделка Покупатель направил запрос на Возврат', 'amount' => 250000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем запрос на Возврат от Покупателя',
            'customer_refund_request' => true
        ],
        // Продавец принял предложение о скидке
        ['name' => self::DEAL_DISCOUNT_REQUEST_CONFIRMED, 'amount' => 270000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем принятие запроса на Скидку Продавцом',
            'customer_discount_request' => 50000.00,
            'contractor_discount_confirm' => true
        ],
        // Продавец принял предложение о скидке, Оператор создал сидку
        ['name' => 'Сделка Скидка создана', 'amount' => 280000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем принятие запроса на Скидку Продавцом и создание Скидки Оператором',
            'customer_discount_request' => 60000.00,
            'contractor_discount_confirm' => true,
            'outgoing_discount_payment_order' => 60000
        ],
        // Покупатель направил предложение о скидке, оно отклонено продавцом
        ['name' => 'Сделка Запрос на скидку отклонен', 'amount' => 300000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем отклонение запроса на скидку Продавцом',
            'customer_discount_request' => 50000.00,
            'contractor_discount_confirm' => false
        ],

        // Покупатель направил 3 предложенияя о скидке, они все отклонены - запрос на Арбитраж
        ['name' => 'Сделка Запросы на скидку отклонены 3 раза', 'amount' => 300000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем отклонение запроса на скидку Продавцом три раза',
            'declined_customer_discount_requests' => [50000.00, 40000.00, 30000.00],
            'contractor_discount_confirm' => false,
            'customer_arbitrage_request' => true
        ],

        // Покупатель направил предложение об арбитраже
        ['name' => 'Сделка Покупатель направил запрос на Арбитраж', 'amount' => 400000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем запрос на Арбитраж от Покупателя',
            'customer_arbitrage_request' => true
        ],
        // Оператор отклонил запрос на согласительную комиссию
        ['name' => 'Сделка Запрос на Арбитраж отклонен', 'amount' => 435000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем отказ Оператора от запроса на Арбитраж',
            'customer_arbitrage_request' => true,
            'contractor_arbitrage_confirm' => false
        ],
        // Оператор отклонил запрос на согласительную комиссию, создался запрос на Суд
        ['name' => 'Сделка Запрос на Арбитраж отклонен - запрос на Суд', 'amount' => 450000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем отказ Оператора на запрос Арбитража и создание запроса на Суд',
            'customer_arbitrage_request' => true,
            'contractor_arbitrage_confirm' => false,
            'customer_tribunal_request' => true
        ],
        // Оператор отклонил запрос на Cуд, создался запрос на согласительную комиссию
        ['name' => 'Сделка Запрос на Суд отклонен - запрос на Арбитраж', 'amount' => 455000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем отказ Оператора на запрос Суда и создание запроса на Арбитраж',
            'customer_tribunal_request' => true,
            'operator_tribunal_confirm' => false,
            'customer_arbitrage_request' => true
        ],
        // Оператор принял запрос на согласительную комиссию
        ['name' => 'Сделка Запрос на Арбитраж принят', 'amount' => 460000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем принятие Оператором запроса на Арбитраж',
            'customer_arbitrage_request' => true,
            'contractor_arbitrage_confirm' => true
        ],
        // Продавец отклонил предложение о возврате
        ['name' => 'Сделка Предложение возврата отклонено', 'amount' => 470000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем отклонение предложения о возврате Продавцом',
            'customer_refund_request' => true,
            'contractor_refund_confirm' => false
        ],
        // Продавец принят предложение о возврате
        ['name' => 'Сделка Предложение возврата принято', 'amount' => 480000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем принятие предложения о возврате Продавцом',
            'customer_refund_request' => true,
            'contractor_refund_confirm' => true,
        ],
        // Продавец отклонил предложение о возврате, создался запрос на Арбитраж
        ['name' => 'Сделка Запрос на Возврат отклонен - запрос на Арбитраж', 'amount' => 490000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем отклонение предложения о Возврате и создание запроса Арбитраж',
            'customer_refund_request' => true,
            'contractor_refund_confirm' => false,
            'customer_arbitrage_request' => true,
        ],
        // Продавец направил предложение о суде
        ['name' => 'Сделка Продавец направил запрос на суд', 'amount' => 500000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем запрос Продавца на суд',
            'customer_tribunal_request' => true,
        ],
        // Продавец направил предложение о суде, оно отклонено оператором
        ['name' => 'Сделка Запроса на суд отклонен', 'amount' => 600000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем отклонение запроса на суд оператором',
            'customer_tribunal_request' => true,
            'operator_tribunal_confirm' => false,
        ],
        // Продавец направил предложение о суде, оно прнято оператором
        ['name' => 'Сделка Запрос на суд принят', 'amount' => 650000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем принятие запроса на суд оператором',
            'customer_tribunal_request' => true,
            'operator_tribunal_confirm' => true,
        ],
        // Продавец направил предложение о возврате, оно принято, оператор создал Возврат
        ['name' => 'Сделка Возврат создан', 'amount' => 780000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем Спор в статусе Возврат',
            'customer_refund_request' => true,
            'contractor_refund_confirm' => true,
            'outgoing_refund_payment_order' => true,
        ],
        // Продавец направил предложение о возврате, оно принято, оператор создал Возврат, платёжка попала в файл на выплату
        ['name' => 'Сделка Ожидает возврата', 'amount' => 700000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем сделку в статусе Ожидает возврата (Спор в статусе Возврат осуществлен)',
            'customer_refund_request' => true,
            'contractor_refund_confirm' => true,
            'outgoing_refund_payment_order' => true,
            'add_outgoing_refund_payment_order_to_file' => true
        ],
        // Продавец направил предложение о возврате, оно принято, оператор создал Возврат, платёжка попала в файл на выплату
        // ...и есть подтверждающая платёжка
        ['name' => 'Сделка Закрыто с возвратом', 'amount' => 760000, 'type' => 'T', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем сделку в статусе Закрыто с возвратом',
            'customer_refund_request' => true,
            'contractor_refund_confirm' => true,
            'outgoing_refund_payment_order' => true,
            'add_outgoing_refund_payment_order_to_file' => true,
            'confirmation_refund_payment_order' => true,
        ],
        // Третейский суд
        ['name' => 'Сделка Третейский суд', 'amount' => 800000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем Третейский суд',
            'customer_tribunal_request' => true,
            'operator_tribunal_confirm' => true,
            'tribunal' => true
        ],
        // Спор закрыт (со скидкой) с тикетом продления 0 дней
        ['name' => 'Сделка Спор со Скидкой закрыт без продления', 'amount' => 850000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем Спор (со скидкой) с тикетом продления 0 дней',
            'customer_discount_request' => 50000.00,
            'contractor_discount_confirm' => true,
            'outgoing_discount_payment_order' => 50000,
            'warranty_extension_days' => 0,
            'dispute_closed' => true
        ],
        // Спор со Скидкой закрыт с тикетом продления 4 дня
        ['name' => 'Сделка Спор со Скидкой закрыт с продлением 4 дня', 'amount' => 880000, 'type' => 'U', 'fee_payer' => 'CUSTOMER',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'dispute_reason' => 'Тестируем Спор со Скидкой закрыт с продлением 4 дня',
            'customer_discount_request' => 10000.00,
            'contractor_discount_confirm' => true,
            'outgoing_discount_payment_order' => 10000,
            'warranty_extension_days' => 4,
            'dispute_closed' => true
        ],
    ];
    const DEALS_IN_STATUS_PENDING_CREDIT_PAYMENT = [ // используется также классом PaymentFixtureLoader
        ['name' => 'Сделка Ожидает выплаты', 'amount' => 2000000, 'type' => 'U', 'fee_payer' => 'CONTRACTOR',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'delivered' => true
        ],
    ];
    const DEALS_IN_STATUS_CLOSED = [ // используется также классом PaymentFixtureLoader
        ['name' => 'Сделка Закрыта', 'amount' => 1000000, 'type' => 'U', 'fee_payer' => 'CONTRACTOR',
            'main_test_user_is' => 'customer', 'customer_confirm' => true, 'contractor_confirm' => true,
            'delivered' => true
        ],
    ];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var User
     */
    private $user1;

    /**
     * @var User
     */
    private $user2;

    /**
     * @var DealType
     */
    private $dealTypeU;

    /**
     * @var DealType
     */
    private $dealTypeT;

    /**
     * @var FeePayerOption
     */
    private $feePayerOptionCustomer;

    /**
     * @var FeePayerOption
     */
    private $feePayerOptionContractor;

    /**
     * @var FeePayerOption
     */
    private $feePayerOptionEachOf50;

    /**
     * @var CivilLawSubject
     */
    private $naturalPersonCivilLawSubject_1;

    /**
     * @var CivilLawSubject
     */
    private $naturalPersonCivilLawSubject_2;

    /**
     * @var CivilLawSubject
     */
    private $legalEntityCivilLawSubject_1;

    /**
     * @var CivilLawSubject
     */
    private $legalEntityCivilLawSubject_2;

    /**
     * @var \DateTime
     */
    private $currentDate;

    /**
     * @return int
     */
    public function getOrder()
    {
        return 110;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function load(ObjectManager $manager)
    {
        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        $this->currentDate   = new \DateTime();

        // Set users to class vars $this->user1 и $this->user2
        $this->setUsers();
        // Set dealTypes to class vars $this->dealTypeU и $this->dealTypeT
        $this->setDealTypes();
        // Set dealTypes to class vars $this->feePayerOptionCustomer, $this->feePayerOptionContractor, $this->feePayerOptionEachOf50
        $this->setFeePayerOption();
        // Set users to class vars $this->civilLawSubject1 и $this->civilLawSubject2
        $this->setCivilLawSubjects();

        /// Сделки в статусе 'Ожидает согласования' ///
        foreach (self::DEALS_IN_STATUS_NEGOTIATION as $deal_array) {
            $partner_invited = false;
            if (array_key_exists('partner_invited', $deal_array)) {
                $partner_invited = $deal_array['partner_invited'];
            }
            /** @var Deal $dealNegotiation */
            $dealNegotiation = $this->createDealInNegotiationStatus($deal_array['name'], $deal_array['type'], $deal_array['fee_payer'],
                $deal_array['main_test_user_is'], $deal_array['customer_confirm'], $deal_array['contractor_confirm'], $partner_invited);

            $this->entityManager->persist($dealNegotiation);

            $this->entityManager->flush();

            $dealNegotiation->setNumber($this->getDealNumber($dealNegotiation));

            if (array_key_exists('comments', $deal_array)) {
//                $customerUser = $dealNegotiation->getCustomer()->getUser();
//                $contractorUser = $dealNegotiation->getContractor()->getUser();
                /** @var User $operator */
                $operator = $this->entityManager->getRepository(User::class)
                    ->findOneBy(array('login' => 'operator'));

                foreach ($deal_array['comments'] as $comment) {
                    $message = new Message();
                    $message->setCreated(new \DateTime());
                    $message->setAuthor($operator);
                    $message->setText($comment['text']);

                    $this->entityManager->persist($message);
                    $dealNegotiation->addMessage($message);
                }
            }

            $this->entityManager->persist($dealNegotiation);

            $this->entityManager->flush();
        }

        /// Сделки в статусе 'Согласовано' ///
        foreach (self::DEALS_IN_STATUS_CONFIRMED as $deal_array) {
            $partner_invited = false;
            if (array_key_exists('partner_invited', $deal_array)) {
                $partner_invited = $deal_array['partner_invited'];
            }
            /** @var Deal $dealConfirmed */
            $dealConfirmed = $this->createDealInConfirmedStatus($deal_array['name'], $deal_array['type'], $deal_array['fee_payer'],
                $deal_array['main_test_user_is'], $deal_array['customer_confirm'], $deal_array['contractor_confirm']);

            $this->entityManager->persist($dealConfirmed);

            $this->entityManager->flush();

            $dealConfirmed->setNumber($this->getDealNumber($dealConfirmed));
            $dealConfirmed->setDealContractFile($this->createDealContract($dealConfirmed));

            $this->entityManager->flush();
        }

        /// Сделки в статусе 'Оплачено' ///
        foreach (self::DEALS_IN_STATUS_DEBIT_MATCHED as $deal_array) {
            /** @var Deal $dealDebitMatched */
            $dealDebitMatched = $this->createDealInDebitMatchedStatus($deal_array['name'], $deal_array['type'], $deal_array['fee_payer'],
                $deal_array['main_test_user_is'], $deal_array['customer_confirm'], $deal_array['contractor_confirm']);

            $this->entityManager->persist($dealDebitMatched);

            $this->entityManager->flush();

            $dealDebitMatched->setNumber($this->getDealNumber($dealDebitMatched));
            $dealDebitMatched->setDealContractFile($this->createDealContract($dealDebitMatched));

            $this->entityManager->flush();
        }

        /// Сделки в статусе 'Товар доставлен' ///
        foreach (self::DEALS_IN_STATUS_DELIVERED as $deal_array) {
            /** @var Deal $dealDebitMatched */
            $dealDebitMatched = $this->createDealInDebitMatchedStatus($deal_array['name'], $deal_array['type'], $deal_array['fee_payer'],
                $deal_array['main_test_user_is'], $deal_array['customer_confirm'], $deal_array['contractor_confirm']);

            $this->entityManager->persist($dealDebitMatched);
            $this->entityManager->flush();

            $dealDebitMatched->setNumber($this->getDealNumber($dealDebitMatched));
            $dealDebitMatched->setDealContractFile($this->createDealContract($dealDebitMatched));

            if (isset($deal_array['delivered'])) {
                $this->createDelivery($dealDebitMatched, $deal_array['delivered']);
            }

            $this->entityManager->persist($dealDebitMatched);
            $this->entityManager->flush();
        }

        /// Сделки в статусе 'Проблемная' ///
        foreach (self::DEALS_IN_STATUS_TROUBLE as $deal_array) {
            /** @var Deal $dealDebitMatched */
            $dealDebitMatched = $this->createDealInDebitMatchedStatus($deal_array['name'], $deal_array['type'], $deal_array['fee_payer'],
                $deal_array['main_test_user_is'], $deal_array['customer_confirm'], $deal_array['contractor_confirm']);

            $this->entityManager->persist($dealDebitMatched);

            $this->entityManager->flush();

            $dealDebitMatched->setNumber($this->getDealNumber($dealDebitMatched));
            if (isset($deal_array['trouble']) && !isset($deal_array['trouble']['contract_file_missing'])) {

                $dealDebitMatched->setDealContractFile($this->createDealContract($dealDebitMatched));
            }

            $this->entityManager->flush();
        }

        /// Сделки в статусе 'Спор' ///
        foreach (self::DEALS_IN_STATUS_DISPUTE as $deal_array) {
            /** @var Deal $dealDebitMatched */
            $dealDebitMatched = $this->createDealInDebitMatchedStatus($deal_array['name'], $deal_array['type'], $deal_array['fee_payer'],
                $deal_array['main_test_user_is'], $deal_array['customer_confirm'], $deal_array['contractor_confirm']);

            $this->entityManager->persist($dealDebitMatched);

            $this->entityManager->flush();

            $dealDebitMatched->setNumber($this->getDealNumber($dealDebitMatched));
            $dealDebitMatched->setDealContractFile($this->createDealContract($dealDebitMatched));

            $this->entityManager->flush();
        }

        /// Сделки в статусе 'Ожидает выплаты' ///
        foreach (self::DEALS_IN_STATUS_PENDING_CREDIT_PAYMENT as $deal_array) {
            /** @var Deal $dealDebitMatched */
            $dealDebitMatched = $this->createDealInDebitMatchedStatus($deal_array['name'], $deal_array['type'], $deal_array['fee_payer'],
                $deal_array['main_test_user_is'], $deal_array['customer_confirm'], $deal_array['contractor_confirm']);

            $this->entityManager->persist($dealDebitMatched);
            $this->entityManager->flush();

            $dealDebitMatched->setNumber($this->getDealNumber($dealDebitMatched));
            $dealDebitMatched->setDealContractFile($this->createDealContract($dealDebitMatched));

            if (isset($deal_array['delivered'])) {
                $this->createDelivery($dealDebitMatched, $deal_array['delivered']);
            }

            $this->entityManager->persist($dealDebitMatched);
            $this->entityManager->flush();
        }

        /// Сделки в статусе 'Закрыто' ///
        foreach (self::DEALS_IN_STATUS_CLOSED as $deal_array) {
            /** @var Deal $dealDebitMatched */
            $dealDebitMatched = $this->createDealInDebitMatchedStatus($deal_array['name'], $deal_array['type'], $deal_array['fee_payer'],
                $deal_array['main_test_user_is'], $deal_array['customer_confirm'], $deal_array['contractor_confirm']);

            $this->entityManager->persist($dealDebitMatched);
            $this->entityManager->flush();

            $dealDebitMatched->setNumber($this->getDealNumber($dealDebitMatched));
            $dealDebitMatched->setDealContractFile($this->createDealContract($dealDebitMatched));

            if (isset($deal_array['delivered'])) {
                $this->createDelivery($dealDebitMatched, $deal_array['delivered']);
            }

            $this->entityManager->persist($dealDebitMatched);
            $this->entityManager->flush();
        }
    }

    /**
     * @param $deal_name
     * @param $type
     * @param $fee_payer
     * @param $main_test_user_is
     * @param $customer_confirm
     * @param $contractor_confirm
     * @return Deal
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createDealInConfirmedStatus($deal_name, $type, $fee_payer, $main_test_user_is, $customer_confirm,
                                                 $contractor_confirm)
    {
        /** @var Deal $dealConfirmed */
        $dealConfirmed = $this->createDealInDebitMatchedStatus($deal_name, $type, $fee_payer, $main_test_user_is, $customer_confirm,
            $contractor_confirm, true);

        $this->createDealContract($dealConfirmed);

        return $dealConfirmed;
    }

    /**
     * @param string $deal_name
     * @param string $type
     * @param string $fee_payer
     * @param string $main_test_user_is
     * @param bool $customer_confirm
     * @param bool $contractor_confirm
     * @return Deal
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createDealInDebitMatchedStatus(string $deal_name, string $type, string $fee_payer,
                                                    string $main_test_user_is, bool $customer_confirm, bool $contractor_confirm)
    {
        if (self::DEAL_BETWEEN_LEGAL_ENTITIES_NAME === $deal_name) {
            $civilCawSubjectUser_1 = $this->legalEntityCivilLawSubject_1;
            $civilCawSubjectUser_2 = $this->legalEntityCivilLawSubject_2;
        } else {
            $civilCawSubjectUser_1 = $this->naturalPersonCivilLawSubject_1;
            $civilCawSubjectUser_2 = $this->naturalPersonCivilLawSubject_2;
        }

        if ($main_test_user_is === 'customer') {
            /** @var DealAgent $customer */
            $customer = $this->createDealAgent($this->user1, $civilCawSubjectUser_1, $customer_confirm);
            /** @var DealAgent $contractor */
            $contractor = $this->createDealAgent($this->user2, $civilCawSubjectUser_2, $contractor_confirm);
        } else {
            /** @var DealAgent $contractor */
            $customer = $this->createDealAgent($this->user2, $civilCawSubjectUser_2, $customer_confirm);
            /** @var DealAgent $customer */
            $contractor = $this->createDealAgent($this->user1, $civilCawSubjectUser_1, $contractor_confirm);
        }

        /** @var DealType $type */
        $type = ($type === 'U') ? $this->dealTypeU : $this->dealTypeT;

        if ($fee_payer === 'CUSTOMER') {
            $feePayer = $this->feePayerOptionCustomer;
        } elseif ($fee_payer === 'CONTRACTOR') {
            $feePayer = $this->feePayerOptionContractor;
        } else {
            $feePayer = $this->feePayerOptionEachOf50;
        }

        $creationDate = clone new \DateTime();
        $creationDate = $creationDate->modify('-3 hours');

        if ($main_test_user_is === 'customer') {
            $owner = $customer;
            $counterAgent = $contractor;
        } else {
            $owner = $contractor;
            $counterAgent = $customer;
        }
        //ставим контрагенту дату приглашения
        $counterAgent->setInvitationDate($creationDate);
        $this->entityManager->persist($counterAgent);
        $this->entityManager->flush();

        $deal = new Deal();
        $deal->setCustomer($customer);
        $deal->setContractor($contractor);
        $deal->setOwner($owner);
        $deal->setName($deal_name);
        $deal->setCreated($creationDate);
        $deal->setDeliveryPeriod(9);
        $deal->setAddonTerms('Дополнительные условия');
        $deal->setDealType($type);
        $deal->setFeePayerOption($feePayer);
        $deal->setUpdated($this->currentDate);

        $deal->setDateOfConfirm($this->currentDate);

        // @TODO Создать PaymentOrders

        return $deal;
    }

    /**
     * @param Deal $deal
     * @return DealContractFile
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createDealContract(Deal $deal)
    {
        $file = new File();
        $file->setCreated($this->currentDate);
        $file->setName('deal_'.$deal->getId());
        $file->setType('pdf');
        $file->setOriginName('deal_'.$deal->getId());
        $file->setPath('/deal-contract');
        $file->setSize(123456);

        $deal->setDateOfConfirm($this->currentDate);

        $this->entityManager->persist($file);
        $this->entityManager->persist($deal);
        $this->entityManager->flush();

        $contract = new DealContractFile();
        $contract->setDeal($deal);
        $contract->setFile($file);

        return $contract;
    }

    /**
     * @param string $deal_name
     * @param string $type
     * @param string $fee_payer
     * @param string $main_test_user_is
     * @param bool $customer_confirm
     * @param bool $contractor_confirm
     * @param $partner_invited
     * @return Deal
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createDealInNegotiationStatus(string $deal_name, string $type, string $fee_payer,
                                                   string $main_test_user_is, bool $customer_confirm, bool $contractor_confirm,
                                                   $partner_invited)
    {
        if ($main_test_user_is === 'customer') {
            /** @var DealAgent $customer */
            $customer = $this->createDealAgent($this->user1, $this->naturalPersonCivilLawSubject_1, $customer_confirm);
            $add_to_user = true;
            if ($partner_invited) {
                $add_to_user = false;
            }
            /** @var DealAgent $customer */
            $contractor = $this->createDealAgent($this->user2, $this->naturalPersonCivilLawSubject_2, $contractor_confirm, $add_to_user);
        } else {
            $add_to_user = true;
            if ($partner_invited) {
                $add_to_user = false;
            }
            /** @var DealAgent $customer */
            $customer = $this->createDealAgent($this->user2, $this->naturalPersonCivilLawSubject_2, $customer_confirm, $add_to_user);
            /** @var DealAgent $customer */
            $contractor = $this->createDealAgent($this->user1, $this->naturalPersonCivilLawSubject_1, $contractor_confirm);
        }

        /** @var DealType $type */
        $type = ($type === 'U') ? $this->dealTypeU : $this->dealTypeT;

        if ($fee_payer === 'CUSTOMER') {
            $feePayer = $this->feePayerOptionCustomer;
        } elseif ($fee_payer === 'CONTRACTOR') {
            $feePayer = $this->feePayerOptionContractor;
        } else {
            $feePayer = $this->feePayerOptionEachOf50;
        }

        $creationDate = clone new \DateTime();
        $creationDate = $creationDate->modify('-3 hours');

        if ($main_test_user_is === 'customer') {
            $owner = $customer;
            $counterAgent = $contractor;
        } else {
            $owner = $contractor;
            $counterAgent = $customer;
        }
        //ставим контрагенту дату приглашения
        $counterAgent->setInvitationDate($creationDate);
        $this->entityManager->persist($counterAgent);
        $this->entityManager->flush();

        $deal = new Deal();
        $deal->setCustomer($customer);
        $deal->setContractor($contractor);
        $deal->setOwner($owner); // Set Owner
        $deal->setName($deal_name);
        $deal->setCreated($creationDate);
        $deal->setDeliveryPeriod(9);
        $deal->setAddonTerms('Дополнительные условия');
        $deal->setDealType($type);
        $deal->setFeePayerOption($feePayer);
        $deal->setUpdated($this->currentDate);

        return $deal;
    }

    /**
     * @param Deal $deal
     * @return string
     */
    private function getDealNumber(Deal $deal)
    {
        return '#GP'.$deal->getId().$deal->getDealType()->getIdent();
    }

    /**
     * Set users
     */
    private function setUsers()
    {
        $this->user1 = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test'));

        $this->user2 = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test2'));
    }

    /**
     * Set dealTypes
     */
    private function setDealTypes()
    {
        $this->dealTypeU = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('ident' => 'U'));

        $this->dealTypeT = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('ident' => 'T'));
    }

    /**
     * Set feePayerOptions
     */
    private function setFeePayerOption()
    {
        $this->feePayerOptionCustomer = $this->entityManager
            ->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $this->feePayerOptionContractor = $this->entityManager
            ->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CONTRACTOR'));

        $this->feePayerOptionEachOf50 = $this->entityManager
            ->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => '50/50'));
    }

    /**
     * @param User $user
     * @param CivilLawSubject $setCivilLawSubject
     * @param bool $confirmed
     * @param bool $add_to_user
     * @param null $dateForDateOfConfirm
     * @return DealAgent
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createDealAgent(User $user, CivilLawSubject $setCivilLawSubject, bool $confirmed,
                                     $add_to_user=true, $dateForDateOfConfirm=null)
    {
        $dealAgent = new DealAgent();
        if ($add_to_user) {
            $dealAgent->setUser($user);
            $dealAgent->setEmail($user->getEmail()->getEmail());
            $dealAgent->setCivilLawSubject($setCivilLawSubject);
        } else {
            $dealAgent->setUser(null);
            $dealAgent->setEmail('test3@guarantpay.com');
            $dealAgent->setCivilLawSubject(null);
        }
        $dealAgent->setDealAgentConfirm($confirmed);
        $dealAgent->setDealAgentOnceConfirm($confirmed);
        $dealAgent->setDateOfConfirm($dateForDateOfConfirm);

        $this->entityManager->persist($dealAgent);
        $this->entityManager->flush();

        return $dealAgent;
    }

    /**
     * Set civilLawSubjects
     */
    private function setCivilLawSubjects()
    {
        /** @var NaturalPerson $naturalPerson_1 */
        $naturalPerson_1 = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Тест', 'lastName' => 'Тестов'));
        /** @var NaturalPerson $naturalPerson_2 */
        $naturalPerson_2 = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('firstName' => 'Контрагент', 'lastName' => 'Контрагентов'));

        /** @var LegalEntity $legalEntity_1 */
        $legalEntity_1 = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Колумбийский сахар'));
        /** @var LegalEntity $legalEntity_2 */
        $legalEntity_2 = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'ООО Simple Technology'));

        $this->naturalPersonCivilLawSubject_1 = $naturalPerson_1->getCivilLawSubject();
        $this->naturalPersonCivilLawSubject_2 = $naturalPerson_2->getCivilLawSubject();

        $this->legalEntityCivilLawSubject_1 = $legalEntity_1->getCivilLawSubject();
        $this->legalEntityCivilLawSubject_2 = $legalEntity_2->getCivilLawSubject();
    }

    /**
     * @param Deal $deal
     * @param bool $delivered
     */
    private function createDelivery(Deal $deal, bool $delivered)
    {
        $current_day = new \DateTime();
        $creationDate = $current_day->modify('+'.$deal->getDeliveryPeriod().' days');
        $creationDate->modify('-1 hours');

        $delivery = new Delivery();
        $delivery->setDeal($deal);
        $delivery->setDeliveryDate($creationDate);
        $delivery->setDeliveryStatus($delivered);

        $this->entityManager->persist($delivery);
        #$this->entityManager->flush($delivery);

        $deal->addDelivery($delivery);
    }
}