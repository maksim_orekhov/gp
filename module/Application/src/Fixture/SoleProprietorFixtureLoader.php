<?php

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\SoleProprietor;

/**
 * Class SoleProprietorFixtureLoader
 * @package Application\Fixture
 */
class SoleProprietorFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const SOLE_PROPRIETORS_DATA = [
        [
            'name' => 'ИП Месси Леонель Аргентинович',
            'first_name' => 'Леонель',
            'secondary_name' => 'Аргентинович',
            'last_name' => 'Месси',
            'birth_date' => '1980-12-31',
            'inn' => '683103646744'
        ],
        [
            'name' => 'ИП Роналду Криштиану Португалович',
            'first_name' => 'Криштиану',
            'secondary_name' => 'Португалович',
            'last_name' => 'Роналду',
            'birth_date' => '1984-05-31',
            'inn' => '683103646744'
        ]
    ];

    public function getOrder()
    {
        return 65;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        foreach (self::SOLE_PROPRIETORS_DATA as $sole_proprietor) {
            $soleProprietor = new SoleProprietor();
            $soleProprietor->setName($sole_proprietor['name']);
            $soleProprietor->setFirstName($sole_proprietor['first_name']);
            $soleProprietor->setSecondaryName($sole_proprietor['secondary_name']);
            $soleProprietor->setLastName($sole_proprietor['last_name']);
            $soleProprietor->setInn($sole_proprietor['inn']);
            $soleProprietor->setBirthDate(\DateTime::createFromFormat('!Y-m-d', $sole_proprietor['birth_date']));

            $entityManager->persist($soleProprietor);
        }

        $entityManager->flush();
    }
}