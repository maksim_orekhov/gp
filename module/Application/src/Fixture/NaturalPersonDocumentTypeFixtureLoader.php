<?php

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\NaturalPersonDocumentType;

class NaturalPersonDocumentTypeFixtureLoader implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        $naturalPersonDocumentManager = $this->container->get(\Application\Service\CivilLawSubject\NaturalPersonDocumentManager::class);

        $types = $naturalPersonDocumentManager::getTypes();

        foreach ($types as $type) {
            $document = new NaturalPersonDocumentType();
            $document->setName($type);
            $entityManager->persist($document);
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 50;
    }
}