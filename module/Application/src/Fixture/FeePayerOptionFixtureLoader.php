<?php

namespace Application\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\FeePayerOption;

/**
 * Class FeePayerOptionFixtureLoader
 * @package Application\Fixture
 */
class FeePayerOptionFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $feePayerOption = new FeePayerOption();
        $feePayerOption->setName('CUSTOMER');
        $feePayerOption->setIsActive(true);
        $entityManager->persist($feePayerOption);

        $feePayerOption2 = new FeePayerOption();
        $feePayerOption2->setName('CONTRACTOR');
        $feePayerOption2->setIsActive(true);
        $entityManager->persist($feePayerOption2);

        $feePayerOption3 = new FeePayerOption();
        $feePayerOption3->setName('50/50');
        $feePayerOption3->setIsActive(true);
        $entityManager->persist($feePayerOption3);

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 90;
    }
}