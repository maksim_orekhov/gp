<?php

namespace Application\Fixture;

use Application\Entity\CivilLawSubject;
use Application\Entity\DealType;
use Application\Entity\Token;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\User;

/**
 * Class TokenFixtureLoader
 * @package Application\Fixture
 *
 * @TODO Пока при создании токена не учитывается форма собственности субъкта
 */
class TokenFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getOrder()
    {
        return 170;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        /** @var DealType $dealType */
        $dealType = $entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test']);

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $token = new Token();
        $token->setName('Токен для сделок с субъектом пользователя test');
        $token->setDealRole('contractor');
        $token->setPartnerIdent('lst_arenda');
        $token->setCreated(new \DateTime());
        $token->setIsActive(true);
        $token->setAmount(null);
        $token->setCounteragentUser($user);
        $token->setCounteragentCivilLawSubject($counteragentCivilLawSubject);
        $token->setDealName('Сделка с субъектом пользователя test');
        $token->setDealType($dealType);
        $token->setTokenKey('arenda_specztechniki');

        $entityManager->persist($token);

        $entityManager->flush();
    }
}