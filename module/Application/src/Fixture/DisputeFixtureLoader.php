<?php

namespace Application\Fixture;

use Application\Entity\ArbitrageConfirm;
use Application\Entity\ArbitrageRequest;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\Discount;
use Application\Entity\DiscountConfirm;
use Application\Entity\DiscountRequest;
use Application\Entity\DisputeCycle;
use Application\Entity\RefundConfirm;
use Application\Entity\RefundRequest;
use Application\Entity\Tribunal;
use Application\Entity\TribunalConfirm;
use Application\Entity\TribunalRequest;
use Application\Entity\User;
use Application\Entity\WarrantyExtension;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Dispute;
use Doctrine\ORM\EntityManager;
use Zend\Form\Element\DateTime;

class DisputeFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \DateTime
     */
    private $currentDate;


    public function getOrder()
    {
        return 140;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        $this->currentDate   = new \DateTime();

        /// Сделки в статусе 'Спор' ///
        foreach (DealFixtureLoader::DEALS_IN_STATUS_DISPUTE as $deal_array) {
            /** @var Deal $deal */
            $deal = $this->entityManager->getRepository(Deal::class)
                ->findOneBy(array('name' => $deal_array['name']));
            /** @var Dispute $dispute */
            $dispute = $this->createDispute($deal, $deal_array['dispute_reason']);

            // Кастомер открыл спор и отозвал
            if (isset($deal_array['dispute_canceled'])) {

                $this->closeDispute($dispute);
            }

            // Покупатель направил предложение о скидке
            if (isset($deal_array['customer_discount_request'])) {
                /** @var DealAgent $author */
                $author = $deal->getCustomer();
                /** @var DiscountRequest $discountRequest */
                $discountRequest = $this->createDiscountRequest($dispute, $author,
                    $deal_array['customer_discount_request']);
                // Продавец отклонил предложение о скидке
                if (isset($deal_array['contractor_discount_confirm'])) {
                    /** @var DealAgent $confirmAuthor */
                    $confirmAuthor = $deal->getContractor();
                    /** @var DiscountConfirm $discountConfirm */
                    $discountConfirm = $this->createDiscountConfirm($discountRequest, $confirmAuthor,
                        $deal_array['contractor_discount_confirm']);
                }
            }

            // Покупатель направил предложение о скидке 3 раза и получил отказ
            if (isset($deal_array['declined_customer_discount_requests'])) {
                /** @var DealAgent $author */
                $author = $deal->getCustomer();

                foreach ($deal_array['declined_customer_discount_requests'] as $request_amount) {
                    /** @var DiscountRequest $discountRequest */
                    $discountRequest = $this->createDiscountRequest($dispute, $author, $request_amount);
                    // Продавец отклонил предложение о скидке
                    if (isset($deal_array['contractor_discount_confirm'])) {
                        /** @var DealAgent $confirmAuthor */
                        $confirmAuthor = $deal->getContractor();
                        /** @var DiscountConfirm $discountConfirm */
                        $discountConfirm = $this->createDiscountConfirm($discountRequest, $confirmAuthor,
                            $deal_array['contractor_discount_confirm']);
                    }
                    sleep (0.1); // Задержка на 0.1 секунду
                }
            }

            // Оператор выписал тикет продления (кол-во дней в $deal_array['warranty_extension_days'])
            if (isset($deal_array['warranty_extension_days'])) {

                $this->createWarrantyExtension($dispute, $deal_array['warranty_extension_days']);
            }

            // Покупатель направил предложение о возврате
            if (isset($deal_array['customer_refund_request'])) {
                /** @var DealAgent $author */
                $author = $deal->getCustomer();
                /** @var DiscountRequest $discountRequest */
                $refundRequest = $this->createRefundRequest($dispute, $author);
                // Продавец принял предложение о возврате
                if (isset($deal_array['contractor_refund_confirm'])) {
                    /** @var DealAgent $confirmAuthor */
                    $confirmAuthor = $deal->getContractor();
                    /** @var RefundConfirm $refundConfirm */
                    $refundConfirm = $this->createRefundConfirm($refundRequest, $confirmAuthor,
                        $deal_array['contractor_refund_confirm']);
                }
            }

            // Покупатель направил предложение об арбитраже
            if (isset($deal_array['customer_arbitrage_request'])) {
                /** @var DealAgent $author */
                $author = $deal->getCustomer();
                /** @var ArbitrageRequest $arbitrageRequest */
                $arbitrageRequest = $this->createArbitrageRequest($dispute, $author);
                if (isset($deal_array['contractor_arbitrage_confirm'])) {
                    /** @var User $confirmAuthor */
                    $confirmAuthor = $this->entityManager->getRepository(User::class)
                        ->findOneBy(array('login' => 'operator'));
                    /** @var ArbitrageRequest $arbitrageConfirm */
                    $arbitrageConfirm = $this->createArbitrageConfirm($arbitrageRequest, $confirmAuthor,
                        $deal_array['contractor_arbitrage_confirm']);
                }
            }

            if (isset($deal_array['customer_tribunal_request'])) {
                /** @var DealAgent $author */
                $author = $deal->getCustomer();
                /** @var TribunalRequest $tribunal */
                $tribunalRequest = $this->createTribunalRequest($dispute, $author);
                if (isset($deal_array['operator_tribunal_confirm'])) {
                    /** @var User $confirmAuthor */
                    $confirmAuthor = $deal = $this->entityManager->getRepository(User::class)
                        ->findOneBy(array('login' => 'operator'));
                    $tribunalConfirm = $this->createTribunalConfirm($tribunalRequest, $confirmAuthor,
                        $deal_array['operator_tribunal_confirm']);
                }
                if (isset($deal_array['tribunal'])) {
                    $tribunal = $this->createTribunal($dispute);
                }
            }

            if (isset($deal_array['dispute_closed'])) {

                $this->closeDispute($dispute);
            }
        }
    }

    /**
     * @param Dispute $dispute
     * @param $days
     * @return WarrantyExtension
     */
    private function createWarrantyExtension(Dispute $dispute, $days)
    {
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();

        $warrantyExtension = new WarrantyExtension();
        $creationDate = clone new \DateTime();
        $warrantyExtension->setCreated($creationDate->modify('-20 seconds'));
        $warrantyExtension->setDays($days);
        $warrantyExtension->setDispute($dispute);
        $warrantyExtension->setDisputeCycle($disputeCycle);

        $dispute->addWarrantyExtension($warrantyExtension);

        $this->entityManager->persist($warrantyExtension);
        $disputeCycle->addWarrantyExtension($warrantyExtension);
        $this->entityManager->persist($disputeCycle);
        $this->entityManager->persist($dispute);
        $this->entityManager->flush();

        return $warrantyExtension;
    }


    /**
     * @param TribunalRequest $tribunalRequest
     * @param User $author
     * @param bool $is_confirm
     * @return TribunalConfirm
     */
    private function createTribunalConfirm(TribunalRequest $tribunalRequest, User $author, bool $is_confirm): TribunalConfirm
    {
        $tribunalConfirm = new TribunalConfirm();
        $tribunalConfirm->setAuthor($author);
        /** @var \DateTime $creationDate */
        $creationDate = clone $tribunalRequest->getCreated();
        $tribunalConfirm->setCreated($creationDate->modify('+1 second'));
        $tribunalConfirm->setTribunalRequest($tribunalRequest);
        $tribunalConfirm->setStatus($is_confirm);

        $this->entityManager->persist($tribunalConfirm);

        $tribunalRequest->setConfirm($tribunalConfirm);

        $this->entityManager->persist($tribunalRequest);
        $this->entityManager->flush($tribunalRequest);

        return $tribunalConfirm;
    }

    /**
     * @param Dispute $dispute
     * @param DealAgent $author
     * @return TribunalRequest
     */
    private function createTribunalRequest(Dispute $dispute, DealAgent $author)
    {
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();

        $tribunalRequest = new TribunalRequest();

        if ($dispute->getArbitrageRequests()->last()) {
            /** @var ArbitrageRequest $arbitrageRequest */
            $arbitrageRequest = $dispute->getArbitrageRequests()->last();
            /** @var \DateTime $creationDate */
            $creationDate = clone $arbitrageRequest->getCreated();
            $creationDate->modify('+2 seconds');
        } else {
            $creationDate = clone new \DateTime();
            $creationDate->modify('-18 min');
        }

        $tribunalRequest->setCreated($creationDate);
        $tribunalRequest->setDispute($dispute);
        $tribunalRequest->setAuthor($author);
        $tribunalRequest->setDisputeCycle($disputeCycle);

        $this->entityManager->persist($tribunalRequest);
        $disputeCycle->addTribunalRequest($tribunalRequest);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush($tribunalRequest);

        return $tribunalRequest;
    }

    /**
     * @param Dispute $dispute
     * @return Tribunal
     */
    private function createTribunal(Dispute $dispute)
    {
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();

        $tribunal = new Tribunal();
        $tribunal->setCreated($this->currentDate);
        $tribunal->setDispute($dispute);
        $tribunal->setDisputeCycle($disputeCycle);

        $this->entityManager->persist($tribunal);
        $disputeCycle->setTribunal($tribunal);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush($tribunal);

        return $tribunal;
    }

    /**
     * @param RefundRequest $refundRequest
     * @param DealAgent $author
     * @param bool $is_confirm
     * @return RefundConfirm
     */
    private function createRefundConfirm(RefundRequest $refundRequest, DealAgent $author, bool $is_confirm): RefundConfirm
    {
        $refundConfirm = new RefundConfirm();
        $refundConfirm->setAuthor($author);
        $creationDate = clone new \DateTime();
        $refundConfirm->setCreated($creationDate->modify('-19 min'));
        $refundConfirm->setRefundRequest($refundRequest);
        $refundConfirm->setStatus($is_confirm);

        $this->entityManager->persist($refundConfirm);

        $refundRequest->setConfirm($refundConfirm);

        $this->entityManager->persist($refundRequest);
        $this->entityManager->flush($refundRequest);

        return $refundConfirm;
    }

    /**
     * @param Dispute $dispute
     * @param DealAgent $author
     * @return RefundRequest
     */
    private function createRefundRequest(Dispute $dispute, DealAgent $author): RefundRequest
    {
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();

        $refundRequest = new RefundRequest();
        $refundRequest->setDispute($dispute);
        $creationDate = clone new \DateTime();
        $refundRequest->setCreated($creationDate->modify('-20 min'));
        $refundRequest->setAuthor($author);
        $refundRequest->setDisputeCycle($disputeCycle);

        $this->entityManager->persist($refundRequest);
        $disputeCycle->addRefundRequest($refundRequest);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush($refundRequest);

        return $refundRequest;
    }

    /**
     * @param DiscountRequest $discountRequest
     * @param DealAgent $author
     * @param bool $is_confirm
     * @return DiscountConfirm
     */
    private function createDiscountConfirm(DiscountRequest $discountRequest, DealAgent $author, bool $is_confirm): discountConfirm
    {
        $discountConfirm = new DiscountConfirm();
        $discountConfirm->setAuthor($author);
        #$creationDate = clone $discountRequest->getCreated();
        $discountConfirm->setCreated($discountRequest->getCreated());
        $discountConfirm->setDiscountRequest($discountRequest);
        $discountConfirm->setStatus($is_confirm);

        $this->entityManager->persist($discountConfirm);
        #$this->entityManager->flush($discountConfirm);

        $discountRequest->setConfirm($discountConfirm);

        $this->entityManager->persist($discountRequest);
        $this->entityManager->flush($discountRequest);

        return $discountConfirm;
    }

    /**
     * @param Dispute $dispute
     * @param DealAgent $author
     * @return ArbitrageRequest
     */
    private function createArbitrageRequest(Dispute $dispute, DealAgent $author): ArbitrageRequest
    {
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();

        $arbitrageRequest = new ArbitrageRequest();

        if ($dispute->getArbitrageRequests()->last()) {
            /** @var ArbitrageRequest $arbitrageRequest */
            $arbitrageRequest = $dispute->getArbitrageRequests()->last();
            /** @var \DateTime $creationDate */
            $creationDate = clone $arbitrageRequest->getCreated();
            $creationDate->modify('+2 seconds');
        } else {
            $creationDate = clone new \DateTime();
            $creationDate->modify('-19 min');
        }

        $creationDate = clone new \DateTime();
        $arbitrageRequest->setCreated($creationDate);
        $arbitrageRequest->setDispute($dispute);
        $arbitrageRequest->setAuthor($author);
        $arbitrageRequest->setDisputeCycle($disputeCycle);

        $this->entityManager->persist($arbitrageRequest);
        $disputeCycle->addArbitrageRequest($arbitrageRequest);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush($arbitrageRequest);

        return $arbitrageRequest;
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @param User $author
     * @param bool $is_confirm
     * @return ArbitrageConfirm
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createArbitrageConfirm(ArbitrageRequest $arbitrageRequest, User $author, bool $is_confirm): ArbitrageConfirm
    {
        $arbitrageConfirm = new ArbitrageConfirm();
        $arbitrageConfirm->setAuthor($author);
        $arbitrageConfirm->setCreated($arbitrageRequest->getCreated());
        $arbitrageConfirm->setArbitrageRequest($arbitrageRequest);
        $arbitrageConfirm->setStatus($is_confirm);

        $this->entityManager->persist($arbitrageConfirm);

        $arbitrageRequest->setConfirm($arbitrageConfirm);

        $this->entityManager->persist($arbitrageRequest);
        $this->entityManager->flush($arbitrageRequest);

        return $arbitrageConfirm;
    }

    /**
     * @param Dispute $dispute
     * @param DealAgent $author
     * @param $amount
     * @return DiscountRequest
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createDiscountRequest(Dispute $dispute, DealAgent $author, $amount): DiscountRequest
    {
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();

        $discountRequest = new DiscountRequest();
        $discountRequest->setDispute($dispute);
        $discountRequest->setAmount($amount);
        $creationDate = clone new \DateTime();
        $discountRequest->setCreated($creationDate->modify('-20 min'));
        $discountRequest->setAuthor($author);
        $discountRequest->setDisputeCycle($disputeCycle);

        $this->entityManager->persist($discountRequest);
        $disputeCycle->addDiscountRequest($discountRequest);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush($discountRequest);

        return $discountRequest;
    }

    /**
     * @param Deal $deal
     * @param string $dispute_reason
     * @return Dispute
     */
    private function createDispute(Deal $deal, string $dispute_reason): Dispute
    {
        $date = new \DateTime();

        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        // Create new Dispute
        $dispute = new Dispute();
        $dispute->setReason($dispute_reason);
        $creationDate = clone new \DateTime();
        $date = $creationDate->modify('-1 hour');
        $dispute->setCreated($date);
        $dispute->setClosed(false);
        $dispute->setAuthor($customer);
        $dispute->setDeal($deal);

        $this->entityManager->persist($dispute);

        $disputeCycle = new DisputeCycle();
        $disputeCycle->setCreated($date);
        $disputeCycle->setDispute($dispute);

        $this->entityManager->persist($disputeCycle);
        $dispute->addDisputeCycle($disputeCycle);
        $this->entityManager->persist($dispute);

        $deal->setDispute($dispute);

        $this->entityManager->persist($deal);

        $this->entityManager->flush();

        return $dispute;
    }

    /**
     * @param Dispute $dispute
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function closeDispute(Dispute $dispute)
    {
        $dispute->setClosed(true);
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();
        $disputeCycle->setClosed(new \DateTime());

        $this->entityManager->persist($dispute);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush();

        return true;
    }
}