<?php

namespace Application\Fixture;

use Application\Entity\LegalEntity;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;

/**
 * Class LegalEntityFixtureLoader
 * @package Application\Fixture
 */
class LegalEntityFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getOrder()
    {
        return 65;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $legalEntity = new LegalEntity();
        #$legalEntity->setCivilLawSubject($setCivilLawSubject1);
        $legalEntity->setName('ООО Колумбийский сахар');
        $legalEntity->setLegalAddress('Москва, ул. Авангардная, 4, офис 56');
        $legalEntity->setInn('4217030520');
        $legalEntity->setKpp('123456');

        $entityManager->persist($legalEntity);

        $legalEntity = new LegalEntity();
        #$legalEntity->setCivilLawSubject($setCivilLawSubject2);
        $legalEntity->setName('ООО Simple Technology');
        $legalEntity->setLegalAddress('3-я улица Ямского Поля, дом.2');
        $legalEntity->setInn('4217030520');
        $legalEntity->setKpp('456789');

        $entityManager->persist($legalEntity);

        $legalEntity = new LegalEntity();
        $legalEntity->setName('ООО Partner');
        $legalEntity->setLegalAddress('2-я улица Ямского Поля, дом.22');
        $legalEntity->setInn('4217030520');
        $legalEntity->setKpp('456555');

        $entityManager->persist($legalEntity);

        $entityManager->flush();
    }
}