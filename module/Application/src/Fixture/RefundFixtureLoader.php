<?php

namespace Application\Fixture;

use Application\Entity\BankClientPaymentOrderFile;
use Application\Entity\DealAgent;
use Application\Entity\Dispute;
use Application\Entity\DisputeCycle;
use Application\Entity\NaturalPerson;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\Refund;
use Application\Service\Payment\CalculatedDataProvider;
use ModuleFileManager\Entity\File;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\GlobalSetting;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\CivilLawSubject;
use Application\Service\BankClient\BankClientManager;
use Doctrine\ORM\EntityManager;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;

/**
 * Class RefundFixtureLoader
 * @package Application\Fixture
 */
class RefundFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \DateTime
     */
    private $currentDate;

    /**
     * @var integer
     */
    private $percentOfFee;

    /**
     * @var BankClientPaymentOrderManager
     */
    private $bankClientPaymentOrderManager;


    /**
     * @return int
     */
    public function getOrder()
    {
        return 150;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function load(ObjectManager $manager)
    {
        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        $this->bankClientPaymentOrderManager = $this->container->get(BankClientPaymentOrderManager::class);
        $this->currentDate   = new \DateTime();

        $this->setPercentOfFee();

        foreach (DealFixtureLoader::DEALS_IN_STATUS_DISPUTE as $deal_array) {
            if (array_key_exists('outgoing_refund_payment_order', $deal_array)
                && true === $deal_array['outgoing_refund_payment_order']) {

                /** @var Deal $deal */
                $deal = $this->entityManager->getRepository(Deal::class)
                    ->findOneBy(array('name' => $deal_array['name']));

                $add_to_file = false;

                if (isset($deal_array['add_outgoing_refund_payment_order_to_file'])) {

                    $add_to_file = $deal_array['add_outgoing_refund_payment_order_to_file'];
                }

                /** @var Refund $refund */
                $refund = $this->createRefundWithPaymentOrder($deal, $add_to_file);

                if (array_key_exists('confirmation_refund_payment_order', $deal_array)
                    && true === $deal_array['confirmation_refund_payment_order']) {

                    $this->addConfirmationPaymentOrder($deal, $refund);
                }

                $this->entityManager->persist($refund);
                $this->entityManager->flush();
            }
        }
    }

    /**
     * @param Deal $deal
     * @param Refund $refund
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function addConfirmationPaymentOrder(Deal $deal, Refund $refund)
    {
        $paymentOrders = $refund->getPaymentOrders();

        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $paymentOrders->last();

        /** @var BankClientPaymentOrder $bankClientPaymentOrder */
        $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();

        $newPaymentOrder = $this->createPaymentOrder();

        // Создание $bankClientPaymentOrder
        $bankClientPaymentOrder2 = new BankClientPaymentOrder();
        $bankClientPaymentOrder2->setCreated($this->currentDate);
        $bankClientPaymentOrder2->setType(PaymentOrderManager::TYPE_INCOMING);
        $bankClientPaymentOrder2->setPaymentOrder($newPaymentOrder);

        $bankClientPaymentOrder2->setDocumentType('Платежное поручение');
        $bankClientPaymentOrder2->setDocumentNumber(4);
        #$bankClientPaymentOrder->setDocumentDate($currentDate->modify('-8 days'));
        $bankClientPaymentOrder2->setDocumentDate(date_format($this->currentDate, 'd.m.Y'));
        $bankClientPaymentOrder2->setAmount($bankClientPaymentOrder->getAmount());
        $bankClientPaymentOrder2->setPayerAccount('40702810590030001259');
        $bankClientPaymentOrder2->setPayer('ИНН 7813271867 ООО "Гарант Пэй"');
        $bankClientPaymentOrder2->setPayerInn('7813271867');
        $bankClientPaymentOrder2->setPayer1('ООО "Гарант Пэй"');
        $bankClientPaymentOrder2->setPayerKpp('781301001');
        $bankClientPaymentOrder2->setPayerBank1('ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" Г. САНКТ-ПЕТЕРБУРГ');
        $bankClientPaymentOrder2->setPayerBik('044030790');
        $bankClientPaymentOrder2->setPayerCorrAccount('30101810900000000790');

        $bankClientPaymentOrder2->setRecipientAccount($bankClientPaymentOrder->getRecipientAccount());
        $bankClientPaymentOrder2->setRecipient('Test Testov');
        $bankClientPaymentOrder2->setRecipientInn('4217030520');
        $bankClientPaymentOrder2->setRecipient1('Test Testov');
        $bankClientPaymentOrder2->setRecipientKpp(null);
        $bankClientPaymentOrder2->setRecipientBank1($bankClientPaymentOrder->getRecipientBank1());
        $bankClientPaymentOrder2->setRecipientBik($bankClientPaymentOrder->getRecipientBik());
        $bankClientPaymentOrder2->setRecipientCorrAccount($bankClientPaymentOrder->getRecipientCorrAccount());

        $bankClientPaymentOrder2->setPaymentPurpose($bankClientPaymentOrder->getPaymentPurpose());
        $bankClientPaymentOrder2->setPaymentType('01');
        $bankClientPaymentOrder2->setPriority('5');
        $bankClientPaymentOrder2->setDateOfReceipt(date_format($this->currentDate, 'd.m.Y'));
        $bankClientPaymentOrder2->setDateOfDebit(date_format($this->currentDate, 'd.m.Y'));
        $bankClientPaymentOrder2->setHash('0faeda34e4c8d8c463d2e464'.$deal->getId());

        /** @var BankClientPaymentOrderFile $bankClientPaymentOrderFile */
        $bankClientPaymentOrderFile = $this->creteBankClientPaymentOrderFile($deal);
        $bankClientPaymentOrder2->setBankClientPaymentOrderFile($bankClientPaymentOrderFile);

        $this->entityManager->persist($bankClientPaymentOrder2);

        $newPaymentOrder->setBankClientPaymentOrder($bankClientPaymentOrder2);

        $this->entityManager->persist($bankClientPaymentOrder2);

        $this->entityManager->persist($newPaymentOrder);

        $refund->addPaymentOrder($newPaymentOrder);

        $this->entityManager->persist($refund);

        $this->entityManager->flush();
    }

    /**
     * @param Deal $deal
     * @param bool $add_to_file
     * @return Refund
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createRefundWithPaymentOrder(Deal $deal, $add_to_file = false)
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $customer->getCivilLawSubject();
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $civilLawSubject->getPaymentMethods()->first();

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();

        $refund = new Refund();
        $refund->setPaymentMethod($paymentMethod);
        $refund->setCreated($this->currentDate);
        $refund->setDispute($deal->getDispute());
        $refund->setDisputeCycle($disputeCycle);

        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $this->createPaymentOrder();

        $refund->addPaymentOrder($paymentOrder);

        $this->entityManager->persist($refund);
        $disputeCycle->setRefund($refund);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush();

        /** @var BankClientPaymentOrder $bankClientPaymentOrder */
        $bankClientPaymentOrder = $this->createBankClientPaymentOrder($deal, $paymentOrder, $refund);

        if ($add_to_file) {
            /** @var BankClientPaymentOrderFile $bankClientPaymentOrderFile */
            $bankClientPaymentOrderFile = $this->creteBankClientPaymentOrderFile($deal);
            $bankClientPaymentOrder->setBankClientPaymentOrderFile($bankClientPaymentOrderFile);
        }

        $this->entityManager->persist($bankClientPaymentOrder);

        $paymentOrder->setBankClientPaymentOrder($bankClientPaymentOrder);

        $this->entityManager->persist($paymentOrder);

        $this->entityManager->flush();

        return $refund;
    }

    /**
     * @param Deal $deal
     * @param PaymentOrder $paymentOrder
     * @param Refund $refund
     * @return BankClientPaymentOrder
     */
    private function createBankClientPaymentOrder(Deal $deal, PaymentOrder $paymentOrder, Refund $refund)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Назначение платежа
        $payment_purpose = $this->bankClientPaymentOrderManager->generatePayOffPaymentPurpose(
            $deal,
            PayOffManager::PURPOSE_TYPE_REFUND,
            $payment->getReleasedValue()
        );

        $refundAmount = CalculatedDataProvider::roundAmount(
            $payment->getExpectedValue() - ($payment->getDealValue() * ($payment->getFee() / 100))
        );

        // Создание $bankClientPaymentOrder
        $bankClientPaymentOrder = new BankClientPaymentOrder();
        $bankClientPaymentOrder->setCreated($refund->getCreated());
        $bankClientPaymentOrder->setType(PaymentOrderManager::TYPE_OUTGOING);
        $bankClientPaymentOrder->setPaymentOrder($paymentOrder);

        $bankClientPaymentOrder->setDocumentType('Платежное поручение');
        $bankClientPaymentOrder->setDocumentNumber(4);
        #$bankClientPaymentOrder->setDocumentDate($currentDate->modify('-8 days'));
        $bankClientPaymentOrder->setDocumentDate(date_format($refund->getCreated(), 'd.m.Y'));
        $bankClientPaymentOrder->setAmount($refundAmount);

        // @TODO Данные из Settings
        $bankClientPaymentOrder->setPayerAccount('40702810590030001259');
        $bankClientPaymentOrder->setPayer('ИНН 7813271867 ООО "Гарант Пэй"');
        $bankClientPaymentOrder->setPayerInn('7813271867');
        $bankClientPaymentOrder->setPayer1('ООО "Гарант Пэй"');
        $bankClientPaymentOrder->setPayerKpp('781301001');
        $bankClientPaymentOrder->setPayerBank1('ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" Г. САНКТ-ПЕТЕРБУРГ');
        $bankClientPaymentOrder->setPayerBik('044030790');
        $bankClientPaymentOrder->setPayerCorrAccount('30101810900000000790');

        /** @var PaymentMethod $recipientPaymentMethod */
        $recipientPaymentMethod = $refund->getPaymentMethod();
        /** @var PaymentMethodBankTransfer $recipientPaymentMethodBankTransfer */
        $recipientPaymentMethodBankTransfer = $recipientPaymentMethod->getPaymentMethodBankTransfer();
        /** @var CivilLawSubject $recipientCivilLawSubject */
        $recipientCivilLawSubject = $recipientPaymentMethod->getCivilLawSubject();
        /** @var NaturalPerson $recipientNaturalPerson */
        $recipientNaturalPerson = $recipientCivilLawSubject->getNaturalPerson();

        $bankClientPaymentOrder->setRecipientAccount($recipientPaymentMethodBankTransfer->getAccountNumber());
        $bankClientPaymentOrder->setRecipient($recipientNaturalPerson->getFirstName() . ' ' . $recipientNaturalPerson->getLastName());
        $bankClientPaymentOrder->setRecipientInn(null);
        $bankClientPaymentOrder->setRecipient1($recipientNaturalPerson->getFirstName() . ' ' . $recipientNaturalPerson->getLastName());
        $bankClientPaymentOrder->setRecipientKpp(null);
        $bankClientPaymentOrder->setRecipientBank1($recipientPaymentMethodBankTransfer->getBankName());
        $bankClientPaymentOrder->setRecipientBik($recipientPaymentMethodBankTransfer->getBik());
        $bankClientPaymentOrder->setRecipientCorrAccount($recipientPaymentMethodBankTransfer->getCorrAccountNumber());

        $bankClientPaymentOrder->setPaymentPurpose($payment_purpose);
        $bankClientPaymentOrder->setPaymentType('01');
        $bankClientPaymentOrder->setPriority('5');
        $bankClientPaymentOrder->setDateOfReceipt(date_format($refund->getCreated(), 'd.m.Y'));
        $bankClientPaymentOrder->setDateOfDebit(date_format($refund->getCreated(), 'd.m.Y'));
        $bankClientPaymentOrder->setHash('0faeda34e4c8d8c463d2e4644ba020a4'.$deal->getId());

        return $bankClientPaymentOrder;
    }

    /**
     * @param Deal $deal
     * @return BankClientPaymentOrderFile
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function creteBankClientPaymentOrderFile(Deal $deal)
    {
        $file = new File();
        $file->setCreated($this->currentDate);
        $file->setName('payment_order_for_deal_'.$deal->getId());
        $file->setType('txt');
        $file->setOriginName('payment_order_for_deal_'.$deal->getId());
        $file->setPath('/bank-client');
        $file->setSize(123456);

        $this->entityManager->persist($file);
        $this->entityManager->flush($file);

        $bankClientPaymentOrderFile = new BankClientPaymentOrderFile();
        $bankClientPaymentOrderFile->setFile($file);
        $bankClientPaymentOrderFile->setFileName($file->getName());
        $bankClientPaymentOrderFile->setHash(microtime());

        $this->entityManager->persist($bankClientPaymentOrderFile);
        $this->entityManager->flush($bankClientPaymentOrderFile);

        return $bankClientPaymentOrderFile;
    }

    /**
     * @return PaymentOrder
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createPaymentOrder()
    {
        /** @var PaymentMethodType $paymentMethodType */
        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(array('name' => PaymentMethodType::BANK_TRANSFER));

        $paymentOrderCreateDate = clone $this->currentDate;

        // Создаём PaymentOrder
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = new PaymentOrder();
        $paymentOrder->setCreated($paymentOrderCreateDate->modify('-18 days'));
        $paymentOrder->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentOrder);
        $this->entityManager->flush($paymentOrder);

        return $paymentOrder;
    }

    private function setPercentOfFee()
    {
        /** @var GlobalSetting $feePercentage */
        $feePercentage = $this->entityManager->getRepository(GlobalSetting::class)
            ->findOneBy(array('name' => 'fee_percentage'));

        $this->percentOfFee = $feePercentage->getValue();
    }
}