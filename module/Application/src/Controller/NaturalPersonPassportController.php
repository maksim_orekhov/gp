<?php

namespace Application\Controller;

use Core\Controller\AbstractRestfulController;
use Application\Entity\NaturalPerson;
use Application\Entity\NaturalPersonPassport;
use Application\Form\NaturalPersonPassportForm;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use Application\Service\CivilLawSubject\NaturalPersonPassportManager;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Doctrine\ORM\EntityManager;
use Zend\View\Model\ViewModel;
use Zend\Hydrator\ClassMethods;
use Application\Service\CivilLawSubject\NaturalPersonManager;

/**
 * Class NaturalPersonPassportController
 * @package Application\Controller
 */
class NaturalPersonPassportController extends AbstractRestfulController
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * Auth Manager
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\CivilLawSubject\NaturalPersonManager
     */
    private $naturalPersonManager;

    /**
     * @var \Application\Service\CivilLawSubject\NaturalPersonDocumentManager
     */
    private $naturalPersonDocumentManager;

    /**
     * @var \Application\Service\CivilLawSubject\NaturalPersonPassportManager
     */
    private $naturalPersonPassportManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * @var array
     */
    private $config;

    /**
     * LegalEntityDocumentController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param NaturalPersonManager $naturalPersonManager
     * @param NaturalPersonDocumentManager $naturalPersonDocumentManager
     * @param NaturalPersonPassportManager $naturalPersonPassportManager
     * @param RbacManager $rbacManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                NaturalPersonManager $naturalPersonManager,
                                NaturalPersonDocumentManager $naturalPersonDocumentManager,
                                NaturalPersonPassportManager $naturalPersonPassportManager,
                                RbacManager $rbacManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->naturalPersonManager = $naturalPersonManager;
        $this->naturalPersonDocumentManager = $naturalPersonDocumentManager;
        $this->naturalPersonPassportManager = $naturalPersonPassportManager;
        $this->rbacManager = $rbacManager;
        $this->config = $config;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function getList()
    {
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // Whether the currant user yas role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            // Check permissions // Important! Operator has specific interface of view
            if (!($profile === false && $isOperator) && !($profile == 'profile' && !$isOperator)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $paginationParams = $this->getPaginationParams($this->config);
            $passports = $this->naturalPersonPassportManager->getCollectionPassport($user, $paginationParams, $isOperator);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginator = null;
        if (method_exists($passports , 'getPages')) {
            $paginator = $passports->getPages();
        }
        // Get Output
        $passportsOutput = $this->naturalPersonPassportManager
            ->extractPassportCollection($passports);

        $data = [
            'paginator'   => $paginator,
            'passports'   => $passportsOutput,
            'is_operator' => $isOperator,
        ];

        if ($isAjax) {
            return $this->message()->success("collection passport", $data);
        }

        $view =  new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person-passport/collection');

        return $view;
    }

    /**
     * @param mixed $id
     * @return \Zend\Http\Response|ViewModel
     */
    public function get($id)
    {
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            $passport = $this->naturalPersonPassportManager->getPassportById($id);
            $naturalPerson = $passport->getNaturalPersonDocument()->getNaturalPerson();

            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            // Check permissions // Important! Only Owner or Operator can see NaturalPersonPassport details
            if (!($profile === false && $isOperator) &&
                !($profile == 'profile' && $this->access('profile.own.edit', ['naturalPerson' => $naturalPerson]))) {
                return $this->redirect()->toRoute('not-authorized');
            }

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        // Extracted passport
        $passportOutput = $this->naturalPersonPassportManager
            ->getPassportsOutput($passport);

        $data = [
            'passport' => $passportOutput,
            'is_operator' => $isOperator
        ];

        //Ajax success
        if ( $isAjax ){
            return $this->message()->success("passport single", $data);
        }

        //Http success
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person-passport/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $form = new NaturalPersonPassportForm('document');

        try {
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $resultSavedForm = $this->saveFormData($formData);

                //Ajax success
                return $this->message()->success('create passport', $resultSavedForm);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $form = new NaturalPersonPassportForm('document');

        try {
            //remove check to file required
            $fileFilter = $form->getInputFilter()->get('file');
            $fileFilter->setRequired(false);
            //set data to form
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData['idPassport'] = $id;
                $resultSavedForm = $this->saveFormData($formData);
                //Ajax success
                return $this->message()->success('update passport', $resultSavedForm);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @return \Zend\Http\Response
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $passport = $this->naturalPersonPassportManager->getPassportById($id);
            $naturalPerson = $passport->getNaturalPersonDocument()->getNaturalPerson();
            $document = $passport->getNaturalPersonDocument();

            // Check permissions // Important! Only Owner or Operator can see NaturalPerson collection
            if (!$this->access('profile.own.edit', ['naturalPerson' => $naturalPerson])) {
                return $this->redirect()->toRoute('not-authorized');
            }

            // Deleting
            $this->naturalPersonDocumentManager->remove($document);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        //Ajax success
        if ($isAjax) {
            return $this->message()->success('Документ удален');
        }
        //Http success
        // @TODO Куда возвращать?
        return $this->redirect()->toRoute(
            'user-profile/natural-person-single',
            ['id' => $naturalPerson->getId()]
        );
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function createFormAction()
    {
        $formDataInit = $this->processFormDataInit();

        $form = new NaturalPersonPassportForm('document');

        try {
            if ( !isset($formDataInit['naturalPerson']) || !$formDataInit['naturalPerson'] instanceof NaturalPerson){
                throw new \Exception('bad data provided: not found natural person');
            }
            $naturalPerson = $formDataInit['naturalPerson'];
            $form->get('idNaturalPerson')->setValue($naturalPerson->getId());

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $filesData = $this->collectIncomingData()['filesData'];
                if (\count($filesData) && $filesData['file']['error'] !== 4) {   //4 - Файл не был загружен
                    $postData = array_merge_recursive(
                        $postData,
                        $filesData
                    );
                }
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idNaturalPerson'] = $naturalPerson->getId();
                    $resultSavedForm = $this->saveFormData($formData);

                    //http success
                    return $this->redirect()->toRoute(
                        'user-profile/natural-person-single',
                        ['id' => $resultSavedForm['naturalPerson']->getId()]
                    );
                }
            }
        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
        $form->prepare();
        $view = new TwigViewModel([
            'passportForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person-passport/create-form');

        return $view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function editFormAction()
    {
        $formDataInit = $this->processFormDataInit();

        $form = new NaturalPersonPassportForm('document');

        try {
            if ( !isset($formDataInit['passport']) || !$formDataInit['passport'] instanceof NaturalPersonPassport){
                throw new \Exception('bad data provided: not found passport');
            }

            $passport = $formDataInit['passport'];
            // Extracted passport
            $passportOutput = $this->naturalPersonPassportManager
                ->getPassportsOutput($formDataInit['passport']);

            $form->setData([
                'passport_serial_number' => $passport->getPassportSerialNumber(),
                'date_issued' => $passport->getDateIssued()->format('d.m.Y'),
                'passport_issue_organisation' => $passport->getPassportIssueOrganisation(),
                'date_expired' => $passport->getDateExpired()->format('d.m.Y'),
            ]);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $filesData = $this->collectIncomingData()['filesData'];
                if (count($filesData) && $filesData['file']['error'] !== 4) {   //4 - Файл не был загружен
                    $postData = array_merge_recursive(
                        $postData,
                        $filesData
                    );
                }
                //remove check to file required
                $fileFilter = $form->getInputFilter()->get('file');
                $fileFilter->setRequired(false);
                //set data to form
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idPassport'] = $passport->getId();
                    $resultSavedForm = $this->saveFormData($formData);

                    //http success
                    return $this->redirect()->toRoute(
                        'user-profile/natural-person-single',
                        ['id' => $resultSavedForm['naturalPerson']->getId()]
                    );
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'passport' => $passportOutput,
            'passportForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person-passport/edit-form');

        return $view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function deleteFormAction()
    {
        $formDataInit = $this->processFormDataInit();

        try {
            if ( !isset($formDataInit['passport']) || !$formDataInit['passport'] instanceof NaturalPersonPassport){
                throw new \Exception('bad data provided: not found passport');
            }
            // Extracted passport
            $passportOutput = $this->naturalPersonPassportManager
                ->getPassportsOutput($formDataInit['passport']);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        $view = new TwigViewModel([
            'passport' => $passportOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person-passport/delete-form');

        return $view;
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \RuntimeException
     */
    public function formInitAction()
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \RuntimeException
     */
    private function processFormDataInit()
    {
        //step 1
        $incomingData = $this->collectIncomingData();

        //check
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            //process data for create
            $naturalPerson = null;
            if ( isset($incomingData['idNaturalPerson']) ){
                $naturalPerson = $this->naturalPersonManager->getBasedOnPattern((int) $incomingData['idNaturalPerson']);
            }
            $allowedNaturalPersons = $this->naturalPersonManager->getAllowedNaturalPersonByUser($user);

            //process data for update or delete
            $passport = null;
            if ( isset($incomingData['idPassport']) ){
                $passport = $this->naturalPersonPassportManager->getPassportById((int) $incomingData['idPassport']);
            }
            $allowedPassports = $this->naturalPersonPassportManager->getAllowedPassportByUser($user);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        //step 2
        return [
            //create
            'naturalPerson' => $naturalPerson,
            'allowedNaturalPersons' => $allowedNaturalPersons,
            //edit
            'passport' => $passport,
            'allowedPassports' => $allowedPassports,
        ];
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    private function saveFormData($data)
    {
        if ( isset($data['idPassport']) ) {
            //update
            if ((int) $data['idPassport'] <= 0 ) {
                throw new \Exception('bad data provided: not found passport id');
            }

            $passport = $this->naturalPersonPassportManager->getPassportById((int) $data['idPassport']);

            if( $passport === null) {
                throw new \Exception('bad data provided: not found passport by id');
            }

            $document = $this->naturalPersonDocumentManager
                ->update($passport->getNaturalPersonDocument(), $data);

            $result = [
                'passport' => $document->getNaturalPersonPassport(),
                'naturalPerson' => $document->getNaturalPerson()
            ];
        } else {
            //create
            if ( !isset($data['idNaturalPerson']) || (int) $data['idNaturalPerson'] <= 0){
                throw new \Exception('bad data provided: not found natural person id');
            }

            $naturalPerson = $this->naturalPersonManager->getBasedOnPattern((int) $data['idNaturalPerson']);

            if( $naturalPerson === null) {
                throw new \Exception('bad data provided: not found natural person by id');
            }

            $document = $this->naturalPersonDocumentManager
                ->create($naturalPerson, NaturalPersonPassport::class, $data);
            $passport = $document->getNaturalPersonPassport();

            $result = [
                'passport' => $passport,
                'naturalPerson' => $naturalPerson
            ];
        }

        return $result;
    }
}