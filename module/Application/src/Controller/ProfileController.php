<?php

namespace Application\Controller;

use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealTokenManager;
use Application\Service\NdsTypeManager;
use Application\Service\UserManager;
use Core\Service\Auth\AuthService;
use Core\Service\CodeOperationInterface;
use Core\Service\TwigViewModel;
use Core\Form\EmailEditForm;
use Core\Form\OperationConfirmForm;
use Core\Form\PhoneEditForm;
use ModuleBank\Service\BankManager;
use ModuleRbac\Service\RbacManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Core\Form\UserForm;
use Application\Form\FileUploadForm;
use Zend\Json\Json;
use Core\Service\OperationConfirmManager;

/**
 * Class ProfileController
 * @package Application\Controller
 */
class ProfileController extends AbstractActionController implements CodeOperationInterface
{
    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Auth\AuthService
     */
    private $authService;

    /**
     * @var \Application\Service\CivilLawSubject\CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * $operationConfirmManager
     * @var \Core\Service\OperationConfirmManager
     */
    private $operationConfirmManager;

    /**
     * @var BankManager
     */
    private $bankManager;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DealTokenManager
     */
    private $dealTokenManager;

    /**
     * @var array
     */
    private $config;

    /**
     * ProfileController constructor.
     * @param UserManager $userManager
     * @param AuthService $authService
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param BankManager $bankManager
     * @param NdsTypeManager $ndsTypeManager
     * @param OperationConfirmManager $operationConfirmManager
     * @param RbacManager $rbacManager
     * @param DealManager $dealManager
     * @param DealTokenManager $dealTokenManager
     * @param $config
     */
    public function __construct(UserManager $userManager,
                                AuthService $authService,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                BankManager $bankManager,
                                NdsTypeManager $ndsTypeManager,
                                OperationConfirmManager $operationConfirmManager,
                                RbacManager $rbacManager,
                                DealManager $dealManager,
                                DealTokenManager $dealTokenManager,
                                $config)
    {
        $this->userManager              = $userManager;
        $this->authService              = $authService;
        $this->civilLawSubjectManager   = $civilLawSubjectManager;
        $this->bankManager              = $bankManager;
        $this->ndsTypeManager           = $ndsTypeManager;
        $this->operationConfirmManager  = $operationConfirmManager;
        $this->rbacManager              = $rbacManager;
        $this->dealManager              = $dealManager;
        $this->dealTokenManager         = $dealTokenManager;
        $this->config                   = $config;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     *
     * @throws \Exception
     * @TODO Требует рефакторинга!
     */
    public function indexAction()
    {
        $userForm = new UserForm();

        $isPost = $this->getRequest()->isPost();
        $isGet = $this->getRequest()->isGet();
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $accepted_params = $this->params()->fromRoute();

        $requestedUser = null;
        $authorizedUser = null;

        try {
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');

            if (isset($accepted_params['login'])) {
                /** @var User $requestedUser */ // For route as /profile/login // Can throw Exception
                $requestedUser = $this->userManager->getUserByLogin($accepted_params['login']);
            } else {
                /** @var User $requestedUser */ // Can throw Exception
                $requestedUser = $this->userManager->getUserByLogin($this->authService->getIdentity());
            }
        }
        catch (\Throwable $t) {
            return $this->message()->error($t->getMessage());
        }

        $isMyProfile = (
            !isset($accepted_params['login'])
            || (isset($accepted_params['login']) && $accepted_params['login'] === $this->authService->getIdentity())
        );

        $page = $this->params()->fromQuery('page', 1);
        $perPage = $this->params()->fromQuery('per_page', $this->config['default_pagination_preferences']['number_per_page']);
        $order = $this->params()->fromQuery('order');
        $sort = $this->params()->fromQuery('sort', $this->config['default_pagination_preferences']['sorting_order']);

        $params = [];
        $params['order'] = ($order) ? [$order => $sort] : ['id' => $this->config['default_pagination_preferences']['sorting_order']];
        $params['per_page'] = $perPage;
        $params['page'] = $page;

        if ($isGet && $isAjax) {// обнуляем параметры пагинации при Ajax что бы отдовать на фронт полную коллекцию
            $params = [];
        }

        // Готовим объекты
        $userOutput = $this->userManager->getUserOutputForSingle($requestedUser);
        // Get user's naturalPersons as array
        $naturalPersonsOutput = $this->civilLawSubjectManager
            ->getNaturalPersonsForUser($requestedUser, $params, true);
        // Get user's soleProprietors as array
        $soleProprietorsOutput = $this->civilLawSubjectManager
            ->getSoleProprietorForUser($requestedUser, $params, true);
        // Get user's legalEntities as array
        $legalEntitiesOutput = $this->civilLawSubjectManager
            ->getLegalEntitiesForUser($requestedUser, $params, true);

        $tokensOutput = $this->dealTokenManager->getUsersCounteragentTokens($requestedUser,true);

        // phoneConfirmForm creation
        $phoneConfirmForm = new OperationConfirmForm();
        // phoneEditForm creation
        $phoneEditForm = new PhoneEditForm();
        $phoneEditForm->setData(['current_phone' => $userOutput['phone']]);
        // emailEditForm creation
        $emailEditForm = new EmailEditForm();
        $emailEditForm->setData(['current_email' => $userOutput['email']]);

        /** @var \DateTime $tokenCreatedDate */
        $tokenCreatedDate = $requestedUser->getEmail()->getConfirmationTokenCreationDate();

        $is_email_token_expired = null;

        if ($requestedUser->getEmail()->getConfirmationToken()) {
            /** @var \DateTime $confirmationTokenCreationDate */
            $confirmationTokenCreationDate = $requestedUser->getEmail()->getConfirmationTokenCreationDate();
            $tokenCreationDate = clone $confirmationTokenCreationDate;
            $tokenExpiredDate = $tokenCreationDate->modify('+'.$this->config['tokens']['email_confirmation_token_expiration_time'].' seconds');
            $is_email_token_expired = $tokenExpiredDate < new \DateTime();
        }

        /** @var array $user_allowed_request_phone_confirmation_code_status */
        $user_allowed_request_phone_confirmation_code_status = $this->operationConfirmManager
            ->userAllowedRequestCodeStatus(
            $requestedUser->getLogin(),
            self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION
        );

        /** @var bool $user_has_active_confirmation_code */
        $user_has_active_confirmation_code = $this->operationConfirmManager
            ->isUserHasActiveCodeOfSpecifiedType(
                $requestedUser,
                self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION
            );

        /** @var array $user_allowed_request_phone_change_code_status */
        $user_allowed_request_phone_change_code_status = $this->operationConfirmManager
            ->userAllowedRequestCodeStatus(
                $requestedUser->getLogin(),
                self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE
            );

        /** @var array $deal_types */
        $deal_types = $this->dealManager->getDealTypesAsArray();
        /** @var array $fee_payer_options */
        $fee_payer_options = $this->dealManager->getActiveFeePayerOptionsArray();

        $view = new TwigViewModel([
            'is_operator'       => $isOperator,
            'userProfile'       => $userOutput,
            'isMyProfile'       => $isMyProfile,
            'naturalPersons'    => $naturalPersonsOutput,
            'soleProprietors'   => $soleProprietorsOutput,
            'legalEntities'     => $legalEntitiesOutput,
            'tokens'            => $tokensOutput,
            'deal_types'        => $deal_types,
            'fee_payer_options' => $fee_payer_options,
            'userForm'          => $userForm,
            'phoneConfirmForm'  => $phoneConfirmForm,
            'phoneEditForm'     => $phoneEditForm,
            'emailEditForm'     => $emailEditForm,
            'is_email_token_expired' => $is_email_token_expired,
            'token_created_date' => $tokenCreatedDate ? $tokenCreatedDate->getTimestamp() : null,
            'email_confirmation_token_request_period' => $this->config['tokens']['email_confirmation_token_request_period'],
            'user_has_active_confirmation_code' => $user_has_active_confirmation_code,
            'user_allowed_request_phone_confirmation_code_status' => $user_allowed_request_phone_confirmation_code_status,
            'user_allowed_request_phone_change_code_status' => $user_allowed_request_phone_change_code_status,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        // Don't pass other users
        if ($isMyProfile === false) {
            return $view;
        }

        // GET
        if ($isGet && $isAjax) {
            $csrf   = $userForm->get('csrf')->getValue();
            /** @var array $payment_methods_array */
            $payment_methods_array = $this->userManager->getUsersPaymentMethodOutput($requestedUser);
            try {
                $popularBanks = $this->bankManager->getPopularBanks();
                $popularBanksOutput = $this->bankManager->extractBankCollection($popularBanks, true);
                $ndsTypes = $this->ndsTypeManager->getNdsTypes();
                $ndsTypesOutput =  $this->ndsTypeManager->extractNdsTypeCollection($ndsTypes);
            }
            catch (\Throwable $t){

                return $this->message()->error($t->getMessage());
            }

            $data   = [
                'csrf'              => $csrf,
                'naturalPersons'    => $naturalPersonsOutput,
                'soleProprietors'   => $soleProprietorsOutput,
                'legalEntities'     => $legalEntitiesOutput,
                'payment_methods'   => $payment_methods_array,
                'tokens'            => $tokensOutput,
                'deal_types'        => $deal_types,
                'fee_payer_options' => $fee_payer_options,
                // Параметры сортировки
                'order'          => $order, // свойство, по которому отсортированы сделки
                'sorting_order'  => $sort, // порядок (по умолчанию - DESC)
                //справочники
                'handbooks' => [
                    'ndsTypes' => $ndsTypesOutput,
                    'popularBanks' => $popularBanksOutput,
                ],
                'is_email_token_expired' => $is_email_token_expired,
                'token_created_date' => $tokenCreatedDate ? $tokenCreatedDate->getTimestamp() : null,
                'email_confirmation_token_request_period' => $this->config['tokens']['email_confirmation_token_request_period'],
                'user_has_active_confirmation_code' => $user_has_active_confirmation_code,
                'user_allowed_request_phone_confirmation_code_status' => $user_allowed_request_phone_confirmation_code_status,
                'user_allowed_request_phone_change_code_status' => $user_allowed_request_phone_change_code_status,
                'current_phone' => $requestedUser->getPhone() ? $requestedUser->getPhone()->getPhoneNumber() : null,
                'current_email' => $requestedUser->getEmail()->getEmail(),
            ];
            return $this->message()->success('CSRF token return', $data);
        }

        if ($isGet && !$isAjax) {
            return $view;
        }

        // POST
        if (!$isPost) {
            return $view;
        }

        $data = $this->params()->fromPost();
        $dataFiles = $this->getRequest()->getFiles()->toArray();

        if (isset($dataFiles['avatar']) && $dataFiles['avatar']['error'] === 0) {
            $data = array_merge_recursive($data, $dataFiles);
        }

        if ($isAjax) {
            $content = $this->getRequest()->getContent();
            $data = Json::decode($content, Json::TYPE_ARRAY);
        }

        $userForm->setData($data);

        if (!$userForm->isValid() && !$isAjax) {
            return $view;
        }

        if (!$userForm->isValid() && $isAjax) {
            return $this->message()->invalidFormData($userForm->getMessages());
        }

        //Save
        $data = $userForm->getData();

        try{
            $this->userManager->editUser($requestedUser, $data);
        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }


        if ($isAjax) {
            return $this->message()->success('Профиль успешно изменен');
        }

        return $this->redirect()->toRoute('profile');
    }

    /**
     * Показ файлов субъектов
     */
    public function showFileAction()
    {
        $civilId = (int) $this->params()->fromRoute('civilId');
        $fileId = $this->params()->fromRoute('fileId');

        $civilLawSubject = $this->civilLawSubjectManager->get($civilId);
        if ($civilLawSubject === null) {
            return $this->message()->error('Not found civil law subject');
        }

        // Check permissions // Important! Only owner can see his file
        if ( !$this->access('profile.own.edit', ['civilLawSubject' => $civilLawSubject]) ) {
            return $this->redirect()->toRoute('not-authorized');
        }

        $fileShowView = $this->forward()->dispatch('ModuleFileManager\Controller\FileController', array(
            'id' => $fileId
        ));

        return $fileShowView;
    }

    /**
     * Показ аватарок
     */
    public function showAvatarFileAction()
    {
        $fileId = $this->params()->fromRoute('fileId', -1);

        try{
            $user = $this->userManager->getUserByAvatarId($fileId);
        }catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
        // Check permissions
        if ( !$this->access('profile.own.edit', ['user' => $user]) ) {
            return $this->redirect()->toRoute('not-authorized');
        }

        $fileShowView = $this->forward()->dispatch('ModuleFileManager\Controller\FileController', array(
            'id' => $fileId
        ));

        return $fileShowView;
    }

    /**
     * @return ViewModel
     */
    public function filesAction()
    {
        return new ViewModel([]);
    }

    /**
     * @return ViewModel
     */
    public function uploadAction()
    {
        $form = new FileUploadForm();

        return new ViewModel([
            'form' => $form
        ]);
    }
}
