<?php

namespace Application\Controller;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\Dispute;
use Application\Entity\RefundRequest;
use Application\Form\RefundRequestForm;
use Application\Form\RequestConfirmForm;
use Application\Form\RequestRefuseForm;
use Application\Listener\ArbitrageRequestListener;
use Application\Listener\RefundConfirmSuccessListener;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Application\Entity\User;
use Application\Form\PaymentMethod\BankTransferForm;
use Application\Form\RefundForm;
use Application\Service\Deal\DealManager;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Dispute\RefundManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;

/**
 * Class RefundController
 * @package Application\Controller
 */
class RefundController extends AbstractRestfulController
{
    /**
     * @var RefundManager
     */
    private $refundManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var DealManager
     */
    private $dealManager;
    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;
    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var ArbitrageRequestListener
     */
    private $arbitrageRequestListener;

    /**
     * @var RefundConfirmSuccessListener
     */
    private $refundConfirmSuccessListener;

    /**
     * RefundController constructor.
     * @param RefundManager $refundManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param DealManager $dealManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param PaymentManager $paymentManager
     * @param DisputeManager $disputeManager
     * @param ArbitrageRequestListener $arbitrageRequestListener
     * @param RefundConfirmSuccessListener $refundConfirmSuccessListener
     */
    public function __construct(RefundManager $refundManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                DealManager $dealManager,
                                PaymentMethodManager $paymentMethodManager,
                                PaymentManager $paymentManager,
                                DisputeManager $disputeManager,
                                ArbitrageRequestListener $arbitrageRequestListener,
                                RefundConfirmSuccessListener $refundConfirmSuccessListener)
    {
        $this->refundManager = $refundManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->dealManager = $dealManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->paymentManager = $paymentManager;
        $this->disputeManager = $disputeManager;
        $this->arbitrageRequestListener = $arbitrageRequestListener;
        $this->refundConfirmSuccessListener = $refundConfirmSuccessListener;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function refundAction()
    {
        $disputeId = (int) $this->params()->fromRoute('idDispute', -1);
        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Dispute $dispute */ // Can throw Exception
            $dispute = $this->disputeManager->getDispute($disputeId);

            // Check permissions
            if(! $this->refundManager->checkPermissionRefund($user, $dispute)){
                return $this->redirect()->toRoute('not-authorized');
            }

            //если в refundManager вызвать disputeManager то произойдет рекурсия поэтому просто сетим в переменную
            $this->refundManager->setDisputeOutput($this->disputeManager->getDisputeOutput($dispute));

            //проверка был ли уже выполнен возврат
            $is_refund_done = $this->refundManager->isRefundDone($dispute);
            $paymentMethodRefund = $this->refundManager->getPaymentMethodRefundByDeal($dispute->getDeal());
            $paymentMethodRefundOutput = $this->refundManager->getPaymentMethodRefundOutput($paymentMethodRefund);
            $debitPaymentOrdersOutput = $this->paymentManager->getIncomingPaymentTransactionsForDeal($dispute->getDeal());

            //форма для генерации платежки возврата
            $refundForm = new RefundForm($paymentMethodRefund, $dispute);
            //доп форма на случай если нет payment method для возврата средств
            $bankTransferForm = new BankTransferForm();

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            //форма генерации платежки
            if ($postData && !$is_refund_done && $postData['type_form'] === RefundForm::TYPE_FORM) {
                //set data to form
                $refundForm->setData($postData);
                //validate form
                if ($refundForm->isValid()) {
                    $formData = $refundForm->getData();

                    $this->refundManager->createRefund($dispute, $formData);

                    return $this->redirect()->toRoute('dispute-single',['id'=>$dispute->getId()]);
                }
                if ($this->getRequest()->isXmlHttpRequest()) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($refundForm->getMessages());
                }
            }

            //форма добавления bank transfer
            if ($postData && !$is_refund_done && $postData['type_form'] === BankTransferForm::TYPE_FORM) {
                // платит всегда customer поэтому нужен его civilLawSubject
                /** @var CivilLawSubject $civilLawSubject */
                $civilLawSubject = $dispute->getDeal()->getCustomer()->getCivilLawSubject();
                $postData['civil_law_subject_id'] = $civilLawSubject->getId();
                $postData['payment_method_type'] = PaymentMethodType::BANK_TRANSFER;
                //set data to form
                $bankTransferForm->setData($postData);
                //validate form
                if ($bankTransferForm->isValid()) {
                    $formData = $bankTransferForm->getData();

                    $this->paymentMethodManager->createBankDetailsCustomerForReturnPaymentOrder($formData, $civilLawSubject);

                    return $this->redirect()->toRoute('dispute-single',['id'=>$dispute->getId()]);
                }
                if($this->getRequest()->isXmlHttpRequest()) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($bankTransferForm->getMessages());
                }
            }
        }
        catch (\Throwable $t) {
            return $this->message()->error($t->getMessage());
        }

        $refundForm->prepare();
        $bankTransferForm->prepare();
        $view = new TwigViewModel([
            'refund_amount' => $this->refundManager->getRefundAmountByDispute($dispute),
            'payment_method_bank_transfer' => $paymentMethodRefundOutput,
            'debitPaymentOrders' => $debitPaymentOrdersOutput,
            'refundForm' => $refundForm,
            'bankTransferForm' => $bankTransferForm,
            'is_refund_done' => $is_refund_done,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function refundRequestSingleAction()
    {
        try{
            $formDataInit = $this->processFormDataInit();
            /** @var User $user */
            $user = $formDataInit['user'];
            //not operator
            if ($this->refundManager->isOperator($user)) {
                return $this->redirect()->toRoute('not-authorized');
            }
            //check data
            if (!isset($formDataInit['refundRequest']) || !$formDataInit['refundRequest'] instanceof RefundRequest) {

                throw new \Exception('bad data provided: RefundRequest not found');
            }
            //check permission
            if (!$this->refundManager->checkPermissionRefundSingle($user, $formDataInit['refundRequest'])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var RefundRequest $refundRequest */
            $refundRequest = $formDataInit['refundRequest'];

            $refundConfirmForm = new RequestConfirmForm();
            $refundConfirmForm->setAttribute('action', $this->url()->fromRoute('deal-refund-confirm', ['idRequest' => $refundRequest->getId()]));

            $refundRefuseForm = new RequestRefuseForm();
            $refundRefuseForm->setAttribute('action', $this->url()->fromRoute('deal-refund-refuse', ['idRequest' => $refundRequest->getId()]));

            $refundRequestOutput = $this->refundManager->getRefundRequestOutput($refundRequest);
            $disputeOutput = $this->disputeManager->getDisputeOutput($refundRequest->getDispute());
            $dealOutput = $this->dealManager->getDealOutputForSingle($refundRequest->getDispute()->getDeal());
        }
        catch (\Throwable $t) {
            return $this->message()->error($t->getMessage());
        }

        $refundConfirmForm->prepare();
        $refundRefuseForm->prepare();
        $view = new TwigViewModel([
            'refundConfirmForm'=>$refundConfirmForm,
            'refundRefuseForm'=>$refundRefuseForm,
            'refundRequest'=>$refundRequestOutput,
            'dispute'=>$disputeOutput,
            'deal'=>$dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function refundRequestAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try{
            $formDataInit = $this->processFormDataInit();
            /** @var User $user */
            $user = $formDataInit['user'];
            //not operator
            if ($this->refundManager->isOperator($user)) {
                return $this->redirect()->toRoute('not-authorized');
            }
            //check data
            if (!isset($formDataInit['deal']) || !$formDataInit['deal'] instanceof Deal){

                throw new \Exception('bad data provided: Deal not found');
            }
            //check permission
            if (!$this->refundManager->checkPermissionRefundRequest($user, $formDataInit['deal'])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var Deal $deal */
            $deal = $formDataInit['deal'];

            $refundRequestForm = new RefundRequestForm($deal);
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $this->refundManager->setDisputeOutput($disputeOutput);
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData)
                && $postData['type_form'] === RefundRequestForm::TYPE_FORM) {

                //set data to form
                $refundRequestForm->setData($postData);
                //validate form
                if ($refundRequestForm->isValid()) {
                    #$formData = $refundRequestForm->getData();
                    $author = $this->dealManager->getUserAgentByDeal($user, $deal);

                    $refundRequest = $this->refundManager->createRefundRequest($deal, $author);

                    if($isAjax) {
                        $refundRequestOutput = $this->refundManager->getRefundRequestOutput($refundRequest);
                        // Return Json with ERROR status
                        return $this->message()->success(
                            'Refund request created',
                            ['refundRequest' => $refundRequestOutput]
                        );
                    }

                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                //ajax response
                if($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($refundRequestForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){
            return $this->message()->error($t->getMessage());
        }

        $refundRequestForm->prepare();
        $view = new TwigViewModel([
            'refundRequestForm' => $refundRequestForm,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function refundConfirmAction()
    {

        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $formDataInit = $this->processFormDataInit();
            /** @var User $user */
            $user = $formDataInit['user'];
            //not operator
            if ($this->refundManager->isOperator($user)) {
                return $this->redirect()->toRoute('not-authorized');
            }
            //check data
            if (!isset($formDataInit['refundRequest']) || !$formDataInit['refundRequest'] instanceof RefundRequest) {

                throw new \Exception('bad data provided: RefundRequest not found');
            }
            //check permission
            if (!$this->refundManager->checkPermissionRefundConfirm($user, $formDataInit['refundRequest'])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var RefundRequest $refundRequest */
            $refundRequest = $formDataInit['refundRequest'];
            /** @var Deal $deal */
            $deal = $refundRequest->getDispute()->getDeal();
            //check of confirmation
            if ($refundRequest->getConfirm()) {

                throw new \Exception('On the current request already decided');
            }

            $refundConfirmForm = new RequestConfirmForm();
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $this->refundManager->setDisputeOutput($disputeOutput);
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);
            $refundRequestOutput = $this->refundManager->getRefundRequestOutput($refundRequest);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData) && $postData['type_form'] === RequestConfirmForm::TYPE_FORM) {
                //set data to form
                $refundConfirmForm->setData($postData);
                //validate form
                if ($refundConfirmForm->isValid()) {
                    $author = $this->dealManager->getUserAgentByDeal($user, $deal);

                    $refundConfirm = $this->refundManager->createRefundConfirm($refundRequest, $author);
                    //@TODO отключил автоматику согласно задаче GP-1575
                    //if status success
//                    if ( $refundConfirm->isStatus() ) {
//                        // Subscribe on event "onDiscountConfirmSuccess"
//                        $this->refundConfirmSuccessListener->subscribe();
//                        // Trigger event "onDiscountConfirmSuccess"
//                        $this->refundConfirmSuccessListener->trigger($refundConfirm);
//                    }

                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                //ajax response
                if($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($refundConfirmForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $refundConfirmForm->prepare();
        $view = new TwigViewModel([
            'refundConfirmForm' => $refundConfirmForm,
            'refundRequest' => $refundRequestOutput,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function refundRefuseAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $formDataInit = $this->processFormDataInit();
            /** @var User $user */
            $user = $formDataInit['user'];
            //not operator
            if ($this->refundManager->isOperator($user)) {
                return $this->redirect()->toRoute('not-authorized');
            }
            //check data
            if (!isset($formDataInit['refundRequest']) || !$formDataInit['refundRequest'] instanceof RefundRequest) {

                throw new \Exception('bad data provided: RefundRequest not found');
            }
            //check permission
            if (!$this->refundManager->checkPermissionRefundRefuse($user, $formDataInit['refundRequest'])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var RefundRequest $refundRequest */
            $refundRequest = $formDataInit['refundRequest'];
            /** @var Deal $deal */
            $deal = $refundRequest->getDispute()->getDeal();
            //check of confirmation
            if ($refundRequest->getConfirm()) {

                throw new \Exception('On the current request already decided');
            }

            $refundRefuseForm = new RequestRefuseForm();
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $this->refundManager->setDisputeOutput($disputeOutput);
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);
            $refundRequestOutput = $this->refundManager->getRefundRequestOutput($refundRequest);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData) && $postData['type_form'] === RequestRefuseForm::TYPE_FORM) {
                // set data to form
                $refundRefuseForm->setData($postData);
                // validate form
                if ($refundRefuseForm->isValid()) {
                    $author = $this->dealManager->getUserAgentByDeal($user, $deal);

                    $this->refundManager->createRefundRefuse($refundRequest, $author);

                    if ($this->refundManager->checkRefundRefusesOverlimit($deal->getDispute())) {
                        // Subscribe on event "onRequestDeclineOverlimit"
                        $this->arbitrageRequestListener->subscribe();
                        // Trigger event "onRequestDeclineOverlimit"
                        $this->arbitrageRequestListener->trigger(
                            $deal,
                            $this->dealManager->getUserAgentByDeal($user, $deal),
                            $disputeOutput
                        );
                    }

                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                //ajax response
                if($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($refundRefuseForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $refundRefuseForm->prepare();
        $view = new TwigViewModel([
            'refundRefuseForm' => $refundRefuseForm,
            'refundRequest' => $refundRequestOutput,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $allowedUser = null;

            $deal = null;
            if (isset($incomingData['idDeal'])){

                /** @var $deal $deal */
                $deal = $this->dealManager
                    ->getDealById((int) $incomingData['idDeal']);

                // Check if $deal belongs to current user
                if ($user && $deal && !DealManager::isUserPartOfDeal($deal, $user)) {

                    $deal = null;
                }
            }
            $allowedDeals = $this->dealManager->getAllowedDealByUser($user);
            $allowedDeals = !empty($allowedDeals) ? $allowedDeals : null;

            $refundRequest = null;
            if (isset($incomingData['idRequest'])){
                /** @var RefundRequest $refundRequest */
                $refundRequest = $this->refundManager
                    ->getRefundRequestById((int)$incomingData['idRequest']);
            }

            $allowedRefundRequests = $this->refundManager->getAllowedRefundRequestsByUser($user);
            $allowedRefundRequests = !empty($allowedRefundRequests) ? $allowedRefundRequests : null;

        } catch (\Throwable $t){

            throw new \Exception($t->getMessage());
        }

        return [
            'user' => $user,
            'allowedUser' => $allowedUser,
            'deal' => $deal,
            'allowedDeals' => $allowedDeals,
            'refundRequest' => $refundRequest,
            'allowedRefundRequests' => $allowedRefundRequests,
        ];
    }
}