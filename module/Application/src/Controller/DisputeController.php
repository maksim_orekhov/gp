<?php

namespace Application\Controller;

use Application\Entity\DealAgent;
use Application\Entity\Dispute;
use Application\Form\DiscountForm;
use Application\Form\DisputeCommentForm;
use Application\Form\DisputeFileUploadForm;
use Application\Form\DisputeOpenForm;
use Application\Form\DisputeOpenSearchForm;
use Application\Form\DisputeSearchForm;
use Application\Form\PaymentMethod\BankTransferForm;
use Application\Form\RefundForm;
use Application\Form\TribunalForm;
use Application\Service\Deal\DealManager;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Dispute\DisputeHistoryManager;
use Application\Service\Dispute\DisputeManager;
use Application\Form\DisputeForm;
use Application\Form\DisputeCloseForm;
use Application\Service\Dispute\DisputeStatus;
use Application\Service\Dispute\RefundManager;
use Application\Service\PaginatorOutputTrait;
use Application\Service\Payment\PaymentManager;
use Application\Util\DataFilter;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleRbac\Service\RbacManager;
use Application\Entity\Deal;
use Application\Entity\User;
use Application\Service\Deal\DealPolitics;
use Core\Controller\AbstractRestfulController;
use Application\Service\Dispute\DisputeOperatorCommentManager;
use Application\Service\Deal\DealStatus;
use Zend\Paginator\Paginator;

/**
 * Class DisputeController
 * @package Application\Controller
 */
class DisputeController extends AbstractRestfulController
{
    use PaginatorOutputTrait;

    const DISPUTE_FILES_STORING_FOLDER = '/dispute-files';
    const LOG_FILE_NAME = 'dispute';
    const OPEN_DISPUTE_LIFE_TIME = 14; // 14 дней
    const DISPUTE_IS_NOT_POSSIBLE = 'Dispute is not possible at this stage';

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * @var DisputeOperatorCommentManager
     */
    private $disputeOperatorCommentManager;

    /**
     * @var DealStatus
     */
    private $dealStatus;

    /**
     * @var RefundManager
     */
    private $refundManager;

    /**
     * @var DiscountManager
     */
    private $discountManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var array
     */
    private $config;
    /**
     * @var DisputeHistoryManager
     */
    private $disputeHistoryManager;

    /**
     * DisputeController constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param DisputeManager $disputeManager
     * @param DealManager $dealManager
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param RbacManager $rbacManager
     * @param DealPolitics $dealPolitics
     * @param DisputeOperatorCommentManager $disputeOperatorCommentManager
     * @param DealStatus $dealStatus
     * @param RefundManager $refundManager
     * @param DiscountManager $discountManager
     * @param PaymentManager $paymentManager
     * @param DisputeHistoryManager $disputeHistoryManager
     * @param array $config
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                DisputeManager $disputeManager,
                                DealManager $dealManager,
                                BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                RbacManager $rbacManager,
                                DealPolitics $dealPolitics,
                                DisputeOperatorCommentManager $disputeOperatorCommentManager,
                                DealStatus $dealStatus,
                                RefundManager $refundManager,
                                DiscountManager $discountManager,
                                PaymentManager $paymentManager,
                                DisputeHistoryManager $disputeHistoryManager,
                                array $config)
    {
        $this->entityManager    = $entityManager;
        $this->disputeManager   = $disputeManager;
        $this->dealManager      = $dealManager;
        $this->baseAuthManager      = $baseAuthManager;
        $this->userManager      = $userManager;
        $this->rbacManager      = $rbacManager;
        $this->dealPolitics     = $dealPolitics;
        $this->disputeOperatorCommentManager = $disputeOperatorCommentManager;
        $this->dealStatus       = $dealStatus;
        $this->refundManager    = $refundManager;
        $this->discountManager  = $discountManager;
        $this->paymentManager   = $paymentManager;
        $this->config           = $config;
        $this->disputeHistoryManager = $disputeHistoryManager;
    }

    /**
     * @return TwigViewModel
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        // Data
        $data = $this->collectIncomingData();

        $paramsFilterAndSort = $this->getPaginationParams($this->config);

        try {
            /** @var DisputeSearchForm $disputeFilterForm */
            $disputeFilterForm = new DisputeSearchForm();

            if (isset($data['filter'])) {
                /** @var DataFilter $dataFilter */
                $dataFilter = new DataFilter();

                $data['filter']['csrf'] = $data['csrf'];

                // Set action
                $disputeFilterForm->setAttribute('action', $this->url()->fromRoute('dispute'));

                $filterResult = $dataFilter->filterData($disputeFilterForm, $data['filter']);

                if ($filterResult['filter']) {
                    $paramsFilterAndSort['filter'] = $filterResult['filter'];
                } else {
                    $disputeFilterForm = $filterResult['form'];
                }
            }

            // Get all disputes with sort and search params
            /** @var Paginator $disputes */
            $disputes = $this->disputeManager->getDisputeCollectionByUser($paramsFilterAndSort);
            // Deal collection for output
            $disputesOutput = $this->disputeManager->getDisputeCollectionForOutput($disputes);

            // Paginator data
            $paginator = $disputes->getPages();
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginatorOutput = null;
        if ($paginator) {
            $paginatorOutput = $this->getPaginatorOutput($paginator);
        }

        // SUCCESS Response with CSRF
        // Ajax - csrf token
        if($isAjax){
            $csrf   = $disputeFilterForm->get('csrf')->getValue();
            $data   = [
                'csrf'          => $csrf,
                'disputes'      => $disputesOutput,
                'paginator'     => $paginatorOutput,
            ];

            return $this->message()->success("CSRF token return", $data);
        }

        $disputeFilterForm->prepare();
        $view = new TwigViewModel([
            'disputes'              => $disputesOutput,
            'disputeFilterForm'     => $disputeFilterForm,
            'paginator'             => $paginatorOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/dispute/collection');

        return $view;
    }

    /**
     * @param mixed $id
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function get($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');

            // Check permissions // Important! Only Operator can view any dispute
            if (!$isOperator) {

                return $this->redirect()->toRoute('not-authorized');
            }

            /** @var Dispute $dispute */ // Can throw Exception
            $dispute = $this->disputeManager->getDispute($id);
            $paymentMethodDispute = $this->disputeManager->getPaymentMethodOnDispute($dispute);
            $max_discount = $this->discountManager->getMaxDiscount($dispute->getDeal());
            //форма для генерации платежки возврата
            $refundForm = new RefundForm($paymentMethodDispute, $dispute);
            //форма для генерации платежки скидки
            $discountForm = new DiscountForm($paymentMethodDispute, $dispute, $max_discount);
            //форма для направления в суд
            $tribunalForm = new TribunalForm($dispute);
            //доп форма на случай если нет payment method для возврата средств
            $bankTransferForm = new BankTransferForm();

            $disputeHistory = $this->disputeHistoryManager->getDisputeHistory($dispute, $user);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Dispute Output
        $disputeOutput = $this->disputeManager->getDisputeOutput($dispute);
        // Deal Output
        $dealOutput = $this->dealManager->getDealOutputForSingle($dispute->getDeal());
        // payment method Output
        $paymentMethodDisputeOutput = $this->disputeManager->getPaymentMethodDisputeOutput($paymentMethodDispute);
        // payment order Output
        $debitPaymentOrdersOutput = $this->paymentManager->getIncomingPaymentTransactionsForDeal($dispute->getDeal());

        $disputeOperatorsCommentsOutput = null;
        if ($isOperator) {
            // Массив комментариев оператов к спору
            $disputeOperatorsCommentsOutput = $this->disputeOperatorCommentManager
                ->getDisputeOperatorsCommentsOutput($dispute, $user);
        }

        // Ajax -
        if ($isAjax) {
            return $this->message()->success(
                "Dispute single",
                [
                    'dispute' => $disputeOutput,
                    'deal' => $dealOutput,
                    'dispute_operators_comments' => $disputeOperatorsCommentsOutput
                ]
            );
        }

        // Http -
        $disputeFileUploadForm = null;
        if ($isOperator) {
            $disputeFileUploadForm = new DisputeFileUploadForm($dispute->getId());
        }

        $disputeCommentForm = null;
        if ($isOperator) {
            $disputeCommentForm = new DisputeCommentForm($dispute->getId());
            $disputeCommentForm->prepare();
        }

        $refundForm->prepare();
        $bankTransferForm->prepare();
        $discountForm->prepare();
        $tribunalForm->prepare();
        $view = new TwigViewModel([
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
            'disputeFileUploadForm' => $disputeFileUploadForm,
            'disputeCommentForm' => $disputeCommentForm,
            'dispute_operators_comments' => $disputeOperatorsCommentsOutput,
            'refundForm' => $refundForm,
            'discountForm' => $discountForm,
            'tribunalForm' => $tribunalForm,
            'bankTransferForm' => $bankTransferForm,
            'payment_method_bank_transfer' => $paymentMethodDisputeOutput,
            'debitPaymentOrders' => $debitPaymentOrdersOutput,
            'refund_amount' => $this->refundManager->getRefundAmountByDispute($dispute),
            'dispute_history' => $disputeHistory,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/dispute/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // @TODO Эти две проверки можно пенести в менеджер!
        if ( !isset($formDataInit['user']) || !$formDataInit['user'] instanceof User){

            throw new \Exception('bad data provided: Current user not found');
        }

        if ( !isset($formDataInit['deal']) || !$formDataInit['deal'] instanceof Deal){

            throw new \Exception('bad data provided: Deal not found');
        }

        /** @var User $user */
        $user = $formDataInit['user'];
        /** @var Deal $deal */
        $deal = $formDataInit['deal'];

        // Check permissions // Important! Only Customer can create Dispute
        if (!$this->access('deal.own.customer', ['user' => $user, 'deal' => $deal])) {

            return $this->redirect()->toRoute('not-authorized');
        }

        // Create form
        $form = new DisputeForm();

        try {
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                // Save Form Data
                $resultSavedForm = $this->saveFormData($formData, $formDataInit['deal']);
                // Dispute Output
                $disputeOutput = $this->disputeManager->getDisputeOutput($resultSavedForm['dispute']);
                // @TODO нужно ли тут?
                #$disputeOutput['deal'] = $this->dealManager->getDealOutputForSingle($deal);

                return $this->message()->success('Dispute created', json_encode($disputeOutput));
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * Form render
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function createFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        try {

            // @TODO Эту проверку можно перенести в менеджер!
            if ( !isset($formDataInit['deal']) || !$formDataInit['deal'] instanceof Deal){

                throw new \Exception('bad data provided: Deal not found');
            }

            /** @var Deal $deal */
            $deal = $formDataInit['deal'];

            // @TODO Метод isDealDisputable не до конца реализован. Доделать после завершения работы по тикету GP-706.
            if (!$this->dealPolitics->isDealDisputable($deal)) {

                throw new \Exception(self::DISPUTE_IS_NOT_POSSIBLE);
            }

            /** @var User $user */
            $user = $formDataInit['user'];

            // Check permissions // Important! Only Customer can create Dispute
            if (!$this->access('deal.own.customer', ['user' => $user, 'deal' => $deal])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            // Create form
            $form = new DisputeForm($deal->getId());

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['author'] = $deal->getCustomer();
                    $resultSavedForm = $this->saveFormData($formData, $deal);

                    //http success
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Deal Output
        $this->dealStatus->setStatus($deal);
        $dealOutput = $this->dealManager->getDealOutputForSingle($deal);

        $form->prepare();
        $view = new TwigViewModel([
            'deal' => $dealOutput,
            'disputeForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/dispute/create-form');

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function closeFormAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $formDataInit = $this->processFormDataInit();

        try {
            // @TODO Эти две проверки можно перенести в менеджер!
            if ( !isset($formDataInit['user']) || !$formDataInit['user'] instanceof User){

                throw new \Exception('bad data provided: Current user not found');
            }

            if (!isset($formDataInit['dispute']) || !$formDataInit['dispute'] instanceof Dispute){

                throw new \Exception('bad data provided: Dispute not found');
            }

            // Get current user
            $user = $formDataInit['user'];
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');
            /** @var Dispute $dispute */
            $dispute = $formDataInit['dispute'];

            // Check permissions // Important! Only Author or Operator can close Dispute
            if (!$isOperator && !$this->access('dispute.own.toggle', ['user' => $user, 'dispute' => $dispute])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            if (!$isOperator && $this->disputeManager->isLeastOneConfirmedRequest($dispute)) {

                throw new \Exception('Forbidden close the dispute with confirmed request');
            }

            // Create DisputeCloseForm
            $disputeCloseForm = new DisputeCloseForm($isOperator);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $disputeCloseForm->setData($postData);
                //validate form
                if ($disputeCloseForm->isValid()) {

                    $formData = $disputeCloseForm->getData();

                    // Создаем тикет продления гарантии по сделке и закрываем спор
                    $dispute = $this->disputeManager->closeDispute($formData, $dispute, $isOperator);

                    // Ajax
                    if($isAjax) {
                        // Dispute Output
                        $disputeOutput = $this->disputeManager->getDisputeOutput($dispute);

                        return $this->message()->success('Dispute closed', json_encode($disputeOutput));
                    }

                    // Http
                    /** @var Deal $deal */
                    $deal = $dispute->getDeal();

                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                if ($isAjax) {

                    return $this->message()->invalidFormData($disputeCloseForm->getMessages());
                }
            }
            // Dispute Output
            $disputeOutput = $this->disputeManager->getDisputeOutput($dispute);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $recommended_number_of_days = null;
        if ($isOperator) {
            $data = [];
            $recommended_number_of_days = $this->disputeManager->getNumberOfExtensionDays($data, $dispute->getCreated());
        }

        // Ajax
        if($isAjax) {
            return $this->message()->success('Dispute', json_encode($disputeOutput));
        }

        // Html
        $disputeCloseForm->prepare();

        $view = new TwigViewModel([
            'dispute' => $disputeOutput,
            'disputeCloseForm' => $disputeCloseForm,
            'is_operator' => $isOperator,
            'recommended_number_of_days' => $recommended_number_of_days

        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/dispute/close-form');

        return $view;
    }

    /**
     * Form render
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function openFormAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $formDataInit = $this->processFormDataInit();

        try {
            // @TODO Эти две проверки можно перенести в менеджер!
            if ( !isset($formDataInit['user']) || !$formDataInit['user'] instanceof User){

                throw new \Exception('bad data provided: Current user not found');
            }

            if ( !isset($formDataInit['dispute']) || !$formDataInit['dispute'] instanceof Dispute){

                throw new \Exception('bad data provided: Dispute not found');
            }

            /** @var User $user */
            $user = $formDataInit['user'];
            /** @var Dispute $dispute */
            $dispute = $formDataInit['dispute'];

            // Check permissions // Important! Only Author can open Dispute again
            if (!$this->access('dispute.own.toggle', ['user' => $user, 'dispute' => $dispute])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            // Если спор открыт
            if (!$dispute->isClosed()) {

                throw new \Exception('The dispute is already open');
            }

            /** @var Deal $deal */
            $deal = $dispute->getDeal();

            if (!$this->dealPolitics->isDealDisputable($deal)) {

                throw new \Exception(self::DISPUTE_IS_NOT_POSSIBLE);
            }

            // Если создатель спора не является кастомером в сделке
            if ($deal->getCustomer() !== $dispute->getAuthor()){

                return $this->redirect()->toRoute('not-authorized');
            }

            // Create DisputeOpenForm
            $disputeOpenForm = new DisputeOpenForm();

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $disputeOpenForm->setData($postData);
                //validate form
                if ($disputeOpenForm->isValid()) {
                    // Открываем закрытый спор
                    $dispute = $this->disputeManager->openDispute($dispute);

                    // Ajax
                    if($isAjax) {
                        // Dispute Output
                        $disputeOutput = $this->disputeManager->getDisputeOutput($dispute);

                        return $this->message()->success('Dispute opened', json_encode($disputeOutput));
                    }

                    //http success
                    /** @var  $deal */
                    $deal = $dispute->getDeal();
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                if ($isAjax) {

                    return $this->message()->invalidFormData($disputeOpenForm->getMessages());
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Dispute Output
        $disputeOutput = $this->disputeManager->getDisputeOutput($dispute);

        // Ajax
        if($isAjax) {
            return $this->message()->success('Dispute', json_encode($disputeOutput));
        }

        // Html
        $disputeOpenForm->prepare();

        $view = new TwigViewModel([
            'dispute' => $disputeOutput,
            'disputeOpenForm' => $disputeOpenForm
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/dispute/open-form');

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     *
     * Права доступа в access_filter - #Operator
     */
    public function fileUploadAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Data
        $data = $this->collectIncomingData();

        // Try get Deal and User
        try {
            if(!key_exists('dispute_id', $data)) {

                throw new \Exception('Некорректные данные');
            }

            /** @var Dispute $dispute */ // Can throw Exception
            $dispute = $this->disputeManager->getDispute($data['dispute_id']);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        /** @var DisputeFileUploadForm $fileUploadForm */
        $fileUploadForm = new DisputeFileUploadForm($dispute->getId());

        if ($this->getRequest()->isPost()) {

            $post = array_merge_recursive(
                $data['postData'],
                //$_FILES
                $data['filesData']
            );

            // Set data
            $fileUploadForm->setData($post);

            if ($fileUploadForm->isValid()) {

                // Form is valid, save the form!
                $data = $fileUploadForm->getData();

                if($data['file']['error']) {
                    return $this->message()->error('Error', $data['file']['error']);
                }

                // Сохраняем файл и добавляем в коллекцию файлов спора
                $file = $this->disputeManager->saveDisputeFile($dispute, $data['file']);

                // If Ajax request, return json
                if($isAjax){
                    return $this->message()->success('The file was successfully uploaded');
                }
                // Http -
                return $this->redirect()->toRoute('dispute-single', ['id' => $dispute->getId()]);
            }
            if($isAjax){
                // Return Json with ERROR status
                return $this->message()->invalidFormData($fileUploadForm->getMessages());
            }
            // В Http вернется форма с указанием ошибок валидации
        }
        // No Post data

        //@TODO Возврат для ajax

        // Html -
        $fileUploadForm->prepare();
        $view = new TwigViewModel([
            'fileUploadForm' => $fileUploadForm
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return mixed
     *
     * Права доступа в access_filter - #Operator
     */
    public function showFileAction()
    {
        $disputeId = (int) $this->params()->fromRoute('disputeId');
        $fileId = $this->params()->fromRoute('fileId');

        try {
            /** @var Deal $deal */ // Can throw Exception
            $dispute = $this->disputeManager->getDispute($disputeId);

            if (!$this->disputeManager->isFileBelongsToDispute($dispute, $fileId)) {

                throw new \Exception('The Dispute does not have such a file');
            }
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $fileShowView = $this->forward()->dispatch('ModuleFileManager\Controller\FileController', array(
            'id' => $fileId
        ));

        return $fileShowView;
    }

    /**
     * @return TwigViewModel
     */
    public function openListAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        // Data
        $data = $this->collectIncomingData();

        $paramsFilterAndSort = $this->getPaginationParams($this->config);

        try {
            /** @var DisputeOpenSearchForm $disputeOpenSearchForm */
            $disputeOpenSearchForm = new DisputeOpenSearchForm();

            if (isset($data['filter'])) {
                /** @var DataFilter $dataFilter */
                $dataFilter = new DataFilter();

                $data['filter']['csrf'] = $data['csrf'];

                // Set action
                $disputeOpenSearchForm->setAttribute('action', $this->url()->fromRoute('dispute'));

                $filterResult = $dataFilter->filterData($disputeOpenSearchForm, $data['filter']);

                if ($filterResult['filter']) {
                    $paramsFilterAndSort['filter'] = $filterResult['filter'];
                } else {
                    $disputeOpenSearchForm = $filterResult['form'];
                }
            }
            // Get all open disputes with sort and search params
            /** @var Paginator $disputes */
            $disputes = $this->disputeManager->getOpenDisputeCollection($paramsFilterAndSort);
            // Deal collection for output
            $disputesOutput = $this->disputeManager
                ->getDisputeCollectionForOutput($disputes);

            // Paginator data
            $paginator = $disputes->getPages();
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginatorOutput = null;
        if ($paginator) {
            $paginatorOutput = $this->getPaginatorOutput($paginator);
        }

        // SUCCESS Response with CSRF
        // Ajax - csrf token
        if($isAjax){
            $csrf   = $disputeOpenSearchForm->get('csrf')->getValue();
            $data   = [
                'csrf'          => $csrf,
                'disputes'      => $disputesOutput,
                'paginator'     => $paginatorOutput,
            ];

            return $this->message()->success("CSRF token return", $data);
        }

        $disputeOpenSearchForm->prepare();
        $view = new TwigViewModel([
            'disputes'              => $disputesOutput,
            'disputeFilterForm'     => $disputeOpenSearchForm,
            'paginator'             => $paginatorOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/dispute/open-collection');

        return $view;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function formInitAction()
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // Deal output
        $formDataInit['deal'] = $formDataInit['deal'] && $formDataInit['user']
            ? $this->dealManager->getDealOutputForSingle($formDataInit['deal'], $formDataInit['user'])
            : null;
        // User output
        $formDataInit['user'] = $this->userManager->getUserOutputForSingle($formDataInit['user']);

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        //check
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            /** @var array $deal_statuses */
            $deal_statuses = DealStatus::getStatuses();
            /** @var array $dispute_statuses */
            $dispute_statuses = DisputeStatus::getStatuses();

            // process data for update or delete
            $dispute = null;
            if (isset($incomingData['idDispute'])){
                /** @var Dispute $dispute */
                $dispute = $this->disputeManager->getDispute((int) $incomingData['idDispute']);
            }
            $deal = null;
            if ($dispute){
                /** @var Deal $deal */
                $deal = $dispute->getDeal();
            } elseif (isset($incomingData['deal_id'])) {
                /** @var Deal $deal */
                $deal = $this->dealManager->getDealById((int) $incomingData['deal_id']);
            }
            $customer = null;
            if ($deal) {
                /** @var DealAgent $customer */
                $customer = $deal->getCustomer();
            }

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        return [
            'deal_statuses' => $deal_statuses,
            'dispute_statuses' => $dispute_statuses,
            'user'      => $user,
            'dispute'   => $dispute,
            'customer'  => $customer,
            'deal'      => $deal
        ];
    }

    /**
     * @param array $data
     * @param Deal $deal
     * @return array
     * @throws \Exception
     */
    private function saveFormData(array $data, Deal $deal)
    {
        if (isset($data['idDispute'])) {
            //update
            if ((int) $data['idDispute'] <= 0) {
                throw new \Exception('bad data provided: not found Dispute id');
            }

            /** @var Dispute $dispute */
            $dispute = $this->disputeManager->getDispute((int) $data['idDispute']);

            if($dispute === null) {
                throw new \Exception('bad data provided: not found Dispute by id');
            }

            // Спор редактировать нельзя!
            #$dispute = $this->disputeManager->updateDispute($data, $dispute);

            $result = [
                'dispute' => $dispute
            ];
        } else {
            //create
            try {
                /** @var Dispute $dispute */
                $dispute = $this->disputeManager->addDispute($data['author'], $data, $deal);
            }
            catch (\Throwable $t) {

                throw new \Exception($t->getMessage());
            }

            $result = [
                'dispute' => $dispute
            ];
        }

        return $result;
    }
}