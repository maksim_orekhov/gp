<?php

namespace Application\Controller;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Form\DiscountRequestForm;
use Application\Form\RequestConfirmForm;
use Application\Form\RequestRefuseForm;
use Application\Listener\ArbitrageRequestListener;
use Application\Entity\User;
use Application\Service\Deal\DealManager;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Dispute\DisputeManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\Paginator\Paginator;

/**
 * Class DiscountRequestController
 * @package Application\Controller
 */
class DiscountRequestController extends AbstractRestfulController
{
    /**
     * @var DiscountManager
     */
    private $discountManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var ArbitrageRequestListener
     */
    private $arbitrageRequestListener;


    /**
     * DiscountRequestController constructor.
     * @param DiscountManager $discountManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param DealManager $dealManager
     * @param DisputeManager $disputeManager
     * @param ArbitrageRequestListener $arbitrageRequestListener
     */
    public function __construct(DiscountManager $discountManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                DealManager $dealManager,
                                DisputeManager $disputeManager,
                                ArbitrageRequestListener $arbitrageRequestListener)
    {
        $this->discountManager = $discountManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->dealManager = $dealManager;
        $this->disputeManager = $disputeManager;
        $this->arbitrageRequestListener = $arbitrageRequestListener;
    }

    /**
     * Только для Оператора
     *
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \RuntimeException
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // If not operator
            if (false === $this->discountManager->isOperator($user)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            /** @var Paginator $paginatedDiscountRequests */
            $paginatedDiscountRequests = $this->discountManager->getAllDiscountRequests();

            $discountCollectionOutput = $this->discountManager
                ->getDiscountRequestCollectionForOutput($paginatedDiscountRequests);

            $paginatorOutput = $paginatedDiscountRequests->getPages();
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'discount_requests' => $discountCollectionOutput,
            'paginator' => $paginatorOutput
        ];

        // Ajax -
        if ( $isAjax ){

            return $this->message()->success('DiscountRequests', $data);
        }

        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/discount-request/collection');

        return $view;
    }

    /**
     * @param mixed $id
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function get($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        if (!isset($id) || (int) $id <= 0) {

            return $this->message()->error('bad data provided');
        }

        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // If operator
            if ($this->discountManager->isOperator($user)) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var DiscountRequest $discountRequest */
            $discountRequest = $this->discountManager->getDiscountRequestById((int)$id);

            // valid data
            if (!$discountRequest || !$discountRequest instanceof DiscountRequest) {

                throw new \Exception('bad data provided: DiscountRequest not found');
            }

            // If Author of the Request
            if (false === $this->discountManager->checkPermissionDiscountSingle($user, $discountRequest)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $discountRequestOutput = $this->discountManager->getDiscountRequestOutput($discountRequest);
            $disputeOutput = $this->disputeManager->getDisputeOutput($discountRequest->getDispute());
            $dealOutput = $this->dealManager->getDealOutputForSingle($discountRequest->getDispute()->getDeal());
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Ajax -
        if ( $isAjax ){
            $data = [
                'discountRequest'   => $discountRequestOutput,
                'dispute'           => $disputeOutput,
                'deal'              => $dealOutput,
            ];

            return $this->message()->success("DiscountRequest single", $data);
        }

        $discountConfirmForm = new RequestConfirmForm();
        $discountConfirmForm->setAttribute('action', $this->url()->fromRoute('deal-discount-confirm', ['idRequest' => $discountRequest->getId()]));

        $discountRefuseForm = new RequestRefuseForm();
        $discountRefuseForm->setAttribute('action', $this->url()->fromRoute('deal-discount-refuse', ['idRequest' => $discountRequest->getId()]));

        // Http -
        $view = new TwigViewModel([
            'discountConfirmForm'=>$discountConfirmForm,
            'discountRefuseForm'=>$discountRefuseForm,
            'discountRequest'=>$discountRequestOutput,
            'dispute'=>$disputeOutput,
            'deal'=>$dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/discount-request/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        // only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            /** @var Deal $deal */ // Throws Exception if no deal found
            $deal = $this->dealManager->getDealById($data['deal']);
            /** @var Dispute $dispute */
            $dispute = $deal->getDispute();

            if (null === $dispute) {

                throw new \Exception('Deal has no dispute');
            }

            // Если уже была создана скидка
            if ($this->discountManager->isDiscountDone($dispute)) {

                throw new \Exception('Discount already done');
            }

            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // Check permissions
            if (!$this->discountManager->checkPermissionDiscountRequest($user, $deal)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $max_discount = $this->discountManager->getMaxDiscount($dispute->getDeal());

            $form = new DiscountRequestForm($deal, $max_discount);

            $form->setData($data);
            // Validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $author = $this->dealManager->getUserAgentByDeal($user, $deal);
                /** @var DiscountRequest $discountRequest */
                $resultSavedForm = $this->saveFormData($deal, $author, $formData);
                $discountRequestOutput = $this->discountManager->getDiscountRequestOutput($resultSavedForm['discountRequest']);

                return $this->message()->success('Discount request created', ['discountRequest' => $discountRequestOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function createFormAction()
    {
        // only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $postData = $this->collectIncomingData()['postData'];
        $incoming_data = $this->processFormDataInit();

        /** @var User $user */
        $user = $incoming_data['user'];

        // not operator
        if ($this->discountManager->isOperator($user)) {

            return $this->redirect()->toRoute('not-authorized');
        }
        // valid data
        if (!isset($incoming_data['dispute']) || !$incoming_data['dispute'] instanceof Dispute){

            throw new \Exception('bad data provided: Dispute not found');
        }
        /** @var Dispute $dispute */
        $dispute = $incoming_data['dispute'];

        // check permission
        if (!$this->discountManager->checkPermissionDiscountRequest($user, $dispute->getDeal())) {

            return $this->redirect()->toRoute('not-authorized');
        }

        /** @var Deal $deal */
        $deal = $dispute->getDeal();

        try {
            $discountRequestForm = new DiscountRequestForm($deal, $incoming_data['max_discount']);

            if ($postData) {
                // set data to form
                $discountRequestForm->setData($postData);
                // validate form
                if ($discountRequestForm->isValid()) {
                    $formData = $discountRequestForm->getData();
                    $author = $this->dealManager->getUserAgentByDeal($user, $deal);
                    /** @var DiscountRequest $discountRequest */
                    $discountRequest = $this->saveFormData($deal, $author, $formData);
                    //@TODO Куда редиректить после success
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
        #$this->discountManager->setDisputeOutput($disputeOutput);
        $dealOutput = $this->dealManager->getDealOutputForSingle($deal);

        $discountRequestForm->prepare();
        $view = new TwigViewModel([
            'discountRequestForm' => $discountRequestForm,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function discountConfirmAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $formDataInit = $this->processFormDataInit();

            /** @var User $user */
            $user = $formDataInit['user'];
            //not operator
            if ($this->discountManager->isOperator($user)) {

                return $this->redirect()->toRoute('not-authorized');
            }
            // valid data
            if (!isset($formDataInit['discountRequest']) || !$formDataInit['discountRequest'] instanceof DiscountRequest) {

                throw new \Exception('bad data provided: DiscountRequest not found');
            }
            // check permission
            if (!$this->discountManager->checkPermissionDiscountConfirm($user, $formDataInit['discountRequest'])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var DiscountRequest $discountRequest */
            $discountRequest = $formDataInit['discountRequest'];
            /** @var Deal $deal */
            $deal = $discountRequest->getDispute()->getDeal();
            //check of confirmation
            if ($discountRequest->getConfirm()) {

                throw new \Exception('On the current request already decided');
            }

            $discountConfirmForm = new RequestConfirmForm();
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $this->discountManager->setDisputeOutput($disputeOutput);
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);
            $discountRequestOutput = $this->discountManager->getDiscountRequestOutput($discountRequest);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData) && $postData['type_form'] === RequestConfirmForm::TYPE_FORM) {
                //set data to form
                $discountConfirmForm->setData($postData);
                //validate form
                if ($discountConfirmForm->isValid()) {
                    $author = $this->dealManager->getUserAgentByDeal($user, $deal);

                    $discountConfirm = $this->discountManager->createDiscountConfirm($discountRequest, $author);

                    //@TODO отключил автоматику согласно задаче GP-1575
                    //if status success
//                    if ( $discountConfirm->isStatus() ) {
//                        // Subscribe on event "onDiscountConfirmSuccess"
//                        $this->discountConfirmSuccessListener->subscribe();
//                        // Trigger event "onDiscountConfirmSuccess"
//                        $this->discountConfirmSuccessListener->trigger($discountConfirm);
//                    }

                    //ajax response
                    if($isAjax) {
                        // Return Json with SUCCESS status
                        return $this->message()->success('DiscountRequest confirmed');
                    }

                    //@TODO Куда редиректить после success
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                //ajax response
                if($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($discountConfirmForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $discountConfirmForm->prepare();
        $view = new TwigViewModel([
            'discountConfirmForm' => $discountConfirmForm,
            'discountRequest' => $discountRequestOutput,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function discountRefuseAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $formDataInit = $this->processFormDataInit();
            /** @var User $user */
            $user = $formDataInit['user'];
            //not operator
            if ($this->discountManager->isOperator($user)) {
                return $this->redirect()->toRoute('not-authorized');
            }
            //valid date
            if (!isset($formDataInit['discountRequest']) || !$formDataInit['discountRequest'] instanceof DiscountRequest) {

                throw new \Exception('bad data provided: DiscountRequest not found');
            }
            //check permission
            if (!$this->discountManager->checkPermissionDiscountRefuse($user, $formDataInit['discountRequest'])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var DiscountRequest $discountRequest */
            $discountRequest = $formDataInit['discountRequest'];
            /** @var Deal $deal */
            $deal = $discountRequest->getDispute()->getDeal();
            //check of confirmation
            if ($discountRequest->getConfirm()) {

                throw new \Exception('On the current request already decided');
            }

            $discountRefuseForm = new RequestRefuseForm();
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $this->discountManager->setDisputeOutput($disputeOutput);
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);
            $discountRequestOutput = $this->discountManager->getDiscountRequestOutput($discountRequest);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData) && $postData['type_form'] === RequestRefuseForm::TYPE_FORM) {
                //set data to form
                $discountRefuseForm->setData($postData);
                //validate form
                if ($discountRefuseForm->isValid()) {
                    $author = $this->dealManager->getUserAgentByDeal($user, $deal);

                    $this->discountManager->createDiscountRefuse($discountRequest, $author);

                    if ($this->discountManager->checkDiscountRefusesOverlimit($deal->getDispute())) {
                        // Subscribe on event "onRequestDeclineOverlimit"
                        $this->arbitrageRequestListener->subscribe();
                        // Trigger event "onRequestDeclineOverlimit"
                        $this->arbitrageRequestListener->trigger(
                            $deal,
                            $this->dealManager->getUserAgentByDeal($user, $deal),
                            $disputeOutput,
                            $discountRequest
                        );
                    }

                    //ajax response
                    if($isAjax) {
                        // Return Json with SUCCESS status
                        return $this->message()->success('DiscountRequest rejected');
                    }

                    //@TODO Куда редиректить после success
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                //ajax response
                if($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($discountRefuseForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $discountRefuseForm->prepare();
        $view = new TwigViewModel([
            'discountRefuseForm' => $discountRefuseForm,
            'discountRequest' => $discountRequestOutput,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return array|\Zend\Http\Response
     */
    public function formInitAction()
    {
        // only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        if (!is_array($formDataInit)) {

            return $this->message()->error('formInit failed');
        }

        //not operator
        if ($this->discountManager->isOperator($formDataInit['user'])) {

            return $this->redirect()->toRoute('not-authorized');
        }

        $formDataInit['user'] = $this->userManager->extractUser($formDataInit['user']);

        if (null !== $formDataInit['dispute']) {
            $formDataInit['dispute'] = $this->disputeManager->getDisputeOutput($formDataInit['dispute']);
        }

        if (null !== $formDataInit['discountRequest']) {
            $formDataInit['discountRequest'] = $this->discountManager
                ->getDiscountRequestOutput($formDataInit['discountRequest']);
        }

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $allowedUser = null;

            $deal = null;
            $dispute = null;
            $max_discount = null;
            if (isset($incomingData['idDispute'])){
                /** @var Dispute $dispute */
                $dispute = $this->disputeManager->getDispute($incomingData['idDispute']);
                // Check if $deal belongs to current user
                if ($dispute) {
                    /** @var Deal $deal */
                    $deal = $dispute->getDeal();
                }

                // Check if $deal belongs to current user
                if ($user && $deal && !DealManager::isUserPartOfDeal($deal, $user)) {

                    $deal = null;
                }
            }

            $discountRequest = null;
            if (isset($incomingData['idRequest'])){
                /** @var DiscountRequest $discountRequest */
                $discountRequest = $this->discountManager
                    ->getDiscountRequestById((int) $incomingData['idRequest']);
            }

            if (null !== $discountRequest && null === $dispute) {
                /** @var Dispute $dispute */
                $dispute = $discountRequest->getDispute();
                /** @var Deal $deal */
                $deal = $dispute->getDeal();
            }

            if (null !== $deal) {
                $max_discount = $this->discountManager->getMaxDiscount($deal);
            }
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return [
            'user' => $user,
            'dispute' => $dispute,
            'discountRequest' => $discountRequest,
            'max_discount' => $max_discount
        ];
    }

    /**
     * @param Deal $deal
     * @param DealAgent $author
     * @param $formData
     * @return array
     */
    private function saveFormData(Deal $deal, DealAgent $author, $formData): array
    {
        //create
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $this->discountManager->createDiscountRequest($deal, $author, $formData);

        $result = [
            'discountRequest' => $discountRequest,
        ];

        return $result;
    }
}