<?php

namespace Application\Controller;

use Application\Service\PaginatorOutputTrait;
use Core\Controller\AbstractRestfulController;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Core\Exception\CriticalException;
use Core\Service\TwigViewModel;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Application\Form\FileUploadForm;
use Zend\View\Renderer\RendererInterface;

/**
 * Class BankClientController
 * @package Application\Controller
 */
class BankClientController extends AbstractRestfulController
{
    use PaginatorOutputTrait;
    use \Application\Provider\JsonResponseTrait;

    const ORIGINAL_ENCODING         = 'windows-1251';
    const FILE_SOURCE_FOLDER        = '/bank-client-stock';
    const FILE_STORING_FOLDER       = '/bank-client';
    const TEMPORARY_FILES_FOLDER    = '/bank-client-tmp';

    const SUCCESS_NEW_FILES_NOT_FOUND = 'Новых файлов не обнаружено';
    const SUCCESS_FILE_PROCESSED = 'Файлы успешно обработаны';
    const SUCCESS_FILE_GENERATED = 'Файл успешно создан';

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\Parser\BankClientParser
     */
    private $parser;

    /**
     * @var \Application\Service\BankClient\BankClientManager
     */
    private $bankClientManager;

    /**
     * @var \ModuleFileManager\Service\FileManager
     */
    private $fileManager;

    /**
     * @var \TCPDF
     */
    protected $tcpdf;

    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * @var array
     */
    private $config;

    /**
     * @var string
     */
    private $file_storing_folder_path;

    /**
     * @var string
     */
    private $file_source_folder_path;

    /**
     * BankClientController constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param \Application\Service\Parser\BankClientParser $parser
     * @param \Application\Service\BankClient\BankClientManager $bankClientManager
     * @param \ModuleFileManager\Service\FileManager $fileManager
     * @param \TCPDF $tspdf
     * @param RendererInterface $renderer
     * @param $config
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                \Application\Service\Parser\BankClientParser $parser,
                                \Application\Service\BankClient\BankClientManager $bankClientManager,
                                \ModuleFileManager\Service\FileManager $fileManager,
                                \TCPDF $tspdf,
                                RendererInterface $renderer,
                                $config)
    {
        $this->entityManager                = $entityManager;
        $this->parser                       = $parser;
        $this->bankClientManager            = $bankClientManager;
        $this->fileManager                  = $fileManager;
        $this->tcpdf                        = $tspdf;
        $this->renderer                     = $renderer;
        $this->config                       = $config;
        $this->file_storing_folder_path     = './'.$config['file-management']['main_upload_folder'] . self::FILE_STORING_FOLDER;
        $this->file_source_folder_path      = './'.$config['file-management']['main_upload_folder'] . self::FILE_SOURCE_FOLDER;
    }

    /**
     * Show all bank-client files
     *
     * @return ViewModel
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $params = $this->getPaginationParams($this->config);

        // @TODO Тут нужна выборка по другому критерию. Пока так, т.к. другого нет.
        // Get bank-client files by path
        $bankClientFiles = $this->bankClientManager->getAllBankClientFilesByPath($params);
        // Get array of processed files
        $processed_files = $this->bankClientManager->getProcessedFilesAsArray();

        // Paginator data
        $paginator = null;
        if (method_exists($bankClientFiles , 'getPages')) {
            $paginator = $bankClientFiles->getPages();
        }
        $paginatorOutput = null;
        if ($paginator) {
            $paginatorOutput = $this->getPaginatorOutput($paginator);
        }

        $bankClientFilesOutput = $this->bankClientManager->getFilesOutput($bankClientFiles);

        // Ajax
        if($isAjax){
            $data   = [
                'files'             => $bankClientFilesOutput,
                'processed_files'   => $processed_files,
                'paginator'         => $paginatorOutput
            ];

            return $this->message()->success('Files', $data);
        }

        $view = new TwigViewModel([
            'files'             => $bankClientFilesOutput,
            'processed_files'   => $processed_files,
            'paginator'         => $paginatorOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return ViewModel
     * @throws \RuntimeException
     * @throws \Exception
     */
    public function paymentOrderFileProcessingAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        $file_names = [];
        $bank_client_files = [];
        $error_message = null;
        $success_message = null;
        $processing_result = [];
        $processing_errors = [];
        // Get files with pattern '*.txt' in specified folder in order of addition
        $files = glob($this->file_storing_folder_path.'/*.txt', GLOB_NOSORT);
        // If there are .txt files in the folder
        if($files) {
            // Sort array by filemtime(время последнего изменения файла)
            usort($files, function($a, $b) {
                return filemtime($a) < filemtime($b);
            });
            // Get all processed files
            $processed_file_hashes = $this->bankClientManager->getProcessedFileHashesAsArray();

            $exceptionMessages = [];
            $new_files_counter = 0;

            // Обходим все файлы в папке и находим необработанные (по отсутствию хэша)
            foreach ($files as $file) {
                try {
                    if (!\in_array($file_hash = hash_file('md5', $file), $processed_file_hashes, true)) {
                        $file_name = basename($file);
                        // Parse File content to array of arrays
                        $bank_client_payment_orders = $this->parser->execute($file);
                        // Create and save and return BankClientPaymentOrder objects

                        $result = $this->bankClientManager->processingPaymentOrders($file_name, $bank_client_payment_orders, $file_hash);

                        /** @noinspection SlowArrayOperationsInLoopInspection */
                        $processing_result = array_merge($processing_result, $result['paymentOrders']);
                        /** @noinspection SlowArrayOperationsInLoopInspection */
                        $processing_errors = array_merge($processing_errors, $result['errors']);
                        // Collect data to array for ViewModel
                        $file_names[] = $file_name;
                        $bank_client_files[] = $bank_client_payment_orders;

                        $new_files_counter++;
                    } else {
                        // Если хэш нового файла уже есть в массиве $processed_file_hashes, а имя файла не совпадает с существующим,
                        // значит, файл не является новым - удаляем его.
                        if (!array_key_exists(basename($file), $processed_file_hashes)) {
                            $fileObject = $this->fileManager->getFileByName(basename($file));
                            $this->fileManager->delete($fileObject->getId());
                        }
                    }
                }
                catch (\Throwable $t) {
                    logException($t, 'bank-client-error');
                    $exceptionMessages[] = $t->getMessage();
                }
            }

            // Если при обработке возникла хотя бы одна ошибка
            // @TODO Но мы теряем сообщения об удачной обработке остальных файлов
            if (\count($exceptionMessages) > 0) {

                $this->message()->sendSpecialMail(implode('<br>', $exceptionMessages));
            }

            // SUCCESS Response
            if ($new_files_counter !== 0) {
                $success_message = self::SUCCESS_FILE_PROCESSED;
            } else {
                $success_message = self::SUCCESS_NEW_FILES_NOT_FOUND;
            }
            // Ajax -
            if($isAjax){
                return $this->message()->success($success_message);
            }
            // Http -
        }

        $view = new TwigViewModel([
            'file_names'          => $file_names,
            'bank_client_files'   => $bank_client_files,
            'newPaymentOrders'    => !empty($processing_result) ? $processing_result : null,
            'processing_errors'   => !empty($processing_errors) ? $processing_errors : null,
            'success_message'     => $success_message,
            'error_message'       => $error_message,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Реализует механизм загрузки нового файла для последующей обработки.
     * Последующая обработка - paymentOrderFileProcessing.
     *
     * @return mixed|ViewModel
     * @throws CriticalException
     */
    public function fileFromStockUploadAction()
    {
        $file_names = [];
        $error_message = null;
        $success_message = null;
        // If stock folder exists
        if (!is_dir($this->file_source_folder_path)) {
            $error_message = 'Folder ' . $this->file_source_folder_path . ' not found';
            $view = new TwigViewModel([
                'file_names' => $file_names,
                'success_message' => $success_message,
                'error_message' => $error_message,
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
            $view->setTemplate('application/bank-client/file-from-stock-upload');

            return $view;
        }
        // Get files with pattern '*.txt' in specified folder in order of addition
        $files = glob($this->file_source_folder_path . '/*.txt', GLOB_NOSORT);
        // If there are .txt files in the folder
        if ($files) {
            // Sort array by filemtime(время последнего изменения файла)
            usort($files, function ($a, $b) {
                return filemtime($a) < filemtime($b);
            });
            // Get all processed files
            $processed_file_hashes = $this->bankClientManager->getProcessedFileHashesAsArray();

            try {
                $new_files_counter = 0;
                // Execute until get to the first processed file
                foreach ($files as $file) {
                    if (!in_array($file_hash = hash_file('md5', $file), $processed_file_hashes)) {
                        // File upload
                        $params = [
                            'file_source' => $file,
                            'file_name' => $origin_name = mb_convert_encoding(basename($file), 'UTF-8', self::ORIGINAL_ENCODING),
                            'file_type' => 'text/plain',
                            'file_size' => filesize($file),
                            'path' => self::FILE_STORING_FOLDER
                        ];
                        $this->fileManager->write($params);

                        $file_names[] = basename($file);
                        $new_files_counter++;
                    }
                }

                // SUCCESS Response
                // @TODO Скорее всего, не нужно, т.е. success_message отобразится тот, что в paymentOrderFileProcessing
                if ($new_files_counter != 0) {
                    $success_message = self::SUCCESS_FILE_PROCESSED;
                } else {
                    $success_message = self::SUCCESS_NEW_FILES_NOT_FOUND;
                }

                $paymentOrderFileProcessingView = $this->forward()->dispatch('Application\Controller\BankClientController', array(
                    'action' => 'paymentOrderFileProcessing'
                ));
                return $paymentOrderFileProcessingView;
            } catch (\Throwable $t) {
                throw new CriticalException($t->getMessage());
            }
        }

        $view = new TwigViewModel([
            'file_names' => $file_names,
            'success_message' => $success_message,
            'error_message' => $error_message,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/bank-client/file-from-stock-upload');

        return $view;
    }
    /**
     * @return \Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function fileUploadAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $fileUploadForm = new FileUploadForm();

        if ($this->getRequest()->isPost()) {

            $post = array_merge_recursive(
                $this->getRequest()->getPost()->toArray(),
                //$_FILES
                $this->getRequest()->getFiles()->toArray()
            );

            $fileUploadForm->setData($post);

            if ($fileUploadForm->isValid()) {

                // Form is valid, save the form!
                $data = $fileUploadForm->getData();

                if($data['file']['error']) {
                    // Flash message for notify User about failed confirmation
                    $this->flashMessenger()->addMessage($data['file']['error'], FlashMessenger::NAMESPACE_ERROR, 100);
                    // Redirect to page without POST
                    return $this->redirect()->toRoute('bank-client/file');
                }

                // Set of data for pass to fileManager
                $file_source = $data['file']['tmp_name'];
                $file_name = $data['file']['name'];
                $file_type = $data['file']['type'];
                $file_size = $data['file']['size'];

                try {
                    $params = [
                        'file_source' => $file_source,
                        'file_name' => $file_name,
                        'file_type' => $file_type,
                        'file_size' => $file_size,
                        'path' => self::FILE_STORING_FOLDER
                    ];

                    $file = $this->fileManager->write($params);

                    // SUCCESS BLOCK //
                    // If Ajax request, return json
                    if($isAjax){
                        // Return Json with SUCCESS status
                        return $this->message()->success('The file was successfully uploaded');
                    }
                    // Http -
                    // Redirect to page without POST
                    return $this->redirect()->toRoute('bank-client');
                    // END OF SUCCESS BLOCK //
                }
                catch (\Throwable $t) {
                    throw new CriticalException($t->getMessage());
                }
            }
            // $form->isValid() failed
            if($isAjax){
                // Return Json with ERROR status
                return $this->message()->invalidFormData($fileUploadForm->getMessages());
            }
            // В Http вернется форма с указанием ошибок валидации
        }
        // No Post data

        // Для Ajax отдаем csrf токен
        // Success response if requested by Ajax
        if($isAjax){
            // Return Json with SUCCESS status (in this case also with $data array, which contains csrf token)
            $csrf   = $fileUploadForm->get('csrf')->getValue();
            $data   = ['csrf' => $csrf];

            return $this->message()->success("CSRF token return", $data);
        }
        // Http -
        $fileUploadForm->prepare();
        $view = new TwigViewModel([
            'fileUploadForm'  => $fileUploadForm
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Show Payment order
     * @throws CriticalException
     */
    public function showPaymentOrderAction()
    {
        $payment_order_id = (int) $this->params()->fromRoute('id');

        try {
            /** @var BankClientPaymentOrder $paymentOrder */
            $paymentOrder = $this->bankClientManager->getPaymentOrderById($payment_order_id);

            $view = new ViewModel([
                'paymentOrder'  => $paymentOrder
            ]);

            $renderer = $this->renderer;
            $view->setTemplate('application/bank-client/payment-order');

            // Чтобы посмотреть как выглядит HTML
            #return $view;

            $html = $renderer->render($view);

            $pdf = $this->tcpdf;

            $pdf->SetFont('arialnarrow', '', 12, '', false);
            $pdf->AddPage();
            $pdf->writeHTML($html, true, false, true, false, '');

            $pdf->Output();
        }
        catch (\Throwable $t) {

            throw new CriticalException($t->getMessage());
        }
    }

    /**
     * @return TwigViewModel
     * @throws CriticalException
     */
    public function creditUnpaidAction()
    {
        try {
            // Get all unpaid payment orders with debitPayment from BankClientPaymentOrder // Can throw Exception
            $paymentTransactions = $this->bankClientManager->getReadyOutgoingPaymentOrders();
        }
        catch (\Throwable $t) {

            throw new CriticalException($t->getMessage());
        }

        $view = new TwigViewModel([
            'paymentTransactions' => $paymentTransactions,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws CriticalException
     */
    public function generateCreditUnpaidFileAction()
    {
        // Get all ready to payment PaymentOrders
        $paymentOrders = $this->bankClientManager->getReadyOutgoingPaymentOrders();

        $file = null;

        if(count($paymentOrders) > 0) {
            try {
                $file = $this->bankClientManager->generateBankClientFile($paymentOrders);
                // Redirect to download  page
                return $this->redirect()->toRoute('bank-client/file-download', ['id' => $file->getId()]);
            }
            catch (\Throwable $t) {
                throw new CriticalException($t->getMessage());
            }
        }

        return new ViewModel();
    }

    /**
     * @return \Zend\Http\Response\Stream
     * @throws CriticalException
     */
    public function fileDownloadAction()
    {
        $file_id = (int) $this->params()->fromRoute('id');
        try {
            // Get file
            $file = $this->fileManager->getFileById($file_id);
            // Get file path
            $file_path = $this->config['file-management']['main_upload_folder'].BankClientController::FILE_STORING_FOLDER . "/" . $file->getName();

            $file = fopen($file_path, 'r');

            $response = new \Zend\Http\Response\Stream();
            $response->setStream($file);
            $response->setStatusCode(200);

            $headers = new \Zend\Http\Headers();
            $headers->addHeaderLine('Content-Type', 'text/plain')
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $file_path . '"')
                ->addHeaderLine('Content-Length', filesize($file_path));

            $response->setHeaders($headers);

            #fclose($file);

            return $response;
        }
        catch (\Throwable $t) {

            throw new CriticalException($t->getMessage());
        }
    }
}
