<?php

namespace Application\Controller;

use Application\Form\DealSearchForm;
use Application\Form\UserSearchForm;
use Application\Service\PaginatorOutputTrait;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use Application\Service\UserManager;
use Application\Util\DataFilter;

/**
 * Class UserController
 * @package Application\Controller
 */
class UserController extends AbstractRestfulController
{
    use PaginatorOutputTrait;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var array
     */
    private $config;

    /**
     * UserController constructor.
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager, array $config)
    {
        $this->userManager = $userManager;
        $this->config = $config;
    }

    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Data
        $data = $this->collectIncomingData();
        $paramsFilterAndSort = $this->getPaginationParams($this->config);

        /** @var UserSearchForm $userFilterForm */
        $userFilterForm = new UserSearchForm();

        if (isset($data['filter'])) {
            /** @var DataFilter $dataFilter */
            $dataFilter = new DataFilter();

            $data['filter']['csrf'] = $data['csrf'];

            // Set action
            $userFilterForm->setAttribute('action', $this->url()->fromRoute('user'));

            $filterResult = $dataFilter->filterData($userFilterForm, $data['filter']);

            if ($filterResult['filter']) {
                $paramsFilterAndSort['filter'] = $filterResult['filter'];
            } else {
                $userFilterForm = $filterResult['form'];
            }
        }

        try {
            /** @var array $users */
            $usersOutput = $this->userManager->getUserCollection($paramsFilterAndSort);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginatorOutput = null;
        if (isset($usersOutput['paginator'])) {
            $paginatorOutput = $this->getPaginatorOutput($usersOutput['paginator']);
        }

        // Ajax -
        if ($isAjax) {
            return $this->message()->success("User collection",
                [
                    'users' => $usersOutput['users'],
                    'paginator' => $paginatorOutput
                ]
            );
        }
        // Http -
        $userFilterForm->prepare();
        $view = new TwigViewModel([
            'userFilterForm'    => $userFilterForm,
            'users'             => $usersOutput['users'],
            'paginator'         => $paginatorOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/user/collection');

        return $view;
    }
}
