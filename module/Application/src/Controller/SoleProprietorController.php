<?php

namespace Application\Controller;

use Application\Entity\SoleProprietor;
use Application\Form\SoleProprietorForm;
use Application\Service\CivilLawSubject\CivilLawSubjectPolitics;
use Application\Service\CivilLawSubject\SoleProprietorManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Controller\AbstractRestfulController;
use Core\Exception\LogicException;
use Core\Service\TwigViewModel;
use Zend\View\Model\ViewModel;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Entity\User;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Doctrine\ORM\EntityManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class SoleProprietorController
 * @package Application\Controller
 */
class SoleProprietorController extends AbstractRestfulController
{
    const SUCCESS_DELETE = 'SoleProprietor successfully deleted';

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\CivilLawSubject\CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * @var CivilLawSubjectPolitics
     */
    private $civilLawSubjectPolitics;

    /**
     * @var SoleProprietorManager
     */
    private $soleProprietorManager;

    /**
     * @var array
     */
    private $config;

    /**
     * SoleProprietorController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param RbacManager $rbacManager
     * @param NdsTypeManager $ndsTypeManager
     * @param CivilLawSubjectPolitics $civilLawSubjectPolitics
     * @param SoleProprietorManager $soleProprietorManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                PaymentMethodManager $paymentMethodManager,
                                RbacManager $rbacManager,
                                NdsTypeManager $ndsTypeManager,
                                CivilLawSubjectPolitics $civilLawSubjectPolitics,
                                SoleProprietorManager $soleProprietorManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->civilLawSubjectManager = $civilLawSubjectManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->rbacManager = $rbacManager;
        $this->ndsTypeManager = $ndsTypeManager;
        $this->civilLawSubjectPolitics = $civilLawSubjectPolitics;
        $this->soleProprietorManager = $soleProprietorManager;
        $this->config = $config;
    }

    /**
     * Show SoleProprietor collection (GET)
     *
     * @return \Zend\Http\Response|ViewModel
     *
     * Права на доступ: Owner и Operator.
     * @throws \Exception
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);

        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->selectUserByLogin($this->baseAuthManager->getIdentity());

            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');
            // Check permissions // Important! Operator has specific interface of view
            if (!($profile === false && $isOperator) && !($profile === 'profile' && !$isOperator)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $paginationParams = $this->getPaginationParams($this->config);
            // Get soleProprietors collection // Can throw Exception
            $soleProprietors = $this->soleProprietorManager
                ->getSoleProprietorCollection($user, $paginationParams, $isOperator);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginator = null;
        if (method_exists($soleProprietors , 'getPages')) {
            $paginator = $soleProprietors->getPages();
        }
        // Get Output
        $soleProprietorsOutput = $this->soleProprietorManager
            ->extractSoleProprietorCollectionOutput($soleProprietors);

        $data = [
            'paginator'         => $paginator,
            'soleProprietors'   => $soleProprietorsOutput,
            'is_operator'       => $isOperator,
        ];

        // Ajax -
        if ($isAjax) {
            return $this->message()->success('SoleProprietor collection', $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/sole-proprietor/collection');

        return $view;
    }

    /**
     * Show SoleProprietor details (GET)
     *
     * @param mixed $id
     * @return \Zend\Http\Response|ViewModel
     *
     * Права на доступ: Owner и Operator
     * @throws \Exception
     */
    public function get($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);

        try {
            /** @var SoleProprietor $soleProprietor */ // Can throw Exception
            $soleProprietor = $this->soleProprietorManager->getSoleProprietorById($id);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');

        // Check permissions // Important! Only Owner or Operator can see SoleProprietor details
        if (!($profile === false && $isOperator) &&
            !($profile === 'profile' && $this->access('profile.own.edit', ['civilLawSubjectChild' => $soleProprietor]))) {

            return $this->redirect()->toRoute('not-authorized');
        }
        // Extracted soleProprietor
        $soleProprietorOutput = $this->soleProprietorManager->getSoleProprietorOutput($soleProprietor);

        $paymentMethods = $soleProprietor->getCivilLawSubject()->getPaymentMethods();
        $paymentMethodsOutput = $this->paymentMethodManager->extractPaymentMethodCollection($paymentMethods);

        $data = [
            'soleProprietor'        => $soleProprietorOutput,
            'is_operator'           => $isOperator,
            'paymentMethods'        => $paymentMethodsOutput,
            'civil_law_subject_id'  => $soleProprietorOutput['civil_law_subject_id'],
        ];

        // Ajax -
        if ( $isAjax ){
            return $this->message()->success('SoleProprietor single', $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/sole-proprietor/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }
        $ndsTypes = $this->ndsTypeManager->getActiveNdsTypes();
        $form = new SoleProprietorForm($ndsTypes);

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->selectUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            if ($this->rbacManager->hasRole($user, 'Operator')) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $form->setData($data);
            // validate form
            if ($form->isValid()) {
                $formData = $form->getData();

                $formData['user'] = $user;
                // Save Form Data
                $resultSavedForm = $this->civilLawSubjectManager
                    ->saveFormData($formData, CivilLawSubjectManager::SOLE_PROPRIETOR_NAME);
                // Hydrate $resultSavedForm for Ajax
                $hydratedData = $this->soleProprietorManager->extractSetOfObjects($resultSavedForm);
                /** @var SoleProprietor $soleProprietor */
                $soleProprietor = $resultSavedForm['soleProprietor'];
                $hydratedData['civil_law_subject_id'] = $soleProprietor->getCivilLawSubject()->getId();

                return $this->message()->success('create sole proprietor', $hydratedData);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            $formDataInit = $this->processFormDataInit();

            // Если не найден редактируемый объект, значит, его не в базе
            // или у пользователя нет прав его редактировать
            if (null === $formDataInit['soleProprietor']) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $ndsTypes = $this->ndsTypeManager->getNdsTypes();
            $form = new SoleProprietorForm($ndsTypes);

            //set data to form
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData['idSoleProprietor'] = $id;
                // Save Form Data
                $resultSavedForm = $this->civilLawSubjectManager->saveFormData($formData, CivilLawSubjectManager::SOLE_PROPRIETOR_NAME);
                // Hydrate $resultSavedForm for Ajax
                $hydratedData = $this->soleProprietorManager->extractSetOfObjects($resultSavedForm);

                return $this->message()->success('update sole proprietor', $hydratedData);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @return bool|TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var SoleProprietor $soleProprietor */ // Can throw Exception
            $soleProprietor = $this->soleProprietorManager->getSoleProprietorById($id);

            // Check permissions // Important! Only owner can delete his SoleProprietor
            if (!$this->access('profile.own.edit', ['civilLawSubjectChild' => $soleProprietor])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            // Deletion
            $this->civilLawSubjectManager->remove($soleProprietor->getCivilLawSubject());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Ajax success
        if ($isAjax) {

            return $this->message()->success(self::SUCCESS_DELETE);
        }
        // Http success
        return $this->redirect()->toRoute('user-profile/sole-proprietor');
    }

    /**
     * Form render
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function createFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();
            // Create form
            $ndsTypes = $this->ndsTypeManager->getNdsTypes();
            $form = new SoleProprietorForm($ndsTypes);

            if (!isset($formDataInit['user']) || !$formDataInit['user'] instanceof User) {
                // @TODO Поменять код ошибки
                throw new LogicException(null, LogicException::USER_BY_LOGIN_NOT_FOUND);
            }

            $user = $formDataInit['user'];

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['user'] = $user;
                    $resultSavedForm = $this->civilLawSubjectManager->saveFormData($formData, CivilLawSubjectManager::SOLE_PROPRIETOR_NAME);

                    //http success
                    return $this->redirect()->toRoute('profile');
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $view = new TwigViewModel([
            'soleProprietorForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/sole-proprietor/create-form');

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function editFormAction()
    {
        // only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();

            $ndsTypes = $this->ndsTypeManager->getNdsTypes();
            $form = new SoleProprietorForm($ndsTypes);

            if (!isset($formDataInit['soleProprietor']) || !$formDataInit['soleProprietor'] instanceof SoleProprietor) {

                throw new LogicException(null, LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_SOLE_PROPRIETOR);
            }

            /** @var SoleProprietor $soleProprietor */
            $soleProprietor = $formDataInit['soleProprietor'];
            // Extracted soleProprietor
            $soleProprietorOutput = $this->soleProprietorManager->getSoleProprietorOutput($formDataInit['soleProprietor']);

            // Set data to form
            $form = $this->soleProprietorManager->setFormData($form, $soleProprietor);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                // set data to form
                $form->setData($postData);
                // validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idSoleProprietor'] = $soleProprietor->getId();
                    $resultSavedForm = $this->civilLawSubjectManager
                        ->saveFormData($formData, CivilLawSubjectManager::SOLE_PROPRIETOR_NAME);

                    // http success
                    return $this->redirect()->toRoute('user-profile/sole-proprietor');
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');

        $form->prepare();
        $view = new TwigViewModel([
            'soleProprietor'        => $soleProprietorOutput,
            'soleProprietorForm'    => $form,
            'is_operator'           => $isOperator
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/sole-proprietor/edit-form');

        return $view;
    }

    /**
     * Remove LegalEntity (GET)
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function deleteFormAction()
    {
        // only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();

            if ( !isset($formDataInit['soleProprietor']) || !$formDataInit['soleProprietor'] instanceof SoleProprietor) {

                throw new LogicException(null,
                    LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_SOLE_PROPRIETOR);
            }
            // Extracted soleProprietor
            $soleProprietorOutput = $this->soleProprietorManager
                ->getSoleProprietorOutput($formDataInit['soleProprietor']);

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Http -
        $view = new TwigViewModel([
            'soleProprietor' => $soleProprietorOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        // only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // Hydrate collection and objects for Ajax
        $formDataInit['user'] = $this->userManager->extractUser($formDataInit['user']);
        $formDataInit['allowedUser'] = $this->userManager->extractUser($formDataInit['allowedUser']);

        if (null !== $formDataInit['soleProprietor']) {
            $formDataInit['soleProprietor'] =
                $this->soleProprietorManager->getSoleProprietorOutput($formDataInit['soleProprietor']);
        }

        $formDataInit['allowedSoleProprietors'] = $formDataInit['allowedSoleProprietors']
            ? $this->soleProprietorManager->extractSoleProprietorCollectionOutput($formDataInit['allowedSoleProprietors'])
            : null;

        $formDataInit['handbooks']['ndsTypes'] = $this->ndsTypeManager->extractNdsTypeCollection($formDataInit['handbooks']['ndsTypes']);

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     *
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }
        $allowedUser = null;

        // process data for update or delete
        $soleProprietor = null;
        if (isset($incomingData['idSoleProprietor']) || isset($incomingData['id'])) {

            $id = isset($incomingData['id']) ? $incomingData['id'] : $incomingData['idSoleProprietor'];

            /** @var SoleProprietor $soleProprietor */
            $soleProprietor = $this->soleProprietorManager->getSoleProprietorById((int) $id);

            // Check if soleProprietor belongs to current user
            if ($soleProprietor && $soleProprietor->getCivilLawSubject()->getUser() !== $user) {

                $soleProprietor = null;
            }
        }

        $allowedSoleProprietors = null;
        if (null !== $user) {
            $allowedSoleProprietors = $this->soleProprietorManager->getAllowedSoleProprietorsByUser($user);
        }
        $ndsTypes = $this->ndsTypeManager->getActiveNdsTypes();

        return [
            //create
            'user' => $user,
            'allowedUser' => $allowedUser,
            //edit
            'soleProprietor' => $soleProprietor,
            'allowedSoleProprietors' => $allowedSoleProprietors,
            //handbooks
            'handbooks' => [
                'ndsTypes' => $ndsTypes
            ]
        ];
    }
}