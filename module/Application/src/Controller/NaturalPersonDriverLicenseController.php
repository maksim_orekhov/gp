<?php

namespace Application\Controller;

use Core\Controller\AbstractRestfulController;
use Application\Entity\NaturalPerson;
use Application\Entity\NaturalPersonDriverLicense;
use Application\Form\NaturalPersonDriverLicenseForm;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Application\Service\CivilLawSubject\NaturalPersonDriverLicenseManager;
use Doctrine\ORM\EntityManager;
use Zend\View\Model\ViewModel;
use Zend\Hydrator\ClassMethods;

/**
 * Class NaturalPersonDriverLicenseController
 * @package Application\Controller
 */
class NaturalPersonDriverLicenseController extends AbstractRestfulController
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * Auth Manager
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\CivilLawSubject\NaturalPersonManager
     */
    private $naturalPersonManager;

    /**
     * @var \Application\Service\CivilLawSubject\NaturalPersonDocumentManager
     */
    private $naturalPersonDocumentManager;

    /**
     * @var NaturalPersonDriverLicenseManager
     */
    private $naturalPersonDriverLicenseManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * @var array
     */
    private $config;

    /**
     * LegalEntityDocumentController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param NaturalPersonManager $naturalPersonManager
     * @param NaturalPersonDocumentManager $naturalPersonDocumentManager
     * @param NaturalPersonDriverLicenseManager $naturalPersonDriverLicenseManager
     * @param RbacManager $rbacManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                NaturalPersonManager $naturalPersonManager,
                                NaturalPersonDocumentManager $naturalPersonDocumentManager,
                                NaturalPersonDriverLicenseManager $naturalPersonDriverLicenseManager,
                                RbacManager $rbacManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->naturalPersonManager = $naturalPersonManager;
        $this->naturalPersonDocumentManager = $naturalPersonDocumentManager;
        $this->naturalPersonDriverLicenseManager = $naturalPersonDriverLicenseManager;
        $this->rbacManager = $rbacManager;
        $this->config = $config;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function getList()
    {
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // Whether the currant user yas role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            // Check permissions // Important! Operator has specific interface of view
            if (!($profile === false && $isOperator) && !($profile == 'profile' && !$isOperator)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $paginationParams = $this->getPaginationParams($this->config);
            $driverLicenses = $this->naturalPersonDriverLicenseManager->getCollectionDriverLicense($user, $paginationParams, $isOperator);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginator = null;
        if (method_exists($paginator , 'getPages')) {
            $paginator = $paginator->getPages();
        }

        // Get Output
        $driverLicensesOutput = $this->naturalPersonDriverLicenseManager
            ->extractDriverLicenseCollection($driverLicenses);

        $data = [
            'paginator'      => $paginator,
            'driverLicenses' => $driverLicensesOutput,
            'is_operator'    => $isOperator,
        ];

        if ($isAjax) {
            return $this->message()->success("collection driver license", $data);
        }

        $view =  new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person-driver-license/collection');

        return $view;
    }

    /**
     * @param mixed $id
     * @return \Zend\Http\Response|ViewModel
     */
    public function get($id)
    {
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        if (!isset($id) || (int) $id <= 0) {
            return $this->message()->error('bad data provided for update');
        }
        try {
            $driverLicense = $this->naturalPersonDriverLicenseManager->getDriverLicenseById($id);
            $naturalPerson = $driverLicense->getNaturalPersonDocument()->getNaturalPerson();

            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            // Check permissions // Important! Only Owner or Operator can see NaturalPersonPassport details
            if (!($profile === false && $isOperator) &&
                !($profile == 'profile' && $this->access('profile.own.edit', ['naturalPerson' => $naturalPerson]))) {
                return $this->redirect()->toRoute('not-authorized');
            }

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
        // Extracted driverLicense
        $driverLicenseOutput = $this->naturalPersonDriverLicenseManager
            ->getDriverLicenseOutput($driverLicense);

        $data = [
            'driverLicense' => $driverLicenseOutput,
            'is_operator' => $isOperator
        ];
        //Ajax success
        if ( $isAjax ){
            return $this->message()->success("driver license single", $data);
        }
        //Http success
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person-driver-license/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $form = new NaturalPersonDriverLicenseForm('document');

        try {
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $resultSavedForm = $this->saveFormData($formData);

                //Ajax success
                return $this->message()->success('create driver license', $resultSavedForm);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $form = new NaturalPersonDriverLicenseForm('document');

        try {
            //remove check to file required
            $fileFilter = $form->getInputFilter()->get('file');
            $fileFilter->setRequired(false);
            //set data to form
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData['idDriverLicense'] = $id;
                $resultSavedForm = $this->saveFormData($formData);
                //Ajax success
                return $this->message()->success('update driver license', $resultSavedForm);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @return \Zend\Http\Response
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $driverLicense = $this->naturalPersonDriverLicenseManager->getDriverLicenseById($id);
            $naturalPerson = $driverLicense->getNaturalPersonDocument()->getNaturalPerson();
            $document = $driverLicense->getNaturalPersonDocument();

            // Check permissions // Important! Only Owner or Operator can see NaturalPerson collection
            if (!$this->access('profile.own.edit', ['naturalPerson' => $naturalPerson])) {
                return $this->redirect()->toRoute('not-authorized');
            }

            // Deleting
            $this->naturalPersonDocumentManager->remove($document);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        //Ajax success
        if ($isAjax) {
            return $this->message()->success('Документ удален');
        }
        //Http success
        // @TODO Куда возвращать?
        return $this->redirect()->toRoute(
            'user-profile/natural-person-single',
            ['id' => $naturalPerson->getId()]
        );
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function createFormAction()
    {
        $formDataInit = $this->processFormDataInit();

        $form = new NaturalPersonDriverLicenseForm('document');

        try {
            if ( !isset($formDataInit['naturalPerson']) || !$formDataInit['naturalPerson'] instanceof NaturalPerson){
                throw new \Exception('bad data provided: not found natural person');
            }
            $naturalPerson = $formDataInit['naturalPerson'];
            $form->get('idNaturalPerson')->setValue($naturalPerson->getId());

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $filesData = $this->collectIncomingData()['filesData'];
                if (\count($filesData) && $filesData['file']['error'] !== 4) {   //4 - Файл не был загружен
                    $postData = array_merge_recursive(
                        $postData,
                        $filesData
                    );
                }
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idNaturalPerson'] = $naturalPerson->getId();
                    $resultSavedForm = $this->saveFormData($formData);

                    //http success
                    return $this->redirect()->toRoute(
                        'user-profile/natural-person-single',
                        ['id' => $resultSavedForm['naturalPerson']->getId()]
                    );
                }
            }
        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
        $form->prepare();
        $view = new TwigViewModel([
            'driverLicenseForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person-driver-license/create-form');

        return $view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function editFormAction()
    {
        $formDataInit = $this->processFormDataInit();

        $form = new NaturalPersonDriverLicenseForm('document');

        try {
            if ( !isset($formDataInit['driverLicense']) || !$formDataInit['driverLicense'] instanceof NaturalPersonDriverLicense){
                throw new \Exception('bad data provided: not driver license');
            }
            $driverLicense = $formDataInit['driverLicense'];
            // Extracted driverLicense
            $driverLicenseOutput = $this->naturalPersonDriverLicenseManager
                ->getDriverLicenseOutput($formDataInit['driverLicense']);

            $form->setData([
                'date_issued' => $driverLicense->getDateIssued()->format('d.m.Y'),
            ]);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $filesData = $this->collectIncomingData()['filesData'];
                if (count($filesData) && $filesData['file']['error'] !== 4) {   //4 - Файл не был загружен
                    $postData = array_merge_recursive(
                        $postData,
                        $filesData
                    );
                }
                //remove check to file required
                $fileFilter = $form->getInputFilter()->get('file');
                $fileFilter->setRequired(false);
                //set data to form
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idDriverLicense'] = $driverLicense->getId();
                    $resultSavedForm = $this->saveFormData($formData);

                    //http success
                    return $this->redirect()->toRoute(
                        'user-profile/natural-person-single',
                        ['id' => $resultSavedForm['naturalPerson']->getId()]
                    );
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
        $form->prepare();
        $view = new TwigViewModel([
            'driverLicense' => $driverLicenseOutput,
            'driverLicenseForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person-driver-license/edit-form');

        return $view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function deleteFormAction()
    {
        $formDataInit = $this->processFormDataInit();

        try {
            if ( !isset($formDataInit['driverLicense']) || !$formDataInit['driverLicense'] instanceof NaturalPersonDriverLicense){
                throw new \Exception('bad data provided: not driver license');
            }
            // Extracted driverLicense
            $driverLicenseOutput = $this->naturalPersonDriverLicenseManager
                ->getDriverLicenseOutput($formDataInit['driverLicense']);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        $view = new TwigViewModel([
            'driverLicense' => $driverLicenseOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person-driver-license/delete-form');

        return $view;
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array
     */
    private function processFormDataInit()
    {
        //step 1
        $incomingData = $this->collectIncomingData();

        //check
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            //process data for create
            $naturalPerson = null;
            if ( isset($incomingData['idNaturalPerson']) ){
                $naturalPerson = $this->naturalPersonManager->getBasedOnPattern((int) $incomingData['idNaturalPerson']);
            }
            $allowedNaturalPersons = $this->naturalPersonManager->getAllowedNaturalPersonByUser($user);

            //process data for update or delete
            $driverLicense = null;
            if ( isset($incomingData['idDriverLicense']) ){
                $driverLicense = $this->naturalPersonDriverLicenseManager->getDriverLicenseById((int) $incomingData['idDriverLicense']);
            }
            $allowedDriverLicenses = $this->naturalPersonDriverLicenseManager->getAllowedDriverLicenseByUser($user);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        //step 2
        return [
            //create
            'naturalPerson' => $naturalPerson,
            'allowedNaturalPersons' => $allowedNaturalPersons,
            //edit
            'driverLicense' => $driverLicense,
            'allowedDriverLicenses' => $allowedDriverLicenses,
        ];
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    private function saveFormData($data)
    {
        // Get current user // Can throw Exception
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

        if ( isset($data['idDriverLicense']) ) {
            //update
            if ((int) $data['idDriverLicense'] <= 0 ) {
                throw new \Exception('bad data provided: not found driver license id');
            }

            $driverLicense = $this->naturalPersonDriverLicenseManager->getDriverLicenseById((int) $data['idDriverLicense']);

            if( $driverLicense === null) {
                throw new \Exception('bad data provided: not found driver license by id');
            }

            // Check if driverLicense belongs to current user
            if ($driverLicense->getNaturalPersonDocument()->getNaturalPerson()->getCivilLawSubject()->getUser() !== $user) {
                throw new \Exception('driverLicense not belongs to current user');
            }

            $document = $this->naturalPersonDocumentManager
                ->update($driverLicense->getNaturalPersonDocument(), $data);

            $result = [
                'driverLicense' => $document->getNaturalPersonDriverLicense(),
                'naturalPerson' => $document->getNaturalPerson()
            ];
        } else {
            //create
            if ( !isset($data['idNaturalPerson']) || (int) $data['idNaturalPerson'] <= 0){
                throw new \Exception('bad data provided: not found natural person id');
            }

            $naturalPerson = $this->naturalPersonManager->getBasedOnPattern((int) $data['idNaturalPerson']);

            if( $naturalPerson === null) {
                throw new \Exception('bad data provided: not found natural person by id');
            }

            // Check if naturalPerson belongs to current user
            if ($naturalPerson->getCivilLawSubject()->getUser() !== $user) {
                throw new \Exception('naturalPerson not belongs to current user');
            }

            $document = $this->naturalPersonDocumentManager
                ->create($naturalPerson, NaturalPersonDriverLicense::class, $data);
            $driverLicense = $document->getNaturalPersonDriverLicense();

            $result = [
                'driverLicense' => $driverLicense,
                'naturalPerson' => $naturalPerson
            ];
        }

        return $result;
    }
}