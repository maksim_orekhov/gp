<?php

namespace Application\Controller;

use Application\Entity\ArbitrageRequest;
use Application\Entity\Deal;
use Application\Entity\Dispute;
use Application\Form\ArbitrageRequestForm;
use Application\Entity\User;
use Application\Form\RequestConfirmForm;
use Application\Form\RequestRefuseForm;
use Application\Listener\TribunalRequestListener;
use Application\Service\Deal\DealManager;
use Application\Service\Dispute\ArbitrageManager;
use Application\Service\Dispute\DisputeManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;

/**
 * Class ArbitrageController
 * @package Application\Controller
 */
class ArbitrageController extends AbstractRestfulController
{
    /**
     * @var ArbitrageManager
     */
    private $arbitrageManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var TribunalRequestListener
     */
    private $tribunalRequestListener;

    /**
     * ArbitrageController constructor.
     * @param ArbitrageManager $arbitrageManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param DealManager $dealManager
     * @param DisputeManager $disputeManager
     * @param TribunalRequestListener $tribunalRequestListener
     */
    public function __construct(ArbitrageManager $arbitrageManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                DealManager $dealManager,
                                DisputeManager $disputeManager,
                                TribunalRequestListener $tribunalRequestListener)
    {
        $this->arbitrageManager = $arbitrageManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->dealManager = $dealManager;
        $this->disputeManager = $disputeManager;
        $this->tribunalRequestListener = $tribunalRequestListener;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function arbitrageRequestAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try{
            $formDataInit = $this->processFormDataInit();

            if (!isset($formDataInit['deal']) || !$formDataInit['deal'] instanceof Deal){

                throw new \Exception('bad data provided: Deal not found');
            }
            /** @var Deal $deal */
            $deal = $formDataInit['deal'];
            /** @var User $user */
            $user = $formDataInit['user'];
            //check permission
            if (!$this->arbitrageManager->checkPermissionArbitrageRequest($user, $deal)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $arbitrageRequestForm = new ArbitrageRequestForm($deal);
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);
            $this->arbitrageManager->setDisputeOutput($disputeOutput);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData) && $postData['type_form'] === ArbitrageRequestForm::TYPE_FORM) {
                //set data to form
                $arbitrageRequestForm->setData($postData);
                //validate form
                if ($arbitrageRequestForm->isValid()) {
                    $author = $this->dealManager->getUserAgentByDeal($user, $deal);
                    $arbitrageRequest = $this->arbitrageManager->createArbitrageRequest($deal, $author);

                    if($isAjax) {
                        $arbitrageRequestOutput = $this->arbitrageManager->getArbitrageRequestOutput($arbitrageRequest);
                        // Return Json with ERROR status
                        return $this->message()->success(
                            'Arbitrage request created',
                            ['arbitrageRequest' => $arbitrageRequestOutput]
                        );
                    }

                    //@TODO Куда редиректить после success
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                //ajax response
                if($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($arbitrageRequestForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){
            return $this->message()->error($t->getMessage());
        }

        $arbitrageRequestForm->prepare();
        $view = new TwigViewModel([
            'arbitrageRequestForm' => $arbitrageRequestForm,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \RuntimeException
     * @throws \Exception
     */
    public function arbitrageConfirmAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $formDataInit = $this->processFormDataInit();

            if (!isset($formDataInit['arbitrageRequest']) || !$formDataInit['arbitrageRequest'] instanceof ArbitrageRequest) {

                throw new \Exception('bad data provided: Arbitrage request not found');
            }
            /** @var ArbitrageRequest $arbitrageRequest */
            $arbitrageRequest = $formDataInit['arbitrageRequest'];
            /** @var User $user */
            $user = $formDataInit['user'];
            /** @var Deal $deal */
            $deal = $arbitrageRequest->getDispute()->getDeal();
            //check permission
            if (!$this->arbitrageManager->checkPermissionArbitrageConfirm($user, $deal)) {

                return $this->redirect()->toRoute('not-authorized');
            }
            //check of confirmation
            if ($arbitrageRequest->getConfirm()) {

                throw new \Exception('On the current request already decided');
            }

            $arbitrageConfirmForm = new RequestConfirmForm();
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);
            $this->arbitrageManager->setDisputeOutput($disputeOutput);
            $arbitrageRequestOutput = $this->arbitrageManager->getArbitrageRequestOutput($arbitrageRequest);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData) && $postData['type_form'] === RequestConfirmForm::TYPE_FORM) {
                //set data to form
                $arbitrageConfirmForm->setData($postData);
                //validate form
                if ($arbitrageConfirmForm->isValid()) {
                    //user - operator
                    $this->arbitrageManager->createArbitrageConfirm($arbitrageRequest, $user);
                    //ajax response
                    if($isAjax) {
                        // Return Json with ERROR status
                        return $this->message()->success('arbitrage confirm success');
                    }
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                //ajax response
                if($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($arbitrageConfirmForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $arbitrageConfirmForm->prepare();
        $view = new TwigViewModel([
            'arbitrageConfirmForm' => $arbitrageConfirmForm,
            'arbitrageRequest' => $arbitrageRequestOutput,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function arbitrageRefuseAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $formDataInit = $this->processFormDataInit();

            if (!isset($formDataInit['arbitrageRequest']) || !$formDataInit['arbitrageRequest'] instanceof ArbitrageRequest) {

                throw new \Exception('bad data provided: Arbitrage request not found');
            }
            /** @var ArbitrageRequest $arbitrageRequest */
            $arbitrageRequest = $formDataInit['arbitrageRequest'];
            /** @var User $user */
            $user = $formDataInit['user'];
            /** @var Deal $deal */
            $deal = $arbitrageRequest->getDispute()->getDeal();
            //check permission
            if (!$this->arbitrageManager->checkPermissionArbitrageRefuse($user, $deal)) {

                return $this->redirect()->toRoute('not-authorized');
            }
            //check of confirmation
            if ($arbitrageRequest->getConfirm()) {

                throw new \Exception('On the current request already decided');
            }

            $arbitrageRefuseForm = new RequestRefuseForm();
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);
            $this->arbitrageManager->setDisputeOutput($disputeOutput);
            $arbitrageRequestOutput = $this->arbitrageManager->getArbitrageRequestOutput($arbitrageRequest);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData) && $postData['type_form'] === RequestRefuseForm::TYPE_FORM) {
                //set data to form
                $arbitrageRefuseForm->setData($postData);
                //validate form
                if ($arbitrageRefuseForm->isValid()) {
                    //user - operator
                    $this->arbitrageManager->createArbitrageRefuse($arbitrageRequest, $user);

                    /** @var Dispute $dispute */
                    $dispute = $deal->getDispute();
                    $tribunalRequests = $dispute->getTribunalRequests();

                    if ($tribunalRequests->count() === 0
                        && $this->arbitrageManager->checkArbitrageRefusesOverlimit($deal->getDispute())) {
                        // Subscribe on event "onRequestDeclineOverlimit"
                        $this->tribunalRequestListener->subscribe();
                        // Trigger event "onRequestDeclineOverlimit"
                        $this->tribunalRequestListener->trigger(
                            $deal,
                            $arbitrageRequest->getAuthor()
                        );
                    }

                    //@TODO Куда редиректить после success
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                //ajax response
                if($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($arbitrageRefuseForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){
            return $this->message()->error($t->getMessage());
        }

        $arbitrageRefuseForm->prepare();
        $view = new TwigViewModel([
            'arbitrageRefuseForm' => $arbitrageRefuseForm,
            'arbitrageRequest' => $arbitrageRequestOutput,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $allowedUser = null;

            $deal = null;
            if (isset($incomingData['idDeal'])){

                /** @var $deal $deal */
                $deal = $this->dealManager
                    ->getDealById((int) $incomingData['idDeal']);
            }
            $allowedDeals = $this->dealManager->getAllowedDealByUser($user);
            $allowedDeals = !empty($allowedDeals) ? $allowedDeals : null;

            $arbitrageRequest = null;
            if (isset($incomingData['idRequest'])){
                /** @var ArbitrageRequest $arbitrageRequest */
                $arbitrageRequest = $this->arbitrageManager
                    ->getArbitrageRequestById((int) $incomingData['idRequest']);
            }

            $allowedArbitrageRequests = $this->arbitrageManager->getAllowedArbitrageRequestsForUserOperator($user);
            $allowedArbitrageRequests = !empty($allowedArbitrageRequests) ? $allowedArbitrageRequests : null;

        } catch (\Throwable $t){

            throw new \Exception($t->getMessage());
        }

        return [
            'user' => $user,
            'allowedUser' => $allowedUser,
            'deal' => $deal,
            'allowedDeals' => $allowedDeals,
            'arbitrageRequest' => $arbitrageRequest,
            'allowedArbitrageRequests' => $allowedArbitrageRequests,
        ];
    }
}