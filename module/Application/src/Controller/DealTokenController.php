<?php

namespace Application\Controller;

use Application\Entity\Token;
use Application\Form\DealPartnerTokenForm;
use Application\Form\DealPartnerTokenSlugForm;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealTokenManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Controller\AbstractRestfulController;
use Core\Exception\LogicException;
use Core\Service\TwigViewModel;
use Zend\Form\Element;
use Zend\View\Model\ViewModel;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Entity\User;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Doctrine\ORM\EntityManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class DealTokenController
 * @package Application\Controller
 */
class DealTokenController extends AbstractRestfulController
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DealTokenManager
     */
    private $dealTokenManager;

    /**
     * @var array
     */
    private $config;

    /**
     * DealTokenController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param RbacManager $rbacManager
     * @param DealManager $dealManager
     * @param DealTokenManager $dealTokenManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                PaymentMethodManager $paymentMethodManager,
                                RbacManager $rbacManager,
                                DealManager $dealManager,
                                DealTokenManager $dealTokenManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->civilLawSubjectManager = $civilLawSubjectManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->rbacManager = $rbacManager;
        $this->dealManager = $dealManager;
        $this->dealTokenManager = $dealTokenManager;
        $this->config = $config;
    }

    /**
     * Show Token collection (GET)
     *
     * @return \Zend\Http\Response|ViewModel
     *
     * Права на доступ: Owner и Operator.
     * @throws \Exception
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // Если есть и равен 'profile', то пользователь, если нет - Оператор
            $profile = $this->params()->fromRoute('profile', false);

            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');
            // Check permissions // Important! Operator has specific interface of view
            if (!($profile === false && $isOperator) && !($profile === 'profile' && !$isOperator)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            #$paginationParams = $this->getPaginationParams($this->config);
            // Get token collection
            // Пока простой список, потом с пагинацией
            #$tokens = $this->dealPartnerTokenManager->getTokenCollection($user, $paginationParams, $isOperator);\
            if (true === $isOperator) {
                $tokens = $this->dealTokenManager->getAllTokens();
            } else {
                $tokens = $this->dealTokenManager->getUsersCounteragentTokens($user);
            }
//           $paginator = null;
//           if (method_exists($tokens , 'getPages')) {
//               $paginator = $tokens->getPages();
//           }
            // Get Output
            $tokensOutput = $this->dealTokenManager->getTokensArrayOutput($tokens);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            #'paginator'     => $paginator,
            'tokens'        => $tokensOutput,
            'is_operator'   => $isOperator,
        ];

        // Ajax -
        if ($isAjax) {
            return $this->message()->success('Token collection', $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/partner-button/collection');

        return $view;
    }

    /**
     * Show DealToken details (GET)
     *
     * @param mixed $id
     * @return \Zend\Http\Response|ViewModel
     *
     * Права на доступ: Owner и Operator
     * @throws \Exception
     */
    public function get($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);

        try {
            /** @var Token $token */
            $token = $this->dealTokenManager->getToken($id);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');
        // Check permissions // Important! Only Owner or Operator can see deal partner Token details
        if (!($profile === false && $isOperator) &&
            !($profile === 'profile' && $this->access('profile.own.edit', ['dealPartnerToken' => $token]))) {

            return $this->redirect()->toRoute('not-authorized');
        }

        $tokenOutput = $this->dealTokenManager->getTokenOutput($token);

        $data = [
            'token'         => $tokenOutput,
            'is_operator'   => $isOperator,
        ];

        // Ajax -
        if ( $isAjax ){
            return $this->message()->success('DealToken single', $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/partner-button/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|bool|TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     *
     * Права на доступ: Verified
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->selectUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            if ($this->rbacManager->hasRole($user, 'Operator')) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $form = new DealPartnerTokenForm($this->dealManager, $user, 'create', false);

            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                if( isset($data['slug'])) {
                    $existsToken = $this->dealTokenManager->getTokenByKey($data['slug']);
                    if (null !== $existsToken) {

                        throw new LogicException(null, LogicException::DEAL_TOKEN_CREATION_SUCH_SLUG_ALREADY_USED);
                    }
                }
                // Save Form Data
                $token = $this->dealTokenManager->createDealPartnerToken($user, $formData);
                // Output
                $tokenOutput = $this->dealTokenManager->getTokenOutput($token);

                return $this->message()->success('create deal partner token', $tokenOutput);
            }

            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            $formDataInit = $this->processFormDataInit();

            if (!isset($formDataInit['user'])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            if ( !isset($formDataInit['token']) || !$formDataInit['token'] instanceof Token) {

                throw new LogicException(null, LogicException::DEAL_TOKEN_NOT_FOUND);
            }
            /** @var User $user */
            $user = $formDataInit['user'];
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            /** @var Token $token */
            $token = $formDataInit['token'];
            // Edit form
            $form = new DealPartnerTokenForm($this->dealManager, $user, 'edit', $isOperator);

            //set data to form
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                if ( isset($data['slug'])) {
                    $existsToken = $this->dealTokenManager->getTokenByKey($data['slug']);
                    if (null !== $existsToken) {

                        throw new LogicException(null, LogicException::DEAL_TOKEN_CREATION_SUCH_SLUG_ALREADY_USED);
                    }
                }

                $formData['token'] = $id;
                // Update Form Data
                $this->dealTokenManager->editDealPartnerToken($user, $token, $formData);
                // Hydrate $resultSavedForm for Ajax
                $tokenOutput = $this->dealTokenManager->getTokenOutput($token);

                return $this->message()->success('update deal partner token', $tokenOutput);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @return bool|TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                // Get current user // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            /** @var Token $token */
            $token = $this->dealTokenManager->getToken($id);
            if ($token && $token->getCounteragentUser() !== $user && $token->getBeneficiarUser() !== $user) {

                throw new LogicException(null, LogicException::DEAL_TOKEN_DELETE_FORBIDDEN_TO_CURRENT_USER);
            }

            // Check permissions // Important! Only owner can delete his legalEntity
            if (!$this->access('profile.own.edit', ['dealPartnerToken' => $token])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            // Deletion
            $this->dealTokenManager->remove($token);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Ajax success
        if ($isAjax) {

            return $this->message()->success('Partner button successfully deleted');
        }
        // Http success
        return $this->redirect()->toRoute('user-profile');
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function createFormAction()
    {
        // only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();
            if (!isset($formDataInit['user']) || !$formDataInit['user'] instanceof User) {

                throw new \Exception('bad data provided: not found user');
            }
            $user = $formDataInit['user'];
            // Whether the currant user has role Operator
            if ($this->rbacManager->hasRole($user, 'Operator')) {

                return $this->redirect()->toRoute('not-authorized');
            }
            // Create form
            $form = new DealPartnerTokenForm($this->dealManager, $user, 'create', false);
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $form->setData($postData);
                // validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    if ( isset($formData['slug'])) {
                        $existsToken = $this->dealTokenManager->getTokenByKey($formData['slug']);
                        if (null !== $existsToken) {

                            throw new LogicException(null, LogicException::DEAL_TOKEN_CREATION_SUCH_SLUG_ALREADY_USED);
                        }
                    }
                    $this->dealTokenManager->createDealPartnerToken($user, $formData);

                    // http success
                    return $this->redirect()->toRoute('user-profile');
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'dealTokenForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/partner-button/create-form');

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function editFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();
            if (!isset($formDataInit['user'])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            if ( !isset($formDataInit['token']) || !$formDataInit['token'] instanceof Token) {

                throw new LogicException(null, LogicException::DEAL_TOKEN_NOT_FOUND);
            }
            /** @var User $user */
            $user = $formDataInit['user'];
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            /** @var Token $token */
            $token = $formDataInit['token'];
            // Edit form
            $form = new DealPartnerTokenForm($this->dealManager, $user, 'edit', $isOperator);
            // Set data to form
            $form = $this->dealTokenManager->setFormData($form, $token, $isOperator);
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                //set data to form
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    if ( isset($postData['slug'])) {
                        $existsToken = $this->dealTokenManager->getTokenByKey($postData['slug']);
                        if (null !== $existsToken) {

                            throw new LogicException(null, LogicException::DEAL_TOKEN_CREATION_SUCH_SLUG_ALREADY_USED);
                        }
                    }

                    $formData['idToken'] = $token->getId();
                    // Update Form Data
                    $this->dealTokenManager->editDealPartnerToken($user, $token, $formData);
                    //http success
                    return $this->redirect()->toRoute('user-profile', ['id'=> $token->getId()]);
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $tokenOutput = $this->dealTokenManager->getTokenOutput($token);
        $form->prepare();
        $view = new TwigViewModel([
            'token'         => $tokenOutput,
            'dealTokenForm' => $form,
            'is_operator'   => $isOperator
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/partner-button/edit-form');

        return $view;
    }

    /**
     * @return bool|TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function deleteFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();
            if ( !isset($formDataInit['token']) || !$formDataInit['token'] instanceof Token) {

                throw new LogicException(null,LogicException::DEAL_TOKEN_NOT_FOUND);
            }
            /** @var Token $token */
            $token = $formDataInit['token'];
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            if ($isOperator && $token) {

                throw new LogicException(null, LogicException::DEAL_TOKEN_DELETE_FORBIDDEN_TO_CURRENT_USER);
            }
            // Extracted legalEntity
            $token = $this->dealTokenManager->getTokenOutput($formDataInit['token']);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Http -
        $view = new TwigViewModel([
            'token' => $token
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/partner-button/delete-form');

        return $view;
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function switchActiveAction()
    {
        //only Ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }
        $this->message()->setJsonStrategy(true);

        try {
            $formDataInit = $this->processFormDataInit();
            if (!isset($formDataInit['user'])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            if ( !isset($formDataInit['token']) || !$formDataInit['token'] instanceof Token) {

                throw new LogicException(null, LogicException::DEAL_TOKEN_NOT_FOUND);
            }
            /** @var Token $token */
            $token = $formDataInit['token'];
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            if ($isOperator && $token) {

                throw new LogicException(null, LogicException::DEAL_TOKEN_SWITCH_ACTIVE_FORBIDDEN_TO_CURRENT_USER);
            }

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $postData = [
                    'is_active' => isset($postData['is_active']) ? trim($postData['is_active']) : true,
                    'csrf' => isset($postData['csrf']) ? trim($postData['csrf']) : null
                ];

                $csrf = new Element\Csrf('csrf');
                $validator = $csrf->getCsrfValidator();
                if (!$validator->isValid($postData['csrf'])) {

                    return $this->message()->invalidFormData($validator->getMessages());
                }
                $token = $this->dealTokenManager->switchActiveDealPartnerToken($token, (bool)$postData['is_active']);
                //http success
                $tokenOutput = $this->dealTokenManager->getTokenOutput($token);

                return $this->message()->success('Success switch button activity', $tokenOutput);
            }

            return $this->message()->error('Incorrect data provided');
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // Hydrate collection and objects for Ajax
        $formDataInit['user'] = $this->userManager->extractUser($formDataInit['user']);
        $formDataInit['allowedUser'] = $this->userManager->extractUser($formDataInit['allowedUser']);
        if (null !== $formDataInit['token']) {
            $formDataInit['token'] = $this->dealTokenManager->getTokenOutput($formDataInit['token']);
        }

        if (null !== $formDataInit['allowedTokens']) {
            $formDataInit['allowedTokens'] = $this->dealTokenManager->getTokensArrayOutput($formDataInit['allowedTokens']);
        }

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     *
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $user = null;
        $isOperator = false;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
        }
        $allowedUser = null;

        //process data for update or delete
        $token = null;
        if (isset($incomingData['idToken']) || isset($incomingData['id'])) {
            $id = $incomingData['id'] ?? $incomingData['idToken'];
            /** @var Token $token */
            $token = $this->dealTokenManager->getToken((int) $id);
            // Check if CounteragentUser belongs or BeneficiarUser
            if (!$isOperator && $token && $token->getCounteragentUser() !== $user && $token->getBeneficiarUser() !== $user) {

                $token = null;
            }
        }

        $allowedTokens = null;
        if (null !== $user) {
            $allowedTokens = $this->dealTokenManager->getUsersCounteragentTokens($user);
        }

        $naturalPersonsOutput = null;
        $soleProprietorsOutput = null;
        $legalEntitiesOutput = null;
        $counteragentTokensOutput = null;

        if ($user) {
            // Get user's naturalPersons as array
            $naturalPersonsOutput = $this->civilLawSubjectManager
                ->getNaturalPersonsForUser($user, [], true);
            $naturalPersonsOutput = !empty($naturalPersonsOutput) ? $naturalPersonsOutput : null;

            // Get user's soleProprietor as array
            $soleProprietorsOutput = $this->civilLawSubjectManager
                ->getSoleProprietorForUser($user, [], true);
            $soleProprietorsOutput = !empty($soleProprietorsOutput) ? $soleProprietorsOutput : null;

            // Get user's legalEntities as array
            $legalEntitiesOutput = $this->civilLawSubjectManager
                ->getLegalEntitiesForUser($user, [], true);
            $legalEntitiesOutput = !empty($legalEntitiesOutput) ? $legalEntitiesOutput : null;

            $counteragentTokensOutput = $this->dealTokenManager->getUsersCounteragentTokens($user,true);
        }

        /** @var array $deal_types */
        $deal_types = $this->dealManager->getDealTypesAsArray();
        /** @var array $fee_payer_options */
        $fee_payer_options = $this->dealManager->getActiveFeePayerOptionsArray();

        return [
            'deal_types' => $deal_types,
            'fee_payer_options' => $fee_payer_options,
            'naturalPersons' => $naturalPersonsOutput,
            'soleProprietors' => $soleProprietorsOutput,
            'legalEntities' => $legalEntitiesOutput,
            'counteragent_tokens' => $counteragentTokensOutput,
            //create
            'user' => $user,
            'allowedUser' => $allowedUser,
            //edit
            'token' => $token,
            'allowedTokens' => $allowedTokens,
        ];
    }

    /**
     * @return array|bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function isSlugExistsAction()
    {
        // only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->selectUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            if (null === $user) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $form = new DealPartnerTokenSlugForm();

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            $form->setData($postData);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                // Save Form Data
                $token = $this->dealTokenManager->getTokenByKey($formData['slug']);

                return $this->message()->success('Is token with such slug exists', null !== $token);
            }

            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }
}