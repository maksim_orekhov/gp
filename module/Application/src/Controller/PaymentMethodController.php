<?php

namespace Application\Controller;

use Application\Exception\Code\CivilLawSubjectExceptionCodeInterface;
use Application\Exception\Code\PaymentMethodExceptionCodeInterface;
use Core\Controller\AbstractRestfulController;
use Application\Entity\PaymentMethod;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Exception\LogicException;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Application\Entity\CivilLawSubject;
use Application\Form\CivilLawSubjectSelectionForm;
use Application\Form\PaymentMethodTypeSelectionForm;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Application\Entity\User;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleBank\Service\BankManager;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Payment\PaymentMethodStrategy;
use ModuleRbac\Service\RbacManager;

/**
 * Class PaymentMethodController
 * @package Application\Controller
 */
class PaymentMethodController extends AbstractRestfulController
{
    const SUCCESS_DELETE = 'PaymentMethod successfully deleted';
    const NATURAL_PERSON_NAME = 'naturalPerson';
    const LEGAL_ENTITY_NAME = 'legalEntity';

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\Payment\PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var \Application\Service\CivilLawSubject\CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var \Application\Service\CivilLawSubject\NaturalPersonManager
     */
    private $naturalPersonManager;

    /**
     * @var \Application\Service\CivilLawSubject\LegalEntityManager
     */
    private $legalEntityManager;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var array
     */
    private $config;
    /**
     * @var BankManager
     */
    private $bankManager;

    /**
     * PaymentMethodController constructor.
     * @param EntityManager $entityManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param NaturalPersonManager $naturalPersonManager
     * @param LegalEntityManager $legalEntityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param RbacManager $rbacManager
     * @param BankManager $bankManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                PaymentMethodManager $paymentMethodManager,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                NaturalPersonManager $naturalPersonManager,
                                LegalEntityManager $legalEntityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                RbacManager $rbacManager,
                                BankManager $bankManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->civilLawSubjectManager = $civilLawSubjectManager;
        $this->naturalPersonManager = $naturalPersonManager;
        $this->legalEntityManager = $legalEntityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->rbacManager = $rbacManager;
        $this->bankManager = $bankManager;
        $this->config = $config;
    }

    /**
     * Show PaymentMethod collection (GET)
     *
     * @return \Zend\Http\Response|ViewModel
     *
     * Права на доступ: Owner или Operator
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);

        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Whether the currant user yas role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');
        // Check permissions // Important! Operator has specific interface of view
        if (!($profile === false && $isOperator) && !($profile === 'profile' && !$isOperator)) {

            return $this->message()->error(null, null, BaseMessageCodeInterface::ERROR_ACCESS_DENIED);
        }

        $paginationParams = $this->getPaginationParams($this->config);
        // Get paymentMethod collection
        $paymentMethods = $this->paymentMethodManager
            ->getPaymentMethodsWithRelations($user, $paginationParams, $isOperator);
        // Get available for user  payment method types
        #$paymentMethodTypes = $this->paymentMethodManager->getAvailablePaymentMethodTypes($user);

        $paginator = null;
        if (method_exists($paymentMethods , 'getPages')) {
            $paginator = $paymentMethods->getPages();
        }
        $paymentMethodsOutput = $this->paymentMethodManager->extractPaymentMethodCollection($paymentMethods);

        $data = [
            'paginator'         => $paginator,
            'paymentMethods'    => $paymentMethodsOutput,
            'is_operator'       => $isOperator,
        ];

        // Ajax -
        if ($isAjax){
            return $this->message()->success("PaymentMethod collection", $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate("application/payment-method/collection");

        return $view;
    }

    /**
     * Show PaymentMethod details (GET)
     *
     * @param mixed $id
     * @return \Zend\Http\Response|ViewModel
     *
     * Права на доступ: Owner и Operator
     * @throws \Exception
     */
    public function get($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        if (!isset($id) || (int) $id <= 0) {
            return $this->message()->error('bad data provided for update');
        }
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);

        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var PaymentMethod $paymentMethod */ // Can throw Exception
            $paymentMethod = $this->paymentMethodManager->getPaymentMethodWithRelations($id);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole($user, 'Operator');
        // Check permissions // Important! Only Owner or Operator can see NaturalPerson details
        if (!($profile === false && $isOperator) &&
            !($profile === 'profile' && $this->access('profile.own.edit', ['paymentMethod' => $paymentMethod]))) {

            return $this->message()->error(null, null, BaseMessageCodeInterface::ERROR_ACCESS_DENIED);
        }

        $paymentMethodOutput = $this->paymentMethodManager
            ->getPaymentMethodOutput($paymentMethod);

        $data = [
            'paymentMethod' => $paymentMethodOutput,
            'is_operator'   => $isOperator
        ];

        // Ajax -
        if ( $isAjax ){
            return $this->message()->success("PaymentMethod single", $data);
        }
        // Http -
        $view = new TwigViewModel($data );
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate("application/payment-method/single");

        return $view;
    }

    /**
     * Important! For Ajax only!
     * Update PaymentMethod. Всегда приходит POST
     *
     * @param mixed $id
     * @param mixed $data
     *
     * Права на доступ: только Owner
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws \RuntimeException
     */
    public function update($id, $data)
    {
        // Ajax only
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'This method not supported for Http'
            ];
        }
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);

        try {
            /** @var PaymentMethod $paymentMethod */
            $paymentMethod = $this->paymentMethodManager->getPaymentMethodWithRelations($id);
            if($paymentMethod === null) {

                throw new \Exception('bad data provided: payment method by id not found');
            }
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');
            // Check permissions // Important! Only Owner or Operator can see NaturalPerson details
            if (!($profile === false && $isOperator) &&
                !($profile === 'profile' && $this->access('profile.own.edit', ['paymentMethod' => $paymentMethod]))) {

                return $this->message()->error(null, null, BaseMessageCodeInterface::ERROR_ACCESS_DENIED);
            }

            /** @var PaymentMethodStrategy $paymentMethodStrategy */ // Can throw Exception
            $paymentMethodStrategy = new PaymentMethodStrategy($paymentMethod);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Create form
        $form = $paymentMethodStrategy->getForm();
        // Set data to form
        $data['civil_law_subject_id'] = $paymentMethod->getCivilLawSubject()->getId();
        $data['payment_method_type'] = $paymentMethod->getPaymentMethodType()->getName();
        $form->setData($data);

        // Validate form
        if ($form->isValid()) {
            $formData = $form->getData();
            // Set paymentMethod Id
            $formData['idPaymentMethod'] = $paymentMethod->getId();
            try {
                // Update PaymentMethod // Can throw Exception
                $result = $this->paymentMethodManager->saveFormData($formData);
                // Extract paymentMethod object for Ajax
                $result['paymentMethod'] = $this->paymentMethodManager
                    ->extractPaymentMethod($result['paymentMethod']);

                return $this->message()->success('PaymentMethod updated', $result);

            } catch (\Throwable $t) {

                return $this->message()->error($t->getMessage());
            }
        }
        // Form is not valid
        return $this->message()->invalidFormData($form->getMessages());
    }

    /**
     * Important! For Ajax only!
     * Create PaymentMethod. Всегда приходит POST
     *
     * @param mixed $data
     * @return array|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     *
     * Права на доступ: Verified
     */
    public function create($data)
    {
        // Ajax only
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'This method not supported for Http'
            ];
        }

        try {
            if (!isset($data['civil_law_subject_id'])) {
                throw new LogicException(null,
                    LogicException::CIVIL_LAW_SUBJECT_NOT_PROPER_PROVIDED);
            }
            /** @var CivilLawSubject $civilLawSubject */
            $civilLawSubject = $this->civilLawSubjectManager->getPattern((int) $data['civil_law_subject_id']);
            if($civilLawSubject === null) {
                throw new \Exception('bad data provided: civil law subject by id not found');
            }
            /** @var PaymentMethodType $paymentMethodType */
            $paymentMethodType = $this->paymentMethodManager
                ->getPaymentMethodTypeByName($data['payment_method_type']);

            if($paymentMethodType === null) {

                throw new \Exception('bad data provided: payment method type by name not found');
            }

            /** @var PaymentMethodStrategy $paymentMethodStrategy */ // Can throw Exception
            $paymentMethodStrategy = new PaymentMethodStrategy(null, $paymentMethodType);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Создаем форму нужного Payment метода через стратегию
        $form = $paymentMethodStrategy->getForm();
        // Заполняем данными
        $form->setData($data);
        // If PaymentMethod has valid data
        if ($form->isValid()) {
            try {
                $result = $this->paymentMethodManager->saveFormData($form->getData());

                // Extract paymentMethod object for Ajax
                $result['paymentMethod'] = $this->paymentMethodManager
                    ->extractPaymentMethod($result['paymentMethod']);

                return $this->message()->success('PaymentMethod created', $result);

            } catch (\Throwable $t) {

                return $this->message()->error($t->getMessage());
            }
        }
        // Form is not valid
        return $this->message()->invalidFormData($form->getMessages());
    }

    /**
     * Remove NaturalPerson
     *
     * @param mixed $id
     * @return TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     *
     * Права: Owner
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var PaymentMethod $paymentMethod */ // Can throw Exception
            $paymentMethod = $this->paymentMethodManager->getPaymentMethodById($id);

            if ($paymentMethod === null || !$paymentMethod->getIsPattern()) {

                throw new \Exception('bad data provided: payment method by id not found');
            }

            // Check permissions // Important! Only owner can delete his paymentMethod
            if (!$this->access('profile.own.edit', ['paymentMethod' => $paymentMethod])) {

                return $this->message()->error(null, null, BaseMessageCodeInterface::ERROR_ACCESS_DENIED);
            }

            // Deletion
            $this->paymentMethodManager->remove($paymentMethod);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Ajax success
        if ($isAjax) {

            return $this->message()->success(self::SUCCESS_DELETE);
        }

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $paymentMethod->getCivilLawSubject();

        if ($civilLawSubject->getNaturalPerson()) {
            $redirect_route_name = 'user-profile/natural-person-single';
            $id = $civilLawSubject->getNaturalPerson()->getId();
        } elseif($civilLawSubject->getSoleProprietor()) {
            $redirect_route_name = 'user-profile/sole-proprietor-single';
            $id = $civilLawSubject->getSoleProprietor()->getId();
        } else {
            $redirect_route_name = 'user-profile/legal-entity-single';
            $id = $civilLawSubject->getLegalEntity()->getId();
        }
        // Http success
        // Возвращает на страницу субъекта права, к которому добавляется метод
        return $this->redirect()->toRoute($redirect_route_name, ['id' => $id]);
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function editFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        try {
            if (!isset($formDataInit['paymentMethod']) || !$formDataInit['paymentMethod'] instanceof PaymentMethod) {

                throw new \Exception('bad data provided: payment method not found');
            }
            /** @var PaymentMethod $paymentMethod */
            $paymentMethod = $formDataInit['paymentMethod'];

            /** @var PaymentMethodStrategy $paymentMethodStrategy */ // Can throw Exception
            $paymentMethodStrategy = new PaymentMethodStrategy($paymentMethod);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        // Create form
        $form = $paymentMethodStrategy->getForm();

        // Set data to form
        $paymentMethodStrategy->fillForm();

        // Проверяем, является ли пост POST-запросом.
        $postData = $this->collectIncomingData()['postData'];

        if($postData) {
            //set data to form
            $form->setData($postData);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData['idPaymentMethod'] = $paymentMethod->getId();
                $resultSavedForm = $this->paymentMethodManager->saveFormData($formData);

                if ($resultSavedForm['paymentMethod']->getCivilLawSubject()->getNaturalPerson()) {
                    $redirect_route_name = 'user-profile/natural-person-single';
                    $id = $resultSavedForm['paymentMethod']->getCivilLawSubject()->getNaturalPerson()->getId();
                } elseif($resultSavedForm['paymentMethod']->getCivilLawSubject()->getSoleProprietor()) {
                    $redirect_route_name = 'user-profile/sole-proprietor-single';
                    $id = $resultSavedForm['paymentMethod']->getCivilLawSubject()->getSoleProprietor()->getId();
                } else {
                    $redirect_route_name = 'user-profile/legal-entity-single';
                    $id = $resultSavedForm['paymentMethod']->getCivilLawSubject()->getLegalEntity()->getId();
                }
                // Http success
                // Возвращает на страницу субъекта права, к которому добавляется метод
                return $this->redirect()->toRoute($redirect_route_name, ['id' => $id]);
            }
        }

        // Set data to form
        $data['civil_law_subject_id'] = $paymentMethod->getCivilLawSubject()->getId();
        $data['payment_method_type'] = $paymentMethod->getPaymentMethodType()->getName();
        $form->setData($data);

        $paymentMethodOutput = $this->paymentMethodManager
            ->getPaymentMethodOutput($paymentMethod);

        $view = new TwigViewModel([
            'paymentMethodForm' => $form,
            'paymentMethod'     => $paymentMethodOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/payment-method/edit-'.$paymentMethod
                ->getPaymentMethodType()->getName());

        return $view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function createFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }
        try {
            $back_url_params = [];
            // Get data for form
            $incoming_data = $this->processFormDataInit();
            // Data
            $data = $this->collectIncomingData();
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            //добавление платежного метода для субьекта права участвуюшего в сделке с последуюшим ридеректом
            if(array_key_exists('idDeal',$data)) {
                $civilLawSubject = $this->civilLawSubjectManager->getCivilLawSubjectByDealIdForCurrentUser($user, $data['idDeal']);
                if($civilLawSubject && (!$incoming_data['civilLawSubject'] || !$incoming_data['civilLawSubject'] instanceof CivilLawSubject)){
                    $incoming_data['civilLawSubject'] = $civilLawSubject;
                }

                $back_url_params = [
                    'route' => 'deal/edit',
                    'params' => [
                        'id' => $data['idDeal']
                    ]
                ];
            }
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        // If no incoming_data['civilLawSubject']
        if (!$incoming_data['civilLawSubject'] || !$incoming_data['civilLawSubject'] instanceof CivilLawSubject) {
            // Return CivilLawSubjectSelectForm
            return $this->createCivilLawSubjectSelectForm($incoming_data);
        }
        // If no incoming_data['paymentMethodType']
        if (!$incoming_data['paymentMethodType'] || !$incoming_data['paymentMethodType'] instanceof PaymentMethodType) {
            // Return PaymentMethodTypeSelectForm
            return $this->createPaymentMethodTypeSelectForm($incoming_data);
        }

        // Return PaymentMethodForm
        return $this->createPaymentMethodForm($incoming_data, $back_url_params);
    }

    /**
     * Remove PaymentMethod (GET)
     * Права на доступ: Owner
     *
     * @return TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws LogicException
     */
    public function deleteFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        try {
            if (null === $formDataInit['paymentMethod']) {

                throw new LogicException(null, PaymentMethodExceptionCodeInterface::PAYMENT_METHOD_NOT_DEFINED);
            }
            /** @var PaymentMethod $paymentMethod */
            $paymentMethod = $this->paymentMethodManager->get($formDataInit['paymentMethod']);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Check permissions // Important! Only Owner can edit the PaymentMethod
        if (!$this->access('profile.own.edit', ['paymentMethod' => $paymentMethod])) {

            return $this->redirect()->toRoute('not-authorized');
        }
        $paymentMethodOutput = $this->paymentMethodManager
            ->getPaymentMethodOutput($paymentMethod);

        $view = new TwigViewModel([
            'paymentMethod' => $paymentMethodOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return mixed
     */
    public function formInitAction()
    {
        // Закрываем экшен для обращений из Http
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'This method not supported for Http'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // Hydrate objects for Ajax
        #$hydrateData = $this->paymentMethodManager->extractSetOfObjects($formDataInit);
        $formDataInit['civilLawSubject'] = $this->paymentMethodManager->extract($formDataInit['civilLawSubject']);
        $formDataInit['paymentMethodType'] = $this->paymentMethodManager->extract($formDataInit['paymentMethodType']);
        $formDataInit['paymentMethod'] =
            $this->paymentMethodManager->extractPaymentMethod($formDataInit['paymentMethod']);
        $formDataInit['availableCivilLawSubjects'] =
            $this->paymentMethodManager->extractSetOfObjects($formDataInit['availableCivilLawSubjects']);
        $formDataInit['availablePaymentMethodTypes'] =
            $this->paymentMethodManager->extractSetOfObjects($formDataInit['availablePaymentMethodTypes']);

        $formDataInit['handbooks']['popularBanks'] = $this->bankManager->extractBankCollection($formDataInit['handbooks']['popularBanks'], true);

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * @param $incoming_data
     * @return ViewModel
     */
    private function createCivilLawSubjectSelectForm($incoming_data)
    {
        $payment_method_type = null;
        if (isset($this->collectIncomingData()['queryData']['payment_method_type'])) {
            $payment_method_type = $this->collectIncomingData()['queryData']['payment_method_type'];
        }

        // Create CivilLawSubjectSelectionForm
        $form = new CivilLawSubjectSelectionForm(
            $incoming_data['availableCivilLawSubjects'],
            $payment_method_type
        );

        $view = new TwigViewModel([
            'civilLawSubjectSelectForm' => $form,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/payment-method/select-civil-law-subject-form');

        return $view;
    }

    /**
     * @param $incoming_data
     * @return ViewModel
     */
    private function createPaymentMethodTypeSelectForm($incoming_data)
    {
        $civil_law_subject_id = null;
        if (isset($this->collectIncomingData()['queryData']['civil_law_subject_id'])) {
            $civil_law_subject_id = $this->collectIncomingData()['queryData']['civil_law_subject_id'];
        }
        // Create PaymentMethodTypeSelectionForm
        $form = new PaymentMethodTypeSelectionForm(
            $incoming_data['availablePaymentMethodTypes'],
            $civil_law_subject_id
        );

        $view = new TwigViewModel([
            'paymentMethodTypeSelectForm' => $form,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/payment-method/select-payment-method-type-form');

        return $view;
    }

    /**
     * @param $incoming_data
     * @param array $back_url_params
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    private function createPaymentMethodForm($incoming_data, $back_url_params = [])
    {
        try {
            /** @var PaymentMethodStrategy $paymentMethodStrategy */ // Can throw Exception
            $paymentMethodStrategy = new PaymentMethodStrategy(null, $incoming_data['paymentMethodType']);
            // Создаем форму нужного Payment метода через стратегию
            $form = $paymentMethodStrategy->getForm();

            // Set 'civil_law_subject_id' and 'payment_method_type' to Form
            $form->setData([
                'civil_law_subject_id' => $incoming_data['civilLawSubject']->getId(),
                'payment_method_type'  => $incoming_data['paymentMethodType']->getName(),
            ]);

            $postData = $this->collectIncomingData()['postData'];

            if ($postData) {

                $form->setData($postData);

                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['civil_law_subject_id'] = $incoming_data['civilLawSubject']->getId();
                    $formData['payment_method_type'] = $incoming_data['paymentMethodType']->getName();

                    // Save formData
                    $resultSavedForm = $this->paymentMethodManager->saveFormData($formData);

                    if ($resultSavedForm['paymentMethod']->getCivilLawSubject()->getNaturalPerson()) {
                        $redirect_route_name = 'user-profile/natural-person-single';
                        $id = $resultSavedForm['paymentMethod']->getCivilLawSubject()->getNaturalPerson()->getId();
                    } elseif($resultSavedForm['paymentMethod']->getCivilLawSubject()->getSoleProprietor()) {
                        $redirect_route_name = 'user-profile/sole-proprietor-single';
                        $id = $resultSavedForm['paymentMethod']->getCivilLawSubject()->getSoleProprietor()->getId();
                    } else {
                        $redirect_route_name = 'user-profile/legal-entity-single';
                        $id = $resultSavedForm['paymentMethod']->getCivilLawSubject()->getLegalEntity()->getId();
                    }

                    if (!empty($back_url_params)) {
                        return $this->redirect()->toRoute($back_url_params['route'], $back_url_params['params']);
                    }
                    // Возвращает на страницу субъекта права, к которому добавляется метод
                    return $this->redirect()->toRoute($redirect_route_name, ['id' => $id]);
                }
            }

        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $paymentMethodTypeOutput = $this->paymentMethodManager
            ->getPaymentMethodTypeOutput($incoming_data['paymentMethodType']);

        $view = new TwigViewModel([
            'paymentMethodForm' => $form,
            'paymentMethodType' => $paymentMethodTypeOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/payment-method/create');

        return $view;
    }

    /**
     * @return array
     */
    private function processFormDataInit()
    {
        // Получаем приходящие разными методами (Post, Get(Query), Route, Ajax) данные
        $incomingData = $this->collectIncomingData();

        //process data for create
        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $popularBanks = $this->bankManager->getPopularBanks();
        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $civilLawSubject = null;
        if (isset($incomingData['civil_law_subject_id'])) {
            /** @var CivilLawSubject $civilLawSubject */
            $civilLawSubject = $this->civilLawSubjectManager
                ->getCivilLawSubjectForUser($user, (int) $incomingData['civil_law_subject_id']);
            // Check if civilLawSubject belongs to current user
            if ($civilLawSubject && $civilLawSubject->getUser() !== $user) {
                $civilLawSubject = null;
            }
        }

        // Получаем collection $civilLawSubjects
        $availableCivilLawSubjects = $this->civilLawSubjectManager->getAvailableCivilLawSubjects($user);

        $paymentMethodType = null;
        if (isset($incomingData['payment_method_type'])) {
            /** @var PaymentMethodType $paymentMethod */
            $paymentMethodType = $this->paymentMethodManager
                ->getPaymentMethodTypeByName($incomingData['payment_method_type']);
        }
        // Получаем коллекцию доступных пользователю методов оплаты
        $availablePaymentMethodTypes = $this->paymentMethodManager->getAvailablePaymentMethodTypes($user);

        //process data for update or delete
        $paymentMethod = null;

        if (isset($incomingData['idPaymentMethod'])) {
            try {
                /** @var PaymentMethod $paymentMethod */
                $paymentMethod = $this->paymentMethodManager
                    ->getPaymentMethodWithRelations($incomingData['idPaymentMethod']);
                // Check if bankDetail belongs to current user
                if ($paymentMethod && $paymentMethod->getCivilLawSubject()->getUser() !== $user) {
                    $paymentMethod = null;
                }

            } catch (\Throwable $t) {

                return $this->message()->error($t->getMessage());
            }
        }

        return [
            // Create
            'civilLawSubject' => $civilLawSubject,
            'paymentMethodType' => $paymentMethodType,
            'availableCivilLawSubjects' => $availableCivilLawSubjects,
            'availablePaymentMethodTypes' => $availablePaymentMethodTypes,
            // Update
            'paymentMethod' => $paymentMethod,
            //handbooks
            'handbooks' => [
                'popularBanks' => $popularBanks
            ]
        ];
    }
}