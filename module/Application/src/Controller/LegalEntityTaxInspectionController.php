<?php

namespace Application\Controller;

use Core\Controller\AbstractRestfulController;
use Application\Entity\LegalEntity;
use Application\Entity\LegalEntityTaxInspection;
use Application\Form\LegalEntityTaxInspectionForm;
use Application\Service\CivilLawSubject\LegalEntityDocumentManager;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Application\Service\CivilLawSubject\LegalEntityTaxInspectionManager;
use Core\Exception\LogicException;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Doctrine\ORM\EntityManager;
use Zend\View\Model\ViewModel;
use Zend\Hydrator\ClassMethods;

/**
 * Class LegalEntityTaxInspectionController
 * @package Application\Controller
 */
class LegalEntityTaxInspectionController extends AbstractRestfulController
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * Auth Manager
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\CivilLawSubject\LegalEntityManager
     */
    private $legalEntityManager;

    /**
     * @var \Application\Service\CivilLawSubject\LegalEntityDocumentManager
     */
    private $legalEntityDocumentManager;

    /**
     * @var \Application\Service\CivilLawSubject\LegalEntityTaxInspectionManager
     */
    private $legalEntityTaxInspectionManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * @var array
     */
    private $config;

    /**
     * LegalEntityTaxInspectionController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param LegalEntityManager $legalEntityManager
     * @param LegalEntityDocumentManager $legalEntityDocumentManager
     * @param LegalEntityTaxInspectionManager $legalEntityTaxInspectionManager
     * @param RbacManager $rbacManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                LegalEntityManager $legalEntityManager,
                                LegalEntityDocumentManager $legalEntityDocumentManager,
                                LegalEntityTaxInspectionManager $legalEntityTaxInspectionManager,
                                RbacManager $rbacManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->legalEntityManager = $legalEntityManager;
        $this->legalEntityDocumentManager = $legalEntityDocumentManager;
        $this->legalEntityTaxInspectionManager = $legalEntityTaxInspectionManager;
        $this->rbacManager = $rbacManager;
        $this->config = $config;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function getList()
    {
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // Whether the currant user yas role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            // Check permissions // Important! Operator has specific interface of view
            if (!($profile === false && $isOperator) && !($profile == 'profile' && !$isOperator)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $paginationParams = $this->getPaginationParams($this->config);
            $taxInspections = $this->legalEntityTaxInspectionManager->getCollectionTaxInspection($user, $paginationParams, $isOperator);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginator = null;
        if (method_exists($taxInspections , 'getPages')) {
            $paginator = $taxInspections->getPages();
        }
        // Get Output
        $taxInspectionsOutput = $this->legalEntityTaxInspectionManager
            ->extractTaxInspectionsCollection($taxInspections);

        $data = [
            'paginator'   => $paginator,
            'taxInspections' => $taxInspectionsOutput,
            'is_operator' => $isOperator,
        ];
        // Ajax -
        if ($isAjax) {
            return $this->message()->success("collection tax inspection", $data);
        }
        // Http -
        $view =  new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity-tax-inspection/collection');

        return $view;
    }

    /**
     * @param mixed $id
     * @return \Zend\Http\Response|ViewModel
     */
    public function get($id)
    {
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        if (!isset($id) || (int) $id <= 0) {
            return $this->message()->error('bad data provided');
        }
        try {
            $taxInspection = $this->legalEntityTaxInspectionManager->getTaxInspectionById($id);
            /** @var LegalEntity $legalEntity */
            $legalEntity = $taxInspection->getLegalEntityDocument()->getLegalEntity();
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            // Check permissions // Important! Only Owner or Operator can see details
            if (!($profile === false && $isOperator) &&
                !($profile == 'profile' && $this->access('profile.own.edit', ['civilLawSubject' => $legalEntity->getCivilLawSubject()]))) {
                return $this->redirect()->toRoute('not-authorized');
            }

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
        // Extracted bankDetail
        $taxInspectionOutput = $this->legalEntityTaxInspectionManager
            ->getTaxInspectionsOutput($taxInspection);

        $data = [
            'taxInspection' => $taxInspectionOutput,
            'is_operator' => $isOperator
        ];

        //Ajax success
        if ( $isAjax ){
            return $this->message()->success("tax inspection single", $data);
        }
        //Http success
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity-tax-inspection/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|TwigViewModel|mixed|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $form = new LegalEntityTaxInspectionForm('document');

        try {
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $resultSavedForm = $this->saveFormData($formData);

                //Ajax success
                return $this->message()->success('create tax inspection', $resultSavedForm);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        try {
            if (!isset($formDataInit['taxInspection']) || !$formDataInit['taxInspection'] instanceof LegalEntityTaxInspection) {

                throw new LogicException('bad data provided: not found tax inspection');
            }

            $form = new LegalEntityTaxInspectionForm('document');

            //remove check to file required
            $fileFilter = $form->getInputFilter()->get('file');
            $fileFilter->setRequired(false);
            //set data to form
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData['idTaxInspection'] = $id;
                $resultSavedForm = $this->saveFormData($formData);
                //Ajax success
                return $this->message()->success('update tax inspection', $resultSavedForm);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @return \Zend\Http\Response
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $taxInspection = $this->legalEntityTaxInspectionManager->getTaxInspectionById($id);
            /** @var LegalEntity $legalEntity */
            $legalEntity = $taxInspection->getLegalEntityDocument()->getLegalEntity();
            $document = $taxInspection->getLegalEntityDocument();

            // Check permissions
            if (!$this->access('profile.own.edit', ['civilLawSubject' => $legalEntity->getCivilLawSubject()])) {
                return $this->redirect()->toRoute('not-authorized');
            }

            // Deleting
            $this->legalEntityDocumentManager->remove($document);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
        //Ajax success
        if ($isAjax) {
            return $this->message()->success('Документ удален');
        }
        //Http success
        // @TODO Куда возвращать?
        return $this->redirect()->toRoute(
            'user-profile/legal-entity-single',
            ['id' => $legalEntity->getId()]
        );
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function createFormAction()
    {
        $formDataInit = $this->processFormDataInit();

        $form = new LegalEntityTaxInspectionForm('document');

        try {
            if ( !isset($formDataInit['legalEntity']) || !$formDataInit['legalEntity'] instanceof LegalEntity){
                throw new \Exception('bad data provided: not found legal entity');
            }
            $legalEntity = $formDataInit['legalEntity'];
            $form->get('idLegalEntity')->setValue($legalEntity->getId());

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $filesData = $this->collectIncomingData()['filesData'];
                if (\count($filesData) && $filesData['file']['error'] !== 4) {   //4 - Файл не был загружен
                    $postData = array_merge_recursive(
                        $postData,
                        $filesData
                    );
                }
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idLegalEntity'] = $legalEntity->getId();
                    $resultSavedForm = $this->saveFormData($formData);

                    //http success
                    return $this->redirect()->toRoute(
                        'user-profile/legal-entity-single',
                        ['id' => $resultSavedForm['legalEntity']->getId()]
                    );
                }
            }
        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
        $form->prepare();
        $view = new TwigViewModel([
            'taxInspectionForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity-tax-inspection/create-form');

        return $view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function editFormAction()
    {
        $formDataInit = $this->processFormDataInit();

        $form = new LegalEntityTaxInspectionForm('document');

        try {
            if ( !isset($formDataInit['taxInspection']) || !$formDataInit['taxInspection'] instanceof LegalEntityTaxInspection){
                throw new \Exception('bad data provided: not found tax inspection');
            }
            $taxInspection = $formDataInit['taxInspection'];
            // Extracted bankDetail
            $taxInspectionOutput = $this->legalEntityTaxInspectionManager
                ->getTaxInspectionsOutput($formDataInit['taxInspection']);

            $form->setData([
                'serial_number' => $taxInspection->getSerialNumber(),
                'date_issued' => $taxInspection->getDateIssued()->format('d.m.Y'),
                'issue_organisation' => $taxInspection->getIssueOrganisation(),
                'issue_registrar' => $taxInspection->getIssueRegistrar(),
                'date_registered' => $taxInspection->getDateRegistered()->format('d.m.Y'),
            ]);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $filesData = $this->collectIncomingData()['filesData'];
                if (count($filesData) && $filesData['file']['error'] !== 4) {   //4 - Файл не был загружен
                    $postData = array_merge_recursive(
                        $postData,
                        $filesData
                    );
                }
                //remove check to file required
                $fileFilter = $form->getInputFilter()->get('file');
                $fileFilter->setRequired(false);
                //set data to form
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idTaxInspection'] = $taxInspection->getId();
                    $resultSavedForm = $this->saveFormData($formData);

                    //http success
                    return $this->redirect()->toRoute(
                        'user-profile/legal-entity-single',
                        ['id' => $resultSavedForm['legalEntity']->getId()]
                    );
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
        $form->prepare();
        $view = new TwigViewModel([
            'taxInspection' => $taxInspectionOutput,
            'taxInspectionForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity-tax-inspection/edit-form');

        return $view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function deleteFormAction()
    {
        $formDataInit = $this->processFormDataInit();

        try {
            if ( !isset($formDataInit['taxInspection']) || !$formDataInit['taxInspection'] instanceof LegalEntityTaxInspection){
                throw new \Exception('bad data provided: not found tax inspection');
            }
            // Extracted bankDetail
            $taxInspectionOutput = $this->legalEntityTaxInspectionManager
                ->getTaxInspectionsOutput($formDataInit['taxInspection']);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        $view = new TwigViewModel([
            'taxInspection' => $taxInspectionOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity-tax-inspection/delete-form');

        return $view;
    }

    /**
     * @return array
     */
    public function formInitAction()
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array
     */
    private function processFormDataInit()
    {
        //step 1
        $incomingData = $this->collectIncomingData();

        //check
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            //process data for create
            $legalEntity = null;
            if ( isset($incomingData['idLegalEntity']) ){
                $legalEntity = $this->legalEntityManager->getBasedOnPattern((int) $incomingData['idLegalEntity']);
            }
            $allowedLegalEntities = $this->legalEntityManager->getAllowedLegalEntityByUser($user);

            //process data for update or delete
            $taxInspection = null;
            if ( isset($incomingData['idTaxInspection']) ){
                $taxInspection = $this->legalEntityTaxInspectionManager->getTaxInspectionById((int) $incomingData['idTaxInspection']);
            }
            $allowedTaxInspections = $this->legalEntityTaxInspectionManager->getAllowedTaxInspectionByUser($user);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        //step 2
        return [
            //create
            'legalEntity' => $legalEntity,
            'allowedLegalEntities' => $allowedLegalEntities,
            //edit
            'taxInspection' => $taxInspection,
            'allowedTaxInspections' => $allowedTaxInspections,
        ];
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    private function saveFormData($data)
    {
        // Get current user // Can throw Exception
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

        if ( isset($data['idTaxInspection']) ) {
            //update
            if ( (int) $data['idTaxInspection'] <= 0 ) {
                throw new \Exception('bad data provided: not found tax inspection id');
            }

            $taxInspection = $this->legalEntityTaxInspectionManager->getTaxInspectionById((int) $data['idTaxInspection']);

            if( $taxInspection === null) {
                throw new \Exception('bad data provided: not found tax inspection by id');
            }

            // Check if taxInspection belongs to current user
            if ($taxInspection->getLegalEntityDocument()->getLegalEntity()->getCivilLawSubject()->getUser() !== $user) {
                throw new \Exception('taxInspection not belongs to current user');
            }

            $document = $this->legalEntityDocumentManager
                ->update($taxInspection->getLegalEntityDocument(), $data);

            $result = [
                'taxInspection' => $document->getLegalEntityTaxInspection(),
                'legalEntity' => $document->getLegalEntity()
            ];
        } else {
            //create
            if ( !isset($data['idLegalEntity']) || (int) $data['idLegalEntity'] <= 0){
                throw new \Exception('bad data provided: not found legal entity id');
            }

            $legalEntity = $this->legalEntityManager->getBasedOnPattern((int) $data['idLegalEntity']);

            if( $legalEntity === null) {
                throw new \Exception('bad data provided: not found legal entity by id');
            }

            // Check if legalEntity belongs to current user
            if ($legalEntity->getCivilLawSubject()->getUser() !== $user) {
                throw new \Exception('legalEntity not belongs to current user');
            }

            $document = $this->legalEntityDocumentManager
                ->create($legalEntity, LegalEntityTaxInspection::class, $data);
            $taxInspection = $document->getLegalEntityTaxInspection();

            $result = [
                'taxInspection' => $taxInspection,
                'legalEntity' => $legalEntity
            ];
        }

        return $result;
    }
}