<?php

namespace Application\Controller;

use Application\Entity\Deal;
use Application\Form\PayVariantForm;
use Application\Service\Deal\DealManager;
use Core\Controller\AbstractRestfulController;
use Application\Entity\User;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class PayController
 * @package Application\Controller
 */
class PayController extends AbstractRestfulController
{
    const BANK_TRANSFER = 'bank_transfer';
    const ACQUIRING_MANDARIN = 'acquiring_mandarin';

    const PAY_VARIANTS = [
        self::BANK_TRANSFER => 'Банковский перевод',
        self::ACQUIRING_MANDARIN => 'Эквайринг Мандарин',
    ];

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var array
     */
    private $config;

    /**
     * PayController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param RbacManager $rbacManager
     * @param DealManager $dealManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                RbacManager $rbacManager,
                                DealManager $dealManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->rbacManager = $rbacManager;
        $this->dealManager = $dealManager;
        $this->config = $config;
    }

//    public function getList()
//    {
//    }

//    public function get($id)
//    {
//    }

//    public function update($id, $data)
//    {
//    }

//    public function create($data)
//    {
//    }

//    public function delete($id)
//    {
//    }

//    public function editFormAction()
//    {
//    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function optionsFormAction()
    {
//        // only Http
//        if ($this->getRequest()->isXmlHttpRequest()) {
//
//            return $this->message()->error('Method not supported');
//        }

        $incoming_data = $this->processFormDataInit();

        /** @var Deal|null $deal */
        $deal = $incoming_data['deal'];

        if (null === $deal) {

            return $this->message()->error('No deal for pay specified');
        }

        /** @var User $user */
        $user = $incoming_data['user'];

        if (null === $user) {

            return $this->message()->error('No user specified');
        }

        if (null === $deal || !DealManager::isUserCustomerOfDeal($user, $deal)) {

            return $this->message()->error('User can not make payments for this deal');
        }

        $payVariantForm = new PayVariantForm($deal->getId());

        try {
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $payVariantForm->setData($postData);
                // Validate form
                if ($payVariantForm->isValid()) {

                    $selected = $payVariantForm->get('pay_variant')->getValue();

                    switch ($selected) {

                        case self::BANK_TRANSFER:
                            // Показываем PDF invoice
                            return $this->redirect()->toRoute('deal-invoice-single', ['id' => $deal->getId()]);

                        case self::ACQUIRING_MANDARIN:
                            // перенаправляем на форму оплаты по карте
                            return $this->redirect()->toRoute('deal-pay-mandarin', ['idDeal' => $deal->getId()]);
                    }
                }
                // Form not Valid
                return $this->message()->error('Invalid provided');
            }

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        return $this->message()->error('No data provided');
    }

//    public function deleteFormAction()
//    {
//    }

    /**
     * @return array
     */
    public function formInitAction()
    {
        // only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        if (!is_array($formDataInit)) {

            return $this->message()->error('formInit failed');
        }

        $formDataInit['user'] = $this->userManager->extractUser($formDataInit['user']);
        $formDataInit['deal'] = $formDataInit['deal'] ? $this->dealManager->getDealOutputForSingle($formDataInit['deal']) : null;

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     *
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        try {
            $deal = null;

            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            if (isset($incomingData['idDeal'])){
                /** @var $deal $deal */ // Can throw Exception
                $deal = $this->dealManager->getDealById((int) $incomingData['idDeal']);
                // Check if $deal belongs to current user
                if ($deal && !$this->dealManager->isUserPartOfDial($deal, $user)
                    && !$this->rbacManager->hasRole(null, 'Operator')) {

                    $deal = null;
                }
            }

        } catch (\Throwable $t){

            $user = null;
            #return $this->message()->error($t->getMessage());
        }

        return [
            'user' => $user,
            'deal' => $deal,
        ];
    }
}