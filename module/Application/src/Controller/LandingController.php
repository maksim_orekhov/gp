<?php
namespace Application\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Exception\BaseExceptionCodeInterface;
use Core\Service\TwigViewModel;
use Zend\Form\Element;

class LandingController extends AbstractRestfulController
{
    use MessageJsonModelTrait;

    public function landingAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        return $view;
    }

    public function landingFeedbackAction()
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'message' => BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE
            ];
        }

        try {
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $message = implode('<br>', $postData);

                $this->message()->sendSpecialMail($message, null, 'Landing feedback');

                return $this->JsonSuccessModel('send feedback success');
            }
        }catch (\Throwable $t){

            return $this->JsonErrorModel($t->getMessage());
        }

        return $this->JsonErrorModel('send feedback fail');
    }

    /**
     * @return array|\Zend\View\Model\JsonModel
     */
    public function integrationRequestSendingAction()
    {
        $this->message()->setJsonStrategy(true);

        try {
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            $queryData = $this->collectIncomingData()['queryData'];
            $data = array_merge_recursive($postData ?? [], $queryData ?? []);

            $csrf = new Element\Csrf('csrf');
            $validator = $csrf->getCsrfValidator();
            if ( !$validator->isValid($data['csrf'] ?? '')) {

                return $this->message()->invalidFormData($validator->getMessages());
            }
            if (array_key_exists('csrf', $data)) {

                unset($data['csrf']);
            }
            $message = [];
            if (!empty($data)) {
                $message = implode('<br>', $data);
            }
            if (!empty($message)) {
                $this->message()->sendSpecialMail($message, null, 'Integration request sending');

                return $this->message()->success('Integration request sending success');
            }

            return $this->message()->error('Sending data is empty');
        }
        catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
    }

    /////////////////////////////////////////////
    ///////////// landing pages /////////////////
    /////////////////////////////////////////////

    public function policyAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function connectionContractAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function termsOfUseAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function howItWorksAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function forWhomAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function whatCanBuyAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function disputeResolutionAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function disputeConsiderationRulesAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function contactsAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function forCustomersAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function forContractorsAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function forBusinessAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function eCommerceAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function forPartnersAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    public function informationAction()
    {
        $view = new TwigViewModel();
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }
}
