<?php

namespace Application\Controller;

use Application\Service\CivilLawSubject\CivilLawSubjectPolitics;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Controller\AbstractRestfulController;
use Application\Entity\NaturalPerson;
use Application\Form\NaturalPersonForm;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use Core\Exception\LogicException;
use Core\Service\TwigViewModel;
use Zend\View\Model\ViewModel;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Entity\User;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Doctrine\ORM\EntityManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class NaturalPersonController
 * @package Application\Controller
 */
class NaturalPersonController extends AbstractRestfulController
{
    const SUCCESS_DELETE = 'NaturalPerson successfully deleted';

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var NaturalPersonManager
     */
    private $naturalPersonManager;

    /**
     * @var NaturalPersonDocumentManager
     */
    private $naturalPersonDocumentManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var array
     */
    private $config;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * @var CivilLawSubjectPolitics
     */
    private $civilLawSubjectPolitics;

    /**
     * NaturalPersonController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param NaturalPersonManager $naturalPersonManager
     * @param NaturalPersonDocumentManager $naturalPersonDocumentManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param RbacManager $rbacManager
     * @param NdsTypeManager $ndsTypeManager
     * @param CivilLawSubjectPolitics $civilLawSubjectPolitics
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                NaturalPersonManager $naturalPersonManager,
                                NaturalPersonDocumentManager $naturalPersonDocumentManager,
                                PaymentMethodManager $paymentMethodManager,
                                RbacManager $rbacManager,
                                NdsTypeManager $ndsTypeManager,
                                CivilLawSubjectPolitics $civilLawSubjectPolitics,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->civilLawSubjectManager = $civilLawSubjectManager;
        $this->naturalPersonManager = $naturalPersonManager;
        $this->naturalPersonDocumentManager = $naturalPersonDocumentManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->rbacManager = $rbacManager;
        $this->ndsTypeManager = $ndsTypeManager;
        $this->civilLawSubjectPolitics = $civilLawSubjectPolitics;
        $this->config = $config;
    }

    /**
     * Show NaturalPerson collection (GET)
     *
     * @return \Zend\Http\Response|ViewModel
     *
     * Права на доступ: Owner и Operator.
     * @throws \Exception
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);

        try {
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            // Check permissions // Important! Operator has specific interface of view
            if (!($profile === false && $isOperator) && !($profile === 'profile' && !$isOperator)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->selectUserByLogin($this->baseAuthManager->getIdentity());
            // Get naturalPerson collection // Can throw Exception
            $paginationParams = $this->getPaginationParams($this->config);
            /** @var NaturalPerson $naturalPersons */
            $naturalPersons = $this->naturalPersonManager
                ->getCollectionNaturalPerson($user, $paginationParams, $isOperator);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginator = null;
        if (method_exists($naturalPersons , 'getPages')) {
            $paginator = $naturalPersons->getPages();
        }
        // Get Output
        $naturalPersonsOutput = $this->naturalPersonManager
            ->extractNaturalPersonCollection($naturalPersons);

        $data = [
            'paginator'         => $paginator,
            'naturalPersons'    => $naturalPersonsOutput,
            'is_operator'       => $isOperator,
        ];

        // Ajax -
        if ($isAjax) {
            return $this->message()->success('NaturalPerson collection', $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person/collection');

        return $view;
    }

    /**
     * Show NaturalPerson details (GET)
     *
     * @param mixed $id
     * @return \Zend\Http\Response|ViewModel
     *
     * Права на доступ: Owner и Operator
     * @throws \Exception
     */
    public function get($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);

        try {
            /** @var NaturalPerson $naturalPerson */ // Can throw Exception
            $naturalPerson = $this->naturalPersonManager->getNaturalPersonById($id);
            $documents = $this->naturalPersonDocumentManager->getCollectionDocumentByNaturalPerson($naturalPerson,[]);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');
        // Check permissions // Important! Only Owner or Operator can see NaturalPerson details
        if (!($profile === false && $isOperator) &&
            !($profile === 'profile' && $this->access('profile.own.edit', ['civilLawSubjectChild' => $naturalPerson]))) {

            return $this->redirect()->toRoute('not-authorized');
        }
        // Extracted naturalPerson
        $naturalPersonOutput = $this->naturalPersonManager
            ->getNaturalPersonOutput($naturalPerson);
        // Extracted documents natural person
        $documentsOutput = $this->naturalPersonDocumentManager
            ->extractNaturalPersonDocumentCollection($documents);

        $paymentMethods = $naturalPerson->getCivilLawSubject()->getPaymentMethods();
        $paymentMethodsOutput = $this->paymentMethodManager->extractPaymentMethodCollection($paymentMethods);

        $data = [
            'naturalPerson'  => $naturalPersonOutput,
            'is_operator'    => $isOperator,
            'paymentMethods' => $paymentMethodsOutput,
            'documents'      => $documentsOutput,
            'civil_law_subject_id' => $naturalPersonOutput['civil_law_subject_id'],
        ];

        // Ajax -
        if ($isAjax) {
            return $this->message()->success('NaturalPerson single', $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $form = new NaturalPersonForm();

        try {
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();

                /** @var User $user */ // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
                // Whether the currant user has role Operator
                if ($this->rbacManager->hasRole($user, 'Operator')) {

                    return $this->redirect()->toRoute('not-authorized');
                }

                $formData['user'] = $user;
                // Save Form Data
                $resultSavedForm = $this->civilLawSubjectManager->saveFormData($formData, CivilLawSubjectManager::NATURAL_PERSON_NAME);
                // Hydrate $resultSavedForm for Ajax
                $hydratedData = $this->naturalPersonManager->extractSetOfObjects($resultSavedForm);
                /** @var NaturalPerson $naturalPerson */
                $naturalPerson = $resultSavedForm['naturalPerson'];

                $hydratedData['civil_law_subject_id'] = $naturalPerson->getCivilLawSubject()->getId();

                return $this->message()->success('create natural person', $hydratedData);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // Если не найден редактируемый объект, значит, его не в базе
        // или у пользователя нет прав его редактировать
        if (null === $formDataInit['naturalPerson']) {

            return $this->redirect()->toRoute('not-authorized');
        }

        $form = new NaturalPersonForm();

        try {
            //set data to form
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData['idNaturalPerson'] = $id;
                // Save Form Data
                $this->civilLawSubjectManager->saveFormData($formData, CivilLawSubjectManager::NATURAL_PERSON_NAME);

                return $this->message()->success('update natural person');
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @return bool|TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var NaturalPerson $naturalPerson */ // Can throw Exception
            $naturalPerson = $this->naturalPersonManager->getNaturalPersonById($id);

            // Check permissions // Important! Only owner can delete his naturalPerson
            if (!$this->access('profile.own.edit', ['civilLawSubjectChild' => $naturalPerson])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            // Deletion
            $this->civilLawSubjectManager->remove($naturalPerson->getCivilLawSubject());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Ajax success
        if ($isAjax) {

            return $this->message()->success(self::SUCCESS_DELETE);
        }
        // Http success
        return $this->redirect()->toRoute('user-profile/natural-person');
    }

    /**
     * Form render
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function createFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();
            // Create form
            $form = new NaturalPersonForm();

            if ( !isset($formDataInit['user']) || !$formDataInit['user'] instanceof User){

                throw new \Exception('bad data provided: not found user');
            }

            /** @var User $user */ // Can throw Exception
            $user = $formDataInit['user'];

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['user'] = $user;
                    $this->civilLawSubjectManager->saveFormData($formData, CivilLawSubjectManager::NATURAL_PERSON_NAME);

                    //http success
                    return $this->redirect()->toRoute('profile');
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $view = new TwigViewModel([
            'naturalPersonForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person/create-form');

        return $view;
    }

    /**
     * Form render (GET)
     * @return array|\Zend\Http\Response|ViewModel
     *
     * Important! Only for Http
     * @throws \Exception
     */
    public function editFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();

            $form = new NaturalPersonForm();

            if (!isset($formDataInit['naturalPerson']) || !$formDataInit['naturalPerson'] instanceof NaturalPerson){

                throw new LogicException(null, LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_NATURAL_PERSON);
            }
            /** @var NaturalPerson $naturalPerson */
            $naturalPerson = $formDataInit['naturalPerson'];
            // Extracted naturalPerson
            $naturalPersonOutput = $this->naturalPersonManager
                ->getNaturalPersonOutput($formDataInit['naturalPerson']);

            // Set data to form
            $form = $this->naturalPersonManager->setFormData($form, $naturalPerson);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                //set data to form
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idNaturalPerson'] = $naturalPerson->getId();
                    $this->civilLawSubjectManager->saveFormData($formData, CivilLawSubjectManager::NATURAL_PERSON_NAME);

                    //http success
                    return $this->redirect()->toRoute('user-profile/natural-person');
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');

        $view = new TwigViewModel([
            'naturalPerson'     => $naturalPersonOutput,
            'naturalPersonForm' => $form,
            'is_operator' => $isOperator,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/natural-person/edit-form');

        return $view;
    }

    /**
     * Remove NaturalPerson (GET)
     * @return ViewModel
     *
     * Important! Only for Http
     * @throws \Exception
     */
    public function deleteFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();

            if ( !isset($formDataInit['naturalPerson']) || !$formDataInit['naturalPerson'] instanceof NaturalPerson) {

                throw new LogicException(null,
                    LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_NATURAL_PERSON);
            }
            // Extracted naturalPerson
            $naturalPersonOutput = $this->naturalPersonManager->getNaturalPersonOutput($formDataInit['naturalPerson']);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Http -
        $view = new TwigViewModel([
            'naturalPerson' => $naturalPersonOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // Hydrate collection and objects for Ajax
        // @TODO Переделать на использование новых экстракторов и отказаться от гидраторов
        $formDataInit['user'] = $this->userManager->extractUser($formDataInit['user']);
        $formDataInit['allowedUser'] = $this->userManager->extractUser($formDataInit['allowedUser']);
        $formDataInit['naturalPerson'] =
            $this->naturalPersonManager->extractNaturalPerson($formDataInit['naturalPerson']);
        $formDataInit['allowedNaturalPersons'] = $formDataInit['allowedNaturalPersons']
            ? $this->naturalPersonManager->extractNaturalPersonCollection($formDataInit['allowedNaturalPersons'])
            : null;

        $formDataInit['handbooks']['ndsTypes'] = $this->ndsTypeManager->extractNdsTypeCollection($formDataInit['handbooks']['ndsTypes']);

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     *
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        // Get current user // Can throw Exception
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        $allowedUser = null;

        //process data for update or delete
        $naturalPerson = null;
        if (isset($incomingData['idNaturalPerson']) || isset($incomingData['id'])) {

            $id = $incomingData['id'] ?? $incomingData['idNaturalPerson'];

            /** @var NaturalPerson $naturalPerson */
            $naturalPerson = $this->naturalPersonManager->getNaturalPersonById((int) $id);

            // Check if naturalPerson belongs to current user
            if ($naturalPerson && $naturalPerson->getCivilLawSubject()->getUser() !== $user) {

                $naturalPerson = null;
            }
        }

        $allowedNaturalPersons = $this->naturalPersonManager->getAllowedNaturalPersonByUser($user);
        $ndsTypes = $this->ndsTypeManager->getNdsTypes();

        return [
            //create
            'user' => $user,
            'allowedUser' => $allowedUser,
            //edit
            'naturalPerson' => $naturalPerson,
            'allowedNaturalPersons' => $allowedNaturalPersons,
            //handbooks
            'handbooks' => [
                'ndsTypes' => $ndsTypes
            ]
        ];
    }
}