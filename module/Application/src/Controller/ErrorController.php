<?php

namespace Application\Controller;

use Application\Form\ErrorReportForm;
use Application\Service\ErrorManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;


/**
 * Class ErrorController
 * @package Application\Controller
 */
class ErrorController extends AbstractRestfulController
{

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var ErrorManager
     */
    private $errorManager;
    /**
     * @var RbacManager
     */
    private $rbacManager;
    /**
     * @var array
     */
    private $config;


    /**
     * ErrorController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param ErrorManager $errorManager
     * @param RbacManager $rbacManager
     * @param array $config
     */
    public function __construct(BaseAuthManager $baseAuthManager,
                                ErrorManager $errorManager,
                                RbacManager $rbacManager,
                                array $config)
    {
        $this->baseAuthManager = $baseAuthManager;
        $this->errorManager = $errorManager;
        $this->rbacManager = $rbacManager;
        $this->config = $config;
    }

    public function sendAction()
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {

            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            $incomingData = $this->collectIncomingData();
            $data = $this->errorManager->processingAjaxData($incomingData['ajaxData']);
            $this->errorManager->sendAnyErrorMessage($data);
            return $this->message()->success('Send completed');
        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }
        return true;
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function errorReportMessageAction()
    {
        $postData = $this->collectIncomingData()['postData'];
        if (! $postData) {
            if ($this->baseAuthManager->getIdentity()) {

                return $this->redirect()->toRoute('profile');
            }
            return $this->redirect()->toRoute('login');
        }

        try {
            $errorReportForm = new ErrorReportForm();
            $errorReportForm->setData($postData);
            if ($errorReportForm->isValid()) {
                $formData = $errorReportForm->getData();

                $user_email = $formData['email'];
                $error_report = $formData['error_report'];
                $error_message = $formData['error_message'];
                $error_tracing = $formData['error_tracing'];

                $this->errorManager->sendErrorReport($user_email, $error_message, $error_report, $error_tracing);

                if ($this->baseAuthManager->getIdentity()) {

                    return $this->message()->success('Отчет успешно отправлен');
                }

                return $this->redirect()->toRoute('login');
            }
            $errorReportForm->prepare();
            $view = new TwigViewModel([
                'errorReportForm' => $errorReportForm
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
            return $view;
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }
    }
}