<?php

namespace Application\Controller;

use Application\Form\SystemPaymentDetailForm;
use Core\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;
use Application\Form\GlobalSettingForm;
use Zend\Json\Json;
use Application\Service\SettingManager;

/**
 * Class SettingController
 * @package Application\Controller
 */
class SettingController extends AbstractRestfulController
{
    /**
     * @var \Application\Service\SettingManager
     */
    private $settingManager;

    /**
     * GlobalSettingController constructor.
     * @param SettingManager $settingManager
     */
    public function __construct(SettingManager $settingManager)
    {
        $this->settingManager = $settingManager;
    }

    public function indexAction()
    {
        return new ViewModel([]);
    }

    /**
     * @return \Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws \RuntimeException
     */
    public function globalSettingAction()
    {
        $isPost = $this->getRequest()->isPost();
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $globalSettings = $this->settingManager->getGlobalSettingsToArray();
        if ( empty($globalSettings) ){

            return $this->message()->error('global settings object is empty');
        }

        $form = new GlobalSettingForm($globalSettings);
        $data = [
            'fee_percentage' => $globalSettings['fee_percentage'],
            'min_delivery_period' => $globalSettings['min_delivery_period'],
        ];
        $form->setData($data);

        // Проверяем, является ли пост POST-запросом.
        if ($isPost) {
            //запрос от ajax формы или нет?
            if ($isAjax) {
                $content = $this->getRequest()->getContent();
                $data = Json::decode($content, Json::TYPE_ARRAY);
            } else {
                $data = $this->params()->fromPost();
            }
            // Заполняем форму данными.
            $form->setData($data);
            if ($form->isValid()) {
                // Get validated form data
                $data = $form->getData();

                try {
                    // update global setting
                    $this->settingManager->updateGlobalSettings($data);
                    // SUCCESS Response
                    if ($isAjax) {
                        // Return Json with SUCCESS status
                        return $this->message()->success('General setting successfully edited');
                    }
                    // HTTP
                    return $this->message()->success('General setting successfully edited');
                } catch (\Throwable $t) {

                    return $this->message()->error($t->getMessage());
                }
            }
            // If $form->isValid() fails
            if($isAjax) {
                // Return Json with ERROR status
                return $this->message()->invalidFormData($form->getMessages());
            }
            // В Http вернется форма с указанием ошибок валидации
        }
        if($isAjax){
            // Return Json with SUCCESS status (in this case also with $data array, which contains csrf token)
            $data = ['csrf' => $form->get('csrf')->getValue()];
            return $this->message()->success('CSRF token return', $data);
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * @return \Core\Service\TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws \RuntimeException
     */
    public function paymentDetailAction()
    {
        $isPost = $this->getRequest()->isPost();
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $systemPaymentDetail = $this->settingManager->getSystemPaymentDetail();
        } catch (\Throwable $t){
            return $this->message()->error($t->getMessage());
        }

        $form = new SystemPaymentDetailForm();
        $data = [
            'name' => $systemPaymentDetail->getName(),
            'bik' => $systemPaymentDetail->getBik(),
            'account_number' => $systemPaymentDetail->getAccountNumber(),
            'payment_recipient_name' => $systemPaymentDetail->getPaymentRecipientName(),
            'inn' => $systemPaymentDetail->getInn(),
            'kpp' => $systemPaymentDetail->getKpp(),
            'bank' => $systemPaymentDetail->getBank(),
            'corr_account_number' => $systemPaymentDetail->getCorrAccountNumber(),
        ];
        $form->setData($data);

        // Проверяем, является ли пост POST-запросом.
        if ($isPost) {
            //запрос от ajax формы или нет?
            if ($isAjax) {
                $content = $this->getRequest()->getContent();
                $data = Json::decode($content, Json::TYPE_ARRAY);
            } else {
                $data = $this->params()->fromPost();
            }
            // Заполняем форму данными.
            $form->setData($data);
            if ($form->isValid()) {
                // Get validated form data
                $data = $form->getData();

                try {
                    // update payment detail
                    $this->settingManager->updateSystemPaymentDetail($systemPaymentDetail, $data);
                    // SUCCESS Response
                    if ($isAjax) {
                        // Return Json with SUCCESS status
                        return $this->message()->success('System payment detail successfully edited');
                    }
                    // HTTP
                    return $this->message()->success('System payment detail successfully edited');
                } catch (\Throwable $t) {

                    return $this->message()->error($t->getMessage());
                }
            }
            if($isAjax) {
                // Return Json with ERROR status
                return $this->message()->invalidFormData($form->getMessages());
            }
            // В Http вернется форма с указанием ошибок валидации
        }
        // If no POST data
        // SUCCESS Response
        // Ajax - отдаем csrf токен
        if($isAjax){
            // Return Json with SUCCESS status (in this case also with $data array, which contains csrf token)
            $data = ['csrf' => $form->get('csrf')->getValue()];
            return $this->message()->success('CSRF token return', $data);
        }

        return new ViewModel([
            'form' => $form,
        ]);
    }
}