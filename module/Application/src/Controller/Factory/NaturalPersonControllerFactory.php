<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\CivilLawSubject\CivilLawSubjectPolitics;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\NaturalPersonController;
use ModuleRbac\Service\RbacManager;

/**
 * Это фабрика для NaturalPersonController
 */
class NaturalPersonControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return NaturalPersonController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);
        $naturalPersonManager = $container->get(NaturalPersonManager::class);
        $naturalPersonDocumentManager = $container->get(NaturalPersonDocumentManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $ndsTypeManager = $container->get(NdsTypeManager::class);
        $civilLawSubjectPolitics = $container->get(CivilLawSubjectPolitics::class);

        $config = $container->get('Config');

        return new NaturalPersonController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $civilLawSubjectManager,
            $naturalPersonManager,
            $naturalPersonDocumentManager,
            $paymentMethodManager,
            $rbacManager,
            $ndsTypeManager,
            $civilLawSubjectPolitics,
            $config
        );
    }
}