<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\NaturalPersonManager;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleBank\Service\BankManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\PaymentMethodController;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use ModuleRbac\Service\RbacManager;

/**
 * Это фабрика для PaymentMethodController
 */
class PaymentMethodControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);
        $naturalPersonManager = $container->get(NaturalPersonManager::class);
        $legalEntityManager = $container->get(LegalEntityManager::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $bankManager = $container->get(BankManager::class);

        $config = $container->get('Config');

        return new PaymentMethodController(
            $entityManager,
            $paymentMethodManager,
            $civilLawSubjectManager,
            $naturalPersonManager,
            $legalEntityManager,
            $userManager,
            $baseAuthManager,
            $rbacManager,
            $bankManager,
            $config
        );
    }
}