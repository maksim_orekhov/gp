<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\LegalEntityDocumentManager;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\LegalEntityDocumentController;

/**
 * Это фабрика для LegalEntityDocumentController
 */
class LegalEntityDocumentControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $legalEntityDocumentManager = $container->get(LegalEntityDocumentManager::class);
        $rbacManager = $container->get(RbacManager::class);

        $config = $container->get('Config');

        return new LegalEntityDocumentController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $legalEntityDocumentManager,
            $rbacManager,
            $config
        );
    }
}