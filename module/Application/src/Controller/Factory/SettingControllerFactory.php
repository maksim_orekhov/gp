<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Application\Controller\SettingController;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\SettingManager;

/**
 * This is the factory for AuthController
 */
class SettingControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $settingManager = $container->get(SettingManager::class);

        return new SettingController($settingManager);
    }
}