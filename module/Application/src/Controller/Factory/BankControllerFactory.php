<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Application\Controller\BankController;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleBank\Service\BankManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * This is the factory for PhoneController
 */
class BankControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BankController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $bankManager = $container->get(BankManager::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $rbacManager = $container->get(RbacManager::class);

        return new BankController(
            $bankManager,
            $userManager,
            $baseAuthManager,
            $rbacManager
        );
    }
}