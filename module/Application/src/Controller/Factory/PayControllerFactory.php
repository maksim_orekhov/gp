<?php
namespace Application\Controller\Factory;

use Application\Service\Deal\DealManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\PayController;
use ModuleRbac\Service\RbacManager;

/**
 * Class PayControllerFactory
 * @package Application\Controller\Factory
 */
class PayControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $dealManager = $container->get(DealManager::class);

        $config = $container->get('Config');

        return new PayController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $rbacManager,
            $dealManager,
            $config
        );
    }
}