<?php
namespace Application\Controller\Factory;

use Core\EventManager\AuthEventProvider;
use ModuleFileManager\Service\FileManager;
use Interop\Container\ContainerInterface;
use Application\Controller\TestController;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Listener\CreditPaymentOrderAvailabilityListener;
use Core\Provider\QrCodeProvider;
use Core\Service\TwigRenderer;

/**
 * This is the factory for TestController
 */
class TestControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $fileManager = $container->get(FileManager::class);
        $creditPaymentOrderAvailabilityListener = $container->get(CreditPaymentOrderAvailabilityListener::class);
        $qrCodeProvider = $container->get(QrCodeProvider::class);
        $twigRenderer = $container->get(TwigRenderer::class);
        $config = $container->get('Config');
        $authEventProvider = $container->get(AuthEventProvider::class);
        $mandarinPaymentOrderManager = $container->get(MandarinPaymentOrderManager::class);

        return new TestController(
            $entityManager,
            $fileManager,
            $creditPaymentOrderAvailabilityListener,
            $qrCodeProvider,
            $twigRenderer,
            $authEventProvider,
            $mandarinPaymentOrderManager,
            $config
        );
    }
}