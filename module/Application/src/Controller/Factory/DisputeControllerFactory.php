<?php
namespace Application\Controller\Factory;

use Application\Service\Deal\DealPolitics;
use Application\Service\Deal\DealStatus;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Dispute\DisputeHistoryManager;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Dispute\RefundManager;
use Application\Service\Payment\PaymentManager;
use Interop\Container\ContainerInterface;
use Application\Controller\DisputeController;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\Deal\DealManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleRbac\Service\RbacManager;
use Application\Service\Dispute\DisputeOperatorCommentManager;

/**
 * Class DisputeCommentControllerFactory
 * @package Application\Controller\Factory
 */
class DisputeControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DisputeController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager  = $container->get('doctrine.entitymanager.orm_default');
        $disputeManager = $container->get(DisputeManager::class);
        $dealManager    = $container->get(DealManager::class);
        $baseAuthManager    = $container->get(BaseAuthManager::class);
        $userManager    = $container->get(UserManager::class);
        $rbacManager    = $container->get(RbacManager::class);
        $dealPolitics   = $container->get(DealPolitics::class);
        $disputeOperatorCommentManager = $container->get(DisputeOperatorCommentManager::class);
        $dealStatus = $container->get(DealStatus::class);
        $refundManager = $container->get(RefundManager::class);
        $discountManager = $container->get(DiscountManager::class);
        $paymentManager = $container->get(PaymentManager::class);
        $disputeHistoryManager = $container->get(DisputeHistoryManager::class);
        $config = $container->get('Config');

        return new DisputeController(
            $entityManager,
            $disputeManager,
            $dealManager,
            $baseAuthManager,
            $userManager,
            $rbacManager,
            $dealPolitics,
            $disputeOperatorCommentManager,
            $dealStatus,
            $refundManager,
            $discountManager,
            $paymentManager,
            $disputeHistoryManager,
            $config
        );
    }
}