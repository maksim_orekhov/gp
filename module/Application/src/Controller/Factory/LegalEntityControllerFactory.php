<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\CivilLawSubject\CivilLawSubjectPolitics;
use Application\Service\CivilLawSubject\LegalEntityDocumentManager;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\LegalEntityController;
use ModuleRbac\Service\RbacManager;

/**
 * Это фабрика для LegalEntityController
 */
class LegalEntityControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return LegalEntityController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);
        $legalEntityManager = $container->get(LegalEntityManager::class);
        $legalEntityDocumentManager = $container->get(LegalEntityDocumentManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $ndsTypeManager = $container->get(NdsTypeManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $civilLawSubjectPolitics = $container->get(CivilLawSubjectPolitics::class);

        $config = $container->get('Config');

        return new LegalEntityController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $civilLawSubjectManager,
            $legalEntityManager,
            $legalEntityDocumentManager,
            $paymentMethodManager,
            $rbacManager,
            $ndsTypeManager,
            $civilLawSubjectPolitics,
            $config
        );
    }
}