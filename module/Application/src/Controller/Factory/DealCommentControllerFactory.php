<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Application\Controller\DealCommentController;
use Application\Service\Deal\DealOperatorCommentManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\Deal\DealManager;
use ModuleMessage\Service\MessageManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class DealCommentControllerFactory
 * @package Application\Controller\Factory
 */
class DealCommentControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DealCommentController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $dealManager = $container->get(DealManager::class);
        $messageManager = $container->get(MessageManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $dealOperatorCommentManager = $container->get(DealOperatorCommentManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $config = $container->get('Config');

        return new DealCommentController(
            $entityManager,
            $dealManager,
            $messageManager,
            $baseAuthManager,
            $userManager,
            $dealOperatorCommentManager,
            $rbacManager,
            $config);
    }
}