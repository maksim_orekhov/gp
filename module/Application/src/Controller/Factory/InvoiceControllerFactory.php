<?php
namespace Application\Controller\Factory;

use Application\Controller\InvoiceController;
use Application\Provider\PdfProvider;
use Application\Service\Deal\DealManager;
use Application\Service\InvoiceManager;
use Application\Service\SettingManager;
use Core\Service\TwigRenderer;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class InvoiceControllerFactory
 * @package Application\Controller\Factory
 */
class InvoiceControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return InvoiceController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $invoiceManager = $container->get(InvoiceManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $dealManager = $container->get(DealManager::class);
        $twigRenderer = $container->get(TwigRenderer::class);
        $pdfProvider = $container->get(PdfProvider::class);
        $rbacManager = $container->get(RbacManager::class);
        $settingManager = $container->get(SettingManager::class);
        $config = $container->get('Config');

        return new InvoiceController(
            $invoiceManager,
            $baseAuthManager,
            $userManager,
            $dealManager,
            $twigRenderer,
            $pdfProvider,
            $rbacManager,
            $settingManager,
            $config
        );
    }
}