<?php
namespace Application\Controller\Factory;

use Application\Controller\DiscountController;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Payment\PaymentMethodManager;
use Interop\Container\ContainerInterface;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DiscountControllerFactory
 * @package Application\Controller\Factory
 */
class DiscountControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DiscountController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $discountManager = $container->get(DiscountManager::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $disputeManager = $container->get(DisputeManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $discountManager->setDisputeManager($disputeManager);

        return new DiscountController(
            $discountManager,
            $userManager,
            $baseAuthManager,
            $disputeManager,
            $paymentMethodManager
        );
    }
}