<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\CivilLawSubject\CivilLawSubjectPolitics;
use Application\Service\CivilLawSubject\SoleProprietorManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\SoleProprietorController;
use ModuleRbac\Service\RbacManager;

/**
 * Class SoleProprietorControllerFactory
 * @package Application\Controller\Factory
 */
class SoleProprietorControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return SoleProprietorController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $ndsTypeManager = $container->get(NdsTypeManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $civilLawSubjectPolitics = $container->get(CivilLawSubjectPolitics::class);
        $soleProprietorManager = $container->get(SoleProprietorManager::class);

        $config = $container->get('Config');

        return new SoleProprietorController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $civilLawSubjectManager,
            $paymentMethodManager,
            $rbacManager,
            $ndsTypeManager,
            $civilLawSubjectPolitics,
            $soleProprietorManager,
            $config
        );
    }
}