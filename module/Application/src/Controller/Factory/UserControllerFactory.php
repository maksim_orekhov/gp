<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\UserController;

/**
 * Class UserControllerFactory
 * @package Application\Controller\Factory
 */
class UserControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var UserManager $userManager */
        $userManager = $container->get(UserManager::class);

        $config = $container->get('Config');

        // Инстанцируем контроллер и внедряем зависимости
        return new UserController(
            $userManager,
            $config
        );
    }
}