<?php
namespace Application\Controller\Factory;

use Application\Controller\DealDeadlineController;
use Application\Service\Deal\DealDeadlineManager;
use Interop\Container\ContainerInterface;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class DealDeadlineControllerFactory
 * @package Application\Controller\Factory
 */
class DealDeadlineControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DealDeadlineController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealDeadlineManager = $container->get(DealDeadlineManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $config = $container->get('Config');

        return new DealDeadlineController(
            $dealDeadlineManager,
            $baseAuthManager,
            $userManager,
            $rbacManager,
            $config
        );
    }
}