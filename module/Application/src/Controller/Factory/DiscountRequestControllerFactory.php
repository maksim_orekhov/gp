<?php
namespace Application\Controller\Factory;

use Application\Controller\DiscountRequestController;
use Application\Listener\ArbitrageRequestListener;
use Application\Service\Deal\DealManager;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Dispute\DisputeManager;
use Interop\Container\ContainerInterface;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DiscountRequestControllerFactory
 * @package Application\Controller\Factory
 */
class DiscountRequestControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DiscountRequestController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var DiscountManager $discountManager */
        $discountManager = $container->get(DiscountManager::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $dealManager = $container->get(DealManager::class);
        /** @var DisputeManager $disputeManager */
        $disputeManager = $container->get(DisputeManager::class);
        $arbitrageRequestListener = $container->get(ArbitrageRequestListener::class);
        $discountManager->setDisputeManager($disputeManager);

        return new DiscountRequestController(
            $discountManager,
            $userManager,
            $baseAuthManager,
            $dealManager,
            $disputeManager,
            $arbitrageRequestListener
        );
    }
}