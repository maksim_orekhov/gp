<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\LegalEntityDocumentManager;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Application\Service\CivilLawSubject\LegalEntityTaxInspectionManager;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\LegalEntityTaxInspectionController;

/**
 * Это фабрика для LegalEntityTaxInspectionController
 */
class LegalEntityTaxInspectionControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return LegalEntityTaxInspectionController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $legalEntityManager = $container->get(LegalEntityManager::class);
        $legalEntityDocumentManager = $container->get(LegalEntityDocumentManager::class);
        $legalEntityTaxInspectionManager = $container->get(LegalEntityTaxInspectionManager::class);
        $rbacManager = $container->get(RbacManager::class);

        $config = $container->get('Config');

        return new LegalEntityTaxInspectionController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $legalEntityManager,
            $legalEntityDocumentManager,
            $legalEntityTaxInspectionManager,
            $rbacManager,
            $config
        );
    }
}