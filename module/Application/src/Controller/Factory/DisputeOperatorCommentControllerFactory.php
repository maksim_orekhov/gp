<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Application\Controller\DisputeOperatorCommentController;
use Application\Service\Dispute\DisputeOperatorCommentManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\Deal\DealManager;
use ModuleMessage\Service\MessageManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleRbac\Service\RbacManager;
use Application\Service\Dispute\DisputeManager;

/**
 * Class DisputeOperatorCommentControllerFactory
 * @package Application\Controller\Factory
 */
class DisputeOperatorCommentControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DisputeOperatorCommentController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $dealManager = $container->get(DealManager::class);
        $messageManager = $container->get(MessageManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $dealCommentManager = $container->get(DisputeOperatorCommentManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $disputeManager = $container->get(DisputeManager::class);

        return new DisputeOperatorCommentController(
            $entityManager,
            $dealManager,
            $messageManager,
            $baseAuthManager,
            $userManager,
            $dealCommentManager,
            $rbacManager,
            $disputeManager
        );
    }
}