<?php
namespace Application\Controller\Factory;

use Application\Controller\ErrorController;
use Application\Service\ErrorManager;
use Interop\Container\ContainerInterface;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class ErrorControllerFactory
 * @package Application\Controller\Factory
 */
class ErrorControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ErrorController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $baseAuthManager = $container->get(BaseAuthManager::class);
        $errorManager = $container->get(ErrorManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $config = $container->get('Config');

        return new ErrorController(
            $baseAuthManager,
            $errorManager,
            $rbacManager,
            $config
        );
    }
}