<?php
namespace Application\Controller\Factory;

use Application\Controller\DeliveryController;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealPolitics;
use Application\Service\DeliveryManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryControllerFactory
 * @package Application\Controller\Factory
 */
class DeliveryControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $deliveryManager = $container->get(DeliveryManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $dealManager = $container->get(DealManager::class);
        $rbacManager = $container->get(RbacManager::class);

        return new DeliveryController(
            $deliveryManager,
            $baseAuthManager,
            $userManager,
            $dealManager,
            $rbacManager
        );
    }
}