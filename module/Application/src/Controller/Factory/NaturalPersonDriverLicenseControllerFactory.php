<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use Application\Service\CivilLawSubject\NaturalPersonDriverLicenseManager;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\NaturalPersonDriverLicenseController;

/**
 * Это фабрика для NaturalPersonDriverLicenseController
 */
class NaturalPersonDriverLicenseControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $naturalPersonManager = $container->get(NaturalPersonManager::class);
        $naturalPersonDocumentManager = $container->get(NaturalPersonDocumentManager::class);
        $naturalPersonDriverLicenseManager = $container->get(NaturalPersonDriverLicenseManager::class);
        $rbacManager = $container->get(RbacManager::class);

        $config = $container->get('Config');

        return new NaturalPersonDriverLicenseController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $naturalPersonManager,
            $naturalPersonDocumentManager,
            $naturalPersonDriverLicenseManager,
            $rbacManager,
            $config
        );
    }
}