<?php
namespace Application\Controller\Factory;

use Application\Controller\DealTokenController;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealTokenManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;

/**
 * Class DealPartnerTokenControllerFactory
 * @package Application\Controller\Factory
 */
class DealTokenControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DealTokenController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $dealManager = $container->get(DealManager::class);
        $dealTokenManager = $container->get(DealTokenManager::class);
        $config = $container->get('Config');

        return new DealTokenController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $civilLawSubjectManager,
            $paymentMethodManager,
            $rbacManager,
            $dealManager,
            $dealTokenManager,
            $config
        );
    }
}