<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\LegalEntityBankDetailManager;
use Application\Service\CivilLawSubject\LegalEntityDocumentManager;
use Application\Service\CivilLawSubject\LegalEntityManager;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\LegalEntityBankDetailController;

/**
 * Это фабрика для LegalEntityBankDetailController
 */
class LegalEntityBankDetailControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return LegalEntityBankDetailController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $legalEntityManager = $container->get(LegalEntityManager::class);
        $legalEntityDocumentManager = $container->get(LegalEntityDocumentManager::class);
        $legalEntityBankDetailManager = $container->get(LegalEntityBankDetailManager::class);
        $rbacManager = $container->get(RbacManager::class);

        $config = $container->get('Config');

        return new LegalEntityBankDetailController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $legalEntityManager,
            $legalEntityDocumentManager,
            $legalEntityBankDetailManager,
            $rbacManager,
            $config
        );
    }
}