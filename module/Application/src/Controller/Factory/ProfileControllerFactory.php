<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealTokenManager;
use Application\Service\NdsTypeManager;
use Interop\Container\ContainerInterface;
use Core\Service\OperationConfirmManager;
use Core\Service\Auth\AuthService;
use Application\Service\UserManager;
use ModuleBank\Service\BankManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Controller\ProfileController;

/**
 * Class ProfileControllerFactory
 * @package Application\Controller\Factory
 */
class ProfileControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ProfileController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $operationConfirmManager = $container->get(OperationConfirmManager::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(AuthService::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);
        $bankManager = $container->get(BankManager::class);
        $ndsTypeManager = $container->get(NdsTypeManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $dealManager = $container->get(DealManager::class);
        $dealTokenManager = $container->get(DealTokenManager::class);
        $config = $container->get('Config');

        // Инстанцируем контроллер и внедряем зависимости
        return new ProfileController(
            $userManager,
            $baseAuthManager,
            $civilLawSubjectManager,
            $bankManager,
            $ndsTypeManager,
            $operationConfirmManager,
            $rbacManager,
            $dealManager,
            $dealTokenManager,
            $config
        );
    }
}