<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use Application\Service\CivilLawSubject\NaturalPersonPassportManager;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\NaturalPersonPassportController;

/**
 * Это фабрика для NaturalPersonPassportController
 */
class NaturalPersonPassportControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $naturalPersonManager = $container->get(NaturalPersonManager::class);
        $naturalPersonDocumentManager = $container->get(NaturalPersonDocumentManager::class);
        $naturalPersonPassportManager = $container->get(NaturalPersonPassportManager::class);
        $rbacManager = $container->get(RbacManager::class);

        $config = $container->get('Config');

        return new NaturalPersonPassportController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $naturalPersonManager,
            $naturalPersonDocumentManager,
            $naturalPersonPassportManager,
            $rbacManager,
            $config
        );
    }
}