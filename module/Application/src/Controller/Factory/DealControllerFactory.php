<?php
namespace Application\Controller\Factory;

use Application\EventManager\DealEventProvider;
use Application\Service\Deal\DealTokenManager;
use Application\Service\Dispute\DisputeHistoryManager;
use Application\Service\Dispute\DiscountManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Deal\DealStatus;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Interop\Container\ContainerInterface;
use Application\Controller\DealController;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use ModuleDelivery\Service\DeliveryOrderManager;
use ModuleDelivery\Service\DeliveryTrackingNumberManager;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\Payment\PaymentPolitics;
use Application\Service\Deal\DealOperatorCommentManager;
use Application\Service\Payment\CalculatedDataProvider;

/**
 * Class BankControllerFactory
 * @package Application\Controller\Factory
 */
class DealControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DealController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $dealManager = $container->get(\Application\Service\Deal\DealManager::class);
        $paymentManager = $container->get(\Application\Service\Payment\PaymentManager::class);
        $dealAgentManager = $container->get(\Application\Service\Deal\DealAgentManager::class);
        $baseAuthManager = $container->get(\Core\Service\Base\BaseAuthManager::class);
        $userManager = $container->get(\Application\Service\UserManager::class);
        $dealConditionsListener = $container->get(\Application\Listener\DealConditionsListener::class);
        $dealPolitics = $container->get(\Application\Service\Deal\DealPolitics::class);
        $paymentPolitics = $container->get(PaymentPolitics::class);
        $deliveryTrackingNumberManager = $container->get(DeliveryTrackingNumberManager::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $ndsTypeManager = $container->get(NdsTypeManager::class);
        $dealOperatorCommentManager = $container->get(DealOperatorCommentManager::class);
        $calculatedDataProvider = $container->get(CalculatedDataProvider::class);
        $dealStatus = $container->get(DealStatus::class);
        $discountManager = $container->get(DiscountManager::class);
        $disputeHistoryManager = $container->get(DisputeHistoryManager::class);
        $dealTokenManager = $container->get(DealTokenManager::class);
        $dealEventProvider = $container->get(DealEventProvider::class);
        $deliveryOrderManager = $container->get(DeliveryOrderManager::class);
        $deliveryOrderDpdManager = $container->get(DeliveryOrderDpdManager::class);
        $deliveryEventProvider = $container->get(DeliveryEventProvider::class);
        $config = $container->get('Config');

        return new DealController(
            $entityManager,
            $dealManager,
            $paymentManager,
            $dealAgentManager,
            $baseAuthManager,
            $userManager,
            $dealConditionsListener,
            $dealPolitics,
            $paymentPolitics,
            $deliveryTrackingNumberManager,
            $rbacManager,
            $civilLawSubjectManager,
            $paymentMethodManager,
            $ndsTypeManager,
            $dealOperatorCommentManager,
            $calculatedDataProvider,
            $dealStatus,
            $discountManager,
            $disputeHistoryManager,
            $dealTokenManager,
            $dealEventProvider->getEventManager(),
            $deliveryOrderManager,
            $deliveryOrderDpdManager,
            $deliveryEventProvider->getEventManager(),
            $config
        );
    }
}