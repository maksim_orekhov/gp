<?php
namespace Application\Controller\Factory;

use Application\Controller\CivilLawSubjectController;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class CivilLawSubjectControllerFactory
 * @package Application\Controller\Factory
 */
class CivilLawSubjectControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CivilLawSubjectController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);

        return new CivilLawSubjectController(
            $userManager,
            $baseAuthManager,
            $civilLawSubjectManager
        );
    }
}