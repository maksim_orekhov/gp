<?php
namespace Application\Controller\Factory;

use Application\Controller\RefundController;
use Application\Listener\ArbitrageRequestListener;
use Application\Listener\RefundConfirmSuccessListener;
use Application\Service\Deal\DealManager;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Dispute\RefundManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Payment\PaymentMethodManager;
use Interop\Container\ContainerInterface;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class RefundControllerFactory
 * @package Application\Controller\Factory
 */
class RefundControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RefundController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $refundManager = $container->get(RefundManager::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $dealManager = $container->get(DealManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $paymentManager = $container->get(PaymentManager::class);
        $disputeManager = $container->get(DisputeManager::class);
        $arbitrageRequestListener = $container->get(ArbitrageRequestListener::class);
        $refundConfirmSuccessListener = $container->get(RefundConfirmSuccessListener::class);
        $refundManager->setDisputeManager($disputeManager);

        return new RefundController(
            $refundManager,
            $userManager,
            $baseAuthManager,
            $dealManager,
            $paymentMethodManager,
            $paymentManager,
            $disputeManager,
            $arbitrageRequestListener,
            $refundConfirmSuccessListener
        );
    }
}