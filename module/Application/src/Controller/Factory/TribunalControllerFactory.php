<?php
namespace Application\Controller\Factory;

use Application\Controller\TribunalController;
use Application\Listener\TribunalConfirmSuccessListener;
use Application\Listener\ArbitrageRequestListener;
use Application\Service\Deal\DealManager;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Dispute\TribunalManager;
use Interop\Container\ContainerInterface;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class TribunalControllerFactory
 * @package Application\Controller\Factory
 */
class TribunalControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return TribunalController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $tribunalManager = $container->get(TribunalManager::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $dealManager = $container->get(DealManager::class);
        $disputeManager = $container->get(DisputeManager::class);
        $tribunalConfirmSuccessListener = $container->get(TribunalConfirmSuccessListener::class);
        $arbitrageRequestListener = $container->get(ArbitrageRequestListener::class);

        return new TribunalController(
            $tribunalManager,
            $userManager,
            $baseAuthManager,
            $dealManager,
            $disputeManager,
            $tribunalConfirmSuccessListener,
            $arbitrageRequestListener
        );
    }
}