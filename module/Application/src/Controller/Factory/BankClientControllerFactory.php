<?php
namespace Application\Controller\Factory;

use Interop\Container\ContainerInterface;
use Application\Controller\BankClientController;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Renderer\RendererInterface;

/**
 * Class BankClientControllerFactory
 * @package Application\Controller\Factory
 */
class BankClientControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $parser = $container->get(\Application\Service\Parser\BankClientParser::class);
        $bankClientManager = $container->get(\Application\Service\BankClient\BankClientManager::class);
        $fileManager = $container->get(\ModuleFileManager\Service\FileManager::class);
        $config = $container->get('Config');
        $tcpdf = $container->get(\TCPDF::class);
        $renderer = $container->get(RendererInterface::class);

        return new BankClientController(
            $entityManager,
            $parser,
            $bankClientManager,
            $fileManager,
            $tcpdf,
            $renderer,
            $config);
    }
}