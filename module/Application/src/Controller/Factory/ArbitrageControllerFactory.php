<?php
namespace Application\Controller\Factory;

use Application\Controller\ArbitrageController;
use Application\Listener\TribunalRequestListener;
use Application\Service\Deal\DealManager;
use Application\Service\Dispute\ArbitrageManager;
use Application\Service\Dispute\DisputeManager;
use Interop\Container\ContainerInterface;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class ArbitrageControllerFactory
 * @package Application\Controller\Factory
 */
class ArbitrageControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ArbitrageController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $arbitrageManager = $container->get(ArbitrageManager::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $dealManager = $container->get(DealManager::class);
        $disputeManager = $container->get(DisputeManager::class);
        $tribunalRequestListener = $container->get(TribunalRequestListener::class);
        $arbitrageManager->setDisputeManager($disputeManager);

        return new ArbitrageController(
            $arbitrageManager,
            $userManager,
            $baseAuthManager,
            $dealManager,
            $disputeManager,
            $tribunalRequestListener
        );
    }
}