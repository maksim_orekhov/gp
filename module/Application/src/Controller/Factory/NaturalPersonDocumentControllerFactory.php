<?php
namespace Application\Controller\Factory;

use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Controller\NaturalPersonDocumentController;

/**
 * Это фабрика для NaturalPersonDocumentController
 */
class NaturalPersonDocumentControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $naturalPersonDocumentManager = $container->get(NaturalPersonDocumentManager::class);
        $rbacManager = $container->get(RbacManager::class);

        $config = $container->get('Config');

        return new NaturalPersonDocumentController(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $naturalPersonDocumentManager,
            $rbacManager,
            $config
        );
    }
}