<?php

namespace Application\Controller;

use Application\Entity\User;
use Application\Form\DeadlineSearchForm;
use Application\Form\NotifyDeadlineForm;
use Application\Service\Deal\DealDeadlineManager;
use Application\Service\PaginatorOutputTrait;
use Application\Util\DataFilter;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleRbac\Service\RbacManager;
use Zend\Paginator\Paginator;

/**
 * Class DealController
 * @package Application\Controller
 */
class DealDeadlineController extends AbstractRestfulController
{
    use PaginatorOutputTrait;
    /**
     * @var DealDeadlineManager
     */
    private $dealDeadlineManager;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var RbacManager
     */
    private $rbacManager;
    /**
     * @var array
     */
    private $config;


    /**
     * DealDeadlineController constructor.
     * @param DealDeadlineManager $dealDeadlineManager
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param RbacManager $rbacManager
     * @param array $config
     */
    public function __construct(DealDeadlineManager $dealDeadlineManager,
                                BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                RbacManager $rbacManager,
                                array $config)
    {
        $this->dealDeadlineManager = $dealDeadlineManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->userManager = $userManager;
        $this->rbacManager = $rbacManager;
        $this->config = $config;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function dealDeadlineAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Data
        $data = $this->collectIncomingData();
        $paramsFilterAndSort = $this->getPaginationParams($this->config);
        $notify_result = false;

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Check permissions
            if (! $this->rbacManager->hasRole($user, 'Operator')) {

                return $this->redirect()->toRoute('not-authorized');
            }

            /** @var DeadlineSearchForm $deadlineSearchForm */
            $deadlineSearchForm = new DeadlineSearchForm();
            if (isset($data['filter'])) {
                /** @var DataFilter $dataFilter */
                $dataFilter = new DataFilter();

                $data['filter']['csrf'] = $data['csrf'];

                $filterResult = $dataFilter->filterData($deadlineSearchForm, $data['filter']);

                if ($filterResult['filter']) {
                    $paramsFilterAndSort['filter'] = $filterResult['filter'];
                } else {
                    $deadlineSearchForm = $filterResult['form'];
                }
            }
            /** @var Paginator $deadlinePayments */
            $deadlinePayments = $this->dealDeadlineManager->getPaymentsWithDeadline($paramsFilterAndSort);
            $deadlineDealsOutput = $this->dealDeadlineManager->getDealDeadlineCollectionForOutputByPayments($deadlinePayments);
            // Paginator data
            $paginator = null;
            if (method_exists($deadlinePayments , 'getPages')) {
                $paginator = $deadlinePayments->getPages();
            }

            $notifyDeadlineForm = new NotifyDeadlineForm();
            // Проверяем, является ли пост POST-запросом.
            $postData = $data['postData'];
            if (\is_array($postData) && array_key_exists('type_form', $postData) && $postData['type_form'] === NotifyDeadlineForm::TYPE_FORM) {
                $notifyDeadlineForm->setData($postData);
                if ($notifyDeadlineForm->isValid()) {
                    //уведомить о приближении дедлайна
                    $notify_result = $this->dealDeadlineManager->notifyDealAgentsAboutNearDeadline();
                }
                if ($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($notifyDeadlineForm->getMessages());
                }
            }
        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $paginatorOutput = null;
        if ($paginator) {
            $paginatorOutput = $this->getPaginatorOutput($paginator);
        }

        // SUCCESS Response with CSRF
        // Ajax - csrf token
        if ($isAjax) {
            $csrf   = $deadlineSearchForm->get('csrf')->getValue();
            $data   = [
                'csrf'      => $csrf,
                'deadlineDeals' => $deadlineDealsOutput,
                'paginator' => $paginatorOutput
            ];

            return $this->message()->success('DeadlineDeals', $data);
        }

        $deadlineSearchForm->prepare();
        $notifyDeadlineForm->prepare();
        $view = new TwigViewModel([
            'notifyDeadlineForm' => $notifyDeadlineForm,
            'deadlineSearchForm' => $deadlineSearchForm,
            'deadlineDeals' => $deadlineDealsOutput,
            'paginator' => $paginatorOutput,
            'is_send_notify' => $notify_result,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/deal-deadline/collection');

        return $view;
    }
}