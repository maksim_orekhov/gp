<?php

namespace Application\Controller;

use Application\Entity\Deal;
use Application\Form\DeliveryConfirmForm;
use Application\Service\DeliveryManager;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Exception\LogicException;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Application\Service\Deal\DealManager;
use Core\Controller\AbstractRestfulController;
use ModuleRbac\Service\RbacManager;

/**
 * Class DeliveryController
 * @package Application\Controller
 */
class DeliveryController extends AbstractRestfulController
{
    const NOT_ALLOWED_CREATE_DELIVERY = 'Not allowed to create delivery confirmations for this deal';

    /**
     * @var DeliveryManager
     */
    private $deliveryManager;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var dealManager
     */
    private $dealManager;
    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * DeliveryController constructor.
     * @param DeliveryManager $deliveryManager
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param DealManager $dealManager
     * @param RbacManager $rbacManager
     */
    public function __construct(DeliveryManager $deliveryManager,
                                BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                DealManager $dealManager,
                                RbacManager $rbacManager)
    {
        $this->deliveryManager = $deliveryManager;
        $this->baseAuthManager     = $baseAuthManager;
        $this->userManager     = $userManager;
        $this->dealManager     = $dealManager;
        $this->rbacManager     = $rbacManager;
    }

    /**
     * @param mixed $data
     * @return array|mixed
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            //create confirm
            if ( !isset($data['idDeal']) || (int) $data['idDeal'] <= 0) {

                throw new \Exception('bad data provided: not found id deal');
            }
            /** @var Deal $deal */ // Can throw Exception
            $deal = $this->dealManager->getDealById($data['idDeal']);
            if( $deal === null) {

                throw new \Exception('bad data provided: not found deal by id');
            }
            //check permission
            // Important! Only Customer can confirm delivery
            if ( !$this->access('deal.own.customer', ['user' => $user, 'deal' => $deal]) ) {

                return $this->message()->error(null, null, BaseMessageCodeInterface::ERROR_ACCESS_DENIED);
            }
            if ( !$this->deliveryManager->checkPermissionForDeliveryConfirmByDeal($deal) ) {

                throw new LogicException(null,
                    LogicException::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED);
            }

            $form = new DeliveryConfirmForm();
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $resultSavedForm = $this->saveFormData($formData);

                //Ajax success
                return $this->message()->success('delivery confirmed', $resultSavedForm);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * подтверждение доставки
     *
     * @return mixed
     * @throws \Exception
     */
    public function confirmFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try{
            /**
             * @var Deal $deal
             */
            $formDataInit = $this->processFormDataInit();
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if ( !isset($formDataInit['deal']) || !$formDataInit['deal'] instanceof Deal){

                throw new \Exception('bad data provided: not found deal');
            }
            $deal = $formDataInit['deal'];
            if ( !$this->access('deal.own.customer', ['user' => $user, 'deal' => $deal]) ) {

                return $this->redirect()->toRoute('access-denied');
            }

            if ( !$this->deliveryManager->checkPermissionForDeliveryConfirmByDeal($deal) ) {

                throw new LogicException(null,
                    LogicException::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED);
            }

            $form = new DeliveryConfirmForm();
            $form->get('idDeal')->setValue($deal->getId());
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idDeal'] = $deal->getId();
                    $this->saveFormData($formData);

                    //http success
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
            }

            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'deal' => $dealOutput,
            'deliveryConfirmForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/delivery/confirm');

        return $view;
    }

    /**
     * @param $data
     * @return array
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    private function saveFormData($data)
    {
        if ( !isset($data['idDeal']) || (int) $data['idDeal'] <= 0) {

            throw new \Exception('bad data provided: not found id deal');
        }
        /** @var Deal $deal */ // Can throw Exception
        $deal = $this->dealManager->getDealById($data['idDeal']);
        if( $deal === null) {

            throw new \Exception('bad data provided: not found deal by id');
        }

        //confirm delivery
        $confirmDelivery = $this->deliveryManager->confirmDelivery($deal);

        if (null !== $confirmDelivery) {
            // Проверяем можно ли создавать платёжку на выплату и создаем
            $this->deliveryManager->createPayOffIfStatusDeliveryAllows($confirmDelivery);
        }

        return [
            'deal' => $deal,
            'confirmDelivery' => $confirmDelivery
        ];
    }


    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function formInitAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }
        try {
            // Can throw Exception
            $formDataInit = $this->processFormDataInit();
            $data = $formDataInit;
            $data['user'] = $this->userManager->extractUser($formDataInit['user']);
            $data['user']['is_operator'] = $this->rbacManager->hasRole(null, 'Operator');
            $data['deal'] = $formDataInit['deal'] && $formDataInit['user'] ? $this->dealManager->getDealOutputForSingle($formDataInit['deal'], $formDataInit['user']) : null;

        }
        catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        return $this->message()->success('formInit', $data);
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            $deal = null;
            if (isset($incomingData['idDeal'])){

                /** @var $deal $deal */
                $deal = $this->dealManager
                    ->getDealById((int) $incomingData['idDeal']);

                // Check if $deal belongs to current user
                if ($user && $deal && !DealManager::isUserPartOfDeal($deal, $user)) {

                    $deal = null;
                }
            }

        } catch (\Throwable $t){

            throw new \Exception($t->getMessage());
        }

        return [
            'user'=> $user,
            'deal'=> $deal,
        ];
    }
}