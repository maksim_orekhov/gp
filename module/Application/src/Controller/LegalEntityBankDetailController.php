<?php

namespace Application\Controller;

use Core\Controller\AbstractRestfulController;
use Application\Entity\LegalEntity;
use Application\Entity\LegalEntityBankDetail;
use Application\Form\LegalEntityBankDetailForm;
use Application\Service\CivilLawSubject\LegalEntityBankDetailManager;
use Application\Service\CivilLawSubject\LegalEntityDocumentManager;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Doctrine\ORM\EntityManager;
use Zend\View\Model\ViewModel;
use Zend\Hydrator\ClassMethods;

/**
 * Class LegalEntityBankDetailController
 * @package Application\Controller
 */
class LegalEntityBankDetailController extends AbstractRestfulController
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * Auth Manager
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\CivilLawSubject\LegalEntityManager
     */
    private $legalEntityManager;

    /**
     * @var \Application\Service\CivilLawSubject\LegalEntityDocumentManager
     */
    private $legalEntityDocumentManager;

    /**
     * @var \Application\Service\CivilLawSubject\LegalEntityBankDetailManager
     */
    private $legalEntityBankDetailManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * @var array
     */
    private $config;

    /**
     * LegalEntityBankDetailController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param LegalEntityManager $legalEntityManager
     * @param LegalEntityDocumentManager $legalEntityDocumentManager
     * @param LegalEntityBankDetailManager $legalEntityBankDetailManager
     * @param RbacManager $rbacManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                LegalEntityManager $legalEntityManager,
                                LegalEntityDocumentManager $legalEntityDocumentManager,
                                LegalEntityBankDetailManager $legalEntityBankDetailManager,
                                RbacManager $rbacManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->legalEntityManager = $legalEntityManager;
        $this->legalEntityDocumentManager = $legalEntityDocumentManager;
        $this->legalEntityBankDetailManager = $legalEntityBankDetailManager;
        $this->rbacManager = $rbacManager;
        $this->config = $config;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function getList()
    {
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // Whether the currant user yas role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            // Check permissions // Important! Operator has specific interface of view
            if (!($profile === false && $isOperator) && !($profile == 'profile' && !$isOperator)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $paginationParams = $this->getPaginationParams($this->config);
            $bankDetails = $this->legalEntityBankDetailManager->getCollectionBankDetail($user, $paginationParams, $isOperator);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginator = null;
        if (method_exists($bankDetails , 'getPages')) {
            $paginator = $bankDetails->getPages();
        }
        // Get Output
        $bankDetailsOutput = $this->legalEntityBankDetailManager
            ->extractBankDetailCollection($bankDetails);

        $data = [
            'paginator'   => $paginator,
            'bankDetails' => $bankDetailsOutput,
            'is_operator' => $isOperator,
        ];

        // Ajax -
        if ($isAjax) {
            return $this->message()->success("collection bank detail", $data);
        }
        // Http -
        $view =  new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity-bank-detail/collection');

        return $view;
    }

    /**
     * @param mixed $id
     * @return \Zend\Http\Response|ViewModel
     */
    public function get($id)
    {
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $bankDetail = $this->legalEntityBankDetailManager->getBankDetailById((int) $id);
            if( $bankDetail === null) {
                throw new \Exception('bad data provided: not found bank detail by id');
            }

            $legalEntity = $bankDetail->getLegalEntityDocument()->getLegalEntity();

            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            // Check permissions // Important! Only Owner or Operator can see details
            if (!($profile === false && $isOperator) &&
                !($profile == 'profile' && $this->access('profile.own.edit', ['legalEntity' => $legalEntity]))) {
                return $this->redirect()->toRoute('not-authorized');
            }

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        // Extracted bankDetail
        $bankDetailOutput = $this->legalEntityBankDetailManager
            ->getBankDetailsOutput($bankDetail);

        $data = [
            'bankDetail' => $bankDetailOutput,
            'is_operator' => $isOperator
        ];

        //Ajax
        if ( $isAjax ){
            return $this->message()->success("bank detail single", $data);
        }
        //Http
        $view =  new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity-bank-detail/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $form = new LegalEntityBankDetailForm('document');

        try {
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $resultSavedForm = $this->saveFormData($formData);

                //Ajax success
                return $this->message()->success('create bank detail', $resultSavedForm);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $form = new LegalEntityBankDetailForm('document');

        try {
            //set data to form
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData['idBankDetail'] = $id;
                $resultSavedForm = $this->saveFormData($formData);
                //Ajax success
                return $this->message()->success('update bank detail', $resultSavedForm);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @return \Zend\Http\Response
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $bankDetail = $this->legalEntityBankDetailManager->getBankDetailById((int) $id);
            if( $bankDetail === null) {
                throw new \Exception('bad data provided: not found bank detail by id');
            }

            $legalEntity = $bankDetail->getLegalEntityDocument()->getLegalEntity();
            $document = $bankDetail->getLegalEntityDocument();

            // Check permissions // Important! Only Owner or Operator can delete the LegalEntity
            if (!$this->access('profile.own.edit', ['legalEntity' => $legalEntity])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            // Deleting
            $this->legalEntityDocumentManager->remove($document);
        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        //Ajax success
        if ($isAjax) {
            return $this->message()->success('Документ удален');
        }
        //Http success
        // @TODO Куда возвращать?
        return $this->redirect()->toRoute(
            'user-profile/legal-entity-single',
            ['id' => $legalEntity->getId()]
        );
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function createFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        $form = new LegalEntityBankDetailForm('document');

        try {
            if ( !isset($formDataInit['legalEntity']) || !$formDataInit['legalEntity'] instanceof LegalEntity){
                throw new \Exception('bad data provided: not found legal entity');
            }
            $legalEntity = $formDataInit['legalEntity'];
            $form->get('idLegalEntity')->setValue($legalEntity->getId());

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idLegalEntity'] = $legalEntity->getId();
                    $resultSavedForm = $this->saveFormData($formData);

                    //http success
                    return $this->redirect()->toRoute(
                        'user-profile/legal-entity-single',
                        ['id' => $resultSavedForm['legalEntity']->getId()]
                    );
                }
            }
        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();

        $view = new TwigViewModel([
            'bankDetailForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity-bank-detail/create-form');

        return $view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function editFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        $form = new LegalEntityBankDetailForm('document');

        try {
            if ( !isset($formDataInit['bankDetail']) || !$formDataInit['bankDetail'] instanceof LegalEntityBankDetail){
                throw new \Exception('bad data provided: not found bank detail');
            }
            $bankDetail = $formDataInit['bankDetail'];
            // Extracted bankDetail
            $bankDetailOutput = $this->legalEntityBankDetailManager
                ->getBankDetailsOutput($formDataInit['bankDetail']);

            $form->setData([
                'checking_account' => $bankDetail->getCheckingAccount(),
                'name' => $bankDetail->getName(),
                'correspondent_account' => $bankDetail->getCorrespondentAccount(),
                'bic' => $bankDetail->getBic(),
            ]);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                //set data to form
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idBankDetail'] = $bankDetail->getId();
                    $resultSavedForm = $this->saveFormData($formData);

                    //http success
                    return $this->redirect()->toRoute(
                        'user-profile/legal-entity-single',
                        ['id' => $resultSavedForm['legalEntity']->getId()]
                    );
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
        $form->prepare();
        $view = new TwigViewModel([
            'bankDetail' => $bankDetailOutput,
            'bankDetailForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity-bank-detail/edit-form');

        return $view;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function deleteFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        try {
            if ( !isset($formDataInit['bankDetail']) || !$formDataInit['bankDetail'] instanceof LegalEntityBankDetail){
                throw new \Exception('bad data provided: not found bank detail');
            }
            // Extracted bankDetail
            $bankDetailOutput = $this->legalEntityBankDetailManager
                ->getBankDetailsOutput($formDataInit['bankDetail']);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        $view = new TwigViewModel([
            'bankDetail' => $bankDetailOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity-bank-detail/delete-form');

        return $view;
    }

    /**
     * @return array
     */
    public function formInitAction()
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // Hydrate objects for Ajax
        $hydrateData = $this->legalEntityBankDetailManager->extractSetOfObjects($formDataInit);

        return $this->message()->success('formInit', $hydrateData);
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array
     */
    private function processFormDataInit()
    {
        //step 1
        $incomingData = $this->collectIncomingData();

        //check
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            //process data for create
            $legalEntity = null;
            if ( isset($incomingData['idLegalEntity']) ){
                /** @var LegalEntity $legalEntity */
                $legalEntity = $this->legalEntityManager->getBasedOnPattern((int) $incomingData['idLegalEntity']);
                // Check if legalEntity belongs to current user
                if ($legalEntity && $legalEntity->getCivilLawSubject()->getUser() !== $user) {
                    $legalEntity = null;
                }
            }
            $allowedLegalEntities = $this->legalEntityManager->getAllowedLegalEntityByUser($user);

            //process data for update or delete
            $bankDetail = null;
            if ( isset($incomingData['idBankDetail']) ){
                /** @var LegalEntityBankDetail $bankDetail */
                $bankDetail = $this->legalEntityBankDetailManager->getBankDetailById((int) $incomingData['idBankDetail']);
                // Check if bankDetail belongs to current user
                if ($bankDetail && $bankDetail->getLegalEntityDocument()->getLegalEntity()->getCivilLawSubject()->getUser() !== $user) {
                    $bankDetail = null;
                }
            }
            $allowedBankDetails = $this->legalEntityBankDetailManager->getAllowedBankDetailByUser($user);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        //step 2
        return [
            //create
            'legalEntity' => $legalEntity,
            'allowedLegalEntities' => $allowedLegalEntities,
            //edit
            'bankDetail' => $bankDetail,
            'allowedBankDetails' => $allowedBankDetails,
        ];
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    private function saveFormData($data)
    {
        // Get current user // Can throw Exception
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

        if ( isset($data['idBankDetail']) ) {
            //update
            if ( (int) $data['idBankDetail'] <= 0 ) {
                throw new \Exception('bad data provided: not found bank detail id');
            }

            $bankDetail = $this->legalEntityBankDetailManager->getBankDetailById((int) $data['idBankDetail']);
            if( $bankDetail === null) {
                throw new \Exception('bad data provided: not found bank detail by id');
            }
            // Check if bankDetail belongs to current user
            if ($bankDetail->getLegalEntityDocument()->getLegalEntity()->getCivilLawSubject()->getUser() !== $user) {
                throw new \Exception('bankDetail not belongs to current user');
            }

            $document = $this->legalEntityDocumentManager
                ->update($bankDetail->getLegalEntityDocument(), $data);

            $result = [
                'bankDetail' => $document->getLegalEntityBankDetail(),
                'legalEntity' => $document->getLegalEntity()
            ];
        } else {
            //create
            if ( !isset($data['idLegalEntity']) || (int) $data['idLegalEntity'] <= 0){
                throw new \Exception('bad data provided: not found legal entity id');
            }

            $legalEntity = $this->legalEntityManager->getBasedOnPattern((int) $data['idLegalEntity']);

            if( $legalEntity === null) {
                throw new \Exception('bad data provided: not found legal entity by id');
            }

            // Check if legalEntity belongs to current user
            if ($legalEntity->getCivilLawSubject()->getUser() !== $user) {
                throw new \Exception('legalEntity not belongs to current user');
            }

            $document = $this->legalEntityDocumentManager
                ->create($legalEntity, LegalEntityBankDetail::class, $data);
            $bankDetail = $document->getLegalEntityBankDetail();

            $result = [
                'bankDetail' => $bankDetail,
                'legalEntity' => $legalEntity
            ];
        }

        return $result;
    }
}