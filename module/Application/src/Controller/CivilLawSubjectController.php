<?php
namespace Application\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Controller\Plugin\Message\MessageJsonModelTrait;
use Core\Service\TwigViewModel;
use Zend\View\Model\ViewModel;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Entity\User;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;

/**
 * Class CivilLawSubjectController
 * @package Application\Controller
 */
class CivilLawSubjectController extends AbstractRestfulController
{
    use MessageJsonModelTrait;
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * CivilLawSubjectController constructor.
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param CivilLawSubjectManager $civilLawSubjectManager
     */
    public function __construct(UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                CivilLawSubjectManager $civilLawSubjectManager)
    {
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->civilLawSubjectManager = $civilLawSubjectManager;
    }

    /**
     * Get CivilLawSubject collections (GET)
     *
     * @return array|TwigViewModel|mixed|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Doctrine\ORM\ORMException
     */
    public function getList()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        /** @var array $civil_law_subjects */
        $civil_law_subjects = $this->civilLawSubjectManager->getCivilLawSubjectsAsSeparateArrays($user);

        return $this->message()->success('Civil Law Subjects', json_encode($civil_law_subjects,JSON_UNESCAPED_UNICODE));
    }

    /**
     *
     * @throws \Exception
     */
    public function searchAction()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }
        $this->message()->setJsonStrategy(true);

        try {
            $data = $this->collectIncomingData();
            if ($data && array_key_exists('search', $data)) {
                $searchCivilLawSubjects = $this->civilLawSubjectManager->searchCivilLawSubjects($data['search']);
                $searchCivilLawSubjectOutput = $this->civilLawSubjectManager->getSearchCivilLawSubjectOutput($searchCivilLawSubjects);

                return $this->message()->success('search result', $searchCivilLawSubjectOutput);
            }
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->message()->error('Not defined the search parameters');
    }
}