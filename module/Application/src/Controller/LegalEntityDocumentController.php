<?php

namespace Application\Controller;

use Core\Controller\AbstractRestfulController;
use Application\Service\CivilLawSubject\LegalEntityDocumentManager;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Doctrine\ORM\EntityManager;
use Zend\Hydrator\ClassMethods;
use Zend\View\Model\ViewModel;

/**
 * Class LegalEntityDocumentController
 * @package Application\Controller
 */
class LegalEntityDocumentController extends AbstractRestfulController
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * Auth Manager
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * Legal Entity Document Manager
     * @var \Application\Service\CivilLawSubject\legalEntityDocumentManager
     */
    private $legalEntityDocumentManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * @var array
     */
    private $config;

    /**
     * LegalEntityDocumentController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param LegalEntityDocumentManager $legalEntityDocumentManager
     * @param RbacManager $rbacManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                LegalEntityDocumentManager $legalEntityDocumentManager,
                                RbacManager $rbacManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->legalEntityDocumentManager = $legalEntityDocumentManager;
        $this->rbacManager = $rbacManager;
        $this->config = $config;
    }

    /**
     * @return ViewModel
     *
     * @TODO Перевести на twig!
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            $isOperator = true;
            $paginationParams = $this->getPaginationParams($this->config);
            $documents = $this->legalEntityDocumentManager->getCollectionDocument($user, $paginationParams, $isOperator);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        if ($isAjax) {
            $data = [
                'paginator'=>[],
                'documents'=>[]
            ];
            $hydrator = new ClassMethods();
            if ( method_exists( $documents , 'getPages' )) {
                $data['paginator'] = $documents->getPages();
            }

            foreach ($documents as $document){
                $data['documents'][] = $hydrator->extract($document);
            }

            return $this->message()->success("collection document", $data);
        }

        $template = 'application/legal-entity-document/collection';
        $view =  new ViewModel([
            'documents' => $documents,
        ]);
        $view->setTemplate($template);

        return $view;
    }
}