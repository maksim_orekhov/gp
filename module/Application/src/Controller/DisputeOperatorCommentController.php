<?php

namespace Application\Controller;

use Application\Entity\DealAgent;
use Application\Form\DisputeCommentForm;
use Application\Service\Deal\DealManager;
use Application\Service\Dispute\DisputeOperatorCommentManager;
use Application\Form\DealCommentForm;
use Core\Service\TwigViewModel;
use ModuleMessage\Entity\Message;
use ModuleMessage\Service\MessageManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleRbac\Service\RbacManager;
use Application\Entity\Deal;
use Application\Entity\User;
use Application\Entity\Dispute;
use Core\Controller\AbstractRestfulController;
use Application\Service\Dispute\DisputeManager;

/**
 * Class DisputeCommentController
 * @package Application\Controller
 */
class DisputeOperatorCommentController extends AbstractRestfulController
{
    const SUCCESS_DELETE = 'Comment successfully deleted';

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var MessageManager
     */
    private $messageManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var DisputeOperatorCommentManager
     */
    private $disputeOperatorCommentManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * DisputeOperatorCommentController constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param DealManager $dealManager
     * @param MessageManager $messageManager
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param DisputeOperatorCommentManager $disputeOperatorCommentManager
     * @param RbacManager $rbacManager
     * @param DisputeManager $disputeManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                DealManager $dealManager,
                                MessageManager $messageManager,
                                BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                DisputeOperatorCommentManager $disputeOperatorCommentManager,
                                RbacManager $rbacManager,
                                DisputeManager $disputeManager)
    {
        $this->entityManager        = $entityManager;
        $this->dealManager          = $dealManager;
        $this->messageManager       = $messageManager;
        $this->baseAuthManager          = $baseAuthManager;
        $this->userManager          = $userManager;
        $this->disputeOperatorCommentManager = $disputeOperatorCommentManager;
        $this->rbacManager          = $rbacManager;
        $this->disputeManager       = $disputeManager;
    }

//    public function getList()
//    {
//        // Не реализован
//    }

    /**
     * @param mixed $id
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function get($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var Message $comment */ // Can throw Exception
            $comment = $this->disputeOperatorCommentManager->getComment($id);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');
        // Check permissions // Important! Only Operator can see any comment
        if (!$isOperator) {

            return $this->redirect()->toRoute('not-authorized');
        }
        // Comment Output
        $commentOutput = $this->disputeOperatorCommentManager->getCommentOutput($comment);

        $data = [
            'comment'  => $commentOutput,
        ];

        // Ajax -
        if ($isAjax) {
            return $this->message()->success("Comment single", $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/deal-comment/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     *
     * Important! Обязательно в data должен быть deal_id
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();
        // Create form
        $form = new DisputeCommentForm();

        try {
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                // Save Form Data
                $resultSavedForm = $this->saveFormData($formData, $formDataInit['dispute']);
                // Comment Output
                $commentOutput = $this->disputeOperatorCommentManager->getCommentOutput($resultSavedForm['comment']);

                return $this->message()->success('Comment created', json_encode($commentOutput));
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // @TODO Когда будут писаться тесты, доделать проверку прав доступа.
        // Check permissions // Important! Only Author can delete his Comment
//        if (!$this->access('message.own.delete', ['user' => $user, 'comment' => $comment])) {
//
//            return $this->redirect()->toRoute('not-authorized');
//        }

        // Create form
        $form = new DealCommentForm();

        try {
            //set data to form
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData['idComment'] = $id;
                // Save Form Data
                $resultSavedForm = $this->saveFormData($formData, $formDataInit['deal']);
                // Comment Output
                $commentOutput = $this->disputeOperatorCommentManager
                    ->getCommentOutput($resultSavedForm['comment']);

                return $this->message()->success('Comment updated', json_encode($commentOutput));
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @return bool|TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Message $comment */ // Can throw Exception
            $comment = $this->disputeOperatorCommentManager->getComment($id);
            /** @var Dispute $dispute */
            $dispute = $this->disputeOperatorCommentManager->getDisputeByComment($comment);

            // Check permissions // Important! Only Author can delete his Comment
            if (!$this->access('message.own.delete', ['user' => $user, 'comment' => $comment])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            // Deletion
            $this->disputeOperatorCommentManager->removeDisputeComment($comment);

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Ajax success
        if ($isAjax) {

            return $this->message()->success(self::SUCCESS_DELETE);
        }
        // Http success
        return $this->redirect()->toRoute('dispute-single', ['id' => $dispute->getId()]);
    }

    /**
     * Form render
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function createFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        /** @var Dispute $dispute */
        $dispute = $formDataInit['dispute'];

        // Create form
        $form = new DisputeCommentForm($dispute->getId());

        try {

            if ( !isset($formDataInit['dispute']) || !$formDataInit['dispute'] instanceof Dispute){

                throw new \Exception('bad data provided: Dispute not found');
            }

            if ( !isset($formDataInit['author']) || !$formDataInit['author'] instanceof User){

                throw new \Exception('bad data provided: Author not found');
            }

            /** @var DealAgent $author */
            $author = $formDataInit['author'];

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $form->setData($postData);
                $formData['dispute_id'] = $dispute->getId();
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['author'] = $author;
                    $resultSavedForm = $this->saveFormData($formData, $dispute);

                    //http success
                    return $this->redirect()->toRoute('dispute-single', ['id' => $dispute->getId()]);
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'disputeCommentForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/dispute-comment/operator-comment-create-form');

        return $view;
    }

    /**
     * Form render (GET)
     *
     * @return TwigViewModel|\Zend\Http\Response
     *
     * Important! Only for Http
     * @throws \Exception
     */
    public function editFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        // Check permissions // Important! Only Author can edit his Comment
        if (!$this->access('message.own.edit', ['user' => $formDataInit['author'], 'comment' => $formDataInit['comment']])) {

            return $this->redirect()->toRoute('not-authorized');
        }

        /** @var Dispute $dispute */
        $dispute = $formDataInit['dispute'];

        $form = new DisputeCommentForm();

        try {
            if (!isset($formDataInit['comment']) || !$formDataInit['comment'] instanceof Message){

                throw new \Exception('bad data provided: Comment not found');
            }
            /** @var Message $comment */
            $comment = $formDataInit['comment'];
            // Comment Output
            $commentOutput = $this->disputeOperatorCommentManager->getCommentOutput($formDataInit['comment']);

            // Set data to form
            $form = $this->disputeOperatorCommentManager->setDisputeCommentFormData($form, $comment, $dispute);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                //set data to form
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idComment'] = $comment->getId();
                    $resultSavedForm = $this->saveFormData($formData, $dispute);

                    //http success
                    return $this->redirect()->toRoute('dispute-single', ['id' => $dispute->getId()]);
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'comment'     => $commentOutput,
            'disputeCommentForm' => $form,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/dispute-comment/operator-comment-edit-form');

        return $view;
    }

    /**
     * Remove Comment (GET)
     *
     * @return TwigViewModel|\Zend\Http\Response
     *
     * Important! Only for Http
     * @throws \Exception
     */
    public function deleteFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        // Check permissions // Important! Only Author can delete his Comment
        if (!$this->access('message.own.delete', ['user' => $formDataInit['author'], 'comment' => $formDataInit['comment']])) {

            return $this->redirect()->toRoute('not-authorized');
        }

        /** @var Dispute $dispute */
        $dispute = $formDataInit['dispute'];

        try {
            if ( !isset($formDataInit['comment']) || !$formDataInit['comment'] instanceof Message){
                throw new \Exception('bad data provided: Comment not found');
            }
            // Comment Output
            $commentOutput = $this->disputeOperatorCommentManager->getCommentOutput($formDataInit['comment']);

            // @TODO Если будет пользователь, то получить dealOutput
            #$dealOutput = $this->dealManager->getDealOutputForSingle($formDataInit['deal']);

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        // Http -
        $view = new TwigViewModel([
            'comment' => $commentOutput,
            'deal_number' => $deal->getNumber(),
            'deal_id' => $deal->getId(),
            'dispute_id' => $dispute->getId()
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/dispute-comment/operator-comment-delete-form');

        return $view;
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // Deal output
        $formDataInit['dispute'] = $this->disputeManager->getDisputeOutput($formDataInit['dispute']);
        // User output
        $formDataInit['author'] = $this->userManager->getUserOutputForSingle($formDataInit['author']);

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array|bool|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        //check
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // process data for update or delete
            $comment = null;
            if (isset($incomingData['idComment'])){
                /** @var Message $comment */
                $comment = $this->disputeOperatorCommentManager->getComment((int) $incomingData['idComment']);
            }
            $dispute = null;
            if ($comment){
                /** @var Dispute $dispute */
                $dispute = $this->disputeOperatorCommentManager->getDisputeByComment($comment);
            } elseif (isset($incomingData['dispute_id'])) {
                /** @var Dispute $dispute */
                $dispute = $this->disputeManager->getDispute((int) $incomingData['dispute_id']);
            } elseif (isset($incomingData['idDispute'])) {
                /** @var Dispute $dispute */
                $dispute = $this->disputeManager->getDispute((int) $incomingData['idDispute']);
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        return [
            'comment'   => $comment,
            'author'    => $user,
            'dispute'   => $dispute
        ];
    }

    /**
     * @param $data
     * @param Dispute $dispute
     * @return array
     * @throws \Exception
     */
    private function saveFormData($data, Dispute $dispute)
    {
        if (isset($data['idComment'])) {
            //update
            if ((int) $data['idComment'] <= 0) {
                throw new \Exception('bad data provided: not found comment id');
            }

            /** @var Message $comment */
            $comment = $this->disputeOperatorCommentManager->getComment((int) $data['idComment']);

            if($comment === null) {
                throw new \Exception('bad data provided: not found comment by id');
            }

            $comment = $this->disputeOperatorCommentManager->updateComment($data, $comment);

            $result = [
                'comment' => $comment
            ];
        } else {
            //create
            try {
                /** @var Message $comment */
                $comment = $this->disputeOperatorCommentManager
                    ->createDisputeComment($data['author'], $data, $dispute);
            }
            catch (\Throwable $t) {

                throw new \Exception($t->getMessage());
            }

            $result = [
                'comment' => $comment
            ];
        }

        return $result;
    }
}