<?php

namespace Application\Controller;

use Application\Entity\Deal;
use Application\Entity\Dispute;
use Application\Entity\TribunalRequest;
use Application\Entity\User;
use Application\Form\RequestConfirmForm;
use Application\Form\RequestRefuseForm;
use Application\Form\TribunalForm;
use Application\Form\TribunalRequestForm;
use Application\Listener\TribunalConfirmSuccessListener;
use Application\Listener\ArbitrageRequestListener;
use Application\Service\Deal\DealManager;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Dispute\TribunalManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;

/**
 * Class TribunalController
 * @package Application\Controller
 */
class TribunalController extends AbstractRestfulController
{
    /**
     * @var TribunalManager
     */
    private $tribunalManager;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var TribunalConfirmSuccessListener
     */
    private $tribunalConfirmSuccessListener;

    /**
     * @var ArbitrageRequestListener
     */
    private $arbitrageRequestListener;

    /**
     * TribunalController constructor.
     * @param TribunalManager $tribunalManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param DealManager $dealManager
     * @param DisputeManager $disputeManager
     * @param TribunalConfirmSuccessListener $tribunalConfirmSuccessListener
     * @param ArbitrageRequestListener $arbitrageRequestListener
     */
    public function __construct(TribunalManager $tribunalManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                DealManager $dealManager,
                                DisputeManager $disputeManager,
                                TribunalConfirmSuccessListener $tribunalConfirmSuccessListener,
                                ArbitrageRequestListener $arbitrageRequestListener)
    {
        $this->tribunalManager = $tribunalManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->dealManager = $dealManager;
        $this->disputeManager = $disputeManager;
        $this->tribunalConfirmSuccessListener = $tribunalConfirmSuccessListener;
        $this->arbitrageRequestListener = $arbitrageRequestListener;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function tribunalAction()
    {
        $disputeId = (int) $this->params()->fromRoute('idDispute', -1);
        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Dispute $dispute */ // Can throw Exception
            $dispute = $this->disputeManager->getDispute($disputeId);
            // Check permissions
            if(!$this->tribunalManager->checkPermissionTribunal($user, $dispute)){
                return $this->redirect()->toRoute('not-authorized');
            }

            //если в tribunalManager вызвать disputeManager то произойдет рекурсия поэтому просто сетим в переменную
            $this->tribunalManager->setDisputeOutput($this->disputeManager->getDisputeOutput($dispute));
            //форма для направления спора в суд!
            $tribunalForm = new TribunalForm($dispute);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            //форма генерации платежки
            if ($postData && $postData['type_form'] === TribunalForm::TYPE_FORM) {
                //set data to form
                $tribunalForm->setData($postData);
                //validate form
                if ($tribunalForm->isValid()) {
                    $this->tribunalManager->createTribunal($dispute);

                    return $this->redirect()->toRoute('dispute-single',['id'=>$dispute->getId()]);
                }
                if($this->getRequest()->isXmlHttpRequest()) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($tribunalForm->getMessages());
                }
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $tribunalForm->prepare();
        $view = new TwigViewModel([
            'tribunalForm' => $tribunalForm
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function tribunalRequestAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try{
            $formDataInit = $this->processFormDataInit();
            /** @var User $user */
            $user = $formDataInit['user'];
            //not operator
            if ($this->tribunalManager->isOperator($user)) {
                return $this->redirect()->toRoute('not-authorized');
            }
            //check data
            if (!isset($formDataInit['deal']) || !$formDataInit['deal'] instanceof Deal) {

                throw new \Exception('bad data provided: Deal not found');
            }
            //check permission
            if (!$this->tribunalManager->checkPermissionTribunalRequest($user, $formDataInit['deal'])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var Deal $deal */
            $deal = $formDataInit['deal'];

            $tribunalRequestForm = new TribunalRequestForm($deal);
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $this->tribunalManager->setDisputeOutput($disputeOutput);
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData)
                && $postData['type_form'] === TribunalRequestForm::TYPE_FORM) {

                //set data to form
                $tribunalRequestForm->setData($postData);
                //validate form
                if ($tribunalRequestForm->isValid()) {
                    #$formData = $refundRequestForm->getData();
                    $author = $this->dealManager->getUserAgentByDeal($user, $deal);

                    $tribunalRequest = $this->tribunalManager->createTribunalRequest($deal, $author);

                    if($isAjax) {
                        $tribunalRequestOutput = $this->tribunalManager
                            ->getTribunalRequestOutput($tribunalRequest);
                        // Return Json with ERROR status
                        return $this->message()->success(
                            'Tribunal request created',
                            ['tribunalRequest' => $tribunalRequestOutput]
                        );
                    }

                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                //ajax response
                if($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($tribunalRequestForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $tribunalRequestForm->prepare();
        $view = new TwigViewModel([
            'tribunalRequestForm' => $tribunalRequestForm,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function tribunalConfirmAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $formDataInit = $this->processFormDataInit();

            //check data
            if (!isset($formDataInit['tribunalRequest']) || !$formDataInit['tribunalRequest'] instanceof TribunalRequest) {

                throw new \Exception('bad data provided: TribunalRequest not found');
            }
            /** @var
             * User $user
             * TribunalRequest $tribunalRequest
             */
            $user = $formDataInit['user'];
            $tribunalRequest = $formDataInit['tribunalRequest'];
            // check permission
            if (!$this->tribunalManager->checkPermissionTribunalConfirm($user, $tribunalRequest)) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var Deal $deal */
            $deal = $tribunalRequest->getDispute()->getDeal();

            //check of confirmation
            if ($tribunalRequest->getConfirm()) {

                throw new \Exception('On the current request already decided');
            }

            $tribunalConfirmForm = new RequestConfirmForm();
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $this->tribunalManager->setDisputeOutput($disputeOutput);
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);
            $tribunalRequestOutput = $this->tribunalManager->getTribunalRequestOutput($tribunalRequest);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData) && $postData['type_form'] === RequestConfirmForm::TYPE_FORM) {
                // set data to form
                $tribunalConfirmForm->setData($postData);
                // validate form
                if ($tribunalConfirmForm->isValid()) {

                    $tribunalConfirm = $this->tribunalManager->createTribunalConfirm($tribunalRequest, $user);
                    //if status success
                    if ( $tribunalConfirm->isStatus() ) {
                        // Subscribe on event "onTribunalConfirmSuccess"
                        $this->tribunalConfirmSuccessListener->subscribe();
                        // Trigger event "onTribunalConfirmSuccess"
                        $this->tribunalConfirmSuccessListener->trigger($tribunalConfirm);
                    }

                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }

                // ajax response
                if($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($tribunalConfirmForm->getMessages());
                }
            }
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $tribunalConfirmForm->prepare();
        $view = new TwigViewModel([
            'tribunalConfirmForm' => $tribunalConfirmForm,
            'tribunalRequest' => $tribunalRequestOutput,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function tribunalRefuseAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $formDataInit = $this->processFormDataInit();

            //check data
            if (!isset($formDataInit['tribunalRequest']) || !$formDataInit['tribunalRequest'] instanceof TribunalRequest) {

                throw new \Exception('bad data provided: TribunalRequest not found');
            }
            /**
             * @var User $user
             * @var TribunalRequest $tribunalRequest
             */
            $user = $formDataInit['user'];
            /** @var TribunalRequest $tribunalRequest */
            $tribunalRequest = $formDataInit['tribunalRequest'];
            // check permission
            if (!$this->tribunalManager->checkPermissionTribunalRefuse($user, $tribunalRequest)) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var Deal $deal */
            $deal = $tribunalRequest->getDispute()->getDeal();

            //check of confirmation
            if ($tribunalRequest->getConfirm()) {

                throw new \Exception('On the current request already decided');
            }

            $tribunalRefuseForm = new RequestRefuseForm();
            $disputeOutput = $this->disputeManager->getDisputeOutput($deal->getDispute());
            $this->tribunalManager->setDisputeOutput($disputeOutput);
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);
            $tribunalRequestOutput = $this->tribunalManager->getTribunalRequestOutput($tribunalRequest);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData && array_key_exists('type_form', $postData) && $postData['type_form'] === RequestRefuseForm::TYPE_FORM) {
                // set data to form
                $tribunalRefuseForm->setData($postData);
                // validate form
                if ($tribunalRefuseForm->isValid()) {

                    $this->tribunalManager->createTribunalRefuse($tribunalRequest, $user);

                    /** @var Dispute $dispute */
                    $dispute = $deal->getDispute();
                    $arbitrageRequests = $dispute->getArbitrageRequests();

                    if ($arbitrageRequests->count() === 0
                        && $this->tribunalManager->checkTribunalRefusesOverlimit($deal->getDispute())) {
                        // Subscribe on event "onRequestDeclineOverlimit"
                        $this->arbitrageRequestListener->subscribe();
                        // Trigger event "onRequestDeclineOverlimit"
                        $this->arbitrageRequestListener->trigger(
                            $deal,
                            $tribunalRequest->getAuthor(),
                            $disputeOutput
                        );
                    }

                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                // ajax response
                if ($isAjax) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($tribunalRefuseForm->getMessages());
                }
            }
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $tribunalRefuseForm->prepare();
        $view = new TwigViewModel([
            'tribunalRefuseForm' => $tribunalRefuseForm,
            'tribunalRequest' => $tribunalRequestOutput,
            'dispute' => $disputeOutput,
            'deal' => $dealOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $allowedUser = null;

            $deal = null;
            if (isset($incomingData['idDeal'])){

                /** @var $deal $deal */
                $deal = $this->dealManager
                    ->getDealById((int) $incomingData['idDeal']);

                // Check if $deal belongs to current user
                if ($user && $deal && !DealManager::isUserPartOfDeal($deal, $user)) {

                    $deal = null;
                }
            }
            $allowedDeals = $this->dealManager->getAllowedDealByUser($user);
            $allowedDeals = !empty($allowedDeals) ? $allowedDeals : null;

            $tribunalRequest = null;
            if (isset($incomingData['idRequest'])){
                /** @var TribunalRequest $tribunalRequest */
                $tribunalRequest = $this->tribunalManager
                    ->getTribunalRequestById((int)$incomingData['idRequest']);
            }

            $allowedTribunalRequests = $this->tribunalManager->getAllowedTribunalRequestsByUser($user);
            $allowedTribunalRequests = !empty($allowedTribunalRequests) ? $allowedTribunalRequests : null;

        } catch (\Throwable $t){

            throw new \Exception($t->getMessage());
        }

        return [
            'user' => $user,
            'allowedUser' => $allowedUser,
            'deal' => $deal,
            'allowedDeals' => $allowedDeals,
            'tribunalRequest' => $tribunalRequest,
            'allowedTribunalRequests' => $allowedTribunalRequests,
        ];
    }
}