<?php

namespace Application\Controller;

use Application\Entity\Deal;
use Application\Entity\SystemPaymentDetails;
use Application\Entity\User;
use Application\Provider\PdfProvider;
use Application\Service\InvoiceManager;
use Application\Service\PaginatorOutputTrait;
use Application\Service\Payment\PaymentPolitics;
use Application\Service\SettingManager;
use Core\Exception\LogicException;
use Core\Service\TwigRenderer;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Application\Service\Deal\DealManager;
use Core\Controller\AbstractRestfulController;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModuleRbac\Service\RbacManager;

/**
 * Class InvoiceController
 * @package Application\Controller
 */
class InvoiceController extends AbstractRestfulController
{
    use PaginatorOutputTrait;

    const DEAL_IS_NOT_APPROVED = 'The deal is not approved yet';

    /**
     * @var InvoiceManager
     */
    private $invoiceManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var dealManager
     */
    private $dealManager;
    /**
     * @var
     */
    private $twigRenderer;
    /**
     * @var PdfProvider
     */
    private $pdfProvider;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * @var array
     */
    private $config;


    /**
     * InvoiceController constructor.
     * @param InvoiceManager $invoiceManager
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param DealManager $dealManager
     * @param TwigRenderer $twigRenderer
     * @param PdfProvider $pdfProvider
     * @param RbacManager $rbacManager
     * @param SettingManager $settingManager
     * @param array $config
     */
    public function __construct(InvoiceManager $invoiceManager,
                                BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                DealManager $dealManager,
                                TwigRenderer $twigRenderer,
                                PdfProvider $pdfProvider,
                                RbacManager $rbacManager,
                                SettingManager $settingManager,
                                array $config)
    {
        $this->invoiceManager = $invoiceManager;
        $this->baseAuthManager    = $baseAuthManager;
        $this->userManager    = $userManager;
        $this->dealManager    = $dealManager;
        $this->twigRenderer   = $twigRenderer;
        $this->pdfProvider    = $pdfProvider;
        $this->rbacManager    = $rbacManager;
        $this->settingManager = $settingManager;
        $this->config         = $config;
    }

    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /**
             * @var User $user
             * @var Deal $deal
             * @var SystemPaymentDetails $paymentDetail
             */
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user yas role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            //get pagination params
            $paginationParams = $this->getPaginationParams($this->config);
            $paymentDetail = $this->settingManager->getSystemPaymentDetail();
            $GPAccountNumber = $paymentDetail->getAccountNumber();
            $deals = $this->invoiceManager->getCollectionConfirmedDealForInvoice($user, $paginationParams, $isOperator);
            // Deal collection for output
            $dealsOutput = $this->dealManager
                ->getDealCollectionForOutput($deals, $user, $GPAccountNumber);
            // Paginator data
            $paginator = $deals->getPages();
            $paginatorOutput = null;
            if ($paginator) {
                $paginatorOutput = $this->getPaginatorOutput($paginator);
            }
        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Ajax - response
        if($isAjax){
            $data   = [
                'deals'         => $dealsOutput,
                'paginator'     => $paginatorOutput,
                'is_operator'   => $isOperator
            ];

            return $this->message()->success('invoice collection', $data);
        }

        // Http - response
        $view = new TwigViewModel([
            'deals'       => $dealsOutput,
            'paginator'     => $paginatorOutput,
            'is_operator' => $isOperator
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/invoice/collection');

        return $view;
    }

    /**
     * @param mixed $id
     * @return bool|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function get($id)
    {
        $work = $this->params()->fromRoute('work', null);

        try {
            /**
             * @var Deal $deal
             * @var User $user
             * @var array $systemPaymentDetailOutput
             */
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user yas role Operator
            $isOperator = $this->rbacManager->hasRole(null, 'Operator');
            $deal = $this->dealManager->getDealById((int) $id);

            if($deal === null) {
                throw new \Exception('bad data provided: not found deal by id');
            }
            // Check permissions // Important! Only Customer can view and download the invoice
            if (!$isOperator && !$this->access('deal.own.customer.invoice', ['user' => $user, 'deal' => $deal])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            // Если сделка еще не одобрена, то - Exception
            if( !$this->dealManager->isDealConfirmedByDealAgents($deal) ) {

                throw new \Exception(self::DEAL_IS_NOT_APPROVED);
            }
            // Проверяем, что сделка может принять платёж через BANK_TRANSFER
            if (false === PaymentPolitics::isPaymentMethodTypeAvailable($deal, PaymentMethodType::BANK_TRANSFER)) {

                throw new LogicException(null, LogicException::PAYMENT_BY_BANK_TRANSFER_CAN_NOT_BE_MADE);
            }
            // Deal output
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal, $user);
            $systemPaymentDetailOutput = $this->settingManager->getSystemPaymentDetailOutput();

            $viewVars = [
                'deal'  => $dealOutput,
                'system_payment_detail' => $systemPaymentDetailOutput,
                'payment_purpose' => $this->invoiceManager->generateInvoicePaymentPurpose($deal),
                'nds_value' => $this->invoiceManager->getNdsValue($deal)
            ];
            $template = 'application/invoice/single';

            $twigRenderer = $this->twigRenderer;
            $twigRenderer->initTwigModel($viewVars);
            $twigRenderer->setTemplate($template);

            $html = $twigRenderer->getHtml();

            $file_name = 'Договор по сделке_«'.$deal->getName().'» '.$deal->getNumber().'_от_'.$deal->getCreated()->format('d.m.Y').'.pdf';
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        // Pdf - response
        if($work === 'download') {
            // Download PFD
            $this->pdfProvider->generateInvoicePdfFromHtml($html, $file_name, 'D');
        } else {
            // Show PFD
            $this->pdfProvider->generateInvoicePdfFromHtml($html, $file_name);
        }

        return;
    }
}