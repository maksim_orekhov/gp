<?php

namespace Application\Controller;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Core\Controller\AbstractRestfulController;
use Core\EventManager\AuthEventProvider;
use Core\Exception\CriticalException;
use Core\Exception\LogicException;
use Core\Service\TwigViewModel;
use ModuleCode\Service\SmsStrategyContext;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use Zend\Math\Rand;
use Zend\View\Model\ViewModel;
use Application\Entity\Test;
use ModuleFileManager\Entity\File;
use Application\Form\FileUploadForm;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Application\Listener\CreditPaymentOrderAvailabilityListener;
use Core\Provider\QrCodeProvider;
use Core\Service\TwigRenderer;


class TestController extends AbstractRestfulController
{
    const FILE_STORING_FOLDER = '/test';

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \ModuleFileManager\Service\FileManager
     */
    private $fileManager;

    /**
     * @var CreditPaymentOrderAvailabilityListener
     */
    private $creditPaymentOrderAvailabilityListener;

    /**
     * @var QrCodeProvider
     */
    private $qrCodeProvider;

    /**
     * @var \Core\Service\TwigRenderer $twigRenderer
     */
    private $twigRenderer;

    /**
     * @var array
     */
    private $config;
    /**
     * @var AuthEventProvider
     */
    private $authEventProvider;

    /**
     * @var MandarinPaymentOrderManager
     */
    private $mandarinPaymentOrderManager;

    /**
     * TestController constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param \ModuleFileManager\Service\FileManager $fileManager
     * @param CreditPaymentOrderAvailabilityListener $creditPaymentOrderAvailabilityListener
     * @param QrCodeProvider $qrCodeProvider
     * @param TwigRenderer $twigRenderer
     * @param AuthEventProvider $authEventProvider
     * @param MandarinPaymentOrderManager $mandarinPaymentOrderManager
     * @param array $config
     */
    public function __construct(\Doctrine\ORM\EntityManager$entityManager,
                                \ModuleFileManager\Service\FileManager $fileManager,
                                CreditPaymentOrderAvailabilityListener $creditPaymentOrderAvailabilityListener,
                                QrCodeProvider $qrCodeProvider,
                                TwigRenderer $twigRenderer,
                                AuthEventProvider $authEventProvider,
                                MandarinPaymentOrderManager $mandarinPaymentOrderManager,
                                array $config)
    {
        $this->entityManager            = $entityManager;
        $this->fileManager              = $fileManager;
        $this->creditPaymentOrderAvailabilityListener = $creditPaymentOrderAvailabilityListener;
        $this->qrCodeProvider           = $qrCodeProvider;
        $this->twigRenderer             = $twigRenderer;
        $this->authEventProvider        = $authEventProvider;
        $this->mandarinPaymentOrderManager = $mandarinPaymentOrderManager;
        $this->config                   = $config;

    }

    /**
     *
     * @throws \Exception
     */
    public function indexAction()
    {
        try {
            throw new \Exception('Ошибка такая сякая');
        }
        catch (\Throwable $t) {

            $this->message()->exception($t);
        }

        return $this->view([]);
    }

//    public function dealMandarinPayedAction()
//    {
//        $deal_id = 18;
//        $mandarinPaymentOrder_id = 2;
//
////        var_dump('deal ' . $deal_id);
////        var_dump('mandarinPaymentOrder ' . $mandarinPaymentOrder_id);
////        die();
//
//        // Получаем нужную сделку и payment
//        /** @var Deal $deal */
//        $deal = $this->entityManager->getRepository(Deal::class)
//            ->findOneBy(['id' => $deal_id]);
//        $payment = $deal->getPayment();
//
//        // получаем нужный PaymentOrder
//        /** @var MandarinPaymentOrder $mandarinPaymentOrder */
//        $mandarinPaymentOrder = $this->entityManager->getRepository(MandarinPaymentOrder::class)
//            ->findOneBy(['id' => $mandarinPaymentOrder_id]);
//
//        $this->mandarinPaymentOrderManager->notifyDealAgentsAboutReceiptMandarinPayment($mandarinPaymentOrder, $payment);
//
//        dump('Ok');
//    }

    /**
     * @throws \Core\Exception\CriticalException
     */
    public function eventAction()
    {
        $deal = $this->entityManager->getRepository(\Application\Entity\Deal::class)
            ->findOneBy(['name' => 'Первая сделка']);

        $this->creditPaymentOrderAvailabilityListener->subscribe();
        $this->creditPaymentOrderAvailabilityListener->trigger($deal);
    }

    /**
     * QR code test
     */
    public function qrcodeAction()
    {
        $qrCodeData = $this->qrCodeProvider->createDataUri('http://alpha.guarantpay.com/', 300);

        return new ViewModel([
            'qrCodeData' => $qrCodeData
        ]);
    }

    /**
     * @return \Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function fileListAction()
    {
        $test = $this->entityManager->getRepository(\Application\Entity\Test::class)
            ->findOneBy(['name' => 'Test One']);

        if(!$test || !$test instanceof Test) {

            throw new \Exception('No test with such name in DB');
        }

        return new ViewModel([
            'owner' => $test
        ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function fileShowAction()
    {
        $accepted_params= $this->params()->fromRoute();

        if ( !isset($accepted_params['file_id']) ) {

            throw new \Exception('Something wrong');
        }

        $fileShowView = $this->forward()->dispatch('ModuleFileManager\Controller\FileController', array(
            'id' => $accepted_params['file_id']
        ));

        // View
        return $fileShowView;
    }

    /**
     * @return \Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function fileUploadAction()
    {
        $accepted_params= $this->params()->fromRoute();

        if ( !isset($accepted_params['owner_id']) ) {

            throw new \Exception('Something wrong');
        }

        $test = $this->entityManager->getRepository(\Application\Entity\Test::class)
            ->findOneById($accepted_params['owner_id']);

        if(!$test || !$test instanceof Test) {

            throw new \Exception('No test with such name in DB');
        }

        $form = new FileUploadForm();

        if ($this->getRequest()->isPost()) {


            $post = array_merge_recursive(
                $this->getRequest()->getPost()->toArray(),
                //$_FILES
                $this->getRequest()->getFiles()->toArray()
            );

            $form->setData($post);

            if ($form->isValid()) {
                // Form is valid, save the form!
                $data = $form->getData();

                if($data['file']['error']) {
                    // Flash message for notify User about failed confirmation
                    $this->flashMessenger()->addMessage($data['file']['error'], FlashMessenger::NAMESPACE_ERROR, 100);
                    // Redirect to page without POST
                    return $this->redirect()->toRoute('test/file');
                }

                // Set of data for pass to FileManager
                $file_source = $data['file']['tmp_name'];
                $file_name = $data['file']['name'];
                $file_type = $data['file']['type'];
                $file_size = $data['file']['size'];

                try {
                    $params = [
                        'file_source' => $file_source,
                        'file_name' => $file_name,
                        'file_type' => $file_type,
                        'file_size' => $file_size,
                        'path' => self::FILE_STORING_FOLDER
                    ];
                    $file = $this->fileManager->write($params);

                    // @TODO Контроллер тестовый.
                    // @TODO В реальном вынести в метод соответствующего ...Manager, который будет выбрасывать исключение в случае неудачи.
                    $test->addFile($file);
                    $this->entityManager->flush();

                    // SUCCESS BLOCK //
                    // If Ajax request, return json
                    if($this->getRequest()->isXmlHttpRequest()){
                        return $this->message()->success('The file was successfully uploaded');
                    }
                    // Redirect to page without POST
                    return $this->redirect()->toRoute('test/files');
                    // END OF SUCCESS BLOCK //

                } catch (\Exception $e) {
                    // Ajax -
                    if($this->getRequest()->isXmlHttpRequest()) {

                        return $this->message()->exception($e);
                    }
                    // Http -
                    return new ViewModel([
                        'server_error_messages' => $e->getMessage(),
                        'form' => $form
                    ]);
                }

            }
            // $form->isValid() failed
            if($this->getRequest()->isXmlHttpRequest()){
                // Return Json with ERROR status
                return $this->message()->invalidFormData($form->getMessages());
            }
            // В Http вернется форма с указанием ошибок валидации
        }
        // Success response if requested by Ajax
        if($this->getRequest()->isXmlHttpRequest()){
            // Return Json with SUCCESS status (in this case also with $data array, which contains csrf token)
            $csrf   = $form->get('csrf')->getValue();
            $data   = ['csrf' => $csrf];
            return $this->message()->success('CSRF token return', $data);
        }
        // Http -
        return new ViewModel([
            'owner' => $test,
            'form'  => $form
        ]);
    }

    /**
     * @return \Zend\Http\Response|JsonModel
     * @throws \Exception
     */
    public function fileDeleteAction()
    {
        $accepted_params= $this->params()->fromRoute();

        if ( !isset($accepted_params['owner_id']) || !isset($accepted_params['file_id']) ) {

            throw new \Exception('Something wrong');
        }

        $test = $this->entityManager->getRepository(\Application\Entity\Test::class)
            ->findOneById($accepted_params['owner_id']);
        $file = $this->entityManager->getRepository(\ModuleFileManager\Entity\File::class)
            ->findOneById($accepted_params['file_id']);

        if($test && $file) {

            try {
                // Try to delete file
                $this->fileDelete($test, $file);

                return $this->message()->success('The file was successfully removed');
            } catch (\Exception $e) {

                return $this->message()->error('Can not delete file');
            }

        } else {
            return $this->message()->error('Something wrong');
        }
    }

    /**
     * Только для тестирования отправки POST через Ajax (код js в file-ajax.phtml)
     * Ссылка - http://dev/test/file-ajax
     * @return ViewModel
     */
    public function fileAjaxAction()
    {
        $form = new FileUploadForm();

        return new ViewModel([
            'form'  => $form
        ]);
    }

    /**
     * для теста верстки писем
     */
    public function mailTemplateAction(){
        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel();
        $twigRenderer->setTemplate('twig/test-email');
        $html = $twigRenderer->getHtml();
        print $html;
        exit;
    }

    /**
     * Для теста error.twig
     *
     * @return TwigViewModel
     */
    public function errorAction()
    {
        return $this->message()->error('Разнообразный и богатый опыт дальнейшее развитие различных форм деятельности в значительной степени обуславливает создание систем массового участия. Разнообразный и богатый опыт постоянный количественный рост и сфера нашей активности обеспечивает широкому кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных задач.');
    }

    /**
     * Для теста success.twig
     *
     * @return TwigViewModel
     */
    public function successAction()
    {
        return $this->message()->success('Таким образом укрепление и развитие структуры позволяет оценить значение системы обучения кадров, соответствует насущным потребностям. Разнообразный и богатый опыт рамки и место обучения кадров требуют определения и уточнения новых предложений.');
    }

    /**
     * Для теста under-construction.twig
     *
     * @return TwigViewModel
     */
    public function underConstructionAction()
    {
        return $this->message()->underConstruction('Задача организации, в особенности же новая модель организационной деятельности обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и административных условий.');
    }

    /**
     * @param Test $owner
     * @param File $file
     * @return void
     */
    private function fileDelete(Test $owner, File $file)
    {
        // Remove File element from collection
        // @TODO В реальном классе код удаления из коллекции можно перенести в менеджер этого класса. Тут не делаю, чтобы не плодить файлов.
        if ($owner->getFiles()->contains($file)) {
            $owner->removeFile($file);

            $this->entityManager->flush();
        }
        // Removing File from DB and disk
        $this->fileManager->delete($file->getId());
    }
}