<?php

namespace Application\Controller;

use Application\Entity\DealAgent;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealOperatorCommentManager;
use Application\Form\DealCommentForm;
use Core\Service\TwigViewModel;
use ModuleMessage\Entity\Message;
use ModuleMessage\Service\MessageManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleRbac\Service\RbacManager;
use Application\Entity\Deal;
use Application\Entity\User;
use Core\Controller\AbstractRestfulController;

/**
 * Class DealCommentController
 * @package Application\Controller
 */
class DealCommentController extends AbstractRestfulController
{
    const SUCCESS_DELETE = "Comment successfully deleted";

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var MessageManager
     */
    private $messageManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var DealOperatorCommentManager
     */
    private $dealOperatorCommentManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var array
     */
    private $config;

    /**
     * DealCommentController constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param DealManager $dealManager
     * @param MessageManager $messageManager
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param DealOperatorCommentManager $dealOperatorCommentManager
     * @param RbacManager $rbacManager
     * @param array $config
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                DealManager $dealManager,
                                MessageManager $messageManager,
                                BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                DealOperatorCommentManager $dealOperatorCommentManager,
                                RbacManager $rbacManager,
                                array $config)
    {
        $this->entityManager        = $entityManager;
        $this->dealManager          = $dealManager;
        $this->messageManager       = $messageManager;
        $this->baseAuthManager          = $baseAuthManager;
        $this->userManager          = $userManager;
        $this->dealOperatorCommentManager = $dealOperatorCommentManager;
        $this->rbacManager          = $rbacManager;
        $this->config               = $config;
    }

    /**
     * @return TwigViewModel
     *
     * @TODO Доделать!
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            // Get naturalPerson collection // Can throw Exception
            $paginationParams = $this->getPaginationParams($this->config);
            // @TODO Метод getAllComments не реализован!
            $allComments = $this->dealOperatorCommentManager->getAllComments($paginationParams);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginator = null;
        if (method_exists($allComments , 'getPages')) {
            $paginator = $allComments->getPages();
        }
        // Get comment Output
        $allCommentsOutput = $this->dealOperatorCommentManager
            ->getCommentCollectionOutput($allComments);

        $data = [
            'paginator'   => $paginator,
            'comments'    => $allCommentsOutput,
        ];

        // Ajax -
        if ($isAjax) {
            return $this->message()->success("Comment collection", json_encode($data));
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate("application/dispute-comment/collection");

        return $view;
    }

    /**
     * @param mixed $id
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function get($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var Message $comment */ // Can throw Exception
            $comment = $this->dealOperatorCommentManager->getComment($id);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');
        // Check permissions // Important! Only Operator can see any comment
        if (!$isOperator) {

            return $this->redirect()->toRoute('not-authorized');
        }
        // Comment Output
        $commentOutput = $this->dealOperatorCommentManager->getCommentOutput($comment);

        $data = [
            'comment'  => $commentOutput,
        ];

        // Ajax -
        if ($isAjax) {
            return $this->message()->success("Comment single", $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/deal-comment/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     *
     * Important! Обязательно в data должен быть deal_id
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();
        // Create form
        $form = new DealCommentForm();

        try {
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                // Save Form Data
                $resultSavedForm = $this->saveFormData($formData, $formDataInit['deal']);
                // Comment Output
                $commentOutput = $this->dealOperatorCommentManager
                    ->getCommentOutput($resultSavedForm['comment']);

                return $this->message()->success('Comment created', json_encode($commentOutput));
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // @TODO Когда будут писаться тесты, доделать проверку прав доступа.
        // Check permissions // Important! Only Author can delete his Comment
//        if (!$this->access('message.own.delete', ['user' => $user, 'comment' => $comment])) {
//
//            return $this->redirect()->toRoute('not-authorized');
//        }

        // Create form
        $form = new DealCommentForm();

        try {
            //set data to form
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData['idComment'] = $id;
                // Save Form Data
                $resultSavedForm = $this->saveFormData($formData, $formDataInit['deal']);
                // Comment Output
                $commentOutput = $this->dealOperatorCommentManager
                    ->getCommentOutput($resultSavedForm['comment']);

                return $this->message()->success('Comment updated', json_encode($commentOutput));
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @return \Zend\Http\Response
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Message $comment */ // Can throw Exception
            $comment = $this->dealOperatorCommentManager->getComment($id);
            /** @var Deal $deal */
            $deal = $this->dealOperatorCommentManager->getDealByComment($comment);

            // Check permissions // Important! Only Author can delete his Comment
            if (!$this->access('message.own.delete', ['user' => $user, 'comment' => $comment])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            // Deletion
            $this->dealOperatorCommentManager->removeDealComment($comment);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        // Ajax success
        if ($isAjax) {

            return $this->message()->success(self::SUCCESS_DELETE);
        }
        // Http success
        return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
    }

    /**
     * Form render
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function createFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        /** @var Deal $deal */
        $deal = $formDataInit['deal'];

        // Create form
        $form = new DealCommentForm($deal->getId());

        try {

            if ( !isset($formDataInit['deal']) || !$formDataInit['deal'] instanceof Deal){

                throw new \Exception('bad data provided: Deal not found');
            }

            if ( !isset($formDataInit['author']) || !$formDataInit['author'] instanceof User){

                throw new \Exception('bad data provided: Author not found');
            }

            /** @var DealAgent $author */
            $author = $formDataInit['author'];

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['author'] = $author;
                    $resultSavedForm = $this->saveFormData($formData, $deal);

                    //http success
                    return $this->redirect()->toRoute('deal/show', ['id' => $postData['deal_id']]);
                }
            }

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        $form->prepare();
        $view = new TwigViewModel([
            'dealCommentForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/deal-comment/create-form');

        return $view;
    }

    /**
     * Form render (GET)
     *
     * @return TwigViewModel|\Zend\Http\Response
     *
     * Important! Only for Http
     */
    public function editFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        // Check permissions // Important! Only Author can edit his Comment
        if (!$this->access('message.own.edit', ['user' => $formDataInit['author'], 'comment' => $formDataInit['comment']])) {

            return $this->redirect()->toRoute('not-authorized');
        }

        /** @var Deal $deal */
        $deal = $formDataInit['deal'];

        $form = new DealCommentForm();

        try {
            if (!isset($formDataInit['comment']) || !$formDataInit['comment'] instanceof Message){

                throw new \Exception('bad data provided: Comment not found');
            }
            /** @var Message $comment */
            $comment = $formDataInit['comment'];
            // Comment Output
            $commentOutput = $this->dealOperatorCommentManager->getCommentOutput($formDataInit['comment']);

            // Set data to form
            $form = $this->dealOperatorCommentManager->setDealCommentFormData($form, $comment, $formDataInit['deal']);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                //set data to form
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idComment'] = $comment->getId();
                    $resultSavedForm = $this->saveFormData($formData, $deal);

                    //http success
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
            }

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        $form->prepare();
        $view = new TwigViewModel([
            'comment'     => $commentOutput,
            'dealCommentForm' => $form,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/deal-comment/edit-form');

        return $view;
    }

    /**
     * Remove Comment (GET)
     *
     * @return TwigViewModel|\Zend\Http\Response
     *
     * Important! Only for Http
     */
    public function deleteFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $formDataInit = $this->processFormDataInit();

        // Check permissions // Important! Only Author can delete his Comment
        if (!$this->access('message.own.delete', ['user' => $formDataInit['author'], 'comment' => $formDataInit['comment']])) {

            return $this->redirect()->toRoute('not-authorized');
        }

        /** @var Deal $deal */
        $deal = $formDataInit['deal'];

        try {
            if ( !isset($formDataInit['comment']) || !$formDataInit['comment'] instanceof Message){
                throw new \Exception('bad data provided: Comment not found');
            }
            // Comment Output
            $commentOutput = $this->dealOperatorCommentManager->getCommentOutput($formDataInit['comment']);

            // @TODO Если будет пользователь, то получить dealOutput
            #$dealOutput = $this->dealManager->getDealOutputForSingle($formDataInit['deal']);

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }
        // Http -
        $view = new TwigViewModel([
            'comment' => $commentOutput,
            'deal_number' => $deal->getNumber(),
            'deal_id' => $deal->getId()
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return array
     */
    public function formInitAction()
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // Deal output
        $formDataInit['deal'] = $this->dealManager->getDealOutputForSingle($formDataInit['deal'], $formDataInit['author']);
        // User output
        $formDataInit['author'] = $this->userManager->getUserOutputForSingle($formDataInit['author']);

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     * @return array
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        //check
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // process data for update or delete
            $comment = null;
            if (isset($incomingData['idComment'])){
                /** @var Message $comment */
                $comment = $this->dealOperatorCommentManager->getComment((int) $incomingData['idComment']);
            }
            $deal = null;
            if ($comment){
                /** @var Deal $deal */
                $deal = $this->dealOperatorCommentManager->getDealByComment($comment);
            } elseif ($incomingData['deal_id']) {
                /** @var Deal $deal */
                $deal = $this->dealManager->getDealById((int) $incomingData['deal_id']);
            }

        } catch (\Throwable $t){

            return $this->message()->error($t->getMessage());
        }

        return [
            'comment'   => $comment,
            'author'    => $user,
            'deal'      => $deal
        ];
    }

    /**
     * @param $data
     * @param Deal $deal
     * @return array
     * @throws \Exception
     */
    private function saveFormData($data, Deal $deal)
    {
        if (isset($data['idComment'])) {
            //update
            if ((int) $data['idComment'] <= 0) {
                throw new \Exception('bad data provided: not found comment id');
            }

            /** @var Message $comment */
            $comment = $this->dealOperatorCommentManager->getComment((int) $data['idComment']);

            if($comment === null) {
                throw new \Exception('bad data provided: not found comment by id');
            }

            $comment = $this->dealOperatorCommentManager->updateComment($data, $comment);

            $result = [
                'comment' => $comment
            ];
        } else {
            //create
            try {
                /** @var Message $comment */
                $comment = $this->dealOperatorCommentManager->createDealComment($data['author'], $data, $deal);
            }
            catch (\Throwable $t) {

                throw new \Exception($t->getMessage());
            }

            $result = [
                'comment' => $comment
            ];
        }

        return $result;
    }
}