<?php
namespace Application\Controller;

use Application\Entity\DealAgent;
use Application\Form\DealUnregisteredForm;
use Application\Listener\DealConditionsListener;
use Application\Service\UserManager;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseAuthManager;
use Core\Service\ORMDoctrineUtil;
use Doctrine\ORM\EntityManager;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use ModuleDelivery\Service\DeliveryOrderManager;
use ModuleDelivery\Service\DeliveryTrackingNumberManager;
use ModuleDeliveryDpd\Entity\DpdDeliveryOrder;
use Application\Form\DatePickupForm;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdManager;
use Zend\Validator\Csrf as CsrfValidator;
use Application\Form\ArbitrageRequestForm;
use Application\Form\DealPartnerForm;
use Application\Form\DeliveryConfirmForm;
use Application\Form\DiscountRequestForm;
use Application\Form\PayVariantForm;
use Application\Form\RefundRequestForm;
use Application\Form\RepaidOverpayForm;
use Application\Form\RequestConfirmForm;
use Application\Form\RequestRefuseForm;
use Application\Form\TribunalRequestForm;
use Application\Service\Deal\DealPolitics;
use Application\Service\Deal\DealTokenManager;
use Application\Service\Dispute\DisputeHistoryManager;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Dispute\DisputeStatus;
use Application\Service\PaginatorOutputTrait;
use Application\Service\Payment\CalculatedDataProvider;
use Core\Exception\LogicException;
use Application\Form\CivilLawSubjectSelectionForm;
use Application\Form\DealCommentForm;
use Application\Form\DealAbnormalForm;
use Application\Form\DealFileUploadForm;
use Application\Form\DisputeForm;
use Application\Form\ExpiredSearchForm;
use Application\Form\LegalEntityForm;
use Application\Form\NaturalPersonForm;
use Application\Form\PaymentMethod\BankTransferForm;
use Application\Form\PaymentMethodSelectionForm;
use Application\Form\TrackingNumberForm;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Application\Util\DataFilter;
use Core\Service\TwigViewModel;
use Application\Service\Deal\DealManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Deal\DealAgentManager;
use Application\Form\DealConditionsConfirmForm;
use Application\Form\DealForm;
use Application\Entity\Deal;
use Application\Entity\User;
use Application\Entity\Payment;
use Core\Controller\AbstractRestfulController;
use ModuleRbac\Service\RbacManager;
use Zend\Mvc\MvcEvent;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;
use Zend\Json\Json;
use Application\Form\DealSearchForm;
use Application\Service\Payment\PaymentPolitics;
use Application\Entity\CivilLawSubject;
use Application\Service\Deal\DealOperatorCommentManager;
use Application\Service\Deal\DealStatus;
use Application\EventManager\DealEventProvider as DealEvent;
use Zend\EventManager\EventManager;

/**
 * Class DealController
 * @package Application\Controller
 */
class DealController extends AbstractRestfulController
{
    use PaginatorOutputTrait;

    const MAX_DEAL_AMOUNT = 1000000000; // Миллиард

    const DEAL_CONTRACTS_STORING_FOLDER = '/deal-contract';
    const DEAL_FILES_STORING_FOLDER = '/deal-files';

    const SUCCESS_DELETE = 'Deal successfully deleted';
    const SUCCESS_CONFIRM = 'Deal successfully confirmed';
    const CAN_NOT_BE_DELETED = 'The deal can not be deleted anymore';
    const DEAL_FILES = 'deal_files';

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var dealManager
     */
    private $dealManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var dealAgentManager
     */
    private $dealAgentManager;

    /**
     * Auth manager.
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * User manager.
     * @var UserManager
     */
    private $userManager;

    /**
     * @var DealConditionsListener
     */
    private $dealConditionsListener;

    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * @var array
     */
    private $config;

    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics; // @TODO Используется всего два раза. Подумать, как убрать.

    /**
     * @var DeliveryTrackingNumberManager
     */
    private $deliveryTrackingNumberManager; // @TODO Используется всего один раз. Подумать, как убрать.

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager; // @TODO Используется всего один раз. Подумать, как убрать.

    /**
     * @var
     */
    private $ndsTypeManager; // @TODO Используется всего один раз. Подумать, как убрать.

    /**
     * @var DealOperatorCommentManager
     */
    private $dealOperatorCommentManager; // @TODO Используется всего один раз. Подумать, как убрать.

    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider; // @TODO Используется всего один раз. Подумать, как убрать.

    /**
     * @var DealStatus
     */
    private $dealStatus; // @TODO Используется всего один раз. Подумать, как убрать.

    /**
     * @var DiscountManager
     */
    private $discountManager; // @TODO Используется всего один раз. Подумать, как убрать.

    /**
     * @var DisputeHistoryManager
     */
    private $disputeHistoryManager; // @TODO Используется всего один раз. Подумать, как убрать.

    /**
     * @var DealTokenManager
     */
    private $dealTokenManager;

    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var DeliveryOrderManager
     */
    private $deliveryOrderManager;
    /**
     * @var DeliveryOrderDpdManager
     */
    private $deliveryOrderDpdManager;
    /**
     * @var EventManager
     */
    private $deliveryEventManager;

    /**
     * DealController constructor.
     * @param EntityManager $entityManager
     * @param DealManager $dealManager
     * @param PaymentManager $paymentManager
     * @param DealAgentManager $dealAgentManager
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param DealConditionsListener $dealConditionsListener
     * @param DealPolitics $dealPolitics
     * @param PaymentPolitics $paymentPolitics
     * @param DeliveryTrackingNumberManager $deliveryTrackingNumberManager
     * @param RbacManager $rbacManager
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param NdsTypeManager $ndsTypeManager
     * @param DealOperatorCommentManager $dealOperatorCommentManager
     * @param CalculatedDataProvider $calculatedDataProvider
     * @param DealStatus $dealStatus
     * @param DiscountManager $discountManager
     * @param DisputeHistoryManager $disputeHistoryManager
     * @param DealTokenManager $dealTokenManager
     * @param EventManager $eventManager
     * @param DeliveryOrderManager $deliveryOrderManager
     * @param DeliveryOrderDpdManager $deliveryOrderDpdManager
     * @param EventManager $deliveryEventManager
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                DealManager $dealManager,
                                PaymentManager $paymentManager,
                                DealAgentManager $dealAgentManager,
                                BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                DealConditionsListener $dealConditionsListener,
                                DealPolitics $dealPolitics,
                                PaymentPolitics $paymentPolitics,
                                DeliveryTrackingNumberManager $deliveryTrackingNumberManager,
                                RbacManager $rbacManager,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                PaymentMethodManager $paymentMethodManager,
                                NdsTypeManager $ndsTypeManager,
                                DealOperatorCommentManager $dealOperatorCommentManager,
                                CalculatedDataProvider $calculatedDataProvider,
                                DealStatus $dealStatus,
                                DiscountManager $discountManager,
                                DisputeHistoryManager $disputeHistoryManager,
                                DealTokenManager $dealTokenManager,
                                EventManager $eventManager,
                                DeliveryOrderManager $deliveryOrderManager,
                                DeliveryOrderDpdManager $deliveryOrderDpdManager,
                                EventManager $deliveryEventManager,
                                array $config)
    {
        $this->entityManager = $entityManager;
        $this->dealManager = $dealManager;
        $this->paymentManager = $paymentManager;
        $this->dealAgentManager = $dealAgentManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->userManager = $userManager;
        $this->dealConditionsListener = $dealConditionsListener;
        $this->dealPolitics = $dealPolitics;
        $this->paymentPolitics = $paymentPolitics;
        $this->deliveryTrackingNumberManager = $deliveryTrackingNumberManager;
        $this->rbacManager = $rbacManager;
        $this->civilLawSubjectManager = $civilLawSubjectManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->ndsTypeManager = $ndsTypeManager;
        $this->dealOperatorCommentManager = $dealOperatorCommentManager;
        $this->calculatedDataProvider = $calculatedDataProvider;
        $this->dealStatus = $dealStatus;
        $this->discountManager = $discountManager;
        $this->disputeHistoryManager = $disputeHistoryManager;
        $this->dealTokenManager = $dealTokenManager;
        $this->eventManager = $eventManager;
        $this->deliveryOrderManager = $deliveryOrderManager;
        $this->config = $config;
        $this->deliveryOrderDpdManager = $deliveryOrderDpdManager;
        $this->deliveryEventManager = $deliveryEventManager;
    }

    /**
     * @return TwigViewModel
     * @throws \Exception
     */
    public function indexAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Data
        $data = $this->collectIncomingData();
        $paramsFilterAndSort = $this->getPaginationParams($this->config);
        $paramsFilterAndSort['unanswered'] = $data['unanswered'] ?? null;

        try {
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');
            /** @var DealSearchForm $dealFilterForm */
            $dealFilterForm = new DealSearchForm($this->dealManager, $isOperator);

            if (isset($data['filter'])) {
                /** @var DataFilter $dataFilter */
                $dataFilter = new DataFilter();

                $data['filter']['csrf'] = $data['csrf'];

                // Set action
                $dealFilterForm->setAttribute('action', $this->url()->fromRoute('deal'));

                $filterResult = $dataFilter->filterData($dealFilterForm, $data['filter']);

                if ($filterResult['filter']) {
                    $paramsFilterAndSort['filter'] = $filterResult['filter'];
                } else {
                    $dealFilterForm = $filterResult['form'];
                }
            }

            // Get all users's deals with sort and search params
            /** @var Paginator $deals */
            $deals = $this->dealManager->getDealCollectionByUser($user, $paramsFilterAndSort, $isOperator);
            // Deal collection for output
            $dealsOutput = $this->dealManager->getDealCollectionForOutput($deals, $user);

            // Paginator data
            $paginator = $deals->getPages();
        }
        catch (\Throwable $t) {
            logException($t, 'deal-errors', 3);
            return $this->message()->exception($t);
        }

        $paginatorOutput = null;
        if ($paginator) {
            $paginatorOutput = $this->getPaginatorOutput($paginator);
        }

        // Ajax - csrf token
        if ($isAjax) {
            $csrf = $dealFilterForm->get('csrf')->getValue();
            $data = [
                'csrf' => $csrf,
                'deals' => $dealsOutput,
                'paginator' => $paginatorOutput,
                'is_operator' => $isOperator
            ];

            return $this->message()->success('CSRF token return', $data);
        }

        // Html -
        $view = new TwigViewModel([
            'dealFilterForm' => $dealFilterForm,
            'deals' => $dealsOutput,
            'paginator' => $paginatorOutput,
            'is_operator' => $isOperator,
            'is_verified' => $this->rbacManager->hasRole(null, 'Verified'),
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function showAction()
    {
        $deal_id = (int)$this->params()->fromRoute('id');
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        // Try get Deal and User
        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');
            /** @var Deal $deal */ // Can throe Exception
            $deal = $this->dealManager->getDealById($deal_id);
            // Check permissions
            // Important! Only Customer, Contractor or Operator can view the Deal
            if (!$isOperator && !$this->access('deal.own.view', ['user' => $user, 'deal' => $deal])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            $disputeHistory = $this->disputeHistoryManager->getDisputeHistory($deal->getDispute(), $user);
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal, $user);

            if ($isAjax) {
                $data = [
                    'deal' => $dealOutput
                ];
                return $this->message()->success('Deal single', $data);
            }
        } catch (\Throwable $t) {
            logException($t, 'deal-errors', 3);
            return $this->message()->exception($t);
        }

        $datePickupForm = new DatePickupForm();
        $dealFileUploadForm = new DealFileUploadForm($deal->getId());
        $trackingNumberForm = new TrackingNumberForm();
        $deliveryConfirmForm = new DeliveryConfirmForm();
        $deliveryConfirmForm->get('idDeal')->setValue($deal->getId());

        $requestConfirmForm = new RequestConfirmForm();
        $requestRefuseForm = new RequestRefuseForm();

        $tribunalRequestForm = new TribunalRequestForm($deal);
        $refundRequestForm = new RefundRequestForm($deal);
        $discountRequestForm = new DiscountRequestForm($deal, $this->discountManager->getMaxDiscount($deal));
        $arbitrageRequestForm = new ArbitrageRequestForm($deal);

        $datePickupForm->prepare();
        $trackingNumberForm->prepare();
        $deliveryConfirmForm->prepare();
        $dealFileUploadForm->prepare();
        $requestConfirmForm->prepare();
        $requestRefuseForm->prepare();
        $tribunalRequestForm->prepare();
        $refundRequestForm->prepare();
        $discountRequestForm->prepare();
        // Массив комментариев оператов к сделке
        $dealOperatorsCommentsOutput = $this->dealOperatorCommentManager
            ->getDealOperatorsCommentsOutput($deal, $user);

        $dealCommentForm = null;
        if ($isOperator) {
            $dealCommentForm = new DealCommentForm($deal->getId());
            $dealCommentForm->prepare();
        }

        $payVariantForm = null;
        if (!$isOperator) { // @TODO Если кастомер и если подходящий статус.
            $payVariantForm = new PayVariantForm($deal->getId()); // @TODO Проверить используется ли на фронте.
        }

        $view = new TwigViewModel([
            'deal' => $dealOutput,
            'is_operator' => $isOperator,
            'dealFileUploadForm' => $dealFileUploadForm,
            'deal_history' =>$dealOutput['deal_history'],
            'trackingNumberForm' => $trackingNumberForm,
            'deliveryConfirmForm' => $deliveryConfirmForm,
            'requestConfirmForm' => $requestConfirmForm,
            'requestRefuseForm' => $requestRefuseForm,
            'tribunalRequestForm' => $tribunalRequestForm,
            'refundRequestForm' => $refundRequestForm,
            'discountRequestForm' => $discountRequestForm,
            'arbitrageRequestForm' => $arbitrageRequestForm,
            'dealCommentForm' => $dealCommentForm,
            'payVariantForm' => $payVariantForm,
            'deal_operators_comments' => $dealOperatorsCommentsOutput,
            'dispute_history' => $disputeHistory,
            'datePickupForm' => $datePickupForm
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function createCivilLawSubjectAction()
    {
        $deal_id = (int)$this->params()->fromRoute('idDeal');

        try {
            $this->entityManager->getConnection()->beginTransaction();
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');
            /** @var Deal $deal */ // Can throe Exception
            $deal = $this->dealManager->getDealById($deal_id);
            // Check permissions

            // Important! Only Customer, Contractor or Operator can view the Deal
            if (!$this->access('deal.own.view', ['user' => $user, 'deal' => $deal]) && !$isOperator) {
                return $this->redirect()->toRoute('not-authorized');
            }

            // Получаем collection $civilLawSubjects
            $availableCivilLawSubjects = $this->civilLawSubjectManager->getAvailableCivilLawSubjects($user);

            $naturalPersonForm = new NaturalPersonForm();
            $ndsTypes = $this->ndsTypeManager->getActiveNdsTypes();
            $legalEntityForm = new LegalEntityForm($ndsTypes);
            $allowedCivilLawSubjectForm = new CivilLawSubjectSelectionForm($availableCivilLawSubjects);
            $allowedCivilLawSubjectForm->get('civil_law_subject_id')->setAttribute('name', 'civil_law_subject');
            $is_form_natural_person = false;
            $is_form_legal_entity = false;

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData) {

                //подготовка данных
                $preparedData = $this->dealManager->createOrGetCivilLawSubject($postData, $deal, $user);
                //// delivery ////
                $deliveryData = $this->dealManager->prepareDeliveryToDeal($postData);
                $deliveryData = $this->dealManager->getValidationDelivery($deliveryData);
                $preparedData = array_merge_recursive($preparedData, $deliveryData);

                if ($preparedData['validation']['is_valid']) {

                    $doctrineEventManager = $this->entityManager->getEventManager();
                    $doctrineEventManager
                        ->addEventListener(
                            array(\Doctrine\ORM\Events::onFlush, \Doctrine\ORM\Events::postFlush),
                            $this->dealConditionsListener
                        );

                    // присечивание
                    $this->dealManager->setCivilLawSubjectToDeal($deal, $user, $preparedData);

                    //// delivery ////
                    if (isset($preparedData['post_data']['delivery'])) {
                        $this->createOrEditDelivery($deal, $preparedData['post_data']['delivery']);
                    }

                    // DB Transaction commit
                    $this->entityManager->getConnection()->commit();

                    $data = [
                        'deal' => $this->dealManager->getDealOutputForSingle($deal),
                        'redirectUrl' => '/deal/show/' . $deal->getId()
                    ];

                    return $this->message()->success('Deal Updated', $data);
                }

                ///// invalidFormData /////
                $invalidFormData = $this->dealManager->getProcessingInvalidFormDataForCivilLawSubjectOrPaymentMethod($preparedData['validation']);

                return $this->message()->invalidFormData($invalidFormData);
            }

            $dealOutput = $this->dealManager->getDealOutputForSingle($deal, $user);
        }
        catch (\Throwable $t) {
            logException($t, 'deal-errors', 3);
            return $this->message()->exception($t);
        }

        $naturalPersonForm->prepare();
        $legalEntityForm->prepare();
        $view = new TwigViewModel([
            'deal' => $dealOutput,
            'naturalPersonForm' => $naturalPersonForm,
            'legalEntityForm' => $legalEntityForm,
            'allowedCivilLawSubjectForm' => $allowedCivilLawSubjectForm,
            'is_form_natural_person' => $is_form_natural_person,
            'is_form_legal_entity' => $is_form_legal_entity,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function createPaymentMethodAction()
    {
        $deal_id = (int)$this->params()->fromRoute('idDeal');

        try {
            $this->entityManager->getConnection()->beginTransaction();
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');

            /** @var Deal $deal */ // Can throe Exception
            $deal = $this->dealManager->getDealById($deal_id);

            // Check permissions
            // Important! Only Customer, Contractor or Operator can view the Deal
            if (!$this->access('deal.own.view', ['user' => $user, 'deal' => $deal]) && !$isOperator) {
                return $this->redirect()->toRoute('not-authorized');
            }

            // Получаем collection PaymentMethods
            $availablePaymentMethods = $this->paymentMethodManager->getAvailablePaymentMethods($user);
            /** @var DealAgent $userDealAgent */
            $userDealAgent = $this->dealManager->getUserAgentByDeal($user, $deal);
            /** @var CivilLawSubject $civilLawSubject */
            $civilLawSubject = $userDealAgent->getCivilLawSubject();
            if ($civilLawSubject === null) {
                return $this->message()->error('В сделке не заполнена информация о субъекте права');
            }

            $allowedPaymentMethodForm = new PaymentMethodSelectionForm($availablePaymentMethods);
            $allowedPaymentMethodForm->get('payment_method_id')->setAttribute('name', 'payment_method');
            $bankTransferForm = new BankTransferForm();
            $is_form_bank_transfer = true;
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {

                //подготовка данных
                $preparedData = $this->dealManager->createOrGetPaymentMethod($postData, $deal, $user);
                //// delivery ////
                $deliveryData = $this->dealManager->prepareDeliveryToDeal($postData);
                $deliveryData = $this->dealManager->getValidationDelivery($deliveryData);
                $preparedData = array_merge_recursive($preparedData, $deliveryData);

                if ($preparedData['validation']['is_valid']) {
                    $doctrineEventManager = $this->entityManager->getEventManager();
                    $doctrineEventManager
                        ->addEventListener(
                            array(\Doctrine\ORM\Events::onFlush, \Doctrine\ORM\Events::postFlush),
                            $this->dealConditionsListener
                        );
                    // присечивание
                    $this->dealManager->setPaymentMethodToDeal($deal, $user, $preparedData);

                    //// delivery ////
                    if (isset($preparedData['post_data']['delivery'])) {
                        $this->createOrEditDelivery($deal, $preparedData['post_data']['delivery']);
                    }

                    // DB Transaction commit
                    $this->entityManager->getConnection()->commit();

                    $data = [
                        'deal' => $this->dealManager->getDealOutputForSingle($deal),
                        'redirectUrl' => '/deal/show/' . $deal->getId()
                    ];

                    return $this->message()->success('Deal Updated', $data);
                }

                ///// invalidFormData /////
                $invalidFormData = $this->dealManager->getProcessingInvalidFormDataForCivilLawSubjectOrPaymentMethod($preparedData['validation']);
                return $this->message()->invalidFormData($invalidFormData);

            }
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal, $user);
        }
        catch (\Throwable $t) {
            // DB Transaction rollback
            ORMDoctrineUtil::rollBackTransaction($this->entityManager);

            logException($t, 'deal-errors', 3);
            return $this->message()->exception($t);
        }
        $bankTransferForm->prepare();
        $view = new TwigViewModel([
            'deal' => $dealOutput,
            'allowedPaymentMethodForm' => $allowedPaymentMethodForm,
            'bankTransferForm' => $bankTransferForm,
            'is_form_bank_transfer' => $is_form_bank_transfer,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        return $view;
    }

    /**
     * @return array|bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function civilLawSubjectPaymentMethodAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }
        try {
            // DB Transaction start
            $this->entityManager->getConnection()->beginTransaction();

            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $deal_id = (int)$this->params()->fromRoute('id', 0);
            //  $form = new DealForm($this->dealManager, $user);
            $deal = $this->dealManager->getDealById($deal_id);
            // Check permissions // Important! Only Customer or Contractor can edit the Deal
            //@TODO везде поменять
            if (!$this->access('deal.own.edit', ['user' => $user, 'deal' => $deal])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData) {
                //подготовка данных
                $preparedData = $this->dealManager->createOrGetCivilLawSubjectOrPaymentMethod($postData, $deal, $user);
                //// delivery ////
                $deliveryData = $this->dealManager->prepareDeliveryToDeal($postData);
                $deliveryData = $this->dealManager->getValidationDelivery($deliveryData);
                $preparedData = array_merge_recursive($preparedData, $deliveryData);

                if ($preparedData['validation']['is_valid']) {
                    $doctrineEventManager = $this->entityManager->getEventManager();
                    $doctrineEventManager
                        ->addEventListener(
                            array(\Doctrine\ORM\Events::onFlush, \Doctrine\ORM\Events::postFlush),
                            $this->dealConditionsListener
                        );
                    // присечивание
                    $this->dealManager->setCivilLawSubjectAndPaymentMethodToDeal($deal, $user, $preparedData);
                    //// delivery ////
                    if (isset($preparedData['post_data']['delivery'])) {
                        $this->createOrEditDelivery($deal, $preparedData['post_data']['delivery']);
                    }

                    // DB Transaction commit
                    $this->entityManager->getConnection()->commit();

                    $data = [
                        'deal' => $this->dealManager->getDealOutputForSingle($deal),
                        'redirectUrl' => '/deal/show/' . $deal->getId()
                    ];

                    return $this->message()->success('Deal Updated', $data);
                }

                ///// invalidFormData /////
                $invalidFormData = $this->dealManager->getProcessingInvalidFormDataForCivilLawSubjectOrPaymentMethod($preparedData['validation']);

                return $this->message()->invalidFormData($invalidFormData);
            }
        }
        catch (\Throwable $t) {
            // DB Transaction rollback
            ORMDoctrineUtil::rollBackTransaction($this->entityManager);

            logException($t, 'deal-errors', 3);
            return $this->message()->exception($t);
        }

        return false;
    }

    /**
     * @return array|bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function createDeliveryAction()
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }
        //return only json
        $this->message()->setJsonStrategy(true);
        $deal_id = (int)$this->params()->fromRoute('idDeal');

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');
            /** @var Deal $deal */ // Can throe Exception
            $deal = $this->dealManager->getDealById($deal_id);
            // Check permissions
            // Important! Only Customer, Contractor or Operator can view the Deal
            if (!$isOperator && !$this->access('deal.own.view', ['user' => $user, 'deal' => $deal])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData) {
                $preparedData = $this->dealManager->prepareDeliveryToDeal($postData);
                $preparedData = $this->dealManager->getValidationDelivery($preparedData);
                if ($preparedData['validation']['is_valid']) {
                    $doctrineEventManager = $this->entityManager->getEventManager();
                    $doctrineEventManager
                        ->addEventListener(
                            array(\Doctrine\ORM\Events::onFlush, \Doctrine\ORM\Events::postFlush),
                            $this->dealConditionsListener
                        );
                    //// delivery ////
                    $this->createOrEditDelivery($deal, $preparedData['post_data']['delivery']);

                    ///// SUCCESS /////
                    return $this->message()->success('Deal delivery registered', [
                        'deal' => $this->dealManager->getDealOutputForSingle($deal)
                    ]);
                }

                return $this->message()->invalidFormData($preparedData['validation']);
            }

            return $this->message()->error('No delivery registered');
        }
        catch (\Throwable $t) {
            logException($t, 'deal-errors', 3);

            return $this->message()->exception($t);
        }
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function createTrackingNumberAction()
    {
        $deal_id = (int)$this->params()->fromRoute('idDeal');

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            $isOperator = $this->rbacManager->hasRole($user, 'Operator');
            /** @var Deal $deal */ // Can throe Exception
            $deal = $this->dealManager->getDealById($deal_id);
            /** @var DeliveryOrder $deliveryOrder */
            $deliveryOrder = $deal->getDeliveryOrder();
            // Check permissions
            if ($deliveryOrder === null
                && !$this->access('deal.own.contractor', ['user' => $user, 'deal' => $deal])
                && !$isOperator
                && $deal->status['code'] !== 3
            ) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $trackingNumberForm = new TrackingNumberForm();
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal, $user);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $trackingNumberForm->setData($postData);
                if ($trackingNumberForm->isValid()) {
                    $formData = $trackingNumberForm->getData();
                    $this->deliveryTrackingNumberManager->createCustomDeliveryTrackingNumber($deliveryOrder, $formData['tracking_number']);

                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
            }
        }
        catch (\Throwable $t) {
            logException($t, 'deal-errors', 3);
            return $this->message()->exception($t);
        }

        $trackingNumberForm->prepare();
        $view = new TwigViewModel([
            'deal' => $dealOutput,
            'trackingNumberForm' => $trackingNumberForm,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function createAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var MvcEvent $mvcEvent */
            $mvcEvent = $this->getEvent();
            $deal_data = $mvcEvent->getParam('deal_data', null);
            $token = $mvcEvent->getParam('token', null);

            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                /** @var User $user */ // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }

            // Проверка прав доступа
            // Если нет $deal_data и если у пользователя нет роли Verified
            if (null === $deal_data && false === $this->rbacManager->hasRole($user, 'Verified')) {
                if ($isAjax){

                    return $this->message()->error(null, ['redirectUrl' => '/deal/create'], BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED);
                }

                // Redirect the user to the "Login" page.
                return $this->redirect()->toRoute('login', [], ['query'=>['redirectUrl' => '/deal/create']]);
            }

            // DB Transaction start
            $this->entityManager->getConnection()->beginTransaction();

            $form = new DealForm($this->dealManager, $user);

            $postData = $deal_data ?: $this->collectIncomingData()['postData'];

            if ($postData) {
                $filesData = $this->collectIncomingData()['filesData'];
                if (\count($filesData)) {
                    foreach ($filesData['files'] as $file) {
                        if ($file['error'] === 4) { $filesData = null; break; }
                    }
                }
                if ($filesData) {
                    $postData = array_merge_recursive($postData, $filesData);
                }
                //подготавливаем массив с разделенными данными и параметрами валидации
                $preparedData = $this->dealManager->prepareDataToDeal($postData);
                ///// step 1: валидация Delivery /////
                $preparedData = $this->dealManager->getValidationDelivery($preparedData);
                ///// step 2: сохранение и валидация CivilLawSubject /////
                $preparedData = $this->dealManager->creatingCivilLawSubjectForDeal($user, $preparedData, $form);
                ///// step 3: сохранение и валидация PaymentMethod /////
                $preparedData = $this->dealManager->creatingPaymentMethodForDeal($preparedData, $form);
                ///// step 4: валидация сделки /////
                $preparedData = $this->dealManager->getValidationDealForm($preparedData, $form);

                //если все step валидны то сохраняем сделку
                if ($preparedData['validation']['is_valid']) {
                    $ownerUser = null;
                    if (array_key_exists('agent_email', $postData) && null !== $postData['agent_email']) {
                        /** @var User $ownerUser */
                        $ownerUser = $this->userManager->getUserByEmailVerifiedByValue($postData['agent_email']);
                    }

                    $data_deal = $preparedData['post_data']['deal'];
                    ///// 5: сохранение dealAgents /////
                    // Распределение ролей в сделке взависимости от выбранной инициатором роли
                    $data_deal_agent = $this->dealAgentManager
                        ->prepareDealAgentToDeal($data_deal, $preparedData['post_data']['agent_email'], $ownerUser);

                    $dealAgentCustomer = $this->dealAgentManager->createDealAgent(
                        $data_deal_agent['userCustomer'],
                        $data_deal_agent['paramCustomer']
                    );
                    $dealAgentContractor = $this->dealAgentManager->createDealAgent(
                        $data_deal_agent['userContractor'],
                        $data_deal_agent['paramContractor']
                    );
                    // Устанавливаем CivilLawSubject к DealAgent контрагента (если данные пришли от партнера)
                    if (array_key_exists('counteragent_civil_law_subject_id', $preparedData['post_data']) &&
                        null !== $preparedData['post_data']['counteragent_civil_law_subject_id']) {

                        $this->dealAgentManager->setCivilLawSubjectById(
                            $this->dealAgentManager->getCounteragentByAgents($dealAgentCustomer, $dealAgentContractor, $user, $preparedData['post_data']['agent_email']),
                            $preparedData['post_data']['counteragent_civil_law_subject_id']
                        );
                    }

                    ///// 5.1: сохранение deal /////
                    // Определение агента владельца сделки // Can throw Exception
                    $dealOwner = $this->dealAgentManager
                        ->defineDealOwner($user, $dealAgentCustomer, $dealAgentContractor, $preparedData['post_data']['agent_email']);
                    // Create Deal
                    $deal = $this->dealManager->createDeal($data_deal, $dealAgentCustomer, $dealAgentContractor, $dealOwner, $token);

                    /// сохраняем в данные event для прикрепления файла ///
                    $event = $this->getEvent();
                    $event->setParam(self::DEAL_FILES, $filesData);

                    //// delivery ////
                    if (isset($preparedData['post_data']['delivery'])) {
                        $this->createOrEditDelivery($deal, $preparedData['post_data']['delivery']);
                    }

                    ////// выполнение действий связанных с окончанием создания сделки ///////
                    $this->eventManager->trigger(DealEvent::EVENT_SUCCESS_CREATING, $this, [
                        'success_creating_params' => [
                            'deal' => $deal
                        ]
                    ]);

                    ///// сохранение deal confirm /////
                    try {
                        if ($this->dealManager->isDealAllowedToBeConfirmByCurrentUser($deal, $user, $token)) {
                            // Подтверждение условий сделки
                            $this->dealManager->confirmConditions($deal, $user);
                        }
                    } catch (\Throwable $t) {

                        logException($t, 'deal-errors', 3);
                    }

                    // DB Transaction commit
                    $this->entityManager->getConnection()->commit();

                    ///// SUCCESS /////
                    if ($isAjax) {
                        $data = [
                            'deal' => $this->dealManager->getDealOutputForSingle($deal)
                        ];
                        return $this->message()->success('Deal registered', $data);
                    }
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                ///// invalidFormData /////
                if ($isAjax) {
                    $invalidFormData = $this->dealManager->getProcessingInvalidFormData($preparedData['validation']);

                    return $this->message()->invalidFormData($invalidFormData);
                }
            }
        }
        catch (\Throwable $t) {
            // DB Transaction rollback
            ORMDoctrineUtil::rollBackTransaction($this->entityManager);
            logException($t, 'deal-errors', 3);
            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'dealForm' => $form,
            'type_form' => $this->params()->fromRoute('type_form', 'default')
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function createUnregisteredFormAction()
    {
        // Если пользователь авторизован, то - редирект на основной экшен создания сделки
        if (null !== $this->baseAuthManager->getIdentity()) {

            return $this->redirect()->toRoute('deal/create');
        }

        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $incomingData = $this->collectIncomingData();

            $user = null;

            $this->eventManager->trigger(DealEvent::EVENT_BEFORE_CREATING, $this);

            $form = new DealUnregisteredForm($this->dealManager, $this->config);

            $postData = $incomingData['postData'];
            if ($postData) {
                $filesData = $this->collectIncomingData()['filesData'];
                if (\count($filesData)) {
                    foreach ($filesData['files'] as $file) {
                        if ($file['error'] === 4) { $filesData = null; break; }
                    }
                }
                if ($filesData) {
                    $postData = array_merge_recursive($postData, $filesData);
                }
                $form->setData($postData);
                // Validate form
                if ($form->isValid()) {
                    $formData = $form->getData();

                    $deal_data = $formData;
                    // Убираем из результирующего массива ненужные ключи submit, files и captcha
                    unset($deal_data['submit'], $deal_data['files'], $deal_data['captcha']);

                    if ($deal_data['counteragent_email'] === $deal_data['agent_email']) {

                        throw new LogicException(null, LogicException::DEAL_UNREGISTERED_POST_DATA_IS_INVALID);
                    }

                    if (isset($postData['delivery'])) {
                        $deal_data['delivery'] = $postData['delivery'];
                    }
                    $event = $this->getEvent();
                    $event->setParam('deal_data', $deal_data);
                    $event->setParam(self::DEAL_FILES, $filesData);
                    // отправляем в экшен create
                    return $this->forward()->dispatch(self::class, array('action' => 'create'));
                }
                ///// invalidFormData /////
                if ($isAjax) {

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
        }
        catch (\Throwable $t) {
            logException($t, 'deal-errors', 3);
            return $this->message()->exception($t);
        }

        $form_action = $this->url()->fromRoute('deal/create-unregistered-form');

        if ($isAjax) {
            $data = [
                'form_action' => $form_action,
            ];
            return $this->message()->success('Deal unregistered form', $data);
        }

        $form->setAttribute('action', $form_action); // На случай, если тэг формы генерируется хелпером формы
        $form->prepare();
        $view = new TwigViewModel([
            'dealForm' => $form,
            'form_action' => $form_action, // На случай, если тэг формы жестко прописывается в twig
            'type_form' => $this->params()->fromRoute('type_form', 'default')
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function createPartnerFormAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $incomingData = $this->collectIncomingData();

            if (!array_key_exists('tokenKey', $incomingData)) {

                throw new LogicException(null, LogicException::DEAL_TOKEN_NOT_PROVIDED);
            }

            // Проверка на наличие токена в базе
            $token = $this->dealTokenManager->getTokenByKey($incomingData['tokenKey']);
            if (null === $token ) {

                throw new LogicException(null, LogicException::DEAL_TOKEN_NOT_FOUND);
            }
            // Can throw Exception
            $token_data = $this->dealTokenManager->getDataFromToken($token);

            $this->eventManager->trigger(DealEvent::EVENT_BEFORE_CREATING, $this);

            $user = null;
            if (null !== $this->baseAuthManager->getIdentity()) {
                /** @var User $user */ // Can throw Exception
                $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            }
            $dealTokenPolitics = $this->dealTokenManager->getDealTokenManagerPolitics();
            $dealTokenPolitics->isUserAllowedCreateBySpecificToken($token_data, $user);
            if (false === $dealTokenPolitics->isUserAllowedCreateBySpecificToken($token_data, $user)) {

                throw new LogicException(null, LogicException::DEAL_TOKEN_AGENT_EMAIL_MATCH_COUNTERAGENT_EMAIL);
            }

            $form = new DealPartnerForm($this->dealManager, $user, $token_data);
            $postData = $incomingData['postData'];

            if ($postData) {
                $filesData = $this->collectIncomingData()['filesData'];
                if (\count($filesData)) {
                    foreach ($filesData['files'] as $file) {
                        if ($file['error'] === 4) { $filesData = null; break; }
                    }
                }
                if ($filesData) {
                    $postData = array_merge_recursive($postData, $filesData);
                }
                $form->setData($postData);
                // Validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    // Мержим массивы с данными в $deal_data
                    $deal_data = array_merge($token_data, $formData);

                    // Убираем из результирующего массива ненужный ключь submit и file
                    unset($deal_data['submit'], $deal_data['files']);

                    if (false === $dealTokenPolitics->isIncomingDataCorrect($deal_data, $token_data, $user)) {

                        throw new LogicException(null, LogicException::DEAL_TOKEN_POST_DATA_IS_INVALID);
                    }

                    if (isset($postData['delivery'])) {
                        $deal_data['delivery'] = $postData['delivery'];
                    }
                    $event = $this->getEvent();
                    $event->setParam('deal_data', $deal_data);
                    $event->setParam('token', $token);
                    $event->setParam(self::DEAL_FILES, $filesData);
                    // отправляем в экшен create
                    return $this->forward()->dispatch(self::class, array('action' => 'create'));
                }
                ///// invalidFormData /////
                if ($isAjax) {

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
        }
        catch (\Throwable $t) {
            logException($t, 'deal-errors', 3);
            return $this->message()->exception($t);
        }

        $form_action = $this->url()->fromRoute('deal/create-partner-form', ['tokenKey' => $incomingData['tokenKey']]);

        $userOutput = null;
        if (null !== $user) {
            $userOutput = $this->userManager->getUserOutputForSingle($user);
        }

        if ($isAjax) {
            $data = [
                'user' => $userOutput,
                'form_action' => $form_action,
                'token_data' => $token_data
            ];
            return $this->message()->success('Deal partner form', $data);
        }

        $form->setAttribute('action', $form_action); // На случай, если тэг формы генерируется хелпером формы
        $form->prepare();
        $view = new TwigViewModel([
            'dealForm' => $form,
            'user' => $userOutput,
            'form_action' => $form_action, // На случай, если тэг формы жестко прописывается в twig
            'token_data' => $token_data
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function editAction()
    {
        $deal_id = (int)$this->params()->fromRoute('id', 0);
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            // DB Transaction start
            $this->entityManager->getConnection()->beginTransaction();

            /** @var Deal $deal */ // Status сделки установлен
            $deal = $this->dealManager->getDealById($deal_id);
            /** @var User $user */
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            // Check permissions // Important! Only Customer or Contractor can edit the Deal
            if (!$this->access('deal.own.edit', ['user' => $user, 'deal' => $deal])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            if ($deal->status['status'] !== 'negotiation') {

                throw new logicException(null, logicException::DEAL_EDITING_IS_IMPOSSIBLE);
            }

            /**
             * 1. Получаем DealAgents относительного текущего пользователя
             * 2. Инициализация формы
             * 3. Заполняем форму редактирования данными
             * 4. Если НЕ владелец блокируем редактирование email и роли в сделке
             */
            $dealAgents = $this->dealManager->getDealAgentsRelativeToCurrentUser($user, $deal);
            /** @var DealForm $form */
            $form = new DealForm($this->dealManager, $user, $dealAgents['deal_role']);
            $this->dealManager->setDealFormDataWhenEditing($deal, $form, $dealAgents);
            $this->dealManager->disallowEditFieldsDealFormForNotOwner($user, $deal, $form);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                // подготавливаем массив с разделенными данными и параметрами валидации
                $preparedData = $this->dealManager->prepareDataToDeal($postData);
                ///// валидация Delivery /////
                $preparedData = $this->dealManager->getValidationDelivery($preparedData);
                ///// step 1: обробатываем post данные с учетом редактирования /////
                $preparedData = $this->dealManager->prepareDataToEditDeal($user, $deal, $dealAgents, $preparedData);
                ///// step 2: сохранение CivilLawSubject /////
                $preparedData = $this->dealManager->creatingCivilLawSubjectForDeal($user, $preparedData, $form);
                ///// step 3: сохранение PaymentMethod /////
                $preparedData = $this->dealManager->creatingPaymentMethodForDeal($preparedData, $form);
                ///// step 4: валидация сделки /////
                $preparedData = $this->dealManager->getValidationDealForm($preparedData, $form);

                //если все step валидны то сохраняем сделку
                if ($preparedData['validation']['is_valid']) {
                    //---------------------------------------------------------------//
                    // Подписываемся на изменения относящихся к Deal сущностей (Deal, Payment, DealAgent)
                    $doctrineEventManager = $this->entityManager->getEventManager();
                    $doctrineEventManager
                        ->addEventListener(
                            array(\Doctrine\ORM\Events::onFlush, \Doctrine\ORM\Events::postFlush),
                            $this->dealConditionsListener
                        );
                    //---------------------------------------------------------------//
                    $data_deal = $preparedData['post_data']['deal'];
                    ///// 5: edit deal /////
                    $deal = $this->dealManager->editDeal($user, $deal, $data_deal, $dealAgents);

                    //// delivery ////
                    if (isset($preparedData['post_data']['delivery'])) {
                        $this->createOrEditDelivery($deal, $preparedData['post_data']['delivery']);
                    }

                    ////// выполнение действий связанных с окончанием изменения сделки ///////
                    $this->eventManager->trigger(DealEvent::EVENT_SUCCESS_EDITING, $this, [
                        'deal' => $deal
                    ]);

                    try {
                        ///// сохранение deal confirm /////
                        if ($this->dealManager->isDealAllowedToBeConfirmByCurrentUser($deal, $user)) {
                            // Подтверждение условий сделки
                            $this->dealManager->confirmConditions($deal, $user);
                        }
                    }
                    catch (\Throwable $t) {

                        logException($t, 'deal-errors', 3);
                    }

                    // DB Transaction commit
                    $this->entityManager->getConnection()->commit();

                    ///// SUCCESS /////
                    if ($isAjax) {
                        $data = [
                            'deal' => $this->dealManager->getDealOutputForSingle($deal)
                        ];

                        return $this->message()->success('Deal successfully edited', $data);
                    }

                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                ///// invalidFormData /////
                if ($isAjax) {
                    $invalidFormData = $this->dealManager->getProcessingInvalidFormData($preparedData['validation']);

                    return $this->message()->invalidFormData($invalidFormData);
                }
            }

            $dealOutput = $this->dealManager->getDealOutputForSingle($deal, $user);
        }
        catch (\Throwable $t) {
            // DB Transaction rollback
            ORMDoctrineUtil::rollBackTransaction($this->entityManager);
            logException($t, 'deal-errors', 3);
            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'dealForm' => $form,
            'deal' => $dealOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @param mixed $id
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Deal $deal */ // Can throw Exception
            $deal = $this->dealManager->getDealById($id);
            // Check permissions // Important! Only Owner can delete his Deal
            if (!DealManager::isUserOwnerOfDeal($deal, $user)) {

                return $this->redirect()->toRoute('not-authorized');
            }
            // Check if removing allowed
            if (!$this->dealManager->isDealRemovingAllowed($deal)) {

                throw new \Exception(self::CAN_NOT_BE_DELETED);
            }
            // Удаление сделки и связей
            $this->dealManager->remove($deal);

        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        // Ajax success
        if ($isAjax) {

            return $this->message()->success(self::SUCCESS_DELETE);
        }
        // Http success
        return $this->redirect()->toRoute('deal');
    }

    /**
     * Remove NaturalPerson (GET)
     *
     * @return \Zend\Http\Response|ViewModel
     *
     * Important! Only for Http
     * @throws \Throwable
     */
    public function deleteFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();

            if (!isset($formDataInit['deal']) || !$formDataInit['deal'] instanceof Deal) {

                throw new \Exception('bad data provided: Deal not found');
            }

            /** @var Deal $deal */
            $deal = $formDataInit['deal'];
            /** @var User $user */
            $user = $formDataInit['user'];

            // Check permissions // Important! Only Owner can delete his Deal
            if (!DealManager::isUserOwnerOfDeal($deal, $user)) {

                return $this->redirect()->toRoute('not-authorized');
            }
            // Check if removing allowed
            if (!$this->dealManager->isDealRemovingAllowed($deal)) {

                throw new \Exception(self::CAN_NOT_BE_DELETED);
            }

            /** @var Deal $deal */
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal, $user);

        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        // Http -
        $view = new TwigViewModel([
            'deal' => $dealOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Throwable
     */
    public function confirmConditionsAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            // Тут уже будут $user и $deal
            $formDataInit = $this->processFormDataInit();

            if (!isset($formDataInit['deal']) || !$formDataInit['deal'] instanceof Deal) {

                throw new \Exception('bad data provided: Deal not found');
            }

            /** @var Deal $deal */
            $deal = $formDataInit['deal'];
            /** @var User $user */
            $user = $formDataInit['user'];

            if (!DealManager::isUserPartOfDeal($deal, $user)) {

                return $this->redirect()->toRoute('not-authorized');
            }
            /** @var Deal $deal */
            $dealOutput = $this->dealManager->getDealOutputForSingle($deal, $user);
            if ($dealOutput['is_need_to_fill_deal']) {
                
                return $this->message()->error('Not fully completed transaction');
            }
            // Проверяем готова ли сделка и dealAgent текущего пользователя к принятию условий
            // Can throw Exception
            $this->dealManager->checkDealAllowedToBeConfirmByCurrentUser($deal, $user);

        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $confirmConditionsForm = new DealConditionsConfirmForm();

        // Проверяем, является ли пост POST-запросом.
        $postData = $this->collectIncomingData()['postData'];
        // test
        if ($postData) {
            //set data to form
            $confirmConditionsForm->setData($postData);
            // validate form
            if ($confirmConditionsForm->isValid()) {
                // Подписываемся на изменения относящихся к Deal сущностей (Deal, Payment, DealAgent)
                $doctrineEventManager = $this->entityManager->getEventManager();
                $doctrineEventManager
                    ->addEventListener(
                        array(\Doctrine\ORM\Events::onFlush, \Doctrine\ORM\Events::postFlush),
                        $this->dealConditionsListener
                    );
                // Подтверждение условий сделки // Can throw Exception
                $this->dealManager->confirmConditions($deal, $user);

                // Ajax success
                if ($isAjax) {

                    return $this->message()->success(self::SUCCESS_CONFIRM);
                }
                // Http success
                return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
            }
            // Ajax -
            if ($isAjax) {
                // Return Json with ERROR status
                return $this->message()->invalidFormData($confirmConditionsForm->getMessages());
            }
        }

        // Ajax -
        if ($isAjax) {
            $csrf = $confirmConditionsForm->get('csrf')->getValue();
            $data = [
                #'deal' => $dealOutput,
                'csrf' => $csrf
            ];

            return $this->message()->success("CSRF token return", $data);
        }

        // Http -
        $view = new TwigViewModel([
            'deal' => $dealOutput,
            'confirmConditionsForm' => $confirmConditionsForm
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Отдает типы сделок
     * Только для Ajax запросов
     * Только для авторизованного пользователя
     *
     * @return array|bool|TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function typesAction()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }
        try {
//            // Get current user // Can throw Exception
//            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            /** @var array $deal_types */
            $deal_types = $this->dealManager->getDealTypesAsArray();

            $data = [
                'deal_types' => $deal_types
            ];

            return $this->message()->success('Deal types', $data);
        }
        catch (\Exception $t) {

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array|bool|TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            // Can throw Exception
            $formDataInit = $this->processFormDataInit();
            $data = $formDataInit;
            $data['user'] = null;
            if ($formDataInit['user']) {
                /** @var User $user */
                $user = $formDataInit['user'];
                $data['user'] = $this->userManager->extractUser($user);
                $data['user']['phone'] = $user->getPhone() && $user->getPhone()->getIsVerified() ? $user->getPhone()->getPhoneNumber() : null;
                $data['user']['email'] = $user->getEmail() && $user->getEmail()->getIsVerified() ? $user->getEmail()->getEmail() : null;
                $data['user']['is_operator'] = $this->rbacManager->hasRole(null, 'Operator');
                $data['user']['is_verified'] = $this->rbacManager->hasRole(null, 'Verified');
            }
            $data['deal'] = $formDataInit['deal'] && $formDataInit['user']
                ? $this->dealManager->getDealOutputForSingle($formDataInit['deal'], $formDataInit['user'])
                : null;
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->message()->success('formInit', $data);
    }

    /**
     * Подготовка данных для инициализации формы
     *
     * @return array
     * @throws \Throwable
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $user = null;
        $is_operator = false;
        $is_user_authorized = false;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $is_operator = $this->rbacManager->hasRole(null, 'Operator');
            $is_user_authorized = true;
        }
        //process data for update or delete
        $deal = null;
        $dispute_history = null;
        $deal_form_output = null;
        $csrf_output = null;
        $deal_statuses = null;
        $dispute_statuses = null;
        $deal_types = null;
        $fee_payer_options = null;
        $naturalPersonsOutput = null;
        $soleProprietorsOutput = null;
        $legalEntitiesOutput = null;

        if (isset($incomingData['idDeal'])) {
            /** @var $deal $deal */
            $deal = $this->dealManager
                ->getDealById((int)$incomingData['idDeal']);
            if ($deal && ($is_operator || DealManager::isUserPartOfDeal($deal, $user))) {
                $dispute_history = $this->disputeHistoryManager->getDisputeHistory($deal->getDispute(), $user);
            }
            if ($deal && !$is_operator && DealManager::isUserPartOfDeal($deal, $user)) {
                $deal_form_output = $this->dealManager->getDealOutputForForm($deal, $user);
            }
            if ($deal && !$is_operator && !DealManager::isUserPartOfDeal($deal, $user)) {
                $deal = null;
            }
        }

        $csrf = new \Zend\Form\Element\Csrf('csrf');
        $csrf->setCsrfValidatorOptions([
            'timeout' => 3600
        ]);
        $csrf_output = $csrf->getValue();

        /** @var array $deal_types */
        $deal_types = $this->dealManager->getDealTypesAsArray();
        /** @var array $fee_payer_options */
        $fee_payer_options = $this->dealManager->getActiveFeePayerOptionsArray();

        if ($user) {
            /** @var array $deal_statuses */
            $deal_statuses = DealStatus::getStatuses();
            /** @var array $dispute_statuses */
            $dispute_statuses = DisputeStatus::getStatuses();

            // Get user's naturalPersons as array
            $naturalPersonsOutput = $this->civilLawSubjectManager
                ->getNaturalPersonsForUser($user, [], true);
            $naturalPersonsOutput = !empty($naturalPersonsOutput) ? $naturalPersonsOutput : null;

            // Get user's soleProprietor as array
            $soleProprietorsOutput = $this->civilLawSubjectManager
                ->getSoleProprietorForUser($user, [], true);
            $soleProprietorsOutput = !empty($soleProprietorsOutput) ? $soleProprietorsOutput : null;

            // Get user's legalEntities as array
            $legalEntitiesOutput = $this->civilLawSubjectManager
                ->getLegalEntitiesForUser($user, [], true);
            $legalEntitiesOutput = !empty($legalEntitiesOutput) ? $legalEntitiesOutput : null;
        }

        $counteragent_token_data = null;
        if (array_key_exists('token_key', $incomingData)) {
            // Проверка на наличие токена в базе
            $token = $this->dealTokenManager->getTokenByKey($incomingData['token_key']);
            if (null === $token ) {

                throw new LogicException(null, LogicException::DEAL_TOKEN_NOT_FOUND);
            }

            $counteragent_token_data = $this->dealTokenManager->getDataFromToken($token);
        }

        $deliveryServiceTypes = $this->deliveryOrderManager->getActiveDeliveryServiceTypes();
        $deliveryServiceTypesOutput = $this->deliveryOrderManager
            ->getDeliveryServiceTypesCollectionForOutput($deliveryServiceTypes);

        return [
            'max_deal_amount' => self::MAX_DEAL_AMOUNT,
            'deal_statuses' => $deal_statuses,
            'dispute_statuses' => $dispute_statuses,
            'deal_types' => $deal_types,
            'fee_payer_options' => $fee_payer_options,
            'naturalPersons' => $naturalPersonsOutput,
            'soleProprietors' => $soleProprietorsOutput,
            'legalEntities' => $legalEntitiesOutput,
            'deal_form' => $deal_form_output,
            'counteragent_token_data' => $counteragent_token_data,
            'is_user_authorized' => $is_user_authorized,
            //create
            'user' => $user,
            //edit
            'deal' => $deal,
            'dispute_history' => $dispute_history,
            'delivery_service_types' => $deliveryServiceTypesOutput,
            'service_dpd_pickup_time_period' => DeliveryOrderDpdManager::RECEIPT_TIME_INTERVALS,
            'service_dpd_delivery_time_period' => DeliveryOrderDpdManager::DELIVERY_TIME_INTERVALS,
            'service_dpd_payment_method_options' => DeliveryOrderDpdManager::PAYMENT_METHOD_OPTIONS,
            'csrf' => $csrf_output
        ];
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\Stdlib\ResponseInterface|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function downloadContractAction()
    {
        $deal_id = (int)$this->params()->fromRoute('id');

        // Try get Deal and User
        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Deal $deal */ // Can throw Exception
            $deal = $this->dealManager->getDealById($deal_id);
            // Если сделка еще не одобрена, то - Exception
            if (!$this->dealPolitics->isDealConfirmedByDealAgents($deal)) {
                throw new \Exception('The deal is not approved yet');
            }
            // Check permissions // Important! Only Customer, Contractor or Operator can download the Contract of Deal
            if (!$this->access('deal.own.contract', ['user' => $user, 'deal' => $deal])
                && !$this->access('deal.all.view')) {
                return $this->redirect()->toRoute('not-authorized');
            }
            // Try download file
            $response = $this->dealManager->downloadContractFile($deal->getDealContractFile()->getFile()->getId());
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $response;
    }

    /**
     * Returns data with Deal Percentage for Ajax
     *
     * @return \Zend\Http\Response|\Zend\View\Model\JsonModel
     * @throws \Exception
     */
    public function getDealPercentageAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        if ($isAjax) {
            $feePercentage = $this->calculatedDataProvider->getFeePercentage();

            return $this->message()->success('deal percentage', ['percentage' => $feePercentage]);
        }

        // HTTP -
        return $this->message()->error('Only for Ajax requests');
    }

    /**
     * Expired Deals Processing
     *
     * @return \Zend\Http\Response|ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function expiredAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $deal_all = 'all' === $this->params()->fromRoute('id');
        $deal_id = (int)$this->params()->fromRoute('id', 0);

        // Data
        $data = $this->collectIncomingData();

        $paramsFilterAndSort = $this->getPaginationParams($this->config);

        /** @var ExpiredSearchForm $expiredSearchForm */
        $expiredSearchForm = new ExpiredSearchForm();
        $dealFilterForm = new DealSearchForm($this->dealManager, true);
        // Set action
        $dealFilterForm->setAttribute('action', $this->url()->fromRoute('deal/expired'));

        if (isset($data['filter'])) {
            /** @var DataFilter $dataFilter */
            $dataFilter = new DataFilter();

            $data['filter']['csrf'] = $data['csrf'];

            $filterResult = $dataFilter->filterData($expiredSearchForm, $data['filter']);

            if ($filterResult['filter']) {
                $paramsFilterAndSort['filter'] = $filterResult['filter'];
            } else {
                $expiredSearchForm = $filterResult['form'];
            }
        }

        // Обработка платёжек, готовых к выплате
        if ($deal_id || $deal_all) {
            try {
                if ($this->dealManager->expiredDealsProcessing($deal_id)) {

                    return $this->redirect()->toRoute('bank-client/credit-unpaid');
                }
            } catch (\Throwable $t) {

                throw new \Exception($t->getMessage());
            }
        }
        // if no id passed

        // Get all payments with expired deals with sort and search params
        $paymentsDataOutput = $this->paymentManager
            ->getPaymentsWithExpiredDeals($paramsFilterAndSort);

        $paginatorOutput = null;
        if ($paymentsDataOutput['paginator']) {
            $paginatorOutput = $this->getPaginatorOutput($paymentsDataOutput['paginator']);
        }

        // SUCCESS Response with CSRF
        // Ajax - csrf token
        if ($isAjax) {
            $csrf = $expiredSearchForm->get('csrf')->getValue();
            $data = [
                'csrf' => $csrf,
                'payments' => $paymentsDataOutput['payments'],
                'paginator' => $paginatorOutput
            ];

            return $this->message()->success("CSRF token return", $data);
        }
        $expiredSearchForm->prepare();
        $view = new TwigViewModel([
            'expiredSearchForm' => $expiredSearchForm,
            'payments' => $paymentsDataOutput['payments'],
            'paginator' => $paginatorOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function abnormalAction()
    {
        $isPost = $this->getRequest()->isPost();
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Page number for Paginator
        $page = $this->params()->fromQuery('page', 1);

        $dealAbnormalForm = new DealAbnormalForm($this->paymentPolitics);

        $sortLinks = null;
        $params = [];

        $order = $this->params()->fromQuery('order');
        $sort = $this->params()->fromQuery('sort', $this->config['default_pagination_preferences']['sorting_order']);

        $params['per_page'] = $this->params()->fromQuery('per_page', $this->config['default_pagination_preferences']['number_per_page']);
        if ($order !== null) {
            $params['order'] = [$order => $sort];
        }

        $search_by = null;
        $search_value = null;

        if ($isPost) {
            // Get data from request
            if ($isAjax) {
                $content = $this->getRequest()->getContent();
                $data = Json::decode($content, Json::TYPE_ARRAY);
            } else {
                $data = $this->params()->fromPost();
            }

            // Заполняем форму данными.
            $dealAbnormalForm->setData($data);

            if ($dealAbnormalForm->isValid()) {
                $data = $dealAbnormalForm->getData();
                if (array_key_exists('abnormal_type', $data) && !empty($data['abnormal_type'])) {
                    $params['search']['abnormal_type'] = $data['abnormal_type'];
                }
            }
            // Form is not valid
            if ($isAjax) {
                return $this->message()->invalidFormData($dealAbnormalForm->getMessages());
            }
        }

        try {
            // Get all payments with expired deals with sort and search params
            $paymentsDataOutput = $this->paymentManager->getPaymentsForAbnormalDeals($params, $page);
            $paymentAbnormalTypes = $this->paymentPolitics->getPaymentAbnormalTypes();

            $paginatorOutput = null;
            if ($paymentsDataOutput['paginator']) {
                $paginatorOutput = $this->getPaginatorOutput($paymentsDataOutput['paginator']);
            }
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        // SUCCESS Response with CSRF
        // Ajax - csrf token
        if ($isAjax) {
            $data = [
                'csrf' => $dealAbnormalForm->get('csrf')->getValue(),
                'payments' => $paymentsDataOutput['payments'],
                'paginator' => $paginatorOutput,
                'payment_abnormal_types' => $paymentAbnormalTypes
            ];

            return $this->message()->success('CSRF token return', $data);
        }

        // Html -
        $view = new TwigViewModel([
            'dealAbnormalForm' => $dealAbnormalForm,
            'payments' => $paymentsDataOutput['payments'],
            'paginator' => $paginatorOutput,
            'sortLinks' => $sortLinks,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel
     * @throws \Exception
     */
    public function overpayAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        $deal_id = (int)$this->params()->fromRoute('idDeal', 0);

        try {
            /** @var Deal $deal */ // Can throw Exception
            $deal = $this->dealManager->getDealById($deal_id);

            $overpaid_amount = $this->dealManager->getOverpaidAmount($deal->getPayment());

            if (null === $overpaid_amount) {

                throw new \Exception('Deal is not overpaid');
            }

            /** @var Payment $payment */
            $payment = $deal->getPayment();

            if ($payment->getRepaidOverpay()) {

                throw new \Exception('RepaidOverpay already done');
            }

            /** @var RepaidOverpayForm $repaidOverpayForm */
            $repaidOverpayForm = new RepaidOverpayForm($deal);

            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $repaidOverpayForm->setData($postData);
                // Validate form
                if ($repaidOverpayForm->isValid()) {
                    // Обработка переплаты
                    if ($this->dealManager->overpaidDealsProcessing($deal)) {

                        return $this->message()->success(
                            'Платёжка на возврат переплаты для сделки ' . $deal->getNumber() . ' сформирована!'
                        );
                        #return $this->redirect()->toRoute('deal/abnormal');
                    }
                }
                if ($isAjax) {
                    return $this->message()->invalidFormData($repaidOverpayForm->getMessages());
                }
            }

            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);

            $incomingPaymentTransactions = $this->dealManager->getIncomingPaymentTransactions($payment);

            $incomingPaymentTransactionsOutput = $this->dealManager
                ->getPaymentTransactionCollectionOutput($incomingPaymentTransactions);
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        // Request without POST
        // Ajax
        if ($isAjax) {
            return $this->message()->success('Overpay', [
                'deal' => $dealOutput,
                'overpaid_amount' => $overpaid_amount,
                'expected_amount' => $payment->getExpectedValue(),
                'incomingTransactions' => $incomingPaymentTransactionsOutput
            ]);
        }
        // Html -
        $view = new TwigViewModel([
            'repaidOverpayForm' => $repaidOverpayForm,
            'deal' => $dealOutput,
            'overpaid_amount' => $overpaid_amount,
            'expected_amount' => $payment->getExpectedValue(),
            'incomingTransactions' => $incomingPaymentTransactionsOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Shows success page
     *
     * @return ViewModel
     */
    public function successAction()
    {

        return new ViewModel();
    }

    /**
     * Shows error page
     *
     * @return ViewModel
     */
    public function errorAction()
    {

        return new ViewModel();
    }

    /**
     * @return mixed
     */
    public function deliveryAction()
    {
        return $this->message()->success('delivery action');
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function fileUploadAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Data
        $data = $this->collectIncomingData();

        // Try get Deal and User
        try {
            if (!array_key_exists('deal_id', $data)) {
                throw new \Exception('Некорректные данные');
            }
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Deal $deal */ // Can throw Exception
            $deal = $this->dealManager->getDealById($data['deal_id']);
            // Check permissions // Important! Only Participant or Operator can upload file
            if (!$this->access('deal.own.edit', ['user' => $user, 'deal' => $deal]) && !$this->rbacManager->hasRole(null, 'Operator')) {
                return $this->redirect()->toRoute('not-authorized');
            }
        } catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        /** @var DealFileUploadForm $fileUploadForm */
        $fileUploadForm = new DealFileUploadForm($deal->getId());

        if ($this->getRequest()->isPost()) {

            $post = array_merge_recursive(
                $data['postData'],
                //$_FILES
                $data['filesData']
            );

            // Set data
            $fileUploadForm->setData($post);

            if ($fileUploadForm->isValid()) {
                // Form is valid, save the form!
                $data = $fileUploadForm->getData();

                if ($data['file']['error']) {
                    return $this->message()->error('Error', $data['file']['error']);
                }

                // Сохраняем файл и добавляем в коллекцию файлов сделки
                $file = $this->dealManager->saveDealFile($deal, $data['file']);

                // If Ajax request, return json
                if ($isAjax) {
                    return $this->message()->success('The file was successfully uploaded');
                }
                // Http -
                return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
            }
            // If form is not valid
            if ($isAjax) {
                // Return Json with ERROR status
                return $this->message()->invalidFormData($fileUploadForm->getMessages());
            }
            // В Http вернется форма с указанием ошибок валидации
        }
        // No Post data

        //@TODO Возврат для ajax

        // Html -
        $fileUploadForm->prepare();
        $view = new TwigViewModel([
            'fileUploadForm' => $fileUploadForm
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Показ файла сделки
     * @throws \Exception
     */
    public function showFileAction()
    {
        $dealId = (int)$this->params()->fromRoute('dealId');
        $fileId = $this->params()->fromRoute('fileId');

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Deal $deal */
            $deal = $this->dealManager->getDealById($dealId);

            // Check permissions // Important! Only Participant or Operator can upload file
            if (!$this->access('deal.own.edit', ['user' => $user, 'deal' => $deal]) && !$this->rbacManager->hasRole(null, 'Operator')) {
                return $this->redirect()->toRoute('not-authorized');
            }
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }


        $fileShowView = $this->forward()->dispatch('ModuleFileManager\Controller\FileController', array(
            'id' => $fileId
        ));

        return $fileShowView;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function disputeCreateFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        $dealId = (int)$this->params()->fromRoute('dealId');

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Deal $deal */
            $deal = $this->dealManager->getDealById($dealId);
            // Check permissions // Important! Only Customer can create Dispute
            if (!$this->access('deal.own.customer', ['user' => $user, 'deal' => $deal])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            if (!$this->dealPolitics->isDealDisputable($deal)) {

                throw new \Exception(DisputeController::DISPUTE_IS_NOT_POSSIBLE);
            }

        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        // Deal Output
        $this->dealStatus->setStatus($deal);
        $dealOutput = $this->dealManager->getDealOutputForSingle($deal);

        // Create form
        $form = new DisputeForm($deal->getId());
        $form->prepare();
        $view = new TwigViewModel([
            'deal' => $dealOutput,
            'disputeForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/dispute/create-form');

        return $view;
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function dealInvitationResendAction()
    {
        try {
            $deal_id = (int) $this->params()->fromRoute('idDeal');
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Deal $deal */
            $deal = $this->dealManager->getDealById($deal_id);

            // Check permissions
            if (!$this->access('deal.own.edit', ['user' => $user, 'deal' => $deal])) {

                return $this->redirect()->toRoute('access-denied');
            }

            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {

                $csrf = $postData['csrf'] ?? null;
                $csrfValidator = new CsrfValidator();

                if ($csrfValidator->isValid($csrf)) {

                    $this->eventManager->trigger(DealEvent::EVENT_RESEND_INVITATION, $this, [
                        'deal' => $deal,
                        'user' => $user
                    ]);

                    if ($this->getRequest()->isXmlHttpRequest()) {

                        return $this->message()->success('invitation resend');
                    }
                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }

                return $this->message()->invalidFormData(['csrf'=>$csrfValidator->getMessages()]);
            }

        } catch (\Throwable $t) {
            // ERROR Response
            return $this->message()->exception($t);
        }

        return $this->redirect()->toRoute('access-denied');
    }

    /**
     * @return TwigViewModel
     */
    public function successfulCreatedAction()
    {
        $view = new TwigViewModel([]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @param Deal $deal
     * @param $data
     */
    private function createOrEditDelivery(Deal $deal, $data)
    {
        /** @var DeliveryOrder $deliveryOrder */
        $deliveryOrder = $deal->getDeliveryOrder();
        if ($deliveryOrder) {
            // если ордер уже был создан то редактируем
            $this->eventManager->trigger(DealEvent::EVENT_EDIT_DELIVERY_ORDER_AFTER_CREATE_DEAL, $this, [
                'edit_delivery_order_after_create_deal_params' => [
                    'delivery_order' => $deliveryOrder,
                    'delivery_data' => $data
                ]
            ]);
        } else {
            // если ордера еще небыло то создаем
            $this->eventManager->trigger(DealEvent::EVENT_CREATE_DELIVERY_ORDER_AFTER_CREATE_DEAL, $this, [
                'create_delivery_order_after_create_deal_params' => [
                    'deal' => $deal,
                    'delivery_data' => $data
                ]
            ]);
        }
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     */
    public function createDpdOrderAction()
    {
        try {
            $isAjax = $this->getRequest()->isXmlHttpRequest();
            $deal_id = (int) $this->params()->fromRoute('idDeal');
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            /** @var Deal $deal */
            $deal = $this->dealManager->getDealById($deal_id);
            // Check permissions
            if (!$this->access('deal.own.edit', ['user' => $user, 'deal' => $deal])) {

                return $this->redirect()->toRoute('access-denied');
            }
            /** @var DeliveryOrder $deliveryOrder */
            $deliveryOrder = $deal->getDeliveryOrder();
            /** @var DpdDeliveryOrder $dpdDeliveryOrder */
            $dpdDeliveryOrder = $deliveryOrder ? $deliveryOrder->getDpdDeliveryOrder() : null;
            if ($deal->status['status'] !== 'debit_matched' ||
                !$this->deliveryOrderDpdManager->isAllowedCreateDpdDeliveryOrder($dpdDeliveryOrder)){

                throw new LogicException(null, LogicException::DELIVERY_DPD_ORDER_CREATE_FORBIDDEN);
            }

            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $datePickupForm = new DatePickupForm();
                $datePickupForm->setData($postData);
                if (!$datePickupForm->isValid()) {
                    if ($isAjax) {

                        return $this->message()->invalidFormData($datePickupForm->getMessages());
                    }

                    return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
                }
                $data = $datePickupForm->getData();

                $datePickup = isset($data['date_pickup']) ? \DateTime::createFromFormat('Y-m-d', $data['date_pickup']) : null;

                // Проверка расписания работы пункта на прием посылок (SelfPickup)
                $validation_message = $this->deliveryOrderDpdManager
                    ->pointScheduleChecking($dpdDeliveryOrder, $datePickup, 'SelfPickup');
                if (null !== $validation_message) {

                    return $this->message()->error($validation_message);
                }

                $dpdDeliveryOrder = $this->deliveryOrderDpdManager->setDatePickupToDpdDeliveryOrder($dpdDeliveryOrder, $datePickup);
                if ($dpdDeliveryOrder->getDatePickup()) {
                    $this->deliveryEventManager->trigger(
                        DeliveryEventProvider::EVENT_CREATE_DELIVERY_SERVICE_ORDER, $this, [
                        'create_delivery_service_order_params' => ['deliveryOrder' => $dpdDeliveryOrder->getDeliveryOrder()]
                    ]);
                }
                if ($isAjax) {

                    return $this->message()->success('dpd delivery order create');
                }
            }
            if ($isAjax) {

                return $this->message()->success('No data provided');
            }

            return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }
    }
}