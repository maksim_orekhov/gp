<?php

namespace Application\Controller;

use Application\Entity\CivilLawSubject;
use Application\Entity\Discount;
use Application\Entity\Dispute;
use Application\Entity\PaymentMethod;
use Application\Entity\User;
use Application\Form\DiscountForm;
use Application\Form\PaymentMethod\BankTransferForm;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Dispute\DisputeManager;
use Application\Service\PaginatorOutputTrait;
use Application\Service\Payment\PaymentMethodManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Zend\Paginator\Paginator;

/**
 * Class DiscountController
 * @package Application\Controller
 */
class DiscountController extends AbstractRestfulController
{
    use PaginatorOutputTrait;

    /**
     * @var DiscountManager
     */
    private $discountManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;
    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * DiscountController constructor.
     * @param DiscountManager $discountManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param DisputeManager $disputeManager
     * @param PaymentMethodManager $paymentMethodManager
     */
    public function __construct(DiscountManager $discountManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                DisputeManager $disputeManager,
                                PaymentMethodManager $paymentMethodManager)
    {
        $this->discountManager = $discountManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->disputeManager = $disputeManager;
        $this->paymentMethodManager = $paymentMethodManager;
    }

    /**
     * Только для Оператора
     *
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // If not operator
            if (false === $this->discountManager->isOperator($user)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            /** @var Paginator $paginatedDiscounts */
            $paginatedDiscounts = $this->discountManager->getAllDiscounts();

            $discountCollectionOutput = $this->discountManager->getDiscountCollectionOutput($paginatedDiscounts);
            $paginatorOutput = $paginatedDiscounts->getPages();
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'discounts' => $discountCollectionOutput,
            'paginator' => $paginatorOutput
        ];

        // Ajax -
        if ( $isAjax ){

            return $this->message()->success('Discounts', $data);
        }

        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/discount/collection');

        return $view;
    }

    /**
     * Доступен только Оператору
     *
     * @param mixed $id
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function get($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        if (!isset($id) || (int) $id <= 0) {

            return $this->message()->error('bad data provided');
        }

        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // not operator
            if (false === $this->discountManager->isOperator($user)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            /** @var Discount $discount */ // Can throw Exception
            $discount = $this->discountManager->getDiscountById($id);
            /** @var array $discountOutput */
            $discountOutput = $this->discountManager->getDiscountOutput($discount);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $data = [
            'discount' => $discountOutput
        ];

        // Ajax -
        if ( $isAjax ){

            return $this->message()->success("Discount single", $data);
        }

        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/discount/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        // only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            /** @var Dispute $dispute */ // Can throw Exception
            $dispute = $this->disputeManager->getDispute($data['dispute_id']);

            $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($dispute->getDeal());
            $paymentMethodBankTransfer = $customerPaymentMethod ? $customerPaymentMethod->getPaymentMethodBankTransfer() : null;

            $max_discount = $this->discountManager->getMaxDiscount($dispute->getDeal());
            /** @var DiscountForm $form */
            $form = new DiscountForm($paymentMethodBankTransfer, $dispute, $max_discount);

            // Проверка был ли уже создана скидка
            if ($this->discountManager->isDiscountDone($dispute)) {

                throw new \Exception('Discount already done');
            }

            if (null === $customerPaymentMethod) {

                throw new \Exception('No customer payment method');
            }

            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            // Check permissions
            if (!$this->discountManager->checkPermissionDiscount($user, $dispute)) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $form->setData($data);
            // Validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                // Save Form Data
                $resultSavedForm = $this->saveFormData($dispute, $formData);
                /** @var array $discountOutput */
                $discountOutput = $this->discountManager->getDiscountOutput($resultSavedForm['discount']);

                return $this->message()->success('Discount created', ['discount' => $discountOutput]);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function createFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $incoming_data = $this->processFormDataInit();

            if (true === $incoming_data['is_discount_done']) {

                throw new \Exception('Discount already done');
            }

            /** @var Dispute $dispute */
            $dispute = $incoming_data['dispute'];

            $is_discount_done = $this->discountManager->isDiscountDone($dispute);
            $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($dispute->getDeal());
            $paymentMethodBankTransfer = $customerPaymentMethod ? $customerPaymentMethod->getPaymentMethodBankTransfer() : null;

            $paymentMethodBankTransferOutput = null;
            if ($paymentMethodBankTransfer) {
                $paymentMethodBankTransferOutput = $this->paymentMethodManager->getPaymentMethodBankTransferOutput($paymentMethodBankTransfer);
            }

            // Форма для генерации платежки скидки
            $discountForm = new DiscountForm(
                $paymentMethodBankTransfer,
                $dispute,
                $incoming_data['max_discount']
            );

            //доп форма на случай если нет payment method
            $bankTransferForm = new BankTransferForm();

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData && !$is_discount_done  && $postData['type_form'] === DiscountForm::TYPE_FORM) {
                $discountForm->setData($postData);
                // Validate form
                if ($discountForm->isValid()) {
                    $formData = $discountForm->getData();
                    /** @var User $user */ // Can throw Exception
                    $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
                    // Check permissions
                    if (!$this->discountManager->checkPermissionDiscount($user, $dispute)) {

                        return $this->redirect()->toRoute('not-authorized');
                    }
                    // Save Form Data
                    $resultSavedForm = $this->saveFormData($dispute, $formData);
                    /** @var array $discountOutput */
                    $discountOutput = $this->discountManager->getDiscountOutput($resultSavedForm['discount']);

                    return $this->redirect()->toRoute('dispute-single',['id' => $dispute->getId()]);
                }
            }

            //форма добавления bank transfer
            if ($postData && !$is_discount_done && $postData['type_form'] === BankTransferForm::TYPE_FORM) {
                // платит всегда customer поэтому нужен его civilLawSubject
                /** @var CivilLawSubject $civilLawSubject */
                $civilLawSubject = $dispute->getDeal()->getCustomer()->getCivilLawSubject();
                $postData['civil_law_subject_id'] = $civilLawSubject->getId();
                $postData['payment_method_type'] = PaymentMethodType::BANK_TRANSFER;
                //set data to form
                $bankTransferForm->setData($postData);
                //validate form
                if ($bankTransferForm->isValid()) {
                    $formData = $bankTransferForm->getData();

                    $this->paymentMethodManager->createBankDetailsCustomerForReturnPaymentOrder($formData, $civilLawSubject);

                    return $this->redirect()->toRoute('dispute-single',['id'=>$dispute->getId()]);
                }
                if($this->getRequest()->isXmlHttpRequest()) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($bankTransferForm->getMessages());
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $discountForm->prepare();
        $bankTransferForm->prepare();
        $view = new TwigViewModel([
            'payment_method_bank_transfer' => $paymentMethodBankTransferOutput,
            'discountForm' => $discountForm,
            'bankTransferForm' => $bankTransferForm,
            'is_discount_done' => $is_discount_done
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return array|bool|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }
        try{
            $formDataInit = $this->processFormDataInit();

            if (!\is_array($formDataInit)) {

                return $this->message()->error('formInit failed');
            }

            $formDataInit['user'] = $this->userManager->extractUser($formDataInit['user']);
            if (null !== $formDataInit['dispute']) {
                $formDataInit['dispute'] = $this->disputeManager->getDisputeOutput($formDataInit['dispute']);
            }

            return $this->message()->success('formInit', $formDataInit);
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }
    }

    /**
     * Подготовка данных для инициализации формы
     *
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();
        // Get current user // Can throw Exception
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        $allowedUser = null;
        // process data for create
        $dispute = null;
        $is_discount_done = null;
        $customerPaymentMethod = null;
        $max_discount = null;
        if (isset($incomingData['idDispute'])) {
            /** @var Dispute $dispute */ // Can throw Exception
            $dispute = $this->disputeManager->getDispute($incomingData['idDispute']);
            // Проверка была ли уже выполнена скидка
            $is_discount_done = $this->discountManager->isDiscountDone($dispute);
            $max_discount = $this->discountManager->getMaxDiscount($dispute->getDeal());
        }

        return [
            // create
            'user' => $user,
            'dispute' => $dispute,
            'is_discount_done' => $is_discount_done,
            'max_discount' => $max_discount,
        ];
    }

    /**
     * @param $dispute
     * @param $formData
     * @return array
     * @throws \Throwable
     */
    private function saveFormData($dispute, $formData): array
    {
        //create
        /** @var Discount $discount */
        $discount = $this->discountManager->createDiscount($dispute, $formData);

        return [
            'discount' => $discount,
        ];
    }
}