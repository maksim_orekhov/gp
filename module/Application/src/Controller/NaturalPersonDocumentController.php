<?php

namespace Application\Controller;

use Core\Controller\AbstractRestfulController;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use ModuleRbac\Service\RbacManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Doctrine\ORM\EntityManager;
use Zend\Hydrator\ClassMethods;
use Zend\View\Model\ViewModel;

/**
 * Class NaturalPersonDocumentController
 * @package Application\Controller
 */
class NaturalPersonDocumentController extends AbstractRestfulController
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * Auth Manager
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * Natural Person Document Manager
     * @var \Application\Service\CivilLawSubject\NaturalPersonDocumentManager
     */
    private $naturalPersonDocumentManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * @var array
     */
    private $config;

    /**
     * NaturalPersonDocumentController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param NaturalPersonDocumentManager $naturalPersonDocumentManager
     * @param RbacManager $rbacManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                NaturalPersonDocumentManager $naturalPersonDocumentManager,
                                RbacManager $rbacManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->naturalPersonDocumentManager = $naturalPersonDocumentManager;
        $this->rbacManager = $rbacManager;
        $this->config = $config;
    }

    /**
     * @return ViewModel
     *
     * @TODO Перевести на twig!
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            $isOperator = true;
            $paginationParams = $this->getPaginationParams($this->config);
            $documents = $this->naturalPersonDocumentManager->getCollectionDocument($user, $paginationParams, $isOperator);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginator = null;
        if (method_exists($documents , 'getPages')) {
            $paginator = $documents->getPages();
        }
        // Get Output
        $naturalPersonDocumentOutput = $this->naturalPersonDocumentManager
            ->extractNaturalPersonDocumentCollection($documents);

        $data = [
            'paginator' => $paginator,
            'documents' => $naturalPersonDocumentOutput,
        ];
        if ($isAjax) {
            return $this->message()->success("natural person document", $data);
        }

        $template = 'application/natural-person-document/collection';
        $view =  new ViewModel($data);
        $view->setTemplate($template);

        return $view;
    }
}