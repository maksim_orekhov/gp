<?php

namespace Application\Controller;

use Application\Service\CivilLawSubject\CivilLawSubjectPolitics;
use Application\Service\CivilLawSubject\LegalEntityDocumentManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Controller\AbstractRestfulController;
use Application\Entity\LegalEntity;
use Application\Form\LegalEntityForm;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Core\Exception\LogicException;
use Core\Service\TwigViewModel;
use Zend\View\Model\ViewModel;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Entity\User;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Doctrine\ORM\EntityManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class LegalEntityController
 * @package Application\Controller
 */
class LegalEntityController extends AbstractRestfulController
{
    const SUCCESS_DELETE = 'LegalEntity successfully deleted';

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * User manager
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\CivilLawSubject\CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var \Application\Service\CivilLawSubject\LegalEntityManager
     */
    private $legalEntityManager;

    /**
     * @var LegalEntityDocumentManager
     */
    private $legalEntityDocumentManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * @var array
     */
    private $config;
    /**
     * @var CivilLawSubjectPolitics
     */
    private $civilLawSubjectPolitics;

    /**
     * PaymentMethodController constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param LegalEntityManager $legalEntityManager
     * @param LegalEntityDocumentManager $legalEntityDocumentManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param RbacManager $rbacManager
     * @param NdsTypeManager $ndsTypeManager
     * @param CivilLawSubjectPolitics $civilLawSubjectPolitics
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                LegalEntityManager $legalEntityManager,
                                LegalEntityDocumentManager $legalEntityDocumentManager,
                                PaymentMethodManager $paymentMethodManager,
                                RbacManager $rbacManager,
                                NdsTypeManager $ndsTypeManager,
                                CivilLawSubjectPolitics $civilLawSubjectPolitics,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->civilLawSubjectManager = $civilLawSubjectManager;
        $this->legalEntityManager = $legalEntityManager;
        $this->legalEntityDocumentManager = $legalEntityDocumentManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->rbacManager = $rbacManager;
        $this->ndsTypeManager = $ndsTypeManager;
        $this->config = $config;
        $this->civilLawSubjectPolitics = $civilLawSubjectPolitics;
    }

    /**
     * Show LegalEntity collection (GET)
     *
     * @return \Zend\Http\Response|ViewModel
     *
     * Права на доступ: Owner и Operator.
     * @throws \Exception
     */
    public function getList()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);

        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');
        // Check permissions // Important! Operator has specific interface of view
        if (!($profile === false && $isOperator) && !($profile === 'profile' && !$isOperator)) {

            return $this->redirect()->toRoute('not-authorized');
        }

        try {
            /** @var User $user */ // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            $paginationParams = $this->getPaginationParams($this->config);
            // Get legalEntity collection // Can throw Exception
            $legalEntities = $this->legalEntityManager
                ->getCollectionLegalEntity($user, $paginationParams, $isOperator);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginator = null;
        if (method_exists($legalEntities , 'getPages')) {
            $paginator = $legalEntities->getPages();
        }
        // Get Output
        $legalEntitiesOutput = $this->legalEntityManager
            ->extractLegalEntityCollection($legalEntities);

        $data = [
            'paginator'     => $paginator,
            'legalEntities' => $legalEntitiesOutput,
            'is_operator'    => $isOperator,
        ];

        // Ajax -
        if ($isAjax) {
            return $this->message()->success("LegalEntity collection", $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate("application/legal-entity/collection");

        return $view;
    }

    /**
     * Show LegalEntity details (GET)
     *
     * @param mixed $id
     * @return \Zend\Http\Response|ViewModel
     *
     * Права на доступ: Owner и Operator
     * @throws \Exception
     */
    public function get($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        // Если есть и равен 'profile', то пользователь, если нет - Оператор
        $profile = $this->params()->fromRoute('profile', false);

        try {
            /** @var LegalEntity $legalEntity */ // Can throw Exception
            $legalEntity = $this->legalEntityManager->getLegalEntityById($id);
            $documents = $this->legalEntityDocumentManager->getCollectionDocumentByLegalEntity($legalEntity,[]);

        } catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');
        // Check permissions // Important! Only Owner or Operator can see LegalEntity details
        if (!($profile === false && $isOperator) &&
            !($profile === 'profile' && $this->access('profile.own.edit', ['civilLawSubjectChild' => $legalEntity]))) {

            return $this->redirect()->toRoute('not-authorized');
        }
        // Extracted legalEntity
        $legalEntityOutput = $this->legalEntityManager
            ->getLegalEntityOutput($legalEntity);
        // Extracted documents legal entity
        $documentsOutput = $this->legalEntityDocumentManager
            ->extractLegalEntityDocumentCollection($documents);

        $paymentMethods = $legalEntity->getCivilLawSubject()->getPaymentMethods();
        $paymentMethodsOutput = $this->paymentMethodManager->extractPaymentMethodCollection($paymentMethods);

        $data = [
            'legalEntity'   => $legalEntityOutput,
            'is_operator'   => $isOperator,
            'paymentMethods' => $paymentMethodsOutput,
            'documents'      => $documentsOutput,
            'civil_law_subject_id' => $legalEntityOutput['civil_law_subject_id'],
        ];

        // Ajax -
        if ( $isAjax ){
            return $this->message()->success('LegalEntity single', $data);
        }
        // Http -
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity/single');

        return $view;
    }

    /**
     * @param mixed $data
     * @return array|TwigViewModel|mixed|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function create($data)
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }
        $ndsTypes = $this->ndsTypeManager->getActiveNdsTypes();
        $form = new LegalEntityForm($ndsTypes);

        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->selectUserByLogin($this->baseAuthManager->getIdentity());
            // Whether the currant user has role Operator
            if ($this->rbacManager->hasRole($user, 'Operator')) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();

                $formData['user'] = $user;

                // Save Form Data
                $resultSavedForm = $this->civilLawSubjectManager
                    ->saveFormData($formData, CivilLawSubjectManager::LEGAL_ENTITY_NAME);
                // Hydrate $resultSavedForm for Ajax
                $hydratedData = $this->legalEntityManager->extractSetOfObjects($resultSavedForm);
                /** @var LegalEntity $legalEntity **/
                $legalEntity = $resultSavedForm['legalEntity'];
                $hydratedData['civil_law_subject_id'] = $legalEntity->getCivilLawSubject()->getId();

                return $this->message()->success('create legal entity', $hydratedData);
            }

            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            $formDataInit = $this->processFormDataInit();

            // Если не найден редактируемый объект, значит, его не в базе
            // или у пользователя нет прав его редактировать
            if (null === $formDataInit['legalEntity']) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $ndsTypes = $this->ndsTypeManager->getNdsTypes();
            $form = new LegalEntityForm($ndsTypes);

            //set data to form
            $form->setData($data);
            //validate form
            if ($form->isValid()) {
                $formData = $form->getData();
                $formData['idLegalEntity'] = $id;
                // Save Form Data
                $resultSavedForm = $this->civilLawSubjectManager->saveFormData($formData, CivilLawSubjectManager::LEGAL_ENTITY_NAME);
                // Hydrate $resultSavedForm for Ajax
                $hydratedData = $this->legalEntityManager->extractSetOfObjects($resultSavedForm);

                return $this->message()->success('update legal entity', $hydratedData);
            }
            // Form is not valid
            return $this->message()->invalidFormData($form->getMessages());

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @param mixed $id
     * @return bool|TwigViewModel|mixed|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function delete($id)
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var LegalEntity $legalEntity */ // Can throw Exception
            $legalEntity = $this->legalEntityManager->getLegalEntityById($id);

            // Check permissions // Important! Only owner can delete his legalEntity
            if (!$this->access('profile.own.edit', ['civilLawSubjectChild' => $legalEntity])) {

                return $this->redirect()->toRoute('not-authorized');
            }
            // Deletion
            $this->civilLawSubjectManager->remove($legalEntity->getCivilLawSubject());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Ajax success
        if ($isAjax) {

            return $this->message()->success(self::SUCCESS_DELETE);
        }
        // Http success
        return $this->redirect()->toRoute('user-profile/legal-entity');
    }

    /**
     * Form render
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function createFormAction()
    {
        // only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();
            // Create form
            $ndsTypes = $this->ndsTypeManager->getNdsTypes();
            $form = new LegalEntityForm($ndsTypes);

            if (!isset($formDataInit['user']) || !$formDataInit['user'] instanceof User) {

                throw new \Exception('bad data provided: not found user');
            }
            $user = $formDataInit['user'];

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['user'] = $user;
                    $resultSavedForm = $this->civilLawSubjectManager->saveFormData($formData, CivilLawSubjectManager::LEGAL_ENTITY_NAME);

                    //http success
                    return $this->redirect()->toRoute('profile');
                }
            }
        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        $view = new TwigViewModel([
            'legalEntityForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity/create-form');

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function editFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();

            $ndsTypes = $this->ndsTypeManager->getNdsTypes();
            $form = new LegalEntityForm($ndsTypes);

            if ( !isset($formDataInit['legalEntity']) || !$formDataInit['legalEntity'] instanceof LegalEntity) {

                throw new LogicException(null, LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_LEGAL_ENTITY);
            }
            /** @var LegalEntity $legalEntity */
            $legalEntity = $formDataInit['legalEntity'];
            // Extracted legalEntity
            $legalEntityOutput = $this->legalEntityManager
                ->getLegalEntityOutput($formDataInit['legalEntity']);

            // Set data to form
            $form = $this->legalEntityManager->setFormData($form, $legalEntity);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if($postData) {
                //set data to form
                $form->setData($postData);
                //validate form
                if ($form->isValid()) {
                    $formData = $form->getData();
                    $formData['idLegalEntity'] = $legalEntity->getId();
                    $resultSavedForm = $this->civilLawSubjectManager->saveFormData($formData, CivilLawSubjectManager::LEGAL_ENTITY_NAME);

                    //http success
                    return $this->redirect()->toRoute('user-profile/legal-entity');
                }
            }

        } catch (\Throwable $t){

            return $this->message()->exception($t);
        }
        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole(null, 'Operator');

        $form->prepare();
        $view = new TwigViewModel([
            'legalEntity'       => $legalEntityOutput,
            'legalEntityForm'   => $form,
            'is_operator'       => $isOperator
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('application/legal-entity/edit-form');

        return $view;
    }

    /**
     * Remove LegalEntity (GET)
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function deleteFormAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $formDataInit = $this->processFormDataInit();

            if ( !isset($formDataInit['legalEntity']) || !$formDataInit['legalEntity'] instanceof LegalEntity) {

                throw new LogicException(null,
                    LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_LEGAL_ENTITY);
            }
            // Extracted legalEntity
            $legalEntityOutput = $this->legalEntityManager->getLegalEntityOutput($formDataInit['legalEntity']);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }

        // Http -
        $view = new TwigViewModel([
            'legalEntity' => $legalEntityOutput
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $formDataInit = $this->processFormDataInit();

        // Hydrate collection and objects for Ajax
        $formDataInit['user'] = $this->userManager->extractUser($formDataInit['user']);
        $formDataInit['allowedUser'] = $this->userManager->extractUser($formDataInit['allowedUser']);
        $formDataInit['legalEntity'] =
            $this->legalEntityManager->extractLegalEntity($formDataInit['legalEntity']);

        $formDataInit['allowedLegalEntities'] = $formDataInit['allowedLegalEntities']
            ? $this->legalEntityManager->extractLegalEntityCollection($formDataInit['allowedLegalEntities'])
            : null;

        $formDataInit['handbooks']['ndsTypes'] = $this->ndsTypeManager->extractNdsTypeCollection($formDataInit['handbooks']['ndsTypes']);

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * Подготовка данных для инициализации формы
     *
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }
        $allowedUser = null;

        //process data for update or delete
        $legalEntity = null;
        if (isset($incomingData['idLegalEntity']) || isset($incomingData['id'])) {

            $id = isset($incomingData['id']) ? $incomingData['id'] : $incomingData['idLegalEntity'];

            /** @var LegalEntity $legalEntity */
            $legalEntity = $this->legalEntityManager->getLegalEntityById((int) $id);

            // Check if legalEntity belongs to current user
            if ($legalEntity && $legalEntity->getCivilLawSubject()->getUser() !== $user) {

                $legalEntity = null;
            }
        }

        $allowedLegalEntities = null;
        if (null !== $user) {
            $allowedLegalEntities = $this->legalEntityManager->getAllowedLegalEntityByUser($user);
        }
        $ndsTypes = $this->ndsTypeManager->getActiveNdsTypes();

        return [
            //create
            'user' => $user,
            'allowedUser' => $allowedUser,
            //edit
            'legalEntity' => $legalEntity,
            'allowedLegalEntities' => $allowedLegalEntities,
            //handbooks
            'handbooks' => [
                'ndsTypes' => $ndsTypes
            ]
        ];
    }
}