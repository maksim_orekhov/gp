<?php

namespace Application\Controller;

use Application\Entity\User;
use Application\Form\UpdateBankForm;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleBank\Entity\Bank;
use ModuleBank\Service\BankManager;
use ModuleRbac\Service\RbacManager;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

/**
 * Class BankController
 * @package Application\Controller
 */
class BankController extends AbstractRestfulController
{
    /**
     * @var BankManager
     */
    private $bankManager;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * BankController constructor.
     * @param BankManager $bankManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param RbacManager $rbacManager
     */
    public function __construct(BankManager $bankManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                RbacManager $rbacManager)
    {
        $this->bankManager = $bankManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->rbacManager = $rbacManager;
    }

    /**
     * @return array|bool|TwigViewModel|JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function searchAction()
    {
        //only ajax and post
        if (!$this->getRequest()->isXmlHttpRequest() || !$this->getRequest()->isPost()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $data['bik'] = $this->params()->fromQuery('bik', null);
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()) {
            $content = $this->getRequest()->getContent();
            $data = Json::decode($content, Json::TYPE_ARRAY);
        }

        $bik = $data['bik'] ?? null;
        try {
            $bank = $this->bankManager->getBankByBik($bik);
            $bankOutput = $bank ? $this->bankManager->getBankOutput($bank) : [];

            if(empty($bankOutput)) {

                return $this->message()->error('information on BIK not found');
            }

            return $this->message()->success('information on BIK found successfully', $bankOutput);

        }catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function bankUpdateAction()
    {
        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if(! $this->rbacManager->hasRole($user, 'Operator') ){
                return $this->redirect()->toRoute('not-authorized');
            }
            $update_result = [];
            $updateBankForm = new UpdateBankForm();
            /** @var Bank $bankWithMaxDate */
            $bankWithMaxDate = $this->bankManager->getBankWithMaxDate();
            $lastUpdate = null;
            if ($bankWithMaxDate) {
                $lastUpdate = $bankWithMaxDate->getCreated()->format('Y-m-d H:i:s');
            }
             // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                //set data to form
                $updateBankForm->setData($postData);
                //validate form
                if (!$updateBankForm->isValid() && $this->getRequest()->isXmlHttpRequest()) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($updateBankForm->getMessages());
                }
                if ($updateBankForm->isValid()) {
                    try {
                        $update_result = $this->bankManager->updateBankBase();

                    } catch (\Throwable $t) {
                        $update_result = [
                            'status' => 'ERROR',
                            'message' => $t->getMessage(),
                            'data' => null
                        ];
                    }
                    //success ajax
                    if ($this->getRequest()->isXmlHttpRequest()) {
                        //
                        return $this->message()->success($update_result['message'], $update_result['data']);
                    }
                }
            }
            $view = new TwigViewModel([
                'updateBankForm' => $updateBankForm,
                'lastUpdate' => $lastUpdate,
                'update_result' => $update_result
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

            return $view;
        }
        catch (\Throwable $t) {
            return $this->message()->error($t->getMessage());
        }
    }
}