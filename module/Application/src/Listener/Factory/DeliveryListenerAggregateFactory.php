<?php
namespace Application\Listener\Factory;

use Application\Listener\DeliveryListenerAggregate;
use Application\Service\DeliveryManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryListenerAggregateFactory
 * @package Application\Listener\Factory
 */
class DeliveryListenerAggregateFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $deliveryManager = $container->get(DeliveryManager::class);

        return new DeliveryListenerAggregate(
            $deliveryManager
        );
    }
}