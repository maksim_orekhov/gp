<?php
namespace Application\Listener\Factory;

use Application\Service\Dispute\TribunalManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Listener\TribunalRequestListener;
use Zend\EventManager\EventManager;

/**
 * Class TribunalRequestListenerFactory
 * @package Application\Listener\Factory
 */
class TribunalRequestListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $tribunalManager = $container->get(TribunalManager::class);
        $events = new EventManager();

        return new TribunalRequestListener(
            $tribunalManager, $events
        );
    }
}