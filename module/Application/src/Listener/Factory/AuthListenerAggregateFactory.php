<?php
namespace Application\Listener\Factory;

use Application\Listener\AuthListenerAggregate;
use Application\Service\Deal\DealAgentManager;
use Application\Service\Deal\DealManager;
use Application\Service\TokenManager;
use Core\EventManager\AuthEventProvider;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\SessionContainerManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class AuthListenerAggregateFactory
 * @package Application\Listener\Factory
 */
class AuthListenerAggregateFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $tokenManager = $container->get(TokenManager::class);
        $baseEmailManager = $container->get(BaseEmailManager::class);
        $dealManager = $container->get(DealManager::class);
        $dealAgentManager = $container->get(DealAgentManager::class);
        $sessionContainerManager = $container->get(SessionContainerManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $baseUserManager = $container->get(BaseUserManager::class);
        $authEventManager = $container->get(AuthEventProvider::class)->getEventManager();

        return new AuthListenerAggregate(
            $tokenManager,
            $baseEmailManager,
            $dealManager,
            $dealAgentManager,
            $sessionContainerManager,
            $baseAuthManager,
            $baseUserManager,
            $authEventManager
        );
    }
}