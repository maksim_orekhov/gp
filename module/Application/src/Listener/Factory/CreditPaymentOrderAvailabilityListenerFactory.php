<?php
namespace Application\Listener\Factory;

use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Listener\CreditPaymentOrderAvailabilityListener;
use Zend\EventManager\EventManager;

/**
 * Class CreditPaymentOrderAvailabilityListenerFactory
 * @package Application\Listener\Factory
 */
class CreditPaymentOrderAvailabilityListenerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CreditPaymentOrderAvailabilityListener|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $payOffManager = $container->get(PayOffManager::class);
        $events = new EventManager();

        return new CreditPaymentOrderAvailabilityListener(
            $payOffManager,
            $events
        );
    }
}