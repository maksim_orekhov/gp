<?php
namespace Application\Listener\Factory;

use Application\Listener\RefundConfirmSuccessListener;
use Application\Service\Dispute\RefundManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\EventManager\EventManager;

/**
 * Class RefundConfirmSuccessListenerFactory
 * @package Application\Listener\Factory
 */
class RefundConfirmSuccessListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $refundManager = $container->get(RefundManager::class);
        $events = new EventManager();

        return new RefundConfirmSuccessListener(
            $refundManager, $events
        );
    }
}