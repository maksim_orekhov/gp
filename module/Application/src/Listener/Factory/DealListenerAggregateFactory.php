<?php
namespace Application\Listener\Factory;

use Application\Listener\DealListenerAggregate;
use Application\Service\Deal\DealManager;
use Core\EventManager\AuthEventProvider;
use Core\EventManager\NotifyEventProvider;
use Interop\Container\ContainerInterface;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DealListenerAggregateFactory
 * @package Application\Listener\Factory
 */
class DealListenerAggregateFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealManager = $container->get(DealManager::class);
        $authEventManager = $container->get(AuthEventProvider::class)->getEventManager();
        $notifyEventManager = $container->get(NotifyEventProvider::class)->getEventManager();
        $deliveryEventManager = $container->get(DeliveryEventProvider::class)->getEventManager();

        return new DealListenerAggregate(
            $dealManager,
            $authEventManager,
            $notifyEventManager,
            $deliveryEventManager
        );
    }
}