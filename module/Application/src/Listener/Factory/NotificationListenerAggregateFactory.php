<?php
namespace Application\Listener\Factory;

use Application\Listener\NotificationListenerAggregate;
use Application\Service\Notification\EmailNotificationManager;
use Application\Service\Notification\SmsNotificationManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class NotificationListenerAggregateFactory
 * @package Application\Listener\Factory
 */
class NotificationListenerAggregateFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return NotificationListenerAggregate|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $emailNotificationManager = $container->get(EmailNotificationManager::class);
        $smsNotificationManager = $container->get(SmsNotificationManager::class);

        return new NotificationListenerAggregate(
            $emailNotificationManager,
            $smsNotificationManager
        );
    }
}