<?php
namespace Application\Listener\Factory;

use Application\Listener\TribunalConfirmSuccessListener;
use Application\Service\Dispute\TribunalManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\EventManager\EventManager;

/**
 * Class DiscountConfirmSuccessListenerFactory
 * @package Application\Listener\Factory
 */
class TribunalConfirmSuccessListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $tribunalManager = $container->get(TribunalManager::class);
        $events = new EventManager();

        return new TribunalConfirmSuccessListener(
            $tribunalManager, $events
        );
    }
}