<?php
namespace Application\Listener\Factory;

use Application\Service\Dispute\ArbitrageManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Listener\ArbitrageRequestListener;
use Zend\EventManager\EventManager;

/**
 * Class ArbitrageRequestListenerFactory
 * @package Application\Listener\Factory
 */
class ArbitrageRequestListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $arbitrageManager = $container->get(ArbitrageManager::class);
        $events = new EventManager();

        return new ArbitrageRequestListener(
            $arbitrageManager, $events
        );
    }
}