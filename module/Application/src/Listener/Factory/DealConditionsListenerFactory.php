<?php
namespace Application\Listener\Factory;

use Application\Provider\PdfProvider;
use Application\Service\Deal\DealAgentManager;
use Application\Service\InvoiceManager;
use Application\Service\SettingManager;
use Core\Service\TwigRenderer;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Listener\DealConditionsListener;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealChangePolitics;
use Application\Service\Deal\DealChangesManager;
use Application\Service\UserManager;
use Core\Service\Auth\AuthService;
use Application\Provider\Mail\DealNotificationSender;
use Application\Provider\Mail\DealConfirmationSender;

/**
 * Class DealConditionsListenerFactory
 * @package Application\Listener\Factory
 */
class DealConditionsListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealManager = $container->get(DealManager::class);
        $dealAgentManager = $container->get(DealAgentManager::class);
        $dealChangePolitics = $container->get(DealChangePolitics::class);
        $dealChangesManager = $container->get(DealChangesManager::class);
        $userManager = $container->get(UserManager::class);
        $authService = $container->get(AuthService::class);
        $config = $container->get('Config');
        $twigRenderer = $container->get(TwigRenderer::class);
        $pdfProvider = $container->get(PdfProvider::class);
        $notificationMailSender = new DealNotificationSender($twigRenderer, $config);
        $confirmationMailSender = new DealConfirmationSender($twigRenderer, $pdfProvider, $config);
        $settingManager = $container->get(SettingManager::class);
        $invoiceManager = $container->get(InvoiceManager::class);

        return new DealConditionsListener(
            $dealManager,
            $dealAgentManager,
            $dealChangesManager,
            $dealChangePolitics,
            $userManager,
            $authService,
            $notificationMailSender,
            $confirmationMailSender,
            $settingManager,
            $invoiceManager,
            $config
        );
    }
}