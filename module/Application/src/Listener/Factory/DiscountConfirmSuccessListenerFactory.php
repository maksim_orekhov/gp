<?php
namespace Application\Listener\Factory;

use Application\Listener\DiscountConfirmSuccessListener;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Payment\PaymentMethodManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\EventManager\EventManager;

/**
 * Class DiscountConfirmSuccessListenerFactory
 * @package Application\Listener\Factory
 */
class DiscountConfirmSuccessListenerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DiscountConfirmSuccessListener|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $discountManager = $container->get(DiscountManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $events = new EventManager();

        return new DiscountConfirmSuccessListener(
            $discountManager,
            $paymentMethodManager,
            $events
        );
    }
}