<?php
namespace Application\Listener\Factory;

use Application\Listener\ApplicationListenerAggregate;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class ApplicationListenerAggregateFactory
 * @package Application\Listener\Factory
 */
class ApplicationListenerAggregateFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ApplicationListenerAggregate|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $application = $container->get('application');
        // Get service manager
        $serviceManager = $application->getServiceManager();

        return new ApplicationListenerAggregate(
            $serviceManager
        );
    }
}