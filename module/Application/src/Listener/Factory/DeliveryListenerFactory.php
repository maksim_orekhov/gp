<?php
namespace Application\Listener\Factory;

use Application\Provider\Mail\TrackingNumberSender;
use Core\Service\TwigRenderer;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Listener\DeliveryListener;

/**
 * Class DeliveryListenerFactory
 * @package Application\Listener\Factory
 */
class DeliveryListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $twigRenderer = $container->get(TwigRenderer::class);
        $trackingNumberSender = new TrackingNumberSender($twigRenderer, $config);

        return new DeliveryListener(
            $trackingNumberSender,
            $config
        );
    }
}