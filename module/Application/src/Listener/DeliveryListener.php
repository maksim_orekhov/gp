<?php

namespace Application\Listener;

use Application\Entity\Deal;
use Application\Entity\Delivery;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use ModuleCode\Service\SmsStrategyContext;
use Application\Provider\Mail\TrackingNumberSender;
use ModuleDelivery\Entity\DeliveryTrackingNumber;
use ModulePaymentOrder\Service\PayOffManager;

/**
 * Class DeliveryListener
 * @package Application\Listener
 */
class DeliveryListener
{
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    private $email_notify_to_tracking_number;
    private $sms_notify_to_tracking_number;

    /**
     * @var Delivery
     */
    private $delivery;

    /**
     * @var TrackingNumberSender
     */
    private $trackingNumberSender;
    /**
     * @var DeliveryTrackingNumber
     */
    private $deliveryTrackingNumber;
    /**
     * @var array
     */
    private $config;

    /**
     * DeliveryListener constructor.
     * @param TrackingNumberSender $trackingNumberSender
     * @param array $config
     */
    public function __construct(TrackingNumberSender $trackingNumberSender,
                                array $config)
    {
        $this->trackingNumberSender = $trackingNumberSender;
        $this->config = $config;

        // set notify params from config
        $this->email_notify_to_tracking_number = $this->getDealNotifyConfig($this->config, 'tracking_number', 'email');
        $this->sms_notify_to_tracking_number = $this->getDealNotifyConfig($this->config, 'tracking_number', 'sms');
    }

    /**
     * @param OnFlushEventArgs $args
     * @return null
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $entities = [];
        $entityManager = $args->getEntityManager();
        $unitOfWork = $entityManager->getUnitOfWork();
        if( !empty($unitOfWork->getScheduledEntityUpdates()) ) {
            $entities = $unitOfWork->getScheduledEntityUpdates();
        } elseif ( !empty($unitOfWork->getScheduledEntityInsertions() ) ) {
            $entities = $unitOfWork->getScheduledEntityInsertions();
        }
        foreach ($entities as $entity) {
            if ($entity instanceof Delivery) {
                $this->delivery = $entity;
            }
            if ($entity instanceof DeliveryTrackingNumber) {
                $this->deliveryTrackingNumber = $entity;
            }
        }
        if (!$this->delivery && !$this->deliveryTrackingNumber){
            return $this->exitPostFlush();
        }
    }

    /**
     * @param PostFlushEventArgs $args
     * @return null
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        try {
            //Удаляем событие чтобы избежать рекурсии
            $args->getEntityManager()->getEventManager()->removeEventListener('postFlush', $this);
            if ($this->deliveryTrackingNumber) {
                ///// уведомляем о добавлении трек-номера /////
                $this->notifyCustomerAboutNewTrackNumber();
            }
        }
        catch (\Throwable $t) {

            logException($t, 'listener-errors');
        }

        return $this->exitPostFlush();
    }

    /**
     * если есть трек номер
     * иначе ничего не делаем
     * @return bool
     */
    private function notifyCustomerAboutNewTrackNumber(): bool
    {
        $trackingNumber = $this->deliveryTrackingNumber->getTrackingNumber();
        $deal = $this->deliveryTrackingNumber->getDeliveryOrder()->getDeal();

        if (!$trackingNumber || !$deal){
            return false;
        }
        if($this->email_notify_to_tracking_number) {
            try {
                $this->notifyCustomerAboutNewTrackNumberByEmail($trackingNumber, $deal);
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
        if($this->sms_notify_to_tracking_number) {
            try {
                $this->notifyCustomerAboutNewTrackNumberBySms($trackingNumber, $deal);
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param $trackingNumber
     * @param Deal $deal
     * @throws \Exception
     */
    private function notifyCustomerAboutNewTrackNumberBySms($trackingNumber, Deal $deal)
    {
        $smsMessage = $this->getCustomerTrackingNumberSmsMessage($deal, $trackingNumber);
        $userPhone = $deal->getCustomer()->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param $trackingNumber
     * @param Deal $deal
     * @throws \Exception
     */
    private function notifyCustomerAboutNewTrackNumberByEmail($trackingNumber, Deal $deal)
    {
        $email = $deal->getCustomer()->getEmail();
        $data = [
            'deal' => $deal,
            'agent_user_login' => $deal->getContractor()->getUser()->getLogin(),
            'counteragent_login' => $deal->getCustomer()->getUser()->getLogin(),
            'tracking_number' => $trackingNumber
        ];

        $this->trackingNumberSender->sendMail($email, $data);
    }

    /**
     * @return null
     */
    private function exitPostFlush()
    {
        $this->delivery = null;
        $this->deliveryTrackingNumber = null;

        return null;
    }
}