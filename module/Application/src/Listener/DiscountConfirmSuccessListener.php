<?php
namespace Application\Listener;

use Application\Entity\Deal;
use Application\Entity\DiscountConfirm;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Service\Payment\PaymentMethodManager;
use Core\Exception\CriticalException;
use Application\Service\Dispute\DiscountManager;

/**
 * Class DiscountConfirmSuccessListener
 * @package Application\Listener
 */
class DiscountConfirmSuccessListener
{
    /**
     * @var \Zend\EventManager\EventManager
     */
    private $events;
    /**
     * @var DiscountManager
     */
    private $discountManager;
    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * DiscountConfirmSuccessListener constructor.
     * @param DiscountManager $discountManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param $events
     */
    public function __construct(DiscountManager $discountManager,
                                PaymentMethodManager $paymentMethodManager,
                                $events)
    {
        $this->discountManager = $discountManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->events = $events;
    }

    /**
     * Subscription on Event "onConfirmSuccess"
     * @throws \Exception
     */
    public function subscribe()
    {
        $this->events->attach('onDiscountConfirmSuccess', function ($e) {
            $params = $e->getParams();
            // Create Discount
            return $this->discountManager->createDiscount($params['dispute'], $params['params']);
        });
    }

    /**
     * @param DiscountConfirm $discountConfirm
     * @return mixed
     * @throws CriticalException
     */
    public function trigger(DiscountConfirm $discountConfirm)
    {
        /**
         * @var PaymentMethodBankTransfer  $paymentMethodBankTransfer
         * @var DiscountRequest  $discountRequest
         * @var Dispute  $dispute
         * @var Deal  $deal
         */
        try{
            $discountRequest = $discountConfirm->getDiscountRequest();
            $dispute = $discountRequest->getDispute();
            $deal = $dispute->getDeal();

            $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($deal);
            $paymentMethodBankTransfer = $customerPaymentMethod ? $customerPaymentMethod->getPaymentMethodBankTransfer() : null;

            $params=[
                'bank_transfer_id' => $paymentMethodBankTransfer ? $paymentMethodBankTransfer->getId() : '',
                'amount_discount' => $discountRequest->getAmount(),
            ];
            // Generate and save Discount
            $result = $this->events->trigger('onDiscountConfirmSuccess', null,
                [   // Event $params
                    'dispute' => $dispute,
                    'params' => $params
                ])->last();

            return $result;
        }
        catch (\Throwable $t){

            logException($t,'listener-errors', 2);
            throw new CriticalException('DiscountConfirmSuccessListener: '. $t->getMessage());
        }
    }
}