<?php
namespace Application\Listener;

use Application\Controller\DealController;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Service\Deal\DealManager;
use Core\EventManager\AuthEventProvider as AuthEvent;
use Core\EventManager\NotifyEventProvider as NotifyEvent;
use Core\Exception\LogicException;
use Core\Listener\ListenerAggregateTrait;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Application\EventManager\DealEventProvider as DealEvent;
use Zend\Mvc\MvcEvent;

/**
 * Class DealListenerAggregate
 * @package Application\Listener
 */
class DealListenerAggregate implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var EventManagerInterface
     */
    private $authEventManager;

    /**
     * @var EventManagerInterface
     */
    private $notifyEventManager;

    /**
     * @var EventManagerInterface
     */
    private $deliveryEventManager;

    /**
     * DealListenerAggregate constructor.
     * @param DealManager $dealManager
     * @param EventManagerInterface $authEventManager
     * @param EventManagerInterface $notifyEventManager
     * @param EventManagerInterface $deliveryEventManager
     */
    public function __construct(DealManager $dealManager,
                                EventManagerInterface $authEventManager,
                                EventManagerInterface $notifyEventManager,
                                EventManagerInterface $deliveryEventManager)
    {
        $this->dealManager = $dealManager;
        $this->authEventManager = $authEventManager;
        $this->notifyEventManager = $notifyEventManager;
        $this->deliveryEventManager = $deliveryEventManager;
    }

    /**
     * @param EventManagerInterface $events
     * @param int $priority
     * @return void
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(DealEvent::EVENT_SUCCESS_CREATING, [$this, 'successCreating'], $priority);
        $this->listeners[] = $events->attach(DealEvent::EVENT_SUCCESS_EDITING, [$this, 'successEditing'], $priority);
        $this->listeners[] = $events->attach(DealEvent::EVENT_RESEND_INVITATION, [$this, 'resendInvitation'], $priority);

        $this->listeners[] = $events->attach(DealEvent::EVENT_CREATE_DELIVERY_ORDER_AFTER_CREATE_DEAL, [$this, 'createDeliveryOrderAfterCreateDeal'], $priority);
        $this->listeners[] = $events->attach(DealEvent::EVENT_EDIT_DELIVERY_ORDER_AFTER_CREATE_DEAL, [$this, 'editDeliveryOrderAfterCreateDeal'], $priority);
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     * @return void
     */
    public function successCreating(EventInterface $event)
    {
        $params = $event->getParam('success_creating_params', null);
        $event->setParam('success_creating_params', null);

        if (!\is_array($params) ||
            !array_key_exists('deal', $params)) {

            return;
        }

        /** @var Deal $deal */
        $deal = $params['deal'];

        //// 2 прикрепление файлов к сделке /////
        $this->attachFilesToDeal($deal, $this->mvcEvent->getParam(DealController::DEAL_FILES, null));

        //// 3.0 готовим данные для уведомлений
        $ownerDealAgent = $deal->getOwner();
        $counterDealAgent = $ownerDealAgent === $deal->getCustomer() ? $deal->getContractor() : $deal->getCustomer();
        $notify_param = [
            'deal' => $deal,
            'ownerDealAgent' => $ownerDealAgent,
            'counterDealAgent' => $counterDealAgent
        ];
        //// 3.1 уведомления для автора сделки
        $this->notificationForOwner($ownerDealAgent, $notify_param);
        //// 3.2 уведомления для контрагента
        $this->notificationForCounterAgent($counterDealAgent, $notify_param);
    }

    /**
     * @param EventInterface $event
     */
    public function successEditing(EventInterface $event)
    {
        /**
         * @var Deal $deal
         * @var DealAgent $counterDealAgent
         */
        $deal = $event->getParam('deal', null);
        $event->setParam('deal', null);
        if (null === $deal) {

            return;
        }

        $ownerDealAgent = $deal->getOwner();
        $counterDealAgent = $ownerDealAgent === $deal->getCustomer() ? $deal->getContractor() : $deal->getCustomer();
        $notify_param = [
            'deal' => $deal,
            'ownerDealAgent' => $ownerDealAgent,
            'counterDealAgent' => $counterDealAgent
        ];

        //// 1. уведомления для контрагента (если сменился)
        if ($counterDealAgent->getInvitationDate() === null) {
            $this->notificationForCounterAgent($counterDealAgent, $notify_param);
        }
    }

    /**
     * @param EventInterface $event
     * @throws LogicException
     */
    public function resendInvitation(EventInterface $event)
    {
        /**
         * @var Deal $deal
         * @var DealAgent $counterDealAgent
         * @var DealAgent $targetDealAgent
         * @var DealAgent $ownerDealAgent
         */
        $deal = $event->getParam('deal', null);
        $user = $event->getParam('user', null);
        $event->setParam('deal', null);
        $event->setParam('user', null);
        if (null === $deal || null === $user) {

            return;
        }

        $ownerDealAgent = $deal->getOwner();
        $counterDealAgent = $ownerDealAgent === $deal->getCustomer() ? $deal->getContractor() : $deal->getCustomer();
        $notify_param = [
            'deal' => $deal,
            'ownerDealAgent' => $ownerDealAgent,
            'counterDealAgent' => $counterDealAgent
        ];

        $customer = $deal->getCustomer();
        $contractor = $deal->getContractor();

        if ($customer->getUser() !== $user) {
            $targetDealAgent = $customer;
        } else {
            $targetDealAgent = $contractor;
        }

        if (!$targetDealAgent->getCivilLawSubject() && $this->dealManager->isAllowingTimeToResendInvitation($targetDealAgent)) {
            $notify_param['is_resend'] = true;
            if ($ownerDealAgent === $targetDealAgent) {
                //// уведомления для автора сделки
                $this->notificationForOwner($ownerDealAgent, $notify_param);
            } else {
                //// уведомления для контрагента
                $this->notificationForCounterAgent($counterDealAgent, $notify_param);
            }
        } else {

            throw new LogicException(null, LogicException::DEAL_INVITATION_NOT_SENT);
        }
    }

    /**
     * @param EventInterface $event
     * @throws LogicException
     */
    public function createDeliveryOrderAfterCreateDeal(EventInterface $event)
    {
        $params = $event->getParam('create_delivery_order_after_create_deal_params', null);
        $event->setParam('create_delivery_order_after_create_deal_params', null);

        if (!\is_array($params) ||
            !array_key_exists('deal', $params) ||
            !array_key_exists('delivery_data', $params)) {

            throw new LogicException(null, LogicException::DEAL_OR_DELIVERY_DATA_NOT_PROVIDED);
        }
        /** @var Deal $deal */
        $deal = $params['deal'];
        /** @var array $delivery_data */
        $delivery_data = $params['delivery_data'];

        //// создание DeliveryOrder
        $this->createDeliveryOrder($deal, $delivery_data);
    }

    /**
     * @param EventInterface $event
     * @throws LogicException
     */
    public function editDeliveryOrderAfterCreateDeal(EventInterface $event)
    {
        $params = $event->getParam('edit_delivery_order_after_create_deal_params', null);
        $event->setParam('edit_delivery_order_after_create_deal_params', null);

        if (!\is_array($params) ||
            !array_key_exists('delivery_order', $params) ||
            !array_key_exists('delivery_data', $params)) {

            throw new LogicException(null, LogicException::DEAL_OR_DELIVERY_DATA_NOT_PROVIDED);
        }

        /** @var DeliveryOrder $deliveryOrder */
        $deliveryOrder = $params['delivery_order'];
        /** @var array $delivery_data */
        $delivery_data = $params['delivery_data'];

        //// редактирование DeliveryOrder
        $this->editDeliveryOrder($deliveryOrder, $delivery_data);
    }

    /**
     * @param Deal $deal
     * @param array $delivery_data
     */
    private function createDeliveryOrder(Deal $deal, array $delivery_data)
    {
        $this->deliveryEventManager->trigger(DeliveryEventProvider::EVENT_CREATE_DELIVERY_ORDER, $this, [
            'create_delivery_order_params' => [
                'deal' => $deal,
                'delivery_data' => $delivery_data
            ]
        ]);
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @param array $delivery_data
     */
    private function editDeliveryOrder(DeliveryOrder $deliveryOrder, array $delivery_data)
    {
        $this->deliveryEventManager->trigger(DeliveryEventProvider::EVENT_EDIT_DELIVERY_ORDER, $this, [
            'edit_delivery_order_params' => [
                'delivery_order' => $deliveryOrder,
                'delivery_data' => $delivery_data
            ]
        ]);
    }

    /**
     * @param Deal $deal
     * @param null $files_data
     * @throws \Exception
     */
    private function attachFilesToDeal(Deal $deal, $files_data = null)
    {
        // Сохраняем файл и добавляем в коллекцию файлов сделки
        if ($files_data) {
            if ( array_key_exists('file', $files_data) ) {
                $file = $files_data['file'];
                $this->dealManager->saveDealFile($deal, $file);
            }

            if ( array_key_exists('files', $files_data) ) {
                foreach ($files_data['files'] as $file) {
                    $this->dealManager->saveDealFile($deal, $file);
                }
            }
        }
    }

    /**
     * @param DealAgent $counterDealAgent
     * @param array $notify_param
     */
    private function notificationForCounterAgent(DealAgent $counterDealAgent, array $notify_param)
    {
        //// sms уведомления
        $this->notifyEventManager->trigger(NotifyEvent::INVITATION_TO_DEAL_FOR_COUNTER_AGENT_BY_SMS, $this, [
            'params' => $notify_param,
        ]);
        //// email уведомления
        if ($counterDealAgent->getUser() === null) {
            //автоматическая регистрация контрагента сделки с последующим уведомлением
            $this->authEventManager->trigger(AuthEvent::EVENT_USER_AUTO_REGISTRATION, $this, [
                'user_auto_registration_params' => ['email' => $counterDealAgent->getEmail()],
                'notification' => [
                    'event' => NotifyEvent::REGISTRATION_AFTER_CREATE_DEAL_FOR_COUNTER_AGENT,
                    'params' => $notify_param,
                ]
            ]);
        } else {
            ///// уведомление для контрагента о приглашении к сделке /////
            $this->notifyEventManager->trigger(NotifyEvent::INVITATION_TO_DEAL_FOR_COUNTER_AGENT, $this, [
                'params' => $notify_param,
            ]);
        }
    }

    /**
     * @param DealAgent $ownerDealAgent
     * @param array $notify_param
     */
    private function notificationForOwner(DealAgent $ownerDealAgent, array $notify_param)
    {
        //// sms уведомления
        $this->notifyEventManager->trigger(NotifyEvent::INVITATION_TO_DEAL_FOR_OWNER_BY_SMS, $this, [
            'params' => $notify_param,
        ]);
        //// email уведомления
        if ($ownerDealAgent->getUser() === null) {
            // автоматическая регистрация автора сделки с последующим уведомлением
            $this->authEventManager->trigger(AuthEvent::EVENT_USER_AUTO_REGISTRATION, $this, [
                'user_auto_registration_params' => ['email' => $ownerDealAgent->getEmail()],
                'notification' => [
                    'event' => NotifyEvent::REGISTRATION_AFTER_CREATE_DEAL_FOR_OWNER,
                    'params' => $notify_param,
                ]
            ]);
        } else {
            ///// уведомление для автора о приглашении к сделке /////
            $this->notifyEventManager->trigger(NotifyEvent::INVITATION_TO_DEAL_FOR_OWNER, $this, [
                'params' => $notify_param,
            ]);
        }
    }
}