<?php
namespace Application\Listener;

use Application\Entity\Dispute;
use Application\Entity\TribunalConfirm;
use Application\Entity\TribunalRequest;
use Core\Exception\CriticalException;
use Application\Service\Dispute\TribunalManager;

/**
 * Class TribunalConfirmSuccessListener
 * @package Application\Listener
 */
class TribunalConfirmSuccessListener
{
    /**
     * @var \Zend\EventManager\EventManager
     */
    private $events;

    /**
     * @var TribunalManager
     */
    private $tribunalManager;

    /**
     * DiscountConfirmSuccessListener constructor.
     * @param TribunalManager $tribunalManager
     * @param $events
     */
    public function __construct(TribunalManager $tribunalManager, $events)
    {
        $this->tribunalManager = $tribunalManager;
        $this->events = $events;
    }

    /**
     * Subscription on Event "onTribunalConfirmSuccess"
     * @throws \Exception
     */
    public function subscribe()
    {
        $this->events->attach('onTribunalConfirmSuccess', function ($e) {
            $params = $e->getParams();
            // Create Discount
            return $this->tribunalManager->createTribunal($params['dispute']);
        });
    }

    /**
     * @param TribunalConfirm $tribunalConfirm
     * @return mixed
     * @throws CriticalException
     */
    public function trigger(TribunalConfirm $tribunalConfirm)
    {
        /**
         * @var TribunalRequest  $tribunalRequest
         * @var Dispute  $dispute
         */
        try{
            $tribunalRequest = $tribunalConfirm->getTribunalRequest();
            $dispute = $tribunalRequest->getDispute();

            // Generate and save Discount
            $result = $this->events->trigger('onTribunalConfirmSuccess', null,
                [   // Event $params
                    'dispute' => $dispute,
                ])->last();

            return $result;
        }
        catch (\Throwable $t){

            logException($t,'listener-errors', 2);
            throw new CriticalException('TribunalConfirmSuccessListener: '. $t->getMessage());
        }
    }
}