<?php
namespace Application\Listener;

use Application\Entity\Deal;
use Application\Entity\Dispute;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\RefundConfirm;
use Application\Entity\RefundRequest;
use Core\Exception\CriticalException;
use Application\Service\Dispute\RefundManager;

/**
 * Class RefundConfirmSuccessListener
 * @package Application\Listener
 */
class RefundConfirmSuccessListener
{
    /**
     * @var \Zend\EventManager\EventManager
     */
    private $events;

    /**
     * @var RefundManager
     */
    private $refundManager;

    /**
     * RefundConfirmSuccessListener constructor.
     * @param RefundManager $refundManager
     * @param $events
     */
    public function __construct(RefundManager $refundManager, $events)
    {
        $this->refundManager = $refundManager;
        $this->events = $events;
    }

    /**
     * Subscription on Event "onRefundConfirmSuccess"
     * @throws \Exception
     */
    public function subscribe()
    {
        $this->events->attach('onRefundConfirmSuccess', function ($e) {
            $params = $e->getParams();
            // Create Discount
            return $this->refundManager->createRefund($params['dispute'], $params['params']);
        });
    }

    /**
     * @param RefundConfirm $refundConfirm
     * @return mixed
     * @throws CriticalException
     */
    public function trigger(RefundConfirm $refundConfirm)
    {
        /**
         * @var PaymentMethodBankTransfer  $paymentMethodRefund
         * @var RefundRequest  $refundRequest
         * @var Dispute  $dispute
         * @var Deal  $deal
         */
        try{
            $refundRequest = $refundConfirm->getRefundRequest();
            $dispute = $refundRequest->getDispute();
            $deal = $dispute->getDeal();
            $paymentMethodRefund = $this->refundManager->getPaymentMethodRefundByDeal($deal);

            $params=[
                'bank_transfer_id' => $paymentMethodRefund ? $paymentMethodRefund->getId() : '',
            ];
            // Generate and save Refund
            $result = $this->events->trigger('onRefundConfirmSuccess', null,
                [   // Event $params
                    'dispute' => $dispute,
                    'params' => $params
                ])->last();

            return $result;
        }
        catch (\Throwable $t){

            logException($t,'listener-errors', 2);
            throw new CriticalException('RefundConfirmSuccessListener: '. $t->getMessage());
        }
    }
}