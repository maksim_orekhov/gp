<?php

namespace Application\Listener;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\User;
use Core\EventManager\NotifyEventProvider as NotifyEvent;
use Application\Service\Notification\EmailNotificationManager;
use Application\Service\Notification\SmsNotificationManager;
use Core\Exception\LogicException;
use Core\Listener\ListenerAggregateTrait;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

/**
 * Class NotificationListenerAggregate
 * @package Application\Listener\Notification
 */
class NotificationListenerAggregate implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /**
     * @var EmailNotificationManager
     */
    private $emailNotificationManager;
    /**
     * @var SmsNotificationManager
     */
    private $smsNotificationManager;

    /**
     * DealNotificationListener constructor.
     * @param EmailNotificationManager $emailNotificationManager
     * @param SmsNotificationManager $smsNotificationManager
     */
    public function __construct(EmailNotificationManager $emailNotificationManager,
                                SmsNotificationManager $smsNotificationManager)
    {
        $this->emailNotificationManager = $emailNotificationManager;
        $this->smsNotificationManager = $smsNotificationManager;
    }

    /**
     * @param EventManagerInterface $events
     * @param int $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(
            NotifyEvent::REGISTRATION_AFTER_CREATE_DEAL_FOR_OWNER,
            [$this, 'registrationAfterCreateDealForOwner'],
            $priority
        );
        $this->listeners[] = $events->attach(
            NotifyEvent::INVITATION_TO_DEAL_FOR_OWNER,
            [$this, 'invitationToDealForOwner'],
            $priority
        );
        $this->listeners[] = $events->attach(
            NotifyEvent::REGISTRATION_AFTER_CREATE_DEAL_FOR_COUNTER_AGENT,
            [$this, 'registrationAfterCreateDealForCounterAgent'],
            $priority
        );
        $this->listeners[] = $events->attach(
            NotifyEvent::INVITATION_TO_DEAL_FOR_COUNTER_AGENT,
            [$this, 'invitationToDealForCounterAgent'],
            $priority
        );
        $this->listeners[] = $events->attach(
            NotifyEvent::INVITATION_TO_DEAL_FOR_OWNER_BY_SMS,
            [$this, 'invitationToDealForOwnerBySms'],
            $priority
        );
        $this->listeners[] = $events->attach(
            NotifyEvent::INVITATION_TO_DEAL_FOR_COUNTER_AGENT_BY_SMS,
            [$this, 'invitationToDealForCounterAgentBySms'],
            $priority
        );
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     */
    public function invitationToDealForOwnerBySms(EventInterface $event)
    {
        $params = $event->getParam('params', null);
        if (!$params ||
            !array_key_exists('deal', $params) ||
            !array_key_exists('ownerDealAgent', $params)) {

            throw new LogicException(
                sprintf(LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER_MESSAGE.': "%s"','NotificationListenerAggregate'),
                LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER);
        }
        /** @var Deal $deal */
        $deal = $params['deal'];
        /** @var DealAgent $ownerDealAgent */
        $ownerDealAgent = $params['ownerDealAgent'];

        ///// отсылка sms на приглашение к сделке /////
        $this->smsNotificationManager->invitationOwnerDealAgentBySms($deal, $ownerDealAgent);
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     */
    public function invitationToDealForCounterAgentBySms(EventInterface $event)
    {
        $params = $event->getParam('params', null);
        if (!$params ||
            !array_key_exists('deal', $params) ||
            !array_key_exists('counterDealAgent', $params)) {

            throw new LogicException(
                sprintf(LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER_MESSAGE.': "%s"','NotificationListenerAggregate'),
                LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER);
        }
        /** @var Deal $deal */
        $deal = $params['deal'];
        /** @var DealAgent $counterDealAgent */
        $counterDealAgent = $params['counterDealAgent'];

        ///// отсылка sms на приглашение к сделке /////
        $this->smsNotificationManager->invitationCounterDealAgentBySms($deal, $counterDealAgent);
    }

    /**
     * @param EventInterface $event
     * @throws LogicException
     * @throws \Throwable
     */
    public function registrationAfterCreateDealForOwner(EventInterface $event)
    {
        $params = $event->getParam('params', null);
        if (!$params ||
            !array_key_exists('deal', $params) ||
            !array_key_exists('ownerDealAgent', $params) ||
            !array_key_exists('counterDealAgent', $params) ||
            !array_key_exists('user', $params) ||
            !array_key_exists('encoded_token', $params) ||
            !array_key_exists('password', $params)) {

            throw new LogicException(
                sprintf(LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER_MESSAGE.': "%s"','NotificationListenerAggregate'),
                LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER);
        }

        /** @var User $user */
        $user = $params['user'];
        /** @var Deal $deal */
        $deal = $params['deal'];
        /** @var DealAgent $counterDealAgent */
        $counterDealAgent = $params['counterDealAgent'];
        /** @var DealAgent $ownerDealAgent */
        $ownerDealAgent = $params['ownerDealAgent'];

        $prepare_data = [
            'login' => $user->getLogin(),
            'email' => $user->getEmail()->getEmail(),
            'password' => $params['password'],
            'encoded_token' => $params['encoded_token'],
            'deal' => $deal,
            'author_deal_agent_name' => $ownerDealAgent->getEmail(),
            'counter_deal_agent_name' => $counterDealAgent->getEmail(),
            'counter_deal_agent_role' => $counterDealAgent === $deal->getCustomer() ? 'CUSTOMER' : 'CONTRACTOR',
            'is_resend' => array_key_exists('is_resend', $params) ? $params['is_resend'] : false,
        ];

        $this->emailNotificationManager->registrationAfterCreateDealForOwner($ownerDealAgent, $prepare_data);
    }

    /**
     * @param EventInterface $event
     * @throws LogicException
     * @throws \Throwable
     */
    public function registrationAfterCreateDealForCounterAgent(EventInterface $event)
    {
        $params = $event->getParam('params', null);
        if (!$params ||
            !array_key_exists('deal', $params) ||
            !array_key_exists('ownerDealAgent', $params) ||
            !array_key_exists('counterDealAgent', $params) ||
            !array_key_exists('user', $params) ||
            !array_key_exists('encoded_token', $params) ||
            !array_key_exists('password', $params)) {

            throw new LogicException(
                sprintf(LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER_MESSAGE.': "%s"','NotificationListenerAggregate'),
                LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER);
        }

        /** @var User $user */
        $user = $params['user'];
        /** @var Deal $deal */
        $deal = $params['deal'];
        /** @var DealAgent $counterDealAgent */
        $counterDealAgent = $params['counterDealAgent'];
        /** @var DealAgent $ownerDealAgent */
        $ownerDealAgent = $params['ownerDealAgent'];

        $prepare_data = [
            'login' => $user->getLogin(),
            'email' => $user->getEmail()->getEmail(),
            'password' => $params['password'],
            'encoded_token' => $params['encoded_token'],
            'deal' => $deal,
            'author_deal_agent_name' => $ownerDealAgent->getEmail(),
            'counter_deal_agent_name' => $counterDealAgent->getEmail(),
            'counter_deal_agent_role' => $counterDealAgent === $deal->getCustomer() ? 'CUSTOMER' : 'CONTRACTOR',
            'is_resend' => array_key_exists('is_resend', $params) ? $params['is_resend'] : false,
        ];

        $this->emailNotificationManager->registrationAfterCreateDealForCounterAgent($counterDealAgent, $prepare_data);
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     * @throws \Throwable
     */
    public function invitationToDealForOwner(EventInterface $event)
    {
        $params = $event->getParam('params', null);
        if (!$params ||
            !array_key_exists('deal', $params) ||
            !array_key_exists('ownerDealAgent', $params) ||
            !array_key_exists('counterDealAgent', $params)) {

            throw new LogicException(
                sprintf(LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER_MESSAGE.': "%s"','NotificationListenerAggregate'),
                LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER);
        }
        /** @var Deal $deal */
        $deal = $params['deal'];
        /** @var DealAgent $counterDealAgent */
        $counterDealAgent = $params['counterDealAgent'];
        /** @var DealAgent $ownerDealAgent */
        $ownerDealAgent = $params['ownerDealAgent'];

        $prepare_data = [
            'deal' => $deal,
            'author_deal_agent_name' => $ownerDealAgent->getEmail(),
            'counter_deal_agent_name' => $counterDealAgent->getEmail(),
            'counter_deal_agent_role' => $counterDealAgent === $deal->getCustomer() ? 'CUSTOMER' : 'CONTRACTOR',
            'is_resend' => array_key_exists('is_resend', $params) ? $params['is_resend'] : false,
        ];

        $this->emailNotificationManager->invitationToDealForOwner($ownerDealAgent, $prepare_data);
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     * @throws \Throwable
     */
    public function invitationToDealForCounterAgent(EventInterface $event)
    {
        $params = $event->getParam('params', null);
        if (!$params ||
            !array_key_exists('deal', $params) ||
            !array_key_exists('ownerDealAgent', $params) ||
            !array_key_exists('counterDealAgent', $params)) {

            throw new LogicException(
                sprintf(LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER_MESSAGE.': "%s"','NotificationListenerAggregate'),
                LogicException::INCORRECT_DATA_PROVIDED_TO_LISTENER);
        }
        /** @var Deal $deal */
        $deal = $params['deal'];
        /** @var DealAgent $counterDealAgent */
        $counterDealAgent = $params['counterDealAgent'];
        /** @var DealAgent $ownerDealAgent */
        $ownerDealAgent = $params['ownerDealAgent'];

        $prepare_data = [
            'deal' => $deal,
            'author_deal_agent_name' => $ownerDealAgent->getEmail(),
            'counter_deal_agent_name' => $counterDealAgent->getEmail(),
            'counter_deal_agent_role' => $counterDealAgent === $deal->getCustomer() ? 'CUSTOMER' : 'CONTRACTOR',
            'is_resend' => array_key_exists('is_resend', $params) ? $params['is_resend'] : false,
        ];

        $this->emailNotificationManager->invitationToDealForCounterAgent($counterDealAgent, $prepare_data);
    }
}