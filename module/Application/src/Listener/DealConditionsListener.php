<?php
namespace Application\Listener;

use Application\Provider\Mail\DealNotificationSender;
use Application\Service\Deal\DealAgentManager;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealChangePolitics;
use Application\Service\Deal\DealChangesManager;
use Application\Service\InvoiceManager;
use Application\Service\SettingManager;
use Application\Service\UserManager;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\User;
use Core\Service\Auth\AuthService;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Application\Provider\Mail\DealConfirmationSender;
use ModuleCode\Service\SmsStrategyContext;

/**
 * Class DealConditionsListener
 * @package Application\Listener
 */
class DealConditionsListener
{
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    /**
     * notify params
     */
    private $email_notify_to_deal_confirmed;
    private $sms_notify_to_deal_confirmed;
    private $email_notify_to_agent_confirmed;
    private $sms_notify_to_agent_confirmed;
    private $email_notify_to_change;
    private $sms_notify_to_change;

    /**
     * @var array
     */
    private $deal_changes = array();

    /**
     * @var null|Deal
     */
    private $deal = null;

    /**
     * @var User
     */
    private $user;

    /**
     * @var User
     */
    private $userCounteragent;

    /**
     * @var array
     */
    private $counteragentDealAgent;

    /**
     * @var string
     */
    private $action = null;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DealChangesManager
     */
    private $dealChangesManager;

    /**
     * @var DealChangePolitics
     */
    private $dealChangePolitics;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * Auth manager.
     * @var \Core\Service\Auth\AuthService
     */
    private $authService;

    /**
     * @var \Application\Provider\Mail\DealNotificationSender
     */
    private $notificationMailSender;


    /**
     * @var \Application\Provider\Mail\DealConfirmationSender
     */
    private $confirmationMailSender;

    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * @var InvoiceManager
     */
    private $invoiceManager;

    /**
     * @var
     */
    private $config;

    /**
     * DealConditionsListener constructor.
     * @param DealManager $dealManager
     * @param DealAgentManager $dealAgentManager
     * @param DealChangesManager $dealChangesManager
     * @param DealChangePolitics $dealChangePolitics
     * @param UserManager $userManager
     * @param \Core\Service\Auth\AuthService $authService
     * @param DealNotificationSender $notificationMailSender
     * @param DealConfirmationSender $confirmationMailSender
     * @param SettingManager $settingManager
     * @param InvoiceManager $invoiceManager
     * @param array $config
     */
    public function __construct(DealManager $dealManager,
                                DealAgentManager $dealAgentManager,
                                DealChangesManager $dealChangesManager,
                                DealChangePolitics $dealChangePolitics,
                                UserManager $userManager,
                                AuthService $authService,
                                DealNotificationSender $notificationMailSender,
                                DealConfirmationSender $confirmationMailSender,
                                SettingManager $settingManager,
                                InvoiceManager $invoiceManager,
                                array $config)
    {
        $this->dealManager              = $dealManager;
        $this->dealAgentManager         = $dealAgentManager;
        $this->dealChangesManager       = $dealChangesManager;
        $this->dealChangePolitics       = $dealChangePolitics;
        $this->userManager              = $userManager;
        $this->authService              = $authService;
        $this->notificationMailSender   = $notificationMailSender;
        $this->confirmationMailSender   = $confirmationMailSender;
        $this->settingManager           = $settingManager;
        $this->invoiceManager           = $invoiceManager;
        $this->config                   = $config;

        // set notify params from config
        $this->email_notify_to_deal_confirmed = $this->getDealNotifyConfig($this->config, 'confirmed_deal', 'email');
        $this->sms_notify_to_deal_confirmed = $this->getDealNotifyConfig($this->config, 'confirmed_deal', 'sms');
        $this->email_notify_to_agent_confirmed = $this->getDealNotifyConfig($this->config, 'confirmed_agent', 'email');
        $this->sms_notify_to_agent_confirmed = $this->getDealNotifyConfig($this->config, 'confirmed_agent', 'sms');
        $this->email_notify_to_change = $this->getDealNotifyConfig($this->config, 'changed_deal', 'email');
        $this->sms_notify_to_change = $this->getDealNotifyConfig($this->config, 'changed_deal', 'sms');
    }

    /**
     * @param OnFlushEventArgs $args
     * @return null
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        // Try get user from Identity. If no user - exit
        try {
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->authService->getIdentity());
        }
        catch (\Throwable $t) {

            return null;
        }
        $this->user = $user;

        $entityManager = $args->getEntityManager();
        $unitOfWork = $entityManager->getUnitOfWork();

        $deal_condition_properties = $this->dealChangesManager->getDealProperties($unitOfWork, $user);

        // If no Deal - exit
        if( !isset($deal_condition_properties['deal']) || empty($deal_condition_properties['deal']))
        {
            return null;
        }
        $this->deal         = $deal_condition_properties['deal'];
        $this->deal_changes = $deal_condition_properties['deal_changed_properties'];
        $this->action       = $deal_condition_properties['action'];

        // If there are real property changes (except those listed in IGNORED_PROPERTIES constant)
        if ( !empty($deal_condition_properties['deal_changed_properties']) ) {

            $this->counteragentDealAgent = $this->dealManager->getUserCounterAgentByDeal($user, $this->deal);

            $this->userCounteragent = $this->counteragentDealAgent->getUser();

            if($this->dealChangePolitics->isCounteragentConfirmMustReset($deal_condition_properties['deal_changed_properties'])) {
                // Counteragent's DealAgent dealAgentConfirm reset
                $this->counteragentDealAgent->setDealAgentConfirm(false);
                // Counteragent's DealAgent dateOfConfirm reset
                $this->counteragentDealAgent->setDateOfConfirm(null);
                // Explicitly triggering a re-computation of the changeset of the affected entity
                $unitOfWork->recomputeSingleEntityChangeSet($entityManager->getClassMetadata(DealAgent::class), $this->counteragentDealAgent);
            }
        }
    }

    /**
     * @param PostFlushEventArgs $args
     * @return null
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        // if there is no change or editing then exit
        if(!$this->action === 'edit' || !$this->isDealConditionsChanged()){

            return $this->exitPostFlush();
        }
        try {
            // if the deal is confirmed by two agents
            if ( $this->dealManager->isConfirmed($this->deal) ) {

                return $this->processingConfirmationByTwoAgents($args);
            }

            // counteragent notification of changes
            return $this->notifyCounteragentAboutChangesDeal();
        } catch (\Throwable $t) {
            //save error in log
            logException($t,'listener-errors', 3);
            return $this->exitPostFlush();
        }
    }

    /**
     * @return bool
     */
    private function isDealConditionsChanged(): bool
    {
        return $this->deal && $this->deal_changes && $this->userCounteragent;
    }

    /**
     * @param PostFlushEventArgs $args
     * @return null
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    private function processingConfirmationByTwoAgents(PostFlushEventArgs $args)
    {
        // Удаляем событие чтобы избежать рекурсии, так как confirm вызывает flush
        $this->removeEventListener($args, 'postFlush', $this);
        //запускаем клонирование подчиненных сущностей
        $this->dealManager->cloneSubordinateEntities($this->deal);
        // Send notification mail to deal agents
        $this->notifyAgentsAboutConfirmedDeal();
        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($this->deal);

        return $this->exitPostFlush();
    }

    /**
     * @param PostFlushEventArgs $args
     * @param $events
     * @param $listener
     * @return bool
     */
    private function removeEventListener(PostFlushEventArgs $args, $events, $listener)
    {
        $args->getEntityManager()->getEventManager()->removeEventListener($events, $listener);

        return true;
    }

    /**
     * Send notification mail to deal agents
     * @return bool
     */
    private function notifyAgentsAboutConfirmedDeal()
    {
        // уведомления если оба агента подтвердили сделку
        if($this->email_notify_to_deal_confirmed) {
            try {
                $this->notifyContractorAboutConfirmedDealByEmail();
                $this->notifyCustomerAboutConfirmedDealByEmail();
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
        if($this->sms_notify_to_deal_confirmed) {
            try {
                $this->notifyContractorAboutConfirmedDealBySms();
                $this->notifyCustomerAboutConfirmedDealBySms();
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function notifyCustomerAboutConfirmedDealByEmail()
    {
        $dealOutput = $this->dealManager->getDealOutputForSingle($this->deal, $this->deal->getCustomer()->getUser());
        $systemPaymentDetailOutput = $this->settingManager->getSystemPaymentDetailOutput();

        $invoiceViewVars = [
            'deal'  => $dealOutput,
            'system_payment_detail' => $systemPaymentDetailOutput,
            'payment_purpose' => $this->invoiceManager->generateInvoicePaymentPurpose($this->deal),
            'nds_value' => $this->invoiceManager->getNdsValue($this->deal)
        ];

        $email = $this->deal->getCustomer()->getEmail();
        $counterAgent = $this->deal->getContractor();
        $data = [
            'deal' => $this->deal,
            'agent_user_login' => $this->deal->getCustomer()->getUser()->getLogin(),
            'counteragent_login' => $counterAgent->getUser()->getLogin(),
            'invoice_view_vars' => $invoiceViewVars,
            'type_notify' => DealConfirmationSender::TYPE_NOTIFY_CUSTOMER,
        ];
        // Send notification mail to Customer
        $this->confirmationMailSender->sendMail($email, $data);

        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function notifyContractorAboutConfirmedDealByEmail()
    {
        $email = $this->deal->getContractor()->getEmail();
        $counterAgent = $this->deal->getCustomer();
        $data = [
            'deal' => $this->deal,
            'agent_user_login' => $this->deal->getContractor()->getUser()->getLogin(),
            'counteragent_login' => $counterAgent->getUser()->getLogin(),
            'invoice_view_vars' => null,
            'type_notify'   => DealConfirmationSender::TYPE_NOTIFY_CONTRACTOR,
        ];
        // Send notification mail to Contractor
        $this->confirmationMailSender->sendMail($email, $data);

        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function notifyContractorAboutConfirmedDealBySms()
    {
        $dealAgent = $this->deal->getContractor();
        $smsMessage = $this->getConfirmedDealSmsMessage($this->deal);

        return $this->notifyAboutConfirmedDealBySms($dealAgent, $smsMessage);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function notifyCustomerAboutConfirmedDealBySms()
    {
        $dealAgent = $this->deal->getCustomer();
        $smsMessage = $this->getConfirmedDealSmsMessage($this->deal);

        return $this->notifyAboutConfirmedDealBySms($dealAgent, $smsMessage);
    }

    /**
     * @return null
     * @throws \Exception
     */
    private function notifyCounteragentAboutChangesDeal()
    {
        if (array_key_exists('Application\Entity\DealAgent', $this->deal_changes)
            && array_key_exists('dealAgentConfirm', $this->deal_changes['Application\Entity\DealAgent'])) {

            // уведомления если один из агентов подтвердил сделку
            if($this->email_notify_to_agent_confirmed) {
                try{
                    $this->notifyCounteragentAboutConfirmationFromDealAgentByEmail();
                } catch (\Throwable $t) {
                    logException($t, 'email-error');
                }
            }
            if($this->sms_notify_to_agent_confirmed) {
                try{
                    $this->notifyCounteragentAboutConfirmationFromDealAgentBySMS();
                } catch (\Throwable $t) {
                    logException($t, 'sms-error');
                }
            }

        }else{

            // уведомления если один из агентов изменил сделку
            if($this->email_notify_to_change) {
                try {
                    $this->notifyCounteragentAboutChangesDealByEmail();
                } catch (\Throwable $t) {
                    logException($t, 'email-error');
                }
            }
            if($this->sms_notify_to_change) {
                try {
                    $this->notifyCounteragentAboutChangesDealBySms();
                } catch (\Throwable $t) {
                    logException($t, 'sms-error');
                }
            }
        }
        return $this->exitPostFlush();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function notifyCounteragentAboutConfirmationFromDealAgentByEmail()
    {
        return $this->notifyCounteragent(DealNotificationSender::TYPE_NOTIFY_CONFIRMATION);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function notifyCounteragentAboutChangesDealByEmail()
    {
        return $this->notifyCounteragent(DealNotificationSender::TYPE_NOTIFY_CHANGE);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function notifyCounteragentAboutConfirmationFromDealAgentBySMS()
    {
        $smsMessage = $this->getConfirmedDealAgentSmsMessage($this->deal);
        return $this->notifyCounteragentBySms($smsMessage);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function notifyCounteragentAboutChangesDealBySms()
    {
        $smsMessage = $this->getChangedDealSmsMessage($this->deal);
        return $this->notifyCounteragentBySms($smsMessage);
    }

    /**
     * @param $type_notify
     * @return bool
     * @throws \Exception
     */
    private function notifyCounteragent($type_notify)
    {
        if($this->user === $this->deal->getCustomer()->getUser()) {
            /** @var DealAgent $counteragentDealAgent */
            $dealAgent = $this->deal->getCustomer();
            $dealCounterAgent = $this->deal->getContractor();
            $counter_deal_agent_role = 'CUSTOMER';
        } else {
            /** @var DealAgent $counteragentDealAgent */
            $dealAgent = $this->deal->getContractor();
            $dealCounterAgent = $this->deal->getCustomer();
            $counter_deal_agent_role = 'CONTRACTOR';
        }
        //если сушествует user
        if($dealCounterAgent->getUser()) {
            $email = $dealCounterAgent->getEmail();
            $data = [
                'deal' => $this->deal,
                'agent_user_login' => $this->user->getLogin(),
                'counteragent_login' => $dealCounterAgent->getUser()->getLogin(),
                'deal_changed_properties' => $this->deal_changes,
                'civil_law_subject_name' => $this->dealAgentManager->getCivilLawSubjectNameByDealAgent($dealAgent),
                'civil_law_subject_type' => $this->dealAgentManager->getCivilLawSubjectTypeByDealAgent($dealAgent),
                'counter_deal_agent_role' => $counter_deal_agent_role,
                DealNotificationSender::TYPE_NOTIFY_KEY => $type_notify
            ];
            $this->notificationMailSender->sendMail($email, $data);
        }
        return true;
    }

    /**
     * @param $smsMessage
     * @return bool
     * @throws \Exception
     */
    private function notifyCounteragentBySms($smsMessage)
    {
        if($this->user === $this->deal->getCustomer()->getUser()) {
            $dealCounterAgent = $this->deal->getContractor();
        } else {
            $dealCounterAgent = $this->deal->getCustomer();
        }
        //если сушествует user
        if($dealCounterAgent->getUser()) {
            $userPhone = $dealCounterAgent->getUser()->getPhone();
            $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
            if ( $smsStrategy->isAllowedSmsNotify() ) {
                // Sms provider selection (by provider balance and so on...)
                $smsProvider = $smsStrategy->getSmsProvider();
                // Send message
                $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
            } else {
                throw new \Exception('Forbidden by policy sms notification');
            }
        }

        return true;
    }

    /**
     * @param DealAgent $dealAgent
     * @param $smsMessage
     * @return bool
     * @throws \Exception
     */
    private function notifyAboutConfirmedDealBySms(DealAgent $dealAgent, $smsMessage)
    {
        $userPhone = $dealAgent->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }

        return true;
    }

    /**
     * @return null
     */
    private function exitPostFlush()
    {
        $this->deal = null;

        return null;
    }
}