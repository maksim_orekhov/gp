<?php
namespace Application\Listener;

use Application\EventManager\ApplicationEventProvider;
use Core\Listener\ListenerAggregateTrait;
use ModuleDeliveryDpd\EventManager\DeliveryDpdCatalogEventProvider;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\Application;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ApplicationListenerAggregate
 * @package Application\Listener
 */
class ApplicationListenerAggregate implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    protected $listeners = array();

    protected $serviceManager;

    public function __construct(ServiceLocatorInterface $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     * @param EventManagerInterface $events
     * @param int $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(
            ApplicationEventProvider::EVENT_DEVELOPER_TOOL_LISTENERS_DISABLE,
            [$this, 'onBigDataUpdating'],
            -9499
        );
    }

    /**
     * @param EventInterface $event
     */
    public function onBigDataUpdating(EventInterface $event)
    {
        try {
            $mvcEvent = $this->mvcEvent;
            /** @var Application $application */
            $application = $mvcEvent->getApplication();

            $eventManager = $application->getEventManager();

            $sharedEventManager = $eventManager->getSharedManager();

            // Отписываемся от слушателей, которые могут вызвать ошибку переполенения памяти из-за аккумуляции больших данных
            $eventManager->detach([$this->serviceManager->get(\ZendDeveloperTools\Listener\FlushListener::class), 'onFinish']);
            $eventManager->detach([$this->serviceManager->get(\ZendDeveloperTools\Listener\ProfilerListener::class), 'onFinish']);

            if (null !== $sharedEventManager) {
                $sharedEventManager->clearListeners('profiler');
            }
        }
        catch (\Throwable $t) {

            logMessage($t->getMessage(), 'dpd_listener');
        }
    }
}