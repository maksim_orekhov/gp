<?php
namespace Application\Listener;

use Application\Service\DeliveryManager;
use Core\Listener\ListenerAggregateTrait;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

/**
 * Class DeliveryListenerAggregate
 * @package ModuleDelivery\Listener
 */
class DeliveryListenerAggregate implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /**
     * @var DeliveryManager
     */
    private $deliveryManager;

    /**
     * DeliveryListenerAggregate constructor.
     * @param DeliveryManager $deliveryManager
     */
    public function __construct(DeliveryManager $deliveryManager)
    {
        $this->deliveryManager = $deliveryManager;
    }

    /**
     * @param EventManagerInterface $events
     * @param int $priority
     * @return void
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(
            DeliveryEventProvider::EVENT_DELIVERY_SERVICE_ORDER_CREATED,
            [$this, 'notifyAboutDeliveryServiceOrderCreation'],
            $priority
        );
        $this->listeners[] = $events->attach(
            DeliveryEventProvider::EVENT_PARCELS_DELIVERED,
            [$this, 'manageSuccessfulDeliveries'],
            $priority
        );
        $this->listeners[] = $events->attach(
            DeliveryEventProvider::EVENT_PARCELS_DELIVERY_FAILED,
            [$this, 'manageUnsuccessfulDeliveries'],
            1
        );
    }

    /**
     * @param EventInterface $event
     */
    public function notifyAboutDeliveryServiceOrderCreation(EventInterface $event)
    {
        $params = $event->getParam('delivery_service_order_created', null);
        $event->setParam('delivery_service_order_created', null);

        if (!\is_array($params) || !array_key_exists('deliveryTrackingNumber', $params)) {

            return;
        }

        $this->deliveryManager->notifyDealAgentsAboutDeliveryServiceOrderCreation(
            $params['deliveryTrackingNumber'],
            $params['trace_page_url']
        );
    }

    /**
     * @param EventInterface $event
     * @throws \Throwable
     * @return void
     */
    public function manageSuccessfulDeliveries(EventInterface $event)
    {
        $params = $event->getParam('parcels_delivered', null);
        $event->setParam('parcels_delivered', null);
        if (!\is_array($params) || !array_key_exists('delivered', $params)) {

            return;
        }

        try {
            $this->deliveryManager->manageSuccessfulDeliveries($params['delivered']);
        }
        catch (\Throwable $t) {

            logMessage($t->getMessage());

            throw $t;
        }
    }

    /**
     * @param EventInterface $event
     * @throws \Throwable
     * @return void
     */
    public function manageUnsuccessfulDeliveries(EventInterface $event)
    {
        $params = $event->getParam('parcels_delivery_failed', null);
        $event->setParam('parcels_delivery_failed', null);

        if (!\is_array($params) || !array_key_exists('failed', $params)) {

            return;
        }

        try {
            $this->deliveryManager->manageUnsuccessfulDeliveries($params['failed'], $params['trace_page_url']);
        }
        catch (\Throwable $t) {

            logMessage($t->getMessage());

            throw $t;
        }
    }
}