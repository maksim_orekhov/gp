<?php
namespace Application\Listener;

use Application\Entity\Deal;
use Application\Service\BankClient\BankClientManager;
use ModulePaymentOrder\Service\PayOffManager;

/**
 * Class CreditPaymentOrderAvailabilityListener
 * @package Application\Listener
 */
class CreditPaymentOrderAvailabilityListener
{
    /**
     * @var PayOffManager
     */
    private $payOffManager;

    /**
     * @var \Zend\EventManager\EventManager
     */
    private $events;

    /**
     * CreditPaymentOrderAvailabilityListener constructor.
     * @param PayOffManager $payOffManager
     * @param $events
     */
    public function __construct(PayOffManager $payOffManager, $events)
    {
        $this->payOffManager = $payOffManager;
        $this->events = $events;
    }

    /**
     * Subscription on Event "onCreditPaymentOrderAvailable"
     * @throws \Exception
     */
    public function subscribe()
    {
        $this->events->attach('onCreditPaymentOrderAvailable', function ($e) {
            $params = $e->getParams();
            /** @var Deal $deal */
            $deal = $params['deal'];
            //запускаем механизм выплаты
            $this->payOffManager->createPayOff($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);
        });
    }

    /**
     * Trigger Event "onCreditPaymentOrderAvailable"
     * @param $deal
     * @return mixed
     */
    public function trigger($deal)
    {
        // Generate and save PaymentOrder
        $result = $this->events->trigger('onCreditPaymentOrderAvailable', null,
            [   // Event $params
                'deal' => $deal,
            ])->last();

        return $result;
    }
}