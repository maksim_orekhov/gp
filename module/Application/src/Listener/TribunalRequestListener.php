<?php
namespace Application\Listener;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Service\Dispute\TribunalManager;

/**
 * Class TribunalRequestListener
 * @package Application\Listener
 */
class TribunalRequestListener
{
    /**
     * @var \Zend\EventManager\EventManager
     */
    private $events;

    /**
     * @var TribunalManager
     */
    private $tribunalManager;

    /**
     * TribunalRequestListener constructor.
     * @param $tribunalManager
     * @param $events
     */
    public function __construct(TribunalManager $tribunalManager, $events)
    {
        $this->tribunalManager = $tribunalManager;
        $this->events = $events;
    }

    /**
     * Subscription on Event "onRequestDeclineOverlimit"
     */
    public function subscribe()
    {
        $this->events->attach('onRequestDeclineOverlimit', function ($e) {
            $params = $e->getParams();
            // Create and save TribunalRequest
            return $this->tribunalManager->createTribunalRequest($params['deal'], $params['author']);
        });
    }

    /**
     * @param Deal $deal
     * @param DealAgent $author
     * @return mixed
     */
    public function trigger(Deal $deal, DealAgent $author)
    {
        // Generate and save PaymentOrder
        $result = $this->events->trigger('onRequestDeclineOverlimit', null,
            [   // Event $params
                'deal' => $deal,
                'author' => $author
            ])->last();

        return $result;
    }
}