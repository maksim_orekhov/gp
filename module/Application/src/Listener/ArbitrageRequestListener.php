<?php
namespace Application\Listener;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Service\Dispute\ArbitrageManager;

/**
 * Class ArbitrageRequestListener
 * @package Application\Listener
 */
class ArbitrageRequestListener
{
    /**
     * @var \Zend\EventManager\EventManager
     */
    private $events;

    /**
     * @var ArbitrageManager
     */
    private $arbitrageManager;

    /**
     * ArbitrageRequestListener constructor.
     * @param $arbitrageManager
     * @param $events
     */
    public function __construct(ArbitrageManager $arbitrageManager, $events)
    {
        $this->arbitrageManager = $arbitrageManager;
        $this->events = $events;
    }

    /**
     * Subscription on Event "onRequestDeclineOverlimit"
     */
    public function subscribe()
    {
        $this->events->attach('onRequestDeclineOverlimit', function ($e) {
            $params = $e->getParams();
            $this->arbitrageManager->setDisputeOutput($params['disputeOutput']);
            // Create and save ArbitrageRequest
            return $this->arbitrageManager->createArbitrageRequest($params['deal'], $params['author'], $params['request']);
        });
    }

    /**
     * @param Deal $deal
     * @param DealAgent $author
     * @param null $request
     * @return mixed
     */
    public function trigger(Deal $deal, DealAgent $author, array $disputeOutput, $request = null)
    {
        // Generate and save PaymentOrder
        $result = $this->events->trigger('onRequestDeclineOverlimit', null,
            [   // Event $params
                'deal' => $deal,
                'author' => $author,
                'disputeOutput' => $disputeOutput,
                'request' => $request
            ])->last();

        return $result;
    }
}