<?php

namespace Application\Listener\Notification;

use Application\Entity\Deal;
use Application\EventManager\DealEventProvider as DealEvent;
use Application\Service\Notification\EmailNotificationManager;
use Application\Service\Notification\SmsNotificationManager;
use Core\Exception\LogicException;
use Core\Listener\ListenerAggregateTrait;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Application\Service\Deal\DealStatus;
use Application\Service\PrepareConfigNotifyTrait;

/**
 * Class DealNotificationListener
 * @package Application\Listener\Notification
 */
class DealNotificationListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;
    use PrepareConfigNotifyTrait;

    private $email_notify_deal_closed;
    private $email_notify_to_invitation;
    private $sms_notify_to_invitation;
    private $sms_notify_deal_closed;

    /**
     * @var EmailNotificationManager
     */
    private $emailNotificationManager;

    /**
     * @var DealStatus
     */
    private $dealStatus;
    /**
     * @var SmsNotificationManager
     */
    private $smsNotificationManager;

    /**
     * DealNotificationListener constructor.
     * @param EmailNotificationManager $emailNotificationManager
     * @param SmsNotificationManager $smsNotificationManager
     * @param DealStatus $dealStatus
     * @param array $config
     */
    public function __construct(EmailNotificationManager $emailNotificationManager,
                                SmsNotificationManager $smsNotificationManager,
                                DealStatus $dealStatus,
                                array $config)
    {
        $this->emailNotificationManager = $emailNotificationManager;
        $this->smsNotificationManager = $smsNotificationManager;
        $this->dealStatus = $dealStatus;

        $this->email_notify_deal_closed = $this->getDealNotifyConfig($config, 'deal_closed', 'email');
        $this->email_notify_to_invitation = $this->getDealNotifyConfig($config, 'invitation_to_deal', 'email');
        $this->sms_notify_to_invitation = $this->getDealNotifyConfig($config, 'invitation_to_deal', 'sms');

        #$this->sms_notify_deal_closed = $this->getDealNotifyConfig($this->config, 'deal_closed', 'sms');
    }

    /**
     * @param EventManagerInterface $events
     * @param int $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
//        $this->listeners[] = $events->attach(
//            DealEvent::EVENT_SUCCESS_CREATING,
//            [$this, 'successDealCreate'],
//            $priority
//        );
        $this->listeners[] = $events->attach(
            DealEvent::EVENT_AFTER_CONFIRMATION_TRANSACTION_CREATION,
            [$this, 'successDealClose'],
            $priority
        );
    }

//    /**
//     * @param EventInterface $event
//     * @throws LogicException
//     * @throws \Exception
//     */
//    public function successDealCreate(EventInterface $event)
//    {
//        $deal = $event->getParam('deal', null);
//        if (!$deal instanceof Deal) {
//
//            throw new LogicException(null, LogicException::DEAL_OBJECT_NOT_PROVIDED);
//        }
//
//        ///// отсылка уведомлений на приглашение к сделке /////
//        if ($this->email_notify_to_invitation) {
//            $this->emailNotificationManager->invitationDealAgentsByEmail($deal);
//        }
//        ///// отсылка sms на приглашение к сделке /////
//        if ($this->sms_notify_to_invitation) {
//            $this->smsNotificationManager->invitationDealAgentsBySms($deal);
//        }
//    }

    /**
     * @param EventInterface $event
     * @return void
     */
    public function successDealClose(EventInterface $event)
    {
        $deal = $event->getParam('deal', null);

        if (!$deal instanceof Deal) {

            return;
        }

        $deal_status = $this->dealStatus->getStatus($deal);

        if ($deal_status['status'] !== 'closed') {

            return;
        }

        if ($this->email_notify_deal_closed) {
            try {
                $this->emailNotificationManager->notifyCustomerAboutDealClosed($deal);
                $this->emailNotificationManager->notifyContractorAboutDealClosed($deal);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }

//        if ($this->sms_notify_deal_closed) {
//            try {
//                $this->notifyCustomerAboutDealClosedBySms($dispute, $discount);
//                $this->notifyContractorAboutDealClosedBySms($dispute, $discount);
//            }
//            catch (\Throwable $t) {
//
//                logException($t, 'sms-error');
//            }
//        }

        return;
    }
}