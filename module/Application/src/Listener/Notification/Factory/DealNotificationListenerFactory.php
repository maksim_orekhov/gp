<?php
namespace Application\Listener\Notification\Factory;

use Application\Listener\Notification\DealNotificationListener;
use Application\Service\Deal\DealStatus;
use Application\Service\Notification\EmailNotificationManager;
use Application\Service\Notification\SmsNotificationManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class DealNotificationListenerFactory
 * @package Application\Service\Notification\Factory
 */
class DealNotificationListenerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return EmailNotificationManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $emailNotificationManager = $container->get(EmailNotificationManager::class);
        $smsNotificationManager = $container->get(SmsNotificationManager::class);
        $dealStatus = $container->get(DealStatus::class);
        $config = $container->get('Config');

        return new DealNotificationListener(
            $emailNotificationManager,
            $smsNotificationManager,
            $dealStatus,
            $config
        );
    }
}