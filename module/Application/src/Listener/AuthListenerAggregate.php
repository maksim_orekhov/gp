<?php

namespace Application\Listener;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\User;
use Application\Service\Deal\DealAgentManager;
use Application\Service\Deal\DealManager;
use Application\Service\TokenManager;
use Core\Entity\Interfaces\EmailInterface;
use Core\EventManager\AuthEventProvider as AuthEvent;
use Core\Exception\LogicException;
use Core\Listener\ListenerAggregateTrait;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\SessionContainerManager;
use Core\Service\TwigViewModel;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Form\Form;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

/**
 * Class AuthListenerAggregate
 * @package Application\Listener
 */
class AuthListenerAggregate implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    const INVITATION_TOKEN = 'invitation_token';
    const INVITATION_DEAL_AGENT = 'invitation_deal_agent';
    const DEAL_WITH_INVITATION = 'deal_with_invitation';
    /**
     * @var TokenManager
     */
    private $tokenManager;
    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;
    /**
     * @var DealManager
     */
    private $dealManager;
    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;
    /**
     * @var SessionContainerManager
     */
    private $sessionContainerManager;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var EventManager
     */
    private $authEventManager;

    /**
     * RegistrationByInvitationListenerAggregate constructor.
     * @param TokenManager $tokenManager
     * @param BaseEmailManager $baseEmailManager
     * @param DealManager $dealManager
     * @param DealAgentManager $dealAgentManager
     * @param SessionContainerManager $sessionContainerManager
     * @param BaseAuthManager $baseAuthManager
     * @param BaseUserManager $baseUserManager
     * @param EventManager $authEventManager
     */
    public function __construct(TokenManager $tokenManager,
                                BaseEmailManager $baseEmailManager,
                                DealManager $dealManager,
                                DealAgentManager $dealAgentManager,
                                SessionContainerManager $sessionContainerManager,
                                BaseAuthManager $baseAuthManager,
                                BaseUserManager $baseUserManager,
                                EventManager $authEventManager)
    {
        $this->tokenManager = $tokenManager;
        $this->baseEmailManager = $baseEmailManager;
        $this->dealManager = $dealManager;
        $this->dealAgentManager = $dealAgentManager;
        $this->sessionContainerManager = $sessionContainerManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->baseUserManager = $baseUserManager;
        $this->authEventManager = $authEventManager;
    }

    /**
     * @param EventManagerInterface $events
     * @param int $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(AuthEvent::EVENT_BEFORE_REGISTRATION, [$this, 'beforeUserRegistration'], $priority);
        $this->listeners[] = $events->attach(AuthEvent::EVENT_SUCCESS_REGISTRATION, [$this, 'successUserRegistration'], $priority);
        $this->listeners[] = $events->attach(AuthEvent::EVENT_VIEW_MODEL_REGISTRATION, [$this, 'viewModelRegistration'], $priority);
        $this->listeners[] = $events->attach(AuthEvent::EVENT_EMAIL_CONFIRMED, [$this, 'emailConfirmed'], $priority);
        //auth
        $this->listeners[] = $events->attach(AuthEvent::EVENT_BEFORE_AUTH, [$this, 'beforeAuth'], $priority);
        $this->listeners[] = $events->attach(AuthEvent::EVENT_AFTER_AUTH, [$this, 'afterAuth'], $priority);
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     * @return void
     */
    public function beforeUserRegistration(EventInterface $event)
    {
        $application = $event->getTarget();
        // Get invitation token
        $invitation_token = trim($application->params()->fromQuery('token', null));
        if (!$invitation_token) {
            return;
        }

        $decodeInvitationToken = $this->tokenManager->checkedAndDecodeInvitationUserToken($invitation_token);

        $invitation_email = strtolower($decodeInvitationToken->email);
        $deal_id = (int)$decodeInvitationToken->dealId;
        $deal_agent_id = (int)$decodeInvitationToken->dealAgentId;

        if ($this->baseEmailManager->checkEmailUnique($invitation_email)) {
            $deal = $this->dealManager->getDealById($deal_id);
            /** @var DealAgent $contractor */
            $contractor = $deal->getContractor();
            /** @var DealAgent $customer */
            $customer = $deal->getCustomer();

            if ($deal_agent_id === $contractor->getId()) {
                $dealAgent = $contractor;
            } elseif ($deal_agent_id === $customer->getId()) {
                $dealAgent = $customer;
            } else {
                throw new LogicException(null, LogicException::DEAL_AGENT_DOES_NOT_MATCH_TO_DEAL);
            }
            $invitation_deal_agent = $dealAgent;

            ///// формируем event параметры /////
            /** @var MvcEvent $mvcEvent */
            $mvcEvent = $application->getEvent();
            $mvcEvent->setParam(self::INVITATION_DEAL_AGENT, $invitation_deal_agent);
            $mvcEvent->setParam(self::INVITATION_TOKEN, $invitation_token);
            $mvcEvent->setParam(self::DEAL_WITH_INVITATION, $deal);
            $mvcEvent->setParam('user_email', $invitation_email);

            //отключаем отправку письма с подтверждением
            //обработка происходит в RegistrationListenerAggregate
            $this->sessionContainerManager->deleteSessionVar('send_confirmation_email');
        }
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     */
    public function successUserRegistration(EventInterface $event)
    {
        $application = $event->getTarget();
        /** @var MvcEvent $mvcEvent */
        $mvcEvent = $application->getEvent();
        $mvcEventParams = $mvcEvent->getParams();
        if (!array_key_exists(self::INVITATION_DEAL_AGENT, $mvcEventParams)) {
            return;
        }
        /**
         * @var User $user
         * @var DealAgent $dealAgent
         * @var EmailInterface $email
         */
        $user = $event->getParam('user', null);
        $email = $user->getEmail();
        $dealAgent = $mvcEventParams[self::INVITATION_DEAL_AGENT];

        $this->dealAgentManager->setInvitationUserToDealAgent($user, $dealAgent);
        // If all verifications successfully passed
        $this->baseEmailManager->setEmailConfirmed($email);

        if ( $email->getIsVerified() ) {
            //set trigger verify email
            $this->authEventManager->trigger(AuthEvent::EVENT_EMAIL_CONFIRMED, $this, [
                'email_confirmed_params' => ['user' => $user]
            ]);
        }
        // Delete unconfirmed_email from session
        $this->sessionContainerManager->deleteSessionVar('unconfirmed_email');
        //auto login
        $this->baseAuthManager->userAutoLogin($user);
        $mvcEvent->setParam('redirect_url', $application->url()->fromRoute('profile'));
    }

    /**
     * @param EventInterface $event
     */
    public function viewModelRegistration(EventInterface $event)
    {
        $application = $event->getTarget();
        /** @var MvcEvent $mvcEvent */
        $mvcEvent = $application->getEvent();
        $mvcEventParams = $mvcEvent->getParams();
        if (!array_key_exists(self::INVITATION_TOKEN, $mvcEventParams) ||
            !array_key_exists(self::DEAL_WITH_INVITATION, $mvcEventParams)) {

            return;
        }

        /** @var string $token */
        $token = $mvcEventParams[self::INVITATION_TOKEN];
        /** @var ViewModel $model */
        $model = $event->getParam('view');
        $variables = $model->getVariables();
        /** @var Form $form */
        $form = $variables['userRegistrationForm'];
        $form->get('token')->setValue($token);

        /** @var Deal $deal */
        $deal = $mvcEventParams[self::DEAL_WITH_INVITATION];
        /** @var DealAgent $ownerDealAgent */
        $ownerDealAgent = $deal->getOwner();

        $form->prepare();
        $model->setVariables(
            [
                'userRegistrationForm' => $form,
                'invitation' => [
                    'deal_id' => $deal->getId(),
                    'deal_name' => $deal->getName(),
                    'deal_author' => $ownerDealAgent->getUser() ? $ownerDealAgent->getUser()->getLogin() : $ownerDealAgent->getEmail(),
                ]
            ],
            true
        );

        $application->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        $this->runViewModel($model);
    }

    /**
     * @param EventInterface $event
     * @return void
     * @throws \Exception
     */
    public function emailConfirmed(EventInterface $event)
    {
        $params = $event->getParam('email_confirmed_params', null);
        $event->setParam('email_confirmed_params', null);
        if (!\is_array($params) ||
            !array_key_exists('user', $params)) {

            return;
        }
        /** @var User $user */
        $user = $params['user'];

        // 1. удаляем пользователей с такам же неподтвержденным email
        $this->baseUserManager->removeDuplicateUserRegistrations($user);
        // 2. добавляем DealAgent с такам же email к пользователю
        $this->dealManager->attachDealAgentsToUser($user);
    }

    /**
     * @param EventInterface $event
     * @throws LogicException
     */
    public function beforeAuth(EventInterface $event)
    {
        /**
         * @var User $user
         * @var MvcEvent $mvcEvent
         */
        if ($this->baseAuthManager->getIdentity() !== null) {

            $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if (!$user) {

                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }

            $redirect_url = $this->getRedirectUrlForLoggedUser($user, $event);

            $application = $event->getTarget();
            $mvcEvent = $application->getEvent();
            $mvcEvent->setParam('logged_in_user_redirection_url', $redirect_url);
        }
    }

    /**
     * @param EventInterface $event
     * @throws LogicException
     */
    public function afterAuth(EventInterface $event)
    {
        /**
         * @var User $user
         * @var MvcEvent $mvcEvent
         */
        if ($this->baseAuthManager->getIdentity() !== null) {

            $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if (!$user) {

                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }

            $redirect_url = $this->getRedirectUrlForLoggedUser($user, $event);

            $application = $event->getTarget();
            /** @var MvcEvent $mvcEvent */
            $mvcEvent = $application->getEvent();
            $mvcEvent->setParam('redirect_url', $redirect_url);
        }
    }

    /**
     * @param User $user
     * @param EventInterface $event
     * @return string
     */
    private function getRedirectUrlForLoggedUser(User $user, EventInterface $event): string
    {
        $application = $event->getTarget();
        /** @var string $redirect_url */
        $redirect_url = $application->params()->fromQuery('redirectUrl', null);

        if ( $redirect_url ) {

            return $redirect_url;
        }

        $emailIsVerified = $user->getEmail() ? $user->getEmail()->getIsVerified() : false;
        $phoneIsVerified = $user->getPhone() ? $user->getPhone()->getIsVerified() : false;

        if ( $phoneIsVerified && $emailIsVerified ) {

            return $application->url()->fromRoute('deal');
        }

        return $application->url()->fromRoute('profile');
    }
}