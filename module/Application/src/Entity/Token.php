<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Token
 *
 * @ORM\Table(name="token")
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\TokenRepository");
 */
class Token
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="deal_role", type="string", length=10, nullable=true)
     */
    private $dealRole;

    /**
     * @var string
     *
     * @ORM\Column(name="partner_ident", type="string", length=60, nullable=true, unique=true)
     */
    private $partnerIdent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true, options={"default" = false})
*/
    private $isDeleted;

    /**
     * @var \Application\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="beneficiar_user_id", referencedColumnName="id")
     * })
     */
    private $beneficiarUser;

    /**
     * @var \Application\Entity\CivilLawSubject
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\CivilLawSubject")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="beneficiar_civil_law_subject_id", referencedColumnName="id")
     * })
     */
    private $beneficiarCivilLawSubject;

    /**
     * @var \Application\Entity\PaymentMethod
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\PaymentMethod")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="beneficiar_payment_method_id", referencedColumnName="id")
     * })
     */
    private $beneficiarPaymentMethod;

    /**
     * @var string
     *
     * @ORM\Column(name="counteragent_email", type="string", length=45, precision=0, scale=0, nullable=true)
     */
    private $counteragentEmail;

    /**
     * @var \Application\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="counteragent_user_id", referencedColumnName="id")
     * })
     */
    private $counteragentUser;

    /**
     * @var \Application\Entity\CivilLawSubject
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\CivilLawSubject")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="counteragent_civil_law_subject_id", referencedColumnName="id")
     * })
     */
    private $counteragentCivilLawSubject;

    /**
     * @var \Application\Entity\PaymentMethod
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\PaymentMethod")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="counteragent_payment_method_id", referencedColumnName="id")
     * })
     */
    private $counteragentPaymentMethod;

    /**
     * @var \Application\Entity\DealType
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\DealType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deal_type_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $dealType;

    /**
     * @var string
     *
     * @ORM\Column(name="deal_name", type="string", length=255, nullable=true, unique=false)
     */
    private $dealName;

    /**
     * @var string
     *
     * @ORM\Column(name="deal_description", type="text", length=2000, nullable=true, unique=false)
     */
    private $dealDescription;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", precision=10, scale=0, nullable=true, unique=false)
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="delivery_period", type="smallint", nullable=true, unique=false)
     */
    private $deliveryPeriod;

    /**
     * @var \Application\Entity\FeePayerOption
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\FeePayerOption", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fee_payer_option_id", referencedColumnName="id")
     * })
     */
    private $feePayerOption;

    /**
     * @var string
     *
     * @ORM\Column(name="token_key", type="string", length=60, nullable=false, unique=true)
     */
    private $tokenKey; // сгенерированный ключ

    /**
     * One Token has Many Deals.
     * @ORM\OneToMany(targetEntity="Deal", mappedBy="dealToken")
     */
    private $deals;


    public function __construct() {

        $this->deals = new ArrayCollection();
    }


    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getDealRole()
    {
        return $this->dealRole;
    }

    /**
     * @param string|null $dealRole
     */
    public function setDealRole($dealRole)
    {
        $this->dealRole = $dealRole;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getPartnerIdent()
    {
        return $this->partnerIdent;
    }

    /**
     * @param string|null $partnerIdent
     */
    public function setPartnerIdent($partnerIdent)
    {
        $this->partnerIdent = $partnerIdent;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return bool|null
     */
    public function isDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param bool|null $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return User|null
     */
    public function getBeneficiarUser()
    {
        return $this->beneficiarUser;
    }

    /**
     * @param User|null $beneficiarUser
     */
    public function setBeneficiarUser($beneficiarUser)
    {
        $this->beneficiarUser = $beneficiarUser;
    }

    /**
     * @return CivilLawSubject|null
     */
    public function getBeneficiarCivilLawSubject()
    {
        return $this->beneficiarCivilLawSubject;
    }

    /**
     * @param CivilLawSubject|null $beneficiarCivilLawSubject
     */
    public function setBeneficiarCivilLawSubject($beneficiarCivilLawSubject)
    {
        $this->beneficiarCivilLawSubject = $beneficiarCivilLawSubject;
    }

    /**
     * @return PaymentMethod|null
     */
    public function getBeneficiarPaymentMethod()
    {
        return $this->beneficiarPaymentMethod;
    }

    /**
     * @param PaymentMethod|null $beneficiarPaymentMethod
     */
    public function setBeneficiarPaymentMethod($beneficiarPaymentMethod)
    {
        $this->beneficiarPaymentMethod = $beneficiarPaymentMethod;
    }

    /**
     * @return string|null
     */
    public function getCounteragentEmail()
    {
        return $this->counteragentEmail;
    }

    /**
     * @param string|null $counteragentEmail
     */
    public function setCounteragentEmail($counteragentEmail)
    {
        $this->counteragentEmail = $counteragentEmail;
    }

    /**
     * @return User|null
     */
    public function getCounteragentUser()
    {
        return $this->counteragentUser;
    }

    /**
     * @param User|null $counteragentUser
     */
    public function setCounteragentUser($counteragentUser)
    {
        $this->counteragentUser = $counteragentUser;
    }

    /**
     * @return CivilLawSubject|null
     */
    public function getCounteragentCivilLawSubject()
    {
        return $this->counteragentCivilLawSubject;
    }

    /**
     * @param CivilLawSubject|null $counteragentCivilLawSubject
     */
    public function setCounteragentCivilLawSubject($counteragentCivilLawSubject)
    {
        $this->counteragentCivilLawSubject = $counteragentCivilLawSubject;
    }

    /**
     * @return PaymentMethod|null
     */
    public function getCounteragentPaymentMethod()
    {
        return $this->counteragentPaymentMethod;
    }

    /**
     * @param PaymentMethod|null $counteragentPaymentMethod
     */
    public function setCounteragentPaymentMethod($counteragentPaymentMethod)
    {
        $this->counteragentPaymentMethod = $counteragentPaymentMethod;
    }

    /**
     * @return DealType|null
     */
    public function getDealType()
    {
        return $this->dealType;
    }

    /**
     * @param DealType|null $dealType
     */
    public function setDealType($dealType)
    {
        $this->dealType = $dealType;
    }

    /**
     * @return string|null
     */
    public function getDealName()
    {
        return $this->dealName;
    }

    /**
     * @param string $dealName|null
     */
    public function setDealName($dealName)
    {
        $this->dealName = $dealName;
    }

    /**
     * @return string|null
     */
    public function getDealDescription()
    {
        return $this->dealDescription;
    }

    /**
     * @param string|null $dealDescription
     */
    public function setDealDescription($dealDescription)
    {
        $this->dealDescription = $dealDescription;
    }

    /**
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float|null $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int|null
     */
    public function getDeliveryPeriod()
    {
        return $this->deliveryPeriod;
    }

    /**
     * @param int|null $deliveryPeriod
     */
    public function setDeliveryPeriod($deliveryPeriod)
    {
        $this->deliveryPeriod = $deliveryPeriod;
    }

    /**
     * @return FeePayerOption|null
     */
    public function getFeePayerOption()
    {
        return $this->feePayerOption;
    }

    /**
     * @param FeePayerOption|null $feePayerOption
     */
    public function setFeePayerOption($feePayerOption)
    {
        $this->feePayerOption = $feePayerOption;
    }

    /**
     * @return string
     */
    public function getTokenKey(): string
    {
        return $this->tokenKey;
    }

    /**
     * @param string $tokenKey
     */
    public function setTokenKey(string $tokenKey)
    {
        $this->tokenKey = $tokenKey;
    }

    /**
     * @return mixed
     */
    public function getDeals()
    {
        return $this->deals;
    }

    /**
     * @param mixed $deals
     */
    public function setDeals($deals)
    {
        $this->deals = $deals;
    }
}

