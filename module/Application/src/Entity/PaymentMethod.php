<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModulePaymentOrder\Entity\PaymentMethodType;

/**
 * PaymentMethod
 *
 * @ORM\Table(name="payment_method", indexes={@ORM\Index(name="fk_payment_method_payment_type1_idx", columns={"payment_method_type_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\PaymentMethodRepository")
 */
class PaymentMethod
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Application\Entity\CivilLawSubject
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\CivilLawSubject", inversedBy="paymentMethods")
     * @ORM\JoinColumn(name="civil_law_subject_id", referencedColumnName="id", nullable=true)
     */
    private $civilLawSubject;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_pattern", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $isPattern;

    /**
     * @var \ModulePaymentOrder\Entity\PaymentMethodType
     *
     * @ORM\ManyToOne(targetEntity="ModulePaymentOrder\Entity\PaymentMethodType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_method_type_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $paymentMethodType;

    /**
     * @var \Application\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User", inversedBy="paymentMethods")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Application\Entity\PaymentMethodBankTransfer
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\PaymentMethodBankTransfer", mappedBy="paymentMethod")
     */
    private $paymentMethodBankTransfer;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set civilLawSubject
     *
     * @param \Application\Entity\CivilLawSubject $civilLawSubject
     *
     * @return PaymentMethod
     */
    public function setCivilLawSubject(\Application\Entity\CivilLawSubject $civilLawSubject = null)
    {
        $this->civilLawSubject = $civilLawSubject;

        return $this;
    }

    /**
     * Get civilLawSubject
     *
     * @return \Application\Entity\CivilLawSubject
     */
    public function getCivilLawSubject()
    {
        return $this->civilLawSubject;
    }

    /**
     * Set isPattern
     *
     * @param boolean $isPattern
     *
     * @return PaymentMethod
     */
    public function setIsPattern($isPattern)
    {
        $this->isPattern = $isPattern;

        return $this;
    }

    /**
     * Get isPattern
     *
     * @return boolean
     */
    public function getIsPattern()
    {
        return $this->isPattern;
    }

    /**
     * Set paymentMethodType
     *
     * @param \ModulePaymentOrder\Entity\PaymentMethodType $paymentMethodType
     *
     * @return PaymentMethod
     */
    public function setPaymentMethodType(\ModulePaymentOrder\Entity\PaymentMethodType $paymentMethodType = null)
    {
        $this->paymentMethodType = $paymentMethodType;

        return $this;
    }

    /**
     * Get paymentMethodType
     *
     * @return \ModulePaymentOrder\Entity\PaymentMethodType
     */
    public function getPaymentMethodType()
    {
        return $this->paymentMethodType;
    }

    /**
     * Set user
     *
     * @param \Application\Entity\User $user
     *
     * @return PaymentMethod
     */
    public function setUser(\Application\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set paymentMethodBankTransfer
     *
     * @param \Application\Entity\PaymentMethodBankTransfer $paymentMethodBankTransfer
     *
     * @return PaymentMethod
     */
    public function setPaymentMethodBankTransfer(\Application\Entity\PaymentMethodBankTransfer $paymentMethodBankTransfer = null)
    {
        $this->paymentMethodBankTransfer = $paymentMethodBankTransfer;

        return $this;
    }

    /**
     * Get paymentMethodBankTransfer
     *
     * @return \Application\Entity\PaymentMethodBankTransfer|null
     */
    public function getPaymentMethodBankTransfer()
    {
        return $this->paymentMethodBankTransfer;
    }
}

