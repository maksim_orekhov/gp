<?php
namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use ModulePaymentOrder\Entity\PaymentOrder;

/**
 * Interface CivilLawSubjectChildInterface
 * @package Application\Entity
 */
interface CivilLawSubjectChildInterface
{
    /**
     * @return CivilLawSubject
     */
    public function getCivilLawSubject();

    /**
     * @param CivilLawSubject $civilLawSubject
     */
    public function setCivilLawSubject(CivilLawSubject $civilLawSubject = null);
}