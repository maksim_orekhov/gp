<?php
namespace Application\Entity;

use Application\Entity\Interfaces\PaymentOrderOwnerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ModulePaymentOrder\Entity\PaymentOrder;

/**
 * Class RepaidOverpay
 * @package Application\Entity
 *
 * @ORM\Table(name="repaid_overpay")
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\RepaidOverpayRepository");
 */
class RepaidOverpay implements PaymentOrderOwnerInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=false, unique=false)
     */
    private $amount;

    /**
     * Many RepaidOverpays have Many PaymentOrders.
     * @ORM\ManyToMany(targetEntity="ModulePaymentOrder\Entity\PaymentOrder", fetch="EAGER")
     * @ORM\JoinTable(name="repaid_overpay_payment_order",
     *      joinColumns={@ORM\JoinColumn(name="repaid_overpay_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="payment_order_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $paymentOrders;

    /**
     * One RepaidOverpay has One PaymentMethod.
     * @ORM\OneToOne(targetEntity="PaymentMethod")
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id")
     */
    private $paymentMethod;

    /**
     * One RepaidOverpay has One Payment.
     * @ORM\OneToOne(targetEntity="Payment", inversedBy="repaidOverpay")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     */
    private $payment;

    /**
     * RepaidOverpay constructor.
     */
    public function __construct()
    {
        $this->paymentOrders = new ArrayCollection;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getPaymentOrders()
    {
        return $this->paymentOrders;
    }

    /**
     * @param mixed $paymentOrders
     */
    public function setPaymentOrders($paymentOrders)
    {
        $this->paymentOrders = $paymentOrders;
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return $this
     */
    public function addPaymentOrder(PaymentOrder $paymentOrder)
    {
        $this->paymentOrders->add($paymentOrder);

        return $this;
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return $this
     */
    public function removePaymentOrder(PaymentOrder $paymentOrder)
    {
        $this->paymentOrders->removeElement($paymentOrder);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
    }
}