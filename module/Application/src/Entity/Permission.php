<?php
namespace Application\Entity;

use Core\Entity\AbstractPermission;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Permission
 * @package Core\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="permission")
 */
class Permission extends AbstractPermission
{}