<?php
namespace Application\Entity;

use Application\Entity\Interfaces\PaymentOrderOwnerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ModulePaymentOrder\Entity\PaymentOrder;

/**
 * Class Discount
 * @package Application\Entity
 *
 * @ORM\Table(name="discount")
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\DiscountRepository");
 */
class Discount implements PaymentOrderOwnerInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * One Discount has One Dispute. Bidirectional.
     * @ORM\OneToOne(targetEntity="Dispute", inversedBy="discount")
     * @ORM\JoinColumn(name="dispute_id", referencedColumnName="id")
     */
    private $dispute;

    /**
     * One Discount has One DisputeCycle. Bidirectional.
     * @ORM\OneToOne(targetEntity="DisputeCycle", inversedBy="discount")
     * @ORM\JoinColumn(name="dispute_cycle_id", referencedColumnName="id")
     */
    private $disputeCycle;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $amount;

    /**
     * Many Discounts have Many PaymentOrders.
     * @ORM\ManyToMany(targetEntity="ModulePaymentOrder\Entity\PaymentOrder", fetch="EAGER")
     * @ORM\JoinTable(name="discount_payment_order",
     *      joinColumns={@ORM\JoinColumn(name="discount_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="payment_order_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $paymentOrders;

    /**
     * One Discount has One PaymentMethod.
     * @ORM\OneToOne(targetEntity="PaymentMethod")
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id")
     */
    private $paymentMethod;

    /**
     * Deal constructor.
     */
    public function __construct()
    {
        $this->paymentOrders = new ArrayCollection;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getDispute()
    {
        return $this->dispute;
    }

    /**
     * @param mixed $dispute
     */
    public function setDispute($dispute)
    {
        $this->dispute = $dispute;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return ArrayCollection
     */
    public function getPaymentOrders()
    {
        return $this->paymentOrders;
    }

    /**
     * @param mixed $paymentOrders
     */
    public function setPaymentOrders($paymentOrders)
    {
        $this->paymentOrders = $paymentOrders;
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return $this
     */
    public function addPaymentOrder(PaymentOrder $paymentOrder)
    {
        $this->paymentOrders->add($paymentOrder);

        return $this;
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return $this
     */
    public function removePaymentOrder(PaymentOrder $paymentOrder)
    {
        $this->paymentOrders->removeElement($paymentOrder);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param PaymentMethod|null $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return mixed
     */
    public function getDisputeCycle()
    {
        return $this->disputeCycle;
    }

    /**
     * @param mixed $disputeCycle
     */
    public function setDisputeCycle($disputeCycle)
    {
        $this->disputeCycle = $disputeCycle;
    }
}