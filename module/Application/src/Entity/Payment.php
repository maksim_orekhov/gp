<?php

namespace Application\Entity;

use Application\Entity\Interfaces\PaymentOrderOwnerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ModulePaymentOrder\Entity\PaymentOrder;

/**
 * Payment
 *
 * @ORM\Table(name="payment", indexes={@ORM\Index(name="fk_payment_deal1_idx", columns={"deal_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\PaymentRepository");
 */
class Payment implements PaymentOrderOwnerInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Application\Entity\PaymentMethod
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\PaymentMethod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $paymentMethod;

    /**
     * @var float
     *
     * @ORM\Column(name="deal_value", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $dealValue;

    /**
     * @var float
     *
     * @ORM\Column(name="expected_value", type="float", precision=11, scale=0, nullable=false, unique=false)
     */
    private $expectedValue;

    /**
     * @var float
     *
     * @ORM\Column(name="released_value", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $releasedValue;

    /**
     * @var float
     *
     * @ORM\Column(name="fee", type="float", precision=10, scale=0, nullable=false, unique=false)
     */
    private $fee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="planned_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $plannedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="de_facto_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $deFactoDate;

    /**
     * One Payment has One Deal.
     * @ORM\OneToOne(targetEntity="Deal", inversedBy="payment")
     * @ORM\JoinColumn(name="deal_id", referencedColumnName="id")
     */
    private $deal;

    /**
     * Many Payments have Many PaymentOrders.
     * @ORM\ManyToMany(targetEntity="ModulePaymentOrder\Entity\PaymentOrder", fetch="EAGER")
     * @ORM\JoinTable(name="payment_payment_order",
     *      joinColumns={@ORM\JoinColumn(name="payment_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="payment_order_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $paymentOrders;

    /**
     * One Payment has One RepaidOverpay.
     * @ORM\OneToOne(targetEntity="RepaidOverpay", mappedBy="payment")
     */
    private $repaidOverpay;

    /**
     * Payment constructor.
     */
    public function __construct() {

        $this->paymentOrders = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expectedValue
     *
     * @param float $expectedValue
     *
     * @return Payment
     */
    public function setExpectedValue($expectedValue)
    {
        $this->expectedValue = $expectedValue;

        return $this;
    }

    /**
     * Get expectedValue
     *
     * @return float
     */
    public function getExpectedValue()
    {
        return $this->expectedValue;
    }

    /**
     * Set dealValue
     *
     * @param float $dealValue
     *
     * @return Payment
     */
    public function setDealValue($dealValue)
    {
        $this->dealValue = $dealValue;

        return $this;
    }

    /**
     * Get dealValue
     *
     * @return float
     */
    public function getDealValue()
    {
        return $this->dealValue;
    }

    /**
     * Set releasedValue
     *
     * @param float $releasedValue
     *
     * @return Payment
     */
    public function setReleasedValue($releasedValue)
    {
        $this->releasedValue = $releasedValue;

        return $this;
    }

    /**
     * Get releasedValue
     *
     * @return float
     */
    public function getReleasedValue()
    {
        return $this->releasedValue;
    }

    /**
     * Set fee
     *
     * @param float $fee
     *
     * @return Payment
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee
     *
     * @return float
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set plannedDate
     *
     * @param \DateTime $plannedDate
     *
     * @return Payment
     */
    public function setPlannedDate($plannedDate)
    {
        $this->plannedDate = $plannedDate;

        return $this;
    }

    /**
     * Get plannedDate
     *
     * @return \DateTime
     */
    public function getPlannedDate()
    {
        return $this->plannedDate;
    }

    /**
     * Set deFactoDate
     *
     * @param \DateTime $deFactoDate
     *
     * @return Payment
     */
    public function setDeFactoDate($deFactoDate)
    {
        $this->deFactoDate = $deFactoDate;

        return $this;
    }

    /**
     * Get deFactoDate
     *
     * @return \DateTime
     */
    public function getDeFactoDate()
    {
        return $this->deFactoDate;
    }

    /**
     * @return mixed
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * @param mixed $deal
     */
    public function setDeal($deal)
    {
        $this->deal = $deal;
    }

    /**
     * @return ArrayCollection
     */
    public function getPaymentOrders()
    {
        return $this->paymentOrders;
    }

    /**
     * @param mixed $paymentOrders
     */
    public function setPaymentOrders($paymentOrders)
    {
        $this->paymentOrders = $paymentOrders;
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return $this
     */
    public function addPaymentOrder(PaymentOrder $paymentOrder)
    {
        $this->paymentOrders->add($paymentOrder);

        return $this;
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return $this
     */
    public function removePaymentOrder(PaymentOrder $paymentOrder)
    {
        $this->paymentOrders->removeElement($paymentOrder);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param PaymentMethod|null $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return mixed
     */
    public function getRepaidOverpay()
    {
        return $this->repaidOverpay;
    }

    /**
     * @param mixed $repaidOverpay
     */
    public function setRepaidOverpay($repaidOverpay)
    {
        $this->repaidOverpay = $repaidOverpay;
    }
}

