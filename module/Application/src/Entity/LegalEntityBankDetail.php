<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LegalEntityBankDetail
 *
 * @ORM\Table(name="legal_entity_bank_detail", indexes={@ORM\Index(name="fk_legal_entity_bank_detail_legal_entity_document1_idx", columns={"legal_entity_document_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\LegalEntityBankDetailRepository")
 */
class LegalEntityBankDetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="checking_account", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $checkingAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="correspondent_account", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $correspondentAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="bic", type="string", length=9, precision=0, scale=0, nullable=false, unique=false)
     */
    private $bic;

    /**
     * @var \Application\Entity\LegalEntityDocument
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\LegalEntityDocument", inversedBy="legalEntityBankDetail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="legal_entity_document_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $legalEntityDocument;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set checkingAccount
     *
     * @param string $checkingAccount
     *
     * @return LegalEntityBankDetail
     */
    public function setCheckingAccount($checkingAccount)
    {
        $this->checkingAccount = $checkingAccount;

        return $this;
    }

    /**
     * Get checkingAccount
     *
     * @return string
     */
    public function getCheckingAccount()
    {
        return $this->checkingAccount;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LegalEntityBankDetail
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set correspondentAccount
     *
     * @param string $correspondentAccount
     *
     * @return LegalEntityBankDetail
     */
    public function setCorrespondentAccount($correspondentAccount)
    {
        $this->correspondentAccount = $correspondentAccount;

        return $this;
    }

    /**
     * Get correspondentAccount
     *
     * @return string
     */
    public function getCorrespondentAccount()
    {
        return $this->correspondentAccount;
    }

    /**
     * Set bic
     *
     * @param string $bic
     *
     * @return LegalEntityBankDetail
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * Get bic
     *
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set legalEntityDocument
     *
     * @param \Application\Entity\LegalEntityDocument $legalEntityDocument
     *
     * @return LegalEntityBankDetail
     */
    public function setLegalEntityDocument(\Application\Entity\LegalEntityDocument $legalEntityDocument = null)
    {
        $this->legalEntityDocument = $legalEntityDocument;

        return $this;
    }

    /**
     * Get legalEntityDocument
     *
     * @return \Application\Entity\LegalEntityDocument
     */
    public function getLegalEntityDocument()
    {
        return $this->legalEntityDocument;
    }
}

