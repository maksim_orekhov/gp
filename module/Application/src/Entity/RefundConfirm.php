<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class RefundConfirm
 * @package Application\Entity
 *
 * @ORM\Table(name="refund_confirm")
 * @ORM\Entity;
 */
class RefundConfirm
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @ORM\OneToOne(targetEntity="RefundRequest", mappedBy="confirm")
     */
    private $refundRequest;

    /**
     * @var DealAgent
     *
     * @ORM\ManyToOne(targetEntity="DealAgent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deal_agent_id", referencedColumnName="id")
     * })
     */
    private $author;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true, unique=false)
     */
    private $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getRefundRequest()
    {
        return $this->refundRequest;
    }

    /**
     * @param mixed $refundRequest
     */
    public function setRefundRequest($refundRequest)
    {
        $this->refundRequest = $refundRequest;
    }

    /**
     * @return DealAgent
     */
    public function getAuthor(): DealAgent
    {
        return $this->author;
    }

    /**
     * @param DealAgent $author
     */
    public function setAuthor(DealAgent $author)
    {
        $this->author = $author;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }
}