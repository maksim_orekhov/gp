<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NaturalPersonDocument
 *
 * @ORM\Table(name="natural_person_document", indexes={@ORM\Index(name="fk_natural_person_document_natural_person1_idx", columns={"natural_person_id"}), @ORM\Index(name="fk_natural_person_document_natural_person_document_type1_idx", columns={"natural_person_document_type_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\NaturalPersonDocumentRepository");
 */
class NaturalPersonDocument
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Application\Entity\NaturalPerson
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\NaturalPerson", inversedBy="naturalPersonDocument")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="natural_person_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $naturalPerson;

    /**
     * @var \Application\Entity\NaturalPersonDocumentType
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\NaturalPersonDocumentType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="natural_person_document_type_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $naturalPersonDocumentType;

    /**
     * @var \Application\Entity\NaturalPersonPassport
     *
     * @ORM\OneToOne(targetEntity="\Application\Entity\NaturalPersonPassport", mappedBy="naturalPersonDocument")
     */
    private $naturalPersonPassport;

    /**
     * @var \Application\Entity\NaturalPersonDriverLicense
     *
     * @ORM\OneToOne(targetEntity="\Application\Entity\NaturalPersonDriverLicense", mappedBy="naturalPersonDocument")
     */
    private $naturalPersonDriverLicense;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, unique=false,  options={"default" : 0})
     */
    private $isActive = 0;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set naturalPerson
     *
     * @param \Application\Entity\NaturalPerson $naturalPerson
     *
     * @return NaturalPersonDocument
     */
    public function setNaturalPerson(\Application\Entity\NaturalPerson $naturalPerson = null)
    {
        $this->naturalPerson = $naturalPerson;

        return $this;
    }

    /**
     * Get naturalPerson
     *
     * @return \Application\Entity\NaturalPerson
     */
    public function getNaturalPerson()
    {
        return $this->naturalPerson;
    }

    /**
     * Set naturalPersonDocumentType
     *
     * @param \Application\Entity\NaturalPersonDocumentType $naturalPersonDocumentType
     *
     * @return NaturalPersonDocument
     */
    public function setNaturalPersonDocumentType(\Application\Entity\NaturalPersonDocumentType $naturalPersonDocumentType = null)
    {
        $this->naturalPersonDocumentType = $naturalPersonDocumentType;

        return $this;
    }

    /**
     * Get naturalPersonDocumentType
     *
     * @return \Application\Entity\NaturalPersonDocumentType
     */
    public function getNaturalPersonDocumentType()
    {
        return $this->naturalPersonDocumentType;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * Set naturalPersonPassport
     *
     * @param \Application\Entity\NaturalPersonPassport $naturalPersonPassport
     *
     * @return NaturalPersonDocument
     */
    public function setNaturalPersonPassport(\Application\Entity\NaturalPersonPassport $naturalPersonPassport = null)
    {
        $this->naturalPersonPassport = $naturalPersonPassport;

        return $this;
    }

    /**
     * Get naturalPersonPassport
     *
     * @return \Application\Entity\NaturalPersonPassport
     */
    public function getNaturalPersonPassport()
    {
        return $this->naturalPersonPassport;
    }

    /**
     * Set naturalPersonDriverLicense
     *
     * @param \Application\Entity\NaturalPersonDriverLicense $naturalPersonDriverLicense
     *
     * @return NaturalPersonDocument
     */
    public function setNaturalPersonDriverLicense(\Application\Entity\NaturalPersonDriverLicense $naturalPersonDriverLicense = null)
    {
        $this->naturalPersonDriverLicense = $naturalPersonDriverLicense;

        return $this;
    }

    /**
     * Get naturalPersonDriverLicense
     *
     * @return \Application\Entity\NaturalPersonDriverLicense
     */
    public function getNaturalPersonDriverLicense()
    {
        return $this->naturalPersonDriverLicense;
    }
}

