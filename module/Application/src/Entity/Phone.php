<?php
namespace Application\Entity;

use Core\Entity\AbstractPhone;
use Doctrine\ORM\Mapping as ORM;

/**
 * Phone
 * @package Core\Entity
 *
 * @ORM\Table(name="phone", indexes={@ORM\Index(name="fk_phone_e1641_idx", columns={"e164_id"})})
 * @ORM\Entity
 */
class Phone extends AbstractPhone
{}

