<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ModuleFileManager\Entity\File;

/**
 * LegalEntityTaxInspection
 *
 * @ORM\Table(name="legal_entity_tax_inspection", indexes={@ORM\Index(name="fk_legal_entity_tax_inspection_legal_entity_document1_idx", columns={"legal_entity_document_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\LegalEntityTaxInspectionRepository")
 */
class LegalEntityTaxInspection
{
    const PATH_FILES = "/legal-entity-tax-inspection";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="serial_number", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $serialNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_issued", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateIssued;

    /**
     * @var string
     *
     * @ORM\Column(name="issue_organisation", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $issueOrganisation;

    /**
     * @var string
     *
     * @ORM\Column(name="issue_registrar", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $issueRegistrar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_registered", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateRegistered;

    /**
     * @var \Application\Entity\LegalEntityDocument
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\LegalEntityDocument", inversedBy="legalEntityTaxInspection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="legal_entity_document_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $legalEntityDocument;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File", fetch="EAGER")
     * @ORM\JoinTable(name="legal_entity_tax_inspection_file",
     *   joinColumns={@ORM\JoinColumn(name="legal_entity_tax_inspection_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
     * )
     */
    private $files;


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     *
     * @return LegalEntityTaxInspection
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set dateIssued
     *
     * @param string $dateIssued
     *
     * @return LegalEntityTaxInspection
     */
    public function setDateIssued($dateIssued)
    {
        $this->dateIssued = $dateIssued;

        return $this;
    }

    /**
     * Get dateIssued
     *
     * @return string
     */
    public function getDateIssued()
    {
        return $this->dateIssued;
    }

    /**
     * Set issueOrganisation
     *
     * @param string $issueOrganisation
     *
     * @return LegalEntityTaxInspection
     */
    public function setIssueOrganisation($issueOrganisation)
    {
        $this->issueOrganisation = $issueOrganisation;

        return $this;
    }

    /**
     * Get issueOrganisation
     *
     * @return string
     */
    public function getIssueOrganisation()
    {
        return $this->issueOrganisation;
    }

    /**
     * Set issueRegistrar
     *
     * @param string $issueRegistrar
     *
     * @return LegalEntityTaxInspection
     */
    public function setIssueRegistrar($issueRegistrar)
    {
        $this->issueRegistrar = $issueRegistrar;

        return $this;
    }

    /**
     * Get issueRegistrar
     *
     * @return string
     */
    public function getIssueRegistrar()
    {
        return $this->issueRegistrar;
    }

    /**
     * Set dateRegistered
     *
     * @param string $dateRegistered
     *
     * @return LegalEntityTaxInspection
     */
    public function setDateRegistered($dateRegistered)
    {
        $this->dateRegistered = $dateRegistered;

        return $this;
    }

    /**
     * Get dateRegistered
     *
     * @return string
     */
    public function getDateRegistered()
    {
        return $this->dateRegistered;
    }

    /**
     * Set legalEntityDocument
     *
     * @param \Application\Entity\LegalEntityDocument $legalEntityDocument
     *
     * @return LegalEntityTaxInspection
     */
    public function setLegalEntityDocument(\Application\Entity\LegalEntityDocument $legalEntityDocument = null)
    {
        $this->legalEntityDocument = $legalEntityDocument;

        return $this;
    }

    /**
     * Get legalEntityDocument
     *
     * @return \Application\Entity\LegalEntityDocument
     */
    public function getLegalEntityDocument()
    {
        return $this->legalEntityDocument;
    }

    /**
     * @return Collection
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    /**
     * @param Collection $files
     */
    public function setFiles(Collection $files)
    {
        $this->files = $files;
    }

    /**
     * Assigns a file to test
     * @param File $file
     * @return $this
     */
    public function addFile(File $file)
    {
        if (!$this->files->contains($file)) {
            $this->files->add($file);
        }

        return $this;
    }

    /**
     * Remove file from collection
     * @param File $file
     * @return $this
     */
    public function removeFile(File $file)
    {
        $this->files->removeElement($file);

        return $this;
    }
}

