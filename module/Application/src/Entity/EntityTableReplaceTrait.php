<?php
namespace Application\Entity;

use Doctrine\ORM\EntityManager;

/**
 * Trait EntityTableReplaceTrait
 * @package Application\Entity
 */
trait EntityTableReplaceTrait
{
    /**
     * @param EntityManager $entityManager
     * @param string $table_name_1
     * @param string $table_name_2
     * @throws \Doctrine\DBAL\DBALException
     */
    public function rename(EntityManager $entityManager, string $table_name_1, string $table_name_2)
    {
        $temporary_name = $table_name_1 . '_temporary';

        $connection = $entityManager->getConnection();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $connection->query('RENAME TABLE '.$table_name_1.' TO '.$temporary_name);
        $connection->query('RENAME TABLE '.$table_name_2.' TO '.$table_name_1);
        $connection->query('RENAME TABLE '.$temporary_name.' TO '.$table_name_2);
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
    }
}