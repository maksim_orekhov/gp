<?php
namespace Application\Entity;

use Doctrine\ORM\EntityManager;

/**
 * Trait EntityMetadataTrait
 * @package Application\Entity
 */
trait EntityMetadataTrait
{
    /**
     * @param EntityManager $entityManager
     * @param string $entity_class
     * @return string
     */
    public function getEntityTableName(EntityManager $entityManager, string $entity_class): string
    {
        $classMetadata = $entityManager->getClassMetadata($entity_class);

        return $classMetadata->getTableName();
    }
}