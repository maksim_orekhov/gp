<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DealContractFile
 * @package Application\Entity
 * @ORM\Table(name="deal_contract_file")
 * @ORM\Entity
 */
class DealContractFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * One DealContractFile has One File
     * @ORM\OneToOne(targetEntity="ModuleFileManager\Entity\File")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     */
    private $file;

    /**
     * One DealContractFile has One Deal.
     * @ORM\OneToOne(targetEntity="Deal", mappedBy="dealContractFile")
     */
    private $deal;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * @param mixed $deal
     */
    public function setDeal($deal)
    {
        $this->deal = $deal;
    }
}