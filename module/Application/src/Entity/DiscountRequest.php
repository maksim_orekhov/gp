<?php
namespace Application\Entity;

use Application\Entity\Interfaces\DisputeSolutionRequestInterface;
use Doctrine\ORM\Mapping as ORM;
/**
 * Class DiscountRequest
 * @package Application\Entity
 *
 * @ORM\Table(name="discount_request")
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\DiscountRequestRepository");
 */
class DiscountRequest implements DisputeSolutionRequestInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Dispute
     *
     * @ORM\ManyToOne(targetEntity="Dispute", inversedBy="discountRequests")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dispute_id", referencedColumnName="id")
     * })
     */
    private $dispute;

    /**
     * @var DisputeCycle
     *
     * @ORM\ManyToOne(targetEntity="DisputeCycle", inversedBy="discountRequests")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dispute_cycle_id", referencedColumnName="id")
     * })
     */
    private $disputeCycle;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2, nullable=false, unique=false)
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var DealAgent
     *
     * @ORM\ManyToOne(targetEntity="DealAgent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deal_agent_id", referencedColumnName="id")
     * })
     */
    private $author;

    /**
     * One DiscountRequest has One DiscountConfirm. Bidirectional.
     * @ORM\OneToOne(targetEntity="DiscountConfirm", inversedBy="discountRequest")
     * @ORM\JoinColumn(name="confirm_id", referencedColumnName="id", nullable=true)
     */
    private $confirm;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Dispute
     */
    public function getDispute()
    {
        return $this->dispute;
    }

    /**
     * @param Dispute $dispute
     */
    public function setDispute($dispute)
    {
        $this->dispute = $dispute;
    }

    /**
     * @return DisputeCycle
     */
    public function getDisputeCycle(): DisputeCycle
    {
        return $this->disputeCycle;
    }

    /**
     * @param DisputeCycle $disputeCycle
     */
    public function setDisputeCycle(DisputeCycle $disputeCycle)
    {
        $this->disputeCycle = $disputeCycle;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return DealAgent
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param DealAgent $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getConfirm()
    {
        return $this->confirm;
    }

    /**
     * @param mixed $confirm
     */
    public function setConfirm($confirm)
    {
        $this->confirm = $confirm;
    }
}