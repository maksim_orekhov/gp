<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemPaymentDetails
 *
 * @ORM\Table(name="system_payment_detail")
 * @ORM\Entity
 */
class SystemPaymentDetails
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=16777215, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="bank", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $bank;

    /**
     * @var integer
     *
     * @ORM\Column(name="bik", type="string", length=9, precision=0, scale=0, nullable=false, unique=false)
     */
    private $bik;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_number", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $accountNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="corr_account_number", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $corrAccountNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_recipient_name", type="string", length=45, precision=0, scale=0, nullable=false, unique=false)
     */
    private $paymentRecipientName;

    /**
     * @var integer
     *
     * @ORM\Column(name="inn", type="string", length=12, precision=0, scale=0, nullable=false, unique=false)
     */
    private $inn;

    /**
     * @var integer
     *
     * @ORM\Column(name="kpp", type="string", length=9, precision=0, scale=0, nullable=true, unique=false)
     */
    private $kpp;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SystemPaymentDetails
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set bank
     *
     * @param integer $bank
     *
     * @return SystemPaymentDetails
     */
    public function setBank($bank)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank
     *
     * @return string
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set bik
     *
     * @param integer $bik
     *
     * @return SystemPaymentDetails
     */
    public function setBik($bik)
    {
        $this->bik = $bik;

        return $this;
    }

    /**
     * Get bik
     *
     * @return integer
     */
    public function getBik()
    {
        return $this->bik;
    }

    /**
     * Set accountNumber
     *
     * @param integer $accountNumber
     *
     * @return SystemPaymentDetails
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return integer
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set corrAccountNumber
     *
     * @param integer $corrAccountNumber
     *
     * @return SystemPaymentDetails
     */
    public function setCorrAccountNumber($corrAccountNumber)
    {
        $this->corrAccountNumber = $corrAccountNumber;

        return $this;
    }

    /**
     * Get corrAccountNumber
     *
     * @return integer
     */
    public function getCorrAccountNumber()
    {
        return $this->corrAccountNumber;
    }

    /**
     * Set paymentRecipientName
     *
     * @param string $paymentRecipientName
     *
     * @return SystemPaymentDetails
     */
    public function setPaymentRecipientName($paymentRecipientName)
    {
        $this->paymentRecipientName = $paymentRecipientName;

        return $this;
    }

    /**
     * Get paymentRecipientName
     *
     * @return string
     */
    public function getPaymentRecipientName()
    {
        return $this->paymentRecipientName;
    }

    /**
     * Set inn
     *
     * @param integer $inn
     *
     * @return SystemPaymentDetails
     */
    public function setInn($inn)
    {
        $this->inn = $inn;

        return $this;
    }

    /**
     * Get inn
     *
     * @return integer
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * Set kpp
     *
     * @param integer $kpp
     *
     * @return SystemPaymentDetails
     */
    public function setKpp($kpp)
    {
        $this->kpp = $kpp;

        return $this;
    }

    /**
     * Get kpp
     *
     * @return integer
     */
    public function getKpp()
    {
        return $this->kpp;
    }
}

