<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class TribunalConfirm
 * @package Application\Entity
 *
 * @ORM\Table(name="tribunal_confirm")
 * @ORM\Entity;
 */
class TribunalConfirm
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @ORM\OneToOne(targetEntity="TribunalRequest", mappedBy="confirm")
     */
    private $tribunalRequest;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $author;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true, unique=false)
     */
    private $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getTribunalRequest()
    {
        return $this->tribunalRequest;
    }

    /**
     * @param mixed $tribunalRequest
     */
    public function setTribunalRequest($tribunalRequest)
    {
        $this->tribunalRequest = $tribunalRequest;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }
}