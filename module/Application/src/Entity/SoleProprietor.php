<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SoleProprietor
 *
 * @ORM\Table(name="sole_proprietor", indexes={@ORM\Index(name="fk_sole_proprietor_civil_law_subject1_idx", columns={"civil_law_subject_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\SoleProprietorRepository");
 */
class SoleProprietor implements CivilLawSubjectChildInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="secondary_name", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $secondaryName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $lastName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string", length=12, precision=0, scale=0, nullable=false, unique=false)
     */
    private $inn;

    /**
     * @var \Application\Entity\CivilLawSubject
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\CivilLawSubject", inversedBy="soleProprietor")
     * @ORM\JoinColumn(name="civil_law_subject_id", referencedColumnName="id", nullable=true)
     */
    private $civilLawSubject;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getSecondaryName(): string
    {
        return $this->secondaryName;
    }

    /**
     * @param string $secondaryName
     */
    public function setSecondaryName(string $secondaryName)
    {
        $this->secondaryName = $secondaryName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate(): \DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     */
    public function setBirthDate(\DateTime $birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string
     */
    public function getInn(): string
    {
        return $this->inn;
    }

    /**
     * @param string $inn
     */
    public function setInn(string $inn)
    {
        $this->inn = $inn;
    }

    /**
     * @return CivilLawSubject
     */
    public function getCivilLawSubject()
    {
        return $this->civilLawSubject;
    }

    /**
     * Set civilLawSubject
     *
     * @param CivilLawSubject $civilLawSubject
     *
     * @return SoleProprietor
     */
    public function setCivilLawSubject(CivilLawSubject $civilLawSubject = null)
    {
        $this->civilLawSubject = $civilLawSubject;

        return $this;
    }
}