<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentMethodBankTransfer
 *
 * @ORM\Table(name="payment_method_bank_transfer", indexes={@ORM\Index(name="fk_bank_transfer_payment_method1_idx", columns={"payment_method_id"})})
 * @ORM\Entity
 */
class PaymentMethodBankTransfer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=255, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_name", type="text", length=255, nullable=false, unique=false)
     */
    private $bankName;

    /**
     * @var integer
     *
     * @ORM\Column(name="bik", type="string", length=9, precision=0, scale=0, nullable=false, unique=false)
     */
    private $bik;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_number", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $accountNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="corr_account_number", type="string", length=20, precision=0, scale=0, nullable=false, unique=false)
     */
    private $corrAccountNumber;

    /**
     * @var \Application\Entity\PaymentMethod
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\PaymentMethod", inversedBy="paymentMethodBankTransfer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $paymentMethod;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PaymentMethodBankTransfer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getBankName(): string
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName(string $bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * Set bik
     *
     * @param integer $bik
     *
     * @return PaymentMethodBankTransfer
     */
    public function setBik($bik)
    {
        $this->bik = $bik;

        return $this;
    }

    /**
     * Get bik
     *
     * @return integer
     */
    public function getBik()
    {
        return $this->bik;
    }

    /**
     * Set accountNumber
     *
     * @param integer $accountNumber
     *
     * @return PaymentMethodBankTransfer
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return integer
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @return integer
     */
    public function getCorrAccountNumber()
    {
        return $this->corrAccountNumber;
    }

    /**
     * @param integer $corrAccountNumber
     * @return PaymentMethodBankTransfer
     */
    public function setCorrAccountNumber($corrAccountNumber)
    {
        $this->corrAccountNumber = $corrAccountNumber;

        return $this;
    }

    /**
     * Set paymentMethod
     *
     * @param \Application\Entity\PaymentMethod $paymentMethod
     *
     * @return PaymentMethodBankTransfer
     */
    public function setPaymentMethod(\Application\Entity\PaymentMethod $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \Application\Entity\PaymentMethod
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }
}

