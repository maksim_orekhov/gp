<?php
namespace Application\Entity;

use Core\Entity\AbstractE164;
use Doctrine\ORM\Mapping as ORM;

/**
 * E164
 * @package Core\Entity
 *
 * @ORM\Table(name="e164")
 * @ORM\Entity
 */
class E164 extends AbstractE164
{}

