<?php
namespace Application\Entity;

use Application\Entity\Interfaces\ApplicationUserInterface;
use Core\Entity\AbstractUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleAuthV2\Entity\Interfaces\AuthUserInterface;
use ModuleFileManager\Entity\File;
use ModuleCode\Entity\Code;

/**
 * Class User
 * @package Application\Entity
 *
 * @ORM\Table(name="user", indexes={@ORM\Index(name="fk_user_phone1_idx", columns={"phone_id"}), @ORM\Index(name="fk_user_email1_idx", columns={"email_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\UserRepository");
 */
class User extends AbstractUser implements ApplicationUserInterface, AuthUserInterface
{
    const PATH_AVATAR_FILES = '/avatar';
    /**
     * @ORM\OneToMany(targetEntity="ModuleCode\Entity\Code", mappedBy="user")
     */
    private $codes;

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\CivilLawSubject", mappedBy="user")
     */
    private $civilLawSubjects;

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\PaymentMethod", mappedBy="user")
     */
    private $paymentMethods;

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\DealAgent", mappedBy="user")
     */
    private $dealAgents;

    /**
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File", fetch="EAGER")
     * @ORM\JoinTable(name="user_avatar_file",
     *   joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
     * )
     */
    private $avatar;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->codes            = new ArrayCollection();
        $this->civilLawSubjects = new ArrayCollection();
        $this->paymentMethods   = new ArrayCollection();
        $this->avatar           = new ArrayCollection();
        $this->dealAgents       = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getCodes()
    {
        return $this->codes;
    }

    /**
     * @param mixed $codes
     */
    public function setCodes($codes)
    {
        $this->codes = $codes;
    }

    /**
     * Add code
     *
     * @param Code $code
     *
     * @return User
     */
    public function addCode(Code $code)
    {
        $this->codes[] = $code;

        return $this;
    }

    /**
     * Remove code
     *
     * @param Code $code
     */
    public function removeCode(Code $code)
    {
        $this->codes->removeElement($code);
    }

    /**
     * @return bool
     */
    public function getIsBanned()
    {
        return $this->isBanned;
    }

    /**
     * @param bool $isBanned
     */
    public function setIsBanned($isBanned)
    {
        $this->isBanned = $isBanned;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @return User
     */
    public function addCivilLawSubject(CivilLawSubject $civilLawSubject)
    {
        $this->civilLawSubjects[] = $civilLawSubject;

        return $this;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @return User
     */
    public function removeCivilLawSubject(CivilLawSubject $civilLawSubject)
    {
        $this->civilLawSubjects->removeElement($civilLawSubject);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCivilLawSubjects()
    {
        return $this->civilLawSubjects;
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @return User
     */
    public function addPaymentMethod(PaymentMethod $paymentMethod)
    {
        $this->paymentMethods[] = $paymentMethod;

        return $this;
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @return User
     */
    public function removePaymentMethod(PaymentMethod $paymentMethod)
    {
        $this->paymentMethods->removeElement($paymentMethod);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPaymentMethods()
    {
        return $this->paymentMethods;
    }

    /**
     * @return File
     */
    public function getAvatar()
    {
        return $this->avatar->last();
    }

    /**
     * @return bool
     */
    public function existAvatar()
    {
        return $this->getAvatar() !== false;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function setAvatar(File $file)
    {
        if (!$this->avatar->contains($file)) {
            $this->avatar->add($file);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeAvatar()
    {
        foreach ($this->avatar as $file) {
            $this->avatar->removeElement($file);
        }

        return $this;
    }

    /**
     * Get dealAgents
     *
     * @return mixed
     */
    public function getDealAgents()
    {
        return $this->dealAgents;
    }

    /**
     * Set dealAgents
     *
     * @param mixed $dealAgents
     */
    public function setDealAgents($dealAgents)
    {
        $this->dealAgents = $dealAgents;
    }
}

