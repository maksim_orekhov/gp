<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ModuleFileManager\Entity\File;

/**
 * NaturalPersonDriverLicense
 *
 * @ORM\Table(name="natural_person_driver_license", indexes={@ORM\Index(name="fk_natural_person_driver_license_natural_person_document1_idx", columns={"natural_person_document_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\NaturalPersonDriverLicenseRepository")
 */
class NaturalPersonDriverLicense
{
    const PATH_FILES = "/natural-person-driver-license";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_issued", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateIssued;

    /**
     * @var \Application\Entity\NaturalPersonDocument
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\NaturalPersonDocument", inversedBy="naturalPersonDriverLicense")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="natural_person_document_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $naturalPersonDocument;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File", fetch="EAGER")
     * @ORM\JoinTable(name="natural_person_driver_license_file",
     *   joinColumns={@ORM\JoinColumn(name="natural_person_driver_license_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
     * )
     */
    private $files;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateIssued
     *
     * @param \DateTime $dateIssued
     *
     * @return NaturalPersonDriverLicense
     */
    public function setDateIssued($dateIssued)
    {
        $this->dateIssued = $dateIssued;

        return $this;
    }

    /**
     * Get dateIssued
     *
     * @return \DateTime
     */
    public function getDateIssued()
    {
        return $this->dateIssued;
    }

    /**
     * Set naturalPersonDocument
     *
     * @param \Application\Entity\NaturalPersonDocument $naturalPersonDocument
     *
     * @return NaturalPersonDriverLicense
     */
    public function setNaturalPersonDocument(\Application\Entity\NaturalPersonDocument $naturalPersonDocument = null)
    {
        $this->naturalPersonDocument = $naturalPersonDocument;

        return $this;
    }

    /**
     * Get naturalPersonDocument
     *
     * @return \Application\Entity\NaturalPersonDocument
     */
    public function getNaturalPersonDocument()
    {
        return $this->naturalPersonDocument;
    }

    /**
     * @return Collection
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    /**
     * @param Collection $files
     */
    public function setFiles(Collection $files)
    {
        $this->files = $files;
    }

    /**
     * Assigns a file to test
     * @param File $file
     * @return $this
     */
    public function addFile(File $file)
    {
        if (!$this->files->contains($file)) {
            $this->files->add($file);
        }

        return $this;
    }

    /**
     * Remove file from collection
     * @param File $file
     * @return $this
     */
    public function removeFile(File $file)
    {
        $this->files->removeElement($file);

        return $this;
    }
}

