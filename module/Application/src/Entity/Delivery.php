<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Delivery
 *
 * @ORM\Table(name="delivery", indexes={@ORM\Index(name="fk_delivery_deal_idx", columns={"deal_id"})})
 * @ORM\Entity
 */
class Delivery
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Application\Entity\Deal
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Deal", inversedBy="deliveries", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deal_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $deal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="delivery_date", type="datetime", nullable=true, unique=false)
     */
    private $deliveryDate;


    /**
     * @var boolean
     *
     * @ORM\Column(name="delivery_status", type="boolean", nullable=true, unique=false)
     */
    private $deliveryStatus;

    /**
     * One Delivery has One DeliveryOrder.
     * @ORM\OneToOne(targetEntity="ModuleDelivery\Entity\DeliveryOrder")
     * @ORM\JoinColumn(name="delivery_order_id", referencedColumnName="id")
     */
    private $deliveryOrder;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deal
     *
     * @param \Application\Entity\Deal $deal
     *
     * @return Delivery
     */
    public function setDeal(\Application\Entity\Deal $deal = null)
    {
        $this->deal = $deal;

        return $this;
    }

    /**
     * Get deal
     *
     * @return \Application\Entity\Deal
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * Set deliveryDate
     *
     * @param \DateTime $deliveryDate
     *
     * @return Delivery
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    /**
     * Get deliveryDate
     *
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * Set deliveryStatus
     *
     * @param boolean $deliveryStatus
     *
     * @return Delivery
     */
    public function setDeliveryStatus($deliveryStatus)
    {
        $this->deliveryStatus = $deliveryStatus;

        return $this;
    }

    /**
     * Get deliveryStatus
     *
     * @return boolean
     */
    public function getDeliveryStatus()
    {
        return $this->deliveryStatus;
    }

    /**
     * @return mixed
     */
    public function getDeliveryOrder()
    {
        return $this->deliveryOrder;
    }

    /**
     * @param mixed $deliveryOrder
     */
    public function setDeliveryOrder($deliveryOrder)
    {
        $this->deliveryOrder = $deliveryOrder;
    }
}

