<?php
namespace Application\Entity;

use Application\Service\Deal\DealDeadlineManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Dispute
 * @package Application\Entity
 *
 * @ORM\Table(name="dispute")
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\DisputeRepository");
 */
class Dispute
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="text", length=20000, nullable=false)
     */
    private $reason;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var boolean
     *
     * @ORM\Column(name="closed", type="boolean", nullable=true, unique=false, options={"default" : 0})
     */
    private $closed;

    /**
     * One Dispute has One DealAgent. Unidirectional.
     * @ORM\OneToOne(targetEntity="DealAgent")
     * @ORM\JoinColumn(name="deal_agent_id", referencedColumnName="id")
     */
    private $author;

    /**
     * One Dispute has One Deal. Bidirectional.
     * @ORM\OneToOne(targetEntity="Deal", inversedBy="dispute")
     * @ORM\JoinColumn(name="deal_id", referencedColumnName="id")
     */
    private $deal;

    /**
     * Many Dispute have Many Messages.
     * @ORM\ManyToMany(targetEntity="ModuleMessage\Entity\Message")
     * @ORM\JoinTable(name="dispute_message",
     *      joinColumns={@ORM\JoinColumn(name="dispute_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="message_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $messages;

    /**
     * One Refund has One Dispute.
     * @ORM\OneToOne(targetEntity="Refund", mappedBy="dispute")
     */
    private $refund;

    /**
     * Many Disputes have Many DealFiles.
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File")
     * @ORM\JoinTable(name="dispute_file",
     *      joinColumns={@ORM\JoinColumn(name="dispute_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $disputeFiles;

    /**
     * @ORM\OneToMany(targetEntity="WarrantyExtension", mappedBy="dispute")
     */
    private $warrantyExtensions;

    /**
     * @ORM\OneToOne(targetEntity="Discount", mappedBy="dispute")
     */
    private $discount;

    /**
     * @ORM\OneToOne(targetEntity="Tribunal", mappedBy="dispute")
     */
    private $tribunal;

    /**
     * @ORM\OneToMany(targetEntity="DiscountRequest", mappedBy="dispute")
     */
    private $discountRequests;

    /**
     * @ORM\OneToMany(targetEntity="RefundRequest", mappedBy="dispute")
     */
    private $refundRequests;

    /**
     * @ORM\OneToMany(targetEntity="TribunalRequest", mappedBy="dispute")
     */
    private $tribunalRequests;
    
    /**
     * @ORM\OneToMany(targetEntity="ArbitrageRequest", mappedBy="dispute")
     */
    private $arbitrageRequests;

    /**
     * @ORM\OneToMany(targetEntity="DisputeCycle", mappedBy="dispute")
     */
    private $disputeCycles;


    /**
     * Dispute constructor.
     */
    public function __construct()
    {
        $this->messages             = new ArrayCollection;
        $this->disputeFiles         = new ArrayCollection;
        $this->warrantyExtensions   = new ArrayCollection;
        $this->discountRequests     = new ArrayCollection;
        $this->refundRequests       = new ArrayCollection;
        $this->tribunalRequests     = new ArrayCollection;
        $this->arbitrageRequests    = new ArrayCollection;
        $this->disputeCycles        = new ArrayCollection;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason(string $reason)
    {
        $this->reason = $reason;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * @param mixed $deal
     */
    public function setDeal($deal)
    {
        $this->deal = $deal;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @param $message
     */
    public function addMessage($message)
    {
        $this->messages->add($message);
    }

    /**
     * @param $message
     * @return $this
     */
    public function removeMessage($message)
    {
        $this->messages->removeElement($message);

        return $this;
    }

    /**
     * @return bool
     */
    public function isClosed()
    {
        return $this->closed;
    }

    /**
     * @param bool $closed
     */
    public function setClosed(bool $closed)
    {
        $this->closed = $closed;
    }

    /**
     * @return mixed
     */
    public function getRefund()
    {
        return $this->refund;
    }

    /**
     * @param mixed $refund
     */
    public function setRefund($refund)
    {
        $this->refund = $refund;
    }

    /**
     * @return mixed
     */
    public function getDisputeFiles()
    {
        return $this->disputeFiles;
    }

    /**
     * @param mixed $disputeFiles
     */
    public function setDisputeFiles($disputeFiles)
    {
        $this->disputeFiles = $disputeFiles;
    }

    /**
     * @param $disputeFile
     */
    public function addDisputeFile($disputeFile)
    {
        $this->disputeFiles->add($disputeFile);
    }

    /**
     * @param $disputeFile
     * @return $this
     */
    public function removeDisputeFile($disputeFile)
    {
        $this->disputeFiles->removeElement($disputeFile);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWarrantyExtensions()
    {
        return $this->warrantyExtensions;
    }

    /**
     * @param $warrantyExtensions
     */
    public function setWarrantyExtensions($warrantyExtensions)
    {
        $this->warrantyExtensions = $warrantyExtensions;
    }

    /**
     * @param $warrantyExtension
     */
    public function addWarrantyExtension($warrantyExtension)
    {
        $this->warrantyExtensions->add($warrantyExtension);
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getTribunal()
    {
        return $this->tribunal;
    }

    /**
     * @param mixed $tribunal
     */
    public function setTribunal($tribunal)
    {
        $this->tribunal = $tribunal;
    }

    /**
     * @return mixed
     */
    public function getDiscountRequests()
    {
        return $this->discountRequests;
    }

    /**
     * @param mixed $discountRequests
     */
    public function setDiscountRequests($discountRequests)
    {
        $this->discountRequests = $discountRequests;
    }

    /**
     * @param $discountRequest
     */
    public function addDiscountRequest($discountRequest)
    {
        $this->discountRequests->add($discountRequest);
    }

    /**
     * @return mixed
     */
    public function getRefundRequests()
    {
        return $this->refundRequests;
    }

    /**
     * @param mixed $refundRequests
     */
    public function setRefundRequests($refundRequests)
    {
        $this->refundRequests = $refundRequests;
    }

    /**
     * @param $refundRequest
     */
    public function addRefundRequest($refundRequest)
    {
        $this->refundRequests->add($refundRequest);
    }

    /**
     * @return mixed
     */
    public function getTribunalRequests()
    {
        return $this->tribunalRequests;
    }

    /**
     * @param mixed $tribunalRequests
     */
    public function setTribunalRequests($tribunalRequests)
    {
        $this->tribunalRequests = $tribunalRequests;
    }

    /**
     * @param $tribunalRequest
     */
    public function addTribunalRequest($tribunalRequest)
    {
        $this->tribunalRequests->add($tribunalRequest);
    }
    
    /**
     * @return mixed
     */
    public function getArbitrageRequests()
    {
        return $this->arbitrageRequests;
    }

    /**
     * @param mixed $arbitrageRequests
     */
    public function setArbitrageRequests($arbitrageRequests)
    {
        $this->arbitrageRequests = $arbitrageRequests;
    }

    /**
     * @param $arbitrageRequest
     */
    public function addArbitrageRequest($arbitrageRequest)
    {
        $this->arbitrageRequests->add($arbitrageRequest);
    }

    /**
     * @return mixed
     */
    public function getDisputeCycles()
    {
        return $this->disputeCycles;
    }

    /**
     * @param mixed $disputeCycles
     */
    public function setDisputeCycles($disputeCycles)
    {
        $this->disputeCycles = $disputeCycles;
    }

    /**
     * @param $disputeCycle
     */
    public function addDisputeCycle($disputeCycle)
    {
        $this->disputeCycles->add($disputeCycle);
    }
}