<?php

namespace Application\Entity;

use Application\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CivilLawSubject
 *
 * @ORM\Table(name="civil_law_subject", indexes={@ORM\Index(name="fk_civil_law_subject_user1_idx", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\CivilLawSubjectRepository")
 */
class CivilLawSubject
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_pattern", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $isPattern;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $created;

    /**
     * @var \Application\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User", inversedBy="civilLawSubjects")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \Application\Entity\NaturalPerson
     *
     * @ORM\OneToOne(targetEntity="NaturalPerson", mappedBy="civilLawSubject")
     */
    private $naturalPerson;

    /**
     * @var \Application\Entity\SoleProprietor
     *
     * @ORM\OneToOne(targetEntity="SoleProprietor", mappedBy="civilLawSubject")
     */
    private $soleProprietor;

    /**
     * @var \Application\Entity\LegalEntity
     *
     * @ORM\OneToOne(targetEntity="LegalEntity", mappedBy="civilLawSubject")
     */
    private $legalEntity;

    /**
     * @var \Application\Entity\PaymentMethod
     *
     * @ORM\OneToMany(targetEntity="PaymentMethod", mappedBy="civilLawSubject")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $paymentMethods;

    /**
     * @ORM\OneToMany(targetEntity="DealAgent", mappedBy="civilLawSubject")
     */
    private $dealAgents;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Entity\NdsType")
     * @ORM\JoinColumn(name="nds_type_id", referencedColumnName="id")
     */
    private $ndsType;


    public function __construct()
    {
        $this->paymentMethods = new ArrayCollection();
        $this->dealAgents = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isPattern
     *
     * @param boolean $isPattern
     *
     * @return CivilLawSubject
     */
    public function setIsPattern($isPattern)
    {
        $this->isPattern = $isPattern;

        return $this;
    }

    /**
     * Get isPattern
     *
     * @return boolean
     */
    public function getIsPattern()
    {
        return $this->isPattern;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return CivilLawSubject
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set user
     *
     * @param \Application\Entity\User $user
     *
     * @return CivilLawSubject
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set naturalPerson
     *
     * @param NaturalPerson $naturalPerson
     *
     * @return CivilLawSubject
     */
    public function setNaturalPerson(NaturalPerson $naturalPerson = null)
    {
        $this->naturalPerson = $naturalPerson;

        return $this;
    }

    /**
     * Get naturalPerson
     *
     * @return NaturalPerson
     */
    public function getNaturalPerson()
    {
        return $this->naturalPerson;
    }

    /**
     * @return SoleProprietor|null
     */
    public function getSoleProprietor()
    {
        return $this->soleProprietor;
    }

    /**
     * @param SoleProprietor|null $soleProprietor
     */
    public function setSoleProprietor($soleProprietor)
    {
        $this->soleProprietor = $soleProprietor;
    }

    /**
     * Set legalEntity
     *
     * @param LegalEntity $legalEntity
     *
     * @return CivilLawSubject
     */
    public function setLegalEntity(LegalEntity $legalEntity = null)
    {
        $this->legalEntity = $legalEntity;

        return $this;
    }

    /**
     * Get legalEntity
     *
     * @return LegalEntity
     */
    public function getLegalEntity()
    {
        return $this->legalEntity;
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @return CivilLawSubject
     */
    public function addPaymentMethod(PaymentMethod $paymentMethod)
    {
        $this->paymentMethods[] = $paymentMethod;

        return $this;
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @return CivilLawSubject
     */
    public function removePaymentMethod(PaymentMethod $paymentMethod)
    {
        $this->paymentMethods->removeElement($paymentMethod);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPaymentMethods()
    {
        return $this->paymentMethods;
    }

    /**
     * @return mixed
     */
    public function getDealAgents()
    {
        return $this->dealAgents;
    }

    /**
     * @param mixed $dealAgents
     */
    public function setDealAgents($dealAgents)
    {
        $this->dealAgents = $dealAgents;
    }

    /**
     * @return NdsType
     */
    public function getNdsType()
    {
        return $this->ndsType;
    }

    /**
     * @param NdsType $ndsType
     */
    public function setNdsType($ndsType)
    {
        $this->ndsType = $ndsType;
    }
}

