<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tribunal
 * @package Application\Entity
 *
 * @ORM\Table(name="tribunal")
 * @ORM\Entity();
 */
class Tribunal
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * One Tribunal has One Dispute. Bidirectional.
     * @ORM\OneToOne(targetEntity="Dispute", inversedBy="tribunal")
     * @ORM\JoinColumn(name="dispute_id", referencedColumnName="id")
     */
    private $dispute;

    /**
     * One Tribunal has One DisputeCycle. Bidirectional.
     * @ORM\OneToOne(targetEntity="DisputeCycle", inversedBy="tribunal")
     * @ORM\JoinColumn(name="dispute_cycle_id", referencedColumnName="id")
     */
    private $disputeCycle;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getDispute()
    {
        return $this->dispute;
    }

    /**
     * @param mixed $dispute
     */
    public function setDispute($dispute)
    {
        $this->dispute = $dispute;
    }

    /**
     * @return mixed
     */
    public function getDisputeCycle()
    {
        return $this->disputeCycle;
    }

    /**
     * @param mixed $disputeCycle
     */
    public function setDisputeCycle($disputeCycle)
    {
        $this->disputeCycle = $disputeCycle;
    }
}