<?php
namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class DisputeCycle
 * @package Application\Entity
 *
 * @ORM\Table(name="dispute_cycle")
 * @ORM\Entity();
 */
class DisputeCycle
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closed", type="datetime", nullable=true)
     */
    private $closed;

    /**
     * @var Dispute
     *
     * @ORM\ManyToOne(targetEntity="Dispute", inversedBy="disputeCycles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dispute_id", referencedColumnName="id")
     * })
     */
    private $dispute;

    /**
     * @ORM\OneToMany(targetEntity="DiscountRequest", mappedBy="disputeCycle")
     */
    private $discountRequests;

    /**
     * @ORM\OneToMany(targetEntity="RefundRequest", mappedBy="disputeCycle")
     */
    private $refundRequests;

    /**
     * @ORM\OneToMany(targetEntity="TribunalRequest", mappedBy="disputeCycle")
     */
    private $tribunalRequests;

    /**
     * @ORM\OneToMany(targetEntity="ArbitrageRequest", mappedBy="disputeCycle")
     */
    private $arbitrageRequests;

    /**
     * One Discount has One DisputeCycle.
     * @ORM\OneToOne(targetEntity="Discount", mappedBy="disputeCycle")
     */
    private $discount;

    /**
     * One Refund has One DisputeCycle.
     * @ORM\OneToOne(targetEntity="Refund", mappedBy="disputeCycle")
     */
    private $refund;

    /**
     * One Tribunal has One DisputeCycle.
     * @ORM\OneToOne(targetEntity="Tribunal", mappedBy="disputeCycle")
     */
    private $tribunal;

    /**
     * @ORM\OneToMany(targetEntity="WarrantyExtension", mappedBy="disputeCycle")
     */
    private $warrantyExtensions;

    /**
     * DisputeCycle constructor.
     */
    public function __construct()
    {
        $this->discountRequests   = new ArrayCollection;
        $this->refundRequests     = new ArrayCollection;
        $this->tribunalRequests   = new ArrayCollection;
        $this->arbitrageRequests  = new ArrayCollection;
        $this->warrantyExtensions = new ArrayCollection;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime|null
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * @param \DateTime $closed
     */
    public function setClosed(\DateTime $closed)
    {
        $this->closed = $closed;
    }

    /**
     * @return Dispute
     */
    public function getDispute(): Dispute
    {
        return $this->dispute;
    }

    /**
     * @param Dispute $dispute
     */
    public function setDispute(Dispute $dispute)
    {
        $this->dispute = $dispute;
    }

    /**
     * @return mixed
     */
    public function getDiscountRequests()
    {
        return $this->discountRequests;
    }

    /**
     * @param mixed $discountRequests
     */
    public function setDiscountRequests($discountRequests)
    {
        $this->discountRequests = $discountRequests;
    }

    /**
     * @param $discountRequest
     */
    public function addDiscountRequest($discountRequest)
    {
        $this->discountRequests->add($discountRequest);
    }

    /**
     * @return mixed
     */
    public function getRefundRequests()
    {
        return $this->refundRequests;
    }

    /**
     * @param mixed $refundRequests
     */
    public function setRefundRequests($refundRequests)
    {
        $this->refundRequests = $refundRequests;
    }

    /**
     * @param $refundRequest
     */
    public function addRefundRequest($refundRequest)
    {
        $this->refundRequests->add($refundRequest);
    }

    /**
     * @return mixed
     */
    public function getTribunalRequests()
    {
        return $this->tribunalRequests;
    }

    /**
     * @param mixed $tribunalRequests
     */
    public function setTribunalRequests($tribunalRequests)
    {
        $this->tribunalRequests = $tribunalRequests;
    }

    /**
     * @param $tribunalRequest
     */
    public function addTribunalRequest($tribunalRequest)
    {
        $this->tribunalRequests->add($tribunalRequest);
    }

    /**
     * @return mixed
     */
    public function getArbitrageRequests()
    {
        return $this->arbitrageRequests;
    }

    /**
     * @param mixed $arbitrageRequests
     */
    public function setArbitrageRequests($arbitrageRequests)
    {
        $this->arbitrageRequests = $arbitrageRequests;
    }

    /**
     * @param $arbitrageRequest
     */
    public function addArbitrageRequest($arbitrageRequest)
    {
        $this->arbitrageRequests->add($arbitrageRequest);
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getRefund()
    {
        return $this->refund;
    }

    /**
     * @param mixed $refund
     */
    public function setRefund($refund)
    {
        $this->refund = $refund;
    }

    /**
     * @return mixed
     */
    public function getTribunal()
    {
        return $this->tribunal;
    }

    /**
     * @param mixed $tribunal
     */
    public function setTribunal($tribunal)
    {
        $this->tribunal = $tribunal;
    }
    /**
     * @return mixed
     */
    public function getWarrantyExtensions()
    {
        return $this->warrantyExtensions;
    }

    /**
     * @param $warrantyExtensions
     */
    public function setWarrantyExtensions($warrantyExtensions)
    {
        $this->warrantyExtensions = $warrantyExtensions;
    }

    /**
     * @param $warrantyExtension
     */
    public function addWarrantyExtension($warrantyExtension)
    {
        $this->warrantyExtensions->add($warrantyExtension);
    }

}