<?php

namespace Application\Entity;

use Application\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * DealAgent
 *
 * @ORM\Table(name="deal_agent")
 * @ORM\Entity
 */
class DealAgent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, precision=0, scale=0, nullable=false, unique=false)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_of_confirm", type="datetime", nullable=true)
     */
    private $dateOfConfirm;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invitation_date", type="datetime", nullable=true)
     */
    private $invitationDate;

    /**
     * @var \Application\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User", inversedBy="dealAgents")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @var CivilLawSubject
     *
     * @ORM\ManyToOne(targetEntity="CivilLawSubject", inversedBy="dealAgents")
     * @ORM\JoinColumn(name="civil_law_subject_id", referencedColumnName="id", nullable=true)
     */
    private $civilLawSubject;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deal_agent_confirm", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dealAgentConfirm;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deal_agent_once_confirm", type="boolean", precision=0, scale=0, nullable=true, unique=false, options={"default" = false})
     */
    private $dealAgentOnceConfirm;

    /**
     * @ORM\OneToMany(targetEntity="Deal", mappedBy="customer")
     */
    private $customerDeals;

    /**
     * @ORM\OneToMany(targetEntity="Deal", mappedBy="contractor")
     */
    private $contractorDeals;


    public function __construct()
    {
        $this->customerDeals = new ArrayCollection();
        $this->contractorDeals = new ArrayCollection;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return DealAgent
     */
    public function setEmail($email)
    {
        $this->email = strtolower($email);

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateOfConfirm()
    {
        return $this->dateOfConfirm;
    }

    /**
     * @param \DateTime|null $dateOfConfirm
     */
    public function setDateOfConfirm($dateOfConfirm)
    {
        $this->dateOfConfirm = $dateOfConfirm;
    }

    /**
     * @return \DateTime|null
     */
    public function getInvitationDate()
    {
        return $this->invitationDate;
    }

    /**
     * @param \DateTime|null $invitationDate
     */
    public function setInvitationDate($invitationDate)
    {
        $this->invitationDate = $invitationDate;
    }

    /**
     * Get user
     *
     * @return \Application\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param User|null $user
     */
    public function setUser(User $user=null)
    {
        $this->user = $user;
    }

    /**
     * @return CivilLawSubject|null
     */
    public function getCivilLawSubject()
    {
        return $this->civilLawSubject;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     */
    public function setCivilLawSubject(CivilLawSubject $civilLawSubject = null)
    {
        $this->civilLawSubject = $civilLawSubject;
    }

    /**
     * Set dealAgentConfirm
     *
     * @param boolean $dealAgentConfirm
     *
     * @return DealAgent
     */
    public function setDealAgentConfirm($dealAgentConfirm)
    {
        $this->dealAgentConfirm = $dealAgentConfirm;

        return $this;
    }

    /**
     * Get dealAgentConfirm
     *
     * @return boolean
     */
    public function getDealAgentConfirm()
    {
        return $this->dealAgentConfirm;
    }

    /**
     * @return mixed
     */
    public function getCustomerDeals()
    {
        return $this->customerDeals;
    }

    /**
     * @param mixed $customerDeals
     */
    public function setCustomerDeals($customerDeals)
    {
        $this->customerDeals = $customerDeals;
    }

    /**
     * @return mixed
     */
    public function getContractorDeals()
    {
        return $this->contractorDeals;
    }

    /**
     * @param mixed $contractorDeals
     */
    public function setContractorDeals($contractorDeals)
    {
        $this->contractorDeals = $contractorDeals;
    }

    /**
     * @return bool
     */
    public function isDealAgentOnceConfirm(): bool
    {
        return $this->dealAgentOnceConfirm;
    }

    /**
     * @param bool $dealAgentOnceConfirm
     */
    public function setDealAgentOnceConfirm(bool $dealAgentOnceConfirm)
    {
        $this->dealAgentOnceConfirm = $dealAgentOnceConfirm;
    }
}

