<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ModuleFileManager\Entity\File;

/**
 * NaturalPersonPassport
 *
 * @ORM\Table(name="natural_person_passport", indexes={@ORM\Index(name="fk_natural_person_passport_natural_person_document1_idx", columns={"natural_person_document_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\NaturalPersonPassportRepository")
 */
class NaturalPersonPassport
{
    const PATH_FILES = "/natural-person-passport";

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="passport_serial_number", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $passportSerialNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_issued", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateIssued;

    /**
     * @var string
     *
     * @ORM\Column(name="passport_issue_organisation", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $passportIssueOrganisation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_expired", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $dateExpired;

    /**
     * @var \Application\Entity\NaturalPersonDocument
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\NaturalPersonDocument", inversedBy="naturalPersonPassport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="natural_person_document_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $naturalPersonDocument;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File", fetch="EAGER")
     * @ORM\JoinTable(name="natural_person_passport_file",
     *   joinColumns={@ORM\JoinColumn(name="natural_person_passport_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id")}
     * )
     */
    private $files;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set passportSerialNumber
     *
     * @param string $passportSerialNumber
     *
     * @return NaturalPersonPassport
     */
    public function setPassportSerialNumber($passportSerialNumber)
    {
        $this->passportSerialNumber = $passportSerialNumber;

        return $this;
    }

    /**
     * Get passportSerialNumber
     *
     * @return string
     */
    public function getPassportSerialNumber()
    {
        return $this->passportSerialNumber;
    }

    /**
     * Set dateIssued
     *
     * @param \DateTime $dateIssued
     *
     * @return NaturalPersonPassport
     */
    public function setDateIssued($dateIssued)
    {
        $this->dateIssued = $dateIssued;

        return $this;
    }

    /**
     * Get dateIssued
     *
     * @return \DateTime
     */
    public function getDateIssued()
    {
        return $this->dateIssued;
    }

    /**
     * Set passportIssueOrganisation
     *
     * @param string $passportIssueOrganisation
     *
     * @return NaturalPersonPassport
     */
    public function setPassportIssueOrganisation($passportIssueOrganisation)
    {
        $this->passportIssueOrganisation = $passportIssueOrganisation;

        return $this;
    }

    /**
     * Get passportIssueOrganisation
     *
     * @return string
     */
    public function getPassportIssueOrganisation()
    {
        return $this->passportIssueOrganisation;
    }

    /**
     * Set dateExpired
     *
     * @param \DateTime $dateExpired
     *
     * @return NaturalPersonPassport
     */
    public function setDateExpired($dateExpired)
    {
        $this->dateExpired = $dateExpired;

        return $this;
    }

    /**
     * Get dateExpired
     *
     * @return \DateTime
     */
    public function getDateExpired()
    {
        return $this->dateExpired;
    }

    /**
     * Set naturalPersonDocument
     *
     * @param \Application\Entity\NaturalPersonDocument $naturalPersonDocument
     *
     * @return NaturalPersonPassport
     */
    public function setNaturalPersonDocument(\Application\Entity\NaturalPersonDocument $naturalPersonDocument = null)
    {
        $this->naturalPersonDocument = $naturalPersonDocument;

        return $this;
    }

    /**
     * Get naturalPersonDocument
     *
     * @return \Application\Entity\NaturalPersonDocument
     */
    public function getNaturalPersonDocument()
    {
        return $this->naturalPersonDocument;
    }

    /**
     * @return Collection
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    /**
     * @param Collection $files
     */
    public function setFiles(Collection $files)
    {
        $this->files = $files;
    }

    /**
     * Assigns a file to test
     * @param File $file
     * @return $this
     */
    public function addFile(File $file)
    {
        if (!$this->files->contains($file)) {
            $this->files->add($file);
        }

        return $this;
    }

    /**
     * Remove file from collection
     * @param File $file
     * @return $this
     */
    public function removeFile(File $file)
    {
        $this->files->removeElement($file);

        return $this;
    }
}

