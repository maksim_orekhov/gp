<?php
namespace Application\Entity\Repository;

use Application\Entity\User;
use Doctrine\ORM\QueryBuilder;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\Paginator\Paginator;
use ModuleMessage\Entity\Message;

/**
 * Class DealRepository
 * @package Application\Entity\Repository
 */
class DealRepository extends BaseRepository
{
    const PER_PAGE_DEFAULT = 10;

    const ALIASES = [
        'deal_id' => 'd.id',
        'deal_number' => 'd.number',
        'deal_name' => 'd.name',
        'deal_created_from' => 'd.created',
        'deal_created_till' => 'd.created',
        'deal_amount' => 'payment.dealValue',
        'deal_role' => 'is_customer',
        'deal_owner' => 'owner_user.login',
    ];

    /**
     * @var string
     */
    private $alias_name;

    /**
     * @param User $user
     * @param array $params
     * @param bool $is_operator
     * @param $GPAccountNumber
     * @return array|Paginator
     */
    public function selectSortedDealsForUser(User $user, array $params, bool $is_operator)
    {
        // NativeSqlQuery
        #$result = $this->executeNativeSqlQuery($user, $params);

        // QueryBuilderQuery
        $result = $this->executeQueryBuilderQuery($user, $params, $is_operator);

        return $result;
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $is_operator
     * @param $GPAccountNumber
     * @return array|Paginator
     */
    private function executeQueryBuilderQuery(User $user, array $params, bool $is_operator)
    {
        $qb = $this->createQueryBuilder('d')
            ->addSelect('type') // d.deal_type (dealType.id)
            ->leftJoin('d.dealType', 'type')
            #->leftJoin('\Application\Entity\DealType', 't', 'WITH', 'd.dealType = t.id')
            ;

        $qb->addSelect('customer') // dealAgent (customer)
            ->leftJoin('d.customer', 'customer')

            ->addSelect('partial customer_user.{id, login}') // {user.id, user.login} (customer)
            ->leftJoin('customer.user', 'customer_user')

            ->addSelect('contractor') // dealAgent (contractor)
            ->leftJoin('d.contractor', 'contractor')

            ->addSelect('partial contractor_user.{id, login}') // {user.id и user.login} (contractor)
            ->leftJoin('contractor.user', 'contractor_user')

            ->addSelect('payment') // payment {payment}
            ->leftJoin('d.payment', 'payment') //  payment.id = deal.id

            ->addSelect('fee_payer_option')
            ->leftJoin('d.feePayerOption', 'fee_payer_option')

            ->addSelect('deal_contract_file')
            ->leftJoin('d.dealContractFile', 'deal_contract_file')

            ->addSelect('deal_token')
            ->leftJoin('d.dealToken', 'deal_token')



            ->addSelect("(CASE WHEN customer.user = :user THEN '1' ELSE '0' END) as is_customer") // is user customer
        ;
        // Important! is_customer нужен для сортировки по роли.
        // Показывает, является ли текущий пользователем Покупателем или нет.

        // Для Оператора (для сортировки по owner)
        if ($is_operator) {
            $qb->addSelect('owner') // dealAgent (owner)
            ->leftJoin('d.owner', 'owner')
            ->addSelect('owner_user') // User
            ->leftJoin('owner.user', 'owner_user')
            ;
        }

        if (isset($params['unanswered'])) {
            $qb->andWhere('(customer.user = :user AND customer.civilLawSubject is null) OR
                           (contractor.civilLawSubject is null AND contractor.user = :user)');
        }

        // Добавляем к запросу параметры фильтрации
        if (array_key_exists('filter', $params)) {
            $this->addQueryBuilderFilterParams($qb, $params['filter']);
        }

        if (false === $is_operator) {
            // Important!
            $qb->andWhere('customer.user = :user OR contractor.user = :user');
        }

        // Добавляем к запросу параметры сортировки
        if (array_key_exists('order', $params)) {
            $this->addQueryBuilderSortParams($qb, $params['order']);
        } else {
            $qb->addOrderBy('d.id', 'DESC');
        }

        $qb->setParameter('user', $user);

        if (isset($params['per_page']) && isset($params['page'])) {
            $dealCollection = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $dealCollection = $qb->getQuery()->getResult();
        }

        // Исключаем ключ 'is_customer' из элементов коллекции. Остаются только объекты Deal
        foreach ($dealCollection as $key=>$deal) {
            $deal[0]->isCustomer = (bool)$deal['is_customer'];
            $dealCollection->getCurrentItems()[$key] = $deal[0];
        }

        return $dealCollection;
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderFilterParams(QueryBuilder $qb, array $params)
    {
        foreach ($params as $item) {
            // exception_cases
            if ($item['property'] === 'deal_role') {
                if ($item['value'] === 'customer') {
                    $qb->andWhere('customer.user = :user');
                } elseif ($item['value'] === 'contractor') {
                    $qb->andWhere('contractor.user = :user');
                }
                // Important Выходим из итерации цикла
                break;
            }
            if ($item['property'] === 'deal_debit_status') {

                $comparison_sign = (int)$item['value'] === 2 ? '<' : '>';

                $qb->andWhere('payment.expectedValue '.$comparison_sign.' (SELECT SUM(bcpo.amount)
                                    FROM \ModulePaymentOrder\Entity\PaymentOrder po
                                    INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo
                                    WITH bcpo.paymentOrder = po
                                    WHERE po MEMBER OF payment.paymentOrders AND bcpo.type = :incoming_type)')
                    ->setParameter('incoming_type', PaymentOrderManager::TYPE_INCOMING);

                // Important Выходим из итерации цикла
                break;
            }
            // common cases
            $qb->andWhere(
                self::ALIASES[$item['property']] . ' ' . self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['operator'] . ' :' . $item['property'])
                ->setParameter($item['property'],
                    self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['pre_item'] . $item['value'] . self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['post_item']
                );
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderSortParams(QueryBuilder $qb, array $params)
    {
        foreach ($params as $key=>$value) {
            switch ($key) {
                case 'number':
                    $qb->addOrderBy('d.number', $value);
                    break;
                case 'name':
                    $qb->addOrderBy('d.name', $value);
                    break;
                case 'amount':
                    $qb->addOrderBy('payment.expectedValue', $value);
                    break;
                case 'created':
                    $qb->addOrderBy('d.created', $value);
                    break;
                case 'role':
                    $qb->addOrderBy('is_customer', $value);
                    break;
                case 'owner':
                    $qb->addOrderBy('owner_user.login', $value);
                    break;
                default:
                    $qb->addOrderBy('d.id', 'DESC');
            }
        }
    }

    /**
     * Выборка сделки по вхождению комментария в коллекцию deal.messages
     *
     * @param Message $comment
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getDealByComment(Message $comment)
    {
        $qb = $this->createQueryBuilder('d')
            ->andWhere(':comment MEMBER OF d.messages')
            ->setParameter('comment', $comment);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param array $params
     * @return array|Paginator
     */
    public function selectSortedDealsByDeadlineForUser(array $params)
    {
        $qb = $this->createQueryBuilder('d')
            ->addSelect('type')
            ->leftJoin('d.dealType', 'type');

//        $qb->andWhere('customer.user = :user OR contractor.user = :user');

        // Добавляем к запросу параметры фильтрации
        if (array_key_exists('filter', $params)) {
//            $this->addQueryBuilderFilterParams($qb, $params['filter']);
        }

        // Добавляем к запросу параметры сортировки
//        if (key_exists('order', $params)) {
//            $this->addQueryBuilderSortParams($qb, $params['order']);
//        } else {
//            $qb->addOrderBy('d.id', 'DESC');
//        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $dealByDeadlineCollection = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $dealByDeadlineCollection = $qb->getQuery()->getResult();
        }

        return $dealByDeadlineCollection;
    }

    /**
     * Get all allowed deals
     *
     * @param User $user
     * @return array
     */
    public function findAllowedDealByUser(User $user)
    {
        $qb = $this->createQueryBuilder('d')
             ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
            ->andWhere('customer.user = :user OR contractor.user = :user')
            ->setParameter('user', $user);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param array $params
     * @param $is_operator
     * @return array
     */
    public function findCollectionConfirmedDealForInvoice(User $user, $params = [], $is_operator)
    {
        $qb = $this->createQueryBuilder('d')
            ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
            ->addSelect('payment')
            ->leftJoin('d.payment', 'payment');
        // Сделка согласована
        $qb->andWhere('customer.dealAgentConfirm = true') // Customer принял условия
            ->andWhere('contractor.dealAgentConfirm = true') // Contractor принял условия
            ->andWhere('d.dealContractFile is not null') // создан контракт файл
            ->andWhere('d.dateOfConfirm is not null'); //есть дата конфирма
        // Сделка не оплачена
//        $qb->andWhere('payment.deFactoDate is null');

        if (false === $is_operator) {
            // only customer Important!
            $qb->andWhere('customer.user = :user')
                ->setParameter('user', $user);
        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $deals = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $deals = $qb->getQuery()->getResult();
        }

        return $deals;
    }

    /**
     * @param User|null $user
     * @return array|Paginator
     */
    public function findDealsWithoutCLSInDealAgent(User $user = null)
    {
        $qb = $this->createQueryBuilder('d')
            ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
            ->andWhere('contractor.civilLawSubject is null OR customer.civilLawSubject is null')
        ;

        if (null === $user) {
            $qb->andWhere('contractor.civilLawSubject is null OR customer.civilLawSubject is null');
        } else {
            $qb->andWhere('(customer.user = :user AND customer.civilLawSubject is null) OR
                           (contractor.civilLawSubject is null AND contractor.user = :user)')
                ->setParameter('user', $user);
        }

        return $qb->getQuery()->getResult();
    }
}