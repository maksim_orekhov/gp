<?php
namespace Application\Entity\Repository;

use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Paginator;

class BaseRepository extends EntityRepository
{
    const OPERATORS_FOR_FILTER_RULES = [
        'like' => [
            'operator'=>'LIKE',
            'pre_item'=>'%',
            'post_item'=>'%'
        ],
        'greater' => [
            'operator'=>'>',
            'pre_item'=>'',
            'post_item'=>''
        ],
        'smaller' => [
            'operator'=>'<',
            'pre_item'=>'',
            'post_item'=>''
        ],
        'date_greater' => [
            'operator'=>'>',
            'pre_item'=>'',
            'post_item'=>''
        ],
        'date_smaller' => [
            'operator'=>'<',
            'pre_item'=>'',
            'post_item'=>''
        ],
        'equal' => [
            'operator'=>'=',
            'pre_item'=>'',
            'post_item'=>''
        ],
        #'deal_type' => '=',
//        'deal_role' => 'complex',
//        'deal_status',
//        'counteragent',
//        'user_login',
//        'customer_login',
//        'contractor_login',
    ];

    /**
     * @param QueryBuilder $query
     * @param $limit
     * @param $page
     * @return Paginator
     */
    public function paginate(QueryBuilder $query, $page=1, $limit=10)
    {
        $page = (abs((int) $page) > 0) ? $page : 1;
        $limit = (abs((int) $limit) > 0) ? $limit : 10;

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);
        $paginator->setDefaultItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);

        return $paginator;
    }

    /**
     * @param $entity_class
     * @param array $params
     * @return array
     */
    public function getOrder($entity_class, $params = [])
    {
        //default orders
        $order = ['field'=>'id','sort'=>'ASC'];
        $entityManager = $this->getEntityManager();
        $entityFieldNames = $entityManager->getClassMetadata($entity_class)->getFieldNames();

        if (!empty($entityFieldNames) && !empty($params)) {
            foreach ($params as $key => $value) {
                if (in_array($key, $entityFieldNames)) {
                    $order['field'] = $key;
                }

                $order['sort'] = (strtoupper($value) === 'DESC') ? 'DESC' : 'ASC';
            }
        }

        return $order;
    }
}