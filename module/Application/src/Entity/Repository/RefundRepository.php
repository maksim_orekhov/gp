<?php
namespace Application\Entity\Repository;

use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class DiscountRepository
 * @package Application\Entity\Repository
 */
class RefundRepository extends BaseRepository
{
    /**
     * @return array
     */
    public function getRefundWithReadyToPayOffOutgoingPaymentOrders()
    {
        $qb = $this->createQueryBuilder('r')
            ->addSelect('po')
            ->innerJoin('r.paymentOrders', 'po')
            ->addSelect('bcpo')
            ->leftJoin('po.bankClientPaymentOrder', 'bcpo')
            ->andWhere('bcpo.bankClientPaymentOrderFile is null')
            ->andWhere('bcpo.type = :outgoing')
            ->setParameter('outgoing', PaymentOrderManager::TYPE_OUTGOING)
        ;

        return $qb->getQuery()->getResult();
    }
}