<?php
namespace Application\Entity\Repository;

use Application\Entity\Deal;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Service\Deal\DealDeadlineManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\ResultSetMapping;
use ModuleMessage\Entity\Message;
use Doctrine\ORM\QueryBuilder;

/**
 * Class DisputeRepository
 * @package Application\Entity\Repository
 */
class DisputeRepository extends BaseRepository
{
    const PER_PAGE_DEFAULT = 10;

    const ALIASES = [
        'deal_id' => 'deal.id',
        'deal_name' => 'deal.name',
        'deal_number' => 'deal.number',
        'dispute_closed' => 'd.closed',
        'dispute_created_from' => 'd.created',
        'dispute_created_till' => 'd.created',
        'deadline_date_from' => 'custom',
        'deadline_date_till' => 'custom',
        'dispute_author' => 'author_user.login'
    ];

    /**
     * Выборка спора по вхождению комментария в коллекцию deal.messages
     *
     * @param Message $comment
     * @return mixed
     */
    public function getDisputeByComment(Message $comment)
    {
        $qb = $this->createQueryBuilder('d')
            ->andWhere(':comment MEMBER OF d.messages')
            ->setParameter('comment', $comment);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * Получение всех споров с разбиением на страницы
     *
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function getSortedDisputesForUser(array $params)
    {
        // $user и $is_operator пока здесь не нужен. Выбираем только для оператора.

        // QueryBuilderQuery
        $result = $this->executeQueryBuilderQuery($params);

        return $result;
    }

    /**
     * Получение всех открытых споров с разбиением на страницы
     *
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function getSortedOpenDisputes(array $params)
    {
        // QueryBuilderQuery
        $result = $this->executeQueryBuilderQuery($params, true);

        return $result;
    }

    /**
     * Выборка всех споров (со связанными сущностями) с разбиением на страницы
     * Усли $only_open = true, то только открытые споры
     *
     * @param array $params
     * @param bool $only_open
     * @return array|\Zend\Paginator\Paginator
     */
    private function executeQueryBuilderQuery(array $params, $only_open = false)
    {
        $qb = $this->createQueryBuilder('d')
            ->addSelect('author') // d.deal_agent (dealAgent.id)
            ->leftJoin('d.author', 'author')
            ->addSelect('author_user') // a.user (user.id)
            ->leftJoin('author.user', 'author_user')
            ->addSelect('deal') // d.deal (deal.id)
            ->leftJoin('d.deal', 'deal')
            ->leftJoin('deal.payment', 'payment')
            ->addSelect('(SELECT
                                CASE WHEN MAX(we.days) IS NULL THEN DATE_ADD(payment.deFactoDate, deal.deliveryPeriod, \'DAY\')
                                    ELSE DATE_ADD(payment.deFactoDate, (deal.deliveryPeriod+MAX(we.days)), \'DAY\')
                                    END as days
                                FROM \Application\Entity\WarrantyExtension we WHERE we.dispute = d.id) as deadline_date
                                ')
        ;
        // Только открытые споры
        if ($only_open) {
            $qb->andWhere('d.closed = false');
        }

        // custom фильтрация
        if (array_key_exists('filter', $params)) {
            /** @var array $filters */
            $filters = $params['filter'];

            foreach ($filters as $filter) {
                switch ($filter['property']){
                    case 'deadline_date_from' :
                        /** @var \DateTime $deadline_date_from */
                        $deadline_date_from = new \DateTime($filter['value'].' 00:00:01');
                        $from = $deadline_date_from->format('d-m-Y H:i:s');
                        $qb->andWhere("$from <=
                                (SELECT
                                CASE WHEN MAX(we2.days) IS NULL THEN DATE_ADD(payment.deFactoDate, deal.deliveryPeriod, \'DAY\')
                                    ELSE DATE_ADD(payment.deFactoDate, (deal.deliveryPeriod+MAX(we2.days)), 'DAY')
                                    END as days2
                                FROM \Application\Entity\WarrantyExtension we2 WHERE we2.dispute = d.id) as deadline_date2
                                ");
                        break;
                    case 'deadline_date_till' :
                        $deadline_date_till = new \DateTime($filter['value'].' 23:59:59');
                        $till = $deadline_date_till->format('d-m-Y H:i:s');
                        $qb->andWhere("$till >=
                                (SELECT
                                CASE WHEN MAX(we3.days) IS NULL THEN DATE_ADD(payment.deFactoDate, deal.deliveryPeriod, 'DAY')
                                    ELSE DATE_ADD(payment.deFactoDate, (deal.deliveryPeriod+MAX(we3.days)), 'DAY')
                                    END as days3
                                FROM \Application\Entity\WarrantyExtension we3 WHERE we3.dispute = d.id) as deadline_date3
                                ");
                        break;
                }
            }
        }

        #sql_dump($qb->getQuery()->getResult());



//        $preDisputes = $qb_pre->getQuery()->getResult();
//
//        $deadline_date_from = null;
//        $deadline_date_till = null;
//        $pre_disputes_filtered = [];
//
//        // custom фильтрация
//        if (array_key_exists('filter', $params)) {
//            /** @var array $filters */
//            $filters = $params['filter'];
//
//            foreach ($filters as $filter) {
//                switch ($filter['property']){
//                    case 'deadline_date_from' :
//                        $deadline_date_from = new \DateTime($filter['value'].' 00:00:01');
//                        break;
//                    case 'deadline_date_till' :
//                        $deadline_date_till = new \DateTime($filter['value'].' 23:59:59');
//                        break;
//                }
//            }
//        }
//
//        /** @var Dispute $preDispute */
//        foreach ($preDisputes as $key => $preDispute) {
//            /** @var Deal $deal */
//            $deal = $preDispute->getDeal();
//            /** @var Payment $payment */
//            $payment = $deal->getPayment();
//            /** @var \DateTime $dateClosedDeal */
//            $dateClosedDeal = DealDeadlineManager::getDealDeadlineDate($payment);
//
//            if (!($deadline_date_from && $deadline_date_from > $dateClosedDeal)
//                && !($deadline_date_till && $deadline_date_till < $dateClosedDeal)) {
//
//                $pre_disputes_filtered[] = [
//                    'id' => $preDispute->getId(),
//                    'date' => $dateClosedDeal->format('d-m-Y H:i:s')
//                ];
//            }
//        }
//
//        $pre_disputes_ids = [];
//        foreach ($pre_disputes_filtered as $dispute_filtered) {
//            $pre_disputes_ids[] = $dispute_filtered['id'];
//        }
//
//        $qb = $this->createQueryBuilder('d')
//            ->addSelect('author') // d.deal_agent (dealAgent.id)
//            ->leftJoin('d.author', 'author')
//            ->addSelect('author_user') // a.user (user.id)
//            ->leftJoin('author.user', 'author_user')
//            ->addSelect('deal') // d.deal (deal.id)
//            ->leftJoin('d.deal', 'deal')
//            ->addSelect('payment')
//            ->leftJoin('deal.payment', 'payment')
//            ->addSelect('(SELECT
//                                CASE WHEN MAX(we.days) IS NULL THEN DATE_ADD(payment.deFactoDate, deal.deliveryPeriod, \'DAY\')
//                                    ELSE DATE_ADD(payment.deFactoDate, (deal.deliveryPeriod+MAX(we.days)), \'DAY\')
//                                    END as days
//                                FROM \Application\Entity\WarrantyExtension we WHERE we.dispute = d.id) as deadline_date
//                                ')
//            ->andWhere('d.id in (:disputes)')
//            ->setParameter('disputes', $pre_disputes_ids);
//        ;

        // Добавляем к запросу параметры фильтрации
        if (array_key_exists('filter', $params)) {
            $this->addQueryBuilderFilterParams($qb, $params['filter']);
        }

        // Добавляем к запросу параметры сортировки
        if (array_key_exists('order', $params)) {
            $this->addQueryBuilderSortParams($qb, $params['order']);
        } else {
            $qb->addOrderBy('d.id', 'DESC');
        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $disputeCollection = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $disputeCollection = $qb->getQuery()->getResult();
        }

        // Исключаем ключ 'deadline_date' из элементов коллекции. Остаются только объекты Discount
        foreach ($disputeCollection as $key=>$dispute) {
            #$dispute[0]->dealDeadlineDate = (bool)$dispute['deadline_date'];
            $disputeCollection->getCurrentItems()[$key] = $dispute[0];
        }

        return $disputeCollection;
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     * @return bool
     */
    private function addQueryBuilderFilterParams(QueryBuilder $qb, array $params)
    {
        foreach ($params as $item) {

            if(self::ALIASES[$item['property']] !== 'custom') {

                // common cases
                $qb->andWhere(
                    self::ALIASES[$item['property']] . ' ' . self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['operator'] . ' :' . $item['property'])
                    ->setParameter($item['property'],
                        self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['pre_item'] . $item['value'] . self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['post_item']
                    );
            }
        }

        return true;
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderSortParams(QueryBuilder $qb, array $params)
    {
        foreach ($params as $key=>$value) {
            switch ($key) {
                case 'deal_name':
                    $qb->addOrderBy('deal.name', $value);
                    break;
                case 'deal_number':
                    $qb->addOrderBy('deal.number', $value);
                    break;
                case 'closed':
                    $qb->addOrderBy('deal.closed', $value);
                    break;
                case 'dispute_created':
                    $qb->addOrderBy('d.created', $value);
                    break;
                case 'deadline_date':
                    $qb->addOrderBy('deadline_date', $value);
                    break;
                default:
                    $qb->addOrderBy('d.id', 'DESC');
            }
        }
    }
}