<?php
/*
    *****  отладочный sql запрос для выборки payment с датой deadline *****

   SELECT  d.`number`,
        p.de_facto_date,
        d.delivery_period,
        DATE_ADD(p.de_facto_date, interval d.delivery_period DAY) as deadline_with_dp,
        CASE WHEN ws.days IS NULL THEN 0 ELSE ws.days END as ws_days,
        DATE_ADD(p.de_facto_date, interval (d.delivery_period + CASE WHEN ws.days IS NULL THEN 0 ELSE ws.days END) DAY) as deadline_with_dp_ws,
        TIMESTAMPDIFF(DAY, CURRENT_TIMESTAMP(), DATE_ADD(p.de_facto_date, interval (d.delivery_period + CASE WHEN ws.days IS NULL THEN 0 ELSE ws.days END) DAY)) as deadline_day,
        TIMESTAMPDIFF(HOUR, CURRENT_TIMESTAMP(), DATE_ADD(p.de_facto_date, interval (d.delivery_period + CASE WHEN ws.days IS NULL THEN 0 ELSE ws.days END) DAY)) as deadline_hour,
        CURRENT_TIMESTAMP() as curr_date
    FROM deal as d
    INNER JOIN payment as p on p.deal_id = d.id
    INNER JOIN deal_agent as customer on customer.id = d.customer_id
    INNER JOIN deal_agent as contractor on contractor.id = d.contractor_id
    LEFT JOIN dispute ds ON d.id = ds.deal_id
    LEFT JOIN warranty_extension ws ON ws.dispute_id = ds.id AND ws.id = (SELECT MAX(ws2.id) FROM warranty_extension ws2 WHERE ws2.dispute_id = ds.id)
WHERE
    customer.deal_agent_confirm = true
    AND contractor.deal_agent_confirm = true
    AND (SELECT COUNT(cpo.id) FROM bank_client_payment_order cpo WHERE cpo.credit_payment_id = p.id) = 0
    AND (SELECT COUNT(dpo.id) FROM bank_client_payment_order dpo WHERE dpo.debit_payment_id = p.id) > 0

*/

namespace Application\Entity\Repository;

use Application\Entity\Deal;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\WarrantyExtension;
use Application\Service\Deal\DealDeadlineManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\Paginator\Paginator;
use Application\Service\Payment\PaymentPolitics;
use Doctrine\ORM\QueryBuilder;

/**
 * Class PaymentRepository
 * @package Application\Entity\Repository
 */
class PaymentRepository extends BaseRepository
{
    const PER_PAGE_DEFAULT = 10;

    const ALIASES_EXPIRED_DEALS = [
        'deal_id' => 'd.id',
        'deal_name' => 'd.name',
        'deal_created_from' => 'd.created',
        'deal_created_till' => 'd.created',
        'deal_amount' => 'p.dealValue',
        'deal_owner' => 'owner_user.login',
        'deadline_date_from' => 'custom',
        'deadline_date_till' => 'custom',
    ];
    const ALIASES_FOR_DEAL_DEADLINE = [
        'deal_id' => 'd.id',
        'deal_name' => 'd.name',
        'deal_created_from' => 'd.created',
        'deal_created_till' => 'd.created',
        'deal_amount' => 'p.releasedValue',
        'deal_owner' => 'owner_user.login',
    ];

    const SQL_TOTAL_INCOMING_AMOUNT_EXCLUDE_OVERPAID = '(SELECT CASE WHEN ro.amount IS NULL THEN SUM(bcpo77.amount) ELSE (SUM(bcpo77.amount) - ro.amount) END
                                                    FROM \ModulePaymentOrder\Entity\PaymentOrder po77                                    
                                                    INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo77
                                                    WITH bcpo77.paymentOrder = po77
                                                    WHERE po77 MEMBER OF p.paymentOrders
                                                    AND bcpo77.type = :incoming_type
                                                    AND bcpo77.recipientAccount = :gp_account)';

    /**
     * @param $deal_id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function selectPaymentByDealId($deal_id)
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('d')
            ->leftJoin('p.deal', 'd')
            ->andWhere('d.id = :deal_id')
            ->setParameter('deal_id', $deal_id);

        return $qb->getQuery()->getSingleResult();
    }


    /**
     * @TODO не учтены добавленные по спору дни (не используется - удалить?)
     * Нормальные (не проблемные) сделки с подтвержденной доставкой и истекшей датой
     * Не проблемные - сумма входящих платежей совпадает с expectedValue и платежи не просрочены
     *
     * @return array
     */
    public function getPaymentsReadyForOutgoingPayment()
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('d')
            ->leftJoin('p.deal', 'd')
            ->addSelect('ro')
            ->leftJoin('p.repaidOverpay', 'ro')
            ->addSelect('t')
            ->leftJoin('d.dealType', 't')
            ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('cpos')
            ->leftJoin('p.creditPaymentOrders', 'cpos')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
        ;
        // исключаем платежки на возврат - important !!!
        $qb->andWhere('(SELECT COUNT(ds.id)
                            FROM \Application\Entity\Dispute ds
                            Join \Application\Entity\Refund rf WITH rf.dispute = ds.id 
                            WHERE ds.deal = d.id ) = 0');

        // Сделка согласована
        $qb->andWhere('customer.dealAgentConfirm = true')       // Customer принял условия
            ->andWhere('contractor.dealAgentConfirm = true');   // Contractor принял условия

        // Есть debitPaymentOrder
        $qb->andWhere('(SELECT COUNT(dpo1.id) FROM \ModulePaymentOrder\Entity\BankClientPaymentOrder dpo1 WHERE dpo1.debitPayment = p.id) > 0');
        // Доставка подтверждена
        $qb->andWhere('(SELECT COUNT(delivery.id) FROM \Application\Entity\Delivery delivery WHERE delivery.deal = d.id AND delivery.deliveryStatus = true) > 0');
        // Есть одна (пока одна, потом может быть несколько) creditPaymentOrder
        $qb->andWhere('(SELECT COUNT(cpo.id) FROM \ModulePaymentOrder\Entity\BankClientPaymentOrder cpo WHERE cpo.creditPayment = p.id) > 0');
        // Истечение срока по Сделке
        #$qb->andWhere("DATE_ADD(p.deFactoDate, d.deliveryPeriod, 'DAY') < CURRENT_DATE()");
        // Сумма amount в платёжках, не привязанных к файлу на оплату, равна releasedValue из Payment
        $qb->andWhere("p.releasedValue = (SELECT SUM(cpo2.amount) FROM \ModulePaymentOrder\Entity\BankClientPaymentOrder cpo2
                        WHERE cpo2.creditPayment = p.id AND cpo2.bankClientPaymentOrderFile is null)");
        // Исключаем проблемные сделки // Эта часть должна зеркально повторять запрос для выборки проблемных сделок
        $qb->andWhere("p.expectedValue = (SELECT SUM(dpo2.amount) FROM \ModulePaymentOrder\Entity\BankClientPaymentOrder dpo2 WHERE dpo2.debitPayment = p.id)");
        $qb->andWhere("DATE_ADD(d.dateOfConfirm, d.deliveryPeriod, 'DAY')
                        > (SELECT MAX(strtodate(dpo3.documentDate,'%d.%m.%Y 23:59:59')) FROM \ModulePaymentOrder\Entity\BankClientPaymentOrder dpo3 WHERE dpo3.debitPayment = p.id)");


        return $qb->getQuery()->getResult();
    }

    /**
     * @TODO не учтены добавленные по спору дни (не используется - удалить?)
     * Возврат
     *
     * @return array
     *
     */
    public function getPaymentsReadyForRefundPayment()
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('d')
            ->leftJoin('p.deal', 'd')
            ->addSelect('ro')
            ->leftJoin('p.repaidOverpay', 'ro')
            ->addSelect('dispute')
            ->innerJoin('d.dispute', 'dispute')
            ->addSelect('refund')
            ->innerJoin('dispute.refund', 'refund') // Important innerJoin!
            ->addSelect('t')
            ->leftJoin('d.dealType', 't')
            ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('cpos')
            ->leftJoin('p.creditPaymentOrders', 'cpos')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
        ;
        // Сделка согласована
        $qb->andWhere('customer.dealAgentConfirm = true')       // Customer принял условия
            ->andWhere('contractor.dealAgentConfirm = true');   // Contractor принял условия
        // Есть debitPaymentOrder
        $qb->andWhere('(SELECT COUNT(dpo1.id) FROM \ModulePaymentOrder\Entity\BankClientPaymentOrder dpo1 WHERE dpo1.debitPayment = p.id) > 0');
        // Есть одна (пока одна, потом может быть несколько) creditPaymentOrder
        $qb->andWhere('(SELECT COUNT(cpo.id) FROM \ModulePaymentOrder\Entity\BankClientPaymentOrder cpo WHERE cpo.creditPayment = p.id) > 0');
        // Сумма amount в платёжках, не привязанных к файлу на оплату, равна releasedValue из Payment
        $qb->andWhere("p.releasedValue = (SELECT SUM(cpo2.amount) FROM \ModulePaymentOrder\Entity\BankClientPaymentOrder cpo2
                        WHERE cpo2.creditPayment = p.id AND cpo2.bankClientPaymentOrderFile is null)");
        // Исключаем проблемные сделки // Эта часть должна зеркально повторять запрос для выборки проблемных сделок
        $qb->andWhere("p.expectedValue = (SELECT SUM(dpo2.amount) FROM \ModulePaymentOrder\Entity\BankClientPaymentOrder dpo2 WHERE dpo2.debitPayment = p.id AND dpo2.bankClientPaymentOrderFile is null)");

        return $qb->getQuery()->getResult();
    }

    /**
     * Select (with searching and sorting) "expired" Payments for expiredAction
     * Для показа Оператору списка готовых с выплате сделок
     *
     * @param $params
     * @param int $deal_id
     * @param string $gp_account
     * @return Paginator
     */
    public function selectSortedPaymentsWithExpiredDeals($params, string $gp_account, $deal_id = 0)
    {
        //получаем предварительный запрос
        $prePayments = $this->selectPreQueryPaymentsOfDealsPaidByCustomer($gp_account);

        // custom фильтрация
        if (array_key_exists('filter', $params)) {
            /** @var array $filters */
            $filters = $params['filter'];
            $deadline_date_from = null;
            $deadline_date_till = null;

            foreach ($filters as $filter) {
                switch ($filter['property']){
                    case 'deadline_date_from' :
                        $deadline_date_from = new \DateTime($filter['value'].' 00:00:01');
                        break;
                    case 'deadline_date_till' :
                        $deadline_date_till = new \DateTime($filter['value'].' 23:59:59');
                        break;
                }
            }

            foreach ($prePayments as $key =>$prePayment) {
                $dateClosedDeal = DealDeadlineManager::getDealDeadlineDate($prePayment);
                if(($deadline_date_from && $deadline_date_from > $dateClosedDeal) || ($deadline_date_till && $deadline_date_till < $dateClosedDeal)){
                    unset($prePayments[$key]);
                }
            }
        }

        //фильтруем payment по оставшемуся сроку
        $fromHour = 0;
        $id_payments = $this->getFilteredPaymentsIdForDiffHourToCurrentDate($prePayments, $fromHour);

        // general query
        $qb = $this->createQueryBuilder('p')
            ->addSelect('d')
            ->leftJoin('p.deal', 'd')
            ->addSelect('t')
            ->leftJoin('d.dealType', 't')
            ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('customer_user')
            ->leftJoin('customer.user', 'customer_user')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
            ->addSelect('contractor_user')
            ->leftJoin('contractor.user', 'contractor_user')
            ->addSelect('owner')
            ->leftJoin('d.owner', 'owner')
            ->addSelect('owner_user')
            ->leftJoin('owner.user', 'owner_user')
        ;
        $qb->andWhere('p.id IN (:id_payments)');
        #$qb->andWhere('d.dateOfConfirm is not null');
        $qb->andWhere('d.dealContractFile is not null');
        $qb->setParameter('id_payments', $id_payments);
        if ($deal_id > 0){
            $qb->andWhere('d.id = :deal_id');
            $qb->setParameter('deal_id', $deal_id);
        }

        // Добавляем к запросу параметры фильтрации
        if (array_key_exists('filter', $params)) {
            $this->addQueryBuilderFilterParamsForPaymentsWithExpiredDeals($qb, $params['filter']);
        }

        // Добавляем к запросу параметры сортировки
        if (array_key_exists('order', $params)) {
            $this->addQueryBuilderSortParamsForPaymentsWithExpiredDeals($qb, $params['order']);
        } else {
            $qb->addOrderBy('d.id', 'DESC');
        }

        if (array_key_exists('per_page', $params) && array_key_exists('page', $params)) {
            $paymentCollection = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $paymentCollection = $qb->getQuery()->getResult();
        }

        return $paymentCollection;
    }

    /**
     * @param $params
     * @param int $page
     * @param $gp_account
     * @return Paginator
     */
    public function selectSortedPaymentsForAbnormalDeals($params, $page=1, $gp_account)
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('ro')
            ->leftJoin('p.repaidOverpay', 'ro')
            ->addSelect('d')
            ->leftJoin('p.deal', 'd')
            ->addSelect('t')
            ->leftJoin('d.dealType', 't')
            ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('customer_user')
            ->leftJoin('customer.user', 'customer_user')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
            ->addSelect('contractor_user')
            ->leftJoin('contractor.user', 'contractor_user')
        ;

        // для сортировки по owner
        $qb->addSelect('owner') // dealAgent (owner)
            ->leftJoin('d.owner', 'owner')
            ->addSelect('owner_user') // User
            ->leftJoin('owner.user', 'owner_user');

        // Есть incoming PaymentOrder
/*        $qb->andWhere('(SELECT COUNT(bcpo2.id)
                            FROM \ModulePaymentOrder\Entity\PaymentOrder po2
                            INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo2
                            WITH bcpo2.paymentOrder = po2
                            WHERE po2 MEMBER OF p.paymentOrders AND bcpo2.type = :incoming_type
                            ) > 0')
            ->setParameter('incoming_type', PaymentOrderManager::TYPE_INCOMING)
            ;*/

        $abnormal_type = null;
        if (isset($params['search']['abnormal_type'])) {
            $abnormal_type = $params['search']['abnormal_type'];
        }

        // Поиск по типам проблем
        // Если что-то меняется в алгоритме выборки проблемных сделок, так же изменить выборку сделки с истекшей датой!
        switch ($abnormal_type) {

            case PaymentPolitics::CONTRACT_FILE_MISSING: // Отсутствует файл контракта
                $qb->andWhere('d.dateOfConfirm is not null');
                $qb->andWhere('d.dealContractFile is null');
                break;
            case PaymentPolitics::PAYMENT_ORDER_FILE_MISSING: // Отсутствует файл платежек
                $qb->andWhere('(SELECT COUNT(bcpo22.id)
                            FROM \ModulePaymentOrder\Entity\PaymentOrder po22
                            INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo22
                            WITH bcpo22.paymentOrder = po22
                            WHERE po22 MEMBER OF p.paymentOrders 
                            AND bcpo22.type = :incoming_type
                            AND bcpo22.bankClientPaymentOrderFile is null
                            ) > 0')
                    ->setParameter('incoming_type', PaymentOrderManager::TYPE_INCOMING);
                break;
            case PaymentPolitics::AMOUNT_NOT_MATCHED: // Недоплата при истекшем сроке оплаты
                $qb->andWhere("p.expectedValue > (SELECT SUM(bcpo5.amount)
                                    FROM \ModulePaymentOrder\Entity\PaymentOrder po5
                                    INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo5
                                    WITH bcpo5.paymentOrder = po5
                                    WHERE po5 MEMBER OF p.paymentOrders
                                    AND bcpo5.type = :incoming_type
                                    AND bcpo5.recipientAccount = :gp_account)
                                    AND
                                    (
                                        p.plannedDate < (SELECT MAX(strtodate(bcpo6.documentDate,'%d.%m.%Y 23:59:59'))
                                        FROM \ModulePaymentOrder\Entity\PaymentOrder po6
                                        INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo6
                                        WITH bcpo6.paymentOrder = po6
                                        WHERE po6 MEMBER OF p.paymentOrders
                                        AND bcpo6.type = :incoming_type
                                        AND bcpo6.recipientAccount = :gp_account)
                                    OR
                                        p.plannedDate < (SELECT MAX(mpo6.created)
                                        FROM \ModulePaymentOrder\Entity\PaymentOrder po62
                                        INNER JOIN \ModulePaymentOrder\Entity\MandarinPaymentOrder mpo6
                                        WITH mpo6.paymentOrder = po62
                                        WHERE po62 MEMBER OF p.paymentOrders
                                        AND mpo6.type = :incoming_type)
                                    )"
                    )
                    ->setParameter('gp_account', $gp_account);
                ;
                break;

            case PaymentPolitics::AMOUNT_IS_OVERPAID: // Переплата
                $qb->andWhere('p.expectedValue < '.self::SQL_TOTAL_INCOMING_AMOUNT_EXCLUDE_OVERPAID);
                break;

            case PaymentPolitics::DATE_DEBIT_IS_EXPIRED: // Дата последнего платежа просрочена
                $qb->andWhere("p.plannedDate
                                < (SELECT MAX(strtodate(bcpo8.documentDate,'%d.%m.%Y 23:59:59'))
                                    FROM \ModulePaymentOrder\Entity\PaymentOrder po8
                                    INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo8
                                    WITH bcpo8.paymentOrder = po8
                                    WHERE po8 MEMBER OF p.paymentOrders
                                    AND bcpo8.type = :incoming_type
                                    AND bcpo8.recipientAccount = :gp_account
                                   )"
                    )
                    ->setParameter('gp_account', $gp_account);
                ;
                break;

            default: // Сумма сделки с просроченной оплатой не соотвествует общей сумме ИЛИ Дата последнего платежа просрочена ИЛИ нет контракта
/*
                SELECT  SUM(amount)
                FROM
                (
                SELECT SUM(bcpo51.amount) as amount
                FROM \ModulePaymentOrder\Entity\PaymentOrder po51
                INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo51
                WITH bcpo51.paymentOrder = po51
                WHERE po51 MEMBER OF p.paymentOrders
                AND bcpo51.type = :incoming_type
                AND bcpo51.recipientAccount = :gp_account

               UNION ALL

               SELECT SUM(mpo52.amount) as amount
                FROM \ModulePaymentOrder\Entity\PaymentOrder po52
                INNER JOIN \ModulePaymentOrder\Entity\MandarinPaymentOrder mpo52
                WITH mpo52.paymentOrder = po52
                WHERE po52 MEMBER OF p.paymentOrders
                AND mpo52.type = :incoming_type

               )
*/
                $qb->andWhere("
                    (p.deFactoDate is NULL AND d.dateOfConfirm is not NULL AND p.plannedDate < :today)
                    OR p.expectedValue
                    < ".self::SQL_TOTAL_INCOMING_AMOUNT_EXCLUDE_OVERPAID." 
                    OR 
                      (
                        p.plannedDate < (SELECT MAX(strtodate(bcpo8.documentDate,'%d.%m.%Y 23:59:59'))
                            FROM \ModulePaymentOrder\Entity\PaymentOrder po8
                            INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo8
                            WITH bcpo8.paymentOrder = po8
                            WHERE po8 MEMBER OF p.paymentOrders
                            AND bcpo8.type = :incoming_type
                            AND bcpo8.recipientAccount = :gp_account)

                        OR
                        p.plannedDate < (SELECT MAX(mpo63.created)
                            FROM \ModulePaymentOrder\Entity\PaymentOrder po63
                            INNER JOIN \ModulePaymentOrder\Entity\MandarinPaymentOrder mpo63
                            WITH mpo63.paymentOrder = po63
                            WHERE po63 MEMBER OF p.paymentOrders
                            AND mpo63.type = :incoming_type)
                      )

                    OR d.dateOfConfirm IS NOT NULL AND d.dealContractFile IS NULL
                    OR (SELECT COUNT(bcpo22.id)
                            FROM \ModulePaymentOrder\Entity\PaymentOrder po22
                            INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo22
                            WITH bcpo22.paymentOrder = po22
                            WHERE po22 MEMBER OF p.paymentOrders 
                            AND bcpo22.type = :incoming_type
                            AND bcpo22.bankClientPaymentOrderFile is null
                            ) > 0
                    "
                    )
                    ->setParameter('today', 'now()')
                    ->setParameter('incoming_type', PaymentOrderManager::TYPE_INCOMING)
                    ->setParameter('gp_account', $gp_account);
                ;
        }

        #sql_dump($qb->getQuery()->getResult());

        // Сортировка по названию сделки или дате создания
        if (array_key_exists('order', $params)) {
            if (array_key_exists('name', $params['order'])) {
                $qb->addOrderBy('d.name', $params['order']['name']);
            } elseif (array_key_exists('created', $params['order'])) {
                $qb->addOrderBy('d.created', $params['order']['created']);
            } elseif (array_key_exists('customer', $params['order'])) {
                $qb->addOrderBy('customer_user.login', $params['order']['customer']);
            } elseif (array_key_exists('contractor', $params['order'])) {
                $qb->addOrderBy('contractor_user.login', $params['order']['contractor']);
            }
        } else {
            $qb->addOrderBy('d.id', 'DESC');
        }

        $per_page = array_key_exists('per_page', $params) ? $params['per_page'] : self::PER_PAGE_DEFAULT;

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderFilterParamsForPaymentsWithExpiredDeals(QueryBuilder $qb, array $params)
    {
        foreach ($params as $item) {
            if(self::ALIASES_EXPIRED_DEALS[$item['property']] !== 'custom') {
                $where_string = self::ALIASES_EXPIRED_DEALS[$item['property']] . ' ' .
                    self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['operator'] . ' :' .
                    $item['property'];

                $parameter_key = $item['property'];
                $parameter_value = self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['pre_item'] .
                    $item['value'] .
                    self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['post_item'];

                // common cases
                $qb->andWhere($where_string)->setParameter($parameter_key, $parameter_value);
            }
        }
    }

    /**
     * @param $params
     * @param string $gp_account
     * @return array|Paginator
     */
    public function selectSortedPaymentsWithNearDeadlineDeals($params, string $gp_account)
    {
        //получаем предварительный запрос
        $prePayments = $this->selectPreQueryPaymentsOfDealsPaidByCustomer($gp_account);
        //фильтруем payment по оставшемуся сроку
        $fromHour = DealDeadlineManager::HOUR_FOR_DEADLINE_NOTIFY;
        $toHour = 0;
        $id_payments = $this->getFilteredPaymentsIdForDiffHourToCurrentDate($prePayments, $fromHour, $toHour);

        // general query
        $qb = $this->createQueryBuilder('p')
            ->addSelect('d')
            ->leftJoin('p.deal', 'd')
             ->addSelect('owner')
            ->leftJoin('d.owner', 'owner')
            ->addSelect('owner_user')
            ->leftJoin('owner.user', 'owner_user')
        ;
        $qb->andWhere('p.id IN (:id_payments)');
        $qb->setParameter('id_payments', $id_payments);

        // Добавляем к запросу параметры фильтрации
        if (array_key_exists('filter', $params)) {
            $this->addQueryBuilderFilterParamsForPaymentByDeadline($qb, $params['filter']);
        }

        // Добавляем к запросу параметры сортировки
        if (array_key_exists('order', $params)) {
            $this->addQueryBuilderSortParamsForPaymentByDeadline($qb, $params['order']);
        } else {
            $qb->addOrderBy('d.id', 'DESC');
        }

        if (array_key_exists('per_page', $params) && array_key_exists('page', $params)) {
            $paymentDeadlineCollection = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $paymentDeadlineCollection = $qb->getQuery()->getResult();
        }

        return $paymentDeadlineCollection;
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderSortParamsForPaymentByDeadline(QueryBuilder $qb, array $params)
    {
        foreach ($params as $key=>$value) {
            switch ($key) {
                case 'name':
                    $qb->addOrderBy('d.name', $value);
                    break;
                case 'amount':
                    $qb->addOrderBy('p.expectedValue', $value);
                    break;
                case 'created':
                    $qb->addOrderBy('d.created', $value);
                    break;
                case 'owner':
                    $qb->addOrderBy('owner_user.login', $value);
                    break;
            }
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderSortParamsForPaymentsWithExpiredDeals(QueryBuilder $qb, array $params)
    {
        foreach ($params as $key=>$value) {
            switch ($key) {
                case 'name':
                    $qb->addOrderBy('d.name', $value);
                    break;
                case 'amount':
                    $qb->addOrderBy('p.expectedValue', $value);
                    break;
                case 'created':
                    $qb->addOrderBy('d.created', $value);
                    break;
                case 'role':
                    $qb->addOrderBy('is_customer', $value);
                    break;
                case 'owner':
                    $qb->addOrderBy('owner_user.login', $value);
                    break;
            }
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderFilterParamsForPaymentByDeadline(QueryBuilder $qb, array $params)
    {
        foreach ($params as $item) {
            $where_string = self::ALIASES_FOR_DEAL_DEADLINE[$item['property']] . ' ' .
                            self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['operator'] . ' :' .
                            $item['property'];

            $parameter_key = $item['property'];
            $parameter_value = self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['pre_item'] .
                               $item['value'] .
                               self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['post_item'];

            // common cases
            $qb->andWhere($where_string)->setParameter($parameter_key,$parameter_value);
        }
    }

    /**
     * @param string $gp_account
     * @return array
     */
    private function selectPreQueryPaymentsOfDealsPaidByCustomer(string $gp_account)
    {
        $preDb = $this->createQueryBuilder('p')
            ->addSelect('d')
            ->leftJoin('p.deal', 'd')
            ->addSelect('ro')
            ->leftJoin('p.repaidOverpay', 'ro')
            ->addSelect('t')
            ->leftJoin('d.dealType', 't')
            ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('customer_user')
            ->leftJoin('customer.user', 'customer_user')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
            ->addSelect('contractor_user')
            ->leftJoin('contractor.user', 'contractor_user')
        ;
        //сделки которые не участвуют в споре
        $preDb->andWhere('(SELECT COUNT(dsp.id) FROM \Application\Entity\Dispute dsp WHERE dsp.deal = d.id AND dsp.closed = 0) = 0');
        // Сделка согласована
        $preDb->andWhere('customer.dealAgentConfirm = true') // Customer принял условия
        ->andWhere('contractor.dealAgentConfirm = true');   // Contractor принял условия

        // Еще нет никаких выплат
        // нет outgoing PaymentOrder
        $preDb->andWhere('(SELECT COUNT(po.id)
                            FROM \ModulePaymentOrder\Entity\PaymentOrder po
                            INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo
                            WITH bcpo.paymentOrder = po
                            WHERE po MEMBER OF p.paymentOrders AND bcpo.type = :outgoing_type
                            ) = 0');

        // нет возврата
        $preDb->andWhere('(SELECT COUNT(bcpo9.id)
                            FROM \Application\Entity\Dispute dis
                            INNER JOIN \Application\Entity\Refund ref
                            WITH ref.dispute = dis
                            INNER JOIN \ModulePaymentOrder\Entity\PaymentOrder po9
                            WITH po9 MEMBER OF ref.paymentOrders
                            INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo9
                            WITH bcpo9.paymentOrder = po9
                            WHERE dis.deal = d AND bcpo9.type = :outgoing_type
                            ) = 0')
        ;

        // Но уже есть incoming PaymentOrder
        $preDb->andWhere('p.expectedValue = ' .self::SQL_TOTAL_INCOMING_AMOUNT_EXCLUDE_OVERPAID);

        // Пришла оплата
        $preDb->andWhere('p.deFactoDate is not null');
        // сделка не проблемная
        $preDb->andWhere('d.dealContractFile is not null');
        $preDb->andWhere('d.dateOfConfirm is not null');
        // Исключаем проблемные сделки // Эта часть должна зеркально повторять запрос для выборки проблемных сделок
        $preDb->andWhere("p.plannedDate > (SELECT MAX(strtodate(bcpo4.documentDate,'%d.%m.%Y 23:59:59'))
                            FROM \ModulePaymentOrder\Entity\PaymentOrder po4
                            INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo4
                            WITH bcpo4.paymentOrder = po4
                            WHERE po4 MEMBER OF p.paymentOrders AND bcpo4.type = :incoming_type
                            )
                        ");

        $preDb->setParameter('incoming_type', PaymentOrderManager::TYPE_INCOMING);
        $preDb->setParameter('outgoing_type', PaymentOrderManager::TYPE_OUTGOING);
        $preDb->setParameter('gp_account', $gp_account);
        #sql_dump($preDb->getQuery()->getResult());

        return $preDb->getQuery()->getResult();
    }

    /**
     * @param $prePayments
     * @param $fromHour
     * @param $toHour
     * @return array
     */
    private function getFilteredPaymentsIdForDiffHourToCurrentDate($prePayments, $fromHour, $toHour = false)
    {
        $payments_id = [];
        $current_date = new \DateTime();
        /** @var array $prePayments */
        /** @var Payment $prePayment */
        foreach ($prePayments as $prePayment) {
            $dateClosedDeal = DealDeadlineManager::getDealDeadlineDate($prePayment);

            $diff = date_diff($current_date, $dateClosedDeal);
            $deadline_hour = hour_date_diff($diff);

            $is_to_hour = $toHour !== false ? $deadline_hour >= $toHour : true;
            if($deadline_hour <= $fromHour && $is_to_hour){
                $payments_id[] = $prePayment->getId();
            }
        }

        return $payments_id;
    }

    /**
     * @return array
     */
    public function getPaymentWithReadyToPayOffOutgoingPaymentOrders()
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('d')
            ->innerJoin('p.deal', 'd')
            ->addSelect('po')
            ->innerJoin('p.paymentOrders', 'po')
            ->addSelect('bcpo')
            ->innerJoin('po.bankClientPaymentOrder', 'bcpo')
            ->andWhere('bcpo.bankClientPaymentOrderFile is null')
            ->andWhere('bcpo.type = :outgoing')
            ->setParameter('outgoing', PaymentOrderManager::TYPE_OUTGOING)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $deal_id
     * @param $gp_account
     * @return array
     */
    public function selectPaymentsWithOverpaidDeals($deal_id = 0, $gp_account)
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('d')
            ->leftJoin('p.deal', 'd')
            ->addSelect('ro')
            ->leftJoin('p.repaidOverpay', 'ro')
            // Есть incoming PaymentOrder
            ->andWhere('(SELECT COUNT(bcpo2.id)
                            FROM \ModulePaymentOrder\Entity\PaymentOrder po2
                            INNER JOIN \ModulePaymentOrder\Entity\BankClientPaymentOrder bcpo2
                            WITH bcpo2.paymentOrder = po2
                            WHERE po2 MEMBER OF p.paymentOrders AND bcpo2.type = :incoming_type
                            ) > 0')
            // Сумма больше ожидаемой
            ->andWhere('p.expectedValue < '.self::SQL_TOTAL_INCOMING_AMOUNT_EXCLUDE_OVERPAID)
            ->setParameter('incoming_type', PaymentOrderManager::TYPE_INCOMING)
            ->setParameter('gp_account', $gp_account)
        ;

        if ($deal_id > 0){
            $qb->andWhere('d.id = :deal_id')->setParameter('deal_id', $deal_id);
        }

        return $qb->getQuery()->getResult();
    }
}