<?php

namespace Application\Entity\Repository;

use Application\Entity\User;

class PaymentMethodRepository extends BaseRepository
{
    const PER_PAGE_DEFAULT = 10;

    /**
     * Выбираем только isPattern = true
     *
     * @param User|null $user
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function getPatternPaymentMethods($user = null, $params = [])
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('t')
            ->leftJoin('p.paymentMethodType', 't')
            ->addSelect('bt')
            ->leftJoin('p.paymentMethodBankTransfer', 'bt');
            // ... сюда добавлять выборку других типов
        $qb
            ->addSelect('cls')
            ->leftJoin('p.civilLawSubject', 'cls')
            ->addSelect('u')
            ->leftJoin('cls.user', 'u');

        if ($user) {
            $qb->andWhere('u = :user')->setParameter('user', $user);;
        }

        $qb->andWhere('p.isPattern = true');

        // Если будет поиск по параметрам ('name', 'created' и т.д.)
        if (key_exists('search', $params)) {

        }

        // Сортировка по названию сделки или дате создания
        if (key_exists('order', $params)) {
            #$qb->addOrderBy('d.name', 'ASC');
        } else {
            $qb->addOrderBy('p.id', 'DESC');
        }

        $per_page = key_exists('per_page', $params) ? $params['per_page'] : self::PER_PAGE_DEFAULT;
        $page = key_exists('page', $params) ? $params['page'] : 1;

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param int $id
     * @return mixed|null
     */
    public function getPatternPaymentMethod(int $id)
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('t')
            ->leftJoin('p.paymentMethodType', 't')
            ->addSelect('cls')
            ->leftJoin('p.civilLawSubject', 'cls')
            ->addSelect('u')
            ->leftJoin('cls.user', 'u')
            ->addSelect('bt')
            ->leftJoin('p.paymentMethodBankTransfer', 'bt')
            // ... сюда добавлять выборку других типов
            ->andWhere('p.id = :id')
            ->andWhere('p.isPattern = true')
            ->andWhere('cls.isPattern = true')
            ->setParameter('id', $id)
        ;

        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (\Throwable $t){
            $result = null;
        }

        return $result;
    }
}