<?php
namespace Application\Entity\Repository;

use Application\Entity\Dispute;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class DiscountRepository
 * @package Application\Entity\Repository
 */
class DiscountRepository extends BaseRepository
{
    /**
     * @return array
     */
    public function getDiscountWithReadyToPayOffOutgoingPaymentOrders()
    {
        $qb = $this->createQueryBuilder('d')
            ->addSelect('po')
            ->innerJoin('d.paymentOrders', 'po')
            ->addSelect('bcpo')
            ->leftJoin('po.bankClientPaymentOrder', 'bcpo')
            ->andWhere('bcpo.bankClientPaymentOrderFile is null')
            ->andWhere('bcpo.type = :outgoing')
            ->setParameter('outgoing', PaymentOrderManager::TYPE_OUTGOING)
        ;

        return $qb->getQuery()->getResult();
    }
}