<?php

namespace Application\Entity\Repository;

use Application\Entity\LegalEntityTaxInspection;

class LegalEntityTaxInspectionRepository extends BaseRepository
{
    /**
     * @param int $id
     * @return mixed
     */
    public function getLegalEntityTaxInspectionBasedOnPatternById(int $id)
    {
        $qb = $this->createQueryBuilder('leti')
            ->addSelect('led')
            ->innerJoin('leti.legalEntityDocument', 'led')
            ->addSelect('le')
            ->innerJoin('led.legalEntity', 'le')
            ->addSelect('cls')
            ->innerJoin('le.civilLawSubject', 'cls')
            ->andWhere('leti.id = :id')
            ->andWhere('cls.isPattern = true')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (\Throwable $t){
            $result = null;
        }

        return $result;
    }

    /**
     * @param $user_id
     * @param array $params
     * @return array
     */
    public function getTaxInspectionBasedByUserId($user_id, $params = [])
    {
        return $this->selectTaxInspectionBased($user_id, $params);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getTaxInspectionBased($params = [])
    {
        return $this->selectTaxInspectionBased(null, $params);
    }

    /**
     * @param null $user_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    private function selectTaxInspectionBased($user_id = null, $params = [])
    {
        $qb = $this->createQueryBuilder('leti')
            ->addSelect('led')
            ->innerJoin('leti.legalEntityDocument', 'led')
            ->addSelect('le')
            ->innerJoin('led.legalEntity', 'le')
            ->addSelect('cls')
            ->innerJoin('le.civilLawSubject', 'cls')
            ->addSelect('u')
            ->innerJoin('cls.user', 'u')
            ->andWhere('cls.isPattern = true');
        if ($user_id) {
            $qb->andWhere('u.id = :user_id')
                ->setParameter('user_id', $user_id);
        }

        if (isset($params['order'])) {
            $order = $this->getOrder(LegalEntityTaxInspection::class, $params['order']);
            $qb->orderBy('leti.' . $order['field'], $order['sort']);
        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $taxInspection = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $taxInspection = $qb->getQuery()->getResult();
        }

        return $taxInspection;
    }
}