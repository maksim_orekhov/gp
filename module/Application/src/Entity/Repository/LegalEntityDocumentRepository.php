<?php

namespace Application\Entity\Repository;

use Application\Entity\LegalEntityDocument;

class LegalEntityDocumentRepository extends BaseRepository
{
    /**
     * @param int $id
     * @return mixed
     */
    public function getLegalEntityDocumentBasedOnPatternById(int $id)
    {
        $qb = $this->createQueryBuilder('led')
            ->addSelect('le')
            ->leftJoin('led.legalEntity', 'le')
            ->addSelect('cls')
            ->leftJoin('le.civilLawSubject', 'cls')
            ->andWhere('led.id = :id')
            ->andWhere('cls.isPattern = true')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (\Throwable $t){
            $result = null;
        }

        return $result;
    }

    /**
     * @param $legal_entity_id
     * @param array $params
     * @return array
     */
    public function getLegalEntitiesDocumentBasedByLegalEntityId($legal_entity_id, $params = [])
    {
        return $this->selectLegalEntityDocumentsBased(null, $legal_entity_id, $params);
    }

    /**
     * @param $user_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function getLegalEntityDocumentsBasedByUserId($user_id, $params = [])
    {
        return $this->selectLegalEntityDocumentsBased($user_id, null, $params);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getLegalEntityDocumentsBased($params = [])
    {
        return $this->selectLegalEntityDocumentsBased(null, null, $params);
    }

    /**
     * @param null $user_id
     * @param null $legal_entity_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    private function selectLegalEntityDocumentsBased($user_id = null, $legal_entity_id = null, $params = [])
    {
        $qb = $this->createQueryBuilder('led')
            ->addSelect('le')
            ->innerJoin('led.legalEntity', 'le')
            ->addSelect('cls')
            ->innerJoin('le.civilLawSubject', 'cls')
            ->addSelect('led')
            ->innerJoin('led.legalEntityDocumentType', 'ledt')
            ->addSelect('u')
            ->innerJoin('cls.user', 'u')
            ->andWhere('cls.isPattern = true');
        if ($user_id > 0) {
            $qb->andWhere('u.id = :user_id')
                ->setParameter('user_id', $user_id);
        }
        if ($legal_entity_id > 0) {
            $qb->andWhere('le.id = :legal_entity_id')
                ->setParameter('legal_entity_id', $legal_entity_id);
        }

        if (isset($params['order'])) {
            if (isset($params['order']['typeDocument'])) {
                $qb->orderBy('ledt.id', $params['order']['typeDocument']);
            } else {
                $order = $this->getOrder(LegalEntityDocument::class, $params['order']);
                $qb->orderBy('led.' . $order['field'], $order['sort']);
            }
        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $document = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $document = $qb->getQuery()->getResult();
        }


        return $document;
    }
}