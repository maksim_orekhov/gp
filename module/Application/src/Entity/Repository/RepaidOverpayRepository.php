<?php
namespace Application\Entity\Repository;

use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class RepaidOverpayRepository
 * @package Application\Entity\Repository
 */
class RepaidOverpayRepository extends BaseRepository
{
    /**
     * @return array
     */
    public function getRepaidOverpayWithReadyToPayOffOutgoingPaymentOrders()
    {
        $qb = $this->createQueryBuilder('ro')
            ->addSelect('po')
            ->innerJoin('ro.paymentOrders', 'po')
            ->addSelect('bcpo')
            ->leftJoin('po.bankClientPaymentOrder', 'bcpo')
            ->andWhere('bcpo.bankClientPaymentOrderFile is null')
            ->andWhere('bcpo.type = :outgoing')
            ->setParameter('outgoing', PaymentOrderManager::TYPE_OUTGOING)
        ;

        return $qb->getQuery()->getResult();
    }
}