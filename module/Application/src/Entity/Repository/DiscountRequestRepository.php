<?php
namespace Application\Entity\Repository;

use Application\Entity\Dispute;
use Application\Entity\User;

/**
 * Class DiscountRequestRepository
 * @package Application\Entity\Repository
 */
class DiscountRequestRepository extends BaseRepository
{
    public function getConfirmedDiscountRequestByDispute($dispute = null)
    {
        if($dispute && $dispute instanceof Dispute) {
            $qb = $this->createQueryBuilder('d')
                ->addSelect('c')
                ->innerJoin('d.confirm', 'c')
                ->andWhere('d.dispute = :dispute')
                ->andWhere('c.status = true')
                ->setParameter('dispute', $dispute);

            return $qb->getQuery()->getResult();
        }

        return null;
    }

    /**
     * @param User $user
     * @return array
     */
    public function findAllowedDiscountRequestsByUser(User $user)
    {
        $qb = $this->createQueryBuilder('dr')
            ->addSelect('ds')
            ->leftJoin('dr.dispute', 'ds')
            ->addSelect('d')
            ->leftJoin('ds.deal', 'd')
            ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
            ->andWhere('customer.user = :user OR contractor.user = :user')
            ->setParameter('user', $user)
        ;

        return $qb->getQuery()->getResult();
    }
}