<?php

namespace Application\Entity\Repository;

use Application\Entity\User;

/**
 * Class TokenRepository
 * @package Application\Entity\Repository
 */
class TokenRepository extends BaseRepository
{
    /**
     * @param User $user
     * @return mixed|null
     */
    public function selectNotDeletedUsersTokens(User $user)
    {
        $qb = $this->createQueryBuilder('t')
            ->addSelect('cu')
            ->leftJoin('t.counteragentUser', 'cu')
            ->addSelect('bu')
            ->leftJoin('t.beneficiarUser', 'bu')
            ->andWhere('t.isDeleted = 0 or t.isDeleted is null')
            ->andWhere('cu = :user OR bu = :user')
            ->setParameter('user', $user)
        ;

        return $qb->getQuery()->getResult();
    }
}