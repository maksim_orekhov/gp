<?php

namespace Application\Entity\Repository;

use Application\Entity\LegalEntityBankDetail;

class LegalEntityBankDetailRepository extends BaseRepository
{
    /**
     * @param int $id
     * @return mixed
     */
    public function getLegalEntityBankDetailBasedOnPatternById(int $id)
    {
        $qb = $this->createQueryBuilder('lebd')
            ->addSelect('led')
            ->innerJoin('lebd.legalEntityDocument', 'led')
            ->addSelect('le')
            ->innerJoin('led.legalEntity', 'le')
            ->addSelect('cls')
            ->innerJoin('le.civilLawSubject', 'cls')
            ->andWhere('lebd.id = :id')
            ->andWhere('cls.isPattern = true')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (\Throwable $t){
            $result = null;
        }

        return $result;
    }

    /**
     * @param $user_id
     * @param array $params
     * @return array
     */
    public function getBankDetailBasedByUserId($user_id, $params = [])
    {
        return $this->selectBankDetailBased($user_id, $params);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getBankDetailBased($params = [])
    {
        return $this->selectBankDetailBased(null, $params);
    }

    /**
     * @param null $user_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    private function selectBankDetailBased($user_id = null, $params = [])
    {
        $qb = $this->createQueryBuilder('lebd')
            ->addSelect('led')
            ->innerJoin('lebd.legalEntityDocument', 'led')
            ->addSelect('le')
            ->innerJoin('led.legalEntity', 'le')
            ->addSelect('cls')
            ->innerJoin('le.civilLawSubject', 'cls')
            ->addSelect('u')
            ->innerJoin('cls.user', 'u')
            ->andWhere('cls.isPattern = true');
        if ($user_id) {
            $qb->andWhere('u.id = :user_id')
                ->setParameter('user_id', $user_id);
        }

        if (isset($params['order'])) {
            $order = $this->getOrder(LegalEntityBankDetail::class, $params['order']);
            $qb->orderBy('lebd.' . $order['field'], $order['sort']);
        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $bankDetail = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $bankDetail = $qb->getQuery()->getResult();
        }

        return $bankDetail;
    }
}