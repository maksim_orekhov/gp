<?php
namespace Application\Entity\Repository;

use Application\Entity\Dispute;
use Application\Entity\User;

/**
 * Class TribunalRequestRepository
 * @package Application\Entity\Repository
 */
class TribunalRequestRepository extends BaseRepository
{
    public function getConfirmedTribunalRequestByDispute($dispute = null)
    {
        if($dispute && $dispute instanceof Dispute) {
            $qb = $this->createQueryBuilder('tr')
                ->andWhere('tr.dispute = :dispute')
                ->andWhere('tr.confirm is not null')
                ->setParameter('dispute', $dispute);

            return $qb->getQuery()->getResult();
        }

        return null;
    }

    /**
     * @param User $user
     * @return array
     */
    public function findAllowedTribunalRequestsByUser(User $user)
    {
        $qb = $this->createQueryBuilder('tr')
            ->addSelect('ds')
            ->leftJoin('tr.dispute', 'ds')
            ->addSelect('d')
            ->leftJoin('ds.deal', 'd')
            ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
            ->andWhere('customer.user = :user OR contractor.user = :user')
            ->setParameter('user', $user)
        ;

        return $qb->getQuery()->getResult();
    }
}