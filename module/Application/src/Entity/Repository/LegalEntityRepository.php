<?php

namespace Application\Entity\Repository;

use Application\Entity\LegalEntity;
use Application\Entity\LegalEntityDocument;

class LegalEntityRepository extends BaseRepository
{
    /**
     * Получение документов юр лица
     * @param LegalEntity $legal
     * @param string $document_type_name
     * @return array
     */
    public function getDocuments(LegalEntity $legal, $document_type_name = 'all')
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $qb = $queryBuilder->select('doc')
            ->from(LegalEntityDocument::class, 'doc')
            ->where('doc.legalEntity = :legal')
            ->setParameter('legal', $legal);

        if ($document_type_name !== 'all') {
            $qb->innerJoin('doc.legalEntityDocumentType', 'type')
                ->andWhere('type.name = :type_name')
                ->setParameter('type_name', $document_type_name);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getLegalEntityBasedOnPatternById(int $id)
    {
        $qb = $this->createQueryBuilder('le')
            ->addSelect('cls')
            ->leftJoin('le.civilLawSubject', 'cls')
            ->andWhere('le.id = :id')
            ->andWhere('cls.isPattern = true')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (\Throwable $t){
            $result = null;
        }
        return $result;
    }
}