<?php

namespace Application\Entity\Repository;


use Application\Entity\NaturalPersonPassport;

class NaturalPersonPassportRepository extends BaseRepository
{
    /**
     * @param int $id
     * @return mixed
     */
    public function getNaturalPersonPassportBasedOnPatternById(int $id)
    {
        $qb = $this->createQueryBuilder('npp')
            ->addSelect('npd')
            ->innerJoin('npp.naturalPersonDocument', 'npd')
            ->addSelect('np')
            ->innerJoin('npd.naturalPerson', 'np')
            ->addSelect('cls')
            ->innerJoin('np.civilLawSubject', 'cls')
            ->andWhere('npp.id = :id')
            ->andWhere('cls.isPattern = true')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (\Throwable $t){
            $result = null;
        }

        return $result;
    }

    /**
     * @param $user_id
     * @param array $params
     * @return array
     */
    public function getPassportBasedByUserId($user_id, $params = [])
    {
        return $this->selectPassportBased($user_id, $params);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getPassportBased($params = [])
    {
        return $this->selectPassportBased(null, $params);
    }

    /**
     * @param null $user_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    private function selectPassportBased($user_id = null, $params = [])
    {
        $qb = $this->createQueryBuilder('npp')
            ->addSelect('npd')
            ->innerJoin('npp.naturalPersonDocument', 'npd')
            ->addSelect('np')
            ->innerJoin('npd.naturalPerson', 'np')
            ->addSelect('cls')
            ->innerJoin('np.civilLawSubject', 'cls')
            ->addSelect('u')
            ->innerJoin('cls.user', 'u')
            ->andWhere('cls.isPattern = true');
        if ($user_id) {
            $qb->andWhere('u.id = :user_id')
                ->setParameter('user_id', $user_id);
        }

        if (isset($params['order'])) {
            $order = $this->getOrder(NaturalPersonPassport::class, $params['order']);
            $qb->orderBy('npp.' . $order['field'], $order['sort']);
        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $passport = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $passport = $qb->getQuery()->getResult();
        }

        return $passport;
    }
}