<?php

namespace Application\Entity\Repository;

use Application\Entity\NaturalPersonDocument;

class NaturalPersonDocumentRepository extends BaseRepository
{
    /**
     * @param int $id
     * @return mixed
     */
    public function getNaturalPersonDocumentBasedOnPatternById(int $id)
    {
        $qb = $this->createQueryBuilder('npd')
            ->addSelect('np')
            ->leftJoin('npd.naturalPerson', 'np')
            ->addSelect('cls')
            ->leftJoin('np.civilLawSubject', 'cls')
            ->andWhere('npd.id = :id')
            ->andWhere('cls.isPattern = true')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (\Throwable $t){
            $result = null;
        }

        return $result;
    }

    /**
     * @param $natural_person_id
     * @param array $params
     * @return array
     */
    public function getNaturalPersonsDocumentBasedByNaturalPersonId($natural_person_id, $params = [])
    {
        return $this->selectNaturalPersonsDocumentBased(null, $natural_person_id, $params);
    }

    /**
     * @param $user_id
     * @param array $params
     * @return array
     */
    public function getNaturalPersonsDocumentBasedByUserId($user_id, $params = [])
    {
        return $this->selectNaturalPersonsDocumentBased($user_id, null, $params);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getNaturalPersonsDocumentBased($params = [])
    {
        return $this->selectNaturalPersonsDocumentBased(null, null, $params);
    }

    /**
     * @param null $user_id
     * @param null $natural_person_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    private function selectNaturalPersonsDocumentBased($user_id = null, $natural_person_id = null, $params = [])
    {
        $qb = $this->createQueryBuilder('npd')
            ->addSelect('np')
            ->innerJoin('npd.naturalPerson', 'np')
            ->addSelect('cls')
            ->innerJoin('np.civilLawSubject', 'cls')
            ->addSelect('npdt')
            ->innerJoin('npd.naturalPersonDocumentType', 'npdt')
            ->addSelect('u')
            ->innerJoin('cls.user', 'u')
            ->andWhere('cls.isPattern = true');
        if ($user_id > 0) {
            $qb->andWhere('u.id = :user_id')
                ->setParameter('user_id', $user_id);
        }
        if ($natural_person_id > 0) {
            $qb->andWhere('np.id = :natural_person_id')
                ->setParameter('natural_person_id', $natural_person_id);
        }

        if (isset($params['order'])) {
            if (isset($params['order']['typeDocument'])) {
                $qb->orderBy('npdt.id', $params['order']['typeDocument']);
            } else {
                $order = $this->getOrder(NaturalPersonDocument::class, $params['order']);
                $qb->orderBy('npd.' . $order['field'], $order['sort']);
            }
        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $document = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $document = $qb->getQuery()->getResult();
        }


        return $document;
    }
}