<?php

namespace Application\Entity\Repository;

use Application\Entity\SoleProprietor;


/**
 * Class SoleProprietorRepository
 * @package Application\Entity\Repository
 */
class SoleProprietorRepository extends BaseRepository
{
    /**
     * @param int $id
     * @return mixed
     */
    public function getSoleProprietorBasedOnPatternById(int $id)
    {
        $qb = $this->createQueryBuilder('sp')
            ->addSelect('cls')
            ->leftJoin('sp.civilLawSubject', 'cls')
            ->andWhere('sp.id = :id')
            ->andWhere('cls.isPattern = true')
            ->setParameter('id', $id);

        try {

            $result = $qb->getQuery()->getSingleResult();
        }
        catch (\Throwable $t){

            $result = null;
        }

        return $result;
    }
}