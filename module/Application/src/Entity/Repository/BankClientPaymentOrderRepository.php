<?php
namespace Application\Entity\Repository;

/**
 * Class BankClientPaymentOrderRepository
 * @package Application\Entity\Repository
 */
class BankClientPaymentOrderRepository extends BaseRepository
{
    /**
     * Select PaymentOrders without relation to File
     *
     * @return array
     *
     * @TODO не используется!
     */
    public function getAllUnpaidBankClientPaymentOrders()
    {
        $qb = $this->createQueryBuilder('po')
            ->addSelect('p')
            ->leftJoin('po.creditPayment', 'p')
            #->andWhere('p.creditPaymentOrders is not NULL')
            ->andWhere('po.bankClientPaymentOrderFile is NULL');

        return $qb->getQuery()->getResult();
    }
}