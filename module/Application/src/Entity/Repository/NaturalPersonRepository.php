<?php

namespace Application\Entity\Repository;

use Application\Entity\NaturalPerson;
use Application\Entity\NaturalPersonDocument;

class NaturalPersonRepository extends BaseRepository
{
    /**
     * Получение документов физ лица
     * @param NaturalPerson $person
     * @param string $document_type_name
     * @return array
     */
    public function getDocuments(NaturalPerson $person, $document_type_name = 'all')
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $qb = $queryBuilder->select('doc')
            ->from(NaturalPersonDocument::class, 'doc')
            ->where('doc.naturalPerson = :person')
            ->setParameter('person', $person);

        if ($document_type_name !== 'all') {
            $qb->innerJoin('doc.naturalPersonDocumentType', 'type')
                ->andWhere('type.name = :type_name')
                ->setParameter('type_name', $document_type_name);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getNaturalPersonBasedOnPatternById(int $id)
    {
        $qb = $this->createQueryBuilder('np')
            ->addSelect('cls')
            ->leftJoin('np.civilLawSubject', 'cls')
            ->andWhere('np.id = :id')
            ->andWhere('cls.isPattern = true')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (\Throwable $t){
            $result = null;
        }

        return $result;
    }
}