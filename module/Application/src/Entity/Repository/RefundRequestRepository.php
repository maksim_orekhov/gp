<?php
namespace Application\Entity\Repository;

use Application\Entity\Dispute;
use Application\Entity\User;

/**
 * Class RefundRequestRepository
 * @package Application\Entity\Repository
 */
class RefundRequestRepository extends BaseRepository
{
    public function getConfirmedRefundRequestByDispute($dispute = null)
    {
        if($dispute && $dispute instanceof Dispute) {
            $qb = $this->createQueryBuilder('rr')
                ->addSelect('rc')
                ->innerJoin('rr.confirm', 'rc')
                ->andWhere('rr.dispute = :dispute')
                #->andWhere('rr.confirm is not null')
                ->andWhere('rc.status = true')
                ->setParameter('dispute', $dispute);

            return $qb->getQuery()->getResult();
        }

        return null;
    }

    /**
     * @param User $user
     * @return array
     */
    public function findAllowedRefundRequestsByUser(User $user)
    {
        $qb = $this->createQueryBuilder('rr')
            ->addSelect('ds')
            ->leftJoin('rr.dispute', 'ds')
            ->addSelect('d')
            ->leftJoin('ds.deal', 'd')
            ->addSelect('customer')
            ->leftJoin('d.customer', 'customer')
            ->addSelect('contractor')
            ->leftJoin('d.contractor', 'contractor')
            ->andWhere('customer.user = :user OR contractor.user = :user')
            ->setParameter('user', $user)
        ;

        return $qb->getQuery()->getResult();
    }
}