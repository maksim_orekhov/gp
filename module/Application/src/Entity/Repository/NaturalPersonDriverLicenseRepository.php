<?php

namespace Application\Entity\Repository;


use Application\Entity\NaturalPersonDriverLicense;

class NaturalPersonDriverLicenseRepository extends BaseRepository
{
    /**
     * @param int $id
     * @return mixed
     */
    public function getNaturalPersonDriverLicenseBasedOnPatternById(int $id)
    {
        $qb = $this->createQueryBuilder('npdl')
            ->addSelect('npd')
            ->innerJoin('npdl.naturalPersonDocument', 'npd')
            ->addSelect('np')
            ->innerJoin('npd.naturalPerson', 'np')
            ->addSelect('cls')
            ->innerJoin('np.civilLawSubject', 'cls')
            ->andWhere('npdl.id = :id')
            ->andWhere('cls.isPattern = true')
            ->setParameter('id', $id);

        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (\Throwable $t){
            $result = null;
        }

        return $result;
    }

    /**
     * @param $user_id
     * @param array $params
     * @return array
     */
    public function getDriverLicenseBasedByUserId($user_id, $params = [])
    {
        return $this->selectDriverLicenseBased($user_id, $params);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getDriverLicenseBased($params = [])
    {
        return $this->selectDriverLicenseBased(null, $params);
    }

    /**
     * @param null $user_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    private function selectDriverLicenseBased($user_id = null, $params = [])
    {
        $qb = $this->createQueryBuilder('npdl')
            ->addSelect('npd')
            ->innerJoin('npdl.naturalPersonDocument', 'npd')
            ->addSelect('np')
            ->innerJoin('npd.naturalPerson', 'np')
            ->addSelect('cls')
            ->innerJoin('np.civilLawSubject', 'cls')
            ->addSelect('u')
            ->innerJoin('cls.user', 'u')
            ->andWhere('cls.isPattern = true');
        if ($user_id) {
            $qb->andWhere('u.id = :user_id')
                ->setParameter('user_id', $user_id);
        }

        if (isset($params['order'])) {
            $order = $this->getOrder(NaturalPersonDriverLicense::class, $params['order']);
            $qb->orderBy('npdl.' . $order['field'], $order['sort']);
        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $driverLicense = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $driverLicense = $qb->getQuery()->getResult();
        }

        return $driverLicense;
    }
}