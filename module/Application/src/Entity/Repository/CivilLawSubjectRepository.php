<?php

namespace Application\Entity\Repository;

use Application\Entity\NaturalPerson;
use Application\Entity\LegalEntity;
use Application\Entity\SoleProprietor;

class CivilLawSubjectRepository extends BaseRepository
{
    /**
     * @param int $user_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function getNaturalPersonsByUserId(int $user_id, array $params = [])
    {
        return $this->getCivilLawSubjectByUserId(NaturalPerson::class, $user_id, $params);
    }

    /**
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function getNaturalPersons(array $params = [])
    {
        return $this->getCivilLawSubjectByUserId(NaturalPerson::class, null, $params);
    }

    /**
     * @param int $user_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function getLegalEntitiesByUserId(int $user_id, $params = [])
    {
        return $this->getCivilLawSubjectByUserId(LegalEntity::class, $user_id, $params);
    }

    /**
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function getLegalEntities(array $params = [])
    {
        return $this->getCivilLawSubjectByUserId(LegalEntity::class, null, $params);
    }

    /**
     * @param int $user_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function getSoleProprietorsByUserId(int $user_id, array $params = [])
    {
        return $this->getCivilLawSubjectByUserId(SoleProprietor::class, $user_id, $params);
    }

    /**
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     */
    public function getSoleProprietors(array $params = [])
    {
        return $this->getCivilLawSubjectByUserId(SoleProprietor::class, null, $params);
    }

    /**
     * Выбираем только isPattern = true
     *
     * @param $entity_class
     * @param null $user_id
     * @param array $params
     * @return array|\Zend\Paginator\Paginator
     *
     * @TODO Придумать другое имя методу?
     */
    private function getCivilLawSubjectByUserId($entity_class, $user_id = null, array $params = [])
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder
            ->select('p')
            ->from($entity_class, 'p')
            ->innerJoin('p.civilLawSubject', 'c')
            ->innerJoin('c.user', 'u')
            ->andWhere('c.isPattern = 1');
        if ($user_id) {
            $queryBuilder
                ->andWhere('u.id = :current_user_id')
                ->setParameter('current_user_id', $user_id);
        }

        if (isset($params['order'])) {
            $order = $this->getOrder($entity_class, $params['order']);
            $queryBuilder->orderBy('p.' . $order['field'], $order['sort']);
        }

        if (isset($params['per_page']) && isset($params['page'])) {
            $clsObject = $this->paginate($queryBuilder, $params['page'], $params['per_page']);
        } else {
            $clsObject = $queryBuilder->getQuery()->getResult();
        }

        return $clsObject;
    }

    /**
     * @param $search_data
     * @param bool $only_pattern
     * @param bool $full_search
     * @return array
     */
    public function searchCivilLawSubjects($search_data, $only_pattern = true, $full_search = false): array
    {
        if (\is_array($search_data)) {

            return [];
        }

        $search_data = trim($search_data);
        if ($search_data === '') {

            return [];
        }

        $search_data =  mb_strtolower($search_data, 'UTF-8');
        $qb = $this->createQueryBuilder('cls');
        $qb->setParameter('is_pattern', $only_pattern);

        switch ($search_data){
            case (int) $search_data > 0 && $full_search === false:
                // ишем по id
                $qb->andWhere('(cls.isPattern = true OR cls.isPattern = :is_pattern) AND cls.id = :search_data');
                $qb->setParameter('search_data', $search_data);
                $result = $qb->getQuery()->getResult();
                break;

            //OOO 2 раза т.к один выриант анг.
            case (bool) preg_match('/^ООО|^ЗАО|^ОАО|^OOO/iu', $search_data) && $full_search === false:
                // ишем по legal_entity
                $qb->addSelect('le')
                    ->leftJoin('cls.legalEntity', 'le');
                $qb->andWhere('(cls.isPattern = true OR cls.isPattern = :is_pattern) AND le.name LIKE :search_data');
                $qb->setParameter('search_data', '%'.$search_data.'%');
                $result = $qb->getQuery()->getResult();
                if (\count($result) === 0) {
                    $full_search = true;
                    return $this->searchCivilLawSubjects($search_data, $only_pattern, $full_search);
                }
                break;

            case (bool) preg_match('/^ИП/iu', $search_data) && $full_search === false:
                // ишем по sole_proprietor
                $qb->addSelect('sp')
                    ->leftJoin('cls.soleProprietor', 'sp');
                $qb->andWhere('(cls.isPattern = true OR cls.isPattern = :is_pattern) AND sp.name LIKE :search_data');
                $qb->setParameter('search_data', '%'.$search_data.'%');
                $result = $qb->getQuery()->getResult();
                if (\count($result) === 0) {
                    $full_search = true;
                    return $this->searchCivilLawSubjects($search_data, $only_pattern, $full_search);
                }
                break;

            default:

                $qb->addSelect('le')
                    ->leftJoin('cls.legalEntity', 'le')
                    ->addSelect('np')
                    ->leftJoin('cls.naturalPerson', 'np')
                    ->addSelect('sp')
                    ->leftJoin('cls.soleProprietor', 'sp');

                $qb->andWhere('(cls.isPattern = true OR cls.isPattern = :is_pattern) AND (
                            le.name LIKE :search_data OR
                            sp.name LIKE :search_data OR

                            CONCAT(np.lastName,\' \', np.firstName) LIKE :search_data OR
                            CONCAT(np.lastName,\' \', np.secondaryName) LIKE :search_data OR
                            CONCAT(np.lastName,\' \', np.firstName,\' \', np.secondaryName) LIKE :search_data OR
                            CONCAT(np.lastName,\' \', np.secondaryName,\' \', np.firstName) LIKE :search_data OR

                            CONCAT(np.firstName,\' \', np.lastName) LIKE :search_data OR
                            CONCAT(np.firstName,\' \', np.secondaryName) LIKE :search_data OR
                            CONCAT(np.firstName,\' \', np.lastName,\' \', np.secondaryName) LIKE :search_data OR
                            CONCAT(np.firstName,\' \', np.secondaryName,\' \', np.lastName) LIKE :search_data OR
                            
                            CONCAT(np.secondaryName,\' \', np.firstName) LIKE :search_data OR
                            CONCAT(np.secondaryName,\' \', np.lastName) LIKE :search_data OR
                            CONCAT(np.secondaryName,\' \', np.lastName,\' \', np.firstName) LIKE :search_data OR
                            CONCAT(np.secondaryName,\' \', np.firstName,\' \', np.lastName) LIKE :search_data
                        )');
                $qb->setParameter('search_data', '%'.$search_data.'%');
                $result = $qb->getQuery()->getResult();
        }

        return $result;
    }
}