<?php
namespace Application\Entity;

use Core\Entity\AbstractRole;
use ModuleRbac\Entity\Interfaces\ModuleRbacRoleInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a role.
 * @ORM\Entity()
 * @ORM\Table(name="role")
 */
class Role extends AbstractRole implements ModuleRbacRoleInterface
{}