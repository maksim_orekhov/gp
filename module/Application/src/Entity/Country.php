<?php
namespace Application\Entity;

use Core\Entity\AbstractCountry;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Country
 * @package Core\Entity
 *
 * @ORM\Table(name="country")
 * @ORM\Entity
 */
class Country extends AbstractCountry
{}

