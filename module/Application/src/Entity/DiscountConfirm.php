<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Class DiscountConfirm
 * @package Application\Entity
 *
 * @ORM\Table(name="discount_confirm")
 * @ORM\Entity();
 */
class DiscountConfirm
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DealAgent
     *
     * @ORM\ManyToOne(targetEntity="DealAgent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deal_agent_id", referencedColumnName="id")
     * })
     */
    private $author;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @ORM\OneToOne(targetEntity="DiscountRequest", mappedBy="confirm")
     */
    private $discountRequest;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true, unique=false)
     */
    private $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return DealAgent
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param DealAgent $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getDiscountRequest()
    {
        return $this->discountRequest;
    }

    /**
     * @param mixed $discountRequest
     */
    public function setDiscountRequest($discountRequest)
    {
        $this->discountRequest = $discountRequest;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }
}