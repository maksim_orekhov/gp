<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LegalEntityDocument
 *
 * @ORM\Table(name="legal_entity_document", indexes={@ORM\Index(name="fk_legal_entity_document_legal_entity1_idx", columns={"legal_entity_id"}), @ORM\Index(name="fk_legal_entity_document_legal_entity_document_type1_idx", columns={"legal_entity_document_type_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\LegalEntityDocumentRepository");
 */
class LegalEntityDocument
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Application\Entity\LegalEntity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\LegalEntity", inversedBy="legalEntityDocument")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="legal_entity_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $legalEntity;

    /**
     * @var \Application\Entity\LegalEntityDocumentType
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\LegalEntityDocumentType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="legal_entity_document_type_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $legalEntityDocumentType;

    /**
     * @var \Application\Entity\LegalEntityBankDetail
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\LegalEntityBankDetail", mappedBy="legalEntityDocument")
     */
    private $legalEntityBankDetail;

    /**
     * @var \Application\Entity\LegalEntityTaxInspection
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\LegalEntityTaxInspection", mappedBy="legalEntityDocument")
     */
    private $legalEntityTaxInspection;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, unique=false,  options={"default" : 0})
     */
    private $isActive = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * Set legalEntity
     *
     * @param \Application\Entity\LegalEntity $legalEntity
     *
     * @return LegalEntityDocument
     */
    public function setLegalEntity(\Application\Entity\LegalEntity $legalEntity = null)
    {
        $this->legalEntity = $legalEntity;

        return $this;
    }

    /**
     * Get legalEntity
     *
     * @return \Application\Entity\LegalEntity
     */
    public function getLegalEntity()
    {
        return $this->legalEntity;
    }

    /**
     * Set legalEntityDocumentType
     *
     * @param \Application\Entity\LegalEntityDocumentType $legalEntityDocumentType
     *
     * @return LegalEntityDocument
     */
    public function setLegalEntityDocumentType(\Application\Entity\LegalEntityDocumentType $legalEntityDocumentType = null)
    {
        $this->legalEntityDocumentType = $legalEntityDocumentType;

        return $this;
    }

    /**
     * Get legalEntityDocumentType
     *
     * @return \Application\Entity\legalEntityDocumentType
     */
    public function getLegalEntityDocumentType()
    {
        return $this->legalEntityDocumentType;
    }

    /**
     * Set legalEntityBankDetail
     *
     * @param \Application\Entity\LegalEntityBankDetail $legalEntityBankDetail
     *
     * @return LegalEntityDocument
     */
    public function setLegalEntityBankDetail(\Application\Entity\LegalEntityBankDetail $legalEntityBankDetail = null)
    {
        $this->legalEntityBankDetail = $legalEntityBankDetail;

        return $this;
    }

    /**
     * Get legalEntityBankDetail
     *
     * @return \Application\Entity\LegalEntityBankDetail|null
     */
    public function getLegalEntityBankDetail()
    {
        return $this->legalEntityBankDetail;
    }

    /**
     * Set legalEntityTaxInspection
     *
     * @param \Application\Entity\LegalEntityTaxInspection $legalEntityTaxInspection
     *
     * @return LegalEntityDocument
     */
    public function setLegalEntityTaxInspection(\Application\Entity\LegalEntityTaxInspection $legalEntityTaxInspection = null)
    {
        $this->legalEntityTaxInspection = $legalEntityTaxInspection;

        return $this;
    }

    /**
     * Get legalEntityTaxInspection
     *
     * @return \Application\Entity\LegalEntityTaxInspection|null
     */
    public function getLegalEntityTaxInspection()
    {
        return $this->legalEntityTaxInspection;
    }
}

