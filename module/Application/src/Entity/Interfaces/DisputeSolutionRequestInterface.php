<?php
namespace Application\Entity\Interfaces;

use Application\Entity\DealAgent;
use Application\Entity\Dispute;


/**
 * Interface DisputeSolutionRequestInterface
 * @package Application\Entity
 */
interface DisputeSolutionRequestInterface
{
    /**
     * @return \DateTime
     */
    public function getCreated();

    /**
     * @return Dispute
     */
    public function getDispute();

    /**
     * @return DealAgent
     */
    public function getAuthor();

    /**
     * @return mixed
     */
    public function getConfirm();
}