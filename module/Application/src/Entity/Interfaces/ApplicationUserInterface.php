<?php
namespace Application\Entity\Interfaces;

use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethod;
use Application\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleFileManager\Entity\File;
use ModuleCode\Entity\Code;

/**
 * Interface ApplicationUserInterface
 * @package Application\Entity\Interfaces
 */
interface ApplicationUserInterface
{
    /**
     * @return mixed
     */
    public function getCodes();
    /**
     * @param mixed $codes
     */
    public function setCodes($codes);
    /**
     * Add code
     *
     * @param Code $code
     *
     * @return User
     */
    public function addCode(Code $code);
    /**
     * Remove code
     *
     * @param Code $code
     */
    public function removeCode(Code $code);
    /**
     * @return bool
     */
    public function getIsBanned();
    /**
     * @param bool $isBanned
     */
    public function setIsBanned($isBanned);
    /**
     * @param CivilLawSubject $civilLawSubject
     * @return User
     */
    public function addCivilLawSubject(CivilLawSubject $civilLawSubject);
    /**
     * @param CivilLawSubject $civilLawSubject
     * @return User
     */
    public function removeCivilLawSubject(CivilLawSubject $civilLawSubject);
    /**
     * @return mixed
     */
    public function getCivilLawSubjects();
    /**
     * @param PaymentMethod $paymentMethod
     * @return User
     */
    public function addPaymentMethod(PaymentMethod $paymentMethod);
    /**
     * @param PaymentMethod $paymentMethod
     * @return User
     */
    public function removePaymentMethod(PaymentMethod $paymentMethod);
    /**
     * @return ArrayCollection
     */
    public function getPaymentMethods();
    /**
     * @return File
     */
    public function getAvatar();
    /**
     * @return bool
     */
    public function existAvatar();
    /**
     * @param File $file
     * @return $this
     */
    public function setAvatar(File $file);
    /**
     * @return $this
     */
    public function removeAvatar();
    /**
     * Get dealAgents
     *
     * @return mixed
     */
    public function getDealAgents();
    /**
     * Set dealAgents
     *
     * @param mixed $dealAgents
     */
    public function setDealAgents($dealAgents);
}