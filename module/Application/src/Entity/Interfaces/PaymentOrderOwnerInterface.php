<?php
namespace Application\Entity\Interfaces;

use Application\Entity\PaymentMethod;
use Doctrine\Common\Collections\ArrayCollection;
use ModulePaymentOrder\Entity\PaymentOrder;

/**
 * Interface PaymentOrderOwnerInterface
 * @package Application\Entity
 */
interface PaymentOrderOwnerInterface
{
    /**
     * @return ArrayCollection
     */
    public function getPaymentOrders();

    /**
     * @param mixed $paymentOrders
     */
    public function setPaymentOrders($paymentOrders);

    /**
     * @param PaymentOrder $paymentOrder
     * @return mixed
     */
    public function addPaymentOrder(PaymentOrder $paymentOrder);

    /**
     * @param PaymentOrder $paymentOrder
     * @return mixed
     */
    public function removePaymentOrder(PaymentOrder $paymentOrder);

    /**
     * @return mixed
     */
    public function getPaymentMethod();

    /**
     * @param PaymentMethod|null $paymentMethod
     */
    public function setPaymentMethod($paymentMethod);
}