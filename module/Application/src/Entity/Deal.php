<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Deal
 *
 * @ORM\Table(name="deal", indexes={@ORM\Index(name="fk_deal_deal_type_idx", columns={"deal_type_id"}), @ORM\Index(name="fk_deal_fee_payer_option1_idx", columns={"fee_payer_option_id"}), @ORM\Index(name="fk_deal_deal_customer_idx", columns={"customer_id"}), @ORM\Index(name="fk_deal_deal_contractor_idx", columns={"contractor_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\DealRepository");
 *
 * @property \Application\Entity\Deal status
 * @property \Application\Entity\Deal deadlineDate
 */
class Deal
{
    const CUSTOMER = 'customer';
    const CONTRACTOR = 'contractor';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=20, nullable=true, unique=true)
     *
     * Important! number зависит от id, поэтому nullable=true, чтобы сделку можно было добавить с number = null (id еще нет)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="addon_terms", type="text", length=16777215, precision=0, scale=0, nullable=true, unique=false)
     */
    private $addonTerms;

    /**
     * @var integer
     *
     * @ORM\Column(name="delivery_period", type="smallint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $deliveryPeriod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_of_confirm", type="datetime", nullable=true)
     */
    private $dateOfConfirm;

    /**
     * @var \Application\Entity\DealAgent
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\DealAgent", inversedBy="contractorDeals", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contractor_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $contractor;

    /**
     * @var \Application\Entity\DealAgent
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\DealAgent", inversedBy="customerDeals", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $customer;

    /**
     * @var \Application\Entity\DealType
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\DealType", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deal_type_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $dealType;

    /**
     * @var \Application\Entity\FeePayerOption
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\FeePayerOption", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fee_payer_option_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $feePayerOption;

    /**
     * One Deal has One Payment.
     * @ORM\OneToOne(targetEntity="Payment", mappedBy="deal")
     */
    private $payment;

    /**
     * One Deal has One DealContractFile.
     * @ORM\OneToOne(targetEntity="DealContractFile", inversedBy="deal", cascade={"persist"})
     * @ORM\JoinColumn(name="deal_contract_file_id", referencedColumnName="id")
     */
    private $dealContractFile;

    /**
     * One Deal has One Owner (DealAgent).
     * @ORM\OneToOne(targetEntity="DealAgent")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Delivery", mappedBy="deal")
     */
    private $deliveries;

    /**
     * Many Deals have Many DealFiles.
     * @ORM\ManyToMany(targetEntity="ModuleFileManager\Entity\File")
     * @ORM\JoinTable(name="deal_file",
     *      joinColumns={@ORM\JoinColumn(name="deal_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="file_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $dealFiles;

    /**
     * One Deal has One Dispute.
     * @ORM\OneToOne(targetEntity="Dispute", mappedBy="deal")
     */
    private $dispute;

    /**
     * Many Deal have Many Messages.
     * @ORM\ManyToMany(targetEntity="ModuleMessage\Entity\Message")
     * @ORM\JoinTable(name="deal_message",
     *      joinColumns={@ORM\JoinColumn(name="deal_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="message_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $messages;

    /**
     * Many Deals have One Token.
     * @ORM\ManyToOne(targetEntity="Token", inversedBy="deals")
     * @ORM\JoinColumn(name="deal_token_id", referencedColumnName="id")
     */
    private $dealToken;

    /**
     * One Deal has One DeliveryOrder.
     * @ORM\OneToOne(targetEntity="ModuleDelivery\Entity\DeliveryOrder", mappedBy="deal")
     */
    private $deliveryOrder;

    /**
     * Deal constructor.
     */
    public function __construct()
    {
        $this->deliveries   = new ArrayCollection;
        $this->dealFiles    = new ArrayCollection;
        $this->messages     = new ArrayCollection;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number)
    {
        $this->number = $number;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Deal
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set addonTerms
     *
     * @param string $addonTerms
     *
     * @return Deal
     */
    public function setAddonTerms($addonTerms)
    {
        $this->addonTerms = $addonTerms;

        return $this;
    }

    /**
     * Get addonTerms
     *
     * @return string
     */
    public function getAddonTerms()
    {
        return $this->addonTerms;
    }

    /**
     * Set deliveryPeriod
     *
     * @param integer $deliveryPeriod
     *
     * @return Deal
     */
    public function setDeliveryPeriod($deliveryPeriod)
    {
        $this->deliveryPeriod = $deliveryPeriod;

        return $this;
    }

    /**
     * Get deliveryPeriod
     *
     * @return integer
     */
    public function getDeliveryPeriod()
    {
        return $this->deliveryPeriod;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime|null $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateOfConfirm()
    {
        return $this->dateOfConfirm;
    }

    /**
     * @param \DateTime $dateOfConfirm
     */
    public function setDateOfConfirm(\DateTime $dateOfConfirm)
    {
        $this->dateOfConfirm = $dateOfConfirm;
    }

    /**
     * Set contractor
     *
     * @param \Application\Entity\DealAgent $contractor
     *
     * @return Deal
     */
    public function setContractor(\Application\Entity\DealAgent $contractor = null)
    {
        $this->contractor = $contractor;

        return $this;
    }

    /**
     * Get contractor
     *
     * @return \Application\Entity\DealAgent
     */
    public function getContractor()
    {
        return $this->contractor;
    }

    /**
     * Set customer
     *
     * @param \Application\Entity\DealAgent $customer
     *
     * @return Deal
     */
    public function setCustomer(\Application\Entity\DealAgent $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Application\Entity\DealAgent
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set dealType
     *
     * @param \Application\Entity\DealType $dealType
     *
     * @return Deal
     */
    public function setDealType(\Application\Entity\DealType $dealType = null)
    {
        $this->dealType = $dealType;

        return $this;
    }

    /**
     * Get dealType
     *
     * @return \Application\Entity\DealType
     */
    public function getDealType()
    {
        return $this->dealType;
    }

    /**
     * Set feePayerOption
     *
     * @param \Application\Entity\FeePayerOption $feePayerOption
     *
     * @return Deal
     */
    public function setFeePayerOption(\Application\Entity\FeePayerOption $feePayerOption = null)
    {
        $this->feePayerOption = $feePayerOption;

        return $this;
    }

    /**
     * Get feePayerOption
     *
     * @return \Application\Entity\FeePayerOption
     */
    public function getFeePayerOption()
    {
        return $this->feePayerOption;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return mixed
     */
    public function getDealContractFile()
    {
        return $this->dealContractFile;
    }

    /**
     * @param mixed $dealContractFile
     */
    public function setDealContractFile($dealContractFile)
    {
        $this->dealContractFile = $dealContractFile;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return Collection
     */
    public function getDeliveries(): Collection
    {
        return $this->deliveries;
    }

    /**
     * @param Collection $deliveries
     */
    public function setDeliveries(Collection $deliveries)
    {
        $this->deliveries = $deliveries;
    }

    /**
     * @param Delivery $delivery
     */
    public function addDelivery(Delivery $delivery)
    {
        $this->deliveries->add($delivery);
    }

    /**
     * @return mixed
     */
    public function getDealFiles()
    {
        return $this->dealFiles;
    }

    /**
     * @param mixed $dealFiles
     */
    public function setDealFiles($dealFiles)
    {
        $this->dealFiles = $dealFiles;
    }

    /**
     * @param $dealFile
     */
    public function addDealFile($dealFile)
    {
        $this->dealFiles->add($dealFile);
    }

    /**
     * @return mixed
     */
    public function getDispute()
    {
        return $this->dispute;
    }

    /**
     * @param mixed $dispute
     */
    public function setDispute($dispute)
    {
        $this->dispute = $dispute;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @param $message
     */
    public function addMessage($message)
    {
        $this->messages->add($message);
    }

    /**
     * @param $message
     * @return $this
     */
    public function removeMessage($message)
    {
        $this->messages->removeElement($message);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDealToken()
    {
        return $this->dealToken;
    }

    /**
     * @param mixed $dealToken
     */
    public function setDealToken($dealToken)
    {
        $this->dealToken = $dealToken;
    }

    /**
     * @return mixed
     */
    public function getDeliveryOrder()
    {
        return $this->deliveryOrder;
    }

    /**
     * @param mixed $deliveryOrder
     */
    public function setDeliveryOrder($deliveryOrder)
    {
        $this->deliveryOrder = $deliveryOrder;
    }
}

