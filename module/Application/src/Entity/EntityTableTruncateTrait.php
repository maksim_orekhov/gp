<?php
namespace Application\Entity;

use Doctrine\ORM\EntityManager;

/**
 * Trait EntityTableTruncateTrait
 * @package Application\Entity
 */
trait EntityTableTruncateTrait
{
    /**
     * @param EntityManager $entityManager
     * @param string $table_name
     * @throws \Doctrine\DBAL\DBALException
     */
    public function truncate(EntityManager $entityManager, string $table_name)
    {
        $connection = $entityManager->getConnection();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $connection->query('TRUNCATE TABLE '.$table_name);
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
    }
}