<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LegalEntityDocumentType
 *
 * @ORM\Table(name="legal_entity_document_type")
 * @ORM\Entity
 */
class LegalEntityDocumentType
{
    const BANK_DETAILS   = 'bank_details';
    const TAX_INSPECTION = 'tax_inspection';

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::BANK_DETAILS,
            self::TAX_INSPECTION,
        ];
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $name;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LegalEntityDocumentType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

