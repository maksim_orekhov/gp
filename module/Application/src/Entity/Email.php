<?php
namespace Application\Entity;

use Core\Entity\AbstractEmail;
use Doctrine\ORM\Mapping as ORM;

/**
 * Email
 * @package Core\Entity
 *
 * @ORM\Table(name="email")
 * @ORM\Entity
 */
class Email extends AbstractEmail
{}

