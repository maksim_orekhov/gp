<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BankClientPaymentOrderFile
 * @package Application\Entity
 * @ORM\Table(name="bank_client_payment_order_file")
 * @ORM\Entity
 */
class BankClientPaymentOrderFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", nullable=false)
     */
    private $fileName;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", nullable=false)
     */
    private $hash;

    /**
     * One BankClientPaymentOrderFile has One File
     * @ORM\OneToOne(targetEntity="ModuleFileManager\Entity\File")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     */
    private $file;

    /**
     * One BankClientPaymentOrderFile has Many BankClientPaymentOrder
     * @ORM\OneToMany(targetEntity="ModulePaymentOrder\Entity\BankClientPaymentOrder", mappedBy="bankClientPaymentOrderFile")
     */
    private $bankClientPaymentOrders;


    public function __construct()
    {
        $this->bankClientPaymentOrders = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getBankClientPaymentOrders()
    {
        return $this->bankClientPaymentOrders;
    }

    /**
     * @param mixed $bankClientPaymentOrders
     */
    public function setBankClientPaymentOrders($bankClientPaymentOrders)
    {
        $this->bankClientPaymentOrders = $bankClientPaymentOrders;
    }
}