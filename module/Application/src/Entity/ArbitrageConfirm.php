<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ArbitrageConfirm
 * @package Application\Entity
 *
 * @ORM\Table(name="arbitrage_confirm")
 * @ORM\Entity;
 */
class ArbitrageConfirm
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @ORM\OneToOne(targetEntity="ArbitrageRequest", mappedBy="confirm")
     */
    private $arbitrageRequest;

    /**
     * @var \Application\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $author;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true, unique=false)
     */
    private $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getArbitrageRequest()
    {
        return $this->arbitrageRequest;
    }

    /**
     * @param mixed $arbitrageRequest
     */
    public function setArbitrageRequest($arbitrageRequest)
    {
        $this->arbitrageRequest = $arbitrageRequest;
    }

    /**
     * @return User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author)
    {
        $this->author = $author;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }
}