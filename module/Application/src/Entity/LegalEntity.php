<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * LegalEntity
 *
 * @ORM\Table(name="legal_entity", indexes={@ORM\Index(name="fk_legal_entity_civil_law_subject1_idx", columns={"civil_law_subject_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\LegalEntityRepository");
 */
class LegalEntity implements CivilLawSubjectChildInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="legal_address", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $legalAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20, precision=0, scale=0, nullable=true, unique=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string", length=12, precision=0, scale=0, nullable=false, unique=false)
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(name="kpp", type="string", length=9, precision=0, scale=0, nullable=false, unique=false)
     */
    private $kpp;

    /**
     * @var string
     *
     * @ORM\Column(name="ogrn", type="string", length=15, precision=0, scale=0, nullable=true, unique=false)
     */
    private $ogrn;

    /**
     * @var \Application\Entity\CivilLawSubject
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\CivilLawSubject", inversedBy="legalEntity")
     * @ORM\JoinColumn(name="civil_law_subject_id", referencedColumnName="id", nullable=true)
     */
    private $civilLawSubject;

    /**
     * @var \Application\Entity\LegalEntityDocument
     *
     * @ORM\OneToMany(targetEntity="Application\Entity\LegalEntityDocument", mappedBy="legalEntity")
     */
    private $legalEntityDocument;


    public function __construct()
    {
        $this->legalEntityDocument = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LegalEntity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set legalAddress
     *
     * @param string $legalAddress
     *
     * @return LegalEntity
     */
    public function setLegalAddress($legalAddress)
    {
        $this->legalAddress = $legalAddress;

        return $this;
    }

    /**
     * Get legalAddress
     *
     * @return string
     */
    public function getLegalAddress()
    {
        return $this->legalAddress;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return LegalEntity
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set inn
     *
     * @param string $inn
     *
     * @return LegalEntity
     */
    public function setInn($inn)
    {
        $this->inn = $inn;

        return $this;
    }

    /**
     * Get inn
     *
     * @return string
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * Set kpp
     *
     * @param string $kpp
     *
     * @return LegalEntity
     */
    public function setKpp($kpp)
    {
        $this->kpp = $kpp;

        return $this;
    }

    /**
     * Get kpp
     *
     * @return string
     */
    public function getKpp()
    {
        return $this->kpp;
    }

    /**
     * Set ogrn
     *
     * @param string $ogrn
     *
     * @return LegalEntity
     */
    public function setOgrn($ogrn)
    {
        $this->ogrn = $ogrn;

        return $this;
    }

    /**
     * Get ogrn
     *
     * @return string
     */
    public function getOgrn()
    {
        return $this->ogrn;
    }

    /**
     * Set civilLawSubject
     *
     * @param CivilLawSubject $civilLawSubject
     *
     * @return LegalEntity
     */
    public function setCivilLawSubject(CivilLawSubject $civilLawSubject = null)
    {
        $this->civilLawSubject = $civilLawSubject;

        return $this;
    }

    /**
     * Get civilLawSubject
     *
     * @return CivilLawSubject
     */
    public function getCivilLawSubject()
    {
        return $this->civilLawSubject;
    }

    /**
     * @param \Application\Entity\LegalEntityDocument $legalEntityDocument
     * @return LegalEntity
     */
    public function addLegalEntityDocument(\Application\Entity\LegalEntityDocument $legalEntityDocument)
    {
        $this->legalEntityDocument[] = $legalEntityDocument;

        return $this;
    }

    /**
     * @param \Application\Entity\LegalEntityDocument $legalEntityDocument
     * @return LegalEntity
     */
    public function removeLegalEntityDocument(\Application\Entity\LegalEntityDocument $legalEntityDocument)
    {
        $this->legalEntityDocument->removeElement($legalEntityDocument);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLegalEntityDocument()
    {
        return $this->legalEntityDocument;
    }
}

