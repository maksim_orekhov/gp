<?php
namespace Application\Entity;

use Application\Entity\Interfaces\DisputeSolutionRequestInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class TribunalRequest
 * @package Application\Entity
 *
 * @ORM\Table(name="tribunal_request")
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\TribunalRequestRepository");
 */
class TribunalRequest implements DisputeSolutionRequestInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var Dispute
     *
     * @ORM\ManyToOne(targetEntity="Dispute", inversedBy="tribunalRequests")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dispute_id", referencedColumnName="id")
     * })
     */
    private $dispute;

    /**
     * @var DisputeCycle
     *
     * @ORM\ManyToOne(targetEntity="DisputeCycle", inversedBy="tribunalRequests")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dispute_cycle_id", referencedColumnName="id")
     * })
     */
    private $disputeCycle;

    /**
     * @var DealAgent
     *
     * @ORM\ManyToOne(targetEntity="DealAgent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deal_agent_id", referencedColumnName="id")
     * })
     */
    private $author;

    /**
     * One TribunalRequest has One TribunalConfirm. Bidirectional.
     * @ORM\OneToOne(targetEntity="TribunalConfirm", inversedBy="tribunalRequest")
     * @ORM\JoinColumn(name="confirm_id", referencedColumnName="id", nullable=true)
     */
    private $confirm;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return Dispute
     */
    public function getDispute(): Dispute
    {
        return $this->dispute;
    }

    /**
     * @param Dispute $dispute
     */
    public function setDispute(Dispute $dispute)
    {
        $this->dispute = $dispute;
    }

    /**
     * @return DisputeCycle
     */
    public function getDisputeCycle(): DisputeCycle
    {
        return $this->disputeCycle;
    }

    /**
     * @param DisputeCycle $disputeCycle
     */
    public function setDisputeCycle(DisputeCycle $disputeCycle)
    {
        $this->disputeCycle = $disputeCycle;
    }

    /**
     * @return DealAgent
     */
    public function getAuthor(): DealAgent
    {
        return $this->author;
    }

    /**
     * @param DealAgent $author
     */
    public function setAuthor(DealAgent $author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getConfirm()
    {
        return $this->confirm;
    }

    /**
     * @param TribunalConfirm $confirm
     */
    public function setConfirm(TribunalConfirm $confirm)
    {
        $this->confirm = $confirm;
    }
}