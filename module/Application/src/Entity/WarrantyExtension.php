<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Dispute;

/**
 * Class Dispute
 * @package Application\Entity
 *
 * @ORM\Table(name="warranty_extension")
 * @ORM\Entity();
 */
class WarrantyExtension
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="days", type="smallint", nullable=false, unique=false)
     */
    private $days;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var Dispute
     *
     * @ORM\ManyToOne(targetEntity="Dispute", inversedBy="warrantyExtensions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dispute_id", referencedColumnName="id")
     * })
     */
    private $dispute;

    /**
     * @var DisputeCycle
     *
     * @ORM\ManyToOne(targetEntity="DisputeCycle", inversedBy="warrantyExtensions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dispute_cycle_id", referencedColumnName="id")
     * })
     */
    private $disputeCycle;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getDays(): int
    {
        return $this->days;
    }

    /**
     * @param int $days
     */
    public function setDays(int $days)
    {
        $this->days = $days;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \Application\Entity\Dispute
     */
    public function getDispute(): \Application\Entity\Dispute
    {
        return $this->dispute;
    }

    /**
     * @param \Application\Entity\Dispute $dispute
     */
    public function setDispute(\Application\Entity\Dispute $dispute)
    {
        $this->dispute = $dispute;
    }

    /**
     * @return DisputeCycle
     */
    public function getDisputeCycle(): DisputeCycle
    {
        return $this->disputeCycle;
    }

    /**
     * @param DisputeCycle $disputeCycle
     */
    public function setDisputeCycle(DisputeCycle $disputeCycle)
    {
        $this->disputeCycle = $disputeCycle;
    }
}