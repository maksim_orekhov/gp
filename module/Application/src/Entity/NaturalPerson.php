<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * NaturalPerson
 *
 * @ORM\Table(name="natural_person", indexes={@ORM\Index(name="fk_natural_person_civil_law_subject1_idx", columns={"civil_law_subject_id"})})
 * @ORM\Entity(repositoryClass="Application\Entity\Repository\NaturalPersonRepository");
 */
class NaturalPerson implements CivilLawSubjectChildInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="secondary_name", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $secondaryName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=45, precision=0, scale=0, nullable=true, unique=false)
     */
    private $lastName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     */
    private $birthDate;

    /**
     * @var \Application\Entity\CivilLawSubject
     *
     * @ORM\OneToOne(targetEntity="Application\Entity\CivilLawSubject", inversedBy="naturalPerson")
     * @ORM\JoinColumn(name="civil_law_subject_id", referencedColumnName="id", nullable=true)
     */
    private $civilLawSubject;

    /**
     * @var \Application\Entity\NaturalPersonDocument
     *
     * @ORM\OneToMany(targetEntity="Application\Entity\NaturalPersonDocument", mappedBy="naturalPerson")
     */
    private $naturalPersonDocument;


    public function __construct()
    {
        $this->naturalPersonDocument = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return NaturalPerson
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set secondaryName
     *
     * @param string $secondaryName
     *
     * @return NaturalPerson
     */
    public function setSecondaryName($secondaryName)
    {
        $this->secondaryName = $secondaryName;

        return $this;
    }

    /**
     * Get secondaryName
     *
     * @return string
     */
    public function getSecondaryName()
    {
        return $this->secondaryName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return NaturalPerson
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return NaturalPerson
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set civilLawSubject
     *
     * @param CivilLawSubject $civilLawSubject
     *
     * @return NaturalPerson
     */
    public function setCivilLawSubject(CivilLawSubject $civilLawSubject = null)
    {
        $this->civilLawSubject = $civilLawSubject;

        return $this;
    }

    /**
     * Get civilLawSubject
     *
     * @return CivilLawSubject
     */
    public function getCivilLawSubject()
    {
        return $this->civilLawSubject;
    }

    /**
     * @param NaturalPersonDocument $naturalPersonDocument
     * @return NaturalPerson
     */
    public function addNaturalPersonDocument(NaturalPersonDocument $naturalPersonDocument)
    {
        $this->naturalPersonDocument[] = $naturalPersonDocument;

        return $this;
    }

    /**
     * @param NaturalPersonDocument $naturalPersonDocument
     * @return NaturalPerson
     */
    public function removeNaturalPersonDocument(NaturalPersonDocument $naturalPersonDocument)
    {
        $this->naturalPersonDocument->removeElement($naturalPersonDocument);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getNaturalPersonDocument()
    {
        return $this->naturalPersonDocument;
    }
}

