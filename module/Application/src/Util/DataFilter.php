<?php
namespace Application\Util;

use Application\Form\FilterForm;
use PHPUnit\Runner\Exception;
use Zend\Form\Element\DateTime;

/**
 * Class DataFilter
 * @package Application\Service
 */
class DataFilter
{
    public function filterData(FilterForm $form, array $data)
    {
        $filtered_data = [];

        $makeData = $this->makeFormData($data);

        // Заполняем форму данными.
        $form->setData($makeData);

        if ($form->isValid()) {

            $formData = $form->getData();
            $filter_rules = $form->getFilterRules();

            foreach ($formData as $key=>$item) {

                $key = $this->makeKey($key);

                if (isset($filter_rules[$key]) && $item != null) {
                    // Если есть параметры дат, то преобразуем строку в объект DateTime
                    if ($filter_rules[$key] === 'date_greater' || $filter_rules[$key] === 'date_smaller') {
                        $item = $this->stringToDateTime($item, $filter_rules[$key]);
                    }

                    $filtered_data[] = [
                        'property' => $key,
                        'value' => $item,
                        'rule' => $filter_rules[$key]
                    ];
                }
            }

            return ['filter' => $filtered_data, 'form' => $form, 'status' => 'SUCCESS'];

        } else {

            return ['filter' => null, 'form' => $form, 'status' => 'ERROR'];
        }
    }

    /**
     * @param string $str_date
     * @param $rule
     * @return string
     */
    private function stringToDateTime(string $str_date, $rule)
    {
        $str_date .= ($rule === 'date_greater') ? ' 00:00:00' : ' 23:59:59';

        $date = \DateTime::createFromFormat('d.m.Y H:i:s', $str_date);

        return $date->format('Y-m-d H:i:s');
    }

    /**
     * @param array $data
     * @return array
     */
    private function makeFormData(array $data)
    {
        $formData = [];
        foreach ($data as $key=>$item) {
            if ($key !== 'csrf') {
                $formData['filter['.$key.']'] = $item;
            } else {
                $formData['csrf'] = $item;
            }
        }

        return $formData;
    }

    /**
     * @param string $key
     * @return string
     */
    private function makeKey(string $key)
    {
        preg_match('/\[(.*)\]/', $key, $match);

        if (!empty($match)) {
            $key = $match[1];
        }

        return $key;
    }
}