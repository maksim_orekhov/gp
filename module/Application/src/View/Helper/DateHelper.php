<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class DateHelper
 * @package Application\View\Helper
 */
class DateHelper extends AbstractHelper
{
    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @param $date
     * @param string $pattern
     * @return false|string
     */
    public function formatToString($date, $pattern = 'd.m.Y')
    {
        return date($pattern, strtotime($date));
    }

    
    /**
     * @param $data
     * @param string $sort_key
     * @return mixed
     */
    public function sortArrayByDate($data, $sort_key = 'created')
    {
        usort($data, function ($a, $b) use ($sort_key) {
            if (array_key_exists($sort_key, $a) && array_key_exists($sort_key, $b)) {
                return strtotime($b[$sort_key]) - strtotime($a[$sort_key]);
            }
        });

        return $data;
    }
}