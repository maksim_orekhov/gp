<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Application\Entity\SystemPaymentDetails;

/**
 * Class NumberToStringHelper
 * @package Application\View\Helper
 */
class InvoiceHelper extends AbstractHelper
{

    public function __invoke()
    {
        return $this;
    }

    /**
     * Возвращает сумму прописью
     *
     * @param $number
     * @param bool $uppercaseFirstSymbol
     * @return string
     */
    public function numberToString($number, $uppercaseFirstSymbol = false)
    {
        $nul='ноль';
        $ten=array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
        );
        $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
        $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
        $unit=array( // Units
            array('копейка' ,'копейки' ,'копеек',	 1),
            array('рубль'   ,'рубля'   ,'рублей'    ,0),
            array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
            array('миллион' ,'миллиона','миллионов' ,0),
            array('миллиард','милиарда','миллиардов',0),
        );

        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($number)));

        $out = array();

        if ((int)$rub > 0) {

            foreach(str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!(int)$v) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= $this->morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            }

        } else {
            $out[] = $nul;
        }

        $out[] = $this->morph((int)$rub, $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        $out[] = $kop.' '.$this->morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop

        $result = trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
        if($uppercaseFirstSymbol && !empty($result)){
            $enc = 'utf-8';
            $firstSymbol = mb_strtoupper(mb_substr($result, 0, 1, $enc), $enc);
            $othersSymbol = mb_substr($result, 1, mb_strlen($result, $enc), $enc);
            $result = $firstSymbol.$othersSymbol;
        }
        return $result;
    }

    /**
     * Склоняем словоформу
     *
     * @param $n
     * @param $f1
     * @param $f2
     * @param $f5
     * @return mixed
     */
    private function morph($n, $f1, $f2, $f5)
    {
        $n = abs((int)$n) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n === 1) return $f1;

        return $f5;
    }

    public function numberWithCurrencyUnit($number)
    {
        $unit='';
        if(!empty($number)){
            $unit = $this->morph((int)$number, 'рубль', 'рубля', 'рублей');
        }
        return $number.' '.$unit;
    }

    /**
     * @param array $deal
     * @return string
     */
    public function fullInformationAboutPayerFromArray(array $deal)
    {
        $payer = $this->formatPayerForInvoiceFromArray($deal);

        $inn = null;
        $kpp = null;
        $address = null;

        if ($deal['customer']['is_legal_entity']) {
            $inn = $deal['customer']['inn'];
            $kpp = $deal['customer']['kpp'];
            $address = $deal['customer']['address'];
        }

        if ($deal['customer']['is_sole_proprietor']) {
            $inn = $deal['customer']['inn'];
        }

        $fullPayer = ($payer) ? $payer: '';
        $fullPayer = ($inn) ? $fullPayer.', ИНН '.$inn: $fullPayer;
        $fullPayer = ($kpp) ? $fullPayer.', КПП '.$kpp: $fullPayer;
        $fullPayer = ($address) ? $fullPayer.', '.$address: $fullPayer;

        return $fullPayer;
    }

    /**
     * @param array $deal
     * @return string
     */
    public function formatPayerForInvoiceFromArray(array $deal)
    {
        if ($deal['customer']['is_natural_person']) {
            $last_name = $deal['customer']['last_name'];
            $first_name = $deal['customer']['first_name'];
            $secondary_name = $deal['customer']['secondary_name'];
            $payer = $last_name. ' ' . $first_name . ' ' . $secondary_name;
        } else {
            $payer = $deal['customer']['name'];
        }

        return $payer;
    }

    /**
     * @param SystemPaymentDetails $systemPaymentDetails
     * @return string
     */
    public function fullInformationSystemPaymentDetail(SystemPaymentDetails $systemPaymentDetails)
    {
        $recipientName = $systemPaymentDetails->getPaymentRecipientName();
        $inn = $systemPaymentDetails->getInn();
        $kpp = $systemPaymentDetails->getKpp();

        $fullPaymentDetails = ($recipientName) ? $recipientName: '';
        $fullPaymentDetails = ($inn) ? $fullPaymentDetails.', ИНН '.$inn: $fullPaymentDetails;
        $fullPaymentDetails = ($kpp) ? $fullPaymentDetails.', КПП '.$kpp: $fullPaymentDetails;

        return $fullPaymentDetails;
    }

    /**
     * @param array $system_payment_details
     * @return mixed|string
     */
    public function fullInformationSystemPaymentDetailFromArray(array $system_payment_details)
    {
        $recipient_name = $system_payment_details['payment_recipient_name'];
        $inn = $system_payment_details['inn'];
        $kpp = $system_payment_details['kpp'];

        $full_payment_details = ($recipient_name) ? $recipient_name: '';
        $full_payment_details = ($inn) ? $full_payment_details.', ИНН '.$inn: $full_payment_details;
        $full_payment_details = ($kpp) ? $full_payment_details.', КПП '.$kpp: $full_payment_details;

        return $full_payment_details;
    }
}