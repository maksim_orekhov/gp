<?php
namespace Application\View\Helper;

use Application\Entity\User;
use Application\Service\Deal\DealManager;
use Application\Service\UserManager;
use Zend\View\Helper\AbstractHelper;

/**
 * Class DealHelper
 * @package Application\View\Helper
 */
class DealHelper extends AbstractHelper
{
    /**
     * @var User
     */
    private $dealManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * DealHelper constructor.
     * @param DealManager $dealManager
     * @param UserManager $userManager
     */
    public function __construct(DealManager $dealManager, UserManager $userManager)
    {
        $this->dealManager = $dealManager;
        $this->userManager = $userManager;
    }

    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @param string $identity
     * @return array|int|null
     * @throws \Core\Exception\LogicException
     */
    public function getUnansweredDealsNumber(string $identity)
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin($identity);

        $unansweredDeals = $this->dealManager->getDealsWithoutCLSInDealAgent($user);

        return \count($unansweredDeals);
    }

    /**
     * @param string $identity
     * @return bool
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\ORMException
     */
    public function isUserHasDeals(string $identity): bool
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin($identity);

        $deals = $this->dealManager->getAllowedDealByUser($user);

        return \count($deals) > 0;
    }
}