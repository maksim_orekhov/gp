<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class RemoteServerModeHelper
 * @package Application\View\Helper
 */
class RemoteServerModeHelper extends AbstractHelper
{
    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @return string
     */
    public function getRemoteServerMode()
    {
        return REMOTE_SERVER_MODE;
    }
}