<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class QueryHelper
 * @package Application\View\Helper
 */
class QueryHelper extends AbstractHelper
{
    const EXCLUDE_FIELD_DEFAULTS = [
        'page' => 1
    ];

    /**
     * @var \Zend\Http\Request
     */
    private $request;

    /**
     * QueryHelper constructor.
     * @param \Zend\Http\Request $request
     */
    public function __construct(\Zend\Http\Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    public function getQuery($include_field = [], $exclude_field = [])
    {
        $param_from_query = $this->request->getQuery()->toArray();
        $param_from_query = $this->excludeFromQuery($param_from_query, $exclude_field);
        foreach ($include_field as $field => $value){
            //remove old field
            if (isset($param_from_query[$field])) {
                unset($param_from_query[$field]);
            }
            //add new field
            $param_from_query[$field] = $value;
        }

        foreach ($param_from_query as $field => $value){
            if (array_key_exists($field, self::EXCLUDE_FIELD_DEFAULTS)
                && self::EXCLUDE_FIELD_DEFAULTS[$field] == $value) {
                unset($param_from_query[$field]);
            }
        }

        $result = http_build_query($param_from_query);
        $result = ($result !== '') ? '?'. $result : $result;

        return $result;
    }

    public function getBaseUrl()
    {
        $baseUri = $this->request->getUri()->getPath();
        return $baseUri;
    }

    /**
     * @param $param_from_query
     * @param array $exclude_field
     * @return mixed
     */
    private function excludeFromQuery($param_from_query, $exclude_field = []){
        foreach ($exclude_field as $field) {
            if (isset($param_from_query[$field])) {
                unset($param_from_query[$field]);
            }
        }

        return $param_from_query;
    }

    public function isActivePage($url)
    {
        return $this->getBaseUrl() == $url;
    }
}