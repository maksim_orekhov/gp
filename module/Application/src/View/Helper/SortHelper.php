<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

/**
 * Class SortHelper
 * @package Application\View\Helper
 */
class SortHelper extends AbstractHelper
{
    const DEFAULT_SORT = 'ASC';
    const NAME_FIELD   = 'order';
    const NAME_SORT    = 'sort';

    /**
     * @var mixed
     */
    private $param_from_query;

    /**
     * SortHelper constructor.
     * @param \Zend\Http\Request $request
     */
    public function __construct(\Zend\Http\Request $request)
    {
        $this->param_from_query = $request->getQuery()->toArray();
    }

    /**
     * @return $this
     */
    public function __invoke()
    {
        return $this;
    }

    /**
     * @param string $field_value
     * @return bool
     */
    public function isActive($field_value = '')
    {
        $param_from_query = $this->param_from_query;
        if (!empty($param_from_query)) {
            return (array_key_exists(self::NAME_FIELD, $param_from_query) && ($param_from_query[self::NAME_FIELD] === $field_value));
        }

        return false;
    }

    /**
     * @return string
     */
    public function getSort()
    {
        $param_from_query = $this->param_from_query;
        if (!empty($param_from_query) && array_key_exists(self::NAME_SORT, $param_from_query)) {
            return $param_from_query[self::NAME_SORT];
        }

        return self::DEFAULT_SORT;
    }

    /**
     * @param $field
     * @param array $exclude_from_query
     * @return string
     */
    public function getSortLink($field, $exclude_from_query = [])
    {
        $param_from_query = $this->param_from_query;

        //current sort
        $order_field = isset($param_from_query[self::NAME_FIELD]) ? $param_from_query[self::NAME_FIELD] : null;
        $sort = isset($param_from_query[self::NAME_SORT]) ? $param_from_query[self::NAME_SORT] : null;

        //detect sort direction
        $new_sort = self::DEFAULT_SORT;
        if ($order_field === $field) {
            $new_sort = ($sort === 'ASC') ? 'DESC' : 'ASC';
        }

        // part sort link
        $sortLink = '?' . self::NAME_FIELD . '=' . $field .'&'. self::NAME_SORT . '=' . $new_sort;

        //remove repetition in form_query
        $exclude_from_query[] = self::NAME_FIELD;
        $exclude_from_query[] = self::NAME_SORT;
        foreach ($exclude_from_query as $key) {
            if (isset($param_from_query[$key])) {
                unset($param_from_query[$key]);
            }
        }

        $part_form_query = http_build_query($param_from_query);
        $part_form_query = ($part_form_query === '') ? $part_form_query : '&'.$part_form_query;

        //full sort link
        $sortLink = $sortLink . $part_form_query;

        return $sortLink;
    }
}