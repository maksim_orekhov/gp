<?php
namespace Application\View\Helper\Factory;

use Application\View\Helper\VersionHelper;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * This is the factory class for AuthAdapter service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class VersionHelperFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return VersionHelper|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $applicationConfig = $container->get('ApplicationConfig');

        $version = get_version_git();
        if (APP_ENV_PROD || APP_ENV_TEST) {
            $version = $applicationConfig['version'] ?? null;
        }

        return new VersionHelper($version);
    }
}