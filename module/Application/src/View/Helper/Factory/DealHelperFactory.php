<?php
namespace Application\View\Helper\Factory;

use Application\Service\UserManager;
use Application\View\Helper\DealHelper;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\Deal\DealManager;

/**
 * Class DealHelperFactory
 * @package Application\View\Helper\Factory
 */
class DealHelperFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DealHelper|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealManager = $container->get(DealManager::class);
        $userManager = $container->get(UserManager::class);

        return new DealHelper($dealManager, $userManager);
    }
}