<?php

namespace Application\Service;

use Application\Entity\User;
use ModuleMessage\Entity\Message;
use ModuleMessage\Service\MessageManager;

/**
 * Class CommentManager
 * @package Application\Service
 */
abstract class CommentManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var MessageManager
     */
    protected $messageManager;


    /**
     * CommentManager constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param MessageManager $messageManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                MessageManager $messageManager)
    {
        $this->entityManager  = $entityManager;
        $this->messageManager = $messageManager;
    }

    /**
     * @param $comments
     * @param null $currentUser
     * @return array
     */
    public function getCommentCollectionOutput($comments, $currentUser = null)
    {
        $commentsOutput = [];
        foreach ($comments as $comment) {
            $commentsOutput[] = $this->messageManager->getMessageOutput($comment, $currentUser);
        }

        return $commentsOutput;
    }

    /**
     * @param $comment
     * @return array
     */
    public function getCommentOutput($comment)
    {
        return $this->messageManager->getMessageOutput($comment);
    }

    /**
     * @param $id
     * @return Message
     * @throws \Exception
     */
    public function getComment($id)
    {
        try {
            /** @var Message $comment */ // Can throw Exception
            $comment = $this->messageManager->getMessageById($id);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $comment;
    }

    /**
     * @param array $data
     * @param Message $comment
     * @return Message
     * @throws \Exception
     */
    public function updateComment(array $data, Message $comment)
    {
        $comment->setText($data['text']);

        try {
            $this->entityManager->flush($comment);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $comment;
    }

    /**
     * @param string $text
     * @param User $author
     * @return Message
     * @throws \Exception
     */
    protected function addComment(string $text, User $author)
    {
        try {
            /** @var Message $comment */
            $comment = $this->messageManager->saveMessage($text, $author);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $comment;
    }
}