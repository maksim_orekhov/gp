<?php
namespace Application\Service;

trait PaginatorOutputTrait
{
    public function getPaginatorOutput($paginator): array
    {
        $output = [];

        if ($paginator) {
            $output["page_count"] = $paginator->pageCount;
            $output["item_count_per_page"] = $paginator->itemCountPerPage;
            $output["first"] = $paginator->first;
            $output["current"] = $paginator->current;
            $output["last"] = $paginator->last;
            $output["pages_in_range"] = [];
            foreach ($paginator->pagesInRange as $page) {
                $output["pages_in_range"][$page] = $page;
            }
            $output["first_page_in_range"] = $paginator->firstPageInRange;
            $output["last_page_in_range"] = $paginator->lastPageInRange;
            $output["current_item_count"] = $paginator->currentItemCount;
            $output["total_item_count"] = $paginator->totalItemCount;
            $output["first_item_number"] = $paginator->firstItemNumber;
            $output["last_item_number"] = $paginator->lastItemNumber;
        }

        return $output;
    }
}