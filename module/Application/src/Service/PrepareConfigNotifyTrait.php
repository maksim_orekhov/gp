<?php
namespace Application\Service;

trait PrepareConfigNotifyTrait
{
    //default notify only email
    private $default_email_notify = true;
    private $default_sms_notify = false;

    /**
     * @param $config
     * @param $notification_name
     * @param $notification_setting
     * @return bool
     */
    public function getDealNotifyConfig($config, $notification_name, $notification_setting)
    {
        //set default setting
        switch ($notification_setting){
            case 'email':
                $notifyConfig = $this->default_email_notify;
                break;
            case 'sms':
                //if simulation enabled
                if (array_key_exists('sms_providers', $config)
                    && array_key_exists('message_send_simulation', $config['sms_providers'])
                    && $config['sms_providers']['message_send_simulation'])
                {
                    //don`t send sms
                    return false;
                }

                $notifyConfig = $this->default_sms_notify;
                break;
            default:
                $notifyConfig = false;
                break;
        }

        //set config setting
        if(array_key_exists('deal_notification', $config)
            && array_key_exists($notification_name, $config['deal_notification'])
            && array_key_exists($notification_setting, $config['deal_notification'][$notification_name])) {
            $notifyConfig = $config['deal_notification'][$notification_name][$notification_setting];
        }

        return $notifyConfig;
    }
}