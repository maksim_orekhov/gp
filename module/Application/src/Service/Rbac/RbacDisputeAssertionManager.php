<?php
namespace Application\Service\Rbac;

use Application\Entity\DealAgent;
use Application\Entity\Dispute;
use Application\Entity\Deal;
use Application\Entity\User;
use Zend\Permissions\Rbac\Rbac;
use Application\Service\UserManager;
use Core\Service\Auth\AuthService;

/**
 * Class RbacDisputeAssertionManager
 * @package Application\Service\Rbac
 */
class RbacDisputeAssertionManager
{

    /**
     * For dynamic assertions
     *
     * @param Rbac      $rbac
     * @param string    $permission
     * @param array     $params
     * @return bool
     */
    public function assert(Rbac $rbac, $permission, $params)
    {
        if( !array_key_exists('user', $params) || !array_key_exists('dispute', $params)) {

            return false;
        }

        // User является Customer'ом, который создал Dispute
        if ($permission === 'dispute.own.toggle') {

            /** @var Dispute $dispute */
            $dispute = $params['dispute'];
            /** @var Deal $deal */
            $deal = $dispute->getDeal();
            /** @var User $user */
            $user = $params['user'];
            /** @var DealAgent $disputeCreatorAgent */
            $disputeAuthor = $dispute->getAuthor();

            if ($disputeAuthor === $deal->getCustomer() && $user === $disputeAuthor->getUser()) {

                return true;
            }
        }

        return false;
    }
}