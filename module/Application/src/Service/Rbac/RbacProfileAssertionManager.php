<?php
namespace Application\Service\Rbac;

use Application\Entity\CivilLawSubjectChildInterface;
use Application\Entity\Token;
use Application\Entity\User;
use Application\Entity\CivilLawSubject;
use ModuleRbac\Service\Rbac;

/**
 * This service is used for invoking user-defined RBAC dynamic assertions
 */
class RbacProfileAssertionManager
{
    /**
     * For dynamic assertions
     *
     * @param Rbac      $rbac
     * @param string    $permission
     * @param array     $params
     * @return bool
     */
    public function assert(Rbac $rbac, $permission, $params)
    {
        if (!array_key_exists('civilLawSubject', $params)
            && !array_key_exists('civilLawSubjectChild', $params)
            && !array_key_exists('user', $params)
            && !array_key_exists('dealPartnerToken', $params)
        ) {

            return false;
        }

        // Get user object by login from Identity
        try {
            $currentUser = $rbac->getUser();
        }
        catch (\Throwable $t) {

            return false;
        }

        if ($permission === 'profile.own.edit') {

            // Проверка, что User из параметров и $currentUser один и тот же пользователь
            if (array_key_exists('user', $params) && $params['user'] instanceof User) {

                return $currentUser === $params['user'];
            }

            // Проверка, что владелец civilLawSubject и $currentUser один и тот же пользователь
            if (array_key_exists('civilLawSubject', $params)
                && $params['civilLawSubject'] instanceof CivilLawSubject) {

                return $currentUser === $params['civilLawSubject']->getUser();
            }

            // Проверка, что владелец civilLawSubjectChild (legalEntity, naturalPerson или soleProprietor)
            // и $currentUser - один и тот же пользователь
            if (array_key_exists('civilLawSubjectChild', $params)
                && $params['civilLawSubjectChild'] instanceof CivilLawSubjectChildInterface) {

                return $currentUser === $params['civilLawSubjectChild']->getCivilLawSubject()->getUser();
            }

            // Проверка, что владелец deal partner Token и $currentUser - один и тот же пользователь
            if (array_key_exists('dealPartnerToken', $params) && $params['dealPartnerToken'] instanceof Token) {

                return $currentUser === $params['dealPartnerToken']->getCounteragentUser();
            }
        }

        return false;
    }
}