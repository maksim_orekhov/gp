<?php
namespace Application\Service\Rbac;

use Zend\Permissions\Rbac\Rbac;

/**
 * Class RbacDealAssertionManager
 * @package Application\Service\Rbac
 */
class RbacDealAssertionManager
{
    /**
     * For dynamic assertions
     *
     * @param Rbac      $rbac
     * @param string    $permission
     * @param array     $params
     * @return bool
     */
    public function assert(Rbac $rbac, $permission, $params)
    {
        if( !array_key_exists('user', $params) || !array_key_exists('deal', $params)) {

            return false;
        }
        // Доступ к просмотру и редактированию сделки предоставляется только владельцам сделки (Customer, Contractor)
        if($permission === 'deal.own.view' or $permission === 'deal.own.edit') {

            if ($params['user'] === $params['deal']->getCustomer()->getUser()
                || $params['user'] === $params['deal']->getContractor()->getUser()) {

                return true;
            }
        }
        // Доступ к счету на оплату предоставляется только пользователю, выступающему в роли Customer
        if($permission === 'deal.own.customer.invoice') {

            if ($params['user'] === $params['deal']->getCustomer()->getUser()) {

                return true;
            }
        }
        // Доступ к скачиванию контракта по сделке предоставляется только владельцам сделки (Customer, Contractor)
        if($permission === 'deal.own.contract') {

            if ($params['user'] === $params['deal']->getCustomer()->getUser()
                || $params['user'] === $params['deal']->getContractor()->getUser()) {

                return true;
            }
        }
        // Доступ для роли Customer
        if($permission === 'deal.own.customer') {

            if ($params['user'] === $params['deal']->getCustomer()->getUser()) {

                return true;
            }
        }

        // Доступ для роли Contractor
        if($permission === 'deal.own.contractor') {
            if ($params['user'] === $params['deal']->getContractor()->getUser()) {

                return true;
            }
        }

        return false;
    }
}