<?php
namespace Application\Service\Rbac;

use ModuleRbac\Service\Rbac;


/**
 * This service is used for invoking user-defined RBAC dynamic assertions
 */
class RbacPaymentMethodAssertionManager
{
    /**
     * For dynamic assertions
     *
     * @param Rbac      $rbac
     * @param string    $permission
     * @param array     $params
     * @return bool
     */
    public function assert(Rbac $rbac, $permission, $params)
    {
        if(!array_key_exists('paymentMethod', $params)) {

            return false;
        }

        // Get user object by login from Identity
        try {
            $currentUser = $rbac->getUser();
        }
        catch (\Throwable $t) {

            return false;
        }

        if ($permission === 'profile.own.edit') {
            if ($currentUser === $params['paymentMethod']->getCivilLawSubject()->getUser()) {

                return true;
            }
        }

        return false;
    }
}