<?php
namespace Application\Service\Rbac;

use Application\Entity\Deal;
use Application\Entity\User;
use ModuleDelivery\Entity\DeliveryOrder;
use Zend\Permissions\Rbac\Rbac;

/**
 * Class RbacDeliveryOrderAssertionManager
 * @package Application\Service\Rbac
 */
class RbacDeliveryOrderAssertionManager
{
    /**
     * For dynamic assertions
     *
     * @param Rbac      $rbac
     * @param string    $permission
     * @param array     $params
     * @return bool
     */
    public function assert(Rbac $rbac, $permission, $params)
    {
        if(!array_key_exists('user', $params) || !array_key_exists('delivery_order', $params)) {

            return false;
        }

        if(!$params['user'] instanceof User || !$params['delivery_order'] instanceof DeliveryOrder) {

            return false;
        }
        // Доступ к просмотру и редактированию сделки предоставляется только владельцам сделки (Customer, Contractor)
        if ($permission === 'deal.own.view' || $permission === 'deal.own.edit') {
            /** @var User $user */
            $user = $params['user'];
            /** @var DeliveryOrder $deliveryOrder */
            $deliveryOrder = $params['delivery_order'];
            /** @var Deal $deal */
            $deal = $deliveryOrder->getDeal();

            if ($user === $deal->getCustomer()->getUser() || $user === $deal->getContractor()->getUser()) {

                return true;
            }
        }

        return false;
    }
}