<?php
namespace Application\Service\BankClient;

use Application\Controller\BankClientController;
use Application\Entity\Discount;
use Application\Entity\Refund;
use Application\Entity\RepaidOverpay;
use Application\Service\Parser\ParserAdapter;
use Application\Service\Payment\PaymentManager;
use Application\Service\SettingManager;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\BankClientPaymentOrderFile;
use ModuleFileManager\Entity\File;
use Application\Entity\Deal;
use Application\Entity\Payment;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use Application\Service\Deal\DealPolitics;
use Application\Service\Parser\BankClientParser;
use ModuleFileManager\Service\FileManager;
use ModulePaymentOrder\Service\BankClientPaymentOrderHandler;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Doctrine\ORM\EntityManager;

/**
 * Class BankClientManager
 * @package Application\Service
 */
class BankClientManager
{
    const EOL = "\r\n";

    const FILE_HEADER = "1CClientBankExchange".self::EOL."ВерсияФормата=1.02".self::EOL."Кодировка=Windows".self::EOL."Получатель=1С".self::EOL;
    const FILE_FOOTER = "КонецФайла";

    const DOCUMENT_TYPE = 'Платежное поручение';

    const INCOMING_PAYMENT_PURPOSE_ENTRY = 'Оплата по сделке ';

    #const DEAL_NUMBER_CREDIT_PAYMENT_PURPOSE_ENTRY = '/#GP(\d+)[UT]{1}/i';
    const DEAL_ID_CREDIT_PAYMENT_PURPOSE_ENTRY = '/GP[\D]*(\d+)/i';
    const DEAL_ID_CREDIT_PAYMENT_PURPOSE_ENTRY_STRONG = '/GP[\D]{0,1}(\d+)/i';

    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var \ModuleFileManager\Service\FileManager
     */
    private $fileManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * @var BankClientParser
     */
    private $bankClientParser;

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * @var BankClientPaymentOrderHandler
     */
    private $bankClientPaymentOrderHandler;

    /**
     * @var array
     */
    private $config;

    /**
     * BankClientManager constructor.
     * @param EntityManager $entityManager
     * @param FileManager $fileManager
     * @param PaymentManager $paymentManager
     * @param DealPolitics $dealPolitics
     * @param BankClientParser $bankClientParser
     * @param SettingManager $settingManager
     * @param PaymentOrderManager $paymentOrderManager
     * @param BankClientPaymentOrderHandler $bankClientPaymentOrderHandler
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                FileManager $fileManager,
                                PaymentManager $paymentManager,
                                DealPolitics $dealPolitics,
                                BankClientParser $bankClientParser,
                                SettingManager $settingManager,
                                PaymentOrderManager $paymentOrderManager,
                                BankClientPaymentOrderHandler $bankClientPaymentOrderHandler,
                                array $config)
    {
        $this->entityManager    = $entityManager;
        $this->fileManager      = $fileManager;
        $this->paymentManager   = $paymentManager;
        $this->dealPolitics     = $dealPolitics;
        $this->bankClientParser = $bankClientParser;
        $this->settingManager   = $settingManager;
        $this->paymentOrderManager = $paymentOrderManager;
        $this->bankClientPaymentOrderHandler = $bankClientPaymentOrderHandler;
        $this->config           = $config;
    }

    /**
     * @param $all_payment_order_hashes
     * @param array $transfer_order_array
     * @return bool
     */
    public function checkIfPaymentOrderAlreadyInSystem($all_payment_order_hashes, array $transfer_order_array): bool
    {
        $hash = $this->bankClientPaymentOrderHandler->getHashForPaymentOrder($transfer_order_array);
        if ( !in_array($hash, $all_payment_order_hashes) ) {

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getAllPaymentOrders()
    {
        // Get all Payment Orders
        $allProcessedPaymentOrders = $this->entityManager->getRepository(BankClientPaymentOrder::class)
            ->findAll();

        return $allProcessedPaymentOrders;
    }

    /**
     * @return array
     */
    public function getAllPaymentOrdersAsHashArray()
    {
        $allPaymentOrders = $this->getAllPaymentOrders();

        return $this->getArrayOfHashes($allPaymentOrders);
    }

    /**
     * @return array
     */
    public function getProcessedFilesAsHashArray(): array
    {
        // Get all processed files from DB
        $paymentOrders = $this->getAllPaymentOrders();
        // Convert array of objects to the array of strings

        return $this->getArrayOfHashes($paymentOrders);
    }

    /**
     * @param string $file_name
     * @param array $bank_client_payment_orders
     * @param string $file_hash
     * @return array
     * @throws \Exception
     */
    public function processingPaymentOrders(string $file_name, array $bank_client_payment_orders, string $file_hash): array
    {
        $paymentOrders = [];
        $errors = [];
        try {
            /** @var File $file */ // Select File by name. If not find throws Exception.
            $file = $this->fileManager->getFileByName($file_name);
            // Get all PaymentOrder's hashes
            $all_payment_order_hashes = $this->getAllPaymentOrdersAsHashArray();
            // Create and save new INCOMING BankClientPaymentOrder objects from array data

            foreach ($bank_client_payment_orders as $payment_order) {
                // Check if GuarantPay is participant and if Payment Order already exists
                if ($this->settingManager->isGuarantPayParticipant($payment_order)
                    && !$this->checkIfPaymentOrderAlreadyInSystem($all_payment_order_hashes, $payment_order) ) {

                    // Try to create and save new BankClientPaymentOrder object
                    try {
                        /** @var BankClientPaymentOrder $paymentOrder */// If can not save throws Exception.
                        $paymentOrder = $this->createNewPaymentOrderFromArray($payment_order);

                        if ($paymentOrder) {
                            $paymentOrders[] = $paymentOrder;
                        }
                    }
                    catch (\Throwable $t) {
                        logException($t, 'bank-client-error');
                        $errors[$file_name.'-'.$payment_order['document_number']] = $t->getMessage();
                    }
                }
            }

            // Registers File as processed. If can not register throws Exception.
            $bankClientPaymentOrderFile = $this->registerFileAsProcessed($file, $file_hash);
            // Set relations paymentOrders to bankClientPaymentOrderFile
            $this->setPaymentOrdersRelations($paymentOrders, $bankClientPaymentOrderFile);
        }
        catch (\Throwable $t) {
            logException($t, 'bank-client-error');
            throw new \Exception($t->getMessage());
        }

        return ['paymentOrders' => $paymentOrders, 'errors'=> $errors];
    }

    /**
     * @param array $paymentOrders
     * @param BankClientPaymentOrderFile $bankClientPaymentOrderFile
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function setPaymentOrdersRelations(array $paymentOrders, BankClientPaymentOrderFile $bankClientPaymentOrderFile)
    {
        foreach ($paymentOrders as $paymentOrder) {
            $paymentOrder->setBankClientPaymentOrderFile($bankClientPaymentOrderFile);
        }

        $this->entityManager->flush();
    }

    /**
     * Checks whether File with specified name already processed
     *
     * @param string $file_name
     * @return bool
     */
    public function isFileProcessed(string $file_name): bool
    {
        $bankClientFile = $this->entityManager->getRepository(BankClientPaymentOrderFile::class)
            ->findOneBy(array('fileName' => $file_name));

        if( !$bankClientFile ) {
            return false;
        }

        return true;
    }

    /**
     * Returns array with names of all processed files
     *
     * @return array
     */
    public function getProcessedFilesAsArray(): array
    {
        // Get all processed files from DB
        $processed_files = $this->getAllProcessedFiles();
        // Convert array of objects to the array of strings
        $processed_file_array = [];
        foreach ($processed_files as $processed_file) {
            $processed_file_array[] = $processed_file->getFileName();
        }

        return $processed_file_array;
    }

    /**
     * Returns array with hashes of all processed files
     *
     * @return array
     */
    public function getProcessedFileHashesAsArray(): array
    {
        // Get all processed files from DB
        $processed_files = $this->getAllProcessedFiles();
        // Convert array of objects to the array of strings

        $processed_file_hashes = $this->getAssociativeArrayOfFileHashes($processed_files);

        return $processed_file_hashes;
    }

    /**
     * Converts array of objects to the associative array of hashes with file names as keys
     *
     * @param $array_of_files
     * @return array
     */
    private function getAssociativeArrayOfFileHashes($array_of_files)
    {
        $array = [];
        foreach ($array_of_files as $file) {
            $array[$file->getFileName()] = $file->getHash();
        }

        return $array;
    }

    /**
     * Converts array of objects to the array of hashes
     *
     * @param $array_of_objects
     * @return array
     */
    private function getArrayOfHashes($array_of_objects)
    {
        $array = [];
        foreach ($array_of_objects as $item) {
            $array[] = $item->getHash();
        }

        return $array;
    }

    /**
     * Create and save new BankClientPaymentOrder object
     *
     * @param array $payment_order
     * @return BankClientPaymentOrder
     * @throws \Exception
     */
    public function createNewPaymentOrderFromArray(array $payment_order): BankClientPaymentOrder
    {
        try {
            // Validate data // Can throw Exception
            $this->bankClientPaymentOrderHandler->validationBankClientPaymentOrderData($payment_order);
        }
        catch (\Throwable $t) {

            throw new \Exception('Validation error on PaymentOrder addition: ' . $t->getMessage());
        }

        $bankClientPaymentOrder = null;

        // Предполагаемый id сделки (слабые ограничения)
        $supposed_deal_id = $this->getSupposedDealIdFromPaymentPurpose($payment_order['payment_purpose']);

        if (null === $supposed_deal_id) {

            throw new \Exception('Платёжка ' . $payment_order['payment_purpose']
                .' - не добавлена (предполагаемый номер Сделки не найден)');
        }

        $hash = $this->bankClientPaymentOrderHandler->getHashForPaymentOrder($payment_order);
        $payment_order['hash'] = $hash;

        // Create new BankClientPaymentOrder object
        /** @var BankClientPaymentOrder $bankClientPaymentOrder */
        $bankClientPaymentOrder = new BankClientPaymentOrder();

        // Hydrate array $payment_order to BankClientPaymentOrder object
        $hydrator = new DoctrineHydrator($this->entityManager, $bankClientPaymentOrder);
        $bankClientPaymentOrder = $hydrator->hydrate($payment_order, $bankClientPaymentOrder);
        // Set created date and type "incoming"
        $bankClientPaymentOrder->setCreated(new \DateTime());
        $bankClientPaymentOrder->setType(PaymentOrderManager::TYPE_INCOMING);

        // Сохраняем в любом случае, так как есть $deal_id
        $this->entityManager->persist($bankClientPaymentOrder);
        $this->entityManager->flush();

        // Надёжный id сделки (сильные ограничения, см. регулярку)
        $reliable_deal_id = $this->getReliableDealIdFromPaymentPurpose($payment_order['payment_purpose']);

        if (null === $reliable_deal_id) {

            throw new \Exception('Платёжка ' . $payment_order['payment_purpose']
                .' - добавлена в базу данных, но к сделке не привязана (номер Сделки не найден)');
        }

        try {
            $this->paymentManager->manageIncomingBankClientPaymentOrderRelations($bankClientPaymentOrder, $reliable_deal_id);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $bankClientPaymentOrder;
    }

    /**
     * @param $payment_purpose
     * @return int|null
     */
    private function getSupposedDealIdFromPaymentPurpose($payment_purpose)
    {
        preg_match_all(self::DEAL_ID_CREDIT_PAYMENT_PURPOSE_ENTRY, $payment_purpose, $matches);

        return isset($matches[1][0]) ? (int) $matches[1][0]: null;
    }

    /**
     * @param $payment_purpose
     * @return int|null
     */
    private function getReliableDealIdFromPaymentPurpose($payment_purpose)
    {
        preg_match_all(self::DEAL_ID_CREDIT_PAYMENT_PURPOSE_ENTRY_STRONG, $payment_purpose, $matches);

        return isset($matches[1][0]) ? (int) $matches[1][0]: null;
    }

    /**
     * Registers specified File as processed
     *
     * @param File $file
     * @param string $file_hash
     * @return BankClientPaymentOrderFile
     * @throws \Exception
     */
    private function registerFileAsProcessed(File $file, string $file_hash): BankClientPaymentOrderFile
    {
        $bankClientPaymentOrderFile = new BankClientPaymentOrderFile();
        $bankClientPaymentOrderFile->setFile($file);
        $bankClientPaymentOrderFile->setFileName($file->getName());
        $bankClientPaymentOrderFile->setHash($file_hash);

        $this->entityManager->persist($bankClientPaymentOrderFile);

        $this->entityManager->flush($bankClientPaymentOrderFile);

        if( !$bankClientPaymentOrderFile->getId() ) {

            throw new \Exception("Can not add new object BankClientPaymentOrderFile to database");
        }

        return $bankClientPaymentOrderFile;
    }

    /**
     * Get all bank-client files
     *
     * @param $params
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function getAllBankClientFilesByPath($params)
    {
        $files = $this->entityManager->getRepository(File::class)
            ->getAllBankClientFilesByPath(BankClientController::FILE_STORING_FOLDER, $params);

        return $files;
    }

    /**
     * @return array
     */
    public function getAllProcessedFiles()
    {
        $files = $this->entityManager->getRepository(BankClientPaymentOrderFile::class)
            ->findAll();

        return $files;
    }

    /**
     * @param $id
     * @return BankClientPaymentOrder
     * @throws \Exception
     */
    public function getPaymentOrderById($id): BankClientPaymentOrder
    {
        $paymentOrder = $this->entityManager->getRepository(BankClientPaymentOrder::class)
            ->findOneBy(array('id' => $id));

        if( !$paymentOrder || !$paymentOrder instanceof BankClientPaymentOrder) {

            throw new \Exception('No PaymentOrder found by Id');
        }

        return $paymentOrder;
    }

    /**
     * @return array|null
     * @throws \Exception
     */
    public function getReadyOutgoingPaymentOrders()
    {
        // Payments, c готовыми на выплату платёжками
        $payments = $this->entityManager->getRepository(Payment::class)
            ->getPaymentWithReadyToPayOffOutgoingPaymentOrders();

        // Discounts, c готовыми на скидку платёжками
        $discounts = $this->entityManager->getRepository(Discount::class)
            ->getDiscountWithReadyToPayOffOutgoingPaymentOrders();

        // Refunds (возврат), c готовыми на возврат платёжками
        $refunds = $this->entityManager->getRepository(Refund::class)
            ->getRefundWithReadyToPayOffOutgoingPaymentOrders();

        // $repaidOverpay (возврат переплаты), c готовыми на переплату платёжками
        $repaidOverpays = $this->entityManager->getRepository(RepaidOverpay::class)
            ->getRepaidOverpayWithReadyToPayOffOutgoingPaymentOrders();

        $payment_transactions = [];

        if (!empty($refunds)) {
            /**
             * @var Refund $refund
             * @var array $refunds
             */
            foreach ($refunds as $refund) {
                /** @var Deal $deal */
                $deal = $refund->getDispute()->getDeal();
                $deal_id = $deal->getId();
                $payment_transactions[$deal->getId()] = [];
                $payment_transactions[$deal->getId()]['deal'] = $deal;
                $payment_transactions[$deal->getId()]['outgoing_transactions'] = [];

                $refundTransaction = $this->paymentOrderManager->getRefundOutgoingPaymentTransaction($refund);

                $payment_transactions[$deal_id]['outgoing_transactions']['refund'] = $refundTransaction;

                try {
                    // Can throw Exception
                    $payment_transactions[$deal_id]['total_incoming_amount'] = $this->getTotalIncomingAmount($deal->getPayment(), true);
                    $payment_transactions[$deal_id]['total_incoming_amount_without_overpay'] = $this->getTotalIncomingAmount($deal->getPayment(), false);
                }
                catch (\Throwable $t) {

                    throw new \Exception($t->getMessage());
                }
            }
        }

        if (!empty($payments)) {
            /** @var Payment $payment */
            foreach ($payments as $payment) {
                /** @var Deal $deal */
                $deal = $payment->getDeal();
                $deal_id = $deal->getId();
                // Обязательно создаем массив с ключом id сделки
                $payment_transactions[$deal_id] = [];
                $payment_transactions[$deal_id]['deal'] = $deal;
                $payment_transactions[$deal_id]['outgoing_transactions'] = [];

                /** @var BankClientPaymentOrder $paymentTransaction */ // @TODO Пока только один тип
                $paymentTransaction = $this->paymentOrderManager->getPaymentOutgoingPaymentTransaction($payment);

                $payment_transactions[$deal_id]['outgoing_transactions']['payment'] = $paymentTransaction;
                try {
                    // Can throw Exception
                    $payment_transactions[$deal_id]['total_incoming_amount'] = $this->getTotalIncomingAmount($payment, true);
                    $payment_transactions[$deal_id]['total_incoming_amount_without_overpay'] = $this->getTotalIncomingAmount($payment, false);
                }
                catch (\Throwable $t) {

                    throw new \Exception($t->getMessage());
                }
            }

            // Скидки без outgoingPaymentTransaction не может быть
            if (!empty($discounts)) {
                /**
                 * @var Discount $discount
                 * @var array $discounts
                 */
                foreach ($discounts as $discount) {
                    /** @var Deal $deal */
                    $deal = $discount->getDispute()->getDeal();
                    $deal_id = $deal->getId();
                    if (isset($payment_transactions[$deal_id])
                        && isset($payment_transactions[$deal_id]['outgoing_transactions'])
                        && isset($payment_transactions[$deal_id]['outgoing_transactions']['payment']))
                    {
                        /** @var BankClientPaymentOrder $paymentTransaction */ // @TODO Пока только один тип
                        $paymentTransaction = $this->paymentOrderManager->getDiscountOutgoingPaymentTransaction($discount);
                        $payment_transactions[$deal_id]['outgoing_transactions']['discount'] = $paymentTransaction;
                    }
                }
            }
        }

        if (!empty($repaidOverpays)) {
            /**
             * @var RepaidOverpay $repaidOverpay
             * @var array $repaidOverpays
             */
            foreach ($repaidOverpays as $repaidOverpay) {
                /** @var Deal $deal */
                $deal = $repaidOverpay->getPayment()->getDeal();
                $deal_id = $deal->getId();
                // Если нет ключа $deal_id
                if (!isset($payment_transactions[$deal_id])) {
                    // Обязательно создаем массив с ключом id сделки
                    $payment_transactions[$deal_id] = [];
                    $payment_transactions[$deal_id]['deal'] = $deal;
                    $payment_transactions[$deal_id]['outgoing_transactions'] = [];
                }
                if (isset($payment_transactions[$deal_id]['outgoing_transactions'])) {
                    /** @var BankClientPaymentOrder $paymentTransaction */
                    $repaidOverpayTransaction = $this->paymentOrderManager->getRepaidOverpayOutgoingPaymentTransaction($repaidOverpay);
                    $payment_transactions[$deal_id]['outgoing_transactions']['repaid_overpay'] = $repaidOverpayTransaction;
                }
            }
        }

        $ready_payment_transactions = [];

        if (!empty($payment_transactions)) {
            // Проверка готовность Сделок, связанных с paymentOrders, для включения в генерацию credit платёжки на выплату Contractor'у
            /** @var array $payment_transaction */
            foreach ($payment_transactions as $payment_transaction) {

                if ($this->dealPolitics->isOutgoingPaymentOrdersReadyToPay($payment_transaction)) {

                    foreach ($payment_transaction['outgoing_transactions'] as $outgoing_transaction) {

                        $ready_payment_transactions[] = $outgoing_transaction;
                    }
                }
            }
        }

        return $ready_payment_transactions;
    }

    /**
     * @param Payment $payment
     * @param bool $with_overpay
     * @return float|int
     * @throws \Exception
     */
    private function getTotalIncomingAmount(Payment $payment, $with_overpay = false)
    {
        try {
            // Can throw Exception
            $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, $with_overpay);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $total_incoming_amount;
    }

    /**
     * @param array $paymentOrders
     * @return mixed
     * @throws \Exception
     */
    public function generateBankClientFile(array $paymentOrders)
    {
        // Save file and create object
        $file = $this->createFileWithNewPaymentOrders($paymentOrders);
        // Get file path
        $file_path = $this->config['file-management']['main_upload_folder'].BankClientController::FILE_STORING_FOLDER . "/" . $file->getName();
        // Registers File as processed. If can not register throws Exception.
        $bankClientPaymentOrderFile = $this->registerFileAsProcessed($file, $file_hash = hash_file('md5', $file_path));
        // Set relations paymentOrders to bankClientPaymentOrderFile
        $this->setPaymentOrdersRelations($paymentOrders, $bankClientPaymentOrderFile);

        return $file;
    }

    /**
     * Созданеие файла с новыми платёжками из данных о новых завершенных сделках
     * @param array $paymentOrders
     * @return File
     * @throws \Exception
     */
    private function createFileWithNewPaymentOrders(array $paymentOrders)
    {
        /** @var BankClientPaymentOrder $firstPaymentOrder */ // Первый элемент массива
        $firstPaymentOrder = $paymentOrders[0];
        /** @var BankClientPaymentOrder $lastPaymentOrder */ // Последний элемент массива
        $lastPaymentOrder = $paymentOrders[count($paymentOrders)-1];

        // Path to folder for temporary file
        $tmp_folder = "./".$this->config['file-management']['main_upload_folder'].BankClientController::TEMPORARY_FILES_FOLDER;
        // Создание директории, если нет
        $this->fileManager->createFolderIfNotExists($tmp_folder);
        // Full path to file
        $path = $tmp_folder . "/orders_" . $firstPaymentOrder->getId() . "-" . $lastPaymentOrder->getId() . ".txt";
        // Creates file if not exists
        $file_with_new_payment_orders = fopen($path, "w") or die("Не удается создать файл");

        // Write file header data
        fwrite($file_with_new_payment_orders, mb_convert_encoding(self::FILE_HEADER, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));

        // Write PaymentOrders data
        foreach ($paymentOrders as $paymentOrder) {
            // Get PaymentOrder as string
            $payment_order = $this->getPaymentOrderAsString($paymentOrder);
            fwrite($file_with_new_payment_orders, mb_convert_encoding($payment_order, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));

            // Write SECTION_END_POINT
            fwrite($file_with_new_payment_orders, mb_convert_encoding(BankClientParser::SECTION_END_POINT.self::EOL, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
        }

        // Write file footer data
        fwrite($file_with_new_payment_orders, mb_convert_encoding(self::FILE_FOOTER, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));

        fclose($file_with_new_payment_orders);

        // Save file
        $params = [
            'file_source' => $path,
            'file_name' => "orders_" . $firstPaymentOrder->getId() . "-" . $lastPaymentOrder->getId() . ".txt",
            'file_type' => "text/plain",
            'file_size' => filesize($path),
            'path' => BankClientController::FILE_STORING_FOLDER
        ];

        $file = $this->fileManager->write($params);

        // Remove temporary file
        unlink($path);

        return $file;
    }

    /**
     * Формирование данных платёжного поручения в виде строки
     * @param BankClientPaymentOrder $paymentOrder
     * @return string
     */
    private function getPaymentOrderAsString(BankClientPaymentOrder $paymentOrder)
    {
        $txt = "СекцияДокумент=".$paymentOrder->getDocumentType().self::EOL;
        $txt .= "Номер=".$paymentOrder->getDocumentNumber().self::EOL;
        $txt .= "Дата=".$paymentOrder->getDocumentDate().self::EOL;
        $txt .= "Сумма=".$paymentOrder->getAmount().self::EOL;
        $txt .= "ПлательщикСчет=".$paymentOrder->getPayerAccount().self::EOL;
        $txt .= "Плательщик=".$paymentOrder->getPayer().self::EOL;
        $txt .= "ПлательщикИНН=".$paymentOrder->getPayerInn().self::EOL;
        $txt .= "Плательщик1=".$paymentOrder->getPayer1().self::EOL;
        $txt .= "ПлательщикКПП=".$paymentOrder->getPayerKpp().self::EOL;
        $txt .= "ПлательщикБанк1=".$paymentOrder->getPayerBank1().self::EOL;
        $txt .= "ПлательщикБИК=".$paymentOrder->getPayerBik().self::EOL;
        $txt .= "ПлательщикКорсчет=".$paymentOrder->getPayerCorrAccount().self::EOL;
        $txt .= "ПолучательСчет=".$paymentOrder->getRecipientAccount().self::EOL;
        $txt .= "Получатель=".$paymentOrder->getRecipient().self::EOL;
        $txt .= "ПолучательИНН=".$paymentOrder->getRecipientInn().self::EOL;
        $txt .= "Получатель1=".$paymentOrder->getRecipient1().self::EOL;
        $txt .= "ПолучательКПП=".$paymentOrder->getRecipientKpp().self::EOL;
        $txt .= "ПолучательБанк1=".$paymentOrder->getRecipientBank1().self::EOL;
        $txt .= "ПолучательБИК=".$paymentOrder->getRecipientBik().self::EOL;
        $txt .= "ПолучательКорсчет=".$paymentOrder->getRecipientCorrAccount().self::EOL;
        $txt .= "НазначениеПлатежа=".$paymentOrder->getPaymentPurpose().self::EOL;
        $txt .= "ВидОплаты=01".self::EOL;
        $txt .= "Очередность=5".self::EOL;

        return $txt;
    }

    /**
     * @param $paymentOrder
     * @return array
     */
    private function getPaymentOrderAsArray($paymentOrder)
    {
        // Convert object to string
        $payment_order = $this->getPaymentOrderAsString($paymentOrder);
        // Convert string to array
        $lines = explode(PHP_EOL, $payment_order);
        $array = [];

        foreach ($lines as $line) {
            $parts_of_line = explode('=', $line);
            // Не обрабатываем пустой элемент (последний всегда пустой)
            if($parts_of_line[0] != null) {
                $array[$this->bankClientParser->getPropertyName($parts_of_line[0])] =
                    $this->bankClientParser->dataTypeConversion($this->bankClientParser->getPropertyName($parts_of_line[0]), $parts_of_line[1]);
            }
        }

        return $array;
    }

    /**
     * @param $path
     * @param $type
     * @return array|mixed
     * @TODO Нужен ли?
     */
    public function getDataFile($path, $type)
    {
        $file_data = [];
        $file_content = $this->fileManager->read($path);

        if (!empty($file_content)) {
            $adapter = new ParserAdapter();
            $file_data = $adapter->execute($file_content, $type);
        }

        return $file_data;
    }

    public function getFilesOutput($files)
    {
        $filesOutput = [];

        /** @var File $file */
        foreach ($files as $file) {
            $filesOutput[] = $this->getFileOutput($file);
        }

        return $filesOutput;
    }

    public function getFileOutput(File $file)
    {
        $fileOutput = null;

        if ($file && $file instanceof File) {
            $fileOutput = [];
            $fileOutput['id'] = $file->getId();
            $fileOutput['name'] = $file->getName();
            $fileOutput['origin_name'] = $file->getOriginName();
            $fileOutput['type'] = $file->getType();
            $fileOutput['path'] = $file->getPath();
            $fileOutput['size'] = $file->getSize();
            $fileOutput['created'] = $file->getCreated()->format('d.m.Y');
        }

        return $fileOutput;
    }
}