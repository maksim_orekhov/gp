<?php
namespace Application\Service\BankClient\Factory;

use Application\Service\Payment\PaymentMethodManager;
use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Service\BankClientPaymentOrderHandler;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\BankClient\BankClientManager;
use ModuleFileManager\Service\FileManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Deal\DealPolitics;
use Application\Service\Parser\BankClientParser;
use Application\Service\SettingManager;
use Application\Service\Payment\PaymentPolitics;
use Application\Service\Deal\Status\TribunalStatus;
use Application\Service\Deal\Status\RefundStatus;
use Application\Service\Deal\Status\DisputeStatus;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Application\Service\Payment\CalculatedDataProvider;

/**
 * This is the factory class for BankClientManager service
 */
class BankClientManagerFactory implements FactoryInterface
{
    /**
     * This method creates the BankClientManager service and returns its instance.
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BankClientManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $fileManager = $container->get(FileManager::class);
        $paymentManager = $container->get(PaymentManager::class);
        $dealPolitics = $container->get(DealPolitics::class);
        $bankClientParser = $container->get(BankClientParser::class);
        $settingManager = $container->get(SettingManager::class);
        $paymentOrderManager = $container->get(PaymentOrderManager::class);
        $bankClientPaymentOrderHandler = $container->get(BankClientPaymentOrderHandler::class);

        $config = $container->get('Config');

        return new BankClientManager(
            $entityManager,
            $fileManager,
            $paymentManager,
            $dealPolitics,
            $bankClientParser,
            $settingManager,
            $paymentOrderManager,
            $bankClientPaymentOrderHandler,
            $config
        );
    }
}