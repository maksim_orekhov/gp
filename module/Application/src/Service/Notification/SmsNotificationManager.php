<?php
namespace Application\Service\Notification;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Service\PrepareConfigNotifyTrait;
use Core\Exception\LogicException;
use ModuleCode\Service\SmsStrategyContext;

/**
 * Class EmailNotificationManager
 * @package Application\Service
 */
class SmsNotificationManager
{
    use \Application\Service\SmsNotifyTrait;
    use PrepareConfigNotifyTrait;

    private $sms_notify_to_invitation;

    /**
     * @var array
     */
    private $config;

    /**
     * SmsNotificationManager constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;

        $this->sms_notify_to_invitation = $this->getDealNotifyConfig($config, 'invitation_to_deal', 'sms');
    }

    /**
     * @param Deal $deal
     * @param DealAgent $ownerDealAgent
     * @throws \Exception
     */
    public function invitationOwnerDealAgentBySms(Deal $deal, DealAgent $ownerDealAgent)
    {
        try{
            if ($this->sms_notify_to_invitation && $ownerDealAgent->getUser()) {
                $userPhone = $ownerDealAgent->getUser()->getPhone();
                $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);

                if ($smsStrategy->isAllowedSmsNotify()) {
                    // Sms provider selection (by provider balance and so on...)
                    $smsProvider = $smsStrategy->getSmsProvider();
                    // Message create
                    $smsMessage = $this->getInvitationToDealForOwnerSmsMessage($deal);
                    // Send message
                    $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);

                } else {

                    throw new LogicException(null, LogicException::SMS_FORBIDDEN_BY_POLICY_NOTIFICATION);
                }
            }
        }
        catch (\Throwable $t) {

            logException($t, 'sms-error');
        }
    }

    /**
     * @param Deal $deal
     * @param DealAgent $counterDealAgent
     * @throws \Exception
     */
    public function invitationCounterDealAgentBySms(Deal $deal, DealAgent $counterDealAgent)
    {
        try {
            if ($this->sms_notify_to_invitation && $counterDealAgent->getUser()) {
                $userPhone = $counterDealAgent->getUser()->getPhone();
                $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);

                if ($smsStrategy->isAllowedSmsNotify()) {
                    // Sms provider selection (by provider balance and so on...)
                    $smsProvider = $smsStrategy->getSmsProvider();
                    // Message create
                    $smsMessage = $this->getInvitationToDealSmsMessage($deal);
                    // Send message
                    $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);

                } else {

                    throw new LogicException(null, LogicException::SMS_FORBIDDEN_BY_POLICY_NOTIFICATION);
                }
            }
        }
        catch (\Throwable $t) {

            logException($t, 'sms-error');
        }
    }
}