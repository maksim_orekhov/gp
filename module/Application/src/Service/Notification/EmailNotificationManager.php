<?php
namespace Application\Service\Notification;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\User;
use Application\Provider\Mail\DealInvitationSender;
use Application\Provider\Mail\DealNotificationSender;
use Application\Provider\Token\DealInvitationTokenProvider;
use Application\Service\PrepareConfigNotifyTrait;
use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;

/**
 * Class EmailNotificationManager
 * @package Application\Service
 */
class EmailNotificationManager
{
    use PrepareConfigNotifyTrait;

    const MAX_RECURSION_LEVEL = 3;

    private $email_notify_to_invitation;

    /**
     * @var DealNotificationSender
     */
    private $dealNotificationSender;
    /**
     * @var DealInvitationTokenProvider
     */
    private $dealInvitationTokenProvider;
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * EmailNotificationManager constructor.
     * @param DealInvitationTokenProvider $dealInvitationTokenProvider
     * @param DealNotificationSender $dealNotificationSender
     * @param EntityManager $entityManager
     * @param array $config
     */
    public function __construct(DealInvitationTokenProvider $dealInvitationTokenProvider,
                                DealNotificationSender $dealNotificationSender,
                                EntityManager $entityManager,
                                array $config)
    {
        $this->dealInvitationTokenProvider = $dealInvitationTokenProvider;
        $this->dealNotificationSender = $dealNotificationSender;
        $this->entityManager = $entityManager;

        $this->email_notify_to_invitation = $this->getDealNotifyConfig($config, 'invitation_to_deal', 'email');
    }

    /**
     * @param Deal $deal
     * @throws \Exception
     */
    public function invitationDealAgentsByEmail(Deal $deal)
    {
        /** @var DealAgent $ownerDealAgent */
        $ownerDealAgent = $deal->getOwner();
        $counterDealAgent = $ownerDealAgent === $deal->getCustomer() ? $deal->getContractor() : $deal->getCustomer();

        // 1. уведомление для автора сделки
        $this->invitationOwnerDealAgentByEmail($deal, $counterDealAgent, $ownerDealAgent);
        // 2. уведомление для контрагента сделки
        $this->invitationCounterDealAgentByEmail($deal, $counterDealAgent, $ownerDealAgent);
    }

    /**
     * @param Deal $deal
     * @param DealAgent $counterDealAgent
     * @param DealAgent $ownerDealAgent
     * @param bool $is_resend
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function invitationOwnerDealAgentByEmail(Deal $deal, DealAgent $counterDealAgent = null, DealAgent $ownerDealAgent = null, $is_resend = false)
    {
        if ($this->email_notify_to_invitation) {
            $ownerDealAgent = $ownerDealAgent ?? $deal->getOwner();
            $counterDealAgent = $counterDealAgent ?? ($ownerDealAgent === $deal->getCustomer() ? $deal->getContractor() : $deal->getCustomer());

            if ($ownerDealAgent->getUser()) { //if user exist
                $param = [
                    'deal' => $deal,
                    'deal_invitation_type' => DealInvitationSender::DEAL_OWNER_NOTIFICATION,
                    'author_deal_agent_name' => $ownerDealAgent->getUser() ? $ownerDealAgent->getUser()->getLogin() : $ownerDealAgent->getEmail(),
                    'counter_deal_agent_name' => $counterDealAgent->getUser() ? $counterDealAgent->getUser()->getLogin() : $counterDealAgent->getEmail(),
                    'counter_deal_agent_role' => $counterDealAgent === $deal->getCustomer() ? 'CUSTOMER' : 'CONTRACTOR',
                    'is_resend' => $is_resend
                ];
            } else {//if new user exist
                $param = [
                    'deal' => $deal,
                    'deal_invitation_type' => DealInvitationSender::DEAL_OWNER_REGISTRATION,
                    'author_deal_agent_name' => $ownerDealAgent->getUser() ? $ownerDealAgent->getUser()->getLogin() : $ownerDealAgent->getEmail(),
                    'counter_deal_agent_name' => $counterDealAgent->getUser() ? $counterDealAgent->getUser()->getLogin() : $counterDealAgent->getEmail(),
                    'counter_deal_agent_role' => $counterDealAgent === $deal->getCustomer() ? 'CUSTOMER' : 'CONTRACTOR',
                    'is_resend' => $is_resend
                ];

                $encoded_token = $this->dealInvitationTokenProvider->encodedToken(null, [
                    'email' => $ownerDealAgent->getEmail(),
                    'dealId' => $deal->getId(),
                    'dealAgentId' => $ownerDealAgent->getId(),
                ]);

                $param['encoded_token'] = $encoded_token;
            }

            $this->sendInvitationForDealAgent($ownerDealAgent->getEmail(), $param);
        }
        //проставляем дату приглашения к сделке или обновляем если приглашение повторно
        $ownerDealAgent->setInvitationDate(new \DateTime());
        $this->entityManager->persist($ownerDealAgent);
        $this->entityManager->flush($ownerDealAgent);
    }

    /**
     * @param Deal $deal
     * @param DealAgent $counterDealAgent
     * @param DealAgent $ownerDealAgent
     * @param bool $is_resend
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function invitationCounterDealAgentByEmail(Deal $deal, DealAgent $counterDealAgent = null, DealAgent $ownerDealAgent = null, $is_resend = false)
    {
        if ($this->email_notify_to_invitation) {
            $ownerDealAgent = $ownerDealAgent ?? $deal->getOwner();
            $counterDealAgent = $counterDealAgent ?? ($ownerDealAgent === $deal->getCustomer() ? $deal->getContractor() : $deal->getCustomer());

            if ($counterDealAgent->getUser()) { //if user exist
                $param = [
                    'deal' => $deal,
                    'deal_invitation_type' => DealInvitationSender::DEAL_NOTIFICATION,
                    'author_deal_agent_name' => $ownerDealAgent->getUser() ? $ownerDealAgent->getUser()->getLogin() : $ownerDealAgent->getEmail(),
                    'counter_deal_agent_name' => $counterDealAgent->getUser() ? $counterDealAgent->getUser()->getLogin() : $counterDealAgent->getEmail(),
                    'counter_deal_agent_role' => $counterDealAgent === $deal->getCustomer() ? 'CUSTOMER' : 'CONTRACTOR',
                    'is_resend' => $is_resend
                ];

            } else {//if new user exist
                $param = [
                    'deal' => $deal,
                    'deal_invitation_type' => DealInvitationSender::DEAL_REGISTRATION,
                    'author_deal_agent_name' => $ownerDealAgent->getUser() ? $ownerDealAgent->getUser()->getLogin() : $ownerDealAgent->getEmail(),
                    'counter_deal_agent_name' => $counterDealAgent->getUser() ? $counterDealAgent->getUser()->getLogin() : $counterDealAgent->getEmail(),
                    'counter_deal_agent_role' => $counterDealAgent === $deal->getCustomer() ? 'CUSTOMER' : 'CONTRACTOR',
                    'is_resend' => $is_resend
                ];

                $encoded_token = $this->dealInvitationTokenProvider->encodedToken(null, [
                    'email' => $counterDealAgent->getEmail(),
                    'dealId' => $deal->getId(),
                    'dealAgentId' => $counterDealAgent->getId(),
                ]);

                $param['encoded_token'] = $encoded_token;
            }

            $this->sendInvitationForDealAgent($counterDealAgent->getEmail(), $param);
        }
        //проставляем дату приглашения к сделке или обновляем если приглашение повторно
        $counterDealAgent->setInvitationDate(new \DateTime());
        $this->entityManager->persist($counterDealAgent);
        $this->entityManager->flush($counterDealAgent);
    }

    /**
     * @param string $email
     * @param array $param
     * @param int $recursion_level
     * @return bool
     * @throws LogicException
     */
    private function sendInvitationForDealAgent(string $email, array $param, $recursion_level = 0): bool
    {
        if ($recursion_level >= self::MAX_RECURSION_LEVEL) {

            throw new LogicException(null, LogicException::DEAL_INVITATION_SENDING_FAIL);
        }

        if (false === $this->dealInvitationTokenProvider->send($email, $param)) {
            //// если отправка провалилась запускаем повторную ////
            return $this->sendInvitationForDealAgent($email, $param, ++$recursion_level);
        }

        return true;
    }


    /**
     * Уведомление Customer'а о завершении сделки
     *
     * @param Deal $deal
     * @throws \Throwable
     */
    public function notifyCustomerAboutDealClosed(Deal $deal)
    {
        try {
            $email = $deal->getCustomer()->getEmail();
            $data = [
                'deal' => $deal,
                'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
                'counter_deal_agent_role' => 'CONTRACTOR',
                DealNotificationSender::TYPE_NOTIFY_KEY => DealNotificationSender::DEAL_CLOSED_FOR_CUSTOMER,
            ];

            $this->dealNotificationSender->sendMail($email, $data);
        }
        catch (\Throwable $t) {

            throw $t;
        }
    }

    /**
     * Уведомление Contractor'а о завершении сделки
     *
     * @param Deal $deal
     * @throws \Throwable
     */
    public  function notifyContractorAboutDealClosed(Deal $deal)
    {
        try {
            $email = $deal->getContractor()->getEmail();
            $data = [
                'deal' => $deal,
                'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
                'counter_deal_agent_role' => 'CUSTOMER',
                DealNotificationSender::TYPE_NOTIFY_KEY => DealNotificationSender::DEAL_CLOSED_FOR_CONTRACTOR,
            ];

            $this->dealNotificationSender->sendMail($email, $data);
        }
        catch (\Throwable $t) {

            throw $t;
        }
    }

    /**
     * @param DealAgent $dealAgent
     * @param array $params
     * @throws \Throwable
     */
    public function registrationAfterCreateDealForOwner(DealAgent $dealAgent, array $params)
    {
        try {
            if ($this->email_notify_to_invitation) {
                $params['deal_invitation_type'] =  DealInvitationSender::REGISTRATION_AFTER_CREATE_DEAL_FOR_OWNER;

                $this->sendInvitationForDealAgent($dealAgent->getEmail(), $params);
            }

            //проставляем дату приглашения к сделке или обновляем если приглашение повторно
            $dealAgent->setInvitationDate(new \DateTime());
            $this->entityManager->persist($dealAgent);
            $this->entityManager->flush($dealAgent);
        }
        catch (\Throwable $t) {
            logException($t, 'email-error');
            throw $t;
        }
    }

    /**
     * @param DealAgent $dealAgent
     * @param array $params
     * @throws \Throwable
     */
    public function registrationAfterCreateDealForCounterAgent(DealAgent $dealAgent, array $params)
    {
        try {
            if ($this->email_notify_to_invitation) {
                $params['deal_invitation_type'] =  DealInvitationSender::REGISTRATION_AFTER_CREATE_DEAL_FOR_COUNTER_AGENT;

                $this->sendInvitationForDealAgent($dealAgent->getEmail(), $params);
            }

            //проставляем дату приглашения к сделке или обновляем если приглашение повторно
            $dealAgent->setInvitationDate(new \DateTime());
            $this->entityManager->persist($dealAgent);
            $this->entityManager->flush($dealAgent);
        }
        catch (\Throwable $t) {
            logException($t, 'email-error');
            throw $t;
        }
    }

    /**
     * @param DealAgent $dealAgent
     * @param array $params
     * @throws \Throwable
     */
    public function invitationToDealForOwner(DealAgent $dealAgent, array $params)
    {
        try {
            if ($this->email_notify_to_invitation) {
                $params['deal_invitation_type'] =  DealInvitationSender::DEAL_OWNER_NOTIFICATION;

                $this->sendInvitationForDealAgent($dealAgent->getEmail(), $params);
            }

            //проставляем дату приглашения к сделке или обновляем если приглашение повторно
            $dealAgent->setInvitationDate(new \DateTime());
            $this->entityManager->persist($dealAgent);
            $this->entityManager->flush($dealAgent);
        }
        catch (\Throwable $t) {
            logException($t, 'email-error');
            throw $t;
        }
    }

    /**
     * @param DealAgent $dealAgent
     * @param array $params
     * @throws \Throwable
     */
    public function invitationToDealForCounterAgent(DealAgent $dealAgent, array $params)
    {
        try {
            if ($this->email_notify_to_invitation) {
                $params['deal_invitation_type'] =  DealInvitationSender::DEAL_NOTIFICATION;

                $this->sendInvitationForDealAgent($dealAgent->getEmail(), $params);
            }

            //проставляем дату приглашения к сделке или обновляем если приглашение повторно
            $dealAgent->setInvitationDate(new \DateTime());
            $this->entityManager->persist($dealAgent);
            $this->entityManager->flush($dealAgent);
        }
        catch (\Throwable $t) {
            logException($t, 'email-error');
            throw $t;
        }
    }
}