<?php
namespace Application\Service\Notification\Factory;

use Application\Service\Notification\SmsNotificationManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class SmsNotificationManagerFactory
 * @package Application\Service\Notification\Factory
 */
class SmsNotificationManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return SmsNotificationManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        return new SmsNotificationManager($config);
    }
}