<?php
namespace Application\Service\Notification\Factory;

use Application\Provider\Mail\DealInvitationSender;
use Application\Provider\Mail\DealNotificationSender;
use Application\Provider\Token\DealInvitationTokenProvider;
use Application\Service\Notification\EmailNotificationManager;
use Application\Service\TokenManager;
use Core\Service\TwigRenderer;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class EmailNotificationManagerFactory
 * @package Application\Service\Notification\Factory
 */
class EmailNotificationManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return EmailNotificationManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $twigRenderer = $container->get(TwigRenderer::class);
        $dealNotificationSender = new DealNotificationSender($twigRenderer, $config);
        $dealInvitationSender = new DealInvitationSender($twigRenderer, $config);
        $tokenManager = $container->get(TokenManager::class);
        $dealInvitationTokenProvider = new DealInvitationTokenProvider($dealInvitationSender, $tokenManager);
        $entityManager  = $container->get('doctrine.entitymanager.orm_default');


        return new EmailNotificationManager(
            $dealInvitationTokenProvider,
            $dealNotificationSender,
            $entityManager,
            $config
        );
    }
}