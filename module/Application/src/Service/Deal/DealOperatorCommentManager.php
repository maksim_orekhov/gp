<?php

namespace Application\Service\Deal;

use Application\Entity\Deal;
use Application\Entity\User;
use Application\Form\DealCommentForm;
use ModuleMessage\Entity\Message;
use ModuleMessage\Service\MessageManager;
use Application\Entity\Role;
use Application\Service\CommentManager;

/**
 * Class DealOperatorCommentManager
 * @package Application\Service\Deal
 */
class DealOperatorCommentManager extends CommentManager
{
    /**
     * DealCommentManager constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param MessageManager $messageManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                MessageManager $messageManager)
    {
        parent::__construct($entityManager, $messageManager);
    }


    /**
     * @param Deal $deal
     * @param null $currentUser
     * @return array
     */
    public function getDealOperatorsCommentsOutput(Deal $deal, $currentUser = null)
    {
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(['name' => 'Operator']);

        // @TODO Возможно, лучший вариант - отфильтровать $deal->getMessages (выбрать из него только комменты Операторов)
        $comments = $this->entityManager->getRepository(Message::class)
            ->getDealOperatorsComments($deal, $role);

        $commentOutput = [];

        if ($comments) {
            $commentOutput = $this->getCommentCollectionOutput($comments, $currentUser);
        }

        return $commentOutput;
    }

    /**
     * @param User $author
     * @param array $data
     * @param Deal $deal
     * @return Message
     * @throws \Exception
     */
    public function createDealComment(User $author, array $data, Deal $deal)
    {
        try {
            /** @var Message $comment */
            $comment = $this->addComment($data['text'], $author);
        }
        catch (\Throwable $t) {
            // Write log
            logMessage('Creation error: ' . $t->getMessage(), 'deal-comments');

            throw new \Exception($t->getMessage());
        }

        $deal->addMessage($comment);

        // @TODO Проверить не сработает ли слушатель при изменении, связанном со сделкой
        $this->entityManager->flush($deal);

        return $comment;
    }

    /**
     * @param Message $comment
     * @throws \Exception
     */
    public function removeDealComment(Message $comment)
    {
        /** @var Deal $deal */
        $deal = $this->getDealByComment($comment);

        // @TODO Возможно трансакция. Если не удалось удалить комментарий из БД, то и не удалять из коллекции
        // Remove comment from collection
        $deal->removeMessage($comment);

        try {
            // Try to remove comment from DB // Can throw Exception
            $this->messageManager->removeMessage($comment);
        }
        catch (\Throwable $t) {
            // Write log
            logMessage('Removal error: ' . $t->getMessage(), 'deal-comments');

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param Message $comment
     * @return Deal
     */
    public function getDealByComment(Message $comment)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->getDealByComment($comment);

        return $deal;
    }

    /**
     * @param DealCommentForm $form
     * @param Message $comment
     * @param Deal $deal
     * @return DealCommentForm
     */
    public function setDealCommentFormData(DealCommentForm $form, Message $comment, Deal $deal)
    {
        $form->setData([
            'text'      => $comment->getText(),
            'deal_id'   => $deal->getId(),
        ]);

        return $form;
    }
}