<?php

namespace Application\Service\Deal;

use Application\Entity\Deal;
use Application\Entity\Discount;
use Application\Entity\Dispute;
use Application\Entity\NdsType;
use Application\Entity\Payment;
use Application\Entity\DealAgent;
use Application\Entity\PaymentMethod;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\RepaidOverpay;
use Application\Service\Payment\CalculatedDataProvider;
use Application\Entity\CivilLawSubject;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Application\Entity\NaturalPerson;
use Application\Entity\LegalEntity;
use Application\Entity\Refund;
use Application\Service\NdsTypeManager;
use Application\Service\SettingManager;
use ModulePaymentOrder\Entity\PaymentTransactionInterface;
use Application\Form\NaturalPersonForm;
use Application\Form\LegalEntityForm;
use Application\Form\PaymentMethodByCivilLawSubjectForm;
use Application\Form\Fieldset\PaymentMethodBankTransferFieldset;
use Application\Service\HydratorManager;
use Zend\Form\Form;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Deal\Status\DebitMatchedStatus;

/**
 * Class DealPolitics
 * @package Application\Service\Deal
 */
class DealPolitics extends HydratorManager
{
    const NAME_POLITICS_IN_SETTING = 'min_delivery_period';

    const ERROR_CIVIL_LAW_SUBJECT_NOT_SELECTED  = 'Не выбран субъект права по сделке';
    const ERROR_PAYMENT_METHOD_NOT_SELECTED     = 'Не выбран метод получения денег по сделке';
    const ERROR_PAYMENT_METHOD_NOT_VALID        = 'Выбран некорректный метод оплаты';
    const ERROR_CIVIL_LAW_SUBJECT_NOT_VALID     = 'Выбран некорректный субъект права по сделке';

    /**
     * @var \Application\Service\SettingManager
     */
    private $settingManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * @var DebitMatchedStatus
     */
    private $debitMatchedStatus;

    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider;

    /**
     * DealPolitics constructor.
     * @param SettingManager $settingManager
     * @param PaymentManager $paymentManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param NdsTypeManager $ndsTypeManager
     * @param DebitMatchedStatus $debitMatchedStatus
     * @param CalculatedDataProvider $calculatedDataProvider
     */
    public function __construct(SettingManager $settingManager,
                                PaymentManager $paymentManager,
                                PaymentMethodManager $paymentMethodManager,
                                NdsTypeManager $ndsTypeManager,
                                DebitMatchedStatus $debitMatchedStatus,
                                CalculatedDataProvider $calculatedDataProvider)
    {
        $this->settingManager       = $settingManager;
        $this->paymentManager       = $paymentManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->ndsTypeManager      = $ndsTypeManager;
        $this->debitMatchedStatus  = $debitMatchedStatus;
        $this->calculatedDataProvider = $calculatedDataProvider;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealConfirmedByDealAgents(Deal $deal): bool
    {
        if ($deal->getCustomer()->getDealAgentConfirm() && $deal->getContractor()->getDealAgentConfirm()) {

            return true;
        }

        return false;
    }

    /**
     * Проверяет, готова ли сделка для включения OUTGOING платёжек по ней в генерируемый файл на выплату, возврат и скидку
     *
     * @param array $payment_transaction
     * @return bool
     */
    public function isOutgoingPaymentOrdersReadyToPay(array $payment_transaction): bool
    {
        /** @var Deal $deal */
        $deal = $payment_transaction['deal'];
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Discount $discount */
        $discount = $dispute ? $dispute->getDiscount() : null;
        /** @var Refund $refund */
        $refund = $dispute ? $dispute->getRefund() : null;
        /** @var RepaidOverpay $repaidOverpay */
        $repaidOverpay = $payment->getRepaidOverpay();

        // ВОЗВРАТ
        if ($refund) {

            return $this->isRefundCorrect($payment, $payment_transaction);
        }

        // ВЫПЛАТА и СКИДКА
        if ($discount) {

            return $this->isOutgoingPaymentWithDiscountCorrect($payment, $payment_transaction);
        }

        // ВОЗВРАТ ПЕРЕПЛАТЫ
        if ($repaidOverpay && isset($payment_transaction['outgoing_transactions']['repaid_overpay'])) {

            return $this->isOutgoingRepaidOverpayCorrect(
                $repaidOverpay,
                $payment_transaction['outgoing_transactions']['repaid_overpay']);
        }

        // ТОЛЬКО ВЫПЛАТА
        return $this->isOutgoingPaymentCorrect($deal, $payment_transaction['outgoing_transactions']['payment']);
    }

    /**
     * @param Deal $deal
     * @param PaymentTransactionInterface $outgoingPaymentTransaction
     * @return bool
     */
    private function isOutgoingPaymentCorrect(Deal $deal, PaymentTransactionInterface $outgoingPaymentTransaction): bool
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $calculated_released_value = $this->calculatedDataProvider
            ->getCalculatedReleasedValue($payment->getDealValue(), $deal->getFeePayerOption());

        if ($outgoingPaymentTransaction->getAmount() === $calculated_released_value) {

            return true;
        }

        return false;
    }

    /**
     * @param RepaidOverpay $repaidOverpay
     * @param PaymentTransactionInterface $paymentTransaction
     * @return bool
     */
    private function isOutgoingRepaidOverpayCorrect(RepaidOverpay $repaidOverpay,
                                                    PaymentTransactionInterface $paymentTransaction)
    {
        if ($paymentTransaction->getAmount() === $repaidOverpay->getAmount()) {

            return true;
        }

        return false;
    }

    /**
     * @param Payment $payment
     * @param $payment_transaction
     * @return bool
     */
    private function isOutgoingPaymentWithDiscountCorrect(Payment $payment, $payment_transaction): bool
    {
        /** @var PaymentTransactionInterface $discountPaymentTransaction */
        $discountPaymentTransaction = $payment_transaction['outgoing_transactions']['discount'];
        /** @var PaymentTransactionInterface $paymentPaymentTransaction */
        $paymentPaymentTransaction = $payment_transaction['outgoing_transactions']['payment'];

        $total_outgoing_amount = $discountPaymentTransaction->getAmount() + $paymentPaymentTransaction->getAmount();

        $maximum_discount_amount = $this->calculatedDataProvider
            ->getMaxDiscountAmount($payment, $payment_transaction['total_incoming_amount']);

        if ($total_outgoing_amount >= $maximum_discount_amount && $total_outgoing_amount === $payment->getReleasedValue()) {

            return true;
        }

        return false;
    }

    /**
     * @param $payment_transaction
     * @return bool
     */
    private function isOutgoingPaymentWithRepaidOverpayCorrect($payment_transaction): bool
    {
        /** @var PaymentTransactionInterface $repaidOverpayPaymentTransaction */
        $repaidOverpayPaymentTransaction = $payment_transaction['outgoing_transactions']['repaid_overpay'];
        $expected_repaid_overpay = $payment_transaction['total_incoming_amount_without_overpay'] - $payment_transaction['total_incoming_amount'];

        return $expected_repaid_overpay === $repaidOverpayPaymentTransaction->getAmount();
    }

    /**
     * @param Payment $payment
     * @param $payment_transaction
     * @return bool
     */
    private function isRefundCorrect(Payment $payment, $payment_transaction): bool
    {
        /** @var PaymentTransactionInterface $refundPaymentTransaction */
        $refundPaymentTransaction = $payment_transaction['outgoing_transactions']['refund'];

        $calculated_refund_amount = $this->calculatedDataProvider
            ->getCalculatedRefundAmount($payment, $payment_transaction['total_incoming_amount']);

        // Должны вернуть Кастомеру не больше того, что от него получено за вычетом комиссии
        if ($refundPaymentTransaction->getAmount() === $calculated_refund_amount) {

            return true;
        }

        return false;
    }

    /**
     * Проверка наличия необходимых данных для подтверждения условий сделки со стороны агента
     *
     * @param Payment $payment
     * @param DealAgent $userAgent
     * @param array $deal_data
     * @return bool
     * @throws \Exception
     */
    public function isDealReadyForConfirmation(Payment $payment, DealAgent $userAgent, array $deal_data): bool
    {
        // If CivilLawSubject is not selected
        if(!$userAgent->getCivilLawSubject() ) {

            throw new \Exception(self::ERROR_CIVIL_LAW_SUBJECT_NOT_SELECTED);

        }

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $userAgent->getCivilLawSubject();
        // If Owner of civilLawSubject and DealAgent is not the same user
        if($userAgent->getUser() !== $civilLawSubject->getUser()) {

            throw new \Exception(self::ERROR_CIVIL_LAW_SUBJECT_NOT_VALID);
        }

        // If civilLawSubject - NaturalPerson
        if($civilLawSubject->getNaturalPerson()) {
            try {
                // Check if NaturalPerson data is valid // Can throw Exception
                $this->isNaturalPersonValid($civilLawSubject->getNaturalPerson(), $deal_data['csrf']);
            }
            catch (\Throwable $t) {
                throw new \Exception($t->getMessage());
            }
        }

        // If civilLawSubject - LegalEntity
        if($civilLawSubject->getLegalEntity()) {
            try {
                // Check if LegalEntity data is valid // Can throw Exception
                $this->isLegalEntityValid($civilLawSubject->getLegalEntity(), $deal_data['csrf']);
            }
            catch (\Throwable $t) {
                throw new \Exception($t->getMessage());
            }
        }

        // Only for Contractor
        if($deal_data['deal_role'] === 'contractor') {

            // If PaymentMethod is not selected
            if(!$payment->getPaymentMethod()) {

                throw new \Exception(self::ERROR_PAYMENT_METHOD_NOT_SELECTED);

            }

            /** @var PaymentMethod $paymentMethod */
            $paymentMethod = $payment->getPaymentMethod();

            // If Owner of paymentMethod and DealAgent is not the same user
            if($userAgent->getUser() !== $paymentMethod->getCivilLawSubject()->getUser()) {

                throw new \Exception(self::ERROR_PAYMENT_METHOD_NOT_VALID);
            }

            // If PaymentMethodType - bank_transfer
            if($paymentMethod->getPaymentMethodType()->getName() === 'bank_transfer') {
                try {
                    // Check if PaymentMethodBankTransfer data is valid // Can throw Exception
                    $this->isPaymentMethodBankTransferValid($paymentMethod->getPaymentMethodBankTransfer(), $deal_data['csrf']);
                }
                catch (\Throwable $t) {

                    throw new \Exception($t->getMessage());
                }
            }

            // #TODO Other Payment Methods
        }

        return true;
    }

    /**
     * @return string|null
     * @throws \Exception
     */
    public function getMinDeliveryPeriod()
    {
        $minDeliveryPeriod = $this->settingManager->getValueByNameGlobalSetting(self::NAME_POLITICS_IN_SETTING);

        if ($minDeliveryPeriod === null){
            throw new \Exception('Politic min delivery period not found');
        }
        return $minDeliveryPeriod;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealDisputable(Deal $deal): bool
    {
        // Спор возможно только при статусе "Оплачено"
        if ($this->debitMatchedStatus->isDealInStatus($deal)) {

            return true;
        }

        return false;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealHasOpenDispute(Deal $deal)
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        if (!$dispute) {

            return false;
        }

        return !$dispute->isClosed();
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealHasTribunalStatus(Deal $deal)
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        if ($dispute) {
            $tribunal = $dispute->getTribunal();

            return (bool)$tribunal;
        }

        return false;
    }

    /**
     * Check id Deal is abnormal (проблемная сделка)
     *
     * @param Deal $deal
     * @param Payment|null $payment
     * @return bool
     * @throws \Exception
     *
     * @TODO Используевание осталось только в тестах! Убрать и удалить!
     */
    public function isDealAbnormal(Deal $deal, Payment $payment=null)
    {
        if (!$payment) {
            /** @var Payment $payment */
            $payment = $this->paymentManager->getPaymentByDeal($deal);
        }

        if(!$deal->getDateOfConfirm()) {

            throw new \Exception('The Deal is not confirmed yet');
        }

        if(!$payment->getDebitPaymentOrder()) {

            throw new \Exception('Payment does not have DebitPaymentOrder yet');
        }

        // Convert string to Datetime object
        $dateOfDebit = \DateTime::createFromFormat('d.m.Y', $payment->getDebitPaymentOrder()->getDateOfDebit());
        $dateOfDealConfirm = $deal->getDateOfConfirm(); // Чтобы не модифицировать значение свойства в объекте.
        // BankClientPaymentOrder.dateOfDebit > (Deal.dateOfConfirm + Deal.deliveryPeriod)
        if ($dateOfDebit > $dateOfDealConfirm->modify('+'.$deal->getDeliveryPeriod().' days')) {

            return true;
        }

        // В BankClientPaymentOrder поступившая сумма не соответствует сумме expectedValue в Payment
        if ($payment->getDebitPaymentOrder()->getAmount() != $payment->getExpectedValue()) {

            return true;
        }

        return false;
    }

    /**
     * @param NaturalPerson $naturalPerson
     * @param string $form_csrf
     * @return bool
     * @throws \Exception
     */
    private function isNaturalPersonValid(NaturalPerson $naturalPerson, string $form_csrf)
    {
        $data = $this->extract($naturalPerson);
        $data['csrf'] = $form_csrf;
        $data['birth_date'] = $naturalPerson->getBirthDate()->format('Y-m-d');

        // Create NaturalPersonForm for Validation purpose
        $form = new NaturalPersonForm();
        // Set data
        $form->setData($data);
        // Try validate
        try {
            // Can throw Exception
            $this->validateFormData($form, 'Выбран субъект права по сделке - Физическое лицо.');
        }
        catch (\Exception $t) {
            throw new \Exception($t->getMessage());
        }

        return true;
    }

    /**
     * @param LegalEntity $legalEntity
     * @param string $form_csrf
     * @return bool
     * @throws \Exception
     */
    private function isLegalEntityValid(LegalEntity $legalEntity, string $form_csrf): bool
    {
        $data = $this->extract($legalEntity);

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $data['civil_law_subject'];
        /** @var NdsType $ndsType */
        $ndsType = $civilLawSubject->getNdsType();
        $data['csrf'] = $form_csrf;
        $data['nds_type'] = ($ndsType) ? $ndsType->getType() : 0;

        $ndsTypes = $this->ndsTypeManager->getNdsTypes();
        // Create LegalEntityForm for Validation purpose
        $form = new LegalEntityForm($ndsTypes);
        // Set data
        $form->setData($data);
        // Try validate
        try {
            // Can throw Exception
            $this->validateFormData($form, 'Выбран субъект права по сделке - Юридическое лицо.');
        }
        catch (\Exception $t) {
            throw new \Exception($t->getMessage());
        }

        return true;
    }

    /**
     * @param PaymentMethodBankTransfer $paymentMethodBankTransfer
     * @param string $form_csrf
     * @return bool
     * @throws \Exception
     */
    private function isPaymentMethodBankTransferValid(PaymentMethodBankTransfer $paymentMethodBankTransfer, string $form_csrf)
    {
        $data['bank_transfer'] = $this->extract($paymentMethodBankTransfer);
        $data['csrf'] = $form_csrf;
        $data['payment_method_type_id'] = $paymentMethodBankTransfer->getPaymentMethod()->getPaymentMethodType()->getId();
        $data['civil_law_subject_id'] = $paymentMethodBankTransfer->getPaymentMethod()->getCivilLawSubject()->getId();

        // Select all Payment Methods
        $paymentMethods = $this->paymentMethodManager->findTypes();
        // Payment Method type name for Bank Transfer
        $keyTypeData = PaymentMethodType::BANK_TRANSFER;
        // Create PaymentMethodByCivilLawSubjectForm for Validation purpose
        $form = new PaymentMethodByCivilLawSubjectForm(
            $paymentMethodBankTransfer->getPaymentMethod()->getCivilLawSubject()->getId(),
            $paymentMethods,
            [$keyTypeData => [
                'type' => PaymentMethodBankTransferFieldset::class,
                'count' => 1,
            ]]
        );
        // Set data
        $form->setData($data);
        // Try validate
        try {
            // Can throw Exception
            $this->validateFormData($form, 'Выбран метод получения денег по сделке.');
        }
        catch (\Exception $t) {
            throw new \Exception($t->getMessage());
        }

        return true;
    }

    /**
     * @param Form $form
     * @param $entity_type_message
     * @return bool
     * @throws \Exception
     */
    private function validateFormData(Form $form, $entity_type_message)
    {
        if( !$form->isValid() ) {

            if(array_key_exists('bank_transfer', $form->getMessages())) {
                $messages = $form->getMessages()['bank_transfer'];
            } else {
                $messages = $form->getMessages();
            }

            $message = $entity_type_message . "<br />";
            foreach ($messages as $key => $value) {
                $message .= $key . " (";
                foreach ($value as $m) {
                    $message .= $m . " / ";
                }
                // Remove last " / " and close bracket
                $message = substr($message, 0, -3) . ")<br />";
            }
            throw new \Exception($message);
        }

        return true;
    }

    /**
     * @param DealAgent $dealAgent
     * @return bool
     */
    public function isCorrectCivilLawSubjectForDealAgent(DealAgent $dealAgent): bool
    {
        $civilLawSubject = $dealAgent->getCivilLawSubject();
        $naturalPerson = $civilLawSubject ? $civilLawSubject->getNaturalPerson() : null;
        $soleProprietor = $civilLawSubject ? $civilLawSubject->getSoleProprietor() : null;
        $legalEntity = $civilLawSubject ? $civilLawSubject->getLegalEntity() : null;

        if ($legalEntity !== null || $naturalPerson !== null || $soleProprietor !== null) {

            return true;
        }

        return false;
    }
}