<?php

namespace Application\Service\Deal;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\Token;
use Application\Entity\User;
use Application\Form\DealPartnerTokenForm;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;

/**
 * Class DealTokenManager
 * @package Application\Service\Deal
 */
class DealTokenManager
{
    const MAX_RECURSION_LEVEL = 20;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var DealTypeManager
     */
    private $dealTypeManager;

    /**
     * @var DealTokenManagerPolitics
     */
    private $dealTokenManagerPolitics;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var DealStatus
     */
    private $dealStatus;

    /**
     * DealTokenManager constructor.
     * @param EntityManager $entityManager
     * @param DealTypeManager $dealTypeManager
     * @param DealTokenManagerPolitics $dealTokenManagerPolitics
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param PaymentMethodManager $paymentMethodManager
     */
    public function __construct(EntityManager $entityManager,
                                DealTypeManager $dealTypeManager,
                                DealTokenManagerPolitics $dealTokenManagerPolitics,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                PaymentMethodManager $paymentMethodManager,
                                DealStatus $dealStatus)
    {
        $this->entityManager    = $entityManager;
        $this->dealTypeManager  = $dealTypeManager;
        $this->dealTokenManagerPolitics = $dealTokenManagerPolitics;
        $this->civilLawSubjectManager = $civilLawSubjectManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->dealStatus = $dealStatus;
    }

    /**
     * @return DealTokenManagerPolitics
     */
    public function getDealTokenManagerPolitics(): DealTokenManagerPolitics
    {
        return $this->dealTokenManagerPolitics;
    }

    /**
     * @return array
     */
    public function getAllTokens()
    {
        return $this->entityManager->getRepository(Token::class)->findAll();
    }

    /**
     * @param User $user
     * @param bool $return_as_array
     * @return array
     */
    public function getUsersCounteragentTokens(User $user, $return_as_array = false): array
    {
        $tokens = $this->entityManager->getRepository(Token::class)
            ->selectNotDeletedUsersTokens($user);

        if (!$return_as_array) {

            return $tokens;
        }

        return $this->getTokensArrayOutput($tokens);
    }

    /**
     * @param int $id
     * @return null|object
     */
    public function getToken(int $id)
    {
        return $this->entityManager->getRepository(Token::class)->find($id);
    }


    /**
     * @param string $key
     * @return Token
     */
    public function getTokenByKey(string $key)
    {
        /** @var Token $token */
        $token = $this->entityManager->getRepository(Token::class)
            ->findOneBy(['tokenKey' => trim($key)]);

        return $token;
    }

    /**
     * @param Token $token
     * @return array
     * @throws LogicException
     * @TODO Не Output ли это? Исключить использование!
     */
    public function getDataFromToken(Token $token): array
    {
        $civil_law_subject = $token->getCounteragentCivilLawSubject() ?? null;
        $civil_law_subject_id = null;
        $civil_law_subject_name = null;
        $counteragent_email = null;
        if($civil_law_subject) {
            $civil_law_subject_id = $civil_law_subject ? $civil_law_subject->getId() : null;
            $civil_law_subject_name = $civil_law_subject ? $this->civilLawSubjectManager->getCivilLawSubjectName($civil_law_subject) : null;
            $counteragent_email = $token->getCounteragentUser() ? $token->getCounteragentUser()->getEmail()->getEmail() : null;
        } else {
            $counteragent_email = $token->getBeneficiarCivilLawSubject() ? $token->getCounteragentEmail() : null;
        }

        $data = [
            'deal_role' => mb_strtolower($token->getDealRole()),
            'deal_type' => $token->getDealType() ? $token->getDealType()->getId() : null,
            'amount' => $token->getAmount() ?? null,
            'fee_payer_option' => $token->getFeePayerOption() ? $token->getFeePayerOption()->getId() : null,
            'delivery_period' => $token->getDeliveryPeriod() ?? null,
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $civil_law_subject_id,
            'counteragent_name' => $civil_law_subject_name,
            'partner_ident' => $token->getPartnerIdent(),
            'deal_description' => $token->getDealDescription(),
            'deal_name' => $token->getDealName(),
        ];

        return $data;
    }

    /**
     * @param array $tokens
     * @return array
     */
    public function getTokensArrayOutput(array $tokens)
    {
        $tokensOutput = [];

        /** @var Token $token */
        foreach ($tokens as $token) {
            $tokensOutput[$token->getId()] = $this->getTokenOutput($token);
        }

        return $tokensOutput;
    }

    /**
     * @param Token $token
     * @return array
     */
    public function getTokenOutput(Token $token): array
    {
        /** @var DealType $dealType */
        $dealType = $token->getDealType();
        $tokenOutput = [];
        $tokenOutput['id']              = $token->getId();
        $tokenOutput['name']            = $token->getName();
        $tokenOutput['deal_role']       = $token->getDealRole() ? mb_strtolower($token->getDealRole()): null;
        $tokenOutput['partner_ident']   = $token->getPartnerIdent();
        $tokenOutput['created']         = $token->getCreated()->format('d.m.Y');
        $tokenOutput['is_active']       = $token->isActive();
        $tokenOutput['deal_type']       = $dealType ? $dealType->getIdent() : null;
        $tokenOutput['deal_name']       = $token->getDealName();
        $tokenOutput['deal_description'] = $token->getDealDescription();
        $tokenOutput['amount']          = $token->getAmount();
        $tokenOutput['delivery_period'] = $token->getDeliveryPeriod();

        $tokenOutput['fee_payer_option'] = null;
        if (null !== $token->getFeePayerOption()) {
            $tokenOutput['fee_payer_option'] = $token->getFeePayerOption()->getId();
        }
        $tokenOutput['token_key']       = $token->getTokenKey();

        $tokenOutput['beneficiar_user'] = null;
        if (null !== $token->getBeneficiarUser()) {
            /** @var User $beneficiarUser */
            $beneficiarUser = $token->getBeneficiarUser();
            $tokenOutput['beneficiar_user'] = [
                'id' => $beneficiarUser->getId(),
                'login' => $beneficiarUser->getLogin(),
            ];
        }

        $tokenOutput['beneficiar_civil_law_subject'] = null;
        if (null !== $token->getBeneficiarCivilLawSubject()) {
            /** @var CivilLawSubject $beneficiarCivilLawSubject */
            $beneficiarCivilLawSubject = $token->getBeneficiarCivilLawSubject();
            $tokenOutput['beneficiar_civil_law_subject'] = $this->civilLawSubjectManager
                ->getCivilLawSubjectOutput($beneficiarCivilLawSubject);
        }

        $tokenOutput['beneficiar_payment_method'] = null;
        if (null !== $token->getBeneficiarPaymentMethod()) {
            /** @var PaymentMethod $beneficiarPaymentMethod */
            $beneficiarPaymentMethod = $token->getBeneficiarPaymentMethod();
            $tokenOutput['beneficiar_payment_method'] = $this->paymentMethodManager
                ->getPaymentMethodOutput($beneficiarPaymentMethod);
        }

        $tokenOutput['counteragent_user'] = null;
        if (null !== $token->getCounteragentUser()) {
            /** @var User $counteragentUser */
            $counteragentUser = $token->getCounteragentUser();
            $tokenOutput['counteragent_user'] = [
                'id' => $counteragentUser->getId(),
                'login' => $counteragentUser->getLogin(),
            ];
        }

        $tokenOutput['counteragent_civil_law_subject'] = null;
        if (null !== $token->getCounteragentCivilLawSubject()) {
            /** @var CivilLawSubject $counteragentCivilLawSubject */
            $counteragentCivilLawSubject = $token->getCounteragentCivilLawSubject();
            $tokenOutput['counteragent_civil_law_subject'] = $this->civilLawSubjectManager
                ->getCivilLawSubjectOutput($counteragentCivilLawSubject);
        }

        $tokenOutput['counteragent_payment_method'] = null;
        if (null !== $token->getCounteragentPaymentMethod()) {
            /** @var PaymentMethod $counteragentPaymentMethod */
            $counteragentPaymentMethod = $token->getCounteragentPaymentMethod();
            $tokenOutput['counteragent_payment_method'] = $this->paymentMethodManager
                ->getPaymentMethodOutput($counteragentPaymentMethod);
        }

        $tokenOutput['counteragent_email'] = $token->getCounteragentEmail();

        $deals_count = $token->getDeals()->count();
        $tokenOutput['deals_count'] = $deals_count;
        $tokenOutput['deals'] = [];
        if ($deals_count > 0) {
            /** @var Deal $deal */
            foreach ($token->getDeals() as $deal) {
                /** @var Payment $payment */
                $payment = $deal->getPayment();
                $tokenOutput['deals'][] = [
                    'id'        => $deal->getId(),
                    'number'    => $deal->getNumber(),
                    'name'      => $deal->getName(),
                    'amount'    => $payment->getDealValue(),
                    'status'    => $this->dealStatus->getStatus($deal)
                ];
            }
        }

        return $tokenOutput;
    }

    /**
     * @param Token $token
     * @param bool $is_active
     * @return Token
     */
    public function switchActiveDealPartnerToken(Token $token, $is_active = true)
    {
        $token->setIsActive((bool) $is_active);

        $this->entityManager->persist($token);
        $this->entityManager->flush($token);

        return $token;
    }

    /**
     * @param User $user
     * @param array $data
     * @return Token
     * @throws LogicException
     */
    public function createDealPartnerToken(User $user, array $data): Token
    {
        try {
            $token = new Token();

            if ($data['button_author_role'] === 'participant') {
                if (isset($data['counteragent_civil_law_subject'])) {
                    /** @var CivilLawSubject $civilLawSubject */
                    $civilLawSubject = $this->civilLawSubjectManager->get((int)$data['counteragent_civil_law_subject']);
                    if ($civilLawSubject) {
                        $token->setCounteragentCivilLawSubject($civilLawSubject);
                    }
                }
                if (isset($data['counteragent_payment_method'])) {
                    /** @var PaymentMethod $paymentMethod */
                    $paymentMethod = $this->paymentMethodManager->get((int)$data['counteragent_payment_method']);
                    if ($paymentMethod) {
                        $token->setCounteragentPaymentMethod($paymentMethod);
                    }
                }
                $token->setCounteragentUser($user);
            } else {
                //// beneficiar ////
                if (isset($data['beneficiar_civil_law_subject'])) {
                    /** @var CivilLawSubject $beneficiarCivilLawSubject */
                    $beneficiarCivilLawSubject = $this->civilLawSubjectManager->get((int)$data['beneficiar_civil_law_subject']);
                    if ($beneficiarCivilLawSubject) {
                        $token->setBeneficiarCivilLawSubject($beneficiarCivilLawSubject);
                    }
                }
                if (isset($data['beneficiar_payment_method'])) {
                    /** @var PaymentMethod $beneficiarPaymentMethod */
                    $beneficiarPaymentMethod = $this->paymentMethodManager->get((int)$data['beneficiar_payment_method']);
                    if ($beneficiarPaymentMethod) {
                        $token->setBeneficiarPaymentMethod($beneficiarPaymentMethod);
                    }
                }
                $token->setBeneficiarUser($user);
                //// end beneficiar ////
            }
            if (isset($data['fee_payer_option'])) {
                /** @var FeePayerOption $feePayerOption */
                $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
                    ->find((int)$data['fee_payer_option']);
                if ($feePayerOption) {
                    $token->setFeePayerOption($feePayerOption);
                }
            }
            if (isset($data['deal_type'])) {
                /** @var DealType $dealType */
                $dealType = $this->dealTypeManager->getDealTypeById((int)$data['deal_type']);
                if ($dealType) {
                    $token->setDealType($dealType);
                }
            }

            $token->setName($data['name']);
            if (isset($data['slug'])) {
                $token->setTokenKey($data['slug']);
            }else{
                $token->setTokenKey($this->generateUniqueTokenKey());
            }
            $token->setDealName($data['deal_name']);
            $token->setDealDescription($data['deal_description']);
            $token->setDealRole($data['deal_role']);
            $token->setCounteragentEmail($data['counteragent_email']);
            $token->setIsActive($data['is_active']);
            $token->setAmount($data['amount']);
            $token->setDeliveryPeriod((int)$data['delivery_period']);
            $token->setCreated(new \DateTime());

            $this->entityManager->persist($token);
            $this->entityManager->flush($token);
        }
        catch (\Throwable $t) {
            logMessage($t->getMessage(), 'deal-partner-token');
            throw new LogicException(null, LogicException::DEAL_TOKEN_CREATION_FAILED);
        }

        return $token;
    }

    /**
     * @param User $user
     * @param Token $token
     * @param array $data
     * @throws LogicException
     */
    public function editDealPartnerToken(User $user, Token $token, array $data)
    {
        try {
            /**
             * @var CivilLawSubject $civilLawSubject
             * @var PaymentMethod $paymentMethod
             * @var FeePayerOption $feePayerOption
             * @var DealType $dealType
             */
            if ($token->getCounteragentUser() === $user) {
                $civilLawSubject = $this->civilLawSubjectManager->get($data['counteragent_civil_law_subject'] ?? 0);
                $paymentMethod = $this->paymentMethodManager->get($data['counteragent_payment_method'] ?? 0);

                $token->setCounteragentCivilLawSubject($civilLawSubject ?? null);
                $token->setCounteragentPaymentMethod($paymentMethod ?? null);
            } elseif ($token->getBeneficiarUser() === $user){
                $civilLawSubject = $this->civilLawSubjectManager->get($data['beneficiar_civil_law_subject'] ?? 0);
                $paymentMethod = $this->paymentMethodManager->get($data['beneficiar_payment_method'] ?? 0);

                $token->setBeneficiarCivilLawSubject($civilLawSubject ?? null);
                $token->setBeneficiarPaymentMethod($paymentMethod ?? null);
            }else{

                throw new LogicException(null, LogicException::DEAL_TOKEN_BUTTON_AUTHOR_ROLE_NOT_DEFINED);
            }
            //set fee payer option
            $fee_payer_option_id = $data['fee_payer_option'] ?? 0;
            $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
                ->find((int) $fee_payer_option_id);
            $token->setFeePayerOption($feePayerOption ?? null);

            $dealType = $this->dealTypeManager->getDealTypeById($data['deal_type'] ?? 0);
            $token->setDealType($dealType ?? null);

            $token->setName($data['name']);
            $token->setDealName($data['deal_name']);
            $token->setDealDescription($data['deal_description']);
            $token->setDealRole($data['deal_role']);
            $token->setPartnerIdent($data['partner_ident'] ?? null);
            $token->setCounteragentEmail($data['counteragent_email']);
            $token->setIsActive($data['is_active']);
            $token->setAmount($data['amount'] ?? null);
            $token->setDeliveryPeriod($data['delivery_period'] ? (int)$data['delivery_period'] : null);

            $this->entityManager->persist($token);
            $this->entityManager->flush($token);
        }
        catch (\Throwable $t) {
            logMessage($t->getMessage(), 'deal-partner-token');
            throw new LogicException(null, LogicException::DEAL_TOKEN_UPDATE_FAILED);
        }
    }

    /**
     * @param DealPartnerTokenForm $form
     * @param Token $token
     * @param $is_operator
     * @return DealPartnerTokenForm
     */
    public function setFormData(DealPartnerTokenForm $form, Token $token, $is_operator): DealPartnerTokenForm
    {
        $data = [
            'name' => $token->getName(),
            'deal_role' => $token->getDealRole(),
            'is_active' => $token->isActive(),
            'counteragent_civil_law_subject' => $token->getCounteragentCivilLawSubject()
                ? $token->getCounteragentCivilLawSubject()->getId()
                : null,
            'counteragent_payment_method' => $token->getCounteragentPaymentMethod()
                ? $token->getCounteragentPaymentMethod()->getId()
                : null,
            'counteragent_email' => $token->getCounteragentEmail(),
            'beneficiar_civil_law_subject' => $token->getBeneficiarCivilLawSubject()
                ? $token->getBeneficiarCivilLawSubject()->getId()
                : null,
            'beneficiar_payment_method' => $token->getBeneficiarPaymentMethod()
                ? $token->getBeneficiarPaymentMethod()->getId()
                : null,
            'deal_type' => $token->getDealType()
                ? $token->getDealType()->getId()
                : null,
            'deal_name' => $token->getDealName(),
            'deal_description' => $token->getDealDescription(),
            'amount' => $token->getAmount(),
            'delivery_period' => $token->getDeliveryPeriod(),
            'slug' => $token->getTokenKey(),
        ];

        if ($is_operator) {
            $data ['partner_ident'] = $token->getPartnerIdent();
        }
        $form->setData($data);

        return $form;
    }

    /**
     * @param Token $token
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Token $token)
    {
        // Если еще не привязанных сделок, то удаляем из БД
        // Если есть, то помечаем, как удаленный
        if ($token->getDeals()->count() === 0) {
            $this->entityManager->remove($token);
        } else {
            $token->setIsDeleted(1);
            $this->entityManager->persist($token);
        }

        $this->entityManager->flush();
    }

    /**
     * @param int $recursion_level
     * @return string
     * @throws LogicException
     */
    private function generateUniqueTokenKey($recursion_level = 0): string
    {
        if ($recursion_level >= self::MAX_RECURSION_LEVEL) {

            throw new LogicException(null, LogicException::DEAL_TOKEN_GENERATION_UNIQUE_KEY_FAILED);
        }

        $token_key = $this->generateRandKey();
        $token = $this->getTokenByKey($token_key);
        if ($token !== null) {
            //// если token с таким token_key существует запускаем повторную генерацию ////
            return $this->generateUniqueTokenKey(++$recursion_level);
        }

        return $token_key;
    }

    /**
     * @return string
     */
    private function generateRandKey(): string
    {
        return uniqid(null, false);
    }
}