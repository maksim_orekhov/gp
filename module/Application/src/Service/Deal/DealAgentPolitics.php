<?php

namespace Application\Service\Deal;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Service\HydratorManager;

/**
 * Class DealAgentPolitics
 * @package Application\Service\Deal
 */
class DealAgentPolitics extends HydratorManager
{
    /**
     * @param Deal $deal
     * @param DealAgent $dealAgent
     * @return bool
     */
    public function isDealAgentAllowedToChangeRole(Deal $deal, DealAgent $dealAgent): bool
    {
        return $dealAgent === $deal->getOwner();
    }

    /**
     * @param Deal $deal
     * @param DealAgent $dealAgent
     * @return bool
     */
    public function isDealAgentAllowedToChangeCounterAgent(Deal $deal, DealAgent $dealAgent): bool
    {
        return $dealAgent === $deal->getOwner();
    }
}