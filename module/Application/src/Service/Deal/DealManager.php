<?php

namespace Application\Service\Deal;

use Application\Controller\DealController;
use Application\Entity\DealAgent;
use Application\Entity\Delivery;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\CivilLawSubject;
use Application\Entity\SoleProprietor;
use Application\Entity\Token;
use Application\Form\CivilLawSubjectSelectionForm;
use Application\Form\NaturalPersonForm;
use Application\Form\PaymentMethodSelectionForm;
use Application\Service\CivilLawSubject\CivilLawSubjectPolitics;
use Application\Service\DeliveryManager;
use Application\Service\Payment\CalculatedDataProvider;
use Application\Service\Payment\PaymentMethodStrategy;
use Application\Service\Payment\PaymentPolitics;
use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;
use Application\Service\Payment\PaymentManager;
use Application\Entity\Deal;
use Application\Entity\DealContractFile;
use Application\Entity\User;
use Application\Entity\FeePayerOption;
use Application\Entity\SystemPaymentDetails;
use Application\Entity\PaymentMethod;
use Application\Entity\LegalEntity;
use Application\Entity\NaturalPerson;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Entity\Email;
use Doctrine\ORM\OptimisticLockException;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\SettingManager;
use Application\Service\Dispute\DisputeManager;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\Entity\DeliveryServiceType;
use ModuleDelivery\Entity\DeliveryTrackingNumber;
use ModuleDelivery\Service\DeliveryOrderManager;
use ModuleDeliveryDpd\Entity\DpdDeliveryOrder;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use ModuleFileManager\Service\FileManager;
use Application\Listener\CreditPaymentOrderAvailabilityListener;
use ModuleFileManager\Entity\File;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Entity\PaymentTransactionInterface;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\Form\Form;
use Zend\View\Renderer\RendererInterface;
use Zend\View\Model\ViewModel;
use Zend\Form\Element;

/**
 * Class DealManager
 * @package Application\Service\Deal
 */
class DealManager
{
    const ERROR_USER_NOT_RELATED_TO_DEAL = 'The user is not related to this Deal';
    const BILL_FOR_PAYMENT = 'Счет на оплату договора №';
    const CONTRACT_FOR_DEAL = 'Скачать контракт по договору №';
    const ERROR_ALREADY_CONFIRMED_BY_BOTH_DEAL_AGENTS = 'Сделка уже согласована';
    const ERROR_ALREADY_CONFIRMED_BY_SPECIFIC_DEAL_AGENT = 'Вы уже согласились с условиями сделки';
    const CIVIL_LAW_SUBJECT_FILLED_IN_INCORRECTLY = 'Субъект права у агента заполнен некорректно';
    const DELIVERY_FILLED_IN_INCORRECTLY = 'Доставка заполнена некорректно!';
    const PAYMENT_SUBJECT_FILLED_IN_INCORRECTLY = 'Платежный метод не может быть пустым';
    const FORBIDDEN_EDIT_EMAIL_NOT_OWNER = 'Email запрешено редактировать не владельцу';
    const FORBIDDEN_EDIT_ROLE_NOT_OWNER = 'Роль запрешено редактировать не владельцу';
    const TRYING_TO_SET_PAYMENT_METHOD_TO_CUSTOMER_ON_DEAL_CREATE = 'Customer не может иметь payment method при создании сделки';
    const DAY_PLURALIZATION = [
        'plural' => 'дней',
        'plural_2-4' => 'дня',
        'one' => 'день'
    ];

    const FEE_PAYER_OPTION_TRANSLATE = [
        'CUSTOMER' => 'Покупатель',
        'CONTRACTOR' => 'Продавец',
        '50/50' => '50/50'
    ];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @var CreditPaymentOrderAvailabilityListener
     */
    private $creditPaymentOrderAvailabilityListener;

    /**
     * @var DeliveryManager
     */
    private $deliveryManager;

    /**
     * @var DealStatus
     */
    private $dealStatus;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * @var RendererInterface
     * @TODO После перевода всех темлейтов на twig, убрать из класса и фабрики
     */
    protected $renderer;

    /**
     * @var \TCPDF
     */
    protected $tcpdf;

    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider;

    /**
     * @var bool
     */
    private $isConfirmStart = false;

    /**
     * @var RepaidOverpayManager
     */
    private $repaidOverpayManager;

    /**
     * @var TimelineManager
     */
    private $timelineManager;

    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * @var DealTypeManager
     */
    private $dealTypeManager;

    /**
     * @var DealTokenManager
     */
    private $dealTokenManager;

    /**
     * @var DeliveryOrderManager
     */
    private $deliveryOrderManager;

    /**
     * @var array
     */
    private $config;

    /**
     * DealManager constructor.
     * @param EntityManager $entityManager
     * @param PaymentManager $paymentManager
     * @param DealAgentManager $dealAgentManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param DealPolitics $dealPolitics
     * @param SettingManager $settingManager
     * @param FileManager $fileManager
     * @param CreditPaymentOrderAvailabilityListener $creditPaymentOrderAvailabilityListener
     * @param RepaidOverpayManager $repaidOverpayManager
     * @param DeliveryManager $deliveryManager
     * @param DealStatus $dealStatus
     * @param DisputeManager $disputeManager
     * @param PaymentOrderManager $paymentOrderManager
     * @param RendererInterface $renderer
     * @param \TCPDF $tspdf
     * @param CalculatedDataProvider $calculatedDataProvider
     * @param TimelineManager $timelineManager
     * @param PaymentPolitics $paymentPolitics
     * @param DealTypeManager $dealTypeManager
     * @param DealTokenManager $dealTokenManager
     * @param DeliveryOrderManager $deliveryOrderManager
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                PaymentManager $paymentManager,
                                DealAgentManager $dealAgentManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                PaymentMethodManager $paymentMethodManager,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                DealPolitics $dealPolitics,
                                SettingManager $settingManager,
                                FileManager $fileManager,
                                CreditPaymentOrderAvailabilityListener $creditPaymentOrderAvailabilityListener,
                                RepaidOverpayManager $repaidOverpayManager,
                                DeliveryManager $deliveryManager,
                                DealStatus $dealStatus,
                                DisputeManager $disputeManager,
                                PaymentOrderManager $paymentOrderManager,
                                RendererInterface $renderer,
                                \TCPDF $tspdf,
                                CalculatedDataProvider $calculatedDataProvider,
                                TimelineManager $timelineManager,
                                PaymentPolitics $paymentPolitics,
                                DealTypeManager $dealTypeManager,
                                DealTokenManager $dealTokenManager,
                                DeliveryOrderManager $deliveryOrderManager,
                                array $config)
    {
        $this->entityManager            = $entityManager;
        $this->paymentManager           = $paymentManager;
        $this->dealAgentManager         = $dealAgentManager;
        $this->userManager              = $userManager;
        $this->baseAuthManager          = $baseAuthManager;
        $this->paymentMethodManager     = $paymentMethodManager;
        $this->civilLawSubjectManager   = $civilLawSubjectManager;
        $this->dealPolitics             = $dealPolitics;
        $this->settingManager           = $settingManager;
        $this->fileManager              = $fileManager;
        $this->creditPaymentOrderAvailabilityListener = $creditPaymentOrderAvailabilityListener;
        $this->repaidOverpayManager     = $repaidOverpayManager;
        $this->deliveryManager          = $deliveryManager;
        $this->dealStatus               = $dealStatus;
        $this->disputeManager           = $disputeManager;
        $this->paymentOrderManager      = $paymentOrderManager;
        $this->renderer                 = $renderer;
        $this->tcpdf                    = $tspdf;
        $this->calculatedDataProvider   = $calculatedDataProvider;
        $this->timelineManager          = $timelineManager;
        $this->paymentPolitics          = $paymentPolitics;
        $this->dealTypeManager          = $dealTypeManager;
        $this->dealTokenManager         = $dealTokenManager;
        $this->deliveryOrderManager     = $deliveryOrderManager;
        $this->config = $config;
    }


    /**
     * @param $id
     * @return Deal
     * @throws \Exception
     */
    public function getDealById($id): Deal
    {
        /** @var Deal $deal */ // Get Deal by Id
        $deal = $this->entityManager->find(Deal::class, $id);

        if (!$deal || !($deal instanceof Deal)) {
            // If no deal found
            throw new LogicException(null, LogicException::DEAL_WITH_SUCH_ID_NOT_FOUND);
        }
        // Dynamically set status
        $this->dealStatus->setStatus($deal);
        // Dynamically set deadlineDate
        $this->setDeadlineDate($deal);

        return $deal;
    }

    /**
     * @param Deal $deal
     */
    private function setDeadlineDate(Deal $deal)
    {
        $deal->deadlineDate = DealDeadlineManager::getDealDeadlineDate($deal->getPayment());
    }

    /**
     * @param Deal $deal
     * @return string
     *
     * Important! Усли меняем алгоритм создания номера, то меняем и в фикстурах
     */
    public function getDealNumber(Deal $deal)
    {
        return '#GP' . $deal->getId() . $deal->getDealType()->getIdent();
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @param $GPAccountNumber
     * @return mixed
     * @throws \Exception
     */
    public function getDealCollectionByUser(User $user, array $params, bool $isOperator)
    {
        try {
            // Get all users's deals with sort and search params
            $deals = $this->entityManager->getRepository(Deal::class)
                ->selectSortedDealsForUser($user, $params, $isOperator);
            // Set status to deals
            // @TODO Переделать после реализации отдельного класса для определения статуса (GP-706)
            $this->setDealsStatus($deals);

        } catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $deals;
    }

    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getAllowedDealByUser(User $user)
    {
        /** @var Deal $deal */
        $deals = $this->entityManager->getRepository(Deal::class)
            ->findAllowedDealByUser($user);

        return $deals;
    }

    /**
     * @param User|null $user
     * @return mixed
     */
    public function getDealsWithoutCLSInDealAgent(User $user = null)
    {
        /** @var Deal $deal */
        $deals = $this->entityManager->getRepository(Deal::class)
            ->findDealsWithoutCLSInDealAgent($user);

        return $deals;
    }

    /**
     * @param $deals
     * @param User $user
     * @param $GPAccountNumber
     * @return array
     */
    public function getDealCollectionForOutput($deals, User $user)
    {
        $dealsOutput = [];
        foreach ($deals as $deal) {
            $dealsOutput[] = $this->getDealForOutput($deal, $user);
        }

        return $dealsOutput;
    }

    /**
     * @param Deal $deal
     * @param User $user
     * @return array
     */
    public function getDealForOutput(Deal $deal, User $user): array
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var DealAgent $owner */
        $owner = $deal->getOwner();
        /** @var User $ownerUser */
        $ownerUser = $owner ? $owner->getUser() : null;
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        /** @var User $customerUser */
        $customerUser = $customer ? $customer->getUser() : null;
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var User $contractorUser */
        $contractorUser = $contractor ? $contractor->getUser() : null;

        $dealOutput = [];
        $dealOutput['id'] = $deal->getId();
        $dealOutput['number'] = $deal->getNumber();
        $dealOutput['name'] = $deal->getName();
        if (!isset($deal->status)) {
            $this->dealStatus->setStatus($deal);
        }
        if (!isset($deal->deadlineDate)) {
            $this->setDeadlineDate($deal);
        }
        $dealOutput['deal_status'] = $deal->status;
        $dealOutput['date_of_deadline'] = $deal->deadlineDate ? $deal->deadlineDate->format('d.m.Y') : null;
        $dealOutput['type'] = $deal->getDealType()->getName();
        $dealOutput['fee_payer_option'] = $this->makeFeePayerOption($deal->getFeePayerOption()->getName());
        $dealOutput['delivery_period'] = $this->makeDeliveryPeriod($deal->getDeliveryPeriod(), self::DAY_PLURALIZATION);
        $dealOutput['addon_terms'] = $deal->getAddonTerms();
        $dealOutput['payment_amount'] = $payment->getExpectedValue();
        $dealOutput['payment_amount_without_fee'] = $payment->getDealValue();
        $dealOutput['created'] = $deal->getCreated()->format('d.m.Y');
        $dealOutput['created_milliseconds'] = $deal->getCreated()->getTimestamp();

        $dealOutput['customer'] = $customerUser ? $customerUser->getLogin() : null;
        $dealOutput['customer_deal_agent_email'] = $customer ? $customer->getEmail() : null;

        $dealOutput['contractor'] = $contractorUser ? $contractorUser->getLogin() : null;
        $dealOutput['contractor_deal_agent_email'] = $contractor ? $contractor->getEmail() : null;

        $dealOutput['bill'] = [];
        if ($payment) {
            $dealOutput['bill']['name'] = self::BILL_FOR_PAYMENT . $deal->getId();
        }

        $dealOutput['contract'] = [];
        if ($deal->getDealContractFile() && $deal->getDealContractFile()->getFile()) {
            $file_name = $deal->getDealContractFile()->getFile()->getName();
            $dealOutput['contract']['name'] = self::CONTRACT_FOR_DEAL . $deal->getId();
            $dealOutput['contract']['url'] = $this->config['file-management']['main_upload_folder'] .
                DealController::DEAL_CONTRACTS_STORING_FOLDER . "/" . $file_name;
        }

        $dealOutput['user_role'] = null;
        $dealOutput['agent_confirm'] = null;
        $dealOutput['counteragent_confirm'] = null;
        if ($user === $customerUser) {
            $dealOutput['user_role'] = 'customer';
            $dealOutput['agent_confirm'] = $customer ? $customer->getDealAgentConfirm() : null;
            $dealOutput['counteragent_confirm'] = $contractor ? $contractor->getDealAgentConfirm() : null;
        } elseif ($user === $contractorUser) {
            $dealOutput['user_role'] = 'contractor';
            $dealOutput['agent_confirm'] = $contractor ? $contractor->getDealAgentConfirm() : null;
            $dealOutput['counteragent_confirm'] = $customer ? $customer->getDealAgentConfirm() : null;
        }

        $dealOutput['is_delivery_confirmed'] = $this->deliveryManager->isDeliveryConfirmed($deal);
        $dealOutput['owner'] = $ownerUser ?
            $ownerUser->getLogin() :
            $owner->getEmail()
        ; // Это нужно для Оператора

        // Current Deal debit
        $dealOutput['debit'] = $this->getCurrentDealDebit($payment);

        // Is Deal has open Dispute
        $dealOutput['is_deal_has_dispute'] = $this->isDealHasDispute($deal);

        return $dealOutput;
    }

    /**
     * @param Deal $deal
     * @param User|null $user
     * @return array
     * @throws LogicException
     * @throws \Exception
     */
    public function getDealOutputForSingle(Deal $deal, User $user = null): array
    {
        if (!$user && $this->baseAuthManager->getIdentity()) {
            /** @var User $user */
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }
        if (!isset($deal->status)) {
            $this->dealStatus->setStatus($deal);
        }
        if (!isset($deal->deadlineDate)) {
            $this->setDeadlineDate($deal);
        }

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        $customerCivilLawSubject = $customer->getCivilLawSubject();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();

        $dealOutput = [];
        $dealOutput['id'] = $deal->getId();
        $dealOutput['number'] = $deal->getNumber();
        $dealOutput['name'] = $deal->getName();
        $dealOutput['full_name'] = 'Сделка «' . $deal->getName() . '» ' . $deal->getNumber();
        $dealOutput['status'] = $deal->status;
        $dealOutput['type'] = $deal->getDealType()->getName();
        $dealOutput['deal_type'] = $deal->getDealType()->getId();
        $dealOutput['type_ident'] = $deal->getDealType()->getIdent();
        $dealOutput['deal_role'] = $customer->getUser() === $user ? 'customer' : 'contractor';
        $dealOutput['owner_role'] = $deal->getOwner() === $customer ? 'customer' : 'contractor';
        $dealOutput['addon_terms'] = $deal->getAddonTerms();
        $dealOutput['fee_payer_option_name'] = $this->makeFeePayerOption($deal->getFeePayerOption()->getName());
        $dealOutput['fee_payer_option'] = $deal->getFeePayerOption()->getId();
        $dealOutput['amount_without_fee'] = $payment->getDealValue();
        $dealOutput['customer_amount'] = $payment->getExpectedValue();
        $dealOutput['customer_fee'] = $payment->getExpectedValue() - $payment->getDealValue();
        $dealOutput['contractor_amount'] = $payment->getReleasedValue();
        $dealOutput['contractor_fee'] = $payment->getDealValue() - $payment->getReleasedValue();
        $dealOutput['fee_amount'] = $this->calculatedDataProvider->getCalculateFeeAmount($payment->getDealValue(), $payment->getFee());
        $dealOutput['fee_percentage'] = $payment->getFee();
        $dealOutput['created'] = $deal->getCreated()->format('d.m.Y');
        $dealOutput['created_milliseconds'] = $deal->getCreated()->getTimestamp();
        $dealOutput['deal_balance'] = $this->getDealBalance($deal);
        $dealOutput['mandarin_acquiring'] = null;

        $paymentTransaction = $this->getLastIncomingPaymentTransaction($payment);

        if ($paymentTransaction instanceof MandarinPaymentOrder) {
            $dealOutput['mandarin_acquiring'] = [
                'status' => $paymentTransaction->getStatus(),
                'created' => $paymentTransaction->getCreated()->format('d.m.Y H:i:s'),
                'amount' => $paymentTransaction->getAmount(),
                'transaction' => $paymentTransaction->getTransaction(),
            ];
        }
        $dealOutput['date_of_confirm'] = $deal->getDateOfConfirm() ? $deal->getDateOfConfirm()->format('d.m.Y') : null;
        $dealOutput['de_facto_date'] = $payment->getDeFactoDate() ? $payment->getDeFactoDate()->format('d.m.Y') : null;
        $dealOutput['delivery_period'] = $deal->getDeliveryPeriod();

        $dealOutput['date_of_deadline_params'] = null;
        $dealOutput['date_of_deadline'] = null;
        if ($payment->getDeFactoDate()) {
            /** @var \DateTime $dealDeadline */
            $dealDeadline = DealDeadlineManager::getDealDeadlineDate($payment);
            $dealOutput['date_of_deadline'] = $dealDeadline->format('d.m.Y');
            $diffDate = date_diff(new \DateTime(), $dealDeadline);

            $dealOutput['date_of_deadline_params'] = [
                'y' => $diffDate->y,
                'm' => $diffDate->m,
                'd' => $diffDate->d,
                'h' => $diffDate->h,
                'i' => $diffDate->i,
                'invert' => $diffDate->invert,
                'days' => $diffDate->invert ? -1 * $diffDate->days : $diffDate->days,
                'hours' => hour_date_diff($diffDate),
            ];
        }

        $dealOutput['date_confirm_delivery'] = null;
        /** @var Delivery $deliveryConfirmed */
        $deliveryConfirmed = $this->deliveryManager->getDeliveryConfirmed($deal);
        if ($deliveryConfirmed) {
            $dealOutput['date_confirm_delivery'] = $deliveryConfirmed->getDeliveryDate()->format('d.m.Y');
            $dealOutput['date_confirm_delivery_full'] = [
                'date' => $deliveryConfirmed->getDeliveryDate()->format('d.m.Y'),
                'time' => $deliveryConfirmed->getDeliveryDate()->format('H:i:s'),
            ];
        }
        $dealOutput['planned_date'] = $payment->getPlannedDate() ? $payment->getPlannedDate()->format('d.m.Y') : null;

        $dealOutput['date_credit_order'] = null;
        if ($payment->getPaymentMethod()) {
            /** @var BankClientPaymentOrder $creditOrder */ // @TODO Предусмотреть и другие типы методов оплаты
            $creditOrder = $this->getOutgoingOrderToContractor($payment);
            if ($creditOrder && $creditOrder->getBankClientPaymentOrderFile()) {
                /** @var \DateTime $creditOrderDate */
                $creditOrderDate = $creditOrder->getCreated();
                $dealOutput['date_credit_order'] = $creditOrderDate->format('d.m.Y');
            }
        }
        $dealOutput['date_closed_deal'] = null;
        $dealClosedDate = $this->getDateOfDealClosed($payment);
        if ($dealClosedDate) {
            $dealOutput['date_closed_deal'] = $dealClosedDate->format('d.m.Y');
        }

        //// customer ////
        $dealOutput['is_customer'] = $customer->getUser() === $user;
        if ($dealOutput['is_customer']) {
            $dealOutput['is_current_user_confirmed'] = $customer->getDealAgentConfirm();
            $dealOutput['is_counteragent_confirmed'] = $contractor->getDealAgentConfirm();
        }
        $dealOutput['is_customer_once_confirmed'] = $customer->isDealAgentOnceConfirm();

        $dealOutput['customer']['invitation'] = null;
        if (!$customerCivilLawSubject && $dealOutput['is_customer'] === false) {
            $invitationDate = $customer->getInvitationDate();
            $dealOutput['customer']['invitation'] = [
                'date' => $invitationDate ? $invitationDate->format('d.m.Y H:i:s') : null,
                'timestamp' => $invitationDate ? $invitationDate->getTimestamp() : null,
                'request_period' => $this->config['tokens']['deal_invitation_request_period'],
            ];
        }
        //customer legal entity
        $naturalPerson = null;
        $soleProprietor = null;
        $legalEntity = null;
        if ($customerCivilLawSubject) {
            $naturalPerson = $customerCivilLawSubject->getNaturalPerson();
            $soleProprietor = $customerCivilLawSubject->getSoleProprietor();
            $legalEntity = $customerCivilLawSubject->getLegalEntity();
        }

        $dealOutput['customer']['civil_law_subject_id'] = $customerCivilLawSubject ? $customerCivilLawSubject->getId() : null;

        //customer natural person
        if ($naturalPerson) {
            $dealOutput['customer']['first_name'] = $naturalPerson->getFirstName();
            $dealOutput['customer']['secondary_name'] = $naturalPerson->getSecondaryName();
            $dealOutput['customer']['last_name'] = $naturalPerson->getLastName();
            $dealOutput['customer']['fio'] = $naturalPerson->getLastName() . ' ' . $naturalPerson->getFirstName() . ' ' . $naturalPerson->getSecondaryName();
            $dealOutput['customer']['birth_date'] = $naturalPerson->getBirthDate()->format('d.m.Y');
            $dealOutput['customer']['phone'] = $customer->getUser() ? '+' . $customer->getUser()->getPhone()->getPhoneNumber() : null;
        }
        //customer sole proprietor
        if ($soleProprietor) {
            $dealOutput['customer']['name'] = $soleProprietor->getName();
            $dealOutput['customer']['first_name'] = $soleProprietor->getFirstName();
            $dealOutput['customer']['secondary_name'] = $soleProprietor->getSecondaryName();
            $dealOutput['customer']['last_name'] = $soleProprietor->getLastName();
            $dealOutput['customer']['fio'] = $soleProprietor->getLastName() . ' ' . $soleProprietor->getFirstName() . ' ' . $soleProprietor->getSecondaryName();
            $dealOutput['customer']['birth_date'] = $soleProprietor->getBirthDate()->format('d.m.Y');
            $dealOutput['customer']['inn'] = $soleProprietor->getInn();
        }
        //customer legal entity
        if ($legalEntity) {
            $dealOutput['customer']['name'] = $legalEntity->getName();
            $dealOutput['customer']['address'] = $legalEntity->getLegalAddress();
            $dealOutput['customer']['inn'] = $legalEntity->getInn();
            $dealOutput['customer']['kpp'] = $legalEntity->getKpp();
        }
        $dealOutput['customer']['email'] = $deal->getCustomer()->getEmail();
        $dealOutput['customer']['is_natural_person'] = (bool)$naturalPerson;
        $dealOutput['customer']['is_sole_proprietor'] = (bool)$soleProprietor;
        $dealOutput['customer']['is_legal_entity'] = (bool)$legalEntity;
        $dealOutput['customer']['is_confirmed'] = $customer->getDealAgentConfirm();

        //// contractor ///
        $dealOutput['is_contractor'] = $contractor->getUser() === $user;
        if ($dealOutput['is_contractor']) {
            $dealOutput['is_current_user_confirmed'] = $contractor->getDealAgentConfirm();
            $dealOutput['is_counteragent_confirmed'] = $customer->getDealAgentConfirm();
        }
        $dealOutput['is_contractor_once_confirmed'] = $contractor->isDealAgentOnceConfirm();

        $dealOutput['contractor']['invitation'] = null;
        if (!$contractorCivilLawSubject && $dealOutput['is_contractor'] === false) {
            $invitationDate = $contractor->getInvitationDate();
            $dealOutput['contractor']['invitation'] = [
                'date' => $invitationDate ? $invitationDate->format('d.m.Y H:i:s') : null,
                'timestamp' => $invitationDate ? $invitationDate->getTimestamp() : null,
                'request_period' => $this->config['tokens']['deal_invitation_request_period'],
            ];
        }
        //contractor legal entity
        $naturalPerson = null;
        $soleProprietor = null;
        $legalEntity = null;
        if ($contractorCivilLawSubject) {
            $naturalPerson = $contractorCivilLawSubject->getNaturalPerson();
            $soleProprietor = $contractorCivilLawSubject->getSoleProprietor();
            $legalEntity = $contractorCivilLawSubject->getLegalEntity();
        }

        $dealOutput['contractor']['civil_law_subject_id'] = $contractorCivilLawSubject ? $contractorCivilLawSubject->getId() : null;
        // contractor natural person
        if ($naturalPerson) {
            $dealOutput['contractor']['first_name'] = $naturalPerson->getFirstName();
            $dealOutput['contractor']['secondary_name'] = $naturalPerson->getSecondaryName();
            $dealOutput['contractor']['last_name'] = $naturalPerson->getLastName();
            $dealOutput['contractor']['fio'] = $naturalPerson->getLastName() . ' ' . $naturalPerson->getFirstName() . ' ' . $naturalPerson->getSecondaryName();
            $dealOutput['contractor']['birth_date'] = $naturalPerson ? $naturalPerson->getBirthDate()->format('d.m.Y') : null;
            $dealOutput['contractor']['phone'] = $contractor->getUser() ? '+' . $contractor->getUser()->getPhone()->getPhoneNumber() : null;
        }
        //customer sole proprietor
        if ($soleProprietor) {
            $dealOutput['contractor']['name'] = $soleProprietor->getName();
            $dealOutput['contractor']['first_name'] = $soleProprietor->getFirstName();
            $dealOutput['contractor']['secondary_name'] = $soleProprietor->getSecondaryName();
            $dealOutput['contractor']['last_name'] = $soleProprietor->getLastName();
            $dealOutput['contractor']['fio'] = $soleProprietor->getLastName() . ' ' . $soleProprietor->getFirstName() . ' ' . $soleProprietor->getSecondaryName();
            $dealOutput['contractor']['birth_date'] = $soleProprietor->getBirthDate()->format('d.m.Y');
            $dealOutput['contractor']['inn'] = $soleProprietor->getInn();
        }
        //customer legal entity
        if ($legalEntity) {
            $dealOutput['contractor']['name'] = $legalEntity->getName();
            $dealOutput['contractor']['address'] = $legalEntity->getLegalAddress();
            $dealOutput['contractor']['inn'] = $legalEntity->getInn();
            $dealOutput['contractor']['kpp'] = $legalEntity->getKpp();
            $dealOutput['contractor']['ogrn'] = $legalEntity->getOgrn();
        }

        $dealOutput['contractor']['email'] = $deal->getContractor()->getEmail();
        $dealOutput['contractor']['is_natural_person'] = (bool)$naturalPerson;
        $dealOutput['contractor']['is_sole_proprietor'] = (bool)$soleProprietor;
        $dealOutput['contractor']['is_legal_entity'] = (bool)$legalEntity;
        $dealOutput['contractor']['is_confirmed'] = $contractor->getDealAgentConfirm();

        $dealOutput['bill'] = [];
        if ($payment) {
            $dealOutput['bill']['name'] = self::BILL_FOR_PAYMENT . $deal->getId();
        }

        $dealOutput['contract'] = [];
        if ($deal->getDealContractFile() && $deal->getDealContractFile()->getFile()) {
            $file_name = $deal->getDealContractFile()->getFile()->getName();
            $dealOutput['contract']['name'] = self::CONTRACT_FOR_DEAL . $deal->getId();
            $dealOutput['contract']['url'] = $this->config['file-management']['main_upload_folder'] .
                DealController::DEAL_CONTRACTS_STORING_FOLDER . '/' . $file_name;
        }

        $dealOutput['is_delivery_confirmed'] = $this->deliveryManager->isDeliveryConfirmed($deal);
        $dealOutput['owner'] = $deal->getOwner()->getUser() ?
            $deal->getOwner()->getUser()->getLogin() :
            $deal->getOwner()->getEmail()
        ; // Это нужно для Оператора
        $dealOutput['is_owner'] = $deal->getOwner()->getUser() === $user;
        $dealOutput['removable'] = $this->isDealRemovingAllowed($deal);
        $dealOutput['editable'] = $this->isDealEditingAllowed($deal);

        $dealOutput['is_need_to_fill_civil_law_subject'] = null;
        $dealOutput['is_need_to_fill_payment_method'] = null;
        $dealOutput['is_need_to_fill_delivery'] = null;
        if (null !== $user) {
            $dealOutput['is_need_to_fill_civil_law_subject'] = $this->isNeedToFillCivilLawSubjectInDeal($deal);
            $dealOutput['is_need_to_fill_payment_method'] = $this->isNeedToFillPaymentMethodInDeal($deal);
            $dealOutput['is_need_to_fill_delivery'] = $this->isNeedToFillDeliveryInDeal($deal);
        }

        $dealOutput['is_need_to_fill_deal'] =
            $dealOutput['is_need_to_fill_civil_law_subject'] ||
            $dealOutput['is_need_to_fill_payment_method'] ||
            $dealOutput['is_need_to_fill_delivery'];

        $dealOutput['payment_method_requisites'] = [];

        if ($payment->getPaymentMethod() && $payment->getPaymentMethod()->getPaymentMethodBankTransfer()) {
            $dealOutput['payment_method_requisites'] = $this->paymentMethodManager
                ->getPaymentMethodBankTransferOutput($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        }

        $dealOutput['files'] = [];
        /** @var File $file */
        foreach ($deal->getDealFiles() as $file) {
            $dealOutput['files'][] = [
                'id' => $file->getId(),
                'name' => $file->getName(),
                'origin_name' => $file->getOriginName(),
                'type' => $file->getType(),
                'path' => $file->getPath(),
                'size' => $file->getSize()
            ];
        }
        // Is Deal has open Dispute
        $dealOutput['is_deal_has_dispute'] = $this->isDealHasDispute($deal);
        // Dispute (открытый или закрытый спор)
        $dealOutput['dispute'] = null;
        if ($deal->getDispute()) {
            $dealOutput['dispute'] = $this->disputeManager->getDisputeOutput($deal->getDispute());
        }
        /** @var DeliveryOrder $deliveryOrder */
        $deliveryOrder = $deal->getDeliveryOrder();
        $deliveryTrackingNumbers = $deliveryOrder ? $deliveryOrder->getDeliveryTrackingNumbers() : [];
        $dealOutput['tracking_numbers'] = [];
        /** @var DeliveryTrackingNumber $deliveryTrackingNumber */
        foreach ($deliveryTrackingNumbers as $deliveryTrackingNumber) {
            $dealOutput['tracking_numbers'][] = [
                'date' => $deliveryTrackingNumber->getCreated()->format('d.m.Y'),
                'time' => $deliveryTrackingNumber->getCreated()->format('H:i:s'),
                'tracking_number' => $deliveryTrackingNumber->getTrackingNumber()
            ];
        }
        //сортируем по дате
        if (!empty($dealOutput['tracking_numbers'])) {
            usort($dealOutput['tracking_numbers'], function ($a, $b) {
                return strtotime($b['date'] . ' ' . $b['time']) - strtotime($a['date'] . ' ' . $a['time']);
            });
        }

        $dealOutput['is_current_user_confirmed'] = $dealOutput['is_current_user_confirmed'] ?? null;
        $dealOutput['confirm_reset_by'] = null;
        if ($dealOutput['is_customer_once_confirmed'] && !$customer->getDealAgentConfirm()) {
            $dealOutput['confirm_reset_by'] = Deal::CONTRACTOR;
        } elseif ($dealOutput['is_contractor_once_confirmed'] && !$contractor->getDealAgentConfirm()) {
            $dealOutput['confirm_reset_by'] = Deal::CUSTOMER;
        }

        if (null !== $user) {
            /** @var DealAgent $userCounterAgent */
            $userCounterAgent = $this->getUserCounterAgentByDeal($user, $deal);
            $dealOutput['is_counteragent_unregistered'] = $userCounterAgent->getUser() ? false : true;
            $dealOutput['timeline'] = $this->timelineManager->buildTimeLine($deal);
            $dealOutput['deal_history'] = $this->timelineManager->buildDealHistory($deal, $user, $dealOutput);
        }

        $dealOutput['available_payment_method_types'] = $this->getAvailablePaymentMethodTypes($deal, $dealOutput['is_customer']);

        $dealOutput['delivery'] = null;
        if (null !== $deliveryOrder) {
            $dealOutput['delivery'] = $this->deliveryOrderManager->getDeliveryOrderOutput($deliveryOrder);
        }

        return $dealOutput;
    }

    /**
     * @param Deal $deal
     * @param bool $is_customer
     * @return array
     */
    public function getAvailablePaymentMethodTypes(Deal $deal, bool $is_customer): array
    {
        $available_payment_method_types = [];

        if (true === $is_customer && $deal->status['status'] === 'confirmed') {
            $available_payment_method_types[PaymentMethodType::BANK_TRANSFER] =
                PaymentPolitics::isPaymentMethodTypeAvailable($deal, PaymentMethodType::BANK_TRANSFER);
            $available_payment_method_types[PaymentMethodType::ACQUIRING_MANDARIN] =
                PaymentPolitics::isPaymentMethodTypeAvailable($deal, PaymentMethodType::ACQUIRING_MANDARIN);
        }

        return $available_payment_method_types;
    }

    /**
     * @param Deal $deal
     * @return float|int
     * @throws \Exception
     */
    public function getDealBalance(Deal $deal)
    {
        return $this->paymentOrderManager->getDealBalanceByPayment($deal->getPayment());
    }


    /**
     * @param Deal $deal
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getDealOutputForForm(Deal $deal, User $user): array
    {
        // Получаем DealAgents относительного текущего пользователя // Can throw Exception!
        $dealAgents = $this->getDealAgentsRelativeToCurrentUser($user, $deal);
        // Распределяем
        /** @var DealAgent $userDealAgent */
        $userDealAgent = $dealAgents['currentUserAgent'];
        /** @var DealAgent $counterAgent */
        $counterAgent = $dealAgents['counterAgent'];
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $payment->getPaymentMethod();
        $payment_method_name = $paymentMethod ? $paymentMethod->getPaymentMethodType()->getName() : null;
        $is_contractor = $deal->getContractor() === $userDealAgent;

        $deal_civil_law_subject_id = null;
        if ($userDealAgent && $userDealAgent->getCivilLawSubject()) {
            $deal_civil_law_subject_id = $userDealAgent->getCivilLawSubject()->getId();
        }

        $payment_method_id = null;
        if ($is_contractor && PaymentMethodType::BANK_TRANSFER === $payment_method_name) {
            $payment_method_id = $paymentMethod->getId();
        }

        $counteragent_email = strtolower($counterAgent->getEmail());

        $dealFormOutput = [];

        $dealFormOutput['id'] = $deal->getId();
        $dealFormOutput['deal_type'] = $deal->getDealType()->getId();
        $dealFormOutput['deal_role'] = $this->getDealRole($user, $deal);
        $dealFormOutput['name'] = $deal->getName();
        $dealFormOutput['addon_terms'] = $deal->getAddonTerms();
        $dealFormOutput['amount'] = $payment->getDealValue();
        $dealFormOutput['fee_payer_option'] = $deal->getFeePayerOption()->getId();
        $dealFormOutput['delivery_period'] = $deal->getDeliveryPeriod();
        $dealFormOutput['deal_civil_law_subject'] = $deal_civil_law_subject_id;
        $dealFormOutput['payment_method'] = $payment_method_id;
        $dealFormOutput['counteragent_email'] = $counteragent_email;
        $dealFormOutput['deal_number'] = $deal->getNumber();
        $dealFormOutput['is_owner'] = $deal->getOwner()->getUser() === $user;
        $dealFormOutput['created'] = $deal->getCreated()->format('d.m.Y');
        $dealFormOutput['delivery'] = null;
        if (null !== $deal->getDeliveryOrder()) {
            $dealFormOutput['delivery'] = $this->deliveryOrderManager->getDeliveryOrderOutput($deal->getDeliveryOrder());
        }

        return $dealFormOutput;
    }

    /**
     * @param Payment $payment
     * @return PaymentTransactionInterface|null
     * @throws \Exception
     */
    private function getOutgoingOrderToContractor(Payment $payment)
    {
        /** @var PaymentTransactionInterface $paymentOutgoingPaymentOrder */
        $paymentOutgoingPaymentOrder = $this->paymentOrderManager
            ->getPaymentOutgoingPaymentTransaction($payment);

        if ($paymentOutgoingPaymentOrder) {

            return $paymentOutgoingPaymentOrder;
        }

        return null;
    }

    /**
     * @param Payment $payment
     * @return mixed|null
     *
     * @TODO Забираю в TimelineManager. Отсюда удалить!
     */
    private function getDateOfDealClosed(Payment $payment)
    {
        $paymentConfirmationPaymentOrder = $this->paymentOrderManager
            ->getPaymentConfirmationPaymentOrder($payment);

        if ($paymentConfirmationPaymentOrder) {

            return $paymentConfirmationPaymentOrder->getCreated();
        }

        return null;
    }

    /**
     * @param Payment $payment
     * @return array
     */
    public function getCurrentDealDebit(Payment $payment): array
    {
        $debit = [];
        $debit['total_amount'] = 0;
        $debit['matched'] = false;
        $debit['overpayment'] = null;
        $debit['underpayment'] = null;
        // Считаем количество debit платёжек

        if ($payment->getPaymentOrders()->count() > 0) {

            /** @var PaymentOrder $paymentOrder */
            foreach ($payment->getPaymentOrders() as $paymentOrder) {

                $payment_orders_ids[$paymentOrder->getPaymentMethodType()->getName()][] = $paymentOrder->getId();
            }

            $allIncomingPaymentTransactions = $this->paymentOrderManager
                ->getAllPaymentTransactionsByType($payment, PaymentOrderManager::TYPE_INCOMING);

            $i = 0;

            /** @var PaymentTransactionInterface $incomingPaymentTransaction */
            foreach ($allIncomingPaymentTransactions as $incomingPaymentTransaction) {

                if ($this->paymentOrderManager->isRecipientGP($incomingPaymentTransaction)) {

                    $debit['total_amount'] += $incomingPaymentTransaction->getAmount();

                    ++$i;
                }
            }

            // Количество debit платёжек
            $debit['number_of_payments'] = $i;
            // Дебет сошелся
            $debit['matched'] = ($debit['total_amount'] === $payment->getExpectedValue());
            // Если не сошелся
            if (!$debit['matched']) {
                if ($debit['total_amount'] > $payment->getExpectedValue()) {
                    // Переплата
                    $debit['overpayment'] = $debit['total_amount'] - $payment->getExpectedValue();
                } else {
                    // Недоплата
                    $debit['underpayment'] = ($payment->getExpectedValue() - $debit['total_amount']);
                }
            }
        }

        return $debit;
    }

    /**
     * @param Deal $deal
     * @return bool
     * @throws LogicException
     */
    public function isNeedToFillCivilLawSubjectInDeal(Deal $deal): bool
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $is_need_to_fill = false;

        if (($customer->getUser() === $user && !$customer->getCivilLawSubject())
            || ($contractor->getUser() === $user && !$contractor->getCivilLawSubject())) {
            $is_need_to_fill = true;
        }

        return $is_need_to_fill;
    }

    /**
     * @param Deal $deal
     * @return bool
     * @throws LogicException
     */
    public function isNeedToFillPaymentMethodInDeal(Deal $deal): bool
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($deal);
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $is_need_to_fill = false;

        if ($contractor->getUser() === $user && !$payment->getPaymentMethod()) {
            $is_need_to_fill = true;
        }

        return $is_need_to_fill;
    }

    /**
     * @param Deal $deal
     * @param User|null $user
     * @return bool
     * @throws LogicException
     */
    public function isNeedToFillDeliveryInDeal(Deal $deal, User $user = null): bool
    {
        /** @var User $user */
        if (!$user) {
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }
        /** @var DeliveryOrder $deliveryOrder */
        $this->entityManager->refresh($deal);
        $deliveryOrder = $deal->getDeliveryOrder();
        if (!$deliveryOrder) {

            return true;
        }
        /** @var DeliveryServiceType $deliveryServiceType */
        $this->entityManager->refresh($deliveryOrder);
        $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();
        /** @var User $customerUser */
        $customerUser = $deal->getCustomer() ? $deal->getCustomer()->getUser() : null;
        /** @var User $contractorUser */
        $contractorUser = $deal->getContractor() ? $deal->getContractor()->getUser() : null;

        switch ($deliveryServiceType->getName()) {
            case DeliveryOrderManager::DELIVERY_SERVICE_TYPE_NAMES['dpd']:
                $is_need_to_fill = false;
                /** @var DpdDeliveryOrder $dpdDeliveryOrder */
                $dpdDeliveryOrder = $deliveryOrder->getDpdDeliveryOrder();
                if (!$dpdDeliveryOrder){

                    $is_need_to_fill = true;
                    break;
                }
                //проверяем receiver_address
                if ($customerUser === $user) {

                    $is_need_to_fill = $this->deliveryOrderManager
                        ->isNotFillDpdAddress($dpdDeliveryOrder->getReceiverAddress());
                    break;
                }

                //проверяем sender_address и посылку
                if ($contractorUser === $user) {

                    if ($this->deliveryOrderManager->isNotFillDpdAddress($dpdDeliveryOrder->getSenderAddress()) ||
                        $this->deliveryOrderManager->isNotFillDpdParcel($dpdDeliveryOrder->getParcels())) {
                        $is_need_to_fill = true;
                    }
                    break;
                }

                break;
            default:
                $is_need_to_fill = false;
        }

        return $is_need_to_fill;
    }

    /**
     * @param string $fee_payer_option_name
     * @return mixed|null
     */
    private function makeFeePayerOption(string $fee_payer_option_name)
    {
        if (isset(self::FEE_PAYER_OPTION_TRANSLATE[$fee_payer_option_name])) {

            return self::FEE_PAYER_OPTION_TRANSLATE[$fee_payer_option_name];
        }

        return null;
    }

    /**
     * @param int $num
     * @param array $pluralization
     * @return string
     */
    private function makeDeliveryPeriod(int $num, array $pluralization): string
    {
        // $TODO Вынести в плагин или хелпер
        $num = abs($num) % 100; // берем число по модулю и сбрасываем сотни (делим на 100, а остаток присваиваем переменной $num)
        $num_x = $num % 10; // сбрасываем десятки и записываем в новую переменную
        if ($num > 10 && $num < 20) // если число принадлежит отрезку [11;19]
            return $num . " " . $pluralization['plural'];
        if ($num_x > 1 && $num_x < 5) // иначе если число оканчивается на 2,3,4
            return $num . " " . $pluralization['plural_2-4'];
        if ($num_x == 1) // иначе если оканчивается на 1
            return $num . " " . $pluralization['one'];
        return $num . " " . $pluralization['plural'];
    }

    /**
     * @param Deal $deal
     * @return Deal
     */
    public function setDealStatus(Deal $deal): Deal
    {

        $this->dealStatus->setStatus($deal);

        return $deal;
    }

    /**
     * @param $deals
     */
    public function setDealsStatus($deals)
    {
        foreach ($deals as $deal) {
            // @TODO Переделать после реализации отдельного класса для определения статуса (GP-706)
            $this->dealStatus->setStatus($deal);
        }
    }

    /**
     * @param array $deal_data
     * @param DealAgent $dealAgentCustomer
     * @param DealAgent $dealAgentContractor
     * @param DealAgent $dealOwner
     * @param Token|null $token
     * @return Deal
     * @throws \Exception
     */
    public function createDeal(
        array $deal_data,
        DealAgent $dealAgentCustomer,
        DealAgent $dealAgentContractor,
        DealAgent $dealOwner,
        Token $token = null
    ): Deal
    {
        try {
            // If no Throwable
            // Create new Deal object
            $deal = new Deal();
            $deal->setCreated(new \DateTime());
            // Fill Deal with data from $deal_data  // Can throw Exception
            $this->fillDealData($deal, $deal_data);
            $deal->setCustomer($dealAgentCustomer);
            $deal->setContractor($dealAgentContractor);
            $deal->setOwner($dealOwner);

            $this->entityManager->persist($deal);
            $this->entityManager->flush();
            // Set composite number (needs deal id)
            $deal->setNumber($this->getDealNumber($deal));
            $this->entityManager->persist($deal);
            // Устанавливаем способ получения денег продавцом (Payment)
            //(метод оплаты в GP нам не важен, так как оплата по нашим реквизитам, важен только номер сделки)

            // Create Payment object
            $payment = $this->paymentManager->createPaymentWithoutSaving($deal, $deal_data['amount']);

            if (array_key_exists('payment_method', $deal_data) && null !== $deal_data['payment_method']) {
                /** @var PaymentMethod $paymentMethod */
                $paymentMethod = $this->paymentMethodManager->getPaymentMethodById($deal_data['payment_method']);
                $payment->setPaymentMethod($paymentMethod);
                $this->paymentMethodManager->processingPaymentMethodNameForDeal($deal, $paymentMethod);
            }

            $deal->setPayment($payment);

            // Если сделка создается по партнерскому токену, то привязываем сделку к токену
            if ($token instanceof Token) {
                $deal->setDealToken($token);
            }

            $this->entityManager->persist($deal);
            $this->entityManager->persist($payment);

            $this->entityManager->flush();
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $deal;
    }

    /**
     * @param array $postData
     * @param Deal $deal
     * @param User $user
     * @return mixed
     * @throws LogicException
     * @throws OptimisticLockException
     */
    public function createOrGetCivilLawSubjectOrPaymentMethod(array $postData = [], Deal $deal, User $user)
    {
        //подготавливаем массив с разделенными данными и параметрами валидации;  может приходить как массив для создания новой сущности, так и id существующего
        $preparedData = $this->splitCivilLawSubjectAndPaymentMethodToArrayOrId($postData, $deal, $user);
        return $this->createCivilLawSubjectPaymentMethod($preparedData, $user, $deal);
    }

    /**
     * @param array $postData
     * @param Deal $deal
     * @param User $user
     * @return mixed
     * @throws LogicException
     * @throws OptimisticLockException
     */
    public function createOrGetPaymentMethod(array $postData = [], Deal $deal, User $user)
    {
        //подготавливаем массив с разделенными данными и параметрами валидации;  может приходить как массив для создания новой сущности, так и id существующего
        $preparedData = $this->splitPaymentMethodToArrayOrId($postData, $deal, $user);

        return $this->createPaymentMethod($preparedData, $user, $deal);
    }
    /**
     * @param array $postData
     * @param Deal $deal
     * @param User $user
     * @return mixed
     * @throws LogicException
     * @throws OptimisticLockException
     */
    public function createOrGetCivilLawSubject(array $postData = [], Deal $deal, User $user)
    {
        //подготавливаем массив с разделенными данными и параметрами валидации;  может приходить как массив для создания новой сущности, так и id существующего
        $preparedData = $this->splitCivilLawSubjectToArrayOrId($postData, $deal, $user);

        return $this->createCivilLawSubject($preparedData, $user, $deal);
    }

    /**
     * @param array $postData
     * @param Deal $deal
     * @param User $user
     * @return array
     * @throws \Exception
     */
    private function splitCivilLawSubjectToArrayOrId(array $postData = [], Deal $deal, User $user): array
    {
        $data = [
            'post_data' => [
                'payment_method' => null,
                'civil_law_subject' => null,
            ],
            'validation' => [
                'is_valid' => true,
                'payment_method' => null,
                'civil_law_subject' => null,
            ],
            'payment_method_id' => null,
            'civil_law_subject_id' => null,
        ];

        $csrf = $postData['csrf'] ?? null;

        $data['civil_law_subject_id'] = $postData['civil_law_subject'] ?? null;
        //если передали массив то проверяем и готовим данные для создания civilLawSubject
        if (isset($postData['civil_law_subject']) && \is_array($postData['civil_law_subject']) ) {
            $data['post_data']['civil_law_subject'] = $postData['civil_law_subject'];
            $data['post_data']['civil_law_subject']['csrf'] = $csrf;
            $data['civil_law_subject_id'] = null;
        }
        if (!array_key_exists('civil_law_subject', $postData)) {
            $data['post_data']['civil_law_subject'] = $postData;
            $data['post_data']['civil_law_subject']['csrf'] = $csrf;
            $data['civil_law_subject_id'] = null;
        }
        return $data;
    }

    /**
     * @param array $postData
     * @param Deal $deal
     * @param User $user
     * @return array
     * @throws LogicException
     * @throws \Exception
     */
    private function splitPaymentMethodToArrayOrId(array $postData = [], Deal $deal, User $user): array
    {
        $data = [
            'post_data' => [
                'payment_method' => null,
                'civil_law_subject' => null,
            ],
            'validation' => [
                'is_valid' => true,
                'payment_method' => null,
                'civil_law_subject' => null,
            ],
            'payment_method_id' => null,
            'civil_law_subject_id' => null,
        ];

        $csrf = $postData['csrf'] ?? null;
        $deal_agent_role = $this->getDealRole($user, $deal);
        //что бы не засетить customer платежный метод
        if (array_key_exists('payment_method_id', $postData) &&
            !$this->paymentPolitics->isDealAgentRoleAllowedToCreatePaymentMethodOnDealCreation($deal_agent_role)) {
            throw new LogicException(self::TRYING_TO_SET_PAYMENT_METHOD_TO_CUSTOMER_ON_DEAL_CREATE);
        }
        $data['payment_method_id'] = $postData['payment_method'] ?? null;
        //если передали массив то проверяем и готовим данные для создания paymentMethod
        if (isset($postData['payment_method']) && \is_array($postData['payment_method'])) {
            $data['post_data']['payment_method'] = $postData['payment_method'];
            $data['post_data']['payment_method']['csrf'] = $csrf;
            //обнуляем данные что бы в deal не сетились данные из массива
            $data['payment_method_id'] = null;
        }
        if (!array_key_exists('payment_method', $postData)) {
            $data['post_data']['payment_method'] = $postData;
            $data['post_data']['payment_method']['csrf'] = $csrf;
            $data['payment_method_id'] = null;
        }


        return $data;
    }

    /**
     * @param array $postData
     * @return array
     * @throws LogicException
     * payment method and payment method id was devided by us
     * existing keys id and array may exists
     * @throws \Exception
     */
    private function splitCivilLawSubjectAndPaymentMethodToArrayOrId(array $postData = [], Deal $deal, User $user)
    {
        $data = [
            'post_data' => [
                'payment_method' => null,
                'civil_law_subject' => null,
            ],
            'validation' => [
                'is_valid' => true,
                'payment_method' => null,
                'civil_law_subject' => null,
            ],
            'payment_method_id' => null,
            'civil_law_subject_id' => null,
        ];

        $csrf = $postData['csrf'] ?? null;
        $deal_agent_role = $this->getDealRole($user, $deal);
        //что бы не засетить customer платежный метод
        if (array_key_exists('payment_method', $postData) &&
            !$this->paymentPolitics->isDealAgentRoleAllowedToCreatePaymentMethodOnDealCreation($deal_agent_role)) {
            throw new LogicException(self::TRYING_TO_SET_PAYMENT_METHOD_TO_CUSTOMER_ON_DEAL_CREATE);
        }
        $data['payment_method_id'] = $postData['payment_method'] ?? null;
        //если передали массив то проверяем и готовим данные для создания paymentMethod
        if (array_key_exists('payment_method', $postData) && \is_array($postData['payment_method'])) {
            $data['post_data']['payment_method'] = $postData['payment_method'];
            $data['post_data']['payment_method']['csrf'] = $csrf;
            //обнуляем данные что бы в deal не сетились данные из массива
            $data['payment_method_id'] = null;

        }
        $data['civil_law_subject_id'] = $postData['civil_law_subject'] ?? null;
        //если передали массив то проверяем и готовим данные для создания civilLawSubject
        if (array_key_exists('civil_law_subject', $postData) && \is_array($postData['civil_law_subject'])) {
            $data['post_data']['civil_law_subject'] = $postData['civil_law_subject'];
            $data['post_data']['civil_law_subject']['csrf'] = $csrf;
            $data['civil_law_subject_id'] = null;
        }

        return $data;
    }

    /**
     * @param array $postData
     * @return array
     * @throws \Exception
     */
    public function prepareDeliveryToDeal(array $postData = []): array
    {
        $data = [
            'post_data' => [
                'delivery' => null,
            ],
            'validation' => [
                'delivery' => null,
                'is_valid' => true,
            ]
        ];
        $csrf = $postData['csrf'] ?? null;
        //если передали массив то проверяем и готовим данные для создания deliveryOrder
        if (array_key_exists('delivery', $postData) && \is_array($postData['delivery'])) {
            $data['post_data']['delivery'] = $this->deliveryOrderManager->prepareDeliveryData($postData['delivery'], $csrf);
        }

        return $data;
    }

    /**
     * @param array $postData
     * @return array
     * @throws \Exception
     */
    public function prepareDataToDeal(array $postData = [])
    {
        $data = [
            'post_data' => [
                'deal' => null,
                'payment_method' => null,
                'civil_law_subject' => null,
                'agent_email' => null,
                'counteragent_civil_law_subject_id' => null,
                'delivery' => null,
            ],
            'validation' => [
                'is_valid' => true,
                'deal' => null,
                'payment_method' => null,
                'civil_law_subject' => null,
                'delivery' => null,
                'custom' => null
            ],
            'payment_method_id' => null,
            'civil_law_subject_id' => null,
        ];

        if (array_key_exists('agent_email', $postData)) {
            $data['post_data']['agent_email'] = $postData['agent_email'];
        }
        if (array_key_exists('counteragent_civil_law_subject', $postData)) {
            $data['post_data']['counteragent_civil_law_subject_id'] = $postData['counteragent_civil_law_subject'];
        }

        $csrf = $postData['csrf'] ?? null;

        //что бы не засетить customer платежный метод
        if (array_key_exists('payment_method', $postData) && array_key_exists('deal_role', $postData) && $postData['deal_role'] === 'customer') {
            $postData['payment_method'] = null;
        }

        //если передали массив то проверяем и готовим данные для создания paymentMethod
        if (array_key_exists('payment_method', $postData) && \is_array($postData['payment_method'])) {
            $data['post_data']['payment_method'] = $postData['payment_method'];
            $data['post_data']['payment_method']['csrf'] = $csrf;
            //обнуляем данные что бы в deal не сетились данные из массива
            $postData['payment_method'] = null;
        }
        //если передали массив то проверяем и готовим данные для создания civilLawSubject
        if (array_key_exists('deal_civil_law_subject', $postData) && \is_array($postData['deal_civil_law_subject'])) {
            $data['post_data']['civil_law_subject'] = $postData['deal_civil_law_subject'];
            $data['post_data']['civil_law_subject']['csrf'] = $csrf;
            //обнуляем данные что бы в deal не сетились данные из массива
            $postData['deal_civil_law_subject'] = null;
        }

        //если передали массив то проверяем и готовим данные для создания deliveryOrder
        $deliveryData = $this->prepareDeliveryToDeal($postData);
        if ($deliveryData['post_data']['delivery']) {
            $data = array_merge_recursive($data, $deliveryData);
            if (isset($deliveryData['validation'])) {
                $data['validation']['is_valid'] = $deliveryData['validation']['is_valid'];
                $data['validation']['delivery'] = $deliveryData['validation']['delivery'];
            }

            //обнуляем данные чтобы в deal не сетились данные из массива
            $postData['delivery'] = null;
        }

        //приводим к нижнему регистру email
        if (array_key_exists('counteragent_email', $postData)) {
            $postData['counteragent_email'] = strtolower($postData['counteragent_email']);
        }

        $data['post_data']['deal'] = $postData;
        $data['payment_method_id'] = $postData['payment_method'] ?? null;
        $data['civil_law_subject_id'] = $postData['deal_civil_law_subject'] ?? null;

        return $data;
    }

    /**
     * @param $preparedData
     * @param $user
     * @return CivilLawSubjectSelectionForm|Form
     * @throws LogicException
     */
    private function getCivilLawSubjectFormAndSetData(&$preparedData, $user)
    {
        if ($preparedData['civil_law_subject_id']!==null) {
            $availableCivilLawSubjects = $this->civilLawSubjectManager->getAvailableCivilLawSubjects($user);
            $civilLawSubjectForm = new CivilLawSubjectSelectionForm($availableCivilLawSubjects);
            $civilLawSubjectForm->setData($preparedData);

        } else {

             $preparedData['post_data']['civil_law_subject']['civil_law_subject_type'] = !empty($preparedData['post_data']['civil_law_subject']['civil_law_subject_type']) ? $this->civilLawSubjectManager
                ->getCivilLawSubjectType($preparedData['post_data']['civil_law_subject']['civil_law_subject_type']) : null;
            if (!CivilLawSubjectPolitics::isExistPolitics($preparedData['post_data']['civil_law_subject']['civil_law_subject_type'])) {

                throw new LogicException(null, LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_TYPE);
            }
            /** @var Form $civilLawSubjectForm */
            $civilLawSubjectForm = $this->civilLawSubjectManager->getFormByCivilLawSubjectType($preparedData['post_data']['civil_law_subject']['civil_law_subject_type']);
            $civilLawSubjectForm->setData($preparedData['post_data']['civil_law_subject']);
        }

        return $civilLawSubjectForm;
    }

    /**
     * @param $preparedData
     * @param $user
     * @param $deal
     * @return PaymentMethodSelectionForm|Form
     * @throws LogicException
     * @throws \Exception
     */
    private function getPaymentMethodFormAndSetData($preparedData, $user, $deal)
    {

        if ($preparedData['payment_method_id']!==null) {
            $availablePaymentMethods = $this->paymentMethodManager->getAvailablePaymentMethods($user);
            $paymentMethodForm = new PaymentMethodSelectionForm($availablePaymentMethods);
            $paymentMethodForm->setData($preparedData);


        } else {
            //если невалидный тип прислан, или он неактивен
            if (empty($preparedData['post_data']['payment_method']['payment_method_type'])) {

                throw new LogicException(null, LogicException::PAYMENT_METHOD_TYPE_NOT_FOUND);
            }
            if (false === PaymentPolitics::isPaymentMethodTypeAvailable($deal, $preparedData['post_data']['payment_method']['payment_method_type'])) {

                throw new LogicException(null, LogicException::PAYMENT_METHOD_OF_THIS_TYPE_CANT_AVAILABLE_MESSAGE);
            }
            $paymentMethodType = $this->paymentMethodManager
                ->getPaymentMethodTypeByName($preparedData['post_data']['payment_method']['payment_method_type']);
            $paymentMethodStrategy = new PaymentMethodStrategy(null, $paymentMethodType);
            // Создаем форму нужного Payment метода через стратегию
            $paymentMethodForm = $paymentMethodStrategy->getForm();
            $paymentMethodForm->setData($preparedData['post_data']['payment_method']);
        }
        return $paymentMethodForm;
    }

    /**
     * @param $preparedData
     * @param $user
     * @param $deal
     * @return array|object
     * @throws LogicException
     * @throws OptimisticLockException
     */
    public function createCivilLawSubjectPaymentMethod($preparedData, $user, $deal)
    {
        $preparedData = $this->createCivilLawSubject($preparedData, $user, $deal);

        return $this->createPaymentMethod($preparedData, $user, $deal);
    }

    /**
     * @param $preparedData
     * @param $user
     * @param $deal
     * @return array|\Traversable
     * @throws LogicException
     * @throws OptimisticLockException
     */
    public function createPaymentMethod(array $preparedData, User $user, Deal $deal)
    {
        //логика работы данной функции, выбираем либо селект, или нужную нам форму,
        // и в зависимости от этого сразу сетим туда нужные данные
        $paymentMethodForm = $this->getPaymentMethodFormAndSetData($preparedData, $user, $deal);

        if (!$paymentMethodForm->isValid() ||  !$preparedData['validation']['is_valid']) {
            $preparedData['validation']['is_valid'] = false;
            $preparedData['validation']['payment_method'] = $paymentMethodForm->getMessages();
            return $preparedData;
        }

        //если форма селекта пришла и валидна, то ничего не создаем и прокидываем дальше
        if ($preparedData['payment_method_id'] != null) return $preparedData;
        $civilLawSubjectId = $preparedData['civil_law_subject_id'] ?? $preparedData['post_data']['payment_method']['civil_law_subject_id'];
        $civilLawSubject = $this->civilLawSubjectManager->getCivilLawSubjectForUser($user, $civilLawSubjectId);

        $paymentMethodFormData = $paymentMethodForm->getData();
        /** @var PaymentMethodType $paymentMethodType */
        $paymentMethodType = $this->paymentMethodManager
            ->getPaymentMethodTypeByName($preparedData['post_data']['payment_method']['payment_method_type']);
        $preparedData['payment_method_id'] = $this->createNewPaymentToDeal($deal, $paymentMethodFormData, $civilLawSubject, $paymentMethodType);
        if (empty($preparedData['payment_method_id'])) throw new LogicException(null, LogicException::PAYMENT_METHOD_TYPE_NOT_FOUND);

        return $preparedData;
    }

    /**
     * @param $preparedData
     * @param $user
     * @param $deal
     * @return array|object
     * @throws LogicException
     * @throws OptimisticLockException
     */
    public function createCivilLawSubject($preparedData, $user, $deal)
    {
        $civilLawSubjectForm = $this->getCivilLawSubjectFormAndSetData($preparedData, $user);
        if (!$civilLawSubjectForm->isValid()) {
            $preparedData['validation'] = [
                'is_valid' => false,
                'civil_law_subject' => $civilLawSubjectForm->getMessages()
            ];
            return $preparedData;
        }
        //если прошло валидацию и id существует, то прокидываем данные, иначе создаем
        if (!empty($preparedData['civil_law_subject_id'])) return $preparedData;

        $civilLawSubjectFormData = $civilLawSubjectForm->getData();

        $civilLawSubject = $this->setNewCivilLawSubjectForm($user, $civilLawSubjectFormData, $preparedData['post_data']['civil_law_subject']['civil_law_subject_type']);
        $preparedData['civil_law_subject_id'] = $civilLawSubject->getCivilLawSubject()->getId();

        return $preparedData;
    }

    /**
     * @param array $data
     * @return array
     * @throws OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getValidationDelivery(array $data): array
    {
        $delivery_data = $data['post_data']['delivery'];

        if ($delivery_data) {
            $validation_messages = $this->deliveryOrderManager->validateDeliveryOrderData($delivery_data);
            if (null !== $validation_messages && !empty($validation_messages)) {
                $data['validation']['is_valid'] = false;
                $data['validation']['delivery'] = $validation_messages;
            }
        }

        return $data;
    }

    /**
     * @param User|null $user
     * @param $data
     * @param Form $form |null
     * @return mixed
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws \Zend\Form\Exception\DomainException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws LogicException
     * @throws \Exception
     */
    public function creatingCivilLawSubjectForDeal($user, $data, Form $form = null)
    {
        if (null === $data['post_data']['civil_law_subject']) {
            // Если не указан, ничего не делаем
            return $data;
        }

        $post_data = $data['post_data']['civil_law_subject'];
        $validation = $this->getValidationCivilLawSubject($post_data);
        //сохраняем если валидно
        if ($validation['is_valid']) {
            $post_data['user'] = $user;
            //сохранение
            $result_saved = $this->civilLawSubjectManager
                ->saveFormData($post_data, $post_data['civil_law_subject_type']);

            /** @var CivilLawSubject $civilLawSubject */
            $civilLawSubject = $result_saved['civilLawSubject'];
            $data['civil_law_subject_id'] = $civilLawSubject->getId();
            //добавляем в данные сделки новый id
            if (array_key_exists('deal', $data['post_data']) && array_key_exists('deal_civil_law_subject', $data['post_data']['deal'])) {
                $data['post_data']['deal']['deal_civil_law_subject'] = $data['civil_law_subject_id'];
            }
            if ($form) {
                //что бы пройти валидацию необходимо в форму добавить новый id
                $options = $form->get('deal_civil_law_subject')->getOptions();
                $options['value_options'][$data['civil_law_subject_id']] = 'name';
                $form->get('deal_civil_law_subject')->setOptions($options);
            }
        } else {
            $data['validation']['is_valid'] = false;
            $data['validation']['civil_law_subject'] = $validation['messages'];
        }

        return $data;
    }

    /**
     * @param User $user
     * @param $formData
     * @return LegalEntity|NaturalPerson
     * @throws LogicException
     * @throws OptimisticLockException
     */
    private function setNewCivilLawSubjectForm(User $user, $formData,$civilLawSubjectId)
    {
        switch ($civilLawSubjectId) {
            case CivilLawSubjectManager::NATURAL_PERSON_NAME:
                $civilLawSubject = $this->civilLawSubjectManager->createNaturalPerson($user, $formData);
                break;
            case CivilLawSubjectManager::SOLE_PROPRIETOR_NAME:
                $civilLawSubject = $this->civilLawSubjectManager->createSoleProprietor($user, $formData);
                break;
            case CivilLawSubjectManager::LEGAL_ENTITY_NAME:
                $civilLawSubject = $this->civilLawSubjectManager->createLegalEntity($user, $formData);
                break;
           default:
                throw new LogicException(null, LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_TYPE);
        }

        return $civilLawSubject;
    }

    /**
     * @param $deal
     * @param $data
     * @param $civilLawSubject
     * @return int
     * @throws OptimisticLockException
     * @throws \Exception
     */
    private function createNewPaymentToDeal(Deal $deal, $formData,CivilLawSubject $civilLawSubject,$paymentMethodType)
    {

        $paymentMethod = $this->paymentMethodManager->addPaymentMethod($civilLawSubject,
            $paymentMethodType,
            $formData
        );
        $this->paymentMethodManager->processingPaymentMethodNameForDeal($deal, $paymentMethod);
        return $paymentMethod->getId();

    }

    /**
     * @param Deal $deal
     * @param User $user
     * @param $data
     * @return Deal
     * @throws OptimisticLockException
     * @throws \Exception
     */
    public function setCivilLawSubjectAndPaymentMethodToDeal(Deal $deal, User $user, $data): Deal
    {
        /** @var DealAgent $userDealAgent */
        $userDealAgent = $this->getUserAgentByDeal($user, $deal);
        $civilLawSubject = $this->civilLawSubjectManager->getCivilLawSubjectForUser($user,$data['civil_law_subject_id']);
        $deal->setUpdated(new \DateTime());
        $this->entityManager->persist($deal);
        $this->entityManager->flush();
        $this->dealAgentManager->setCivilLawSubjectToDealAgent($userDealAgent, $civilLawSubject);
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $paymentMethod = $payment->getPaymentMethod();
        $this->paymentMethodManager->processingPaymentMethodNameForDeal($deal, $paymentMethod);
        $this->paymentManager->setPaymentMethod($payment, $data['payment_method_id']);

        return $deal;
    }

    /**
     * @param Deal $deal
     * @param User $user
     * @param $data
     * @return Deal
     * @throws OptimisticLockException
     * @throws \Exception
     */
    public function setPaymentMethodToDeal(Deal $deal, User $user, $data): Deal
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $paymentMethod = $payment->getPaymentMethod();
        $this->paymentMethodManager->processingPaymentMethodNameForDeal($deal, $paymentMethod);
        $this->paymentManager->setPaymentMethod($payment, $data['payment_method_id']);

        return $deal;
    }

    /**
     * @param Deal $deal
     * @param User $user
     * @param $data
     * @return Deal
     * @throws OptimisticLockException
     * @throws \Exception
     */
    public function setCivilLawSubjectToDeal(Deal $deal, User $user, $data): Deal
    {
        /** @var DealAgent $userDealAgent */
        $userDealAgent = $this->getUserAgentByDeal($user, $deal);
        $civilLawSubject = $this->civilLawSubjectManager->getCivilLawSubjectForUser($user,$data['civil_law_subject_id']);
        $deal->setUpdated(new \DateTime());
        $this->entityManager->persist($deal);
        $this->entityManager->flush();
        $this->dealAgentManager->setCivilLawSubjectToDealAgent($userDealAgent, $civilLawSubject);

        return $deal;
    }

    /**
     * @param $data
     * @param Form $form
     * @return array
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws \Zend\Form\Exception\DomainException
     * @throws \Exception
     * @throws LogicException
     */
    public function creatingPaymentMethodForDeal($data, Form $form = null): array
    {
        if ($data['post_data']['payment_method'] === null) return $data;
        $post_data = $data['post_data']['payment_method'];
        $post_data['civil_law_subject_id'] = $data['civil_law_subject_id'];

        $validation = $this->getValidationPaymentMethod($post_data);

        //сохраняем если валидно
        if ($validation['is_valid']) {
            // Save data
            $resultPaymentMethod = $this->paymentMethodManager->saveFormData($post_data);
            /** @var PaymentMethod $paymentMethod */
            $paymentMethod = $resultPaymentMethod['paymentMethod'];
            $data['payment_method_id'] = $paymentMethod->getId();
            //добавляем в данные сделки новый id
            if (array_key_exists('deal', $data['post_data']) && array_key_exists('payment_method', $data['post_data']['deal'])) {
                $data['post_data']['deal']['payment_method'] = $data['payment_method_id'];
            }
            if ($form) {
                //что бы пройти валидацию необходимо в форму добавить новый id
                $options = $form->get('payment_method')->getOptions();
                $options['value_options'][$data['payment_method_id']] = 'name';
                $form->get('payment_method')->setOptions($options);
            }
        } else {
            $data['validation']['is_valid'] = false;
            $data['validation']['payment_method'] = $validation['messages'];
        }

        return $data;
    }

    /**
     * @param $data
     * @return array
     * @throws \Zend\Form\Exception\DomainException
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws LogicException
     */
    public function getValidationCivilLawSubject($data): array
    {
        $validation = [
            'is_valid' => true,
            'messages' => null
        ];

        //если не null то нужно валидировать
        if ($data) {
            //если нет ключа то формируем invalid form data
            if (!array_key_exists('civil_law_subject_type', $data)) {
                $data['civil_law_subject_type'] = '';
            }
            $type = $data['civil_law_subject_type'];
            if (empty($type)) {
                return [
                    'is_valid' => false,
                    'messages' => [
                        'civil_law_subject_type' => ['isEmpty' => 'Value is required and can\'t be empty']
                    ]
                ];
            }
            /** @var string $civilLawSubjectType */
            $civilLawSubjectType = $this->civilLawSubjectManager
                ->getCivilLawSubjectType($data['civil_law_subject_type']);
            /** @var Form $civilLawSubjectForm */
            $civilLawSubjectForm = $this->civilLawSubjectManager->getFormByCivilLawSubjectType($civilLawSubjectType);

            //check validation payment method
            $civilLawSubjectForm->setData($data);
            if (!$civilLawSubjectForm->isValid()) {
                $validation['messages'] = $civilLawSubjectForm->getMessages();
                $validation['is_valid'] = false;
            }
        }

        return $validation;
    }

    /**
     * @param $data
     * @return array
     * @throws \Zend\Form\Exception\DomainException
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws LogicException
     * @throws \Exception
     */
    public function getValidationPaymentMethod($data): array
    {
        $validation = [
            'is_valid' => true,
            'messages' => null
        ];

        //если не null то нужно валидировать
        if ($data) {
            //если нет ключа то формируем invalid form data
            if (!array_key_exists('payment_method_type', $data)) {
                throw new LogicException(null, LogicException::DEAL_CREATE_BAD_DATA_PROVIDED);
            }
            $type = $data['payment_method_type'];
            if (empty($type)) {
                return [
                    'is_valid' => false,
                    'messages' => [
                        'payment_method_type' => ['isEmpty' => 'Value is required and can\'t be empty']
                    ]
                ];
            }
            /** @var PaymentMethodType $paymentMethod */
            $paymentMethodType = $this->paymentMethodManager
                ->getPaymentMethodTypeByName($type);

            /** @var PaymentMethodStrategy $paymentMethodStrategy */
            $paymentMethodStrategy = new PaymentMethodStrategy(null, $paymentMethodType);
            // Создаем форму нужного Payment метода через стратегию
            $paymentMethodForm = $paymentMethodStrategy->getForm();

            //check validation payment method
            $paymentMethodForm->setData($data);
            if (!$paymentMethodForm->isValid()) {
                $validation['messages'] = $paymentMethodForm->getMessages();
                $validation['is_valid'] = false;
            }
        }

        return $validation;
    }

    /**
     * @param $data
     * @param Form $form
     * @return mixed
     */
    public function getValidationDealForm($data, Form $form)
    {
        //валидация сделки
        $form->setData($data['post_data']['deal']);
        if ($form->isValid()) {
            $data['post_data']['deal'] = $form->getData();
        } else {
            $data['validation']['is_valid'] = false;
            $data['validation']['deal'] = $form->getMessages();
        }

        return $data;
    }

    public function checkAvalibleMethodsForUser($postData, $availablePaymentMethods, $availableCivilLawSubjects)
    {
        if (!array_key_exists($postData['payment_method'], $availablePaymentMethods) && !is_array($postData['payment_method'])) {
            return [' civil_law_subject_type' => self::FORBIDDEN_EDIT_ROLE_NOT_OWNER];
        }
        if (!$availableCivilLawSubjects) {
            return [' civil_law_subject_type' => self::FORBIDDEN_EDIT_ROLE_NOT_OWNER];
        }

        return false;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @param array $dealAgents
     * @param array $data
     * @return array
     */
    public function prepareDataToEditDeal(User $user, Deal $deal, array $dealAgents, array $data)
    {
        /** @var DealAgent $counterAgent */
        $counterAgent = $dealAgents['counterAgent'];
        $isOwner = $deal->getOwner()->getUser() === $user;
        //если редактирует не владелец
        if (!$isOwner) {
            if (array_key_exists('deal_role', $data['post_data']['deal'])
                && $data['post_data']['deal']['deal_role'] !== $dealAgents['deal_role']) {
                $data['validation']['is_valid'] = false;
                $data['validation']['custom']['deal_role'] = [
                    'forbiddenEdit' => [self::FORBIDDEN_EDIT_ROLE_NOT_OWNER]
                ];
            }
            if (array_key_exists('counteragent_email', $data['post_data']['deal'])
                && $data['post_data']['deal']['counteragent_email'] !== strtolower($counterAgent->getEmail())) {
                $data['validation']['is_valid'] = false;
                $data['validation']['custom']['counteragent_email'] = [
                    'forbiddenEdit' => [self::FORBIDDEN_EDIT_EMAIL_NOT_OWNER]
                ];
            }
            // Роль Не владельцу менять нельзя, поэтому присваиваем текущую
            $data['post_data']['deal']['deal_role'] = $dealAgents['deal_role'];
            // Если редактирует НЕ владелец, то в форме нет поля для контрагента. Присваиваем текущее значание.
            $data['post_data']['deal']['counteragent_email'] = strtolower($counterAgent->getEmail());
        }

        return $data;
    }

    /**
     * @param array $validationData
     * @param bool $is_deal_data
     * @return array|mixed
     */
    public function getProcessingInvalidFormDataForCivilLawSubjectOrPaymentMethod(array $validationData = [], $is_deal_data = true)
    {
        $messages = [];

        //2 добавляем сообщения валидации civil_law_subject в результируюший массив
        if (array_key_exists('civil_law_subject', $validationData) && $validationData['civil_law_subject']) {
            $deal_key = 'civil_law_subject';

            $messages[$deal_key] = $validationData['civil_law_subject'];
            //csrf передается во все формы одинаковый поэтому
            //в этом массиве он будет излишнем
            if (array_key_exists('csrf', $validationData['civil_law_subject'])) {
                $messages['csrf'] = $validationData['civil_law_subject']['csrf'];
                unset($messages[$deal_key]['csrf']);
                //после удаления csrf сообщения массив может оказаться пустым поэтому
                //проверяем и чистим если нужно
                if (empty($messages[$deal_key])) {
                    unset($messages[$deal_key]);
                }
            }
        }
        //3 добавляем сообщения валидации payment_method в результируюший массив
        if (array_key_exists('payment_method', $validationData) && $validationData['payment_method']) {
            $messages['payment_method'] = $validationData['payment_method'];
            //csrf передается во все формы одинаковый поэтому
            //в этом массиве он будет излишнем
            if (array_key_exists('csrf', $validationData['payment_method'])) {
                $messages['csrf'] = $validationData['payment_method']['csrf'];
                unset($messages['payment_method']['csrf']);
                //после удаления csrf сообщения массив может оказаться пустым поэтому
                //проверяем и чистим если нужно
                if (empty($messages['payment_method'])) {
                    unset($messages['payment_method']);
                }
            }
        }

        //4 добавляем сообщения валидации delivery в результируюший массив
        if (array_key_exists('delivery', $validationData) && $validationData['delivery']) {
            $messages = $validationData['delivery'];
            //csrf передается во все формы одинаковый поэтому
            //в этом массиве он будет излишнем
            if (array_key_exists('csrf', $validationData['delivery'])) {
                $messages['csrf'] = $validationData['delivery']['csrf'];
                unset($messages['delivery']['csrf']);
                //после удаления csrf сообщения массив может оказаться пустым поэтому
                //проверяем и чистим если нужно
                if (empty($messages['delivery'])) {
                    unset($messages['delivery']);
                }
            }
        }

        //5 произвольная валидация
        if (array_key_exists('custom', $validationData) && $validationData['custom']) {
            $messages = array_merge($messages, $validationData['custom']);
        }

        return $messages;
    }

    /**
     * @param array $validationData
     * @param bool $is_deal_data
     * @return array|mixed
     */
    public function getProcessingInvalidFormData(array $validationData = [], $is_deal_data = true)
    {
        $messages = [];

        //1 добавляем сообщения валидации deal в результируюший массив
        if (array_key_exists('deal', $validationData) && $validationData['deal']) {
            $messages = $validationData['deal'];
        }
        //2 добавляем сообщения валидации delivery в результируюший массив
        if (array_key_exists('delivery', $validationData) && $validationData['delivery']) {
            $messages['delivery'] = $validationData['delivery'];
            //csrf передается во все формы одинаковый поэтому
            //в этом массиве он будет излишнем
            if (array_key_exists('csrf', $validationData['delivery'])) {
                $messages['csrf'] = $validationData['delivery']['csrf'];
                unset($messages['delivery']['csrf']);
                //после удаления csrf сообщения массив может оказаться пустым поэтому
                //проверяем и чистим если нужно
                if (empty($messages['delivery'])) {
                    unset($messages['delivery']);
                }
            }
        }

        //3 добавляем сообщения валидации civil_law_subject в результируюший массив
        if (array_key_exists('civil_law_subject', $validationData) && $validationData['civil_law_subject']) {
            $deal_key = $is_deal_data ? 'deal_civil_law_subject' : 'civil_law_subject';

            $messages[$deal_key] = $validationData['civil_law_subject'];
            //csrf передается во все формы одинаковый поэтому
            //в этом массиве он будет излишнем
            if (array_key_exists('csrf', $validationData['civil_law_subject'])) {
                $messages['csrf'] = $validationData['civil_law_subject']['csrf'];
                unset($messages[$deal_key]['csrf']);
                //после удаления csrf сообщения массив может оказаться пустым поэтому
                //проверяем и чистим если нужно
                if (empty($messages[$deal_key])) {
                    unset($messages[$deal_key]);
                }
            }
        }
        //4 добавляем сообщения валидации payment_method в результируюший массив
        if (array_key_exists('payment_method', $validationData) && $validationData['payment_method']) {
            $messages['payment_method'] = $validationData['payment_method'];
            //csrf передается во все формы одинаковый поэтому
            //в этом массиве он будет излишнем
            if (array_key_exists('csrf', $validationData['payment_method'])) {
                $messages['csrf'] = $validationData['payment_method']['csrf'];
                unset($messages['payment_method']['csrf']);
                //после удаления csrf сообщения массив может оказаться пустым поэтому
                //проверяем и чистим если нужно
                if (empty($messages['payment_method'])) {
                    unset($messages['payment_method']);
                }
            }
        }
        //5 произвольная валидация
        if (array_key_exists('custom', $validationData) && $validationData['custom']) {
            $messages['custom'] = array_merge($messages, $validationData['custom']);
        }

        return $messages;
    }

    /**
     * @param array $validationData
     * @return array
     */
    public function getProcessingInvalidTokenData(array $validationData = [])
    {
        $messages = [];

        // 1 добавляем сообщения валидации counteragent_civil_law_subject в результируюший массив
        if (array_key_exists('counteragent_civil_law_subject', $validationData) && $validationData['counteragent_civil_law_subject']) {
            $messages['counteragent_civil_law_subject'] = array_merge($messages, $validationData['counteragent_civil_law_subject']);
        }

        // 2 произвольная валидация
        if (array_key_exists('custom', $validationData) && $validationData['custom']) {
            $messages = array_merge($messages, $validationData['custom']);
        }

        return $messages;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @param array $post_data
     * @param array $deal_agents
     * @return Deal
     * @throws \Exception
     */
    public function editDeal(User $user, Deal $deal, array $post_data, array $deal_agents)
    {
        try {
            /** @var Payment $payment */
            $payment = $deal->getPayment();

            // Если поменялись роли, то меняем местами DealAgents
            if ($post_data['deal_role'] !== $deal_agents['deal_role']) {
                // Меняем местами DealAgents // Can throw Exception
                $this->dealAgentManager->swapDealAgents($deal, $deal_agents['currentUserAgent']);
            }

            // Получаем распределение DealAgents после ротации
            $deal_agents = $this->getDealAgentsRelativeToCurrentUser($user, $deal);

            /** @var DealAgent $currentUserDealAgent */
            $currentUserDealAgent = $deal_agents['currentUserAgent'];

            ///// step 1. Fill Deal with simple data from $deal_data /////
            $this->fillDealData($deal, $post_data);

            // $currentUserDealAgent->setDealAgentOnceConfirm(true);
            $this->entityManager->persist($currentUserDealAgent);

            ///// step 2. set new CivilLawSubject when editing deal /////
            $this->setNewCivilLawSubjectWhenEditingDeal($currentUserDealAgent, $post_data);

            // Если изменился email 'counteragent_email'.
            // Если новый email принадлежит тому же пользователю, то ничего не делаем (при наличии User у DealAgent).
            // Если User нет, или email не принадлежит User, то считаем, что это email другого пользователя.
            if ($this->isDealAgentMustBeRecreated($deal_agents['counterAgent'], $post_data['counteragent_email'])) {
                // Создаем нового агента для контрагента
                $this->dealAgentManager->replaceCounteragentDealAgent($deal, $deal_agents, $post_data['counteragent_email']);
            }

            $deal->setNumber($this->getDealNumber($deal));

            $this->entityManager->persist($deal);

            // Recalculate and set Payment properties
            /** @var FeePayerOption $feePayerOption */
            $feePayerOption = $this->getFeePayerOptionById($post_data['fee_payer_option']);

            $this->paymentManager->setCalculatedPaymentData(
                $payment,
                $post_data['amount'],
                $feePayerOption
            );

            // @TODO Подумать над названием метода
            $this->paymentManager->setPaymentMethodWithoutSaving(
                $deal,
                $currentUserDealAgent,
                $post_data['payment_method']
            );

            $this->entityManager->persist($payment);
            $this->entityManager->flush();
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $deal;
    }

    /**
     * @param DealAgent $dealAgent
     * @param string $counteragent_email
     * @return bool
     */
    private function isDealAgentMustBeRecreated(DealAgent $dealAgent, string $counteragent_email): bool
    {
        /** @var User|null $user */
        $user = $dealAgent->getUser();

        if (null !== $user) {
            /** @var Email $email */
            $email = $user->getEmail();

            if (strtolower($email->getEmail()) === strtolower($counteragent_email)) {

                return false;
            }
        }

        if (strtolower($dealAgent->getEmail()) === strtolower($counteragent_email)) {

            return false;
        }

        return true;
    }

    /**
     * @param DealAgent $dealAgent
     * @param $post_data
     * @return bool
     * @throws LogicException
     */
    public function setNewCivilLawSubjectWhenEditingDeal(DealAgent $dealAgent, $post_data): bool
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $dealAgent->getCivilLawSubject();
        $civil_law_subject_id = $civilLawSubject ? $civilLawSubject->getId() : null;

        // Если изменен CivilLawSubject, изменить у текущего агента.
        if ($post_data['deal_civil_law_subject'] && $civil_law_subject_id !== $post_data['deal_civil_law_subject']) {
            try {
                // Can throw Exception
                $this->dealAgentManager->setCivilLawSubjectById($dealAgent, $post_data['deal_civil_law_subject']);
            } catch (\Throwable $t) {

                throw new LogicException($t->getMessage());
            }
        }

        return true;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealConfirmedByDealAgents(Deal $deal)
    {
        return $this->dealPolitics->isDealConfirmedByDealAgents($deal);
    }

    /**
     * @param Deal $deal
     * @param User $user
     * @return bool
     */
    public function isUserPartOfDial(Deal $deal, User $user)
    {
        return $deal->getCustomer()->getUser() === $user || $deal->getContractor()->getUser() === $user;
    }

    /**
     * @param Deal $deal
     * @param User|null $user
     * @param Token|null $token
     * @return bool
     */
    public function isDealAllowedToBeConfirmByCurrentUser(Deal $deal, User $user = null, Token $token = null)
    {
        try {
            $this->checkDealAllowedToBeConfirmByCurrentUser($deal, $user, $token);

            return true;
        }
        catch (\Throwable $t) {
            #logMessage($t->getMessage(), 'deal-confirm-errors');
            return false;
        }
    }

    /**
     * @param Deal $deal
     * @param User|null $user
     * @param Token|null $token
     * @return bool
     * @throws \Exception
     */
    public function checkDealAllowedToBeConfirmByCurrentUser(Deal $deal, User $user = null, Token $token = null): bool
    {
        // Если сделка создается по токену с партнерского сайта
        if (null === $user && null !== $token) {

            return true;
        }

        // Проверяем можно ли вносить изменения в сделку.
        if ($this->dealPolitics->isDealConfirmedByDealAgents($deal)) {

            throw new \Exception(self::ERROR_ALREADY_CONFIRMED_BY_BOTH_DEAL_AGENTS);
        }

        /** @var DealAgent $dealAgent */ // Can throw Exception
        $dealAgent = $this->getUserAgentByDeal($user, $deal);
        if (!$this->dealPolitics->isCorrectCivilLawSubjectForDealAgent($dealAgent)) {

            throw new \Exception(self::CIVIL_LAW_SUBJECT_FILLED_IN_INCORRECTLY);
        }

        // Если данный DealAgent уже подтвердил условия, то ничего не делаем
        if ($dealAgent->getDealAgentConfirm()) {

            throw new \Exception(self::ERROR_ALREADY_CONFIRMED_BY_SPECIFIC_DEAL_AGENT);
        }

        // Если доставка заполнена некорректно
        if ($this->isNeedToFillDeliveryInDeal($deal, $user)) {

            throw new \Exception(self::DELIVERY_FILLED_IN_INCORRECTLY);
        }

        // Проверяем готовы ли связанные со сделкой сущности текущего пользователя
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $deal_data = [];
        $deal_data['deal_role'] = $this->getDealRole($user, $deal);
        // Создаем csrf токен для дальнейшей проверки (валидация CivilLawSubject и PaymentMethod)
        $csrfElement = new Element\Csrf('csrf');
        $deal_data['csrf'] = $csrfElement->getValue();

        // If Deal is not fully ready for deal agent confirmation - throw Exception.
        $this->dealPolitics->isDealReadyForConfirmation($payment, $dealAgent, $deal_data);

        return true;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @param Form $form
     * @return bool
     */
    public function disallowEditFieldsDealFormForNotOwner(User $user, Deal $deal, Form $form)
    {
        if ($deal->getOwner()->getUser() !== $user) {
            $form->get('deal_role')->setAttribute('disabled', 'disabled');
            $form->get('counteragent_email')->setAttribute('disabled', 'disabled');
        }

        return true;
    }

    /**
     * @param Deal $deal
     * @param Form $form
     * @param array $dealAgents
     */
    public function setDealFormDataWhenEditing(Deal $deal, Form $form, array $dealAgents)
    {
        /** @var DealAgent $currentUserDealAgent |$counteragentDealAgent */
        $currentUserDealAgent = $dealAgents['currentUserAgent'];
        $counteragentDealAgent = $dealAgents['counterAgent'];
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $currentUserDealAgent->getCivilLawSubject();
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Set form data
        $data = [];
        $data['deal_role'] = $dealAgents['deal_role'];
        $data['deal_civil_law_subject'] = $civilLawSubject ? $civilLawSubject->getId() : null;
        $data['counteragent_email'] = strtolower($counteragentDealAgent->getEmail());

        $data['name'] = $deal->getName();
        $data['deal_type'] = $deal->getDealType()->getId();
        $data['fee_payer_option'] = $deal->getFeePayerOption()->getId();
        $data['delivery_period'] = $deal->getDeliveryPeriod();
        $data['addon_terms'] = $deal->getAddonTerms();
        $data['amount'] = $payment->getDealValue();
        $data['payment_method'] = $payment->getPaymentMethod() ? $payment->getPaymentMethod()->getId() : null;

        $form->setData($data);
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function confirmConditions(Deal $deal, User $user = null): bool
    {
        if (null !== $user) {
            /** @var DealAgent $dealAgent */ // Can throw Exception
            $dealAgent = $this->getUserAgentByDeal($user, $deal);
        } else {
            /** @var DealAgent $dealAgent */
            $dealAgent = $deal->getOwner();
        }

        // Подтверждаем условия
        $dealAgent->setDealAgentConfirm(true);
        $dealAgent->setDealAgentOnceConfirm(true);
        // Set current date as DateOfConfirm of DealAgent
        $dealAgent->setDateOfConfirm(new \DateTime());

        if ($this->isConfirmed($deal)) {
            /** @var Payment $payment */
            $payment = $deal->getPayment();
            /** @var \DateTime $plannedDate */
            $plannedDate = $this->paymentManager->getPlannedDateForPayment($payment, $deal->getDeliveryPeriod());
            // Устанавливаем дед-лайн для оплаты
            $payment->setPlannedDate($plannedDate);
            // Устанавливаем дату конфёрма сделки
            $deal->setDateOfConfirm(new \DateTime());
        }

        // Изменяем свойство Updated в том числе и для того, что Listener отреагировал на подтверждение
        $deal->setUpdated(new \DateTime());

        #$this->entityManager->persist($dealAgent);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Deal $deal
     * @param array $deal_data
     * @return Deal
     * @throws \Exception
     */
    private function fillDealData(Deal $deal, array $deal_data)
    {
        try {
            // Тип сделки (Товар/Услуга) // Can throw Exception
            $dealType = $this->getDealTypeById($deal_data['deal_type']);

            $deal->setName($deal_data['name']);
            $deal->setDealType($dealType);
            // Нужно, чтобы объект Deal всегда попадал в DealConditionsListener
            // (в массив измененных объектов OnFlushEventArgs)
            $deal->setUpdated(new \DateTime());
            // Записываем доп условия (пользовательский текст)
            $deal->setAddonTerms($deal_data['addon_terms']);
            // Записываем дедлайн по сделке, в днях
            $deal->setDeliveryPeriod((int)$deal_data['delivery_period']);
            // Устанавливаем кто платит комиссию (покупатель, контрактор, 50/50) // Can throw Exception!
            $feePayerOption = $this->getFeePayerOptionById($deal_data['fee_payer_option']);
            // If no Throwable
            $deal->setFeePayerOption($feePayerOption);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage() . ' in fillDealData');
        }

        return $deal;
    }

    /**
     * @param int $id
     * @return null|object
     * @throws \Exception
     */
    public function getDealTypeById(int $id)
    {
        return $this->dealTypeManager->getDealTypeById($id);
    }

    /**
     * Returns collection of Deal Type objects
     *
     * @return array
     */
    public function getDealTypes()
    {
        return $this->dealTypeManager->getDealTypes();
    }

    /**
     * Returns associative array of Deal Types names
     *
     * @return array
     */
    public function getDealTypesAsArray()
    {
        return $this->dealTypeManager->getDealTypesAsArray();
    }

    /**
     * @param int $id
     * @return null|object
     * @throws \Exception
     */
    public function getFeePayerOptionById(int $id)
    {
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)->find($id);

        if (!$feePayerOption || !($feePayerOption instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with such id found');
        }

        return $feePayerOption;
    }

    /**
     * @return array
     */
    public function getActiveFeePayerOptionsArray(): array
    {
        $feePayerOptions = $this->getFeePayerOptions();

        $fee_payer_options_array = [];
        /** @var FeePayerOption $feePayerOption */
        foreach ($feePayerOptions as $feePayerOption) {
            $fee_payer_options_array[] = [
                'id' => $feePayerOption->getId(),
                'name' => $feePayerOption->getName(),
                'description' => $feePayerOption->getDescription()
            ];
        }

        return $fee_payer_options_array;
    }

    /**
     * @return array
     */
    public function getFeePayerOptions()
    {
        $feePayerOptions = $this->entityManager->getRepository(FeePayerOption::class)
            ->findBy(['isActive' => true]);

        return $feePayerOptions;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getCivilLawSubjectsAsArray(): array
    {
        $requestedUser = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

        $naturalPersons = $this->entityManager->getRepository(CivilLawSubject::class)
            ->getNaturalPersonsByUserId($requestedUser->getId());
        $soleProprietors = $this->entityManager->getRepository(CivilLawSubject::class)
            ->getSoleProprietorsByUserId($requestedUser->getId());
        $legalEntities = $this->entityManager->getRepository(CivilLawSubject::class)
            ->getLegalEntitiesByUserId($requestedUser->getId());

        $civil_law_subjects_array = [];
        //collect legalEntities array
        /** @var LegalEntity $legalEntity */
        foreach ($legalEntities as $legalEntity) {
            $civil_law_subjects_array[$legalEntity->getCivilLawSubject()->getId()] = $legalEntity->getName();
        }
        //collect naturalPersons array
        /** @var NaturalPerson $naturalPerson */
        foreach ($naturalPersons as $naturalPerson) {
            $civil_law_subjects_array[$naturalPerson->getCivilLawSubject()->getId()] =
                $naturalPerson->getLastName() . ' ' . $naturalPerson->getFirstName() . ' ' . $naturalPerson->getSecondaryName();
        }
        //collect soleProprietors array
        /** @var SoleProprietor $soleProprietor */
        foreach ($soleProprietors as $soleProprietor) {
            $civil_law_subjects_array[$soleProprietor->getCivilLawSubject()->getId()] = $soleProprietor->getName();
        }

        return $civil_law_subjects_array;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return mixed
     * @throws \Exception
     */
    public function getDealAgentsRelativeToCurrentUser(User $user, Deal $deal)
    {
        if ($user === $deal->getCustomer()->getUser()) {
            $dealAgents['currentUserAgent'] = $deal->getCustomer();
            $dealAgents['counterAgent'] = $deal->getContractor();
            $dealAgents['deal_role'] = Deal::CUSTOMER;
        } elseif ($user === $deal->getContractor()->getUser()) {
            $dealAgents['currentUserAgent'] = $deal->getContractor();
            $dealAgents['counterAgent'] = $deal->getCustomer();
            $dealAgents['deal_role'] = Deal::CONTRACTOR;
        } else {
            throw new \Exception('The current user does not relate to any of the DealAgents');
        }

        return $dealAgents;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return DealAgent
     *
     * Используется в DealConditionsListener
     */
    public function getUserCounterAgentByDeal(User $user, Deal $deal): DealAgent
    {
        /** @var DealAgent $counterAgent */
        if ($user === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        return $counterAgent;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return DealAgent
     * @throws \Exception
     *
     * Используется в DealController
     */
    public function getUserAgentByDeal(User $user, Deal $deal)
    {
        if ($user === $deal->getCustomer()->getUser()) {
            $dealAgent = $deal->getCustomer();
        } elseif ($user === $deal->getContractor()->getUser()) {
            $dealAgent = $deal->getContractor();
        } else {

            throw new \Exception('The user does not match either the Customer or the Contractor');
        }

        return $dealAgent;
    }

    /**
     * @param Deal $deal
     * @param $email
     * @return DealAgent
     * @throws LogicException
     */
    public function getDealCounterAgentByDealAndInvitationEmail(Deal $deal, $email)
    {
        if ($email === $deal->getContractor()->getEmail()) {
            $dealAgent = $deal->getCustomer();
        } elseif ($email === $deal->getCustomer()->getEmail()) {
            $dealAgent = $deal->getContractor();
        } else {
            throw new LogicException(null, LogicException::DEAL_AGENTS_DOES_NOT_MATCH_EMAIL_PROVIDED);
        }
        return $dealAgent;
    }

    /**
     * Обрабатывает все истекшие сделки или одну, если передан id
     * Возвращает false, если нечего обрабатывать и true, если обработана хотя бы одна сделка.
     *
     * @param int $deal_id
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function expiredDealsProcessing($deal_id = 0)
    {
        /** @var SystemPaymentDetails $systemPaymentDetail */
        $systemPaymentDetail = $this->settingManager->getSystemPaymentDetail();

        $gp_account = $systemPaymentDetail->getAccountNumber();

        /** @var array $payments */
        $payments = $this->entityManager->getRepository(Payment::class)
            ->selectSortedPaymentsWithExpiredDeals([], $gp_account, $deal_id);

        if (!empty($payments)) {
            /** @var Payment $payment */
            foreach ($payments as $payment) {
                $this->creditPaymentOrderAvailabilityListenerEventHandler($payment->getDeal());
            }

            return true;
        }
        return false;
    }

    /**
     * @param $deal
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function overpaidDealsProcessing(Deal $deal)
    {
        /** @var SystemPaymentDetails $systemPaymentDetail */
        $systemPaymentDetail = $this->settingManager->getSystemPaymentDetail();

        $gp_account = $systemPaymentDetail->getAccountNumber();

        /** @var array $payments */
        $payments = $this->entityManager->getRepository(Payment::class)
            ->selectPaymentsWithOverpaidDeals($deal->getId(), $gp_account);

        if (!empty($payments)) {
            /** @var Payment $payment */
            foreach ($payments as $payment) {
                ///// создаем сущность переплаты и запускаем создание платежки /////
                $this->repaidOverpayManager->createRepaidOverpay($payment);
            }

            return true;
        }

        return false;
    }

    /**
     * Проверка что сделка подтверждена
     * @param Deal $deal
     * @return bool
     */
    public function isConfirmed(Deal $deal)
    {
        return $deal->getContractor()->getDealAgentConfirm() && $deal->getCustomer()->getDealAgentConfirm();
    }

    /**
     * @param Deal|null $deal
     * @param User|null $user
     * @return bool
     */
    public static function isUserPartOfDeal(Deal $deal = null, User $user = null): bool
    {
        return $user && $deal && ($deal->getCustomer()->getUser() === $user || $deal->getContractor()->getUser() === $user);
    }

    /**
     * @param Deal|null $deal
     * @param User|null $user
     * @return bool
     */
    public static function isUserOwnerOfDeal(Deal $deal = null, User $user = null): bool
    {
        return $user && $deal && ($deal->getOwner()->getUser() === $user);
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealRemovingAllowed(Deal $deal): bool
    {
        if (!$this->isConfirmed($deal)) {

            return true;
        }

        return false;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealEditingAllowed(Deal $deal)
    {
        if (!$this->isConfirmed($deal)) {

            return true;
        }

        return false;
    }

    /**
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    public function remove(Deal $deal): bool
    {
        if ($this->isDealRemovingAllowed($deal)) {

            try {
                /** @var Payment $payment */
                $payment = $this->paymentManager->getPaymentByDealId($deal->getId());

                $this->entityManager->remove($payment);
                $this->entityManager->remove($deal->getCustomer());
                $this->entityManager->remove($deal->getContractor());
                $this->deliveryOrderManager->remove($deal->getDeliveryOrder());

                $this->entityManager->remove($deal);
                $this->entityManager->flush();

                return true;

            } catch (\Throwable $t) {

                throw new \Exception('The Deal can not be deleted (' . $t->getMessage() . ')');
            }

        }

        throw new \Exception('Trying to delete a non-existent deal');
    }

    /**
     * Клонирование подчиненных сущностей
     * @param Deal $deal
     * @return mixed
     * @throws \Exception
     */
    public function cloneSubordinateEntities(Deal $deal)
    {
        if (!$this->isConfirmed($deal)) {
            throw new \Exception('Deal not confirm');
        }

        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        /** @var CivilLawSubject $oldContractorCivilLawSubject */
        $oldContractorCivilLawSubject = $contractor->getCivilLawSubject();
        /** @var CivilLawSubject $oldCustomerCivilLawSubject */
        $oldCustomerCivilLawSubject = $customer->getCivilLawSubject();

        /** @var bool $contractorExistCivilLawSubject */
        $contractorExistCivilLawSubject = (bool)$oldContractorCivilLawSubject;
        /** @var bool $customerExistCivilLawSubject */
        $customerExistCivilLawSubject = (bool)$oldCustomerCivilLawSubject;

        if ($contractorExistCivilLawSubject) {
            if ($oldContractorCivilLawSubject->getIsPattern() == false) {
                throw new \Exception('ContractorCivilLawSubject is clone');
            }
            /** @var CivilLawSubject $newContractorCivilLawSubject */
            $newContractorCivilLawSubject = $this->civilLawSubjectManager->clone($oldContractorCivilLawSubject);
            // Set new cloned ContractorCivilLawSubject to DealAgent
            $contractor->setCivilLawSubject($newContractorCivilLawSubject);
        }

        if ($customerExistCivilLawSubject) {
            if ($oldCustomerCivilLawSubject->getIsPattern() == false) {
                throw new \Exception('CustomerCivilLawSubject is clone');
            }
            /** @var CivilLawSubject $newCustomerCivilLawSubject */
            $newCustomerCivilLawSubject = $this->civilLawSubjectManager->clone($oldCustomerCivilLawSubject);
            // Set new cloned CustomerCivilLawSubject to DealAgent
            $customer->setCivilLawSubject($newCustomerCivilLawSubject);
        }

        // Если у двух агентов не было CivilLawSubject, что то не так
        if (!$contractorExistCivilLawSubject && !$customerExistCivilLawSubject) {
            throw new \Exception('contractor and customer not found CivilLawSubject');
        }

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $payment->getPaymentMethod();
        $setCivilLawSubject = null;

        if ($contractorExistCivilLawSubject) {
            $setCivilLawSubject = $newContractorCivilLawSubject;
        }

        // paymentMethodCivilLawSubject не принадлежал ни $customer ни $contractor, что то не так
        if ($setCivilLawSubject === null) {

            throw new \Exception('paymentMethodCivilLawSubject error');
        }

        $newPaymentMethod = $this->paymentMethodManager->clone($paymentMethod, $setCivilLawSubject);
        $payment->setPaymentMethod($newPaymentMethod);

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return string
     * @throws \Exception
     */
    public function getDealRole(User $user, Deal $deal): string
    {
        if ($user === $deal->getCustomer()->getUser()) {
            return 'customer';
        }

        if ($user === $deal->getContractor()->getUser()) {
            return 'contractor';
        }

        throw new \Exception(self::ERROR_USER_NOT_RELATED_TO_DEAL);
    }

    /**
     * @param User|null $user
     * @param Deal|null $deal
     * @return bool
     */
    public static function isUserCustomerOfDeal(User $user = null, Deal $deal = null): bool
    {
        return $user && $deal && $user === $deal->getCustomer()->getUser();
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    public function getMinDeliveryPeriodFromPolitic()
    {
        return $this->dealPolitics->getMinDeliveryPeriod();
    }

    /**
     * @param Deal $deal
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function createDealPDF(Deal $deal)
    {
        // If Deal DealContractFile(File) already exists do nothing
        if ($deal->getDealContractFile() && $deal->getDealContractFile() instanceof DealContractFile) {
            return false;
        }
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var SystemPaymentDetails $systemPaymentDetail */ // Guarant-Pay PaymentMethod
        $systemPaymentDetail = $this->settingManager->getSystemPaymentDetail();
        /** @var PaymentMethod $contractorPaymentMethod */// Contractor Payment Method
        $contractorPaymentMethod = $payment->getPaymentMethod();
        /** @var LegalEntity|NaturalPerson $customerCivilLowSubject */ // Customer CivilLowSubject
        $customerCivilLowSubject = $this->civilLawSubjectManager->getAssignedCivilLawSubject($deal->getCustomer()->getCivilLawSubject());
        /** @var LegalEntity|NaturalPerson $contractorCivilLowSubject */ // Contractor CivilLowSubject
        $contractorCivilLowSubject = $this->civilLawSubjectManager->getAssignedCivilLawSubject($deal->getContractor()->getCivilLawSubject());

        $view = new ViewModel([
            'deal' => $deal,
            'payment' => $payment,
            'systemPaymentDetail' => $systemPaymentDetail,
            'contractorPaymentMethod' => $contractorPaymentMethod,
            'customerCivilLowSubject' => $customerCivilLowSubject,
            'contractorCivilLowSubject' => $contractorCivilLowSubject,
            'main_project_url' => $this->config['main_project_url']
        ]);

        $renderer = $this->renderer;
        $view->setTemplate('application/deal/deal-contract-pdf');

        $html = $renderer->render($view);

        $date = new \DateTime();
        // File name
        $file_name = 'deal-contract-' . $deal->getId() . '-' . $date->format('Ymd') . '.pdf';
        // PDF Creation
        $title = 'Договор по сделке #' . $deal->getId();

        // Try create and save PDF file
        try {
            $pdf = $this->tcpdf;

            $pdf->SetFont('arialnarrow', '', 14, '', false);
            $pdf->AddPage();
            // set document information
            $pdf->SetCreator('GP');
            $pdf->SetAuthor('GP');
            $pdf->SetTitle($title);
            try {
                // Вариант вставки картинки
                $logo = file_get_contents($this->config['pdf']['main_logo']);

            } catch (\Throwable $t) {

                $logo = null;
            }
            $pdf->Image('@' . $logo, 11, 14);

            $pdf->writeHTML($html, true, false, true, false, '');

            $content = $pdf->Output($file_name, 'S');

            // Save file
            $params = [
                'file_source' => $content,
                'file_name' => $file_name,
                'file_type' => "application/pdf",
                'file_size' => strlen($content),
                'path' => DealController::DEAL_CONTRACTS_STORING_FOLDER
            ];
            $file = $this->fileManager->write($params, true);
        } catch (\Exception $t) {
            //save error in log
            logException($t, 'deal-errors', 3);

            return false;
        }

        // Set greatest of DealAgent's DateOfConfirm as DateOfConfirm of Deal
        if ($deal->getCustomer()->getDateOfConfirm() > $deal->getContractor()->getDateOfConfirm()) {
            $deal->setDateOfConfirm($deal->getCustomer()->getDateOfConfirm());
        } else {
            $deal->setDateOfConfirm($deal->getContractor()->getDateOfConfirm());
        }
        // Create new DealContractFile object
        $dealContractFile = new DealContractFile();
        // Set File to DealContractFile
        $dealContractFile->setFile($file);
        // Set DealContractFile to Deal
        $deal->setDealContractFile($dealContractFile);

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param $file_id
     * @return \Zend\Stdlib\ResponseInterface
     * @throws \Exception
     */
    public function downloadContractFile($file_id)
    {
        try {
            $response = $this->fileManager->download($file_id, DealController::DEAL_CONTRACTS_STORING_FOLDER);
        } catch (\Throwable $t) {

            throw new \Exception($t);
        }

        return $response;
    }

    /**
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    private function creditPaymentOrderAvailabilityListenerEventHandler(Deal $deal)
    {
        $this->creditPaymentOrderAvailabilityListener->subscribe();
        $this->creditPaymentOrderAvailabilityListener->trigger($deal);

        return true;
    }

    /**
     * @param Deal $deal
     * @param array $file_data
     * @return File
     * @throws \Exception
     */
    public function saveDealFile(Deal $deal, array $file_data)
    {
        // Set of data for pass to fileManager
        $file_source = $file_data['tmp_name'];
        $file_name = $file_data['name'];
        $file_type = $file_data['type'];
        $file_size = $file_data['size'];

        $params = [
            'file_source' => $file_source,
            'file_name' => $file_name,
            'file_type' => $file_type,
            'file_size' => $file_size,
            'path' => DealController::DEAL_FILES_STORING_FOLDER
        ];

        $file = $this->fileManager->write($params);

        // Add $file to collection
        $deal->addDealFile($file);

        $this->entityManager->flush();

        return $file;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealHasDispute(Deal $deal): bool
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        return $dispute && !$dispute->isClosed();
    }

    /**
     * @param Deal $deal
     * @return File
     * @throws \Exception
     */
    public function getDealContractFile(Deal $deal)
    {
        /** @var File $contractFile */
        $contractFile = $deal->getDealContractFile();

        if ($contractFile) {

            return $contractFile;
        }

        throw new \Exception('Deal Contract file is absent');
    }

    /**
     * @param Payment $payment
     * @return float|int|null
     * @throws \Exception
     */
    public function getOverpaidAmount(Payment $payment)
    {
        try {
            // Can throw Exception
            $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment);
        } catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        if ($total_incoming_amount > $payment->getExpectedValue()) {

            return $total_incoming_amount - $payment->getExpectedValue();
        }

        return null;
    }

    /**
     * @param Payment $payment
     * @return array
     */
    public function getIncomingPaymentTransactions(Payment $payment): array
    {
        return $this->paymentOrderManager->getIncomingPaymentTransactions($payment);
    }

    /**
     * @param Payment $payment
     * @return array
     */
    public function getIncomingPaymentAllTransactions(Payment $payment): array
    {
        return $this->paymentOrderManager->getIncomingPaymentAllTransactions($payment);
    }

    /**
     * @param Payment $payment
     * @return mixed|null
     */
    public function getLastIncomingPaymentTransaction(Payment $payment)
    {
        $paymentTransactions = $this->getIncomingPaymentAllTransactions($payment);

        $paymentTransaction = end($paymentTransactions);

        return $paymentTransaction ?? null;
    }

    /**
     * @param array $paymentTransactions
     * @return array
     */
    public function getPaymentTransactionCollectionOutput(array $paymentTransactions): array
    {
        return $this->paymentOrderManager
            ->getPaymentTransactionCollectionOutput($paymentTransactions);
    }

    /**
     * @param Deal $deal
     * @return float|int
     * @throws \Exception
     */
    public function getDealPaidAmount(Deal $deal)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        return $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment);
    }

    /**
     * @param User $user
     * @throws OptimisticLockException
     */
    public function attachDealAgentsToUser(User $user)
    {
        $emailIsVerified = $user->getEmail() ? $user->getEmail()->getIsVerified() : false;
        logMessage('attachDealAgentsToUser | '.$emailIsVerified);
        if ( $emailIsVerified ) {
            $email_value = $user->getEmail()->getEmail();
            $dealAgents = $this->dealAgentManager->getDealAgentsWithoutUserByEmailValue($email_value);
            logMessage('countDealAgents | '.\count($dealAgents));
            /** @var DealAgent $dealAgent */
            foreach ($dealAgents as $dealAgent){
                $civilLawSubject = $dealAgent->getCivilLawSubject();
                $dealAgent->setUser($user);
                if ($civilLawSubject && $civilLawSubject->getUser() === null) {
                    $civilLawSubject->setUser($user);
                    $this->entityManager->persist($civilLawSubject);
                }

                $this->entityManager->persist($dealAgent);
            }

            $this->entityManager->flush();
        }
    }

    /**
     * @param DealAgent $dealAgent
     * @return bool
     */
    public function isAllowingTimeToResendInvitation(DealAgent $dealAgent):bool
    {
        if ($dealAgent->getInvitationDate()) {
            /** @var \DateTime $currentDate */
            $currentDate = new \DateTime();
            /** @var \DateTime $createdDate */
            $invitationDate = clone $dealAgent->getInvitationDate();
            $request_period = $this->config['tokens']['deal_invitation_request_period'];

            return $invitationDate->modify('+'.$request_period.' seconds') < $currentDate;
        }

        return true;
    }
}