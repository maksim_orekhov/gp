<?php

namespace Application\Service\Deal;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\SystemPaymentDetails;
use Application\Provider\Mail\DealDeadlineNotificationSender;
use Application\Service\Dispute\DisputeManager;
use Application\Service\SettingManager;
use Doctrine\ORM\EntityManager;
use ModuleCode\Service\SmsStrategyContext;
use Zend\Paginator\Paginator;

/**
 * Class DealDeadlineManager
 * @package Application\Service\Deal
 */
class DealDeadlineManager
{
    const HOUR_FOR_DEADLINE_NOTIFY = '24'; // дедлайн в часах

    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    /**
     * notify params
     */
    private $email_notify_near_deadline;
    private $sms_notify_near_deadline;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * @var array
     */
    private $config;
    /**
     * @var DealDeadlineNotificationSender
     */
    private $dealDeadlineNotificationSender;

    /**
     * DealDeadlineManager constructor.
     * @param EntityManager $entityManager
     * @param DealDeadlineNotificationSender $dealDeadlineNotificationSender
     * @param SettingManager $settingManager
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                DealDeadlineNotificationSender $dealDeadlineNotificationSender,
                                SettingManager $settingManager,
                                array $config)
    {
        $this->entityManager = $entityManager;
        $this->settingManager = $settingManager;
        $this->config = $config;

        // set notify params from config
        $this->sms_notify_near_deadline = $this->getDealNotifyConfig($this->config, 'near_deadline', 'sms');
        $this->email_notify_near_deadline = $this->getDealNotifyConfig($this->config, 'near_deadline', 'email');
        $this->dealDeadlineNotificationSender = $dealDeadlineNotificationSender;
    }

    /**
     * @param array $params
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function getPaymentsWithDeadline(array $params = [])
    {
        /** @var SystemPaymentDetails $systemPaymentDetail */
        $systemPaymentDetail = $this->settingManager->getSystemPaymentDetail();

        $gp_account = $systemPaymentDetail->getAccountNumber();

        /** @var array $deadlinePayments */
        $deadlinePayments = $this->entityManager->getRepository(Payment::class)
            ->selectSortedPaymentsWithNearDeadlineDeals($params, $gp_account);

        return $deadlinePayments;
    }

    /**
     * @param $deadlinePayments
     * @return array
     */
    public function getDealDeadlineCollectionForOutputByPayments($deadlinePayments)
    {
        $deadlineDealsOutput = [];
        /** @var Paginator $deadlinePayments */
        foreach ($deadlinePayments as $deadlinePayment) {
            $deadlineDealsOutput[] = $this->getDealDeadlineForOutputByPayment($deadlinePayment);
        }

        return $deadlineDealsOutput;
    }

    /**
     * @param Payment $deadlinePayment
     * @return array
     */
    public function getDealDeadlineForOutputByPayment(Payment $deadlinePayment)
    {
        /** @var Deal $deal */
        $deal = $deadlinePayment->getDeal();

        $deadlineDealOutput = [];

        $deadlineDealOutput['id'] = $deal->getId();
        $deadlineDealOutput['name'] = $deal->getName();
        $deadlineDealOutput['number'] = $deal->getNumber();
        $deadlineDealOutput['created'] = $deal->getCreated()->format('d-m-Y H:i');
        $deadlineDealOutput['owner'] = $deal->getOwner()->getUser()->getLogin(); // Это нужно для Оператора
        $deadlineDealOutput['amount_without_fee'] = $deadlinePayment->getDealValue();

        $dateClosedDeal = self::getDealDeadlineDate($deadlinePayment);
        $deadline = date_diff(new \DateTime(), $dateClosedDeal);

        $deadlineDealOutput['date_of_deadline'] = $dateClosedDeal->format('d-m-Y H:i');
        $deadlineDealOutput['date_of_deadline_params'] = [
            'y' => $deadline->y,
            'm' => $deadline->m,
            'd' => $deadline->d,
            'h' => $deadline->h,
            'i' => $deadline->i,
            'invert' => $deadline->invert,
            'days' => $deadline->invert ? -1 * $deadline->days: $deadline->days,
            'hours' => hour_date_diff($deadline),
        ];

        return $deadlineDealOutput;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function notifyDealAgentsAboutNearDeadline()
    {
        /** @var SystemPaymentDetails $systemPaymentDetail */
        $systemPaymentDetail = $this->settingManager->getSystemPaymentDetail();

        $gp_account = $systemPaymentDetail->getAccountNumber();

        /** @var array $deadlinePayments */
        $deadlinePayments = $this->entityManager->getRepository(Payment::class)
            ->selectSortedPaymentsWithNearDeadlineDeals([], $gp_account);

        $current_date = new \DateTime();
        /** @var Payment $deadlinePayment */
        foreach ($deadlinePayments as $deadlinePayment) {
            $dateClosedDeal = self::getDealDeadlineDate($deadlinePayment);
            $diff = date_diff($current_date, $dateClosedDeal);
            $deadline_hour = hour_date_diff($diff);
            if($deadline_hour <= self::HOUR_FOR_DEADLINE_NOTIFY){
                $this->notifyDealAgentsAboutNearDeadlineByDeal($deadlinePayment->getDeal());
            }
        }

        return true;
    }

    /**
     * @param Payment $payment
     * @return \DateTime
     */
    public static function getDealDeadlineDate(Payment $payment)
    {
        if (!$payment->getDeFactoDate()) {

            return null;
        }
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $last_warranty_extension_days = 0;
        if ($dispute) {
            $warranty_extension = DisputeManager::getLastWarrantyExtension($dispute->getWarrantyExtensions());
            $last_warranty_extension_days = $warranty_extension ? $warranty_extension->getDays() : 0;
        }
        $deFactoDate = clone $payment->getDeFactoDate();
        $days = $deal->getDeliveryPeriod() + $last_warranty_extension_days;

        return $deFactoDate->modify('+'.$days.' days');
    }

    /**
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    public function notifyDealAgentsAboutNearDeadlineByDeal(Deal $deal)
    {
        if($this->email_notify_near_deadline){
            try {
                $this->notifyCustomerAboutNearDeadlineByEmail($deal);
                $this->notifyContractorAboutNearDeadlineByEmail($deal);
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
        if($this->sms_notify_near_deadline){
            try {
                $this->notifyCustomerAboutNearDeadlineBySms($deal);
                $this->notifyContractorAboutNearDeadlineBySms($deal);
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param Deal $deal
     */
    private function notifyCustomerAboutNearDeadlineByEmail(Deal $deal)
    {
        $email = $deal->getCustomer()->getEmail();

        $data = [
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DealDeadlineNotificationSender::TYPE_NOTIFY_KEY => DealDeadlineNotificationSender::TYPE_NOTIFY_NEAR_DEADLINE_FOR_CUSTOMER,
        ];

        $this->dealDeadlineNotificationSender->sendMail($email, $data);
    }

    /**
     * @param Deal $deal
     */
    private function notifyContractorAboutNearDeadlineByEmail(Deal $deal)
    {
        $email = $deal->getContractor()->getEmail();

        $data = [
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DealDeadlineNotificationSender::TYPE_NOTIFY_KEY => DealDeadlineNotificationSender::TYPE_NOTIFY_NEAR_DEADLINE_FOR_CONTRACTOR,
        ];

        $this->dealDeadlineNotificationSender->sendMail($email, $data);
    }

    /**
     * @param Deal $deal
     * @throws \Exception
     */
    private function notifyCustomerAboutNearDeadlineBySms(Deal $deal)
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getNearDeadlineForCustomerSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Deal $deal
     * @throws \Exception
     */
    private function notifyContractorAboutNearDeadlineBySms(Deal $deal)
    {
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getNearDeadlineForContractorSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }
}