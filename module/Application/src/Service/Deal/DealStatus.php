<?php

namespace Application\Service\Deal;

use Application\Entity\Deal;
use Application\Service\Deal\Status\PendingRefundPaymentStatus;
use Application\Service\Deal\Status\StatusInterface;
use Application\Service\Deal\Status\TroubleStatus;
use Application\Service\Payment\PaymentManager;
use Application\Service\Deal\Status\ConfirmedStatus;
use Application\Service\Deal\Status\DebitMatchedStatus;
use Application\Service\Deal\Status\DeliveredStatus;
use Application\Service\Deal\Status\PendingCreditPaymentStatus;
use Application\Service\Deal\Status\ClosedStatus;
use Application\Service\Deal\Status\DisputeStatus;
use Application\Service\Deal\Status\RefundStatus;
use Application\Service\Deal\Status\TribunalStatus;
use Application\Service\Deal\Status\WaitingApprovalStatus;

/**
 * Class Status
 * @package Application\Service\Deal\Status
 */
class DealStatus
{
    const DEAL_STATUSES = [ // Important! Не менять значения ключа 'status'
        '0' =>  ['code' => 1,  'status' => 'negotiation',            'priority' => 1,   'is_blocker' => false, 'name' => 'Ожидает согласования'],
        '1' =>  ['code' => 5,  'status' => 'confirmed',              'priority' => 10,  'is_blocker' => false, 'name' => 'Согласовано'],
        '2' =>  ['code' => 10, 'status' => 'debit_matched',          'priority' => 20,  'is_blocker' => false, 'name' => 'Оплачено'],
        '3' =>  ['code' => 15, 'status' => 'dispute',                'priority' => 30,  'is_blocker' => false, 'name' => 'Спор'],
        '4' =>  ['code' => 20, 'status' => 'delivered',              'priority' => 40,  'is_blocker' => false, 'name' => 'Товар доставлен'],
        '5' =>  ['code' => 25, 'status' => 'pending_credit_payment', 'priority' => 50,  'is_blocker' => false, 'name' => 'Ожидает выплаты'],
        '6' =>  ['code' => 30, 'status' => 'closed',                 'priority' => 60,  'is_blocker' => true,  'name' => 'Закрыто'],
        '7' =>  ['code' => 34, 'status' => 'pending_refund_payment', 'priority' => 70,  'is_blocker' => false, 'name' => 'Ожидает возврата'],
        '8' =>  ['code' => 35, 'status' => 'refund',                 'priority' => 100, 'is_blocker' => true,  'name' => 'Закрыто с возвратом'],
        '9' =>  ['code' => 40, 'status' => 'tribunal',               'priority' => 200, 'is_blocker' => true,  'name' => 'Суд'],
        '10' => ['code' => 90, 'status' => 'trouble',                'priority' => 403, 'is_blocker' => true,  'name' => 'Проблемная'],
        '99' => ['code' => 99, 'status' => 'undefined',              'priority' => 404, 'is_blocker' => true,  'name' => 'Статус не определен']
    ];

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var array
     */
    private $status_objects;

    /**
     * DealStatus constructor.
     * @param PaymentManager $paymentManager
     * @param ConfirmedStatus $confirmedStatus
     * @param DebitMatchedStatus $debitMatchedStatus
     * @param DeliveredStatus $deliveredStatus
     * @param PendingCreditPaymentStatus $pendingCreditPaymentStatus
     * @param ClosedStatus $closedStatus
     * @param DisputeStatus $disputeStatus
     * @param PendingRefundPaymentStatus $pendingRefundPaymentStatus
     * @param RefundStatus $refundStatus
     * @param TribunalStatus $tribunalStatus
     * @param WaitingApprovalStatus $waitingApprovalStatus
     * @param TroubleStatus $troubleStatus
     */
    public function __construct(PaymentManager $paymentManager,
                                ConfirmedStatus $confirmedStatus,
                                DebitMatchedStatus $debitMatchedStatus,
                                DeliveredStatus $deliveredStatus,
                                PendingCreditPaymentStatus $pendingCreditPaymentStatus,
                                ClosedStatus $closedStatus,
                                DisputeStatus $disputeStatus,
                                PendingRefundPaymentStatus $pendingRefundPaymentStatus,
                                RefundStatus $refundStatus,
                                TribunalStatus $tribunalStatus,
                                WaitingApprovalStatus $waitingApprovalStatus,
                                TroubleStatus $troubleStatus)
    {
        $this->paymentManager = $paymentManager;

        $this->status_objects = [
            'negotiation'            => $waitingApprovalStatus,
            'confirmed'              => $confirmedStatus,
            'debit_matched'          => $debitMatchedStatus,
            'dispute'                => $disputeStatus,
            'delivered'              => $deliveredStatus,
            'pending_credit_payment' => $pendingCreditPaymentStatus,
            'closed'                 => $closedStatus,
            'pending_refund_payment' => $pendingRefundPaymentStatus,
            'refund'                 => $refundStatus,
            'tribunal'               => $tribunalStatus,
            'trouble'                => $troubleStatus,
            'undefined'              => null,
        ];
    }

    /**
     * @param Deal $deal
     * @return Deal
     */
    public function setStatus(Deal $deal)
    {
        /** @var array $status */
        $status = $this->getStatus($deal);

        $deal->status = $status;

        return $deal;
    }

    /**
     * @param Deal $deal
     * @return array
     */
    public function getStatus(Deal $deal): array
    {
        $status = null;
        /** @var array deal_statuses */
        $deal_statuses = $this->sortStatusArrayByPriority(self::DEAL_STATUSES);

        foreach ($deal_statuses as $deal_status) {

            $statusObject = $this->status_objects[$deal_status['status']];

            if ($statusObject instanceof StatusInterface && $statusObject->isDealInStatus($deal)) {

                $status = $deal_status;

                if (null !== $status && $status['status'] === 'trouble') {

                    $status = $this->addTroubleReasons($deal, $status);
                }

                if (true === $deal_status['is_blocker']) {

                    return $status;
                }
            }
        }

        if (null === $status) {

            $status = self::DEAL_STATUSES['99'];
        }

        return $status;
    }

    /**
     * @param $deal
     * @param $status
     * @return mixed
     */
    private function addTroubleReasons($deal, $status)
    {
        /** @var TroubleStatus $troubleStatus */
        $troubleStatus = $this->status_objects['trouble'];
        $trouble_reasons = $troubleStatus->getTroubleReason($deal);

        $status['name'] .= ': ';
        foreach ($trouble_reasons as $trouble_reason) {
            $status['name'] .= ' ' . $trouble_reason . ';';
        }

        return $status;
    }

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return self::DEAL_STATUSES;
    }

    /**
     * Сортировка массива стутасов по приоритету
     *
     * @param $dispute_statuses
     * @return mixed
     */
    private function sortStatusArrayByPriority($dispute_statuses)
    {
        usort($dispute_statuses, function($a, $b) {
            return ($a['priority'] < $b['priority']) ? -1 : 1;
        });

        return $dispute_statuses;
    }
}