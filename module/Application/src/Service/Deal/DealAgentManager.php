<?php

namespace Application\Service\Deal;

use Application\Entity\LegalEntity;
use Application\Entity\Payment;
use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\User;
use Application\Entity\CivilLawSubject;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;

/**
 * Class DealAgentManager
 * @package Application\Service\Deal
 */
class DealAgentManager
{
    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    private $entityManager;

    /**
     * Doctrine entity manager.
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DealAgentPolitics
     */
    private $dealAgentPolitics;

    /**
     * @var \Application\Service\CivilLawSubject\CivilLawSubjectManager
     */
    private $civilLawSubjectManager;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * DealAgentManager constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param DealAgentPolitics $dealAgentPolitics
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                DealAgentPolitics $dealAgentPolitics)
    {
        $this->entityManager          = $entityManager;
        $this->userManager            = $userManager;
        $this->baseAuthManager            = $baseAuthManager;
        $this->civilLawSubjectManager = $civilLawSubjectManager;
        $this->dealAgentPolitics      = $dealAgentPolitics;
    }

    /**
     * @param $userDealAgent
     * @param array $param
     * @return DealAgent
     * @throws LogicException
     */
    public function createDealAgent($userDealAgent, array $param = []): DealAgent
    {
        // Create new DealAgent object
        $dealAgent = new DealAgent();
        $dealAgent->setDealAgentConfirm(false);
        $dealAgent->setDealAgentOnceConfirm(false);
        if ($userDealAgent instanceof User) {
            $dealAgent->setUser($userDealAgent);
            $dealAgent->setEmail($userDealAgent->getEmail()->getEmail());
            if (isset($param['deal_civil_law_subject']) && 0 !== (int) $param['deal_civil_law_subject']) {
                $this->setCivilLawSubjectById($dealAgent, $param['deal_civil_law_subject']);
            }
        } else {
            // Если user'а нет в системе, пишем только email (он нужен только для отправки приглашения)
            $dealAgent->setUser(null);
            $dealAgent->setEmail(strtolower($userDealAgent));
            if (isset($param['deal_civil_law_subject']) && 0 !== (int) $param['deal_civil_law_subject']) {
                $this->setCivilLawSubjectById($dealAgent, $param['deal_civil_law_subject']);
            }
        }

        return $dealAgent;
    }

    /**
     * @param DealAgent $dealAgent
     * @param CivilLawSubject $civilLawSubject
     * @return DealAgent
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setCivilLawSubjectToDealAgent(DealAgent $dealAgent, CivilLawSubject $civilLawSubject)
    {
        $dealAgent->setCivilLawSubject($civilLawSubject);
        $this->entityManager->persist($dealAgent);
        $this->entityManager->flush();
        return $dealAgent;
    }

    /**
     * @param DealAgent $dealAgent
     * @param int $civil_law_subject_id
     * @return bool
     * @throws LogicException
     */
    public function setCivilLawSubjectById(DealAgent $dealAgent, int $civil_law_subject_id): bool
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->civilLawSubjectManager->get($civil_law_subject_id);

        if (!$civilLawSubject) {

            throw new LogicException('CivilLawSubject with such id not found');
        }

        $dealAgent->setCivilLawSubject($civilLawSubject);

        return true;
    }

    /**
     * @param $param
     * @return User|null
     * @throws \Exception
     */
    public function getUserForDealAgent($param)
    {
        if(!$param) {

            throw new \Exception('No user data provided for DealAgentManager');
        }

        try {

            return $this->userManager->getUserByLoginOrEmail($param);
        }
        catch (\Throwable $t) {

            return $param;
        }
    }

    /**
     * Возвращает объект User или входящий $email_value
     *
     * @param string $email_value
     * @return User|null|string
     * @throws \Exception
     */
    public function getUserForDealAgentByEmail(string $email_value)
    {
        if(!$email_value) {
            throw new \Exception("No email value provided for counteragent's DealAgent definition");
        }

        /** @var User $user */
        $user = $this->getUserForDealAgent($email_value);

        if ($user && $user instanceof User) {
            return $user;
        }

        return $email_value;
    }

    /**
     * Remove DealAgent in EntityManager
     * To remove from DB flush method must be requested in the calling method
     *
     * @param DealAgent $dealAgent
     * @return bool
     */
    public function removeDealAgent(DealAgent $dealAgent)
    {
        $this->entityManager->remove($dealAgent);

        return true;
    }

    /**
     * @param User $user
     * @param DealAgent $dealAgent
     * @return null|object
     * @throws \Exception
     */
    public function setInvitationUserToDealAgent(User $user, DealAgent $dealAgent){
        if ($user && !$dealAgent->getUser()) {
            $dealAgent->setUser($user);
            /** @var CivilLawSubject $civilLawSubject */
            $civilLawSubject = $dealAgent->getCivilLawSubject();
            // Если есть civilLawSubject, но у него нет user
            if (null !== $civilLawSubject && null === $civilLawSubject->getUser()) {
                $civilLawSubject->setUser($user);
                $this->entityManager->persist($civilLawSubject);
            }
            $this->entityManager->persist($dealAgent);
            $this->entityManager->flush();
        }

        return $dealAgent;
    }

    /**
     * @param Deal $deal
     * @param array $deal_agents
     * @param string $counteragent_email
     * @return DealAgent|null
     * @throws \Exception
     */
    public function replaceCounteragentDealAgent(Deal $deal, array $deal_agents, string $counteragent_email)
    {
        // Important! Возвращает User или $counteragent_email. Использовать только для передачи в createDealAgent!
        /** @var User|string $counteragentUser */
        $counteragentUser = $this->getUserForDealAgent($counteragent_email);

        // Разрешаем менять агентов в сделке только владельцу сделки (изменение роли)
        if (!$this->dealAgentPolitics->isDealAgentAllowedToChangeCounterAgent($deal, $deal_agents['currentUserAgent'])) {

            throw new LogicException('Forbidden to change counteragent in the deal');
        }

        /** @var DealAgent $counteragentDealAgent */
        $counteragentDealAgent = $this->createDealAgent($counteragentUser);

        $oldDealAgent = null;

        // Если меняем Customer
        if ($deal->getCustomer() === $deal_agents['counterAgent'] && $deal->getOwner() !== $deal_agents['counterAgent']) {
            // Deposit current DealAgent
            $oldDealAgent = $deal->getCustomer();
            // Set new DealAgent
            $deal->setCustomer($counteragentDealAgent);
        }
        // Если меняем Contractor
        if ($deal->getContractor() === $deal_agents['counterAgent'] && $deal->getOwner() !== $deal_agents['counterAgent']) {
            // Deposit current DealAgent
            $oldDealAgent = $deal->getContractor();
            // Set new DealAgent
            $deal->setContractor($counteragentDealAgent);

            /** @var Payment $payment */
            $payment = $deal->getPayment();
            // Сбрасываем PaymentMethod
            $payment->setPaymentMethod(null);

            $this->entityManager->persist($payment);
        }

        if (null !== $oldDealAgent) {
            // Remove old DealAgent
            $this->removeDealAgent($oldDealAgent);

            return $counteragentDealAgent;
        }

        return null;
    }

    /**
     * @param Deal $deal
     * @param DealAgent $currentUserAgent
     * @return bool
     * @throws LogicException
     */
    public function swapDealAgents(Deal $deal, DealAgent $currentUserAgent): bool
    {
        // Разрешаем менять агентов в сделке только владельцу сделки (изменение роли)
        if (!$this->dealAgentPolitics->isDealAgentAllowedToChangeRole($deal, $currentUserAgent)) {

            throw new LogicException('Forbidden to change the role in the deal');
        }

        // Меняем
        $dealAgentCustomer      = $deal->getContractor();
        $dealAgentContractor    = $deal->getCustomer();

        $deal->setCustomer($dealAgentCustomer);
        $deal->setContractor($dealAgentContractor);

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        // Сбрасываем PaymentMethod
        $payment->setPaymentMethod(null);

        return true;
    }

    /**
     * @param $post_data
     * @param string|null $agent_email
     * @param User|null $ownerUser
     * @return array
     * @throws \Exception
     */
    public function prepareDealAgentToDeal($post_data, string $agent_email = null, User $ownerUser = null): array
    {
        $currentUser = null;
        if ($this->baseAuthManager->getIdentity()) {
            $currentUser = $this->getUserForDealAgent($this->baseAuthManager->getIdentity());
        }

        if($post_data['deal_role'] === 'customer') {
            $userCustomer   = $ownerUser ?: $currentUser ?: $agent_email;
            $userContractor = $this->getUserForDealAgent($post_data['counteragent_email']);
            $paramCustomer = $post_data;
            $paramContractor = [];
        } else {
            $userCustomer   = $this->getUserForDealAgent($post_data['counteragent_email']);
            $userContractor = $ownerUser ?: $currentUser ?: $agent_email;
            $paramCustomer = [];
            $paramContractor = $post_data;
        }

        return [
            'userCustomer' => $userCustomer,
            'userContractor' => $userContractor,
            'paramCustomer' => $paramCustomer,
            'paramContractor' => $paramContractor,
        ];
    }

    /**
     * @param Deal $deal
     * @param User|string $representativeCustomerUser
     * @param array $dealAgent_customer_data
     * @return DealAgent
     * @throws \Exception
     */
    private function defineDealAgentCustomer(Deal $deal, $representativeCustomerUser, array $dealAgent_customer_data)
    {
        try {
            // Can throw Exception!
            /** @var string $customer_email */
            $customer_email = $this->extractEmailFromRepresentative($representativeCustomerUser);
        }
        catch (\Exception $t) {

            throw new \Exception($t . ' ( for Customer)');
        }

        $contractorDeal = $deal->getContractor();
        $contractorUser = $contractorDeal->getUser();
        $contractor_user_email = $contractorUser ? $contractorUser->getEmail()->getEmail() : $contractorDeal->getEmail();

        $customerDeal = $deal->getCustomer();
        $customerUser = $customerDeal->getUser();
        $customer_user_email = $customerUser ? $customerUser->getEmail()->getEmail() : $customerDeal->getEmail();

        /** @var string $customer_user_email */
        if ($customer_user_email === $customer_email) {
            $dealAgentCustomer = $customerDeal;
        } else {
            $dealAgentCustomer = $this->createDealAgent($representativeCustomerUser, $dealAgent_customer_data);
            // CivilLawSubject присвоется только агенту текущего пользователя. Для контрагента этих данных нет в форме.
            // Для нового котрагента CivilLawSubject не сетится.
            // Если поменялись роли, то CivilLawSubject нового агента Кастомера берется из текущего агента Контрактора,
            // если там есть.

            if ($contractor_user_email === $customer_email
                && !$dealAgentCustomer->getCivilLawSubject()
                && $contractorDeal->getCivilLawSubject()) {

                $dealAgentCustomer->setCivilLawSubject($contractorDeal->getCivilLawSubject());
            }
        }

        return $dealAgentCustomer;
    }

    /**
     * @param Deal $deal
     * @param User|string $representativeContractorUser
     * @param array $dealAgent_contractor_data
     * @return DealAgent
     * @throws \Exception
     */
    private function defineDealAgentContractor(
        Deal $deal,
        $representativeContractorUser,
        array $dealAgent_contractor_data
    )
    {
        try {
            // Can throw Exception!
            /** @var string $contractor_email */
            $contractor_email = $this->extractEmailFromRepresentative($representativeContractorUser);
        }
        catch (\Exception $t) {

            throw new \Exception($t . ' ( for Contractor)');
        }

        $contractorDeal = $deal->getContractor();
        $contractorUser = $contractorDeal->getUser();
        $contractor_user_email = $contractorUser ? $contractorUser->getEmail()->getEmail() : $contractorDeal->getEmail();

        $customerDeal = $deal->getCustomer();
        $customerUser = $customerDeal->getUser();
        $customer_user_email = $customerUser ? $customerUser->getEmail()->getEmail() : $customerDeal->getEmail();

        /** @var string $contractor_user_email */
        if ($contractor_user_email === $contractor_email) {
            $dealAgentContractor = $contractorDeal;
        } else {
            $dealAgentContractor = $this->createDealAgent($representativeContractorUser, $dealAgent_contractor_data);
            // CivilLawSubject присвоется только агенту текущего пользователя. Для контрагента этих данных нет в форме.
            // Для нового котрагента CivilLawSubject не сетится.
            // Если поменялись роли, то CivilLawSubject нового агента Кастомера берется из текущего агента Контрактора,
            // если там есть.
            if ($customer_user_email === $contractor_email
                && !$dealAgentContractor->getCivilLawSubject()
                && $customerDeal->getCivilLawSubject()) {

                $dealAgentContractor->setCivilLawSubject($customerDeal->getCivilLawSubject());
            }
        }

        return $dealAgentContractor;
    }

    /**
     * @param User|null $user
     * @param DealAgent $dealAgentCustomer
     * @param DealAgent $dealAgentContractor
     * @param string|null $agent_email
     * @return mixed
     * @throws \Exception
     */
    public function defineDealOwner($user, DealAgent $dealAgentCustomer, DealAgent $dealAgentContractor, $agent_email = null)
    {
        if ($this->isDealAgentOwner($user, $dealAgentCustomer, $agent_email)) {
            // Если Customer - владелец
            $dealOwner = $dealAgentCustomer;

        } elseif ($this->isDealAgentOwner($user, $dealAgentContractor, $agent_email)) {
            // Если Contractor - владелец
            $dealOwner = $dealAgentContractor;

        } else {
            // Владелец не опроеделен
            throw new \Exception('The owner of the Deal not determined');
        }

        return $dealOwner;
    }

    /**
     * @param User|null $user
     * @param DealAgent $dealAgent
     * @param string|null $agent_email
     * @return bool
     */
    private function isDealAgentOwner($user, DealAgent $dealAgent, $agent_email = null): bool
    {
        if ($user && $dealAgent->getUser() && $dealAgent->getUser() === $user) {

            return true;
        }

        if ($agent_email && $dealAgent->getEmail() && $dealAgent->getEmail() === $agent_email) {

            return true;
        }

        return false;
    }


    /**
     * @param User|string $representative
     * @return string
     * @throws \Exception
     */
    private function extractEmailFromRepresentative($representative)
    {
        if ($representative instanceof User) {
            $email_value = $representative->getEmail()->getEmail();
        } elseif (\is_string($representative)) {
            $email_value = $representative;
        } else {
            throw new \Exception('The data type does not match');
        }

        return $email_value;
    }

    /**
     * @param $dealAgent
     * @return null|string
     */
    public function getCivilLawSubjectNameByDealAgent($dealAgent)
    {
        $civilLawSubjectName = null;
        /** @var DealAgent $dealAgent */
        if($dealAgent && $dealAgent->getCivilLawSubject() && $dealAgent->getCivilLawSubject()->getLegalEntity()){
            $legalEntity = $dealAgent->getCivilLawSubject()->getLegalEntity();
            $civilLawSubjectName = $legalEntity->getName();
        }elseif($dealAgent && $dealAgent->getCivilLawSubject() && $dealAgent->getCivilLawSubject()->getNaturalPerson()){
            $naturalPerson = $dealAgent->getCivilLawSubject()->getNaturalPerson();
            $civilLawSubjectName = $naturalPerson->getLastName().' '.$naturalPerson->getFirstName().' '.$naturalPerson->getSecondaryName();
        }
        return $civilLawSubjectName;
    }

    /**
     * @param $dealAgent
     * @return null|string
     */
    public function getCivilLawSubjectTypeByDealAgent($dealAgent)
    {
        $civilLawSubjectType = null;
        /** @var DealAgent $dealAgent */
        if($dealAgent && $dealAgent->getCivilLawSubject() && $dealAgent->getCivilLawSubject()->getLegalEntity()){
            $civilLawSubjectType = CivilLawSubjectManager::LEGAL_ENTITY_NAME;
        }elseif($dealAgent && $dealAgent->getCivilLawSubject() && $dealAgent->getCivilLawSubject()->getNaturalPerson()){
            $civilLawSubjectType = CivilLawSubjectManager::NATURAL_PERSON_NAME;
        }

        return $civilLawSubjectType;
    }


    /**
     * @param DealAgent $dealAgent
     * @return \Application\Entity\NdsType|null
     */
    public function getDealAgentNdsType(DealAgent $dealAgent)
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $dealAgent->getCivilLawSubject();

        if (null !== $civilLawSubject->getNaturalPerson()) {

            return null;
        }

        return $civilLawSubject->getNdsType();
    }

    /**
     * @param DealAgent $customer
     * @param DealAgent $contractor
     * @param User|null $user
     * @param string|null $agent_email
     * @return DealAgent
     * @TODO Мождет быть, искать через "counteragent_email"
     */
    public function getCounteragentByAgents(DealAgent $customer, DealAgent $contractor, $user, $agent_email = null): DealAgent
    {
        if ((null !== $user && $user === $customer->getUser()) ||
            (null !== $agent_email && $agent_email === $customer->getEmail())) {

            return $contractor;
        }

        return $customer;
    }

    /**
     * @param $email_value
     * @return mixed
     */
    public function getDealAgentsWithoutUserByEmailValue($email_value = '')
    {
        if (empty($email_value)) {
            return [];
        }

        return $this->entityManager->getRepository(DealAgent::class)->findBy([
            'email' => $email_value,
            'user' => null,
        ]);
    }
}