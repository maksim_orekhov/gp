<?php

namespace Application\Service\Deal;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\LegalEntity;
use Application\Entity\NaturalPerson;
use Application\Entity\Payment;
use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\Entity\DeliveryServiceType;
use ModuleDelivery\Service\DeliveryOrderManager;
use ModuleDeliveryDpd\Entity\DpdCity;
use ModuleDeliveryDpd\Entity\DpdDeliveryAddress;

/**
 * Class DealChangesManager
 * @package Application\Service\Deal
 */
class DealChangesManager
{
    //
    const PROPERTIES_NAME_TRANSLATIONS = [
        'Application\Entity\Deal' => [
            'name'              => 'Название сделки',
            'dealType'          => 'Тип сделки',
            'feePayerOption'    => 'Оплата комиссии',
            'addonTerms'        => 'Дополнительные условия',
            'deliveryPeriod'    => 'Срок доставки товара/оказания услуги',
            'created'           => 'Дата создания',
        ],
        'Application\Entity\Payment' => [
            'dealValue'         => 'Сумма сделки',
            'expectedValue'     => 'Сумма к оплате покупателем/заказчиком',
            'releasedValue'     => 'Сумма к получению продавцом/исполнителем',
        ],
        'Application\Entity\DealAgent' => [
            'civilLawSubject'   => 'Субъект гражданского права',
            'dealAgentConfirm'  => 'Согласие c условиями сделки'
        ],
        'ModuleDelivery\Entity\DeliveryOrder' => [
            'deliveryServiceType'   => 'Тип доставки',
        ],
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress' => [
            'cityId'   => 'Населенный пункт отправления/доставки',
        ]
    ];

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var DealChangePolitics
     */
    private $dealChangePolitics;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;
    /**
     * @var DeliveryOrderManager
     */
    private $deliveryOrderManager;

    /**
     * @var User
     */
    private $user;
    /**
     * @var Deal
     */
    private $deal;

    /**
     * DealChangesManager constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param DealChangePolitics $dealChangePolitics
     * @param CivilLawSubjectManager $civilLawSubjectManager
     * @param DeliveryOrderManager $deliveryOrderManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                DealChangePolitics $dealChangePolitics,
                                CivilLawSubjectManager $civilLawSubjectManager,
                                DeliveryOrderManager $deliveryOrderManager)
    {
        $this->entityManager            = $entityManager;
        $this->dealChangePolitics       = $dealChangePolitics;
        $this->civilLawSubjectManager   = $civilLawSubjectManager;
        $this->deliveryOrderManager = $deliveryOrderManager;
    }

    /**
     * @param \Doctrine\ORM\UnitOfWork $unitOfWork
     * @param User $user
     * @return array
     */
    public function getDealProperties(\Doctrine\ORM\UnitOfWork $unitOfWork, User $user): array
    {
        // Массив для сбора измененных свойств всех entity (название entity, массив измененых свойств)
        $deal_changed_properties = [];
        try {
            $this->user = $user;
            $this->deal = null;

            $entities = [];
            $action = null;
            if (!empty($unitOfWork->getScheduledEntityUpdates())) {
                $entities = $unitOfWork->getScheduledEntityUpdates();
                $action = 'edit';
            } elseif (!empty($unitOfWork->getScheduledEntityInsertions())) {
                $entities = $unitOfWork->getScheduledEntityInsertions();
                $action = 'create';
            }
            // Обходим updatedEntities и собираем измененные свойства со старыми и новыми значениями
            foreach ($entities as $entity) {
                if ($entity instanceof Deal ||
                    $entity instanceof Payment ||
                    $entity instanceof DealAgent ||
                    $entity instanceof DeliveryOrder ||
                    $entity instanceof DpdDeliveryAddress
                ) {

                    if ($entity instanceof Deal) {
                        $this->deal = $entity;
                    }
                    if ($entity instanceof DeliveryOrder) {
                        $this->deal = $entity->getDeal();
                    }
                    if ($entity instanceof DpdDeliveryAddress) {
                        $this->deal = $this->getDealByDeliveryAddressId($entity->getId());
                    }

                    // Get set of entity changed properties
                    $changeset = $unitOfWork->getEntityChangeSet($entity);
                    // Get Entity name (eg 'Application\Entity\Deal')
                    $entity_name = $this->entityManager->getClassMetadata(\get_class($entity))->getName();
                    // If changeset not empty get 'new' and 'previous' values of changed properties
                    if (\is_array($changeset) && !empty($changeset)) {
                        // Get array of not ignored changed properties
                        $entity_changed_properties = $this->getEntityChangesArray($changeset, $entity_name);
                        // Ef not empty add to deal_changed_properties array
                        if (!empty($entity_changed_properties)) {
                            $deal_changed_properties[$entity_name] = $entity_changed_properties;
                        }
                    }
                }
            }
        }
        catch (\Throwable $t) {

            logMessage($t->getMessage(), 'deal-errors');
        }
        return [
            'deal' => $this->deal,
            'action' => $action,
            'deal_changed_properties' => $deal_changed_properties
        ];
    }

    /**
     * @param array $changeset
     * @param string $entity_name
     * @return array
     */
    private function getEntityChangesArray(array $changeset, string $entity_name): array
    {
        // Массив для сбора измененных свойств entity (название свойства, его старое и новое значения)
        $entity_changed_properties = [];

        foreach ($changeset as $key=>$change) {
            // Если свойства нет в массиве игнорируемых
            if (!$this->dealChangePolitics->isPropertyIgnored($entity_name.'.'.$key)) {
                if (array_key_exists(0, $change) && array_key_exists(1, $change)) {
                    // previous and new values
                    $previous = !\is_object($change[0]) ? $change[0] : $this->getObjectValue($change[0]);
                    $new = !\is_object($change[1]) ? $change[1] : $this->getObjectValue($change[1]);
                    //пропуск
                    $pass = false;
                    // Если изменен feePayerOption
                    if ($key === 'feePayerOption') {
                        $previous   = $previous ? DealManager::FEE_PAYER_OPTION_TRANSLATE[$previous] : null;
                        $new        = $new ? DealManager::FEE_PAYER_OPTION_TRANSLATE[$new] : null;
                    }
                    // Если изменен civilLawSubject
                    if ($key === 'civilLawSubject') {
                        $previous   = $this->getCivilLawSubjectAssignedName($previous);
                        $new        = $this->getCivilLawSubjectAssignedName($new);
                    }
                    // Если изменен cityId
                    if ($key === 'cityId') {
                        $contractor = $this->deal ? $this->deal->getContractor() : null;
                        if ((int) $previous === (int) $new || ($contractor && $contractor->getUser() === $this->user)) {
                            $pass = true;
                        }
                        $previous   = $this->getFullCityNameAssignedName($previous);
                        $new        = $this->getFullCityNameAssignedName($new);
                    }
                    // Если изменен deliveryServiceType
                    if ($key === 'deliveryServiceType') {
                        $previous   = $this->getDeliveryServiceTypeAssignedName($previous);
                        $new        = $this->getDeliveryServiceTypeAssignedName($new);
                    }

                    if (array_key_exists($entity_name, self::PROPERTIES_NAME_TRANSLATIONS)
                        && array_key_exists($key, self::PROPERTIES_NAME_TRANSLATIONS[$entity_name])) {
                        $property_name = self::PROPERTIES_NAME_TRANSLATIONS[$entity_name][$key];
                    } else {
                        $property_name = null;
                    }

                    if ($property_name && !$pass) {
                        $entity_changed_properties[$key] = [
                            'previous' => $previous,
                            'new' => $new,
                            'property_name' => $property_name
                        ];
                    }
                }
            }
        }

        return $entity_changed_properties;
    }

    /**
     * @param int|null $civilLawSubject_id
     * @return null|string
     */
    private function getCivilLawSubjectAssignedName($civilLawSubject_id = null)
    {
        $name = null;

        if (!$civilLawSubject_id) {

            return $name;
        }

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->civilLawSubjectManager->get($civilLawSubject_id);

        if ($civilLawSubject instanceof CivilLawSubject) {

            $AssignedCivilLawSubject = $this->civilLawSubjectManager->getAssignedCivilLawSubject($civilLawSubject);

            if ($AssignedCivilLawSubject instanceof NaturalPerson) {

                $name = $AssignedCivilLawSubject->getLastName() . " "
                    . $AssignedCivilLawSubject->getFirstName() . " "
                    . $AssignedCivilLawSubject->getSecondaryName();

            } elseif ($AssignedCivilLawSubject instanceof LegalEntity) {

                $name = $AssignedCivilLawSubject->getName();
            }
        }

        return $name;
    }

    /**
     * @param int|null $city_id
     * @return null|string
     */
    private function getFullCityNameAssignedName($city_id = null)
    {
        $name = null;

        if (!$city_id) {

            return $name;
        }

        $dpdCity = $this->deliveryOrderManager->getDpdCityByCityId($city_id);

        if ($dpdCity instanceof DpdCity) {

            $name = $dpdCity->getFullCityName() ?? $dpdCity->getCityName();
        }

        return $name;
    }

    /**
     * @param int|null $id
     * @return null|string
     */
    private function getDealByDeliveryAddressId($id = null)
    {
        $name = null;

        if (!$id) {

            return null;
        }
        /** @var DeliveryOrder $deliveryOrder */
        $deliveryOrder = $this->deliveryOrderManager->getDeliveryOrderByAddressId($id);

        return $deliveryOrder->getDeal();
    }

    /**
     * @param string|null $name
     * @return null|string
     */
    private function getDeliveryServiceTypeAssignedName($name = null)
    {
        $deliveryServiceType = $this->deliveryOrderManager->getDeliveryServiceTypeByName($name ?? '');

        if ($deliveryServiceType instanceof DeliveryServiceType) {

            $name = $deliveryServiceType->getOutputName();
        }

        return $name;
    }

    /**
     * @param $object
     * @return int|string
     */
    private function getObjectValue($object)
    {
        if($object instanceof \DateTime) {
            $value = date_format($object,"d-m-Y H:i");
        } elseif(method_exists($object, 'getName')) {
            $value = $object->getName();
        } else {
            $value = $object->getId();
        }

        return $value;
    }
}