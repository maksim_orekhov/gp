<?php

namespace Application\Service\Deal;

use Application\Entity\DealType;
use Core\Exception\LogicException;
use \Doctrine\ORM\EntityManager;

/**
 * Class DealTypeManager
 * @package Application\Service\Deal
 */
class DealTypeManager
{
    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    private $entityManager;

    /**
     * DealTypeManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @param $id
     * @return null|object
     * @throws LogicException
     */
    public function getDealTypeById($id)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)->find((int) $id);

        if (!$dealType || !($dealType instanceof DealType)) {

            throw new LogicException(null, LogicException::DEAL_TYPE_NOT_FOUND);
        }

        return $dealType;
    }

    /**
     * Returns collection of Deal Type objects
     *
     * @return array
     */
    public function getDealTypes()
    {
        return $this->entityManager->getRepository(DealType::class)->findAll();
    }

    /**
     * Returns associative array of Deal Types names
     *
     * @return array
     */
    public function getDealTypesAsArray()
    {
        $dealTypes = $this->getDealTypes();

        $deal_types_array = [];
        /** @var DealType $dealType */
        foreach ($dealTypes as $dealType) {
            $id = $dealType->getId();
            $deal_types_array[$id] = [
                'id' => $id,
                'ident' => $dealType->getIdent(),
                'name' => $dealType->getName()
            ];
        }

        return $deal_types_array;
    }
}