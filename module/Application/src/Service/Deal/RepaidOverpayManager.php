<?php
namespace Application\Service\Deal;

use Application\Entity\Payment;
use Application\Entity\RepaidOverpay;
use Application\Service\Deal\Status\RefundStatus;
use Application\Service\Deal\Status\TribunalStatus;
use Application\Service\Payment\PaymentMethodManager;
use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;

/**
 * Class RepaidOverpayManager
 * @package Application\Service\Deal
 */
class RepaidOverpayManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var PaymentOrderManager
     */
    private $paymentOrderManager;
    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;
    /**
     * @var PayOffManager
     */
    private $payOffManager;
    /**
     * @var TribunalStatus
     */
    private $tribunalStatus;
    /**
     * @var RefundStatus
     */
    private $refundStatus;

    /**
     * RepaidOverpayManager constructor.
     * @param EntityManager $entityManager
     * @param PaymentOrderManager $paymentOrderManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param PayOffManager $payOffManager
     * @param TribunalStatus $tribunalStatus
     * @param RefundStatus $refundStatus
     */
    public function __construct(EntityManager $entityManager,
                                PaymentOrderManager $paymentOrderManager,
                                PaymentMethodManager $paymentMethodManager,
                                PayOffManager $payOffManager,
                                TribunalStatus $tribunalStatus,
                                RefundStatus $refundStatus)
    {
        $this->entityManager = $entityManager;
        $this->paymentOrderManager = $paymentOrderManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->payOffManager = $payOffManager;
        $this->tribunalStatus = $tribunalStatus;
        $this->refundStatus = $refundStatus;
    }

    /**
     * @param Payment $payment
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function createRepaidOverpay(Payment $payment)
    {
        $deal = $payment->getDeal();
        // 1. проверяем статус сделки
        if ($this->tribunalStatus->isDealInStatus($deal) || $this->refundStatus->isDealInStatus($deal)) {

            throw new LogicException(null, LogicException::REPAID_OVERPAY_CREATE_IS_NOT_ALLOWED_STATUS_DEAL);
        }
        // 2. получаем и проверяем сумму переплаты
        $overpaid_amount = $this->paymentOrderManager->getTotalOverpaidAmountByPayment($payment);
        if ($overpaid_amount === 0) {

            throw new LogicException(null, LogicException::REPAID_OVERPAY_NON_PERMISSIBLE_AMOUNT);
        }
        // 3. получаем реквизиты
        $paymentMethodBankTransfer = $this->paymentMethodManager->getCustomerPaymentMethodByDeal($deal);
        if(!$paymentMethodBankTransfer){
            throw new \Exception('PaymentMethodBankTransfer not found');
        }

        try {
            // DB Transaction start
            $this->entityManager->getConnection()->beginTransaction();

            $repaidOverpay = new RepaidOverpay();
            $repaidOverpay->setCreated(new \DateTime());
            $repaidOverpay->setPaymentMethod($paymentMethodBankTransfer->getPaymentMethod());
            $repaidOverpay->setPayment($payment);
            $repaidOverpay->setAmount($overpaid_amount);
            $this->entityManager->persist($repaidOverpay);
            $this->entityManager->flush($repaidOverpay);

            $payment->setRepaidOverpay($repaidOverpay);
            $this->entityManager->persist($payment);
            $this->entityManager->flush($payment);

            //запускаем механизм выплаты переплаты
            /** @var PaymentOrder $paymentOrder */
            $this->payOffManager->createPayOff($deal, PayOffManager::PURPOSE_TYPE_OVERPAY);

            // DB Transaction commit
            $this->entityManager->getConnection()->commit();

        }
        catch (\Throwable $throwable) {
            // DB Transaction rollback
            $this->entityManager->getConnection()->rollBack();
            $this->entityManager->getConnection()->close();

            logException($throwable, 'payoff-errors', 2);

            throw $throwable;
        }

        return true;
    }
}