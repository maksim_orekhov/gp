<?php

namespace Application\Service\Deal;

use Application\Entity\Deal;
use Application\Entity\DealType;
use Application\Entity\Delivery;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\Refund;
use Application\Entity\Tribunal;
use Application\Entity\User;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\PaymentTransactionInterface;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class TimelineManager
 * @package Application\Service\Deal
 */
class TimelineManager
{
    const TIMELITE_NEGOTIATION_PHASE_AFTER = 'Создана';

    const TIMELITE_CONFIRMED_PHASE_BEFORE = 'Согласование';
    const TIMELITE_CONFIRMED_PHASE_AFTER = 'Согласована';

    const TIMELITE_DEBIT_MATCHED_PHASE_BEFORE = 'Ожидает оплаты';
    const TIMELITE_DEBIT_MATCHED_PHASE_AFTER = 'Оплачена';

    const TIMELITE_DISPUTE_PHASE_IN = 'Спор';

    const TIMELITE_PENDING_REFUND_PAYMENT_PHASE_U = 'Ожидает отказа';
    const TIMELITE_PENDING_REFUND_PAYMENT_PHASE_T = 'Ожидает возврата';

    const TIMELITE_REFUND_PHASE_U = 'Отказ';
    const TIMELITE_REFUND_PHASE_T = 'Возврат';

    const TIMELITE_TRIBUNAL_PHASE_IN = 'Суд';

    const TIMELITE_DELIVERED_PHASE_BEFORE_T = 'Доставка товара';
    const TIMELITE_DELIVERED_PHASE_BEFORE_U = 'Выполнение';
    const TIMELITE_DELIVERED_PHASE_AFTER_T = 'Доставлено';
    const TIMELITE_DELIVERED_PHASE_AFTER_U = 'Выполнено';
    const MANDARIN_ACQUIRING = 'MANDARIN_ACQUIRING_';
    const TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_BEFORE = 'Выплата';
    const TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_IN = 'Ожидает выплаты';
    const TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_AFTER = 'Выплачено';

    const TIMELITE_CLOSED_PHASE_BEFORE = 'Закрытие сделки';
    const TIMELITE_CLOSED_PHASE_AFTER = 'Сделка закрыта';

    const TIMELITE_TROUBLE_PHASE_IN = 'Проблема';
    private $contragentStatusUnregistered = 'true';
    /**
     * @var PaymentOrderManager
     */
    private $paymentOrderManager;
    private $is_need_to_fill_civil_law_subject;
    private $is_need_to_fill_payment_method;
    private $is_need_to_fill_delivery;
    private $is_owner;
    private $type_ident;
    private $date_of_deadline_params;
    private $date_deadline_trigger = false;

    /**
     * TimelineManager constructor.
     * @param PaymentOrderManager $paymentOrderManager
     */
    public function __construct(PaymentOrderManager $paymentOrderManager)
    {
        $this->paymentOrderManager = $paymentOrderManager;
    }

    /**
     * @param Deal $deal
     * @return array
     * @throws \Exception
     */
    public function buildTimeLine(Deal $deal): array
    {
        $timeline_phases = $this->formTimeLineDraft($deal);

        $is_next_status_set = false;

        // Если не было спора или он закрыт удаляем елемент ['dispute'], иначе ['delivered']
        if (null === $deal->getDispute() || true === $deal->getDispute()->isClosed()) {
            // Нет спора
            unset(
                $timeline_phases['dispute'],
                $timeline_phases['tribunal'],
                $timeline_phases['pending_refund_payment'],
                $timeline_phases['refund']
            );
        } else {
            // Есть спор
            $timeline_phases = $this->getTimelineForDisputeDeal($timeline_phases);
            $is_next_status_set = true; // Чтобы после Спора ничего не было
        }

        $timeline = [];

        foreach ($timeline_phases as $kay => $phase) {

            if ($kay === 'trouble') {
                break;
            }

            $timeline[$kay] = $phase;

            if (empty($phase['date'])) {

                if (false === $is_next_status_set) {
                    $timeline[$kay]['is_next_status'] = true;
                    $is_next_status_set = true;
                }

                if (!array_key_exists('trouble', $timeline) && true === $timeline_phases['trouble']['is_current_status']) {

                    $phase['troubles'] = $timeline_phases['trouble']['troubles'];

                    unset($timeline[$kay]);
                    $timeline['trouble'] = $timeline_phases['trouble'];

                    $is_next_status_set = true; // Чтобы после Спора ничего не было
                }
            }
        }

        return $timeline;
    }

    /**
     * @param Deal $deal
     * @param User $user
     * @param bool $dealOutput
     * @return array
     * @throws \Exception
     */
    public function buildDealHistory(Deal $deal, User $user, $dealOutput = true): array
    {
        /** @var  $dealHistory */
        $dealHistory = $this->formTimeLineDraft($deal);
        $deadLineDate = $deal->deadlineDate ? $deal->deadlineDate->format('d.m.Y') : null;
        $this->contragentStatusUnregistered = $dealOutput['is_counteragent_unregistered'];
        $this->is_need_to_fill_civil_law_subject = $dealOutput['is_need_to_fill_civil_law_subject'];
        $this->is_need_to_fill_payment_method = $dealOutput['is_need_to_fill_payment_method'];
        $this->is_need_to_fill_delivery = $dealOutput['is_need_to_fill_delivery'];
        $this->is_owner = $dealOutput['is_owner'];
        $this->type_ident = $dealOutput['type_ident'];
        $this->date_of_deadline_params = $dealOutput['date_of_deadline_params'];

        unset(
            $dealHistory['dispute'],
            $dealHistory['tribunal'],
            $dealHistory['pending_refund_payment'],
            $dealHistory['refund']
        );

        $dealHistoryOutput = [];
        foreach ($dealHistory as $key => $dealHistorySingle) {
            if ($dealHistorySingle['is_current_status_match']) {
                $arrayResult['code'] = strtoupper($key . $this->getUserStatus($deal, $user));
                $arrayResult['data'] = $this->getDealHistoryParams($deal, $key);
                $arrayResult['date'] = $dealHistorySingle['date'];
                $arrayResult['created'] = $this->formatDateOfEvent($dealHistorySingle['date']);
                $arrayResult['description'] = $dealHistorySingle['description'];
                $dealHistoryOutput[] = $arrayResult;
            }
        }
        //@TODO закоментировал 01.08.2018 так как выдает ошибку при выводе истории сделки
        #if ($dealOutput['confirm_reset_by']) {
        #    array_push($dealHistoryOutput, self::getIsCurrentUserOnceConfirmed($dealOutput, self::getUserStatus($deal, $user)));
        #}

        /** @var array $dealOutput */
        if (array_key_exists('mandarin_acquiring', $dealOutput) && $dealOutput['mandarin_acquiring'] !== null) {
            $dealHistoryOutput[] = $this->addMandarinStatuses($dealOutput, $deadLineDate, $this->getUserStatus($deal, $user));
        }
        if ($deal->deadlineDate) {
            /** @var \DateTime $deadLineDateCloned */
            $deadLineDateCloned = clone $deal->deadlineDate;
            $currentDate = new \DateTime();
            if ($deal->deadlineDate < $currentDate && $deadLineDate !== null) {
                $dealHistoryOutput[] = $this->checkDeadlineStatusForExpired($deal->deadlineDate, $this->getUserStatus($deal, $user));
            } elseif ($deadLineDate !== null && $deadLineDateCloned->modify('-' . DealDeadlineManager::HOUR_FOR_DEADLINE_NOTIFY . ' hours') <= $currentDate) {
                $dealHistoryOutput[] = $this->checkDeadlineStatusForExpiredInOneDay($deal->deadlineDate, $this->getUserStatus($deal, $user));
            }
        }

        return $dealHistoryOutput;
    }

    /**
     * @param $dealOutput
     * @param $dateDeadline
     * @param $prefix
     * @return array
     */
    private function addMandarinStatuses($dealOutput, $dateDeadline, $prefix)
    {
        return ['code' => self::MANDARIN_ACQUIRING . strtoupper($dealOutput['mandarin_acquiring']['status']) . $prefix,
            'data' => [
                'date_of_deadline' => $dateDeadline,
                'type_ident' => $this->type_ident,
            ],
            'created' => $dealOutput['mandarin_acquiring']['created'],
        ];
    }

    /**
     * @param $dateDeadline
     * @param $now
     * @param String $prefix
     * @return array
     */
    private function checkDeadlineStatusForExpiredInOneDay(\DateTime $dateDeadline, String $prefix)
    {
        return ['code' => 'DEADLINE_EXPIRED_IN_ONE_DAY' . $prefix,
            'data' => [
                'date_of_deadline' => $dateDeadline->format('d.m.Y'),
                'is_owner' => $this->is_owner,
            ],
            'created' => $dateDeadline->format('d.m.Y H:i:s')
        ];
    }

    /**
     * @param $dateDeadline
     * @param $now
     * @param String $prefix
     * @return array
     */
    private function checkDeadlineStatusForExpired(\DateTime $dateDeadline, String $prefix)
    {
        return ['code' => 'DEADLINE_EXPIRED' . $prefix,
            'data' => [
                'date_of_deadline' => $dateDeadline->format('d.m.Y'),
                'is_owner' => $this->is_owner,
            ],
            'created' => $dateDeadline->format('d.m.Y H:i:s')

        ];
    }

    /**
     * @param $dealOutput
     * @param $prefix
     * @return array
     */
    private function getIsCurrentUserOnceConfirmed($dealOutput, $prefix)
    {
        $dealOutput['confirm_reset_by'] = $dealOutput['confirm_reset_by'] ?? null;
        return ['code' => 'EDITED' . $prefix,
            'data' => [
                'is_customer_once_confirmed' => $dealOutput['is_customer_once_confirmed'],
                'is_contractor_once_confirmed' => $dealOutput['is_contractor_once_confirmed'],
                'confirm_reset_by' => $dealOutput['confirm_reset_by']
            ]

        ];
    }

    /**
     * @param Deal $deal
     */
    private function getDealHistoryParams(Deal $deal, $key)
    {

        switch ($key) {
            case 'created':
                return [
                    'is_counteragent_unregistered' => $this->contragentStatusUnregistered,
                    'is_need_to_fill_civil_law_subject' => $this->is_need_to_fill_civil_law_subject,
                    'is_need_to_fill_payment_method' => $this->is_need_to_fill_payment_method,
                    'is_need_to_fill_delivery' => $this->is_need_to_fill_delivery,
                    'is_owner' => $this->is_owner,
                ];
                break;
            case 'debit_matched':
                return [
                    'date_of_deadline' => $deal->deadlineDate ? $deal->deadlineDate->format('d.m.Y') : null,
                    'deal_type_identity' => $deal->getDealType()->getIdent(),
                    'date_of_deadline_params' => $this->date_of_deadline_params
                ];
                break;
            case 'closed':
                $this->date_deadline_trigger = true;
                return [];
                break;
            default:
                return [
                    'is_owner' => $this->is_owner,
                ];
        }

    }

    private function getUserStatus(Deal $deal, $user)
    {
        if ($user === $deal->getCustomer()->getUser()) return '_FOR_CUSTOMER';
        if ($user === $deal->getContractor()->getUser()) return '_FOR_CONTRACTOR';
        return '_FOR_OPERATOR';
    }

    /**
     * @param $timeArray
     * @return null|string
     */
    private function formatDateOfEvent($timeArray)
    {
        /** @var \DateTime $dateOfEvent */
        if (isset($timeArray['day']) && isset($timeArray['time'])) {
            $dateOfEvent = new \DateTime($timeArray['day'] . $timeArray['time']);

            return $dateOfEvent->format('d.m.Y H:i:s');
        }

        return null;
    }

    /**
     * @param array $timeline_phases
     * @return array
     */
    private function getTimelineForDisputeDeal(array $timeline_phases)
    {
        unset($timeline_phases['delivered']);

        if (empty($timeline_phases['tribunal']['date'])) {

            unset($timeline_phases['tribunal']);
        }

        if (empty($timeline_phases['refund']['date'])) {

            unset($timeline_phases['refund']);
        }

        if (empty($timeline_phases['pending_refund_payment']['date'])) {

            unset($timeline_phases['pending_refund_payment']);
        }

        if (array_key_exists('pending_refund_payment', $timeline_phases)
            || array_key_exists('refund', $timeline_phases)
            || array_key_exists('tribunal', $timeline_phases)) {

            unset(
                $timeline_phases['pending_credit_payment'],
                $timeline_phases['closed']
            );
        }

        return $timeline_phases;
    }

    /**
     * @param Deal $deal
     * @return array
     * @throws \Exception
     */
    private function formTimeLineDraft(Deal $deal): array
    {
        $timeline_phases = [];

        foreach (DealStatus::DEAL_STATUSES as $status) {

            switch ($status['status']) {

                case 'negotiation':
                    $timeline_phases['created'] = $this->addNegotiationStatusData($deal, $status);
                    break;

                case 'confirmed':
                    $timeline_phases['confirmed'] = $this->addConfirmedStatusData($deal, $status);
                    break;

                case 'debit_matched':
                    $timeline_phases['debit_matched'] = $this->addDebitMatchedStatusData($deal, $status);
                    break;

                case 'dispute':
                    $timeline_phases['dispute'] = $this->addDisputeStatusData($deal, $status);
                    break;

                case 'delivered':
                    $timeline_phases['delivered'] = $this->addDeliveredStatusData($deal, $status);
                    break;

                case 'pending_credit_payment':
                    $timeline_phases['pending_credit_payment'] = $this->addPendingCreditPaymentStatusData($deal, $status);
                    break;

                case 'closed':
                    $timeline_phases['closed'] = $this->addClosedStatusData($deal, $status);
                    break;

                case 'pending_refund_payment':
                    $timeline_phases['pending_refund_payment'] = $this->addPendingRefundPaymentStatusData($deal, $status);
                    break;

                case 'refund':
                    $timeline_phases['refund'] = $this->addRefundStatusData($deal, $status);
                    break;

                case 'tribunal':
                    $timeline_phases['tribunal'] = $this->addTribunalStatusData($deal, $status);
                    break;

                case 'trouble':
                    $timeline_phases['trouble'] = $this->addTroubleStatusData($deal, $status);
                    break;
            }
        }

        return $timeline_phases;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     */
    private function addTroubleStatusData(Deal $deal, array $status): array
    {
        $array = [];
        $array['is_current_status_match'] = false;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        $array['description'] = self::TIMELITE_TROUBLE_PHASE_IN;
        $array['date'] = [];

        if (false === $array['is_current_status']) {

            return $array;
        }

        $array['is_current_status_match'] = true;
        $array['troubles'] = $deal->status['name'];

        return $array;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     */
    private function addTribunalStatusData(Deal $deal, array $status): array
    {
        $array = [];
        $array['is_current_status_match'] = false;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        $array['description'] = self::TIMELITE_TRIBUNAL_PHASE_IN;
        $array['date'] = [];

        if ($deal->status['code'] < $status['code']) {

            return $array;
        }

        if (false === $array['is_current_status']) {

            return $array;
        }

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Tribunal $tribunal */
        $tribunal = $dispute->getTribunal();
        if (null !== $tribunal && null !== $tribunal->getCreated()) {

            $array['date']['day'] = $tribunal->getCreated()->format('d.m.Y');
            $array['date']['time'] = $tribunal->getCreated()->format('H:i:s');

            $array['is_current_status_match'] = true;
        }

        return $array;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     */
    private function addRefundStatusData(Deal $deal, array $status): array
    {
        $array = [];
        $array['is_current_status_match'] = false;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        $array['description'] = self::TIMELITE_CLOSED_PHASE_BEFORE;
        $array['date'] = [];

        if ($deal->status['code'] < $status['code']) {

            return $array;
        }

        if (false === $array['is_current_status']) {

            return $array;
        }

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Refund refund */
        $refund = $dispute->getRefund();
        if (null !== $refund && null !== $refund->getCreated()) {
            $array['date']['day'] = $refund->getCreated()->format('d.m.Y');
            $array['date']['time'] = $refund->getCreated()->format('H:i:s');

            $array['is_current_status_match'] = true;

            /** @var DealType $dealType */
            $dealType = $deal->getDealType();
            if ($dealType->getIdent() === 'T') {
                $array['description'] = self::TIMELITE_REFUND_PHASE_T;
            } else {
                $array['description'] = self::TIMELITE_REFUND_PHASE_U;
            }
        }

        return $array;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     */
    private function addPendingRefundPaymentStatusData(Deal $deal, array $status): array
    {
        $array = [];
        $array['is_current_status_match'] = false;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        $array['description'] = self::TIMELITE_CLOSED_PHASE_BEFORE;
        $array['date'] = [];

        if ($deal->status['code'] < $status['code']) {

            return $array;
        }

        if (false === $array['is_current_status']) {

            return $array;
        }

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Refund refund */
        $refund = $dispute->getRefund();
        if (null !== $refund && null !== $refund->getCreated()) {
            $array['date']['day'] = $refund->getCreated()->format('d.m.Y');
            $array['date']['time'] = $refund->getCreated()->format('H:i:s');

            $array['is_current_status_match'] = true;

            /** @var DealType $dealType */
            $dealType = $deal->getDealType();
            if ($dealType->getIdent() === 'T') {
                $array['description'] = self::TIMELITE_PENDING_REFUND_PAYMENT_PHASE_T;
            } else {
                $array['description'] = self::TIMELITE_PENDING_REFUND_PAYMENT_PHASE_U;
            }
        }

        return $array;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     */
    private function addClosedStatusData(Deal $deal, array $status): array
    {
        $array = [];
        $array['is_current_status_match'] = false;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        $array['description'] = self::TIMELITE_CLOSED_PHASE_BEFORE;
        $array['date'] = [];

        if ($deal->status['code'] < $status['code']) {

            return $array;
        }

        /** @var \DateTime $dealCloseDate */
        $dealCloseDate = $this->getDateOfDealClosed($deal->getPayment());

        if (null !== $dealCloseDate) {
            $array['date']['day'] = $dealCloseDate->format('d.m.Y');
            $array['date']['time'] = $dealCloseDate->format('H:i:s');

            $array['is_current_status_match'] = true;
            $array['description'] = self::TIMELITE_CLOSED_PHASE_AFTER;
        }

        return $array;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     * @throws \Exception
     */
    private function addPendingCreditPaymentStatusData(Deal $deal, array $status): array
    {
        $array = [];
        $array['is_current_status_match'] = false;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        $array['description'] = self::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_BEFORE;
        $array['date'] = [];

        if ($deal->status['code'] < $status['code']) {

            return $array;
        }

        /** @var PaymentTransactionInterface $paymentOutgoingPaymentOrder */
        $paymentOutgoingPaymentOrder = $this->paymentOrderManager
            ->getPaymentOutgoingPaymentTransaction($deal->getPayment());

        if ($paymentOutgoingPaymentOrder instanceof BankClientPaymentOrder
            && null !== $paymentOutgoingPaymentOrder->getBankClientPaymentOrderFile()) {

            $array['date']['day'] = $paymentOutgoingPaymentOrder->getCreated()->format('d.m.Y');
            $array['date']['time'] = $paymentOutgoingPaymentOrder->getCreated()->format('H:i:s');

            $array['is_current_status_match'] = true;
            $array['description'] = self::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_IN;

            if ($this->paymentOrderManager->isPayOff($paymentOutgoingPaymentOrder->getPaymentOrder())) {

                $array['description'] = self::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_AFTER;
            }
        }

        return $array;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     */
    private function addDeliveredStatusData(Deal $deal, array $status): array
    {
        /** @var DealType $dealType */
        $dealType = $deal->getDealType();

        $array = [];
        $array['is_current_status_match'] = false;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        if ($dealType->getIdent() === 'T') {
            $array['description'] = self::TIMELITE_DELIVERED_PHASE_BEFORE_T;
        } else {
            $array['description'] = self::TIMELITE_DELIVERED_PHASE_BEFORE_U;
        }
        $array['date'] = [];

        if ($deal->status['code'] < $status['code']) {

            return $array;
        }

        /** @var Delivery $delivery */
        foreach ($deal->getDeliveries() as $delivery) {
            if (true === $delivery->getDeliveryStatus()) {
                $array['date']['day'] = $delivery->getDeliveryDate()->format('d.m.Y');
                $array['date']['time'] = $delivery->getDeliveryDate()->format('H:i:s');

                $array['is_current_status_match'] = true;

                if ($dealType->getIdent() === 'T') {
                    $array['description'] = self::TIMELITE_DELIVERED_PHASE_AFTER_T;
                } else {
                    $array['description'] = self::TIMELITE_DELIVERED_PHASE_AFTER_U;
                }
            }
        }

        return $array;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     */
    private function addDisputeStatusData(Deal $deal, array $status): array
    {
        $array = [];
        $array['is_current_status_match'] = false;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        $array['phase_name'] = null;
        $array['date'] = [];

        if ($deal->status['code'] < $status['code']) {

            return $array;
        }

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        if (null !== $dispute && false === $dispute->isClosed() && null !== $dispute->getCreated()) {
            $array['date']['day'] = $dispute->getCreated()->format('d.m.Y');
            $array['date']['time'] = $dispute->getCreated()->format('H:i:s');

            $array['is_current_status_match'] = true;
            #$array['is_next_status'] = true;
            $array['description'] = self::TIMELITE_DISPUTE_PHASE_IN;
        }

        return $array;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     */
    private function addDebitMatchedStatusData(Deal $deal, array $status): array
    {
        $array = [];
        $array['is_current_status_match'] = false;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        $array['description'] = self::TIMELITE_DEBIT_MATCHED_PHASE_BEFORE;
        $array['date'] = [];

        if ($deal->status['code'] < $status['code']) {

            return $array;
        }

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        if (null !== $payment->getDeFactoDate()) {
            $array['date']['day'] = $payment->getDeFactoDate()->format('d.m.Y');
            $array['date']['time'] = $payment->getDeFactoDate()->format('H:i:s');

            $array['is_current_status_match'] = true;
            $array['description'] = self::TIMELITE_DEBIT_MATCHED_PHASE_AFTER;
        }

        return $array;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     */
    private function addConfirmedStatusData(Deal $deal, array $status): array
    {
        $array = [];
        $array['is_current_status_match'] = false;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        $array['description'] = self::TIMELITE_CONFIRMED_PHASE_BEFORE;
        $array['date'] = [];

        if ($deal->status['code'] < $status['code']) {

            return $array;
        }

        if (null !== $deal->getDateOfConfirm()) {
            $array['date']['day'] = $deal->getDateOfConfirm()->format('d.m.Y');
            $array['date']['time'] = $deal->getDateOfConfirm()->format('H:i:s');

            $array['is_current_status_match'] = true;
            $array['description'] = self::TIMELITE_CONFIRMED_PHASE_AFTER;
        }

        return $array;
    }

    /**
     * @param Deal $deal
     * @param array $status
     * @return array
     */
    private function addNegotiationStatusData(Deal $deal, array $status): array
    {
        $array = [];
        $array['is_current_status_match'] = true;
        $array['is_current_status'] = $this->isInStatus($deal, $status);
        $array['is_next_status'] = false;
        $array['description'] = self::TIMELITE_NEGOTIATION_PHASE_AFTER;

        $array['date'] = [
            'day' => $deal->getCreated()->format('d.m.Y'),
            'time' => $deal->getCreated()->format('H:i:s'),
        ];

        return $array;
    }


    /**
     * @param Deal $deal
     * @param array $status
     * @return bool
     */
    private function isInStatus(Deal $deal, array $status)
    {
        return $status['status'] === $deal->status['status'];
    }

    /**
     * @param Payment $payment
     * @return mixed|null
     */
    private function getDateOfDealClosed(Payment $payment)
    {
        $paymentConfirmationPaymentOrder = $this->paymentOrderManager
            ->getPaymentConfirmationPaymentOrder($payment);

        if ($paymentConfirmationPaymentOrder) {

            return $paymentConfirmationPaymentOrder->getCreated();
        }

        return null;
    }
}