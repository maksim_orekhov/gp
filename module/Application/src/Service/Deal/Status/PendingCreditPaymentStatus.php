<?php

namespace Application\Service\Deal\Status;

use Application\Entity\Dispute;
use Application\Service\Payment\PaymentPolitics;
use Application\Entity\Deal;
use Application\Entity\Payment;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Entity\PaymentOrder;
use Application\Service\SettingManager;
use ModulePaymentOrder\Entity\PaymentTransactionInterface;
use ModulePaymentOrder\Service\PaymentOrderPolitics;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * ['code' => 25, 'status' => 'pending_credit_payment', 'name' => 'Ожидает выплаты']
 *
 * Class PendingCreditPaymentStatus
 * @package Application\Service\Deal\Status
 */
class PendingCreditPaymentStatus implements StatusInterface
{
    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * PendingCreditPayment constructor.
     * @param PaymentPolitics $paymentPolitics
     * @param SettingManager $settingManager
     * @param PaymentOrderManager $paymentOrderManager
     */
    public function __construct(PaymentPolitics $paymentPolitics,
                                SettingManager $settingManager,
                                PaymentOrderManager $paymentOrderManager)
    {
        $this->paymentPolitics  = $paymentPolitics;
        $this->settingManager   = $settingManager;
        $this->paymentOrderManager = $paymentOrderManager;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealInStatus(Deal $deal)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Проверяем готова ли сделка к выплате (сумма credit Amount равна ReleasedValue)
        if ($this->isOutgoingPaymentOrdersReadyToPay($payment)) {

            return true;
        }

        return false;
    }

    /**
     * @param Payment $payment
     * @return bool
     */
    protected function isOutgoingPaymentOrdersReadyToPay(Payment $payment): bool
    {
        $outgoing_payment_amount = 0;
        $discount_amount = 0;

        // Проверяем наличие платёжек у Payment
        if ($payment->getPaymentOrders()->count() > 0) {

            // PAYMENT
            $allOutgoingPaymentTransactions = $this->paymentOrderManager
                ->getAllPaymentTransactionsByType($payment, PaymentOrderManager::TYPE_OUTGOING);

            /** @var PaymentTransactionInterface $outgoingPaymentTransaction */
            foreach ($allOutgoingPaymentTransactions as $outgoingPaymentTransaction) {

                if ($this->paymentOrderManager->isPayerGP($outgoingPaymentTransaction)) {

                    if (!PaymentOrderPolitics::isOutgoingPaymentTransactionReadyToPay($outgoingPaymentTransaction)) {

                        return false;
                    }

                    $outgoing_payment_amount += $outgoingPaymentTransaction->getAmount();
                }
            }

            // DISCOUNT
            /** @var Deal $deal */
            $deal = $payment->getDeal();
            /** @var Dispute $dispute */
            $dispute = $deal->getDispute();

            if ($dispute && $dispute->getDiscount()) {

                $allDiscountTransactions = $this->paymentOrderManager
                    ->getAllPaymentTransactionsByType($dispute->getDiscount(), PaymentOrderManager::TYPE_OUTGOING);

                /** @var PaymentTransactionInterface $discountTransaction */
                foreach ($allDiscountTransactions as $discountTransaction) {

                    if ($this->paymentOrderManager->isPayerGP($discountTransaction)) {

                        if (!PaymentOrderPolitics::isOutgoingPaymentTransactionReadyToPay($discountTransaction)) {

                            return false;
                        }

                        $discount_amount += $discountTransaction->getAmount();
                    }
                }
            }
        }

        return ($outgoing_payment_amount + $discount_amount) === $payment->getReleasedValue();
    }
}