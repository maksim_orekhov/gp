<?php

namespace Application\Service\Deal\Status;

use Application\Entity\Discount;
use Application\Entity\Dispute;
use Application\Entity\RepaidOverpay;
use Application\Service\Payment\PaymentPolitics;
use Application\Service\SettingManager;
use Application\Entity\Deal;
use Application\Entity\Payment;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * ['code' => 30, 'status' => 'closed', 'name' => 'Закрыто']
 *
 * Class ClosedStatus
 * @package Application\Service\Deal\Status
 */
class ClosedStatus implements StatusInterface
{
    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * ClosedStatus constructor.
     * @param PaymentPolitics $paymentPolitics
     * @param SettingManager $settingManager
     * @param PaymentOrderManager $paymentOrderManager
     */
    public function __construct(PaymentPolitics $paymentPolitics,
                                SettingManager $settingManager,
                                PaymentOrderManager $paymentOrderManager)
    {
        $this->paymentPolitics  = $paymentPolitics;
        $this->settingManager   = $settingManager;
        $this->paymentOrderManager = $paymentOrderManager;
    }

    /**
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    public function isDealInStatus(Deal $deal): bool
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $discount = null;

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        if ($dispute) {
            /** @var Discount $discount */
            $discount = $dispute->getDiscount();
        }

        // Проверяем выплачена ли сделка (Payment и Discount платёжки)
        if ($this->isDealPayOff($payment, $discount)) {

            return true;
        }

        return false;
    }

    /**
     * @param Payment $payment
     * @param Discount|null $discount
     * @return bool
     * @throws \Exception
     */
    protected function isDealPayOff(Payment $payment, $discount): bool
    {
        // Проверяем наличие подтверждающей outgoing Payment платежки
        /** @var BankClientPaymentOrder $paymentConfirmationPaymentOrder */
        $paymentConfirmationPaymentOrder = $this->paymentOrderManager
            ->getPaymentConfirmationPaymentOrder($payment);

        if ($paymentConfirmationPaymentOrder === null) {

            return false;
        }

        $payment_amount = $paymentConfirmationPaymentOrder->getAmount();

        if ($discount) {
            // Проверяем наличие платежки Discount
            /** @var BankClientPaymentOrder $discountConfirmationPaymentOrder */
            $discountConfirmationPaymentOrder = $this->paymentOrderManager
                ->getDiscountConfirmationPaymentOrder($discount);

            if ($discountConfirmationPaymentOrder !== null) {

                $payment_amount += $discount->getAmount();
            }
        }

        return $this->isRepaidOverpayPaidOff($payment)
            && $this->isDiscountPaidOff($discount)
            && $payment_amount === $payment->getReleasedValue();
    }

    /**
     * @param Payment $payment
     * @return bool
     * @throws \Exception
     */
    private function isRepaidOverpayPaidOff(Payment $payment): bool
    {
        /** @var RepaidOverpay $repaidOverpay */
        $repaidOverpay = $payment->getRepaidOverpay();

        if ($repaidOverpay) {
            // Проверяем наличие платежки RepaidOverpay
            /** @var BankClientPaymentOrder $repaidOverpayConfirmationPaymentOrder */
            $repaidOverpayConfirmationPaymentOrder = $this->paymentOrderManager
                ->getRepaidOverpayOutgoingPaymentTransaction($repaidOverpay);

            if ($repaidOverpayConfirmationPaymentOrder !== null) {

                return  $repaidOverpay->getAmount() === $repaidOverpayConfirmationPaymentOrder->getAmount();
            }
        }

        return true;
    }

    /**
     * @param Discount|null $discount
     * @return bool
     * @throws \Exception
     */
    private function isDiscountPaidOff($discount): bool
    {
        if ($discount) {
            // Проверяем наличие платежки Discount
            /** @var BankClientPaymentOrder $discountConfirmationPaymentOrder */
            $discountConfirmationPaymentOrder = $this->paymentOrderManager
                ->getDiscountConfirmationPaymentOrder($discount);

            if ($discountConfirmationPaymentOrder !== null) {

                return $discount->getAmount() === $discountConfirmationPaymentOrder->getAmount();
            }
        }

        return true;
    }
}