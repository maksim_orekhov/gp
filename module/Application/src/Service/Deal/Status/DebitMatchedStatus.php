<?php

namespace Application\Service\Deal\Status;

use Application\Service\Payment\PaymentPolitics;
use Application\Entity\Deal;
use Application\Entity\Payment;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * ['code' => 10, 'status' => 'debit_matched', 'name' => 'Оплачено']
 *
 * Class DebitMatchedStatus
 * @package Application\Service\Deal\Status
 */
class DebitMatchedStatus implements StatusInterface
{
    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * DebitMatchedStatus constructor.
     * @param PaymentPolitics $paymentPolitics
     * @param PaymentOrderManager $paymentOrderManager
     */
    public function __construct(PaymentPolitics $paymentPolitics,
                                PaymentOrderManager $paymentOrderManager)
    {
        $this->paymentPolitics  = $paymentPolitics;
        $this->paymentOrderManager = $paymentOrderManager;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealInStatus(Deal $deal): bool
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Проверяем, можно ли считать сделку оплаченой
        if ($payment->getPaymentOrders()->count() > 0) {

            try {
                $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);
            }
            catch (\Throwable $t) {

                return false;
            }

            // Если сделка не полностью оплачена
            if (!$this->paymentPolitics->isDealPaid($total_incoming_amount, $payment->getExpectedValue())) {

                return false;
            }
        }

        $allIncomingPaymentOrders = $this->paymentOrderManager
            ->getAllPaymentTransactionsByType($payment, PaymentOrderManager::TYPE_INCOMING);

        // Если нет нарушения политики (если сделка не проблемная)
        if (count($allIncomingPaymentOrders) > 0
            && empty($this->paymentPolitics->getDealTroubleTypes($payment, $allIncomingPaymentOrders))) {

            return true;
        }

        return false;
    }
}