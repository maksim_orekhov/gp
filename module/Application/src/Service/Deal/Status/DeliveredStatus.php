<?php

namespace Application\Service\Deal\Status;

use Application\Service\DeliveryManager;
use Application\Entity\Deal;

/**
 * ['code' => 20, 'status' => 'delivered', 'name' => 'Товар доставлен']
 *
 * Class DeliveredStatus
 * @package Application\Service\Deal\Status
 */
class DeliveredStatus implements StatusInterface
{
    /**
     * @var DeliveryManager
     */
    private $deliveryManager;


    public function __construct(DeliveryManager $deliveryManager)
    {
        $this->deliveryManager = $deliveryManager;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealInStatus(Deal $deal)
    {
        // Проверяем, что товар доставлен \ услуга оказана
        if ($this->deliveryManager->isDeliveryConfirmed($deal)) {

            return true;
        }

        return false;
    }
}