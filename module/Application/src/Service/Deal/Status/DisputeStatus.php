<?php

namespace Application\Service\Deal\Status;

use Application\Service\Deal\DealPolitics;
use Application\Entity\Deal;

/**
 * ['code' => 15, 'status' => 'dispute', 'name' => 'Спор'],
 *
 * Class TribunalStatus
 * @package Application\Service\Deal\Status
 */
class DisputeStatus implements StatusInterface
{
    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * DiscountStatus constructor.
     * @param DealPolitics $dealPolitics
     */
    public function __construct(DealPolitics $dealPolitics)
    {
        $this->dealPolitics = $dealPolitics;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealInStatus(Deal $deal)
    {
        // Если у сделки есть Спор (Dispute)
        if ($this->dealPolitics->isDealHasOpenDispute($deal)) {

            return true;
        }

        return false;
    }
}