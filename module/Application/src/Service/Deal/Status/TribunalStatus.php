<?php

namespace Application\Service\Deal\Status;

use Application\Service\Deal\DealPolitics;
use Application\Entity\Deal;

/**
 * ['code' => 40, 'status' => 'tribunal', 'name' => 'Трибунал']
 *
 * Class TribunalStatus
 * @package Application\Service\Deal\Status
 */
class TribunalStatus implements StatusInterface
{
    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * DiscountStatus constructor.
     * @param DealPolitics $dealPolitics
     */
    public function __construct(DealPolitics $dealPolitics)
    {
        $this->dealPolitics = $dealPolitics;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealInStatus(Deal $deal)
    {
        // Проверяем, что в сделке выполнен возврат средств
        if ($this->dealPolitics->isDealHasTribunalStatus($deal)) {

            return true;
        }

        return false;
    }
}