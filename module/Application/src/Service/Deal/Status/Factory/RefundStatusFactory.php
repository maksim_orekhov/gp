<?php

namespace Application\Service\Deal\Status\Factory;

use Application\Service\Deal\Status\RefundStatus;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class RefundStatusFactory
 * @package Application\Service\Deal\Status\Factory
 */
class RefundStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $paymentOrderManager = $container->get(PaymentOrderManager::class);

        return new RefundStatus($paymentOrderManager);
    }
}