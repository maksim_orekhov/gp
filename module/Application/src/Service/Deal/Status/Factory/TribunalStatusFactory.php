<?php

namespace Application\Service\Deal\Status\Factory;

use Application\Service\Deal\DealPolitics;
use Application\Service\Deal\Status\TribunalStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class TribunalStatusFactory
 * @package Application\Service\Deal\Status\Factory
 */
class TribunalStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealPolitics = $container->get(DealPolitics::class);

        return new TribunalStatus($dealPolitics);
    }
}