<?php

namespace Application\Service\Deal\Status\Factory;

use Application\Service\Payment\PaymentPolitics;
use Application\Service\Deal\Status\PendingCreditPaymentStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\SettingManager;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class PendingCreditPaymentStatusFactory
 * @package Application\Service\Deal\Status\Factory
 */
class PendingCreditPaymentStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $paymentPolitics = $container->get(PaymentPolitics::class);
        $settingManager = $container->get(SettingManager::class);
        $paymentOrderManager = $container->get(PaymentOrderManager::class);

        return new PendingCreditPaymentStatus($paymentPolitics, $settingManager, $paymentOrderManager);
    }
}