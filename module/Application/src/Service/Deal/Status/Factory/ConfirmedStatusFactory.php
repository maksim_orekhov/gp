<?php

namespace Application\Service\Deal\Status\Factory;

use Application\Service\Deal\DealManager;
use Application\Service\Deal\Status\ConfirmedStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class ConfirmedStatusFactory
 * @package Application\Service\Deal\Status\Factory
 */
class ConfirmedStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealManager = $container->get(DealManager::class);

        return new ConfirmedStatus($dealManager);
    }
}