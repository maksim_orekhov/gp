<?php

namespace Application\Service\Deal\Status\Factory;

use Application\Service\DeliveryManager;
use Application\Service\Deal\Status\DeliveredStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class DeliveredStatusFactory
 * @package Application\Service\Deal\Status\Factory
 */
class DeliveredStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $deliveryManager     = $container->get(DeliveryManager::class);

        return new DeliveredStatus($deliveryManager);
    }
}