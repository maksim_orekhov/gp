<?php

namespace Application\Service\Deal\Status\Factory;

use Application\Service\Payment\PaymentPolitics;
use Application\Service\SettingManager;
use Application\Service\Deal\Status\ClosedStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class ClosedStatusFactory
 * @package Application\Service\Deal\Status\Factory
 */
class ClosedStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $paymentPolitics    = $container->get(PaymentPolitics::class);
        $settingManager     = $container->get(SettingManager::class);
        $paymentOrderManager = $container->get(PaymentOrderManager::class);

        return new ClosedStatus($paymentPolitics, $settingManager, $paymentOrderManager);
    }
}