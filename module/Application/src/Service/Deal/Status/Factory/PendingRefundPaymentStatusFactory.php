<?php

namespace Application\Service\Deal\Status\Factory;

use Application\Service\Deal\Status\PendingRefundPaymentStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class PendingCreditPaymentStatusFactory
 * @package Application\Service\Deal\Status\Factory
 */
class PendingRefundPaymentStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $paymentOrderManager = $container->get(PaymentOrderManager::class);

        return new PendingRefundPaymentStatus($paymentOrderManager);
    }
}