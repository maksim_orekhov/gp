<?php

namespace Application\Service\Deal\Status\Factory;

use Application\Service\Payment\PaymentPolitics;
use Application\Service\Deal\Status\DebitMatchedStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class DebitMatchedStatusFactory
 * @package Application\Service\Deal\Status\Factory
 */
class DebitMatchedStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $paymentPolitics = $container->get(PaymentPolitics::class);
        $paymentOrderManager = $container->get(PaymentOrderManager::class);

        return new DebitMatchedStatus($paymentPolitics, $paymentOrderManager);
    }
}