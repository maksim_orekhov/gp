<?php

namespace Application\Service\Deal\Status\Factory;

use Application\Service\Deal\DealPolitics;
use Application\Service\Deal\Status\DisputeStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class DisputeStatusFactory
 * @package Application\Service\Deal\Status\Factory
 */
class DisputeStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealPolitics = $container->get(DealPolitics::class);

        return new DisputeStatus($dealPolitics);
    }
}