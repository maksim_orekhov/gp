<?php

namespace Application\Service\Deal\Status\Factory;

use Application\Service\Deal\Status\TroubleStatus;
use Application\Service\Payment\PaymentPolitics;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class TroubleStatusFactory
 * @package Application\Service\Deal\Status\Factory
 */
class TroubleStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $paymentPolitics = $container->get(PaymentPolitics::class);
        $paymentOrderManager = $container->get(PaymentOrderManager::class);

        return new TroubleStatus($paymentPolitics, $paymentOrderManager);
    }
}