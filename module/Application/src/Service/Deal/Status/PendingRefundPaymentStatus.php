<?php

namespace Application\Service\Deal\Status;

use Application\Entity\Dispute;
use Application\Entity\Refund;
use Application\Entity\Deal;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * ['code' => 34, 'status' => 'pending_refund_payment', 'name' => 'Ожидает возврата']
 *
 * Class DiscountStatus
 * @package Application\Service\Deal\Status
 */
class PendingRefundPaymentStatus implements StatusInterface
{
    /**
     * @var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * PendingRefundPaymentStatus constructor.
     * @param PaymentOrderManager $paymentOrderManager
     */
    public function __construct(PaymentOrderManager $paymentOrderManager)
    {
        $this->paymentOrderManager = $paymentOrderManager;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealInStatus(Deal $deal): bool
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        if ($dispute) {
            /** @var Refund $refund */
            $refund = $dispute->getRefund();
            // Проверяем, что есть Refund и непогашенная(нет подтверждающей) исходящая платёжка, попавшая в файл на выплату
            if (null !== $refund
                && $this->isRefundPaymentOrderInPending($refund)) {

                return true;
            }
        }

        return false;
    }

    /**
     * @param Refund $refund
     * @return bool
     */
    private function isRefundPaymentOrderInPending(Refund $refund): bool
    {
        try {
            if ($refund->getPaymentOrders()->count() > 0) {
                /** @var PaymentOrder $paymentOrder */
                foreach ($refund->getPaymentOrders() as $paymentOrder) {
                    /** @var BankClientPaymentOrder $bankClientPaymentOrder */
                    $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();
                    // Проверяем, что возвратная OUTGOING платёжка уже попала в файл на выплату, но реальный возврат не осуществлён
                    if (null !== $bankClientPaymentOrder
                        && $bankClientPaymentOrder->getType() === PaymentOrderManager::TYPE_OUTGOING
                        && null !== $bankClientPaymentOrder->getBankClientPaymentOrderFile()
                        && !$this->paymentOrderManager->isPayOff($paymentOrder)) {

                        return true;
                    }
                }
            }
        }
        catch (\Throwable $t) {

            return false;
        }

        return false;
    }
}