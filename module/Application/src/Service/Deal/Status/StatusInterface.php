<?php

namespace Application\Service\Deal\Status;

use Application\Entity\Deal;

/**
 * Interface StatusInterface
 * @package Application\Service\Deal\Status
 */
interface StatusInterface
{
    /**
     * @param Deal $deal
     * @return mixed
     */
    function isDealInStatus(Deal $deal);
}