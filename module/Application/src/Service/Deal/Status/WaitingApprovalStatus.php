<?php

namespace Application\Service\Deal\Status;

use Application\Entity\Deal;

/**
 * '0' => ['code' => 1, 'status' => 'negotiation', 'name' => 'Ожидает согласования'],
 *
 * Class ConfirmedStatus
 * @package Application\Service\Deal\Status
 */
class WaitingApprovalStatus implements StatusInterface
{
    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealInStatus(Deal $deal): bool
    {
        // Если сделка не подтверждена обоими (или одним) агентами и нет файла контракта
        if (!$this->isConfirmed($deal) && null === $deal->getDealContractFile()) {

            return true;
        }

        return false;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    private function isConfirmed(Deal $deal)
    {
        return $deal->getContractor()->getDealAgentConfirm() && $deal->getCustomer()->getDealAgentConfirm();
    }
}