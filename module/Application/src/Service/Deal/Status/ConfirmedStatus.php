<?php

namespace Application\Service\Deal\Status;

use Application\Entity\Deal;
use Application\Entity\Payment;

/**
 * ['code' => 5, 'status' => 'confirmed', 'name' => 'Согласовано']
 *
 * Class ConfirmedStatus
 * @package Application\Service\Deal\Status
 */
class ConfirmedStatus implements StatusInterface
{
    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealInStatus(Deal $deal)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Если сделка подтверждена обоими агентами, есть файл контракта, есть getDateOfConfirm и нет DeFactoDate
        if ($this->isConfirmed($deal) && null !== $deal->getDealContractFile()
            && null !== $deal->getDateOfConfirm() && null === $payment->getDeFactoDate()) {

            return true;
        }

        return false;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    private function isConfirmed(Deal $deal)
    {
        return $deal->getContractor()->getDealAgentConfirm() && $deal->getCustomer()->getDealAgentConfirm();
    }
}