<?php

namespace Application\Service\Deal\Status;

use Application\Service\Payment\PaymentPolitics;
use Application\Entity\Deal;
use Application\Entity\Payment;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class DebitMatchedStatus
 * @package Application\Service\Deal\Status
 */
class TroubleStatus implements StatusInterface
{
    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * DebitMatchedStatus constructor.
     * @param PaymentPolitics $paymentPolitics
     * @param PaymentOrderManager $paymentOrderManager
     */
    public function __construct(PaymentPolitics $paymentPolitics,
                                PaymentOrderManager $paymentOrderManager)
    {
        $this->paymentPolitics  = $paymentPolitics;
        $this->paymentOrderManager = $paymentOrderManager;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealInStatus(Deal $deal): bool
    {
        if (null === $deal->getDateOfConfirm()) {

            return false;
        }

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $allIncomingPaymentOrders = $this->paymentOrderManager
            ->getAllPaymentTransactionsByType($payment, PaymentOrderManager::TYPE_INCOMING);

        // Если нет нарушения политики (если сделка не проблемная)
        if (empty($this->paymentPolitics->getDealTroubleTypes($payment, $allIncomingPaymentOrders))) {

            return false;
        }

        return true;
    }

    /**
     * @param Deal $deal
     * @return array
     *
     * @TODO Перенести в DealManager?
     */
    public function getTroubleReason(Deal $deal): array
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $allIncomingPaymentOrders = $this->paymentOrderManager
            ->getAllPaymentTransactionsByType($payment, PaymentOrderManager::TYPE_INCOMING);

        return $this->paymentPolitics->getDealTroubleTypes($payment, $allIncomingPaymentOrders);
    }
}