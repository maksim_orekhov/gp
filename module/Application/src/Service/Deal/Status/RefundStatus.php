<?php

namespace Application\Service\Deal\Status;

use Application\Entity\Dispute;
use Application\Entity\Refund;
use Application\Entity\Deal;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * ['code' => 35, 'status' => 'refund', 'name' => 'Возврат']
 *
 * Class DiscountStatus
 * @package Application\Service\Deal\Status
 */
class RefundStatus implements StatusInterface
{
    /**
     * @var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * PendingRefundPaymentStatus constructor.
     * @param PaymentOrderManager $paymentOrderManager
     */
    public function __construct(PaymentOrderManager $paymentOrderManager)
    {
        $this->paymentOrderManager = $paymentOrderManager;
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDealInStatus(Deal $deal)
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        if ($dispute) {
            /** @var Refund $refund */
            $refund = $dispute->getRefund();
            // Проверяем, что есть Refund и погашенная(подтверждённая) исходящая платёжка
            if (null !== $refund
                && $this->isRefundHasPaidOffOutgoingBankClientPaymentOrder($refund)) {

                return true;
            }
        }

        return false;
    }

    /**
     * @param Refund $refund
     * @return bool
     */
    private function isRefundHasPaidOffOutgoingBankClientPaymentOrder(Refund $refund): bool
    {
        try {
            if ($refund->getPaymentOrders()->count() > 0) {
                /** @var PaymentOrder $paymentOrder */
                $paymentOrder = $refund->getPaymentOrders()->first();
                /** @var BankClientPaymentOrder $bankClientPaymentOrder */
                $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();

                if (null !== $bankClientPaymentOrder
                    && null !== $bankClientPaymentOrder->getBankClientPaymentOrderFile()
                    && $this->paymentOrderManager->isPayOff($paymentOrder)) {

                    return true;
                }
            }
        }
        catch (\Throwable $t) {

            return false;
        }

        return false;
    }
}