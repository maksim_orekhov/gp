<?php

namespace Application\Service\Deal;

/**
 * Class DealChangePolitics
 * @package Application\Service\Deal
 */
class DealChangePolitics
{
    // Массив свойств объектов, на изменение которых не реагируем
    const IGNORED_PROPERTIES = [
        'Application\Entity\Deal.number',
        'Application\Entity\Deal.updated',
        'Application\Entity\Deal.deFactoDate',
        'Application\Entity\Deal.dealContractFile',
        'Application\Entity\Deal.dateOfConfirm',
        'Application\Entity\Deal.owner',
        'Application\Entity\Deal.paymentOrders',
        'Application\Entity\Payment.paymentMethod',
        'Application\Entity\Payment.plannedDate',
        'Application\Entity\DealAgent.dateOfConfirm',
        'Application\Entity\DealAgent.civilLawSubject',
        // delivery Address
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.name',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.terminalCode',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.index',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.street',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.streetAbr',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.house',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.houseKorpus',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.str',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.vlad',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.extraInfo',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.office',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.flat',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.workTimeFrom',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.workTimeTo',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.dinnerTimeFrom',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.dinnerTimeTo',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.contactFio',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.contactPhone',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.contactEmail',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.instructions',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.needPass',
        // delivery order
        'ModuleDelivery\Entity\DeliveryOrder.orderNumber',
        'ModuleDelivery\Entity\DeliveryOrder.created',
        'ModuleDelivery\Entity\DeliveryOrder.termsOfDelivery',
        'ModuleDelivery\Entity\DeliveryOrder.deal',
        'ModuleDelivery\Entity\DeliveryOrder.dpdDeliveryOrder',
        'ModuleDelivery\Entity\DeliveryOrder.deliveryTrackingNumbers',
    ];

    // Массив свойств объектов, при изменении которых не сбрасываем согласие контрагента
    const COUNTERAGENT_CONFIRM_RESET_IGNORED_PROPERTIES = [
        'Application\Entity\Deal.number',
        'Application\Entity\Deal.updated',
        'Application\Entity\Deal.deFactoDate',
        'Application\Entity\Deal.dealContractFile',
        'Application\Entity\Deal.dateOfConfirm',
        'Application\Entity\Deal.owner',
        'Application\Entity\Deal.paymentOrders',
        'Application\Entity\Payment.paymentMethod',
        'Application\Entity\Payment.plannedDate',
        'Application\Entity\DealAgent.dealAgentConfirm',
        'Application\Entity\DealAgent.dateOfConfirm',
        'Application\Entity\DealAgent.civilLawSubject',
        // delivery Address
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.name',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.terminalCode',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.index',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.street',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.streetAbr',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.house',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.houseKorpus',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.str',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.vlad',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.extraInfo',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.office',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.flat',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.workTimeFrom',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.workTimeTo',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.dinnerTimeFrom',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.dinnerTimeTo',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.contactFio',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.contactPhone',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.contactEmail',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.instructions',
        'ModuleDeliveryDpd\Entity\DpdDeliveryAddress.needPass',
        // delivery order
        'ModuleDelivery\Entity\DeliveryOrder.orderNumber',
        'ModuleDelivery\Entity\DeliveryOrder.created',
        'ModuleDelivery\Entity\DeliveryOrder.termsOfDelivery',
        'ModuleDelivery\Entity\DeliveryOrder.deal',
        'ModuleDelivery\Entity\DeliveryOrder.dpdDeliveryOrder',
        'ModuleDelivery\Entity\DeliveryOrder.deliveryTrackingNumbers',
    ];

    /**
     * @param string $property
     * @return bool
     */
    public function isPropertyIgnored(string $property)
    {
        if(\in_array($property, self::IGNORED_PROPERTIES, true)) {

            return true;
        }

        return false;
    }

    /**
     * @param array $changed_properties
     * @return bool
     */
    public function isCounteragentConfirmMustReset(array $changed_properties): bool
    {
        $confirm_reset = false;

        if(\is_array($changed_properties) && !empty($changed_properties)) {

            foreach($changed_properties as $key => $entity_changes) {

                foreach ($entity_changes as $key2 => $change) {

                    if( !\in_array($key . '.' . $key2, self::COUNTERAGENT_CONFIRM_RESET_IGNORED_PROPERTIES, true)) {

                        $confirm_reset = true;
                    }

                }
            }
        }

        return $confirm_reset;
    }
}