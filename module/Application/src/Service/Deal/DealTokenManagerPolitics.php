<?php

namespace Application\Service\Deal;
use Application\Entity\CivilLawSubject;
use Application\Entity\User;

/**
 * Class DealTokenManagerPolitics
 * @package Application\Service\Deal
 */
class DealTokenManagerPolitics
{
    /**
     * @param array $data
     * @return bool
     */
    public function isCounteragentDataSufficient(array $data): bool
    {
        if (null === $data['counteragent_email'] && null === $data['counteragent_civil_law_subject']) {

            return false;
        }

        return true;
    }

    /**
     * @param CivilLawSubject $civilLLawSubject
     * @param string $counteragent_email
     * @return bool
     */
    public function isCounteragentDataMatch(CivilLawSubject $civilLLawSubject, string $counteragent_email)
    {
        $email = $civilLLawSubject->getUser()->getEmail();

        return strtolower($email->getEmail()) === strtolower($counteragent_email);
    }

    public function isUserAllowedCreateBySpecificToken(array $token_data, User $user = null): bool
    {
        $agent_email = null;
        if ($user instanceof User) {
            $agent_email = $user->getEmail()->getEmail();
        }

        if ($agent_email && $agent_email === $token_data['counteragent_email']) {

            return false;
        }

        return true;
    }

    /**
     * @param array $deal_data
     * @param array $token_data
     * @param User|null $user
     * @return bool
     */
    public function isIncomingDataCorrect(array $deal_data, array $token_data, User $user = null): bool
    {
        $agent_email = null;
        if (array_key_exists('agent_email', $deal_data) ) {
            $agent_email = $deal_data['agent_email'];
        } elseif ($user instanceof User) {
            $agent_email = $user->getEmail()->getEmail();
        }

        if (null === $agent_email) {

            return false;
        }

        if ($agent_email === $deal_data['counteragent_email']) {

            return false;
        }

        return $this->isDealAmountCorrect($deal_data['amount'], $token_data['amount']);
    }

    /**
     * @param $amount_from_deal_data
     * @param $amount_from_token
     * @return bool
     */
    private function isDealAmountCorrect($amount_from_deal_data, $amount_from_token): bool
    {
        if (null === $amount_from_token && $amount_from_deal_data !== null) {

            return true;
        }

        if (null !== $amount_from_token && (float)$amount_from_deal_data === (float)$amount_from_token)  {

            return true;
        }

        return false;
    }
}