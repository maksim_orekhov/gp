<?php

namespace Application\Service\Deal\Factory;

use Application\Provider\Mail\DealDeadlineNotificationSender;
use Application\Service\Deal\DealDeadlineManager;
use Application\Service\SettingManager;
use Core\Service\TwigRenderer;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class DealDeadlineManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DealDeadlineManager|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $twigRenderer = $container->get(TwigRenderer::class);
        $settingManager = $container->get(SettingManager::class);
        $config = $container->get('Config');
        $dealDeadlineNotificationSender = new DealDeadlineNotificationSender($twigRenderer, $config);

        return new DealDeadlineManager(
            $entityManager,
            $dealDeadlineNotificationSender,
            $settingManager,
            $config
        );
    }
}