<?php

namespace Application\Service\Deal\Factory;

use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Deal\DealStatus;
use Application\Service\Deal\DealTokenManager;
use Application\Service\Deal\DealTokenManagerPolitics;
use Application\Service\Deal\DealTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class DealTokenManagerFactory
 * @package Application\Service\Deal\Factory
 */
class DealTokenManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $dealTypeManager = $container->get(DealTypeManager::class);
        $dealTokenManagerPolitics = $container->get(DealTokenManagerPolitics::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $dealStatus = $container->get(DealStatus::class);

        return new DealTokenManager(
            $entityManager,
            $dealTypeManager,
            $dealTokenManagerPolitics,
            $civilLawSubjectManager,
            $paymentMethodManager,
            $dealStatus
        );
    }
}