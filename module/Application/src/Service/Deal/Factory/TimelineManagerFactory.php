<?php

namespace Application\Service\Deal\Factory;

use Application\Service\Deal\TimelineManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class TimelineManagerFactory
 * @package Application\Service\Deal\Factory
 */
class TimelineManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $paymentOrderManager = $container->get(PaymentOrderManager::class);

        return new TimelineManager($paymentOrderManager);
    }
}