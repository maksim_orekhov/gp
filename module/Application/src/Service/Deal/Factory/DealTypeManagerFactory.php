<?php

namespace Application\Service\Deal\Factory;

use Application\Service\Deal\DealTypeManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class DealTypeManagerFactory
 * @package Application\Service\Deal\Factory
 */
class DealTypeManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new DealTypeManager($entityManager);
    }
}