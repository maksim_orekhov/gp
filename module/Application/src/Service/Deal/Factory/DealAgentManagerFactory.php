<?php

namespace Application\Service\Deal\Factory;

use Core\Service\Base\BaseAuthManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\UserManager;
use Application\Service\Deal\DealAgentManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Deal\DealAgentPolitics;

class DealAgentManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        $entityManager          = $container->get('doctrine.entitymanager.orm_default');
        $userManager            = $container->get(UserManager::class);
        $baseAuthManager            = $container->get(BaseAuthManager::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);
        $dealAgentPolitics      = $container->get(DealAgentPolitics::class);

        return new DealAgentManager(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $civilLawSubjectManager,
            $dealAgentPolitics
        );
    }
}