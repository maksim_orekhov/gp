<?php
namespace Application\Service\Deal\Factory;

use Application\Service\NdsTypeManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Deal\DealPolitics;
use Application\Service\SettingManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\Deal\Status\DebitMatchedStatus;
use Application\Service\Payment\CalculatedDataProvider;

/**
 * Class DealPoliticsFactory
 * @package Application\Service\Deal\Factory
 */
class DealPoliticsFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DealPolitics
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $settingManager = $container->get(SettingManager::class);
        $paymentManager = $container->get(PaymentManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $ndsTypeManager = $container->get(NdsTypeManager::class);
        $debitMatchedStatus = $container->get(DebitMatchedStatus::class);
        $calculatedDataProvider = $container->get(CalculatedDataProvider::class);

        return new DealPolitics(
            $settingManager,
            $paymentManager,
            $paymentMethodManager,
            $ndsTypeManager,
            $debitMatchedStatus,
            $calculatedDataProvider
        );
    }
}