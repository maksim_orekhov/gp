<?php
namespace Application\Service\Deal\Factory;

use Application\Service\Deal\DealTokenManager;
use Application\Service\Deal\DealTypeManager;
use Application\Service\Deal\RepaidOverpayManager;
use Application\Service\Deal\TimelineManager;
use Application\Service\DeliveryManager;
use ModuleDelivery\Service\DeliveryOrderManager;
use ModuleFileManager\Service\FileManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealAgentManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Deal\DealPolitics;
use Application\Service\SettingManager;
use Application\Listener\CreditPaymentOrderAvailabilityListener;
use Zend\View\Renderer\RendererInterface;
use Application\Service\Deal\DealStatus;
use Application\Service\Dispute\DisputeManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Application\Service\Payment\CalculatedDataProvider;
use Application\Service\Payment\PaymentPolitics;

class DealManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DealManager|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager      =   $container->get('doctrine.entitymanager.orm_default');
        $paymentManager     =   $container->get(PaymentManager::class);
        $dealAgentManager   =   $container->get(DealAgentManager::class);
        $userManager        =   $container->get(\Application\Service\UserManager::class);
        $baseAuthManager    =   $container->get(\Core\Service\Base\BaseAuthManager::class);
        $paymentMethodManager = $container->get(\Application\Service\Payment\PaymentMethodManager::class);
        $civilLawSubjectManager = $container->get(\Application\Service\CivilLawSubject\CivilLawSubjectManager::class);
        $dealPolitics = $container->get(DealPolitics::class);
        $settingManager = $container->get(SettingManager::class);
        $fileManager = $container->get(FileManager::class);
        $creditPaymentOrderAvailabilityListener = $container->get(CreditPaymentOrderAvailabilityListener::class);
        $repaidOverpayManager = $container->get(RepaidOverpayManager::class);
        $deliveryManager = $container->get(DeliveryManager::class);
        $dealStatus = $container->get(DealStatus::class);
        $disputeManager = $container->get(DisputeManager::class);
        $paymentOrderManager = $container->get(PaymentOrderManager::class);
        $renderer = $container->get(RendererInterface::class);
        $tcpdf = $container->get(\TCPDF::class);
        $calculatedDataProvider = $container->get(CalculatedDataProvider::class);
        $timelineManager = $container->get(TimelineManager::class);
        $paymentPolitics = $container->get(PaymentPolitics::class);
        $dealTypeManager = $container->get(DealTypeManager::class);
        $dealTokenManager = $container->get(DealTokenManager::class);
        $deliveryOrderManager = $container->get(DeliveryOrderManager::class);
        $config = $container->get('Config');

        return new DealManager(
            $entityManager,
            $paymentManager,
            $dealAgentManager,
            $userManager,
            $baseAuthManager,
            $paymentMethodManager,
            $civilLawSubjectManager,
            $dealPolitics,
            $settingManager,
            $fileManager,
            $creditPaymentOrderAvailabilityListener,
            $repaidOverpayManager,
            $deliveryManager,
            $dealStatus,
            $disputeManager,
            $paymentOrderManager,
            $renderer,
            $tcpdf,
            $calculatedDataProvider,
            $timelineManager,
            $paymentPolitics,
            $dealTypeManager,
            $dealTokenManager,
            $deliveryOrderManager,
            $config
        );
    }
}