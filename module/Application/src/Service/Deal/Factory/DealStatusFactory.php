<?php

namespace Application\Service\Deal\Factory;

use Application\Service\Deal\Status\PendingRefundPaymentStatus;
use Application\Service\Deal\Status\TroubleStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Deal\DealStatus;
use Application\Service\Payment\PaymentManager;
use Application\Service\Deal\Status\ConfirmedStatus;
use Application\Service\Deal\Status\DebitMatchedStatus;
use Application\Service\Deal\Status\DeliveredStatus;
use Application\Service\Deal\Status\PendingCreditPaymentStatus;
use Application\Service\Deal\Status\ClosedStatus;
use Application\Service\Deal\Status\DisputeStatus;
use Application\Service\Deal\Status\RefundStatus;
use Application\Service\Deal\Status\TribunalStatus;
use Application\Service\Deal\Status\WaitingApprovalStatus;

/**
 * Class DealStatusFactory
 * @package Application\Service\Deal\Factory
 */
class DealStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $confirmedStatus = $container->get(ConfirmedStatus::class);
        $paymentManager = $container->get(PaymentManager::class);
        $debitMatchedStatus = $container->get(DebitMatchedStatus::class);
        $deliveredStatus = $container->get(DeliveredStatus::class);
        $pendingCreditPaymentStatus = $container->get(PendingCreditPaymentStatus::class);
        $closedStatus = $container->get(ClosedStatus::class);
        $disputeStatus = $container->get(DisputeStatus::class);
        $pendingRefundPaymentStatus = $container->get(PendingRefundPaymentStatus::class);
        $refundStatus = $container->get(RefundStatus::class);
        $tribunalStatus = $container->get(TribunalStatus::class);
        $waitingApprovalStatus = $container->get(WaitingApprovalStatus::class);
        $troubleStatus = $container->get(TroubleStatus::class);

        return new DealStatus(
            $paymentManager,
            $confirmedStatus,
            $debitMatchedStatus,
            $deliveredStatus,
            $pendingCreditPaymentStatus,
            $closedStatus,
            $disputeStatus,
            $pendingRefundPaymentStatus,
            $refundStatus,
            $tribunalStatus,
            $waitingApprovalStatus,
            $troubleStatus
        );
    }
}