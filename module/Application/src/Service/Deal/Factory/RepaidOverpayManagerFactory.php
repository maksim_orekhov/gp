<?php
namespace Application\Service\Deal\Factory;

use Application\Service\Deal\RepaidOverpayManager;
use Application\Service\Deal\Status\RefundStatus;
use Application\Service\Deal\Status\TribunalStatus;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Payment\PaymentMethodManager;

/**
 * Class RepaidOverpayManagerFactory
 * @package Application\Service\Deal\Factory
 */
class RepaidOverpayManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RepaidOverpayManager
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $paymentOrderManager = $container->get(PaymentOrderManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $payOffManager = $container->get(PayOffManager::class);
        $tribunalStatus = $container->get(TribunalStatus::class);
        $refundStatus = $container->get(RefundStatus::class);


        return new RepaidOverpayManager(
            $entityManager,
            $paymentOrderManager,
            $paymentMethodManager,
            $payOffManager,
            $tribunalStatus,
            $refundStatus
        );
    }
}