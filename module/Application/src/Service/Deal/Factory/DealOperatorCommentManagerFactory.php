<?php

namespace Application\Service\Deal\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Deal\DealOperatorCommentManager;
use ModuleMessage\Service\MessageManager;

/**
 * Class DealOperatorCommentManagerFactory
 * @package Application\Service\Deal\Factory
 */
class DealOperatorCommentManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager  = $container->get('doctrine.entitymanager.orm_default');
        $messageManager = $container->get(MessageManager::class);

        return new DealOperatorCommentManager(
            $entityManager,
            $messageManager
        );
    }
}