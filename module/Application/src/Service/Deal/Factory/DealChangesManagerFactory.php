<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09.10.2017
 * Time: 23:01
 */

namespace Application\Service\Deal\Factory;

use ModuleDelivery\Service\DeliveryOrderManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Deal\DealChangesManager;
use Application\Service\Deal\DealChangePolitics;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;

/**
 * Class DealChangesManager
 * @package Application\Service\Deal\Factory
 */
class DealChangesManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        $entityManager          = $container->get('doctrine.entitymanager.orm_default');
        $dealChangePolitics     = $container->get(DealChangePolitics::class);
        $civilLawSubjectManager = $container->get(CivilLawSubjectManager::class);
        $deliveryOrderManager = $container->get(DeliveryOrderManager::class);

        return new DealChangesManager(
            $entityManager,
            $dealChangePolitics,
            $civilLawSubjectManager,
            $deliveryOrderManager
        );
    }
}