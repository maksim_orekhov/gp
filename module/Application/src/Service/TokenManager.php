<?php
namespace Application\Service;

use Core\Adapter\TokenAdapter;
use Core\Exception\LogicException;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseTokenManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\SessionContainerManager;
use Doctrine\ORM\EntityManager;

class TokenManager extends BaseTokenManager
{
    public function __construct(EntityManager $entityManager,
                                TokenAdapter $tokenAdapter,
                                BaseEmailManager $baseEmailManager,
                                BaseUserManager $baseUserManager,
                                SessionContainerManager $sessionContainerManager,
                                array $config)
    {
        parent::__construct($entityManager, $tokenAdapter, $baseEmailManager, $baseUserManager, $sessionContainerManager, $config);

        $this->entityManager = $entityManager;
        $this->tokenAdapter = $tokenAdapter;
        $this->baseEmailManager = $baseEmailManager;
        $this->baseUserManager = $baseUserManager;
        $this->sessionContainerManager = $sessionContainerManager;
        $this->config = $config;
    }

    /**
     * @param $email_value
     * @param $deal_id
     * @param $deal_agent_id
     * @return string
     * @throws \Exception
     */
    public function createInvitationUserToken($email_value, $deal_id, $deal_agent_id)
    {
        try {
            $encodedToken = $this->tokenAdapter->createToken([
                    'email'         => $email_value,
                    'dealId'        => $deal_id,
                    'dealAgentId'   => $deal_agent_id
                ]);
        }
        catch(\Throwable $t) {

            throw new LogicException(null, LogicException::TOKEN_GENERATING_FAILED);
        }

        return $encodedToken;
    }

    /**
     * @param $invitation_token
     * @return object
     * @throws LogicException
     */
    public function checkedAndDecodeInvitationUserToken($invitation_token)
    {
        $required_properties = ['email','dealAgentId','dealId'];

        if ( !\is_string($invitation_token) ) {

            throw new LogicException(null, LogicException::USER_INVITATION_TOKEN_NOT_VALID);
        }

        try {
            $decodeToken = $this->tokenAdapter->decodeToken($invitation_token);
        }
        catch (\Throwable $t) {

            throw new LogicException(null, LogicException::USER_INVITATION_TOKEN_NOT_VALID);
        }

        foreach ($required_properties as $property){
            if(!property_exists($decodeToken, $property)){

                throw new LogicException(null, LogicException::USER_INVITATION_TOKEN_NOT_VALID);
            }
        }

        return $decodeToken;
    }
}