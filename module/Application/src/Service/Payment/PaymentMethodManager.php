<?php

namespace Application\Service\Payment;

use Application\Entity\DealAgent;
use Core\Exception\LogicException;
use Doctrine\Common\Collections\ArrayCollection;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\Deal;
use Application\Entity\LegalEntity;
use Application\Entity\NaturalPerson;
use Application\Entity\Payment;
use Application\Service\HydratorManager;
use Application\Entity\User;
use Doctrine\ORM\EntityManager;
use Application\Entity\PaymentMethod;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Hydrator\PaymentMethodHydrator;

class PaymentMethodManager extends HydratorManager
{
    const BANK_DETAILS_CUSTOMER_NAME = 'Счет плательшика по сделке ';
    const PATTERN_BANK_TRANSFER_NAME = 'Реквизиты %bank_name% по сделке %deal_number%';

    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var PaymentMethodPolitics
     */
    protected $paymentMethodPolitics;

    /**
     * @var PaymentMethodHydrator
     */
    private $paymentMethodHydrator;


    /**
     * PaymentMethodManager constructor.
     * @param $entityManager
     */
    public function __construct(
        EntityManager $entityManager,
        PaymentMethodPolitics $paymentMethodPolitics,
        PaymentMethodHydrator $paymentMethodHydrator
    )
    {
        $this->entityManager = $entityManager;
        $this->paymentMethodPolitics = $paymentMethodPolitics;
        $this->paymentMethodHydrator = $paymentMethodHydrator;
    }

    /**
     * @param $id
     * @return object
     */
    public function get($id)
    {
        return $this->entityManager->getRepository(PaymentMethod::class)->find((int) $id);
    }

    /**
     * @return array
     */
    public function findTypes()
    {
        return $this->entityManager->getRepository(PaymentMethodType::class)->findAll();
    }

    /**
     * @param $id
     * @return object
     */
    public function findType($id)
    {
        return $this->entityManager->getRepository(PaymentMethodType::class)->find($id);
    }

    /**
     * @param $paymentMethod
     * @return array|null
     */
    public function extractPaymentMethod($paymentMethod)
    {
        if (!$paymentMethod || !($paymentMethod instanceof PaymentMethod)) {

            return null;
        }

        return $this->paymentMethodHydrator->extract($paymentMethod);
    }

    /**
     * @param $idPaymentMethod
     * @return PaymentMethod|null
     * @throws \Exception
     */
    public function getPaymentMethodById($idPaymentMethod)
    {
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->get($idPaymentMethod);

        return $paymentMethod;
    }

    /**
     * @param string $payment_method_type
     * @return null|object
     */
    public function getPaymentMethodTypeByName(string $payment_method_type)
    {
        $paymentMethod = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(['name' => $payment_method_type]);

        // Допольнительная проверка, имеет ли пользователь использовать этот метод оплаты
        // Если нет, то return null

        return $paymentMethod;
    }

    /**
     * @param Deal $deal
     * @param PaymentMethod|null $paymentMethod
     * @return bool
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function processingPaymentMethodNameForDeal(Deal $deal, PaymentMethod $paymentMethod = null)
    {
        if ($paymentMethod) {
            switch ($paymentMethod->getPaymentMethodType()->getName()) {
                case PaymentMethodType::BANK_TRANSFER:
                    $paymentMethodBankTransfer = $paymentMethod->getPaymentMethodBankTransfer();
                    if ($paymentMethodBankTransfer && !$paymentMethodBankTransfer->getName()) {
                        $paymentMethodBankTransfer->setName(
                            self::buildBankTransferNameToDeal(
                                $paymentMethodBankTransfer->getBankName(),
                                $deal->getNumber()
                            )
                        );
                        $this->entityManager->persist($paymentMethodBankTransfer);
                        $this->entityManager->flush($paymentMethodBankTransfer);
                    }
                    break;
                case PaymentMethodType::ACQUIRING_MANDARIN:
                    //code...
                    break;
                case PaymentMethodType::EMONEY:
                    //code...
                    break;
            }
        }

        return true;
    }

    /**
     * Get all paymentMethods (в метод репозитория можно добавлять сортировки и поиск по параметрам)
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @return mixed
     */
    public function getPaymentMethodsWithRelations(User $user, array $params, bool $isOperator)
    {
        // @TODO Использовать $params для передачи параметров выборки в метод репозитория

        $paymentMethods = $this->entityManager->getRepository(PaymentMethod::class)
            ->getPatternPaymentMethods($isOperator ? null : $user);

        return $paymentMethods;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getPaymentMethodWithRelations(int $id)
    {
        $paymentMethod = $this->entityManager->getRepository(PaymentMethod::class)
            ->getPatternPaymentMethod($id);

        return $paymentMethod;
    }

    /**
     * Доступные пользователю методы оплаты
     *
     * @param User $user
     * @return array
     */
    public function getAvailablePaymentMethodTypes(User $user)
    {
        $paymentMethods = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findBy(['isActive' => true]);

        // Тут фильтрация методов оплаты для конктретного пользователя $user

        return (!empty($paymentMethods)) ? $paymentMethods : null;
    }

    /**
     * Добавления метода оплаты к CivilLawSubject
     * @param CivilLawSubject $civilLawSubject
     * @param PaymentMethodType $paymentMethodType
     * @param bool $is_pattern
     *
     * @return PaymentMethod
     * @throws \Exception
     */
    public function create(CivilLawSubject $civilLawSubject, PaymentMethodType $paymentMethodType, $is_pattern = true)
    {
        $paymentMethod = new PaymentMethod();
        $paymentMethod->setCivilLawSubject($civilLawSubject);
        $paymentMethod->setPaymentMethodType($paymentMethodType);
        $paymentMethod->setIsPattern($is_pattern);

        $civilLawSubject->addPaymentMethod($paymentMethod);

        $this->entityManager->persist($paymentMethod);
        $this->entityManager->flush();

        if($paymentMethod->getId()) {
            return $paymentMethod;
        }

        throw new \Exception('Can not add new payment method');
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @param Payment $payment
     * @return PaymentMethodBankTransfer
     * @throws \Exception
     */
    public function savingBankDetailsCustomerFromPaymentOrder(BankClientPaymentOrder $bankClientPaymentOrder, Payment $payment)
    {
        $deal = $payment->getDeal();
        // платит всегда customer поэтому нужен его civilLawSubject
        $civilLawSubject = $deal->getCustomer()->getCivilLawSubject();

        /**
         * добавляем PaymentMethod не pattern!!!
         * и добавляем Bank Transfer так как выплата будет происходить по реквизитам
         */
        $params = [
            'name' => self::BANK_DETAILS_CUSTOMER_NAME.$deal->getNumber(), //название
            'bank_name' => $bankClientPaymentOrder->getPayerBank1(), // название банка плательшика
            'account_number' => $bankClientPaymentOrder->getPayerAccount(), // Счет плательшика
            'bik' => $bankClientPaymentOrder->getPayerBik(), // bik плательшика
            'corr_account_number' => $bankClientPaymentOrder->getPayerCorrAccount(), // Счет плательшика
        ];
        $bankTransfer = $this->createPaymentMethodBankTransfer($params, $civilLawSubject, null, false);

        return $bankTransfer;
    }

    /**
     * @param $formData
     * @param CivilLawSubject $civilLawSubject
     * @return PaymentMethodBankTransfer
     * @throws \Exception
     */
    public function createBankDetailsCustomerForReturnPaymentOrder($formData, CivilLawSubject $civilLawSubject)
    {
        $paymentMethod = $civilLawSubject->getPaymentMethods()->first();
        if($paymentMethod){
            throw new \Exception('The payment method of the deal already exists');
        }
        $bankTransfer = $this->createPaymentMethodBankTransfer($formData, $civilLawSubject, null, false);

        return $bankTransfer;
    }

    /**
     * Получение название функции обработчика
     *
     * @param $paymentMethodType
     * @return bool|mixed
     * @throws \Exception
     */
    public function getCallbackByType(PaymentMethodType $paymentMethodType)
    {
        if ($paymentMethodType === null) {

            throw new \Exception('Not found paymentMethodType');
        }

        $callbacks = [
            PaymentMethodType::BANK_TRANSFER => 'createPaymentMethodBankTransfer',
        ];

        $result = $callbacks[$paymentMethodType->getName()] ?? false;

        if ($result === false) {
            throw new \Exception('Not found callback function for save payment method');
        }

        return $result;
    }

    /**
     * Создает субьекту права новый метод оплаты типа "банковский перевод"
     * @param array $payment_method_data
     * @param CivilLawSubject $civilLawSubject
     * @param PaymentMethod $paymentMethod
     * @param bool $isPattern
     * @return PaymentMethodBankTransfer
     * @throws \Exception
     */
    public function createPaymentMethodBankTransfer(
        $payment_method_data,
        CivilLawSubject $civilLawSubject = null,
        PaymentMethod $paymentMethod = null,
        $isPattern = true)
    {
        $missKeys = $this->checkKeyRequired(PaymentMethodType::FIELD_BANK_TRANSFER, $payment_method_data);
        if (count($missKeys)) {
            throw new \Exception('Missing required keys: '.implode(", ", $missKeys));
        }
        $paymentMethodType = $this->entityManager
            ->getRepository(PaymentMethodType::class)->findOneByName(PaymentMethodType::BANK_TRANSFER);

        if ($paymentMethodType === null) {
            throw new \Exception('Not found paymentMethodType');
        }

        if ($paymentMethod === null) {
            $paymentMethod = $this->create($civilLawSubject, $paymentMethodType, $isPattern);
        }
        $payment_method_data['name'] = $payment_method_data['name'] ?? '';
        //устанавливаем расширяющую сущность
        $paymentMethodBankTransfer = new PaymentMethodBankTransfer();
        $paymentMethodBankTransfer->setName($payment_method_data['name']);
        $paymentMethodBankTransfer->setBankName($payment_method_data['bank_name']);
        $paymentMethodBankTransfer->setAccountNumber($payment_method_data['account_number']);
        $paymentMethodBankTransfer->setCorrAccountNumber($payment_method_data['corr_account_number']);
        $paymentMethodBankTransfer->setBik($payment_method_data['bik']);

        //связываем расширение с методом оплаты субьекта права
        $paymentMethodBankTransfer->setPaymentMethod($paymentMethod);
        $paymentMethod->setPaymentMethodBankTransfer($paymentMethodBankTransfer);

        $this->entityManager->persist($paymentMethodBankTransfer);
        $this->entityManager->flush();

        if($paymentMethodBankTransfer->getId()) {
            return $paymentMethodBankTransfer;
        }
        throw new \Exception('Can not create new payment bank transfer method');
    }

    /**
     * Клонирование метода оплаты
     * @param PaymentMethod $paymentMethod
     * @param CivilLawSubject $civilLawSubject
     * @return PaymentMethod
     * @throws \Exception
     */
    public function clone(PaymentMethod $paymentMethod, CivilLawSubject $civilLawSubject = null)
    {
        $civilLawSubject = $civilLawSubject ?? $paymentMethod->getCivilLawSubject();
        $paymentMethodType = $paymentMethod->getPaymentMethodType();

        $newPaymentMethod = $this->create($civilLawSubject, $paymentMethodType, $is_pattern = false);

        if ($paymentMethod->getPaymentMethodBankTransfer() !== null) {
            $paymentMethodBankTransfer = $paymentMethod->getPaymentMethodBankTransfer();
            $params = [
                'name' => $paymentMethodBankTransfer->getName(),
                'bank_name' => $paymentMethodBankTransfer->getBankName(),
                'bik' => $paymentMethodBankTransfer->getBik(),
                'account_number' => $paymentMethodBankTransfer->getAccountNumber(),
                'corr_account_number' => $paymentMethodBankTransfer->getCorrAccountNumber(),
            ];
            $this->createPaymentMethodBankTransfer($params, null, $newPaymentMethod);
        }

        return $newPaymentMethod;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @param PaymentMethodType $paymentMethodType
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    public function isPaymentMethodUnique(CivilLawSubject $civilLawSubject, PaymentMethodType $paymentMethodType, array $params)
    {
        $paymentMethods = $this->entityManager->getRepository(PaymentMethod::class)->findBy([
            'civilLawSubject'   => $civilLawSubject,
            'paymentMethodType' => $paymentMethodType,
        ]);

        try {
            foreach ($paymentMethods as $paymentMethod) {
                // Check if data in $params do not match any existing paymentMethod's properties
                $this->paymentMethodPolitics->isPaymentMethodDataUnique($paymentMethod, $params);
            }
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return true;
    }

    /**
     * Get CivilLawSubject collection by User and isPattern
     *
     * @param User $user
     * @return array
     */
    public function getAvailablePaymentMethods(User $user)
    {
        $paymentMethods = [];

        /** @var CivilLawSubject $civilLawSubject */
        foreach ($user->getCivilLawSubjects() as $civilLawSubject) {
            /** @var PaymentMethod $paymentMethod */
            foreach ($civilLawSubject->getPaymentMethods() as $paymentMethod) {
                // Чтобы методы не повторялись, проверяем на наличие ключа в итоговом массиве
                if (!array_key_exists($paymentMethod->getId(), $paymentMethods) && $paymentMethod->getIsPattern()) {
                    if ($paymentMethod->getPaymentMethodType()->getName() === PaymentMethodType::BANK_TRANSFER) {
                        $paymentMethods[$paymentMethod->getId()] = $paymentMethod->getPaymentMethodBankTransfer();
                    } elseif ($paymentMethod->getPaymentMethodType()->getName() === PaymentMethodType::ACQUIRING_MANDARIN) {
                        $paymentMethods[$paymentMethod->getId()] = null;
                    } elseif ($paymentMethod->getPaymentMethodType()->getName() === PaymentMethodType::EMONEY) {
                        $paymentMethods[$paymentMethod->getId()] = null;
                    }
                }
            }
        }

        return !empty($paymentMethods) ? $paymentMethods : null;
    }

    /**
     * @param $civilSubject
     * @return array
     * @throws \Exception
     */
    public function getPaymentMethodsByCivilSubject($civilSubject)
    {
        if(!$civilSubject instanceof NaturalPerson && !$civilSubject instanceof LegalEntity){
            throw new \Exception('civil subject is not a Legal Entity or Natural Person');
        }

        $payment_methods_array = [];
        $paymentMethods = $civilSubject->getCivilLawSubject()->getPaymentMethods();

        foreach ($paymentMethods as $paymentMethod){
            //TODO: возвращаю только pattern может это и не надо

            if( $paymentMethod->getIsPattern() ) {
                $payment_methods_array[$paymentMethod->getPaymentMethodType()->getOutputName()][] = $paymentMethod->getPaymentMethodBankTransfer();

            }
        }

        return $payment_methods_array;
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @param $params
     * @return PaymentMethod|null
     * @throws \Exception
     */
    public function update(PaymentMethod $paymentMethod, $params)
    {
        $updatedPaymentMethod = null;

        if ($paymentMethod->getPaymentMethodType()->getName() === PaymentMethodType::BANK_TRANSFER) {
            /** @var PaymentMethodBankTransfer $associatedPaymentMethod */
            $paymentMethodBankTransfer = $this->updatePaymentMethodBankTransfer(
                $paymentMethod->getPaymentMethodBankTransfer(), $params
            );
            $updatedPaymentMethod = $paymentMethodBankTransfer->getPaymentMethod();
        }

        // ...Другие методы оплаты

        if (!$updatedPaymentMethod) {

            throw new \Exception('No defined associated payment method ');
        }

        return $updatedPaymentMethod;
    }

    /**
     * @param PaymentMethodBankTransfer $bankTransfer
     * @param array $params
     * @return PaymentMethodBankTransfer
     * @throws \Exception
     */
    private function updatePaymentMethodBankTransfer(PaymentMethodBankTransfer $bankTransfer, $params)
    {
        $missKeys = $this->checkKeyRequired([
            'name',
            'bank_name',
            'bik',
            'account_number',
            'corr_account_number',
        ], $params);
        if (count($missKeys)) {
            throw new \Exception('Missing required keys: '.implode(", ", $missKeys));
        }

        $person = $this->hydrate($params, $bankTransfer);
        $this->entityManager->persist($person);
        $this->entityManager->flush();

        return $bankTransfer;
    }

    private function updatePaymentMethodCreditCard(PaymentMethodCreditCard $creditCard, $params)
    {

    }

    private function updatePaymentMethodEmoney(PaymentMethodEmoney $emoney, $params)
    {

    }

    /**
     * Возвращает название метода оплаты, связанного с "проски сущностью" $paymentMethod
     *
     * @param PaymentMethod $paymentMethod
     * @return null|string
     */
    public function getPaymentName(PaymentMethod $paymentMethod)
    {
        switch ($paymentMethod->getPaymentMethodType()->getName()) {
            case PaymentMethodType::BANK_TRANSFER:
                return $paymentMethod->getPaymentMethodBankTransfer()->getName();
            case PaymentMethodType::ACQUIRING_MANDARIN:
                #return $paymentMethod->getPaymentMethodCreditCard()->getName();
            case PaymentMethodType::EMONEY:
                #$return paymentMethod->getPaymentMethodEmoney()->getName();
            default:
                return null;
        }
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(PaymentMethod $paymentMethod)
    {
        if ($paymentMethod->getPaymentMethodBankTransfer() !== null) {

            $this->entityManager->remove($paymentMethod->getPaymentMethodBankTransfer());
        }

        // @TODO Другие типы

        $payments = $this->entityManager->getRepository(Payment::class)
            ->findBy(['paymentMethod' => $paymentMethod]);

        /** @var Payment $payment */
        foreach ($payments as $payment) {

            $payment->setPaymentMethod(null);

            $this->entityManager->persist($payment);
        }

        $this->entityManager->remove($paymentMethod);
        $this->entityManager->flush();
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @return array
     */
    public function getPaymentMethodOutput($paymentMethod)
    {
        $paymentMethodTypeOutput = $this->getPaymentMethodTypeOutput($paymentMethod->getPaymentMethodType());
        $paymentMethodsOutput['type'] = $paymentMethodTypeOutput;
        $paymentMethodsOutput['id'] = $paymentMethod->getId();
        switch ($paymentMethodTypeOutput['name']){
            case 'bank_transfer':
                $paymentMethodsOutput[$paymentMethodTypeOutput['name']] = $this->getPaymentMethodBankTransferOutput($paymentMethod->getPaymentMethodBankTransfer());
                break;
            case 'acquiring_mandarin':
                $paymentMethodsOutput[$paymentMethodTypeOutput['name']] = null;
                break;
            case 'emoney':
                $paymentMethodsOutput[$paymentMethodTypeOutput['name']] = null;
                break;
        }

        return $paymentMethodsOutput;
    }

    /**
     * @param PaymentMethodBankTransfer $paymentMethodBankTransfer
     * @return array
     */
    public function getPaymentMethodBankTransferOutput(PaymentMethodBankTransfer $paymentMethodBankTransfer)
    {
        $paymentMethodBankTransferOutput = [];

        $paymentMethodBankTransferOutput['id'] = $paymentMethodBankTransfer->getId();
        $paymentMethodBankTransferOutput['name'] = $paymentMethodBankTransfer->getName();
        $paymentMethodBankTransferOutput['bik'] = $paymentMethodBankTransfer->getBik();
        $paymentMethodBankTransferOutput['account_number'] = $paymentMethodBankTransfer->getAccountNumber();
        $paymentMethodBankTransferOutput['corr_account_number'] = $paymentMethodBankTransfer->getCorrAccountNumber();
        $paymentMethodBankTransferOutput['bank_name'] = $paymentMethodBankTransfer->getBankName();

        return $paymentMethodBankTransferOutput;
    }

    /**
     * @param PaymentMethodType $paymentMethodType
     * @return array
     */
    public function getPaymentMethodTypeOutput(PaymentMethodType $paymentMethodType)
    {
        $paymentMethodTypeOutput = [];

        $paymentMethodTypeOutput['id']          = $paymentMethodType->getId();
        $paymentMethodTypeOutput['name']        = $paymentMethodType->getName();
        $paymentMethodTypeOutput['output_name'] = $paymentMethodType->getOutputName();
        $paymentMethodTypeOutput['description'] = $paymentMethodType->getDescription();
        $paymentMethodTypeOutput['is_active']   = $paymentMethodType->getIsActive();

        return $paymentMethodTypeOutput;
    }

    /**
     * Умное извлечение коллекции (отсекаются ненужные данные)
     *
     * @param $paymentMethods
     * @return array
     */
    public function extractPaymentMethodCollection($paymentMethods)
    {
        $paymentMethodsOutput = [];

        foreach ($paymentMethods as $paymentMethod) {

            $paymentMethodsOutput[] = $this->getPaymentMethodOutput($paymentMethod);
        }

        return $paymentMethodsOutput;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getPaymentMethodBankTransferById($id)
    {
        return $this->entityManager->getRepository(PaymentMethodBankTransfer::class)->find($id);
    }

    /**
     * @param Deal $deal
     * @return PaymentMethodBankTransfer|null
     * @throws \Exception
     */
    public function getCustomerPaymentMethodByDeal(Deal $deal)
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $deal->getCustomer()->getCivilLawSubject();
        if($civilLawSubject->getIsPattern()){
            throw new \Exception('CivilLawSubject of the paid deal can not be a pattern');
        }

        $paymentMethods = $civilLawSubject->getPaymentMethods();

        if(\count($paymentMethods) > 1){
            throw new \Exception('Customer payment method found more than one');
        }

        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $paymentMethods->first();

        return $paymentMethod ? $paymentMethod->getPaymentMethodBankTransfer() : null;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @param PaymentMethodType $paymentMethodType
     * @param array $params зависит от типа $payment_method_type_id
     *
     * @return PaymentMethod
     * @throws \Exception
     */
    public function addPaymentMethod(CivilLawSubject $civilLawSubject, PaymentMethodType $paymentMethodType, $params)
    {
        // @TODO Добавить условие для проверки (например, номер счета)
        try {
            // Can throw Exception
            $this->isPaymentMethodUnique($civilLawSubject, $paymentMethodType, $params);

            $callback = $this->getCallbackByType($paymentMethodType);
            /** @var PaymentMethodBankTransfer $object */
            $object = $this->$callback($params, $civilLawSubject);

            return $object->getPaymentMethod();

        } catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param int $id
     * @return null|object
     */
    public function getCivilLavSubjectForBindingPaymentMethod(int $id)
    {
        return $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['id' => $id, 'isPattern' => true]);
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function saveFormData($data)
    {
        if (isset($data['idPaymentMethod'])) {
            // UPDATE
            /** @var PaymentMethod $paymentMethod */
            $paymentMethod = $this->get($data['idPaymentMethod']);

            // Update PaymentMethod
            $paymentMethod = $this->update($paymentMethod, $data);

        } else {
            // CREATE
            /** @var CivilLawSubject $civilLawSubject */
            $civilLawSubject = $this->getCivilLavSubjectForBindingPaymentMethod((int) $data['civil_law_subject_id']);
            /** @var PaymentMethodType $paymentMethodType */
            $paymentMethodType = $this->getPaymentMethodTypeByName($data['payment_method_type']);

            /** @var PaymentMethod $paymentMethod */
            $paymentMethod = $this->addPaymentMethod(
                $civilLawSubject,
                $paymentMethodType,
                $data
            );
        }

        return [
            'paymentMethod' => $paymentMethod
        ];
    }

    /**
     * @param string $bank_name
     * @param string $deal_number
     * @return mixed|string
     */
    public static function buildBankTransferNameToDeal($bank_name = '', $deal_number = '')
    {
        $setting = [
            '%bank_name%' => $bank_name,
            '%deal_number%' => $deal_number
        ];

        $str = self::PATTERN_BANK_TRANSFER_NAME;

        foreach ($setting as $key => $value) {
            $str = str_ireplace($key, $value, $str);
        }

        return $str;
    }

    /**
     * @param Deal $deal
     * @return PaymentMethod|null
     * @throws LogicException
     */
    public function getCustomerPaymentMethod(Deal $deal)
    {
        /**
         * @var DealAgent $customer
         * @var CivilLawSubject $civilLawSubject
         * @var ArrayCollection $paymentMethods
         * @var PaymentMethod $paymentMethod
         * @var PaymentMethodType $paymentMethodType
         */
        $customer = $deal->getCustomer();
        $civilLawSubject = $customer->getCivilLawSubject();
        $paymentMethods = $civilLawSubject ? $civilLawSubject->getPaymentMethods() : null;
        $paymentMethod = $paymentMethods ? $paymentMethods->first() : null;

        if ($paymentMethod && $paymentMethod->getIsPattern() === true) {

            throw new LogicException(null, LogicException::PAYMENT_METHOD_FORBIDDEN_TO_USE_PATTERN);
        }

        return $paymentMethod;
    }
}