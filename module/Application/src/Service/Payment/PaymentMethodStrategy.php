<?php
namespace Application\Service\Payment;

use Application\Entity\PaymentMethod;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Application\Form\PaymentMethod\BankTransferForm;
use Application\Form\PaymentMethod\CashForm;
use Application\Form\PaymentMethod\CreditCardForm;
use Application\Form\PaymentMethod\EmoneyForm;

/**
 * Class PaymentMethodStrategy
 * @package Application\Service\Payment
 */
class PaymentMethodStrategy
{
    /**
     * Zend\Form\Form
     */
    private $form;

    /**
     * @TODO Создать общий интерфейс для всех PaymentMethod
     */
    private $associatedPaymentMethod=null;

    /**
     * PaymentMethodStrategy constructor.
     * @param PaymentMethod|null $paymentMethod
     * @param PaymentMethodType|null $paymentMethodType
     * @throws \Exception
     */
    public function __construct(PaymentMethod $paymentMethod = null, PaymentMethodType $paymentMethodType = null)
    {
        $paymentMethodType_name = $paymentMethodType ? $paymentMethodType->getName() : $paymentMethod->getPaymentMethodType()->getName();
        switch ($paymentMethodType_name) {
            case PaymentMethodType::BANK_TRANSFER:
                $this->form = new BankTransferForm();
                if ($paymentMethod) $this->associatedPaymentMethod = $paymentMethod->getPaymentMethodBankTransfer();
                break;
            case PaymentMethodType::ACQUIRING_MANDARIN:
                // @TODO Переименовать форму (acquiring_mandarin_card_binding)
                $this->form = new CreditCardForm();
                #if ($paymentMethod) $this->associatedPaymentMethod = $paymentMethod->getPaymentMethodCreditCard();
                break;
            case PaymentMethodType::EMONEY:
                $this->form = new EmoneyForm();
                #if ($paymentMethod) $this->associatedPaymentMethod = $paymentMethod->getPaymentMethodEmoney();
                break;
            default:
                throw new \Exception('No form for this PaymentMethod type found');
        }
    }

    /**
     * @return \Zend\Form\Form
     */
    public function getForm()
    {
        return $this->form;
    }

    public function getAssociatedPaymentMethod()
    {
        return $this->associatedPaymentMethod;
    }

    /**
     * @return bool
     */
    public function fillForm()
    {
        // Получаем имя класса без namespace
        $fillMethodName = 'fill'.basename(get_class($this->form));

        $this->$fillMethodName();

        return true;
    }

    /**
     * @return bool
     */
    private function fillBankTransferForm()
    {
        $this->form->setData([
            'name' => $this->associatedPaymentMethod->getName(),
            'bank_name' => $this->associatedPaymentMethod->getBankName(),
            'bik' => $this->associatedPaymentMethod->getBik(),
            'account_number' => $this->associatedPaymentMethod->getAccountNumber(),
            'corr_account_number' => $this->associatedPaymentMethod->getCorrAccountNumber(),
        ]);

        return true;
    }

    private function fillCashForm()
    {
        $this->form->setData([
            'name' => $this->associatedPaymentMethod->getName(),
        ]);
    }

    private function fillCreditCardForm()
    {
        $this->form->setData([
            'name' => $this->associatedPaymentMethod->getName(),
        ]);
    }

    private function fillEmoneyForm()
    {
        $this->form->setData([
            'name' => $this->associatedPaymentMethod->getName(),
        ]);
    }
}