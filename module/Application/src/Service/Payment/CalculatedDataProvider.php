<?php

namespace Application\Service\Payment;

use Application\Entity\FeePayerOption;
use Application\Service\SettingManager;
use Application\Entity\Payment;

/**
 * Class CalculatedDataProvider
 * @package Application\Service\Payment
 */
class CalculatedDataProvider
{
    const NAME_POLITICS_IN_SETTING = 'fee_percentage';
    const NAME_CUSTOMER = 'CUSTOMER';
    const NAME_CONTRACTOR = 'CONTRACTOR';
    const NAME_50X50 = '50/50';

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * PaymentPolitics constructor.
     * @param SettingManager $settingManager
     */
    public function __construct(SettingManager $settingManager)
    {
        $this->settingManager = $settingManager;
    }

    /**
     * @return string|null
     * @throws \Exception
     */
    public function getFeePercentage()
    {
        $feePercentage = $this->settingManager->getValueByNameGlobalSetting(self::NAME_POLITICS_IN_SETTING);

        if ($feePercentage === null){

            throw new \Exception('Politic fee percentage not found');
        }

        return $feePercentage;
    }

    /**
     * @param $amount
     * @return float|int
     * @throws \Exception
     */
    public function getFeeAmount($amount)
    {
        $feePercentage = $this->getFeePercentage();

        return $this->getCalculateFeeAmount($amount, $feePercentage);
    }

    /**
     * @param $amount
     * @param $feePercentage
     * @return float|int
     */
    public function getCalculateFeeAmount($amount, $feePercentage)
    {
        $feeAmount = $amount * ($feePercentage / 100);

        return self::roundAmount($feeAmount);
    }

    /**
     * @param $amount
     * @param FeePayerOption $feePayerOption
     * @return float|int
     * @throws \Exception
     */
    public function getCalculatedExpectedValue($amount, FeePayerOption $feePayerOption)
    {
        $feeAmount = $this->getFeeAmount($amount);
        $feePayerOptionName = $feePayerOption->getName();
        $is50X50Pays = $feePayerOptionName === self::NAME_50X50;
        $isCustomerPays = $feePayerOptionName === self::NAME_CUSTOMER;

        // если 50/50 делим комиссию попалам
        if ($is50X50Pays) {
            $feeAmount /= 2;
        }
        // если CUSTOMER или 50/50 то прибавляем комиссию
        $expectedValue = ($isCustomerPays || $is50X50Pays) ? $amount + $feeAmount: $amount;

        return self::roundAmount($expectedValue);
    }

    /**
     * @param $amount
     * @param FeePayerOption $feePayerOption
     * @return float|int
     * @throws \Exception
     */
    public function getCalculatedReleasedValue($amount, FeePayerOption $feePayerOption)
    {
        $feeAmount = $this->getFeeAmount($amount);
        $feePayerOptionName = $feePayerOption->getName();
        $is50X50Pays = $feePayerOptionName === self::NAME_50X50;
        $isContractorPays = $feePayerOptionName === self::NAME_CONTRACTOR;

        // если 50/50 делим комиссию попалам
        if ($is50X50Pays) {
            $feeAmount /= 2;
        }
        // если CONTRACTOR или 50/50 то вычитаем комиссию
        $releasedValue = ($isContractorPays || $is50X50Pays) ? $amount - $feeAmount: $amount;

        return self::roundAmount($releasedValue);
    }

    /**
     * @param Payment $payment
     * @param $total_incoming_amount
     * @return float|int
     */
    public function getCalculatedRefundAmount(Payment $payment, $total_incoming_amount)
    {
        $feeAmount = $payment->getDealValue() * ($payment->getFee() / 100);

        $refundAmount = $total_incoming_amount - $feeAmount;

        return self::roundAmount($refundAmount);
    }

    /**
     * максимальная скидка
     * 1 не может быть больше чем покупатель заплатил
     * 2 не может быть больше чем продовец должен получить
     *
     * @param Payment $payment
     * @param $total_incoming_amount
     * @return float|int
     */
    public function getMaxDiscountAmount(Payment $payment, $total_incoming_amount)
    {
        $maxDiscount = ($total_incoming_amount < $payment->getReleasedValue())
            ? $total_incoming_amount
            : $payment->getReleasedValue();

        return self::roundAmount($maxDiscount);
    }

    /**
     * Округляет $amount в большую сторону от нуля до 2х знаков
     *
     * @param $amount
     * @return float|int
     */
    public static function roundAmount($amount)
    {
        return round($amount, 2, PHP_ROUND_HALF_UP);
    }
}