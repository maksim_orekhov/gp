<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09.10.2017
 * Time: 20:15
 */

namespace Application\Service\Payment\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\Payment\PaymentMethodPolitics;
use Application\Hydrator\PaymentMethodHydrator;

class PaymentMethodManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PaymentMethodManager
     */
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $paymentMethodPolitics = $container->get(PaymentMethodPolitics::class);
        $paymentMethodHydrator = $container->get(PaymentMethodHydrator::class);

        return new PaymentMethodManager(
            $entityManager,
            $paymentMethodPolitics,
            $paymentMethodHydrator
        );
    }
}