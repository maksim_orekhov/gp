<?php

namespace Application\Service\Payment\Factory;

use Application\EventManager\DealEventProvider;
use Application\Provider\Mail\PaymentReceiptSender;
use Application\Service\Payment\CalculatedDataProvider;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use Application\Service\SettingManager;
use Core\Service\TwigRenderer;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Payment\PaymentManager;
use Application\Service\Payment\PaymentMethodManager;
use ModulePaymentOrder\Service\PaymentMethodTypeManager;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class PaymentManagerFactory
 * @package Application\Service\Payment\Factory
 */
class PaymentManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $paymentPolitics = $container->get(\Application\Service\Payment\PaymentPolitics::class);
        $config = $container->get('Config');
        $twigRenderer = $container->get(TwigRenderer::class);
        $paymentReceiptSender = new PaymentReceiptSender($twigRenderer, $config);
        $settingManager = $container->get(SettingManager::class);
        $bankClientPaymentOrderManager = $container->get(BankClientPaymentOrderManager::class);
        $paymentMethodTypeManager = $container->get(PaymentMethodTypeManager::class);
        $paymentOrderManager = $container->get(PaymentOrderManager::class);
        $calculatedDataProvider = $container->get(CalculatedDataProvider::class);
        $dealEventProvider = $container->get(DealEventProvider::class);
        $deliveryEventProvider = $container->get(DeliveryEventProvider::class);

        return new PaymentManager(
            $entityManager,
            $paymentMethodManager,
            $paymentPolitics,
            $settingManager,
            $paymentReceiptSender,
            $bankClientPaymentOrderManager,
            $paymentMethodTypeManager,
            $paymentOrderManager,
            $calculatedDataProvider,
            $dealEventProvider,
            $deliveryEventProvider->getEventManager(),
            $config
        );
    }
}