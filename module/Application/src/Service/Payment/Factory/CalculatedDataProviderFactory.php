<?php
namespace Application\Service\Payment\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Payment\CalculatedDataProvider;
use Application\Service\SettingManager;

/**
 * Class CalculatedDataProviderFactory
 * @package Application\Service\Payment\Factory
 */
class CalculatedDataProviderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        $settingManager = $container->get(SettingManager::class);

        return new CalculatedDataProvider($settingManager);
    }
}