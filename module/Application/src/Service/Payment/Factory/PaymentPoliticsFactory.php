<?php
namespace Application\Service\Payment\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Payment\PaymentPolitics;
use Application\Service\SettingManager;

/**
 * Class PaymentPoliticsFactory
 * @package Application\Service\Payment\Factory
 */
class PaymentPoliticsFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        $settingManager = $container->get(SettingManager::class);

        return new PaymentPolitics($settingManager);
    }
}