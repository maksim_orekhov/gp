<?php

namespace Application\Service\Payment;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\RepaidOverpay;
use Application\Service\SettingManager;
use Application\Entity\Payment;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\Entity\DeliveryServiceType;
use ModuleDeliveryDpd\Entity\DpdDeliveryOrder;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Entity\PaymentTransactionInterface;
use ModulePaymentOrder\Service\BankClientPaymentOrderHandler;

/**
 * Class PaymentPolitics
 * @package Application\Service\Payment
 */
class PaymentPolitics
{
    const NAME_POLITICS_IN_SETTING = 'fee_percentage';
    const NAME_CUSTOMER = 'CUSTOMER';
    const NAME_CONTRACTOR = 'CONTRACTOR';
    const NAME_50X50 = '50/50';

    const AMOUNT_NOT_MATCHED    = 'amount_not_matched';
    const AMOUNT_IS_OVERPAID    = 'amount_is_overpaid';
    const DATE_DEBIT_IS_EXPIRED = 'date_debit_is_expired';
    const DATE_DEBIT_IS_NULL    = 'date_is_expired';
    const CONTRACT_FILE_MISSING = 'contract_file_missing';
    const PAYMENT_ORDER_FILE_MISSING = 'payment_order_file_missing';
    const ACCEPTED_DELIVERY_ORDER_SERVICE_ORDER_NUMBER_MISSING = 'accepted delivery order has no service order number';

    /**
     * @var array
     */
    private $paymentAbnormalTypes = [
        self::AMOUNT_NOT_MATCHED => 'Дата истекла, оплата полностью не поступила',
        self::DATE_DEBIT_IS_EXPIRED => 'Просрочена дата платежа',
        self::AMOUNT_IS_OVERPAID => 'Переплата',
        self::CONTRACT_FILE_MISSING => 'Отсутствует файл контракта',
        self::PAYMENT_ORDER_FILE_MISSING => 'Отсутствует ссылка на файл платежек',
        self::ACCEPTED_DELIVERY_ORDER_SERVICE_ORDER_NUMBER_MISSING => 'У принятого заказа на доставку отсутствует номер в системе сервиса доставки (трек-номер)',
        #self::DATE_DEBIT_IS_NULL => 'Отсутствует дата платежа',
    ];

    /**
     * @var \Application\Service\SettingManager
     */
    private $settingManager;

    /**
     * PaymentPolitics constructor.
     * @param SettingManager $settingManager
     */
    public function __construct(SettingManager $settingManager)
    {
        $this->settingManager = $settingManager;
    }

    /**
     * @param $deal_agent_role
     * @return bool
     */
    public function isDealAgentRoleAllowedToCreatePaymentMethodOnDealCreation($deal_agent_role)
    {
      return  $deal_agent_role !== 'customer';
    }

    /**
     * @return array
     */
    public function getPaymentAbnormalTypes()
    {
        return $this->paymentAbnormalTypes;
    }

    /**
     * @param Payment $payment
     * @param $allIncomingPaymentOrders
     * @return array
     *
     * @TODO Пернести в DealPolitics или DealManager
     */
    public function getDealTroubleTypes(Payment $payment, $allIncomingPaymentOrders): array
    {
        $paymentTroubleTypesByPayment = [];

        /** @var Deal $deal */
        $deal = $payment->getDeal();
        //если нет файла контракта
        if (null !== $deal->getDateOfConfirm() && null === $deal->getDealContractFile()) {

            $paymentTroubleTypesByPayment[self::CONTRACT_FILE_MISSING] = $this->paymentAbnormalTypes[self::CONTRACT_FILE_MISSING];
        }

        if (null === $allIncomingPaymentOrders) {

            return $paymentTroubleTypesByPayment;
        }

        $total_amount = 0;
        $dateOfLastIncoming = null;
        /** @var RepaidOverpay $repaidOverpay */
        $repaidOverpay = $payment->getRepaidOverpay();

        /** @var PaymentTransactionInterface $incomingPaymentOrder */
        foreach($allIncomingPaymentOrders as $incomingPaymentOrder) {
            // Если BankClientPaymentOrder
            if ($incomingPaymentOrder instanceof BankClientPaymentOrder
                && $this->settingManager->isRecipientGuarantPay(BankClientPaymentOrderHandler::getBankClientPaymentOrderAsArray($incomingPaymentOrder))) {
                if ($incomingPaymentOrder->getBankClientPaymentOrderFile() !== null) {
                    $total_amount += $incomingPaymentOrder->getAmount();
                    $paymentOrderDocumentDate = \DateTime::createFromFormat('d.m.Y', $incomingPaymentOrder->getDocumentDate());

                    if ($paymentOrderDocumentDate > $dateOfLastIncoming) {

                        $dateOfLastIncoming = $paymentOrderDocumentDate;
                    }
                } else {
                    $paymentTroubleTypesByPayment[self::PAYMENT_ORDER_FILE_MISSING] = $this->paymentAbnormalTypes[self::PAYMENT_ORDER_FILE_MISSING];
                }

            } elseif ($incomingPaymentOrder instanceof MandarinPaymentOrder) {
                // Если MandarinPaymentOrder
                $paymentOrderDocumentDate = $incomingPaymentOrder->getCreated();
                $total_amount += $incomingPaymentOrder->getAmount();

                if ($paymentOrderDocumentDate > $dateOfLastIncoming) {

                    $dateOfLastIncoming = $paymentOrderDocumentDate;
                }
            }
        }
        
        // если есть возврат переплаты
        if ($repaidOverpay) {
            $total_amount -= $repaidOverpay->getAmount();
        }

        // amount not matched (недоплата)
        if ($total_amount < $payment->getExpectedValue() && $this->isPaymentExpired($payment, $dateOfLastIncoming)) {

            $paymentTroubleTypesByPayment[self::AMOUNT_NOT_MATCHED] = $this->paymentAbnormalTypes[self::AMOUNT_NOT_MATCHED];
        }

        // amount not matched (переплата)
        if ($total_amount > $payment->getExpectedValue()) {

            $paymentTroubleTypesByPayment[self::AMOUNT_IS_OVERPAID] = $this->paymentAbnormalTypes[self::AMOUNT_IS_OVERPAID];
        }

        // date of payment is expired
        if ($this->isPaymentExpired($payment, $dateOfLastIncoming)) {

            $paymentTroubleTypesByPayment[self::DATE_DEBIT_IS_EXPIRED] = $this->paymentAbnormalTypes[self::DATE_DEBIT_IS_EXPIRED];
        }

        // accepted delivery order has no track number
        if ($this->isAcceptedDeliveryOrderHasNoServiceOrderNumber($deal->getDeliveryOrder())) {
            $paymentTroubleTypesByPayment[self::ACCEPTED_DELIVERY_ORDER_SERVICE_ORDER_NUMBER_MISSING] =
                $this->paymentAbnormalTypes[self::ACCEPTED_DELIVERY_ORDER_SERVICE_ORDER_NUMBER_MISSING];
        }


        return $paymentTroubleTypesByPayment;
    }

    /**
     * @param DeliveryOrder|null $deliveryOrder
     * @return bool
     */
    private function isAcceptedDeliveryOrderHasNoServiceOrderNumber(DeliveryOrder $deliveryOrder = null): bool
    {
        if (null === $deliveryOrder) {

            return false;
        }

        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();

        if ($deliveryServiceType->getName() === 'DPD') {
            /** @var DpdDeliveryOrder $dpdDeliveryOrder */
            $dpdDeliveryOrder = $deliveryOrder->getDpdDeliveryOrder();

            if ($dpdDeliveryOrder->getDpdOrderCreationStatus() === 'OK' && null === $dpdDeliveryOrder->getOrderNumber()) {

                return true;
            }
        }

        return false;
    }

    /**
     * Просрочен ли платеж по сделке по дате последнего платежа.
     *
     * @param Payment $payment
     * @param $dateOfLastIncoming
     * @return bool
     */
    public function isPaymentExpired(Payment $payment, $dateOfLastIncoming): bool
    {
        if (!$payment->getPlannedDate()) {

            return false;
        }

        if ($dateOfLastIncoming > $payment->getPlannedDate()) {

            return true;
        }

        return false;
    }

    /**
     * Выплачена ли сделка
     *
     * @param $total_incoming_amount
     * @param $expected_value
     * @return bool
     */
    public function isDealPaid($total_incoming_amount, $expected_value): bool
    {
        return $total_incoming_amount === $expected_value;
    }

    /**
     * @param Deal $deal
     * @param $payment_method_type
     * @return bool
     */
    public static function isPaymentMethodTypeAvailable(Deal $deal, string $payment_method_type): bool
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $paymentOrders = $payment->getPaymentOrders();

        // Если у payment нет платёжек вообще
        if ($paymentOrders->count() === 0) {

            return true;
        }

        /** @var PaymentOrder $paymentOrder */
        foreach ($paymentOrders as $paymentOrder) {
            /** @var PaymentMethodType $paymentMethodType */
            $paymentMethodType = $paymentOrder->getPaymentMethodType();

            if ($paymentMethodType->getName() !== $payment_method_type) {

                return false;
            }
        }

        return true;
    }

    /**
     * @param Deal $deal
     * @param DealAgent $dealAgent
     * @return bool
     */
    public function isDealAgentAllowedSetPaymentMethod(Deal $deal, DealAgent $dealAgent): bool
    {
        return $deal->getContractor() === $dealAgent;
    }
}