<?php

namespace Application\Service\Payment;

use Application\Entity\PaymentMethod;
use ModulePaymentOrder\Entity\PaymentMethodType;

/**
 * Class LegalEntityPolitics
 * @package Application\Service\CivilLawSubject
 */
class PaymentMethodPolitics
{
    /**
     * @param PaymentMethod $paymentMethod
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    public function isPaymentMethodDataUnique(PaymentMethod $paymentMethod, array $params)
    {
        // If PaymentMethodType bank_transfer
        if ($paymentMethod->getPaymentMethodType()->getName() == PaymentMethodType::BANK_TRANSFER) {

            $paymentMethodBankTransfer = $paymentMethod->getPaymentMethodBankTransfer();

            // Проверка параметров
            if ($paymentMethodBankTransfer->getAccountNumber() == $params['account_number']) {

                throw new \Exception('PaymentMethod with such AccountNumber already exists');
            }

            // Другие проверки PaymentMethodType bank_transfer...
        }

        // Проверка других PaymentMethodType...

        return true;
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @return bool
     */
    public function isLock(PaymentMethod $paymentMethod)
    {
        if ( !$paymentMethod->getIsPattern() || !$paymentMethod->getCivilLawSubject()->getIsPattern()){

            return true;
        }

        return false;
    }
}