<?php

namespace Application\Service\Payment;

use Application\Entity\DealAgent;
use Application\Entity\Discount;
use Application\Entity\Dispute;
use Application\Entity\Interfaces\PaymentOrderOwnerInterface;
use Application\Entity\Refund;
use Application\Entity\RepaidOverpay;
use Application\Entity\SystemPaymentDetails;
use Application\EventManager\DealEventProvider;
use Application\Provider\Mail\PaymentReceiptSender;
use Application\Service\Deal\DealDeadlineManager;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Entity\PaymentTransactionInterface;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use Application\Service\SettingManager;
use Doctrine\ORM\EntityManager;
use Application\Entity\Payment;
use Application\Entity\Deal;
use Application\Entity\PaymentMethod;
use Application\Entity\FeePayerOption;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModuleCode\Service\SmsStrategyContext;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentMethodTypeManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\EventManager\EventManager;

class PaymentManager
{
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    const PAYMENT_DEBIT_STATUS = ['1' => 'underpayment', '2' => 'matched', '3' => 'overpayment'];

    private $sms_notify_receipt_payment_full;
    private $email_notify_receipt_payment_full;
    private $sms_notify_receipt_payment_part;
    private $email_notify_receipt_payment_part;

    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var \Application\Service\Payment\PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * @var PaymentReceiptSender
     */
    private $paymentReceiptSender;

    /**
     * @var BankClientPaymentOrderManager
     */
    private $bankClientPaymentOrderManager;

    /**
     * @var \Application\Service\SettingManager
     */
    private $settingManager;

    /**
     * @var PaymentMethodTypeManager
     */
    private $paymentMethodTypeManager;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider;

    /**
     * @var DealEventProvider
     */
    private $dealEventProvider;

    /**
     * @var EventManager
     */
    private $deliveryEventManager;

    /**
     * @var array
     */
    private $config;

    /**
     * PaymentManager constructor.
     * @param EntityManager $entityManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param PaymentPolitics $paymentPolitics
     * @param SettingManager $settingManager
     * @param PaymentReceiptSender $paymentReceiptSender
     * @param BankClientPaymentOrderManager $bankClientPaymentOrderManager
     * @param PaymentMethodTypeManager $paymentMethodTypeManager
     * @param PaymentOrderManager $paymentOrderManager
     * @param CalculatedDataProvider $calculatedDataProvider
     * @param DealEventProvider $dealEventProvider
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                PaymentMethodManager $paymentMethodManager,
                                PaymentPolitics $paymentPolitics,
                                SettingManager $settingManager,
                                PaymentReceiptSender $paymentReceiptSender,
                                BankClientPaymentOrderManager $bankClientPaymentOrderManager,
                                PaymentMethodTypeManager $paymentMethodTypeManager,
                                PaymentOrderManager $paymentOrderManager,
                                CalculatedDataProvider $calculatedDataProvider,
                                DealEventProvider $dealEventProvider,
                                EventManager $eventManager,
                                array $config)
    {
        $this->entityManager        = $entityManager;
        $this->paymentMethodManager = $paymentMethodManager;
        $this->paymentPolitics      = $paymentPolitics;
        $this->paymentReceiptSender = $paymentReceiptSender;
        $this->settingManager       = $settingManager;
        $this->bankClientPaymentOrderManager = $bankClientPaymentOrderManager;
        $this->paymentMethodTypeManager = $paymentMethodTypeManager;
        $this->paymentOrderManager  = $paymentOrderManager;
        $this->calculatedDataProvider = $calculatedDataProvider;
        $this->dealEventProvider = $dealEventProvider;
        $this->deliveryEventManager = $eventManager;
        $this->config               = $config;

        $this->sms_notify_receipt_payment_full = $this->getDealNotifyConfig($this->config, 'payment_receipt_full', 'sms');
        $this->email_notify_receipt_payment_full = $this->getDealNotifyConfig($this->config, 'payment_receipt_full', 'email');
        $this->sms_notify_receipt_payment_part = $this->getDealNotifyConfig($this->config, 'payment_receipt_part', 'sms');
        $this->email_notify_receipt_payment_part = $this->getDealNotifyConfig($this->config, 'payment_receipt_part', 'email');
    }

    /**
     * @param $id
     * @return null|object
     */
    public function find($id)
    {
        return $this->entityManager->getRepository(Payment::class)->find($id);
    }

    /**
     * @param Deal $deal
     * @return null|object
     */
    public function getPaymentByDeal(Deal $deal)
    {
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        return $payment;
    }

    /**
     * Устанавливаем выбранной оплате соответсвующий способ оплаты субьекта права
     * @param Payment $payment
     * @param integer $payment_method_id
     * @return Payment
     * @throws \Exception
     */
    public function setPaymentMethod(Payment $payment, $payment_method_id): Payment
    {
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->entityManager->getRepository(PaymentMethod::class)->find($payment_method_id);

        if ($paymentMethod === null) {

            throw new \Exception('Not found PaymentMethod');
        }

        $payment->setPaymentMethod($paymentMethod);

        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        return $payment;
    }

    /**
     * @param Deal $deal
     * @param DealAgent $dealAgent
     * @param int|null $payment_method_id
     * @return mixed
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setPaymentMethodWithoutSaving(Deal $deal, DealAgent $dealAgent, $payment_method_id)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Set Payment Method
        if ($this->paymentPolitics->isDealAgentAllowedSetPaymentMethod($deal, $dealAgent)) {
            /** @var PaymentMethod $paymentMethod */
            $paymentMethod = $this->paymentMethodManager->get($payment_method_id);
            if ($paymentMethod) {
                $payment->setPaymentMethod($paymentMethod);
                $this->paymentMethodManager->processingPaymentMethodNameForDeal($payment->getDeal(), $paymentMethod);
            }
        }

        return $payment;
    }

    /**
     * Возвращает пропущенные обязаятельные ключи в массиве
     * @param array $requiredKeys
     * @param array $array
     * @return array
     */
    private function checkKeyRequired($requiredKeys = [], $array = [])
    {
        $keysCheck = array_keys($array);
        $missKeys = array_filter($requiredKeys, function($key) use ($keysCheck) {
            return !in_array($key, $keysCheck);
        });

        return $missKeys;
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @param null $delivery_period
     * @return Payment
     * @throws \Exception
     */
    public function createPaymentWithoutSaving(Deal $deal, $amount, $delivery_period=null): Payment
    {
        $payment = new Payment;
        //@TODO Set payment method
        #$payment->setPaymentMethod($this->setPaymentMethod($payment_method_id));

        $fee_payer_option = $deal->getFeePayerOption();

        // Set calculated data
        $payment = $this->setCalculatedPaymentData($payment, $amount, $fee_payer_option);
        // Set deal
        $payment->setDeal($deal);

        return $payment;
    }

    /**
     * @param Payment $payment
     * @param float $amount
     * @param FeePayerOption $feePayerOption
     * @return Payment
     * @throws \Exception
     */
    public function setCalculatedPaymentData(Payment $payment, float $amount, FeePayerOption $feePayerOption): Payment
    {
        $payment->setDealValue($amount);

        $fee = $this->calculatedDataProvider->getFeePercentage();
        $payment->setFee((float)$fee);

        $expected_value = $this->calculatedDataProvider->getCalculatedExpectedValue($amount, $feePayerOption);
        $payment->setExpectedValue((float)$expected_value);

        $released_value = $this->calculatedDataProvider->getCalculatedReleasedValue($amount, $feePayerOption);
        $payment->setReleasedValue((float)$released_value);

        return $payment;
    }

    /**
     * @param Payment $payment
     * @param int $delivery_period
     * @return \DateTime
     */
    public function getPlannedDateForPayment(Payment $payment, int $delivery_period): \DateTime
    {
        $currentDate = new \DateTime();

        return $currentDate->modify('+'.$delivery_period.' days');
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @param int $deal_id
     * @return PaymentOrderOwnerInterface|null
     * @throws \Exception
     *
     * @TODO Перенести в BankClientPaymentOrderManager?
     */
    public function manageIncomingBankClientPaymentOrderRelations(BankClientPaymentOrder $bankClientPaymentOrder,
                                                                  int $deal_id)
    {
        /**
         * @var Payment $payment
         * @var PaymentOrder $paymentOrder
         */
        $payment = $this->getPaymentByDealId($deal_id);

        // Если нет Payment, значит, нет Сделки
        if (!$payment) {
            throw new \Exception('Платёжка' . $bankClientPaymentOrder->getPaymentPurpose()
                .'- добавлена, но не привязана к Сделке (Сделки с ID '.$deal_id.' нет в системе)');
        }

        /** @var PaymentMethodType $paymentMethodType */
        $paymentMethodType = $this->paymentMethodTypeManager
            ->getPaymentMethodTypeByName('bank_transfer');

        // Если сделка еще не полностью оплачена (status Expected payment)
        if ($this->isIncomingPaymentExpected($payment)) {

            $this->entityManager->persist($bankClientPaymentOrder);
            $paymentOrder = $this->paymentOrderManager
                ->createPaymentOrderAndSetRelations($payment, $paymentMethodType, $bankClientPaymentOrder);

            if (!$paymentOrder->getId()) {

                throw new \Exception('Unable create Payment Order');
            }

            try {
                $payment_status = $this->getPaymentDebitStatus($payment);
                //// сделка оплачена ////
                if($payment_status === 'matched' ||  $payment_status === 'overpayment'){
                    // Записываем дату фактической оплаты
                    $payment->setDeFactoDate(new \DateTime());
                    ///// Сохранение банковских реквизитов customer /////
                    $this->paymentMethodManager->savingBankDetailsCustomerFromPaymentOrder($bankClientPaymentOrder, $payment);
                    /** @var Deal $deal */
                    $deal = $payment->getDeal();
                    //// *** доставка *** ////
                    /// @TODO доставка временно реализована без автоматики
                    // Вызываем событие Создания заказа на доставку в выбранном сервисе доставки
                    #$this->deliveryEventManager->trigger(
                    #    DeliveryEventProvider::EVENT_CREATE_DELIVERY_SERVICE_ORDER, $this, [
                    #    'create_delivery_service_order_params' => ['deliveryOrder' => $deal->getDeliveryOrder()]
                    #]);
                }
            }
            catch (\Throwable $t){

                throw new \Exception('The payment method customer not saved');
            }

            // Если сделка полностью оплачена Покупателем и нет переплаты
            if ($payment_status === 'matched') {
                //уведомить агентов сделки о получении оплаты в полном объеме
                $this->notifyDealAgentsAboutReceiptPaymentFull($bankClientPaymentOrder, $payment);
            } elseif ($payment_status === 'underpayment') {
                //уведомить агентов сделки о получении оплаты в частичном объеме
                $this->notifyDealAgentsAboutReceiptPaymentPart($bankClientPaymentOrder, $payment);
            }

            $this->entityManager->persist($payment);
            $this->entityManager->flush($payment);

            return $payment;
        }

        // Этот случай для добавления подтверждающей платежки (исходящая от GP)
        if ($this->isIncomingConfirmationTransactionExpected($payment->getDeal())) {

            // Подтверждающие платёжки только сохраняются, но не привязываются к PaymentOrder
            $this->entityManager->persist($bankClientPaymentOrder);

            // Определяем paymentOrderOwner (к чему пришла подтверждающая платёжка)
            $paymentOrderOwner = $this->bankClientPaymentOrderManager
                ->getPaymentOrderOwnerForBankClientPaymentOrder($payment->getDeal(), $bankClientPaymentOrder);

            if (null !== $paymentOrderOwner) {
                /** @var PaymentOrder $paymentOrder */
                $paymentOrder = $this->paymentOrderManager
                    ->createPaymentOrderAndSetRelations($paymentOrderOwner, $paymentMethodType, $bankClientPaymentOrder);

                if (!$paymentOrder->getId()) {

                    throw new \Exception('Unable create confirming Payment Order');
                }

                $this->entityManager->persist($paymentOrderOwner);
                $this->entityManager->flush($paymentOrderOwner);

                // Вызывываем событие. В нем определяется закрыта ли сделка. Если да, то рассылаются уведомления о закрытии.
                $this->dealEventProvider->getEventManager()->trigger(DealEventProvider::EVENT_AFTER_CONFIRMATION_TRANSACTION_CREATION, $this, [
                    'deal' => $payment->getDeal()
                ]);
            }

            return $paymentOrderOwner;
        }

        // иначе
        throw new \Exception('По сделке '.$payment->getDeal()->getNumber().' платежи не ожидаются');
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @param Payment $payment
     */
    private function notifyDealAgentsAboutReceiptPaymentFull(BankClientPaymentOrder $bankClientPaymentOrder, Payment $payment)
    {
        if($this->sms_notify_receipt_payment_full){
            try {
                $this->notifyCustomerAboutReceiptPaymentFullBySms($bankClientPaymentOrder, $payment);
                $this->notifyContractorAboutReceiptPaymentFullBySms($bankClientPaymentOrder, $payment);
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }
        if($this->email_notify_receipt_payment_full){
            try {
                $this->notifyCustomerAboutReceiptPaymentByEmail(
                    $payment,
                    PaymentReceiptSender::PAYMENT_FULL,
                    $bankClientPaymentOrder->getAmount()
                );
                $this->notifyContractorAboutReceiptPaymentByEmail($payment, PaymentReceiptSender::PAYMENT_FULL);
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @param Payment $payment
     */
    private function notifyDealAgentsAboutReceiptPaymentPart(BankClientPaymentOrder $bankClientPaymentOrder, Payment $payment)
    {
        if($this->sms_notify_receipt_payment_part){
            try {
                $this->notifyCustomerAboutReceiptPaymentPartBySms($bankClientPaymentOrder, $payment);
                $this->notifyContractorAboutReceiptPaymentPartBySms($bankClientPaymentOrder, $payment);
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }
        if($this->email_notify_receipt_payment_part){
            try {
                $this->notifyCustomerAboutReceiptPaymentByEmail(
                    $payment,
                    PaymentReceiptSender::PAYMENT_PART,
                    $bankClientPaymentOrder->getAmount()
                );
                $this->notifyContractorAboutReceiptPaymentByEmail(
                    $payment,
                    PaymentReceiptSender::PAYMENT_PART,
                    $bankClientPaymentOrder->getAmount()
                );
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
    }

    /**
     * @param Payment $payment
     * @param $payment_receipt_type
     * @param null $amount
     * @return bool
     * @throws \Exception
     */
    private function notifyCustomerAboutReceiptPaymentByEmail(Payment $payment, $payment_receipt_type, $amount = null)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        $email = $deal->getCustomer()->getEmail();
        $data = [
            'deal' => $deal,
            'agent_user_login'      => $deal->getCustomer()->getUser()->getLogin(),
            'counteragent_login'    => $deal->getContractor()->getUser()->getLogin(),
            'type_agent_notify'     => PaymentReceiptSender::TYPE_NOTIFY_CUSTOMER,
            'pay_amount'            => $amount,
            PaymentReceiptSender::TYPE_NOTIFY_KEY   => PaymentReceiptSender::PAYMENT_RECEIPT_CUSTOMER,
            'payment_receipt_type'  => $payment_receipt_type,
        ];
        // Send notification mail to Contractor
        $this->paymentReceiptSender->sendMail($email, $data);

        return true;
    }

    /**
     * @param Payment $payment
     * @param $payment_receipt_type
     * @param $amount
     * @return bool
     * @throws \Exception
     */
    private function notifyContractorAboutReceiptPaymentByEmail(Payment $payment, $payment_receipt_type, $amount = null)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        $email = $deal->getContractor()->getEmail();
        $data = [
            'deal' => $deal,
            'agent_user_login'      => $deal->getContractor()->getUser()->getLogin(),
            'counteragent_login'    => $deal->getCustomer()->getUser()->getLogin(),
            'type_agent_notify'     => PaymentReceiptSender::TYPE_NOTIFY_CONTRACTOR,
            'pay_amount'            => $amount,
            PaymentReceiptSender::TYPE_NOTIFY_KEY   => PaymentReceiptSender::PAYMENT_RECEIPT_CONTRACTOR,
            'payment_receipt_type'  => $payment_receipt_type,
        ];
        // Send notification mail to Contractor
        $this->paymentReceiptSender->sendMail($email, $data);

        return true;
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @param Payment $payment
     * @return bool
     * @throws \Exception
     */
    private function notifyCustomerAboutReceiptPaymentFullBySms(BankClientPaymentOrder $bankClientPaymentOrder, Payment $payment)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        $dealAgent = $deal->getCustomer();
        $data = [
            'amount' => $bankClientPaymentOrder->getAmount(),
            'total_amount' => $this->getTotalIncomingAmount($payment),
        ];
        $smsMessage = $this->getCustomerPaymentReceiptFullSmsMessage($deal, $data);

        return $this->notifyAboutReceiptPaymentBySms($dealAgent, $smsMessage);
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @param Payment $payment
     * @return bool
     * @throws \Exception
     */
    private function notifyContractorAboutReceiptPaymentFullBySms(BankClientPaymentOrder $bankClientPaymentOrder, Payment $payment)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        $dealAgent = $deal->getContractor();
        $data = [
            'amount' => $bankClientPaymentOrder->getAmount(),
            'total_amount' => $this->getTotalIncomingAmount($payment)
        ];
        $smsMessage = $this->getContractorPaymentReceiptFullSmsMessage($deal, $data);

        return $this->notifyAboutReceiptPaymentBySms($dealAgent, $smsMessage);
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @param Payment $payment
     * @return bool
     * @throws \Exception
     */
    private function notifyCustomerAboutReceiptPaymentPartBySms(BankClientPaymentOrder $bankClientPaymentOrder, Payment $payment)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        $dealAgent = $deal->getCustomer();
        $data = [
            'amount' => $bankClientPaymentOrder->getAmount(),
            'total_amount' => $this->getTotalIncomingAmount($payment),
            'expect_amount' => $payment->getExpectedValue(),
        ];
        $smsMessage = $this->getCustomerPaymentReceiptPartSmsMessage($deal, $data);

        return $this->notifyAboutReceiptPaymentBySms($dealAgent, $smsMessage);
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @param Payment $payment
     * @return bool
     * @throws \Exception
     */
    private function notifyContractorAboutReceiptPaymentPartBySms(BankClientPaymentOrder $bankClientPaymentOrder, Payment $payment)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        $dealAgent = $deal->getContractor();
        $data = [
            'amount' => $bankClientPaymentOrder->getAmount(),
            'total_amount' => $this->getTotalIncomingAmount($payment),
            'expect_amount' => $payment->getExpectedValue(),
        ];
        $smsMessage = $this->getContractorPaymentReceiptPartSmsMessage($deal, $data);

        return $this->notifyAboutReceiptPaymentBySms($dealAgent, $smsMessage);
    }

    /**
     * @param DealAgent $dealAgent
     * @param $smsMessage
     * @return bool
     * @throws \Exception
     */
    private function notifyAboutReceiptPaymentBySms(DealAgent $dealAgent, $smsMessage)
    {
        $userPhone = $dealAgent->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);

        } else {

            throw new \Exception('Forbidden by policy sms notification');
        }

        return true;
    }

    /**
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    private function isIncomingConfirmationTransactionExpected(Deal $deal): bool
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Discount $discount */
        $discount = $dispute ? $dispute->getDiscount() : null;
        /** @var Refund $refund */
        $refund = $dispute ? $dispute->getRefund() : null;
        /** @var RepaidOverpay $repaidOverpay */
        $repaidOverpay = $payment->getRepaidOverpay();

        $outgoingPaymentTransaction = null;
        $outgoingDiscountTransaction = null;
        $outgoingRefundTransaction = null;
        $outgoingRepaidOverpayTransaction = null;

        try {
            /** @var PaymentTransactionInterface $outgoingPaymentTransaction */ // Can throw exception
            $outgoingPaymentTransaction = $this->paymentOrderManager
                ->getPaymentOutgoingPaymentTransaction($payment);

            if ($discount) {
                /** @var PaymentTransactionInterface $outgoingDiscountTransaction */ // Can throw exception
                $outgoingDiscountTransaction = $this->paymentOrderManager
                    ->getDiscountOutgoingPaymentTransaction($discount);
            }

            if ($refund) {
                /** @var PaymentTransactionInterface $outgoingRefundTransaction */ // Can throw exception
                $outgoingRefundTransaction = $this->paymentOrderManager
                    ->getRefundOutgoingPaymentTransaction($refund);
            }

            if ($repaidOverpay) {
                /** @var PaymentTransactionInterface $outgoingRepaidOverpayTransaction */ // Can throw exception
                $outgoingRepaidOverpayTransaction = $this->paymentOrderManager
                    ->getRepaidOverpayOutgoingPaymentTransaction($repaidOverpay);
            }
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        if ($refund && $outgoingRefundTransaction) {

            return $this->checkBankClientPaymentOrderReadyToConfirm($outgoingRefundTransaction);
        }

        if ($outgoingPaymentTransaction && $this->checkBankClientPaymentOrderReadyToConfirm($outgoingPaymentTransaction)) {

            return true;
        }

        if ($discount && $outgoingDiscountTransaction && $this->checkBankClientPaymentOrderReadyToConfirm($outgoingDiscountTransaction)) {

            return true;
        }

        if ($repaidOverpay && $outgoingRepaidOverpayTransaction) {

            return $this->checkBankClientPaymentOrderReadyToConfirm($outgoingRepaidOverpayTransaction);
        }

        // @TODO Другие типы платёжей

        return false;
    }

    /**
     * @param PaymentTransactionInterface $bankClientPaymentOrder
     * @return bool
     */
    private function checkBankClientPaymentOrderReadyToConfirm(PaymentTransactionInterface $bankClientPaymentOrder): bool
    {
        if (!$bankClientPaymentOrder instanceof BankClientPaymentOrder) {

            return false;
        }

        if ($bankClientPaymentOrder->getType() !== PaymentOrderManager::TYPE_OUTGOING) {

            return false;
        }

        if (!$bankClientPaymentOrder->getBankClientPaymentOrderFile()) {

            return false;
        }

        try {
            // Can throw Exception
            if ($this->paymentOrderManager->isPayOff($bankClientPaymentOrder->getPaymentOrder())) {

                return false;
            }
        }
        catch (\Throwable $t) {

            return false;
        }

        return true;
    }

    /**
     * @param Payment $payment
     * @return float|int
     * @throws \Exception
     */
    public function getTotalIncomingAmount(Payment $payment)
    {
        return $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, false);
    }

    /**
     * @param $deal_id
     * @return mixed
     * @throws \Exception
     */
    public function getPaymentByDealId($deal_id)
    {
        $payment = $this->entityManager->getRepository(Payment::class)
            ->selectPaymentByDealId($deal_id);

        return $payment;
    }

    /**
     * @param Payment $payment
     * @return bool
     * @throws \Exception
     */
    public function isIncomingPaymentExpected(Payment $payment)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();

        // DeFactoDate не установлен (если есть дата, то сделка считается оплаченной)
        // Contractor и Customer дали согласие
        // в сделке есть file
        if (!$payment->getDeFactoDate()
            && $deal->getDateOfConfirm()
            && $deal->getDealContractFile()
            && $deal->getContractor()->getDealAgentConfirm()
            && $deal->getCustomer()->getDealAgentConfirm()
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param array $params
     * @param int $page
     * @return array
     * @throws \Exception
     */
    public function getPaymentsForAbnormalDeals(array $params, int $page)
    {
        /** @var SystemPaymentDetails $systemPaymentDetail */
        $systemPaymentDetail = $this->settingManager->getSystemPaymentDetail();

        $gp_account = $systemPaymentDetail->getAccountNumber();

        $payments = $this->entityManager->getRepository(Payment::class)
            ->selectSortedPaymentsForAbnormalDeals($params, $page, $gp_account);

        $paymentsOutput = $this->getPaymentsCollectionForOutput($payments);

        return ['payments' => $paymentsOutput, 'paginator' => $payments->getPages()];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getPaymentsWithExpiredDeals(array $params)
    {
        /** @var SystemPaymentDetails $systemPaymentDetail */
        $systemPaymentDetail = $this->settingManager->getSystemPaymentDetail();

        $gp_account = $systemPaymentDetail->getAccountNumber();

        // Get all payments with expired deals with sort and search params
        $payments = $this->entityManager->getRepository(Payment::class)
            ->selectSortedPaymentsWithExpiredDeals($params, $gp_account);

        $paymentsOutput = $this->getPaymentsCollectionForOutput($payments);

        return ['payments' => $paymentsOutput, 'paginator' => $payments->getPages()];
    }

    /**
     * @param $payments
     * @return array
     */
    public function getPaymentsCollectionForOutput($payments)
    {
        $paymentsOutput = [];
        foreach ($payments as $payment) {
            $paymentsOutput[] = $this->getPaymentForOutput($payment);
        }

        return $paymentsOutput;
    }

    /**
     * @param Payment $payment
     * @return array
     */
    public function getPaymentForOutput(Payment $payment): array
    {
        $paymentOutput = [];

        /** @var Deal $deal */
        $deal = $payment->getDeal();

        // Important! Чтобы не модифицировать даты в объектах, клонируем их. Далее работаем только с копиями!
        $deFactoDate = null;
        if($payment->getDeFactoDate()) {
            $deFactoDate = clone $payment->getDeFactoDate();
        }
        /** @var \DateTime $dateOfConfirm */
        $dateOfConfirm = clone $payment->getDeal()->getDateOfConfirm();

        $paymentOutput['id'] = $payment->getId();
        $paymentOutput['amount_without_fee'] = $payment->getDealValue();
        $paymentOutput['customer_amount'] = $payment->getExpectedValue();
        $paymentOutput['planned_date'] = $payment->getPlannedDate()->format('d.m.Y');
        $paymentOutput['planned_date_milliseconds'] = $payment->getPlannedDate()->getTimestamp();

        $paymentOutput['de_facto_date'] = null;
        $paymentOutput['date_of_deadline'] = null;
        if ($deFactoDate) {
            $paymentOutput['de_facto_date'] = $deFactoDate->format('d.m.Y');
            $paymentOutput['de_facto_date_milliseconds'] = $deFactoDate->getTimestamp();

            /** @var \DateTime $dealDeadline */
            $dealDeadline = DealDeadlineManager::getDealDeadlineDate($payment);
            $paymentOutput['date_of_deadline'] = $dealDeadline->format('d.m.Y');
        }

        $paymentOutput['deal'] = [];
        $paymentOutput['deal']['id'] = $deal->getId();
        $paymentOutput['deal']['name'] = $deal->getName();
        $paymentOutput['deal']['owner'] = $deal->getOwner()->getUser()->getLogin();
        $paymentOutput['deal']['expired_date'] = $dateOfConfirm->modify('+'.$payment->getDeal()->getDeliveryPeriod().' days');
        $paymentOutput['deal']['created'] = $deal->getCreated()->format('d.m.Y');
        $paymentOutput['deal']['created_milliseconds'] = $deal->getCreated()->getTimestamp();
        $paymentOutput['deal']['customer'] = $deal->getCustomer()->getUser() ? $deal->getCustomer()->getUser()->getLogin() : $deal->getCustomer()->getEmail();
        $paymentOutput['deal']['contractor'] = $deal->getContractor()->getUser() ? $deal->getContractor()->getUser()->getLogin() : $deal->getContractor()->getEmail();

        $paymentOutput['deal']['abnormality_reasons'] = $this->getTroubleReasons($payment);

        return $paymentOutput;
    }

    /**
     * @param Payment $payment
     * @return array
     *
     * @TODO Пененсти в DealManager?
     */
    public function getTroubleReasons(Payment $payment): array
    {
        $allIncomingPaymentOrders = null;

        if ($payment->getPaymentOrders()->count() > 0) {

            $allIncomingPaymentOrders = $this->paymentOrderManager
                ->getAllPaymentTransactionsByType($payment, PaymentOrderManager::TYPE_INCOMING);

        }

        /** @var array $trouble_reasons */
        $trouble_reasons = $this->paymentPolitics
            ->getDealTroubleTypes($payment, $allIncomingPaymentOrders);

        return $trouble_reasons;
    }

    /**
     * @param Payment $payment
     * @return mixed
     * @throws \Exception
     */
    public function getPaymentDebitStatus(Payment $payment)
    {
        $total_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment);

        // Переплата
        if ($total_amount > $payment->getExpectedValue()) {

            return self::PAYMENT_DEBIT_STATUS['3']; // overpayment
        }
        // Полностью оплачена
        if ($total_amount === $payment->getExpectedValue()) {

            return self::PAYMENT_DEBIT_STATUS['2']; // matched
        }
        // Недоплата
        return self::PAYMENT_DEBIT_STATUS['1']; // underpayment
    }

    /**
     * @param Deal $deal
     * @return array
     * @throws \Exception
     */
    public function getIncomingPaymentTransactionsForDeal(Deal $deal)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $incomingPaymentOrders = $this->paymentOrderManager->getIncomingPaymentTransactions($payment);

        return $this->getPaymentTransactionsCollectionOutput($incomingPaymentOrders);
    }

    /**
     * @param Payment $payment
     * @return float|int
     * @throws \Exception
     */
    public function getRefundAmountByPayment(Payment $payment)
    {
        $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);

        return $this->calculatedDataProvider->getCalculatedRefundAmount($payment, $total_incoming_amount);
    }

    /**
     * @param $paymentOrders
     * @return array
     */
    private function getPaymentTransactionsCollectionOutput($paymentOrders): array
    {
        $paymentTransactionOutput = [];

        /** @var PaymentOrder $paymentOrder */
        foreach ($paymentOrders as $paymentOrder) {
            if ($paymentOrder instanceof BankClientPaymentOrder) {
                // @TODO Передалть getBankClientPaymentOrderOutput в static
                $paymentTransactionOutput[] = $this->bankClientPaymentOrderManager->getBankClientPaymentOrderOutput($paymentOrder);
            } elseif ($paymentOrder instanceof MandarinPaymentOrder) {
                $paymentTransactionOutput[] = MandarinPaymentOrderManager::getMandarinPaymentOrderOutput($paymentOrder);
            }
        }

        return $paymentTransactionOutput;
    }
}