<?php
namespace Application\Service;

use Application\Entity\Deal;

trait SmsNotifyTrait
{
    /**
     * @param Deal $deal
     * @return string
     */
    public function getInvitationToDealForOwnerSmsMessage(Deal $deal)
    {
        return 'GP: Pozdravlyaem vami byla uspeshno sozdana sdelka nomer '.$this->getSafeNumber($deal->getNumber());
    }


    /**
     * @param Deal $deal
     * @return string
     */
    public function getInvitationToDealSmsMessage(Deal $deal)
    {
        return 'GP: Vas priglasili k uchastiyu v sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getConfirmedDealAgentSmsMessage(Deal $deal)
    {
        return 'GP: Pol\'zovatel\' soglasilsya s usloviyami sdelki nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getChangedDealSmsMessage(Deal $deal)
    {
        return 'GP: Pol\'zovatel\' izmenil usloviya sdelki nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getConfirmedDealSmsMessage(Deal $deal)
    {
        return 'GP: Dostignuto soglashenie po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @param $data
     * @return string
     */
    public function getCustomerPaymentReceiptFullSmsMessage(Deal $deal, $data)
    {
        return 'GP: Postupila oplata: '.$data['amount'].' rub. Itogo summa po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' : '.$data['total_amount'].' rub. Pozdravlyaem, sdelka polnost\'yu oplachena.';
    }

    /**
     * @param Deal $deal
     * @param $data
     * @return string
     */
    public function getContractorPaymentReceiptFullSmsMessage(Deal $deal, $data)
    {
        return 'GP: Postupila oplata: '.$data['amount'].' rub. Itogo summa po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' : '.$data['total_amount'].' rub. Pol\'zovatel\' oplatil sdelku v polnom ob\'eme!';
    }

    /**
     * @param Deal $deal
     * @param $data
     * @return string
     */
    public function getCustomerPaymentReceiptPartSmsMessage(Deal $deal, $data)
    {
        return 'GP: Postupila oplata: '.$data['amount'].' rub. Itogo summa po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' : '.$data['total_amount'].' rub iz '.$data['expect_amount'].' rub.';
    }

    /**
     * @param Deal $deal
     * @param $data
     * @return string
     */
    public function getContractorPaymentReceiptPartSmsMessage(Deal $deal, $data)
    {
        return 'GP: Postupila oplata: '.$data['amount'].' rub. Itogo summa po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' : '.$data['total_amount'].' rub iz '.$data['expect_amount'].' rub.';
    }

    /**
     * @param Deal $deal
     * @param $trackingNumber
     * @return string
     */
    public function getCustomerTrackingNumberSmsMessage(Deal $deal, $trackingNumber)
    {
        return 'GP: Vash trek-nomer k sdelke '.$this->getSafeNumber($deal->getNumber()).' : '.$trackingNumber;
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getCustomerDisputeOpenSmsMessage(Deal $deal)
    {
        return 'GP: Vami byl otkryt spor po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getContractorDisputeOpenSmsMessage(Deal $deal)
    {
        return 'GP: Pol\'zovatel\' otkryl spor po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getCustomerClosedDisputeForCustomerSmsMessage(Deal $deal)
    {
        return 'GP: Vami byl otozvan spor po sdelke '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getCustomerClosedDisputeForContractorSmsMessage(Deal $deal)
    {
        return 'GP: Pol\'zovatel\' otozval spor po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @param $number_of_extension_days
     * @return string
     */
    public function getClosedDisputeWithDayForCustomerSmsMessage(Deal $deal, $number_of_extension_days)
    {
        return 'GP: Spor po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' zakryt. Period garantii prodlen na '.$number_of_extension_days.' d.';
    }

    /**
     * @param Deal $deal
     * @param $number_of_extension_days
     * @return string
     */
    public function getClosedDisputeWithDayForContractorSmsMessage(Deal $deal, $number_of_extension_days)
    {
        return 'GP: Spor po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' zakryt. Period garantii prodlen na '.$number_of_extension_days.' d.';
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getClosedDisputeWithoutDayForCustomerSmsMessage(Deal $deal)
    {
        return 'GP: Spor po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' zakryt bez prodleniya perioda garantii';
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getClosedDisputeWithoutDayForContractorSmsMessage(Deal $deal)
    {
        return 'GP: Spor po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' zakryt bez prodleniya perioda garantii';
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getDiscountProvidedForCustomerSmsMessage(Deal $deal, $amount)
    {
        return 'GP: Vam byla predostavlena skidka '.$amount.' rub. k sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getDiscountProvidedForContractorSmsMessage(Deal $deal, $amount)
    {
        return 'GP: Pol\'zovatelyu byla predostavlena skidka '.$amount.' rub. k sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getRefundProvidedForCustomerSmsMessage(Deal $deal, $amount)
    {
        return 'GP: Prinyato reshenie o vozvrate sredstv '.$amount.' rub. po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getRefundProvidedForContractorSmsMessage(Deal $deal, $amount)
    {
        return 'GP: Prinyato reshenie o vozvrate sredstv '.$amount.' rub. po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getReferredToTribunalForCustomerSmsMessage(Deal $deal)
    {
        return 'GP: Spor po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' peredan v tretejskij sud';
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getReferredToTribunalForContractorSmsMessage(Deal $deal)
    {
        return 'GP: Spor po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' peredan v tretejskij sud';
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getNearDeadlineForCustomerSmsMessage(Deal $deal)
    {
        return 'GP: Srok garantii po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' skoro zakonchitsya';
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getNearDeadlineForContractorSmsMessage(Deal $deal)
    {
        return 'GP: Srok garantii po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' skoro zakonchitsya';
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getDiscountRequestForCounterAgentSmsMessage(Deal $deal, $amount)
    {
        return 'Pol\'zovatel\' zaprosil skidku v razmere '.$amount.' rub. po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getDiscountConfirmForAuthorRequestSmsMessage(Deal $deal, $amount)
    {
        return 'Vash zapros na skidku v razmere '.$amount.' rub. po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' podtverzhden';
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getDiscountRefuseForAuthorRequestSmsMessage(Deal $deal, $amount)
    {
        return 'Vash zapros na skidku v razmere '.$amount.' rub. po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' otklonen';
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getArbitrageRequestForCounterAgentSmsMessage(Deal $deal)
    {
        return 'Pol\'zovatel\' zaprosil soglasitel\'nuyu komissiyu po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getArbitrageConfirmForAuthorRequestSmsMessage(Deal $deal)
    {
        return 'Vash zapros na soglasitel\'nuyu komissiyu po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' podtverzhden';
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getArbitrageRefuseForAuthorRequestSmsMessage(Deal $deal)
    {
        return 'Vash zapros na soglasitel\'nuyu komissiyu po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' otklonen';
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getRefundRequestForCounterAgentSmsMessage(Deal $deal, $amount)
    {
        return 'Pol\'zovatel\' zaprosil vozvrat summy '.$amount.' rub. po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getRefundConfirmForAuthorRequestSmsMessage(Deal $deal, $amount)
    {
        return 'Vash zapros na vozvrat summy '.$amount.' rub. po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' podtverzhden';
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getRefundRefuseForAuthorRequestSmsMessage(Deal $deal, $amount)
    {
        return 'Vash zapros na vozvrat summy '.$amount.' rub. po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' otklonen';
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getTribunalRequestForCounterAgentSmsMessage(Deal $deal)
    {
        return 'Pol\'zovatel\' zaprosil tretejskij sud po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getTribunalConfirmForAuthorRequestSmsMessage(Deal $deal)
    {
        return 'Vash zapros na tretejskij sud po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' podtverzhden';
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getTribunalRefuseForAuthorRequestSmsMessage(Deal $deal)
    {
        return 'Vash zapros na tretejskij sud po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' otklonen';
    }


    /**
     * Метод общий для двух вариантов
     *
     * @param Deal $deal
     * @param string $tracking_number
     * @return string
     */
    public function getDeliveryServiceOrderCreationSmsMessage(Deal $deal, string $tracking_number): string
    {
        return 'Sozdan zakaz na dostavku po sdelke nomer '.$this->getSafeNumber($deal->getNumber(). '. Treknomer: '.$tracking_number);
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getSuccessfulDeliverySmsMessage(Deal $deal): string
    {
        return 'Tovar po sdelke nomer '.$this->getSafeNumber($deal->getNumber().' dostavlen');
    }

    /**
     * @param Deal $deal
     * @param string $tracking_number
     * @return string
     */
    public function getUnsuccessfulDeliverySmsMessage(Deal $deal, string $tracking_number): string
    {
        return 'Ne udalos\' dostavit\' tovar po sdelke nomer '.$this->getSafeNumber($deal->getNumber(). '. Treknomer: '.$tracking_number);
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getDeliveryConfirmedForCustomerSmsMessage(Deal $deal)
    {
        if($deal->getDealType()->getIdent() === 'U') {
            return 'Vami bylo podtverzhdeno vypolnenie uslugi po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
        }
        return 'Vami bylo podtverzhdeno poluchenie tovara po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getDeliveryConfirmedForContractorSmsMessage(Deal $deal)
    {
        if($deal->getDealType()->getIdent() === 'U') {
            return 'Pol\'zovatel\' podtverdil vypolnenie uslugi po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
        }
        return 'Pol\'zovatel\' podtverdil poluchenie tovara po sdelke nomer '.$this->getSafeNumber($deal->getNumber());
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getCustomerMandarinPaymentReceiptFullSmsMessage(Deal $deal, $amount)
    {
        return 'GP: Postupila oplata po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' : '.$amount.' rub. Sdelka polnost\'yu oplachena.';
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getContractorMandarinPaymentReceiptFullSmsMessage(Deal $deal, $amount)
    {
        return 'GP: Postupila oplata po sdelke nomer '.$this->getSafeNumber($deal->getNumber()).' : '.$amount.' rub. Sdelka polnost\'yu oplachena.';
    }

    /**
     * @param $deal_number
     * @return bool|string
     *
     * @TODO Исключить использование, когда(если) откажемся от решётки в номере сделки
     */
    private function getSafeNumber($deal_number)
    {
        return str_replace('#','', $deal_number);
    }
}