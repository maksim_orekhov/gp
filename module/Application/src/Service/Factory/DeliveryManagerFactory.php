<?php
namespace Application\Service\Factory;

use Application\Provider\Mail\DealNotificationSender;
use Application\Service\Deal\DealPolitics;
use Core\Service\TwigRenderer;
use Interop\Container\ContainerInterface;
use ModuleDelivery\Service\DeliveryOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\DeliveryManager;

/**
 * This is the factory class for TokenAdapter service
 */
class DeliveryManagerFactory implements FactoryInterface
{
    /**
     * This method creates the MailAdapter service and returns its instance.
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager  = $container->get('doctrine.entitymanager.orm_default');
        $config         = $container->get('Config');
        $twigRenderer   = $container->get(TwigRenderer::class);
        $dealNotificationSender = new DealNotificationSender($twigRenderer, $config);
        $dealPolitics    = $container->get(DealPolitics::class);
        $deliveryOrderManager = $container->get(DeliveryOrderManager::class);
        $payOffManager    = $container->get(PayOffManager::class);

        return new DeliveryManager(
            $entityManager,
            $dealNotificationSender,
            $dealPolitics,
            $deliveryOrderManager,
            $payOffManager,
            $config
        );
    }
}