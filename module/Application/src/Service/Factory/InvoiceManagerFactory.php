<?php
namespace Application\Service\Factory;

use Application\Service\InvoiceManager;
use Application\Service\NdsTypeManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class InvoiceManagerFactory
 * @package Application\Service\Factory
 */
class InvoiceManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return InvoiceManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager  = $container->get('doctrine.entitymanager.orm_default');
        $ndsTypeManager = $container->get(NdsTypeManager::class);

        return new InvoiceManager(
            $entityManager,
            $ndsTypeManager
        );
    }
}