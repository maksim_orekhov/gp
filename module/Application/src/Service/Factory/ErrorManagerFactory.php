<?php
namespace Application\Service\Factory;

use Application\Service\ErrorManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Provider\Mail\ErrorMailSender;

class ErrorManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ErrorManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $errorMailSender = $container->get(ErrorMailSender::class);

        return new ErrorManager(
            $config,
            $errorMailSender
        );
    }
}