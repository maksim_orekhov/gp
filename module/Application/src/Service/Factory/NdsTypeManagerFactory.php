<?php
namespace Application\Service\Factory;

use Application\Service\NdsTypeManager;
use Application\Service\Payment\CalculatedDataProvider;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * This is the factory class for TokenAdapter service
 */
class NdsTypeManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return NdsTypeManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $calculatedDataProvider    = $container->get(CalculatedDataProvider::class);

        return new NdsTypeManager($entityManager, $calculatedDataProvider);
    }
}