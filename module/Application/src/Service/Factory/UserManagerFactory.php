<?php
namespace Application\Service\Factory;

use Core\Service\Base\BaseEmailManager;
use ModuleFileManager\Service\FileManager;
use Application\Hydrator\UserHydrator;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\UserManager;

/**
 * Это фабрика для UserManager. Ее целью является инстанцирование сервиса.
 */
class UserManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserManager|object
     */
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $fileManager = $container->get(FileManager::class);
        $userHydrator = $container->get(UserHydrator::class);
        $baseEmailManager = $container->get(BaseEmailManager::class);

        return new UserManager(
            $entityManager,
            $fileManager,
            $userHydrator,
            $baseEmailManager
        );
    }
}
