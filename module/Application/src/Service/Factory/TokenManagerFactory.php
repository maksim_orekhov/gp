<?php
namespace Application\Service\Factory;

use Application\Service\TokenManager;
use Core\Adapter\TokenAdapter;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\SessionContainerManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class BaseTokenManagerFactory
 * @package Core\Service
 */
class TokenManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return TokenManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $tokenAdapter = $container->get(TokenAdapter::class);
        $baseEmailManager = $container->get(BaseEmailManager::class);
        $baseUserManager = $container->get(BaseUserManager::class);
        $sessionContainerManager = $container->get(SessionContainerManager::class);
        $config = $container->get('config');

        return new TokenManager(
            $entityManager,
            $tokenAdapter,
            $baseEmailManager,
            $baseUserManager,
            $sessionContainerManager,
            $config
        );
    }
}
