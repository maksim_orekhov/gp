<?php
namespace Application\Service\Parser;

/**
 * Class ParserBank
 * @package Application\Service\Parser
 */
class ParserBank
{
    /**
     * @param $file_content
     * @return array
     */
    public function execute($file_content)
    {
        $documents = [];
        $documents_block = [];
        $section = false;
        $section_start = 'СекцияДокумент';
        $section_end = 'КонецДокумента';

        $data = $this->processingData($file_content);

        foreach ($data as $key => $item) {
            $result = explode('=', $item);
            $name = trim($result[0]);

            if (count($result) == 2) {
                $value = trim($result[1]);

                if ($name == $section_start) {
                    $section = true;
                }
                if ($this->rules($name) && $section) {
                    $documents_block[$this->rules($name)] = array(
                        'NAME'  => $name,
                        'VALUE' => $value
                    );
                }
            } else {

                if ($name == $section_end) {
                    $documents[] = $documents_block;
                    $documents_block = [];
                    $section = false;
                }
            }
        }
        return $documents;
    }

    /**
     * @param $text
     * @return array
     */
    private function processingData($text){
        $text = rtrim($text);
        $text = mb_convert_encoding($text, "UTF-8", "windows-1251");
        $data = explode("\n", $text);

        return $data;
    }

    /**
     * @param $rule
     * @return bool|mixed
     */
    private function rules($rule) {
        $rules = [
            'СекцияДокумент' => 'doctype',
            'Номер' => 'inbankid',
            'Дата' => 'docdate',
            'Сумма' => 'summ',
            'ДатаСписано' => 'outdate',
            'ДатаПоступило' => 'indate',
            'ПлательщикСчет' => 'payeraccount',
            'Плательщик' => 'payerinfo',
            'ПлательщикИНН' => 'payerinn',
            'Плательщик1' => 'payer',
            'ПлательщикРасчСчет' => 'payerdealaccount',
            'ПлательщикБанк1' => 'payerbank1',
            'ПлательщикБанк2' => 'payerbank2',
            'ПлательщикБИК' => 'payerbik',
            'ПлательщикКорсчет' => 'payerfixaccount',
            'ПолучательСчет' => 'recieveraccount',
            'Получатель' => 'recieverinfo',
            'ПолучательИНН' => 'recieverinn',
            'Получатель1' => 'reciever1',
            'ПолучательРасчСчет' => 'recieverdealaccount',
            'ПолучательБанк1' => 'recieverbank1',
            'ПолучательБанк2' => 'recieverbank2',
            'ПолучательБИК' => 'recieverbik',
            'ПолучательКорсчет' => 'recieverfixaccount',
            'ВидОплаты' => 'paytype',
            'НазначениеПлатежа' => 'paydirection',
            'СтатусСоставителя' => 'makerstatus',
            'ПлательщикКПП' => 'payerkpp',
            'ПолучательКПП' => 'recieverkpp',
            'ПоказательКБК' => 'showerkbk',
            'ОКАТО' => 'okato',
            'ПоказательОснования' => 'showerfundament',
            'ПоказательПериода' => 'showerperiod',
            'ПоказательНомера' => 'showernumber',
            'ПоказательДаты' => 'showerdate',
            'ПоказательТипа' => 'showertype',
            'СрокПлатежа' => 'paymentperiod',
            'Очередность' => 'quenue',
        ];

        if (key_exists($rule, $rules)) {

            return $rules[$rule];
        }

        return false;
    }
}