<?php
namespace Application\Service\Parser;

use Application\Service\Parser\ParserBank;

/**
 * Class ParserAdapter
 * @package Application\Service\Parser
 */
class ParserAdapter
{
    /**
     * @var array
     */
    private $classes;

    /**
     * ParserAdapter constructor.
     */
    public function __construct()
    {
        $this->classes = array(
            'bank_client' => ParserBank::class,
        );
    }

    /**
     * @param $file_content
     * @param $parse_type
     * @return mixed
     */
    public function execute($file_content, $parse_type)
    {
        $parse_class = $this->getAdapter($parse_type);

        $parser = new $parse_class();

        return $parser->execute($file_content);
    }

    /**
     * @param $parse_type
     * @return mixed
     * @throws \Exception
     */
    private function getAdapter($parse_type)
    {
        if (!empty($parse_type) && !key_exists($parse_type, $this->classes)) {

            throw new \Exception('Not found parser Class');
        }
        return $this->classes[$parse_type];
    }
}