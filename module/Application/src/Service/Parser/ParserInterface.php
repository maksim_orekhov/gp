<?php

namespace Application\Service\Parser;

/**
 * Interface ParserInterface
 * @package Application\Provider\Sms
 */
interface ParserInterface
{
    /**
     * @param string $path_to_file
     * @return mixed
     */
    public function execute($path_to_file);
}