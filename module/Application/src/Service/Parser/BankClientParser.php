<?php
namespace Application\Service\Parser;

use Application\Service\Parser\ParserInterface;

/**
 * Class BankClientParser
 * @package Application\Service\Parser
 */
class BankClientParser implements ParserInterface
{
    const SECTION_START_POINT   = 'СекцияДокумент';
    const SECTION_END_POINT     = 'КонецДокумента';
    const CHARGE_POINT_PROPERTY_NAMES = [
        'СекцияДокумент'        => 'document_type',     //
        'Номер'                 => 'document_number',   //
        'Дата'                  => 'document_date',     //
        'ДатаПоступило'         => 'date_of_receipt',   //
        'ДатаСписано'           => 'date_of_debit',     //
        'Сумма'                 => 'amount',            //
        'ПолучательСчет'        => 'recipient_account', //
        'Получатель'            => 'recipient',         //
        'ПолучательИНН'         => 'recipient_inn',     //
        'Получатель1'           => 'recipient_1',       //
        'ПолучательКПП'         => 'recipient_kpp',     //
        'ПолучательБанк1'       => 'recipient_bank_1',  //
        'ПолучательБИК'         => 'recipient_bik',     //
        'ПолучательКорсчет'     => 'recipient_corr_account',//
        'ПлательщикСчет'        => 'payer_account',     //
        'Плательщик'            => 'payer',             //
        'ПлательщикИНН'         => 'payer_inn',         //
        'Плательщик1'           => 'payer_1',           //
        'ПлательщикКПП'         => 'payer_kpp',         //
        'ПлательщикБанк1'       => 'payer_bank_1',      //
        'ПлательщикБИК'         => 'payer_bik',         //
        'ПлательщикКорсчет'     => 'payer_corr_account',//
        'НазначениеПлатежа'     => 'payment_purpose',   //
        'ВидОплаты'             => 'payment_type',      //
        'Очередность'           => 'priority',          //

        // Этих параметров нет в файле
        'ПлательщикРасчСчет'    => 'payerdealaccount',
        'ПлательщикБанк2'       => 'payerbank2',
        'ПолучательРасчСчет'    => 'recieverdealaccount',
        'ПолучательБанк2'       => 'recieverbank2',
        'СтатусСоставителя'     => 'makerstatus',
        'ПоказательКБК'         => 'showerkbk',
        'ОКАТО'                 => 'okato',
        'ПоказательОснования'   => 'showerfundament',
        'ПоказательПериода'     => 'showerperiod',
        'ПоказательНомера'      => 'showernumber',
        'ПоказательДаты'        => 'showerdate',
        'ПоказательТипа'        => 'showertype',
        'СрокПлатежа'           => 'paymentperiod',
        ];

    /**
     * @inheritdoc
     */
    public function execute($path_to_file)
    {
        $array_of_charges = [];
        $charge = [];
        $section = false;
        // Try open file for read
        if (($handle = fopen($path_to_file, "r")) !== false) {
            // Get line by line
            while (($line = fgets($handle, 1000)) !== false) {
                // Encoding line to UTF-8
                $line = mb_convert_encoding($line, 'UTF-8', \Application\Controller\BankClientController::ORIGINAL_ENCODING);
                // Split line to array by '='
                $result = explode('=', $line);
                // Get line(parameter) name
                $charge_point_name = trim($result[0]);
                // If line contains value
                if (count($result) === 2) {
                    // Get line value
                    $value = (trim($result[1]) === '' || trim($result[1]) === '0') ? null : trim($result[1]);
                    // If line is the SECTION_START_POINT
                    if ($charge_point_name === self::SECTION_START_POINT) {
                        $section = true;
                    }
                    if ($this->getPropertyName($charge_point_name) && $section) {
                        $charge[$this->getPropertyName($charge_point_name)] = $this->dataTypeConversion($this->getPropertyName($charge_point_name), $value);
                    }
                } else {
                    // If line does not contain value, check whether it is the SECTION_END_POINT
                    if ($charge_point_name === self::SECTION_END_POINT) {
                        $array_of_charges[] = $charge;
                        $charge = [];
                        $section = false;
                    }
                }
            }
            // Close file
            fclose($handle);
        }

        return $array_of_charges;
    }

    /**
     * @param $name
     * @return bool|mixed
     */
    public function getPropertyName($name) {

        if (array_key_exists($name, self::CHARGE_POINT_PROPERTY_NAMES)) {

            return self::CHARGE_POINT_PROPERTY_NAMES[$name];
        }

        return false;
    }

    /**
     * @param $name
     * @param $value
     * @return float
     */
    public function dataTypeConversion($name, $value)
    {
        if($name === 'amount') {
            return (float)$value;
        }

        return $value;
    }
}