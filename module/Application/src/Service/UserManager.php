<?php
namespace Application\Service;

use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethod;
use Core\Entity\Interfaces\EmailInterface;
use Core\Entity\Interfaces\UserInterface;
use Core\Exception\LogicException;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseUserManager;
use Doctrine\ORM\EntityManager;
use ModuleFileManager\Service\FileManager;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModuleCode\Entity\CodeOperationType;
use ModuleCode\Entity\Code;
use Application\Entity\Role;
use Application\Entity\User;
use Application\Entity\Email;
use Application\Hydrator\UserHydrator;

/**
 * Class UserManager
 * @package ModuleAuth\Service
 */
class UserManager extends BaseUserManager
{
    const ERROR_EMAIL_NOT_FOUND                 = 'Can not find user with this email';
    const ERROR_USER_WITH_SUCH_EMAIL_NOT_FOUND  = 'User with provided email not found';
    const ERROR_EMAIL_OWNER_NOT_FOUND           = 'Owner of provided Email not found';
    const ERROR_PHONE_OWNER_NOT_FOUND           = 'Owner of provided Phone not found';
    const ERROR_PASSWORD_CANT_BE_CHANGED        = 'Can not change password. Contact the administrator.';

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @var UserHydrator
     */
    private $userHydrator;

    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;

    /**
     * UserManager constructor.
     * @param EntityManager $entityManager
     * @param FileManager $fileManager
     * @param UserHydrator $userHydrator
     */
    public function __construct(EntityManager $entityManager,
                                FileManager $fileManager,
                                UserHydrator $userHydrator,
                                BaseEmailManager $baseEmailManager)
    {
        parent::__construct($entityManager);

        $this->fileManager = $fileManager;
        $this->userHydrator = $userHydrator;
        $this->baseEmailManager = $baseEmailManager;
    }

    /**
     * @param $login
     * @return User
     * @throws LogicException
     */
    public function getUserByLogin($login)
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(array('login' => $login));

        if (!$user || !$user instanceof User) {

            throw new LogicException(null, LogicException::USER_BY_LOGIN_NOT_FOUND);
        }

        return $user;
    }

    /**
     * @param $login
     * @return User
     * @throws LogicException
     */
    public function selectUserByLogin($login)
    {
        return $this->getUserByLogin($login);
    }

    /**
     * @param User $user
     * @param array $params [
     *  'avatar' => [
     *      'tmp_name' => 'C:\wamp64\tmp\php61AD.tmp',
     *      'name' => 'file.jpg',
     *      'type' => 'image/jpeg',
     *      'size' => 9232,
     *      'error' => 0,
     *  ]
     * ]
     * @return User
     * @throws \Exception
     */
    public function editUser(User $user, array $params = [])
    {
        if (isset($params['avatar'])) {

            if ($user->existAvatar()) {
                $avatar = $user->getAvatar();
                $user->removeAvatar();

                $this->fileManager->delete($avatar->getId());

                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }

            $file = $this->fileManager->write([
                'file_source' => $params['avatar']['tmp_name'],
                'file_name' => $params['avatar']['name'],
                'file_type' => $params['avatar']['type'],
                'file_size' => $params['avatar']['size'],
                'path' => User::PATH_AVATAR_FILES,
            ]);
            $user->setAvatar($file);

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return $user;
    }

    /**
     * @param $email_value
     * @return UserInterface|null
     */
    public function getUserByEmailVerifiedByValue($email_value)
    {
        /** @var EmailInterface $email */
        $email = $this->baseEmailManager->getEmailVerifiedByValue($email_value);

        if ($email !== null) {

            return $this->getUserByEmail($email);
        }

        return null;
    }


    /**
     * @param $param
     * @return User|null
     * @throws \Exception
     */
    public function getUserByLoginOrEmail($param)
    {
        try {
            $user = $this->getUserByLogin($param);
            return $user;
        }
        catch (\Throwable $t) {
            $user = $this->selectUserByEmail($param);
            return $user;
        }
    }

    /**
     * @param $email_value
     * @return null|object
     * @TODO Остался только в тестах (нужно убрать)
     */
    public function selectEmailByEmailValue($email_value)
    {
        $email = $this->entityManager
            ->getRepository(Email::class)
            ->findOneBy(['email' => strtolower($email_value)]);

        return $email;
    }

    /**
     * @param string $email_value
     * @return User
     * @throws \Exception
     */
    public function selectUserByEmail($email_value)
    {
        // Get Email object
        $email = $this->entityManager
            ->getRepository(Email::class)
            ->findOneBy(['email' => strtolower($email_value), 'isVerified' => true]);
        if(!$email || !$email instanceof Email) {

            throw new \Exception(self::ERROR_EMAIL_NOT_FOUND);
        }
        // Get User object
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('email' => $email));

        if(!$user || !$user instanceof User){

            throw new \Exception(self::ERROR_USER_WITH_SUCH_EMAIL_NOT_FOUND);
        }

        return $user;
    }

    /**
     * @param User              $user
     * @param CodeOperationType $codeOperationType
     * @return mixed
     * @see custom CodeRepository
     * @TODO Повторяется в CodeManager. Проверить, можно ли убрать отсюда и использовать только из CodeManager.
     */
    public function getLastUserCodeByOperationType(User $user, CodeOperationType $codeOperationType)
    {
        $code = $this->entityManager->getRepository(Code::class)
            ->findOneBy(array('user' => $user, 'codeOperationType' => $codeOperationType),
                        array('id' => 'DESC'));

        return $code;
    }

    /**
     * @param User $user
     * @param CodeOperationType $codeOperationType
     * @return null|int
     * @see custom CodeRepository
     */
    public function getCountUserCodesByOperationType(User $user, CodeOperationType $codeOperationType)
    {
        $code_count = $this->entityManager->getRepository(Code::class)
            ->countUserCodesByOperationType($user, $codeOperationType);

        return $code_count;
    }

    /**
     * Умное извлечение данных из объекта (отсекаются ненужные данные)
     *
     * @param $user
     * @return null
     */
    public function extractUser($user)
    {
        if (!$user || !($user instanceof User)) {

            return null;
        }

        return $this->userHydrator->extract($user);
    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserOutputForSingle(User $user)
    {
        $userOutput = [];

        $userOutput['id']               = $user->getId();
        $userOutput['login']            = $user->getLogin();
        $userOutput['email']            = $user->getEmail()->getEmail();
        $userOutput['phone']            = $user->getPhone() ? '+'.$user->getPhone()->getPhoneNumber() : null;
        $userOutput['created']          = $user->getCreated()->format('d.m.Y');
        $userOutput['isBanned']         = $user->getIsBanned();

        $userOutput['roles'] = [];
        /** @var \Application\Entity\Role $role */
        foreach ($user->getRoles() as $role) {
            $userOutput['roles'][]      = $role->getName();
        }
        $userOutput['avatar_id'] = $user->existAvatar() ? $user->getAvatar()->getId() : null;


        return $userOutput;
    }

    /**
     * @param $id
     * @return User
     */
    public function getUserByAvatarId($id)
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->selectUsersByAvatarId((int) $id);

        return $user;
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function getUserCollection(array $params) {
        try {
            // Get all users with sort and filter params
            $users = $this->entityManager->getRepository(User::class)
                ->selectSortedUser($params);

        } catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        $userOutput = [];
        /** @var User $user */
        foreach ($users as $user) {
            $userOutput[] = $this->getUserOutputForSingle($user);
        }

        return ['users' => $userOutput, 'paginator' => $users->getPages()];
    }

    /**
     * @param User $user
     * @return array|null
     */
    public function getUsersPaymentMethodOutput(User $user)
    {
        if (!$user || !($user instanceof User) ) {

            return null;
        }

        $payment_methods_array = [];

        /** @var CivilLawSubject $civilLawSubject */
        foreach ($user->getCivilLawSubjects() as $civilLawSubject) {
            if ($civilLawSubject->getIsPattern()) {
                /** @var PaymentMethod $paymentMethod */
                foreach ($civilLawSubject->getPaymentMethods() as $paymentMethod) {
                    // Чтобы методы не повторялись, проверяем на наличие ключа в итоговом массиве
                    if ($paymentMethod->getIsPattern() && $paymentMethod->getPaymentMethodBankTransfer() !== null) {
                        $payment_methods_array[] = [
                            'id' => $paymentMethod->getId(),
                            'civil_law_subject_id' => $civilLawSubject->getId(),
                            'type' => PaymentMethodType::BANK_TRANSFER,
                            'name' => $paymentMethod->getPaymentMethodBankTransfer()->getName()
                        ];
                    }
                }
            }
        }

        return $payment_methods_array;
    }

    /**
     * @param UserInterface $user
     * @param CodeOperationType $codeOperationType
     * @return mixed
     */
    public function getUserCodesByOperationType(UserInterface $user, CodeOperationType $codeOperationType)
    {
        $codes = $this->entityManager->getRepository(Code::class)
            ->selectAllUserCodesByOperationType($user, $codeOperationType);

        return $codes;
    }
}