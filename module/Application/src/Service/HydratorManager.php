<?php
namespace Application\Service;

use Zend\Hydrator\ClassMethods;

class HydratorManager
{
    /**
     * Hydrator
     * @var \Zend\Hydrator\ClassMethods
     */
    protected $hydrator;

    /**
     * Получение Hydrator
     * @return ClassMethods
     */
    public function getHydrator()
    {
        if ($this->hydrator === null) {
            $this->hydrator = new ClassMethods();
        }
        return $this->hydrator;
    }

    /**
     * Возвращает пропущенные обязательные ключи в массиве
     * @param  array $requiredKeys массив с обязательными ключами
     * @param  array $array массив в котором нужно проверить ключи
     * @return array массив из пропущенных ключей
     */
    protected function checkKeyRequired(array $requiredKeys = [], array $array = [])
    {
        if (\count($array) === 0) {
            return $requiredKeys;
        }

        $keysCheck = array_keys($array);
        $missKeys = array_filter($requiredKeys, function($key) use ($keysCheck) {
            return !in_array($key, $keysCheck);
        });

        return $missKeys;
    }

    /**
     * Сеттит данные массива в объект entity
     * @param array $data массив с данными
     * @param object $entityObject объект в который нужно засеттить данные
     * @return object
     */
    protected function hydrate($data, $entityObject)
    {
        return $this->getHydrator()->hydrate($data, $entityObject);
    }

    /**
     * Получение данных из объекта в масссив
     *
     * @param $object
     * @return array|null
     */
    public function extract($object)
    {
        if (!\is_object($object)) {
            return null;
        }
        return $this->getHydrator()->extract($object);
    }

    /**
     * Преобразование объектов в массиве в массивы
     * @param $data
     * @return array
     */
    public function extractSetOfObjects($data)
    {
        if (\is_array($data)) {
            foreach ($data as $key => $val) {
                if (\is_object($val)) {
                    $data[$key] = $this->extract($val);
                } elseif (\is_array($val)) {
                    $data[$key] = $this->extractSetOfObjects($val);
                }
            }
        } elseif (\is_object($data)) {
            $data = $this->extract($data);
        }

        return $data;
    }
}