<?php
namespace Application\Service\CivilLawSubject;

use Application\Entity\CivilLawSubject;
use Application\Entity\SoleProprietor;
use Application\Form\SoleProprietorForm;
use Core\Exception\LogicException;
use Application\Entity\User;
use \Doctrine\ORM\EntityManager;
use Application\Service\CivilLawSubject\Base\CivilLawSubjectChildManager;

/**
 * Class SoleProprietorManager
 * @package Application\Service\CivilLawSubject
 */
class SoleProprietorManager extends CivilLawSubjectChildManager
{

    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * SoleProprietorManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param SoleProprietor $soleProprietor
     * @return array|null
     */
    public function getSoleProprietorOutput(SoleProprietor $soleProprietor)
    {
        if (null === $soleProprietor || !$soleProprietor instanceof SoleProprietor) {

            return null;
        }

        $sole_proprietor = [];

        $sole_proprietor['id'] = $soleProprietor->getId();
        $sole_proprietor['name'] = $soleProprietor->getName();
        $sole_proprietor['first_name'] = $soleProprietor->getFirstName();
        $sole_proprietor['secondary_name'] = $soleProprietor->getSecondaryName();
        $sole_proprietor['last_name'] = $soleProprietor->getLastName();
        $sole_proprietor['birth_date'] = $soleProprietor->getBirthDate()->format('d.m.Y');
        $sole_proprietor['inn'] = $soleProprietor->getInn();
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $soleProprietor->getCivilLawSubject();
        $sole_proprietor['civil_law_subject_id'] = $civilLawSubject->getId();
        $sole_proprietor['nds'] = $this->getNdsTypeOutput($civilLawSubject);
        $sole_proprietor['payment_methods'] = $this->getPaymentMethodCollectionOutput($civilLawSubject);

        return $sole_proprietor;
    }

    /**
     * @param $soleProprietors
     * @return array
     */
    public function extractSoleProprietorCollectionOutput($soleProprietors): array
    {
        $soleProprietorsOutput = [];
        /** @var SoleProprietor $soleProprietor */
        foreach ($soleProprietors as $soleProprietor) {
            $soleProprietorsOutput[] = $this->getSoleProprietorOutput($soleProprietor);
        }

        return $soleProprietorsOutput;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function get($id)
    {
        return $this->entityManager->getRepository(SoleProprietor::class)->find($id);
    }
    /**
     * @param $id
     * @return array
     */
    public function getAll()
    {
        return $this->entityManager->getRepository(SoleProprietor::class)->findAll();
    }
    /**
     * @param int $id
     * @return mixed
     */
    public function getBasedOnPattern(int $id)
    {
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->getSoleProprietorBasedOnPatternById($id);

        return $soleProprietor;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getSoleProprietorById(int $id)
    {
        return $this->getBasedOnPattern($id);
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @param array $params
     * @return SoleProprietor|object
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(CivilLawSubject $civilLawSubject, array $params = [])
    {
        $missKeys = $this->checkKeyRequired([
            'name',
            'first_name',
            'secondary_name',
            'last_name',
            'birth_date',
            'inn'
        ], $params);

        if (count($missKeys)) {

            throw new LogicException('Missing required keys: '.implode(", ", $missKeys));
        }

        if (!$params['birth_date'] instanceof \DateTime) {
            $params['birth_date'] = \DateTime::createFromFormat('d.m.Y', $params['birth_date']);
        }

        /** @var $soleProprietor $soleProprietor */
        $soleProprietor = new SoleProprietor();
        $soleProprietor->setName($params['name']);
        $soleProprietor->setFirstName($params['first_name']);
        $soleProprietor->setSecondaryName($params['secondary_name']);
        $soleProprietor->setLastName($params['last_name']);
        $soleProprietor->setBirthDate($params['birth_date']);
        $soleProprietor->setInn($params['inn']);
        #$soleProprietor = $this->hydrate($params, $soleProprietor);

        $soleProprietor->setCivilLawSubject($civilLawSubject);
        $civilLawSubject->setSoleProprietor($soleProprietor);

        $this->entityManager->persist($soleProprietor);
        $this->entityManager->flush();

        return $soleProprietor;
    }

    /**
     * @param SoleProprietor $soleProprietor
     * @param array $params
     * @return SoleProprietor|object
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(SoleProprietor $soleProprietor, array $params = [])
    {
        $missKeys = $this->checkKeyRequired([
            'name',
            'first_name',
            'secondary_name',
            'last_name',
            'birth_date',
            'inn'
        ], $params);

        if (count($missKeys)) {

            throw new LogicException('Missing required keys: '.implode(", ", $missKeys));
        }

        $soleProprietor->setName($params['name']);
        $soleProprietor->setFirstName($params['first_name']);
        $soleProprietor->setSecondaryName($params['secondary_name']);
        $soleProprietor->setLastName($params['last_name']);
        $soleProprietor->setBirthDate(\DateTime::createFromFormat('d.m.Y', $params['birth_date']));
        $soleProprietor->setInn($params['inn']);
        #$soleProprietor = $this->hydrate($params, $soleProprietor);

        $this->entityManager->persist($soleProprietor);
        $this->entityManager->flush();

        return $soleProprietor;
    }

    /**
     * @param SoleProprietor $soleProprietor
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(SoleProprietor $soleProprietor)
    {
        $this->entityManager->remove($soleProprietor);
        $this->entityManager->remove($soleProprietor->getCivilLawSubject());

        $this->entityManager->flush();
    }

    /**
     * @param SoleProprietor $soleProprietor
     * @param CivilLawSubject $civilLawSubject
     * @return SoleProprietor|object
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clone(SoleProprietor $soleProprietor, CivilLawSubject $civilLawSubject)
    {
        $params = [
            'name'              => $soleProprietor->getName(),
            'first_name'        => $soleProprietor->getFirstName(),
            'secondary_name'    => $soleProprietor->getSecondaryName(),
            'last_name'         => $soleProprietor->getLastName(),
            'birth_date'        => $soleProprietor->getBirthDate(),
            'inn'               => $soleProprietor->getInn(),
            'civil_law_subject' => $civilLawSubject
        ];

        return $this->create($civilLawSubject, $params);
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @return null
     */
    public function getSoleProprietorCollection(User $user, array $params, bool $isOperator)
    {
        if($isOperator) {
            $soleProprietors = $this->entityManager->getRepository(CivilLawSubject::class)
                ->getSoleProprietors($params);
        } elseif ($user && $user instanceof User) {
            $soleProprietors = $this->entityManager->getRepository(CivilLawSubject::class)
                ->getSoleProprietorsByUserId($user->getId(), $params);
        } else {

            $soleProprietors = null;
        }

        return $soleProprietors;
    }

    /**
     * @param SoleProprietorForm $form
     * @param SoleProprietor $soleProprietor
     * @return SoleProprietorForm
     */
    public function setFormData(SoleProprietorForm $form, SoleProprietor $soleProprietor): SoleProprietorForm
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $soleProprietor->getCivilLawSubject();

        $form->setData([
            'name'              => $soleProprietor->getName(),
            'first_name'        => $soleProprietor->getFirstName(),
            'secondary_name'    => $soleProprietor->getSecondaryName(),
            'last_name'         => $soleProprietor->getLastName(),
            'birth_date'        => $soleProprietor->getBirthDate()->format('d.m.Y'),
            'inn'               => $soleProprietor->getInn(),
            'nds_type'          => $civilLawSubject->getNdsType() ? $civilLawSubject->getNdsType()->getType() : null,
        ]);

        return $form;
    }

    /**
     * @param User $user
     * @return mixed|null
     */
    public function getAllowedSoleProprietorsByUser(User $user)
    {
        $allowedSoleProprietors = null;
        if ($user && $user instanceof User){
            $allowedSoleProprietors = $this->entityManager->getRepository(CivilLawSubject::class)
                ->getSoleProprietorsByUserId($user->getId());
        }

        if ($allowedSoleProprietors && !empty($allowedSoleProprietors)) {

            return $allowedSoleProprietors;
        }

        return null;
    }
}