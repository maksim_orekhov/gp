<?php
namespace Application\Service\CivilLawSubject;

use Application\Entity\CivilLawSubject;
use Application\Service\CivilLawSubject\Base\CivilLawSubjectChildManager;
use Application\Hydrator\LegalEntityHydrator;
use Application\Entity\LegalEntity;
use Application\Entity\User;
use Application\Form\LegalEntityForm;
use Core\Exception\LogicException;

/**
 * Class LegalEntityManager
 * @package Application\Service\CivilLawSubject
 */
class LegalEntityManager extends CivilLawSubjectChildManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var LegalEntityManager
     */
    private $legalEntityDocumentManager;

    /**
     * @var LegalEntityHydrator
     */
    private $legalEntityHydrator;

    /**
     * LegalEntityManager constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param LegalEntityDocumentManager $legalEntityDocumentManager
     * @param LegalEntityHydrator $legalEntityHydrator
     */
    public function __construct(
        \Doctrine\ORM\EntityManager $entityManager,
        LegalEntityDocumentManager $legalEntityDocumentManager,
        LegalEntityHydrator $legalEntityHydrator)
    {
        $this->entityManager = $entityManager;
        $this->legalEntityDocumentManager = $legalEntityDocumentManager;
        $this->legalEntityHydrator = $legalEntityHydrator;
    }

    /**
     * Умное извлечение коллекции (отсекаются ненужные данные)
     *
     * @param $legalEntities
     * @return array
     */
    public function extractLegalEntityCollection($legalEntities)
    {
        $legalEntitiesOutput = [];
        foreach ($legalEntities as $legalEntity) {

            $legalEntitiesOutput[] = $this->getLegalEntityOutput($legalEntity);
        }

        return $legalEntitiesOutput;
    }

    /**
     * Умное извлечение объекта (отсекаются ненужные данные)
     *
     * @param $legalEntity
     * @return array|null
     */
    public function extractLegalEntity($legalEntity)
    {
        if (!$legalEntity || !($legalEntity instanceof LegalEntity)) {

            return null;
        }

        return $this->legalEntityHydrator->extract($legalEntity);
    }

    /**
     * @param $id
     * @return LegalEntity|null
     */
    public function get($id)
    {
        return $this->entityManager->getRepository(LegalEntity::class)->find($id);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getBasedOnPattern(int $id)
    {
        $legalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->getLegalEntityBasedOnPatternById($id);

        return $legalEntity;
    }

    /**
     * @return LegalEntityDocumentManager|LegalEntityManager
     */
    public function getDocumentManager()
    {
        return $this->legalEntityDocumentManager;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @param array $params [
     *  'name' => 'ООО Симпл Технолоджи',
     *  'legal_address' => 'г. Москва, ул. Авангардная, д. 3 пом. II офис 1407',
     *  'phone' => '+7-999-888-77-66',
     *  'inn' => '7743211967',
     *  'kpp' => '774301001',
     *  'ogrn' => '1177746547340',
     * ]
     * @return LegalEntity
     * @throws \Exception
     */
    public function create(CivilLawSubject $civilLawSubject, array $params = [])
    {
        $missKeys = $this->checkKeyRequired([
            'name',
            'legal_address',
            'inn',
            'kpp',
        ], $params);

        if (count($missKeys)) {

            throw new LogicException('Missing required keys: '.implode(", ", $missKeys));
        }

        /** @var LegalEntity $legal */
        $legal = new LegalEntity();
        $legal = $this->hydrate($params, $legal);

        $legal->setCivilLawSubject($civilLawSubject);
        $civilLawSubject->setLegalEntity($legal);

        $this->entityManager->persist($legal);
        $this->entityManager->flush();

        return $legal;
    }

    /**
     * @param LegalEntity $legal
     * @param array $params [
     *  'name' => 'ООО Симпл Технолоджи',
     *  'legal_address' => 'г. Москва, ул. Авангардная, д. 3 пом. II офис 1407',
     *  'phone' => '+7-999-888-77-66',
     *  'inn' => '7743211967',
     *  'kpp' => '774301001',
     *  'ogrn' => '1177746547340',
     * ]
     * @return object
     * @throws \Exception
     */
    public function update(LegalEntity $legal, $params = [])
    {
        $missKeys = $this->checkKeyRequired([
            'name',
            'legal_address',
            'inn',
            'kpp',
        ], $params);
        if (count($missKeys)) {

            throw new LogicException('Missing required keys: '.implode(", ", $missKeys));
        }

        // @TODO Заменить на обычное присваивание
        $legal = $this->hydrate($params, $legal);

        $this->entityManager->persist($legal);
        $this->entityManager->flush();

        return $legal;
    }

    /**
     * @param LegalEntity $legal
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(LegalEntity $legal)
    {
        foreach ($legal->getLegalEntityDocument() as $document) {
            $this->legalEntityDocumentManager->remove($document);
        }

        $this->entityManager->remove($legal);
        $this->entityManager->flush();
    }

    /**
     * Получение расширенных документов с группирокой по типам
     *
     * @param LegalEntity $legalEntity
     * @return array
     * @throws \Exception
     */
    public function getDocuments(LegalEntity $legalEntity)
    {
        $documents = $legalEntity->getLegalEntityDocument();
        return $this->legalEntityDocumentManager->getGroupByType($documents);
    }

    /**
     * Клонирование юр лица
     *
     * @param LegalEntity $legalEntity
     * @param CivilLawSubject $civilLawSubject
     * @return LegalEntity
     * @throws \Exception
     */
    public function clone(LegalEntity $legalEntity, CivilLawSubject $civilLawSubject)
    {
        $paramsLegalEntity = $this->extract($legalEntity);

        $params = [
            'name' => $paramsLegalEntity['name'],
            'legal_address' => $paramsLegalEntity['legal_address'],
            'inn' => $paramsLegalEntity['inn'],
            'kpp' => $paramsLegalEntity['kpp'],
            'civil_law_subject' => $civilLawSubject,
        ];

        $newLegalEntity = $this->create($civilLawSubject, $params);
        foreach ($legalEntity->getLegalEntityDocument() as $document) {
            // TODO Добавить проверку что документ потвержден
            if (!$document->isActive()) {
                continue;
            }

            $this->legalEntityDocumentManager->clone($document, $newLegalEntity);
        }

        return $newLegalEntity;
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @return null
     */
    public function getCollectionLegalEntity(User $user, array $params, bool $isOperator)
    {
        if($isOperator) {
            $legalEntities = $this->entityManager->getRepository(CivilLawSubject::class)
                ->getLegalEntities($params);
        } elseif ($user && $user instanceof User) {
            $legalEntities = $this->entityManager->getRepository(CivilLawSubject::class)
                ->getLegalEntitiesByUserId($user->getId(), $params);
        } else {

            $legalEntities = null;
        }

        return $legalEntities;
    }

    /**
     * @param $idLegalEntity
     * @return LegalEntity|null
     * @throws \Exception
     *
     * @TODO Разобраться где используется и почему такое незавние?!
     */
    public function getLegalEntityPersonById($idLegalEntity)
    {
        if ($idLegalEntity === -1 || $idLegalEntity === 0) {
            throw new \Exception('Bad parameter idLegalEntity');
        }

        $legalEntity = $this->get($idLegalEntity);

        if ($legalEntity === null) {
            throw new \Exception('Not found Legal Entity by id');
        }

        return $legalEntity;
    }

    /**
     * @param $idLegalEntity
     * @return mixed
     * @throws \Exception
     */
    public function getLegalEntityById($idLegalEntity)
    {
        $legalEntity = $this->getBasedOnPattern($idLegalEntity);

        return $legalEntity;
    }

    /**
     * @param LegalEntityForm $form
     * @param LegalEntity $legalEntity
     * @return LegalEntityForm
     */
    public function setFormData(LegalEntityForm $form, LegalEntity $legalEntity)
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $legalEntity->getCivilLawSubject();

        $form->setData([
            'name'          => $legalEntity->getName(),
            'legal_address' => $legalEntity->getLegalAddress(),
            'phone'         => $legalEntity->getPhone(),
            'inn'           => $legalEntity->getInn(),
            'kpp'           => $legalEntity->getKpp(),
            'ogrn'          => $legalEntity->getOgrn(),
            'nds_type'   => $civilLawSubject->getNdsType() ? $civilLawSubject->getNdsType()->getType() : null,
        ]);

        return $form;
    }

    /**
     * @param User $user
     * @return mixed|null
     */
    public function getAllowedLegalEntityByUser(User $user)
    {
        $allowedLegalEntities = null;
        if ( $user && $user instanceof User){
            $allowedLegalEntities = $this->entityManager->getRepository(CivilLawSubject::class)
                ->getLegalEntitiesByUserId($user->getId());
        }

        return $allowedLegalEntities && !empty($allowedLegalEntities) ? $allowedLegalEntities : null;
    }

    /**
     * @param LegalEntity $legalEntity
     * @return array
     */
    public function getLegalEntityOutput(LegalEntity $legalEntity): array
    {
        $legalEntityOutput = [];

        $legalEntityOutput['id']            = $legalEntity->getId();
        $legalEntityOutput['name']          = $legalEntity->getName();
        $legalEntityOutput['legal_address'] = $legalEntity->getLegalAddress();
        $legalEntityOutput['phone']         = $legalEntity->getPhone();
        $legalEntityOutput['inn']           = $legalEntity->getInn();
        $legalEntityOutput['kpp']           = $legalEntity->getKpp();
        $legalEntityOutput['ogrn']          = $legalEntity->getOgrn();
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $legalEntity->getCivilLawSubject();
        $legalEntityOutput['civil_law_subject_id'] = $civilLawSubject->getId();
        $legalEntityOutput['nds']           = $this->getNdsTypeOutput($civilLawSubject);
        $legalEntityOutput['payment_methods'] = $this->getPaymentMethodCollectionOutput($civilLawSubject);
        $legalEntityOutput['documents'] = null;

        $documents = $this->legalEntityDocumentManager->getCollectionDocumentByLegalEntity($legalEntity, []);
        $documentsOutput = $this->legalEntityDocumentManager->extractLegalEntityDocumentCollection($documents);

        if (!empty($documentsOutput)) {
            $legalEntityOutput['documents'] = $documentsOutput;
        }

        return $legalEntityOutput;
    }
}