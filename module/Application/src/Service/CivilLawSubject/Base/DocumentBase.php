<?php
namespace Application\Service\CivilLawSubject\Base;

use Application\Service\HydratorManager;

class DocumentBase extends HydratorManager
{
    /**
     * Переопределяем для гидрирование даты
     * Вставляет данные массива в объект entity
     * @param array $data
     * @param $entityObject
     * @return object
     */
    protected function hydrate($data, $entityObject)
    {
        foreach ($data as $key => $item) {
            if (strpos($key, 'date') !== false) {
                $this->getHydrator()->addStrategy($key, new \Application\Hydrator\Strategy\DateTimeStrategy());
            }
        }

        return $this->getHydrator()->hydrate($data, $entityObject);
    }

    /**
     * Получение информации о владельце документов
     * @return array
     */
    public static function getOwnerData()
    {
        return [
            'owner' => '',
            'document' => '',
            'document_type' => '',
        ];
    }

    /**
     * Получение списка доступных документов
     * @return array
     */
    public static function getTypes()
    {
        return [];
    }

    /**
     * Получение типа документа
     * @param $class_name
     * @return string
     * @throws \Exception
     */
    public static function getType($class_name)
    {
        $types = static::getTypes();
        if (!isset($types[$class_name])) {
            throw new \Exception('Not found document type');
        }

        return $types[$class_name];
    }

    /**
     * Получение класса документа
     * @param $type
     * @return string
     * @throws \Exception
     */
    public static function getClassByType($type)
    {
        $types = array_flip(static::getTypes());
        if (!isset($types[$type])) {
            throw new \Exception('Not found document type');
        }

        return $types[$type];
    }

    /**
     * Создание документа
     * @param $person
     * @param $class_name_document
     * @param array $params
     * @param bool $flag_is_clone
     * @return object
     * @throws \Exception
     */
    public function create($person, $class_name_document, $params = [], $flag_is_clone = false)
    {
        if (!$this->isCorrectOwner($person)) {
            throw new \Exception('expected '.$this->getOwnerClass().' received: '.get_class($person));
        }

        $missKeys = $this->checkKeyRequiredDefault($class_name_document, $params);
        if ($flag_is_clone) {
            if (($key = array_search('file', $missKeys)) !== false) {
                unset($missKeys[$key]);
            }
        }
        if (count($missKeys)) {
            throw new \Exception('Missing required keys: '.implode(", ", $missKeys));
        }

        $type = static::getType($class_name_document);
        $documentType = $this->entityManager
            ->getRepository($this->getDocumentTypeClass())
            ->findOneByName($type);

        $documentClassName = $this->getDocumentClass();
        $document = new $documentClassName();
        $document->{$this->getStringMethod('owner')}($person);
        $document->{$this->getStringMethod('document_type')}($documentType);

        $person->{$this->getStringMethod('document', 'add')}($document);

        $documentReal = new $class_name_document;
        $documentReal->{$this->getStringMethod('document')}($document);
        $documentReal = $this->hydrate($params, $documentReal);

        $this->setRealDocument($document, $documentReal);

        $this->entityManager->persist($documentType);
        $this->entityManager->persist($document);
        $this->entityManager->persist($documentReal);
        $this->entityManager->flush();

        if ($flag_is_clone === false) {
            $this->setActive($document);
        }

        if (isset($params['file'])) {
            $this->writeFile($document, $params['file']);
        }

        return $document;
    }

    /**
     * Изменение документа
     * @param $document
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function update($document, $params = [])
    {
        if (!$this->isCorrectDocument($document)) {
            throw new \Exception('expected '.$this->getDocumentClass().' received: '.get_class($document));
        }

        $extendedDocument = $this->get($document);

        $excludeFields = ['file'];
        $require_fields = $this->getRequiredFields($extendedDocument, [], $excludeFields);
        $missKeys = $this->checkKeyRequired($require_fields, $params);
        if (count($missKeys)) {
            throw new \Exception('Missing required keys: '.implode(", ", $missKeys));
        }

        $extendedDocument = $this->hydrate($params, $extendedDocument);
        if (isset($params['file'])) {
            $this->removeFiles($document);
        }

        $this->entityManager->persist($extendedDocument);
        $this->entityManager->flush();

        if (isset($params['file'])) {
            $this->writeFile($document, $params['file']);
        }

        return $document;
    }

    /**
     * Удаление документа
     * @param $document
     * @throws \Exception
     */
    public function remove($document)
    {
        if (!$this->isCorrectDocument($document)) {
            throw new \Exception('expected '.$this->getDocumentClass().' received: '.get_class($document));
        }

        $this->removeFiles($document);
        $documentReal = $this->get($document);

        $this->entityManager->remove($documentReal);
        $this->entityManager->remove($document);
        $this->entityManager->flush();
    }

    /**
     * Получение документа который расширяет базовый документ
     * @param $document
     * @return mixed
     * @throws \Exception
     */
    public function get($document)
    {
        if (!$this->isCorrectDocument($document)) {
            throw new \Exception('expected '.$this->getDocumentClass().' received: '.get_class($document));
        }

        $documentReal = null;
        $types = static::getTypes();
        foreach ($types as $className => $name) {
            $method = 'get'.$this->getOnlyNameClass($className);
            if ($document->{$method}() !== null) {
                $documentReal = $document->{$method}();
            }
        }

        return $documentReal;
    }

    /**
     * @return string
     */
    public function getMainUploadFolder()
    {
        return $this->config['file-management']['main_upload_folder'];
    }

    /**
     * Клонирование документа
     * @param object $documentBase
     * @param object|null $owner
     * @return object
     * @throws \Exception
     */
    public function clone($documentBase, $owner = null)
    {
        if ($owner === null) {
            $owner = $documentBase->{$this->getStringMethod('owner', 'get')}();
        }
        $document = $this->get($documentBase);
        $className = get_class($document);

        $params = $this->extract($document);
        unset($params['id']);
        unset($params['natural_person_document']);
        unset($params['legal_entity_document']);
        unset($params['files']);

//        if (method_exists($document, 'getFiles')) {
//            foreach ($document->getFiles() as $file) {
//                $params['file'] = [
//                    'tmp_name'  => "./".$this->getMainUploadFolder().$className::PATH_FILES.'/'.$file->getName(),
//                    'name'      => $file->getOriginName(),
//                    'type' => $file->getType(),
//                    'size' => $file->getSize(),
//                    'path'      => $className::PATH_FILES,
//                ];
//            }
//        }

        return $this->create($owner, $className, $params, $flag_is_clone = true);
    }

    /**
     * Получение расширенных документов с группирокой по типам
     * @param array $documents
     * @return array
     * @throws \Exception
     */
    public function getGroupByType($documents = [])
    {
        $result = [];

        foreach ($documents as $document) {
            $extendedDocument = $this->get($document);
            $key = static::getType(get_class($extendedDocument));

            $result[$key][] = $extendedDocument;
        }

        return $result;
    }

    /**
     * Сохранение файл к документу
     * @param $document
     * @param $paramFile
     * @return mixed
     * @throws \Exception
     */
    public function writeFile($document, $paramFile)
    {
        if (!$this->isCorrectDocument($document)) {
            throw new \Exception('expected '.$this->getDocumentClass().' received: '.get_class($document));
        }

        $documentReal = $this->get($document);

        $file = $this->fileManager->write([
            'file_source' => $paramFile['tmp_name'],
            'file_name' => $paramFile['name'],
            'file_type' => $paramFile['type'],
            'file_size' => $paramFile['size'],
            'path' => $documentReal::PATH_FILES,
        ]);

        $documentReal->addFile($file);

        $this->entityManager->persist($documentReal);
        $this->entityManager->flush();

        return $documentReal;
    }

    /**
     * Удаление прикрепленных файлов в документе
     * @param $document
     * @throws \Exception
     */
    public function removeFiles($document)
    {
        if (!$this->isCorrectDocument($document)) {
            throw new \Exception('expected '.$this->getDocumentClass().' received: '.get_class($document));
        }

        $documentReal = $this->get($document);

        if (method_exists($documentReal, 'getFiles')) {
            foreach ($documentReal->getFiles() as $file) {
                $documentReal->removeFile($file);
                $this->fileManager->delete($file->getId());
            }
            $this->entityManager->remove($documentReal);
            $this->entityManager->flush();
        }
    }

    /**
     * Установка активности документа
     * @param $document
     * @return mixed
     * @throws \Exception
     */
    public function setActive($document)
    {
        if (!$this->isCorrectDocument($document)) {
            throw new \Exception('expected '.$this->getDocumentClass().' received: '.get_class($document));
        }

        $person = $document->{$this->getStringMethod('owner', 'get')}();

        $documents = $this->entityManager
            ->getRepository($this->getOwnerClass())
            ->getDocuments($person, $document->{$this->getStringMethod('document_type', 'get')}()->getName());

        foreach ($documents as $documentOff) {
            $documentOff->setIsActive(false);
            $this->entityManager->persist($documentOff);
        }

        $document->setIsActive(true);

        $this->entityManager->persist($document);
        $this->entityManager->flush();

        return $document;
    }

    /**
     * Сеттит в static::getOwnerData()['document'] расширяющий его класс
     * Например $document->setNaturalPersonPassoprt($passport);
     *
     * @param $document
     * @param $documentReal
     * @return mixed
     * @throws \Exception
     */
    protected function setRealDocument($document, $documentReal)
    {
        $method = 'set'.$this->getOnlyNameClass($documentReal);
        $document->{$method}($documentReal);

        return $document;
    }

    /**
     * Получение только конечного названия класса
     * @param $path_class
     * @return string
     */
    protected function getOnlyNameClass($path_class)
    {
        if (is_object($path_class)) {
            $path_class = get_class($path_class);
        }

        $pathName = explode('\\',$path_class);

        return $pathName[count($pathName) - 1];
    }

    /**
     * Получение класса владельца
     * @return string
     */
    protected function getOwnerClass()
    {
        return static::getOwnerData()['owner'];
    }

    /**
     * Получение класса документа
     * @return string
     */
    protected function getDocumentClass()
    {
        return static::getOwnerData()['document'];
    }

    /**
     * Получение класса типа документа
     * @return string
     */
    protected function getDocumentTypeClass()
    {
        return static::getOwnerData()['document_type'];
    }

    /**
     * Получение строки для вызова, например сеттер
     * @return string
     */
    protected function getStringMethod($classKey, $prefix = 'set')
    {
        $className = $this->getOnlyNameClass(static::getOwnerData()[$classKey]);

        return $prefix.$className;
    }

    /**
     * Проверка на владелца документа
     * @param $owner
     * @return bool
     */
    protected function isCorrectOwner($owner)
    {
        return $this->getOwnerClass() === get_class($owner);
    }

    /**
     * Проверка документа
     * @param $document
     * @return bool
     */
    protected function isCorrectDocument($document)
    {
        return $this->getDocumentClass() === get_class($document);
    }

    /**
     * Возвращает список обязательных полей
     * данные берутся из entity
     * Если есть метод getFiles в entity, то добавляется обязательное поле file
     * @param  string $entity_class_name
     * @param  array $union_fields
     * @param  array $exclude_fields
     * @return array
     */
    protected function getRequiredFields($entity_class_name, $union_fields = [], $exclude_fields = [])
    {
        if (is_object($entity_class_name)) {
            $entity_class_name = get_class($entity_class_name);
        }

        $fields = $this->entityManager->getClassMetadata($entity_class_name)->getColumnNames();
        $fieldIds = $this->entityManager->getClassMetadata($entity_class_name)->getIdentifierColumnNames();
        $result = array_diff($fields, $fieldIds);

        if (method_exists($entity_class_name, 'getFiles')) {
            $result[] = 'file';
        }

        if (count($union_fields)) {
            foreach ($union_fields as $field) {
                if (!in_array($field, $result)) {
                    $result[] = $field;
                }
            }
        }

        if (count($exclude_fields)) {
            $result = array_diff($result, $exclude_fields);
        }

        return $result;
    }

    /**
     * Возвращает пропущенные обязательные ключи в массиве сравнивает с доступными полями Entity
     * @param  string $entity_class_name
     * @param  array $params
     * @return array
     */
    protected function checkKeyRequiredDefault($entity_class_name, $params)
    {
        $require_fields = $this->getRequiredFields($entity_class_name);

        return $this->checkKeyRequired($require_fields, $params);
    }
}