<?php
namespace Application\Service\CivilLawSubject\Base;

use Application\Entity\CivilLawSubject;
use Application\Entity\CivilLawSubjectChildInterface;
use Application\Entity\LegalEntity;
use Application\Entity\NaturalPerson;
use Application\Entity\NdsType;
use Application\Entity\PaymentMethod;
use Application\Entity\SoleProprietor;
use Application\Service\HydratorManager;
use Doctrine\ORM\EntityManager;
use ModulePaymentOrder\Entity\PaymentMethodType;

/**
 * Class CivilLawSubjectChildManager
 * @package Application\Service\CivilLawSubject\Base
 */
abstract class CivilLawSubjectChildManager extends HydratorManager
{
    /**
     * @param CivilLawSubject $civilLawSubject
     * @return array
     */
    protected function getPaymentMethodCollectionOutput(CivilLawSubject $civilLawSubject): array
    {
        $payment_methods = [];
        if ($civilLawSubject->getPaymentMethods()) {
            /** @var PaymentMethod $paymentMethod */
            foreach($civilLawSubject->getPaymentMethods() as $paymentMethod) {

                if ($this->isPaymentMethodHasBankTransfer($paymentMethod)) {
                    $payment_methods[] = [
                        'id'    => $paymentMethod->getId(),
                        'type'  => PaymentMethodType::BANK_TRANSFER,
                        'name'  => $paymentMethod->getPaymentMethodBankTransfer()->getName()
                    ];
                }

                // @TODO Другие методы оплаты
            }
        }

        return $payment_methods;
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @return bool
     */
    protected function isPaymentMethodHasBankTransfer(PaymentMethod $paymentMethod): bool
    {
        return PaymentMethodType::BANK_TRANSFER === $paymentMethod->getPaymentMethodType()->getName()
            && null !== $paymentMethod->getPaymentMethodBankTransfer();
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @return array|null
     */
    public function getNdsTypeOutput(CivilLawSubject $civilLawSubject)
    {
        $nds_type = null;

        /** @var NdsType $ndsType */
        $ndsType = $civilLawSubject->getNdsType();

        if (null !== $ndsType) {
            $nds_type = [];
            $nds_type['type'] = (int)$ndsType->getType();
            $nds_type['name'] = $ndsType->getName();
        }

        return $nds_type;
    }

    /**
     * Преобразование объектов в массиве в массивы
     * @param $data
     * @return array
     */
    public function extractSetOfObjects($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                if (is_object($val)) {
                    $data[$key] = $this->extract($val);
                } elseif (is_array($val)) {
                    $data[$key] = $this->extractSetOfObjects($val);
                }
            }
        } elseif (is_object($data)) {
            $data = $this->extract($data);
        }

        return $data;
    }
}