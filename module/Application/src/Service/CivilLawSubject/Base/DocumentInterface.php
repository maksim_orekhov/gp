<?php

namespace Application\Service\CivilLawSubject\Base;

interface DocumentInterface
{
    /**
     * Получение информации о владельце документов
     * @return array
     */
    public static function getOwnerData();

    /**
     * Получение списка доступных документов
     * @return array
     */
    public static function getTypes();

    /**
     * Получение типа документа
     * @param $class_name
     * @return string
     * @throws \Exception
     */
    public static function getType($class_name);

    /**
     * Получение класса документа
     * @param $type
     * @return string
     * @throws \Exception
     */
    public static function getClassByType($type);

    /**
     * Создание документа
     * @param $person
     * @param $class_name_document
     * @param array $params
     * @param bool $flag_is_clone
     * @return object
     * @throws \Exception
     */
    public function create($person, $class_name_document, $params = [], $flag_is_clone = false);

    /**
     * Изменение документа
     * @param $document
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function update($document, $params = []);

    /**
     * Удаление документа
     * @param $document
     * @throws \Exception
     */
    public function remove($document);

    /**
     * Получение документа который расширяет базовый документ
     * @param $document
     * @return object
     * @throws \Exception
     */
    public function get($document);

    /**
     * @return string
     */
    public function getMainUploadFolder();

    /**
     * Клонирование документа
     * @param object $documentBase
     * @param object|null $owner
     * @return object
     */
    public function clone($documentBase, $owner = null);

    /**
     * Получение расширенных документов с группирокой по типам
     * @param array $documents
     * @return array
     */
    public function getGroupByType($documents = []);

    /**
     * Сохранение файл к документу
     * @param $document
     * @param $paramFile
     * @return mixed
     * @throws \Exception
     */
    public function writeFile($document, $paramFile);

    /**
     * Удаление прикрепленных файлов в документе
     * @param $document
     * @throws \Exception
     */
    public function removeFiles($document);

    /**
     * Установка активности документа
     * @param $document
     * @return mixed
     * @throws \Exception
     */
    public function setActive($document);
}