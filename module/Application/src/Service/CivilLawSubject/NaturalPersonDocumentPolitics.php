<?php

namespace Application\Service\CivilLawSubject;

use Application\Entity\NaturalPersonDocument;

/**
 * Class NaturalPersonDocumentPolitics
 * @package Application\Service\CivilLawSubject
 */
class NaturalPersonDocumentPolitics
{
    /**
     * @param NaturalPersonDocument $naturalPersonDocument
     * @return bool
     */
    public function isLock(NaturalPersonDocument $naturalPersonDocument)
    {
        if (!$naturalPersonDocument->getNaturalPerson()->getCivilLawSubject()->getIsPattern()){

            return true;
        }

        return false;
    }
}