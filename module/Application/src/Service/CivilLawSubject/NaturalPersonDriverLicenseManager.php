<?php
namespace Application\Service\CivilLawSubject;

use Application\Entity\NaturalPersonDriverLicense;
use Application\Entity\User;
use Doctrine\ORM\EntityManager;

class NaturalPersonDriverLicenseManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var
     */
    protected $config;

    /**
     * NaturalPersonDriverLicenseManager constructor.
     * @param EntityManager $entityManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->config = $config;
    }

    /**
     * @param $idDriverLicense
     * @return mixed
     * @throws \Exception
     */
    public function getDriverLicenseById($idDriverLicense)
    {
        if ($idDriverLicense === -1 || $idDriverLicense === 0) {

            throw new \Exception('Bad parameter idDriverLicense');
        }

        /** @var NaturalPersonDriverLicense $driverLicense */
        $driverLicense = $this->entityManager->getRepository(NaturalPersonDriverLicense::class)
            ->getNaturalPersonDriverLicenseBasedOnPatternById($idDriverLicense);

        if ($driverLicense === null) {

            throw new \Exception('Not found driver license by id');
        }
        return $driverLicense;
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @return array
     * @throws \Exception
     */
    public function getCollectionDriverLicense(User $user, array $params, bool $isOperator = false)
    {
        if($isOperator) {
            $collectionDriverLicense = $this->entityManager->getRepository(NaturalPersonDriverLicense::class)
                ->getDriverLicenseBased($params);
        } elseif ($user && $user instanceof User) {
            $collectionDriverLicense = $this->entityManager->getRepository(NaturalPersonDriverLicense::class)
                ->getDriverLicenseBasedByUserId($user->getId(), $params);
        } else {

            throw new \Exception('No data provided for getting driver license collection');
        }

        return $collectionDriverLicense;
    }

    /**
     * @param User $user
     * @return null
     */
    public function getAllowedDriverLicenseByUser(User $user){
        $allowedDriverLicenses = null;
        if ( $user && $user instanceof User){
            $allowedDriverLicenses = $this->entityManager->getRepository(NaturalPersonDriverLicense::class)
                ->getDriverLicenseBasedByUserId($user->getId());
        }

        return ($allowedDriverLicenses && !empty($allowedDriverLicenses)) ? $allowedDriverLicenses : null;
    }

    /**
     * @param NaturalPersonDriverLicense $driverLicense
     * @return array
     */
    public function getDriverLicenseOutput(NaturalPersonDriverLicense $driverLicense)
    {
        $driverLicensesOutput = [];

        $driverLicensesOutput['id']          = $driverLicense->getId();
        $driverLicensesOutput['date_issued'] = $driverLicense->getDateIssued()->format('d.m.Y');
        $driverLicensesOutput['files']       = null;

        if ( count($driverLicense->getFiles()) > 0){
            foreach ($driverLicense->getFiles() as $file){
                $driverLicensesOutput['files'][]=[
                    'civilId' => $driverLicense->getNaturalPersonDocument()->getNaturalPerson()->getCivilLawSubject()->getId(),
                    'id' => $file->getId(),
                    'name' => $file->getOriginName(),
                    'type' => substr($file->getOriginName(), strrpos($file->getOriginName(), '.') + 1),
                ];
            }
        }

        return $driverLicensesOutput;
    }

    /**
     * @param $driverLicenses
     * @return array
     */
    public function extractDriverLicenseCollection($driverLicenses)
    {
        $driverLicensesOutput = [];

        foreach ($driverLicenses as $driverLicense) {

            $driverLicensesOutput[] = $this->getDriverLicenseOutput($driverLicense);
        }

        return $driverLicensesOutput;
    }
}