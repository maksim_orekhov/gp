<?php
namespace Application\Service\CivilLawSubject;

use Application\Entity\LegalEntity;
use Application\Entity\LegalEntityDocument;
use Application\Service\CivilLawSubject\Base\DocumentBase;
use Application\Service\CivilLawSubject\Base\DocumentInterface;
use Application\Entity\User;

class LegalEntityDocumentManager extends DocumentBase implements DocumentInterface
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \ModuleFileManager\Service\FileManager
     */
    protected $fileManager;

    protected $config;

    public function __construct(
        \Doctrine\ORM\EntityManager $entityManager,
        \ModuleFileManager\Service\FileManager $fileManager,
        $config
    )
    {
        $this->entityManager = $entityManager;
        $this->fileManager = $fileManager;
        $this->config = $config;
    }

    /**
     * Получение информации о владельце документов
     * @return array
     */
    public static function getOwnerData()
    {
        return [
            'owner' => \Application\Entity\LegalEntity::class,
            'document' => \Application\Entity\LegalEntityDocument::class,
            'document_type' => \Application\Entity\LegalEntityDocumentType::class,
        ];
    }

    /**
     * Получение списка доступных документов
     * @return array
     */
    public static function getTypes()
    {
        return [
            \Application\Entity\LegalEntityBankDetail::class => 'bank_details',
            \Application\Entity\LegalEntityTaxInspection::class => 'tax_inspection',
        ];
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @return mixed
     * @throws \Exception
     */
    public function getCollectionDocument(User $user, array $params, bool $isOperator = false)
    {
        if($isOperator) {
            $documents = $this->entityManager->getRepository(LegalEntityDocument::class)
                ->getLegalEntityDocumentsBased($params);
        } elseif ($user && $user instanceof User) {
            $documents = $this->entityManager->getRepository(LegalEntityDocument::class)
                ->getLegalEntityDocumentsBasedByUserId($user->getId(), $params);
        } else {

            throw new \Exception('No data provided for getting tax inspection collection');
        }

        return $documents;
    }

    /**
     * @param LegalEntity $legalEntity
     * @param $params
     * @return mixed
     */
    public function getCollectionDocumentByLegalEntity(LegalEntity $legalEntity, $params)
    {

        $documents = $this->entityManager->getRepository(LegalEntityDocument::class)
            ->getLegalEntitiesDocumentBasedByLegalEntityId($legalEntity->getId(), $params);

        return $documents;
    }

    /**
     * @param LegalEntityDocument $legalEntityDocument
     * @return array
     */
    public function getLegalEntityDocumentOutput(LegalEntityDocument $legalEntityDocument)
    {
        $legalEntityDocumentOutput = [];

        $bankDetail = $legalEntityDocument->getLegalEntityBankDetail();
        $taxInspection = $legalEntityDocument->getLegalEntityTaxInspection();
        $currentDocument = ($bankDetail) ? $bankDetail: $taxInspection;

        $legalEntityDocumentOutput['id']        = $currentDocument->getId();
        $legalEntityDocumentOutput['type_name'] = $legalEntityDocument->getLegalEntityDocumentType()->getName();
        $legalEntityDocumentOutput['files'] = null;



        $legalEntityDocumentOutput['date_issued'] = (method_exists($currentDocument , 'getDateIssued')) ? $currentDocument->getDateIssued()->format('d-m-Y'): null;
        if (method_exists($currentDocument , 'getFiles') && count($currentDocument->getFiles()) > 0){
            foreach ($currentDocument->getFiles() as $file){
                $legalEntityDocumentOutput['files'][]=[
                    'civilId' => $legalEntityDocument->getLegalEntity()->getCivilLawSubject()->getId(),
                    'id' => $file->getId(),
                    'name' => $file->getOriginName(),
                    'type' => substr($file->getOriginName(), strrpos($file->getOriginName(), '.') + 1),
                ];
            }
        }


        return $legalEntityDocumentOutput;
    }

    /**
     * @param $legalEntityDocuments
     * @return array
     */
    public function extractLegalEntityDocumentCollection($legalEntityDocuments)
    {
        $legalEntityDocumentsOutput = [];

        foreach ($legalEntityDocuments as $legalEntityDocument) {

            $legalEntityDocumentsOutput[] = $this->getLegalEntityDocumentOutput($legalEntityDocument);
        }

        return $legalEntityDocumentsOutput;
    }
}