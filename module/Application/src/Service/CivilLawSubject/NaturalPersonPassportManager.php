<?php
namespace Application\Service\CivilLawSubject;

use Application\Entity\NaturalPersonPassport;
use Application\Entity\User;
use Doctrine\ORM\EntityManager;

class NaturalPersonPassportManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var
     */
    protected $config;

    /**
     * NaturalPersonDriverLicenseManager constructor.
     * @param EntityManager $entityManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->config = $config;
    }

    /**
     * @param $idPassport
     * @return mixed
     * @throws \Exception
     */
    public function getPassportById($idPassport)
    {
        if ($idPassport === -1 || $idPassport === 0) {

            throw new \Exception('Bad parameter idPassport');
        }

        /** @var NaturalPersonPassport $passport */
        $passport = $this->entityManager->getRepository(NaturalPersonPassport::class)
            ->getNaturalPersonPassportBasedOnPatternById($idPassport);

        if ($passport === null) {

            throw new \Exception('Not found passport by id');
        }
        return $passport;
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @return array
     * @throws \Exception
     */
    public function getCollectionPassport(User $user, array $params, bool $isOperator = false)
    {
        if($isOperator) {
            $collectionPassport = $this->entityManager->getRepository(NaturalPersonPassport::class)
                ->getPassportBased($params);
        } elseif ($user && $user instanceof User) {
            $collectionPassport = $this->entityManager->getRepository(NaturalPersonPassport::class)
                ->getPassportBasedByUserId($user->getId(), $params);
        } else {

            throw new \Exception('No data provided for getting passport collection');
        }

        return $collectionPassport;
    }

    /**
     * @param User $user
     * @return null
     */
    public function getAllowedPassportByUser(User $user){
        $allowedPassports = null;
        if ( $user && $user instanceof User){
            $allowedPassports = $this->entityManager->getRepository(NaturalPersonPassport::class)
                ->getPassportBasedByUserId($user->getId());
        }

        return ($allowedPassports && !empty($allowedPassports)) ? $allowedPassports : null;
    }

    /**
     * @param NaturalPersonPassport $passport
     * @return array
     */
    public function getPassportsOutput(NaturalPersonPassport $passport)
    {
        $passportsOutput = [];

        $passportsOutput['id']                          = $passport->getId();
        $passportsOutput['passport_serial_number']      = $passport->getPassportSerialNumber();
        $passportsOutput['date_issued']                 = $passport->getDateIssued()->format('d.m.Y');
        $passportsOutput['passport_issue_organisation'] = $passport->getPassportIssueOrganisation();
        $passportsOutput['date_expired']                = $passport->getDateExpired()->format('d.m.Y');
        $passportsOutput['files'] = null;

        if ( count($passport->getFiles()) > 0){
            foreach ($passport->getFiles() as $file){
                $passportsOutput['files'][]=[
                    'civilId' => $passport->getNaturalPersonDocument()->getNaturalPerson()->getCivilLawSubject()->getId(),
                    'id' => $file->getId(),
                    'name' => $file->getOriginName(),
                    'type' => substr($file->getOriginName(), strrpos($file->getOriginName(), '.') + 1),
                ];
            }
        }

        return $passportsOutput;
    }

    /**
     * @param $passports
     * @return array
     */
    public function extractPassportCollection($passports)
    {
        $passportsOutput = [];

        foreach ($passports as $passport) {

            $passportsOutput[] = $this->getPassportsOutput($passport);
        }

        return $passportsOutput;
    }
}