<?php

namespace Application\Service\CivilLawSubject;

use Application\Entity\LegalEntityDocument;

/**
 * Class LegalEntityDocumentPolitics
 * @package Application\Service\CivilLawSubject
 */
class LegalEntityDocumentPolitics
{
    /**
     * @param LegalEntityDocument $legalEntityDocument
     * @return bool
     */
    public function isLock(LegalEntityDocument $legalEntityDocument)
    {
        if (!$legalEntityDocument->getLegalEntity()->getCivilLawSubject()->getIsPattern()){

            return true;
        }

        return false;
    }
}