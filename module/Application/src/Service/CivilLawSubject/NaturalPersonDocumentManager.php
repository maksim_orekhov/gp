<?php
namespace Application\Service\CivilLawSubject;

use Application\Entity\NaturalPerson;
use Application\Entity\NaturalPersonDocument;
use Application\Service\CivilLawSubject\Base\DocumentBase;
use Application\Service\CivilLawSubject\Base\DocumentInterface;
use Application\Entity\User;


class NaturalPersonDocumentManager extends DocumentBase implements DocumentInterface
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \ModuleFileManager\Service\FileManager
     */
    protected $fileManager;

    protected $config;

    public function __construct(
        \Doctrine\ORM\EntityManager $entityManager,
        \ModuleFileManager\Service\FileManager $fileManager,
        $config)
    {
        $this->entityManager = $entityManager;
        $this->fileManager = $fileManager;
        $this->config = $config;
    }

    /**
     * Получение информации о владельце документов
     * @return array
     */
    public static function getOwnerData()
    {
        return [
            'owner' => \Application\Entity\NaturalPerson::class,
            'document' => \Application\Entity\NaturalPersonDocument::class,
            'document_type' => \Application\Entity\NaturalPersonDocumentType::class,
        ];
    }

    /**
     * Получение списка доступных документов
     * @return array
     */
    public static function getTypes()
    {
        return [
            \Application\Entity\NaturalPersonPassport::class => 'passport',
            \Application\Entity\NaturalPersonDriverLicense::class => 'driver_license',
        ];
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @return mixed
     * @throws \Exception
     */
    public function getCollectionDocument(User $user, array $params, bool $isOperator = false)
    {
        if($isOperator) {
            $documents = $this->entityManager->getRepository(NaturalPersonDocument::class)
                ->getNaturalPersonsDocumentBased($params);
        } elseif ($user && $user instanceof User) {
            $documents = $this->entityManager->getRepository(NaturalPersonDocument::class)
                ->getNaturalPersonsDocumentBasedByUserId($user->getId(), $params);
        } else {

            throw new \Exception('No data provided for getting tax inspection collection');
        }

        return $documents;
    }

    /**
     * @param NaturalPerson $naturalPerson
     * @param $params
     * @return mixed
     */
    public function getCollectionDocumentByNaturalPerson(NaturalPerson $naturalPerson, $params)
    {

        $documents = $this->entityManager->getRepository(NaturalPersonDocument::class)
                ->getNaturalPersonsDocumentBasedByNaturalPersonId($naturalPerson->getId(), $params);

        return $documents;
    }

    /**
     * @param NaturalPersonDocument $naturalPersonDocument
     * @return array
     */
    public function getNaturalPersonDocumentOutput(NaturalPersonDocument $naturalPersonDocument)
    {
        $naturalPersonDocumentOutput = [];

        $driverLicense = $naturalPersonDocument->getNaturalPersonDriverLicense();
        $passport = $naturalPersonDocument->getNaturalPersonPassport();
        $currentDocument = ($passport) ? $passport: $driverLicense;

        $naturalPersonDocumentOutput['id']        = $currentDocument->getId();
        $naturalPersonDocumentOutput['type_name'] = $naturalPersonDocument->getNaturalPersonDocumentType()->getName();
        $naturalPersonDocumentOutput['files'] = null;

        $naturalPersonDocumentOutput['date_issued'] = (method_exists($currentDocument , 'getDateIssued')) ? $currentDocument->getDateIssued()->format('d-m-Y'): null;
        if ( count($currentDocument->getFiles()) > 0){
            foreach ($currentDocument->getFiles() as $file){
                $naturalPersonDocumentOutput['files'][]=[
                    'civilId' => $naturalPersonDocument->getNaturalPerson()->getCivilLawSubject()->getId(),
                    'id' => $file->getId(),
                    'name' => $file->getOriginName(),
                    'type' => substr($file->getOriginName(), strrpos($file->getOriginName(), '.') + 1),
                ];
            }
        }


        return $naturalPersonDocumentOutput;
    }

    /**
     * @param $naturalPersonDocuments
     * @return array
     */
    public function extractNaturalPersonDocumentCollection($naturalPersonDocuments)
    {
        $naturalPersonDocumentsOutput = [];

        foreach ($naturalPersonDocuments as $naturalPersonDocument) {

            $naturalPersonDocumentsOutput[] = $this->getNaturalPersonDocumentOutput($naturalPersonDocument);
        }

        return $naturalPersonDocumentsOutput;
    }
}