<?php

namespace Application\Service\CivilLawSubject;

use Application\Entity\NaturalPerson;

/**
 * Class LegalEntityPolitics
 * @package Application\Service\CivilLawSubject
 */
class NaturalPersonPolitics
{
    /**
     * @param NaturalPerson $naturalPerson
     * @return bool
     */
    public function isLock(NaturalPerson $naturalPerson)
    {
        if (!$naturalPerson->getCivilLawSubject()->getIsPattern()){

            return true;
        }

        return false;
    }
}