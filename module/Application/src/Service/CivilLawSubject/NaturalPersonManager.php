<?php
namespace Application\Service\CivilLawSubject;

use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethod;
use Application\Service\CivilLawSubject\Base\CivilLawSubjectChildManager;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Application\Hydrator\NaturalPersonHydrator;
use Application\Entity\NaturalPerson;
use Application\Entity\User;
use Application\Form\NaturalPersonForm;

class NaturalPersonManager extends CivilLawSubjectChildManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var NaturalPersonDocumentManager
     */
    private $naturalPersonDocumentManager;

    /**
     * @var NaturalPersonHydrator
     */
    private $naturalPersonHydrator;

    /**
     * @var array
     */
    private $config;

    /**
     * NaturalPersonManager constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param NaturalPersonDocumentManager $naturalPersonDocumentManager
     * @param NaturalPersonHydrator $naturalPersonHydrator
     * @param array $config
     */
    public function __construct(
        \Doctrine\ORM\EntityManager $entityManager,
        NaturalPersonDocumentManager $naturalPersonDocumentManager,
        NaturalPersonHydrator $naturalPersonHydrator,
        array $config
    )
    {
        $this->entityManager = $entityManager;
        $this->naturalPersonDocumentManager = $naturalPersonDocumentManager;
        $this->naturalPersonHydrator = $naturalPersonHydrator;
        $this->config = $config;
    }

    /**
     * Переопределяем для гидрирование даты
     * Вставляет данные массива в объект entity
     * @param array $data
     * @param $entityObject
     * @return object
     */
    protected function hydrate($data, $entityObject)
    {
        foreach ($data as $key => $item) {
            if (strpos($key, 'date') !== false) {
                $this->getHydrator()->addStrategy($key, new \Application\Hydrator\Strategy\DateTimeStrategy());
            }
        }

        return $this->getHydrator()->hydrate($data, $entityObject);
    }

    /**
     * @param $naturalPersons
     * @return array
     */
    public function extractNaturalPersonCollection($naturalPersons)
    {
        $naturalPersonsOutput = [];

        foreach ($naturalPersons as $naturalPerson) {

            $naturalPersonsOutput[] = $this->getNaturalPersonOutput($naturalPerson);
        }

        return $naturalPersonsOutput;
    }

    /**
     * Умное извлечение объекта (отсекаются ненужные данные)
     *
     * @param $naturalPerson
     * @return array|null
     */
    public function extractNaturalPerson($naturalPerson)
    {
        if (!$naturalPerson || !($naturalPerson instanceof NaturalPerson)) {

            return null;
        }

        return $this->naturalPersonHydrator->extract($naturalPerson);
    }

    /**
     * @param $id
     * @return NaturalPerson|null
     */
    public function get($id)
    {
        return $this->entityManager->getRepository(NaturalPerson::class)->find($id);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getBasedOnPattern($id)
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->getNaturalPersonBasedOnPatternById($id);

        return $naturalPerson;
    }

    /**
     * @return NaturalPersonDocumentManager
     */
    public function getDocumentManager()
    {
        return $this->naturalPersonDocumentManager;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @param array $params [
     *  'first_name' => 'Александр',
     *  'secondary_name' => 'Александрович',
     *  'last_name' => 'Александров',
     *  'birth_date' => '1992-07-09',
     * ]
     * @return NaturalPerson
     * @throws \Exception
     */
    public function create(CivilLawSubject $civilLawSubject, array $params = [])
    {
        $missKeys = $this->checkKeyRequired([
            'first_name',
            'secondary_name',
            'last_name',
            'birth_date',
        ], $params);
        if (count($missKeys)) {
            throw new \Exception('Missing required keys: '.implode(', ', $missKeys));
        }

        $person = new NaturalPerson();
        $person = $this->hydrate($params, $person);
        $person->setCivilLawSubject($civilLawSubject);

        $civilLawSubject->setNaturalPerson($person);

        $this->entityManager->persist($person);
        $this->entityManager->flush();

        /** @var NaturalPerson $person */
        return $person;
    }

    /**
     * @param NaturalPerson $person
     * @param array $params [
     *  'first_name' => 'Александр',
     *  'secondary_name' => 'Александрович',
     *  'last_name' => 'Александров',
     *  'birth_date' => '1992-07-09',
     * ]
     * @return NaturalPerson
     * @throws \Exception
     */
    public function update(NaturalPerson $person, $params = [])
    {
        $missKeys = $this->checkKeyRequired([
            'first_name',
            'secondary_name',
            'last_name',
            'birth_date',
        ], $params);
        if (count($missKeys)) {
            throw new \Exception('Missing required keys: '.implode(", ", $missKeys));
        }

        $person = $this->hydrate($params, $person);
        $this->entityManager->persist($person);
        $this->entityManager->flush();

        /** @var NaturalPerson $person */
        return $person;
    }

    /**
     * @param NaturalPerson $naturalPerson
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(NaturalPerson $naturalPerson)
    {
        foreach ($naturalPerson->getNaturalPersonDocument() as $document) {
            $this->naturalPersonDocumentManager->remove($document);
        }

        $this->entityManager->remove($naturalPerson);
        $this->entityManager->flush();
    }

    /**
     * Получение расширенных документов с группирокой по типам
     *
     * @param NaturalPerson $naturalPerson
     * @return array
     * @throws \Exception
     */
    public function getDocuments(NaturalPerson $naturalPerson)
    {
        $documents = $naturalPerson->getNaturalPersonDocument();
        return $this->naturalPersonDocumentManager->getGroupByType($documents);
    }

    /**
     * Клонирование физ лица
     *
     * @param NaturalPerson $naturalPerson
     * @param CivilLawSubject $civilLawSubject
     * @return NaturalPerson
     * @throws \Exception
     */
    public function clone(NaturalPerson $naturalPerson, CivilLawSubject $civilLawSubject)
    {
        $paramsNaturalPerson = $this->extract($naturalPerson);
        $params = [
            'first_name' => $paramsNaturalPerson['first_name'],
            'secondary_name' => $paramsNaturalPerson['secondary_name'],
            'last_name' => $paramsNaturalPerson['last_name'],
            'birth_date' => $paramsNaturalPerson['birth_date'],
            'civil_law_subject' => $civilLawSubject,
        ];
        $newNaturalPerson = $this->create($civilLawSubject, $params);

        foreach ($naturalPerson->getNaturalPersonDocument() as $document) {
            // TODO Добавить проверку что документ потвержден
            if (!$document->isActive()) {
                continue;
            }

            $this->naturalPersonDocumentManager->clone($document, $newNaturalPerson);
        }

        return $newNaturalPerson;
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @return null
     */
    public function getCollectionNaturalPerson(User $user, array $params, bool $isOperator)
    {
        if($isOperator) {
            $naturalPersons = $this->entityManager->getRepository(CivilLawSubject::class)
                ->getNaturalPersons($params);
        } elseif ($user && $user instanceof User) {
            $naturalPersons = $this->entityManager->getRepository(CivilLawSubject::class)
                ->getNaturalPersonsByUserId($user->getId(), $params);
        } else {

            $naturalPersons = null;
        }

        return $naturalPersons;
    }

    /**
     * @param $idNaturalPerson
     * @return NaturalPerson|null
     * @throws \Exception
     */
    public function getSingleNaturalPersonById($idNaturalPerson)
    {
        if ($idNaturalPerson === -1 || $idNaturalPerson === 0) {

            throw new \Exception('Bad parameter idNaturalPerson');
        }

        $naturalPerson = $this->get($idNaturalPerson);

        if ($naturalPerson === null) {

            throw new \Exception('Not found Natural Person by id');
        }

        return $naturalPerson;
    }

    /**
     * @param $idNaturalPerson
     * @return mixed
     * @throws \Exception
     */
    public function getNaturalPersonById($idNaturalPerson)
    {
        return $this->getBasedOnPattern($idNaturalPerson);
    }

    /**
     * @param NaturalPersonForm $form
     * @param NaturalPerson $naturalPerson
     * @return NaturalPersonForm
     */
    public function setFormData(NaturalPersonForm $form, NaturalPerson $naturalPerson)
    {
        $form->setData([
            'first_name' => $naturalPerson->getFirstName(),
            'secondary_name' => $naturalPerson->getSecondaryName(),
            'last_name' => $naturalPerson->getLastName(),
            'birth_date' => $naturalPerson->getBirthDate()->format('d.m.Y'),
        ]);

        return $form;
    }

    /**
     * @param User $user
     * @return mixed|null
     */
    public function getAllowedNaturalPersonByUser(User $user)
    {
        $allowedNaturalPerson = null;
        if ( $user && $user instanceof User){
            $allowedNaturalPerson = $this->entityManager->getRepository(CivilLawSubject::class)
                ->getNaturalPersonsByUserId($user->getId());
        }

        return ($allowedNaturalPerson && !empty($allowedNaturalPerson)) ? $allowedNaturalPerson : null;
    }

    /**
     * @param NaturalPerson $naturalPerson
     * @return array
     */
    public function getNaturalPersonOutput(NaturalPerson $naturalPerson)
    {
        $naturalPersonOutput = [];

        $naturalPersonOutput['id']              = $naturalPerson->getId();
        $naturalPersonOutput['first_name']      = $naturalPerson->getFirstName();
        $naturalPersonOutput['secondary_name']  = $naturalPerson->getSecondaryName();
        $naturalPersonOutput['last_name']       = $naturalPerson->getLastName();
        $naturalPersonOutput['birth_date']      = $naturalPerson->getBirthDate() ? $naturalPerson->getBirthDate()->format('d.m.Y') : null;
        $naturalPersonOutput['birth_date_milliseconds'] = $naturalPerson->getBirthDate() ? $naturalPerson->getBirthDate()->getTimestamp() : null;
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $naturalPerson->getCivilLawSubject();
        $naturalPersonOutput['civil_law_subject_id'] = $civilLawSubject->getId();
        $naturalPersonOutput['payment_methods'] = $this->getPaymentMethodCollectionOutput($civilLawSubject);
        $naturalPersonOutput['documents'] = null;

        $documents = $this->naturalPersonDocumentManager->getCollectionDocumentByNaturalPerson($naturalPerson, []);
        $documentsOutput = $this->naturalPersonDocumentManager->extractNaturalPersonDocumentCollection($documents);
        if (!empty($documentsOutput)){
            $naturalPersonOutput['documents'] = $documentsOutput;
        }

        return $naturalPersonOutput;
    }
}