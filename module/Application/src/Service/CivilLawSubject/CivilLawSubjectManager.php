<?php
namespace Application\Service\CivilLawSubject;

use Application\Entity\CivilLawSubjectChildInterface;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\NdsType;
use Application\Entity\SoleProprietor;
use Application\Form\LegalEntityForm;
use Application\Form\NaturalPersonForm;
use Application\Form\SoleProprietorForm;
use Application\Service\NdsTypeManager;
use Core\Exception\LogicException;
use Application\Service\HydratorManager;
use Application\Entity\CivilLawSubject;
use Application\Entity\LegalEntity;
use Application\Entity\NaturalPerson;
use Application\Entity\PaymentMethod;
use Application\Entity\User;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManager;
use Application\Service\Payment\PaymentMethodManager;

/**
 * Class CivilLawSubjectManager
 * @package Application\Service\CivilLawSubject
 */
class CivilLawSubjectManager extends HydratorManager
{
    const LEGAL_ENTITY_NAME = 'legal-entity';
    const SOLE_PROPRIETOR_NAME = 'sole-proprietor';
    const NATURAL_PERSON_NAME = 'natural-person';

    const CIVIL_LAW_SUBJECT_TYPE_NAMES = [
        self::NATURAL_PERSON_NAME,
        self::SOLE_PROPRIETOR_NAME,
        self::LEGAL_ENTITY_NAME
    ];

    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var NaturalPersonManager
     */
    private $naturalPersonManager;

    /**
     * @var SoleProprietorManager
     */
    private $soleProprietorManager;

    /**
     * @var LegalEntityManager
     */
    private $legalEntityManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var CivilLawSubjectPolitics
     */
    private $civilLawSubjectPolitics;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * CivilLawSubjectManager constructor.
     * @param EntityManager $entityManager
     * @param NaturalPersonManager $naturalPersonManager
     * @param SoleProprietorManager $soleProprietorManager
     * @param LegalEntityManager $legalEntityManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param CivilLawSubjectPolitics $civilLawSubjectPolitics
     * @param NdsTypeManager $ndsTypeManager
     */
    public function __construct(EntityManager $entityManager,
                                NaturalPersonManager $naturalPersonManager,
                                SoleProprietorManager $soleProprietorManager,
                                LegalEntityManager $legalEntityManager,
                                PaymentMethodManager $paymentMethodManager,
                                CivilLawSubjectPolitics $civilLawSubjectPolitics,
                                NdsTypeManager $ndsTypeManager)
    {
        $this->entityManager            = $entityManager;
        $this->naturalPersonManager     = $naturalPersonManager;
        $this->soleProprietorManager    = $soleProprietorManager;
        $this->legalEntityManager       = $legalEntityManager;
        $this->paymentMethodManager     = $paymentMethodManager;
        $this->civilLawSubjectPolitics  = $civilLawSubjectPolitics;
        $this->ndsTypeManager           = $ndsTypeManager;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function get($id)
    {
        return $this->entityManager->getRepository(CivilLawSubject::class)->find((int) $id);
    }

    /**
     * @param User|null $user
     * @param int $is_pattern
     * @param null $ndsType
     * @return CivilLawSubject
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create($user, $is_pattern = 1, $ndsType = null)
    {
        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern($is_pattern);
        $civilLawSubject->setCreated(new \DateTime(date('Y-m-d H:i:s')));

        if($ndsType !== null){
            $civilLawSubject->setNdsType($ndsType);
        }

        $this->entityManager->persist($civilLawSubject);
        $this->entityManager->flush();

        return $civilLawSubject;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @param $ndsType
     * @return CivilLawSubject
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(CivilLawSubject $civilLawSubject, $ndsType)
    {
        if($ndsType !== null){
            $civilLawSubject->setNdsType($ndsType);
        }

        $this->entityManager->persist($civilLawSubject);
        $this->entityManager->flush();

        return $civilLawSubject;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @throws ConnectionException
     * @throws LogicException
     * @throws \Throwable
     */
    public function remove(CivilLawSubject $civilLawSubject)
    {
        //is remove allowed by policy
        $check_remove_result = $this->civilLawSubjectPolitics
            ->checkRemovalIsAllowed($civilLawSubject, $this->entityManager);

        if (false === $check_remove_result['allowed']) {

            throw new LogicException(null, $check_remove_result['reason']);
        }

        $this->entityManager->getConnection()->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit(false);
        try {
            //удаляем все привязанные платежные методы
            $civilLawSubject = $this->removeAllPaymentMethod($civilLawSubject);
            //открепить от всех агентов сделки
            $civilLawSubject = $this->detachFromAllDealAgents($civilLawSubject);

            // для удаления физ лица
            if ($civilLawSubject->getNaturalPerson() !== null) {
                $this->naturalPersonManager->remove($civilLawSubject->getNaturalPerson());
            }
            // для удаления ИП
            if ($civilLawSubject->getSoleProprietor() !== null) {
                $this->soleProprietorManager->remove($civilLawSubject->getSoleProprietor());
            }
            // для удаления юр лица
            if ($civilLawSubject->getLegalEntity() !== null) {
                $this->legalEntityManager->remove($civilLawSubject->getLegalEntity());
            }

            $this->entityManager->remove($civilLawSubject);
            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();
        }
        catch (\Throwable $t) {
            $this->entityManager->getConnection()->close();
            $this->entityManager->getConnection()->rollBack();

            throw $t;
        }
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @return CivilLawSubject
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    private function removeAllPaymentMethod(CivilLawSubject $civilLawSubject)
    {
        $paymentMethods = $civilLawSubject->getPaymentMethods();

        /** @var PaymentMethod $paymentMethod */
        foreach ($paymentMethods as $paymentMethod){
            if($paymentMethod->getIsPattern()) {
                $this->paymentMethodManager->remove($paymentMethod);
            } else {
                throw new \Exception('attempt to remove non-pattern payment method');
            }
        }

        return $civilLawSubject;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @return CivilLawSubject
     * @throws \Exception
     */
    private function detachFromAllDealAgents(CivilLawSubject $civilLawSubject)
    {
        $dealAgents = $this->entityManager->getRepository(DealAgent::class)
            ->findBy(['civilLawSubject' => $civilLawSubject]);
        /** @var DealAgent $dealAgent */
        foreach ($dealAgents as $dealAgent){
            if (! $dealAgent->getDateOfConfirm()) {
                $dealAgent->setCivilLawSubject(null);
                $dealAgent->setDealAgentConfirm(0);
                $this->entityManager->persist($dealAgent);
            } else {
                throw new \Exception('attempt to change the deal agent confirmed the deal');
            }
        }
        $this->entityManager->flush();

        return $civilLawSubject;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getPattern($id)
    {
        return $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['id' => $id, 'isPattern' => true]);
    }

    /**
     * Get Pattern CivilLawSubject for specific User
     *
     * @param User $user
     * @return array
     */
    public function getPatternsByUser(User $user)
    {
        return $this->entityManager->getRepository(CivilLawSubject::class)
            ->findBy(['user' => $user, 'isPattern' => true]);
    }

    /**
     * Get CivilLawSubject by Id, User and isPattern
     *
     * @param User $user
     * @param int $civil_law_subject_id
     * @return CivilLawSubject
     */
    public function getCivilLawSubjectForUser(User $user, int $civil_law_subject_id)
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject =  $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['id' => $civil_law_subject_id, 'user' => $user, 'isPattern' => true]);

        // Дополнительные проверки, имеет ли пользователь право использователь этот civilLawSubject
        // Если нет, то return null

        return $civilLawSubject;
    }

    /**
     * Get CivilLawSubject collection by User and isPattern
     *
     * @param User $user
     * @return CivilLawSubject|null
     */
    public function getAvailableCivilLawSubjects(User $user)
    {
        $civilLawSubjects = null;
        if ($user && $user instanceof User) {
            /** @var CivilLawSubject $civilLawSubjects */
            $civilLawSubjects =  $this->entityManager->getRepository(CivilLawSubject::class)
                ->findBy(['user' => $user, 'isPattern' => true]);
            // тут ктото у коллекции объектов брал ид. я убирать не стал. просто заккоментил   $civilLawSubjects->getId();
        }

        // Тут допольнительная Фильтрация civilLawSubjects конктретного пользователя $user

        return (!empty($civilLawSubjects)) ? $civilLawSubjects : null;
    }

    /**
     * Get CivilLawSubject collection by User and isPattern
     *
     * @param User $user
     * @return bool
     */
    public function getAvailableCivilLawSubjectsAndCheckForUser(User $user, $postData = false)
    {
        $civilLawSubjects = null;
        if (array_key_exists('civil_law_subject', $postData) && !is_array($postData['civil_law_subject'])) {
            if ($user && $user instanceof User) {
                /** @var CivilLawSubject $civilLawSubjects */
                $civilLawSubjects = $this->entityManager->getRepository(CivilLawSubject::class)
                    ->findBy(['user' => $user, 'isPattern' => true]);
                foreach ($civilLawSubjects as $civilLawSubject) {

                    if ($postData['civil_law_subject'] == $civilLawSubject->getId()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Создание физ. лица
     *
     * @param User|user $user
     * @param array $params
     * @param int $is_pattern
     * @return \Application\Entity\NaturalPerson
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function createNaturalPerson($user, $params = [], $is_pattern = 1)
    {
        $civilLawSubject = $this->create($user, $is_pattern);

        return $this->naturalPersonManager->create($civilLawSubject, $params);
    }

    /**
     * Создание ИП
     *
     * @param User|null $user
     * @param array $params
     * @param int $is_pattern
     * @return SoleProprietor|object
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createSoleProprietor($user, array $params = [], $is_pattern = 1)
    {
        if (!array_key_exists('nds_type', $params) || null === $params['nds_type']) {

            throw new LogicException(null, LogicException::NDS_TYPE_NOT_SPECIFIED);
        }

        /** @var NdsType $ndsType */
        $ndsType = $this->ndsTypeManager->getNdsTypeByType($params['nds_type']);

        if (null === $ndsType) {

            throw new LogicException(null, LogicException::NDS_TYPE_NOT_FOUND);
        }

        $civilLawSubject = $this->create($user, $is_pattern, $ndsType);

        return $this->soleProprietorManager->create($civilLawSubject, $params);
    }

    /**
     * @param SoleProprietor $soleProprietor
     * @param $params
     * @return SoleProprietor|object
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateSoleProprietor(SoleProprietor $soleProprietor, $params)
    {
        if (!array_key_exists('nds_type', $params) || null === $params['nds_type']) {

            throw new LogicException(null, LogicException::NDS_TYPE_NOT_SPECIFIED);
        }

        /** @var NdsType $ndsType */
        $ndsType = $this->ndsTypeManager->getNdsTypeByType($params['nds_type']);

        if (null === $ndsType) {

            throw new LogicException(null, LogicException::NDS_TYPE_NOT_FOUND);
        }

        $this->update($soleProprietor->getCivilLawSubject(), $ndsType);

        return $this->soleProprietorManager->update($soleProprietor, $params);
    }

    /**
     * Создание юр. лица
     *
     * @param User|null $user
     * @param array $params
     * @param int $is_pattern
     * @return \Application\Entity\LegalEntity
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function createLegalEntity($user, $params = [], $is_pattern = 1): LegalEntity
    {
        if (!array_key_exists('nds_type', $params) || null === $params['nds_type']) {

            throw new LogicException(null, LogicException::NDS_TYPE_NOT_SPECIFIED);
        }

        /** @var NdsType $ndsType */
        $ndsType = $this->ndsTypeManager->getNdsTypeByType($params['nds_type']);

        if (null === $ndsType) {

            throw new LogicException(null, LogicException::NDS_TYPE_NOT_FOUND);
        }

        $civilLawSubject = $this->create($user, $is_pattern, $ndsType);

        return $this->legalEntityManager->create($civilLawSubject, $params);
    }

    /**
     * @param LegalEntity $legalEntity
     * @param $params
     * @return object
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function updateLegalEntity(LegalEntity $legalEntity, $params)
    {
        if (!array_key_exists('nds_type', $params) || null === $params['nds_type']) {

            throw new LogicException(null, LogicException::NDS_TYPE_NOT_SPECIFIED);
        }

        /** @var NdsType $ndsType */
        $ndsType = $this->ndsTypeManager->getNdsTypeByType($params['nds_type']);

        if (null === $ndsType) {

            throw new LogicException(null, LogicException::NDS_TYPE_NOT_FOUND);
        }

        $this->update($legalEntity->getCivilLawSubject(), $ndsType);

        return $this->legalEntityManager->update($legalEntity, $params);
    }

    /**
     * Клонирование CivilLawSubject
     *
     * @param CivilLawSubject $civilLawSubject
     * @return CivilLawSubject
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function clone(CivilLawSubject $civilLawSubject)
    {
        $user = $civilLawSubject->getUser();
        $newCivilLawSubject = $this->create($user, $is_pattern = 0, $civilLawSubject->getNdsType());

        if ($civilLawSubject->getNaturalPerson() !== null) {
            $this->naturalPersonManager->clone($civilLawSubject->getNaturalPerson(), $newCivilLawSubject);
        }
        if ($civilLawSubject->getSoleProprietor() !== null) {
            $this->soleProprietorManager->clone($civilLawSubject->getSoleProprietor(), $newCivilLawSubject);
        }
        if ($civilLawSubject->getLegalEntity() !== null) {
            $this->legalEntityManager->clone($civilLawSubject->getLegalEntity(), $newCivilLawSubject);
        }

        return $newCivilLawSubject;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @return CivilLawSubjectChildInterface|null
     */
    public function getAssignedCivilLawSubject(CivilLawSubject $civilLawSubject)
    {
        if ($civilLawSubject->getNaturalPerson() && $civilLawSubject->getNaturalPerson() instanceof NaturalPerson) {

            return $civilLawSubject->getNaturalPerson();
        }

        if ($civilLawSubject->getSoleProprietor() && $civilLawSubject->getSoleProprietor() instanceof SoleProprietor) {

            return $civilLawSubject->getSoleProprietor();
        }

        if ($civilLawSubject->getLegalEntity() && $civilLawSubject->getLegalEntity() instanceof LegalEntity) {

            return $civilLawSubject->getLegalEntity();
        }

        return null;
    }

    /**
     * @param User $user
     * @param $params
     * @param bool $return_as_array
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function getNaturalPersonsForUser(User $user, $params, $return_as_array = false): array
    {
        $naturalPersons = $this->entityManager->getRepository(CivilLawSubject::class)
            ->getNaturalPersonsByUserId($user->getId(), $params);

        if (!$return_as_array) {

            return $naturalPersons;
        }

        // Если нужно вернуть в виде массива (extracted)
        $naturalPersonsOutput = [];

        foreach ($naturalPersons as $naturalPerson) {
            // Add new element to array
            $naturalPersonsOutput[] = $this->naturalPersonManager
                ->getNaturalPersonOutput($naturalPerson);
        }

        return $naturalPersonsOutput;
    }

    /**
     * @param User $user
     * @param $params
     * @param bool $return_as_array
     * @return array
     */
    public function getSoleProprietorForUser(User $user, $params, $return_as_array = false): array
    {
        $soleProprietors = $this->entityManager->getRepository(CivilLawSubject::class)
            ->getSoleProprietorsByUserId($user->getId(), $params);

        if (!$return_as_array) {

            return $soleProprietors;
        }

        // Если нужно вернуть в виде массива (extracted)
        $soleProprietorsOutput = [];

        foreach ($soleProprietors as $soleProprietor) {
            // Add new element to array
            $soleProprietorsOutput[] = $this->soleProprietorManager
                ->getSoleProprietorOutput($soleProprietor);
        }

        return $soleProprietorsOutput;
    }

    /**
     * @param User $user
     * @param $params
     * @param bool $return_as_array
     * @return array
     */
    public function getLegalEntitiesForUser(User $user, $params, $return_as_array = false): array
    {
        $legalEntities = $this->entityManager->getRepository(CivilLawSubject::class)
            ->getLegalEntitiesByUserId($user->getId(), $params);

        if (!$return_as_array) {

            return $legalEntities;
        }

        // Если нужно вернуть в виде массива (extracted)
        $legalEntitiesOutput = [];

        foreach ($legalEntities as $legalEntity) {
            // Add new element to array
            $legalEntitiesOutput[] = $this->legalEntityManager
                ->getLegalEntityOutput($legalEntity);
        }

        return $legalEntitiesOutput;
    }

    /**
     * @param User $user
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function getCivilLawSubjectsAsSeparateArrays(User $user)
    {
        $legalEntities = $this->entityManager->getRepository(CivilLawSubject::class)
            ->getLegalEntitiesByUserId($user->getId());
        $soleProprietors = $this->entityManager->getRepository(CivilLawSubject::class)
            ->getSoleProprietorsByUserId($user->getId());
        $naturalPersons = $this->entityManager->getRepository(CivilLawSubject::class)
            ->getNaturalPersonsByUserId($user->getId());

        $natural_persons = [];
        if (null !== $naturalPersons) {
            $natural_persons = $this->naturalPersonManager->extractNaturalPersonCollection($naturalPersons);
        }

        $sole_proprietors = [];
        if (null !== $soleProprietors) {
            $sole_proprietors = $this->soleProprietorManager->extractSoleProprietorCollectionOutput($soleProprietors);
        }

        $legal_entities = [];
        if (null !== $legalEntities) {
            $legal_entities = $this->legalEntityManager->extractLegalEntityCollection($legalEntities);
        }

        return [
            'natural_persons'   => $natural_persons,
            'sole_proprietors'  => $sole_proprietors,
            'legal_entities'    => $legal_entities,
        ];
    }

    /**
     * @param User $user
     * @param $deal_id
     * @return CivilLawSubject|null
     * @throws \Exception
     */
    public function getCivilLawSubjectByDealIdForCurrentUser(User $user, $deal_id)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['id' => $deal_id]);

        if($user === $deal->getCustomer()->getUser()) {
            $dealAgent = $deal->getCustomer();
        } elseif ($user === $deal->getContractor()->getUser()) {
            $dealAgent = $deal->getContractor();
        } else {

            throw new \Exception('The user does not match either the Customer or the Contractor');
        }

        return $dealAgent->getCivilLawSubject();
    }

    /**
     * @param null $type
     * @return string|null
     */
    public function getCivilLawSubjectType($type = null)
    {
        $type = strtolower(str_ireplace(['_','-'], '', $type));
        $type_legal_entity      = strtolower(str_ireplace(['_','-'], '', self::LEGAL_ENTITY_NAME));
        $type_natural_person    = strtolower(str_ireplace(['_','-'], '', self::NATURAL_PERSON_NAME));
        $type_sole_proprietor   = strtolower(str_ireplace(['_','-'], '', self::SOLE_PROPRIETOR_NAME));

        switch ($type){
            case $type_natural_person:
                return self::NATURAL_PERSON_NAME;
            case $type_sole_proprietor:
                return self::SOLE_PROPRIETOR_NAME;
            case $type_legal_entity:
                return self::LEGAL_ENTITY_NAME;
            default:
                return null;
        }
    }

    /**
     * @param string $type
     * @return LegalEntityForm|NaturalPersonForm|SoleProprietorForm
     * @throws LogicException
     */
    public function getFormByCivilLawSubjectType(string $type = '')
    {
        switch ($type){
            case self::NATURAL_PERSON_NAME:
                return new NaturalPersonForm();
            case self::SOLE_PROPRIETOR_NAME:
                $ndsTypes = $this->ndsTypeManager->getNdsTypes();
                return new SoleProprietorForm($ndsTypes);
            case self::LEGAL_ENTITY_NAME:
                $ndsTypes = $this->ndsTypeManager->getNdsTypes();
                return new LegalEntityForm($ndsTypes);
            default:
                throw new LogicException(null, LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_TYPE);
        }
    }

    /**
     * @param $data
     * @param $type
     * @return array|string
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws LogicException
     * @throws \Exception
     */
    public function saveFormData($data, $type)
    {
        switch ($this->getCivilLawSubjectType($type)) {
            case self::NATURAL_PERSON_NAME:
                return $this->saveFormDataNaturalPerson($data);
            case self::SOLE_PROPRIETOR_NAME:
                return $this->saveFormDataSoleProprietor($data);
            case self::LEGAL_ENTITY_NAME:
                return $this->saveFormDataLegalEntity($data);
            default:
                throw new LogicException(null, LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_TYPE);
        }
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    public function saveFormDataLegalEntity($data)
    {
        if ( isset($data['idLegalEntity']) ) {
            //update
            if ( (int) $data['idLegalEntity'] <= 0 ) {

                throw new \Exception('bad data provided: not found legal entity id');
            }

            $legalEntity = $this->legalEntityManager->getLegalEntityById((int) $data['idLegalEntity']);

            if( $legalEntity === null) {

                throw new \Exception('bad data provided: not found legal entity by id');
            }

            $legalEntity = $this->updateLegalEntity($legalEntity, $data);

            $result = [
                'civilLawSubject' => $legalEntity->getCivilLawSubject(),
                'legalEntity' => $legalEntity,
                'user' => $legalEntity->getCivilLawSubject()->getUser(),
            ];
        } else {
            //create
            /** @var LegalEntity $legalEntity */
            $legalEntity = $this->createLegalEntity($data['user'], $data);

            $result = [
                'civilLawSubject' => $legalEntity->getCivilLawSubject(),
                'legalEntity' => $legalEntity,
                'user' => $data['user']
            ];
        }

        return $result;
    }


    /**
     * @param $data
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function saveFormDataNaturalPerson($data)
    {
        if (isset($data['idNaturalPerson'])) {
            //update
            if ((int) $data['idNaturalPerson'] <= 0) {
                throw new \Exception('bad data provided: not found natural person id');
            }

            $naturalPerson = $this->naturalPersonManager->getNaturalPersonById((int) $data['idNaturalPerson']);

            if( $naturalPerson === null) {
                throw new \Exception('bad data provided: not found natural person by id');
            }

            $naturalPerson = $this->naturalPersonManager->update($naturalPerson, $data);

            $result = [
                'civilLawSubject' => $naturalPerson->getCivilLawSubject(),
                'naturalPerson' => $naturalPerson,
                'user' => $naturalPerson->getCivilLawSubject()->getUser(),
            ];
        } else {
            //create

            /** @var NaturalPerson $naturalPerson */
            $naturalPerson = $this->createNaturalPerson($data['user'], $data);

            $result = [
                'civilLawSubject' => $naturalPerson->getCivilLawSubject(),
                'naturalPerson' => $naturalPerson,
                'user' => $data['user']
            ];
        }

        return $result;
    }

    /**
     * @param $data
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function saveFormDataSoleProprietor($data)
    {
        if (isset($data['idSoleProprietor'])) {

            // update
            if ((int) $data['idSoleProprietor'] <= 0) {

                throw new \Exception('bad data provided: not found sole proprietor id');
            }

            /** @var SoleProprietor $soleProprietor */
            $soleProprietor = $this->soleProprietorManager->getSoleProprietorById((int)$data['idSoleProprietor']);

            if($soleProprietor === null) {

                throw new \Exception('bad data provided: not found sole proprietor by id');
            }

            $soleProprietor = $this->updateSoleProprietor($soleProprietor, $data);

            $result = [
                'civilLawSubject'   => $soleProprietor->getCivilLawSubject(),
                'soleProprietor'    => $soleProprietor,
                'user'              => $soleProprietor->getCivilLawSubject()->getUser(),
            ];

        } else {

            // create
            /** @var SoleProprietor $soleProprietor */
            $soleProprietor = $this->createSoleProprietor($data['user'], $data);

            $result = [
                'civilLawSubject'   => $soleProprietor->getCivilLawSubject(),
                'soleProprietor'    => $soleProprietor,
                'user'              => $data['user']
            ];
        }

        return $result;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @return string
     */
    public function getCivilLawSubjectName(CivilLawSubject $civilLawSubject)
    {
        if ($civilLawSubject->getNaturalPerson() && $civilLawSubject->getNaturalPerson() instanceof NaturalPerson) {
            /** @var NaturalPerson $naturalPerson */
            $naturalPerson = $civilLawSubject->getNaturalPerson();

            $name = $naturalPerson->getLastName() . ' ' . $naturalPerson->getFirstName();

            if ($naturalPerson->getSecondaryName()) {
                $name .= ' ' . $naturalPerson->getSecondaryName();
            }

            return $name;
        }

        if ($civilLawSubject->getSoleProprietor() && $civilLawSubject->getSoleProprietor() instanceof SoleProprietor) {

            return $civilLawSubject->getSoleProprietor()->getName();
        }

        if ($civilLawSubject->getLegalEntity() && $civilLawSubject->getLegalEntity() instanceof LegalEntity) {

            return $civilLawSubject->getLegalEntity()->getName();
        }
    }

    /**
     * @param $search_data
     * @return array
     */
    public function searchCivilLawSubjects($search_data): array
    {
        return $this->entityManager->getRepository(CivilLawSubject::class)
            ->searchCivilLawSubjects($search_data);
    }

    /**
     * @param array $civilLawSubjects
     * @return array
     */
    public function getSearchCivilLawSubjectOutput(array $civilLawSubjects): array
    {
        $dataOutput = [];

        /** @var CivilLawSubject $civilLawSubject */
        foreach ($civilLawSubjects as $civilLawSubject) {
            $dataOutput[$civilLawSubject->getId()] = $this->getCivilLawSubjectOutput($civilLawSubject);
        }

        return $dataOutput;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @return array
     */
    public function getCivilLawSubjectOutput(CivilLawSubject $civilLawSubject): array
    {
        $civilLawSubjectOutput = null;

        $naturalPerson = $civilLawSubject->getNaturalPerson();
        $legalEntity = $civilLawSubject->getLegalEntity();
        $soleProprietor = $civilLawSubject->getSoleProprietor();

        if ($naturalPerson !== null) {
            $civilLawSubjectOutput = [
                'type' => 'natural_person',
                'id' => $civilLawSubject->getId(),
                'name' => $naturalPerson->getLastName() . ' ' . $naturalPerson->getFirstName() . ' ' . $naturalPerson->getSecondaryName()
            ];
        } elseif ($legalEntity !== null) {
            $civilLawSubjectOutput = [
                'type' => 'legal_entity',
                'id' => $civilLawSubject->getId(),
                'name' => $legalEntity->getName()
            ];
        } elseif ($soleProprietor !== null) {
            $civilLawSubjectOutput = [
                'type' => 'sole_proprietor',
                'id' => $civilLawSubject->getId(),
                'name' => $soleProprietor->getName()
            ];
        }

        return $civilLawSubjectOutput;
    }
}