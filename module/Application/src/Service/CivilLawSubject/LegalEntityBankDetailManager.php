<?php
namespace Application\Service\CivilLawSubject;

use Application\Entity\LegalEntityBankDetail;
use Application\Service\HydratorManager;
use Application\Entity\User;
use Doctrine\ORM\EntityManager;

class LegalEntityBankDetailManager extends HydratorManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var
     */
    protected $config;

    /**
     * NaturalPersonDriverLicenseManager constructor.
     * @param EntityManager $entityManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->config = $config;
    }

    /**
     * @param $idBankDetail
     * @return mixed
     * @throws \Exception
     */
    public function getBankDetailById($idBankDetail)
    {
        /** @var LegalEntityBankDetail $bankDetail */
        $bankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->getLegalEntityBankDetailBasedOnPatternById((int) $idBankDetail);

        return $bankDetail;
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @return array
     * @throws \Exception
     */
    public function getCollectionBankDetail(User $user, array $params, bool $isOperator = false)
    {
        if($isOperator) {
            $collectionBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
                ->getBankDetailBased($params);
        } elseif ($user && $user instanceof User) {
            $collectionBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
                ->getBankDetailBasedByUserId($user->getId(), $params);
        } else {

            throw new \Exception('No data provided for getting bank detail collection');
        }

        return $collectionBankDetail;
    }

    /**
     * @param User $user
     * @return null
     */
    public function getAllowedBankDetailByUser(User $user){
        $allowedBankDetails = null;
        if ( $user && $user instanceof User){
            $allowedBankDetails = $this->entityManager->getRepository(LegalEntityBankDetail::class)
                ->getBankDetailBasedByUserId($user->getId());
        }
        return ($allowedBankDetails && !empty($allowedBankDetails)) ? $allowedBankDetails : null;
    }

    /**
     * @param LegalEntityBankDetail $bankDetail
     * @return array
     */
    public function getBankDetailsOutput(LegalEntityBankDetail $bankDetail)
    {
        $bankDetailsOutput = [];

        $bankDetailsOutput['id']                     = $bankDetail->getId();
        $bankDetailsOutput['name']                   = $bankDetail->getName();
        $bankDetailsOutput['checking_account']       = $bankDetail->getCheckingAccount();
        $bankDetailsOutput['correspondent_account']  = $bankDetail->getCorrespondentAccount();
        $bankDetailsOutput['bic']                    = $bankDetail->getBic();

        return $bankDetailsOutput;
    }

    /**
     * @param $bankDetails
     * @return array
     */
    public function extractBankDetailCollection($bankDetails)
    {
        $bankDetailsOutput = [];

        foreach ($bankDetails as $bankDetail) {

            $bankDetailsOutput[] = $this->getBankDetailsOutput($bankDetail);
        }

        return $bankDetailsOutput;
    }
}