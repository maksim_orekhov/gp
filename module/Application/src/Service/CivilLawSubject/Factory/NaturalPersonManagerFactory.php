<?php
namespace Application\Service\CivilLawSubject\Factory;

use Application\Hydrator\NaturalPersonHydrator;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;

class NaturalPersonManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $naturalPersonDocumentManager = $container->get(NaturalPersonDocumentManager::class);
        $naturalPersonHydrator = $container->get(NaturalPersonHydrator::class);
        $config = $container->get('Config');

        return new NaturalPersonManager(
            $entityManager,
            $naturalPersonDocumentManager,
            $naturalPersonHydrator,
            $config
        );
    }
}