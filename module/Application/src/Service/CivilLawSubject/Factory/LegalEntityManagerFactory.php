<?php
namespace Application\Service\CivilLawSubject\Factory;

use Application\Hydrator\LegalEntityHydrator;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Application\Service\CivilLawSubject\LegalEntityDocumentManager;

class LegalEntityManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $legalEntityDocumentManager = $container->get(LegalEntityDocumentManager::class);
        $legalEntityHydrator = $container->get(LegalEntityHydrator::class);

        return new LegalEntityManager($entityManager, $legalEntityDocumentManager, $legalEntityHydrator);
    }
}