<?php
namespace Application\Service\CivilLawSubject\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\CivilLawSubject\LegalEntityDocumentManager;
use ModuleFileManager\Service\FileManager;

class LegalEntityDocumentManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $fileManager = $container->get(FileManager::class);
        $config = $container->get('Config');

        return new LegalEntityDocumentManager(
            $entityManager,
            $fileManager,
            $config
        );
    }
}