<?php
namespace Application\Service\CivilLawSubject\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use ModuleFileManager\Service\FileManager;

class NaturalPersonDocumentManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $fileManager = $container->get(FileManager::class);
        $config = $container->get('Config');

        return new NaturalPersonDocumentManager(
            $entityManager,
            $fileManager,
            $config
        );
    }
}