<?php
namespace Application\Service\CivilLawSubject\Factory;

use Application\Service\CivilLawSubject\CivilLawSubjectPolitics;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use Application\Service\CivilLawSubject\SoleProprietorManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;

class CivilLawSubjectManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CivilLawSubjectManager|object
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $naturalPersonManager = $container->get(NaturalPersonManager::class);
        $soleProprietorManager = $container->get(SoleProprietorManager::class);
        $legalEntityManager = $container->get(LegalEntityManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $civilLawSubjectPolitics = $container->get(CivilLawSubjectPolitics::class);
        $ndsTypeManager = $container->get(NdsTypeManager::class);

        return new CivilLawSubjectManager(
            $entityManager,
            $naturalPersonManager,
            $soleProprietorManager,
            $legalEntityManager,
            $paymentMethodManager,
            $civilLawSubjectPolitics,
            $ndsTypeManager
        );
    }
}