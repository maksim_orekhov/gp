<?php
namespace Application\Service\CivilLawSubject\Factory;

use Application\Service\CivilLawSubject\SoleProprietorManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class SoleProprietorManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new SoleProprietorManager($entityManager);
    }
}