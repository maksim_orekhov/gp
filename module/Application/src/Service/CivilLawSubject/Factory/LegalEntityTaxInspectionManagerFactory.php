<?php
namespace Application\Service\CivilLawSubject\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\CivilLawSubject\LegalEntityTaxInspectionManager;

class LegalEntityTaxInspectionManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $config = $container->get('Config');

        return new LegalEntityTaxInspectionManager(
            $entityManager,
            $config
        );
    }
}