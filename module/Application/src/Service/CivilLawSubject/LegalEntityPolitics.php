<?php

namespace Application\Service\CivilLawSubject;

use Application\Entity\LegalEntity;

/**
 * Class LegalEntityPolitics
 * @package Application\Service\CivilLawSubject
 */
class LegalEntityPolitics
{
    /**
     * @param LegalEntity $legalEntity
     * @return bool
     */
    public function isLock(LegalEntity $legalEntity)
    {
        if (!$legalEntity->getCivilLawSubject()->getIsPattern()){

            return true;
        }

        return false;
    }
}