<?php

namespace Application\Service\CivilLawSubject;

use Application\Entity\CivilLawSubject;
use Application\Entity\DealAgent;
use Application\Entity\Token;
use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;

/**
 * Class CivilLawSubjectPolitics
 * @package Application\Service\CivilLawSubject
 */
class CivilLawSubjectPolitics
{
    /**
     * 1 запрешено если не pattern
     * 2 запрешено, если cls прикрепелен к сделке, которая подтверждена владельцем этого cls
     *
     * @param CivilLawSubject $civilLawSubject
     * @param EntityManager $entityManager
     * @return array
     */
    public function checkRemovalIsAllowed(CivilLawSubject $civilLawSubject, EntityManager $entityManager): array
    {
        $result = [];
        $result['allowed'] = true;
        $result['reason'] = null;

        // не pattern
        if (false === $civilLawSubject->getIsPattern()) {
            $result['allowed'] = false;
            $result['reason'] = LogicException::CIVIL_LAW_SUBJECT_REMOVAL_NOT_ALLOWED_BY_PATTERN;

            return $result;
        }
        // cls прикрепелен к сделке, которая подтверждена владельцем этого cls
        if (true === $this->isCivilLawSubjectHasDealAgentWithConfirm($civilLawSubject->getDealAgents())) {
            $result['allowed'] = false;
            $result['reason'] = LogicException::CIVIL_LAW_SUBJECT_REMOVAL_NOT_ALLOWED_BY_DEAL_BEING_CONFIRMED;

            return $result;
        }

        // cls связан с партнерским токеном (counteragent или beneficiar)
        if (true === $this->isCivilLawSubjectRelatedToPartnerToken($civilLawSubject, $entityManager)) {
            $result['allowed'] = false;
            $result['reason'] = LogicException::CIVIL_LAW_SUBJECT_REMOVAL_NOT_ALLOWED_BY_TOKEN_RELATION;

            return $result;
        }

        return $result;
    }

    /**
     * @param $data
     * @return bool
     */
    public static function isExistPolitics($data)
    {
        return in_array($data, CivilLawSubjectManager::CIVIL_LAW_SUBJECT_TYPE_NAMES);
    }

    /**
     * @param $dealAgents
     * @return bool
     */
    private function isCivilLawSubjectHasDealAgentWithConfirm($dealAgents): bool
    {
        /** @var DealAgent $dealAgent */
        foreach ($dealAgents as $dealAgent) {
            if ($dealAgent->getDealAgentConfirm()) {

                return true;
            }
        }

        return false;
    }

    /**
     * @param $dealAgents
     * @return bool
     */
    private function isCivilLawSubjectRelatedToPartnerToken(CivilLawSubject $civilLawSubject, EntityManager $entityManager): bool
    {
        $counteragentToken = $entityManager->getRepository(Token::class)
            ->findOneBy(['counteragentCivilLawSubject' => $civilLawSubject]);

        $beneficiarToken = $entityManager->getRepository(Token::class)
            ->findOneBy(['beneficiarCivilLawSubject' => $civilLawSubject]);

        return null !== $counteragentToken || null !== $beneficiarToken;
    }
}