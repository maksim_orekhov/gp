<?php
namespace Application\Service\CivilLawSubject;

use Application\Entity\LegalEntityTaxInspection;
use Application\Entity\User;
use Application\Service\HydratorManager;
use Doctrine\ORM\EntityManager;

class LegalEntityTaxInspectionManager extends HydratorManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var
     */
    protected $config;

    /**
     * NaturalPersonDriverLicenseManager constructor.
     * @param EntityManager $entityManager
     * @param $config
     */
    public function __construct(EntityManager $entityManager,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->config = $config;
    }

    /**
     * @param $idTaxInspection
     * @return mixed
     * @throws \Exception
     */
    public function getTaxInspectionById($idTaxInspection)
    {
        if ($idTaxInspection === -1 || $idTaxInspection === 0) {

            throw new \Exception('Bad parameter idTaxInspection');
        }

        /** @var LegalEntityTaxInspection $taxInspection */
        $taxInspection = $this->entityManager->getRepository(LegalEntityTaxInspection::class)
            ->getLegalEntityTaxInspectionBasedOnPatternById($idTaxInspection);

        if ($taxInspection === null) {

            throw new \Exception('Not found tax inspection by id');
        }
        return $taxInspection;
    }

    /**
     * @param User $user
     * @param array $params
     * @param bool $isOperator
     * @return array
     * @throws \Exception
     */
    public function getCollectionTaxInspection(User $user, array $params, bool $isOperator = false)
    {
        if($isOperator) {
            $collectionTaxInspection = $this->entityManager->getRepository(LegalEntityTaxInspection::class)
                ->getTaxInspectionBased($params);
        } elseif ($user && $user instanceof User) {
            $collectionTaxInspection = $this->entityManager->getRepository(LegalEntityTaxInspection::class)
                ->getTaxInspectionBasedByUserId($user->getId(), $params);
        } else {

            throw new \Exception('No data provided for getting tax inspection collection');
        }

        return $collectionTaxInspection;
    }

    /**
     * @param User $user
     * @return null
     */
    public function getAllowedTaxInspectionByUser(User $user){
        $allowedTaxInspections = null;
        if ( $user && $user instanceof User){
            $allowedTaxInspections = $this->entityManager->getRepository(LegalEntityTaxInspection::class)
                ->getTaxInspectionBasedByUserId($user->getId());
        }

        return ($allowedTaxInspections && !empty($allowedTaxInspections)) ? $allowedTaxInspections : null;
    }

    /**
     * @param LegalEntityTaxInspection $taxInspection
     * @return array
     */
    public function getTaxInspectionsOutput(LegalEntityTaxInspection $taxInspection)
    {
        $taxInspectionsOutput = [];

        $taxInspectionsOutput['id']                 = $taxInspection->getId();
        $taxInspectionsOutput['serial_number']      = $taxInspection->getSerialNumber();
        $taxInspectionsOutput['date_issued']        = $taxInspection->getDateIssued()->format('d.m.Y');
        $taxInspectionsOutput['issue_organisation'] = $taxInspection->getIssueOrganisation();
        $taxInspectionsOutput['issue_registrar']    = $taxInspection->getIssueRegistrar();
        $taxInspectionsOutput['date_registered']    = $taxInspection->getDateRegistered()->format('d.m.Y');
        $taxInspectionsOutput['files'] = null;

        if ( count($taxInspection->getFiles()) > 0){
            foreach ($taxInspection->getFiles() as $file){
                $taxInspectionsOutput['files'][]=[
                    'civilId' => $taxInspection->getLegalEntityDocument()->getLegalEntity()->getCivilLawSubject()->getId(),
                    'id' => $file->getId(),
                    'name' => $file->getOriginName(),
                    'type' => substr($file->getOriginName(), strrpos($file->getOriginName(), '.') + 1),
                ];
            }
        }

        return $taxInspectionsOutput;
    }

    /**
     * @param $taxInspections
     * @return array
     */
    public function extractTaxInspectionsCollection($taxInspections)
    {
        $taxInspectionsOutput = [];

        foreach ($taxInspections as $taxInspection) {

            $taxInspectionsOutput[] = $this->getTaxInspectionsOutput($taxInspection);
        }

        return $taxInspectionsOutput;
    }
}