<?php

namespace Application\Service\Dispute;

/**
 * Class RefundPolitics
 * @package Application\Service\Dispute
 */
class TribunalPolitics
{
    /**
     * @return bool
     */
    public function isTribunalDeclineOverlimit(array $negative_confirms): bool
    {

        return count($negative_confirms) >= TribunalManager::TRIBUNAL_REQUEST_REFUSES_LIMIT;
    }
}