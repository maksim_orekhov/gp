<?php
namespace Application\Service\Dispute\Factory;

use Application\Provider\Mail\DisputeNotificationSender;
use Application\Service\Dispute\DisputePolitics;
use Application\Service\Dispute\DisputeStatus;
use Application\Service\Dispute\TribunalManager;
use Application\Service\Dispute\TribunalPolitics;
use Application\Service\Payment\PaymentPolitics;
use Core\Service\TwigRenderer;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class TribunalManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return TribunalManager|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $disputePolitics = $container->get(DisputePolitics::class);
        $rbacManager = $container->get(RbacManager::class);
        $paymentPolitics = $container->get(PaymentPolitics::class);
        $tribunalPolitics = $container->get(TribunalPolitics::class);
        $twigRenderer = $container->get(TwigRenderer::class);
        $paymentOrderManager = $container->get(PaymentOrderManager::class);
        $disputeStatus = $container->get(DisputeStatus::class);
        $config = $container->get('Config');
        $disputeNotificationSender = new DisputeNotificationSender($twigRenderer, $config);

        return new TribunalManager(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $disputePolitics,
            $rbacManager,
            $paymentPolitics,
            $tribunalPolitics,
            $disputeNotificationSender,
            $paymentOrderManager,
            $disputeStatus,
            $config
        );
    }
}