<?php
namespace Application\Service\Dispute\Factory;

use Application\Service\Dispute\DisputeHistoryManager;
use Application\Service\Dispute\DisputeStatus;
use Application\Service\Dispute\RefundManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class DisputeHistoryManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DisputeHistoryManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $rbacManager = $container->get(RbacManager::class);
        $disputeStatus = $container->get(DisputeStatus::class);
        $refundManager = $container->get(RefundManager::class);

        return new DisputeHistoryManager(
            $rbacManager,
            $disputeStatus,
            $refundManager
        );
    }
}