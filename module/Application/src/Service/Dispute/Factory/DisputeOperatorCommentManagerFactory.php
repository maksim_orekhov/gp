<?php

namespace Application\Service\Dispute\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Dispute\DisputeOperatorCommentManager;
use ModuleMessage\Service\MessageManager;

/**
 * Class DisputeOperatorCommentManagerFactory
 * @package Application\Service\Dispute\Factory
 */
class DisputeOperatorCommentManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager  = $container->get('doctrine.entitymanager.orm_default');
        $messageManager = $container->get(MessageManager::class);

        return new DisputeOperatorCommentManager(
            $entityManager,
            $messageManager
        );
    }
}