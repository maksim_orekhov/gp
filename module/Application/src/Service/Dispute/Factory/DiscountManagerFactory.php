<?php
namespace Application\Service\Dispute\Factory;

use Application\Provider\Mail\DisputeNotificationSender;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Dispute\DiscountPolitics;
use Application\Service\Dispute\DisputePolitics;
use Application\Service\Dispute\DisputeStatus;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\Payment\PaymentPolitics;
use Core\Service\TwigRenderer;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Payment\CalculatedDataProvider;

class DiscountManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DiscountManager|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $payOffManager = $container->get(PayOffManager::class);
        $paymentPolitics = $container->get(PaymentPolitics::class);
        $disputePolitics = $container->get(DisputePolitics::class);
        $discountPolitics = $container->get(DiscountPolitics::class);
        $rbacManager = $container->get(RbacManager::class);
        $twigRenderer = $container->get(TwigRenderer::class);
        $calculatedDataProvider = $container->get(CalculatedDataProvider::class);
        $paymentOrderManager = $container->get(PaymentOrderManager::class);
        $disputeStatus = $container->get(DisputeStatus::class);
        $config = $container->get('Config');
        $disputeNotificationSender = new DisputeNotificationSender($twigRenderer, $config);

        return new DiscountManager(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $paymentMethodManager,
            $payOffManager,
            $paymentPolitics,
            $disputePolitics,
            $discountPolitics,
            $rbacManager,
            $disputeNotificationSender,
            $calculatedDataProvider,
            $paymentOrderManager,
            $disputeStatus,
            $config
        );
    }
}