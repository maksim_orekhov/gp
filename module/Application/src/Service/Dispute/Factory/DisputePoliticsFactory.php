<?php

namespace Application\Service\Dispute\Factory;

use Application\Service\Dispute\DisputePolitics;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

/**
 * Class DisputePoliticsFactory
 * @package Application\Service\Dispute\Factory
 */
class DisputePoliticsFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new DisputePolitics();
    }
}