<?php

namespace Application\Service\Dispute\Factory;

use Application\Service\Dispute\Status\ArbitrageRequestAcceptedStatus;
use Application\Service\Dispute\Status\ArbitrageRequestRejectedStatus;
use Application\Service\Dispute\Status\ArbitrageRequestStatus;
use Application\Service\Dispute\Status\ClosedStatus;
use Application\Service\Dispute\Status\DiscountRequestAcceptedStatus;
use Application\Service\Dispute\Status\DiscountRequestRejectedStatus;
use Application\Service\Dispute\Status\DiscountRequestStatus;
use Application\Service\Dispute\Status\InitialStatus;
use Application\Service\Dispute\Status\RefundIsMadeStatus;
use Application\Service\Dispute\Status\RefundRequestAcceptedStatus;
use Application\Service\Dispute\Status\RefundRequestRejectedStatus;
use Application\Service\Dispute\Status\RefundRequestStatus;
use Application\Service\Dispute\Status\RefundStatus;
use Application\Service\Dispute\Status\TribunalRequestAcceptedStatus;
use Application\Service\Dispute\Status\TribunalRequestRejectedStatus;
use Application\Service\Dispute\Status\TribunalRequestStatus;
use Application\Service\Dispute\Status\TribunalStatus;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Dispute\DisputeStatus;

/**
 * Class DisputeStatusFactory
 * @package Application\Service\Deal\Factory
 */
class DisputeStatusFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $initialStatus = $container->get(InitialStatus::class);
        $discountRequestStatus = $container->get(DiscountRequestStatus::class);
        $refundRequestStatus = $container->get(RefundRequestStatus::class);
        $refundRequestAcceptedStatus = $container->get(RefundRequestAcceptedStatus::class);
        $discountRequestAcceptedStatus = $container->get(DiscountRequestAcceptedStatus::class);
        $refundRequestRejectedStatus = $container->get(RefundRequestRejectedStatus::class);
        $discountRequestRejectedStatus = $container->get(DiscountRequestRejectedStatus::class);
        $refundStatus = $container->get(RefundStatus::class);
        $refundIsMadeStatus = $container->get(RefundIsMadeStatus::class);
        $arbitrageRequestStatus = $container->get(ArbitrageRequestStatus::class);
        $arbitrageRequestRejectedStatus = $container->get(ArbitrageRequestRejectedStatus::class);
        $arbitrageRequestAcceptedStatus = $container->get(ArbitrageRequestAcceptedStatus::class);
        $tribunalRequestStatus = $container->get(TribunalRequestStatus::class);
        $tribunalRequestRejectedStatus = $container->get(TribunalRequestRejectedStatus::class);
        $tribunalRequestAcceptedStatus = $container->get(TribunalRequestAcceptedStatus::class);
        $tribunalStatus = $container->get(TribunalStatus::class);
        $closedStatus = $container->get(ClosedStatus::class);

        return new DisputeStatus(
            $initialStatus,
            $discountRequestStatus,
            $refundRequestStatus,
            $refundRequestAcceptedStatus,
            $discountRequestAcceptedStatus,
            $refundRequestRejectedStatus,
            $discountRequestRejectedStatus,
            $refundStatus,
            $refundIsMadeStatus,
            $arbitrageRequestStatus,
            $arbitrageRequestRejectedStatus,
            $arbitrageRequestAcceptedStatus,
            $tribunalRequestStatus,
            $tribunalRequestRejectedStatus,
            $tribunalRequestAcceptedStatus,
            $tribunalStatus,
            $closedStatus
        );
    }
}