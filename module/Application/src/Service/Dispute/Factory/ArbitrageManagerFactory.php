<?php
namespace Application\Service\Dispute\Factory;

use Application\Provider\Mail\DisputeNotificationSender;
use Application\Service\Dispute\ArbitrageManager;
use Application\Service\Dispute\ArbitragePolitics;
use Application\Service\Dispute\DisputePolitics;
use Application\Service\Dispute\DisputeStatus;
use Application\Service\Payment\PaymentPolitics;
use Core\Service\TwigRenderer;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class ArbitrageManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return ArbitrageManager|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $paymentPolitics = $container->get(PaymentPolitics::class);
        $disputePolitics = $container->get(DisputePolitics::class);
        $arbitragePolitics = $container->get(ArbitragePolitics::class);
        $rbacManager = $container->get(RbacManager::class);
        $paymentOrderManager = $container->get(PaymentOrderManager::class);
        $config = $container->get('Config');
        $twigRenderer = $container->get(TwigRenderer::class);
        $disputeStatus = $container->get(DisputeStatus::class);
        $disputeNotificationSender = new DisputeNotificationSender($twigRenderer, $config);

        return new ArbitrageManager(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $paymentPolitics,
            $disputePolitics,
            $arbitragePolitics,
            $rbacManager,
            $paymentOrderManager,
            $disputeNotificationSender,
            $disputeStatus,
            $config
        );
    }
}