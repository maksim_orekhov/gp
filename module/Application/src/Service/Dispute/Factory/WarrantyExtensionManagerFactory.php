<?php

namespace Application\Service\Dispute\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Dispute\WarrantyExtensionManager;

/**
 * Class WarrantyExtensionManagerFactory
 * @package Application\Service\Dispute\Factory
 */
class WarrantyExtensionManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new WarrantyExtensionManager(
            $entityManager
        );
    }
}