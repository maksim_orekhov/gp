<?php

namespace Application\Service\Dispute\Factory;

use Application\Provider\Mail\DisputeNotificationSender;
use Application\Service\Deal\DealStatus;
use Application\Service\Dispute\ArbitrageManager;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Dispute\DisputeStatus;
use Application\Service\Dispute\RefundManager;
use Application\Service\Dispute\TribunalManager;
use Application\Service\Payment\PaymentMethodManager;
use Core\Service\TwigRenderer;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleRbac\Service\RbacManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Dispute\WarrantyExtensionManager;
use ModuleFileManager\Service\FileManager;
use Application\Service\Dispute\DisputePolitics;

/**
 * Class DisputeManagerFactory
 * @package Application\Service\Dispute\Factory
 */
class DisputeManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $warrantyExtensionManager = $container->get(WarrantyExtensionManager::class);
        $fileManager = $container->get(FileManager::class);
        $paymentMethodManager = $container->get(PaymentMethodManager::class);
        $arbitrageManager = $container->get(ArbitrageManager::class);
        $refundManager = $container->get(RefundManager::class);
        $discountManager = $container->get(DiscountManager::class);
        $tribunalManager = $container->get(TribunalManager::class);
        $disputePolitics = $container->get(DisputePolitics::class);
        $config = $container->get('Config');
        $twigRenderer = $container->get(TwigRenderer::class);
        $disputeNotificationSender = new DisputeNotificationSender($twigRenderer, $config);
        $dealStatus = $container->get(DealStatus::class);
        $disputeStatus = $container->get(DisputeStatus::class);

        return new DisputeManager(
            $entityManager,
            $userManager,
            $baseAuthManager,
            $rbacManager,
            $warrantyExtensionManager,
            $fileManager,
            $paymentMethodManager,
            $arbitrageManager,
            $refundManager,
            $discountManager,
            $tribunalManager,
            $disputePolitics,
            $disputeNotificationSender,
            $dealStatus,
            $disputeStatus,
            $config
        );
    }
}