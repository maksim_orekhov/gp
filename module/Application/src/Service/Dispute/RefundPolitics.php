<?php

namespace Application\Service\Dispute;

/**
 * Class RefundPolitics
 * @package Application\Service\Dispute
 */
class RefundPolitics
{
    /**
     * @return bool
     */
    public function isRefundDeclineOverlimit(array $negative_confirms): bool
    {

        return count($negative_confirms) >= RefundManager::REFUND_REQUEST_REFUSES_LIMIT;
    }
}