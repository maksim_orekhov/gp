<?php

namespace Application\Service\Dispute;

use Application\Controller\DisputeController;
use Application\Entity\Dispute;
use Application\Entity\WarrantyExtension;
use Application\Form\DisputeCloseForm;
use Core\Exception\LogicException;

/**
 * Class WarrantyExtensionManager
 * @package Application\Service\Dispute
 */
class WarrantyExtensionManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * DisputeManager constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $id
     * @return WarrantyExtension
     * @throws \Exception
     */
    public function getWarrantyExtension($id)
    {
        /** @var WarrantyExtension $warrantyExtension */
        $warrantyExtension = $this->entityManager->find(WarrantyExtension::class, $id);

        if(!$warrantyExtension || !($warrantyExtension instanceof WarrantyExtension)) {
            // If no deal found
            throw new \Exception('WarrantyExtension with such id not found');
        }

        return $warrantyExtension;
    }

    /**
     * @param WarrantyExtension $warrantyExtension
     * @return array
     */
    public function getWarrantyExtensionOutput(WarrantyExtension $warrantyExtension)
    {
        $warrantyExtensionOutput = [];

        $warrantyExtensionOutput['id'] = $warrantyExtension->getId();
        $warrantyExtensionOutput['days'] = $warrantyExtension->getDays();
        $warrantyExtensionOutput['created'] = $warrantyExtension->getCreated()->format('d.m.Y H.i');

        return $warrantyExtensionOutput;
    }

    /**
     * @param $warrantyExtensions
     * @return array
     */
    public function extractWarrantyExtensionCollection($warrantyExtensions)
    {
        $warrantyExtensionsOutput = [];

        foreach ($warrantyExtensions as $warrantyExtension) {

            $warrantyExtensionsOutput[] = $this->getWarrantyExtensionOutput($warrantyExtension);
        }

        return $warrantyExtensionsOutput;
    }

    /**
     * @param Dispute $dispute
     * @param int $number_of_days
     * @return WarrantyExtension
     * @throws \Exception
     */
    public function addWarrantyExtension(Dispute $dispute, int $number_of_days)
    {
        try {
            /** @var WarrantyExtension $warrantyExtension */
            $warrantyExtension= $this->createWarrantyExtension($dispute, $number_of_days);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $warrantyExtension;
    }

    /**
     * @param Dispute $dispute
     * @param int $number_of_days
     * @return WarrantyExtension
     * @throws \Exception
     */
    public function createWarrantyExtension(Dispute $dispute, int $number_of_days)
    {
        $disputeCycle = DisputeManager::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || ($disputeCycle->getClosed() !== null && !$dispute->isClosed())) {
            throw new LogicException(null, LogicException::DISPUTE_ACTIVE_CYCLE_NOT_FOUND);
        }

        $warrantyExtension = new WarrantyExtension();
        $warrantyExtension->setDays($number_of_days);
        $warrantyExtension->setCreated(new \DateTime());
        $warrantyExtension->setDispute($dispute);
        $warrantyExtension->setDisputeCycle($disputeCycle);

        // Prepare object
        $this->entityManager->persist($warrantyExtension);
        $disputeCycle->addWarrantyExtension($warrantyExtension);
        $this->entityManager->persist($disputeCycle);
        // Save to DB
        $this->entityManager->flush();

        if (!$warrantyExtension->getId()) {
            // Write log
            logMessage(
                'WarrantyExtension creation error: (Dispute id - ' .  $dispute->getId() . ')',
                DisputeController::LOG_FILE_NAME
            );

            throw new \Exception('New WarrantyExtension creation failed');
        }

        return $warrantyExtension;
    }
}