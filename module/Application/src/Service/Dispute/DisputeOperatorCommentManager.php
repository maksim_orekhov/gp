<?php

namespace Application\Service\Dispute;

use Application\Entity\Dispute;
use Application\Entity\User;
use ModuleMessage\Entity\Message;
use Application\Form\DisputeCommentForm;
use Application\Service\CommentManager;
use Application\Entity\Role;
use ModuleMessage\Service\MessageManager;

/**
 * Class DisputeOperatorCommentManager
 * @package Application\Service\Dispute
 */
class DisputeOperatorCommentManager extends CommentManager
{
    /**
     * DealCommentManager constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param MessageManager $messageManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                MessageManager $messageManager)
    {
        parent::__construct($entityManager, $messageManager);
    }

    /**
     * @param Dispute $dispute
     * @param null $currentUser
     * @return array
     */
    public function getDisputeOperatorsCommentsOutput(Dispute $dispute, $currentUser = null)
    {
        /** @var \Application\Entity\Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(['name' => 'Operator']);

        $comments = $this->entityManager->getRepository(Message::class)
            ->getDisputeOperatorsComments($dispute, $role);

        $commentOutput = [];

        if ($comments) {
            $commentOutput = $this->getCommentCollectionOutput($comments, $currentUser);
        }

        return $commentOutput;
    }

    /**
     * @param User $author
     * @param array $data
     * @param Dispute $dispute
     * @return Message
     * @throws \Exception
     */
    public function createDisputeComment(User $author, array $data, Dispute $dispute)
    {
        try {
            /** @var Message $comment */
            $comment = $this->addComment($data['text'], $author);
        }
        catch (\Throwable $t) {
            // Write log
            logMessage('Creation error: ' . $t->getMessage(), 'dispute-comments');

            throw new \Exception($t->getMessage());
        }

        $dispute->addMessage($comment);

        $this->entityManager->flush($dispute);

        return $comment;
    }

    /**
     * @param Message $comment
     * @throws \Exception
     */
    public function removeDisputeComment(Message $comment)
    {
        /** @var Dispute $dispute */
        $dispute = $this->getDisputeByComment($comment);

        // @TODO Возможно трансакция. Если не удалось удалить комментарий из БД, то и не удалять из коллекции
        // Remove comment from collection
        $dispute->removeMessage($comment);

        try {
            // Try to remove comment from DB // Can throw Exception
            $this->messageManager->removeMessage($comment);
        }
        catch (\Throwable $t) {
            // Write log
            logMessage('Removal error: ' . $t->getMessage(), 'dispute-comments');

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param Message $comment
     * @return Dispute
     */
    public function getDisputeByComment(Message $comment)
    {
        /** @var Dispute $dispute */
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->getDisputeByComment($comment);

        return $dispute;
    }

    /**
     * @param DisputeCommentForm $form
     * @param Message $comment
     * @param Dispute $dispute
     * @return DisputeCommentForm
     */
    public function setDisputeCommentFormData(DisputeCommentForm $form, Message $comment, Dispute $dispute)
    {
        $form->setData([
            'text'          => $comment->getText(),
            'dispute_id'    => $dispute->getId(),
        ]);

        return $form;
    }
}