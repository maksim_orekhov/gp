<?php

namespace Application\Service\Dispute;

/**
 * Class DiscountPolitics
 * @package Application\Service\Dispute
 */
class DiscountPolitics
{
    /**
     * @param array $negative_confirms
     * @return bool
     *
     * Нужно возвращать true, если уже есть два конфирма с полем confirm = false (то есть два отказа)
     */
    public function isDiscountDeclineOverlimit(array $negative_confirms)
    {

        return count($negative_confirms) >= DiscountManager::DISCOUNT_REQUEST_REFUSES_LIMIT;
    }
}