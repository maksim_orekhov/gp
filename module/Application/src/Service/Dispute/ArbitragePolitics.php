<?php

namespace Application\Service\Dispute;

/**
 * Class ArbitragePolitics
 * @package Application\Service\Dispute
 */
class ArbitragePolitics
{
    /**
     * @return bool
     */
    public function isArbitrageDeclineOverlimit(array $negative_confirms): bool
    {

        return \count($negative_confirms) >= ArbitrageManager::ARBITRAGE_REQUEST_REFUSES_LIMIT;
    }
}