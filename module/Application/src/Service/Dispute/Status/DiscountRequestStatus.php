<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\Interfaces\DisputeSolutionRequestInterface;

/**
 * Class DiscountRequestStatus
 * @package Application\Service\Dispute\Status
 */
class DiscountRequestStatus extends BaseDisputeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     *
     * предложено решение - спор открыт, есть связанная сущность DiscountRequest,
     * отсутствуют другие сущности (TribunalRequest, ArbitrageRequest)
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }

        /** @var DisputeSolutionRequestInterface $lastRequest */
        $lastRequest = $this->getLastRequest($dispute);

        if (!$lastRequest instanceof DiscountRequest) {

            return false;
        }

        if (null === $lastRequest->getConfirm()) {

            return true;
        }

        return false;
    }
}