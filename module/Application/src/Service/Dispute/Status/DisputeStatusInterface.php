<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\Dispute;

/**
 * Interface DisputeStatusInterface
 * @package Application\Service\Dispute\Status
 */
interface DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return mixed
     */
    public function isDisputeInStatus(Dispute $dispute);
}