<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\Dispute;

/**
 * Class ClosedStatus
 * @package Application\Service\Dispute\Status
 */
class ClosedStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return true;
        }

        return false;
    }
}