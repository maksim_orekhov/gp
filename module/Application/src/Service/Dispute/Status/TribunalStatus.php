<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\Dispute;

/**
 * Class TribunalRequestStatus
 * @package Application\Service\Dispute\Status
 */
class TribunalStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }

        if (null === $dispute->getTribunal()) {

            return false;
        }

        return true;
    }
}