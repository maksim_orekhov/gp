<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\ArbitrageConfirm;
use Application\Entity\ArbitrageRequest;
use Application\Entity\Dispute;
use Application\Entity\Interfaces\DisputeSolutionRequestInterface;

/**
 * Class ArbitrageRequestAcceptedStatus
 * @package Application\Service\Dispute\Status
 */
class ArbitrageRequestAcceptedStatus extends BaseDisputeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }

        /** @var DisputeSolutionRequestInterface $lastRequest */
        $lastRequest = $this->getLastRequest($dispute);

        if (!$lastRequest instanceof ArbitrageRequest) {

            return false;
        }

        /** @var ArbitrageConfirm|null $arbitrageConfirm */
        $arbitrageConfirm = $lastRequest->getConfirm();

        return $arbitrageConfirm && $arbitrageConfirm->isStatus();
    }
}