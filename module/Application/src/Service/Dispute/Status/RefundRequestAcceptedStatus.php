<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\Dispute;
use Application\Entity\RefundConfirm;
use Application\Entity\RefundRequest;
use Application\Entity\Interfaces\DisputeSolutionRequestInterface;

/**
 * RefundRequestAcceptedStatus
 * @package Application\Service\Dispute\Status
 */
class RefundRequestAcceptedStatus extends BaseDisputeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     *
     * предложение принято - есть DiscountRequest или RefundRequest
     * и связанные с ними DiscountConfirm.status = true или RefundConfirm.status = true
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }

        /** @var DisputeSolutionRequestInterface $lastRequest */
        $lastRequest = $this->getLastRequest($dispute);

        if (!$lastRequest instanceof RefundRequest) {

            return false;
        }

        /** @var RefundConfirm $refundConfirm */
        $refundConfirm = $lastRequest->getConfirm();

        return $refundConfirm && $refundConfirm->isStatus();
    }
}