<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\Dispute;
use Application\Entity\RefundConfirm;
use Application\Entity\RefundRequest;
use Application\Entity\Interfaces\DisputeSolutionRequestInterface;

/**
 * Class RefundRequestRejectedStatus
 * @package Application\Service\Dispute\Status
 *
 */
class RefundRequestRejectedStatus extends BaseDisputeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     *
     * предложение отклонено - есть DiscountRequest или RefundRequest
     * и связанные с ними DiscountConfirm.status = false или RefundConfirm.status = false
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }

        /** @var DisputeSolutionRequestInterface $lastRequest */
        $lastRequest = $this->getLastRequest($dispute);

        if (!$lastRequest instanceof RefundRequest) {

            return false;
        }

        /** @var RefundConfirm $refundConfirm */
        $refundConfirm = $lastRequest->getConfirm();

        return $refundConfirm && !$refundConfirm->isStatus();
    }
}