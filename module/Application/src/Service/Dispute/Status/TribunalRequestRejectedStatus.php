<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\ArbitrageRequest;
use Application\Entity\Dispute;
use Application\Entity\TribunalConfirm;
use Application\Entity\TribunalRequest;
use Application\Entity\Interfaces\DisputeSolutionRequestInterface;

/**
 * Class TribunalRequestRejectedStatus
 * @package Application\Service\Dispute\Status
 */
class TribunalRequestRejectedStatus extends BaseDisputeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }
        /** @var DisputeSolutionRequestInterface $lastRequest */
        $lastRequest = $this->getLastRequest($dispute);

        if (!$lastRequest instanceof TribunalRequest) {

            return false;
        }

        /** @var TribunalConfirm $tribunalConfirm */
        $tribunalConfirm = $lastRequest->getConfirm();

        return $tribunalConfirm && !$tribunalConfirm->isStatus();
    }
}