<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\Dispute;
use Application\Entity\RefundRequest;
use Application\Entity\Interfaces\DisputeSolutionRequestInterface;

/**
 * Class RefundRequestStatus
 * @package Application\Service\Dispute\Status
 */
class RefundRequestStatus extends BaseDisputeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     *
     * предложено решение - спор открыт, есть связанная сущность RefundRequest,
     * отсутствуют другие сущности (TribunalRequest, ArbitrageRequest)
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }

        /** @var DisputeSolutionRequestInterface $lastRequest */
        $lastRequest = $this->getLastRequest($dispute);

        if (!$lastRequest instanceof RefundRequest) {

            return false;
        }

        if (null === $lastRequest->getConfirm()) {

            return true;
        }

        return false;
    }
}