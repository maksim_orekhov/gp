<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\ArbitrageRequest;
use Application\Entity\Dispute;
use Application\Entity\Interfaces\DisputeSolutionRequestInterface;

/**
 * Class ArbitrageRequestStatus
 * @package Application\Service\Dispute\Status
 */
class ArbitrageRequestStatus extends BaseDisputeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }

        /** @var DisputeSolutionRequestInterface $lastRequest */
        $lastRequest = $this->getLastRequest($dispute);

        if (!$lastRequest instanceof ArbitrageRequest) {

            return false;
        }

        if (null === $lastRequest->getConfirm()) {

            return true;
        }

        return false;
    }
}