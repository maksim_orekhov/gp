<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\Dispute;
use Application\Entity\Interfaces\DisputeSolutionRequestInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Stdlib\ArrayUtils;

/**
 * Class BaseDisputeStatus
 * @package Application\Service\Dispute\Status
 */
class BaseDisputeStatus
{
    /**
     * @param Dispute $dispute
     * @return DisputeSolutionRequestInterface|null
     */
    protected function getLastRequest(Dispute $dispute)
    {
        /** @var ArrayCollection $discountRequests */
        $discountRequests = $dispute->getDiscountRequests();
        /** @var ArrayCollection $refundRequests */
        $refundRequests = $dispute->getRefundRequests();
        /** @var ArrayCollection $arbitrageRequests */
        $arbitrageRequests = $dispute->getArbitrageRequests();
        /** @var ArrayCollection $tribunalRequests */
        $tribunalRequests = $dispute->getTribunalRequests();

        /** @var array $requests */
        $requests = ArrayUtils::merge(
            $tribunalRequests->toArray(),
            ArrayUtils::merge(
                $arbitrageRequests->toArray(),
                ArrayUtils::merge(
                    $discountRequests->toArray(),
                    $refundRequests->toArray()
                )
            )
        );

        if (empty($requests)) {

            return null;
        }

        $lastRequest = null;
        $last_request_date = null;

        /** @var DisputeSolutionRequestInterface $request */
        foreach ($requests as $request) {
            if ($request->getCreated() > $last_request_date) {
                $last_request_date = $request->getCreated();
                $lastRequest = $request;
            }
        }

        return $lastRequest;
    }
}