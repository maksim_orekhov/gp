<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\DiscountConfirm;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\Interfaces\DisputeSolutionRequestInterface;

/**
 * Class DiscountRequestRejectedStatus
 * @package Application\Service\Dispute\Status
 */
class DiscountRequestRejectedStatus extends BaseDisputeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }

        /** @var DisputeSolutionRequestInterface $lastRequest */
        $lastRequest = $this->getLastRequest($dispute);

        if (!$lastRequest instanceof DiscountRequest) {

            return false;
        }

        /** @var DiscountConfirm $discountConfirm */
        $discountConfirm = $lastRequest->getConfirm();

        return $discountConfirm && !$discountConfirm->isStatus();
    }
}