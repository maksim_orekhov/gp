<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\Dispute;
use Application\Entity\TribunalRequest;
use Application\Entity\Interfaces\DisputeSolutionRequestInterface;

/**
 * Class TribunalRequestStatus
 * @package Application\Service\Dispute\Status
 */
class TribunalRequestStatus extends BaseDisputeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }
        /** @var DisputeSolutionRequestInterface $lastRequest */
        $lastRequest = $this->getLastRequest($dispute);

        if (!$lastRequest instanceof TribunalRequest) {

            return false;
        }

        if (null === $lastRequest->getConfirm()) {

            return true;
        }

        return false;
    }
}