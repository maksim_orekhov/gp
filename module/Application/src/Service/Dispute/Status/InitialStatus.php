<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\Dispute;

/**
 * Class InitialStatus
 * @package Application\Service\Dispute\Status
 */
class InitialStatus extends BaseDisputeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }

        // Сущности Arbitrage нет
        if ($dispute->getDiscount() || $dispute->getRefund() || $dispute->getTribunal()) {

            return false;
        }

        if ($this->getLastRequest($dispute)) {

            return false;
        }

        return true;
    }
}