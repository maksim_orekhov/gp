<?php

namespace Application\Service\Dispute\Status;

use Application\Entity\Dispute;
use Application\Entity\Refund;
use Application\Service\BankClient\BankClientManager;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\PayOffManager;

/**
 * Class RefundIsMadeStatus
 * @package Application\Service\Dispute\Status
 */
class RefundIsMadeStatus implements DisputeStatusInterface
{
    /**
     * @param Dispute $dispute
     * @return bool
     *
     * Сгенерирована платёжка на возврат и она вошла в файл на выплату
     */
    public function isDisputeInStatus(Dispute $dispute): bool
    {
        if ($dispute->isClosed()) {

            return false;
        }

        // Если есть Refund и у него есть Платёжка, вошедшая в файл на выплату
        if ($dispute->getRefund()
            && $this->isRefundHasOutgoingBankClientPaymentOrderRelatedToFile($dispute->getRefund())) {

            return true;
        }

        return false;
    }

    /**
     * @param $refund
     * @return bool
     */
    private function isRefundHasOutgoingBankClientPaymentOrderRelatedToFile(Refund $refund): bool
    {
        /** @var PaymentOrder $paymentOrder */
        foreach ($refund->getPaymentOrders() as $paymentOrder) {
            /** @var BankClientPaymentOrder $bankClientPaymentOrder */
            $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();

            $purpose = $bankClientPaymentOrder->getPaymentPurpose();

            if (strpos($purpose, PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_REFUND]) !== false
                && null !== $bankClientPaymentOrder->getBankClientPaymentOrderFile()) {

                return true;
            }
        }

        return false;
    }
}