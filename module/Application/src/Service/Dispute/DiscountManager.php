<?php

namespace Application\Service\Dispute;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\Discount;
use Application\Entity\DiscountConfirm;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\User;
use Application\Provider\Mail\DisputeNotificationSender;
use Application\Service\Payment\CalculatedDataProvider;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\Payment\PaymentPolitics;
use Application\Service\Dispute\DisputeManager;
use Core\Exception\LogicException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleCode\Service\SmsStrategyContext;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use ModuleRbac\Service\RbacManager;
use Zend\Paginator\Adapter;
use Zend\Paginator\Paginator;

/**
 * Class DiscountManager
 * @package Application\Service\Dispute
 */
class DiscountManager
{
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    const DISCOUNT_REQUEST_REFUSES_LIMIT = 3;

    /**
     * notify params
     */
    private $email_notify_discount_provided;
    private $sms_notify_discount_provided;
    private $sms_notify_discount_request;
    private $email_notify_discount_request;
    private $sms_notify_discount_confirm;
    private $email_notify_discount_confirm;

    /**
     * $var DisputeManager
     */
    private $disputeManager;

    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var PayOffManager
     */
    private $payOffManager;

    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * @var DisputePolitics
     */
    private $disputePolitics;

    /**
     * @var DiscountPolitics
     */
    private $discountPolitics;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var DisputeNotificationSender
     */
    private $disputeNotificationSender;

    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * @var array
     */
    private $config;

    /**
     * @var array
     */
    private $disputeOutput = [];

    /**
     * @var DisputeStatus
     */
    private $disputeStatus;

    /**
     * DiscountManager constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param PayOffManager $payOffManager
     * @param PaymentPolitics $paymentPolitics
     * @param DisputePolitics $disputePolitics
     * @param DiscountPolitics $discountPolitics
     * @param RbacManager $rbacManager
     * @param DisputeNotificationSender $disputeNotificationSender
     * @param CalculatedDataProvider $calculatedDataProvider
     * @param PaymentOrderManager $paymentOrderManager
     * @param DisputeStatus $disputeStatus
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                PaymentMethodManager $paymentMethodManager,
                                PayOffManager $payOffManager,
                                PaymentPolitics $paymentPolitics,
                                DisputePolitics $disputePolitics,
                                DiscountPolitics $discountPolitics,
                                RbacManager $rbacManager,
                                DisputeNotificationSender $disputeNotificationSender,
                                CalculatedDataProvider $calculatedDataProvider,
                                PaymentOrderManager $paymentOrderManager,
                                DisputeStatus $disputeStatus,
                                array $config)
    {
        $this->entityManager             = $entityManager;
        $this->userManager               = $userManager;
        $this->baseAuthManager               = $baseAuthManager;
        $this->paymentMethodManager      = $paymentMethodManager;
        $this->payOffManager             = $payOffManager;
        $this->paymentPolitics           = $paymentPolitics;
        $this->disputePolitics           = $disputePolitics;
        $this->discountPolitics          = $discountPolitics;
        $this->rbacManager               = $rbacManager;
        $this->disputeNotificationSender = $disputeNotificationSender;
        $this->calculatedDataProvider    = $calculatedDataProvider;
        $this->paymentOrderManager       = $paymentOrderManager;
        $this->disputeStatus             = $disputeStatus;
        $this->config                    = $config;

        // set notify params from config
        $this->sms_notify_discount_provided = $this->getDealNotifyConfig($this->config, 'discount_provided', 'sms');
        $this->email_notify_discount_provided = $this->getDealNotifyConfig($this->config, 'discount_provided', 'email');
        $this->sms_notify_discount_request = $this->getDealNotifyConfig($this->config, 'discount_request', 'sms');
        $this->email_notify_discount_request = $this->getDealNotifyConfig($this->config, 'discount_request', 'email');
        $this->sms_notify_discount_confirm = $this->getDealNotifyConfig($this->config, 'discount_confirm', 'sms');
        $this->email_notify_discount_confirm = $this->getDealNotifyConfig($this->config, 'discount_confirm', 'email');
    }

    /**
     * @param $id
     * @return null|object
     * @throws \Exception
     */
    public function getDiscountById($id)
    {
        $discount = $this->entityManager->find(Discount::class, $id);

        if (!$discount) {

            throw new \Exception('No Discount found');
        }

        return $discount;
    }

    /**
     * @param $id
     * @return null|object
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getDiscountRequestById($id)
    {
        return $this->entityManager->find(DiscountRequest::class, $id);
    }

    /**
     * @param DisputeManager $disputeManager
     */
    public function setDisputeManager(DisputeManager $disputeManager)
    {
        $this->disputeManager = $disputeManager;
    }

    /**
     * @return DisputeManager
     */
    public function getDisputeManager(): DisputeManager
    {
        return $this->disputeManager;
    }

    /**
     * @param $disputeOutput
     * @return array|null
     */
    public function setDisputeOutput($disputeOutput)
    {
        if($disputeOutput && is_array($disputeOutput)){
            $this->disputeOutput = $disputeOutput;
        }

        return $this->disputeOutput;
    }

    /**
     * @return Paginator
     */
    public function getAllDiscounts(): Paginator
    {
        /** @var array $discounts */
        $discounts = $this->entityManager->getRepository(Discount::class)->findAll();

        return new Paginator(new Adapter\ArrayAdapter($discounts));
    }

    /**
     * @return Paginator
     */
    public function getAllDiscountRequests(): Paginator
    {
        /** @var array $discountRequests */
        $discountRequests = $this->entityManager->getRepository(DiscountRequest::class)->findAll();

        return new Paginator(new Adapter\ArrayAdapter($discountRequests));
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function isOperator(User $user)
    {
        return $this->rbacManager->hasRole($user, 'Operator');
    }

    /**
     * @param Dispute $dispute
     * @param $params
     * @return Discount
     * @throws \Exception
     * @throws \Throwable
     */
    public function createDiscount(Dispute $dispute, $params)
    {
        if (! isset($params['bank_transfer_id']) ) {

            throw new \Exception('Not found key "bank_transfer_id" in the form data');
        }
        if (! isset($params['amount_discount']) ) {

            throw new \Exception('Not found key "amount_discount" in the form data');
        }
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        // 1. проверяем статус
        $disputeCycle = DisputeManager::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || ($disputeCycle->getClosed() !== null && !$dispute->isClosed())) {
            throw new LogicException(null, LogicException::DISPUTE_ACTIVE_CYCLE_NOT_FOUND);
        }
        // 2. получаем и проверяем сумму скидки относсительно максимального лимита скидки
        $maxDiscount = $this->getMaxDiscount($dispute->getDeal());
        if ($params['amount_discount'] === null || $params['amount_discount'] > $maxDiscount) {

            throw new \Exception('Discount amount is not valid');
        }
        // 3. получаем платежные реквизиты
        /** @var PaymentMethodBankTransfer $paymentMethodBankTransfer */
        $paymentMethodBankTransfer = $this->paymentMethodManager
            ->getPaymentMethodBankTransferById((int) $params['bank_transfer_id']);
        if ($paymentMethodBankTransfer === null) {

            throw new \Exception('PaymentMethodBankTransfer not found by id');
        }
        try {
            // DB Transaction start
            $this->entityManager->getConnection()->beginTransaction();

            $discount = new Discount();
            $discount->setCreated(new \DateTime());
            $discount->setDispute($dispute);
            $discount->setAmount($params['amount_discount']);
            $discount->setPaymentMethod($paymentMethodBankTransfer->getPaymentMethod());
            $discount->setDisputeCycle($disputeCycle);

            $this->entityManager->persist($discount);
            $dispute->setDiscount($discount);
            $disputeCycle->setDiscount($discount);
            $this->entityManager->persist($dispute);
            $this->entityManager->persist($disputeCycle);

            $this->entityManager->flush();

            ///// Генерация платежки на выплату возврата //////
            $this->payOffManager->createPayOff($deal, PayOffManager::PURPOSE_TYPE_DISCOUNT);

            // DB Transaction commit
            $this->entityManager->getConnection()->commit();

        } catch (\Throwable $throwable) {
            // DB Transaction rollback
            $this->entityManager->getConnection()->rollBack();
            $this->entityManager->getConnection()->close();

            logException($throwable, 'payoff-errors', 2);

            throw $throwable;
        }

        //уведомить участников сделки о предоставлении скидки
        $this->notifyDealAgentsAboutDiscountProvided($dispute, $discount);

        return $discount;
    }

    /**
     * @param Deal $deal
     * @param $params
     * @param DealAgent $author
     * @return DiscountRequest
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function createDiscountRequest(Deal $deal, DealAgent $author, $params)
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        if (! $dispute ) {

            throw new \Exception('The deal has no dispute');
        }

        if (! isset($params['amount_discount']) ) {

            throw new \Exception('Not found key "amount_discount" in the form data');
        }

        $disputeCycle = DisputeManager::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || ($disputeCycle->getClosed() !== null && !$dispute->isClosed())) {
            throw new LogicException(null, LogicException::DISPUTE_ACTIVE_CYCLE_NOT_FOUND);
        }

        $maxDiscount = $this->getMaxDiscount($deal);

        if ($params['amount_discount'] === null || $params['amount_discount'] > $maxDiscount) {

            throw new \Exception('Discount amount is not valid');
        }

        $discountRequest = new DiscountRequest();
        $discountRequest->setCreated(new \DateTime());
        $discountRequest->setDispute($dispute);
        $discountRequest->setAmount($params['amount_discount']);
        $discountRequest->setAuthor($author);
        $discountRequest->setDisputeCycle($disputeCycle);

        $this->entityManager->persist($discountRequest);
        $disputeCycle->addDiscountRequest($discountRequest);
        $this->entityManager->persist($disputeCycle);
        $this->entityManager->flush();

        if (!$discountRequest->getId()) {

            throw new \Exception('DiscountRequest not created');
        }

        // уведомляем агентов о создании запроса на скидку
        $this->notifyDealAgentsAboutDiscountRequest($discountRequest);

        return $discountRequest;
    }

    /**
     * @param DiscountRequest $discountRequest
     * @param DealAgent $author
     * @param bool $status
     * @return DiscountConfirm
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function createDiscountConfirm(DiscountRequest $discountRequest, DealAgent $author, $status = true)
    {
        $discountConfirm = new DiscountConfirm();
        $discountConfirm->setCreated(new \DateTime());
        $discountConfirm->setDiscountRequest($discountRequest);
        $discountConfirm->setAuthor($author);
        $discountConfirm->setStatus($status);

        $this->entityManager->persist($discountConfirm);
        $this->entityManager->flush($discountConfirm);

        if (!$discountConfirm->getId()) {

            throw new \Exception('DiscountConfirm not created');
        }

        $discountRequest->setConfirm($discountConfirm);

        $this->entityManager->persist($discountRequest);
        $this->entityManager->flush($discountRequest);

        // Если при отказе лимит достигнут, то уведомление тут не отправляем.
        // Отправится уведомлениео о передаче в Arbitrage.
        if (false === $status && true === $this->checkDiscountRefusesOverlimit($discountRequest->getDispute())) {

            return $discountConfirm;
        }

        // уведомить контрагента о принятии решения по запросу скидки
        $this->notifyDealAgentsAboutDiscountConfirm($discountConfirm);

        return $discountConfirm;
    }

    /**
     * @param DiscountRequest $discountRequest
     * @param DealAgent $author
     * @return DiscountConfirm
     * @throws \Exception
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createDiscountRefuse(DiscountRequest $discountRequest, DealAgent $author)
    {
        $status = false;

        return $this->createDiscountConfirm($discountRequest, $author, $status);
    }

    /**
     * 1 Operator
     * 2 Что сделка находится в статусе не ниже "Оплачено"
     * 3 Если разрешено политикой
     *
     * @param User $user
     * @param Dispute $dispute
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionDiscount(User $user, Dispute $dispute): bool
    {
        /**
         * @var Deal $deal
         * @var Payment $payment
         */
        $deal = $dispute->getDeal();
        $this->disputeStatus->setStatus($dispute);
        $payment = $deal->getPayment();
        $is_operator = $this->rbacManager->hasRole($user, 'Operator');

        $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);

        $is_deal_paid = $this->paymentPolitics->isDealPaid($total_incoming_amount, $payment->getExpectedValue());
        $is_allowed_add_discount = $dispute ? $this->disputePolitics->isAllowedAddDiscount($dispute) : false;

        return $is_operator && $is_deal_paid && $is_allowed_add_discount;
    }

    /**
     * 1 Участник сделки в роли customer
     * 2 Что сделка находится в статусе не ниже "Оплачено"
     * 3 Если разрешено политикой
     *
     * @param User $user
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionDiscountRequest(User $user, Deal $deal): bool
    {
        /**
         * @var Dispute $dispute
         * @var Payment $payment
         */
        $dispute = $deal->getDispute();
        $payment = $deal->getPayment();
        $this->disputeStatus->setStatus($dispute);
        $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);
        $is_operator = $this->rbacManager->hasRole($user, 'Operator');
        $is_contractor = $deal->getContractor()->getUser() === $user;

        $is_deal_paid = $this->paymentPolitics->isDealPaid($total_incoming_amount, $payment->getExpectedValue());
        $is_allowed_add_discount = $dispute ? $this->disputePolitics->isAllowedAddDiscount($dispute) : false;

        return (
            $this->disputePolitics->isAllowedAddDiscountRequest($dispute)
            && $is_deal_paid
            && $is_allowed_add_discount
            && !$is_operator
            && !$is_contractor
        );
    }

    /**
     * @param User $user
     * @param DiscountRequest $discountRequest
     * @return bool
     */
    public function checkPermissionDiscountConfirm(User $user, DiscountRequest $discountRequest)
    {
        /** @var Deal $deal */
        $deal = $discountRequest->getDispute()->getDeal();
        $dispute = $deal->getDispute();
        $this->disputeStatus->setStatus($dispute);

        $is_user_part_of_dispute = DisputeManager::isUserPartOfDispute($discountRequest->getDispute(), $user);

        return $this->disputePolitics->isAllowedAddDiscountConfirm($dispute, $user, $is_user_part_of_dispute);
    }

    /**
     * @param User $user
     * @param DiscountRequest $discountRequest
     * @return bool
     */
    public function checkPermissionDiscountRefuse(User $user, DiscountRequest $discountRequest)
    {
        return $this->checkPermissionDiscountConfirm($user, $discountRequest);
    }

    /**
     * @param User $user
     * @param DiscountRequest $discountRequest
     * @return bool
     */
    public function checkPermissionDiscountSingle(User $user, DiscountRequest $discountRequest)
    {
        return $this->checkPermissionDiscountConfirm($user, $discountRequest);
    }

    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getAllowedDiscountRequestsByUser(User $user)
    {
        $allowedDiscountRequests = $this->entityManager
            ->getRepository(DiscountRequest::class)->findAllowedDiscountRequestsByUser($user);

        return $allowedDiscountRequests;
    }

    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function checkDiscountRefusesOverlimit(Dispute $dispute)
    {
        $discountRequests = $dispute->getDiscountRequests();

        $negative_confirms = [];
        /** @var DiscountRequest $discountRequest */
        foreach ($discountRequests as $discountRequest) {
            /** @var DiscountConfirm $confirm */
            $confirm = $discountRequest->getConfirm();
            if (!$confirm->isStatus()) {
                $negative_confirms[] = $confirm;
            }
        }

        return $this->discountPolitics->isDiscountDeclineOverlimit($negative_confirms);
    }

    /**
     * Уведомление участников сделки о предоставлении скидки
     *
     * @param Dispute $dispute
     * @param Discount $discount
     * @return bool
     */
    private function notifyDealAgentsAboutDiscountProvided(Dispute $dispute, Discount $discount)
    {
        if ($this->email_notify_discount_provided) {
            try {
                $this->notifyCustomerAboutDiscountProvidedByEmail($dispute, $discount);
                $this->notifyContractorAboutDiscountProvidedByEmail($dispute, $discount);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_discount_provided) {
            try {
                $this->notifyCustomerAboutDiscountProvidedBySms($dispute, $discount);
                $this->notifyContractorAboutDiscountProvidedBySms($dispute, $discount);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param DiscountRequest $discountRequest
     * @return bool
     */
    private function notifyDealAgentsAboutDiscountRequest(DiscountRequest $discountRequest)
    {
        if ($this->email_notify_discount_request) {
            try {
                $this->notifyContractorAboutDiscountRequestByEmail($discountRequest);
                $this->notifyCustomerAboutDiscountRequestByEmail($discountRequest);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_discount_request) {
            try {
                $this->notifyCounterAgentAboutDiscountRequestBySms($discountRequest);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param DiscountConfirm $discountConfirm
     * @return bool
     */
    private function notifyDealAgentsAboutDiscountConfirm(DiscountConfirm $discountConfirm)
    {
        if ($this->email_notify_discount_confirm) {
            try {
                $this->notifyCustomerAboutDiscountConfirmByEmail($discountConfirm);
                $this->notifyContractorAboutDiscountConfirmByEmail($discountConfirm);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_discount_confirm) {
            try {
                $this->notifyAuthorRequestAboutDiscountConfirmBySms($discountConfirm);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param DiscountConfirm $discountConfirm
     * @throws \Exception
     */
    private function notifyCustomerAboutDiscountConfirmByEmail(DiscountConfirm $discountConfirm)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $currentAgent
         * @var DealAgent $authorRequest
         */
        $discountRequest = $discountConfirm->getDiscountRequest();
        $deal = $discountRequest->getDispute()->getDeal();
        $dispute = $deal->getDispute();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $discountRequestOutput = $this->getDiscountRequestOutput($discountRequest);
        $currentAgent = $discountConfirm->getAuthor();
        $authorRequest = $discountRequest->getAuthor();

        if ($discountConfirm->isStatus()) {
            $type_notify = DisputeNotificationSender::DISCOUNT_CONFIRM_FOR_AUTHOR_REQUEST;
        } else {
            $type_notify = DisputeNotificationSender::DISCOUNT_REFUSE_FOR_CUSTOMER;
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'discountRequestOutput' => $discountRequestOutput,
            'contractor_login' => $currentAgent->getUser()->getLogin(),
            'contractor_email' => $currentAgent->getUser()->getEmail()->getEmail(),
            'contractor_phone' => $currentAgent->getUser()->getPhone()->getPhoneNumber(),
            'customer_login' => $authorRequest->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => $type_notify,
        ];

        $this->disputeNotificationSender->sendMail($authorRequest->getEmail(), $data);
    }

    /**
     * @param DiscountConfirm $discountConfirm
     * @throws \Exception
     */
    private function notifyContractorAboutDiscountConfirmByEmail(DiscountConfirm $discountConfirm)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $currentAgent
         * @var DealAgent $authorRequest
         */
        $discountRequest = $discountConfirm->getDiscountRequest();
        $deal = $discountRequest->getDispute()->getDeal();
        $dispute = $deal->getDispute();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $discountRequestOutput = $this->getDiscountRequestOutput($discountRequest);
        $currentAgent = $discountConfirm->getAuthor();
        $authorRequest = $discountRequest->getAuthor();

        if ($discountConfirm->isStatus()) {
            $type_notify = DisputeNotificationSender::DISCOUNT_CONFIRM_FOR_CONTRACTOR;
        } else {
            $type_notify = DisputeNotificationSender::DISCOUNT_REFUSE_FOR_CONTRACTOR;
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'discountRequestOutput' => $discountRequestOutput,
            'contractor_login' => $currentAgent->getUser()->getLogin(),
            'customer_login' => $authorRequest->getUser()->getLogin(),
            'customer_email' => $authorRequest->getUser()->getEmail()->getEmail(),
            'customer_phone' => $authorRequest->getUser()->getPhone()->getPhoneNumber(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => $type_notify,
        ];

        $this->disputeNotificationSender->sendMail($currentAgent->getEmail(), $data);
    }

    /**
     * @param DiscountConfirm $discountConfirm
     * @throws \Exception
     */
    private function notifyAuthorRequestAboutDiscountConfirmBySms(DiscountConfirm $discountConfirm)
    {
        /**
         * @var Deal $deal
         * @var DealAgent $authorRequest
         * @var DiscountRequest $discountRequest
         */
        $discountRequest = $discountConfirm->getDiscountRequest();
        $deal = $discountRequest->getDispute()->getDeal();
        $authorRequest = $discountRequest->getAuthor();

        $userPhone = $authorRequest->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            if ($discountConfirm->isStatus()) {
                $smsMessage = $this->getDiscountConfirmForAuthorRequestSmsMessage($deal, $discountRequest->getAmount());
            } else {
                $smsMessage = $this->getDiscountRefuseForAuthorRequestSmsMessage($deal, $discountRequest->getAmount());
            }
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param DiscountRequest $discountRequest
     * @throws \Exception
     */
    private function notifyCounterAgentAboutDiscountRequestBySms(DiscountRequest $discountRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $discountRequest->getDispute();
        $deal = $dispute->getDeal();
        $currentAgent = $discountRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $userPhone = $counterAgent->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getDiscountRequestForCounterAgentSmsMessage($deal, $discountRequest->getAmount());
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param DiscountRequest $discountRequest
     * @throws \Exception
     */
    private function notifyCustomerAboutDiscountRequestByEmail(DiscountRequest $discountRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $discountRequest->getDispute();
        $deal = $dispute->getDeal();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $discountRequestOutput = $this->getDiscountRequestOutput($discountRequest);
        $currentAgent = $discountRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'discountRequestOutput' => $discountRequestOutput,
            'customer_login' => $currentAgent->getUser()->getLogin(),
            'contractor_login' => $counterAgent->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::DISCOUNT_REQUEST_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($currentAgent->getEmail(), $data);
    }

    /**
     * @param DiscountRequest $discountRequest
     * @throws \Exception
     */
    private function notifyContractorAboutDiscountRequestByEmail(DiscountRequest $discountRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $discountRequest->getDispute();
        $deal = $dispute->getDeal();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $discountRequestOutput = $this->getDiscountRequestOutput($discountRequest);
        $currentAgent = $discountRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'discountRequestOutput' => $discountRequestOutput,
            'customer_login' => $currentAgent->getUser()->getLogin(),
            'contractor_login' => $counterAgent->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::DISCOUNT_REQUEST_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($counterAgent->getEmail(), $data);
    }

    /**
     * Уведомление о предоставлении скидки для кастомера
     *
     * @param Dispute $dispute
     * @param Discount $discount
     * @throws \Exception
     */
    private function notifyCustomerAboutDiscountProvidedByEmail(Dispute $dispute, Discount $discount)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getCustomer()->getEmail();
        $dispute = $deal->getDispute();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $discountOutput = $this->getDiscountOutput($discount);

        logMessage('notifyCustomerAboutDiscountProvidedByEmail - '.$discountOutput['amount'], 'discount');

        $data = [
            'disputeOutput' => $disputeOutput,
            'discountOutput' => $discountOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::DISCOUNT_PROVIDED_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * Уведомление о предоставлении скидки для контрактора
     *
     * @param Dispute $dispute
     * @param Discount $discount
     * @throws \Exception
     */
    private function notifyContractorAboutDiscountProvidedByEmail(Dispute $dispute, Discount $discount)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getContractor()->getEmail();
        $dispute = $deal->getDispute();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $discountOutput = $this->getDiscountOutput($discount);

        logMessage('notifyCustomerAboutDiscountProvidedByEmail - '.$discountOutput['amount'], 'discount');

        $data = [
            'disputeOutput' => $disputeOutput,
            'discountOutput' => $discountOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::DISCOUNT_PROVIDED_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isDiscountDone(Dispute $dispute)
    {
        return (bool)$dispute->getDiscount();
    }

    /**
     * @param Deal $deal
     * @return float|int
     * @throws \Exception
     */
    public function getMaxDiscount(Deal $deal)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        try {
            $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);
            $maxDiscount = $this->calculatedDataProvider->getMaxDiscountAmount($payment, $total_incoming_amount);

            return $maxDiscount;
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param $discounts
     * @return array
     */
    public function getDiscountCollectionOutput($discounts)
    {
        $discountOutput = [];
        /** @var ArrayCollection $discounts */
        foreach ($discounts as $discount) {
            $discountOutput[] = $this->getDiscountOutput($discount);
        }

        return $discountOutput;
    }

    /**
     * @param Discount $discount
     * @return array
     */
    public function getDiscountOutput(Discount $discount)
    {
        $discountOutput = [];

        /** @var Dispute $dispute */
        $dispute = $discount->getDispute();
        /** @var Deal $deal */
        $deal = $dispute->getDeal();

        $discountOutput['id'] = $discount->getId();
        $discountOutput['created'] = $discount->getCreated()->format('d.m.Y H:i');
        $discountOutput['created_full'] = [];
        $discountOutput['created_full']['date'] = $discount->getCreated()->format('d.m.Y');
        $discountOutput['created_full']['time'] = $discount->getCreated()->format('H:i');
        $discountOutput['amount'] = $discount->getAmount();

        $discountOutput['dispute_id'] = $dispute->getId();
        $discountOutput['dispute_reason'] = $dispute->getReason();
        $discountOutput['dispute_created'] = $dispute->getCreated()->format('d.m.Y');;

        $discountOutput['deal_id'] = $deal->getId();
        $discountOutput['deal_name'] = $deal->getName();
        $discountOutput['deal_number'] = $deal->getNumber();
        $discountOutput['deal_amount_without_fee'] = $deal->getPayment()->getDealValue();

        return $discountOutput;
    }

    /**
     * @param DiscountRequest $discountRequest
     * @return array
     */
    public function getDiscountRequestOutput(DiscountRequest $discountRequest)
    {
        try {
            /** @var User $user */
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        } catch (\Throwable $t){
            $user = null;
        }

        $discountRequestOutput = [];

        $discountRequestOutput['id'] = $discountRequest->getId();
        /** @var User $authorUser */
        $authorUser = $discountRequest->getAuthor()->getUser();
        $discountRequestOutput['created'] = $discountRequest->getCreated()->format('d.m.Y H:i');
        $discountRequestOutput['created_full'] = [];
        $discountRequestOutput['created_full']['date'] = $discountRequest->getCreated()->format('d.m.Y');
        $discountRequestOutput['created_full']['time'] = $discountRequest->getCreated()->format('H.i');

        $discountRequestOutput['amount'] = $discountRequest->getAmount();
        $discountRequestOutput['is_author'] = $user === $authorUser;
        $discountRequestOutput['author'] = [
            'name' => $authorUser->getLogin(),
            'email' => $authorUser->getEmail()->getEmail(),
            'phone' => '+'.$authorUser->getPhone()->getPhoneNumber(),
        ];
        $discountRequestOutput['confirm'] = null;
        if ($discountRequest->getConfirm()) {
            /** @var DiscountConfirm $discountConfirm */
            $discountConfirm = $discountRequest->getConfirm();
            $discountRequestOutput['confirm'] = [
                'id' => $discountConfirm->getId(),
                'status' => $discountConfirm->isStatus(),
                'author' => [
                    'name' => $discountConfirm->getAuthor()->getUser()->getLogin(),
                    'email' => $discountConfirm->getAuthor()->getUser()->getEmail()->getEmail(),
                    'phone' => '+'.$discountConfirm->getAuthor()->getUser()->getPhone()->getPhoneNumber(),
                ],
                'created' => $discountConfirm->getCreated()->format('d.m.Y H:i'),
            ];
        }

        /** @var Dispute $dispute */
        $dispute = $discountRequest->getDispute();

        $discountRequestOutput['dispute'] = [];
        $discountRequestOutput['dispute']['id'] = $dispute->getId();
        $discountRequestOutput['dispute']['reason'] = $dispute->getReason();
        $discountRequestOutput['dispute']['created'] = $dispute->getCreated()->format('d.m.Y');;

        /** @var Deal $deal */
        $deal = $dispute->getDeal();

        $discountRequestOutput['deal'] = [];
        $discountRequestOutput['deal']['id'] = $deal->getId();
        $discountRequestOutput['deal']['name'] = $deal->getName();
        $discountRequestOutput['deal']['number'] = $deal->getNumber();
        $discountRequestOutput['deal']['amount_without_fee'] = $deal->getPayment()->getDealValue();

        return $discountRequestOutput;
    }

    /**
     * @param $discountRequests
     * @return array
     * @throws \Exception
     */
    public function getDiscountRequestCollectionForOutput($discountRequests)
    {
        $discountRequestsOutput = [];
        /** @var ArrayCollection $discountRequests */
        foreach ($discountRequests as $discountRequest) {
            $discountRequestsOutput[] = $this->getDiscountRequestOutput($discountRequest);
        }

        return $discountRequestsOutput;
    }

    /**
     * @param Dispute $dispute
     * @param Discount $discount
     * @throws \Exception
     */
    private function notifyCustomerAboutDiscountProvidedBySms(Dispute $dispute, Discount $discount)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getDiscountProvidedForCustomerSmsMessage($deal, $discount->getAmount());
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Dispute $dispute
     * @param Discount $discount
     * @throws \Exception
     */
    private function notifyContractorAboutDiscountProvidedBySms(Dispute $dispute, Discount $discount)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getDiscountProvidedForContractorSmsMessage($deal, $discount->getAmount());
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }
}