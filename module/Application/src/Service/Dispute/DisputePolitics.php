<?php

namespace Application\Service\Dispute;

use Application\Entity\ArbitrageConfirm;
use Application\Entity\ArbitrageRequest;
use Application\Entity\DiscountConfirm;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\RefundConfirm;
use Application\Entity\RefundRequest;
use Application\Entity\TribunalConfirm;
use Application\Entity\TribunalRequest;
use Application\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class DisputePolitics
 * @package Application\Service\Dispute
 */
class DisputePolitics
{
    public function __construct()
    {

    }

    /**
     * разрешено если
     * 1 нет трибунала
     * 2 нет возврата
     * 3 нет скидки
     * 4 спор не закрыт
     *
     * @param Dispute $dispute
     * @return bool
     */
    public function isAllowedAddRefund(Dispute $dispute)
    {
        return !$dispute->getTribunal() && !$dispute->getRefund() && !$dispute->getDiscount() && !$dispute->isClosed();
    }

    /**
     * разрешено если
     * 1 нет трибунала
     * 2 нет возврата
     * 3 нет скидки
     * 4 спор не закрыт
     *
     * @param Dispute $dispute
     * @return bool
     */
    public function isAllowedAddDiscount(Dispute $dispute)
    {
        return !$dispute->getTribunal() && !$dispute->getRefund() && !$dispute->getDiscount() && !$dispute->isClosed();
    }

    /**
     * разрешено если
     * 1 нет трибунала
     * 2 нет возврата
     * 4 спор не закрыт
     *
     * @param Dispute $dispute
     * @return bool
     */
    public function isAllowedAddTribunal(Dispute $dispute)
    {
        return !$dispute->getTribunal() && !$dispute->getRefund() && !$dispute->isClosed();
    }

    /**
     * разрешено если
     * 1 нет трибунала
     * 2 нет возврата
     * 3 спор не закрыт
     *
     * @param Dispute $dispute
     * @return bool
     */
    public function isAllowedAddWarrantyExtension(Dispute $dispute)
    {
        return !$dispute->getTribunal() && !$dispute->getRefund() && !$dispute->isClosed();
    }

    /**
     * разрешено если
     * 1 нет трибунала
     * 2 нет возврата
     * 3 спор не закрыт
     *
     * @param Dispute $dispute
     * @return bool
     */
    public function isAllowedAddArbitrage(Dispute $dispute)
    {
        return !$dispute->getTribunal() && !$dispute->getRefund() && !$dispute->isClosed();
    }

    /**
     * разрешено если
     * 1 разрешено политикой isAllowedAddDiscount()
     * 2 нет подтвержденных запросов на скидку
     *
     * @param Dispute $dispute
     * @return bool
     */
    public function isAllowedAddDiscountRequest(Dispute $dispute)
    {
        return $this->isAllowedAddDiscount($dispute)
            && !$this->isAnyRequestsActive($dispute->status['status'])
            && !$this->isAnyRequestsAccepted($dispute->status['status'])
            && $dispute->status['status'] !== 'closed'
            && $dispute->status['status'] !== 'undefined';
    }

    /**
     * разрешено если
     * 1 разрешено политикой isAllowedAddDiscount()
     * 2 нет подтвержденных запросов на скидку
     *
     * @param Dispute $dispute
     * @param User $user
     * @param bool $is_user_part_of_dispute
     * @return bool
     */
    public function isAllowedAddDiscountConfirm(Dispute $dispute, User $user, $is_user_part_of_dispute = false)
    {
        return $this->isAllowedAddDiscount($dispute)
            && $dispute->status['status'] === 'discount_request'
            && self::isUserNotAuthorRequest($dispute->getDiscountRequests(), $user)
            && $is_user_part_of_dispute;
    }

    /**
     * разрешено если
     * 1 разрешено политикой isAllowedAddRefund()
     * 2 нет подтвержденных запросов на скидку
     *
     * @param Dispute $dispute
     * @return bool
     */
    public function isAllowedAddRefundRequest(Dispute $dispute)
    {
        return $this->isAllowedAddRefund($dispute)
            && !$this->isAnyRequestsActive($dispute->status['status'])
            && !$this->isAnyRequestsAccepted($dispute->status['status'])
            && $dispute->status['status'] !== 'closed'
            && $dispute->status['status'] !== 'undefined';
    }

    /**
     * разрешено если
     * 1 разрешено политикой isAllowedAddRefund()
     * 2 нет подтвержденных запросов на возврат
     *
     * @param Dispute $dispute
     * @param User $user
     * @param bool $is_user_part_of_dispute
     * @return bool
     */
    public function isAllowedAddRefundConfirm(Dispute $dispute, User $user, $is_user_part_of_dispute = false)
    {
        return $this->isAllowedAddRefund($dispute)
            && $dispute->status['status'] === 'refund_request'
            && self::isUserNotAuthorRequest($dispute->getRefundRequests(), $user)
            && $is_user_part_of_dispute;
    }

    /**
     * разрешено если
     * 1 разрешено политикой isAllowedAddArbitrage()
     * 2 нет подтвержденных запросов на арбитраж
     *
     * @param Dispute $dispute
     * @return bool
     */
    public function isAllowedAddArbitrageRequest(Dispute $dispute)
    {
        return $this->isAllowedAddArbitrage($dispute)
            && !$this->isAnyRequestsActive($dispute->status['status'])
            && $dispute->status['status'] !== 'arbitrage_request_accepted'
            && $dispute->status['status'] !== 'tribunal_request_accepted'
            && $dispute->status['status'] !== 'refund_request_accepted'
            && $dispute->status['status'] !== 'closed'
            && $dispute->status['status'] !== 'undefined';
    }

    /**
     * разрешено если
     * 1 разрешено политикой isAllowedAddArbitrage()
     * 2 нет подтвержденных запросов на арбитраж
     *
     * @param Dispute $dispute
     * @param User $user
     * @param bool $is_operator
     * @return bool
     */
    public function isAllowedAddArbitrageConfirm(Dispute $dispute, User $user, $is_operator = false)
    {
        return $this->isAllowedAddArbitrage($dispute)
            && $dispute->status['status'] === 'arbitrage_request'
            && self::isUserNotAuthorRequest($dispute->getArbitrageRequests(), $user)
            && $is_operator;
    }

    /**
     * разрешено если
     * 1 разрешено политикой isAllowedAddTribunal()
     * 2 нет подтвержденных запросов на трибунал
     *
     * @param Dispute $dispute
     * @return bool
     */
    public function isAllowedAddTribunalRequest(Dispute $dispute)
    {
        return $this->isAllowedAddTribunal($dispute)
            && !$this->isAnyRequestsActive($dispute->status['status'])
            && $dispute->status['status'] !== 'tribunal_request_accepted'
            && $dispute->status['status'] !== 'refund_request_accepted'
            && $dispute->status['status'] !== 'closed'
            && $dispute->status['status'] !== 'undefined';
    }

    /**
     * разрешено если
     * 1 разрешено политикой isAllowedAddTribunal()
     * 2 нет подтвержденных запросов на трибунал
     *
     * @param Dispute $dispute
     * @param User $user
     * @param bool $is_operator
     * @return bool
     */
    public function isAllowedAddTribunalConfirm(Dispute $dispute, User $user, $is_operator = false)
    {
        return $this->isAllowedAddTribunal($dispute)
            && $dispute->status['status'] === 'tribunal_request'
            && self::isUserNotAuthorRequest($dispute->getTribunalRequests(), $user)
            && $is_operator;
    }

    /**
     * @param $requests
     * @return bool
     */
    public static function isPositiveConfirmInRequests($requests)
    {
        /**
         * @var ArrayCollection $requests
         * @var DiscountRequest|RefundRequest|ArbitrageRequest|TribunalRequest $request
         * @var DiscountConfirm|RefundConfirm|ArbitrageConfirm|TribunalConfirm $confirm
         */
        foreach ($requests as $request){
            $confirm = $request->getConfirm();
            //if isStatus === true
            if ($confirm && $confirm->isStatus()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $status
     * @return bool
     */
    public function isAnyRequestsAccepted($status)
    {
       return $status === 'discount_request_accepted'
           || $status === 'refund_request_accepted'
           || $status === 'arbitrage_request_accepted'
           || $status === 'tribunal_request_accepted';
    }

    /**
     * @param $status
     * @return bool
     */
    public function isAnyRequestsActive($status)
    {
       return $status === 'discount_request'
           || $status === 'refund_request'
           || $status === 'tribunal_request'
           || $status === 'arbitrage_request';
    }

    /**
     * @param $requests
     * @param User $user
     * @return bool
     */
    public static function isUserNotAuthorRequest($requests, User $user): bool
    {
        /**
         * @var ArrayCollection $requests
         * @var DiscountRequest|RefundRequest|ArbitrageRequest|TribunalRequest $request
         * @var DiscountConfirm|RefundConfirm|ArbitrageConfirm|TribunalConfirm $confirm
         */

        foreach ($requests as $request){
            if (!$request->getConfirm()) {

                return $request->getAuthor()->getUser() !== $user;
            }
        }

        return false;
    }
}