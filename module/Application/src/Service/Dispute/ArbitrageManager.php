<?php

namespace Application\Service\Dispute;

use Application\Entity\ArbitrageConfirm;
use Application\Entity\ArbitrageRequest;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\RefundRequest;
use Application\Entity\User;
use Application\Provider\Mail\DisputeNotificationSender;
use Application\Service\Payment\PaymentPolitics;
use Core\Exception\LogicException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use ModuleCode\Service\SmsStrategyContext;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class ArbitrageManager
 * @package Application\Service\Dispute
 */
class ArbitrageManager
{
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    const ARBITRAGE_REQUEST_REFUSES_LIMIT = 1;

    /**
     * notify params
     */
    private $sms_notify_arbitrage_request;
    private $email_notify_arbitrage_request;
    private $sms_notify_arbitrage_confirm;
    private $email_notify_arbitrage_confirm;

    /**
     * $var DisputeManager
     */
    private $disputeManager;

    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * @var DisputePolitics
     */
    private $disputePolitics;

    /**
     * @var ArbitragePolitics
     */
    private $arbitragePolitics;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * @var DisputeNotificationSender
     */
    private $disputeNotificationSender;

    /**
     * @var array
     */
    private $config;

    /**
     * @var array
     */
    private $disputeOutput = [];

    /**
     * @var DisputeStatus
     */
    private $disputeStatus;

    /**
     * ArbitrageManager constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param PaymentPolitics $paymentPolitics
     * @param DisputePolitics $disputePolitics
     * @param ArbitragePolitics $arbitragePolitics
     * @param RbacManager $rbacManager
     * @param PaymentOrderManager $paymentOrderManager
     * @param DisputeNotificationSender $disputeNotificationSender
     * @param DisputeStatus $disputeStatus
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                PaymentPolitics $paymentPolitics,
                                DisputePolitics $disputePolitics,
                                ArbitragePolitics $arbitragePolitics,
                                RbacManager $rbacManager,
                                PaymentOrderManager $paymentOrderManager,
                                DisputeNotificationSender $disputeNotificationSender,
                                DisputeStatus $disputeStatus,
                                array $config)
    {
        $this->entityManager             = $entityManager;
        $this->userManager               = $userManager;
        $this->baseAuthManager               = $baseAuthManager;
        $this->paymentPolitics           = $paymentPolitics;
        $this->disputePolitics           = $disputePolitics;
        $this->arbitragePolitics         = $arbitragePolitics;
        $this->rbacManager               = $rbacManager;
        $this->paymentOrderManager       = $paymentOrderManager;
        $this->disputeNotificationSender = $disputeNotificationSender;
        $this->disputeStatus             = $disputeStatus;
        $this->config                    = $config;

        $this->sms_notify_arbitrage_request = $this->getDealNotifyConfig($config, 'arbitrage_request', 'sms');
        $this->email_notify_arbitrage_request = $this->getDealNotifyConfig($config, 'arbitrage_request', 'email');
        $this->sms_notify_arbitrage_confirm = $this->getDealNotifyConfig($config, 'arbitrage_confirm', 'sms');
        $this->email_notify_arbitrage_confirm = $this->getDealNotifyConfig($config, 'arbitrage_confirm', 'email');
    }

    /**
     * @param $id
     * @return null|object
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getArbitrageRequestById($id)
    {
        return $this->entityManager->find(ArbitrageRequest::class, $id);
    }

    /**
     * @param DisputeManager $disputeManager
     */
    public function setDisputeManager(DisputeManager $disputeManager)
    {
        $this->disputeManager = $disputeManager;
    }

    /**
     * @return DisputeManager
     */
    public function getDisputeManager(): DisputeManager
    {
        return $this->disputeManager;
    }

    /**
     * @param $disputeOutput
     * @return array|null
     */
    public function setDisputeOutput($disputeOutput)
    {
        if($disputeOutput && is_array($disputeOutput)){
            $this->disputeOutput = $disputeOutput;
        }

        return $this->disputeOutput;
    }

    /**
     * @param User $user
     * @return mixed
     * @throws \Exception
     */
    public function getAllowedArbitrageRequestsForUserOperator(User $user)
    {
        $allowedArbitrageRequests = [];
        if($this->rbacManager->hasRole($user, 'Operator')){
            $allowedArbitrageRequests = $this->entityManager
                ->getRepository(ArbitrageRequest::class)->findAll();
        }

        return $allowedArbitrageRequests;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionArbitrageRequest(User $user, Deal $deal)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $this->disputeStatus->setStatus($dispute);
        $is_operator = $this->rbacManager->hasRole($user, 'Operator');
        $is_contractor = $deal->getContractor()->getUser() === $user;

        $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);

        $is_deal_paid = $this->paymentPolitics->isDealPaid($total_incoming_amount, $payment->getExpectedValue());
        $is_allowed_add_arbitrage = $dispute ? $this->disputePolitics->isAllowedAddArbitrage($dispute) : false;
        /**
         * 1 Участник сделки в роли customer
         * 2 Что сделка находится в статусе не ниже "Оплачено"
         * 3 Если разрешено политикой создовать ArbitrageRequest
         * 4 При арбитраже проверяется таже политика что и при Tribunal
         * 5 Если не оператор
         */
        return (
            $this->disputePolitics->isAllowedAddArbitrageRequest($dispute)
            && $is_deal_paid
            && $is_allowed_add_arbitrage
            && !$is_operator
            && !$is_contractor
        );
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionArbitrageConfirm(User $user, Deal $deal)
    {
        $dispute = $deal->getDispute();
        $this->disputeStatus->setStatus($dispute);
        $is_operator = $this->rbacManager->hasRole($user, 'Operator');
        /*
         * 1 Если разрешено политикой ArbitrageConfirm
         */
        return $this->disputePolitics->isAllowedAddArbitrageConfirm($dispute, $user, $is_operator);
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionArbitrageRefuse(User $user, Deal $deal)
    {
        /**
         * Проверяется таже политика что и при подтверждении
         * 1 Если разрешено политикой ArbitrageConfirm
         */
        return $this->checkPermissionArbitrageConfirm($user, $deal);
    }

    /**
     * @param Deal $deal
     * @param DealAgent $author
     * @param null $request
     * @return ArbitrageRequest
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createArbitrageRequest(Deal $deal, DealAgent $author, $request = null): ArbitrageRequest
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        if (! $dispute ) {

            throw new \Exception('The deal has no dispute');
        }
        $disputeCycle = DisputeManager::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || ($disputeCycle->getClosed() !== null && !$dispute->isClosed())) {
            throw new LogicException(null, LogicException::DISPUTE_ACTIVE_CYCLE_NOT_FOUND);
        }

        $arbitrageRequest = new ArbitrageRequest();
        $arbitrageRequest->setCreated(new \DateTime());
        $arbitrageRequest->setDispute($dispute);
        $arbitrageRequest->setAuthor($author);
        $arbitrageRequest->setDisputeCycle($disputeCycle);

        $this->entityManager->persist($arbitrageRequest);
        $disputeCycle->addArbitrageRequest($arbitrageRequest);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush();

        if (!$arbitrageRequest->getId()) {

            throw new \Exception('Arbitrage request not created');
        }

        if ($request instanceof DiscountRequest) {
            // уведомить агентов о создании запроса на арбитражную комиссию после отказов от предложенной скидки
            $this->notifyDealAgentsAboutArbitrageRequestOnDiscountRequestOverlimit($arbitrageRequest, $request);
        } elseif ($request instanceof RefundRequest) {
            // уведомить агентов о создании запроса на арбитражную комиссию после отказа от предложения возврата
            #$this->notifyDealAgentsAboutArbitrageRequestOnRefundRequestDecline($arbitrageRequest);
        } else {
            // уведомить контрагента о запросе арбитражной комиссии
            $this->notifyCounterAgentAboutArbitrageRequest($arbitrageRequest);
        }

        return $arbitrageRequest;
    }

    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function checkArbitrageRefusesOverlimit(Dispute $dispute)
    {
        $arbitrageRequests = $dispute->getArbitrageRequests();

        $negative_confirms = [];
        /** @var ArbitrageRequest $arbitrageRequest */
        foreach ($arbitrageRequests as $arbitrageRequest) {
            /** @var ArbitrageConfirm $confirm */
            $confirm = $arbitrageRequest->getConfirm();
            if (!$confirm->isStatus()) {
                $negative_confirms[] = $confirm;
            }
        }

        return $this->arbitragePolitics->isArbitrageDeclineOverlimit($negative_confirms);
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @param DiscountRequest $discountRequest
     * @return bool
     */
    private function notifyDealAgentsAboutArbitrageRequestOnDiscountRequestOverlimit(
        ArbitrageRequest $arbitrageRequest,
        DiscountRequest $discountRequest)
    {
        if ($this->email_notify_arbitrage_request) {
            try {
                $this->notifyCustomerAboutArbitrageRequestOnDiscountRequestOverlimitByEmail($arbitrageRequest, $discountRequest);
                $this->notifyContractorAboutArbitrageRequestOnDiscountRequestOverlimitByEmail($arbitrageRequest, $discountRequest);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_arbitrage_request) {
            try {
                $this->notifyCounterAgentAboutArbitrageRequestBySms($arbitrageRequest);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @param DiscountRequest $discountRequest
     * @throws \Exception
     */
    private function notifyCustomerAboutArbitrageRequestOnDiscountRequestOverlimitByEmail(
        ArbitrageRequest $arbitrageRequest,
        DiscountRequest $discountRequest
    )
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $arbitrageRequest->getDispute();
        $deal = $dispute->getDeal();
        $disputeOutput = $this->disputeOutput;
        $arbitrageRequestOutput = $this->getArbitrageRequestOutput($arbitrageRequest);
        $currentAgent = $arbitrageRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'arbitrageRequestOutput' => $arbitrageRequestOutput,
            'contractor_login' => $currentAgent->getUser()->getLogin(),
            'customer_login' => $counterAgent->getUser()->getLogin(),
            'discount_amount' => $discountRequest->getAmount(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY =>
                DisputeNotificationSender::ARBITRAGE_REQUEST_AFTER_DISCOUNT_REQUESTS_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($counterAgent->getEmail(), $data);
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @param DiscountRequest $discountRequest
     * @throws \Exception
     */
    private function notifyContractorAboutArbitrageRequestOnDiscountRequestOverlimitByEmail(
        ArbitrageRequest $arbitrageRequest,
        DiscountRequest $discountRequest
    )
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $arbitrageRequest->getDispute();
        $deal = $dispute->getDeal();
        $disputeOutput = $this->disputeOutput;
        $arbitrageRequestOutput = $this->getArbitrageRequestOutput($arbitrageRequest);
        $currentAgent = $arbitrageRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'arbitrageRequestOutput' => $arbitrageRequestOutput,
            'contractor_login' => $currentAgent->getUser()->getLogin(),
            'customer_login' => $counterAgent->getUser()->getLogin(),
            'discount_amount' => $discountRequest->getAmount(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY =>
                DisputeNotificationSender::ARBITRAGE_REQUEST_AFTER_DISCOUNT_REQUESTS_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($currentAgent->getEmail(), $data);
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @return bool
     */
    private function notifyCounterAgentAboutArbitrageRequest(ArbitrageRequest $arbitrageRequest)
    {
        if ($this->email_notify_arbitrage_request) {
            try {
                $this->notifyCustomerAboutArbitrageRequestByEmail($arbitrageRequest);
                $this->notifyContractorAboutArbitrageRequestByEmail($arbitrageRequest);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_arbitrage_request) {
            try {
                $this->notifyCounterAgentAboutArbitrageRequestBySms($arbitrageRequest);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param ArbitrageConfirm $arbitrageConfirm
     * @return bool
     */
    private function notifyAuthorRequestAboutArbitrageConfirm(ArbitrageConfirm $arbitrageConfirm)
    {
        if ($this->email_notify_arbitrage_confirm) {
            try {
                $this->notifyCustomerAboutArbitrageConfirmByEmail($arbitrageConfirm);
                $this->notifyContractorAboutArbitrageConfirmByEmail($arbitrageConfirm);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_arbitrage_confirm) {
            try {
                $this->notifyAuthorRequestAboutArbitrageConfirmBySms($arbitrageConfirm);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }


    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @throws \Exception
     */
    private function notifyCustomerAboutArbitrageRequestByEmail(ArbitrageRequest $arbitrageRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $arbitrageRequest->getDispute();
        $deal = $dispute->getDeal();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $arbitrageRequestOutput = $this->getArbitrageRequestOutput($arbitrageRequest);
        $currentAgent = $arbitrageRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'arbitrageRequestOutput' => $arbitrageRequestOutput,
            'customer_login' => $currentAgent->getUser()->getLogin(),
            'contractor_login' => $counterAgent->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::ARBITRAGE_REQUEST_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($currentAgent->getEmail(), $data);
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @throws \Exception
     */
    private function notifyContractorAboutArbitrageRequestByEmail(ArbitrageRequest $arbitrageRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $arbitrageRequest->getDispute();
        $deal = $dispute->getDeal();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $arbitrageRequestOutput = $this->getArbitrageRequestOutput($arbitrageRequest);
        $currentAgent = $arbitrageRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'arbitrageRequestOutput' => $arbitrageRequestOutput,
            'customer_login' => $currentAgent->getUser()->getLogin(),
            'contractor_login' => $counterAgent->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::ARBITRAGE_REQUEST_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($counterAgent->getEmail(), $data);
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @throws \Exception
     */
    private function notifyCounterAgentAboutArbitrageRequestBySms(ArbitrageRequest $arbitrageRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $arbitrageRequest->getDispute();
        $deal = $dispute->getDeal();
        $currentAgent = $arbitrageRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $userPhone = $counterAgent->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getArbitrageRequestForCounterAgentSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param ArbitrageConfirm $arbitrageConfirm
     * @throws \Exception
     *
     * @TODO Нуждается в переделке при status = true
     */
    private function notifyCustomerAboutArbitrageConfirmByEmail(ArbitrageConfirm $arbitrageConfirm)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var User $currentAgent
         * @var DealAgent $authorRequest
         */
        $arbitrageRequest = $arbitrageConfirm->getArbitrageRequest();
        $deal = $arbitrageRequest->getDispute()->getDeal();
        $dispute = $deal->getDispute();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $arbitrageRequestOutput = $this->getArbitrageRequestOutput($arbitrageRequest);
        $customerAgent = $deal->getCustomer();
        $contractorAgent = $deal->getContractor();

        if ($arbitrageConfirm->isStatus()) {
            $type_notify = DisputeNotificationSender::ARBITRAGE_CONFIRM_FOR_AUTHOR_REQUEST;
        } else {
            $type_notify = DisputeNotificationSender::ARBITRAGE_REFUSE_FOR_CUSTOMER;
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'arbitrageRequestOutput' => $arbitrageRequestOutput,
            'customer_login' => $customerAgent->getUser()->getLogin(),
            'contractor_login' => $contractorAgent->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => $type_notify,
        ];

        $this->disputeNotificationSender->sendMail($customerAgent->getEmail(), $data);
    }

    /**
     * @param ArbitrageConfirm $arbitrageConfirm
     * @throws \Exception
     *
     * @TODO Нуждается в переделке при status = true
     */
    private function notifyContractorAboutArbitrageConfirmByEmail(ArbitrageConfirm $arbitrageConfirm)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var User $currentAgent
         * @var DealAgent $authorRequest
         */
        $arbitrageRequest = $arbitrageConfirm->getArbitrageRequest();
        $deal = $arbitrageRequest->getDispute()->getDeal();
        $dispute = $deal->getDispute();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $arbitrageRequestOutput = $this->getArbitrageRequestOutput($arbitrageRequest);
        $customerAgent = $deal->getCustomer();
        $contractorAgent = $deal->getContractor();

        if ($arbitrageConfirm->isStatus()) {
            $type_notify = DisputeNotificationSender::ARBITRAGE_CONFIRM_FOR_AUTHOR_REQUEST;
        } else {
            $type_notify = DisputeNotificationSender::ARBITRAGE_REFUSE_FOR_CUSTOMER;
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'arbitrageRequestOutput' => $arbitrageRequestOutput,
            'customer_login' => $customerAgent->getUser()->getLogin(),
            'contractor_login' => $contractorAgent->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => $type_notify,
        ];

        $this->disputeNotificationSender->sendMail($contractorAgent->getEmail(), $data);
    }

    /**
     * @param ArbitrageConfirm $arbitrageConfirm
     * @throws \Exception
     */
    private function notifyAuthorRequestAboutArbitrageConfirmBySms(ArbitrageConfirm $arbitrageConfirm)
    {
        /**
         * @var Deal $deal
         * @var DealAgent $authorRequest
         * @var ArbitrageRequest $arbitrageRequest
         */
        $arbitrageRequest = $arbitrageConfirm->getArbitrageRequest();
        $deal = $arbitrageRequest->getDispute()->getDeal();
        $authorRequest = $arbitrageRequest->getAuthor();

        $userPhone = $authorRequest->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            if ($arbitrageConfirm->isStatus()) {
                $smsMessage = $this->getArbitrageConfirmForAuthorRequestSmsMessage($deal);
            } else {
                $smsMessage = $this->getArbitrageRefuseForAuthorRequestSmsMessage($deal);
            }
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @param User $user
     * @return ArbitrageConfirm
     * @throws \Exception
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createArbitrageRefuse(ArbitrageRequest $arbitrageRequest, User $user)
    {
        $status = false;

        return $this->createArbitrageConfirm($arbitrageRequest, $user, $status);
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @param User $user
     * @param boolean $status
     * @return ArbitrageConfirm
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function createArbitrageConfirm(ArbitrageRequest $arbitrageRequest, User $user, $status = true)
    {
        $arbitrageConfirm = new ArbitrageConfirm();
        $arbitrageConfirm->setCreated(new \DateTime());
        $arbitrageConfirm->setArbitrageRequest($arbitrageRequest);
        $arbitrageConfirm->setAuthor($user);
        $arbitrageConfirm->setStatus($status);

        $this->entityManager->persist($arbitrageConfirm);
        $this->entityManager->flush($arbitrageConfirm);

        if (!$arbitrageConfirm->getId()) {

            throw new \Exception('Arbitrage confirm not created');
        }

        $arbitrageRequest->setConfirm($arbitrageConfirm);

        $this->entityManager->persist($arbitrageRequest);
        $this->entityManager->flush($arbitrageRequest);


        //уведомить контрагента о принятии решения по запросу арбитражной комиссии
        $this->notifyAuthorRequestAboutArbitrageConfirm($arbitrageConfirm);

        return $arbitrageConfirm;
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @return array
     */
    public function getArbitrageRequestOutput(ArbitrageRequest $arbitrageRequest)
    {
        try {
            /** @var User $user */
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        } catch (\Throwable $t){
            $user = null;
        }

        $arbitrageRequestOutput = [];

        $arbitrageRequestOutput['id'] = $arbitrageRequest->getId();
        /** @var User $authorUser */
        $authorUser = $arbitrageRequest->getAuthor()->getUser();
        $arbitrageRequestOutput['created'] = $arbitrageRequest->getCreated()->format('d.m.Y H:i');
        $arbitrageRequestOutput['is_author'] = $user === $authorUser;
        $arbitrageRequestOutput['author'] = [
            'name' => $authorUser->getLogin(),
            'email' => $authorUser->getEmail()->getEmail(),
            'phone' => '+'.$authorUser->getPhone()->getPhoneNumber(),
        ];
        $arbitrageRequestOutput['confirm'] = null;
        if ($arbitrageRequest->getConfirm()) {
            /** @var ArbitrageConfirm $arbitrageConfirm */
            $arbitrageConfirm = $arbitrageRequest->getConfirm();
            $arbitrageRequestOutput['confirm'] = [
                'id' => $arbitrageConfirm->getId(),
                'status' => $arbitrageConfirm->isStatus(),
                'author' => [
                    'name' => $arbitrageConfirm->getAuthor()->getLogin(),
                    'email' => $arbitrageConfirm->getAuthor()->getEmail()->getEmail(),
                    'phone' => '+'.$arbitrageConfirm->getAuthor()->getPhone()->getPhoneNumber(),
                ],
                'created' => $arbitrageConfirm->getCreated()->format('d.m.Y H:i'),
            ];
        }

        return $arbitrageRequestOutput;
    }

    /**
     * @param $arbitrageRequests
     * @return array
     * @throws \Exception
     */
    public function getArbitrageRequestCollectionForOutput($arbitrageRequests)
    {
        $arbitrageRequestsOutput = [];
        /** @var ArrayCollection $arbitrageRequests */
        foreach ($arbitrageRequests as $arbitrageRequest) {
            $arbitrageRequestsOutput[] = $this->getArbitrageRequestOutput($arbitrageRequest);
        }

        return $arbitrageRequestsOutput;
    }
}