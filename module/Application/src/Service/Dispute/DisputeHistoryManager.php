<?php

namespace Application\Service\Dispute;

use Application\Entity\ArbitrageConfirm;
use Application\Entity\ArbitrageRequest;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\Discount;
use Application\Entity\DiscountConfirm;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\DisputeCycle;
use Application\Entity\Payment;
use Application\Entity\Refund;
use Application\Entity\RefundConfirm;
use Application\Entity\RefundRequest;
use Application\Entity\Tribunal;
use Application\Entity\TribunalConfirm;
use Application\Entity\TribunalRequest;
use Application\Entity\User;
use Application\Entity\WarrantyExtension;
use Application\Service\Deal\DealDeadlineManager;
use Core\Exception\LogicException;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleRbac\Service\RbacManager;

/**
 * Class DisputeHistoryManager
 * @package Application\Service\Dispute
 *
 */
class DisputeHistoryManager
{
    const WAITING_DAY_AFTER_OPENED_DISPUTE = 3;

    const ROLE_AUTHOR = 'AUTHOR';
    const ROLE_NOT_AUTHOR = 'NOT_AUTHOR';
    const ROLE_OBSERVER = 'OBSERVER';

    const ROLE_OPERATOR = 'OPERATOR';
    const ROLE_CUSTOMER = 'CUSTOMER';
    const ROLE_CONTRACTOR = 'CONTRACTOR';

    // code for templates
    const DISPUTE_OPEN = 'DISPUTE_OPEN';
    const DISCOUNT_REQUEST = 'DISCOUNT_REQUEST';
    const DISCOUNT_REQUEST_ACCEPTED = 'DISCOUNT_REQUEST_ACCEPTED';
    const DISCOUNT_REQUEST_REJECTED = 'DISCOUNT_REQUEST_REJECTED';
    const REFUND_REQUEST = 'REFUND_REQUEST';
    const REFUND_REQUEST_ACCEPTED = 'REFUND_REQUEST_ACCEPTED';
    const REFUND_REQUEST_REJECTED = 'REFUND_REQUEST_REJECTED';
    const ARBITRAGE_REQUEST = 'ARBITRAGE_REQUEST';
    const ARBITRAGE_REQUEST_ACCEPTED = 'ARBITRAGE_REQUEST_ACCEPTED';
    const ARBITRAGE_REQUEST_REJECTED = 'ARBITRAGE_REQUEST_REJECTED';
    const ARBITRAGE_REQUEST_WITH_DISCOUNT = 'ARBITRAGE_REQUEST_WITH_DISCOUNT';
    const ARBITRAGE_REQUEST_WITH_REFUND = 'ARBITRAGE_REQUEST_WITH_REFUND';
    const ARBITRAGE_REQUEST_WITH_TRIBUNAL = 'ARBITRAGE_REQUEST_WITH_TRIBUNAL';
    const TRIBUNAL_REQUEST = 'TRIBUNAL_REQUEST';
    const TRIBUNAL_REQUEST_ACCEPTED = 'TRIBUNAL_REQUEST_ACCEPTED';
    const TRIBUNAL_REQUEST_REJECTED = 'TRIBUNAL_REQUEST_REJECTED';
    const DISCOUNT = 'DISCOUNT';
    const REFUND = 'REFUND';
    const TRIBUNAL = 'TRIBUNAL';
    const WARRANTY_EXTENSION = 'WARRANTY_EXTENSION';
    const DISPUTE_CANCELED = 'DISPUTE_CANCELED';
    const DISPUTE_CLOSED = 'DISPUTE_CLOSED';
    const DISPUTE_CLOSED_REFUND = 'DISPUTE_CLOSED_REFUND';
    const DISPUTE_CLOSED_DISCOUNT = 'DISPUTE_CLOSED_DISCOUNT';
    const DISPUTE_CLOSED_DISCOUNT_WITH_DAYS = 'DISPUTE_CLOSED_DISCOUNT_WITH_DAYS';
    const DISPUTE_CLOSED_WITH_DAYS = 'DISPUTE_CLOSED_WITH_DAYS';

    /**
     * @var RbacManager
     */
    private $rbacManager;
    /**
     * @var DisputeStatus
     */
    private $disputeStatus;
    /**
     * @var RefundManager
     */
    private $refundManager;

    /**
     * DealHistoryManager constructor.
     * @param RbacManager $rbacManager
     * @param DisputeStatus $disputeStatus
     * @param RefundManager $refundManager
     */
    public function __construct(RbacManager $rbacManager,
                                DisputeStatus $disputeStatus,
                                RefundManager $refundManager)
    {
        $this->rbacManager = $rbacManager;
        $this->disputeStatus = $disputeStatus;
        $this->refundManager = $refundManager;
    }

    /**
     * @param $disputeCycles
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getDisputeCycleCollection($disputeCycles, User $user)
    {
        $history = [];
        /** @var ArrayCollection $disputeCycles */
        /** @var DisputeCycle $disputeCycle */
        foreach ($disputeCycles as $disputeCycle) {
            $history_cycle = $this->getDisputeCycleSingle($disputeCycle, $user);
            if (!empty($history_cycle)) {
                /** @noinspection SlowArrayOperationsInLoopInspection */
                $history = array_merge($history, $history_cycle);
            }
        }

        return $history;
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getDisputeCycleSingle(DisputeCycle $disputeCycle, User $user)
    {
        $history_cycle = [];
        // 1. открытие спора
        /** @noinspection SlowArrayOperationsInLoopInspection */
        $history_cycle = array_merge($history_cycle, $this->getHistoryDisputeOpen($disputeCycle, $user));
        // 2.1 запрос на скидку
        // 2.2 отклонение запросов на скидку
        // 2.3 подтверждение запросов на скидку
        /** @noinspection SlowArrayOperationsInLoopInspection */
        $history_cycle = array_merge($history_cycle, $this->getHistoryDiscountRequests($disputeCycle, $user));
        // 3.1 запрос на возврат
        // 3.2 отклонение запросов на возврат
        // 3.3 подтверждение запросов на возврат
        /** @noinspection SlowArrayOperationsInLoopInspection */
        $history_cycle = array_merge($history_cycle, $this->getHistoryRefundRequests($disputeCycle, $user));
        // 4.1 запрос на арбитраж
        // 4.2 запрос на арбитраж после 3-х оказов на скидку
        // 4.3 запрос на арбитраж после 1 оказа на возврат
        // 4.4 запрос на арбитраж после 1 оказа на трибунал (если не было запросов на арбитраж)
        // 4.5 отклонение запросов на арбитраж
        // 4.6 подтверждение запросов на арбитраж
        /** @noinspection SlowArrayOperationsInLoopInspection */
        $history_cycle = array_merge($history_cycle, $this->getHistoryArbitrageRequests($disputeCycle, $user));
        // 5.1 запрос на суд
        // 5.2 отклонение запросов на суд
        // 5.3 подтверждение запросов на суд
        /** @noinspection SlowArrayOperationsInLoopInspection */
        $history_cycle = array_merge($history_cycle, $this->getHistoryTribunalRequests($disputeCycle, $user));
        // 6. оператор создал скидку
        /** @noinspection SlowArrayOperationsInLoopInspection */
        $history_cycle = array_merge($history_cycle, $this->getHistoryDiscount($disputeCycle, $user));
        // 7. оператор создал возврат
        /** @noinspection SlowArrayOperationsInLoopInspection */
        $history_cycle = array_merge($history_cycle, $this->getHistoryRefund($disputeCycle, $user));
        // 8. оператор создал трибунал
        /** @noinspection SlowArrayOperationsInLoopInspection */
        $history_cycle = array_merge($history_cycle, $this->getHistoryTribunal($disputeCycle, $user));
        // 9. добавленные дни к спору
        /** @noinspection SlowArrayOperationsInLoopInspection */
        $history_cycle = array_merge($history_cycle, $this->getHistoryWarrantyExtension($disputeCycle, $user));
        // 10.1 спор отозван кастомером
        // 10.2 спор закрыт без скидки и без добавленных дней
        // 10.3 спор закрыт без скидки но с добавленными днями
        // 10.4 спор закрыт со скидкой и без добавленных дней
        // 10.5 спор закрыт со скидкой и с добавленными днями
        // 10.5 спор закрыт с возвратом
        /** @noinspection SlowArrayOperationsInLoopInspection */
        $history_cycle = array_merge($history_cycle, $this->getHistoryClosed($disputeCycle, $user));

        return $history_cycle;
    }

    /**
     * @param Dispute|null $dispute
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getDisputeHistory(Dispute $dispute = null, User $user)
    {
        if ($dispute) {
            $this->disputeStatus->setStatus($dispute);
            $disputeCycles = $dispute->getDisputeCycles();
            $collectionHistory = $this->getDisputeCycleCollection($disputeCycles, $user);

            return $this->getProcessedHistory($collectionHistory, $dispute, $user);
        }

        return [];
    }

    /**
     * @param array $collectionHistory
     * @param Dispute $dispute
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getProcessedHistory(array $collectionHistory, Dispute $dispute, User $user)
    {
        $isUserPartOfDispute = DisputeManager::isUserPartOfDispute($dispute, $user);
        $isOperator = $this->rbacManager->hasRole($user, 'Operator');

        //sorting history
        $history = $this->sortingHistory($collectionHistory);

        //processing for operator
        if ($isOperator) {
            return $history;
        }
        //processing for users
        if ($isUserPartOfDispute) {
//          return $this->getProcessedHistoryForUsers($history);
            return $history; //@TODO возвращаем для участников полную историю согласно задаче GP-1750
        }

        return [];
    }

    /**
     * @param array $collectionHistory
     * @return array
     */
    public function sortingHistory(array $collectionHistory)
    {
        $sortedHistory = $collectionHistory;
        if(!empty($sortedHistory)){
            usort($sortedHistory, function ($a, $b) {
                return strtotime($a['created']) - strtotime($b['created']);
            });
        }

        return $sortedHistory;
    }

    /**
     * @param array $processedHistory
     * @return array
     */
    public function getProcessedHistoryForUsers(array $processedHistory)
    {
        return $this->getLastHistory($processedHistory);
    }

    /**
     * @param array $processedHistory
     * @return array
     */
    public function getLastHistory(array $processedHistory)
    {
        $lastHistory = null;
        $last_history_date = null;
        foreach ($processedHistory as $history) {
            if ($history['created'] >= $last_history_date) {
                $last_history_date = $history['created'];
                $lastHistory = $history;
            }
        }
        return [$lastHistory];
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @return array
     */
    public function getWaitingDay(DisputeCycle $disputeCycle)
    {
        $disputeCreated = $disputeCycle->getCreated();
        $disputeCreatedCloned = clone $disputeCreated;
        $date_waiting = $disputeCreatedCloned->modify('+'.self::WAITING_DAY_AFTER_OPENED_DISPUTE.' days');
        $diffDateWaiting = date_diff(new \DateTime(), $date_waiting);

        return [
            'date_waiting' => $date_waiting->format('d.m.Y H:i:s'),
            'date_waiting_params' => [
                'd' => $diffDateWaiting->invert ? 0: $diffDateWaiting->days,
                'h' => $diffDateWaiting->invert ? 0: $diffDateWaiting->h,
                'i' => $diffDateWaiting->invert ? 0: $diffDateWaiting->i,
            ],
        ];
    }

    /**
     * @param Payment $payment
     * @return array
     */
    public function getDeadlineDay(Payment $payment)
    {
        $dateDeadline = DealDeadlineManager::getDealDeadlineDate($payment);
        $deadlineDiff = date_diff(new \DateTime(), $dateDeadline);

        return [
            'date_deadline' => $dateDeadline->format('d.m.Y H:i:s'),
            'date_deadline_params' => [
                'd' => $deadlineDiff->invert ? 0: $deadlineDiff->days,
                'h' => $deadlineDiff->invert ? 0: $deadlineDiff->h,
                'i' => $deadlineDiff->invert ? 0: $deadlineDiff->i,
            ]
        ];
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getHistoryDisputeOpen(DisputeCycle $disputeCycle, User $user)
    {
        /** @var \DateTime $expectedCloseDate */
        $history = [];
        /** @var Dispute $dispute */
        $dispute = $disputeCycle->getDispute();
        /** @var Payment $payment */
        $payment = $dispute->getDeal()->getPayment();
        if ($dispute) {
            $disputeCreated = $disputeCycle->getCreated();
            $date_waiting = $this->getWaitingDay($disputeCycle);
            $expectedCloseDate = DisputeManager::getDisputeExpectedCloseDate($disputeCycle);
            $dateDeadline = $this->getDeadlineDay($payment);

            $history[] = [
                'code' => $this->getCodeForAgent($dispute, $user, self::DISPUTE_OPEN),
                'data' => [
                    'expected_close_date'=> $expectedCloseDate->format('d.m.Y H:i:s'),
                    'expected_days_for_close'=> DisputeManager::countDifferenceWithCurrentDataInDays($expectedCloseDate),
                    'date_waiting' => $date_waiting['date_waiting'],
                    'date_waiting_params' => $date_waiting['date_waiting_params'],
                    'date_open_dispute' => $disputeCreated->format('d.m.Y H:i:s'),
                    'date_deadline' => $dateDeadline['date_deadline'],
                    'date_deadline_params' => $dateDeadline['date_deadline_params'],
                ],
                'created' => $disputeCreated->format('d.m.Y H:i:s')
            ];
        }

        return $history;
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getHistoryDiscountRequests(DisputeCycle $disputeCycle, User $user)
    {
        /**
         * @var ArrayCollection $discountRequests
         * @var DiscountRequest $discountRequest
         * @var DiscountConfirm $discountConfirm
         */
        $discountRequests = $disputeCycle->getDiscountRequests();
        /** @var Dispute $dispute */
        $dispute = $disputeCycle->getDispute();
        /** @var Payment $payment */
        $payment = $dispute->getDeal()->getPayment();

        $history = [];

        if ($discountRequests) {
            $date_waiting = $this->getWaitingDay($disputeCycle);
            $expectedCloseDate = DisputeManager::getDisputeExpectedCloseDate($disputeCycle);
            $expectedDaysForClose = DisputeManager::countDifferenceWithCurrentDataInDays($expectedCloseDate);
            $dateDeadline = $this->getDeadlineDay($payment);
            $dateOpenDispute = $disputeCycle->getCreated()->format('d.m.Y H:i:s');

            $data = [
                'expected_close_date'=> $expectedCloseDate->format('d.m.Y H:i:s'),
                'expected_days_for_close'=> $expectedDaysForClose,
                'date_waiting' => $date_waiting['date_waiting'],
                'date_waiting_params' => $date_waiting['date_waiting_params'],
                'date_open_dispute' => $dateOpenDispute,
                'date_deadline' => $dateDeadline['date_deadline'],
                'date_deadline_params' => $dateDeadline['date_deadline_params'],
            ];

            foreach ($discountRequests as $discountRequest){
                // 1. request
                $discountRequestCreated = $discountRequest->getCreated()->format('d.m.Y H:i:s');
                $data['amount'] = $discountRequest->getAmount();

                $history[] = [
                    'code' => $this->getCodeForAuthorRequest($discountRequest, $user, self::DISCOUNT_REQUEST),
                    'data' => $data,
                    'created' => $discountRequestCreated
                ];
                // 2. request confirm
                $discountConfirm = $discountRequest->getConfirm();
                if ( $discountConfirm && $discountConfirm->isStatus() ) {
                    $discountConfirmCreated = $discountConfirm->getCreated()->format('d.m.Y H:i:s');

                    $history[] = [
                        'code' => $this->getCodeForAuthorConfirm($discountConfirm, $user, self::DISCOUNT_REQUEST_ACCEPTED),
                        'data' => $data,
                        'created' => $discountConfirmCreated
                    ];
                }
                // 3. request refuse
                if ( $discountConfirm && !$discountConfirm->isStatus() ) {
                    $discountConfirmCreated = $discountConfirm->getCreated()->format('d.m.Y H:i:s');
                    $history[] = [
                        'code' => $this->getCodeForAuthorConfirm($discountConfirm, $user, self::DISCOUNT_REQUEST_REJECTED),
                        'data' => $data,
                        'created' => $discountConfirmCreated
                    ];
                }
            }
        }

        return $history;
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getHistoryRefundRequests(DisputeCycle $disputeCycle, User $user)
    {
        /**
         * @var ArrayCollection $refundRequests
         * @var RefundRequest $refundRequest
         * @var RefundConfirm $refundConfirm
         */
        $refundRequests = $disputeCycle->getRefundRequests();
        /** @var Dispute $dispute */
        $dispute = $disputeCycle->getDispute();
        /** @var Payment $payment */
        $payment = $dispute->getDeal()->getPayment();

        $history = [];

        if ($refundRequests) {
            $date_waiting = $this->getWaitingDay($disputeCycle);
            $expectedCloseDate = DisputeManager::getDisputeExpectedCloseDate($disputeCycle);
            $expectedDaysForClose = DisputeManager::countDifferenceWithCurrentDataInDays($expectedCloseDate);
            $dateDeadline = $this->getDeadlineDay($payment);
            $dateOpenDispute = $disputeCycle->getCreated()->format('d.m.Y H:i:s');

            $data = [
                'amount' => $this->refundManager->getRefundAmountByDispute($dispute),
                'expected_close_date'=> $expectedCloseDate->format('d.m.Y H:i:s'),
                'expected_days_for_close'=> $expectedDaysForClose,
                'date_waiting' => $date_waiting['date_waiting'],
                'date_waiting_params' => $date_waiting['date_waiting_params'],
                'date_open_dispute' => $dateOpenDispute,
                'date_deadline' => $dateDeadline['date_deadline'],
                'date_deadline_params' => $dateDeadline['date_deadline_params'],
                'deal_type_ident' => $dispute->getDeal()->getDealType()->getIdent(),
            ];

            foreach ($refundRequests as $refundRequest){

                // 1. request
                $refundRequestCreated = $refundRequest->getCreated()->format('d.m.Y H:i:s');
                $history[] = [
                    'code' => $this->getCodeForAuthorRequest($refundRequest, $user, self::REFUND_REQUEST),
                    'data' => $data,
                    'created' => $refundRequestCreated
                ];
                // 2. request confirm
                $refundConfirm = $refundRequest->getConfirm();
                if ( $refundConfirm && $refundConfirm->isStatus() ) {
                    $refundConfirmCreated = $refundConfirm->getCreated()->format('d.m.Y H:i:s');

                    $history[] = [
                        'code' => $this->getCodeForAuthorConfirm($refundConfirm, $user, self::REFUND_REQUEST_ACCEPTED),
                        'data' => $data,
                        'created' => $refundConfirmCreated
                    ];
                }
                // 3. request refuse
                if ( $refundConfirm && !$refundConfirm->isStatus() ) {
                    $refundConfirmCreated = $refundConfirm->getCreated()->format('d.m.Y H:i:s');
                    $history[] = [
                        'code' => $this->getCodeForAuthorConfirm($refundConfirm, $user, self::REFUND_REQUEST_REJECTED),
                        'data' => $data,
                        'created' => $refundConfirmCreated
                    ];
                }
            }
        }

        return $history;
    }
    /**
     * @param DisputeCycle $disputeCycle
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getHistoryArbitrageRequests(DisputeCycle $disputeCycle, User $user)
    {
        /**
         * @var ArrayCollection $arbitrageRequests
         * @var ArbitrageRequest $arbitrageRequest
         * @var ArbitrageConfirm $arbitrageConfirm
         */
        $arbitrageRequests = $disputeCycle->getArbitrageRequests();
        /** @var Dispute $dispute */
        $dispute = $disputeCycle->getDispute();
        /** @var Payment $payment */
        $payment = $dispute->getDeal()->getPayment();

        $history = [];

        if ($arbitrageRequests) {
            $expectedCloseDate = DisputeManager::getDisputeExpectedCloseDate($disputeCycle);
            $expectedDaysForClose = DisputeManager::countDifferenceWithCurrentDataInDays($expectedCloseDate);
            $dateDeadline = $this->getDeadlineDay($payment);
            $dateOpenDispute = $disputeCycle->getCreated()->format('d.m.Y H:i:s');

            $data =  [
                'expected_close_date'=> $expectedCloseDate->format('d.m.Y H:i:s'),
                'expected_days_for_close'=> $expectedDaysForClose,
                'date_open_dispute' => $dateOpenDispute,
                'date_deadline' => $dateDeadline['date_deadline'],
                'date_deadline_params' => $dateDeadline['date_deadline_params'],
            ];

            foreach ($arbitrageRequests as $arbitrageRequest){
                $arbitrageTrigger = $this->getTriggerArbitrage($arbitrageRequest, $disputeCycle);
                switch ($arbitrageTrigger) {
                    case 'DiscountConfirm':
                        // 1. arbitrage request after discount
                        $arbitrageRequestCreated = $arbitrageRequest->getCreated()->format('d.m.Y H:i:s');
                        $history[] = [
                            'code' => $this->getCodeForAuthorRequest($arbitrageRequest, $user, self::ARBITRAGE_REQUEST_WITH_DISCOUNT),
                            'data' => $data,
                            'created' => $arbitrageRequestCreated
                        ];
                        break;
                    case 'RefundConfirm':
                        // 2. arbitrage request after refund
                        $arbitrageRequestCreated = $arbitrageRequest->getCreated()->format('d.m.Y H:i:s');
                        $history[] = [
                            'code' => $this->getCodeForAuthorRequest($arbitrageRequest, $user, self::ARBITRAGE_REQUEST_WITH_REFUND),
                            'data' => $data,
                            'created' => $arbitrageRequestCreated
                        ];
                        break;
                    case 'TribunalConfirm':
                        // 3. arbitrage request after tribunal
                        $arbitrageRequestCreated = $arbitrageRequest->getCreated()->format('d.m.Y H:i:s');
                        $history[] = [
                            'code' => $this->getCodeForAuthorRequest($arbitrageRequest, $user, self::ARBITRAGE_REQUEST_WITH_TRIBUNAL),
                            'data' => $data,
                            'created' => $arbitrageRequestCreated
                        ];
                        break;
                    default:
                        // 3. arbitrage request
                        $arbitrageRequestCreated = $arbitrageRequest->getCreated()->format('d.m.Y H:i:s');
                        $history[] = [
                            'code' => $this->getCodeForAuthorRequest($arbitrageRequest, $user, self::ARBITRAGE_REQUEST),
                            'data' => $data,
                            'created' => $arbitrageRequestCreated
                        ];
                }
                // 4. arbitrage confirm
                $arbitrageConfirm = $arbitrageRequest->getConfirm();
                if ( $arbitrageConfirm && $arbitrageConfirm->isStatus() ) {
                    $arbitrageConfirmCreated = $arbitrageConfirm->getCreated()->format('d.m.Y H:i:s');

                    $history[] = [
                        'code' => $this->getCodeForAuthorConfirm($arbitrageConfirm, $user, self::ARBITRAGE_REQUEST_ACCEPTED),
                        'data' => $data,
                        'created' => $arbitrageConfirmCreated
                    ];
                }
                // 5. arbitrage refuse
                if ( $arbitrageConfirm && !$arbitrageConfirm->isStatus() ) {
                    $arbitrageConfirmCreated = $arbitrageConfirm->getCreated()->format('d.m.Y H:i:s');
                    $history[] = [
                        'code' => $this->getCodeForAuthorConfirm($arbitrageConfirm, $user, self::ARBITRAGE_REQUEST_REJECTED),
                        'data' => $data,
                        'created' => $arbitrageConfirmCreated
                    ];
                }
            }
        }

        return $history;
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getHistoryTribunalRequests(DisputeCycle $disputeCycle, User $user)
    {
        /**
         * @var ArrayCollection $tribunalRequests
         * @var TribunalRequest $tribunalRequest
         * @var TribunalConfirm $tribunalConfirm
         */
        $tribunalRequests = $disputeCycle->getTribunalRequests();
        /** @var Dispute $dispute */
        $dispute = $disputeCycle->getDispute();
        /** @var Payment $payment */
        $payment = $dispute->getDeal()->getPayment();

        $history = [];

        if ($tribunalRequests) {
            $expectedCloseDate = DisputeManager::getDisputeExpectedCloseDate($disputeCycle);
            $expectedDaysForClose = DisputeManager::countDifferenceWithCurrentDataInDays($expectedCloseDate);
            $dateDeadline = $this->getDeadlineDay($payment);
            $dateOpenDispute = $disputeCycle->getCreated()->format('d.m.Y H:i:s');

            $data =  [
                'expected_close_date'=> $expectedCloseDate->format('d.m.Y H:i:s'),
                'expected_days_for_close'=> $expectedDaysForClose,
                'date_open_dispute' => $dateOpenDispute,
                'date_deadline' => $dateDeadline['date_deadline'],
                'date_deadline_params' => $dateDeadline['date_deadline_params'],
            ];

            foreach ($tribunalRequests as $tribunalRequest){
                // 1. request
                $tribunalRequestCreated = $tribunalRequest->getCreated()->format('d.m.Y H:i:s');
                $history[] = [
                    'code' => $this->getCodeForAuthorRequest($tribunalRequest, $user, self::TRIBUNAL_REQUEST),
                    'data' => $data,
                    'created' => $tribunalRequestCreated
                ];
                // 2. request confirm
                $tribunalConfirm = $tribunalRequest->getConfirm();
                if ( $tribunalConfirm && $tribunalConfirm->isStatus() ) {
                    $tribunalConfirmCreated = $tribunalConfirm->getCreated()->format('d.m.Y H:i:s');

                    $history[] = [
                        'code' => $this->getCodeForAuthorConfirm($tribunalConfirm, $user, self::TRIBUNAL_REQUEST_ACCEPTED),
                        'data' => $data,
                        'created' => $tribunalConfirmCreated
                    ];
                }
                // 3. request refuse
                if ( $tribunalConfirm && !$tribunalConfirm->isStatus() ) {
                    $tribunalConfirmCreated = $tribunalConfirm->getCreated()->format('d.m.Y H:i:s');
                    $history[] = [
                        'code' => $this->getCodeForAuthorConfirm($tribunalConfirm, $user, self::TRIBUNAL_REQUEST_REJECTED),
                        'data' => $data,
                        'created' => $tribunalConfirmCreated
                    ];
                }
            }
        }

        return $history;
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getHistoryDiscount(DisputeCycle $disputeCycle, User $user)
    {
        /**
         * @var Discount $discount
         */
        $discount = $disputeCycle->getDiscount();
        /** @var Dispute $dispute */
        $dispute = $disputeCycle->getDispute();
        /** @var Payment $payment */
        $payment = $dispute->getDeal()->getPayment();

        $history = [];

        if ($discount) {
            $date_waiting = $this->getWaitingDay($disputeCycle);
            $expectedCloseDate = DisputeManager::getDisputeExpectedCloseDate($disputeCycle);
            $expectedDaysForClose = DisputeManager::countDifferenceWithCurrentDataInDays($expectedCloseDate);
            $dateDeadline = $this->getDeadlineDay($payment);
            $dateOpenDispute = $disputeCycle->getCreated()->format('d.m.Y H:i:s');

            $data = [
                'expected_close_date'=> $expectedCloseDate->format('d.m.Y H:i:s'),
                'expected_days_for_close'=> $expectedDaysForClose,
                'date_waiting' => $date_waiting['date_waiting'],
                'date_waiting_params' => $date_waiting['date_waiting_params'],
                'date_open_dispute' => $dateOpenDispute,
                'date_deadline' => $dateDeadline['date_deadline'],
                'date_deadline_params' => $dateDeadline['date_deadline_params'],
                'amount' => $discount->getAmount()
            ];

            $discountCreated = $discount->getCreated()->format('d.m.Y H:i:s');
            $history[] = [
                'code' => $this->getCodeForAgent($dispute, $user, self::DISCOUNT),
                'data' => $data,
                'created' => $discountCreated
            ];
        }

        return $history;
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getHistoryRefund(DisputeCycle $disputeCycle, User $user)
    {
        /**
         * @var Refund $refund
         */
        $refund = $disputeCycle->getRefund();
        /** @var Dispute $dispute */
        $dispute = $disputeCycle->getDispute();
        /** @var Payment $payment */
        $payment = $dispute->getDeal()->getPayment();

        $history = [];

        if ($refund) {
            $date_waiting = $this->getWaitingDay($disputeCycle);
            $expectedCloseDate = DisputeManager::getDisputeExpectedCloseDate($disputeCycle);
            $expectedDaysForClose = DisputeManager::countDifferenceWithCurrentDataInDays($expectedCloseDate);
            $dateDeadline = $this->getDeadlineDay($payment);
            $dateOpenDispute = $disputeCycle->getCreated()->format('d.m.Y H:i:s');

            $data = [
                'expected_close_date'=> $expectedCloseDate->format('d.m.Y H:i:s'),
                'expected_days_for_close'=> $expectedDaysForClose,
                'date_waiting' => $date_waiting['date_waiting'],
                'date_waiting_params' => $date_waiting['date_waiting_params'],
                'date_open_dispute' => $dateOpenDispute,
                'date_deadline' => $dateDeadline['date_deadline'],
                'date_deadline_params' => $dateDeadline['date_deadline_params'],
                'amount' => $this->refundManager->getRefundAmountByDispute($dispute)
            ];

            $refundCreated = $refund->getCreated()->format('d.m.Y H:i:s');
            $history[] = [
                'code' => $this->getCodeForAgent($dispute, $user, self::REFUND),
                'data' => $data,
                'created' => $refundCreated
            ];
        }

        return $history;
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getHistoryTribunal(DisputeCycle $disputeCycle, User $user)
    {
        /**
         * @var Tribunal $tribunal
         */
        $tribunal = $disputeCycle->getTribunal();
        /** @var Dispute $dispute */
        $dispute = $disputeCycle->getDispute();
        /** @var Payment $payment */
        $payment = $dispute->getDeal()->getPayment();

        $history = [];

        if ($tribunal) {
            $expectedCloseDate = DisputeManager::getDisputeExpectedCloseDate($disputeCycle);
            $expectedDaysForClose = DisputeManager::countDifferenceWithCurrentDataInDays($expectedCloseDate);
            $dateDeadline = $this->getDeadlineDay($payment);
            $dateOpenDispute = $disputeCycle->getCreated()->format('d.m.Y H:i:s');

            $data =  [
                'expected_close_date'=> $expectedCloseDate->format('d.m.Y H:i:s'),
                'expected_days_for_close'=> $expectedDaysForClose,
                'date_open_dispute' => $dateOpenDispute,
                'date_deadline' => $dateDeadline['date_deadline'],
                'date_deadline_params' => $dateDeadline['date_deadline_params'],
            ];

            $tribunalCreated = $tribunal->getCreated()->format('d.m.Y H:i:s');
            $history[] = [
                'code' => $this->getCodeForAgent($dispute, $user, self::TRIBUNAL),
                'data' => $data,
                'created' => $tribunalCreated
            ];
        }

        return $history;
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @param User $user
     * @return array
     * @throws \Exception
     */
    public function getHistoryWarrantyExtension(DisputeCycle $disputeCycle, User $user)
    {
        $warrantyExtensions = $disputeCycle->getWarrantyExtensions();
        /** @var Dispute $dispute */
        $dispute = $disputeCycle->getDispute();
        /** @var Payment $payment */
        $payment = $dispute->getDeal()->getPayment();

        $history = [];

        if (!$warrantyExtensions->isEmpty()) {
            $expectedCloseDate = DisputeManager::getDisputeExpectedCloseDate($disputeCycle);
            $expectedDaysForClose = DisputeManager::countDifferenceWithCurrentDataInDays($expectedCloseDate);
            $dateDeadline = $this->getDeadlineDay($payment);
            $data = [
                'expected_close_date' => $expectedCloseDate->format('d.m.Y H:i:s'),
                'expected_days_for_close' => $expectedDaysForClose,
                'date_deadline' => $dateDeadline['date_deadline'],
                'date_deadline_params' => $dateDeadline['date_deadline_params'],
            ];

            /** @var ArrayCollection $warrantyExtensions */
            /** @var WarrantyExtension $warrantyExtension */
            foreach ($warrantyExtensions as $warrantyExtension) {
                if ($warrantyExtension->getDays() > 0) {
                    $data['warranty_day'] = $warrantyExtension->getDays();

                    $history[] = [
                        'code' => $this->getCodeForAgent($dispute, $user, self::WARRANTY_EXTENSION),
                        'data' => $data,
                        'created' => $warrantyExtension->getCreated()->format('d.m.Y H:i:s')
                    ];
                }
            }
        }

        return $history;
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @param $user
     * @return array
     * @throws \Exception
     */
    public function getHistoryClosed(DisputeCycle $disputeCycle, User $user)
    {
        /** @var Dispute $dispute */
        $dispute = $disputeCycle->getDispute();
        /** @var Payment $payment */
        $payment = $dispute->getDeal()->getPayment();

        $history = [];

        if ($dispute) {
            $expectedCloseDate = DisputeManager::getDisputeExpectedCloseDate($disputeCycle);
            $expectedDaysForClose = DisputeManager::countDifferenceWithCurrentDataInDays($expectedCloseDate);
            $dateDeadline = $this->getDeadlineDay($payment);

            $data =  [
                'expected_close_date'=> $expectedCloseDate->format('d.m.Y H:i:s'),
                'expected_days_for_close'=> $expectedDaysForClose,
                'date_deadline' => $dateDeadline['date_deadline'],
                'date_deadline_params' => $dateDeadline['date_deadline_params'],
            ];

            if($disputeCycle->getClosed()) {
                /** @var Discount $discount */
                $discount = $disputeCycle->getDiscount();
                $warranty_extension = DisputeManager::getLastWarrantyExtension($disputeCycle->getWarrantyExtensions());

                //1. спор отозван кастомером
                // если isEmpty === true это озночает что закрыл кастомер
                if ($disputeCycle->getWarrantyExtensions()->isEmpty()) {
                    $data['date_closed_dispute'] = $disputeCycle->getClosed()->format('d.m.Y H:i:s');

                    $history[] = [
                        'code' => $this->getCodeForAgent($dispute, $user, self::DISPUTE_CANCELED),
                        'data' => $data,
                        'created' => $disputeCycle->getClosed()->format('d.m.Y H:i:s')
                    ];
                }

                //2. спор закрыт без скидки и без добавленных дней
                if (!$discount && $warranty_extension && $warranty_extension->getDays() === 0) {
                    $data['date_closed_dispute'] = $disputeCycle->getClosed()->format('d.m.Y H:i:s');

                    $history[] = [
                        'code' => $this->getCodeForAgent($dispute, $user, self::DISPUTE_CLOSED),
                        'data' => $data,
                        'created' => $disputeCycle->getClosed()->format('d.m.Y H:i:s')
                    ];
                }

                //3. спор закрыт без скидки но с добавленными днями
                if (!$discount && $warranty_extension && $warranty_extension->getDays() > 0) {
                    $data['date_closed_dispute'] = $disputeCycle->getClosed()->format('d.m.Y H:i:s');
                    $data['warranty_day'] = $warranty_extension->getDays();

                    $history[] = [
                        'code' => $this->getCodeForAgent($dispute, $user, self::DISPUTE_CLOSED_WITH_DAYS),
                        'data' => $data,
                        'created' => $disputeCycle->getClosed()->format('d.m.Y H:i:s')
                    ];
                }

                //4. спор закрыт со скидкой и без добавленных дней
                if ($discount && $warranty_extension && $warranty_extension->getDays() === 0) {
                    $data['date_closed_dispute'] = $disputeCycle->getClosed()->format('d.m.Y H:i:s');
                    $data['amount'] = $discount->getAmount();

                    $history[] = [
                        'code' => $this->getCodeForAgent($dispute, $user, self::DISPUTE_CLOSED_DISCOUNT),
                        'data' => $data,
                        'created' => $disputeCycle->getClosed()->format('d.m.Y H:i:s')
                    ];
                }

                //5. спор закрыт со скидкой и с добавленными днями
                if ($discount && $warranty_extension && $warranty_extension->getDays() > 0) {
                    $data['date_closed_dispute'] = $disputeCycle->getClosed()->format('d.m.Y H:i:s');
                    $data['warranty_day'] = $warranty_extension->getDays();
                    $data['amount'] = $discount->getAmount();

                    $history[] = [
                        'code' => $this->getCodeForAgent($dispute, $user, self::DISPUTE_CLOSED_DISCOUNT_WITH_DAYS),
                        'data' => $data,
                        'created' => $disputeCycle->getClosed()->format('d.m.Y H:i:s')
                    ];
                }
            }
            if ($this->disputeStatus->getStatus($dispute)['status'] === 'refund_is_made') {
                //5. спор закрыт с возвратом
                //если есть возврат то дату закрытия привяжем к возврату.
                /** @var Refund $refund */
                $refund = $disputeCycle->getRefund();
                if ($refund) {
                    $data['amount'] = $this->refundManager->getRefundAmountByDispute($dispute);
                    $data['date_closed_dispute'] = $refund->getCreated()->format('d.m.Y H:i:s');

                    $history[] = [
                        'code' => $this->getCodeForAgent($dispute, $user, self::DISPUTE_CLOSED_REFUND),
                        'data' => $data,
                        'created' => $refund->getCreated()->format('d.m.Y H:i:s')
                    ];
                }
            }
        }

        return $history;
    }

    /**
     * @param Dispute $dispute
     * @param User $user
     * @param $code
     * @return string
     * @throws \Exception
     */
    public function getCodeForAgent(Dispute $dispute, User $user, $code)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $role = null;
        if ($this->rbacManager->hasRole($user, 'Operator')) {
            $role = self::ROLE_OPERATOR;
        }
        if ($deal->getContractor()->getUser() === $user) {
            $role = self::ROLE_CONTRACTOR;
        }
        if ($deal->getCustomer()->getUser() === $user) {
            $role = self::ROLE_CUSTOMER;
        }

        // если роль не определена то:
        if ( $role === null ) {

            throw new LogicException(null, LogicException::DISPUTE_HISTORY_ROLE_OF_USER_FAILED);
        }

        return self::buildCode($code, $role);
    }

    /**
     * @param DiscountRequest|RefundRequest|ArbitrageRequest|TribunalRequest $request
     * @param User $user
     * @param $code
     * @return string
     * @throws \Exception
     */
    public function getCodeForAuthorRequest($request, User $user, $code)
    {
        /** @var Dispute $dispute */
        $dispute = $request->getDispute();
        $role = null;
        if ($this->rbacManager->hasRole($user, 'Operator')) {
            $role = self::ROLE_OBSERVER;
        }
        if ($request->getAuthor()->getUser() === $user) {
            $role = self::ROLE_AUTHOR;
        }
        if (DisputeManager::isUserPartOfDispute($dispute, $user) && $request->getAuthor()->getUser() !== $user) {
            $role = self::ROLE_NOT_AUTHOR;
        }
        // если роль не определена то:
        if ( $role === null ) {

            throw new LogicException(null, LogicException::DISPUTE_HISTORY_ROLE_OF_USER_FAILED);
        }

        return self::buildCode($code, $role);
    }

    /**
     * @param DiscountConfirm|RefundConfirm|ArbitrageConfirm|TribunalConfirm $confirm
     * @param User $user
     * @param $code
     * @return string
     * @throws \Exception
     */
    public function getCodeForAuthorConfirm($confirm, User $user, $code)
    {
        $role = null;
        $explode = explode('\\', \get_class($confirm));
        $confirm_class = end($explode);
        /** @var Dispute $dispute */
        switch ($confirm_class){
            case 'DiscountConfirm':
                if ($this->rbacManager->hasRole($user, 'Operator')) {
                    $role = self::ROLE_OBSERVER;
                }
                if ($confirm->getAuthor()->getUser() === $user) {
                    $role = self::ROLE_AUTHOR;
                }
                /** @var DealAgent $authorRequest */
                $authorRequest = $confirm->getDiscountRequest()->getAuthor();
                if ($authorRequest->getUser() === $user) {
                    $role = self::ROLE_NOT_AUTHOR;
                }
                break;
            case 'RefundConfirm':
                if ($this->rbacManager->hasRole($user, 'Operator')) {
                    $role = self::ROLE_OBSERVER;
                }
                if ($confirm->getAuthor()->getUser() === $user) {
                    $role = self::ROLE_AUTHOR;
                }
                /** @var DealAgent $authorRequest */
                $authorRequest = $confirm->getRefundRequest()->getAuthor();
                if ($authorRequest->getUser() === $user) {
                    $role = self::ROLE_NOT_AUTHOR;
                }
                break;
            case 'ArbitrageConfirm':
                $dispute = $confirm->getArbitrageRequest()->getDispute();
                if ($confirm->getAuthor() === $user) {
                    $role = self::ROLE_AUTHOR;
                }
                /** @var DealAgent $authorRequest */
                $authorRequest = $confirm->getArbitrageRequest()->getAuthor();
                if ($authorRequest->getUser() === $user) {
                    $role = self::ROLE_NOT_AUTHOR;
                }
                if (DisputeManager::isUserPartOfDispute($dispute, $user) && $authorRequest->getUser() !== $user) {
                    $role = self::ROLE_OBSERVER;
                }
                break;
            case 'TribunalConfirm':
                $dispute = $confirm->getTribunalRequest()->getDispute();
                if ($confirm->getAuthor() === $user) {
                    $role = self::ROLE_AUTHOR;
                }
                /** @var DealAgent $authorRequest */
                $authorRequest = $confirm->getTribunalRequest()->getAuthor();
                if ($authorRequest->getUser() === $user) {
                    $role = self::ROLE_NOT_AUTHOR;
                }
                if (DisputeManager::isUserPartOfDispute($dispute, $user) && $authorRequest->getUser() !== $user) {
                    $role = self::ROLE_OBSERVER;
                }
                break;
        }

        // если роль не определена то:
        if ( $role === null ) {

            throw new LogicException(null, LogicException::DISPUTE_HISTORY_ROLE_OF_USER_FAILED);
        }

        return self::buildCode($code, $role);
    }

    /**
     * @param $code
     * @param $role
     * @return string
     */
    public static function buildCode($code, $role)
    {
        return $code . '_FOR_' . $role;
    }


    /**
     * @param DiscountRequest|RefundRequest|ArbitrageRequest|TribunalRequest $request
     * @param DisputeCycle $disputeCycle
     * @return string|null
     */
    public function getTriggerArbitrage($request, DisputeCycle $disputeCycle)
    {
        $refundRequests = $disputeCycle->getRefundRequests();
        $discountRequests = $disputeCycle->getDiscountRequests();
        $tribunalRequests = $disputeCycle->getTribunalRequests();
        $arbitrageRequests = $disputeCycle->getArbitrageRequests();

        $params = [
            'count_refuse' => 0,
            'count_request' => 0,
            'fixed_date' => $request->getCreated(),
            'prev_date' => null,
            'prev_confirm' => null,
        ];

        //1. discount
        //ишем подходяшие отказы на скидки и передаем предыдущую найденую подходящюю дату
        $params = $this->getParamsPrevRequestRefuse($discountRequests, $params);
        //инвертируем условие запуска автоматического создания арбитража
        if ($params['count_refuse'] < DiscountManager::DISCOUNT_REQUEST_REFUSES_LIMIT
            && $this->getLastClassName($params['prev_confirm']) === 'DiscountConfirm') {
            //значит возврат не мог спровоцировать запуск арбитража
            $params['prev_date'] = null;
            $params['prev_confirm'] = null;
        }
        //2. refund
        //ишем подходяшие отказы на возвраты и передаем предыдущую найденую подходящюю дату
        $params = $this->getParamsPrevRequestRefuse($refundRequests, $params);
        //инвертируем условие запуска автоматического создания арбитража
        if ($params['count_refuse'] < RefundManager::REFUND_REQUEST_REFUSES_LIMIT
            && $this->getLastClassName($params['prev_confirm']) === 'RefundConfirm') {
            //значит возврат не мог спровоцировать запуск арбитража
            $params['prev_date'] = null;
            $params['prev_confirm'] = null;
        }
        //3. tribunal
        //ишем подходяшие отказы на трибуналы и передаем предыдущую найденую подходящюю дату
        $params = $this->getParamsPrevRequestRefuse($tribunalRequests, $params);
        //инвертируем условие запуска автоматического создания арбитража
        if (($params['count_refuse'] < TribunalManager::TRIBUNAL_REQUEST_REFUSES_LIMIT || $arbitrageRequests->count() !== 1)
            && $this->getLastClassName($params['prev_confirm']) === 'TribunalConfirm') {
            //значит возврат не мог спровоцировать запуск арбитража
            $params['prev_date'] = null;
            $params['prev_confirm'] = null;
        }

        return $this->getLastClassName($params['prev_confirm']);
    }

    /**
     * @param $class
     * @return mixed
     */
    public function getLastClassName($class)
    {
        $explode = explode('\\', \get_class($class));

        return $class ? end($explode) : null;
    }

    /**
     * @param $refundRequests
     * @param array $params
     * @return array
     */
    public function getParamsPrevRequestRefuse($refundRequests, Array $params)
    {
        /**
         * @var DiscountRequest|RefundRequest|ArbitrageRequest|TribunalRequest $request
         * @var DiscountConfirm|RefundConfirm|ArbitrageConfirm|TribunalConfirm $confirm
         */
        $params['count_refuse'] = 0;
        $params['count_request'] = 0;
        /** @var ArrayCollection $refundRequests */
        foreach ($refundRequests as $request) {
            if ($request->getCreated() <= $params['fixed_date']) {
                $params['count_request']++;
            }

            $confirm = $request->getConfirm();
            if ($confirm && !$confirm->isStatus() ) {
                $date = $confirm->getCreated() ;
                //считаем только те которые меньше даты переданного реквеста
                if ($date <= $params['fixed_date']) {
                    $params['count_refuse']++;
                    //Вычисляем максимально близкую дату
                    //1. фиксируем полученную дату
                    //2. фиксируем сущность для последуюшей идентификации
                    if ($date >= $params['prev_date']) {
                        $params['prev_date'] = $date;
                        $params['prev_confirm'] = $confirm;
                    }
                }
            }
        }

        return $params;
    }
}