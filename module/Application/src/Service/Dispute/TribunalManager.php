<?php

namespace Application\Service\Dispute;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\Tribunal;
use Application\Entity\TribunalConfirm;
use Application\Entity\TribunalRequest;
use Application\Entity\User;
use Application\Provider\Mail\DisputeNotificationSender;
use Application\Service\Payment\PaymentPolitics;
use Core\Exception\LogicException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleCode\Service\SmsStrategyContext;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class TribunalManager
 * @package Application\Service\Dispute
 */
class TribunalManager
{
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    const TRIBUNAL_REQUEST_REFUSES_LIMIT = 1;

    /**
     * notify params
     */
    private $email_notify_referred_to_tribunal;
    private $sms_notify_referred_to_tribunal;
    private $email_notify_tribunal_request;
    private $sms_notify_tribunal_request;
    private $email_notify_tribunal_confirm;
    private $sms_notify_tribunal_confirm;

    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var DisputePolitics
     */
    private $disputePolitics;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * @var TribunalPolitics
     */
    private $tribunalPolitics;

    /**
     * @var array
     */
    private $config;

    /**
     * @var DisputeNotificationSender
     */
    private $disputeNotificationSender;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * @var array
     */
    private $disputeOutput = [];

    /**
     * @var DisputeStatus
     */
    private $disputeStatus;

    /**
     * TribunalManager constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param DisputePolitics $disputePolitics
     * @param RbacManager $rbacManager
     * @param PaymentPolitics $paymentPolitics
     * @param TribunalPolitics $tribunalPolitics
     * @param DisputeNotificationSender $disputeNotificationSender
     * @param PaymentOrderManager $paymentOrderManager
     * @param DisputeStatus $disputeStatus
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                DisputePolitics $disputePolitics,
                                RbacManager $rbacManager,
                                PaymentPolitics $paymentPolitics,
                                TribunalPolitics $tribunalPolitics,
                                DisputeNotificationSender $disputeNotificationSender,
                                PaymentOrderManager $paymentOrderManager,
                                DisputeStatus $disputeStatus,
                                array $config)
    {
        $this->entityManager             = $entityManager;
        $this->userManager               = $userManager;
        $this->baseAuthManager               = $baseAuthManager;
        $this->disputePolitics           = $disputePolitics;
        $this->rbacManager               = $rbacManager;
        $this->paymentPolitics           = $paymentPolitics;
        $this->tribunalPolitics          = $tribunalPolitics;
        $this->disputeNotificationSender = $disputeNotificationSender;
        $this->paymentOrderManager       = $paymentOrderManager;
        $this->disputeStatus             = $disputeStatus;
        $this->config                    = $config;

        // set notify params from config
        $this->sms_notify_referred_to_tribunal = $this->getDealNotifyConfig($this->config, 'referred_to_tribunal', 'sms');
        $this->email_notify_referred_to_tribunal = $this->getDealNotifyConfig($this->config, 'referred_to_tribunal', 'email');
        $this->sms_notify_tribunal_request = $this->getDealNotifyConfig($config, 'tribunal_request', 'sms');
        $this->email_notify_tribunal_request = $this->getDealNotifyConfig($config, 'tribunal_request', 'email');
        $this->sms_notify_tribunal_confirm = $this->getDealNotifyConfig($config, 'tribunal_confirm', 'sms');
        $this->email_notify_tribunal_confirm = $this->getDealNotifyConfig($config, 'tribunal_confirm', 'email');
    }

    /**
     * @param $id
     * @return null|object
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getTribunalRequestById($id)
    {
        return $this->entityManager->find(TribunalRequest::class, $id);
    }

    /**
     * @param $disputeOutput
     * @return array|null
     */
    public function setDisputeOutput($disputeOutput)
    {
        if($disputeOutput && is_array($disputeOutput)){
            $this->disputeOutput = $disputeOutput;
        }

        return $this->disputeOutput;
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function isOperator(User $user)
    {
        return $this->rbacManager->hasRole($user, 'Operator');
    }

    /**
     *
     * @param User $user
     * @param Dispute $dispute
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionTribunal(User $user, Dispute $dispute): bool
    {
        /**
         * @var Deal $deal
         * @var Payment $payment
         */
        $deal = $dispute->getDeal();
        $payment = $deal->getPayment();
        $is_operator = $this->rbacManager->hasRole($user, 'Operator');
        $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);

        $is_deal_paid = $this->paymentPolitics->isDealPaid($total_incoming_amount, $payment->getExpectedValue());
        $is_allowed_add_tribunal = $this->disputePolitics->isAllowedAddTribunal($dispute);

        /**
         * 1 Если Operator
         * 2 Что сделка находится в статусе не ниже "Оплачено"
         * 3 Если разрешено политикой
         */
        return (
            $is_operator
            && $is_deal_paid
            && $is_allowed_add_tribunal
        );
    }

    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getAllowedTribunalRequestsByUser(User $user)
    {
        $allowedTribunalRequests = $this->entityManager
            ->getRepository(TribunalRequest::class)->findAllowedTribunalRequestsByUser($user);

        return $allowedTribunalRequests;
    }

    /**
     * @param Dispute $dispute
     * @return Tribunal
     * @throws \Exception
     */
    public function createTribunal(Dispute $dispute)
    {
        if ( $dispute->isClosed() ){
            throw new \Exception("The dispute is not in status \"open dispute\"");
        }

        if (! $this->disputePolitics->isAllowedAddTribunal($dispute)){
            throw new \Exception("Forbidden by dispute politics");
        }

        $disputeCycle = DisputeManager::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || ($disputeCycle->getClosed() !== null && !$dispute->isClosed())) {
            throw new LogicException(null, LogicException::DISPUTE_ACTIVE_CYCLE_NOT_FOUND);
        }

        $tribunal = new Tribunal();
        $tribunal->setCreated(new \DateTime());
        $tribunal->setDispute($dispute);
        $tribunal->setDisputeCycle($disputeCycle);

        $this->entityManager->persist($tribunal);
        $disputeCycle->setTribunal($tribunal);
        $this->entityManager->persist($disputeCycle);
        $this->entityManager->flush();

        //уведомить участников сделки о передаче спора в третейский суд
        $this->notifyDealAgentsAboutReferredToTribunal($dispute, $tribunal);

        return $tribunal;
    }

    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function checkTribunalRefusesOverlimit(Dispute $dispute)
    {
        $tribunalRequests = $dispute->getTribunalRequests();

        $negative_confirms = [];
        /** @var TribunalRequest $tribunalRequest */
        foreach ($tribunalRequests as $tribunalRequest) {
            /** @var TribunalConfirm $confirm */
            $confirm = $tribunalRequest->getConfirm();
            if (!$confirm->isStatus()) {
                $negative_confirms[] = $confirm;
            }
        }

        return $this->tribunalPolitics->isTribunalDeclineOverlimit($negative_confirms);
    }

    /**
     * @param Deal $deal
     * @param DealAgent $author
     * @return TribunalRequest
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function createTribunalRequest(Deal $deal, DealAgent $author)
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        if (!$dispute) {
            throw new \Exception('The deal has no dispute');
        }
        $disputeCycle = DisputeManager::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || ($disputeCycle->getClosed() !== null && !$dispute->isClosed())) {
            throw new LogicException(null, LogicException::DISPUTE_ACTIVE_CYCLE_NOT_FOUND);
        }

        $tribunalRequest = new TribunalRequest();
        $tribunalRequest->setCreated(new \DateTime());
        $tribunalRequest->setDispute($dispute);
        $tribunalRequest->setAuthor($author);
        $tribunalRequest->setDisputeCycle($disputeCycle);

        $this->entityManager->persist($tribunalRequest);
        $disputeCycle->addTribunalRequest($tribunalRequest);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush();

        if (!$tribunalRequest->getId()) {

            throw new \Exception('TribunalRequest not created');
        }

        //уведомить контрагента о запросе трибунала
        $this->notifyCounterAgentAboutTribunalRequest($tribunalRequest);

        return $tribunalRequest;
    }

    /**
     * @param TribunalRequest $tribunalRequest
     * @param User $user
     * @param boolean $status
     * @return TribunalConfirm
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function createTribunalConfirm(TribunalRequest $tribunalRequest, User $user, $status = true)
    {
        $tribunalConfirm = new TribunalConfirm();
        $tribunalConfirm->setCreated(new \DateTime());
        $tribunalConfirm->setTribunalRequest($tribunalRequest);
        $tribunalConfirm->setAuthor($user);
        $tribunalConfirm->setStatus($status);

        $this->entityManager->persist($tribunalConfirm);
        $this->entityManager->flush($tribunalConfirm);

        if (!$tribunalConfirm->getId()) {

            throw new \Exception('TribunalConfirm not created');
        }

        $tribunalRequest->setConfirm($tribunalConfirm);

        $this->entityManager->persist($tribunalRequest);
        $this->entityManager->flush($tribunalRequest);

        //уведомить контрагента о принятии решения по запросу трибунала
        $this->notifyCounterAgentAboutTribunalConfirm($tribunalConfirm);

        return $tribunalConfirm;
    }

    /**
     * @param TribunalRequest $tribunalRequest
     * @param User $user
     * @return TribunalConfirm
     * @throws \Exception
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createTribunalRefuse(TribunalRequest $tribunalRequest, User $user)
    {
        $status = false;

        return $this->createTribunalConfirm($tribunalRequest, $user, $status);
    }

    /**
     * @param Tribunal $tribunal
     * @return array
     */
    public function getTribunalOutput(Tribunal $tribunal)
    {
        $tribunalOutput = [];

        $tribunalOutput['id'] = $tribunal->getId();
        $tribunalOutput['created'] = $tribunal->getCreated()->format('d.m.Y H.i');

        return $tribunalOutput;
    }

    /**
     * @param TribunalRequest $tribunalRequest
     * @return array
     */
    public function getTribunalRequestOutput(TribunalRequest $tribunalRequest): array
    {
        try {
            /** @var User $user */
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        } catch (\Throwable $t){
            $user = null;
        }

        $tribunalRequestOutput = [];

        $tribunalRequestOutput['id'] = $tribunalRequest->getId();
        /** @var User $authorUser */
        $authorUser = $tribunalRequest->getAuthor()->getUser();
        $tribunalRequestOutput['created'] = $tribunalRequest->getCreated()->format('d.m.Y H:i');
        $tribunalRequestOutput['is_author'] = $user === $authorUser;
        $tribunalRequestOutput['author'] = [
            'name' => $authorUser->getLogin(),
            'email' => $authorUser->getEmail()->getEmail(),
            'phone' => '+'.$authorUser->getPhone()->getPhoneNumber(),
        ];
        $tribunalRequestOutput['confirm'] = null;
        if ($tribunalRequest->getConfirm()) {
            /** @var TribunalConfirm $tribunalConfirm */
            $tribunalConfirm = $tribunalRequest->getConfirm();
            $tribunalRequestOutput['confirm'] = [
                'id' => $tribunalConfirm->getId(),
                'status' => $tribunalConfirm->isStatus(),
                'author' => [
                    'name' => $tribunalConfirm->getAuthor()->getLogin(),
                    'email' => $tribunalConfirm->getAuthor()->getEmail()->getEmail(),
                    'phone' => '+'.$tribunalConfirm->getAuthor()->getPhone()->getPhoneNumber(),
                ],
                'created' => $tribunalConfirm->getCreated()->format('d.m.Y H:i'),
            ];
        }

        return $tribunalRequestOutput;
    }

    /**
     * @param $tribunalRequests
     * @return array
     * @throws \Exception
     */
    public function getTribunalRequestCollectionForOutput($tribunalRequests)
    {
        $tribunalRequestsOutput = [];
        /** @var ArrayCollection $tribunalRequests */
        foreach ($tribunalRequests as $tribunalRequest) {
            $tribunalRequestsOutput[] = $this->getTribunalRequestOutput($tribunalRequest);
        }

        return $tribunalRequestsOutput;
    }

    /**
     * @param User $user
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionTribunalRequest(User $user, Deal $deal): bool
    {
        /**
         * @var Dispute $dispute
         * @var Payment $payment
         */
        $dispute = $deal->getDispute();
        $payment = $deal->getPayment();
        $this->disputeStatus->setStatus($dispute);
        $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);
        $is_operator = $this->rbacManager->hasRole($user, 'Operator');
        $is_contractor = $deal->getContractor()->getUser() === $user;

        $is_deal_paid = $this->paymentPolitics->isDealPaid($total_incoming_amount, $payment->getExpectedValue());
        $is_allowed_add_tribunal = $dispute ? $this->disputePolitics->isAllowedAddTribunal($dispute) : false;
        /**
         * 1 Участник сделки в роли customer
         * 2 Что сделка находится в статусе не ниже "Оплачено"
         * 3 Если разрешено политикой создовать TribunalRequest
         * 4 Если разрешено политикой создовать Tribunal
         * 5 Если не оператор
         */
        return (
            $this->disputePolitics->isAllowedAddTribunalRequest($dispute)
            && $is_deal_paid
            && $is_allowed_add_tribunal
            && !$is_operator
            && !$is_contractor
        );
    }

    /**
     * @param User $user
     * @param TribunalRequest $tribunalRequest
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionTribunalConfirm(User $user, TribunalRequest $tribunalRequest)
    {
        $dispute = $tribunalRequest->getDispute();
        $this->disputeStatus->setStatus($dispute);
        $is_operator = $this->rbacManager->hasRole($user, 'Operator');
        /**
         * 1 Если разрешено политикой создовать TribunalConfirm
         */
        return  $this->disputePolitics->isAllowedAddTribunalConfirm($dispute, $user, $is_operator);
    }

    /**
     * @param User $user
     * @param TribunalRequest $tribunalRequest
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionTribunalRefuse(User $user, TribunalRequest $tribunalRequest)
    {
        /**
         * теже проверки что при создании TribunalConfirm
         * 1 Если разрешено политикой создовать TribunalConfirm
         */
        return $this->checkPermissionTribunalConfirm($user, $tribunalRequest);
    }

    /**
     * уведомить участников сделки о передаче спора в третейский суд
     *
     * @param Dispute $dispute
     * @param Tribunal $tribunal
     * @return bool
     * @throws \Exception
     */
    private function notifyDealAgentsAboutReferredToTribunal(Dispute $dispute, Tribunal $tribunal)
    {
        if ($this->email_notify_referred_to_tribunal) {
            try {
                $this->notifyCustomerAboutReferredToTribunalByEmail($dispute, $tribunal);
                $this->notifyContractorAboutReferredToTribunalByEmail($dispute, $tribunal);
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_referred_to_tribunal) {
            try {
                $this->notifyCustomerAboutReferredToTribunalBySms($dispute);
                $this->notifyContractorAboutReferredToTribunalBySms($dispute);
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param TribunalRequest $tribunalRequest
     * @return bool
     */
    private function notifyCounterAgentAboutTribunalRequest(TribunalRequest $tribunalRequest)
    {
        if ($this->email_notify_tribunal_request) {
            try {
                $this->notifyContractorAboutTribunalRequestByEmail($tribunalRequest);
                $this->notifyCustomerAboutTribunalRequestByEmail($tribunalRequest);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_tribunal_request) {
            try {
                $this->notifyCounterAgentAboutTribunalRequestBySms($tribunalRequest);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param TribunalConfirm $tribunalConfirm
     * @return bool
     */
    private function notifyCounterAgentAboutTribunalConfirm(TribunalConfirm $tribunalConfirm)
    {
        if ($this->email_notify_tribunal_confirm) {
            try {
                $this->notifyCounterAgentAboutTribunalConfirmByEmail($tribunalConfirm);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_tribunal_confirm) {
            try {
                $this->notifyCounterAgentAboutTribunalConfirmBySms($tribunalConfirm);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param TribunalRequest $tribunalRequest
     * @throws \Exception
     */
    private function notifyCustomerAboutTribunalRequestByEmail(TribunalRequest $tribunalRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $tribunalRequest->getDispute();
        $deal = $dispute->getDeal();
        $email = $deal->getCustomer()->getEmail();
        $disputeOutput = $this->disputeOutput;
        $tribunalRequestOutput = $this->getTribunalRequestOutput($tribunalRequest);
        $currentAgent = $tribunalRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'tribunalRequestOutput' => $tribunalRequestOutput,
            'customer_login' => $currentAgent->getUser()->getLogin(),
            'contractor_login' => $counterAgent->getUser()->getLogin(),
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::TRIBUNAL_REQUEST_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * @param TribunalRequest $tribunalRequest
     * @throws \Exception
     */
    private function notifyContractorAboutTribunalRequestByEmail(TribunalRequest $tribunalRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $tribunalRequest->getDispute();
        $deal = $dispute->getDeal();
        $disputeOutput = $this->disputeOutput;
        $tribunalRequestOutput = $this->getTribunalRequestOutput($tribunalRequest);
        $currentAgent = $tribunalRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'tribunalRequestOutput' => $tribunalRequestOutput,
            'customer_login' => $currentAgent->getUser()->getLogin(),
            'contractor_login' => $counterAgent->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::TRIBUNAL_REQUEST_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($counterAgent->getEmail(), $data);
    }

    /**
     * @param TribunalRequest $tribunalRequest
     * @throws \Exception
     */
    private function notifyCounterAgentAboutTribunalRequestBySms(TribunalRequest $tribunalRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $tribunalRequest->getDispute();
        $deal = $dispute->getDeal();
        $currentAgent = $tribunalRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $userPhone = $counterAgent->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getTribunalRequestForCounterAgentSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param TribunalConfirm $tribunalConfirm
     * @throws \Exception
     */
    private function notifyCounterAgentAboutTribunalConfirmByEmail(TribunalConfirm $tribunalConfirm)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var User $currentAgent
         * @var DealAgent $authorRequest
         */
        $tribunalRequest = $tribunalConfirm->getTribunalRequest();
        $deal = $tribunalRequest->getDispute()->getDeal();
        $disputeOutput = $this->disputeOutput;
        $tribunalRequestOutput = $this->getTribunalRequestOutput($tribunalRequest);

        $currentAgent = $tribunalConfirm->getAuthor();
        $authorRequest = $tribunalRequest->getAuthor();

        if ($tribunalConfirm->isStatus()) {
            $type_notify = DisputeNotificationSender::TRIBUNAL_CONFIRM_FOR_AUTHOR_REQUEST;
        } else {
            $type_notify = DisputeNotificationSender::TRIBUNAL_REFUSE_FOR_AUTHOR_REQUEST;
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'tribunalRequestOutput' => $tribunalRequestOutput,
            'agent_user_login' => $currentAgent->getLogin(),
            'counteragent_login' => $authorRequest->getUser()->getLogin(),
            DisputeNotificationSender::TYPE_NOTIFY_KEY => $type_notify,
        ];

        $this->disputeNotificationSender->sendMail($authorRequest->getEmail(), $data);
    }

    /**
     * @param TribunalConfirm $tribunalConfirm
     * @throws \Exception
     */
    private function notifyCounterAgentAboutTribunalConfirmBySms(TribunalConfirm $tribunalConfirm)
    {
        /**
         * @var Deal $deal
         * @var DealAgent $authorRequest
         * @var TribunalRequest $tribunalRequest
         */
        $tribunalRequest = $tribunalConfirm->getTribunalRequest();
        $deal = $tribunalRequest->getDispute()->getDeal();
        $authorRequest = $tribunalRequest->getAuthor();

        $userPhone = $authorRequest->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            if ($tribunalConfirm->isStatus()) {
                $smsMessage = $this->getTribunalConfirmForAuthorRequestSmsMessage($deal);
            } else {
                $smsMessage = $this->getTribunalRefuseForAuthorRequestSmsMessage($deal);
            }
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * Уведомление о передаче спора в третейский суд для кастомера
     *
     * @param Dispute $dispute
     * @param Tribunal $tribunal
     * @throws \Exception
     */
    private function notifyCustomerAboutReferredToTribunalByEmail(Dispute $dispute, Tribunal $tribunal)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getCustomer()->getEmail();
        $disputeOutput = $this->disputeOutput;
        $tribunalOutput = $this->getTribunalOutput($tribunal);

        $data = [
            'disputeOutput' => $disputeOutput,
            'tribunalOutput' => $tribunalOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::REFERRED_TO_TRIBUNAL_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * Уведомление о передаче спора в третейский суд для контрактора
     *
     * @param Dispute $dispute
     * @param Tribunal $tribunal
     * @throws \Exception
     */
    private function notifyContractorAboutReferredToTribunalByEmail(Dispute $dispute, Tribunal $tribunal)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getContractor()->getEmail();
        $disputeOutput = $this->disputeOutput;
        $tribunalOutput = $this->getTribunalOutput($tribunal);

        $data = [
            'disputeOutput' => $disputeOutput,
            'tribunalOutput' => $tribunalOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::REFERRED_TO_TRIBUNAL_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyCustomerAboutReferredToTribunalBySms(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $contractor */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getReferredToTribunalForCustomerSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyContractorAboutReferredToTribunalBySms(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getReferredToTribunalForContractorSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }
}