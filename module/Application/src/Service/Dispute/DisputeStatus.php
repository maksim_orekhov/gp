<?php

namespace Application\Service\Dispute;

use Application\Entity\Deal;
use Application\Entity\Dispute;
use Application\Service\Dispute\Status\ArbitrageRequestRejectedStatus;
use Application\Service\Dispute\Status\ClosedStatus;
use Application\Service\Dispute\Status\DiscountRequestAcceptedStatus;
use Application\Service\Dispute\Status\DisputeStatusInterface;
use Application\Service\Dispute\Status\RefundIsMadeStatus;
use Application\Service\Dispute\Status\RefundRequestAcceptedStatus;
use Application\Service\Dispute\Status\DiscountRequestStatus;
use Application\Service\Dispute\Status\RefundStatus;
use Application\Service\Dispute\Status\TribunalRequestAcceptedStatus;
use Application\Service\Dispute\Status\TribunalRequestRejectedStatus;
use Application\Service\Dispute\Status\TribunalStatus;
use Application\Service\Dispute\Status\TribunalRequestStatus;
use Application\Service\Dispute\Status\ArbitrageRequestAcceptedStatus;
use Application\Service\Dispute\Status\ArbitrageRequestStatus;
use Application\Service\Dispute\Status\RefundRequestRejectedStatus;
use Application\Service\Dispute\Status\DiscountRequestRejectedStatus;
use Application\Service\Dispute\Status\RefundRequestStatus;
use Application\Service\Dispute\Status\InitialStatus;

/**
 * Class DisputeStatus
 * @package Application\Service\Deal
 */
class DisputeStatus
{
    const DISPUTE_STATUSES = [ // Important! Не менять значения ключа 'status'
        '0'=>['code' => 1, 'status' => 'initial',                        'priority' => 1, 'is_blocker' => false, 'name' => 'Первичный'],
        '1'=>['code' => 5, 'status' => 'discount_request',               'priority' => 10, 'is_blocker' => false, 'name' => 'Предложено решение: Скидка'],
        '2'=>['code' => 10, 'status' => 'refund_request',                'priority' => 15, 'is_blocker' => false, 'name' => [
            'T' => 'Предложено решение: Возврат',
            'U' => 'Предложено решение: Отказ от услуги'
        ]],
        '3'=>['code' => 15, 'status' => 'discount_request_rejected',     'priority' => 20, 'is_blocker' => false, 'name' => 'Предложение скидки отклонено'],
        '4'=>['code' => 20, 'status' => 'refund_request_rejected',       'priority' => 25, 'is_blocker' => false, 'name' => [
            'T' => 'Предложение возврата отклонено',
            'U' => 'Предложение отказа от услуги отклонено'
        ]],
        '5'=>['code' => 25, 'status' => 'discount_request_accepted',     'priority' => 30, 'is_blocker' => false, 'name' => 'Предложение скидки принято'],
        '6'=>['code' => 30, 'status' => 'refund_request_accepted',       'priority' => 35, 'is_blocker' => false, 'name' => [
            'T' => 'Предложение возврата принято',
            'U' => 'Предложение отказа от услуги принято'
        ]],
        '7'=>['code' => 35, 'status' => 'arbitrage_request',             'priority' => 40, 'is_blocker' => false, 'name' => 'Запрос на согласительную комиссию'],
        '8'=>['code' => 40, 'status' => 'tribunal_request',              'priority' => 45, 'is_blocker' => false, 'name' => 'Запрос на суд'],
        '9'=>['code' => 45, 'status' => 'arbitrage_request_rejected',    'priority' => 50, 'is_blocker' => false, 'name' => 'Запрос на согласительную комиссию отклонен'],
        '10'=>['code' => 50, 'status' => 'tribunal_request_rejected',    'priority' => 55, 'is_blocker' => false, 'name' => 'Запрос на суд отклонен'],
        '11'=>['code' => 55, 'status' => 'arbitrage_request_accepted',   'priority' => 60, 'is_blocker' => false, 'name' => 'Запрос на согласительную комиссию принят'],
        '12'=>['code' => 30, 'status' => 'tribunal_request_accepted',    'priority' => 65, 'is_blocker' => false, 'name' => 'Запрос на суд принят'],
        '13'=>['code' => 35, 'status' => 'refund',                       'priority' => 95, 'is_blocker' => true,  'name' => 'Возврат осуществлен'],
        '14'=>['code' => 37, 'status' => 'refund_is_made',               'priority' => 100, 'is_blocker' => true, 'name' => 'Закрыт с возвратом'],
        '15'=>['code' => 40, 'status' => 'tribunal',                     'priority' => 110, 'is_blocker' => true, 'name' => 'Третейский суд'],
        '16'=>['code' => 45, 'status' => 'closed',                       'priority' => 200, 'is_blocker' => true, 'name' => 'Спор закрыт'],
        '99'=>['code' => 99, 'status' => 'undefined',                    'priority' => 404, 'is_blocker' => true, 'name' => 'Не определен']
    ];

    /**
     * @var array
     */
    private $status_objects;

    /**
     * DisputeStatus constructor.
     * @param InitialStatus $initialStatus
     * @param DiscountRequestStatus $discountRequestStatus
     * @param RefundRequestStatus $refundRequestStatus
     * @param RefundRequestAcceptedStatus $refundRequestAcceptedStatus
     * @param DiscountRequestAcceptedStatus $discountRequestAcceptedStatus
     * @param RefundRequestRejectedStatus $refundRequestRejectedStatus
     * @param DiscountRequestRejectedStatus $discountRequestRejectedStatus
     * @param RefundStatus $refundStatus
     * @param RefundIsMadeStatus $refundIsMadeStatus
     * @param ArbitrageRequestStatus $arbitrageRequestStatus
     * @param ArbitrageRequestRejectedStatus $arbitrageRequestRejectedStatus
     * @param ArbitrageRequestAcceptedStatus $arbitrageRequestAcceptedStatus
     * @param TribunalRequestStatus $tribunalRequestStatus
     * @param TribunalRequestRejectedStatus $tribunalRequestRejectedStatus
     * @param TribunalRequestAcceptedStatus $tribunalRequestAcceptedStatus
     * @param TribunalStatus $tribunalStatus
     * @param ClosedStatus $closedStatus
     */
    public function __construct(InitialStatus $initialStatus,
                                DiscountRequestStatus $discountRequestStatus,
                                RefundRequestStatus $refundRequestStatus,
                                RefundRequestAcceptedStatus $refundRequestAcceptedStatus,
                                DiscountRequestAcceptedStatus $discountRequestAcceptedStatus,
                                RefundRequestRejectedStatus $refundRequestRejectedStatus,
                                DiscountRequestRejectedStatus $discountRequestRejectedStatus,
                                RefundStatus $refundStatus,
                                RefundIsMadeStatus $refundIsMadeStatus,
                                ArbitrageRequestStatus $arbitrageRequestStatus,
                                ArbitrageRequestRejectedStatus $arbitrageRequestRejectedStatus,
                                ArbitrageRequestAcceptedStatus $arbitrageRequestAcceptedStatus,
                                TribunalRequestStatus $tribunalRequestStatus,
                                TribunalRequestRejectedStatus $tribunalRequestRejectedStatus,
                                TribunalRequestAcceptedStatus $tribunalRequestAcceptedStatus,
                                TribunalStatus $tribunalStatus,
                                ClosedStatus $closedStatus)
    {
        $this->status_objects = [
            'initial'                       => $initialStatus,
            'discount_request'              => $discountRequestStatus,
            'refund_request'                => $refundRequestStatus,
            'discount_request_rejected'     => $discountRequestRejectedStatus,
            'refund_request_rejected'       => $refundRequestRejectedStatus,
            'discount_request_accepted'     => $discountRequestAcceptedStatus,
            'refund_request_accepted'       => $refundRequestAcceptedStatus,
            'arbitrage_request'             => $arbitrageRequestStatus,
            'arbitrage_request_rejected'    => $arbitrageRequestRejectedStatus,
            'arbitrage_request_accepted'    => $arbitrageRequestAcceptedStatus,
            'tribunal_request'              => $tribunalRequestStatus,
            'tribunal_request_rejected'     => $tribunalRequestRejectedStatus,
            'tribunal_request_accepted'     => $tribunalRequestAcceptedStatus,
            'refund'                        => $refundStatus,
            'refund_is_made'                => $refundIsMadeStatus,
            'tribunal'                      => $tribunalStatus,
            'closed'                        => $closedStatus,
            'undefined'                     => null,
        ];
    }

    /**
     * @param Dispute $dispute
     * @return mixed
     */
    public function setStatus(Dispute $dispute)
    {
        /** @var array $status */
        $status = $this->getStatus($dispute);

        $dispute->status = $status;

        return $dispute;
    }

    /**
     * @param Dispute $dispute
     * @return array
     */
    public function getStatus(Dispute $dispute): array
    {
        $status = null;
        /** @var array $dispute_statuses */
        $dispute_statuses = $this->sortStatusArrayByPriority(self::DISPUTE_STATUSES);

        foreach ($dispute_statuses as $dispute_status) {

            $statusObject = $this->status_objects[$dispute_status['status']];

            if ($statusObject instanceof DisputeStatusInterface && $statusObject->isDisputeInStatus($dispute)) {

                $status = $dispute_status;

                if (true === $dispute_status['is_blocker']) {
                    // Если в блокирующем статусе name - это массив ('U', 'T'), то добавить обработку
                    return $status;
                }
            }
        }

        if (null === $status) {

            $status = self::DISPUTE_STATUSES['99'];
        }

        // Если в статусе name - это массив ('U', 'T')
        if (\is_array($status['name'])) {
            $status['name'] = $this->getStatusNameFromArray($dispute, $status);
        }

        return $status;
    }

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return self::DISPUTE_STATUSES;
    }

    /**
     * Сортировка массива стутасов по приоритету
     *
     * @param $dispute_statuses
     * @return mixed
     */
    private function sortStatusArrayByPriority($dispute_statuses)
    {
        usort($dispute_statuses, function($a, $b) {
            return ($a['priority'] < $b['priority']) ? -1 : 1;
        });

        return $dispute_statuses;
    }

    /**
     * @param Dispute $dispute
     * @param array $status
     * @return mixed
     */
    private function getStatusNameFromArray(Dispute $dispute, array $status)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();

        if (array_key_exists($deal->getDealType()->getIdent(), $status['name'])) {
            $status['name'] = $status['name'][$deal->getDealType()->getIdent()];
        }

        return $status['name'];
    }
}