<?php

namespace Application\Service\Dispute;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\Refund;
use Application\Entity\RefundConfirm;
use Application\Entity\RefundRequest;
use Application\Entity\User;
use Application\Provider\Mail\DisputeNotificationSender;
use Application\Service\Payment\CalculatedDataProvider;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\Payment\PaymentPolitics;
use Core\Exception\LogicException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleCode\Service\SmsStrategyContext;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class RefundManager
 * @package Application\Service\Dispute
 */
class RefundManager
{
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    const REFUND_REQUEST_REFUSES_LIMIT = 1;

    /**
     * notify params
     */
    private $email_notify_refund_provided;
    private $sms_notify_refund_provided;
    private $email_notify_refund_request;
    private $sms_notify_refund_request;
    private $email_notify_refund_confirm;
    private $sms_notify_refund_confirm;

    /**
     * $var DisputeManager
     */
    private $disputeManager;

    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var PayOffManager
     */
    private $payOffManager;

    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * @var DisputePolitics
     */
    private $disputePolitics;

    /**
     * @var RefundPolitics
     */
    private $refundPolitics;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var DisputeNotificationSender
     */
    private $disputeNotificationSender;

    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider;

    /**
     * $var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * @var array
     */
    private $config;

    /**
     * @var array
     */
    private $disputeOutput = [];

    /**
     * @var DisputeStatus
     */
    private $disputeStatus;

    /**
     * RefundManager constructor.
     * @param EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param PayOffManager $payOffManager
     * @param PaymentPolitics $paymentPolitics
     * @param DisputePolitics $disputePolitics
     * @param RefundPolitics $refundPolitics
     * @param RbacManager $rbacManager
     * @param DisputeNotificationSender $disputeNotificationSender
     * @param CalculatedDataProvider $calculatedDataProvider
     * @param PaymentOrderManager $paymentOrderManager
     * @param DisputeStatus $disputeStatus
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                PaymentMethodManager $paymentMethodManager,
                                PayOffManager $payOffManager,
                                PaymentPolitics $paymentPolitics,
                                DisputePolitics $disputePolitics,
                                RefundPolitics $refundPolitics,
                                RbacManager $rbacManager,
                                DisputeNotificationSender $disputeNotificationSender,
                                CalculatedDataProvider $calculatedDataProvider,
                                PaymentOrderManager $paymentOrderManager,
                                DisputeStatus $disputeStatus,
                                array $config)
    {
        $this->entityManager             = $entityManager;
        $this->userManager               = $userManager;
        $this->baseAuthManager               = $baseAuthManager;
        $this->paymentMethodManager      = $paymentMethodManager;
        $this->payOffManager             = $payOffManager;
        $this->paymentPolitics           = $paymentPolitics;
        $this->disputePolitics           = $disputePolitics;
        $this->refundPolitics            = $refundPolitics;
        $this->rbacManager               = $rbacManager;
        $this->disputeNotificationSender = $disputeNotificationSender;
        $this->calculatedDataProvider    = $calculatedDataProvider;
        $this->paymentOrderManager       = $paymentOrderManager;
        $this->disputeStatus             = $disputeStatus;
        $this->config                    = $config;

        // set notify params from config
        $this->sms_notify_refund_provided = $this->getDealNotifyConfig($config, 'refund_provided', 'sms');
        $this->email_notify_refund_provided = $this->getDealNotifyConfig($config, 'refund_provided', 'email');
        $this->sms_notify_refund_request = $this->getDealNotifyConfig($config, 'refund_request', 'sms');
        $this->email_notify_refund_request = $this->getDealNotifyConfig($config, 'refund_request', 'email');
        $this->sms_notify_refund_confirm = $this->getDealNotifyConfig($config, 'refund_confirm', 'sms');
        $this->email_notify_refund_confirm = $this->getDealNotifyConfig($config, 'refund_confirm', 'email');
    }

    /**
     * @param $id
     * @return null|object
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getRefundRequestById($id)
    {
        return $this->entityManager->find(RefundRequest::class, $id);
    }

    /**
     * @param DisputeManager $disputeManager
     */
    public function setDisputeManager(DisputeManager $disputeManager)
    {
        $this->disputeManager = $disputeManager;
    }

    /**
     * @return DisputeManager
     */
    public function getDisputeManager(): DisputeManager
    {
        return $this->disputeManager;
    }

    /**
     * @param $disputeOutput
     * @return array|null
     */
    public function setDisputeOutput($disputeOutput)
    {
        if($disputeOutput && is_array($disputeOutput)){
            $this->disputeOutput = $disputeOutput;
        }

        return $this->disputeOutput;
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function isOperator(User $user)
    {
        return $this->rbacManager->hasRole($user, 'Operator');
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isRefundDoneByDeal(Deal $deal)
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        if ($dispute) {
            $refund = $dispute->getRefund();

            return (bool)$refund;
        }

        return false;
    }

    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isRefundDone(Dispute $dispute)
    {
        return (bool)$dispute->getRefund();
    }

    /**
     * @param Deal $deal
     * @return \Application\Entity\PaymentMethodBankTransfer|null
     * @throws \Exception
     */
    public function getPaymentMethodRefundByDeal(Deal $deal)
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $deal->getCustomer()->getCivilLawSubject();
        if($civilLawSubject->getIsPattern()){
            throw new \Exception('CivilLawSubject of the paid deal can not be a pattern');
        }

        $paymentMethods = $civilLawSubject->getPaymentMethods();
        if(\count($paymentMethods) > 1){
            throw new \Exception('Customer payment method found more than one');
        }

        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $paymentMethods->first();

        return ($paymentMethod) ? $paymentMethod->getPaymentMethodBankTransfer() : null;
    }

    /**
     * @param PaymentMethodBankTransfer|null $paymentMethodRefund
     * @return array
     */
    public function getPaymentMethodRefundOutput($paymentMethodRefund)
    {
        $paymentMethodRefundOutput = [];
        if($paymentMethodRefund) {
            $paymentMethodRefundOutput = $this->paymentMethodManager->getPaymentMethodBankTransferOutput($paymentMethodRefund);
        }
        return $paymentMethodRefundOutput;
    }

    /**
     * @param Dispute $dispute
     * @param $params
     * @return Refund
     * @throws \Exception
     * @throws \Throwable
     */
    public function createRefund(Dispute $dispute, $params)
    {
        if (! isset($params['bank_transfer_id']) ) {

            throw new \Exception('Not found key "bank_transfer_id" in the form data');
        }
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        // 1. проверяем статус
        $disputeCycle = DisputeManager::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || ($disputeCycle->getClosed() !== null && !$dispute->isClosed())) {
            throw new LogicException(null, LogicException::DISPUTE_ACTIVE_CYCLE_NOT_FOUND);
        }
        // 2. получаем платежные реквизиты
        /** @var PaymentMethodBankTransfer $paymentMethodBankTransfer */
        $paymentMethodBankTransfer = $this->paymentMethodManager
            ->getPaymentMethodBankTransferById((int) $params['bank_transfer_id']);
        if ($paymentMethodBankTransfer === null) {

            throw new \Exception('PaymentMethodBankTransfer not found by id');
        }
        try {
            // DB Transaction start
            $this->entityManager->getConnection()->beginTransaction();

            $refund = new Refund();
            $refund->setCreated(new \DateTime());
            $refund->setDispute($dispute);
            $refund->setPaymentMethod($paymentMethodBankTransfer->getPaymentMethod());
            $refund->setDisputeCycle($disputeCycle);

            $this->entityManager->persist($refund);
            $dispute->setRefund($refund);
            $disputeCycle->setRefund($refund);
            $this->entityManager->persist($disputeCycle);
            $this->entityManager->persist($dispute);

            $this->entityManager->flush();

            ///// Генерация платежки на выплату возврата /////
            $this->payOffManager->createPayOff($deal, PayOffManager::PURPOSE_TYPE_REFUND);

            // DB Transaction commit
            $this->entityManager->getConnection()->commit();

        } catch (\Throwable $throwable) {
            // DB Transaction rollback
            $this->entityManager->getConnection()->rollBack();
            $this->entityManager->getConnection()->close();

            logException($throwable, 'payoff-errors', 2);

            throw $throwable;
        }

        //уведомить участников сделки о возврате средств
        $this->notifyDealAgentsAboutRefundProvided($dispute, $refund);

        return $refund;
    }

    /**
     * @param Deal $deal
     * @param DealAgent $author
     * @return RefundRequest
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function createRefundRequest(Deal $deal, DealAgent $author)
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        if (!$dispute) {

            throw new \Exception('The deal has no dispute');
        }
        $disputeCycle = DisputeManager::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || ($disputeCycle->getClosed() !== null && !$dispute->isClosed())) {
            throw new LogicException(null, LogicException::DISPUTE_ACTIVE_CYCLE_NOT_FOUND);
        }

        $refundRequest = new RefundRequest();
        $refundRequest->setCreated(new \DateTime());
        $refundRequest->setDispute($dispute);
        $refundRequest->setAuthor($author);
        $refundRequest->setDisputeCycle($disputeCycle);

        $this->entityManager->persist($refundRequest);
        $disputeCycle->addRefundRequest($refundRequest);
        $this->entityManager->persist($disputeCycle);

        $this->entityManager->flush();

        if (!$refundRequest->getId()) {

            throw new \Exception('RefundRequest not created');
        }

        //уведомить контрагента о запросе возврата
        $this->notifyCounterAgentAboutRefundRequest($refundRequest);

        return $refundRequest;
    }

    /**
     * @param RefundRequest $refundRequest
     * @param DealAgent $author
     * @param bool $status
     * @return RefundConfirm
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function createRefundConfirm(RefundRequest $refundRequest, DealAgent $author, $status = true)
    {
        $refundConfirm = new RefundConfirm();
        $refundConfirm->setCreated(new \DateTime());
        $refundConfirm->setRefundRequest($refundRequest);
        $refundConfirm->setAuthor($author);
        $refundConfirm->setStatus($status);

        $this->entityManager->persist($refundConfirm);
        $this->entityManager->flush($refundConfirm);

        if (!$refundConfirm->getId()) {

            throw new \Exception('RefundConfirm not created');
        }

        $refundRequest->setConfirm($refundConfirm);

        $this->entityManager->persist($refundRequest);
        $this->entityManager->flush($refundRequest);

        //уведомить участников о принятии решения по запросу возврата
        $this->notifyDealAgentsRequestAboutRefundConfirm($refundConfirm);

        return $refundConfirm;
    }

    /**
     * @param RefundRequest $refundRequest
     * @param DealAgent $author
     * @return RefundConfirm
     * @throws \Exception
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createRefundRefuse(RefundRequest $refundRequest, DealAgent $author)
    {
        $status = false;

        return $this->createRefundConfirm($refundRequest, $author, $status);
    }

    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function checkRefundRefusesOverlimit(Dispute $dispute)
    {
        $refundRequests = $dispute->getRefundRequests();

        $negative_confirms = [];
        /** @var RefundRequest $refundRequest */
        foreach ($refundRequests as $refundRequest) {
            /** @var RefundConfirm $confirm */
            $confirm = $refundRequest->getConfirm();
            if (!$confirm->isStatus()) {
                $negative_confirms[] = $confirm;
            }
        }

        return $this->refundPolitics->isRefundDeclineOverlimit($negative_confirms);
    }


    /**
     * Уведомление участников сделки о принятии возврата
     *
     * @param RefundConfirm $refundConfirm
     * @return bool
     */
    private function notifyDealAgentsRequestAboutRefundConfirm(RefundConfirm $refundConfirm)
    {
        if ($this->email_notify_refund_confirm) {
            try {
                $this->notifyCustomerAboutRefundConfirmByEmail($refundConfirm);
                $this->notifyContractorAboutRefundConfirmByEmail($refundConfirm);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_refund_confirm) {
            try {
                $this->notifyAuthorRequestAboutRefundConfirmBySms($refundConfirm);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * Уведомление участников сделки о предоставлении скидки
     *
     * @param Dispute $dispute
     * @param Refund $refund
     * @return bool
     * @throws \Exception
     */
    private function notifyDealAgentsAboutRefundProvided(Dispute $dispute, Refund $refund)
    {
        if ($this->email_notify_refund_provided) {
            try {
                $this->notifyCustomerAboutRefundProvidedByEmail($dispute, $refund);
                $this->notifyContractorAboutRefundProvidedByEmail($dispute, $refund);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_refund_provided) {
            try {
                $this->notifyCustomerAboutRefundProvidedBySms($dispute);
                $this->notifyContractorAboutRefundProvidedBySms($dispute);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param RefundRequest $refundRequest
     * @return bool
     */
    private function notifyCounterAgentAboutRefundRequest(RefundRequest $refundRequest)
    {
        if ($this->email_notify_refund_request) {
            try {
                $this->notifyCustomerAboutRefundRequestByEmail($refundRequest);
                $this->notifyContractorAboutRefundRequestByEmail($refundRequest);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_refund_request) {
            try {
                $this->notifyCounterAgentAboutRefundRequestBySms($refundRequest);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param RefundRequest $refundRequest
     * @throws \Exception
     */
    private function notifyCustomerAboutRefundRequestByEmail(RefundRequest $refundRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $refundRequest->getDispute();
        $deal = $dispute->getDeal();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $refundRequestOutput = $this->getRefundRequestOutput($refundRequest);
        $currentAgent = $refundRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'refundRequestOutput' => $refundRequestOutput,
            'customer_login' => $currentAgent->getUser()->getLogin(),
            'contractor_login' => $counterAgent->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::REFUND_REQUEST_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($currentAgent->getEmail(), $data);
    }

    /**
     * @param RefundRequest $refundRequest
     * @throws \Exception
     */
    private function notifyContractorAboutRefundRequestByEmail(RefundRequest $refundRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $refundRequest->getDispute();
        $deal = $dispute->getDeal();
        if (null === $this->disputeManager) {

            throw new LogicException('DisputeManager not initialized');
        }
        $disputeOutput = $this->getDisputeManager()->getDisputeOutput($dispute);
        $refundRequestOutput = $this->getRefundRequestOutput($refundRequest);
        $currentAgent = $refundRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'refundRequestOutput' => $refundRequestOutput,
            'customer_login' => $currentAgent->getUser()->getLogin(),
            'contractor_login' => $counterAgent->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::REFUND_REQUEST_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($counterAgent->getEmail(), $data);
    }

    /**
     * @param RefundRequest $refundRequest
     * @throws \Exception
     */
    private function notifyCounterAgentAboutRefundRequestBySms(RefundRequest $refundRequest)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $counterAgent
         */
        $dispute = $refundRequest->getDispute();
        $deal = $dispute->getDeal();
        $currentAgent = $refundRequest->getAuthor();

        if($currentAgent->getUser() === $deal->getCustomer()->getUser()) {
            $counterAgent = $deal->getContractor();
        } else {
            $counterAgent = $deal->getCustomer();
        }

        $userPhone = $counterAgent->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getRefundRequestForCounterAgentSmsMessage($deal, $this->getRefundAmountByDispute($dispute));
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param RefundConfirm $refundConfirm
     * @throws \Exception
     */
    private function notifyCustomerAboutRefundConfirmByEmail(RefundConfirm $refundConfirm)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $currentAgent
         * @var DealAgent $authorRequest
         */
        $refundRequest = $refundConfirm->getRefundRequest();
        $deal = $refundRequest->getDispute()->getDeal();
        $disputeOutput = $this->disputeOutput;
        $refundRequestOutput = $this->getRefundRequestOutput($refundRequest);
        $currentAgent = $refundConfirm->getAuthor();
        $authorRequest = $refundRequest->getAuthor();

        if ($refundConfirm->isStatus()) {
            $type_notify = DisputeNotificationSender::REFUND_REQUEST_ACCEPTED_FOR_CUSTOMER;
        } else {
            $type_notify = DisputeNotificationSender::REFUND_REFUSE_FOR_AUTHOR_REQUEST;
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'refundRequestOutput' => $refundRequestOutput,
            'contractor_login' => $currentAgent->getUser()->getLogin(),
            'customer_login' => $authorRequest->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => $type_notify,
        ];

        $this->disputeNotificationSender->sendMail($authorRequest->getEmail(), $data);
    }

    /**
     * @param RefundConfirm $refundConfirm
     * @throws \Exception
     */
    private function notifyContractorAboutRefundConfirmByEmail(RefundConfirm $refundConfirm)
    {
        /**
         * @var Dispute $dispute
         * @var Deal $deal
         * @var DealAgent $currentAgent
         * @var DealAgent $authorRequest
         */
        $refundRequest = $refundConfirm->getRefundRequest();
        $deal = $refundRequest->getDispute()->getDeal();
        $disputeOutput = $this->disputeOutput;
        $refundRequestOutput = $this->getRefundRequestOutput($refundRequest);
        $currentAgent = $refundConfirm->getAuthor();
        $authorRequest = $refundRequest->getAuthor();

        if ($refundConfirm->isStatus()) {
            $type_notify = DisputeNotificationSender::REFUND_REQUEST_ACCEPTED_FOR_CONTRACTOR;
        } else {
            $type_notify = DisputeNotificationSender::REFUND_REFUSE_FOR_AUTHOR_REQUEST;
        }

        $data = [
            'deal' => $deal,
            'disputeOutput' => $disputeOutput,
            'refundRequestOutput' => $refundRequestOutput,
            'contractor_login' => $currentAgent->getUser()->getLogin(),
            'customer_login' => $authorRequest->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => $type_notify,
        ];

        $this->disputeNotificationSender->sendMail($currentAgent->getEmail(), $data);
    }

    /**
     * @param RefundConfirm $refundConfirm
     * @throws \Exception
     */
    private function notifyAuthorRequestAboutRefundConfirmBySms(RefundConfirm $refundConfirm)
    {
        /**
         * @var Deal $deal
         * @var Dispute $dispute
         * @var DealAgent $authorRequest
         * @var RefundRequest $refundRequest
         */
        $refundRequest = $refundConfirm->getRefundRequest();
        $dispute = $refundRequest->getDispute();
        $deal = $dispute->getDeal();
        $authorRequest = $refundRequest->getAuthor();

        $userPhone = $authorRequest->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            if ($refundConfirm->isStatus()) {
                $smsMessage = $this->getRefundConfirmForAuthorRequestSmsMessage($deal, $this->getRefundAmountByDispute($dispute));
            } else {
                $smsMessage = $this->getRefundRefuseForAuthorRequestSmsMessage($deal, $this->getRefundAmountByDispute($dispute));
            }
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * Уведомление о возврате средств для кастомера
     *
     * @param Dispute $dispute
     * @param Refund $refund
     * @throws \Exception
     */
    private function notifyCustomerAboutRefundProvidedByEmail(Dispute $dispute, Refund $refund)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getCustomer()->getEmail();
        $disputeOutput = $this->disputeOutput;
        $refundOutput = $this->getRefundOutput($refund);

        $data = [
            'disputeOutput' => $disputeOutput,
            'refundOutput' => $refundOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::REFUND_PROVIDED_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * Уведомление о возврате средств для контрактора
     *
     * @param Dispute $dispute
     * @param Refund $refund
     * @throws \Exception
     */
    private function notifyContractorAboutRefundProvidedByEmail(Dispute $dispute, Refund $refund)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getContractor()->getEmail();
        $disputeOutput = $this->disputeOutput;
        $refundOutput = $this->getRefundOutput($refund);

        $data = [
            'disputeOutput' => $disputeOutput,
            'refundOutput' => $refundOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::REFUND_PROVIDED_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * @param Dispute $dispute
     * @return float|int
     * @throws \Exception
     */
    public function getRefundAmountByDispute(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        try {
            $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);
            $refundAmount = $this->calculatedDataProvider->getCalculatedRefundAmount($payment, $total_incoming_amount);

            return $refundAmount;
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * 1 Operator
     * 2 Что сделка находится в статусе не ниже "Оплачено"
     * 3 Если разрешено политикой
     *
     * @param User $user
     * @param Dispute $dispute
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionRefund(User $user, Dispute $dispute): bool
    {
        /**
         * @var Deal $deal
         * @var Payment $payment
         */
        $deal = $dispute->getDeal();
        $this->disputeStatus->setStatus($dispute);
        $payment = $deal->getPayment();
        $is_operator = $this->rbacManager->hasRole($user, 'Operator');

        $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);

        $is_deal_paid = $this->paymentPolitics->isDealPaid($total_incoming_amount, $payment->getExpectedValue());
        $is_allowed_add_refund = $dispute ? $this->disputePolitics->isAllowedAddRefund($dispute) : false;

        return $is_operator && $is_deal_paid && $is_allowed_add_refund;
    }

    /**
     * 1 Участник сделки в роли customer
     * 2 Что сделка находится в статусе не ниже "Оплачено"
     * 3 Если разрешено политикой
     *
     * @param User $user
     * @param Deal $deal
     * @return bool
     * @throws \Exception
     */
    public function checkPermissionRefundRequest(User $user, Deal $deal): bool
    {
        /**
         * @var Dispute $dispute
         * @var Payment $payment
         */
        $dispute = $deal->getDispute();
        $payment = $deal->getPayment();
        $this->disputeStatus->setStatus($dispute);
        $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);
        $is_operator = $this->rbacManager->hasRole($user, 'Operator');
        $is_contractor = $deal->getContractor()->getUser() === $user;

        $is_deal_paid = $this->paymentPolitics->isDealPaid($total_incoming_amount, $payment->getExpectedValue());
        $is_allowed_add_refund = $dispute ? $this->disputePolitics->isAllowedAddRefund($dispute) : false;

        return (
            $this->disputePolitics->isAllowedAddRefundRequest($dispute)
            && $is_deal_paid
            && $is_allowed_add_refund
            && !$is_operator
            && !$is_contractor
        );
    }

    /**
     * @param User $user
     * @param RefundRequest $refundRequest
     * @return bool
     */
    public function checkPermissionRefundConfirm(User $user, RefundRequest $refundRequest)
    {
        /** @var Deal $deal */
        $deal = $refundRequest->getDispute()->getDeal();
        $dispute = $deal->getDispute();
        $this->disputeStatus->setStatus($dispute);

        $is_user_part_of_dispute = DisputeManager::isUserPartOfDispute($refundRequest->getDispute(), $user);

        return $this->disputePolitics->isAllowedAddRefundConfirm($dispute, $user, $is_user_part_of_dispute);
    }

    /**
     * @param User $user
     * @param RefundRequest $refundRequest
     * @return bool
     */
    public function checkPermissionRefundRefuse(User $user, RefundRequest $refundRequest)
    {
        return $this->checkPermissionRefundConfirm($user, $refundRequest);
    }

    /**
     * @param User $user
     * @param RefundRequest $refundRequest
     * @return bool
     */
    public function checkPermissionRefundSingle(User $user, RefundRequest $refundRequest)
    {
        return $this->checkPermissionRefundConfirm($user, $refundRequest);
    }

    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getAllowedRefundRequestsByUser(User $user)
    {
        $allowedRefundRequests = $this->entityManager
            ->getRepository(RefundRequest::class)->findAllowedRefundRequestsByUser($user);

        return $allowedRefundRequests;
    }

    /**
     * @param Refund $refund
     * @return array
     * @throws \Exception
     */
    public function getRefundOutput(Refund $refund)
    {
        $refundOutput = [];

        $refundOutput['id'] = $refund->getId();
        $refundOutput['created'] = $refund->getCreated()->format('d.m.Y H.i');
        $refundOutput['amount'] = $this->getRefundAmountByDispute($refund->getDispute());

        return $refundOutput;
    }

    /**
     * @param RefundRequest $refundRequest
     * @return array
     * @throws \Exception
     */
    public function getRefundRequestOutput(RefundRequest $refundRequest): array
    {
        try {
            /** @var User $user */
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        } catch (\Throwable $t){
            $user = null;
        }

        /** @var Dispute $dispute */
        $dispute = $refundRequest->getDispute();

        $refundRequestOutput = [];

        $refundRequestOutput['id'] = $refundRequest->getId();
        /** @var User $authorUser */
        $authorUser = $refundRequest->getAuthor()->getUser();
        $refundRequestOutput['created'] = $refundRequest->getCreated()->format('d.m.Y H:i');
        $refundRequestOutput['amount'] = $this->getRefundAmountByDispute($dispute);
        $refundRequestOutput['is_author'] = $user === $authorUser;
        $refundRequestOutput['author'] = [
            'name' => $authorUser->getLogin(),
            'email' => $authorUser->getEmail()->getEmail(),
            'phone' => '+'.$authorUser->getPhone()->getPhoneNumber(),
        ];
        $refundRequestOutput['confirm'] = null;
        if ($refundRequest->getConfirm()) {
            /** @var RefundConfirm $refundConfirm */
            $refundConfirm = $refundRequest->getConfirm();
            $refundRequestOutput['confirm'] = [
                'id' => $refundConfirm->getId(),
                'status' => $refundConfirm->isStatus(),
                'author' => [
                    'name' => $refundConfirm->getAuthor()->getUser()->getLogin(),
                    'email' => $refundConfirm->getAuthor()->getUser()->getEmail()->getEmail(),
                    'phone' => '+'.$refundConfirm->getAuthor()->getUser()->getPhone()->getPhoneNumber(),
                ],
                'created' => $refundConfirm->getCreated()->format('d.m.Y H:i'),
            ];
        }
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $refundRequestOutput['title'] = $deal->getDealType()->getIdent() === 'U' ? 'Отказ от услуги' : 'Возврат товара';

        return $refundRequestOutput;
    }

    /**
     * @param $refundRequests
     * @return array
     * @throws \Exception
     */
    public function getRefundRequestCollectionForOutput($refundRequests)
    {
        $refundRequestsOutput = [];
        /** @var ArrayCollection $refundRequests */
        foreach ($refundRequests as $refundRequest) {
            $refundRequestsOutput[] = $this->getRefundRequestOutput($refundRequest);
        }

        return $refundRequestsOutput;
    }


    /**
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyCustomerAboutRefundProvidedBySms(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $contractor */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getRefundProvidedForCustomerSmsMessage($deal, $this->getRefundAmountByDispute($dispute));
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {

            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyContractorAboutRefundProvidedBySms(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getRefundProvidedForContractorSmsMessage($deal, $this->getRefundAmountByDispute($dispute));
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }
}