<?php

namespace Application\Service\Dispute;

use Application\Controller\DisputeController;
use Application\Entity\CivilLawSubject;
use Application\Entity\DisputeCycle;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\WarrantyExtension;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\Dispute;
use Application\Entity\User;
use Application\Provider\Mail\DisputeNotificationSender;
use Application\Service\Deal\DealDeadlineManager;
use Application\Service\Deal\DealStatus;
use Application\Service\Payment\PaymentMethodManager;
use Core\Exception\LogicException;
use Doctrine\Common\Collections\ArrayCollection;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModuleCode\Service\SmsStrategyContext;
use ModuleFileManager\Entity\File;
use ModuleFileManager\Service\FileManager;
use Application\Form\DisputeForm;
use ModuleRbac\Service\RbacManager;


/**
 * Class DisputeManager
 * @package Application\Service\Dispute
 */
class DisputeManager
{
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    /**
     * notify params
     */
    private $email_notify_open_dispute;
    private $sms_notify_open_dispute;
    private $email_notify_customer_closed_dispute;
    private $sms_notify_customer_closed_dispute;
    private $sms_notify_closed_dispute_with_extension;
    private $email_notify_closed_dispute_with_extension;
    private $sms_notify_closed_dispute_without_extension;
    private $email_notify_closed_dispute_without_extension;

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var WarrantyExtensionManager
     */
    private $warrantyExtensionManager;

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var ArbitrageManager
     */
    private $arbitrageManager;

    /**
     * @var RefundManager
     */
    private $refundManager;

    /**
     * @var DiscountManager
     */
    private $discountManager;

    /**
     * @var TribunalManager
     */
    private $tribunalManager;

    /**
     * @var DisputePolitics
     */
    private $disputePolitics;

    /**
     * @var DisputeNotificationSender
     */
    private $disputeNotificationSender;

    /**
     * @var DealStatus
     */
    private $dealStatus;

    /**
     * @var DisputeStatus
     */
    private $disputeStatus;

    /**
     * @var array
     */
    private $config;

    /**
     * DisputeManager constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param RbacManager $rbacManager
     * @param WarrantyExtensionManager $warrantyExtensionManager
     * @param FileManager $fileManager
     * @param PaymentMethodManager $paymentMethodManager
     * @param ArbitrageManager $arbitrageManager
     * @param RefundManager $refundManager
     * @param DiscountManager $discountManager
     * @param TribunalManager $tribunalManager
     * @param DisputePolitics $disputePolitics
     * @param DisputeNotificationSender $disputeNotificationSender
     * @param DealStatus $dealStatus
     * @param DisputeStatus $disputeStatus
     * @param array $config
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                RbacManager $rbacManager,
                                WarrantyExtensionManager $warrantyExtensionManager,
                                FileManager $fileManager,
                                PaymentMethodManager $paymentMethodManager,
                                ArbitrageManager $arbitrageManager,
                                RefundManager $refundManager,
                                DiscountManager $discountManager,
                                TribunalManager $tribunalManager,
                                DisputePolitics $disputePolitics,
                                DisputeNotificationSender $disputeNotificationSender,
                                DealStatus $dealStatus,
                                DisputeStatus $disputeStatus,
                                array $config)
    {
        $this->entityManager             = $entityManager;
        $this->userManager               = $userManager;
        $this->baseAuthManager               = $baseAuthManager;
        $this->rbacManager               = $rbacManager;
        $this->warrantyExtensionManager  = $warrantyExtensionManager;
        $this->fileManager               = $fileManager;
        $this->paymentMethodManager      = $paymentMethodManager;
        $this->arbitrageManager          = $arbitrageManager;
        $this->refundManager             = $refundManager;
        $this->discountManager           = $discountManager;
        $this->tribunalManager           = $tribunalManager;
        $this->disputePolitics           = $disputePolitics;
        $this->disputeNotificationSender = $disputeNotificationSender;
        $this->dealStatus                = $dealStatus;
        $this->disputeStatus             = $disputeStatus;
        $this->config                    = $config;

        // set notify params from config
        $this->sms_notify_open_dispute = $this->getDealNotifyConfig($this->config, 'open_dispute', 'sms');
        $this->email_notify_open_dispute = $this->getDealNotifyConfig($this->config, 'open_dispute', 'email');
        $this->sms_notify_customer_closed_dispute = $this->getDealNotifyConfig($this->config, 'customer_closed_dispute', 'sms');
        $this->email_notify_customer_closed_dispute = $this->getDealNotifyConfig($this->config, 'customer_closed_dispute', 'email');
        $this->sms_notify_closed_dispute_with_extension = $this->getDealNotifyConfig($this->config, 'closed_dispute_with_extension', 'sms');
        $this->email_notify_closed_dispute_with_extension = $this->getDealNotifyConfig($this->config, 'closed_dispute_with_extension', 'email');
        $this->sms_notify_closed_dispute_without_extension = $this->getDealNotifyConfig($this->config, 'closed_dispute_without_extension', 'sms');
        $this->email_notify_closed_dispute_without_extension = $this->getDealNotifyConfig($this->config, 'closed_dispute_without_extension', 'email');
    }

    /**
     * @param array $params
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getDisputeCollectionByUser(array $params)
    {
        // Get all disputes with sort and search params
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->getSortedDisputesForUser($params);

        return $dispute;
    }

    /**
     * @param $params
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getOpenDisputeCollection($params)
    {
        // Get all open disputes with sort and search params
        $disputes = $this->entityManager->getRepository(Dispute::class)
            ->getSortedOpenDisputes($params);

//        if (array_key_exists('order', $params) && array_key_exists('deadline_date', $params['order'])) {
//            // сортируем по дате окончания
//            usort($disputes, function (Dispute $a, Dispute $b) use ($params) {
//                /** @var Deal a_deal */
//                $a_deal = $a->getDeal();
//                /** @var Payment a_payment */
//                $a_payment = $a_deal->getPayment();
//                /** @var \DateTime $dateClosedDeal */
//                $a_dateClosedDeal = DealDeadlineManager::getDealDeadlineDate($a_payment);
//
//                /** @var Deal b_deal */
//                $b_deal = $b->getDeal();
//                /** @var Payment b_payment */
//                $b_payment = $b_deal->getPayment();
//                /** @var \DateTime $dateClosedDeal */
//                $b_dateClosedDeal = DealDeadlineManager::getDealDeadlineDate($b_payment);
//
//                if ($params['order']['deadline_date'] === 'DESC' ) {
//                    return strtotime($b_dateClosedDeal->format('d-m-Y H:i:s')) - strtotime($a_dateClosedDeal->format('d-m-Y H:i:s'));
//                } else {
//                    return strtotime($a_dateClosedDeal->format('d-m-Y H:i:s')) - strtotime($b_dateClosedDeal->format('d-m-Y H:i:s'));
//                }
//            });
//        }

        #sql_dump($disputes);

        return $disputes;
    }


    /**
     * @param $disputes
     * @return array
     * @throws \Exception
     */
    public function getDisputeCollectionForOutput($disputes)
    {
        $disputesOutput = [];

        foreach ($disputes as $dispute) {
            $disputesOutput[] = $this->getDisputeOutput($dispute);
        }

        return $disputesOutput;
    }

    /**
     * @param $id
     * @return Dispute
     * @throws \Exception
     */
    public function getDispute($id)
    {
        /** @var Dispute $dispute */ // Get Dispute by Id
        $dispute = $this->entityManager->find(Dispute::class, $id);

        if (!$dispute || !($dispute instanceof Dispute)) {
            // If no deal found
            throw new \Exception('Dispute with such id not found');
        }

        return $dispute;
    }

    /**
     * @param Dispute $dispute
     * @return array
     * @throws \Exception
     */
    public function getDisputeOutput(Dispute $dispute)
    {
        try {
            /** @var User $user */
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            $is_operator = $this->rbacManager->hasRole($user, 'Operator');
        } catch (\Throwable $t){
            $is_operator = false;
        }

        $disputeCycle = self::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || ($disputeCycle->getClosed() !== null && !$dispute->isClosed())) {
            throw new LogicException(null, LogicException::DISPUTE_ACTIVE_CYCLE_NOT_FOUND);
        }

        $this->disputeStatus->setStatus($dispute);

        $disputeOutput = [];

        $disputeOutput['id'] = $dispute->getId();
        $disputeOutput['status'] = $this->disputeStatus->getStatus($dispute);
        $disputeOutput['reason'] = $dispute->getReason();
        $disputeOutput['created'] = $dispute->getCreated()->format('d.m.Y');
        $disputeOutput['created_milliseconds'] = $dispute->getCreated()->getTimestamp();
        $disputeOutput['created_full'] = [
            'date' => $dispute->getCreated()->format('d.m.Y'),
            'time' => $dispute->getCreated()->format('H:i:s'),
        ];
        /** @var User $disputeAuthor */
        $disputeAuthor = $dispute->getAuthor()->getUser();
        $disputeOutput['author'] = $disputeAuthor->getLogin();
        /** @var \DateTime $expectedCloseDate */
        $expectedCloseDate = self::getDisputeExpectedCloseDate($disputeCycle);
        $disputeOutput['expected_close_date'] = $expectedCloseDate->format('d.m.Y');
        // Количество дней до расчетного закрытия спора
        $disputeOutput['expected_days_for_close'] = self::countDifferenceWithCurrentDataInDays($expectedCloseDate);
        $disputeOutput['warranty_extension_days'] = self::getLastWarrantyExtensionDays($dispute->getWarrantyExtensions());

        $disputeOutput['is_close'] = $dispute->isClosed();
        $disputeOutput['discount'] = null;
        if ($dispute->getDiscount()){
            $disputeOutput['discount'] = $this->discountManager->getDiscountOutput($dispute->getDiscount());
        }
        $disputeOutput['refund'] = null;
        if ($dispute->getRefund()){
            $disputeOutput['refund'] = $this->refundManager->getRefundOutput($dispute->getRefund());
        }
        $disputeOutput['tribunal'] = null;
        if ($dispute->getTribunal()){
            $disputeOutput['tribunal'] = $this->tribunalManager->getTribunalOutput($dispute->getTribunal());
        }
        $disputeOutput['warranty_extensions'] = $this->warrantyExtensionManager
            ->extractWarrantyExtensionCollection($dispute->getWarrantyExtensions());

        if (empty($disputeOutput['warranty_extensions'])){
            $disputeOutput['warranty_extensions'] = null;
        }

        $disputeOutput['files'] = [];
        /** @var File $file */
        foreach ($dispute->getDisputeFiles() as $file) {
            $disputeOutput['files'][] = [
                'id' => $file->getId(),
                'name' => $file->getName(),
                'origin_name' => $file->getOriginName(),
                'type' => $file->getType(),
                'path' => $file->getPath(),
                'size' => $file->getSize()
            ];
        }

        $disputeOutput['arbitrage_requests'] = null;
        if ($dispute->getArbitrageRequests() ) {
            $disputeOutput['arbitrage_requests'] = $this->arbitrageManager->getArbitrageRequestCollectionForOutput($dispute->getArbitrageRequests());
            if(empty($disputeOutput['arbitrage_requests'])){
                $disputeOutput['arbitrage_requests'] = null;
            }
        }
        $disputeOutput['tribunal_requests'] = null;
        if ($dispute->getTribunalRequests() ) {
            $disputeOutput['tribunal_requests'] = $this->tribunalManager->getTribunalRequestCollectionForOutput($dispute->getTribunalRequests());
            if(empty($disputeOutput['tribunal_requests'])){
                $disputeOutput['tribunal_requests'] = null;
            }
        }
        $disputeOutput['discount_requests'] = null;
        if ($dispute->getDiscountRequests() ) {
            $disputeOutput['discount_requests'] = $this->discountManager->getDiscountRequestCollectionForOutput($dispute->getDiscountRequests());
            if(empty($disputeOutput['discount_requests'])){
                $disputeOutput['discount_requests'] = null;
            }
        }
        $disputeOutput['refund_requests'] = null;
        if ($dispute->getRefundRequests() ) {
            $disputeOutput['refund_requests'] = $this->refundManager->getRefundRequestCollectionForOutput($dispute->getRefundRequests());
            if(empty($disputeOutput['refund_requests'])){
                $disputeOutput['refund_requests'] = null;
            }
        }
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $is_contractor = $deal->getContractor()->getUser() === $user;
        $is_user_part_of_dispute = self::isUserPartOfDispute($dispute, $user);

        $disputeOutput['is_allowed_arbitrage_request'] = $this->disputePolitics->isAllowedAddArbitrageRequest($dispute) && !$is_operator && !$is_contractor;
        $disputeOutput['is_allowed_tribunal_request'] = $this->disputePolitics->isAllowedAddTribunalRequest($dispute) && !$is_operator && !$is_contractor;
        $disputeOutput['is_allowed_discount_request'] = $this->disputePolitics->isAllowedAddDiscountRequest($dispute) && !$is_operator && !$is_contractor;
        $disputeOutput['is_allowed_refund_request'] = $this->disputePolitics->isAllowedAddRefundRequest($dispute) && !$is_operator && !$is_contractor;

        $disputeOutput['is_allowed_arbitrage_confirm'] = $this->disputePolitics->isAllowedAddArbitrageConfirm($dispute, $user, $is_operator);
        $disputeOutput['is_allowed_tribunal_confirm'] = $this->disputePolitics->isAllowedAddTribunalConfirm($dispute, $user, $is_operator);
        $disputeOutput['is_allowed_discount_confirm'] = $this->disputePolitics->isAllowedAddDiscountConfirm($dispute, $user, $is_user_part_of_dispute);
        $disputeOutput['is_allowed_refund_confirm'] = $this->disputePolitics->isAllowedAddRefundConfirm($dispute, $user, $is_user_part_of_dispute);

        $disputeOutput['is_allow_close_dispute'] = !$this->isLeastOneConfirmedRequest($dispute);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $disputeOutput['deal_id'] = $deal->getId();
        $disputeOutput['deal_name'] = $deal->getName();
        $disputeOutput['deal_number'] = $deal->getNumber();
        $disputeOutput['deal_status'] = $this->dealStatus->getStatus($deal);
        $disputeOutput['deal_type_ident'] = $deal->getDealType()->getIdent();

        /** @var \DateTime $deFactoDate */
        $deFactoDate = clone $payment->getDeFactoDate();
        /** @var \DateTime $dateOfDelivery */
        $dateOfDelivery = $deFactoDate->modify('+'.$deal->getDeliveryPeriod().' days');

        $disputeOutput['deal_closed_date_by_contract'] = $dateOfDelivery->format('d.m.Y');

        $dealDeadline = DealDeadlineManager::getDealDeadlineDate($payment);

        if ($dateOfDelivery < $dealDeadline) {
            $disputeOutput['deal_closed_date_by_warranty'] = $dealDeadline->format('d.m.Y');
        } else {
            $disputeOutput['deal_closed_date_by_warranty'] = null;
        }

        $disputeOutput['max_discount_amount'] = $this->discountManager->getMaxDiscount($dispute->getDeal());

        return $disputeOutput;
    }

    /**
     * @param Dispute $dispute
     * @return bool
     */
    public function isLeastOneConfirmedRequest(Dispute $dispute)
    {
        $arbitrageRequests = $dispute->getArbitrageRequests();
        $tribunalRequests = $dispute->getTribunalRequests();
        $discountRequests = $dispute->getDiscountRequests();
        $refundRequests = $dispute->getRefundRequests();

        return DisputePolitics::isPositiveConfirmInRequests($discountRequests)
            || DisputePolitics::isPositiveConfirmInRequests($refundRequests)
            || DisputePolitics::isPositiveConfirmInRequests($arbitrageRequests)
            || DisputePolitics::isPositiveConfirmInRequests($tribunalRequests);
    }

    /**
     * Создание нового спора
     *
     * @param DealAgent $author
     * @param array $data
     * @param Deal $deal
     * @return Dispute
     * @throws \Exception
     */
    public function addDispute(DealAgent $author, array $data, Deal $deal)
    {
        try {
            /** @var Dispute $dispute */
            $dispute = $this->createDispute($data['reason'], $author, $deal);
            //уведомить участников сделки об открытом споре
            $this->notifyDealAgentsAboutOpenDispute($dispute);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $dispute;
    }

    /**
     * @param DisputeForm $form
     * @param Dispute $dispute
     * @return DisputeForm
     */
    public function setDisputeFormData(DisputeForm $form, Dispute $dispute)
    {
        $form->setData([
            'text'    => $dispute->getReason(),
            'deal_id' => $dispute->getDeal()->getId(),
        ]);

        return $form;
    }

    /**
     * @param Dispute $dispute
     * @return DisputeCycle|null
     */
    public static function getActiveDisputeCycle(Dispute $dispute)
    {
        /**
         * @var ArrayCollection $disputeCycles
         * @var DisputeCycle $disputeCycle
         */
        $last_dispute_cycle_date = null;
        $activeDisputeCycle = null;

        $disputeCycles = $dispute->getDisputeCycles();
        foreach ($disputeCycles as $disputeCycle) {
            if ($disputeCycle->getCreated() >= $last_dispute_cycle_date) {
                $last_dispute_cycle_date = $disputeCycle->getCreated();
                $activeDisputeCycle = $disputeCycle;
            }
        }

        return $activeDisputeCycle;
    }

    /**
     * @param array $data
     * @param Dispute $dispute
     * @param bool $isOperator
     * @return Dispute
     * @throws \Exception
     */
    public function closeDispute(array $data, Dispute $dispute, bool $isOperator)
    {
        $number_of_extension_days = 0;
        $disputeCycle = self::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || ($disputeCycle->getClosed() !== null && !$dispute->isClosed())) {
            throw new LogicException(null, LogicException::DISPUTE_ACTIVE_CYCLE_NOT_FOUND);
        }

        // Если сделку закрывает Оператор, то создает тикет WarrantyExtension
        if ($isOperator) {
            try {
                if (!$this->disputePolitics->isAllowedAddWarrantyExtension($dispute)) {

                    throw new \Exception('The Dispute can not be closed by adding Warranty Extension');
                }
                // Количество дней продления гарантии по сделке
                $number_of_extension_days = $this->getNumberOfExtensionDays($data, $dispute->getCreated());

                /** @var WarrantyExtension $warrantyExtension */ // Can throw Exception
                $warrantyExtension = $this->warrantyExtensionManager
                    ->addWarrantyExtension($dispute, (int)$number_of_extension_days);
            }
            catch (\Throwable $t){

                throw new \Exception($t->getMessage());
            }
        } else {
            //нельзя закрыть сделку покупателю если есть подтвержденный запрос
            if ($this->isLeastOneConfirmedRequest($dispute)) {

                throw new \Exception('Forbidden close the dispute with confirmed request');
            }
        }

        $dispute->setClosed(true);
        $this->entityManager->persist($dispute);
        //closed cycle
        $disputeCycle->setClosed(new \DateTime());
        $this->entityManager->persist($disputeCycle);
        // Save to DB
        $this->entityManager->flush();

        if (!$dispute->isClosed()) {
            // Write log
            logMessage(
                'Dispute closing failed (id - ' . $dispute->getId() . ')',
                DisputeController::LOG_FILE_NAME
            );

            throw new \Exception('Dispute closing failed');
        }

        //уведомить участников сделки о закрытии спора
        $this->notifyDealAgentsAboutClosedDispute($dispute, $isOperator, $number_of_extension_days);

        return $dispute;
    }

    /**
     * Открытие старого спора
     *
     * @param Dispute $dispute
     * @return Dispute
     * @throws \Exception
     */
    public function openDispute(Dispute $dispute)
    {
        if (!$dispute->isClosed()) {
            throw new \Exception('The Dispute is already open');
        }
        $disputeCycle = self::getActiveDisputeCycle($dispute);
        if ( !$disputeCycle || $disputeCycle->getClosed() === null) {
            throw new LogicException(null, LogicException::DISPUTE_PREVIOUS_CYCLE_INCORRECT);
        }

        $dispute->setClosed(false);

        $disputeCycle = new DisputeCycle();
        $disputeCycle->setCreated(new \DateTime());
        $disputeCycle->setDispute($dispute);

        $this->entityManager->persist($disputeCycle);
        $dispute->addDisputeCycle($disputeCycle);
        $this->entityManager->persist($dispute);

        $this->entityManager->flush();

        if ($dispute->isClosed()) {
            // Write log
            logMessage(
                'Dispute reopen failed (Dispute id - ' . $dispute->getId() . ')',
                DisputeController::LOG_FILE_NAME
            );

            throw new \Exception('Dispute reopen failed');
        }

        //уведомить участников сделки об открытии спора
        $this->notifyDealAgentsAboutOpenDispute($dispute);

        return $dispute;
    }

    /**
     * @param Dispute $dispute
     * @param array $file_data
     * @return File
     * @throws \Exception
     */
    public function saveDisputeFile(Dispute $dispute, array $file_data)
    {
        // Set of data for pass to fileManager
        $file_source = $file_data['tmp_name'];
        $file_name = $file_data['name'];
        $file_type = $file_data['type'];
        $file_size = $file_data['size'];

        try {
            $params = [
                'file_source' => $file_source,
                'file_name' => $file_name,
                'file_type' => $file_type,
                'file_size' => $file_size,
                'path' => DisputeController::DISPUTE_FILES_STORING_FOLDER
            ];

            $file = $this->fileManager->write($params);

            // Add $file to collection
            $dispute->addDisputeFile($file);

            $this->entityManager->flush();

            return $file;
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param Dispute $dispute
     * @param int $file_id
     * @return mixed
     * @throws \Exception
     */
    public function isFileBelongsToDispute(Dispute $dispute, int $file_id)
    {
        try {
            /** @var File $file */ // Can throw Exception
            $file = $this->fileManager->getFileById($file_id);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $dispute->getDisputeFiles()->contains($file);
    }

    /**
     * @param array $data
     * @param \DateTime $date
     * @return int|mixed
     */
    public function getNumberOfExtensionDays(array $data, \DateTime $date)
    {
        if (isset($data['days'])) {

            return $data['days'];
        }

        // Если нет даты, то кол-во дней - это разница между текущей датой и началом спора
        $days = self::countDifferenceWithCurrentDataInDays($date);

        return $days < 1 ? 1 : $days;
    }

    /**
     * Количество дней продления по WarrantyExtension
     *
     * @param Dispute $dispute
     * @return int
     *
     * Количество дней продления берется из последнего тикета.
     */
    public function getExtensionDaysByWarrantyExtensions(Dispute $dispute)
    {
        /** @var WarrantyExtension $warrantyExtension */
        $warrantyExtension = $this->entityManager->getRepository(WarrantyExtension::class)
            ->findOneBy(array('dispute' => $dispute), array('id' => 'DESC'));

        if (!$warrantyExtension || !($warrantyExtension instanceof WarrantyExtension)) {

            return 0;
        }

        return $warrantyExtension->getDays();
    }

    /**
     * @param $warrantyExtensions
     * @return WarrantyExtension|null
     */
    public static function getLastWarrantyExtension($warrantyExtensions)
    {
        $last_warranty_extension_date = null;
        $last_warranty_extension = null;
        /** @var WarrantyExtension $warrantyExtension */
        /** @var ArrayCollection $warrantyExtensions */
        foreach ($warrantyExtensions as $warrantyExtension) {
            if ($warrantyExtension->getCreated() >= $last_warranty_extension_date) {
                $last_warranty_extension_date = $warrantyExtension->getCreated();
                $last_warranty_extension = $warrantyExtension;
            }
        }

        return $last_warranty_extension;
    }

    /**
     * @param $warrantyExtensions
     * @return int
     */
    public static function getLastWarrantyExtensionDays($warrantyExtensions)
    {
        $warrantyExtensions = $warrantyExtensions ?? [];
        $lastWarrantyExtension = self::getLastWarrantyExtension($warrantyExtensions);

        return $lastWarrantyExtension ? $lastWarrantyExtension->getDays() : 0;
    }

    /**
     * Общее количество дней продления
     *
     * @param DisputeCycle $disputeCycle
     * @return int
     */
    public static function getExpectedExtensionDays(DisputeCycle $disputeCycle)
    {
        return DisputeController::OPEN_DISPUTE_LIFE_TIME + self::getLastWarrantyExtensionDays($disputeCycle->getWarrantyExtensions());
    }

    /**
     * @param DisputeCycle $disputeCycle
     * @return \DateTime
     */
    public static function getDisputeExpectedCloseDate(DisputeCycle $disputeCycle)
    {
        $extension_days = self::getExpectedExtensionDays($disputeCycle);
        /** @var \DateTime $createdDate */
        $createdDate = clone $disputeCycle->getCreated();

        return $createdDate->modify('+'.$extension_days.' days');
    }

    /**
     * @param string $reason
     * @param DealAgent $author
     * @param Deal $deal
     * @return Dispute
     * @throws \Exception
     */
    private function createDispute(string $reason, DealAgent $author, Deal $deal)
    {
        $currentDate = new \DateTime();
        /** @var Dispute $dispute */
        $dispute = new Dispute();

        $dispute->setReason($reason);
        $dispute->setAuthor($author);
        $dispute->setDeal($deal);
        $dispute->setCreated($currentDate);
        $dispute->setClosed(false);

        $this->entityManager->persist($dispute);

        $disputeCycle = new DisputeCycle();
        $disputeCycle->setCreated($currentDate);
        $disputeCycle->setDispute($dispute);

        $this->entityManager->persist($disputeCycle);
        $dispute->addDisputeCycle($disputeCycle);
        $this->entityManager->persist($dispute);

        // Save to DB
        $this->entityManager->flush();

        if (!$dispute->getId()) {
            // Write log
            logMessage(
                'New Dispute creation failed (Deal id - ' . $deal->getId() . ')',
                DisputeController::LOG_FILE_NAME
            );

            throw new \Exception('New Dispute creation failed');
        }

        return $dispute;
    }

    /**
     * Возвращает разницу между текущей датой и $date (в днях)
     *
     * @param \DateTime $date
     * @return mixed
     */
    public static function countDifferenceWithCurrentDataInDays(\DateTime $date)
    {
        $interval = date_diff(new \DateTime(), $date);

        return $interval->invert ? 0: $interval->days + 1;
    }

    /**
     * @param Dispute $dispute
     * @return \Application\Entity\PaymentMethodBankTransfer|null
     * @throws \Exception
     */
    public function getPaymentMethodOnDispute(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $deal->getCustomer()->getCivilLawSubject();

        if($civilLawSubject->getIsPattern()){
            throw new \Exception('CivilLawSubject of the paid deal can not be a pattern');
        }

        $paymentMethods = $civilLawSubject->getPaymentMethods();
        if(count($paymentMethods) > 1){throw new \Exception('Customer payment method found more than one');}

        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $paymentMethods->first();

        return ($paymentMethod) ? $paymentMethod->getPaymentMethodBankTransfer() : null;
    }

    /**
     * @param PaymentMethodBankTransfer|null $paymentMethodDispute
     * @return array
     */
    public function getPaymentMethodDisputeOutput($paymentMethodDispute)
    {
        $paymentMethodDisputeOutput = [];
        if($paymentMethodDispute) {
            $paymentMethodDisputeOutput = $this->paymentMethodManager->getPaymentMethodBankTransferOutput($paymentMethodDispute);
        }
        return $paymentMethodDisputeOutput;
    }

    /**
     * @param Dispute $dispute
     * @return bool
     * @throws \Exception
     */
    public function notifyDealAgentsAboutOpenDispute(Dispute $dispute)
    {
        //for the existing user
        if($this->email_notify_open_dispute){
            try {
                $this->notifyCustomerAboutOpenDisputeByEmail($dispute);
                $this->notifyContractorAboutOpenDisputeByEmail($dispute);
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
        if($this->sms_notify_open_dispute){
            try {
                $this->notifyCustomerAboutOpenDisputeBySms($dispute);
                $this->notifyContractorAboutOpenDisputeBySms($dispute);
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * @param Dispute $dispute
     * @return mixed|void
     * @throws \Exception
     */
    public function notifyCustomerAboutOpenDisputeByEmail(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getCustomer()->getEmail();
        $disputeOutput = $this->getDisputeOutput($dispute);
        $data = [
            'disputeOutput' => $disputeOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'contractor_email' => $deal->getContractor()->getUser()->getEmail()->getEmail(),
            'contractor_phone' => $deal->getContractor()->getUser()->getPhone()->getPhoneNumber(),
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::OPEN_DISPUTE_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * @param Dispute $dispute
     * @return mixed|void
     * @throws \Exception
     */
    public function notifyContractorAboutOpenDisputeByEmail(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getContractor()->getEmail();
        $disputeOutput = $this->getDisputeOutput($dispute);
        $data = [
            'disputeOutput' => $disputeOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'customer_email' => $deal->getCustomer()->getUser()->getEmail()->getEmail(),
            'customer_phone' => $deal->getCustomer()->getUser()->getPhone()->getPhoneNumber(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::OPEN_DISPUTE_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * @param Dispute $dispute
     * @throws \Exception
     */
    public function notifyCustomerAboutOpenDisputeBySms(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getCustomerDisputeOpenSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Dispute $dispute
     * @throws \Exception
     */
    public function notifyContractorAboutOpenDisputeBySms(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getContractorDisputeOpenSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Dispute $dispute
     * @param $is_operator
     * @param $number_of_extension_days
     * @return bool
     * @throws \Exception
     */
    public function notifyDealAgentsAboutClosedDispute(Dispute $dispute, $is_operator, $number_of_extension_days)
    {
        //уведомление о закрытии спора покупателем
        if ($this->email_notify_customer_closed_dispute && !$is_operator) {
            try {
                $this->notifyCustomerAboutCustomerClosedDisputeByEmail($dispute);
                $this->notifyContractorAboutCustomerClosedDisputeByEmail($dispute);
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_customer_closed_dispute && !$is_operator) {
            try {
                $this->notifyCustomerAboutCustomerClosedDisputeBySms($dispute);
                $this->notifyContractorAboutCustomerClosedDisputeBySms($dispute);
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }
        //уведомление о закрытии спора и продление периода гарантии
        if ($this->email_notify_closed_dispute_with_extension && $is_operator && $number_of_extension_days > 0) {
            try {
                $this->notifyCustomerAboutOperatorClosedDisputeWithDayByEmail($dispute, $number_of_extension_days);
                $this->notifyContractorAboutOperatorClosedDisputeWithDayByEmail($dispute, $number_of_extension_days);
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_closed_dispute_with_extension && $is_operator && $number_of_extension_days > 0) {
            try {
                $this->notifyCustomerAboutClosedDisputeWithDayBySms($dispute, $number_of_extension_days);
                $this->notifyContractorAboutClosedDisputeWithDayBySms($dispute, $number_of_extension_days);
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }
        //уведомление о закрытии спора без продление периода гарантии
        if ($this->email_notify_closed_dispute_without_extension && $is_operator && $number_of_extension_days == 0) {
            try {
                $this->notifyCustomerAboutOperatorClosedDisputeWithoutDayByEmail($dispute);
                $this->notifyContractorAboutOperatorClosedDisputeWithoutDayByEmail($dispute);
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_closed_dispute_without_extension && $is_operator &&  $number_of_extension_days == 0) {
            try {
                $this->notifyCustomerAboutClosedDisputeWithoutDayBySms($dispute);
                $this->notifyContractorAboutClosedDisputeWithoutDayBySms($dispute);
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     *  Уведомление о закрытии спора без продления периода гарантии для кастомера
     *
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyCustomerAboutOperatorClosedDisputeWithoutDayByEmail(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getCustomer()->getEmail();
        $disputeOutput = $this->getDisputeOutput($dispute);
        $data = [
            'disputeOutput' => $disputeOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::CLOSED_DISPUTE_WITHOUT_DAY_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * Уведомление о закрытии спора без продления периода гарантии для контрактора
     *
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyContractorAboutOperatorClosedDisputeWithoutDayByEmail(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getContractor()->getEmail();
        $disputeOutput = $this->getDisputeOutput($dispute);
        $data = [
            'disputeOutput' => $disputeOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::CLOSED_DISPUTE_WITHOUT_DAY_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     *  Уведомление о закрытии спора и продление периода гарантии для кастомера
     *
     * @param Dispute $dispute
     * @param $number_of_extension_days
     * @throws \Exception
     */
    private function notifyCustomerAboutOperatorClosedDisputeWithDayByEmail(Dispute $dispute, $number_of_extension_days)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getCustomer()->getEmail();
        $disputeOutput = $this->getDisputeOutput($dispute);
        $data = [
            'disputeOutput' => $disputeOutput,
            'deal' => $deal,
            'extension_days' => $number_of_extension_days,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::CLOSED_DISPUTE_WITH_DAY_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * Уведомление о закрытии спора и продление периода гарантии для контрактора
     *
     * @param Dispute $dispute
     * @param $number_of_extension_days
     * @throws \Exception
     */
    private function notifyContractorAboutOperatorClosedDisputeWithDayByEmail(Dispute $dispute, $number_of_extension_days)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getContractor()->getEmail();
        $disputeOutput = $this->getDisputeOutput($dispute);
        $data = [
            'disputeOutput' => $disputeOutput,
            'deal' => $deal,
            'extension_days' => $number_of_extension_days,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::CLOSED_DISPUTE_WITH_DAY_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * уведомление о закрытии спора кастомером для кастомера
     *
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyCustomerAboutCustomerClosedDisputeByEmail(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getCustomer()->getEmail();
        $disputeOutput = $this->getDisputeOutput($dispute);
        $data = [
            'disputeOutput' => $disputeOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CONTRACTOR',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::CUSTOMER_CLOSED_DISPUTE_FOR_CUSTOMER,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * уведомление о закрытии спора кастомером для контрактора
     *
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyContractorAboutCustomerClosedDisputeByEmail(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        $email = $deal->getContractor()->getEmail();
        $disputeOutput = $this->getDisputeOutput($dispute);
        $data = [
            'disputeOutput' => $disputeOutput,
            'deal' => $deal,
            'customer_login' => $deal->getCustomer()->getUser()->getLogin(),
            'contractor_login' => $deal->getContractor()->getUser()->getLogin(),
            'counter_deal_agent_role' => 'CUSTOMER',
            DisputeNotificationSender::TYPE_NOTIFY_KEY => DisputeNotificationSender::CUSTOMER_CLOSED_DISPUTE_FOR_CONTRACTOR,
        ];

        $this->disputeNotificationSender->sendMail($email, $data);
    }

    /**
     * sms уведомление о закрытии спора кастомером для кастомера
     *
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyCustomerAboutCustomerClosedDisputeBySms(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getCustomerClosedDisputeForCustomerSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * sms уведомление о закрытии спора кастомером для контрактора
     *
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyContractorAboutCustomerClosedDisputeBySms(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getCustomerClosedDisputeForContractorSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * sms уведомление о закрытии спора c продлением гарантии для кастомера
     *
     * @param Dispute $dispute
     * @param $number_of_extension_days
     * @throws \Exception
     */
    private function notifyCustomerAboutClosedDisputeWithDayBySms(Dispute $dispute, $number_of_extension_days)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getClosedDisputeWithDayForCustomerSmsMessage($deal, $number_of_extension_days);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * sms уведомление о закрытии спора c продлением гарантии для контрактора
     *
     * @param Dispute $dispute
     * @param $number_of_extension_days
     * @throws \Exception
     */
    private function notifyContractorAboutClosedDisputeWithDayBySms(Dispute $dispute, $number_of_extension_days)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getClosedDisputeWithDayForContractorSmsMessage($deal, $number_of_extension_days);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyCustomerAboutClosedDisputeWithoutDayBySms(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $contractor */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getClosedDisputeWithoutDayForCustomerSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Dispute $dispute
     * @throws \Exception
     */
    private function notifyContractorAboutClosedDisputeWithoutDayBySms(Dispute $dispute)
    {
        /** @var Deal $deal */
        $deal = $dispute->getDeal();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getClosedDisputeWithoutDayForContractorSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Dispute $dispute
     * @param User $user
     * @return bool
     */
    public static function isUserPartOfDispute(Dispute $dispute, User $user): bool
    {
        $deal = $dispute->getDeal();

        return $deal->getCustomer()->getUser() === $user || $deal->getContractor()->getUser() === $user;
    }
}