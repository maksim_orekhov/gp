<?php
namespace Application\Service;

use Application\Entity\GlobalSetting;
use Application\Entity\SystemPaymentDetails;
use Doctrine\ORM\EntityManager;

/**
 * Class SettingManager
 * @package Application\Service
 */
class SettingManager extends HydratorManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array \Application\Entity\GlobalSetting
     */
    public function findAllGlobalSettings()
    {
        return $this->entityManager->getRepository(GlobalSetting::class)->findAll();
    }

    /**
     * @return array
     */
    public function getGlobalSettingsToArray()
    {
        $result = [];
        $globalSettings = $this->findAllGlobalSettings();
        foreach ($globalSettings as $globalSetting) {
            $result[$globalSetting->getName()] = $globalSetting->getValue();
        }
        return $result;
    }

    public function getValueByNameGlobalSetting($name_setting)
    {
        $globalSetting = $this->entityManager->getRepository(GlobalSetting::class)->findOneBy(array('name' => $name_setting));
        if ($globalSetting === null){

            return null;
        }

        return $globalSetting->getValue();
    }

    /**
     * @param array $data
     * @return array
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateGlobalSettings(array $data = [])
    {
        $globalSettings = $this->findAllGlobalSettings();

        $result = [];
        foreach ($globalSettings as $key => $globalSetting) {
            $result[$key] = $this->setGlobalSetting($globalSetting, $data);
        }
        $this->entityManager->flush();

        return $result;
    }

    /**
     * @param GlobalSetting $globalSetting
     * @param array $data
     * @return GlobalSetting
     */
    private function setGlobalSetting(GlobalSetting $globalSetting, array $data = [])
    {
        $settingName = $globalSetting->getName();
        $settingOldValue = $globalSetting->getValue();

        if (array_key_exists($settingName, $data) && $data[$settingName] !== $settingOldValue) {
            $globalSetting->setValue($data[$settingName]);
            $this->entityManager->persist($globalSetting);
        }

        return $globalSetting;
    }

    /**
     * @return object \Application\Entity\SystemPaymentDetails
     * @throws \Exception
     */
    public function getSystemPaymentDetail()
    {
        $paymentDetail = $this->entityManager->getRepository(SystemPaymentDetails::class)->findAll();
        if ( empty($paymentDetail) || !\is_array($paymentDetail) ) {
            throw new \Exception("Not found payment details");
        }
        if ( \count($paymentDetail) > 1) {
            throw new \Exception("Was found more than one payment details");
        }

        $paymentDetail = $paymentDetail[0];

        return $paymentDetail;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getSystemPaymentDetailOutput()
    {
        try {
            /** @var SystemPaymentDetails $paymentDetail */ // Can throw Exception
            $paymentDetail = $this->getSystemPaymentDetail();
        }
        catch (\Throwable $t) {

            throw new \Exception($t);
        }

        $payment_detail = [];
        $payment_detail['id']                       = $paymentDetail->getId();
        $payment_detail['name']                     = $paymentDetail->getName();
        $payment_detail['bik']                      = $paymentDetail->getBik();
        $payment_detail['account_number']           = $paymentDetail->getAccountNumber();
        $payment_detail['payment_recipient_name']   = $paymentDetail->getPaymentRecipientName();
        $payment_detail['inn']                      = $paymentDetail->getInn();
        $payment_detail['kpp']                      = $paymentDetail->getKpp();
        $payment_detail['bank']                     = $paymentDetail->getBank();
        $payment_detail['corr_account_number']      = $paymentDetail->getCorrAccountNumber();

        return $payment_detail;
    }

    /**
     * @param $systemPaymentDetail
     * @param array $data
     * @return object
     * @throws \Exception
     */
    public function updateSystemPaymentDetail($systemPaymentDetail, array $data = []) {
        if ( ! $systemPaymentDetail instanceof SystemPaymentDetails){

            throw new \Exception('Is not subject SystemPaymentDetails');
        }

        $missKeys = $this->checkKeyRequired([
            'name',
            'bik',
            'account_number',
            'payment_recipient_name',
            'inn',
            'kpp',
            'bank',
            'corr_account_number',
        ], $data);

        if (count($missKeys)) {
            throw new \Exception('Missing required keys: '.implode(", ", $missKeys));
        }

        $systemPaymentDetail = $this->hydrate($data, $systemPaymentDetail);
        $this->entityManager->persist($systemPaymentDetail);
        $this->entityManager->flush();

        return $systemPaymentDetail;
    }

    /**
     * @return SystemPaymentDetails
     */
    public function getGuarantPayBankTransferDetails()
    {
        /** @var SystemPaymentDetails $paymentDetails */
        $paymentDetails = $this->entityManager->getRepository(SystemPaymentDetails::class)
            ->findOneBy(['name' => 'ООО "Гарант Пэй"']);

        return $paymentDetails;
    }

    /**
     * Проверяет, что GP является участником платежа (получатель или плательщик)
     *
     * @param array $payment_order
     * @return bool
     */
    public function isGuarantPayParticipant(array $payment_order): bool
    {
        return $this->isRecipientGuarantPay($payment_order) || $this->isPayerGuarantPay($payment_order);
    }

    /**
     * @param array $payment_order
     * @return bool
     *
     * @TODO Вынести из этого класса в другой! (PaymentOrderManager?)
     */
    public function isRecipientGuarantPay(array $payment_order): bool
    {
        // @TODO Пока проверяем один вариант. Если счетов будет много, то выбирать все и обходить циклом.
        /** @var SystemPaymentDetails $paymentDetails */
        $paymentDetails = $this->getGuarantPayBankTransferDetails();

        // Если хотя бы один параметр из этих данных платёжки не соотвествуют реквизитам GP
        if ($paymentDetails->getAccountNumber() === $payment_order['recipient_account']
            && $paymentDetails->getInn() === $payment_order['recipient_inn']) {

            return true;
        }

        return false;
    }

    /**
     * @param array $payment_order
     * @return bool
     *
     * @TODO Вынести из этого класса в другой! (PaymentOrderManager?)
     */
    public function isPayerGuarantPay(array $payment_order): bool
    {
        // @TODO Пока проверяем один вариант. Если счетов будет много, то выбирать все и обходить циклом.
        /** @var SystemPaymentDetails $paymentDetails */
        $paymentDetails = $this->entityManager->getRepository(SystemPaymentDetails::class)
            ->findOneBy(['name' => 'ООО "Гарант Пэй"']);
        // Если хотя бы один параметр из этих данных платёжки не соотвествуют реквизитам GP
        if ($paymentDetails->getAccountNumber() != $payment_order['payer_account']
            || $paymentDetails->getCorrAccountNumber() != $payment_order['payer_corr_account']
            #|| $paymentDetails->getPaymentRecipientName() != $payment_order['payer_1']
            #|| $paymentDetails->getBank() != $payment_order['payer_bank_1']
            || $paymentDetails->getBik() != $payment_order['payer_bik']
            || $paymentDetails->getInn() != $payment_order['payer_inn']
            || $paymentDetails->getKpp() != $payment_order['payer_kpp']) {

            return false;
        }

        return true;
    }
}