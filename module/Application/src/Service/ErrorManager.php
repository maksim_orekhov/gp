<?php

namespace Application\Service;

use Application\Provider\Mail\ErrorMailSender;


class ErrorManager
{
    const DEFAULT_MAX_LEVEL = 5;

    const INVALID_DATA = 'Trying send notification email with invalid data';

    /**
     * @var ErrorMailSender $errorMailSender
     */
    private $errorMailSender;
    /**
     * @var array
     */
    private $config;

    /**
     * InvoiceManager constructor.
     * @param array $config
     */
    public function __construct(array $config, $errorMailSender)
    {
        $this->errorMailSender = $errorMailSender;
        $this->config = $config;
    }

    /**
     * @param $needle
     * @param $haystack
     * @return bool
     */
    private static function in_array_recursive($needle, $haystack)
    {

        foreach ($haystack AS $element) {
            if ($element === $needle) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function sendAnyErrorMessage($data)
    {

        $email = $this->config['admin_email'];
        $data['message'] = (is_string($data['message'])) ? $data['message'] : self::print_r_level($data['message']);
        $this->errorMailSender->sendMail($email, $data);
        return true;
    }

    /**
     * @return array
     */
    public function processingAjaxData($data)
    {
        return [
            'code' => $data['code'] ?? self::INVALID_DATA,
            'message' => $data['message'] ?? self::INVALID_DATA,
        ];
    }

    /**
     * @param $user_email
     * @param $error_message
     * @param $error_report
     * @param null $error_tracing
     * @throws \Exception
     */
    public function sendErrorReport($user_email, $error_message, $error_report, $error_tracing = null)
    {
        $email = $this->config['admin_email'];
        $data = [
            'code' => ErrorMailSender::ERROR_REPORT,
            'user_email' => $user_email,
            'error_message' => $error_message,
            'error_tracing' => $error_tracing,
            'error_report' => $error_report,
        ];
        $this->errorMailSender->sendMail($email, $data);
    }

    /**
     * @param $data
     * @param int $level
     * @return string
     */
    private static function print_r_level($data, $level = self::DEFAULT_MAX_LEVEL)
    {
        static $innerLevel = 1;

        static $tabLevel = 1;

        static $cache = array();

        $output = '';
        $type = gettype($data);
        $tabs = str_repeat('    ', $tabLevel);
        $quoteTabs = str_repeat('    ', $tabLevel - 1);

        $recursiveType = array('object', 'array');

        // Recursive
        if (in_array($type, $recursiveType)) {
            $elements = array();

            // If type is object, try to get properties by Reflection.
            if ($type == 'object') {
                if (self::in_array_recursive($data, $cache)) {
                    return "\n{$quoteTabs}*RECURSION*\n";
                }

                // Cache the data
                $cache[] = $data;

                $output = get_class($data) . ' ' . ucfirst($type);
                $ref = new \ReflectionObject($data);
                $properties = $ref->getProperties();

                foreach ($properties as $property) {
                    $property->setAccessible(true);

                    $pType = '\'' . $property->getName() . '\'';

                    if ($property->isProtected()) {
                        $pType = "protected:" . $pType;
                    } elseif ($property->isPrivate()) {
                        $pType = "private:" . $pType;
                    }

                    if ($property->isStatic()) {
                        $pType = "static:" . $pType;
                    }

                    $elements[$pType] = $property->getValue($data);
                }
            } // If type is array, just return it's value.
            elseif ($type == 'array') {
                $output = ucfirst($type);
                $elements = $data;
            }

            // Start dumping data
            if ($level == 0 || $innerLevel < $level) {
                // Start recursive print
                $output .= "\n{$quoteTabs}(";

                foreach ($elements as $key => $element) {
                    $output .= "\n{$tabs}{$key} => ";

                    // Increment level
                    $tabLevel = $tabLevel + 2;
                    $innerLevel++;

                    $output .= in_array(gettype($element), $recursiveType) ? self::print_r_level($element, $level) : $element;

                    // Decrement level
                    $tabLevel = $tabLevel - 2;
                    $innerLevel--;
                }

                $output .= "\n{$quoteTabs})\n";
            } else {
                $output .= "\n{$quoteTabs}*MAX LEVEL*\n";
            }
        }

        // Clean cache
        if ($innerLevel == 1) {
            $cache = array();
        }

        return $output;
    }
}