<?php
namespace Application\Service;

use Application\Entity\Deal;
use Application\Entity\User;
use Application\Service\BankClient\BankClientManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

class InvoiceManager
{
    const ACCORDING_TO_OFFER_CONTRACT = ' согласно договора оферты №1 от 03.04.2018';

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * InvoiceManager constructor.
     * @param EntityManager $entityManager
     * @param NdsTypeManager $ndsTypeManager
     */
    public function __construct(EntityManager $entityManager, NdsTypeManager $ndsTypeManager)
    {
        $this->entityManager    = $entityManager;
        $this->ndsTypeManager   = $ndsTypeManager;
    }

    /**
     * @param User $user
     * @param array $paginationParams
     * @param bool $isOperator
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getCollectionConfirmedDealForInvoice(User $user, array $paginationParams, $isOperator = false)
    {
        $collectionDeal = $this->entityManager->getRepository(Deal::class)
            ->findCollectionConfirmedDealForInvoice($user, $paginationParams, $isOperator);

        return $collectionDeal;
    }

    /**
     * @param $dealInvoices
     * @return array
     */
    public function getDealCollectionForInvoiceOutput($dealInvoices)
    {
        $dealInvoicesOutput = [];
        /** @var ArrayCollection $dealInvoices */
        foreach ($dealInvoices as $dealInvoice) {
            $dealInvoicesOutput[] = $this->getDealForInvoiceOutput($dealInvoice);
        }

        return $dealInvoicesOutput;
    }

    /**
     * @param Deal $dealInvoice
     * @return array
     */
    public function getDealForInvoiceOutput(Deal $dealInvoice)
    {
        $dealInvoiceOutput = [];

        $dealInvoiceOutput['id'] = $dealInvoice->getId();
        $dealInvoiceOutput['deal_name'] = $dealInvoice->getName();
        $dealInvoiceOutput['deal_number'] = $dealInvoice->getNumber();

        return $dealInvoiceOutput;
    }

    /**
     * @param Deal $deal
     * @return string
     */
    public function getNdsDeclaration(Deal $deal): string
    {
        return $this->ndsTypeManager->getPaymentNdsTypeDeclaration($deal);
    }

    /**
     * @param Deal $deal
     * @return float|int|null
     * @throws \Exception
     */
    public function getNdsValue(Deal $deal)
    {
        try {
            return $this->ndsTypeManager->getPaymentNdsTypeValue($deal);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param Deal $deal
     * @param bool $with_nds_value
     * @return string
     * @throws \Exception
     *
     * Схема:
     * Оплата по сделке #GP202U от 11.07.2018 согласно договора оферты №1 от 03.04.2018. В т. ч. НДС 18% - 610.17
     */
    public function generateInvoicePaymentPurpose(Deal $deal, $with_nds_value = false): string
    {
        $payment_purpose = BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY . $deal->getNumber() . ' от ' . $deal->getCreated()->format('d.m.Y');
        $payment_purpose .= self::ACCORDING_TO_OFFER_CONTRACT;
        $payment_purpose .= '. ' . $this->ndsTypeManager->getPaymentNdsTypeDeclaration($deal);

        if ($with_nds_value) {
            try {
                $payment_purpose .= ' - ' . $this->ndsTypeManager->getPaymentNdsTypeValue($deal) . '.';
            }
            catch (\Throwable $t) {

                throw new \Exception($t->getMessage());
            }
        }

        return $payment_purpose;
    }
}