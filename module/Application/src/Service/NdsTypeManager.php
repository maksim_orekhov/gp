<?php
namespace Application\Service;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\Payment;
use Application\Service\Payment\CalculatedDataProvider;
use Doctrine\ORM\EntityManager;
use Application\Entity\NdsType;

/**
 * Class NdsTypeManager
 * @package Application\Service
 */
class NdsTypeManager
{
    const NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY = [
        '0' => 'В т. ч. НДС 18%',
        '1' => 'В т. ч. НДС 18%',
        '2' => 'В т. ч. НДС 18%',
        '3' => 'В т. ч. НДС 18%',
        '4' => 'В т. ч. НДС 18%'
    ];

    const NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY = [
        '0' => 'Без НДС',
        '1' => 'Без НДС',
        '2' => 'Без НДС',
        '3' => 'В т. ч. НДС 10%',
        '4' => 'В т. ч. НДС 18%'
    ];

    const NDS_TYPES_DECLARATIONS_FOR_PAYOFF = 'В т. ч. НДС'; // Добавляется процент и сумма
    const NOT_SUBJECT_FOR_NDS = 'НДС не облагается';

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider;

    /**
     * NdsTypeManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager,
                                CalculatedDataProvider $calculatedDataProvider)
    {
        $this->entityManager            = $entityManager;
        $this->calculatedDataProvider   = $calculatedDataProvider;
    }

    /**
     * @return array
     */
    public function getNdsTypes()
    {
        return $this->entityManager->getRepository(NdsType::class)->findAll();
    }

    /**
     * @return array
     */
    public function getActiveNdsTypes(): array
    {
        return $this->entityManager
            ->getRepository(NdsType::class)->findBy(['isActive' => 1]);
    }

    /**
     * @param $nds_type_id
     * @return null|object
     */
    public function getNdsTypeById($nds_type_id)
    {
        return $this->entityManager->getRepository(NdsType::class)->find((int) $nds_type_id);
    }

    /**
     * @param int $nds_type
     * @return null|object
     */
    public function getNdsTypeByType(int $nds_type)
    {
        /** @var NdsType $ndsType */
        return $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(array('type' => $nds_type));
    }

    /**
     * @param $ndsTypes
     * @return array
     */
    public function extractNdsTypeCollection($ndsTypes): array
    {
        $ndsTypesOutput = [];

        foreach ($ndsTypes as $ndsType) {

            $ndsTypesOutput[] = $this->getNdsTypeOutput($ndsType);
        }

        return $ndsTypesOutput;
    }

    /**
     * @param NdsType $ndsType
     * @return array
     */
    public function getNdsTypeOutput(NdsType $ndsType): array
    {
        $ndsTypeOutput = [];
        $ndsTypeOutput['id'] = $ndsType->getId();
        $ndsTypeOutput['type'] = $ndsType->getType();
        $ndsTypeOutput['name'] = $ndsType->getName();
        $ndsTypeOutput['description'] = $ndsType->getDescription();

        return $ndsTypeOutput;
    }

    /**
     * @param DealAgent $dealAgent
     * @return \Application\Entity\NdsType|null
     */
    public function getNdsTypeOfDealAgent(DealAgent $dealAgent)
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $dealAgent->getCivilLawSubject();

        if (null !== $civilLawSubject->getNaturalPerson()) {

            return null;
        }

        return $civilLawSubject->getNdsType();
    }


    /// Ниже методы для описания типа и суммы НДС. Возможно, вынести в отдельный класс.

    /**
     * @param Deal $deal
     * @return string
     */
    public function getPaymentNdsTypeDeclaration(Deal $deal): string
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->getNdsType($deal);

        return self::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[$ndsType->getType()];
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return mixed|string
     *
     * В том числе НДС процент - сумма
     * @throws \Exception
     */
    public function getPaymentPayOffNdsTypeDeclaration(Deal $deal)
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->getNdsType($deal);

        $nds_declaration = self::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[$ndsType->getType()];

        $value = $this->getPaymentNdsTypeValue($deal);

        if (null !== $value) {
            $nds_declaration .=  ' ' . $value;
        }

        return $nds_declaration;
    }

    /**
     * @param Deal $deal
     * @return float|int|null
     * @throws \Exception
     */
    public function getPaymentNdsTypeValue(Deal $deal)
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->getNdsType($deal);

        try {
            return $this->getNdsAmountForInvoice($deal, $ndsType->getType());
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return mixed|string
     *
     * В том числе НДС процент - сумма
     * @throws \Exception
     */
    public function getPayOffNdsTypeDeclaration(Deal $deal, $amount)
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->getNdsType($deal);

        $nds_declaration = self::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[$ndsType->getType()];

        $value = $this->getPaymentNdsTypeValue($deal);

        if (null !== $value) {
            $nds_declaration .=  ' ' . $value;
        }

        return $nds_declaration;
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getRefundNdsTypeDeclaration(Deal $deal, $amount): string
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->getNdsType($deal);

        $nds_declaration = self::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY[$ndsType->getType()];

        if($nds_declaration !== self::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY['0']) {

            $value = $this->getOutgoingPayNdsAmount($ndsType->getType(), $amount);

            if (null !== $value) {
                $nds_declaration .=  ' ' . $value;
            }
        }

        return $nds_declaration;
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getDiscountNdsTypeDeclaration(Deal $deal, $amount): string
    {
        return $this->getRefundNdsTypeDeclaration($deal, $amount);
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return string
     */
    public function getOverpayNdsTypeDeclaration(Deal $deal, $amount): string
    {
        return $this->getRefundNdsTypeDeclaration($deal, $amount);
    }

    /**
     * @param $value
     * @return float|int
     *
     * Продавец с НДС 18%
     * для входящего:
     * выставляется счет на сумму сделки c учетом коммисии, в назначении
     * платежа пишется в т. ч. НДС 18%, НДС = С / 1,18 × 0,18 — где С - сумма, которую платит покупатель
     * для исхлдящего:
     * все то же самое, только где С - сумма, которая выплачивается продавцу
     */
    public function countNds18($value)
    {
        $value = ($value/1.18) * 0.18;

        return CalculatedDataProvider::roundAmount($value);
    }

    /**
     * @param $fee_amount
     * @return float|int
     *
     * Продавец без НДС
     * выставляется счет на сумму сделки c учетом коммисии, в назначении
     * платежа пишется в т. ч. НДС 18%, НДС = К / 1,18 × 0,18 — где К сумма комиссии
     */
    public function countNdsWithoutNds($fee_amount)
    {
        $value = ($fee_amount/1.18) * 0.18;

        return CalculatedDataProvider::roundAmount($value);
    }

    /**
     * @param $released_value
     * @return float|int
     *
     * Продавец с НДС 10%
     * выставляется счет на сумму сделки c учетом коммисии, в назначении
     * платежа пишется в т. ч. НДС 18%, НДС = (К / 1,18 × 0,18)+(З / 1,10 × 0,10)
     * — где К сумма комиссии, З сумма которая переводится продавцу
     */
    public function countNds10($released_value, $fee_amount)
    {
        $value = CalculatedDataProvider::roundAmount($fee_amount/1.18 * 0.18)
            + CalculatedDataProvider::roundAmount($released_value/1.10 * 0.10);

        return $value;
    }

    /**
     * @param $released_value
     * @return float|int
     */
    public function countNds10ForOutgoingPay($released_value)
    {
        $value = ($released_value/1.10) * 0.10;

        return CalculatedDataProvider::roundAmount($value);
    }

    /**
     * @param $fee_amount
     * @return float|int
     *
     * Продавец с НДС 0%
     * выставляется счет на сумму сделки c учетом коммисии, в назначении
     * платежа пишется в т. ч. НДС 18%, НДС = К / 1,18 × 0,18 — где К сумма комиссии
     */
    public function countNds0($fee_amount)
    {
        $value = ($fee_amount/1.18) * 0.18;

        return CalculatedDataProvider::roundAmount($value);
    }

    /**
     * @param Deal $deal
     * @return NdsType|null|object
     */
    private function getNdsType(Deal $deal)
    {
        /** @var DealAgent $contractorDealAgent */
        $contractorDealAgent = $deal->getContractor();
        /** @var NdsType|null $ndsType */
        $ndsType = $this->getNdsTypeOfDealAgent($contractorDealAgent);

        if (null === $ndsType) {
            // Если $ndsType = null (физ.лицо), то приравниваем к типу "Без НДС"
            $ndsType = $this->getNdsTypeByType(2);
        }

        return $ndsType;
    }

    /**
     * @param Deal $deal
     * @param $type_of_nds_type
     * @return float|int|null
     * @throws \Exception
     */
    private function getNdsAmountForInvoice(Deal $deal, $type_of_nds_type)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $nds_value = null;

        switch ($type_of_nds_type) {

            case 1:
                $nds_value = $this->countNds0(
                    $this->calculatedDataProvider->getFeeAmount($payment->getDealValue())
                );
                break;

            case 2:
                $nds_value = $this->countNdsWithoutNds(
                    $this->calculatedDataProvider->getFeeAmount($payment->getDealValue())
                );
                break;

            case 3:
                $nds_value = $this->countNds10(
                    $payment->getReleasedValue(),
                    $this->calculatedDataProvider->getFeeAmount($payment->getDealValue())
                );
                break;

            case 4:
                $nds_value = $this->countNds18($payment->getExpectedValue());
                break;
        }

        return $nds_value;
    }

    /**
     * @param int $type_of_nds_type
     * @param $amount
     * @return float|int|null
     */
    private function getOutgoingPayNdsAmount(int $type_of_nds_type, $amount)
    {
        $nds_value = [];

        switch ($type_of_nds_type) {

            case 1:
                $nds_value = null;
                break;

            case 2:
                $nds_value = null;
                break;

            case 3:
                $nds_value = $this->countNds10ForOutgoingPay($amount);
                break;

            case 4:
                $nds_value = $this->countNds18($amount);
                break;
        }

        return $nds_value;
    }
}