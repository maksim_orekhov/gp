<?php
namespace Application\Service;

use Application\Entity\DealAgent;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Provider\Mail\DealNotificationSender;
use Application\Service\Deal\DealPolitics;
use Doctrine\ORM\EntityManager;
use Application\Entity\Delivery;
use Application\Entity\Deal;
use ModuleCode\Service\SmsStrategyContext;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\Entity\DeliveryTrackingNumber;
use ModuleDelivery\Service\DeliveryOrderManager;
use ModulePaymentOrder\Service\PayOffManager;

/**
 * Class DeliveryManager
 * @package Application\Service
 */
class DeliveryManager
{
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    /**
     * notify params
     */
    private $email_notify_delivery_confirm;
    private $sms_notify_delivery_confirm;

    private $email_notify_delivery_tracking_number_creation;
    private $sms_notify_delivery_tracking_number_creation;

    private $sms_notify_successful_delivery;
    private $email_notify_successful_delivery;

    private $sms_notify_unsuccessful_delivery;
    private $email_notify_unsuccessful_delivery;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var DealNotificationSender
     */
    private $dealNotificationSender;

    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * @var DeliveryOrderManager
     */
    private $deliveryOrderManager;

    /**
     * @var PayOffManager
     */
    private $payOffManager;

    /**
     * @var array
     */
    private $config;

    /**
     * DeliveryManager constructor.
     * @param EntityManager $entityManager
     * @param DealNotificationSender $dealNotificationSender
     * @param DealPolitics $dealPolitics
     * @param DeliveryOrderManager $deliveryOrderManager
     * @param PayOffManager $payOffManager
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                DealNotificationSender $dealNotificationSender,
                                DealPolitics $dealPolitics,
                                DeliveryOrderManager $deliveryOrderManager,
                                PayOffManager $payOffManager,
                                array $config)
    {
        $this->entityManager          = $entityManager;
        $this->dealNotificationSender = $dealNotificationSender;
        $this->dealPolitics           = $dealPolitics;
        $this->deliveryOrderManager   = $deliveryOrderManager;
        $this->payOffManager          = $payOffManager;
        $this->config                 = $config;

        // set notify params from config
        $this->sms_notify_delivery_confirm = $this->getDealNotifyConfig($config, 'delivery_confirm', 'sms');
        $this->email_notify_delivery_confirm = $this->getDealNotifyConfig($config, 'delivery_confirm', 'email');

        $this->sms_notify_delivery_tracking_number_creation = $this->getDealNotifyConfig($config,'delivery_tracking_number_creation', 'sms');
        $this->email_notify_delivery_tracking_number_creation = $this->getDealNotifyConfig($config, 'delivery_tracking_number_creation', 'email');

        $this->sms_notify_successful_delivery = $this->getDealNotifyConfig($config, 'delivery_successful', 'sms');
        $this->email_notify_successful_delivery = $this->getDealNotifyConfig($config, 'delivery_successful', 'email');

        $this->sms_notify_unsuccessful_delivery = $this->getDealNotifyConfig($config, 'delivery_unsuccessful', 'sms');
        $this->email_notify_unsuccessful_delivery = $this->getDealNotifyConfig($config, 'delivery_unsuccessful', 'email');
    }

    /**
     * @param array $delivered
     * @return bool
     * @throws \Exception
     */
    public function manageSuccessfulDeliveries(array $delivered): bool
    {
        $delivery_orders = '';

        try {
            foreach ($delivered as $data) {
                /*
                 * Каждый элемент содержит три ключа со значениями:
                 * 'order_number' // Номер заказа в информационной системе клиента
                 * 'dpd_order_number' // Номер заказа в информационной системе DPD
                 * 'delivery_time' // Время перехода состояния / 2012-04-04T17:10:15
                 */
                /** @var DeliveryOrder $deliveryOrder */
                $deliveryOrder = $this->deliveryOrderManager->getDeliveryOrderByNumber($data['order_number']);

                if ($deliveryOrder instanceof DeliveryOrder) {
                    /** @var Deal $deal */
                    $deal = $deliveryOrder->getDeal();

                    // Если доставка по этой сделке уже подтверждена, ничего не делаем и переходим к следующему элементу
                    if (true === $this->isDeliveryConfirmed($deal)) {
                        logMessage('Delivery for deal ' . $deal->getNumber(). ' ('. $deliveryOrder->getOrderNumber() .') already confirmed', 'delivery-successful');
                        continue;
                    }

                    /** @var \DateTime $deliveryDate */
                    $deliveryDate = \DateTime::createFromFormat('Y-m-d\TH:i:s', $data['delivery_time']);
                    /** @var Delivery $delivery */
                    $delivery = $this->confirmDelivery($deal, $deliveryDate);

                    if (null === $delivery) {
                        logMessage('No successful Delivery created for DeliveryOrder ' . $data['order_number'], 'delivery');
                    } else {
                        // Проверяем можно ли создавать платёжку на выплату и создаем
                        $this->createPayOffIfStatusDeliveryAllows($delivery);
                    }

                    $delivery_orders .= ' ' . $data['order_number'];
                }
            }
        }
        catch (\Throwable $t) {

            logMessage($t->getMessage(), 'delivery-error');
        }

        if ($delivery_orders !== '') {
            logMessage($delivery_orders . ' delivered', 'delivery-successful');
        }

        return true;
    }

    /**
     * @param array $failed
     * @param string $trace_page_url
     * @return bool
     */
    public function manageUnsuccessfulDeliveries(array $failed, string $trace_page_url)
    {
        $delivery_orders = '';

        try {
            foreach ($failed as $data) {
                /*
                 * Каждый элемент содержит два ключа со значениями:
                 * 'order_number' // Номер заказа в информационной системе клиента
                 * 'dpd_order_number' // Номер заказа в информационной системе DPD
                 * 'delivery_fail_time' // Время перехода состояния / 2012-04-04T17:10:15
                 */
                /** @var DeliveryOrder $deliveryOrder */
                $deliveryOrder = $this->deliveryOrderManager->getDeliveryOrderByNumber($data['order_number']);
                if ($deliveryOrder instanceof DeliveryOrder) {
                    /** @var Deal $deal */
                    $deal = $deliveryOrder->getDeal();

                    // Если доставка по этой сделке уже подтверждена, ничего не делаем и переходим к следующему элементу
                    if (true === $this->isDeliveryConfirmed($deal)) {
                        logMessage('Delivery for deal ' . $deal->getNumber(). ' ('. $deliveryOrder->getOrderNumber() .') already confirmed', 'delivery-unsuccessful');
                        continue;
                    }

                    /** @var \DateTime $deliveryDate */
                    $deliveryDate = \DateTime::createFromFormat('Y-m-d\TH:i:s', $data['delivery_fail_time']);

                    /** @var DeliveryTrackingNumber $deliveryTrackingNumber */
                    $deliveryTrackingNumber = $deliveryOrder->getDeliveryTrackingNumbers()->last();

                    /** @var Delivery $delivery */
                    $delivery = $this->failDelivery(
                        $deal,
                        $deliveryTrackingNumber->getTrackingNumber(),
                        $trace_page_url,
                        $deliveryDate
                    );

                    if (null === $delivery) {

                        logMessage('No unsuccessful Delivery created for DeliveryOrder '.$data['order_number'], 'delivery');
                    }

                    $delivery_orders .= ' ' . $data['order_number'];
                }
            }
        }
        catch (\Throwable $t) {

            logMessage($t->getMessage(), 'delivery-erros');
        }

        if ($delivery_orders !== '') {
            logMessage($delivery_orders . ' delivery failed', 'delivery-unsuccessful');
        }

        return true;
    }

    /**
     * @param Delivery $delivery
     * @return bool
     * @throws \Core\Exception\LogicException
     */
    public function createPayOffIfStatusDeliveryAllows(Delivery $delivery)
    {
        if ($delivery->getDeliveryStatus()){
            /** @var Deal $deal */
            $deal = $delivery->getDeal();
            //запускаем механизм выплаты
            $this->payOffManager->createPayOff($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);
        }

        return true;
    }

    /**
     * Разрешено конфирмить если
     * 1 не запрешено спором
     * 2 сделка подтверждена обоими агентами
     * 3 сделка оплачена
     * 4 сделка еще не подтверждена
     *
     * @param Deal $deal
     * @return bool
     */
    public function checkPermissionForDeliveryConfirmByDeal(Deal $deal)
    {
        $is_forbidden_confirm_at_dispute = false;
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        if($dispute && (!$dispute->isClosed() || $dispute->getTribunal() || $dispute->getRefund())){

            $is_forbidden_confirm_at_dispute = true;
        }

        $is_deal_approved = $this->dealPolitics->isDealConfirmedByDealAgents($deal);

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $is_deal_paid = $payment && $payment->getDeFactoDate() !== null;

        $is_delivery_confirmed = $this->isDeliveryConfirmed($deal);

        return !$is_forbidden_confirm_at_dispute && $is_deal_approved && $is_deal_paid && !$is_delivery_confirmed;
    }

    /**
     * @param Deal $deal
     * @param \DateTime|null $date
     * @return Delivery
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function confirmDelivery(Deal $deal, \DateTime $date = null): Delivery
    {
        $confirmDate = $date ?: new \DateTime();

        // Create new Delivery object
        $delivery = new Delivery();
        $delivery->setDeliveryDate($confirmDate);
        $delivery->setDeliveryStatus(true);
        $delivery->setDeal($deal);

        $this->entityManager->persist($delivery);
        $this->entityManager->flush();

        // notification about success delivery
        $this->notifyDealAgentsAboutSuccessfulDelivery($deal);

        return $delivery;
    }

    /**
     * @param Deal $deal
     * @param string $tracking_number
     * @param string $trace_page_url
     * @param \DateTime|null $date
     * @return Delivery
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function failDelivery(Deal $deal, string $tracking_number, string $trace_page_url, \DateTime $date = null): Delivery
    {
        $failDate = $date ?: new \DateTime();

        // Create new Delivery object
        $delivery = new Delivery();
        $delivery->setDeliveryDate($failDate);
        $delivery->setDeliveryStatus(false);
        $delivery->setDeal($deal);

        $this->entityManager->persist($delivery);
        $this->entityManager->flush();

        //notification about unsuccessful delivery
        $this->notifyDealAgentsAboutUnsuccessfulDelivery($deal, $tracking_number, $trace_page_url);

        return $delivery;
    }

    /**
     * @param Deal $deal
     * @param string $tracking_number
     * @param string $trace_page_url
     * @return bool
     */
    public function notifyDealAgentsAboutUnsuccessfulDelivery(Deal $deal, string $tracking_number, string $trace_page_url)
    {
        if ($this->email_notify_unsuccessful_delivery) {
            try {
                $this->notifyCustomerAboutUnsuccessfulDeliveryByEmail($deal, $tracking_number, $trace_page_url);
                $this->notifyContractorAboutUnsuccessfulDeliveryByEmail($deal, $tracking_number, $trace_page_url);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_unsuccessful_delivery) {
            try {
                $this->notifyCustomerAboutUnsuccessfulDeliveryBySms($deal, $tracking_number);
                $this->notifyContractorAboutUnsuccessfulDeliveryBySms($deal, $tracking_number);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * Email уведомление для покупателя о неуспешной доставке
     *
     * @param Deal $deal
     * @param string $tracking_number
     * @param string $trace_page_url
     * @throws \Exception
     */
    public function notifyCustomerAboutUnsuccessfulDeliveryByEmail(Deal $deal, string $tracking_number, string $trace_page_url)
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $data = [
            'deal' => $deal,
            'tracking_number' => $tracking_number,
            'trace_page_url' => $trace_page_url,
            'customer_login' => $customer->getUser()->getLogin(),
            'contractor_login' => $contractor->getUser()->getLogin(),
            DealNotificationSender::TYPE_NOTIFY_KEY => DealNotificationSender::TYPE_NOTIFY_UNSUCCESSFUL_DELIVERY_CUSTOMER,
        ];

        $this->dealNotificationSender->sendMail($customer->getEmail(), $data);
    }

    /**
     * Email уведомление для продовца о неуспешной доставке
     *
     * @param Deal $deal
     * @param string $tracking_number
     * @param string $trace_page_url
     * @throws \Exception
     */
    public function notifyContractorAboutUnsuccessfulDeliveryByEmail(Deal $deal, string $tracking_number, string $trace_page_url)
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $data = [
            'deal' => $deal,
            'tracking_number' => $tracking_number,
            'trace_page_url' => $trace_page_url,
            'customer_login' => $customer->getUser()->getLogin(),
            'contractor_login' => $contractor->getUser()->getLogin(),
            DealNotificationSender::TYPE_NOTIFY_KEY => DealNotificationSender::TYPE_NOTIFY_UNSUCCESSFUL_DELIVERY_CONTRACTOR,
        ];

        $this->dealNotificationSender->sendMail($contractor->getEmail(), $data);
    }

    /**
     * Sms уведомление для покупателя о неуспешной доставке
     *
     * @param Deal $deal
     * @param string $tracking_number
     * @throws \Exception
     */
    public function notifyCustomerAboutUnsuccessfulDeliveryBySms(Deal $deal, string $tracking_number)
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getUnsuccessfulDeliverySmsMessage($deal, $tracking_number);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * Sms уведомление для продовца о неуспешной доставке
     *
     * @param Deal $deal
     * @param string $tracking_number
     * @throws \Exception
     */
    public function notifyContractorAboutUnsuccessfulDeliveryBySms(Deal $deal, string $tracking_number)
    {
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getUnsuccessfulDeliverySmsMessage($deal, $tracking_number);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function notifyDealAgentsAboutSuccessfulDelivery(Deal $deal)
    {
        if ($this->email_notify_successful_delivery) {
            try {
                $this->notifyCustomerAboutSuccessfulDeliveryByEmail($deal);
                $this->notifyContractorAboutSuccessfulDeliveryByEmail($deal);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_successful_delivery) {
            try {
                $this->notifyCustomerAboutSuccessfulDeliveryBySms($deal);
                $this->notifyContractorAboutSuccessfulDeliveryBySms($deal);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * Email уведомление для покупателя об успешной доставке
     *
     * @param Deal $deal
     * @throws \Exception
     */
    public function notifyCustomerAboutSuccessfulDeliveryByEmail(Deal $deal)
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $data = [
            'deal' => $deal,
            'customer_login' => $customer->getUser()->getLogin(),
            'contractor_login' => $contractor->getUser()->getLogin(),
            DealNotificationSender::TYPE_NOTIFY_KEY => DealNotificationSender::TYPE_NOTIFY_SUCCESSFUL_DELIVERY_CUSTOMER,
        ];

        $this->dealNotificationSender->sendMail($customer->getEmail(), $data);
    }

    /**
     * Email уведомление для продовца об успешной доставке
     *
     * @param Deal $deal
     * @throws \Exception
     */
    public function notifyContractorAboutSuccessfulDeliveryByEmail(Deal $deal)
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $data = [
            'deal' => $deal,
            'customer_login' => $customer->getUser()->getLogin(),
            'contractor_login' => $contractor->getUser()->getLogin(),
            DealNotificationSender::TYPE_NOTIFY_KEY => DealNotificationSender::TYPE_NOTIFY_SUCCESSFUL_DELIVERY_CONTRACTOR,
        ];

        $this->dealNotificationSender->sendMail($contractor->getEmail(), $data);
    }

    /**
     * Sms уведомление для покупателя об успешной доставке
     *
     * @param Deal $deal
     * @throws \Exception
     */
    public function notifyCustomerAboutSuccessfulDeliveryBySms(Deal $deal)
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getSuccessfulDeliverySmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * Sms уведомление для продовца об успешной доставке
     *
     * @param Deal $deal
     * @throws \Exception
     */
    public function notifyContractorAboutSuccessfulDeliveryBySms(Deal $deal)
    {
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getSuccessfulDeliverySmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param DeliveryTrackingNumber $deliveryTrackingNumber
     * @param string $trace_page_url
     * @return bool
     */
    public function notifyDealAgentsAboutDeliveryServiceOrderCreation(DeliveryTrackingNumber $deliveryTrackingNumber, string $trace_page_url): bool
    {
        /** @var \ModuleDelivery\Entity\DeliveryOrder $deliveryOrder */
        $deliveryOrder = $deliveryTrackingNumber->getDeliveryOrder();
        /** @var Deal $deal */
        $deal = $deliveryOrder->getDeal();

        if ($this->email_notify_delivery_tracking_number_creation) {
            try {
                $this->notifyCustomerAboutDeliveryServiceOrderCreationByEmail($deal, $deliveryTrackingNumber, $trace_page_url);
                $this->notifyContractorAboutDeliveryServiceOrderCreationByEmail($deal, $deliveryTrackingNumber, $trace_page_url);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_delivery_tracking_number_creation) {
            try {
                $this->notifyCustomerAboutDeliveryServiceOrderCreationBySms($deal, $deliveryTrackingNumber);
                $this->notifyContractorAboutDeliveryServiceOrderCreationBySms($deal, $deliveryTrackingNumber);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        logMessage($deliveryTrackingNumber->getTrackingNumber(), 'tracking-number');

        return true;
    }

    /**
     * Email уведомление для покупателя о создании трекер номера
     *
     * @param Deal $deal
     * @param DeliveryTrackingNumber $deliveryTrackingNumber
     * @param string $trace_page_url
     * @throws \Exception
     */
    private function notifyCustomerAboutDeliveryServiceOrderCreationByEmail(Deal $deal,
                                                                              DeliveryTrackingNumber $deliveryTrackingNumber,
                                                                              string $trace_page_url)
    {
        /**
         * @var DealAgent $currentAgent
         * @var DealAgent $authorRequest
         */
        $customer = $deal->getCustomer();
        $contractor = $deal->getContractor();

        $data = [
            'deal' => $deal,
            'tracking_number' => $deliveryTrackingNumber->getTrackingNumber(),
            'trace_page_url' => $trace_page_url,
            'customer_login' => $customer->getUser()->getLogin(),
            'contractor_login' => $contractor->getUser()->getLogin(),
            DealNotificationSender::TYPE_NOTIFY_KEY => DealNotificationSender::TYPE_NOTIFY_DELIVERY_SERVICE_ORDER_CREATION_CUSTOMER,
        ];

        $this->dealNotificationSender->sendMail($customer->getEmail(), $data);
    }

    /**
     * Email уведомление для продавца о создании трекер номера
     *
     * @param Deal $deal
     * @param DeliveryTrackingNumber $deliveryTrackingNumber
     * @param string $trace_page_url
     * @throws \Exception
     */
    private function notifyContractorAboutDeliveryServiceOrderCreationByEmail(Deal $deal,
                                                                                DeliveryTrackingNumber $deliveryTrackingNumber,
                                                                                string $trace_page_url)
    {
        /**
         * @var DealAgent $currentAgent
         * @var DealAgent $authorRequest
         */
        $customer = $deal->getCustomer();
        $contractor = $deal->getContractor();

        $data = [
            'deal' => $deal,
            'tracking_number' => $deliveryTrackingNumber->getTrackingNumber(),
            'trace_page_url' => $trace_page_url,
            'customer_login' => $customer->getUser()->getLogin(),
            'contractor_login' => $contractor->getUser()->getLogin(),
            DealNotificationSender::TYPE_NOTIFY_KEY => DealNotificationSender::TYPE_NOTIFY_DELIVERY_SERVICE_ORDER_CREATION_CONTRACTOR,
        ];

        $this->dealNotificationSender->sendMail($contractor->getEmail(), $data);
    }

    /**
     * Sms уведомление для покупателя о создании трекер номера
     *
     * @param Deal $deal
     * @throws \Exception
     */
    private function notifyCustomerAboutDeliveryServiceOrderCreationBySms(Deal $deal, DeliveryTrackingNumber $deliveryTrackingNumber)
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getDeliveryServiceOrderCreationSmsMessage($deal, $deliveryTrackingNumber->getTrackingNumber());
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * Sms уведомление для продовца о создании трекер номера
     *
     * @param Deal $deal
     * @throws \Exception
     */
    private function notifyContractorAboutDeliveryServiceOrderCreationBySms(Deal $deal, DeliveryTrackingNumber $deliveryTrackingNumber)
    {
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getDeliveryServiceOrderCreationSmsMessage($deal, $deliveryTrackingNumber->getTrackingNumber());
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function notifyDealAgentsAboutDeliveryConfirmed(Deal $deal)
    {
        if ($this->email_notify_delivery_confirm) {
            try {
                $this->notifyCustomerAboutDeliveryConfirmedByEmail($deal);
                $this->notifyContractorAboutDeliveryConfirmedByEmail($deal);
            }
            catch (\Throwable $t) {

                logException($t, 'email-error');
            }
        }
        if ($this->sms_notify_delivery_confirm) {
            try {
                $this->notifyCustomerAboutDeliveryConfirmedBySms($deal);
                $this->notifyContractorAboutDeliveryConfirmedBySms($deal);
            }
            catch (\Throwable $t) {

                logException($t, 'sms-error');
            }
        }

        return true;
    }

    /**
     * Email уведомление для покупателя о подтверждении доставки
     *
     * @param Deal $deal
     * @throws \Exception
     */
    public function notifyCustomerAboutDeliveryConfirmedByEmail(Deal $deal)
    {
        /**
         * @var DealAgent $currentAgent
         * @var DealAgent $authorRequest
         */
        $customer = $deal->getCustomer();
        $contractor = $deal->getContractor();

        $data = [
            'deal' => $deal,
            'customer_login' => $customer->getUser()->getLogin(),
            'contractor_login' => $contractor->getUser()->getLogin(),
            DealNotificationSender::TYPE_NOTIFY_KEY => DealNotificationSender::TYPE_NOTIFY_DELIVERY_CONFIRM_CUSTOMER,
        ];

        $this->dealNotificationSender->sendMail($customer->getEmail(), $data);
    }

    /**
     * Email уведомление для продовца о подтверждении доставки
     *
     * @param Deal $deal
     * @throws \Exception
     */
    public function notifyContractorAboutDeliveryConfirmedByEmail(Deal $deal)
    {
        /**
         * @var DealAgent $currentAgent
         * @var DealAgent $authorRequest
         */
        $customer = $deal->getCustomer();
        $contractor = $deal->getContractor();

        $data = [
            'deal' => $deal,
            'customer_login' => $customer->getUser()->getLogin(),
            'contractor_login' => $contractor->getUser()->getLogin(),
            DealNotificationSender::TYPE_NOTIFY_KEY => DealNotificationSender::TYPE_NOTIFY_DELIVERY_CONFIRM_CONTRACTOR,
        ];

        $this->dealNotificationSender->sendMail($contractor->getEmail(), $data);
    }

    /**
     * Sms уведомление для покупателя о подтверждении доставки
     *
     * @param Deal $deal
     * @throws \Exception
     */
    public function notifyCustomerAboutDeliveryConfirmedBySms(Deal $deal)
    {
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();

        $userPhone = $customer->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getDeliveryConfirmedForCustomerSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * Sms уведомление для продовца о подтверждении доставки
     *
     * @param Deal $deal
     * @throws \Exception
     */
    public function notifyContractorAboutDeliveryConfirmedBySms(Deal $deal)
    {
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        $userPhone = $contractor->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getDeliveryConfirmedForContractorSmsMessage($deal);
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param Deal $deal
     * @return bool
     */
    public function isDeliveryConfirmed(Deal $deal)
    {
        $delivery = $this->entityManager->getRepository(Delivery::class)->findBy([
            'deal' => $deal,
            'deliveryStatus' => true,
        ]);

        return \count($delivery) ? true: false;
    }

    /**
     * @param Deal $deal
     * @return array
     */
    public function getDeliveryConfirmed(Deal $deal)
    {
        $delivery = $this->entityManager->getRepository(Delivery::class)->findBy([
            'deal' => $deal,
            'deliveryStatus' => true,
        ]);

        return (count($delivery) > 0) ? $delivery[0]: null;
    }
}