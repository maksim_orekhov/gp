<?php
namespace Application\Service;

/**
 * Class BankManager
 * @package Application\Service
 */
class BankManager
{
    use \Core\Provider\HttpRequestTrait;

    /**
     * @see http://www.cbr.ru/scripts/Root.asp?PrtId=WSCO
     */

    /**
     * @var string
     */
    private $url = "http://www.cbr.ru/CreditInfoWebServ/CreditOrgInfo.asmx?WSDL";

    /**
     * @param string$bic
     * @return bool|array [
     *  'RegNumber',
     *  'BIC',
     *  'OrgName',
     *  'OrgFullName',
     *  'phones',
     *  'DateKGRRegistration',
     *  'MainRegNumber',
     *  'MainDateReg',
     *  'UstavAdr',
     *  'FactAdr',
     *  'UstMoney',
     *  'OrgStatus',
     *  'RegCode',
     *  'SSV_Date',
     *  'LCode',
     *  'LT',
     *  'LDate',
     * ]
     */
    public function searchBank($bic)
    {
        $result = false;
        $code = $this->doRequest('BicToIntCode', [
            'BicCode' => $bic,
        ]);

        if ($code !== false) {
            $result = $this->doRequest('creditInfoByIntCodeXML', [
                'InternalCode' => $code,
            ]);
        }

        return $result;
    }

    /**
     * @param string $method
     * @param array $arguments
     * @return mixed
     */
    public function doRequest($method, $arguments)
    {
        $result = $this->zendSoapRequest($this->url, $method, $arguments);

        return $this->prepareResult($result);
    }

    /**
     * @param mixed $data
     * @return mixed array or one var
     */
    private function prepareResult($data)
    {
        $result = $data;

        if ($result === -1) {
            $result = false;
        }

        if (isset($result['any'])) {
            $result = $result['any'];
        }

        if (strpos($result,'</') !== false) {
            $result = $this->xmlToArray($result);
        }

        if (is_array($result) && count($result) === 0) {
            $result = false;
        }

        return $result;
    }

    /**
     * @param string $xml
     * @return array
     */
    private function xmlToArray($xml)
    {
        preg_match_all('/<(.*?)>([^<]+)<\/\1>/i', $xml, $match);
        $result = array();
        foreach ($match[1] as $x => $y)
        {
            $result[$y] = $match[2][$x];
        }

        return $result;
    }
}