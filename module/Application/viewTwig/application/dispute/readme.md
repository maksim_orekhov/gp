# Dispute templates
### список переменных
1. dispute

### описание переменных
1\) dispute:
```
dispute [
    'id' => int 12
    'reason' => string 'Контрактор козёл!'
    'author' => string 'test'
    'created' => string '18.03.2018'
    'expected_close_date' => string '02.04.2018'
    'warranty_extension_days' => int 5 (если нет продления, то 0)
    'is_close' => boolean true
    'discount' =>     
        'id' => int 6
        'created' => string '01.03.2018' (length=10)
        'amount' => float 100
    'refund' =>        
        'id' => int 6
        'created' => string '01.03.2018' (length=10)
        'amount' => float 100
    'warranty_extensions' => 
        0 => 
            array (size=3)
              'id' => int 1
              'created' => string '20.03.2018'
              'days' => int 4
            и т.д.
        
    Минимальный набор информации о сделке (для формирования ссылки, например)
    'deal_id' => int 25
    'deal_name' => string 'Сделка Фикстура' (length=29)
    'deal_number' => string '#GP25U' (length=6)
  
    'files' => 
        0 => 
            'id' => int 4
            'name' => string 'e6a0cee5c9501835472f2d8dd38089c7.jpg'
            'origin_name' => string 'гоголь.jpg'
            'type' => string 'image/jpeg'
            'path' => string '/dispute-files'
            'size' => int 122691
        1 => и т.д.
]
```