# Deal templates
### список переменных
1. deal
1. deals
1. dealFilterForm
1. is_operator
1. paginator
1. confirmConditionsForm
1. dealForm
1. allowedCivilLawSubjectForm
1. is_form_natural_person
1. is_form_legal_entity
1. naturalPersonForm 
1. legalEntityForm
1. allowedPaymentMethodForm
1. bankTransferForm
1. cachForm
1. creditCardForm
1. emoneyForm
1. is_form_bank_transfer
1. is_form_cach
1. is_form_credit_card
1. is_form_emoney
1. trackingNumberForm
### описание переменных
1\) deal: array (показаны ключи со значениями из тестовой сделки):
```
deal [
      'id' => int 1
      'number' = string 'GP1U'
      'name' => string 'Сделка Фикстура'
      'full_name' => string 'Сделка «Сделка Фикстура» №1'
      'status' => ['code' => 20, 'status' => 'delivered', 'name' => 'Товар доставлен']
      'type' => string 'Услуга'
      'addon_terms' => string 'Дополнительные условия'
      'fee_payer_option' => string 'Покупатель'
      'amount_without_fee' => float 100000
      'customer_amount' => float 104000
      'customer_fee' => float 4000
      'contractor_amount' => float 100000
      'contractor_fee' => float 0
      'created' => string '03.02.2018'
      'de_facto_date' => null
      'delivery_period' => int 9
      'date_of_deadline' => null
      'date_credit_order' => 12.03.2018
      'date_of_confirm' => 12.03.2018
      'date_closed_deal' => 12.03.2018
      'planned_date' => null
      'is_customer' => boolean true
      'customer' =>
          'name' => null
          'address' => null
          'phone' => string '+79033463906'
          'inn' => null
          'kpp' => null
          'ogrn' => null
          'first_name' => string 'Тест'
          'secondary_name' => string 'Тестович'
          'last_name' => string 'Тестов' (length=12)
          'fio' => string 'Тестов Тест Тестович'
          'birth_date' => string '31.12.1980'
          'email' => string 'test@simple-technology.ru'
          'is_legal_entity' => boolean false
          'is_natural_person' => boolean true
      '_is_contractor_' => boolean false
      'contractor' =>
          'name' => null
          'address' => null
          'phone' => string '+79033463907'
          'inn' => null
          'kpp' => null
          'ogrn' => null
          'first_name' => string 'Контрагент'
          'secondary_name' => string 'Контрагентович'
          'last_name' => string 'Контрагентов' (length=24)
          'fio' => string 'Контрагентов Контрагент Контрагентович'
          'birth_date' => string '11.10.1985'
          'email' => string 'test@simple-technology.ru'
          'is_legal_entity' => boolean false
          'is_natural_person' => boolean true
      'bill' =>
          'name' => string 'Счет на оплату договора №1'
      'contract' => // До заключения сделки - пустой
          'name' => // string
          'url' => // string
      'is_delivery_confirmed' => boolean false
      'is_owner' => boolean true
      'removable' => boolean true
      'editable' => boolean true
      'is_current_user_confirmed' => boolean false
      'is_counteragent_confirmed' => boolean false
      'is_need_to_fill_deal' => boolean false
      'is_need_to_fill_civil_law_subject' => boolean false
      'is_need_to_fill_payment_method' => boolean false
      'payment_method_requisites' =>
        'id' => int 182
        'name' => string 'Счет БинБанк'
        'bik' => string '044525213'
        'account_number' => string '40702810090030001140'
        'payment_recipient_name' => string 'Контрагент Контрагентов'
        'inn' => string '683103646744'
        'kpp' => string '781001009'
        'corr_account_number' => string '30101810900000000793'
        'bank_name' => string 'БинБанк'
      'files' => 
          0 => 
              'name' => string '2bda4144d557e0f6e764b6adbc95470f.jpg'
              'origin_name' => string 'Ходченкова.jpg'
              'type' => string 'image/jpeg'
              'path' => string '/deal'
              'size' => int 80436
          1 => 
              'name' => string '7692cc816c1d3be5c99b6b452cb36aa5.jpg'
              'origin_name' => string 'Петров.jpg'
              'type' => string 'image/jpeg'
              'path' => string '/deal-files'
              'size' => int 185929
          и т.д.

      'is_deal_has_dispute' => boolean false
      'dispute' => 
            'id' => int 3
            'reason' => string 'Контрарктор Контрактович охренел!'
            'created' => string '20.03.2018'
            'author' => string 'test'
            'expected_close_date' => string '07.04.2018'
            'is_close' => boolean false
            'discount' => null
            'refund' => null
            'warranty_extensions' => 
              array (size=1)
                0 => 
                  array (size=3)
                    ...
            'files' => 
              array (size=1)
                0 => 
                  array (size=6)
                    ...
            'deal_id' => int 25
            'deal_name' => string 'Сделка Фикстура' (length=29)
            'deal_number' => string '#GP25U' (length=6)
```
2\) deals: array (показаны ключи со значениями из тестовой сделки):
```
deals [
    0 => [
        ['id']
        ['number']
        ['name']
        ['deal_status']
        ['type']
        ['fee_payer_option']
        ['delivery_period']
        ['addon_terms']
        ['payment_amount']
        ['payment_amount_without_fee']
        ['created']
        ['customer']
        ['customer_deal_agent_email']
        ['contractor']
        ['contractor_deal_agent_email']
        ['bill']
            ['bill']['name']
        ['contract']
            ['contract']['name']
            ['contract']['url']
        ['user_role'] = null;
        ['agent_confirm']
        ['counteragent_confirm']
        ['user_role']
        ['agent_confirm']
        ['counteragent_confirm']
        ['is_delivery_confirmed']
        ['owner']
        ['debit']
             ['total_amount']
             ['matched']
             ['overpayment']
             ['underpayment']
             ['number_of_payments']
        ],
    1 => [],
    ...
    и т.д.
    ]
```
3\) dealFilterForm: объект формы
```
dealFilterForm => {
        filter[name] (text)
        filter[deal_amount] (text)
        filter[deal_role] (select)
        filter[deal_created_from] (text)
        filter[deal_created_till] (text)
        filter[deal_status] (select) - на бэке функционала для этого параметра еще нет
        csrf
    }
```
4\) paginator: object
```
paginator => {
  'pageCount' => int 1
  'itemCountPerPage' => int 20
  'first' => int 1
  'current' => int 1
  'last' => int 1
  'pagesInRange' => [
      1 => int 1
   ]
  'firstPageInRange' => int 1
  'lastPageInRange' => int 1
  'currentItemCount' => int 3
  'totalItemCount' => int 3
  'firstItemNumber' => int 1
  'lastItemNumber' => int 3
}
```
5\) is_operator : boolean
```
 is_operator = true or false
```
6\) confirmConditionsForm : объект формы
```
confirmConditionsForm {
   'confirm',
   'csrf'
}
```
7\) dealForm : объект формы
```
dealForm => {
    'name'
    'amount'
    'deal_role'
    'deal_civil_law_subject'
    'payment_method'
    'counteragent_email'
    'deal_type'
    'fee_payer_option'
    'addon_terms'
    'delivery_period'
    'deal_agent_confirm'
    'csrf'
    'submit'
}
```
8\) allowedCivilLawSubjectForm  : объект формы
```
allowedCivilLawSubjectForm {
    civil_law_subject_id
}
```
9\) is_form_natural_person : boolean
```
 is_form_natural_person = true or false
```
10\) is_form_legal_entity : boolean
```
 is_form_legal_entity = true or false
```
11\) naturalPersonForm : объект формы
 ```
naturalPersonForm => {
    'first_name'
    'secondary_name'
    'last_name'
    'birth_date'
    'csrf'
    'submit'
}
 ```
12\) is_form_legal_entity : объект формы
 ```
legalEntityForm => {
    'name'
    'legal_address'
    'phone'
    'inn'
    'kpp'
    'ogrn'
    'nds_type_id'
    'csrf'
    'submit'
}
 ```
 13\) allowedPaymentMethodForm : объект формы
```
allowedPaymentMethodForm => {
    payment_method_id
}
```
 14\) bankTransferForm : объект формы
```
bankTransferForm => {
    'civil_law_subject_id' (type 'hidden')
    'payment_method_type' (type 'hidden')
    'name'
    'bank_name'
    'bik'
    'inn'
    'kpp'
    'account_number'
    'corr_account_number'
    'payment_recipient_name'
    'csrf' (type 'hidden')
    'submit'
}
```
15\) cachForm : объект формы
```
cachForm => null - еще в проекте
```
16\) creditCardForm : объект формы
```
creditCardForm => null - еще в проекте
```
17\) emoneyForm : объект формы
```
emoneyForm => null - еще в проекте
```
18\) is_form_bank_transfer : boolean
```
is_form_bank_transfer = true or false
```
19\) is_form_cach : boolean
```
is_form_cach = true or false
```
20\) is_form_credit_card : boolean
```
is_form_credit_card = true or false
```
21\) is_form_emoney : boolean
```
is_form_emoney = true or false
```
22\) trackingNumberForm : объект формы
```
trackingNumberForm => {
    'tracking_number'
    'csrf',
    'submit'
}
```
