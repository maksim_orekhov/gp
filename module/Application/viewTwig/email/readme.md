# Email templates

### описание переменных
1\) tribunal:  
```
tribunal [
    'id' => int 12
    'created' => string '18.03.2018 07.23'    
]
```
2\) discount:
```
discount [
    'id' => int 12
    'created' => string '18.03.2018 07.23' 
    'amount' => string '1000' 
]
```
3\) refund:
```
refund [
    'id' => int 12
    'created' => string '18.03.2018 07.23' 
    'amount' => string '1000' 
]
```
4\) deal:
```
deal [
    'id' => int 12
    'number' => string '#GP12U' 
    'name' => string 'Сделка Фикстура'
    'type' => string 'Услуга'
    'addon_terms' => string 'Дополнительные условия'
    'fee' => float 2
    'fee_payer_option' => string 'CUSTOMER'
    'amount_without_fee' => float 10000
    'customer_amount' => float 10200
    'contractor_amount' => float 9800
    'delivery_period' => int 9
    'created' => string '18.03.2018'
    'confirm_url' => string 'https://dev.local/deal/12/confirm-conditions'
    'deal_url' => string 'https://dev.local/deal/show/12'
    'deal_url_edit' => string 'https://dev.local/deal/edit/12'
    'deal_close_date' => string '27.03.2018'
]
```
5\) dispute:
```
dispute [
    'id' => int 31
      'status' => 
        array (size=5)
          'code' => int 1
          'status' => string 'initial' (length=7)
          'priority' => int 1
          'is_blocker' => boolean false
          'name' => string 'Первичный' (length=18)
      'reason' => string 'Тестируем наличие спора' (length=44)
      'created' => string '21.05.2018' (length=10)
      'created_milliseconds' => int 1526888644
      'created_full' => 
        array (size=2)
          'date' => string '21.05.2018' (length=10)
          'time' => string '10:44:04' (length=8)
      'author' => string 'test' (length=4)
      'expected_close_date' => string '04.06.2018' (length=10)
      'expected_days_for_close' => int 2
      'deal_extension_days' => int 0
      'deal_extension_date' => null
      'is_close' => boolean false
      'discount' => null
      'refund' => null
      'tribunal' => null
      'warranty_extensions' => null
      'files' => 
        array (size=0)
          empty
      'arbitrage_requests' => null
      'tribunal_requests' => null
      'discount_requests' => null
      'refund_requests' => null
      'is_allowed_arbitrage_request' => boolean true
      'is_allowed_tribunal_request' => boolean true
      'is_allowed_discount_request' => boolean true
      'is_allowed_refund_request' => boolean true
      'is_allowed_arbitrage_confirm' => boolean false
      'is_allowed_tribunal_confirm' => boolean false
      'is_allowed_discount_confirm' => boolean false
      'is_allowed_refund_confirm' => boolean false
      'is_allow_close_dispute' => boolean true
      'deal_id' => int 65
      'deal_name' => string 'Сделка Спор открыт' (length=34)
      'deal_number' => string '#GP65U' (length=6)
      'deal_status' => 
        array (size=5)
          'code' => int 15
          'status' => string 'dispute' (length=7)
          'priority' => int 30
          'is_blocker' => boolean false
          'name' => string 'Спор' (length=8)
      'deal_type_ident' => string 'U' (length=1)
      'deal_closed_date_by_contract' => string '13.06.2018' (length=10)
      'deal_closed_date_by_warranty' => null
      'max_discount_amount' => float 100000
]
```
6\) extension_days:
```
extension_days  = int 9
```
7\) main_project_url:
```
main_project_url  = string 'https://dev.local/'
```
8\) contractor_login:
```
contractor_login  = string 'Тест Тестович'
```
9\) customer_login:
```
contractor_login  = string 'Контрактор Контрактович'
```
10\) user_login
```
user_login  = string 'login'
```
11\) subject
```
subject  = string 'subject' // тема письма
```
11\) email_confirmation_url
```
email_confirmation_url  = string 'https://dev.local/email/confirm?token=121212'
```
12\) email_confirmation_page
```
email_confirmation_page  = string 'https://dev.local/email/confirm'
```
12\) email_confirmation_token
```
email_confirmation_token  = string '121212121dfd34fdsf43fdf'
```
13\) qrCodeData
```
qrCodeData  = string 'https://dev.local/img/123.jpg' //<img src='{{ qrCodeData }}' />
```
14\) password_reset_url
```
password_reset_url  = string 'https://dev.local/forgot/password?token=121221'
```
14\) code
```
code  = int '12121'
```
14\) invitation_url
```
invitation_url  = string 'https://dev.local/register?token=121221fd3df4'
```
15\) invitation_token
```
invitation_token  = string '121221fd3df4'
```
16\) payment_receipt_type
```
payment_receipt_type  = string 'payment_part' // or payment_full : частичая или полная оплата
```
17\) tracking_number
```
tracking_number  = string 'ZF4444444RU'
```
18\) discountRequest
```
discountRequest  = [
    'id' => int 10
      'created' => string '04.06.2018 10:44' (length=16)
      'created_full' => 
        array (size=2)
          'date' => string '04.06.2018' (length=10)
          'time' => string '10.44' (length=5)
      'amount' => string '50000.00' (length=8)
      'is_author' => boolean true
      'author' => 
        array (size=3)
          'name' => string 'test' (length=4)
          'email' => string 'test@test.com' (length=19)
          'phone' => string '+79036553906' (length=12)
      'confirm' => 
        array (size=4)
          'id' => int 7
          'status' => boolean true
          'author' => 
            array (size=3)
              'name' => string 'test2' (length=5)
              'email' => string 'test2@test2.com' (length=20)
              'phone' => string '+79036553906' (length=12)
          'created' => string '04.06.2018 10:44' (length=16)
      'dispute' => 
        array (size=3)
          'id' => int 34
          'reason' => string 'Тестируем принятие запроса на Скидку Продавцом' (length=87)
          'created' => string '04.06.2018' (length=10)
      'deal' => 
        array (size=4)
          'id' => int 68
          'name' => string 'Сделка Предложение скидки принято' (length=63)
          'number' => string '#GP68U' (length=6)
          'amount_without_fee' => float 270000
]
```
19\) arbitrageRequest
```
arbitrageRequest  = [
    'id' => int 9
      'created' => string '04.06.2018 10:44' (length=16)
      'is_author' => boolean true
      'author' => 
        array (size=3)
          'name' => string 'test' (length=4)
          'email' => string 'test@test.com' (length=19)
          'phone' => string '+71111111111' (length=12)
      'confirm' => 
        array (size=4)
          'id' => int 6
          'status' => boolean true
          'author' => 
            array (size=3)
              'name' => string 'operator' (length=8)
              'email' => string 'operator@operator.com' (length=23)
              'phone' => string '+71111111111' (length=12)
          'created' => string '04.06.2018 10:44' (length=16)
]
```
20\) tribunalRequest
```
tribunalRequest  = [
    'id' => int 11
      'created' => string '04.06.2018 10:44' (length=16)
      'is_author' => boolean false
      'author' => 
        array (size=3)
          'name' => string 'test2' (length=5)
          'email' => string 'test2@test2.com' (length=20)
          'phone' => string '+71111111111' (length=12)
      'confirm' => 
        array (size=4)
          'id' => int 8
          'status' => boolean true
          'author' => 
            array (size=3)
              'name' => string 'operator' (length=8)
              'email' => string 'operator@operator.com' (length=23)
              'phone' => string '+71111111111' (length=12)
          'created' => string '04.06.2018 10:44' (length=16)
]
```