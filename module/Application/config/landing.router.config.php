<?php
namespace Application;

use Zend\Router\Http\Literal;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action' => 'landing',
                    ],
                ],
            ],
            'landing-feedback' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/landing-feedback',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action' => 'landingFeedback',
                    ],
                ],
                'strategies' => [
                    'ViewJsonStrategy',
                ],
            ],
            'integration-request-sending' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/integration-request-sending',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action' => 'integrationRequestSending',
                    ],
                ],
                'strategies' => [
                    'ViewJsonStrategy',
                ],
            ],
            'policy' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/policy',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'policy',
                    ],
                ],
            ],
            'connection-contract' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/connection-contract',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'connectionContract',
                    ],
                ],
            ],
            'terms-of-use' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/terms-of-use',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'termsOfUse',
                    ],
                ],
            ],
            'how-it-works' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/how-it-works',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'howItWorks',
                    ],
                ],
            ],
            'for-whom' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/for-whom',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'forWhom',
                    ],
                ],
            ],
            'dispute-consideration-rules' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/dispute-consideration-rules',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'disputeConsiderationRules',
                    ],
                ],
            ],
            'what-can-buy' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/what-can-buy',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'whatCanBuy',
                    ],
                ],
            ],
            'dispute-resolution' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/dispute-resolution',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'disputeResolution',
                    ],
                ],
            ],
            'contacts' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/contacts',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'contacts',
                    ],
                ],
            ],
            'for-customers' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/for-customers',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'forCustomers',
                    ],
                ],
            ],
            'for-contractors' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/for-contractors',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'forContractors',
                    ],
                ],
            ],
            'for-business' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/for-business',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'forBusiness',
                    ],
                ],
            ],
            'e-commerce' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/e-commerce',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'eCommerce',
                    ],
                ],
            ],
            'for-partners' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/for-partners',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'forPartners',
                    ],
                ],
            ],
            'information' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/information',
                    'defaults' => [
                        'controller' => Controller\LandingController::class,
                        'action'     => 'information',
                    ],
                ],
            ],
        ],
    ],
];