<?php
namespace Application;

use Application\EventManager\ApplicationEventProvider;
use Application\EventManager\DealEventProvider;
use Application\Listener\ApplicationListenerAggregate;
use Application\Listener\DealListenerAggregate;
use Application\Listener\Notification\DealNotificationListener;
use Application\Listener\AuthListenerAggregate;
use Application\Listener\NotificationListenerAggregate;
use Core\EventManager\AuthEventProvider;
use Core\EventManager\NotifyEventProvider;
use ModuleDelivery\EventManager\DeliveryEventProvider;

return [
    // Navigation
    'navigation' => [
        'default' => [
            [
                'label' => 'Home',
                'route' => 'home',
            ],
            [
                'label' => 'User Registration',
                'route' => 'register',
            ],
        ],
    ],
    'event_provider' => [
        //provider class => listeners
        AuthEventProvider::class => [
            //listener class => priority
            AuthListenerAggregate::class => 1,
        ],
        //provider class => listeners
        DealEventProvider::class => [
            //listener class => priority
            DealListenerAggregate::class => 9999,
            DealNotificationListener::class => 1,

        ],
        //provider class => listeners
        NotifyEventProvider::class => [
            //listener class => priority
            NotificationListenerAggregate::class => 9999,
        ],
        ApplicationEventProvider::class => [
            //listener class => priority
            ApplicationListenerAggregate::class => 1,
        ],
    ],
];
