<?php
namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

//
//*** NaturalPerson route blocks *** //
//
$natural_person = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/natural-person',
        'defaults' => [
            'controller' => Controller\NaturalPersonController::class,
        ],
    ]
];
$natural_person_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person/:id',
        'constraints' => [
            'id'     => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonController::class,
        ],
    ]
];
$natural_person_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/natural-person/form-init',
        'defaults' => [
            'controller' => Controller\NaturalPersonController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$natural_person_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/natural-person/create',
        'defaults' => [
            'controller' => Controller\NaturalPersonController::class,
            'action' => 'createForm',
        ],
    ],
];
$natural_person_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person/:idNaturalPerson/edit',
        'constraints' => [
            'idNaturalPerson' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonController::class,
            'action' => 'editForm',
        ],
    ],
];
$natural_person_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person/:idNaturalPerson/delete',
        'constraints' => [
            'idNaturalPerson' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonController::class,
            'action' => 'deleteForm',
        ],
    ]
];
//
//*** SoleProprietor route blocks *** //
//
$sole_proprietor = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/sole-proprietor',
        'defaults' => [
            'controller' => Controller\SoleProprietorController::class,
        ],
    ]
];
$sole_proprietor_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/sole-proprietor/:id',
        'constraints' => [
            'id'     => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\SoleProprietorController::class,
        ],
    ]
];
$sole_proprietor_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/sole-proprietor/form-init',
        'defaults' => [
            'controller' => Controller\SoleProprietorController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$sole_proprietor_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/sole-proprietor/create',
        'defaults' => [
            'controller' => Controller\SoleProprietorController::class,
            'action' => 'createForm',
        ],
    ],
];
$sole_proprietor_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/sole-proprietor/:idSoleProprietor/edit',
        'constraints' => [
            'idSoleProprietor' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\SoleProprietorController::class,
            'action' => 'editForm',
        ],
    ],
];
$sole_proprietor_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/sole-proprietor/:idSoleProprietor/delete',
        'constraints' => [
            'idSoleProprietor' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\SoleProprietorController::class,
            'action' => 'deleteForm',
        ],
    ]
];
//
//*** Legal entity route blocks *** //
//
$legal_entity = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/legal-entity',
        'defaults' => [
            'controller' => Controller\LegalEntityController::class,
        ],
    ]
];
$legal_entity_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity/:id',
        'constraints' => [
            'id'     => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityController::class,
        ],
    ]
];
$legal_entity_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/legal-entity/form-init',
        'defaults' => [
            'controller' => Controller\LegalEntityController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$legal_entity_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/legal-entity/create',
        'defaults' => [
            'controller' => Controller\LegalEntityController::class,
            'action' => 'createForm',
        ],
    ],
];
$legal_entity_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity/:idLegalEntity/edit',
        'constraints' => [
            'idLegalEntity' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityController::class,
            'action' => 'editForm',
        ],
    ],
];
$legal_entity_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity/:idLegalEntity/delete',
        'constraints' => [
            'idLegalEntity' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityController::class,
            'action' => 'deleteForm',
        ],
    ]
];
//
//*** PaymentMethod route blocks *** //
//
$payment_method = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/payment-method',
        'defaults' => [
            'controller' => Controller\PaymentMethodController::class,
        ],
    ]
];
$payment_method_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/payment-method/:id',
        'constraints' => [
            'id'     => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\PaymentMethodController::class,
        ],
    ]
];
$payment_method_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/payment-method/form-init',
        'defaults' => [
            'controller' => Controller\PaymentMethodController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$payment_method_form_create = [
    'type'    => 'Segment',
    'options' => array(
        'route'    => '[/civil-law-subject/:civil_law_subject_id]/payment-method[/:payment_method_type]/create',
        'constraints' => array(
            'civil_law_subject_id' => '\d+',
            'payment_method_type' => '[a-z_]*',
        ),
        'defaults' => array(
            'controller' => Controller\PaymentMethodController::class,
            'action' => 'createForm',
        ),
    )
];
$payment_method_form_create_with_deal = [
    'type'    => 'Segment',
    'options' => array(
        'route'    => '[/deal/:idDeal]/payment-method/create',
        'constraints' => array(
            'idDeal' => '\d+',
        ),
        'defaults' => array(
            'payment_method_type' => 'bank_transfer',
            'controller' => Controller\PaymentMethodController::class,
            'action' => 'createForm'
        ),
    )
];
$payment_method_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/payment-method/:idPaymentMethod/edit',
        'constraints' => [
            'idPaymentMethod' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\PaymentMethodController::class,
            'action' => 'editForm',
        ],
    ]
];
$payment_method_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/payment-method/:idPaymentMethod/delete',
        'constraints' => [
            'idPaymentMethod' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\PaymentMethodController::class,
            'action' => 'deleteForm',
        ],
    ]
];
//
//*** Natural person driver license route blocks *** //
//
$natural_person_driver_license = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person-driver-license',
        'defaults' => [
            'controller' => Controller\NaturalPersonDriverLicenseController::class,
        ],
    ],
];
$natural_person_driver_license_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person-driver-license/:id',
        'constraints' => [
            'id'     => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonDriverLicenseController::class,
        ],
    ]
];
$natural_person_driver_license_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/natural-person-driver-license/form-init',
        'defaults' => [
            'controller' => Controller\NaturalPersonDriverLicenseController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$natural_person_driver_license_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/natural-person/:idNaturalPerson/driver-license/create',
        'constraints' => [
            'idNaturalPerson' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonDriverLicenseController::class,
            'action' => 'createForm',
        ],
    ],
];
$natural_person_driver_license_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person-driver-license/:idDriverLicense/edit',
        'constraints' => [
            'idDriverLicense' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonDriverLicenseController::class,
            'action' => 'editForm',
        ],
    ]
];
$natural_person_driver_license_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person-driver-license/:idDriverLicense/delete',
        'constraints' => [
            'idDriverLicense' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonDriverLicenseController::class,
            'action' => 'deleteForm',
        ],
    ]
];
//
//*** Natural person passport route blocks *** //
//
$natural_person_passport = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person-passport',
        'defaults' => [
            'controller' => Controller\NaturalPersonPassportController::class,
        ],
    ],
];
$natural_person_passport_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person-passport/:id',
        'constraints' => [
            'id'     => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonPassportController::class,
        ],
    ]
];
$natural_person_passport_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/natural-person-passport/form-init',
        'defaults' => [
            'controller' => Controller\NaturalPersonPassportController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$natural_person_passport_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/natural-person/:idNaturalPerson/passport/create',
        'constraints' => [
            'idNaturalPerson' => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonPassportController::class,
            'action' => 'createForm',
        ],
    ],
];
$natural_person_passport_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person-passport/:idPassport/edit',
        'constraints' => [
            'idPassport' => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonPassportController::class,
            'action' => 'editForm',
        ],
    ]
];
$natural_person_passport_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/natural-person-passport/:idPassport/delete',
        'constraints' => [
            'idPassport' => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\NaturalPersonPassportController::class,
            'action' => 'deleteForm',
        ],
    ]
];
//
//*** Legal entity tax inspection route blocks *** //
//
$legal_entity_bank_detail = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity-bank-detail',
        'defaults' => [
            'controller' => Controller\LegalEntityBankDetailController::class,
        ],
    ],
];
$legal_entity_bank_detail_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity-bank-detail/:id',
        'constraints' => [
            'id' => '\d+',
            'profile' => 'profile'
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityBankDetailController::class,
        ],
    ]
];
$legal_entity_bank_detail_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/legal-entity-bank-detail/form-init',
        'defaults' => [
            'controller' => Controller\LegalEntityBankDetailController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$legal_entity_bank_detail_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/legal-entity/:idLegalEntity/bank-detail/create',
        'constraints' => [
            'idLegalEntity' => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityBankDetailController::class,
            'action' => 'createForm',
        ],
    ],
];
$legal_entity_bank_detail_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity-bank-detail/:idBankDetail/edit',
        'constraints' => [
            'idBankDetail' => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityBankDetailController::class,
            'action' => 'editForm',
        ],
    ]
];
$legal_entity_bank_detail_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity-bank-detail/:idBankDetail/delete',
        'constraints' => [
            'idBankDetail' => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityBankDetailController::class,
            'action' => 'deleteForm',
        ],
    ]
];
//
//*** Legal entity tax inspection route blocks *** //
//
$legal_entity_tax_inspection = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity-tax-inspection',
        'defaults' => [
            'controller' => Controller\LegalEntityTaxInspectionController::class,
        ],
    ]
];
$legal_entity_tax_inspection_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity-tax-inspection/:id',
        'constraints' => [
            'id'     => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityTaxInspectionController::class,
        ],
    ]
];
$legal_entity_tax_inspection_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/legal-entity-tax-inspection/form-init',
        'defaults' => [
            'controller' => Controller\LegalEntityTaxInspectionController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$legal_entity_tax_inspection_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/legal-entity/:idLegalEntity/tax-inspection/create',
        'constraints' => [
            'idLegalEntity' => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityTaxInspectionController::class,
            'action' => 'createForm',
        ],
    ],
];
$legal_entity_tax_inspection_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity-tax-inspection/:idTaxInspection/edit',
        'constraints' => [
            'idTaxInspection' => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityTaxInspectionController::class,
            'action' => 'editForm',
        ],
    ]
];
$legal_entity_tax_inspection_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/legal-entity-tax-inspection/:idTaxInspection/delete',
        'constraints' => [
            'idTaxInspection' => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\LegalEntityTaxInspectionController::class,
            'action' => 'deleteForm',
        ],
    ]
];
//
//*** Natural Person Document ***//
//
$natural_person_document = [
    'type'    => Literal::class,
    'options' => [
        'route'    => '/natural-person-document',
        'defaults' => [
            'controller' => Controller\NaturalPersonDocumentController::class,
        ],
    ]
];
//
//*** Legal Entity Document ***//
//
$legal_entity_document = [
    'type'    => Literal::class,
    'options' => [
        'route'    => '/legal-entity-document',
        'defaults' => [
            'controller' => Controller\LegalEntityDocumentController::class,
        ],
    ]
];

//
//*** Deal Comment ***//
//
$deal_comment = [ // переделать!
    'type'    => Segment::class,
    'options' => [
        'route'    => '/deal-comment',
        'defaults' => [
            'controller' => Controller\DealController::class,
            'action' => 'comments',
        ],
    ]
];
$deal_comment_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/deal-comment/:id',
        'constraints' => [
            'id'     => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\DealCommentController::class,
        ],
    ]
];
$deal_comment_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/deal-comment/form-init',
        'defaults' => [
            'controller' => Controller\DealCommentController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$deal_comment_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/deal-comment/create',
        'defaults' => [
            'controller' => Controller\DealCommentController::class,
            'action' => 'createForm',
        ],
    ],
];
$deal_comment_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/deal-comment/:idComment/edit',
        'constraints' => [
            'idComment' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DealCommentController::class,
            'action' => 'editForm',
        ],
    ],
];
$deal_comment_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/deal-comment/:idComment/delete',
        'constraints' => [
            'idComment' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DealCommentController::class,
            'action' => 'deleteForm',
        ],
    ]
];

//
//*** Deal Deadline ***//
//

$deal_deadline_notify = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/deal-deadline/notify',
        'defaults' => [
            'controller' => Controller\DealDeadlineController::class,
            'action' => 'dealDeadline',
        ],
    ]
];

//
//*** Dispute ***//
//
$dispute = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/dispute',
        'defaults' => [
            'controller' => Controller\DisputeController::class,
        ],
    ]
];
$dispute_open_list = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/dispute/open',
        'defaults' => [
            'controller' => Controller\DisputeController::class,
            'action' => 'openList',
        ],
    ],
];
$dispute_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/dispute/:id',
        'constraints' => [
            'id'     => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\DisputeController::class,
        ],
    ]
];
$dispute_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/dispute/form-init',
        'defaults' => [
            'controller' => Controller\DisputeController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$dispute_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/dispute/create',
        'defaults' => [
            'controller' => Controller\DisputeController::class,
            'action' => 'createForm',
        ],
    ],
];
/* Редактировать спор нельзя!
$dispute_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/dispute/:idDispute/edit',
        'constraints' => [
            'idDispute' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DisputeController::class,
            'action' => 'editForm',
        ],
    ],
];
/* Удалить спор нельзя!
$dispute_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/dispute/:idDispute/delete',
        'constraints' => [
            'idDispute' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DisputeController::class,
            'action' => 'deleteForm',
        ],
    ]
];
*/
$dispute_form_close = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/dispute/:idDispute/close',
        'constraints' => [
            'idDispute' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DisputeController::class,
            'action' => 'closeForm',
        ],
    ],
];
$dispute_form_open = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/dispute/:idDispute/open',
        'constraints' => [
            'idDispute' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DisputeController::class,
            'action' => 'openForm',
        ],
    ],
];
$dispute_file_upload = [
    'type' => 'Segment',
    'options' => [
        'route'    => '/dispute/file-upload',
        'defaults' => [
            'controller' => Controller\DisputeController::class,
            'action' => 'fileUpload',
        ],
    ],
];
$dispute_file = [ // Нужен ли этот роут? Может быть, напрямую к роуту FileManager'а обращаться
    'type' => Segment::class,
    'options' => [
        'route'    => '/dispute/:disputeId/file/:fileId',
        'constraints' => [
            'disputeId' => '\d+',
            'fileId' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DisputeController::class,
            'action' => 'showFile',
        ],
    ],
];

//
//*** Dispute Operator Comment ***//
//
$dispute_operator_comment = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/dispute/operator-comment',
        'defaults' => [
            'controller' => Controller\DisputeOperatorCommentController::class,
        ],
    ]
];
$dispute_operator_comment_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/dispute/operator-comment/:id',
        'constraints' => [
            'id'     => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\DisputeOperatorCommentController::class,
        ],
    ]
];
$dispute_operator_comment_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/dispute/operator-comment/form-init',
        'defaults' => [
            'controller' => Controller\DisputeOperatorCommentController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$dispute_operator_comment_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/dispute/:idDispute/operator-comment/create',
        'constraints' => [
            'idDispute' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DisputeOperatorCommentController::class,
            'action' => 'createForm',
        ],
    ],
];
$dispute_operator_comment_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/dispute/operator-comment/:idComment/edit',
        'constraints' => [
            'idComment' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DisputeOperatorCommentController::class,
            'action' => 'editForm',
        ],
    ],
];
$dispute_operator_comment_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/dispute/operator-comment/:idComment/delete',
        'constraints' => [
            'idComment' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DisputeOperatorCommentController::class,
            'action' => 'deleteForm',
        ],
    ]
];
$dispute_refund = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/dispute/:idDispute/refund',
        'constraints' => [
            'idDispute' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\RefundController::class,
            'action' => 'refund',
        ],
    ]
];

//Discount
$dispute_discount = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/discount',
        'defaults' => [
            'controller' => Controller\DiscountController::class,
        ],
        #'verb' => 'post' // only POST!
    ]
];
$deal_discount_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/discount/:id',
        'constraints' => [
            'id' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DiscountController::class,
        ],
        'verb' => 'get' // only GET!
    ]
];
$dispute_discount_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/discount/form-init',
        'defaults' => [
            'controller' => Controller\DiscountController::class,
            'action' => 'formInit',
        ],
    ]
];
$dispute_discount_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/dispute/:idDispute/discount/create',
        'constraints' => [
            'idDispute' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DiscountController::class,
            'action' => 'createForm',
        ],
    ],
];

//Discount-request
$deal_discount_request = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/discount/request',
        'defaults' => [
            'controller' => Controller\DiscountRequestController::class
        ],
    ]
];
$deal_discount_request_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/discount/request/:id',
        'constraints' => [
            'id' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DiscountRequestController::class,
        ],
        'verb' => 'get' // only GET!
    ]
];
$deal_discount_request_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/discount/request/form-init',
        'defaults' => [
            'controller' => Controller\DiscountRequestController::class,
            'action' => 'formInit',
        ],
    ]
];
$deal_discount_request_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/dispute/:idDispute/discount/request/create',
        'constraints' => [
            'idDispute' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DiscountRequestController::class,
            'action' => 'createForm',
        ],
    ],
];
$deal_discount_confirm = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/deal/discount/request/:idRequest/confirm',
        'constraints' => [
            'idRequest' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DiscountRequestController::class,
            'action' => 'discountConfirm',
        ],
    ]
];
$deal_discount_refuse = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/deal/discount/request/:idRequest/refuse',
        'constraints' => [
            'idRequest' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DiscountRequestController::class,
            'action' => 'discountRefuse',
        ],
    ]
];

// refund
$deal_refund_request_single = [
    'type' => Segment::class,
    'options' => [
        'route' => '/deal/refund/request/:idRequest',
        'constraints' => [
            'idRequest' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\RefundController::class,
            'action' => 'refundRequestSingle',
        ],
    ]
];
$deal_refund_request = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/deal/:idDeal/refund/request',
        'constraints' => [
            'idDeal' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\RefundController::class,
            'action' => 'refundRequest',
        ],
    ]
];
$deal_refund_confirm = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/deal/refund/request/:idRequest/confirm',
        'constraints' => [
            'idRequest' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\RefundController::class,
            'action' => 'refundConfirm',
        ],
    ]
];
$deal_refund_refuse = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/deal/refund/request/:idRequest/refuse',
        'constraints' => [
            'idRequest' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\RefundController::class,
            'action' => 'refundRefuse',
        ],
    ]
];
// tribunal
$dispute_tribunal = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/dispute/:idDispute/tribunal',
        'constraints' => [
            'idDispute' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\TribunalController::class,
            'action' => 'tribunal',
        ],
    ]
];
$deal_tribunal_request = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/deal/:idDeal/tribunal/request',
        'constraints' => [
            'idDeal' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\TribunalController::class,
            'action' => 'tribunalRequest',
        ],
    ]
];
$deal_tribunal_confirm = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/deal/tribunal/request/:idRequest/confirm',
        'constraints' => [
            'idRequest' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\TribunalController::class,
            'action' => 'tribunalConfirm',
        ],
    ]
];
$deal_tribunal_refuse = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/deal/tribunal/request/:idRequest/refuse',
        'constraints' => [
            'idRequest' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\TribunalController::class,
            'action' => 'tribunalRefuse',
        ],
    ]
];
//arbitrage
$deal_arbitrage_request = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/deal/:idDeal/arbitrage/request',
        'constraints' => [
            'idDeal' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\ArbitrageController::class,
            'action' => 'arbitrageRequest',
        ],
    ]
];
$deal_arbitrage_confirm = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/deal/arbitrage/request/:idRequest/confirm',
        'constraints' => [
            'idRequest' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\ArbitrageController::class,
            'action' => 'arbitrageConfirm',
        ],
    ]
];
$deal_arbitrage_refuse = [
    'type' => Segment::class,
    'options' => [
        'route' => '/deal/arbitrage/request/:idRequest/refuse',
        'constraints' => [
            'idRequest' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\ArbitrageController::class,
            'action' => 'arbitrageRefuse',
        ],
    ]
];
$deal_invoice = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/invoice',
        'defaults' => [
            'controller' => Controller\InvoiceController::class,
        ],
    ],
];
$deal_invoice_single = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/invoice/:id[/:work]',
        'constraints' => [
            'id' => '[1-9][0-9]*',
            'work' => 'download',
        ],
        'defaults' => [
            'controller' => Controller\InvoiceController::class,
        ],
    ],
];

$delivery_form_confirm = [
    'type'    => 'Segment',
    'options' => [
        'route'    => '/deal/:idDeal/delivery/confirm',
        'constraints' => [
            'idDeal' => '[1-9][0-9]*',
        ],
        'defaults' => [
            'controller' => Controller\DeliveryController::class,
            'action' => 'confirmForm',
        ],
    ]
];
$delivery = [
    'type'    => 'Segment',
    'options' => [
        'route'    => '/delivery',
        'defaults' => [
            'controller' => Controller\DeliveryController::class,
        ],
    ]
];

// Deal-token (partner-button)
$token = [
    'type'    => Segment::class,
    'options' => [
        'route' => '/partner-button',
        'defaults' => [
            'controller' => Controller\DealTokenController::class,
        ],
    ]
];
$token_single = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/partner-button/:id',
        'constraints' => [
            'id'     => '\d+'
        ],
        'defaults' => [
            'controller' => Controller\DealTokenController::class,
        ],
    ]
];
$token_form_init = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/partner-button/form-init',
        'defaults' => [
            'controller' => Controller\DealTokenController::class,
            'action' => 'formInit',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ]
];
$token_form_create = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/partner-button/create',
        'defaults' => [
            'controller' => Controller\DealTokenController::class,
            'action' => 'createForm',
        ],
    ],
];
$token_form_edit = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/partner-button/:idToken/edit',
        'constraints' => [
            'idToken' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DealTokenController::class,
            'action' => 'editForm',
        ],
    ],
];
$token_form_delete = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/partner-button/:idToken/delete',
        'constraints' => [
            'idToken' => '\d+',
        ],
        'defaults' => [
            'controller' => Controller\DealTokenController::class,
            'action' => 'deleteForm',
        ],
    ]
];
$token_single_by_slug = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/partner-button/slug',
        'defaults' => [
            'controller' => Controller\DealTokenController::class,
            'action' => 'isSlugExists',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
        'verb' => 'post' // only POST!
    ]
];
$token_switch_active = [
    'type'    => Segment::class,
    'options' => [
        'route'    => '/partner-button/switch-active',
        'defaults' => [
            'controller' => Controller\DealTokenController::class,
            'action' => 'switchActive',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ]
    ]
];

return [
    'router' => [
        'routes' => [
            'error' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/error/send',
                    'defaults' => [
                        'controller' => Controller\ErrorController::class,
                        'action'     => 'send',
                    ],
                ],
            ],
            'error-report-message' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/error-report-message',
                    'defaults' => [
                        'controller' => Controller\ErrorController::class,
                        'action'     => 'errorReportMessage',
                    ],
                ],
            ],
            //
            // User
            //
            'user' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/user',
                    'defaults' => [
                        'controller' => Controller\UserController::class,
                    ],
                ],
            ],
            //
            // Bank
            //
            'bank' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/bank/search',
                    'defaults' => [
                        'controller' => Controller\BankController::class,
                        'action'     => 'search',
                    ],
                ],
                'strategies' => [
                    'ViewJsonStrategy',
                ],
            ],
            //
            // Bank
            //
            'bank-update' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/bank/update',
                    'defaults' => [
                        'controller' => Controller\BankController::class,
                        'action'     => 'bankUpdate',
                    ],
                ],
            ],
            //
            // Current user
            //
            'current-user' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/current-user',
                    'defaults' => [],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'civil-law-subjects' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/civil-subjects',
                            'defaults' => [
                                'controller' => Controller\CivilLawSubjectController::class,
                            ],
                        ],
                    ]
                ]
            ],
            'civil-law-subject-search' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/civil-law-subject/search',
                    'defaults' => [
                        'controller' => Controller\CivilLawSubjectController::class,
                        'action'     => 'search',
                    ],
                ],
                'strategies' => [
                    'ViewJsonStrategy',
                ],
            ],
            //
            // Bank client
            //
            'bank-client' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/bank-client',
                    'defaults' => [
                        'controller' => Controller\BankClientController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                /* ---===============  Child  ===============--- */
                'child_routes' => [
                    'file-stock-upload' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/file-stock-upload',
                            'defaults' => array(
                                'controller' => Controller\BankClientController::class,
                                'action' => 'fileFromStockUpload',
                            ),
                        ),
                    ),
                    'process' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/process',
                            'defaults' => array(
                                'controller' => Controller\BankClientController::class,
                                'action' => 'paymentOrderFileProcessing',
                            ),
                        ),
                    ),
                    'show-payment-order' => array(
                        'type'    => Segment::class,
                        'options' => array(
                            'route'    => '/show-payment-order/:id',
                            'defaults' => array(
                                'controller' => Controller\BankClientController::class,
                                'action' => 'showPaymentOrder',
                            ),
                        ),
                    ),
                    'file-upload' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/file-upload',
                            'defaults' => array(
                                'controller' => Controller\BankClientController::class,
                                'action' => 'fileUpload',
                            ),
                        ),
                        'strategies' => [
                            'ViewJsonStrategy',
                        ],
                    ),
                    'credit-unpaid' => array(
                        'type'    => Segment::class,
                        'options' => array(
                            'route'    => '/credit-unpaid',
                            'defaults' => array(
                                'controller' => Controller\BankClientController::class,
                                'action' => 'creditUnpaid',
                            ),
                        ),
                    ),
                    'credit-unpaid/generate' => array(
                        'type'    => Segment::class,
                        'options' => array(
                            'route'    => '/credit-unpaid/generate',
                            'defaults' => array(
                                'controller' => Controller\BankClientController::class,
                                'action' => 'generateCreditUnpaidFile',
                            ),
                        ),
                    ),
                    'file-download' => array(
                        'type'    => Segment::class,
                        'options' => array(
                            'route'    => '/file-download/:id',
                            'defaults' => array(
                                'controller' => Controller\BankClientController::class,
                                'action' => 'fileDownload',
                            ),
                        ),
                    ),
                ]
                /* ---=============== </Child ===============--- */
            ],
            //
            // PaymentMethod for Operator
            //
            'payment-method' => $payment_method,
            'payment-method-single' => $payment_method_single,
            //
            // NaturalPerson for Operator
            //
            'natural-person' => $natural_person,
            'natural-person-single' => $natural_person_single,
            //
            // SoleProprietor for Operator
            //
            'sole-proprietor' => $sole_proprietor,
            'sole-proprietor-single' => $sole_proprietor_single,
            //
            // LegalEntity for Operator
            //
            'legal-entity' => $legal_entity,
            'legal-entity-single' => $legal_entity_single,
            //
            // Natural Person Driver License Document
            //
            'natural-person-driver-license' => $natural_person_driver_license,
            'natural-person-driver-license-single' => $natural_person_driver_license_single,
            //
            // Natural Person Passport Document
            //
            'natural-person-passport' => $natural_person_passport,
            'natural-person-passport-single' => $natural_person_passport_single,
            //
            // Legal Entity Bank Detail Document
            //
            'legal-entity-bank-detail' => $legal_entity_bank_detail,
            'legal-entity-bank-detail-single' => $legal_entity_bank_detail_single,
            //
            // Legal Entity Tax Inspection Document
            //
            'legal-entity-tax-inspection' => $legal_entity_tax_inspection,
            'legal-entity-tax-inspection-single' => $legal_entity_tax_inspection_single,
            //
            // Natural person and legal entity document
            //
            'natural-person-document' => $natural_person_document,
            'legal-entity-document' => $legal_entity_document,
            //
            // user routes
            //
            'user-profile' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/:profile',
                    'constraints' => [
                        'profile' => 'profile'
                    ],
                    'defaults' => [
                        'controller' => Controller\NaturalPersonController::class,
                        'profile'    => 'profile'
                    ],
                ],
                'may_terminate' => true,
                /* ---===============  Child for user-profile ===============--- */
                'child_routes' => [
                    //natural-person
                    'natural-person' => $natural_person,
                    'natural-person-single' => $natural_person_single,
                    'natural-person-form-edit' => $natural_person_form_edit,
                    'natural-person-form-delete' => $natural_person_form_delete,
                    'natural-person-form-create' => $natural_person_form_create,
                    'natural-person-form-init' => $natural_person_form_init,
                    //sole-proprietor
                    'sole-proprietor' => $sole_proprietor,
                    'sole-proprietor-single' => $sole_proprietor_single,
                    'sole-proprietor-form-edit' => $sole_proprietor_form_edit,
                    'sole-proprietor-form-delete' => $sole_proprietor_form_delete,
                    'sole-proprietor-form-create' => $sole_proprietor_form_create,
                    'sole-proprietor-form-init' => $sole_proprietor_form_init,
                    //legal-entity
                    'legal-entity' => $legal_entity,
                    'legal-entity-single' => $legal_entity_single,
                    'legal-entity-form-edit' => $legal_entity_form_edit,
                    'legal-entity-form-delete' => $legal_entity_form_delete,
                    'legal-entity-form-create' => $legal_entity_form_create,
                    'legal-entity-form-init' => $legal_entity_form_init,
                    //payment-method
                    'payment-method' => $payment_method,
                    'payment-method-single' => $payment_method_single,
                    'payment-method-form-edit' => $payment_method_form_edit,
                    'payment-method-form-delete' => $payment_method_form_delete,
                    'payment-method-form-create' => $payment_method_form_create,
                    'payment-method-form-init' => $payment_method_form_init,
                    //driver-license
                    'natural-person-driver-license' => $natural_person_driver_license,
                    'natural-person-driver-license-single' => $natural_person_driver_license_single,
                    'natural-person-driver-license-form-edit' => $natural_person_driver_license_form_edit,
                    'natural-person-driver-license-form-delete' => $natural_person_driver_license_form_delete,
                    'natural-person-driver-license-form-create' => $natural_person_driver_license_form_create,
                    'natural-person-driver-license-form-init' => $natural_person_driver_license_form_init,
                    //passport
                    'natural-person-passport' => $natural_person_passport,
                    'natural-person-passport-single' => $natural_person_passport_single,
                    'natural-person-passport-form-edit' => $natural_person_passport_form_edit,
                    'natural-person-passport-form-delete' => $natural_person_passport_form_delete,
                    'natural-person-passport-form-create' => $natural_person_passport_form_create,
                    'natural-person-passport-form-init' => $natural_person_passport_form_init,
                    //bank-detail
                    'legal-entity-bank-detail' => $legal_entity_bank_detail,
                    'legal-entity-bank-detail-single' => $legal_entity_bank_detail_single,
                    'legal-entity-bank-detail-form-edit' => $legal_entity_bank_detail_form_edit,
                    'legal-entity-bank-detail-form-delete' => $legal_entity_bank_detail_form_delete,
                    'legal-entity-bank-detail-form-create' => $legal_entity_bank_detail_form_create,
                    'legal-entity-bank-detail-form-init' => $legal_entity_bank_detail_form_init,
                    //tax_inspection
                    'legal-entity-tax-inspection' => $legal_entity_tax_inspection,
                    'legal-entity-tax-inspection-single' => $legal_entity_tax_inspection_single,
                    'legal-entity-tax-inspection-form-edit' => $legal_entity_tax_inspection_form_edit,
                    'legal-entity-tax-inspection-form-delete' => $legal_entity_tax_inspection_form_delete,
                    'legal-entity-tax-inspection-form-create' => $legal_entity_tax_inspection_form_create,
                    'legal-entity-tax-inspection-form-init' => $legal_entity_tax_inspection_form_init,
                    // deal-token
                    'deal-token' => $token,
                    'deal-token-single' => $token_single,
                    'deal-token-form-edit' => $token_form_edit,
                    'deal-token-form-create' => $token_form_create,
                    'deal-token-form-init' => $token_form_init,
                    'deal-token-form-delete' => $token_form_delete,
                    'deal-token-single-by-slug' => $token_single_by_slug,
                    'deal-token-switch-active' => $token_switch_active,
                ]
                /* ---===============  End child user-profile ===============--- */
            ],
            //
            // Profile
            //
            'profile' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/profile',
                    'defaults' => [
                        'controller' => Controller\ProfileController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                /* ---===============  Child  ===============--- */
                'child_routes' => [
                    'login' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/user/:login',
                            'constraints' => [
                                //'login'     => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ],
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action'     => 'index',
                            ],
                        ],
                    ],
                    'file' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/file/:civilId/:fileId',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action' => 'showFile',
                            ],
                        ],
                    ],
                    'avatar' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/avatar/:fileId',
                            'defaults' => [
                                'controller' => Controller\ProfileController::class,
                                'action' => 'showAvatarFile',
                            ],
                        ],
                    ],
                ]
                /* ---=============== </Child ===============--- */
            ],
            //
            // Deal
            //
            'deal-form-delete' => [ // Follows AbstractRestfulController
                'type'    => Segment::class,
                'options' => [
                    'route' => '/deal/:idDeal/delete',
                    'constraints' => [
                        'idDeal' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'deleteForm',
                    ],
                ]
            ],
            'deal-confirm-conditions' => [ // Follows AbstractRestfulController
                'type'    => Segment::class,
                'options' => [
                    'route' => '/deal/:idDeal/confirm-conditions',
                    'constraints' => [
                        'idDeal' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'confirmConditions',
                    ],
                ]
            ],
            'deal-successful-created' => [ // Follows AbstractRestfulController
                'type'    => Segment::class,
                'options' => [
                    'route' => '/deal/successful-created',
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'successfulCreated',
                    ],
                ]
            ],
            'deal-invitation-resend' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/deal/:idDeal/invitation/resend',
                    'constraints' => [
                        'idDeal' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'dealInvitationResend',
                    ],
                ]
            ],
            'deal-create-civil-law-subject' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/deal/:idDeal/civil-law-subject',
                    'constraints' => [
                        'idDeal' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'createCivilLawSubject',
                    ],
                ]
            ],
            'deal-create-payment-method' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/deal/:idDeal/payment-method',
                    'constraints' => [
                        'idDeal' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'createPaymentMethod',
                    ],
                ]
            ],
            'deal-civil-law-subject_payment-method' => [ // Follows AbstractRestfulController
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/deal/:id/civil-law-subject_payment-method',
                    'constraints' => [
                        'id'     => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'civilLawSubjectPaymentMethod',
                    ],
                ]
            ],
            'deal-create-delivery' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/deal/:idDeal/delivery',
                    'constraints' => [
                        'idDeal' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'createDelivery',
                    ],
                ]
            ],
            'deal-create-dpd-order' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/deal/:idDeal/create-dpd-order',
                    'constraints' => [
                        'idDeal' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'createDpdOrder',
                    ],
                ]
            ],
            'payment-method-create-with-deal' => $payment_method_form_create_with_deal,
            'deal-create-tracking-number' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/deal/:idDeal/tracking-number',
                    'constraints' => [
                        'idDeal' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'createTrackingNumber',
                    ],
                ]
            ],
            'deal_pay_options_form' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/deal/:idDeal/pay/options',
                    'constraints' => [
                        'idDeal' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\PayController::class,
                        'action' => 'optionsForm',
                    ],
                ],
            ],
            'deal-pay-mandarin-form-init' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/deal/:idDeal/pay/mandarin/form-init',
                    'defaults' => [
                        'controller' => \ModuleAcquiringMandarin\Controller\MandarinPayController::class,
                        'action' => 'formInit',
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            'deal-pay-mandarin' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/deal/:idDeal/pay/mandarin',
                    'constraints' => [
                        ':idDeal'     => '\d+'
                    ],
                    'defaults' => [
                        'controller' => \ModuleAcquiringMandarin\Controller\MandarinPayController::class,
                        'action' => 'pay',
                    ],
                ]
            ],
            'deal-single' => [ // Follows AbstractRestfulController
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/deal/:id',
                    'constraints' => [
                        'id'     => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Controller\DealController::class,

                    ],
                ]
            ],
            'deal-form-init' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/deal/form-init',
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'formInit',
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            'deal-types' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/deal/types',
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action' => 'types',
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],

            // @TODO Переделать остальные роуты и экшены на AbstractRestfulController
            'deal' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/deal',
                    'defaults' => [
                        'controller' => Controller\DealController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                /* ---===============  Child for Deal ===============--- */
                'child_routes' => [
                    'create' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'create',
                                'type_form' => 'default'
                            ),
                        ),
                    ),
                    'create-service-customer' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create/service-customer',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'create',
                                'type_form' => 'service_customer'
                            ),
                        ),
                    ),
                    'create-service-contractor' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create/service-contractor',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'create',
                                'type_form' => 'service_contractor'
                            ),
                        ),
                    ),
                    'create-product-customer' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create/product-customer',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'create',
                                'type_form' => 'product_customer'
                            ),
                        ),
                    ),
                    'create-product-contractor' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create/product-contractor',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'create',
                                'type_form' => 'product_contractor'
                            ),
                        ),
                    ),
                    'create-unregistered-form' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create/unregistered-form',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'createUnregisteredForm',
                                'type_form' => 'default',
                            ),
                        ),
                    ),
                    'create-unregistered-form-service-customer' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create/unregistered-form/service-customer',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'createUnregisteredForm',
                                'type_form' => 'service_customer',
                            ),
                        ),
                    ),
                    'create-unregistered-form-service-contractor' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create/unregistered-form/service-contractor',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'createUnregisteredForm',
                                'type_form' => 'service_contractor',
                            ),
                        ),
                    ),
                    'create-unregistered-form-product-customer' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create/unregistered-form/product-customer',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'createUnregisteredForm',
                                'type_form' => 'product_customer',
                            ),
                        ),
                    ),
                    'create-unregistered-form-product-contractor' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create/unregistered-form/product-contractor',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'createUnregisteredForm',
                                'type_form' => 'product_contractor',
                            ),
                        ),
                    ),
                    'create-partner-form' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/create/partner-form/:tokenKey',
                            'constraints' => [
                                'tokenKey' => '[A-Za-z0-9_]*'
                            ],
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'createPartnerForm',
                            ),
                        ),
                    ),
                    'edit' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/edit/:id',
                            'constraints' => [
                                'id' => '\d+'
                            ],
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'edit',
                            ),
                        ),
                    ),
                    'show' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/show/:id',
                            'constraints' => array(
                                'id'     => '[0-9]*'
                            ),
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'show',
                            ),
                        ),
                    ),
                    'contract' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/contract/:id',
                            'constraints' => array(
                                'id'   => '[0-9]*'
                            ),
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'downloadContract',
                            ),
                        ),
                    ),
                    'deal-percentage' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/deal-percentage',
                            'defaults' => [
                                'controller' => Controller\DealController::class,
                                'action' => 'getDealPercentage',
                            ],
                        ],
                        'strategies' => [
                            'ViewJsonStrategy',
                        ],
                    ],
                    'show-all' => [ // @TODO Deprecated! Не используется.
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/show-all',
                            'defaults' => [
                                'controller' => Controller\DealController::class,
                                'action' => 'showAll',
                            ],
                        ],
                    ],
                    'expired' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/expired[/:id]',
                            'constraints' => [
                                'id'     => '[a-z0-9]*' // нам нужен id или "all"
                            ],
                            'defaults' => [
                                'controller' => Controller\DealController::class,
                                'action' => 'expired',
                            ],
                        ],
                    ],
                    'abnormal' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/abnormal',
                            'defaults' => [
                                'controller' => Controller\DealController::class,
                                'action' => 'abnormal',
                            ],
                        ],
                    ],
                    'overpay' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/overpay/:idDeal',
                            'constraints' => [
                                'idDeal'     => '\d+'
                            ],
                            'defaults' => [
                                'controller' => Controller\DealController::class,
                                'action' => 'overpay',
                            ],
                        ],
                    ],
                    'success' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/success',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'success',
                            ),
                        ),
                    ),
                    'error' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/error',
                            'defaults' => array(
                                'controller' => Controller\DealController::class,
                                'action' => 'error',
                            ),
                        ),
                    ),
                    'file-upload' => [
                        'type' => 'Segment',
                        'options' => [
                            'route'    => '/file-upload',
                            'defaults' => [
                                'controller' => Controller\DealController::class,
                                'action' => 'fileUpload',
                            ],
                        ],
                    ],
                    'file' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/file/:dealId/:fileId',
                            'defaults' => [
                                'controller' => Controller\DealController::class,
                                'action' => 'showFile',
                            ],
                        ],
                    ],
                    'dispute-create' => [
                        'type' => Segment::class,
                        'options' => [
                            'route'    => '/:dealId/dispute-create',
                            'constraints' => [
                                'dealId' => '\d+'
                            ],
                            'defaults' => [
                                'controller' => Controller\DealController::class,
                                'action' => 'disputeCreateForm',
                            ],
                        ],
                    ],
                ]
                /* ---=============== </ Child for Deal ===============--- */
            ],
            'deal-invoice' => $deal_invoice,
            'deal-invoice-single' => $deal_invoice_single,
            'deal-deadline-notify' => $deal_deadline_notify,
            'delivery' => $delivery,
            'delivery-form-confirm' => $delivery_form_confirm,
            //
            // Deal Comment for Operator
            //
            'deal-comment' => $deal_comment,
            'deal-comment-single' => $deal_comment_single,
            'deal-comment-form-edit' => $deal_comment_form_edit,
            'deal-comment-form-delete' => $deal_comment_form_delete,
            'deal-comment-form-create' => $deal_comment_form_create,
            'deal-comment-form-init' => $deal_comment_form_init,
            //
            // Deal Token for Operator
            //
            'deal-token' => $token,
            'deal-token-single' => $token_single,
            'deal-token-form-edit' => $token_form_edit,
            'deal-token-form-init' => $token_form_init,
            //
            // Dispute for Operator
            //
            'dispute' => $dispute,
            'dispute-open-list' => $dispute_open_list,
            'dispute-single' => $dispute_single,
            #'dispute-form-edit' => $dispute_form_edit,     // Спор редактировать нельзя
            #'dispute-form-delete' => $dispute_form_delete, // Спор удалить нельзя
            'dispute-form-create' => $dispute_form_create,
            'dispute-form-init' => $dispute_form_init,
            'dispute-form-close' => $dispute_form_close,
            'dispute-form-open' => $dispute_form_open,
            'dispute-file-upload' => $dispute_file_upload,
            'dispute-file' => $dispute_file,
            //
            // Dispute Comment for Operator
            //
            'dispute-operator-comment' => $dispute_operator_comment,
            'dispute-operator-comment-single' => $dispute_operator_comment_single,
            'dispute-operator-comment-form-edit' => $dispute_operator_comment_form_edit,
            'dispute-operator-comment-form-delete' => $dispute_operator_comment_form_delete,
            'dispute-operator-comment-form-create' => $dispute_operator_comment_form_create,
            'dispute-operator-comment-form-init' => $dispute_operator_comment_form_init,
            //discount
            'dispute-discount' => $dispute_discount,
            'deal_discount_single' => $deal_discount_single,
            'dispute-discount-form-create' => $dispute_discount_form_create,
            'dispute-discount-form-init' => $dispute_discount_form_init,
            //discount-request
            'deal-discount-request' => $deal_discount_request,
            'deal-discount-request-single' => $deal_discount_request_single,
            'deal-discount-request-form-create' => $deal_discount_request_form_create,
            'deal-discount-request-form-init' => $deal_discount_request_form_init,
            'deal-discount-confirm' => $deal_discount_confirm,
            'deal-discount-refuse' => $deal_discount_refuse,

            //refund
            'dispute-refund' => $dispute_refund,
            'deal-refund-request' => $deal_refund_request,
            'deal-refund-confirm' => $deal_refund_confirm,
            'deal-refund-refuse' => $deal_refund_refuse,
            'deal-refund-refuse-single' => $deal_refund_request_single,
            //tribunal
            'dispute-tribunal' => $dispute_tribunal,
            'deal-tribunal-request' => $deal_tribunal_request,
            'deal-tribunal-confirm' => $deal_tribunal_confirm,
            'deal-tribunal-refuse' => $deal_tribunal_refuse,
            //arbitrage
            'deal-arbitrage-request' => $deal_arbitrage_request,
            'deal-arbitrage-confirm' => $deal_arbitrage_confirm,
            'deal-arbitrage-refuse' => $deal_arbitrage_refuse,
            //
            // Test
            //
            'test' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/test',
                    'defaults' => [
                        'controller' => Controller\TestController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                /* ---===============  Child for file ===============--- */
                'child_routes' => [

                    'mandarin-payed' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/mandarin-payed',
                            'defaults' => array(
                                'controller' => Controller\TestController::class,
                                'action' => 'dealMandarinPayed',
                            ),
                        ),
                    ),
                    'files' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/files',
                            'defaults' => array(
                                'controller' => Controller\TestController::class,
                                'action' => 'fileList',
                            ),
                        ),
                    ),
                    'file-show' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/file-show/:file_id',
                            'constraints' => array(
                                'file_id'    => '[0-9]*',
                            ),
                            'defaults' => array(
                                'controller' => Controller\TestController::class,
                                'action' => 'fileShow',
                            ),
                        ),
                    ),
                    'file-upload' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/file-upload[/:owner_id]',
                            'constraints' => array(
                                'owner_id'    => '[0-9]*',
                            ),
                            'defaults' => array(
                                'controller' => Controller\TestController::class,
                                'action' => 'fileUpload',
                            ),
                        ),
                        'strategies' => [
                            'ViewJsonStrategy',
                        ],
                    ),
                    'file-ajax' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/file-ajax',
                            'defaults' => array(
                                'controller' => Controller\TestController::class,
                                'action' => 'fileAjax',
                            ),
                        ),
                    ),

                    'file-delete' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/file-delete[/owner/:owner_id][/file/:file_id]',
                            'constraints' => array(
                                //'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'owner_id'    => '[0-9]*',
                                'file_id'     => '[0-9]*',
                            ),
                            'defaults' => array(
                                'controller' => Controller\TestController::class,
                                'action' => 'fileDelete',
                            ),
                        ),
                    ),
                    'event' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/event',
                            'defaults' => array(
                                'controller' => Controller\TestController::class,
                                'action' => 'event',
                            ),
                        ),
                    ),
                    'qrcode' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/qrcode',
                            'defaults' => array(
                                'controller' => Controller\TestController::class,
                                'action' => 'qrcode',
                            ),
                        ),
                    ),
                    'mail-template' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/mail-template',
                            'defaults' => [
                                'controller' => Controller\TestController::class,
                                'action' => 'mailTemplate',
                            ],
                        ],
                    ],
                    'error' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/error',
                            'defaults' => [
                                'controller' => Controller\TestController::class,
                                'action' => 'error',
                            ],
                        ],
                    ],
                    'success' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/success',
                            'defaults' => [
                                'controller' => Controller\TestController::class,
                                'action' => 'success',
                            ],
                        ],
                    ],
                    'under-construction' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/under-construction',
                            'defaults' => [
                                'controller' => Controller\TestController::class,
                                'action' => 'underConstruction',
                            ],
                        ],
                    ],
                ]
                /* ---=============== </ Child for file ===============--- */
            ],
            //
            // Setting
            //
            'setting' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/setting',
                    'defaults' => [
                        'controller' => Controller\SettingController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                /* ---===============  Child for settings ===============--- */
                'child_routes' => [
                    'global-setting' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/global-setting',
                            'defaults' => array(
                                'controller' => Controller\SettingController::class,
                                'action' => 'globalSetting',
                            ),
                        ),
                    ),
                    'payment-detail' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/payment-detail',
                            'defaults' => array(
                                'controller' => Controller\SettingController::class,
                                'action' => 'paymentDetail',
                            ),
                        ),
                    )
                ]
            ],
        ]
    ],
];
