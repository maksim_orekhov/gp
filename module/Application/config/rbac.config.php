<?php
namespace Application;

return [
    'rbac_manager' => [
        'assertions' => [
            Service\Rbac\RbacProfileAssertionManager::class,
            Service\Rbac\RbacDealAssertionManager::class,
            Service\Rbac\RbacPaymentMethodAssertionManager::class,
            Service\Rbac\RbacDisputeAssertionManager::class,
            Service\Rbac\RbacDeliveryOrderAssertionManager::class,
        ],
    ],
];
