<?php
namespace Application;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'view_manager' => [
        'display_not_found_reason' => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/authentication'   => __DIR__ . '/../view/layout/authentication.phtml',
            'layout/landing'          => __DIR__ . '/../view/layout/landing.phtml',
            'layout/application'      => __DIR__ . '/../view/layout/application.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'twig/error/404'          => __DIR__ . '/../viewTwig/error/404.twig',
            'twig/error/500'          => __DIR__ . '/../viewTwig/error/500.twig',
            //message templates
            'message/error'             => __DIR__ . '/../view/message/error.phtml',
            'message/success'            => __DIR__ . '/../view/message/success.phtml',
            'message/under-construction' => __DIR__ . '/../view/message/under-construction.phtml',
            //email templates
            'email_confirm'  => __DIR__ . '/../view/template-email/template_email_confirm.phtml',
            'password_reset'  => __DIR__ . '/../view/template-email/template_password_reset.phtml',
            'confirmation_code' => __DIR__ . '/../view/template-email/template_confirmation_code.phtml',
            'deal_notification' => __DIR__ . '/../view/template-email/template_deal_notification.phtml',
            'deal_invitation' => __DIR__ . '/../view/template-email/template_deal_invitation.phtml',
            'deal_confirm_to_customer' => __DIR__ . '/../view/template-email/template_deal_confirm_to_customer.phtml',
            'deal_confirm_to_contractor' => __DIR__ . '/../view/template-email/template_deal_confirm_to_contractor.phtml',
            'deal_payment_confirm_to_customer' => __DIR__ . '/../view/template-email/template_deal_payment_confirm_to_customer.phtml',
            'deal_payment_confirm_to_contractor' => __DIR__ . '/../view/template-email/template_deal_payment_confirm_to_contractor.phtml',
            // twig
            'twig/layout/authentication'         => __DIR__ . '/../viewTwig/layout/authentication.twig',
            'twig/layout/landing'                => __DIR__ . '/../viewTwig/layout/landing.twig',
            'twig/layout/rules'                  => __DIR__ . '/../viewTwig/layout/rules.twig',
            'twig/layout/application'            => __DIR__ . '/../viewTwig/layout/application.twig',
            'twig/macro/component'               => __DIR__ . '/../viewTwig/macro/component.twig',
            // twig email
            'twig/error_report'                 => __DIR__ . '/../viewTwig/email/error_report.twig',
            'twig/email_confirm'                 => __DIR__ . '/../viewTwig/email/email_confirm.twig',
            'twig/password_reset'                => __DIR__ . '/../viewTwig/email/password_reset.twig',
            'twig/confirmation_code'             => __DIR__ . '/../viewTwig/email/confirmation_code.twig',
            'twig/registration_for_deal'         => __DIR__ . '/../viewTwig/email/registration_for_deal.twig',
            'twig/registration_owner_to_deal'    => __DIR__ . '/../viewTwig/email/registration_owner_to_deal.twig',
            'twig/registration_owner_after_create_deal' => __DIR__ . '/../viewTwig/email/registration_owner_after_create_deal.twig',
            'twig/registration_counter_agent_after_create_deal' => __DIR__ . '/../viewTwig/email/registration_counter_agent_after_create_deal.twig',
            'twig/invitation_to_deal'            => __DIR__ . '/../viewTwig/email/invitation_to_deal.twig',
            'twig/invitation_to_deal_for_author' => __DIR__ . '/../viewTwig/email/invitation_to_deal_for_author.twig',
            'twig/deal_changed'                  => __DIR__ . '/../viewTwig/email/deal_changed.twig',
            'twig/deal_agent_confirm'            => __DIR__ . '/../viewTwig/email/deal_agent_confirm.twig',
            'twig/deal_confirm_to_customer'      => __DIR__ . '/../viewTwig/email/deal_confirm_to_customer.twig',
            'twig/deal_confirm_to_contractor'    => __DIR__ . '/../viewTwig/email/deal_confirm_to_contractor.twig',
            'twig/deal_closed_for_customer'    => __DIR__ . '/../viewTwig/email/deal_closed_for_customer.twig',
            'twig/deal_closed_for_contractor'    => __DIR__ . '/../viewTwig/email/deal_closed_for_contractor.twig',
            'twig/payment_receipt_to_contractor_full' => __DIR__ . '/../viewTwig/email/payment_receipt_to_contractor_full.twig',
            'twig/payment_receipt_to_contractor_part' => __DIR__ . '/../viewTwig/email/payment_receipt_to_contractor_part.twig',
            'twig/payment_receipt_to_customer_full'   => __DIR__ . '/../viewTwig/email/payment_receipt_to_customer_full.twig',
            'twig/payment_receipt_to_customer_part'   => __DIR__ . '/../viewTwig/email/payment_receipt_to_customer_part.twig',
            'twig/tracking_number_customer'      => __DIR__ . '/../viewTwig/email/tracking_number_customer.twig', // Нужно ли?
            'twig/delivery_confirmed_for_customer'              => __DIR__ . '/../viewTwig/email/delivery_confirmed_for_customer.twig',
            'twig/delivery_confirmed_for_contractor'            => __DIR__ . '/../viewTwig/email/delivery_confirmed_for_contractor.twig',
            'twig/delivery_service_order_creation_customer'   => __DIR__ . '/../viewTwig/email/delivery_service_order_creation_customer.twig',
            'twig/delivery_service_order_creation_contractor' => __DIR__ . '/../viewTwig/email/delivery_service_order_creation_contractor.twig',
            'twig/delivery_successful_contractor'   => __DIR__ . '/../viewTwig/email/delivery_successful_contractor.twig',
            'twig/delivery_successful_customer'     => __DIR__ . '/../viewTwig/email/delivery_successful_customer.twig',
            'twig/delivery_unsuccessful_contractor'   => __DIR__ . '/../viewTwig/email/delivery_unsuccessful_contractor.twig',
            'twig/delivery_unsuccessful_customer'     => __DIR__ . '/../viewTwig/email/delivery_unsuccessful_customer.twig',
            'twig/dispute_open_for_customer'      => __DIR__ . '/../viewTwig/email/dispute_open_for_customer.twig',
            'twig/dispute_open_for_contractor'    => __DIR__ . '/../viewTwig/email/dispute_open_for_contractor.twig',
            'twig/customer_closed_dispute_for_contractor' => __DIR__ . '/../viewTwig/email/customer_closed_dispute_for_contractor.twig',
            'twig/customer_closed_dispute_for_customer'   => __DIR__ . '/../viewTwig/email/customer_closed_dispute_for_customer.twig',
            'twig/closed_dispute_with_day_for_customer'   => __DIR__ . '/../viewTwig/email/closed_dispute_with_day_for_customer.twig',
            'twig/closed_dispute_with_day_for_contractor' => __DIR__ . '/../viewTwig/email/closed_dispute_with_day_for_contractor.twig',
            'twig/closed_dispute_without_day_for_customer'   => __DIR__ . '/../viewTwig/email/closed_dispute_without_day_for_customer.twig',
            'twig/closed_dispute_without_day_for_contractor' => __DIR__ . '/../viewTwig/email/closed_dispute_without_day_for_contractor.twig',
            'twig/refund_provided_for_customer'     => __DIR__ . '/../viewTwig/email/refund_provided_for_customer.twig',
            'twig/refund_provided_for_contractor'   => __DIR__ . '/../viewTwig/email/refund_provided_for_contractor.twig',
            'twig/referred_to_tribunal_for_customer'    => __DIR__ . '/../viewTwig/email/referred_to_tribunal_for_customer.twig',
            'twig/referred_to_tribunal_for_contractor'  => __DIR__ . '/../viewTwig/email/referred_to_tribunal_for_contractor.twig',
            'twig/near_deadline_for_customer'   => __DIR__ . '/../viewTwig/email/near_deadline_for_customer.twig',
            'twig/near_deadline_for_contractor' => __DIR__ . '/../viewTwig/email/near_deadline_for_contractor.twig',
            'twig/discount_request_for_customer'        => __DIR__ . '/../viewTwig/email/discount_request_for_customer.twig',
            'twig/discount_request_for_contractor'      => __DIR__ . '/../viewTwig/email/discount_request_for_contractor.twig',
            'twig/discount_confirm_for_author_request'  => __DIR__ . '/../viewTwig/email/discount_confirm_for_author_request.twig',
            'twig/discount_confirm_for_contractor'      => __DIR__ . '/../viewTwig/email/discount_confirm_for_contractor.twig',
            'twig/discount_refuse_for_customer'         => __DIR__ . '/../viewTwig/email/discount_refuse_for_customer.twig',
            'twig/discount_refuse_for_contractor'       => __DIR__ . '/../viewTwig/email/discount_refuse_for_contractor.twig',
            'twig/discount_provided_for_customer'       => __DIR__ . '/../viewTwig/email/discount_provided_for_customer.twig',
            'twig/discount_provided_for_contractor'     => __DIR__ . '/../viewTwig/email/discount_provided_for_contractor.twig',
            'twig/arbitrage_request_for_customer' => __DIR__ . '/../viewTwig/email/arbitrage_request_for_customer.twig',
            'twig/arbitrage_request_for_contractor' => __DIR__ . '/../viewTwig/email/arbitrage_request_for_contractor.twig',
            'twig/arbitrage_confirm_for_author_request' => __DIR__ . '/../viewTwig/email/arbitrage_confirm_for_author_request.twig',
            'twig/arbitrage_refuse_for_customer' => __DIR__ . '/../viewTwig/email/arbitrage_refuse_for_customer.twig',
            'twig/arbitrage_refuse_for_contractor' => __DIR__ . '/../viewTwig/email/arbitrage_refuse_for_contractor.twig',
            'twig/arbitrage_request_after_discount_requests_for_customer' => __DIR__ . '/../viewTwig/email/arbitrage_request_after_discount_requests_for_customer.twig',
            'twig/arbitrage_request_after_discount_requests_for_contractor' => __DIR__ . '/../viewTwig/email/arbitrage_request_after_discount_requests_for_contractor.twig',
            'twig/refund_request_for_customer' => __DIR__ . '/../viewTwig/email/refund_request_for_customer.twig',
            'twig/refund_request_for_contractor' => __DIR__ . '/../viewTwig/email/refund_request_for_contractor.twig',
            'twig/refund_request_accepted_for_contractor' => __DIR__ . '/../viewTwig/email/refund_request_accepted_for_contractor.twig',
            'twig/refund_request_accepted_for_customer' => __DIR__ . '/../viewTwig/email/refund_request_accepted_for_customer.twig',
            'twig/refund_refuse_for_author_request' => __DIR__ . '/../viewTwig/email/refund_refuse_for_author_request.twig',
            'twig/tribunal_request_for_customer' => __DIR__ . '/../viewTwig/email/tribunal_request_for_customer.twig',
            'twig/tribunal_request_for_contractor' => __DIR__ . '/../viewTwig/email/tribunal_request_for_contractor.twig',
            'twig/tribunal_confirm_for_author_request' => __DIR__ . '/../viewTwig/email/tribunal_confirm_for_author_request.twig',
            'twig/tribunal_refuse_for_author_request' => __DIR__ . '/../viewTwig/email/tribunal_refuse_for_author_request.twig',
            //для теста верстки писем
            'twig/test-email'                    => __DIR__ . '/../viewTwig/email/TestEmail.twig',
            //message templates
            'twig/message/error'             => __DIR__ . '/../viewTwig/message/error.twig',
            'twig/message/success'            => __DIR__ . '/../viewTwig/message/success.twig',
            'twig/message/under-construction' => __DIR__ . '/../viewTwig/message/under-construction.twig',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
            __DIR__ . '/../viewTwig',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'view_helper_config' => [
        'flashmessenger' => [
            'message_open_format' => '<div%s>',
            'message_separator_string' => '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div><div%s>',
            'message_close_string' => '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>'
        ]
    ],
    'view_helpers' => [
        'factories' => [
            View\Helper\QueryHelper::class => View\Helper\Factory\QueryHelperFactory::class,
            View\Helper\SortHelper::class => View\Helper\Factory\SortHelperFactory::class,
            View\Helper\NumberToStringHelper::class => InvokableFactory::class,
            View\Helper\InvoiceHelper::class => InvokableFactory::class,
            View\Helper\PhoneHelper::class => InvokableFactory::class,
            View\Helper\DateHelper::class => InvokableFactory::class,
            View\Helper\VersionHelper::class => View\Helper\Factory\VersionHelperFactory::class,
            View\Helper\DealHelper::class => View\Helper\Factory\DealHelperFactory::class,
            View\Helper\RemoteServerModeHelper::class => InvokableFactory::class,
        ],
        'aliases' => [
            'queryHelper' => View\Helper\QueryHelper::class,
            'sortHelper' => View\Helper\SortHelper::class,
            'numberToStringHelper' => View\Helper\NumberToStringHelper::class,
            'invoiceHelper' => View\Helper\InvoiceHelper::class,
            'phoneHelper' => View\Helper\PhoneHelper::class,
            'dateHelper' => View\Helper\DateHelper::class,
            'versionHelper' => View\Helper\VersionHelper::class,
            'dealHelper' => View\Helper\DealHelper::class,
            'remoteServerModeHelper' => View\Helper\RemoteServerModeHelper::class,
        ],
    ],
];
