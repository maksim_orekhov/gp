<?php
namespace Application;

return [
    // The 'access_filter' key is used by the Application module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            'mode' => 'permissive',     // все, что не запрещено, разрешено
            //'mode' => 'restrictive',  // все, что не разрешено, запрещено
        ],
        'controllers' => [
            Controller\ProfileController::class => [
                // Разрешить доступ с indexAction аутентифицированным пользователям с привилегией "profile.view".
                [
                    'actions' => [
                        'index',
                    ],
                    'allows' => ['@']
                ],
                [
                    'actions' => [
                        'showAvatarFile',
                    ],
                    'allows' => ['+profile.own.edit']
                ],
                [
                    'actions' => [
                        'edit',
                        'showFile',
                    ],
                    'allows' => ['+profile.own.edit']
                ],
                // Еще примеры настроек доступа
                // Разрешить всем доступ к действиям "index"
                #['actions' => ['index'], 'allow' => '*'],
                // Разрешить авторизованным пользователям доступ к действию "index"
                #['actions' => ['index'], 'allow' => '@']
            ],
            Controller\NaturalPersonController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                    ],
                    'allows' => ['+profile.own.edit', '#Operator']
                ],
                [
                    'actions' => [
                        'update',
                        'delete',
                        'editForm',
                        'deleteForm',
                        'formInit'
                    ],

                    'allows' => ['+profile.own.edit']
                ],
                [
                    'actions' => [
                        'create',
                        'createForm',
                    ],
                    'allows' => ['#Verified']
                ],
            ],
            Controller\SoleProprietorController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                    ],
                    'allows' => ['+profile.own.edit', '#Operator']
                ],
                [
                    'actions' => [
                        'update',
                        'delete',
                        'editForm',
                        'deleteForm',
                        #'formInit'
                    ],
                    'allows' => ['+profile.own.edit']
                ],
                [
                    'actions' => [
                        'create',
                        'createForm',
                    ],
                    'allows' => ['#Verified']
                ],
            ],
            Controller\LegalEntityController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                    ],
                    'allows' => ['+profile.own.edit', '#Operator']
                ],
                [
                    'actions' => [
                        'update',
                        'delete',
                        'editForm',
                        'deleteForm',
                        #'formInit'
                    ],
                    'allows' => ['+profile.own.edit']
                ],
                [
                    'actions' => [
                        'create',
                        'createForm',
                    ],
                    'allows' => ['#Verified']
                ],
            ],
            Controller\PaymentMethodController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                    ],
                    'allows' => ['+profile.own.edit', '#Operator']
                ],
                [
                    'actions' => [
                        'update',
                        'create',
                        'delete',
                        'editForm',
                        'deleteForm',
                        'createForm',
                        'formInit'
                    ],
                    'allows' => ['+profile.own.edit']
                ],
            ],
            Controller\NaturalPersonPassportController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                    ],
                    'allows' => ['+profile.own.edit', '#Operator']
                ],
                [
                    'actions' => [
                        'update',
                        'create',
                        'delete',
                        'editForm',
                        'createForm',
                        'deleteForm',
                        'formInit'
                    ],
                    'allows' => ['+profile.own.edit']
                ],
            ],
            Controller\NaturalPersonDriverLicenseController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                    ],
                    'allows' => ['+profile.own.edit', '#Operator']
                ],
                [
                    'actions' => [
                        'update',
                        'create',
                        'delete',
                        'editForm',
                        'createForm',
                        'deleteForm',
                        'formInit'
                    ],
                    'allows' => ['+profile.own.edit']
                ],
            ],
            Controller\LegalEntityBankDetailController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                    ],
                    'allows' => ['+profile.own.edit', '#Operator']
                ],
                [
                    'actions' => [
                        'update',
                        'create',
                        'delete',
                        'editForm',
                        'createForm',
                        'deleteForm',
                        'formInit'
                    ],
                    'allows' => ['+profile.own.edit']
                ],
            ],
            Controller\LegalEntityTaxInspectionController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                    ],
                    'allows' => ['+profile.own.edit', '#Operator']
                ],
                [
                    'actions' => [
                        'update',
                        'create',
                        'delete',
                        'editForm',
                        'createForm',
                        'deleteForm',
                        'formInit'
                    ],
                    'allows' => ['+profile.own.edit']
                ],
            ],
            Controller\LegalEntityDocumentController::class => [
                [
                    'actions' => [
                        'getList'
                    ],
                    'allows' => ['#Operator']
                ],
            ],
            Controller\NaturalPersonDocumentController::class => [
                [
                    'actions' => [
                        'getList'
                    ],
                    'allows' => ['#Operator']
                ],
            ],
            Controller\DealController::class => [
                ['actions' => ['index'], 'allows' => ['+deal.any', '#Operator', '#Unverified']],
                #['actions' => ['create'], 'allows' => ['+deal.own.view']],
                ['actions' => ['show'], 'allows' => ['@']],
                [
                    'actions' => [
                        'edit',
                        'createPaymentMethod',
                        'createCivilLawSubject',
                        'civilLawSubjectPaymentMethod',
                        'createDelivery',
                        'createDpdOrder',
                        'dealInvitationResend',
                        'confirmConditions'
                    ],
                    'allows' => ['+deal.own.edit']
                ],
                ['actions' => ['downloadContract'], 'allows' => ['+deal.own.contract']],
                [
                    'actions' => [
                        'showAll',
                        'expired',
                        'abnormal',
                        'overpay'
                    ],
                    'allows' => ['#Operator']
                ],
                [
                    'actions' => [
                        'delete',
                        'deleteForm'
                    ],
                    'allows' => ['+deal.own.edit']
                ],
            ],
            Controller\DealTokenController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                        'create',
                        'update',
                        'delete',
                        'createForm',
                        'editForm',
                        'deleteForm',
                        'formInit',
                        'isSlugExists',
                        'switchActive',
                    ],
                    'allows' => ['#Verified']
                ],
            ],
            Controller\DealCommentController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                        'update',
                        'create',
                        'delete',
                        'editForm',
                        'createForm',
                        'deleteForm',
                        'formInit'
                    ],
                    'allows' => ['#Operator']
                ],
            ],
            Controller\DisputeController::class => [
                [
                    'actions' => [
                        'create',
                        'createForm',
                        'openForm'
                    ],
                    'allows' => ['#Verified'] // Создать/возобновить спор может только Контрактор (AssertionManager)
                ],
                [
                    'actions' => [
                        'get',
                        'formInit',
                        'closeForm',
                    ],
                    'allows' => ['#Operator', '#Verified']
                ],
                [
                    'actions' => [
                        'getList',
                        'openList',
                        'fileUpload',
                        'showFile',
                    ],
                    'allows' => ['#Operator']
                ],
            ],
            Controller\DisputeOperatorCommentController::class => [
                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                        'createForm',
                        'editForm',
                        'deleteForm'
                    ],
                    'allows' => ['#Operator']
                ],
            ],
            Controller\BankClientController::class => [
                // Разрешить доступ аутентифицированным пользователям с привилегией "role.Operator".
                ['actions' => ['index'], 'allows' => ['+bank-client.view']],
                ['actions' =>
                    [
                        'fileUpload',
                        'paymentOrderFileProcessing',
                        'fileFromStockUpload',
                        'showPaymentOrder',
                        'creditUnpaid',
                        'generateCreditUnpaidFile',
                        'fileDownload',
                    ],
                    'allows' => ['+bank-client.execute']
                ]
            ],
            Controller\SettingController::class => [
                // Разрешить доступ аутентифицированным пользователям с ролью Administrator.
                ['actions' => '*', 'allows' => ['#Administrator']],
            ],
            Controller\UserController::class => [
                [
                    'actions' => [
                        'getList',
                    ],
                    'allows' => ['#Operator']
                ],
            ],
            Controller\ErrorController::class => [
                [
                    'actions' => [
                        'send',
                    ],
                    'allows' => ['*'] //to all
                ],
            ],
            Controller\RefundController::class => [
                [
                    'actions' => [
                        'refund',
                    ],
                    'allows' => ['#Operator']
                ],
                [
                    'actions' => [
                        'refundRequestSingle',
                        'refundRequest',
                        'refundConfirm',
                        'refundRefuse',
                    ],
                    'allows' => ['@']
                ],
            ],
            Controller\DiscountController::class => [
                [
                    'actions' => [
                        'getList',
                        'get',
                        'create',
                        'createForm',
                        'formInit'
                    ],
                    'allows' => ['#Operator']
                ],
            ],
            Controller\DiscountRequestController::class => [
                [
                    'actions' => [
                        'getList',
                    ],
                    'allows' => ['#Operator']
                ],
                [
                    'actions' => [
                        'get',
                        'create',
                        'createForm',
                        'formInit',
                        'discountConfirm',
                        'discountRefuse'
                    ],
                    'allows' => ['@']
                ]
            ],
            Controller\TribunalController::class => [
                [
                    'actions' => [
                        'tribunal',
                        'tribunalConfirm',
                        'tribunalRefuse',
                    ],
                    'allows' => ['#Operator']
                ],
                [
                    'actions' => [
                        'tribunalRequest',
                    ],
                    'allows' => ['@']
                ],
            ],
            Controller\ArbitrageController::class => [
                [
                    'actions' => [
                        'arbitrageConfirm',
                        'arbitrageRefuse',
                    ],
                    'allows' => ['#Operator']
                ],
                [
                    'actions' => [
                        'arbitrageRequest',
                    ],
                    'allows' => ['@']
                ],
            ],
            Controller\BankController::class => [
                [
                    'actions' => [
                        'bankUpdate',
                    ],
                    'allows' => ['#Operator']
                ],
                [
                    'actions' => [
                        'search',
                    ],
                    'allows' => ['@']
                ],
            ],
            Controller\DealDeadlineController::class => [
                [
                    'actions' => [
                        'dealDeadline',
                    ],
                    'allows' => ['#Operator']
                ],
            ],
            Controller\InvoiceController::class => [
                [
                    'actions' => [
                        'getList',
                    ],
                    'allows' => ['@']
                ],
                [
                    'actions' => [
                        'get',
                    ],
                    'allows' => ['#Operator', '+deal.own.customer.invoice']
                ],
            ],
            Controller\DeliveryController::class => [
                [
                    'actions' => [
                        'confirmForm',
                        'create'
                    ],
                    'allows' => ['+deal.own.edit']
                ],
            ],
            Controller\PayController::class => [
                [
                    'actions' => [
                        'optionsForm',
                    ],
                    'allows' => ['#Verified']
                ],
            ],
            Controller\CivilLawSubjectController::class => [
                [
                    'actions' => [
                        'getList',
                        'search',
                    ],
                    'allows' => ['@']
                ],
            ],
        ]
    ],
];
