<?php
namespace Application;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\LandingController::class => InvokableFactory::class,
            Controller\BankController::class => Controller\Factory\BankControllerFactory::class,
            Controller\ProfileController::class => Controller\Factory\ProfileControllerFactory::class,
            Controller\TestController::class => Controller\Factory\TestControllerFactory::class,
            Controller\BankClientController::class => Controller\Factory\BankClientControllerFactory::class,
            Controller\DealController::class => Controller\Factory\DealControllerFactory::class,
            Controller\SettingController::class => Controller\Factory\SettingControllerFactory::class,
            Controller\PaymentMethodController::class => Controller\Factory\PaymentMethodControllerFactory::class,
            Controller\NaturalPersonController::class => Controller\Factory\NaturalPersonControllerFactory::class,
            Controller\LegalEntityController::class => Controller\Factory\LegalEntityControllerFactory::class,
            Controller\LegalEntityDocumentController::class => Controller\Factory\LegalEntityDocumentControllerFactory::class,
            Controller\NaturalPersonDocumentController::class => Controller\Factory\NaturalPersonDocumentControllerFactory::class,
            Controller\NaturalPersonDriverLicenseController::class => Controller\Factory\NaturalPersonDriverLicenseControllerFactory::class,
            Controller\NaturalPersonPassportController::class => Controller\Factory\NaturalPersonPassportControllerFactory::class,
            Controller\SoleProprietorController::class => Controller\Factory\SoleProprietorControllerFactory::class,
            Controller\LegalEntityBankDetailController::class => Controller\Factory\LegalEntityBankDetailControllerFactory::class,
            Controller\LegalEntityTaxInspectionController::class => Controller\Factory\LegalEntityTaxInspectionControllerFactory::class,
            Controller\CivilLawSubjectController::class => Controller\Factory\CivilLawSubjectControllerFactory::class,
            Controller\UserController::class => Controller\Factory\UserControllerFactory::class,
            Controller\DealCommentController::class => Controller\Factory\DealCommentControllerFactory::class,
            Controller\DisputeController::class => Controller\Factory\DisputeControllerFactory::class,
            Controller\DisputeOperatorCommentController::class => Controller\Factory\DisputeOperatorCommentControllerFactory::class,
            Controller\RefundController::class => Controller\Factory\RefundControllerFactory::class,
            Controller\DiscountController::class => Controller\Factory\DiscountControllerFactory::class,
            Controller\DiscountRequestController::class => Controller\Factory\DiscountRequestControllerFactory::class,
            Controller\TribunalController::class => Controller\Factory\TribunalControllerFactory::class,
            Controller\DealDeadlineController::class => Controller\Factory\DealDeadlineControllerFactory::class,
            Controller\ArbitrageController::class => Controller\Factory\ArbitrageControllerFactory::class,
            Controller\InvoiceController::class => Controller\Factory\InvoiceControllerFactory::class,
            Controller\DeliveryController::class => Controller\Factory\DeliveryControllerFactory::class,
            Controller\PayController::class => Controller\Factory\PayControllerFactory::class,
            Controller\DealTokenController::class => Controller\Factory\DealTokenControllerFactory::class,
            Controller\ErrorController::class => Controller\Factory\ErrorControllerFactory::class,
        ],
    ],
];
