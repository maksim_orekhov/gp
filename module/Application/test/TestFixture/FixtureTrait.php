<?php
namespace ApplicationTest\TestFixture;

use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Entity\NaturalPerson;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Application\Entity\User;

trait FixtureTrait
{
    public function fixtureBankClientPaymentOrder()
    {
        $payment = $this->fixturePayment();
        $deal = $payment->getDeal();

        $bankClientPaymentOrder = new BankClientPaymentOrder();
        // Set created date
        $bankClientPaymentOrder->setCreated(new \DateTime());
        $bankClientPaymentOrder->setDocumentType('Платежное поручение');
        $bankClientPaymentOrder->setDocumentNumber($deal->getId());
        $bankClientPaymentOrder->setDocumentDate('04.03.2018');
        $bankClientPaymentOrder->setAmount($payment->getExpectedValue());
        $bankClientPaymentOrder->setRecipientAccount('40702810590030001259');
        $bankClientPaymentOrder->setRecipient('ИНН 7813271867 ООО "Гарант Пэй"');
        $bankClientPaymentOrder->setRecipientInn('7813271867');
        $bankClientPaymentOrder->setRecipient1('ООО "Гарант Пэй"');
        $bankClientPaymentOrder->setRecipientKpp('781301001');
        $bankClientPaymentOrder->setRecipientBank1('ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" Г. САНКТ-ПЕТЕРБУРГ');
        $bankClientPaymentOrder->setRecipientBik('044030790');
        $bankClientPaymentOrder->setRecipientCorrAccount('30101810900000000790');

        $bankClientPaymentOrder->setPayerAccount('40702810900000027373');
        $bankClientPaymentOrder->setPayer('ИНН 4217030520 Test');
        $bankClientPaymentOrder->setPayerInn('4217030520');
        $bankClientPaymentOrder->setPayer1('Test');
        $bankClientPaymentOrder->setPayerKpp(null);
        $bankClientPaymentOrder->setPayerBank1('АО "РАЙФФАЙЗЕНБАНК"');
        $bankClientPaymentOrder->setPayerBik('044525700');
        $bankClientPaymentOrder->setPayerCorrAccount('30101810200000000700');

        $bankClientPaymentOrder->setPaymentPurpose('Оплата по сделке '.$deal->getNumber());
        $bankClientPaymentOrder->setPaymentType('01');
        $bankClientPaymentOrder->setPriority(5);
        $bankClientPaymentOrder->setDateOfReceipt('24.02.2018');
        $bankClientPaymentOrder->setDateOfDebit('16.02.2018');
        $bankClientPaymentOrder->setHash('0faeda34e4c8d8c463d2e4644ba020a4');

        $this->entityManager->persist($bankClientPaymentOrder);
        $this->entityManager->flush();

        return $bankClientPaymentOrder;
    }

    public function fixtureCustomer()
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->fixtureCivilLawSubjectCustomer();

        $dealAgent = new DealAgent();
        $dealAgent->setCivilLawSubject($civilLawSubject);
        $dealAgent->setUser($civilLawSubject->getUser());
        $dealAgent->setDealAgentConfirm(true);
        $dealAgent->setDateOfConfirm(new \DateTime());
        $dealAgent->setEmail($civilLawSubject->getUser()->getEmail()->getEmail());

        $this->entityManager->persist($dealAgent);
        $this->entityManager->flush();

        return $dealAgent;
    }

    public function fixtureContractor()
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->fixtureCivilLawSubjectCustomer();

        $dealAgent = new DealAgent();
        $dealAgent->setCivilLawSubject($civilLawSubject);
        $dealAgent->setUser($civilLawSubject->getUser());
        $dealAgent->setDealAgentConfirm(true);
        $dealAgent->setDateOfConfirm(new \DateTime());
        $dealAgent->setEmail($civilLawSubject->getUser()->getEmail()->getEmail());

        $this->entityManager->persist($dealAgent);
        $this->entityManager->flush();

        return $dealAgent;
    }

    public function fixtureDeal()
    {
        /** @var DealAgent $customer */
        $customer = $this->fixtureCustomer();
        /** @var DealAgent $customer */
        $contractor = $this->fixtureContractor();

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $deal = new Deal();
        $deal->setCustomer($customer);
        $deal->setContractor($contractor);
        $deal->setOwner($customer); // Set Customer as Owner
        $deal->setName('Сделка Фикстура');
        $deal->setCreated(new \DateTime());
        $deal->setDeliveryPeriod(9);
        $deal->setAddonTerms('Дополнительные условия');
        $deal->setDealType($dealType);
        $deal->setFeePayerOption($feePayerOption);
        $deal->setUpdated($customer->getDateOfConfirm());

        $this->entityManager->persist($deal);
        $this->entityManager->flush();

        $deal->setNumber('#GP'.$deal->getId().$deal->getDealType()->getIdent());

        $this->entityManager->flush();

        return $deal;
    }

    public function fixtureCivilLawSubjectContractor()
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test2'));

        $naturalPerson = new NaturalPerson();
        $naturalPerson->setFirstName('Контрагент');
        $naturalPerson->setSecondaryName('Контрагентович');
        $naturalPerson->setLastName('Контрагентов');
        $naturalPerson->setBirthDate(\DateTime::createFromFormat('!Y-m-d', '1985-10-11'));
        $this->entityManager->persist($naturalPerson);

        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern(true);
        $civilLawSubject->setCreated(new \DateTime());
        $civilLawSubject->setNaturalPerson($naturalPerson);
        $this->entityManager->persist($civilLawSubject);

        $naturalPerson->setCivilLawSubject($civilLawSubject);

        $this->entityManager->flush();

        return $civilLawSubject;
    }

    public function fixtureCivilLawSubjectCustomer()
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'test2'));

        $naturalPerson = new NaturalPerson();
        $naturalPerson->setFirstName('Тест');
        $naturalPerson->setSecondaryName('Тестович');
        $naturalPerson->setLastName('Тестов');
        $naturalPerson->setBirthDate(\DateTime::createFromFormat('!Y-m-d', '1980-12-31'));
        $this->entityManager->persist($naturalPerson);

        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern(true);
        $civilLawSubject->setCreated(new \DateTime());
        $civilLawSubject->setNaturalPerson($naturalPerson);
        $this->entityManager->persist($civilLawSubject);

        $naturalPerson->setCivilLawSubject($civilLawSubject);

        $this->entityManager->flush();

        return $civilLawSubject;
    }

    public function fixturePaymentMethodContractor()
    {
        $setCivilLawSubject = $this->fixtureCivilLawSubjectContractor();

        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(array('name' => 'bank_transfer'));

        $paymentMethod = new PaymentMethod();
        $paymentMethod->setIsPattern(true);
        $paymentMethod->setCivilLawSubject($setCivilLawSubject);
        $paymentMethod->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentMethod);
        $this->entityManager->flush();

        return $paymentMethod;
    }

    public function fixturePayment()
    {
        $deal = $this->fixtureDeal();
        $paymentMethodContractor = $this->fixturePaymentMethodContractor();

        $payment = new Payment();
        $payment->setPaymentMethod($paymentMethodContractor);
        $payment->setDeal($deal);
        $payment->setDealValue(100000);
        $payment->setExpectedValue(104000);
        $payment->setReleasedValue(100000);
        $payment->setFee(4);

        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        return $payment;
    }
}