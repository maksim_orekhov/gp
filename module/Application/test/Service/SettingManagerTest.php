<?php

namespace ApplicationTest\Service;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Application\Service\UserManager;
use Application\Service\SettingManager;
use Application\Entity\GlobalSetting;

/**
 * Class SettingManagerTest
 * @package ApplicationTest\Service
 */
class SettingManagerTest extends AbstractHttpControllerTestCase
{
    const GP_1              = 'ООО "Гарант Пэй"';
    const GP_ACCOUNT        = '40702810590030001259';
    const GPDEALACCOUNT     = '40702810590030001259';
    const GP_INN            = '7813271867';
    const GP_KPP            = '781301001';
    const GP_BIK            = '044030790';
    const GP_BANK_1         = 'ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" Г. САНКТ-ПЕТЕРБУРГ';
    const GP_CORR_ACCOUNT   = '30101810900000000790';

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var SettingManager;
     */
    private $settingManager;

    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../config/application.config.php',
                    include __DIR__ . '/../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->settingManager = $serviceManager->get(SettingManager::class);

        $config = $this->getApplicationConfig();
        $user = $this->userManager->selectUserByLogin($config['tests']['user_login']);
        $this->user = $user;

        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        $this->entityManager->getConnection()->rollback();
        $this->entityManager->getConnection()->close();

        parent::tearDown();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group setting
     */
    public function testFindAllGlobalSettings()
    {
        $removeAllGlobalSettings = $this->entityManager->getRepository(GlobalSetting::class)->findAll();
        foreach ($removeAllGlobalSettings as $removeAllGlobalSetting){
            $this->entityManager->remove($removeAllGlobalSetting);
        }
        $this->entityManager->flush();

        $testGlobalSettings = $this->settingManager->findAllGlobalSettings();

        // GlobalSettings быть не должно, мы их удалили
        $this->assertEquals(true, empty($testGlobalSettings));

        $param = [
            '0' => [
                'name' => 'fee_percentage',
                'value' => '4',
                'description' => 'Процент гонорара',
            ],
            '1' => [
                'name' => 'min_delivery_period',
                'value' => '7',
                'description' => 'Минимальный срок поставки',
            ]
        ];

        // создаем для тестов new GlobalSetting с известными нам значениями
        $globalSetting = new GlobalSetting();
        $globalSetting->setName($param['0']['name']);
        $globalSetting->setDescription($param['0']['description']);
        $globalSetting->setValue($param['0']['value']);
        $this->entityManager->persist($globalSetting);

        $globalSetting2 = new GlobalSetting();
        $globalSetting2->setName($param['1']['name']);
        $globalSetting2->setDescription($param['1']['description']);
        $globalSetting2->setValue($param['1']['value']);
        $this->entityManager->persist($globalSetting2);

        $this->entityManager->flush();

        //testing findAllGlobalSettings
        $testGlobalSettings = $this->settingManager->findAllGlobalSettings();

        // GlobalSettings не должн быть пустым так как мы только что их создали
        $this->assertEquals(true, !empty($testGlobalSettings));
        foreach ($testGlobalSettings as $key => $testGlobalSetting){
            $this->assertEquals(true, ($testGlobalSetting instanceof GlobalSetting));
            $this->assertEquals($param[$key]['name'], $testGlobalSetting->getName());
            $this->assertEquals($param[$key]['description'], $testGlobalSetting->getDescription());
            $this->assertEquals($param[$key]['value'], $testGlobalSetting->getValue());
        }
    }


    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group setting
     * @group bank-client
     *
     * @dataProvider providerPaymentOrderArraysForTestParticipantGuarantPay
     */
    public function testIsGuarantPayParticipant($given, $expected)
    {
        $this->assertEquals($expected, $this->settingManager->isGuarantPayParticipant($given));
    }

    // Variants of payment order arrays
    public function providerPaymentOrderArraysForTestParticipantGuarantPay(): array
    {
        return [
            [self::getPaymentOrderArrayRecipientGP(), true],
            [self::getPaymentOrderArrayPayerGP(), true],
            [self::getPaymentOrderArrayNoGP(), false],
        ];
    }


    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group setting
     * @group bank-client
     *
     * @dataProvider providerPaymentOrderArraysForTestPayerGuarantPay
     */
    public function testIsPayerGuarantPay($given, $expected)
    {
        $this->assertEquals($expected, $this->settingManager->isPayerGuarantPay($given));
    }

    // Variants of payment order arrays
    public function providerPaymentOrderArraysForTestPayerGuarantPay(): array
    {
        return [
            [self::getPaymentOrderArrayRecipientGP(), false],
            [self::getPaymentOrderArrayPayerGP(), true],
            [self::getPaymentOrderArrayNoGP(), false],
        ];
    }


    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group setting
     * @group bank-client
     *
     * @dataProvider providerPaymentOrderArraysForTestRecipientGuarantPay
     */
    public function testIsRecipientGuarantPay($given, $expected)
    {
        $this->assertEquals($expected, $this->settingManager->isRecipientGuarantPay($given));
    }

    // Variants of payment order arrays
    public function providerPaymentOrderArraysForTestRecipientGuarantPay(): array
    {
        return [
            [self::getPaymentOrderArrayRecipientGP(), true],
            [self::getPaymentOrderArrayPayerGP(), false],
            [self::getPaymentOrderArrayNoGP(), false],
        ];
    }


    /**
     * @return array
     */
    public static function getPaymentOrderArrayRecipientGP()
    {
        return [
            'document_type' => 'Платежное поручение',
            'document_number' => '320',
            'document_date' => '23.05.2018',
            'amount' => 20400,
            'payer_1' => 'галимов андрей викторович',
            'payer_account' => '40702810902890029523',
            'payerdealaccount' => '40702810902890029523',
            'payer_inn' => '7805709836',
            'payer_kpp' => null,
            'payer_bik' => '044525201',
            'payer_bank_1' => 'ПАО АКБ "АВАНГАРД"',
            'payer_corr_account' => '30101810000000000201',
            'recipient_1' => self::GP_1,
            'recipient_account' => self::GP_ACCOUNT,
            'recieverdealaccount' => self::GPDEALACCOUNT,
            'recipient_inn' => self::GP_INN,
            'recipient_kpp' => self::GP_KPP,
            'recipient_bik' => self::GP_BIK,
            'recipient_bank_1' => self::GP_BANK_1,
            'recipient_corr_account' => self::GP_CORR_ACCOUNT,
            'payment_type' => '01',
            'priority' => '5',
            'payment_purpose' => 'Оплата по сделке GP320T'
        ];
    }

    /**
     * @return array
     */
    public static function getPaymentOrderArrayPayerGP()
    {
        return [
            'document_type' => 'Платежное поручение',
            'document_number' => '320',
            'document_date' => '23.05.2018',
            'amount' => 20400,
            'recipient_1' => 'галимов андрей викторович',
            'recipient_account' => '40702810902890029523',
            'recieverdealaccount' => '40702810902890029523',
            'recipient_inn' => '7805709836',
            'recipient_kpp' => null,
            'recipient_bik' => '044525201',
            'recipient_bank_1' => 'ПАО АКБ "АВАНГАРД"',
            'recipient_corr_account' => '30101810000000000201',
            'payer_1' => self::GP_1,
            'payer_account' => self::GP_ACCOUNT,
            'payerdealaccount' => self::GPDEALACCOUNT,
            'payer_inn' => self::GP_INN,
            'payer_kpp' => self::GP_KPP,
            'payer_bik' => self::GP_BIK,
            'payer_bank_1' => self::GP_BANK_1,
            'payer_corr_account' => self::GP_CORR_ACCOUNT,
            'payment_type' => '01',
            'priority' => '5',
            'payment_purpose' => 'Оплата по сделке GP320T'
        ];
    }

    /**
     * @return array
     */
    public static function getPaymentOrderArrayNoGP()
    {
        return [
            'document_type' => 'Платежное поручение',
            'document_number' => '320',
            'document_date' => '23.05.2018',
            'amount' => 20400,
            'payer_1' => 'галимов андрей викторович',
            'payer_account' => '40702810902890029523',
            'payerdealaccount' => '40702810902890029523',
            'payer_inn' => '7805709836',
            'payer_kpp' => null,
            'payer_bik' => '044525201',
            'payer_bank_1' => 'ПАО АКБ "АВАНГАРД"',
            'payer_corr_account' => '30101810000000000201',
            'recipient_1' => 'ООО "Не Гарант Пэй"',
            'recipient_account' => '40702810590030001200', // изменен
            'recieverdealaccount' => '40702810590030001200', // изменен
            'recipient_inn' => '7813271860', // изменен
            'recipient_kpp' => '781301000', // изменен
            'recipient_bik' => '044030799', // изменен
            'recipient_bank_1' => 'ПАО "БАНК "САНКТ-ПЕТЕРБУРГ"',
            'recipient_corr_account' => '30101810900000000799', // изменен
            'payment_type' => '01',
            'priority' => '5',
            'payment_purpose' => 'Оплата по сделке GP320T'
        ];
    }

}