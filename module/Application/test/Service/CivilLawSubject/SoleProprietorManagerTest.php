<?php

namespace ApplicationTest\Service\CivilLawSubject;

use Application\Entity\NdsType;
use Application\Service\CivilLawSubject\SoleProprietorManager;
use Application\Service\UserManager;
use Core\Exception\LogicException;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Entity\SoleProprietor;
use Application\Entity\CivilLawSubject;
use Zend\Stdlib\ArrayUtils;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;

class SoleProprietorManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var SoleProprietorManager
     */
    private $soleProprietorManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../../config/application.config.php',
                    include __DIR__ . '/../../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->soleProprietorManager = $serviceManager->get(SoleProprietorManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);

        $config = $this->getApplicationConfig();
        $user = $this->userManager->selectUserByLogin($config['tests']['user_login']);
        $this->user = $user;

        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @param null $params
     * @return SoleProprietor|object
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createSoleProprietor($params = null)
    {
        $saveParams = $params;
        if ($params === null) {
            /** @var NdsType $ndsType */
            $ndsType = $this->entityManager->getRepository(NdsType::class)
                ->findOneBy(['name' => '18%']);
            $saveParams = [
                'name' => 'ИП New Sole Proprietor',
                'first_name' => 'New',
                'secondary_name' => 'Sole',
                'last_name' => 'Proprietor',
                'birth_date' => date('d.m.Y'),
                'inn' => '683103646744',
                'nds_type' => $ndsType->getType()
            ];
        }

        return $this->civilLawSubjectManager->createSoleProprietor($this->user, $saveParams);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreate()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        $params = [
            'name' => 'ИП New Sole Proprietor',
            'first_name' => 'New',
            'secondary_name' => 'Sole',
            'last_name' => 'Proprietor',
            'birth_date' => date('d.m.Y'),
            'inn' => '683103646744',
            'nds_type' => $ndsType->getType(),
        ];

        $result = $this->createSoleProprietor($params);

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(['name' => 'ИП New Sole Proprietor', 'lastName' => 'Proprietor']);

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $soleProprietor->getCivilLawSubject();

        $this->assertEquals(SoleProprietor::class, \get_class($result));
        $this->assertEquals($result->getId(), $soleProprietor->getId());
        $this->assertEquals($this->user->getId(), $soleProprietor->getCivilLawSubject()->getUser()->getId());
        $this->assertEquals($params['name'], $soleProprietor->getName());
        $this->assertEquals($params['first_name'], $soleProprietor->getFirstName());
        $this->assertEquals($params['secondary_name'], $soleProprietor->getSecondaryName());
        $this->assertEquals($params['last_name'], $soleProprietor->getLastName());
        $this->assertEquals($params['inn'], $soleProprietor->getInn());
        $this->assertEquals($params['birth_date'], $soleProprietor->getBirthDate()->format('d.m.Y'));
        $this->assertEquals($params['nds_type'], $civilLawSubject->getNdsType()->getType());
    }

    /**
     * Не указан nds_type
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateWithoutNdsFail()
    {
        try {
            $result = $this->createSoleProprietor([]);
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals(LogicException::NDS_TYPE_NOT_SPECIFIED_MESSAGE, $e->getMessage());
    }

    /**
     * Указан несуществуюбщий nds_type
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateWithWrongNdsFail()
    {
        try {
            $result = $this->createSoleProprietor(['nds_type' => 999999]); // несуществуюбщий
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals(LogicException::NDS_TYPE_NOT_FOUND_MESSAGE, $e->getMessage());
    }

    /**
     * Указан корректный nds_type, но других параметров нет
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateWithNdsAndWithoutOtherFail()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        try {
            $result = $this->createSoleProprietor(['nds_type' => $ndsType->getType()]);
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals('Missing required keys: name, first_name, secondary_name, last_name, birth_date, inn', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testUpdate()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->createSoleProprietor();

        $params = [
            'name' => 'ИП Updated Sole Proprietor',
            'first_name' => 'Updated',
            'secondary_name' => 'Sole',
            'last_name' => 'Proprietor',
            'birth_date' => date('d.m.Y'),
            'inn' => '683103646744',
            'nds_type' => $ndsType->getType(),
        ];

        $result = $this->civilLawSubjectManager->updateSoleProprietor($soleProprietor, $params);

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $soleProprietor->getCivilLawSubject();

        $this->assertEquals(SoleProprietor::class, get_class($result));
        $this->assertEquals($result->getId(), $result->getId());
        $this->assertEquals($this->user->getId(), $result->getCivilLawSubject()->getUser()->getId());
        $this->assertEquals($params['name'], $result->getName());
        $this->assertEquals($params['first_name'], $result->getFirstName());
        $this->assertEquals($params['secondary_name'], $result->getSecondaryName());
        $this->assertEquals($params['last_name'], $result->getLastName());
        $this->assertEquals($params['birth_date'], $result->getBirthDate()->format('d.m.Y'));
        $this->assertEquals($params['inn'], $result->getInn());
        $this->assertEquals($params['nds_type'], $civilLawSubject->getNdsType()->getType());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     */
    public function testUpdateWithoutNdsFail()
    {
        try {
            /** @var SoleProprietor $soleProprietor */
            $soleProprietor = $this->createSoleProprietor();
            $result = $this->civilLawSubjectManager->updateSoleProprietor($soleProprietor, []);
            $e = null;
        } catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals(LogicException::NDS_TYPE_NOT_SPECIFIED_MESSAGE, $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     */
    public function testUpdateWithWrongNdsFail()
    {
        try {
            /** @var SoleProprietor $soleProprietor */
            $soleProprietor = $this->createSoleProprietor();
            $result = $this->civilLawSubjectManager
                ->updateSoleProprietor($soleProprietor, ['nds_type' => 999999]);
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals(LogicException::NDS_TYPE_NOT_FOUND_MESSAGE, $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     */
    public function testWithNdsAndWithoutOtherFail()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        try {
            /** @var SoleProprietor $soleProprietor */
            $soleProprietor = $this->createSoleProprietor();
            $result = $this->civilLawSubjectManager
                ->updateSoleProprietor($soleProprietor, ['nds_type' => $ndsType->getType()]);
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals('Missing required keys: name, first_name, secondary_name, last_name, birth_date, inn', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     *
     * @throws LogicException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testRemove()
    {
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->createSoleProprietor();

        $sole_proprietor_id = $soleProprietor->getId();

        $civil_law_subject_id = $soleProprietor->getCivilLawSubject()->getId();

        $this->civilLawSubjectManager->remove($soleProprietor->getCivilLawSubject());

        $findCivilLawSubject = $this->entityManager
            ->getRepository(CivilLawSubject::class)->find($civil_law_subject_id);

        $findSoleProprietor = $this->entityManager
            ->getRepository(SoleProprietor::class)->find($sole_proprietor_id);

        $this->assertNull($soleProprietor->getId());
        $this->assertEquals(null, $findCivilLawSubject);
        $this->assertEquals(null, $findSoleProprietor);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     *
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testClone()
    {
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->createSoleProprietor();

        $civilLawSubject = $this->civilLawSubjectManager->create($this->user, $is_pattern = 0);

        $newSoleProprietor = $this->soleProprietorManager->clone($soleProprietor, $civilLawSubject);

        $this->assertEquals(true, $soleProprietor->getCivilLawSubject()->getIsPattern());
        $this->assertEquals(false, $newSoleProprietor->getCivilLawSubject()->getIsPattern());
        $this->assertNotEquals($newSoleProprietor->getId(), $soleProprietor->getId());
        $this->assertEquals($newSoleProprietor->getName(), $soleProprietor->getName());
        $this->assertEquals($newSoleProprietor->getFirstName(), $soleProprietor->getFirstName());
        $this->assertEquals($newSoleProprietor->getSecondaryName(), $soleProprietor->getSecondaryName());
        $this->assertEquals($newSoleProprietor->getLastName(), $soleProprietor->getLastName());
        $this->assertEquals($newSoleProprietor->getBirthDate(), $soleProprietor->getBirthDate());
        $this->assertEquals($newSoleProprietor->getInn(), $soleProprietor->getInn());
    }
}