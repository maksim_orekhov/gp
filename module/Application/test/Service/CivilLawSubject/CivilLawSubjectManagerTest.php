<?php

namespace ApplicationTest\Service\CivilLawSubject;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\DealType;
use Application\Entity\NdsType;
use Application\Entity\PaymentMethod;
use Application\Entity\SoleProprietor;
use Application\Entity\Token;
use Application\Entity\User;
use ApplicationTest\Bootstrap;
use ApplicationTest\Controller\SoleProprietorControllerTest;
use Core\Exception\LogicException;
use Core\Service\Base\BaseAuthManager;
use Core\Service\ORMDoctrineUtil;
use Zend\Session\SessionManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\UserManager;
use Application\Entity\NaturalPersonPassport;
use Application\Entity\NaturalPersonDriverLicense;
use Application\Entity\CivilLawSubject;

/**
 * Class CivilLawSubjectManagerTest
 * @package ApplicationTest\Service\CivilLawSubject
 */
class CivilLawSubjectManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var NaturalPersonDocumentManager
     */
    private $naturalPersonDocumentManager;

    /**
     * @var NaturalPersonManager
     */
    private $naturalPersonManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->naturalPersonDocumentManager = $serviceManager->get(NaturalPersonDocumentManager::class);
        $this->naturalPersonManager = $serviceManager->get(NaturalPersonManager::class);
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];
        $user = $this->userManager->selectUserByLogin($config['tests']['user_login']);
        $this->user = $user;

        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;
        $this->userManager = null;
        $this->baseRoleManager = null;
        $this->fileManager = null;

        gc_collect_cycles();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testCreate()
    {
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user);

        $this->assertEquals($this->user->getId(), $civilLawSubject->getUser()->getId());
        $this->assertEquals(true, $civilLawSubject->getIsPattern());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testCreateIsNotPattern()
    {
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user, false);

        $this->assertEquals($this->user->getId(), $civilLawSubject->getUser()->getId());
        $this->assertEquals(false, $civilLawSubject->getIsPattern());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testCreateNaturalPerson()
    {
        $params = [
            'first_name' => 'Александр',
            'secondary_name' => 'Александрович',
            'last_name' => 'Александров',
            'birth_date' => date('Y-m-d'),
        ];

        $person = $this->civilLawSubjectManager->createNaturalPerson($this->user, $params);
        $civilLawSubject = $person->getCivilLawSubject();

        $this->assertEquals($this->user->getId(), $civilLawSubject->getUser()->getId());
        $this->assertEquals(true, $civilLawSubject->getIsPattern());
        $this->assertEquals($params['first_name'], $person->getFirstName());
        $this->assertEquals($params['secondary_name'], $person->getSecondaryName());
        $this->assertEquals($params['last_name'], $person->getLastName());
        $this->assertEquals(new \DateTime($params['birth_date']), $person->getBirthDate());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testCreateLegalEntity()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        $params = [
            'name' => 'ООО Симпл Технолоджи',
            'legal_address' => 'г. Москва, ул. Авангардная, д. 3 пом. II офис 1407',
            'inn' => '7743211967',
            'kpp' => '774301001',
            'nds_type' => $ndsType->getType()
        ];
        $legal = $this->civilLawSubjectManager->createLegalEntity($this->user, $params);
        $civilLawSubject = $legal->getCivilLawSubject();

        $this->assertEquals($this->user->getId(), $civilLawSubject->getUser()->getId());
        $this->assertEquals(true, $civilLawSubject->getIsPattern());
        $this->assertEquals($params['name'], $legal->getName());
        $this->assertEquals($params['legal_address'], $legal->getLegalAddress());
        $this->assertEquals($params['inn'], $legal->getInn());
        $this->assertEquals($params['kpp'], $legal->getKpp());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testClone()
    {
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user);

        $cloneCivilLawSubject = $this->civilLawSubjectManager->clone($civilLawSubject);
        $this->assertEquals(false, $cloneCivilLawSubject->getIsPattern());
        $this->assertNotEquals($civilLawSubject->getId(), $cloneCivilLawSubject->getId());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Throwable
     */
    public function testCloneWithAssociation()
    {
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user);

        $params = [
            'first_name' => 'Александр',
            'secondary_name' => 'Александрович',
            'last_name' => 'Александров',
            'birth_date' => date('Y-m-d'),
        ];
        $person = $this->naturalPersonManager->create($civilLawSubject, $params);

        $saveParamsPassport = [
            'passport_serial_number' => '11 00 012345',
            'date_issued' => date('Y-m-d'),
            'passport_issue_organisation' => 'ОУФМС',
            'date_expired' => date('Y-m-d'),
            'file' => [
                'tmp_name'  => './module/Application/test/files/image_file_for_test.png',
                'name'      => "image_file_for_test.png",
                'type' => 'image/png',
                'size' => filesize('./module/Application/test/files/image_file_for_test.png'),
                'path'      => NaturalPersonPassport::PATH_FILES,
            ],
        ];
        $documentPassport = $this->naturalPersonDocumentManager->create($person, NaturalPersonPassport::class, $saveParamsPassport);
        $passport = $this->naturalPersonDocumentManager->get($documentPassport);

        $saveParamsDriverLicense = [
            'date_issued' => date('Y-m-d'),
            'file' => [
                'tmp_name'  => './module/Application/test/files/image_file_for_test.png',
                'name'      => "image_file_for_test.png",
                'type' => 'image/png',
                'size' => filesize('./module/Application/test/files/image_file_for_test.png'),
                'path'      => NaturalPersonDriverLicense::PATH_FILES,
            ],
        ];
        $documentDriverLicense = $this->naturalPersonDocumentManager->create($person, NaturalPersonDriverLicense::class, $saveParamsDriverLicense);
        $driverLicense = $this->naturalPersonDocumentManager->get($documentDriverLicense);

        $cloneCivilLawSubject = $this->civilLawSubjectManager->clone($civilLawSubject);
        $cloneNaturalPerson = $cloneCivilLawSubject->getNaturalPerson();
        $cloneDocuments = $this->naturalPersonManager->getDocuments($cloneNaturalPerson);
        $clonePassport = $cloneDocuments['passport'][0];
        $cloneDriverLicense = $cloneDocuments['driver_license'][0];

        $this->assertEquals(false, $cloneCivilLawSubject->getIsPattern());
        $this->assertNotEquals($civilLawSubject->getId(), $cloneNaturalPerson->getId());
        $this->assertNotEquals($person->getId(), $cloneCivilLawSubject->getId());
        $this->assertNotEquals($passport->getId(), $clonePassport->getId());
        $this->assertNotEquals($driverLicense->getId(), $cloneDriverLicense->getId());

        $this->civilLawSubjectManager->remove($person->getCivilLawSubject());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Throwable
     */
    public function testRemove()
    {
        /** @var CivilLawSubject $newCivilLawSubject */
        $newCivilLawSubject = $this->createNewCivilLawSubject();
        $newCivilLawSubject_id = $newCivilLawSubject->getId();

        $this->civilLawSubjectManager->remove($newCivilLawSubject);

        $findCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->find($newCivilLawSubject_id);

        $this->assertEquals(null, $findCivilLawSubject);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Throwable
     */
    public function testRemoveWithPaymentMethods()
    {
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user);
        $params = [
            'name' => 'Имя',
            'bank_name' => 'ВТБ',
            'bik' => '111222',
            'account_number' => '222333',
            'payment_recipient_name' => '444555',
            'inn' => '666777',
            'kpp' => '777888',
            'corr_account_number' => '12345678912345678912'
        ];
        $bankTransfer = $this->paymentMethodManager->createPaymentMethodBankTransfer($params, $civilLawSubject);

        $civilLawSubject->addPaymentMethod($bankTransfer->getPaymentMethod());

        $subjectId = $civilLawSubject->getId();
        $userId = $civilLawSubject->getUser()->getId();
        $paymentMethodId = $bankTransfer->getPaymentMethod()->getId();

        $this->civilLawSubjectManager->remove($civilLawSubject);

        $findCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)->find($subjectId);
        $findPaymentMethod = $this->entityManager->getRepository(PaymentMethod::class)->find($paymentMethodId);

        $this->assertNull($findCivilLawSubject);
        $this->assertNull($findPaymentMethod);
    }

    /**
     * Попытка удалить клонированный CLS (не паттерн)
     * Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Throwable
     */
    public function testRemoveNotPattern()
    {
        // Получаем "подписанную" сделку. Клоны CLS сформированы, т.е. уже не паттерны.
        $deal_name = 'Сделка Соглашение достигнуто';
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var DealAgent $customerDealAgent */
        $customerDealAgent = $deal->getCustomer();
        /** @var CivilLawSubject $customerCivilLawSubject */
        $customerCivilLawSubject = $customerDealAgent->getCivilLawSubject();

        $this->assertFalse($customerCivilLawSubject->getIsPattern());

        try {
            $this->civilLawSubjectManager->remove($customerCivilLawSubject);
        }
        catch (\Throwable $t) {

            $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_REMOVAL_NOT_ALLOWED_BY_PATTERN_MESSAGE, $t->getMessage());
        }

        $findCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->find($customerCivilLawSubject->getId());

        $this->assertNotNull($findCivilLawSubject);
    }

    /**
     * Попытка удалить CLS, участвующий в сделке, которую подтвердил DealAgent
     * Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Throwable
     */
    public function testRemoveWithDealAgentConfirm()
    {
        // Получаем сделку, подтверженную с одной стороны
        $deal_name = 'Сделка Фикстура';
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var DealAgent $customerDealAgent */
        $customerDealAgent = $deal->getCustomer();
        /** @var CivilLawSubject $customerCivilLawSubject */
        $customerCivilLawSubject = $customerDealAgent->getCivilLawSubject();

        $this->assertTrue($customerCivilLawSubject->getIsPattern());

        try {
            $this->civilLawSubjectManager->remove($customerCivilLawSubject);
        }
        catch (\Throwable $t) {

            $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_REMOVAL_NOT_ALLOWED_BY_DEAL_BEING_CONFIRMED_MESSAGE, $t->getMessage());
        }

        $findCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->find($customerCivilLawSubject->getId());

        $this->assertNotNull($findCivilLawSubject);
    }

    /**
     * Попытка удалить CLS, указанный в партнерском токене в качестве контрагента
     * Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Throwable
     */
    public function testRemoveRelatedToTokenAsCounteragent()
    {
        /** @var CivilLawSubject $newCivilLawSubject */
        $newCivilLawSubject = $this->createNewCivilLawSubject();
        /** @var Token $token */
        $token = $this->createNewDealToken($newCivilLawSubject);

        $this->assertTrue($newCivilLawSubject->getIsPattern());
        $this->assertNotNull($token->getCounteragentCivilLawSubject());

        try {
            $this->civilLawSubjectManager->remove($newCivilLawSubject);
        }
        catch (\Throwable $t) {

            $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_REMOVAL_NOT_ALLOWED_BY_TOKEN_RELATION_MESSAGE, $t->getMessage());
        }

        $findCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->find($newCivilLawSubject->getId());

        $this->assertNotNull($findCivilLawSubject);
    }

    /**
     * Попытка удалить CLS, указанный в партнерском токене в качестве бенифициара
     * Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Throwable
     */
    public function testRemoveRelatedToTokenAsBeneficiar()
    {
        /** @var CivilLawSubject $сivilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['isPattern' => true]);

        /** @var CivilLawSubject $newCivilLawSubject */
        $beneficiarCivilLawSubject = $this->createNewCivilLawSubject();
        /** @var Token $token */
        $token = $this->createNewDealToken($civilLawSubject, $beneficiarCivilLawSubject);

        $this->assertTrue($beneficiarCivilLawSubject->getIsPattern());
        $this->assertNotNull($token->getCounteragentCivilLawSubject());
        $this->assertNotNull($token->getBeneficiarCivilLawSubject());

        try {
            $this->civilLawSubjectManager->remove($beneficiarCivilLawSubject);
        }
        catch (\Throwable $t) {

            $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_REMOVAL_NOT_ALLOWED_BY_TOKEN_RELATION_MESSAGE, $t->getMessage());
        }

        $findCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->find($beneficiarCivilLawSubject->getId());

        $this->assertNotNull($findCivilLawSubject);
    }

    /**
     * @param $given_login
     * @param $expected
     * @throws \Doctrine\ORM\ORMException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @dataProvider providerOfSoleProprietorForUser
     *
     * @throws \Exception
     */
    public function testGetCivilLawSubjectsAsSeparateArrays($given_login, $expected)
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin($given_login);
        // Залогиниваем пользователя
        $this->login($given_login);

        $civil_law_subjects_arrays = $this->civilLawSubjectManager->getCivilLawSubjectsAsSeparateArrays($user);

        $this->assertArrayHasKey('natural_persons', $civil_law_subjects_arrays);
        $this->assertGreaterThan(0, count($civil_law_subjects_arrays['natural_persons']));
        $natural_person = $civil_law_subjects_arrays['natural_persons'][0];
        $this->assertEquals($expected['natural_persons']['first_name'], $natural_person['first_name']);
        $this->assertEquals($expected['natural_persons']['last_name'], $natural_person['last_name']);

        $this->assertArrayHasKey('sole_proprietors', $civil_law_subjects_arrays);
        $this->assertGreaterThan(0, count($civil_law_subjects_arrays['sole_proprietors']));
        $sole_proprietor = $civil_law_subjects_arrays['sole_proprietors'][0];
        $this->assertEquals($expected['sole_proprietors']['name'], $sole_proprietor['name']);
        $this->assertEquals($expected['sole_proprietors']['first_name'], $sole_proprietor['first_name']);
        $this->assertEquals($expected['sole_proprietors']['last_name'], $sole_proprietor['last_name']);

        $this->assertArrayHasKey('legal_entities', $civil_law_subjects_arrays);
        $this->assertGreaterThan(0, count($civil_law_subjects_arrays['legal_entities']));
        $legal_entity = $civil_law_subjects_arrays['legal_entities'][0];
        $this->assertEquals($expected['legal_entities']['name'], $legal_entity['name']);
    }

    /**
     * @return array
     */
    public function providerOfSoleProprietorForUser(): array
    {
        return [
            ['test', [
                        'natural_persons' => [
                            'first_name' => 'Тест', 'last_name' => 'Тестов'
                        ],
                        'sole_proprietors' => [
                            'name' => SoleProprietorControllerTest::TEST_SOLE_PROPRIETOR_1_NAME, 'first_name' => 'Леонель', 'last_name' => 'Месси'
                        ],
                        'legal_entities' => [
                            'name' => 'ООО Колумбийский сахар'
                        ],
                    ]
            ],
            ['test2', [
                        'natural_persons' => [
                            'first_name' => 'Контрагент', 'last_name' => 'Контрагентов'
                        ],
                        'sole_proprietors' => [
                            'name' => SoleProprietorControllerTest::TEST_SOLE_PROPRIETOR_2_NAME, 'first_name' => 'Криштиану', 'last_name' => 'Роналду'
                        ],
                        'legal_entities' => [
                            'name' => 'ООО Simple Technology'
                        ],
                    ]
            ],
        ];
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $login
     * @return CivilLawSubject
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createNewCivilLawSubject($login = 'test'): CivilLawSubject
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $login));

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '10%']);

        // Create CivilLawSubject
        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern(true);
        $civilLawSubject->setCreated(new \DateTime());
        $civilLawSubject->setNdsType($ndsType);

        $this->entityManager->persist($civilLawSubject);

        // Create SoleProprietor

        $soleProprietor = new SoleProprietor();
        $soleProprietor->setCivilLawSubject($civilLawSubject);
        $soleProprietor->setName('ИП New Sole Proprietor');
        $soleProprietor->setFirstName('New');
        $soleProprietor->setSecondaryName('Sole');
        $soleProprietor->setLastName('Proprietor');
        $soleProprietor->setBirthDate(\DateTime::createFromFormat('!Y-m-d', '1979-07-25'));
        $soleProprietor->setInn('683103646744');

        $civilLawSubject->setSoleProprietor($soleProprietor);

        $this->entityManager->persist($soleProprietor);

        $this->entityManager->flush();

        return $civilLawSubject;
    }

    /**
     * @param $counteragentCivilLawSubject
     * @param null $beneficiarCivilLawSubject
     * @return Token
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createNewDealToken($counteragentCivilLawSubject, $beneficiarCivilLawSubject = null): Token
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test']);

        // Сам токен в этих тестах не проверяется, поэтому - произвольный набор символов
        $encodedToken = 'gghhghhghhfg45454545fgfgfgfgfg';

        $token = new Token();
        $token->setName('Токен для проверки удаления CLS');
        $token->setPartnerIdent('test_cls');
        $token->setCreated(new \DateTime());
        $token->setIsActive(true);
        $token->setAmount(null);
        $token->setCounteragentUser($user);
        $token->setCounteragentCivilLawSubject($counteragentCivilLawSubject);
        $token->setDealName('Сделка с субъектом пользователя test для проверки удаления CLS');
        $token->setDealType($dealType);
        $token->setTokenData($encodedToken);
        if ($beneficiarCivilLawSubject) {
            $token->setBeneficiarCivilLawSubject($beneficiarCivilLawSubject);
        }

        $this->entityManager->persist($token);

        $this->entityManager->flush();

        return $token;
    }
}