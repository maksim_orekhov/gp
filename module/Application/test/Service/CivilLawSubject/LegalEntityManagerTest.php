<?php

namespace ApplicationTest\Service\CivilLawSubject;

use Application\Entity\NdsType;
use Core\Exception\LogicException;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Application\Service\UserManager;
use Application\Entity\LegalEntity;
use Application\Entity\CivilLawSubject;
use Zend\Stdlib\ArrayUtils;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;

class LegalEntityManagerTest extends AbstractHttpControllerTestCase
{
    const TEST_LEGAL_ENTITY_NAME = 'ООО Байдарка и каноэ';

    protected $traceError = true;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var LegalEntityManager
     */
    private $legalEntityManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../../config/application.config.php',
                    include __DIR__ . '/../../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->legalEntityManager = $serviceManager->get(LegalEntityManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);

        $config = $this->getApplicationConfig();
        $user = $this->userManager->selectUserByLogin($config['tests']['user_login']);
        $this->user = $user;

        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @param null $params
     * @return LegalEntity
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createLegalEntity($params = null): LegalEntity
    {
        $saveParams = $params;
        if ($params === null) {
            /** @var NdsType $ndsType */
            $ndsType = $this->entityManager->getRepository(NdsType::class)
                ->findOneBy(['name' => '18%']);
            $saveParams = [
                'name' => self::TEST_LEGAL_ENTITY_NAME,
                'legal_address' => 'г. Москва, ул. Авангардная, д. 3 пом. II офис 1407',
                'inn' => '7743211967',
                'kpp' => '774301001',
                'nds_type' => $ndsType->getType()
            ];
        }

        return $this->civilLawSubjectManager->createLegalEntity($this->user, $saveParams);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreate()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        $params = [
            'name' => self::TEST_LEGAL_ENTITY_NAME,
            'legal_address' => 'г. Москва, ул. Авангардная, д. 3 пом. II офис 1407',
            'inn' => '7743211967',
            'kpp' => '774301001',
            'nds_type' => $ndsType->getType()
        ];
        $result = $this->createLegalEntity($params);

        /** @var LegalEntity $find */
        $find = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(['name' => self::TEST_LEGAL_ENTITY_NAME]);

        $this->assertEquals(LegalEntity::class, get_class($result));
        $this->assertEquals($result->getId(), $find->getId());
        $this->assertEquals($this->user->getId(), $find->getCivilLawSubject()->getUser()->getId());
        $this->assertEquals($params['name'], $find->getName());
        $this->assertEquals($params['legal_address'], $find->getLegalAddress());
        $this->assertEquals($params['inn'], $find->getInn());
        $this->assertEquals($params['kpp'], $find->getKpp());
    }

    /**
     * Не указан nds_type
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithoutNdsFail()
    {
        try {
            $result = $this->createLegalEntity([]);
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals(LogicException::NDS_TYPE_NOT_SPECIFIED_MESSAGE, $e->getMessage());
    }

    /**
     * Указан несуществуюбщий nds_type
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithWrongNdsFail()
    {
        try {
            $result = $this->createLegalEntity(['nds_type' => 999999]); // несуществуюбщий
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals(LogicException::NDS_TYPE_NOT_FOUND_MESSAGE, $e->getMessage());
    }

    /**
     * Указан корректный nds_type, но других параметров нет
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateWithNdsAndWithoutOtherFail()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        try {
            $result = $this->createLegalEntity(['nds_type' => $ndsType->getType()]);
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals('Missing required keys: name, legal_address, inn, kpp', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @throws \Exception
     */
    public function testUpdate()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->createLegalEntity();

        $params = [
            'name' => self::TEST_LEGAL_ENTITY_NAME . ' БЛЭК',
            'legal_address' => 'г. Москва, ул. Авангардная, д. 3 пом. II офис 1407',
            'inn' => '123123',
            'kpp' => '34343434',
            'nds_type' => $ndsType->getType()
        ];
        $result = $this->legalEntityManager->update($legalEntity, $params);

        $this->assertEquals(LegalEntity::class, get_class($result));
        $this->assertEquals($result->getId(), $result->getId());
        $this->assertEquals($this->user->getId(), $result->getCivilLawSubject()->getUser()->getId());
        $this->assertEquals($params['name'], $result->getName());
        $this->assertEquals($params['legal_address'], $result->getLegalAddress());
        $this->assertEquals($params['inn'], $result->getInn());
        $this->assertEquals($params['kpp'], $result->getKpp());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     */
    public function testUpdateWithoutNdsFail()
    {
        try {
            /** @var LegalEntity $legalEntity */
            $legalEntity = $this->createLegalEntity();
            $result = $this->civilLawSubjectManager->updateLegalEntity($legalEntity, []);
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals(LogicException::NDS_TYPE_NOT_SPECIFIED_MESSAGE, $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     */
    public function testUpdateWithWrongNdsFail()
    {
        try {
            /** @var LegalEntity $legalEntity */
            $legalEntity = $this->createLegalEntity();
            $result = $this->civilLawSubjectManager
                ->updateLegalEntity($legalEntity, ['nds_type' => 999999]);
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals(LogicException::NDS_TYPE_NOT_FOUND_MESSAGE, $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     */
    public function testWithNdsAndWithoutOtherFail()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        try {
            /** @var LegalEntity $legalEntity */
            $legalEntity = $this->createLegalEntity();
            $result = $this->civilLawSubjectManager
                ->updateLegalEntity($legalEntity, ['nds_type' => $ndsType->getType()]);
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Core\Exception\LogicException', get_class($e));
        $this->assertEquals('Missing required keys: name, legal_address, inn, kpp', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @group legal-entity
     */
    public function testRemove()
    {
        $legalEntity = $this->createLegalEntity();
        $legalId = $legalEntity->getId();
        $subjectId = $legalEntity->getCivilLawSubject()->getId();
        $this->civilLawSubjectManager->remove($legalEntity->getCivilLawSubject());

        $findCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)->find($subjectId);
        $findLegalEntity = $this->entityManager->getRepository(LegalEntity::class)->find($legalId);

        $this->assertEquals(null, $legalEntity->getId());
        $this->assertEquals(null, $findCivilLawSubject);
        $this->assertEquals(null, $findLegalEntity);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @throws \Exception
     */
    public function testClone()
    {
        $legalEntity = $this->createLegalEntity();
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user, $is_pattern = 0);
        $newLegalEntity = $this->legalEntityManager->clone($legalEntity, $civilLawSubject);

        $this->assertEquals(true, $legalEntity->getCivilLawSubject()->getIsPattern());
        $this->assertEquals(false, $newLegalEntity->getCivilLawSubject()->getIsPattern());
        $this->assertNotEquals($newLegalEntity->getId(), $legalEntity->getId());
        $this->assertEquals($newLegalEntity->getName(), $legalEntity->getName());
        $this->assertEquals($newLegalEntity->getLegalAddress(), $legalEntity->getLegalAddress());
        #$this->assertEquals($newLegal->getPhone(), $legal->getPhone());
        $this->assertEquals($newLegalEntity->getInn(), $legalEntity->getInn());
        $this->assertEquals($newLegalEntity->getKpp(), $legalEntity->getKpp());
        #$this->assertEquals($newLegal->getOgrn(), $legal->getOgrn());
    }
}