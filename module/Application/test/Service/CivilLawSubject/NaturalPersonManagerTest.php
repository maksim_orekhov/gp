<?php

namespace ApplicationTest\Service\CivilLawSubject;

use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use Application\Service\UserManager;
use Application\Entity\NaturalPerson;
use Application\Entity\CivilLawSubject;
use Zend\Stdlib\ArrayUtils;

class NaturalPersonManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var NaturalPersonManager
     */
    private $naturalPersonManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../../config/application.config.php',
                    include __DIR__ . '/../../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->naturalPersonManager = $serviceManager->get(NaturalPersonManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);

        $config = $this->getApplicationConfig();
        $user = $this->userManager->getUserByLogin($config['tests']['user_login']);
        $this->user = $user;

        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @param null $params
     * @return NaturalPerson
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createNaturalPerson($params = null): NaturalPerson
    {
        $saveParams = $params;
        if ($params === null) {
            $saveParams = [
                'first_name' => 'Александр',
                'secondary_name' => 'Александрович',
                'last_name' => 'Александров',
                'birth_date' => date('Y-m-d'),
            ];
        }

        return $this->civilLawSubjectManager->createNaturalPerson($this->user, $saveParams);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @group natural-person
     */
    public function testCreate()
    {
        $params = [
            'first_name' => 'Александр',
            'secondary_name' => 'Александрович',
            'last_name' => 'Александров',
            'birth_date' => date('Y-m-d'),
        ];
        $result = $this->createNaturalPerson($params);

        $personId = $this->user->getCivilLawSubjects()->last()->getNaturalPerson()->getId();
        $find = $this->entityManager->getRepository(NaturalPerson::class)->find($personId);

        $this->assertEquals(NaturalPerson::class, get_class($result));
        $this->assertEquals($result->getId(), $find->getId());
        $this->assertEquals($this->user->getId(), $find->getCivilLawSubject()->getUser()->getId());
        $this->assertEquals($params['first_name'], $find->getFirstName());
        $this->assertEquals($params['secondary_name'], $find->getSecondaryName());
        $this->assertEquals($params['last_name'], $find->getLastName());
        $this->assertEquals(new \DateTime($params['birth_date']), $find->getBirthDate());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group natural-person
     */
    public function testCreateFail()
    {
        try {
            $result = $this->createNaturalPerson([]);
            $e = null;
        }
        catch (\Exception $e) {}

        $this->assertEquals('Exception', get_class($e));
        $this->assertEquals('Missing required keys: first_name, secondary_name, last_name, birth_date', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testUpdate()
    {
        $naturalPerson = $this->createNaturalPerson();

        $params = [
            'first_name' => 'Тест',
            'secondary_name' => 'Тестович',
            'last_name' => 'Тестов',
            'birth_date' => date('Y-m-d'),
        ];
        $result = $this->naturalPersonManager->update($naturalPerson, $params);

        $this->assertEquals(NaturalPerson::class, get_class($result));
        $this->assertEquals($result->getId(), $result->getId());
        $this->assertEquals($this->user->getId(), $result->getCivilLawSubject()->getUser()->getId());
        $this->assertEquals($params['first_name'], $result->getFirstName());
        $this->assertEquals($params['secondary_name'], $result->getSecondaryName());
        $this->assertEquals($params['last_name'], $result->getLastName());
        $this->assertEquals(new \DateTime($params['birth_date']), $result->getBirthDate());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group natural-person
     */
    public function testUpdateFail()
    {
        try {
            $person = $this->createNaturalPerson();
            $result = $this->naturalPersonManager->update($person, []);
            $e = null;
        } catch (\Exception $e) {}

        $this->assertEquals('Exception', get_class($e));
        $this->assertEquals('Missing required keys: first_name, secondary_name, last_name, birth_date', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @group natural-person
     */
    public function testRemove()
    {
        $person = $this->createNaturalPerson();
        $personId = $person->getId();
        $subjectId = $person->getCivilLawSubject()->getId();
        $this->civilLawSubjectManager->remove($person->getCivilLawSubject());

        $findCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)->find($subjectId);
        $findNaturalPerson = $this->entityManager->getRepository(NaturalPerson::class)->find($personId);

        $this->assertEquals(null, $person->getId());
        $this->assertEquals(null, $findCivilLawSubject);
        $this->assertEquals(null, $findNaturalPerson);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testClone()
    {
        $person = $this->createNaturalPerson();
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user, $is_pattern = 0);
        $newPerson = $this->naturalPersonManager->clone($person, $civilLawSubject);

        $this->assertEquals(true, $person->getCivilLawSubject()->getIsPattern());
        $this->assertEquals(false, $newPerson->getCivilLawSubject()->getIsPattern());
        $this->assertNotEquals($newPerson->getId(), $person->getId());
        $this->assertEquals($newPerson->getFirstName(), $person->getFirstName());
        $this->assertEquals($newPerson->getSecondaryName(), $person->getSecondaryName());
        $this->assertEquals($newPerson->getLastName(), $person->getLastName());
        $this->assertEquals($newPerson->getBirthDate(), $person->getBirthDate());
    }
}