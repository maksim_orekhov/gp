<?php

namespace ApplicationTest\Service\CivilLawSubject;

use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Entity\NaturalPersonDocument;
use Application\Entity\NaturalPersonDriverLicense;
use Application\Entity\User;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use Application\Service\CivilLawSubject\NaturalPersonManager;
use Application\Service\UserManager;
use Application\Entity\NaturalPersonPassport;
use Application\Entity\NaturalPerson;
use Zend\Stdlib\ArrayUtils;

class NaturalPersonDocumentManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var NaturalPerson
     */
    private $person;

    /**
     * @var User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var NaturalPersonDocumentManager
     */
    private $naturalPersonDocumentManager;

    /**
     * @var NaturalPersonManager
     */
    private $naturalPersonManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    private $main_upload_folder;

    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../../config/application.config.php',
                    include __DIR__ . '/../../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->em = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->naturalPersonDocumentManager = $serviceManager->get(NaturalPersonDocumentManager::class);
        $this->naturalPersonManager = $serviceManager->get(NaturalPersonManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);

        $this->em->getConnection()->beginTransaction();

        $userManager = $serviceManager->get(UserManager::class);

        $config = $this->getApplicationConfig();
        $this->main_upload_folder = $config['file-management']['main_upload_folder'];
        $this->user = $userManager->getUserByLogin($config['tests']['user_login']);
        $this->person = $this->civilLawSubjectManager->createNaturalPerson($this->user, [
            'first_name' => 'Александр',
            'secondary_name' => 'Александрович',
            'last_name' => 'Александров',
            'birth_date' => date('Y-m-d'),
        ]);
    }

    public function tearDown()
    {
        $this->civilLawSubjectManager->remove($this->person->getCivilLawSubject());

        $this->em->getConnection()->rollback();
        $this->em->getConnection()->close();

        parent::tearDown();
    }

    /**
     * @param $params
     * @return NaturalPersonDocument
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    private function createDocumentPassport($params = null)
    {
        $saveParams = $params;
        if ($params === null) {
            $saveParams = [
                'passport_serial_number' => '11 00 012345',
                'date_issued' => date('Y-m-d'),
                'passport_issue_organisation' => 'ОУФМС',
                'date_expired' => date('Y-m-d'),
                'file' => $this->getfileData(),
            ];
        }

        return $this->naturalPersonDocumentManager->create($this->person, NaturalPersonPassport::class, $saveParams);
    }

    /**
     * @param $params
     * @return NaturalPersonDocument
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    private function createDocumentDriverLicense($params = null)
    {
        $saveParams = $params;
        if ($params === null) {
            $saveParams = [
                'date_issued' => date('Y-m-d'),
                'file' => $this->getFileData(),
            ];
        }

        return $this->naturalPersonDocumentManager->create($this->person, NaturalPersonDriverLicense::class, $saveParams);
    }

    private function getFileData()
    {
        return [
            'tmp_name'  => './module/Application/test/files/image_file_for_test.png',
            'name'      => "image_file_for_test.png",
            'type' => 'image/png',
            'size' => filesize('./module/Application/test/files/image_file_for_test.png'),
            'path'      => NaturalPersonPassport::PATH_FILES,
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreatePassport()
    {
        $paramDocs = [
            'passport_serial_number' => '11 00 012345',
            'date_issued' => date('Y-m-d'),
            'passport_issue_organisation' => 'ОУФМС',
            'date_expired' => date('Y-m-d'),
            'file' => $this->getfileData(),
        ];
        $result = $this->createDocumentPassport($paramDocs);
        $passport = $result->getNaturalPersonPassport();

        $find = $this->em->getRepository(NaturalPersonDocument::class)
            ->find($result->getId());
        $findPassport = $find->getNaturalPersonPassport();

        $types = $this->naturalPersonDocumentManager::getTypes();

        $this->assertEquals('Application\Entity\NaturalPersonDocument', get_class($result));
        $this->assertEquals($types[NaturalPersonPassport::class], $result->getNaturalPersonDocumentType()->getName());
        $this->assertEquals(true, $result->isActive());
        $this->assertEquals($paramDocs['passport_serial_number'], $passport->getPassportSerialNumber());
        $this->assertEquals(new \DateTime($paramDocs['date_issued']), $passport->getDateIssued());
        $this->assertEquals($paramDocs['passport_issue_organisation'], $passport->getPassportIssueOrganisation());
        $this->assertEquals(new \DateTime($paramDocs['date_expired']), $passport->getDateExpired());
        $this->assertEquals($paramDocs['file']['name'], $passport->getFiles()->first()->getOriginName());


        $this->assertEquals(true, $find->isActive());
        $this->assertEquals($paramDocs['passport_serial_number'], $findPassport->getPassportSerialNumber());
        $this->assertEquals(new \DateTime($paramDocs['date_issued']), $findPassport->getDateIssued());
        $this->assertEquals($paramDocs['passport_issue_organisation'], $findPassport->getPassportIssueOrganisation());
        $this->assertEquals(new \DateTime($paramDocs['date_expired']), $findPassport->getDateExpired());
        $this->assertEquals($paramDocs['file']['name'], $findPassport->getFiles()->first()->getOriginName());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreatePassportFail()
    {
        try {
            $result = $this->createDocumentPassport([]);
            $e = null;
        } catch (\Exception $e) {}

        $this->assertEquals('Exception', get_class($e));
        $this->assertEquals('Missing required keys: passport_serial_number, date_issued, passport_issue_organisation, date_expired, file', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreateDriverLicense()
    {
        $paramDocs = [
            'date_issued' => date('Y-m-d'),
            'file' => $this->getFileData(),
        ];
        $result = $this->createDocumentDriverLicense($paramDocs);
        $driverLicense = $result->getNaturalPersonDriverLicense();

        $find = $this->em->getRepository(NaturalPersonDocument::class)
            ->find($result->getId());
        $findDriverLicense = $find->getNaturalPersonDriverLicense();

        $types = $this->naturalPersonDocumentManager::getTypes();

        $this->assertEquals('Application\Entity\NaturalPersonDocument', get_class($result));
        $this->assertEquals($types[NaturalPersonDriverLicense::class], $result->getNaturalPersonDocumentType()->getName());
        $this->assertEquals(true, $result->isActive());
        $this->assertEquals(new \DateTime($paramDocs['date_issued']), $driverLicense->getDateIssued());
        $this->assertEquals($paramDocs['file']['name'], $driverLicense->getFiles()->first()->getOriginName());


        $this->assertEquals(true, $find->isActive());
        $this->assertEquals(new \DateTime($paramDocs['date_issued']), $findDriverLicense->getDateIssued());
        $this->assertEquals($paramDocs['file']['name'], $findDriverLicense->getFiles()->first()->getOriginName());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreateDriverLicenseFail()
    {
        try {
            $result = $this->createDocumentDriverLicense([]);
            $e = null;
        } catch (\Exception $e) {}

        $this->assertEquals('Exception', get_class($e));
        $this->assertEquals('Missing required keys: date_issued, file', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUploadFile()
    {
        $document = $this->createDocumentPassport();

        $uploadDir = "./".$this->main_upload_folder.NaturalPersonPassport::PATH_FILES;
        $fileName = $document->getNaturalPersonPassport()->getFiles()->first()->getName();

        $this->assertFileExists($uploadDir.'/'.$fileName);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testAssociationGet()
    {
        $person = $this->person;
        $this->createDocumentPassport();
        $this->createDocumentPassport();
        $examplePassport = $this->createDocumentPassport();

        $exampleDriverLicense = $this->createDocumentDriverLicense();
        $this->createDocumentDriverLicense();

        $findDocuments = $this->naturalPersonManager->getDocuments($person);
        $typePassport = $this->naturalPersonDocumentManager::getType(NaturalPersonPassport::class);
        $typeDriverLicense = $this->naturalPersonDocumentManager::getType(NaturalPersonDriverLicense::class);

        $this->assertEquals(5, count($person->getNaturalPersonDocument()));
        $this->assertEquals(2, count($findDocuments));
        $this->assertEquals(3, count($findDocuments[$typePassport]));
        $this->assertEquals(2, count($findDocuments[$typeDriverLicense]));
        $this->assertEquals($person->getId(), $examplePassport->getNaturalPerson()->getId());
        $this->assertEquals($person->getId(), $exampleDriverLicense->getNaturalPerson()->getId());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSetLastActive()
    {
        $person = $this->person;
        $this->createDocumentPassport();
        $this->createDocumentPassport();
        $this->createDocumentPassport();

        $this->createDocumentDriverLicense();
        $this->createDocumentDriverLicense();

        $findDocuments = $this->naturalPersonManager->getDocuments($person);
        $typePassport = $this->naturalPersonDocumentManager::getType(NaturalPersonPassport::class);
        $typeDriverLicense = $this->naturalPersonDocumentManager::getType(NaturalPersonDriverLicense::class);

        $this->assertEquals(false, $findDocuments[$typePassport][0]->getNaturalPersonDocument()->isActive());
        $this->assertEquals(false, $findDocuments[$typePassport][1]->getNaturalPersonDocument()->isActive());
        $this->assertEquals(true,  $findDocuments[$typePassport][2]->getNaturalPersonDocument()->isActive());

        $this->assertEquals(false, $findDocuments[$typeDriverLicense][0]->getNaturalPersonDocument()->isActive());
        $this->assertEquals(true,  $findDocuments[$typeDriverLicense][1]->getNaturalPersonDocument()->isActive());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdatePassport()
    {
        $paramDocsAdd = [
            'passport_serial_number' => '11 00 012345',
            'date_issued' => date('Y-m-d'),
            'passport_issue_organisation' => 'ОУФМС',
            'date_expired' => date('Y-m-d'),
            'file' => $this->getFileData(),
        ];
        $passportAddFirst = $this->createDocumentPassport($paramDocsAdd);
        $passportAddLast = $this->createDocumentPassport($paramDocsAdd);

        $paramDocs = [
            'passport_serial_number' => '00 00 000000',
            'date_issued' => date('Y-m-d'),
            'passport_issue_organisation' => 'ОУФМС2',
            'date_expired' => date('Y-m-d'),
            'file' => $this->getFileData(),
        ];
        $result = $this->naturalPersonDocumentManager->update($passportAddLast, $paramDocs);
        $passport = $result->getNaturalPersonPassport();

        $this->assertEquals(true, $result->isActive());
        $this->assertEquals($paramDocs['passport_serial_number'], $passport->getPassportSerialNumber());
        $this->assertEquals(new \DateTime($paramDocs['date_issued']), $passport->getDateIssued());
        $this->assertEquals($paramDocs['passport_issue_organisation'], $passport->getPassportIssueOrganisation());
        $this->assertEquals(new \DateTime($paramDocs['date_expired']), $passport->getDateExpired());
        $this->assertEquals($paramDocs['file']['name'], $passport->getFiles()->first()->getOriginName());

        $this->assertEquals(false, $passportAddFirst->isActive());
        $this->assertEquals($paramDocsAdd['passport_serial_number'], $passportAddFirst->getNaturalPersonPassport()->getPassportSerialNumber());
        $this->assertEquals(new \DateTime($paramDocsAdd['date_issued']), $passportAddFirst->getNaturalPersonPassport()->getDateIssued());
        $this->assertEquals($paramDocsAdd['passport_issue_organisation'], $passportAddFirst->getNaturalPersonPassport()->getPassportIssueOrganisation());
        $this->assertEquals(new \DateTime($paramDocsAdd['date_expired']), $passportAddFirst->getNaturalPersonPassport()->getDateExpired());
        $this->assertEquals($paramDocsAdd['file']['name'], $passportAddFirst->getNaturalPersonPassport()->getFiles()->first()->getOriginName());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdatePassportFail()
    {
        try {
            $document = $this->createDocumentPassport();
            $result = $this->naturalPersonDocumentManager->update($document, []);
            $e = null;
        } catch (\Exception $e) {}

        $this->assertEquals('Exception', get_class($e));
        $this->assertEquals('Missing required keys: passport_serial_number, date_issued, passport_issue_organisation, date_expired', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateDriverLicense()
    {
        $paramDocsAdd = [
            'date_issued' => date('Y-m-d'),
            'file' => $this->getFileData(),
        ];
        $driverLicenseAddFirst = $this->createDocumentDriverLicense($paramDocsAdd);
        $driverLicenseAddLast = $this->createDocumentDriverLicense($paramDocsAdd);

        $paramDocs = [
            'date_issued' => date('Y-m-d', strtotime("2009-09-30 20:24:00")),
            'file' => $this->getFileData(),
        ];
        $result = $this->naturalPersonDocumentManager->update($driverLicenseAddLast, $paramDocs);
        $driverLicense = $result->getNaturalPersonDriverLicense();

        $this->assertEquals(true, $result->isActive());
        $this->assertEquals(new \DateTime($paramDocs['date_issued']), $driverLicense->getDateIssued());
        $this->assertEquals($paramDocs['file']['name'], $driverLicense->getFiles()->first()->getOriginName());

        $this->assertEquals(false, $driverLicenseAddFirst->isActive());
        $this->assertEquals(new \DateTime($paramDocsAdd['date_issued']), $driverLicenseAddFirst->getNaturalPersonDriverLicense()->getDateIssued());
        $this->assertEquals($paramDocsAdd['file']['name'], $driverLicenseAddFirst->getNaturalPersonDriverLicense()->getFiles()->first()->getOriginName());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateDriverLicenseFail()
    {
        try {
            $document = $this->createDocumentDriverLicense();
            $result = $this->naturalPersonDocumentManager->update($document, []);
            $e = null;
        } catch (\Exception $e) {}

        $this->assertEquals('Exception', get_class($e));
        $this->assertEquals('Missing required keys: date_issued', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testRemove()
    {
        $person = $this->person;
        $doc1 = $this->createDocumentPassport();
        $doc2 = $this->createDocumentPassport();
        $doc3 = $this->createDocumentPassport();

        $doc4 = $this->createDocumentDriverLicense();
        $doc5 = $this->createDocumentDriverLicense();

        $idExample = $doc3->getId();

        $this->naturalPersonDocumentManager->remove($doc1);
        $this->assertEquals(4, count($person->getNaturalPersonDocument()));
        $this->naturalPersonDocumentManager->remove($doc2);
        $this->assertEquals(3, count($person->getNaturalPersonDocument()));
        $this->naturalPersonDocumentManager->remove($doc3);
        $this->assertEquals(2, count($person->getNaturalPersonDocument()));
        $this->naturalPersonDocumentManager->remove($doc4);
        $this->assertEquals(1, count($person->getNaturalPersonDocument()));
        $this->naturalPersonDocumentManager->remove($doc5);
        $this->assertEquals(0, count($person->getNaturalPersonDocument()));

        $find = $this->em->getRepository(NaturalPersonDocument::class)->find($idExample);
        $this->assertEquals(null, $find);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testRemoveFile()
    {
        $document = $this->createDocumentPassport();

        $uploadDir = "./".$this->main_upload_folder.NaturalPersonPassport::PATH_FILES;
        $fileName = $document->getNaturalPersonPassport()->getFiles()->first()->getName();

        $this->naturalPersonDocumentManager->remove($document);

        $this->assertFileNotExists($uploadDir.'/'.$fileName);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testClone()
    {
        $document = $this->createDocumentPassport();
        $passport = $this->naturalPersonDocumentManager->get($document);

        $newDocument = $this->naturalPersonDocumentManager->clone($document);
        $newPassport = $this->naturalPersonDocumentManager->get($newDocument);

        $this->assertEquals(true, $document->isActive());
        $this->assertEquals(false, $newDocument->isActive());
        $this->assertNotEquals($newDocument->getId(), $document->getId());
        $this->assertEquals($passport->getPassportSerialNumber(), $newPassport->getPassportSerialNumber());
        $this->assertEquals($passport->getDateIssued(), $newPassport->getDateIssued());
        $this->assertEquals($passport->getPassportIssueOrganisation(), $newPassport->getPassportIssueOrganisation());
        $this->assertEquals($passport->getDateExpired(), $newPassport->getDateExpired());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCloneWithOwner()
    {
        $this->createDocumentPassport();
        $this->createDocumentPassport();
        $this->createDocumentPassport();

        $this->createDocumentDriverLicense();
        $this->createDocumentDriverLicense();

        $person = $this->person;
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user, $is_pattern = 0);
        $newPerson = $this->naturalPersonManager->clone($person, $civilLawSubject);

        $this->assertEquals(2, count($newPerson->getNaturalPersonDocument()));   // Клонируются только активные документы
    }
}