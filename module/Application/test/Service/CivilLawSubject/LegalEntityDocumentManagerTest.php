<?php

namespace ApplicationTest\Service\CivilLawSubject;

use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Entity\LegalEntity;
use Application\Entity\LegalEntityDocument;
use Application\Entity\LegalEntityTaxInspection;
use Application\Entity\LegalEntityBankDetail;
use Application\Service\CivilLawSubject\LegalEntityDocumentManager;
use Application\Service\CivilLawSubject\LegalEntityManager;
use Application\Service\UserManager;
use Application\Entity\User;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;

class LegalEntityDocumentManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var LegalEntity
     */
    private $legal;

    /**
     * @var User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var LegalEntityDocumentManager
     */
    private $legalEntityDocumentManager;

    /**
     * @var LegalEntityManager
     */
    private $legalEntityManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    private $main_upload_folder;

    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../../config/application.config.php',
                    include __DIR__ . '/../../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->em = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->legalEntityDocumentManager = $serviceManager->get(LegalEntityDocumentManager::class);
        $this->legalEntityManager = $serviceManager->get(LegalEntityManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);

        $this->em->getConnection()->beginTransaction();

        $userManager = $serviceManager->get(UserManager::class);

        $config = $this->getApplicationConfig();
        $this->main_upload_folder = $config['file-management']['main_upload_folder'];
        $this->user = $userManager->selectUserByLogin($config['tests']['user_login']);
        $this->legal = $this->civilLawSubjectManager->createLegalEntity($this->user, [
            'name' => 'ООО Симпл Технолоджи',
            'legal_address' => 'г. Москва, ул. Авангардная, д. 3 пом. II офис 1407',
            'phone' => '+7-999-888-77-66',
            'inn' => '7743211967',
            'kpp' => '774301001',
            'ogrn' => '1177746547340',
        ]);
    }

    public function tearDown()
    {
        $this->civilLawSubjectManager->remove($this->legal->getCivilLawSubject());

        $this->em->getConnection()->rollback();
        $this->em->getConnection()->close();

        parent::tearDown();
    }

    /**
     * @param $params
     * @return LegalEntityDocument
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    private function createDocumentBankDetail($params = null)
    {
        $saveParams = $params;
        if ($params === null) {
            $saveParams = [
                'checking_account' => '13123123213',
                'name' => 'Сбербанк',
                'correspondent_account' => '30101810400000000225',
                'bic' => '044525225',
            ];
        }

        return $this->legalEntityDocumentManager->create($this->legal, LegalEntityBankDetail::class, $saveParams);
    }

    /**
     * @param $params
     * @return LegalEntityDocument
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    private function createDocumentTaxInspection($params = null)
    {
        $saveParams = $params;
        if ($params === null) {
            $saveParams = [
                'serial_number' => '13123123213',
                'date_issued' =>  date('Y-m-d'),
                'issue_organisation' => 'ОУФМС',
                'issue_registrar' => 'ОУФМС22',
                'date_registered' => date('Y-m-d'),
                'file' => $this->getFileData(),
            ];
        }

        return $this->legalEntityDocumentManager->create($this->legal, LegalEntityTaxInspection::class, $saveParams);
    }

    private function getFileData()
    {
        return [
            'tmp_name'  => './module/Application/test/files/image_file_for_test.png',
            'name'      => "image_file_for_test.png",
            'type' => 'image/png',
            'size' => filesize('./module/Application/test/files/image_file_for_test.png'),
            'path'      => LegalEntityTaxInspection::PATH_FILES,
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreateBankDetail()
    {
        $paramDocs = [
            'checking_account' => '13123123213',
            'name' => 'Сбербанк',
            'correspondent_account' => '30101810400000000225',
            'bic' => '044525225',
        ];
        $result = $this->createDocumentBankDetail($paramDocs);
        $bankDetail = $result->getLegalEntityBankDetail();

        $find = $this->em->getRepository(LegalEntityDocument::class)
            ->find($result->getId());
        $findBankDetail = $find->getLegalEntityBankDetail();

        $types = $this->legalEntityDocumentManager::getTypes();

        $this->assertEquals('Application\Entity\LegalEntityDocument', get_class($result));
        $this->assertEquals($types[LegalEntityBankDetail::class], $result->getLegalEntityDocumentType()->getName());
        $this->assertEquals(true, $result->isActive());
        $this->assertEquals($paramDocs['checking_account'], $bankDetail->getCheckingAccount());
        $this->assertEquals($paramDocs['name'], $bankDetail->getName());
        $this->assertEquals($paramDocs['correspondent_account'], $bankDetail->getCorrespondentAccount());
        $this->assertEquals($paramDocs['bic'], $bankDetail->getBic());

        $this->assertEquals(true, $find->isActive());
        $this->assertEquals($paramDocs['checking_account'], $findBankDetail->getCheckingAccount());
        $this->assertEquals($paramDocs['name'], $findBankDetail->getName());
        $this->assertEquals($paramDocs['correspondent_account'], $findBankDetail->getCorrespondentAccount());
        $this->assertEquals($paramDocs['bic'], $findBankDetail->getBic());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreateBankDetailFail()
    {
        try {
            $result = $this->createDocumentBankDetail([]);
            $e = null;
        } catch (\Exception $e) {}

        $this->assertEquals('Exception', get_class($e));
        $this->assertEquals('Missing required keys: checking_account, name, correspondent_account, bic', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreateTaxInspection()
    {
        $paramDocs = [
            'serial_number' => '13123123213',
            'date_issued' =>  date('Y-m-d'),
            'issue_organisation' => 'ОУФМС',
            'issue_registrar' => 'ОУФМС22',
            'date_registered' => date('Y-m-d'),
            'file' => $this->getFileData(),
        ];
        $result = $this->createDocumentTaxInspection($paramDocs);
        $taxInspection = $result->getLegalEntityTaxInspection();

        $find = $this->em->getRepository(LegalEntityDocument::class)
            ->find($result->getId());
        $findTaxInspection = $find->getLegalEntityTaxInspection();

        $types = $this->legalEntityDocumentManager::getTypes();

        $this->assertEquals('Application\Entity\LegalEntityDocument', get_class($result));
        $this->assertEquals($types[LegalEntityTaxInspection::class], $result->getLegalEntityDocumentType()->getName());
        $this->assertEquals(true, $result->isActive());
        $this->assertEquals($paramDocs['serial_number'], $taxInspection->getSerialNumber());
        $this->assertEquals(new \DateTime($paramDocs['date_issued']), $taxInspection->getDateIssued());
        $this->assertEquals($paramDocs['issue_organisation'], $taxInspection->getIssueOrganisation());
        $this->assertEquals($paramDocs['issue_registrar'], $taxInspection->getIssueRegistrar());
        $this->assertEquals(new \DateTime($paramDocs['date_registered']), $taxInspection->getDateRegistered());
        $this->assertEquals($paramDocs['file']['name'], $taxInspection->getFiles()->first()->getOriginName());

        $this->assertEquals(true, $find->isActive());
        $this->assertEquals($paramDocs['serial_number'], $findTaxInspection->getSerialNumber());
        $this->assertEquals(new \DateTime($paramDocs['date_issued']), $findTaxInspection->getDateIssued());
        $this->assertEquals($paramDocs['issue_organisation'], $findTaxInspection->getIssueOrganisation());
        $this->assertEquals($paramDocs['issue_registrar'], $findTaxInspection->getIssueRegistrar());
        $this->assertEquals(new \DateTime($paramDocs['date_registered']), $findTaxInspection->getDateRegistered());
        $this->assertEquals($paramDocs['file']['name'], $findTaxInspection->getFiles()->first()->getOriginName());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreateTaxInspectionFail()
    {
        try {
            $result = $this->createDocumentTaxInspection([]);
            $e = null;
        } catch (\Exception $e) {}

        $this->assertEquals('Exception', get_class($e));
        $this->assertEquals('Missing required keys: serial_number, date_issued, issue_organisation, issue_registrar, date_registered, file', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUploadFile()
    {
        $document = $this->createDocumentTaxInspection();

        $uploadDir = "./".$this->main_upload_folder.LegalEntityTaxInspection::PATH_FILES;
        $fileName = $document->getLegalEntityTaxInspection()->getFiles()->first()->getName();

        $this->assertFileExists($uploadDir.'/'.$fileName);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testAssociationGet()
    {
        $legal = $this->legal;
        $this->createDocumentBankDetail();
        $this->createDocumentBankDetail();
        $exampleBankDetail = $this->createDocumentBankDetail();

        $exampleTaxInspection = $this->createDocumentTaxInspection();
        $this->createDocumentTaxInspection();

        $findDocuments = $this->legalEntityManager->getDocuments($legal);
        $typeBankDetail = $this->legalEntityDocumentManager::getType(LegalEntityBankDetail::class);
        $typeTaxInspection = $this->legalEntityDocumentManager::getType(LegalEntityTaxInspection::class);

        $this->assertEquals(5, count($legal->getLegalEntityDocument()));
        $this->assertEquals(2, count($findDocuments));
        $this->assertEquals(3, count($findDocuments[$typeBankDetail]));
        $this->assertEquals(2, count($findDocuments[$typeTaxInspection]));
        $this->assertEquals($legal->getId(), $exampleBankDetail->getLegalEntity()->getId());
        $this->assertEquals($legal->getId(), $exampleTaxInspection->getLegalEntity()->getId());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSetLastActive()
    {
        $legal = $this->legal;
        $this->createDocumentBankDetail();
        $this->createDocumentBankDetail();
        $this->createDocumentBankDetail();

        $this->createDocumentTaxInspection();
        $this->createDocumentTaxInspection();

        $findDocuments = $this->legalEntityManager->getDocuments($legal);
        $typeBankDetail = $this->legalEntityDocumentManager::getType(LegalEntityBankDetail::class);
        $typeTaxInspection = $this->legalEntityDocumentManager::getType(LegalEntityTaxInspection::class);

        $this->assertEquals(false, $findDocuments[$typeBankDetail][0]->getLegalEntityDocument()->isActive());
        $this->assertEquals(false, $findDocuments[$typeBankDetail][1]->getLegalEntityDocument()->isActive());
        $this->assertEquals(true,  $findDocuments[$typeBankDetail][2]->getLegalEntityDocument()->isActive());

        $this->assertEquals(false, $findDocuments[$typeTaxInspection][0]->getLegalEntityDocument()->isActive());
        $this->assertEquals(true,  $findDocuments[$typeTaxInspection][1]->getLegalEntityDocument()->isActive());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateBankDetail()
    {
        $paramDocsAdd = [
            'checking_account' => '13123123213',
            'name' => 'Сбербанк',
            'correspondent_account' => '30101810400000000225',
            'bic' => '044525225',
        ];
        $bankDetailAddFirst = $this->createDocumentBankDetail($paramDocsAdd);
        $bankDetailAddLast = $this->createDocumentBankDetail($paramDocsAdd);

        $paramDocs = [
            'checking_account' => 'asdasdasd',
            'name' => '333',
            'correspondent_account' => 'asdasd',
            'bic' => 'cxvcxvxc',
        ];
        $result = $this->legalEntityDocumentManager->update($bankDetailAddLast, $paramDocs);
        $bankDetail = $result->getLegalEntityBankDetail();

        $this->assertEquals(true, $result->isActive());
        $this->assertEquals($paramDocs['checking_account'], $bankDetail->getCheckingAccount());
        $this->assertEquals($paramDocs['name'], $bankDetail->getName());
        $this->assertEquals($paramDocs['correspondent_account'], $bankDetail->getCorrespondentAccount());
        $this->assertEquals($paramDocs['bic'], $bankDetail->getBic());

        $this->assertEquals(false, $bankDetailAddFirst->isActive());
        $this->assertEquals($paramDocsAdd['checking_account'], $bankDetailAddFirst->getLegalEntityBankDetail()->getCheckingAccount());
        $this->assertEquals($paramDocsAdd['name'], $bankDetailAddFirst->getLegalEntityBankDetail()->getName());
        $this->assertEquals($paramDocsAdd['correspondent_account'], $bankDetailAddFirst->getLegalEntityBankDetail()->getCorrespondentAccount());
        $this->assertEquals($paramDocsAdd['bic'], $bankDetailAddFirst->getLegalEntityBankDetail()->getBic());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateBankDetailFail()
    {
        try {
            $document = $this->createDocumentBankDetail();
            $result = $this->legalEntityDocumentManager->update($document, []);
            $e = null;
        } catch (\Exception $e) {}

        $this->assertEquals('Exception', get_class($e));
        $this->assertEquals('Missing required keys: checking_account, name, correspondent_account, bic', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateTaxInspection()
    {
        $paramDocsAdd = [
            'serial_number' => '13123123213',
            'date_issued' =>  date('Y-m-d'),
            'issue_organisation' => 'ОУФМС',
            'issue_registrar' => 'ОУФМС22',
            'date_registered' => date('Y-m-d'),
            'file' => $this->getFileData(),
        ];
        $taxInspectionAddFirst = $this->createDocumentTaxInspection($paramDocsAdd);
        $taxInspectionAddLast = $this->createDocumentTaxInspection($paramDocsAdd);

        $paramDocs = [
            'serial_number' => 'asdasdsa',
            'date_issued' =>  date('Y-m-d'),
            'issue_organisation' => '213213',
            'issue_registrar' => 'asdsad',
            'date_registered' => date('Y-m-d'),
            'file' => $this->getFileData(),
        ];
        $result = $this->legalEntityDocumentManager->update($taxInspectionAddLast, $paramDocs);
        $taxInspection = $result->getLegalEntityTaxInspection();

        $this->assertEquals(true, $result->isActive());
        $this->assertEquals($paramDocs['serial_number'], $taxInspection->getSerialNumber());
        $this->assertEquals(new \DateTime($paramDocs['date_issued']), $taxInspection->getDateIssued());
        $this->assertEquals($paramDocs['issue_organisation'], $taxInspection->getIssueOrganisation());
        $this->assertEquals($paramDocs['issue_registrar'], $taxInspection->getIssueRegistrar());
        $this->assertEquals(new \DateTime($paramDocs['date_registered']), $taxInspection->getDateRegistered());
        $this->assertEquals($paramDocs['file']['name'], $taxInspection->getFiles()->first()->getOriginName());

        $this->assertEquals(false, $taxInspectionAddFirst->isActive());
        $this->assertEquals($paramDocsAdd['serial_number'], $taxInspectionAddFirst->getLegalEntityTaxInspection()->getSerialNumber());
        $this->assertEquals(new \DateTime($paramDocsAdd['date_issued']), $taxInspectionAddFirst->getLegalEntityTaxInspection()->getDateIssued());
        $this->assertEquals($paramDocsAdd['issue_organisation'], $taxInspectionAddFirst->getLegalEntityTaxInspection()->getIssueOrganisation());
        $this->assertEquals($paramDocsAdd['issue_registrar'], $taxInspectionAddFirst->getLegalEntityTaxInspection()->getIssueRegistrar());
        $this->assertEquals(new \DateTime($paramDocsAdd['date_registered']), $taxInspectionAddFirst->getLegalEntityTaxInspection()->getDateRegistered());
        $this->assertEquals($paramDocsAdd['file']['name'], $taxInspectionAddFirst->getLegalEntityTaxInspection()->getFiles()->first()->getOriginName());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testUpdateTaxInspectionFail()
    {
        try {
            $document = $this->createDocumentTaxInspection();
            $result = $this->legalEntityDocumentManager->update($document, []);
            $e = null;
        } catch (\Exception $e) {}

        $this->assertEquals('Exception', get_class($e));
        $this->assertEquals('Missing required keys: serial_number, date_issued, issue_organisation, issue_registrar, date_registered', $e->getMessage());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testRemove()
    {
        $legal = $this->legal;
        $doc1 = $this->createDocumentBankDetail();
        $doc2 = $this->createDocumentBankDetail();
        $doc3 = $this->createDocumentBankDetail();

        $doc4 = $this->createDocumentTaxInspection();
        $doc5 = $this->createDocumentTaxInspection();

        $idExample = $doc3->getId();

        $this->legalEntityDocumentManager->remove($doc1);
        $this->assertEquals(4, count($legal->getLegalEntityDocument()));
        $this->legalEntityDocumentManager->remove($doc2);
        $this->assertEquals(3, count($legal->getLegalEntityDocument()));
        $this->legalEntityDocumentManager->remove($doc3);
        $this->assertEquals(2, count($legal->getLegalEntityDocument()));
        $this->legalEntityDocumentManager->remove($doc4);
        $this->assertEquals(1, count($legal->getLegalEntityDocument()));
        $this->legalEntityDocumentManager->remove($doc5);
        $this->assertEquals(0, count($legal->getLegalEntityDocument()));

        $find = $this->em->getRepository(LegalEntityDocument::class)->find($idExample);
        $this->assertEquals(null, $find);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testRemoveFile()
    {
        $document = $this->createDocumentTaxInspection();

        $uploadDir = "./".$this->main_upload_folder.LegalEntityTaxInspection::PATH_FILES;
        $fileName = $document->getLegalEntityTaxInspection()->getFiles()->first()->getName();

        $this->legalEntityDocumentManager->remove($document);

        $this->assertFileNotExists($uploadDir.'/'.$fileName);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testClone()
    {
        $document = $this->createDocumentBankDetail();
        $bankDetail = $this->legalEntityDocumentManager->get($document);

        $newDocument = $this->legalEntityDocumentManager->clone($document);
        $newBankDetail = $this->legalEntityDocumentManager->get($newDocument);

        $this->assertEquals(true, $document->isActive());
        $this->assertEquals(false, $newDocument->isActive());
        $this->assertNotEquals($newDocument->getId(), $document->getId());
        $this->assertEquals($bankDetail->getCheckingAccount(), $newBankDetail->getCheckingAccount());
        $this->assertEquals($bankDetail->getName(), $newBankDetail->getName());
        $this->assertEquals($bankDetail->getCorrespondentAccount(), $newBankDetail->getCorrespondentAccount());
        $this->assertEquals($bankDetail->getBic(), $newBankDetail->getBic());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCloneWithOwner()
    {
        $this->createDocumentBankDetail();
        $this->createDocumentBankDetail();
        $this->createDocumentBankDetail();

        $this->createDocumentTaxInspection();
        $this->createDocumentTaxInspection();

        $legal = $this->legal;
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user, $is_pattern = 0);
        $newLegal = $this->legalEntityManager->clone($legal, $civilLawSubject);

        $this->assertEquals(2, count($newLegal->getLegalEntityDocument()));   // Клонируются только активные документы
    }
}