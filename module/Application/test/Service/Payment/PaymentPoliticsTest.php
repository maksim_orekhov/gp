<?php

namespace ApplicationTest\Service\Payment;

use Application\Entity\Deal;
use Application\Entity\Payment;
use ModuleAcquiringMandarin\Controller\CallbackHandleController;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Service\Payment\PaymentPolitics;
use Zend\Stdlib\ArrayUtils;

class PaymentPoliticsTest extends AbstractHttpControllerTestCase
{
    const DEAL_PARTIAL_PAID_BY_BANK_TRANSFER_NAME = 'Сделка Частично оплачено';
    const DEAL_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';

    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;


    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../../config/application.config.php',
                    include __DIR__ . '/../../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->paymentPolitics = $serviceManager->get(PaymentPolitics::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Сделка DEAL_PARTIAL_PAID_NAME частично оплачена через bank_transfer
     * Должен вернуть false
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIsPaymentMethodTypeAvailableForAcquiringMandarinIfPaidByBankTransfer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_PARTIAL_PAID_BY_BANK_TRANSFER_NAME]);

        $this->assertNotNull($deal);

        $result = PaymentPolitics::isPaymentMethodTypeAvailable($deal, PaymentMethodType::ACQUIRING_MANDARIN);

        $this->assertFalse($result);
    }

    /**
     * Сделка DEAL_STATUS_CONFIRMED_NAME не оплачена
     * Должен вернуть true
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIsPaymentMethodTypeAvailableForAcquiringMandarinForNotPaid()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        $result = PaymentPolitics::isPaymentMethodTypeAvailable($deal, PaymentMethodType::ACQUIRING_MANDARIN);

        $this->assertTrue($result);
    }

    /**
     * Сделка DEAL_STATUS_CONFIRMED_NAME частично оплачена (моделируем) через acquiring_mandarin
     * Должен вернуть false
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIsPaymentMethodTypeAvailableForBankTransferIfPaidAcquiringMandarin()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_STATUS_CONFIRMED_NAME]);

        // Делаем платёжку типа MandarinPaymentOrder для моделирования частичной оплаты
        $paymentOrder = $this->createMandarinPaymentOrder($deal);

        $this->assertNotNull($paymentOrder->getMandarinPaymentOrder());

        $this->assertNotNull($deal);

        $result = PaymentPolitics::isPaymentMethodTypeAvailable($deal, PaymentMethodType::BANK_TRANSFER);

        $this->assertFalse($result);
    }

    /**
     * Сделка DEAL_PARTIAL_PAID_NAME частично оплачена через bank_transfer
     * Должен вернуть true
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIsPaymentMethodTypeAvailableForBankTransferIfPaidByBankTransfer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_PARTIAL_PAID_BY_BANK_TRANSFER_NAME]);

        $this->assertNotNull($deal);

        $result = PaymentPolitics::isPaymentMethodTypeAvailable($deal, PaymentMethodType::BANK_TRANSFER);

        $this->assertTrue($result);
    }

    /**
     * Сделка DEAL_STATUS_CONFIRMED_MAME не оплачена
     * Должен вернуть true
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIsPaymentMethodTypeAvailableForBankTransferForNotPaid()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        $result = PaymentPolitics::isPaymentMethodTypeAvailable($deal, PaymentMethodType::BANK_TRANSFER);

        $this->assertTrue($result);
    }


    /**
     * @param Deal $deal
     * @return PaymentOrder
     */
    private function createMandarinPaymentOrder(Deal $deal): PaymentOrder
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var PaymentMethodType $paymentMethodType */
        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(['name' => PaymentMethodType::ACQUIRING_MANDARIN]);

        $order_id = 'ghgh3vbvb7hjhjhj_hjkkhjjhfgf5nsroplqc62cvhjk890gde'; // Пока произвольный набор символов
        $amount = $payment->getExpectedValue() - 2000;
        $transaction = '123fg456hhi8676'; // Произвольный набор символов
        $payment_purpose = 'Оплата по сделке ' . $deal->getNumber();

        $mandarinPaymentOrder = new MandarinPaymentOrder();
        $mandarinPaymentOrder->setType(PaymentOrderManager::TYPE_INCOMING);
        $mandarinPaymentOrder->setOrderId($order_id);
        $mandarinPaymentOrder->setAmount($amount);
        $mandarinPaymentOrder->setAction(CallbackHandleController::ACTION_FOR_PAYMENT);
        $mandarinPaymentOrder->setTransaction($transaction);
        $mandarinPaymentOrder->setStatus(MandarinPaymentOrderManager::STATUS_PROCESSING);
        $mandarinPaymentOrder->setCustomerEmail($deal->getCustomer()->getEmail());
        $mandarinPaymentOrder->setCustomerPhone('+'.$deal->getCustomer()->getUser()->getPhone()->getPhoneNumber());
        $mandarinPaymentOrder->setCardHolder('CARD HOLDER');
        $mandarinPaymentOrder->setCardNumber('4929509947106878');
        $mandarinPaymentOrder->setCardExpirationYear(22);
        $mandarinPaymentOrder->setCardExpirationMonth(12);
        $mandarinPaymentOrder->setPaymentPurpose($payment_purpose);
        $mandarinPaymentOrder->setCreated(new \DateTime());

        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = new PaymentOrder();
        $paymentOrder->setCreated(new \DateTime());
        $paymentOrder->setPaymentMethodType($paymentMethodType);

        // Add PaymentOrder to $payment->paymentOrders collection
        $payment->addPaymentOrder($paymentOrder);
        // Set PaymentOrder id to $mandarinPaymentOrder
        $mandarinPaymentOrder->setPaymentOrder($paymentOrder);

        $this->entityManager->persist($mandarinPaymentOrder);

        $paymentOrder->setMandarinPaymentOrder($mandarinPaymentOrder);

        $this->entityManager->persist($paymentOrder);
        $this->entityManager->flush();

        return $paymentOrder;
    }
}