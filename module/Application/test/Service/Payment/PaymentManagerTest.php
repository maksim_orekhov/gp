<?php

namespace ApplicationTest\Service\Payment;

use Application\Entity\DealType;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Application\Service\UserManager;
use Application\Service\Payment\PaymentManager;
use Application\Entity\FeePayerOption;
use Application\Entity\Payment;
use Application\Service\Payment\PaymentPolitics;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Application\Entity\PaymentMethod;
use Application\Entity\Deal;
use Application\Service\Deal\DealManager;
use Application\Entity\CivilLawSubject;


class PaymentManagerTest extends AbstractHttpControllerTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Application\Service\Payment\PaymentManager
     */
    private $paymentManager;

    /**
     * @var \Application\Service\Payment\PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * @var \Application\Service\Deal\DealManager;
     */
    private $dealManager;

    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../../config/application.config.php',
                    include __DIR__ . '/../../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->paymentManager = $serviceManager->get(PaymentManager::class);
        $this->paymentPolitics = $serviceManager->get(PaymentPolitics::class);
        $this->dealManager = $serviceManager->get(DealManager::class);

        $config = $this->getApplicationConfig();
        $user = $this->userManager->getUserByLogin($config['tests']['user_login']);
        $this->user = $user;

        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        $this->entityManager->getConnection()->rollback();
        $this->entityManager->getConnection()->close();

        parent::tearDown();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testFind()
    {
        $dealValue = 1000;
        $expectedValue = 2000;
        $releasedValue = 3000;
        $fee = 5;

        $payment = new Payment();
        $payment->setDealValue($dealValue);
        $payment->setExpectedValue($expectedValue);
        $payment->setReleasedValue($releasedValue);
        $payment->setFee($fee);

        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        $testPayment = $this->paymentManager->find($payment->getId());

        $this->assertEquals($payment->getId(), $testPayment->getId());
        $this->assertEquals($fee, $testPayment->getFee());
        $this->assertEquals($dealValue, $testPayment->getDealValue());
        $this->assertEquals($expectedValue, $testPayment->getExpectedValue());
        $this->assertEquals($releasedValue, $testPayment->getReleasedValue());
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetPaymentByDeal()
    {
        // Create new Deal object
        $deal = new Deal();
        $deal->setCreated(new \DateTime());
        $deal->setName('test');
        // ищем тип сделки (Услуга)
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneByName('Услуга');

        $dealType = $this->dealManager->getDealTypeById($dealType->getId());
        $deal->setDealType($dealType);
        $deal->setUpdated(new \DateTime());
        $deal->setDeliveryPeriod(4);

        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('Customer');

        if(!$feePayerOption || !($feePayerOption instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with Contractor found');
        }
        $deal->setFeePayerOption($feePayerOption);
        $this->entityManager->persist($deal);

        // Create new Payment object
        $payment = new Payment();
        $payment->setDealValue(1000);
        $payment->setExpectedValue(1000);
        $payment->setReleasedValue(1000);
        $payment->setDeal($deal);
        $payment->setFee(4);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        // Test
        $testPayment = $this->paymentManager->getPaymentByDeal($deal);

        $this->assertEquals(true, $testPayment !== null);
        $this->assertEquals(true, $testPayment instanceof Payment);
        $this->assertEquals($deal->getId(), $testPayment->getDeal()->getId());
    }

    /**
     * тесты расчетов для Customer
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSetCalculatedPaymentDataForCustomer()
    {
        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('Customer');

        $param = [
            'feePayerOption' => $feePayerOption->getId(),
            'amount'=> 5000,
            'delivery_period' => 9
        ];
        $currentDate = new \DateTime();
        $delivery_period = $currentDate->modify('+'.$param['delivery_period'].' days');
        $fee = $this->paymentPolitics->getFeePercentage();
        $feePayerOptionCustomer = $this->entityManager->getRepository(FeePayerOption::class)->find($param['feePayerOption']);

        if(!$feePayerOptionCustomer || !($feePayerOptionCustomer instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with customer found');
        }

        $expected_value = $this->paymentPolitics->getCalculatedExpectedValue($param['amount'], $feePayerOptionCustomer);
        $released_value = $this->paymentPolitics->getCalculatedReleasedValue($param['amount'], $feePayerOptionCustomer);

        $payment = new Payment;
        $payment = $this->paymentManager->setCalculatedPaymentData($payment, $param['amount'], $feePayerOptionCustomer);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        $this->assertEquals($param['amount'], $payment->getDealValue());
        $this->assertEquals($delivery_period, $payment->getPlannedDate());
        $this->assertEquals($fee, $payment->getFee());
        $this->assertEquals($expected_value, $payment->getExpectedValue());
        $this->assertEquals($released_value, $payment->getReleasedValue());
    }

    /**
     * тесты расчетов для Contractor
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSetCalculatedPaymentDataForContractor()
    {
        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('Contractor');

        $param = [
            'feePayerOption' => $feePayerOption->getId(),
            'amount'=> 5000,
            'delivery_period' => 9
        ];
        $currentDate = new \DateTime();
        $delivery_period = $currentDate->modify('+'.$param['delivery_period'].' days');
        $fee = $this->paymentPolitics->getFeePercentage();
        $feePayerOptionContractor = $this->entityManager->getRepository(FeePayerOption::class)->find($param['feePayerOption']);

        if(!$feePayerOptionContractor || !($feePayerOptionContractor instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with Contractor found');
        }

        $expected_value = $this->paymentPolitics->getCalculatedExpectedValue($param['amount'], $feePayerOptionContractor);
        $released_value = $this->paymentPolitics->getCalculatedReleasedValue($param['amount'], $feePayerOptionContractor);

        $payment = new Payment;
        $payment = $this->paymentManager->setCalculatedPaymentData($payment, $param['amount'], $feePayerOptionContractor);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        $this->assertEquals($param['amount'], $payment->getDealValue());
        $this->assertEquals($delivery_period, $payment->getPlannedDate());
        $this->assertEquals($fee, $payment->getFee());
        $this->assertEquals($expected_value, $payment->getExpectedValue());
        $this->assertEquals($released_value, $payment->getReleasedValue());
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSetPaymentMethod()
    {
        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneByName(PaymentMethodType::BANK_TRANSFER);
        if ($paymentMethodType === null) {
            throw new \Exception('Not found paymentMethodType');
        }

        $paymentMethod = new PaymentMethod();
        $paymentMethod->setPaymentMethodType($paymentMethodType);
        $paymentMethod->setIsPattern(1);
        $this->entityManager->persist($paymentMethod);
        $this->entityManager->flush();

        $payment = new Payment();
        $payment->setDealValue(1000);
        $payment->setExpectedValue(1000);
        $payment->setReleasedValue(1000);
        $payment->setFee(4);

        // Test
        $testPayment = $this->paymentManager->setPaymentMethod($payment, $paymentMethod->getId());

        $this->assertEquals($paymentMethod->getId(), $testPayment->getPaymentMethod()->getId());
    }

    /**
     * тесты расчетов для 50x50
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSetCalculatedPaymentDataFor50X50()
    {
        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('50/50');

        $param = [
            'feePayerOption' => $feePayerOption->getId(),
            'amount'=> 5000,
            'delivery_period' => 9
        ];
        $currentDate = new \DateTime();
        $delivery_period = $currentDate->modify('+'.$param['delivery_period'].' days');
        $fee = $this->paymentPolitics->getFeePercentage();
        $feePayerOption50X50 = $this->entityManager->getRepository(FeePayerOption::class)->find($param['feePayerOption']);

        if(!$feePayerOption50X50 || !($feePayerOption50X50 instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with 50/50 found');
        }

        $expected_value = $this->paymentPolitics->getCalculatedExpectedValue($param['amount'], $feePayerOption50X50);
        $released_value = $this->paymentPolitics->getCalculatedReleasedValue($param['amount'], $feePayerOption50X50);

        $payment = new Payment;
        $payment = $this->paymentManager->setCalculatedPaymentData($payment, $param['amount'], $feePayerOption50X50);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        $this->assertEquals($param['amount'], $payment->getDealValue());
        $this->assertEquals($delivery_period, $payment->getPlannedDate());
        $this->assertEquals($fee, $payment->getFee());
        $this->assertEquals($expected_value, $payment->getExpectedValue());
        $this->assertEquals($released_value, $payment->getReleasedValue());
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreatePaymentWithoutSaving()
    {
        $amount = 5000;
        $fee = $this->paymentPolitics->getFeePercentage();

        // Create new Deal object
        $deal = new Deal();
        $deal->setCreated(new \DateTime());
        $deal->setName('test');

        // ищем тип сделки (Услуга)
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneByName('Услуга');

        $deal->setDealType($dealType);
        $deal->setUpdated(new \DateTime());
        $deal->setDeliveryPeriod(9);

        $FeePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('Customer');

        if(!$FeePayerOption || !($FeePayerOption instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with Customer found');
        }
        $deal->setFeePayerOption($FeePayerOption);
        $this->entityManager->persist($deal);
        $this->entityManager->flush();

        $payment = $this->paymentManager->createPaymentWithoutSaving($deal, $amount);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        $expected_value = $this->paymentPolitics->getCalculatedExpectedValue($amount, $FeePayerOption);
        $released_value = $this->paymentPolitics->getCalculatedReleasedValue($amount, $FeePayerOption);

        $this->assertEquals($deal->getId(), $payment->getDeal()->getId());
        $this->assertEquals($fee, $payment->getFee());
        $this->assertEquals($amount, $payment->getDealValue());
        $this->assertEquals($expected_value, $payment->getExpectedValue());
        $this->assertEquals($released_value, $payment->getReleasedValue());
    }
}