<?php

namespace ApplicationTest\Service\Payment;

use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\Payment;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Service\Payment\PaymentMethodManager;
use Application\Entity\PaymentMethod;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\CivilLawSubject;

class PaymentMethodManagerTest extends AbstractHttpControllerTestCase
{
    use \ApplicationTest\TestFixture\FixtureTrait;

    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../../config/application.config.php',
                    include __DIR__ . '/../../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);

        $config = $this->getApplicationConfig();
        $user = $this->userManager->getUserByLogin($config['tests']['user_login']);
        $this->user = $user;

        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testFind()
    {
        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($this->user);
        $civilLawSubject->setIsPattern(1);
        $civilLawSubject->setCreated(new \DateTime(date('Y-m-d H:i:s')));
        $this->entityManager->persist($civilLawSubject);

        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneByName(PaymentMethodType::BANK_TRANSFER);

        if ($paymentMethodType === null) {
            throw new \Exception('Not found paymentMethodType');
        }

        $paymentMethod = new PaymentMethod();
        $paymentMethod->setCivilLawSubject($civilLawSubject);
        $paymentMethod->setPaymentMethodType($paymentMethodType);
        $paymentMethod->setIsPattern(1);
        $this->entityManager->persist($paymentMethod);
        $this->entityManager->flush();

        $testPaymentMethod = $this->paymentMethodManager->get($paymentMethod->getId());

        $this->assertEquals(true, $testPaymentMethod instanceof PaymentMethod);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testFindTypes()
    {
        $testPaymentMethodTypes = $this->paymentMethodManager->findTypes();

        // paymentMethodTypes должен быть один (с названием PaymentMethodType::BANK_TRANSFER)
        $this->assertEquals(4, count($testPaymentMethodTypes));

        // Создаем и добавляем новый
        $paymentMethodTypes = new PaymentMethodType();
        $paymentMethodTypes->setName('test_method_type');
        $paymentMethodTypes->setDescription('test_method_type');
        $paymentMethodTypes->setOutputName('test_method_type');
        $paymentMethodTypes->setIsActive(1);
        $this->entityManager->persist($paymentMethodTypes);
        $this->entityManager->flush();

        $testPaymentMethodTypes = $this->paymentMethodManager->findTypes();

        // paymentMethodTypes должно быть 2
        $this->assertEquals(5, count($testPaymentMethodTypes));

        $this->assertEquals(true, ($testPaymentMethodTypes[1] instanceof PaymentMethodType));
        $this->assertEquals(PaymentMethodType::BANK_TRANSFER, $testPaymentMethodTypes[0]->getName());
        $this->assertEquals(PaymentMethodType::CACH, $testPaymentMethodTypes[1]->getName());
        $this->assertEquals(PaymentMethodType::ACQUIRING_MANDARIN, $testPaymentMethodTypes[2]->getName());
        $this->assertEquals(PaymentMethodType::EMONEY, $testPaymentMethodTypes[3]->getName());
        $this->assertEquals('test_method_type', $testPaymentMethodTypes[4]->getName());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testFindType()
    {
        $paymentMethodTypes = new PaymentMethodType();
        $paymentMethodTypes->setName('test_method_type');
        $paymentMethodTypes->setDescription('test_method_type');
        $paymentMethodTypes->setOutputName('test_method_type');
        $paymentMethodTypes->setIsActive(1);
        $this->entityManager->persist($paymentMethodTypes);
        $this->entityManager->flush();

        $testPaymentMethodTypes = $this->paymentMethodManager->findType($paymentMethodTypes->getId());

        $this->assertEquals(true, ($testPaymentMethodTypes instanceof PaymentMethodType));
        $this->assertEquals('test_method_type', $testPaymentMethodTypes->getName());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetCallbackByType()
    {
        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneByName(PaymentMethodType::BANK_TRANSFER);
        $callbackByType = $this->paymentMethodManager->getCallbackByType($paymentMethodType);

        $this->assertEquals(true, is_string($callbackByType));
        $this->assertEquals('createPaymentMethodBankTransfer', $callbackByType);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreate()
    {
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user);

        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneByName(PaymentMethodType::BANK_TRANSFER);

        $paymentMethod = $this->paymentMethodManager->create($civilLawSubject, $paymentMethodType);

        $this->assertEquals($civilLawSubject->getId(), $paymentMethod->getCivilLawSubject()->getId());
        $this->assertEquals($paymentMethodType->getId(), $paymentMethod->getPaymentMethodType()->getId());
        $this->assertEquals(true, $paymentMethod->getIsPattern());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreateBankTransfer()
    {
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user);
        $params = [
            'name' => 'Имя',
            'bank_name' => 'ВТБ',
            'bik' => '111222',
            'account_number' => '22233344455566677788',
            'corr_account_number' => '88877766655544433332',
            'payment_recipient_name' => '444555',
            'inn' => '666777',
            'kpp' => '777888',
        ];
        $bankTransfer = $this->paymentMethodManager->createPaymentMethodBankTransfer($params, $civilLawSubject);
        $paymentMethod = $bankTransfer->getPaymentMethod();
        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)->findOneByName(PaymentMethodType::BANK_TRANSFER);

        $this->assertEquals($paymentMethodType->getId(), $paymentMethod->getPaymentMethodType()->getId());
        $this->assertEquals(true, $paymentMethod->getIsPattern());
        $this->assertEquals($params['name'], $bankTransfer->getName());
        $this->assertEquals($params['bank_name'], $bankTransfer->getBankName());
        $this->assertEquals($params['bik'], $bankTransfer->getBik());
        $this->assertEquals($params['account_number'], $bankTransfer->getAccountNumber());
        $this->assertEquals($params['corr_account_number'], $bankTransfer->getCorrAccountNumber());
        $this->assertEquals($params['payment_recipient_name'], $bankTransfer->getPaymentRecipientName());
        $this->assertEquals($params['inn'], $bankTransfer->getInn());
        $this->assertEquals($params['kpp'], $bankTransfer->getKpp());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testClone()
    {
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user);
        $params = [
            'name' => 'Имя',
            'bank_name' => 'ВТБ',
            'bik' => '111222',
            'account_number' => '22233344455566677788',
            'corr_account_number' => '88877766655544433332',
            'payment_recipient_name' => '444555',
            'inn' => '666777',
            'kpp' => '777888',
        ];
        $bankTransfer = $this->paymentMethodManager->createPaymentMethodBankTransfer($params, $civilLawSubject);
        $paymentMethod = $bankTransfer->getPaymentMethod();

        $clonePaymentMethod = $this->paymentMethodManager->clone($paymentMethod);
        $cloneBankTransfer = $clonePaymentMethod->getPaymentMethodBankTransfer();

        $this->assertEquals(false, $clonePaymentMethod->getIsPattern());
        $this->assertEquals($params['name'], $cloneBankTransfer->getName());
        $this->assertEquals($params['bank_name'], $bankTransfer->getBankName());
        $this->assertEquals($params['bik'], $cloneBankTransfer->getBik());
        $this->assertEquals($params['account_number'], $cloneBankTransfer->getAccountNumber());
        $this->assertEquals($params['corr_account_number'], $bankTransfer->getCorrAccountNumber());
        $this->assertEquals($params['payment_recipient_name'], $cloneBankTransfer->getPaymentRecipientName());
        $this->assertEquals($params['inn'], $cloneBankTransfer->getInn());
        $this->assertEquals($params['kpp'], $cloneBankTransfer->getKpp());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSavingBankDetailsCustomerFromPaymentOrder()
    {
        $bankClientPaymentOrder = $this->fixtureBankClientPaymentOrder();
        $payment = $this->fixturePayment();
        $deal = $payment->getDeal();
        $dealCustomerCivilLawSubject = $deal->getCustomer()->getCivilLawSubject();

        $this->assertInstanceOf(BankClientPaymentOrder::class, $bankClientPaymentOrder);
        $this->assertInstanceOf(Payment::class, $payment);

        $bankTransfer = $this->paymentMethodManager->savingBankDetailsCustomerFromPaymentOrder($bankClientPaymentOrder, $payment);
        $paymentCustomerCivilLawSubject = $bankTransfer->getPaymentMethod()->getCivilLawSubject();

        $this->assertInstanceOf(PaymentMethodBankTransfer::class, $bankTransfer);

        $this->assertEquals($bankTransfer->getName(), PaymentMethodManager::BANK_DETAILS_CUSTOMER_NAME.$deal->getNumber());
        $this->assertEquals($bankTransfer->getBankName(), $bankClientPaymentOrder->getPayerBank1());
        $this->assertEquals($bankTransfer->getAccountNumber(), $bankClientPaymentOrder->getPayerAccount());
        $this->assertEquals($bankTransfer->getPaymentRecipientName(), $bankClientPaymentOrder->getPayer1());
        $this->assertEquals($bankTransfer->getBik(), $bankClientPaymentOrder->getPayerBik());
        $this->assertEquals($bankTransfer->getInn(), $bankClientPaymentOrder->getPayerInn());
        $this->assertEquals($bankTransfer->getKpp(), $bankClientPaymentOrder->getPayerKpp());
        $this->assertEquals($bankTransfer->getCorrAccountNumber(), $bankClientPaymentOrder->getPayerCorrAccount());

        $this->assertEquals($dealCustomerCivilLawSubject, $paymentCustomerCivilLawSubject);
    }
}