<?php

namespace ApplicationTest\Service\Payment;

use Application\Service\Payment\CalculatedDataProvider;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\FeePayerOption;

class CalculatedDataProviderTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider;


    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../../config/application.config.php',
                    include __DIR__ . '/../../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->calculatedDataProvider = $serviceManager->get(CalculatedDataProvider::class);
    }

    public function tearDown()
    {
        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testGetFeePercentage()
    {
        $feePercentage = $this->calculatedDataProvider->getFeePercentage();

        $this->assertEquals(true, (isset($feePercentage) && !empty($feePercentage)));
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal*
     */
    public function testGetFeeAmount()
    {
        $amount = 10000;

        $feePercentage = $this->calculatedDataProvider->getFeePercentage();
        $assertFeeAmount = $amount * ($feePercentage / 100);
        $feeAmount = $this->calculatedDataProvider->getFeeAmount($amount);

        $this->assertEquals($assertFeeAmount, $feeAmount);
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testGetCalculatedExpectedValue()
    {
        $amount = 10000;
        $feePercentage = $this->calculatedDataProvider->getFeePercentage();

        // if fee 4% and amount 10000 expected amount 10400
        $expectedAmountCustomer = $amount + ($amount * ($feePercentage / 100));
        // if fee 4% and amount 10000 expected amount 10000
        $expectedAmountContractor = $amount;
        // if fee 4% and amount 10000 expected amount 10200
        $expectedAmount50x50 = $amount + (($amount * ($feePercentage / 100)) / 2);

        $payerCustomer = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('Customer');
        if(!$payerCustomer || !($payerCustomer instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with customer found');
        }
        $payerContractor = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('Contractor');
        if(!$payerContractor || !($payerContractor instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with Contractor found');
        }
        $payer50x50 = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('50/50');
        if(!$payer50x50 || !($payer50x50 instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with 50x50 found');
        }

        $expectedValueCustomer = $this->calculatedDataProvider
            ->getCalculatedExpectedValue($amount, $payerCustomer);
        $expectedValueContractor = $this->calculatedDataProvider
            ->getCalculatedExpectedValue($amount, $payerContractor);
        $expectedValue50x50 = $this->calculatedDataProvider
            ->getCalculatedExpectedValue($amount, $payer50x50);

        $this->assertEquals(true, (isset($feePercentage) && !empty($feePercentage)));
        $this->assertEquals($expectedAmountCustomer, $expectedValueCustomer);
        $this->assertEquals($expectedAmountContractor, $expectedValueContractor);
        $this->assertEquals($expectedAmount50x50, $expectedValue50x50);
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testGetCalculatedReleasedValue()
    {
        $amount = 5000;
        $feePercentage = $this->calculatedDataProvider->getFeePercentage();

        // if fee 4% and amount 5000 expected amount 5000
        $releasedAmountCustomer = $amount;
        // if fee 4% and amount 5000 expected amount 4800
        $releasedAmountContractor = $amount - ($amount * ($feePercentage / 100));
        // if fee 4% and amount 5000 expected amount 4900
        $releasedAmount50x50 = $amount - (($amount * ($feePercentage / 100)) / 2);

        $payerCustomer = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('Customer');
        if(!$payerCustomer || !($payerCustomer instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with customer found');
        }
        $payerContractor = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('Contractor');
        if(!$payerContractor || !($payerContractor instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with Contractor found');
        }
        $payer50x50 = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneByName('50/50');
        if(!$payer50x50 || !($payer50x50 instanceof FeePayerOption)) {
            throw new \Exception('No FeePayerOption with 50x50 found');
        }

        $releasedValueCustomer = $this->calculatedDataProvider
            ->getCalculatedReleasedValue($amount, $payerCustomer);
        $releasedValueContractor = $this->calculatedDataProvider
            ->getCalculatedReleasedValue($amount, $payerContractor);
        $releasedValue50x50 = $this->calculatedDataProvider
            ->getCalculatedReleasedValue($amount, $payer50x50);

        $this->assertEquals(true, (isset($feePercentage) && !empty($feePercentage)));
        $this->assertEquals($releasedAmountCustomer, $releasedValueCustomer);
        $this->assertEquals($releasedAmountContractor, $releasedValueContractor);
        $this->assertEquals($releasedAmount50x50, $releasedValue50x50);
    }
}