<?php

namespace ApplicationTest\Service\Payment;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Service\Payment\PaymentMethodPolitics;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethod;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Application\Service\UserManager;

class PaymentMethodPoliticsTest extends AbstractHttpControllerTestCase
{
    /**
     * @var \Application\Service\Payment\PaymentMethodPolitics
     */
    private $paymentMethodPolitics;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../../config/application.config.php',
                    include __DIR__ . '/../../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->paymentMethodPolitics = $serviceManager->get(PaymentMethodPolitics::class);
        $this->userManager = $serviceManager->get(UserManager::class);

        $config = $this->getApplicationConfig();
        $user = $this->userManager->getUserByLogin($config['tests']['user_login']);
        $this->user = $user;

        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        $this->entityManager->getConnection()->rollback();
        $this->entityManager->getConnection()->close();

        parent::tearDown();
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testIsLock()
    {
        $is_pattern = 1;

        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($this->user);
        $civilLawSubject->setIsPattern($is_pattern);
        $civilLawSubject->setCreated(new \DateTime(date('Y-m-d H:i:s')));
        $this->entityManager->persist($civilLawSubject);

        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)->findOneByName(PaymentMethodType::BANK_TRANSFER);
        if ($paymentMethodType === null) {
            throw new \Exception('Not found paymentMethodType');
        }

        $paymentMethod = new PaymentMethod();
        $paymentMethod->setCivilLawSubject($civilLawSubject);
        $paymentMethod->setPaymentMethodType($paymentMethodType);
        $paymentMethod->setIsPattern($is_pattern);
        $this->entityManager->persist($paymentMethod);

        $isLock = $this->paymentMethodPolitics->isLock($paymentMethod);

        // если $is_pattern = 1 то ожидаем что не заблокировано
        $this->assertEquals(false, $isLock);

        $is_pattern = 0;

        $paymentMethod->setIsPattern($is_pattern);
        $this->entityManager->persist($paymentMethod);
        $this->entityManager->flush();

        $isLock = $this->paymentMethodPolitics->isLock($paymentMethod);

        // если $is_pattern = 0 то ожидаем что заблокировано
        $this->assertEquals(true, $isLock);
    }
}