<?php
namespace ApplicationTest\Service\Dispute;

use Application\Entity\Deal;
use Application\Service\Dispute\DisputeHistoryManager;
use CoreTest\ViewVarsTrait;
use Doctrine\ORM\EntityManager;
use Core\Service\Base\BaseAuthManager;
use Zend\Session\SessionManager;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use ReflectionClass;

/**
 * Class DisputeHistoryManagerTest
 * @package ApplicationTest\Service\Dispute
 */
class DisputeHistoryManagerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;
    use DisputeHistoryBaseTestTrait;

    const CONFIRMED_DEAL_NAME = 'Сделка Оплаченная';
    //1. открытие спора
    const DEAL_DISPUTE_OPEN = 'Сделка Спор открыт';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            include __DIR__ . '/../../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollBack();
        $this->entityManager->getConnection()->close();
        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;

        gc_collect_cycles();
    }

    /**
     * Тест конструктора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \ReflectionException
     */
    public function testDisputeHistoryManagerConstruct()
    {
        // now call the constructor
        $reflectedClass = new ReflectionClass(DisputeHistoryManager::class);
        $constructor = $reflectedClass->getConstructor();
        $parameters = $constructor->getParameters();

        $this->assertEquals('rbacManager', $parameters[0]->getName());
        $this->assertEquals('disputeStatus', $parameters[1]->getName());
        $this->assertEquals('refundManager', $parameters[2]->getName());
    }

    /**
     * Тест наличия важных констант
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \ReflectionException
     */
    public function testDisputeHistoryManagerConstant()
    {
        $expected_const_arr = [
            'WAITING_DAY_AFTER_OPENED_DISPUTE',
            'ROLE_AUTHOR',
            'ROLE_NOT_AUTHOR',
            'ROLE_OBSERVER',
            'ROLE_OPERATOR',
            'ROLE_CUSTOMER',
            'ROLE_CONTRACTOR',
            'DISPUTE_OPEN',
            'DISCOUNT_REQUEST',
            'DISCOUNT_REQUEST_ACCEPTED',
            'DISCOUNT_REQUEST_REJECTED',
            'REFUND_REQUEST',
            'REFUND_REQUEST_ACCEPTED',
            'REFUND_REQUEST_REJECTED',
            'ARBITRAGE_REQUEST',
            'ARBITRAGE_REQUEST_ACCEPTED',
            'ARBITRAGE_REQUEST_REJECTED',
            'ARBITRAGE_REQUEST_WITH_DISCOUNT',
            'ARBITRAGE_REQUEST_WITH_REFUND',
            'ARBITRAGE_REQUEST_WITH_TRIBUNAL',
            'TRIBUNAL_REQUEST',
            'TRIBUNAL_REQUEST_ACCEPTED',
            'TRIBUNAL_REQUEST_REJECTED',
            'DISCOUNT',
            'REFUND',
            'TRIBUNAL',
            'WARRANTY_EXTENSION',
            'DISPUTE_CANCELED',
            'DISPUTE_CLOSED',
            'DISPUTE_CLOSED_REFUND',
            'DISPUTE_CLOSED_DISCOUNT',
            'DISPUTE_CLOSED_DISCOUNT_WITH_DAYS',
            'DISPUTE_CLOSED_WITH_DAYS',
        ];

        // now call the constructor
        $reflectedClass = new ReflectionClass(DisputeHistoryManager::class);
        $constants = $reflectedClass->getConstants();

        foreach ($expected_const_arr as $const_name) {
            $this->assertArrayHasKey($const_name, $constants);
        }
    }

    /**
     * Пустая коллекция если спор ни разу не открывался
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     */
    public function testGetDisputeCycleCollectionMethodForDealWithoutDispute()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);
        $this->dispatch('/deal/show/' . $deal->getId(), 'GET');
        $this->assertResponseStatusCode(200);
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertArrayHasKey('dispute_history', $view_vars);
        $this->assertEmpty($view_vars['dispute_history']);

    }//one_tribunal_dicline

    ///////////////////////////////////////////
    /// основные тесты для dispute history ///
    //////////////////////////////////////////

    /////////// 1. открытие спора ////////////
    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     */
    public function testHistoryWithDisputeOpenForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISPUTE_OPEN]);

        $this->getDisputeHistoryBaseTest($deal);
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertArrayHasKey('dispute_history', $view_vars);
        $dispute_history = $view_vars['dispute_history'];
        $this->assertInternalType('array', $dispute_history);

        $dispute_history_end = end($dispute_history);
        $this->assertArrayHasKey('code', $dispute_history_end);
        $this->assertArrayHasKey('data', $dispute_history_end);
        $this->assertArrayHasKey('created', $dispute_history_end);

        $this->assertEquals(DisputeHistoryManager::buildCode(
                                DisputeHistoryManager::DISPUTE_OPEN,
                                DisputeHistoryManager::ROLE_OPERATOR
                            ), $dispute_history_end['code']);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}