<?php

namespace ApplicationTest\Service\Deal;

use Application\Controller\DealController;
use Application\Controller\DisputeController;
use Application\Entity\ArbitrageConfirm;
use Application\Entity\ArbitrageRequest;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\DiscountConfirm;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\Refund;
use Application\Entity\RefundConfirm;
use Application\Entity\RefundRequest;
use Application\Entity\Tribunal;
use Application\Entity\TribunalConfirm;
use Application\Entity\TribunalRequest;
use Application\Entity\User;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Dispute\Status\InitialStatus;
use Application\Service\Dispute\Status\RefundIsMadeStatus;
use Application\Service\Dispute\Status\RefundStatus;
use Application\Service\Dispute\Status\TribunalStatus;
use Application\Service\Dispute\Status\ClosedStatus;
use Application\Service\Dispute\DisputeStatus;
use Application\Service\Dispute\Status\ArbitrageRequestAcceptedStatus;
use Application\Service\Dispute\Status\ArbitrageRequestRejectedStatus;
use Application\Service\Dispute\Status\ArbitrageRequestStatus;
use Application\Service\Dispute\Status\DiscountRequestAcceptedStatus;
use Application\Service\Dispute\Status\DiscountRequestRejectedStatus;
use Application\Service\Dispute\Status\DiscountRequestStatus;
use Application\Service\Dispute\Status\RefundRequestAcceptedStatus;
use Application\Service\Dispute\Status\RefundRequestRejectedStatus;
use Application\Service\Dispute\Status\RefundRequestStatus;
use Application\Service\Dispute\Status\TribunalRequestAcceptedStatus;
use Application\Service\Dispute\Status\TribunalRequestRejectedStatus;
use Application\Service\Dispute\Status\TribunalRequestStatus;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use CoreTest\ViewVarsTrait;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Session\SessionManager;
use Zend\Form\Element;

class DisputeStatusTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;
    const REFUND_IS_MADE = 'Сделка Ожидает возврата';
    const INITIAL = 'Сделка Спор открыт';
    const BUYER_HAS_SENT_A_REQUEST_FOR_REFUND = 'Сделка Покупатель направил запрос на Возврат';
    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var DisputeStatus
     */
    private $disputeStatus;

    /**
     * @var ArbitrageRequestAcceptedStatus
     */
    private $arbitrageRequestAcceptedStatus;

    /**
     * @var ArbitrageRequestRejectedStatus
     */
    private $arbitrageRequestRejectedStatus;

    /**
     * @var ArbitrageRequestStatus
     */
    private $arbitrageRequestStatus;

    /**
     * @var ClosedStatus
     */
    private $closedStatus;

    /**
     * @var DiscountRequestAcceptedStatus
     */
    private $discountRequestAcceptedStatus;

    /**
     * @var DiscountRequestRejectedStatus
     */
    private $discountRequestRejectedStatus;

    /**
     * @var DiscountRequestStatus
     */
    private $discountRequestStatus;

    /**
     * @var RefundRequestAcceptedStatus
     */
    private $refundRequestAcceptedStatus;

    /**
     * @var RefundRequestRejectedStatus
     */
    private $refundRequestRejectedStatus;

    /**
     * @var RefundRequestStatus
     */
    private $refundRequestStatus;

    /**
     * @var TribunalRequestAcceptedStatus
     */
    private $tribunalRequestAcceptedStatus;

    /**
     * @var TribunalRequestRejectedStatus
     */
    private $tribunalRequestRejectedStatus;

    /**
     * @var TribunalRequestStatus
     */
    private $tribunalRequestStatus;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var RefundStatus
     */
    private $refundStatus;

    /**
     * @var RefundIsMadeStatus
     */
    private $refundIsMadeStatus;

    /**
     * @var TribunalStatus
     */
    private $tribunalStatus;

    /**
     * @var InitialStatus
     */
    private $initialStatus;


    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                include __DIR__ . '/../../../../../config/application.config.php',
                include __DIR__ . '/../../../../../config/autoload/global.php'
            )
        );

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];
        // Отключаем отправку уведомлений на email
        $this->resetConfigParameters();
        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->disputeStatus = $serviceManager->get(DisputeStatus::class);
        $this->arbitrageRequestAcceptedStatus = $serviceManager->get(ArbitrageRequestAcceptedStatus::class);
        $this->arbitrageRequestRejectedStatus = $serviceManager->get(ArbitrageRequestRejectedStatus::class);
        $this->arbitrageRequestStatus = $serviceManager->get(ArbitrageRequestStatus::class);
        $this->closedStatus = $serviceManager->get(ClosedStatus::class);
        $this->discountRequestAcceptedStatus = $serviceManager->get(DiscountRequestAcceptedStatus::class);
        $this->discountRequestRejectedStatus = $serviceManager->get(DiscountRequestRejectedStatus::class);
        $this->discountRequestStatus = $serviceManager->get(DiscountRequestStatus::class);
        $this->refundRequestAcceptedStatus = $serviceManager->get(RefundRequestAcceptedStatus::class);
        $this->refundRequestRejectedStatus = $serviceManager->get(RefundRequestRejectedStatus::class);
        $this->refundRequestStatus = $serviceManager->get(RefundRequestStatus::class);
        $this->tribunalRequestAcceptedStatus = $serviceManager->get(TribunalRequestAcceptedStatus::class);
        $this->tribunalRequestRejectedStatus = $serviceManager->get(TribunalRequestRejectedStatus::class);
        $this->tribunalRequestStatus = $serviceManager->get(TribunalRequestStatus::class);
        $this->refundStatus = $serviceManager->get(RefundStatus::class);
        $this->refundIsMadeStatus = $serviceManager->get(RefundIsMadeStatus::class);
        $this->tribunalStatus = $serviceManager->get(TribunalStatus::class);
        $this->initialStatus = $serviceManager->get(InitialStatus::class);
        $this->disputeManager =  $serviceManager->get(DisputeManager::class);
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        $this->entityManager->getConnection()->rollBack();
        $this->entityManager->getConnection()->close();

        parent::tearDown();
    }
    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    ////////////// Initial

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     */
    public function testInitialStatus()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::INITIAL]);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        // InitialStatus
        $isInStatus_1 = $this->initialStatus->isDisputeInStatus($dispute);
        // Проверка класса InitialStatus до закрытия спора
        $this->assertTrue($isInStatus_1);

        // DisputeStatus
        $status = $this->disputeStatus->getStatus($dispute);
        // Проверка класса DisputeStatus
        $this->assertInternalType('array', $status);
        $this->assertArrayHasKey('code', $status);
        $this->assertArrayHasKey('status', $status);
        $this->assertEquals('initial', $status['status']);
        $this->assertArrayHasKey('is_blocker', $status);
        $this->assertEquals(false, $status['is_blocker']);
        $this->assertArrayHasKey('name', $status);
        $this->assertEquals('Первичный', $status['name']);

        // Закрываем спор
        $this->closeDispute($dispute);

        // InitialStatus
        $isInStatus_2 = $this->initialStatus->isDisputeInStatus($dispute);
        // Проверка класса InitialStatus после закрытия спора
        $this->assertFalse($isInStatus_2);
    }

    /**
     *проверяем что оператору приходит верный статус
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @group deal
     * @group dispute
     * @group dispute-status
     * @throws \Exception
     */
    public function testCloseDealOperatorInformationCheck()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::BUYER_HAS_SENT_A_REQUEST_FOR_REFUND]);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var User $customer */
        $customer = $deal->getCustomer()->getUser();
        $this->login($customer->getLogin());
        $csrfElement = new Element\Csrf('csrf');
        $isXmlHttpRequest = true;
        $postData = [
            'csrf' => $csrfElement->getValue(),
        ];
        $isOperator=false;
 /** @var Dispute $dispute */
        $dispute = $this->disputeManager->closeDispute($postData, $dispute, $isOperator);
        $this->logout();
        $this->login('operator');
        $this->dispatch('/deal/show/' . $deal->getId(), 'GET');
        $view_vars = $this->getViewVars();
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/show');
        $data = end($view_vars['dispute_history']);
        $this->assertTrue($dispute->isClosed());
        $this->assertTrue($data['code']=='DISPUTE_CANCELED_FOR_OPERATOR');

    }

    ////////////// Arbitrage

    /**
     * @return array
     */
    public function givenDataForArbitrageRequestAcceptedStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], true], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], false], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], false], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], false], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], true], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], false], // последний реквест отвергнут
            [[['confirm' => 'yes', 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForArbitrageRequestAcceptedStatus
     */
    public function testArbitrageRequestAcceptedStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем ArbitrageRequest с нужными параметрами
            $this->createArbitrageRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрыть спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // ArbitrageRequestAcceptedStatus
        $isInStatus = $this->arbitrageRequestAcceptedStatus->isDisputeInStatus($dispute);
        // Проверка класса ArbitrageRequestAcceptedStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('arbitrage_request_accepted', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на согласительную комиссию принят', $status['name']);
        }
    }

    /**
     * @return array
     */
    public function givenDataForArbitrageRequestRejectedStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], false], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], true], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], false], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], false], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], false], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], true], // последний реквест отвергнут
            [[['confirm' => 'no', 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForArbitrageRequestRejectedStatus
     */
    public function testArbitrageRequestRejectedStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем ArbitrageRequest с нужными параметрами
            $this->createArbitrageRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрыть спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // ArbitrageRequestAcceptedStatus
        $isInStatus = $this->arbitrageRequestRejectedStatus->isDisputeInStatus($dispute);
        // Проверка класса ArbitrageRequestRejectedStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('arbitrage_request_rejected', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на согласительную комиссию отклонен', $status['name']);
        }
    }

    /**
     * @return array
     */
    public function givenDataForArbitrageRequestStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], false], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], false], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], true], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], true], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], false], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], false], // последний реквест отвергнут
            [[['confirm' => null, 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForArbitrageRequestStatus
     */
    public function testArbitrageRequestStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем ArbitrageRequest с нужными параметрами
            $this->createArbitrageRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрываем спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // ArbitrageRequestAcceptedStatus
        $isInStatus = $this->arbitrageRequestStatus->isDisputeInStatus($dispute);
        // Проверка класса ArbitrageRequestStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('arbitrage_request', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            $this->assertEquals('Запрос на согласительную комиссию', $status['name']);
        }
    }

    ////// Closed

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     */
    public function testClosedStatus()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        // СlosedStatus
        $isInStatus_1 = $this->closedStatus->isDisputeInStatus($dispute);
        // Проверка класса ClosedStatus до закрытия спора
        $this->assertFalse($isInStatus_1);

        // Закрываем спор
        $this->closeDispute($dispute);

        // СlosedStatus
        $isInStatus_2 = $this->closedStatus->isDisputeInStatus($dispute);
        // Проверка класса ClosedStatus после закрытия спора
        $this->assertTrue($isInStatus_2);

        // DisputeStatus
        $status = $this->disputeStatus->getStatus($dispute);
        // Проверка класса DisputeStatus
        $this->assertInternalType('array', $status);
        $this->assertArrayHasKey('code', $status);
        $this->assertArrayHasKey('status', $status);
        $this->assertEquals('closed', $status['status']);
        $this->assertArrayHasKey('is_blocker', $status);
        $this->assertEquals(true, $status['is_blocker']);
        $this->assertArrayHasKey('name', $status);
        $this->assertEquals('Спор закрыт', $status['name']);
    }

    ////////////// Discount

    /**
     * @return array
     */
    public function givenDataForDiscountRequestAcceptedStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], true], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], false], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], false], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], false], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], true], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], false], // последний реквест отвергнут
            [[['confirm' => 'yes', 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForDiscountRequestAcceptedStatus
     */
    public function testDiscountRequestAcceptedStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем DiscountRequest с нужными параметрами
            $this->createDiscountRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрыть спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // DiscountRequestAcceptedStatus
        $isInStatus = $this->discountRequestAcceptedStatus->isDisputeInStatus($dispute);
        // Проверка класса DiscountRequestAcceptedStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('discount_request_accepted', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на скидку принят', $status['name']);
        }
    }

    /**
     * @return array
     */
    public function givenDataForDiscountRequestRejectedStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], false], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], true], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], false], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], false], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], false], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], true], // последний реквест отвергнут
            [[['confirm' => 'no', 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForDiscountRequestRejectedStatus
     */
    public function testDiscountRequestRejectedStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем DiscountRequest с нужными параметрами
            $this->createDiscountRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрыть спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // DiscountRequestAcceptedStatus
        $isInStatus = $this->discountRequestRejectedStatus->isDisputeInStatus($dispute);
        // Проверка класса DiscountRequestRejectedStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('discount_request_rejected', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на скидку отклонен', $status['name']);
        }
    }

    /**
     * @return array
     */
    public function givenDataForDiscountRequestStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], false], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], false], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], true], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], true], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], false], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], false], // последний реквест отвергнут
            [[['confirm' => null, 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForDiscountRequestStatus
     */
    public function testDiscountRequestStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем DiscountRequest с нужными параметрами
            $this->createDiscountRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрываем спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // DiscountRequestAcceptedStatus
        $isInStatus = $this->discountRequestStatus->isDisputeInStatus($dispute);
        // Проверка класса DiscountRequestStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('discount_request', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на скидку', $status['name']);
        }
    }

    ////////////// Refund

    /**
     * @return array
     */
    public function givenDataForRefundRequestAcceptedStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], true], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], false], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], false], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], false], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], true], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], false], // последний реквест отвергнут
            [[['confirm' => 'yes', 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForRefundRequestAcceptedStatus
     */
    public function testRefundRequestAcceptedStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем RefundRequest с нужными параметрами
            $this->createRefundRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрыть спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // RefundRequestAcceptedStatus
        $isInStatus = $this->refundRequestAcceptedStatus->isDisputeInStatus($dispute);
        // Проверка класса RefundRequestAcceptedStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('refund_request_accepted', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на возврат принят', $status['name']);
        }
    }

    /**
     * @return array
     */
    public function givenDataForRefundRequestRejectedStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], false], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], true], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], false], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], false], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], false], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], true], // последний реквест отвергнут
            [[['confirm' => 'no', 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForRefundRequestRejectedStatus
     */
    public function testRefundRequestRejectedStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем RefundRequest с нужными параметрами
            $this->createRefundRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрыть спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // RefundRequestAcceptedStatus
        $isInStatus = $this->refundRequestRejectedStatus->isDisputeInStatus($dispute);
        // Проверка класса RefundRequestRejectedStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('refund_request_rejected', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на возврат отклонен', $status['name']);
        }
    }

    /**
     * @return array
     */
    public function givenDataForRefundRequestStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], false], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], false], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], true], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], true], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], false], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], false], // последний реквест отвергнут
            [[['confirm' => null, 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForRefundRequestStatus
     */
    public function testRefundRequestStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем RefundRequest с нужными параметрами
            $this->createRefundRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрываем спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // ArbitrageRequestAcceptedStatus
        $isInStatus = $this->refundRequestStatus->isDisputeInStatus($dispute);
        // Проверка класса RefundRequestStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('refund_request', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на возврат', $status['name']);
        }
    }

    // RefundInMadeStatus

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     */
    public function testRefundIsMadeStatus()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_IS_MADE]);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        // RefundInMadeStatus
        $isInStatus = $this->refundIsMadeStatus->isDisputeInStatus($dispute);

        // Проверка класса RefundInMadeStatus
        $this->assertTrue($isInStatus);

        // DisputeStatus
        $status = $this->disputeStatus->getStatus($dispute);
        // Проверка класса DisputeStatus
        $this->assertInternalType('array', $status);
        $this->assertArrayHasKey('code', $status);
        $this->assertArrayHasKey('status', $status);
        $this->assertEquals('refund_is_made', $status['status']);
        $this->assertArrayHasKey('is_blocker', $status);
        $this->assertEquals(true, $status['is_blocker']);
        $this->assertArrayHasKey('name', $status);
        #$this->assertEquals('Запрос на трибунал', $status['name'])
    }

    // @TODO RefundStatus

    /**
     * @return array
     */
    public function givenDataForRefundStatus(): array
    {
        return [
            [ // единственный (он же последний) реквест подтвержден и создан возврат
                ['confirm' => 'yes', 'past' => null, 'refund' => true],
                ['refund_status' => true, 'dispute_status' => 'refund']
            ],

            [ // единственный (он же последний) реквест отвергнут, не создан возврат
                ['confirm' => 'no', 'past' => null, 'refund' => false],
                ['refund_status' => false, 'dispute_status' => 'refund_request_rejected']
            ],

            [ // единственный (он же последний) реквест еще не отвечен, не создан возврат
                ['confirm' => null, 'past' => null, 'refund' => false],
                ['refund_status' => false, 'dispute_status' => 'refund_request']
            ],

            [ // спор закрыт
                ['confirm' => null, 'past' => null, 'dispute_closed' => true],
                ['refund_status' => false, 'dispute_status' => 'closed']
            ],

            [ // создан трибунал, но спор закрыт
                ['confirm' => null, 'past' => null, 'dispute_closed' => true, 'refund' => true],
                ['refund_status' => false, 'dispute_status' => 'closed']
            ],
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForRefundStatus
     */
    public function testRefundStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        // Создаем RefundRequest с нужными параметрами
        $this->createRefundRequest($dispute, $author, $given['confirm'], $given['past']);

        if (array_key_exists('refund', $given) && $given['refund'] === true) {
            $this->createRefund($dispute);
        }
        // Закрываем спор, если нужно
        if (array_key_exists('dispute_closed', $given) && $given['dispute_closed'] === true) {
            $this->closeDispute($dispute);
        }

        // RefundStatus
        $isInStatus = $this->refundStatus->isDisputeInStatus($dispute);

        // Проверка класса RefundStatus
        $this->assertSame($expected['refund_status'], $isInStatus);

        // DisputeStatus
        $status = $this->disputeStatus->getStatus($dispute);
        // Проверка класса DisputeStatus
        $this->assertInternalType('array', $status);
        $this->assertArrayHasKey('code', $status);
        $this->assertArrayHasKey('status', $status);
        $this->assertEquals($expected['dispute_status'], $status['status']);
        $this->assertArrayHasKey('is_blocker', $status);
        #$this->assertEquals(true, $status['is_blocker']);
        $this->assertArrayHasKey('name', $status);
        #$this->assertEquals('Запрос на трибунал', $status['name'])
    }

    ////////////// Tribunal

    /**
     * @return array
     */
    public function givenDataForTribunalRequestAcceptedStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], true], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], false], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], false], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], false], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], true], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], false], // последний реквест отвергнут
            [[['confirm' => 'yes', 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForTribunalRequestAcceptedStatus
     */
    public function testTribunalRequestAcceptedStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем TribunalRequest с нужными параметрами
            $this->createTribunalRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрыть спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // TribunalRequestAcceptedStatus
        $isInStatus = $this->tribunalRequestAcceptedStatus->isDisputeInStatus($dispute);
        // Проверка класса TribunalRequestAcceptedStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('tribunal_request_accepted', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на согласительную комиссию принят', $status['name']);
        }
    }

    /**
     * @return array
     */
    public function givenDataForTribunalRequestRejectedStatus() :array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], false], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], true], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], false], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], false], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], false], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], true], // последний реквест отвергнут
            [[['confirm' => 'no', 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForTribunalRequestRejectedStatus
     */
    public function testTribunalRequestRejectedStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем TribunalRequest с нужными параметрами
            $this->createTribunalRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрыть спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // TribunalRequestAcceptedStatus
        $isInStatus = $this->tribunalRequestRejectedStatus->isDisputeInStatus($dispute);
        // Проверка класса TribunalRequestRejectedStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('tribunal_request_rejected', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на согласительную комиссию отклонен', $status['name']);
        }
    }

    /**
     * @return array
     */
    public function givenDataForTribunalRequestStatus(): array
    {
        return [
            [[['confirm' => 'yes', 'past' => null]], false], // единственный (он же последний) реквест подтвержден
            [[['confirm' => 'no', 'past' => null]], false], // единственный (он же последний) реквест отвергнут
            [[['confirm' => null, 'past' => null]], true], // единственный (он же последний) реквест еще не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => null, 'past' => null]], true], // последний реквест не отвечен
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'yes', 'past' => null]], false], // последний реквест подтвержден
            [[['confirm' => 'no', 'past' => '1'],['confirm' => 'no', 'past' => null]], false], // последний реквест отвергнут
            [[['confirm' => null, 'past' => null, 'dispute_closed' => true]], false], // спор закрыт
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForTribunalRequestStatus
     */
    public function testTribunalRequestStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        foreach ($given as $request) {
            // Создаем TribunalRequest с нужными параметрами
            $this->createTribunalRequest($dispute, $author, $request['confirm'], $request['past']);
            // Закрываем спор, если нужно
            if (array_key_exists('dispute_closed', $request) && $request['dispute_closed'] === true) {
                $this->closeDispute($dispute);
            }
        }

        // TribunalRequestStatus
        $isInStatus = $this->tribunalRequestStatus->isDisputeInStatus($dispute);
        // Проверка класса TribunalRequestStatus
        $this->assertSame($expected, $isInStatus);

        if ($expected === true) {
            // DisputeStatus
            $status = $this->disputeStatus->getStatus($dispute);
            // Проверка класса DisputeStatus
            $this->assertInternalType('array', $status);
            $this->assertArrayHasKey('code', $status);
            $this->assertArrayHasKey('status', $status);
            $this->assertEquals('tribunal_request', $status['status']);
            $this->assertArrayHasKey('is_blocker', $status);
            $this->assertEquals(false, $status['is_blocker']);
            $this->assertArrayHasKey('name', $status);
            #$this->assertEquals('Запрос на трибунал', $status['name']);
        }
    }

    /**
     * @return array
     */
    public function givenDataForTribunalStatus(): array
    {
        return [
            [ // единственный (он же последний) реквест подтвержден и создан трибунал
                ['confirm' => 'yes', 'past' => null, 'tribunal' => true],
                ['tribunal_status' => true, 'dispute_status' => 'tribunal']
            ],

            [ // единственный (он же последний) реквест отвергнут, не создан трибунал
                ['confirm' => 'no', 'past' => null, 'tribunal' => false],
                ['tribunal_status' => false, 'dispute_status' => 'tribunal_request_rejected']
            ],

            [ // единственный (он же последний) реквест еще не отвечен, не создан трибунал
                ['confirm' => null, 'past' => null, 'tribunal' => false],
                ['tribunal_status' => false, 'dispute_status' => 'tribunal_request']
            ],

            [ // спор закрыт
                ['confirm' => null, 'past' => null, 'dispute_closed' => true],
                ['tribunal_status' => false, 'dispute_status' => 'closed']
            ],

            [ // создан трибунал, но спор закрыт
                ['confirm' => null, 'past' => null, 'dispute_closed' => true, 'tribunal' => true],
                ['tribunal_status' => false, 'dispute_status' => 'closed']
            ],
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group dispute-status
     *
     * @dataProvider givenDataForTribunalStatus
     */
    public function testTribunalStatus($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DealAgent $author */
        $author = $deal->getCustomer();

        // Создаем TribunalRequest с нужными параметрами
        $this->createTribunalRequest($dispute, $author, $given['confirm'], $given['past']);

        if (array_key_exists('tribunal', $given) && $given['tribunal'] === true) {
            $tribunal = $this->createTribunal($dispute);
        }
        // Закрываем спор, если нужно
        if (array_key_exists('dispute_closed', $given) && $given['dispute_closed'] === true) {
            $this->closeDispute($dispute);
        }

        // TribunalStatus
        $isInStatus = $this->tribunalStatus->isDisputeInStatus($dispute);

        // Проверка класса TribunalStatus
        $this->assertSame($expected['tribunal_status'], $isInStatus);

        // DisputeStatus
        $status = $this->disputeStatus->getStatus($dispute);
        // Проверка класса DisputeStatus
        $this->assertInternalType('array', $status);
        $this->assertArrayHasKey('code', $status);
        $this->assertArrayHasKey('status', $status);
        $this->assertEquals($expected['dispute_status'], $status['status']);
        $this->assertArrayHasKey('is_blocker', $status);
        #$this->assertEquals(true, $status['is_blocker']);
        $this->assertArrayHasKey('name', $status);
        #$this->assertEquals('Запрос на трибунал', $status['name'])
    }



    /**
     * @param Dispute $dispute
     * @return Dispute
     */
    private function closeDispute(Dispute $dispute)
    {
        $dispute->setClosed(true);

        $this->entityManager->persist($dispute);
        $this->entityManager->flush($dispute);

        return $dispute;
    }

    /**
     * @param Dispute $dispute
     * @param DealAgent $author
     * @param $confirm
     * @param $past
     * @return ArbitrageRequest
     */
    private function createArbitrageRequest(Dispute $dispute, DealAgent $author, $confirm, $past) :ArbitrageRequest
    {
        $date = clone new \DateTime();
        if (null !== $past) {
            $date = $date->modify('-'.$past.' hour');
        }
        $arbitrageRequest = new ArbitrageRequest();
        $arbitrageRequest->setCreated($date);
        $arbitrageRequest->setDispute($dispute);
        $arbitrageRequest->setAuthor($author);

        $this->entityManager->persist($arbitrageRequest);
        $this->entityManager->flush($arbitrageRequest);

        if (null === $confirm) {

            return $arbitrageRequest;
        }

        $confirm_status = $confirm === 'yes';

        // Создаем Confirm
        $this->createArbitrageConfirm($arbitrageRequest, $author, $confirm_status);

        return $arbitrageRequest;
    }

    /**
     * @param Dispute $dispute
     * @param DealAgent $author
     * @param $confirm
     * @param $past
     * @return DiscountRequest
     */
    private function createDiscountRequest(Dispute $dispute, DealAgent $author, $confirm, $past) :DiscountRequest
    {
        $date = clone new \DateTime();
        if (null !== $past) {
            $date = $date->modify('-'.$past.' hour');
        }
        $discountRequest = new DiscountRequest();
        $discountRequest->setAmount(100);
        $discountRequest->setCreated($date);
        $discountRequest->setDispute($dispute);
        $discountRequest->setAuthor($author);

        $this->entityManager->persist($discountRequest);
        $this->entityManager->flush($discountRequest);

        if (null === $confirm) {

            return $discountRequest;
        }

        $confirm_status = $confirm === 'yes';

        // Создаем Confirm
        $this->createDiscountConfirm($discountRequest, $author, $confirm_status);

        return $discountRequest;
    }

    /**
     * @param Dispute $dispute
     * @param DealAgent $author
     * @param $confirm
     * @param $past
     * @return RefundRequest
     */
    private function createRefundRequest(Dispute $dispute, DealAgent $author, $confirm, $past) :RefundRequest
    {
        $date = clone new \DateTime();
        if (null !== $past) {
            $date = $date->modify('-'.$past.' hour');
        }
        $refundRequest = new RefundRequest();
        $refundRequest->setCreated($date);
        $refundRequest->setDispute($dispute);
        $refundRequest->setAuthor($author);

        $this->entityManager->persist($refundRequest);
        $this->entityManager->flush($refundRequest);

        if (null === $confirm) {

            return $refundRequest;
        }

        $confirm_status = $confirm === 'yes';

        // Создаем Confirm
        $this->createRefundConfirm($refundRequest, $author, $confirm_status);

        return $refundRequest;
    }

    /**
     * @param Dispute $dispute
     * @param DealAgent $author
     * @param $confirm
     * @param $past
     * @return TribunalRequest
     */
    private function createTribunalRequest(Dispute $dispute, DealAgent $author, $confirm, $past) :TribunalRequest
    {
        $date = clone new \DateTime();
        if (null !== $past) {
            $date = $date->modify('-'.$past.' hour');
        }
        $tribunalRequest = new TribunalRequest();
        $tribunalRequest->setCreated($date);
        $tribunalRequest->setDispute($dispute);
        $tribunalRequest->setAuthor($author);

        $this->entityManager->persist($tribunalRequest);
        $this->entityManager->flush($tribunalRequest);

        if (null === $confirm) {

            return $tribunalRequest;
        }

        $confirm_status = $confirm === 'yes';

        // Создаем Confirm
        $this->createTribunalConfirm($tribunalRequest, $author, $confirm_status);

        return $tribunalRequest;
    }

    /**
     * @param ArbitrageRequest $arbitrageRequest
     * @param DealAgent $author
     * @param bool $is_confirm
     * @return ArbitrageConfirm
     */
    private function createArbitrageConfirm(ArbitrageRequest $arbitrageRequest, DealAgent $author, bool $is_confirm): ArbitrageConfirm
    {
        $arbitrageConfirm = new ArbitrageConfirm();
        $arbitrageConfirm->setAuthor($author->getUser());
        $arbitrageConfirm->setCreated(new \DateTime());
        $arbitrageConfirm->setArbitrageRequest($arbitrageRequest);
        $arbitrageConfirm->setStatus($is_confirm);

        $this->entityManager->persist($arbitrageConfirm);

        $arbitrageRequest->setConfirm($arbitrageConfirm);

        $this->entityManager->persist($arbitrageRequest);
        $this->entityManager->flush($arbitrageRequest);

        return $arbitrageConfirm;
    }

    /**
     * @param DiscountRequest $discountRequest
     * @param DealAgent $author
     * @param bool $is_confirm
     * @return DiscountConfirm
     */
    private function createDiscountConfirm(DiscountRequest $discountRequest, DealAgent $author, bool $is_confirm): DiscountConfirm
    {
        $discountConfirm = new DiscountConfirm();
        $discountConfirm->setAuthor($author);
        $discountConfirm->setCreated(new \DateTime());
        $discountConfirm->setDiscountRequest($discountRequest);
        $discountConfirm->setStatus($is_confirm);

        $this->entityManager->persist($discountConfirm);

        $discountRequest->setConfirm($discountConfirm);

        $this->entityManager->persist($discountRequest);
        $this->entityManager->flush($discountRequest);

        return $discountConfirm;
    }

    /**
     * @param RefundRequest $refundRequest
     * @param DealAgent $author
     * @param bool $is_confirm
     * @return RefundConfirm
     */
    private function createRefundConfirm(RefundRequest $refundRequest, DealAgent $author, bool $is_confirm): RefundConfirm
    {
        $refundConfirm = new RefundConfirm();
        $refundConfirm->setAuthor($author);
        $refundConfirm->setCreated(new \DateTime());
        $refundConfirm->setRefundRequest($refundRequest);
        $refundConfirm->setStatus($is_confirm);

        $this->entityManager->persist($refundConfirm);

        $refundRequest->setConfirm($refundConfirm);

        $this->entityManager->persist($refundRequest);
        $this->entityManager->flush($refundRequest);

        return $refundConfirm;
    }

    /**
     * @param TribunalRequest $tribunalRequest
     * @param DealAgent $author
     * @param bool $is_confirm
     * @return TribunalConfirm
     */
    private function createTribunalConfirm(TribunalRequest $tribunalRequest, DealAgent $author, bool $is_confirm) :TribunalConfirm
    {
        $tribunalConfirm = new TribunalConfirm();
        $tribunalConfirm->setAuthor($author->getUser());
        $tribunalConfirm->setCreated(new \DateTime());
        $tribunalConfirm->setTribunalRequest($tribunalRequest);
        $tribunalConfirm->setStatus($is_confirm);

        $this->entityManager->persist($tribunalConfirm);

        $tribunalRequest->setConfirm($tribunalConfirm);

        $this->entityManager->persist($tribunalRequest);
        $this->entityManager->flush($tribunalRequest);

        return $tribunalConfirm;
    }

    /**
     * @param Dispute $dispute
     * @return Tribunal
     */
    private function createTribunal(Dispute $dispute) :Tribunal
    {
        $tribunal = new Tribunal();
        $tribunal->setCreated(new \DateTime());
        $tribunal->setDispute($dispute);

        $dispute->setTribunal($tribunal);

        $this->entityManager->persist($tribunal);
        $this->entityManager->persist($dispute);
        $this->entityManager->flush();

        return $tribunal;
    }

    /**
     * @param Dispute $dispute
     * @return Refund
     */
    private function createRefund(Dispute $dispute) :Refund
    {
        $refund = new Refund();
        $refund->setCreated(new \DateTime());
        $refund->setDispute($dispute);

        $dispute->setRefund($refund);

        $this->entityManager->persist($refund);
        $this->entityManager->persist($dispute);
        $this->entityManager->flush();

        return $refund;
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * Разлогиневаем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function logout()
    {
        $this->baseAuthManager->logout();
    }
}