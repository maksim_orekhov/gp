<?php
namespace ApplicationTest\Service\Dispute;

use Application\Controller\DealController;
use Application\Controller\DisputeController;
use Application\Entity\Deal;
use Application\Entity\Dispute;

trait DisputeHistoryBaseTestTrait
{
    /**
     * @param Deal|null $deal
     * @throws \Exception
     */
    private function getDisputeHistoryBaseTest(Deal $deal = null)
    {
        $this->assertNotNull($deal);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);

        $this->dispatch('/deal/show/' . $deal->getId(), 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/show');
    }
}