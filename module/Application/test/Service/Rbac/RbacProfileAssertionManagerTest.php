<?php

namespace ApplicationTest\Service\Rbac;

use Application\Entity\CivilLawSubject;
use Application\Entity\NaturalPerson;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Permissions\Rbac\Rbac;
use Zend\Session\SessionManager;
use Application\Entity\User;

class RbacProfileAssertionManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    private $user_login;
    private $user_login_2;
    private $user_password;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    protected $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * @var \ModuleRbac\Service\RbacProfileAssertionManager
     */
    private $assertionManager;

    /**
     * RBAC service.
     * @var \Zend\Permissions\Rbac\Rbac
     */
    private $rbac;

    /**
     * @var \ModuleRbac\Service\RoleManager
     */
    private $roleManager;

    /**
     * @var \ModuleRbac\Service\PermissionManager
     */
    private $permissionManager;

    /**
     * @var string
     *
     * Предполагается, что permission $this->testing_permission уже есть в базе.
     * Именно она проверяется в тестируемом классе(RbacAssertionManager).
     * Если что-то меняется, иметь в виду!
     */
    private $testing_permission = 'profile.own.edit';

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            include __DIR__ . '/../../../../../config/autoload/global.php'
        ));
        $serviceManager = $this->getApplicationServiceLocator();

        parent::setUp();

        $config = $this->getApplicationConfig();

        $this->user_login = $config['tests']['user_login'];
        $this->user_login_2 = 'test2';
        $this->user_password = $config['tests']['user_password'];

        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->rbacManager = $serviceManager->get(\ModuleRbac\Service\RbacManager::class);
        $this->assertionManager = $serviceManager->get(\Application\Service\Rbac\RbacProfileAssertionManager::class);
        $this->roleManager = $serviceManager->get(\ModuleRbac\Service\RoleManager::class);
        $this->permissionManager = $serviceManager->get(\ModuleRbac\Service\PermissionManager::class);

        // Create Rbac container.
        $this->rbac = new Rbac();

        // Перед каждым тестом обновляем кэш
        $this->rbacManager->init(true);
    }

    public function tearDown() {

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
    }

    /**
     * Дополнительная динамическая проверка для неавторизованного пользователя
     * Сейчас проверяем случай, когда никто не авторизован
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testAssertForUnauthorizedUser()
    {
        $user = $this->userManager->getUserByLogin($this->user_login);

        $params = [
            'user' => $user
        ];

        $this->assertFalse(false, $this->assertionManager->assert($this->rbac, $this->testing_permission, $params));
    }

    /**
     * Дополнительная динамическая проверка для неавторизованного пользователя
     * Сейчас проверяем случай, когда авторизован другой пользователь
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testAssertForUnauthorizedUserWhenAuthorizedOtherUser()
    {
        // Authorize user
        $this->baseAuthManager->login($this->user_login_2, $this->user_password, 0);

        $this->assertEquals($this->user_login_2, $this->baseAuthManager->getIdentity());

        $user = $this->userManager->getUserByLogin($this->user_login);

        $params = [
            'user' => $user
        ];

        $this->assertFalse(false, $this->assertionManager->assert($this->rbac, $this->testing_permission, $params));
    }

    /**
     * Дополнительная динамическая проверка для авторизованного пользователя
     * Предполагается, что permission $this->testing_permission уже есть в базе.
     * Именно она проверяется в тестируемом классе(RbacProfileAssertionManager).
     * Если что-то меняется, иметь в виду!
     *
     */
    public function testAssertForAuthorizedUser()
    {
        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();

        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();

        // Authorize user
        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $role_name = 'TEST_ROLE';
        $other_permission = 'super.permission';

        $user = $this->userManager->getUserByLogin($this->user_login);

        $this->dataPreparation($user, $role_name, $this->testing_permission, $other_permission);

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(array('user' => $user, 'isPattern' => true));

        $params_with_civilLawSubject = [
            'user' => $user,
            'civilLawSubject' => $civilLawSubject
        ];

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('civilLawSubject' => $civilLawSubject));

        $params_with_naturalPerson = [
            'user' => $user,
            'naturalPerson' => $naturalPerson
        ];

        // @TODO Реализовать по возможности asserts с этими параметрами
        $params_with_legalEntity = [
            'user' => $user,
            'naturalPerson' => null
        ];
        $params_with_naturalPersonDocument = [
            'user' => $user,
            'naturalPersonDocument' => null
        ];
        $params_with_legalEntityDocument = [
            'user' => $user,
            'legalEntityDocument' => null
        ];


        $params2 = [
            'other_key' => $user
        ];

        // Test
        $this->assertEquals(true, $this->assertionManager->assert($this->rbac, $this->testing_permission, $params_with_civilLawSubject));
        $this->assertEquals(false, $this->assertionManager->assert($this->rbac, $other_permission, $params_with_civilLawSubject));
        $this->assertEquals(true, $this->assertionManager->assert($this->rbac, $this->testing_permission, $params_with_naturalPerson));
        $this->assertEquals(false, $this->assertionManager->assert($this->rbac, $other_permission, $params_with_naturalPerson));
        $this->assertEquals(false, $this->assertionManager->assert($this->rbac, $other_permission, $params2));

        // Transaction rollback
        $this->entityManager->getConnection()->rollback();
    }

    /**
     * Дополнительная динамическая проверка для авторизованного пользователя через isGranted (rbacManager)
     * Предполагается, что permission $this->testing_permission уже есть в базе.
     * Именно она проверяется в тестируемом классе(RbacAssertionManager).
     * Если что-то меняется, иметь в виду!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testAssertForAuthorizedUserThroughIsGranted()
    {
        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();

        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        // Authorize user
        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);

        $role_name = 'TEST_ROLE';
        $other_permission = 'super.permission';

        // Get user from DB
        $user = $this->userManager->getUserByLogin($this->user_login);

        $this->dataPreparation($user, $role_name, $this->testing_permission, $other_permission);

        $params_with_civilLawSubject = [
            'user' => $user,
            'civilLawSubject' => $user->getCivilLawSubjects()->first()
        ];

        // Test
        $this->assertEquals(true, $this->rbacManager->isGranted($user, $this->testing_permission, $params_with_civilLawSubject));
        $this->assertEquals(true, $this->rbacManager->isGranted(null, $this->testing_permission, $params_with_civilLawSubject));
        $this->assertEquals(false, $this->rbacManager->isGranted($user, $other_permission, $params_with_civilLawSubject));
        $this->assertEquals(false, $this->rbacManager->isGranted(null, $other_permission, $params_with_civilLawSubject));

        // Transaction rollback
        $this->entityManager->getConnection()->rollback();
    }

    /**
     * @param User $user
     * @param $role_name
     * @param $testing_permission
     * @param $other_permission
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    private function dataPreparation(User $user, $role_name, $testing_permission, $other_permission)
    {
        // Add new role to DB
        $this->roleManager->addRole(['name' => $role_name, 'description' => '']);
        // Add permissions to DB
        $this->permissionManager->addPermission(['name' => $other_permission, 'description' => '']);

        // Get new role from DB
        $role = $this->roleManager->getRoleByName($role_name);
        // Get new permission from DB
        $permission = $this->permissionManager->getPermissionByName($testing_permission);

        // Assign permission to role
        $role->addPermission($permission);

        // Assign role to user
        $user->addRole($role);

        // Save
        $this->entityManager->flush();

        // Инициализируем Rbac и перезаписываем кэш
        $this->rbacManager->init(true);
    }
}