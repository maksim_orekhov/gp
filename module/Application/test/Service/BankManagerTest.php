<?php

namespace ApplicationTest\Service;

use PHPUnit\Framework\TestCase;
use Application\Service\BankManager;

class BankManagerTest extends TestCase
{
    const BANKS = [
        [
            'BIC' => '044525225',
            'OrgName' => 'СБЕРБАНК РОССИИ',
            'InternalCode' => '350000004',
        ],
    ];

    /**
     * @var BankManager
     */
    private $bankManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        $this->bankManager = new BankManager();
    }

    public function testGetInternalCodeOk()
    {
        foreach (self::BANKS as $bank) {
            $code = $this->bankManager->doRequest('BicToIntCode', [
                'BicCode' => $bank['BIC'],
            ]);

            $this->assertEquals($bank['InternalCode'], $code);
        }
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetInternalCodeFail()
    {
        $bicNull = null;
        $bicNotExist = '12345';

        $codeNull = $this->bankManager->doRequest('BicToIntCode', ['BicCode' => $bicNull]);
        $codeNotExist = $this->bankManager->doRequest('BicToIntCode', ['BicCode' => $bicNotExist]);

        $this->assertEquals(false, $codeNull);
        $this->assertEquals(false, $codeNotExist);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetBankInfoOk()
    {
        foreach (self::BANKS as $bank) {
            $result = $this->bankManager->doRequest('creditInfoByIntCodeXML', [
                'InternalCode' => $bank['InternalCode'],
            ]);

            $this->assertEquals($bank['OrgName'], $result['OrgName']);
        }
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testGetBankInfoOkFail()
    {
        $result = $this->bankManager->doRequest('creditInfoByIntCodeXML', [
            'InternalCode' => '12345',
        ]);

        $this->assertEquals(false, $result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSearchBankOk()
    {
        foreach (self::BANKS as $bank) {
            $result = $this->bankManager->searchBank($bank['BIC']);

            $this->assertEquals($bank['OrgName'], $result['OrgName']);
        }
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testSearchBankFail()
    {
        foreach (self::BANKS as $bank) {
            $result = $this->bankManager->searchBank(12345);

            $this->assertEquals(false, $result);
        }
    }
}