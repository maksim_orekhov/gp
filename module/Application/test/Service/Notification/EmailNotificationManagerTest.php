<?php

namespace ApplicationTest\Service\Notification;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\User;
use Application\Service\Notification\EmailNotificationManager;
use ApplicationTest\Bootstrap;
use Core\Service\ORMDoctrineUtil;
use Doctrine\ORM\EntityManager;
use ReflectionClass;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;


/**
 * Class EmailNotificationManagerTest
 * @package ApplicationTest\Service
 */
class EmailNotificationManagerTest extends AbstractHttpControllerTestCase
{
    const DEAL_INVITATION = 'Сделка с приглашением';

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var EmailNotificationManager
     */
    private $emailNotificationManager;
    /**
     * @var string
     */
    private $user_login;
    /**
     * @var string
     */
    private $user_password;

    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->emailNotificationManager = $serviceManager->get(EmailNotificationManager::class);

        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;
        $this->emailNotificationManager = null;
    }

    /**
     * Тест конструктора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \ReflectionException
     */
    public function testDisputeHistoryManagerConstruct()
    {
        // now call the constructor
        $reflectedClass = new ReflectionClass(EmailNotificationManager::class);
        $constructor = $reflectedClass->getConstructor();
        $parameters = $constructor->getParameters();

        $this->assertEquals('dealInvitationTokenProvider', $parameters[0]->getName());
        $this->assertEquals('dealNotificationSender', $parameters[1]->getName());
        $this->assertEquals('entityManager', $parameters[2]->getName());
        $this->assertEquals('config', $parameters[3]->getName());
    }

    /**
     * @throws \Exception
     */
    public function testInvitationDealAgentsByEmail()
    {
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::DEAL_INVITATION));
        /**
         * @var DealAgent $ownerDealAgent
         * @var DealAgent $counterDealAgent
         */
        $ownerDealAgent = $deal->getOwner();
        $counterDealAgent = $ownerDealAgent === $deal->getCustomer() ? $deal->getContractor() : $deal->getCustomer();
        $oldOwnerInvitationDate = $ownerDealAgent->getInvitationDate();
        $oldCounterInvitationDate = $counterDealAgent->getInvitationDate();

        $this->emailNotificationManager->invitationDealAgentsByEmail($deal);

        $newOwnerInvitationDate = $ownerDealAgent->getInvitationDate();
        $newCounterInvitationDate = $counterDealAgent->getInvitationDate();

        //после отправки у dealAgent обновляется поле invitation_date
        $this->assertTrue($oldOwnerInvitationDate < $newOwnerInvitationDate);
        $this->assertTrue($oldCounterInvitationDate < $newCounterInvitationDate);
    }
}