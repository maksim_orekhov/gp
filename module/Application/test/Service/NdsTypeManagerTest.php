<?php

namespace ApplicationTest\Service;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\NdsType;
use Application\Entity\Payment;
use Application\Fixture\DealFixtureLoader;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\CalculatedDataProvider;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use CoreTest\CallPrivateMethodTrait;

/**
 * Class SettingManagerTest
 * @package ApplicationTest\Service
 */
class NdsTypeManagerTest extends AbstractHttpControllerTestCase
{
    use CallPrivateMethodTrait;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var NdsTypeManager;
     */
    private $ndsTypeManager;

    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider;


    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../config/application.config.php',
                    include __DIR__ . '/../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->ndsTypeManager = $serviceManager->get(NdsTypeManager::class);
        $this->calculatedDataProvider = $serviceManager->get(CalculatedDataProvider::class);

        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group nds
     *
     * @dataProvider dataGetPaymentNdsTypeDeclarationForIncomingPay
     *
     * @throws \Exception
     */
    public function testGetPaymentNdsTypeDeclarationForIncomingPay($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $nds_type_declaration = $this->ndsTypeManager->getPaymentNdsTypeDeclaration($deal);

        $this->assertEquals($expected, $nds_type_declaration);
    }

    /**
     * @return array
     */
    public function dataGetPaymentNdsTypeDeclarationForIncomingPay(): array
    {
        return [
            [null,      NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[2]], // Если не выбран НДС, считаем по типу 2
            ['0',       NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[1]],
            ['без НДС', NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[2]],
            ['10%',     NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[3]],
            ['18%',     NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[4]]
        ];
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group nds
     *
     * @dataProvider dataGetPaymentNdsTypeValueForIncomingPay
     *
     * @throws \Exception
     */
    public function testGetPaymentNdsValueForIncomingPay($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $nds_value = $this->ndsTypeManager->getPaymentNdsTypeValue($deal);

        $this->assertEquals($expected, $nds_value);
    }

    /**
     * @return array
     */
    public function dataGetPaymentNdsTypeValueForIncomingPay(): array
    {
        return [
            [null,      1525.42], // Если не выбран НДС, считаем по типу 2
            ['0',       1525.42],
            ['без НДС', 1525.42],
            ['10%',     24252.69],
            ['18%',     39661.02]
        ];
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group nds
     *
     * @dataProvider dataGetPayoffNdsTypeDeclaration
     *
     * @throws \Exception
     */
    public function testGetPayoffNdsTypeDeclaration($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $fee_amount = $this->calculatedDataProvider->getFeeAmount($payment->getDealValue());

        $nds_type_declaration = $this->ndsTypeManager->getPaymentPayOffNdsTypeDeclaration($deal);

        $expected_declaration = $expected;

        if ($given === null) {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNdsWithoutNds($fee_amount);
        }
        if ($given === '0') {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNds0($fee_amount);
        }
        if ($given === 'без НДС') {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNdsWithoutNds($fee_amount);
        }
        if ($given === '10%') {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNds10($payment->getReleasedValue(), $fee_amount);
        }
        if ($given === '18%') {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNds18($payment->getExpectedValue());
        }

        $this->assertInternalType('string', $nds_type_declaration);
        $this->assertEquals($expected_declaration, $nds_type_declaration);
    }

    /**
     * @return array
     */
    public function dataGetPayoffNdsTypeDeclaration(): array
    {
        return [
            [null,      NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[2]],
            ['0',       NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[1]],
            ['без НДС', NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[2]],
            ['10%',     NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[3]],
            ['18%',     NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY[4]]
        ];
    }

    /**
     * @return array
     */
    public function dataGetPayoffNdsTypeDeclarationToCustomer(): array
    {
        return [
            [null,      NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY[0]],
            ['0',       NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY[1]],
            ['без НДС', NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY[2]],
            ['10%',     NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY[3]],
            ['18%',     NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY[4]]
        ];
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group nds
     *
     * @dataProvider dataGetPayoffNdsTypeDeclarationToCustomer
     */
    public function testGetRefundNdsTypeDeclaration($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $nds_type_declaration = $this->ndsTypeManager->getRefundNdsTypeDeclaration($deal, $payment->getReleasedValue());

        $expected_declaration = $expected;

        if ($given === '10%') {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNds10ForOutgoingPay($payment->getReleasedValue());
        }
        if ($given === '18%') {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNds18($payment->getReleasedValue());
        }

        $this->assertInternalType('string', $nds_type_declaration);
        $this->assertEquals($expected_declaration, $nds_type_declaration);

        $this->assertInternalType('string', $nds_type_declaration);
        $this->assertEquals($expected_declaration, $nds_type_declaration);
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group nds
     *
     * @dataProvider dataGetPayoffNdsTypeDeclarationToCustomer
     */
    public function testGetDiscountNdsTypeDeclaration($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $discount_amount = '15000';

        $nds_type_declaration = $this->ndsTypeManager->getDiscountNdsTypeDeclaration($deal, $discount_amount);

        $expected_declaration = $expected;

        if ($given === '10%') {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNds10ForOutgoingPay($discount_amount);
        }
        if ($given === '18%') {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNds18($discount_amount);
        }

        $this->assertInternalType('string', $nds_type_declaration);
        $this->assertEquals($expected_declaration, $nds_type_declaration);

        $this->assertInternalType('string', $nds_type_declaration);
        $this->assertEquals($expected_declaration, $nds_type_declaration);
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group nds
     *
     * @dataProvider dataGetPayoffNdsTypeDeclarationToCustomer
     */
    public function testGetOverpayNdsTypeDeclaration($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $overpay_amount = '555';

        $nds_type_declaration = $this->ndsTypeManager->getOverpayNdsTypeDeclaration($deal, $overpay_amount);

        $expected_declaration = $expected;

        if ($given === '10%') {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNds10ForOutgoingPay($overpay_amount);
        }
        if ($given === '18%') {
            $expected_declaration .= ' ' . $this->ndsTypeManager->countNds18($overpay_amount);
        }

        $this->assertInternalType('string', $nds_type_declaration);
        $this->assertEquals($expected_declaration, $nds_type_declaration);

        $this->assertInternalType('string', $nds_type_declaration);
        $this->assertEquals($expected_declaration, $nds_type_declaration);
    }

    /**
     * @param $deal_expected_value
     * @param $expected_nds_value
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @dataProvider dataForCountNds18
     *
     * @group deal
     * @group nds
     */
    public function testCountNds18($deal_expected_value, $expected_nds_value)
    {
        $nds_value = $this->ndsTypeManager->countNds18($deal_expected_value);

        $this->assertEquals($expected_nds_value, $nds_value);
    }

    /**
     * @return array
     *
     * Данные подготовлены с помощью сервиса https://www.calc.ru/kalkulyator-nds.html
     */
    public function dataForCountNds18(): array
    {
        return [
            [118000, 18000],
            [100000, 15254.24],
            [507448, 77407.32]
        ];
    }


    /**
     * @param $fee_amount
     * @param $expected_nds_value
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @dataProvider dataCountNdsWithoutNds
     *
     * @group deal
     * @group nds
     */
    public function testCountNdsWithoutNds($fee_amount, $expected_nds_value)
    {
        $nds_value = $this->ndsTypeManager->countNdsWithoutNds($fee_amount);

        $this->assertEquals($expected_nds_value, $nds_value);
    }

    /**
     * @return array
     *
     * Данные подготовлены с помощью сервиса https://www.calc.ru/kalkulyator-nds.html
     */
    public function dataCountNdsWithoutNds(): array
    {
        return [
            [2000, 305.08],
            [40000, 6101.69],
            [55000.50, 8389.91],
        ];
    }


    /**
     * @param $deal_expected_value
     * @param $expected_nds_value
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @dataProvider dataCountNds10
     *
     * @group deal
     * @group nds
     */
    public function testCountNds10($deal_expected_value, $expected_nds_value)
    {
        $fee_amount = $this->calculatedDataProvider->getFeeAmount($deal_expected_value);

        $released_value = $deal_expected_value - $fee_amount;

        $fee_amount_nds_18 = ($fee_amount/1.18) * 0.18;
        $released_value_nds_10 = ($released_value/1.10) * 0.10;

        $total_nds_amount = CalculatedDataProvider::roundAmount($fee_amount_nds_18 + $released_value_nds_10);

        $nds_value = $this->ndsTypeManager->countNds10($released_value, $fee_amount);

        $this->assertEquals($total_nds_amount, $nds_value);
    }

    /**
     * @return array
     *
     * Второй параметр в массиве не используется
     */
    public function dataCountNds10(): array
    {
        return [
            [100000, null],
            [546, null],
            [58956734, null],
        ];
    }


    /**
     * @param $given
     * @param $expected
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @dataProvider dataCountNds10ForOutgoingPay
     *
     * @group deal
     * @group nds
     */
    public function testCountNds10ForOutgoingPay($given, $expected)
    {
        $nds_value = $this->ndsTypeManager->countNds10ForOutgoingPay($given);

        $this->assertEquals($expected, $nds_value);
    }

    /**
     * @return array
     *
     * Данные подготовлены с помощью сервиса https://www.calc.ru/kalkulyator-nds.html
     */
    public function dataCountNds10ForOutgoingPay(): array
    {
        return [
            [100000, 9090.91],
            [546, 49.64],
            [58956734, 5359703.09],
        ];
    }


    /**
     * @param $fee_amount
     * @param $expected_nds_value
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @dataProvider dataCountNds0
     *
     * @group deal
     * @group nds
     */
    public function testCountNds0($fee_amount, $expected_nds_value)
    {
        $nds_value = $this->ndsTypeManager->countNds0($fee_amount);

        $this->assertEquals($expected_nds_value, $nds_value);
    }

    /**
     * @return array
     *
     * Данные подготовлены с помощью сервиса https://www.calc.ru/kalkulyator-nds.html
     */
    public function dataCountNds0(): array
    {
        return [
            [28500, 4347.46],
            [5043, 769.27],
            [100002, 15254.54],
        ];
    }


    /**
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group nds
     *
     * @dataProvider providerForTestGetOutgoingPayNdsAmount
     */
    public function testGetOutgoingPayNdsAmount(array $given, $expected)
    {
        $nds_value = $this->invokeMethod($this->ndsTypeManager, 'getOutgoingPayNdsAmount', $given);

        $this->assertEquals($expected, $nds_value);
    }

    /**
     * @return array
     */
    public function providerForTestGetOutgoingPayNdsAmount(): array
    {
        return [
            [[1, 100000], null],
            [[2, 100000], null],
            [[3, 150000], 13636.36],
            [[4, 150000], 22881.36],
        ];
    }


    /**
     * @param CivilLawSubject $civilLawSubject
     * @param NdsType|null $ndsType
     * @return CivilLawSubject
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function setNdsType(CivilLawSubject $civilLawSubject, $ndsType): CivilLawSubject
    {
        $civilLawSubject->setNdsType($ndsType);

        $this->entityManager->persist($civilLawSubject);
        $this->entityManager->flush();

        return $civilLawSubject;
    }
}