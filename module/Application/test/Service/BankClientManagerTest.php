<?php
namespace ApplicationTest\Service;

use Core\Service\Base\BaseRoleManager;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Zend\Test\PHPUnit\Controller\AbstractControllerTestCase;
use Application\Service\BankClient\BankClientManager;
use Application\Listener\CreditPaymentOrderAvailabilityListener;
use Core\Service\Base\BaseAuthManager;
use Zend\Session\SessionManager;
use Application\Service\UserManager;
use Application\Entity\Role;
use Application\Entity\Deal;
use Application\Entity\User;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\SystemPaymentDetails;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealAgentManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\SettingManager;
use CoreTest\CallPrivateMethodTrait;

/**
 * Class BankClientManagerTest
 * @package ApplicationTest\Service
 */
class BankClientManagerTest extends AbstractControllerTestCase
{
    use CallPrivateMethodTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var BankClientManager
     */
    private $bankClientManager;

    /**
     * @var CreditPaymentOrderAvailabilityListener
     */
    private $creditPaymentOrderAvailabilityListener;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;


    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $this->setApplicationConfig(include __DIR__ . '/../../../../config/application.config.php');

        parent::setUp();

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->bankClientManager = $serviceManager->get(BankClientManager::class);
        $this->creditPaymentOrderAvailabilityListener = $serviceManager->get(CreditPaymentOrderAvailabilityListener::class);

        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->dealAgentManager = $serviceManager->get(DealAgentManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->paymentManager = $serviceManager->get(PaymentManager::class);
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);
        $this->settingManager = $serviceManager->get(SettingManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        $this->entityManager->getConnection()->rollback();
        $this->entityManager->getConnection()->close();

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Создание объекта платёжного поручения (кредит платёжка) bankClientPaymentOrder из данных сделки.
     * Требует наличия в системе объектов deal, payment, dealAgent, paymentMethod и paymentMethodBankTransfer.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testCreateNewPaymentOrder()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $deal_name = 'Тестовая сделка';
        $deal_role = 'customer';
        $amount = 300000;
        $confirmed = true;
        // Crete Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount, $confirmed);

        /** @var Deal $testDeal */
        $testDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Тестовая сделка']);

        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($testDeal);
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $payment->getPaymentMethod();

        $this->creditPaymentOrderAvailabilityListener->subscribe();
        $this->creditPaymentOrderAvailabilityListener->trigger($testDeal);

        /** @var BankClientPaymentOrder $paymentOrder */
        $paymentOrder = $this->entityManager->getRepository(BankClientPaymentOrder::class)
            ->findOneBy(['paymentPurpose' => 'Выплата по сделке #'.$testDeal->getId()]);

        /** @var SystemPaymentDetails $systemPaymentDetail */
        $systemPaymentDetail = $this->settingManager->getSystemPaymentDetail();

        $this->assertEquals(true, ($paymentOrder instanceof BankClientPaymentOrder));
        $this->assertEquals($paymentOrder->getDocumentType(), "Платежное поручение");
        $this->assertEquals($paymentOrder->getDocumentNumber(), $testDeal->getId());
        $this->assertEquals($paymentOrder->getAmount(), $payment->getReleasedValue());
        $this->assertEquals($paymentOrder->getRecipientAccount(), $paymentMethod->getPaymentMethodBankTransfer()->getAccountNumber());
        #$this->assertEquals($paymentOrder->getRecipientInn(), $paymentMethod->getPaymentMethodBankTransfer()->getInn());
        $this->assertEquals($paymentOrder->getRecipient1(), 'Александр Александров');
        $this->assertEquals($paymentOrder->getPayerAccount(), $systemPaymentDetail->getAccountNumber());
        $this->assertEquals($paymentOrder->getPayer(), "ИНН " . $systemPaymentDetail->getInn() . " " . $systemPaymentDetail->getName());
    }

    // Variants of payment purposes for tests
    public function providerPaymentPurposesForSupposedDealId(): array
    {
        return [
            ['Перевод с карты *1996, Оплата по сделке #gp15Т согласно договора оферты №1 от 03.04.2018. В т.ч НДС 18%.НДС включен', 15],
            ['Оплата по сделке GP22Т согласно договора оферты №1 от 03.04.2018. В т.ч НДС 18%.НДС включен', 22],
            ['Возврат по сделке #GP 28Т от 03.04.2018. В т.ч НДС 18%.НДС включен', 28],
            ['Возврат по сделке №GP 29u от 25.04.2018', 29],
            ['Возврат по сделке GP127U GP128U от 25.04.2018', 127],
            ['Возврат по сделке gp105t от 03.04.2018', 105],
            ['Скидка по сделке GP125u 03.04.2018', 125],
            ['GP-32Т от 03.04.2018. В т.ч НДС 18%.НДС включен', 32],
            ['GP36', 36],
            ['... GP#38...', 38],
            ['GP  апцлпне-пии%  42', 42],
            ['GP T', null],
            ['GР 190', null], // Р - русская
            ['Оплата по сделке 220 согласно договора оферты №1 от 03.04.2018. В т.ч НДС 18%.НДС включен', null],
            ['GP U 18%.НДС включен', 18], // некоорентый возврат
            ['GPТ согласно договора оферты от 23.04.2018', 23], // некоорентый возврат
        ];
    }

    /**
     * Проверка метода предварительной детекции и получения id сделки из назначения платежа
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     *
     * @dataProvider providerPaymentPurposesForSupposedDealId
     */
    public function testGetSupposedDealIdFromPaymentPurpose($given, $expected)
    {
        $deal_id = $this->invokeMethod($this->bankClientManager, 'getSupposedDealIdFromPaymentPurpose', array($given));

        $this->assertEquals($expected, $deal_id);
    }

    // Variants of payment purposes for tests
    public function providerPaymentPurposesForReliableDealId(): array
    {
        return [
            ['Оплата по сделке #gp15Т согласно договора оферты №1 от 03.04.2018. В т.ч НДС 18%.НДС включен', 15],
            ['Оплата по сделке GP 22Т', 22],
            ['Возврат по сделке #GP 28Т от 03.04.2018. В т.ч НДС 18%.НДС включен', 28],
            ['... №GP 29u от 25.04.2018', 29],
            ['... GP127U GP128U от 25.04.2018', 127],
            ['... gp105t от 03.04.2018', 105],
            ['Скидка по сделке GP125u 03.04.2018', 125],
            ['GP-32Т от 03.04.2018. В т.ч НДС 18%.НДС включен', 32],
            ['GP36', 36],
            ['... GP#38...', 38],
            ['GP--1056', null],
            ['GP**1200', null],
            ['GP  1250', null], // два пробела
            ['GPапцлпне-пии 42', null],
            ['GP ап44', null],
            ['GP T', null],
            ['GР 190', null], // Р - русская
            ['Оплата по сделке 220 согласно договора оферты №1 от 03.04.2018. В т.ч НДС 18%.НДС включен', null],
            ['GP U 18%.НДС включен', null],
            ['GPТ согласно договора оферты от 23.04.2018', null]
        ];
    }

    /**
     * Проверка метода надежной детекции и получения id сделки из назначения платежа
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     *
     * @dataProvider providerPaymentPurposesForReliableDealId
     */
    public function testGetReliableDealIdFromPaymentPurpose($given, $expected)
    {
        $deal_id = $this->invokeMethod($this->bankClientManager, 'getReliableDealIdFromPaymentPurpose', array($given));

        $this->assertEquals($expected, $deal_id);
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var \Application\Entity\Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя test
     */
    private function login()
    {
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);
    }

    /**
     * @param string $deal_name
     * @param string $deal_role
     * @param $amount
     * @param bool $confirmed
     * @return Deal
     */
    private function createTestDeal(string $deal_name, string $deal_role, $amount, $confirmed = false)
    {
        // Create CivilLawSubject
        $params = [
            'first_name' => 'Александр',
            'secondary_name' => 'Александрович',
            'last_name' => 'Александров',
            'birth_date' => date('Y-m-d'),
        ];
        $this->civilLawSubjectManager->createNaturalPerson($this->user, $params);

        // Create paymentMethod и paymentMethodBankTransfer
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user);
        $params = [
            'name' => 'Счет ВТБ',
            'bank_name' => 'ВТБ',
            'bik' => '111222',
            'account_number' => '22233344455566677788',
            'payment_recipient_name' => 'Александр Александров',
            'inn' => '666777',
            'kpp' => '777888',
            'corr_account_number' => '30101810900000000790'
        ];
        $bankTransfer = $this->paymentMethodManager->createPaymentMethodBankTransfer($params, $civilLawSubject);
        $paymentMethod = $bankTransfer->getPaymentMethod();

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $data = [
            'name'                      => $deal_name,
            'amount'                    => $amount,
            'deal_role'                 => $deal_role,
            'deal_civil_law_subject'    => $this->user->getCivilLawSubjects()->last()->getId(),
            'payment_method'            => '',
            'counteragent_email'        => 'test2@simple-technology.ru 	',
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => '5',
            'deal_agent_confirm'        => 1
        ];

        // Try to register dill
        // Распределение ролей в сделке взависимости от выбранной инициатором роли
        if($data['deal_role'] == 'customer') {
            $userCustomer   = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $userContractor = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $paramCustomer = $data;
            $paramContractor = [];
        } else {
            $userCustomer   = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $userContractor = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $paramCustomer = [];
            $paramContractor = $data;
        }

        $dealAgentCustomer = $this->dealAgentManager->createDealAgent($userCustomer, $paramCustomer);
        $dealAgentContractor = $this->dealAgentManager->createDealAgent($userContractor, $paramContractor);

        $user = $this->userManager->selectUserByLogin($this->authService->getIdentity());

        $dealOwner = $this->dealAgentManager->defineDealOwner($user, $dealAgentCustomer, $dealAgentContractor);

        // Create Deal
        $deal = $this->dealManager->createDeal($data, $dealAgentCustomer, $dealAgentContractor, $dealOwner);

        if ($confirmed) {
            // Set Customer confirm
            $deal->getCustomer()->setDealAgentConfirm(true);
            $deal->getContractor()->setDealAgentConfirm(true);
        }

        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($deal);
        $payment->setPaymentMethod($paymentMethod);

        $this->entityManager->flush();

        return $deal;
    }
}