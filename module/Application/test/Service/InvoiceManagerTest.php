<?php

namespace ApplicationTest\Service;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\NdsType;
use Application\Fixture\DealFixtureLoader;
use Application\Service\BankClient\BankClientManager;
use Application\Service\InvoiceManager;
use Application\Service\NdsTypeManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use CoreTest\CallPrivateMethodTrait;

/**
 * Class SettingManagerTest
 * @package ApplicationTest\Service
 */
class InvoiceManagerTest extends AbstractHttpControllerTestCase
{
    use CallPrivateMethodTrait;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var NdsTypeManager;
     */
    private $ndsTypeManager;

    /**
     * @var InvoiceManager;
     */
    private $invoiceManager;


    public function setUp()
    {
        $this->setApplicationConfig(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../config/application.config.php',
                    include __DIR__ . '/../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../config/autoload/local.php'
            )
        );

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->ndsTypeManager = $serviceManager->get(NdsTypeManager::class);
        $this->invoiceManager = $serviceManager->get(InvoiceManager::class);

        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group nds
     *
     * @dataProvider dataGenerateInvoicePaymentPurpose
     *
     * @throws \Exception
     */
    public function testGenerateInvoicePaymentPurpose($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $invoice_payment_purpose = $this->invoiceManager->generateInvoicePaymentPurpose($deal);

        $created = $deal->getCreated()->format('d.m.Y');

        $this->assertEquals($expected['pre'] . $deal->getNumber() . ' от ' . $created . $expected['post'], $invoice_payment_purpose);
    }

    /**
     * @return array
     */
    public function dataGenerateInvoicePaymentPurpose(): array
    {
        return [
            [null, [ // Если не выбран НДС, считаем по типу 2
                'pre' => BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY,
                'post' => InvoiceManager::ACCORDING_TO_OFFER_CONTRACT.'. ' . NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY['2']
            ]],
            ['0', [
                'pre' => BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY,
                'post' => InvoiceManager::ACCORDING_TO_OFFER_CONTRACT.'. ' . NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY['1']
            ]],
            ['без НДС', [
                'pre' => BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY,
                'post' => InvoiceManager::ACCORDING_TO_OFFER_CONTRACT.'. ' . NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY['2']
            ]],
            ['10%', [
                'pre' => BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY,
                'post' => InvoiceManager::ACCORDING_TO_OFFER_CONTRACT.'. ' . NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY['3']
            ]],
            ['18%', [
                'pre' => BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY,
                'post' => InvoiceManager::ACCORDING_TO_OFFER_CONTRACT.'. ' . NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_INCOMING_PAY['4']
            ]],
        ];
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group nds
     *
     * @dataProvider dataGenerateInvoicePaymentPurpose
     *
     * @throws \Exception
     */
    public function testGenerateInvoicePaymentPurposeWithNdsValue($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $invoice_payment_purpose = $this->invoiceManager->generateInvoicePaymentPurpose($deal, true);

        $nds_value = $this->invoiceManager->getNdsValue($deal);

        $created = $deal->getCreated()->format('d.m.Y');

        $this->assertEquals(
            $expected['pre'] . $deal->getNumber() . ' от ' . $created . $expected['post'] . ' - ' . $nds_value . '.',
            $invoice_payment_purpose
        );
    }


    /**
     * @param CivilLawSubject $civilLawSubject
     * @param NdsType|null $ndsType
     * @return CivilLawSubject
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function setNdsType(CivilLawSubject $civilLawSubject, $ndsType): CivilLawSubject
    {
        $civilLawSubject->setNdsType($ndsType);

        $this->entityManager->persist($civilLawSubject);
        $this->entityManager->flush();

        return $civilLawSubject;
    }
}