<?php
namespace ApplicationTest\Service\Parser;

use PHPUnit\Framework\TestCase;
use Application\Service\Parser\ParserBank;

/**
 * Class ParserBankTest
 * @package ApplicationTest\Service\Parser
 */
class ParserBankTest extends TestCase
{
    /**
     * @var ParserBank
     */
    public $parserBank;
    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        $this->parserBank = new ParserBank();
    }

    /**
     * парсинг файла
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testExecute()
    {
        // @ TODO Заменить массив на соответсвующий файлу payment_order_from_fixture.txt
        $result_data = array(
            0 => array(
                'doctype' => array(
                    'NAME'=>'СекцияДокумент',
                    'VALUE'=>'Платежное поручение'
                ),
                'inbankid' => array(
                    'NAME'=>'Номер',
                    'VALUE'=>'124'
                ),
                'docdate' => array(
                    'NAME'=>'Дата',
                    'VALUE'=>'11.05.2017'
                ),
                'outdate' => array(
                    'NAME'=>'ДатаСписано',
                    'VALUE'=>'12.05.2017'
                ),
                'summ' => array(
                    'NAME'=>'Сумма',
                    'VALUE'=>'274320'
                ),
                'payeraccount' => array(
                    'NAME'=>'ПлательщикСчет',
                    'VALUE'=>'40702810090030001128'
                ),
                'payerinfo' => array(
                    'NAME'=>'Плательщик',
                    'VALUE'=>'ИНН 7810465141 Тест Тестов'
                ),
                'payerinn' => array(
                    'NAME'=>'ПлательщикИНН',
                    'VALUE'=>'7810465141'
                ),
                'payer' => array(
                    'NAME'=>'Плательщик1',
                    'VALUE'=>'Тест Тестов'
                ),
                'payerkpp' => array(
                    'NAME'=>'ПлательщикКПП',
                    'VALUE'=>'781001001'
                ),
                'payerbank1' => array(
                    'NAME'=>'ПлательщикБанк1',
                    'VALUE'=>'ВТБ'
                ),
                'payerbik' => array(
                    'NAME'=>'ПлательщикБИК',
                    'VALUE'=>'044525201'
                ),
                'payerfixaccount' => array(
                    'NAME'=>'ПлательщикКорсчет',
                    'VALUE'=>'30101810900000000790'
                ),
                'recieveraccount' => array(
                    'NAME'=>'ПолучательСчет',
                    'VALUE'=>'12345678912345678912'
                ),
                'recieverinfo' => array(
                    'NAME'=>'Получатель',
                    'VALUE'=>'ИНН 123456789123 guarant-pay'
                ),
                'recieverinn' => array(
                    'NAME'=>'ПолучательИНН',
                    'VALUE'=>'123456789123'
                ),
                'reciever1' => array(
                    'NAME'=>'Получатель1',
                    'VALUE'=>'guarant-pay'
                ),
                'recieverkpp' => array(
                    'NAME'=>'ПолучательКПП',
                    'VALUE'=>'123456789'
                ),
                'recieverbank1' => array(
                    'NAME'=>'ПолучательБанк1',
                    'VALUE'=>'ПАО БАНК САНКТ-ПЕТЕРБУРГ'
                ),
                'recieverbik' => array(
                    'NAME'=>'ПолучательБИК',
                    'VALUE'=>'123456789'
                ),
                'recieverfixaccount' => array(
                    'NAME'=>'ПолучательКорсчет',
                    'VALUE'=>'30101810300000000811'
                ),
                'paydirection' => array(
                    'NAME'=>'НазначениеПлатежа',
                    'VALUE'=>'Оплата по сделке #151'
                ),
                'paytype' => array(
                    'NAME'=>'ВидОплаты',
                    'VALUE'=>'01'
                ),
                'quenue' => array(
                    'NAME'=>'Очередность',
                    'VALUE'=>'5'
                ),
            )
        );
        $file_path = "./module/Application/test/files/test_bank_file_with_one_order.txt";

        $this->assertFileExists($file_path);

        $file_content = file_get_contents($file_path);

        $result = $this->parserBank->execute($file_content);

        $this->assertEquals($result_data, $result);
    }
}