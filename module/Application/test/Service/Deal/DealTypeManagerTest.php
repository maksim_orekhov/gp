<?php

namespace ApplicationTest\Service\Deal;

use Application\Entity\DealType;
use Application\Service\Deal\DealTypeManager;
use ApplicationTest\Bootstrap;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;

/**
 * Class DealTypeManagerTest
 * @package ApplicationTest\Service\Deal
 */
class DealTypeManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var DealTypeManager
     */
    private $dealTypeManager;


    public function setUp()
    {
        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();

        // Add content of global.php and other to ApplicationConfig
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->dealTypeManager = $serviceManager->get(DealTypeManager::class);
    }

    public function tearDown()
    {
        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @throws \Core\Exception\LogicException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-type
     */
    public function testGetDealTypeById()
    {
        $dealTypes = $this->entityManager->getRepository(DealType::class)->findAll();

        /** @var DealType $dealType */
        foreach ($dealTypes as $dealType) {

            $selectedDealType = $this->dealTypeManager->getDealTypeById($dealType->getId());

            $this->assertSame($dealType, $selectedDealType);
        }
    }

    /**
     * @throws \Core\Exception\LogicException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-type
     *
     * @expectedException \Core\Exception\LogicException
     * @expectedExceptionMessage Deal type not found
     */
    public function testGetDealTypeByNonexistentId()
    {
        // Находим последний элемент
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array(), array('id' => 'DESC'));

        // Пытаемся получить DealType по несуществующему ID - выбросится LogicException
        $selectedDealType = $this->dealTypeManager->getDealTypeById($dealType->getId()+1);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-type
     */
    public function testGetDealTypes()
    {
        // Массив объектов
        $dealTypes = $this->dealTypeManager->getDealTypes();

        $this->assertInternalType('array', $dealTypes);
        $this->assertGreaterThan(1, count($dealTypes));
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-type
     */
    public function testGetDealTypesAsArray()
    {
        // Массив объектов
        $dealTypes = $this->dealTypeManager->getDealTypes();
        // Массив массивов
        $deal_types = $this->dealTypeManager->getDealTypesAsArray();

        $this->assertInternalType('array', $deal_types);
        $this->assertGreaterThan(1, \count($deal_types));

        /** @var DealType $dealType */
        foreach ($dealTypes as $dealType) {
            $this->assertEquals($dealType->getId(), $deal_types[$dealType->getId()]['id']);
            $this->assertEquals($dealType->getIdent(), $deal_types[$dealType->getId()]['ident']);
            $this->assertEquals($dealType->getName(), $deal_types[$dealType->getId()]['name']);
        }
    }
}