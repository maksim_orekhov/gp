<?php

namespace ApplicationTest\Service\Deal;

use Application\Entity\Deal;
use Application\Entity\Payment;
use ApplicationTest\Controller\DealControllerTest;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealPolitics;
use Application\Service\Payment\PaymentManager;
use Zend\Stdlib\ArrayUtils;
use Application\Controller\DealController;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\SystemPaymentDetails;
use Application\Service\BankClient\BankClientManager;
use Application\Service\Parser\BankClientParser;
use Application\Controller\BankClientController;

class DealPoliticsTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * @var BankClientManager
     */
    public $bankClientManager;

    /**
     * @var BankClientParser
     */
    private $bankClientParser;

    /**
     * @var string
     */
    public $main_upload_folder;


    public function setUp()
    {
        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            include __DIR__ . '/../../../../../config/autoload/global.php'
        ));

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->paymentManager = $serviceManager->get(PaymentManager::class);
        $this->dealPolitics = $serviceManager->get(DealPolitics::class);
        $this->bankClientManager = $serviceManager->get(\Application\Service\BankClient\BankClientManager::class);
        $this->bankClientParser = $serviceManager->get(BankClientParser::class);

        $config = $this->getApplicationConfig();
        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }


    /**
     * Exception если у сделки нет DateOfConfirm
     *
     * @expectedException \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testIsDealAbnormalExceptionIfNoDateOfConfirm()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Фикстура']);
        // Проверка Exception, если у сделки нет даты подтверждения
        $this->dealPolitics->isDealAbnormal($deal);
    }

    /**
     * Меняем даты и сумму сделки и проверяем IsDealAbnormal
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testIsDealAbnormal()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Фикстура']);

        $period = DealControllerTest::MINIMUM_DELIVERY_PERIOD + 2;
        // Set agents confirm
        $deal->getCustomer()->setDealAgentConfirm(true);
        $currentDate = new \DateTime();
        $deal->getCustomer()->setDateOfConfirm($currentDate->modify('-' . ($period + 1) . ' days'));
        $deal->getContractor()->setDealAgentConfirm(true);
        $currentDate = new \DateTime();
        $deal->getContractor()->setDateOfConfirm($currentDate->modify('-' . $period . ' days'));

        $this->entityManager->flush();

        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($deal);

        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($deal);

        $file_name = 'payment_order_for_debit.txt';
        // Path
        $path = "./module/Application/test/files/".$file_name;

        // Создаем файл платёжки и по нему PaymentOrder
        $this->createPaymentOrderFromFile($deal, $payment, $path);

        // Меняем DateOfConfirm
        $currentDate = new \DateTime();
        $deal->setDateOfConfirm($currentDate->modify('-' . 22 . ' days'));
        /** @var BankClientPaymentOrder $debitPaymentOrder */
        $debitPaymentOrder = $payment->getDebitPaymentOrder();
        // Сеттим $dateOfDebit (string) заведомо больше, чем DateOfConfirm + DeliveryPeriod
        $currentDate = new \DateTime();
        $dateOfDebit = $currentDate->modify('-' . 2 . ' days');
        $debitPaymentOrder->setDateOfDebit($dateOfDebit->format('d.m.Y'));

        $this->entityManager->flush();

        // Проверка BankClientPaymentOrder.dateOfDebit > (Deal.dateOfConfirm + Deal.deliveryPeriod)
        $this->assertTrue($this->dealPolitics->isDealAbnormal($deal));

        // Сеттим $dateOfDebit (string) заведомо меньше, чем DateOfConfirm + DeliveryPeriod
        $currentDate = new \DateTime();
        $dateOfDebit = $currentDate->modify('-' . 25 . ' days');
        $debitPaymentOrder->setDateOfDebit($dateOfDebit->format('d.m.Y'));

        // Теперь BankClientPaymentOrder.dateOfDebit > (Deal.dateOfConfirm + Deal.deliveryPeriod) = dalse
        $this->assertFalse($dateOfDebit > $deal->getDateOfConfirm()->modify('+'.$deal->getDeliveryPeriod().' days'));

        // Проверяем, что в BankClientPaymentOrder поступившая сумма СООТВЕТСВУЕТ сумме expectedValue в Payment
        $this->assertTrue($payment->getDebitPaymentOrder()->getAmount() == $payment->getExpectedValue());
        // Проверка не должна выявить проблем в тестовой Сделке
        $this->assertFalse($this->dealPolitics->isDealAbnormal($deal));

        // Меняем сумму сделки в DebitPaymentOrder
        $debitPaymentOrder->setAmount($payment->getExpectedValue() + 1);
        // Проверка выявляет проблем в тестовой Сделке
        $this->assertTrue($this->dealPolitics->isDealAbnormal($deal));

        // Удаляем результаты работы
        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.DealController::FILE_STORING_FOLDER;
        // Удаляем сгенерированный файл
        $this->removeFile($upload_folder."/".$deal->getDealContractFile()->getFIle()->getName());
    }

    /**
     * @param Deal $deal
     * @param Payment $payment
     * @param string $path
     */
    private function createPaymentOrderFromFile(Deal $deal, Payment $payment, string $path)
    {
        /** @var PaymentMethodBankTransfer $customerBankTransfer */ // Реквизиты customer'а
        $customerBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('paymentRecipientName' => 'Тест Тестов'));
        /** @var SystemPaymentDetails $systemPaymentDetails */ // Реквизиты GP
        $systemPaymentDetails = $this->entityManager->getRepository(SystemPaymentDetails::class)
            ->findOneBy(array('name' => 'Guarant Pay'));

        // Creates file if not exists
        $file_with_payment_order = fopen($path, "w") or die("Не удается создать файл");
        // Write file header data
        fwrite($file_with_payment_order, mb_convert_encoding(
                BankClientManager::FILE_HEADER,
                BankClientController::ORIGINAL_ENCODING, 'UTF-8')
        );
        // Main body of PaymentOrder
        $currentDate = new \DateTime();
        $txt = "СекцияДокумент=".BankClientManager::DOCUMENT_TYPE.PHP_EOL;
        $txt .= "Номер=".$deal->getId().PHP_EOL;
        $txt .= "Дата=".$currentDate->format('d.m.Y').PHP_EOL;
        $txt .= "ДатаСписано=".$currentDate->format('d.m.Y').PHP_EOL;
        $txt .= "Сумма=".$payment->getExpectedValue().PHP_EOL;
        $txt .= "ПлательщикСчет=".$customerBankTransfer->getAccountNumber().PHP_EOL;
        $txt .= "Плательщик=ИНН ".$customerBankTransfer->getInn()." ".$customerBankTransfer->getPaymentRecipientName().PHP_EOL;
        $txt .= "ПлательщикИНН=".$customerBankTransfer->getInn().PHP_EOL;
        $txt .= "Плательщик1=".$customerBankTransfer->getPaymentRecipientName().PHP_EOL;
        $txt .= "ПлательщикКПП=".$customerBankTransfer->getKpp().PHP_EOL;
        $txt .= "ПлательщикБанк1=".$customerBankTransfer->getBankName().PHP_EOL;
        $txt .= "ПлательщикБИК=".$customerBankTransfer->getBik().PHP_EOL;
        $txt .= "ПлательщикКорсчет=".$customerBankTransfer->getCorrAccountNumber().PHP_EOL;
        $txt .= "ПолучательСчет=".$systemPaymentDetails->getAccountNumber().PHP_EOL;
        $txt .= "Получатель=ИНН ".$systemPaymentDetails->getInn()." ".$systemPaymentDetails->getPaymentRecipientName().PHP_EOL;
        $txt .= "ПолучательИНН=".$systemPaymentDetails->getInn().PHP_EOL;
        $txt .= "Получатель1=".$systemPaymentDetails->getPaymentRecipientName().PHP_EOL;
        $txt .= "ПолучательКПП=".$systemPaymentDetails->getKpp().PHP_EOL;
        $txt .= "ПолучательБанк1=".$systemPaymentDetails->getBank().PHP_EOL;
        $txt .= "ПолучательБИК=".$systemPaymentDetails->getBik().PHP_EOL;
        $txt .= "ПолучательКорсчет=".$systemPaymentDetails->getCorrAccountNumber().PHP_EOL;
        $txt .= "НазначениеПлатежа=".BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY.$deal->getId().PHP_EOL;
        $txt .= "ВидОплаты=01".PHP_EOL;
        $txt .= "Очередность=5".PHP_EOL;
        // Write main body
        fwrite($file_with_payment_order, mb_convert_encoding($txt, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
        // Write SECTION_END_POINT
        fwrite($file_with_payment_order, mb_convert_encoding(BankClientParser::SECTION_END_POINT.PHP_EOL,
            BankClientController::ORIGINAL_ENCODING, 'UTF-8'));

        // Создаем PaymentOrder с правильным параметром НазначениеПлатежа
        // Get data as array
        $payment_order_data = $this->bankClientParser->execute($path);
        // Create PaymentOrder
        $this->bankClientManager->createNewPaymentOrderFromArray($payment_order_data[0]);
    }

    /**
     * @param $path
     */
    private function removeFile($path)
    {
        unlink($path);
    }
}