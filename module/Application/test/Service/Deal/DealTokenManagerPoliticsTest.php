<?php

namespace ApplicationTest\Service\Deal;

use Application\Entity\CivilLawSubject;
use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Deal\DealTokenManagerPolitics;
use ApplicationTest\Bootstrap;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;

/**
 * Class DealTokenManagerPoliticsTest
 * @package ApplicationTest\Service\Deal
 */
class DealTokenManagerPoliticsTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var DealTokenManagerPolitics
     */
    private $dealTokenManagerPolitics;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    public function setUp()
    {
        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();

        // Add content of global.php and other to ApplicationConfig
        $this->setApplicationConfig($config);

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->dealTokenManagerPolitics = $serviceManager->get(DealTokenManagerPolitics::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @param array $given
     * @param bool $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-token
     *
     * @dataProvider dataIsCounteragDataCorrectent
     */
    public function testIsCounteragDataCorrectent(array $given, bool $expected)
    {
        $result = $this->dealTokenManagerPolitics->isCounteragentDataSufficient($given);

        $this->assertEquals($expected, $result);
    }

    /**
     * @return array
     */
    public function dataIsCounteragDataCorrectent(): array
    {
        return [
            [['counteragent_email' => 'test2@simple-technology.ru', 'counteragent_civil_law_subject' => 'something'], true],
            [['counteragent_email' => 'test2@simple-technology.ru', 'counteragent_civil_law_subject' => null], true],
            [['counteragent_email' => null, 'counteragent_civil_law_subject' => 'something'], true],
            [['counteragent_email' => null, 'counteragent_civil_law_subject' => null], false],
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-token
     */
    public function testIsCounteragentDataMatchFalse()
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => $this->user_login]);

        /** @var CivilLawSubject $counteragentCivilLLawSubject */
        $counteragentCivilLLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $counteragent_email = 'test2@simple-technology.ru';

        $result = $this->dealTokenManagerPolitics
            ->isCounteragentDataMatch($counteragentCivilLLawSubject, $counteragent_email);

        $this->assertFalse($result);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-token
     */
    public function testIsCounteragentDataMatchTrue()
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => $this->user_login]);

        /** @var CivilLawSubject $counteragentCivilLLawSubject */
        $counteragentCivilLLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $counteragent_email = 'test@simple-technology.ru';

        $result = $this->dealTokenManagerPolitics
            ->isCounteragentDataMatch($counteragentCivilLLawSubject, $counteragent_email);

        $this->assertTrue($result);
    }
}