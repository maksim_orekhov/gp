<?php

namespace ApplicationTest\Service\Deal;

use Application\Entity\Deal;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\TimelineManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;

class TimelineManagerTest extends AbstractHttpControllerTestCase
{
    const DEAL_IN_STATUS_NEGOTIATION_NAME = 'Сделка Negotiation';
    const DEAL_IN_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';
    const DEAL_IN_STATUS_DEBIT_MATCHED_NAME = 'Сделка Оплаченная';
    const DEAL_IN_STATUS_DISPUTE_NAME = 'Сделка Спор открыт';
    const DEAL_WITH_CLOSED_DISPUTE_NAME = 'Сделка Спор со Скидкой закрыт без продления';
    const DEAL_IN_STATUS_DELIVERED_NAME = 'Сделка Доставка подтверждена';
    const DEAL_IN_STATUS_PENDING_CREDIT_PAYMENT_NAME = 'Сделка Ожидает выплаты';
    const DEAL_IN_STATUS_CLOSED = 'Сделка Закрыта';
    const DEAL_IN_STATUS_PENDING_REFUND_PAYMENT = 'Сделка Ожидает возврата';
    const DEAL_IN_STATUS_REFUND = 'Сделка Закрыто с возвратом';
    const DEAL_IN_STATUS_TRIBUNAL = 'Сделка Третейский суд';
    const DEAL_IN_STATUS_TROUBLE = 'Сделка Переплата';


    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var TimelineManager
     */
    private $timelineManager;


    public function setUp()
    {
        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            include __DIR__ . '/../../../../../config/autoload/global.php'
        ));

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->timelineManager = $serviceManager->get(TimelineManager::class);

        $config = $this->getApplicationConfig();
        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        #$this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        // Transaction rollback
        #$this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }


    /**
     * Timeline для сделки в статусе TROUBLE
     *
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusTrouble()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_TROUBLE]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertTrue($timeline['debit_matched']['is_current_status_match']);
        $this->assertFalse($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_AFTER, $timeline['debit_matched']['description']);
        $this->assertNotEmpty($timeline['debit_matched']['date']);

        $this->assertArrayNotHasKey('delivered', $timeline);

        $this->assertArrayHasKey('trouble', $timeline);
        $this->assertTrue($timeline['trouble']['is_current_status_match']);
        $this->assertTrue($timeline['trouble']['is_current_status']);
        $this->assertFalse($timeline['trouble']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_TROUBLE_PHASE_IN, $timeline['trouble']['description']);
        #$this->assertNotEmpty($timeline['trouble']['date']); // Тут даты нет
        $this->assertNotEmpty($timeline['trouble']['troubles']);

        $this->assertArrayHasKey('pending_credit_payment', $timeline);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status_match']);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status']);
        $this->assertFalse($timeline['pending_credit_payment']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_BEFORE, $timeline['pending_credit_payment']['description']);
        $this->assertEmpty($timeline['pending_credit_payment']['date']);

        $this->assertArrayHasKey('closed', $timeline);
        $this->assertFalse($timeline['closed']['is_current_status_match']);
        $this->assertFalse($timeline['closed']['is_current_status']);
        $this->assertFalse($timeline['closed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CLOSED_PHASE_BEFORE, $timeline['closed']['description']);
        $this->assertEmpty($timeline['closed']['date']);
    }

    /**
     * Timeline для сделки в статусе PENDING_REFUND_TRIBUNAL
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusTribunal()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_TRIBUNAL]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertTrue($timeline['debit_matched']['is_current_status_match']);
        $this->assertFalse($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_AFTER, $timeline['debit_matched']['description']);
        $this->assertNotEmpty($timeline['debit_matched']['date']);

        $this->assertArrayHasKey('dispute', $timeline);
        $this->assertTrue($timeline['dispute']['is_current_status_match']);
        $this->assertFalse($timeline['dispute']['is_current_status']);
        $this->assertFalse($timeline['dispute']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DISPUTE_PHASE_IN, $timeline['dispute']['description']);
        $this->assertNotEmpty($timeline['dispute']['date']);

        $this->assertArrayHasKey('tribunal', $timeline);
        $this->assertTrue($timeline['tribunal']['is_current_status_match']);
        $this->assertTrue($timeline['tribunal']['is_current_status']);
        $this->assertFalse($timeline['tribunal']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_TRIBUNAL_PHASE_IN, $timeline['tribunal']['description']);
        $this->assertNotEmpty($timeline['tribunal']['date']);

        $this->assertArrayNotHasKey('delivered', $timeline);

        $this->assertArrayNotHasKey('pending_credit_payment', $timeline);

        $this->assertArrayNotHasKey('closed', $timeline);
    }

    /**
     * Timeline для сделки в статусе PENDING_REFUND_PAYMENT
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusRefund()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_REFUND]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertTrue($timeline['debit_matched']['is_current_status_match']);
        $this->assertFalse($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_AFTER, $timeline['debit_matched']['description']);
        $this->assertNotEmpty($timeline['debit_matched']['date']);

        $this->assertArrayHasKey('dispute', $timeline);
        $this->assertTrue($timeline['dispute']['is_current_status_match']);
        $this->assertFalse($timeline['dispute']['is_current_status']);
        $this->assertFalse($timeline['dispute']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DISPUTE_PHASE_IN, $timeline['dispute']['description']);
        $this->assertNotEmpty($timeline['dispute']['date']);

        $this->assertArrayHasKey('refund', $timeline);
        $this->assertTrue($timeline['refund']['is_current_status_match']);
        $this->assertTrue($timeline['refund']['is_current_status']);
        $this->assertFalse($timeline['refund']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_REFUND_PHASE_T, $timeline['refund']['description']);
        $this->assertNotEmpty($timeline['refund']['date']);

        $this->assertArrayNotHasKey('delivered', $timeline);

        $this->assertArrayNotHasKey('pending_credit_payment', $timeline);

        $this->assertArrayNotHasKey('closed', $timeline);
    }

    /**
     * Timeline для сделки в статусе PENDING_REFUND_PAYMENT
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusPendingRefundPayment()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_PENDING_REFUND_PAYMENT]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertTrue($timeline['debit_matched']['is_current_status_match']);
        $this->assertFalse($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_AFTER, $timeline['debit_matched']['description']);
        $this->assertNotEmpty($timeline['debit_matched']['date']);

        $this->assertArrayHasKey('dispute', $timeline);
        $this->assertTrue($timeline['dispute']['is_current_status_match']);
        $this->assertFalse($timeline['dispute']['is_current_status']);
        $this->assertFalse($timeline['dispute']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DISPUTE_PHASE_IN, $timeline['dispute']['description']);
        $this->assertNotEmpty($timeline['dispute']['date']);

        $this->assertArrayHasKey('pending_refund_payment', $timeline);
        $this->assertTrue($timeline['pending_refund_payment']['is_current_status_match']);
        $this->assertTrue($timeline['pending_refund_payment']['is_current_status']);
        $this->assertFalse($timeline['pending_refund_payment']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_PENDING_REFUND_PAYMENT_PHASE_T, $timeline['pending_refund_payment']['description']);
        $this->assertNotEmpty($timeline['pending_refund_payment']['date']);

        $this->assertArrayNotHasKey('delivered', $timeline);

        $this->assertArrayNotHasKey('pending_credit_payment', $timeline);

        $this->assertArrayNotHasKey('closed', $timeline);
    }

    /**
     * Timeline для сделки в статусе CLOSED
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusClosed()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_CLOSED]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertTrue($timeline['debit_matched']['is_current_status_match']);
        $this->assertFalse($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_AFTER, $timeline['debit_matched']['description']);
        $this->assertNotEmpty($timeline['debit_matched']['date']);

        $this->assertArrayHasKey('delivered', $timeline);
        $this->assertTrue($timeline['delivered']['is_current_status_match']);
        $this->assertFalse($timeline['delivered']['is_current_status']);
        $this->assertFalse($timeline['delivered']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DELIVERED_PHASE_AFTER_U, $timeline['delivered']['description']);
        $this->assertNotEmpty($timeline['delivered']['date']);

        $this->assertArrayHasKey('pending_credit_payment', $timeline);
        $this->assertTrue($timeline['pending_credit_payment']['is_current_status_match']);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status']);
        $this->assertFalse($timeline['pending_credit_payment']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_AFTER, $timeline['pending_credit_payment']['description']);
        $this->assertNotEmpty($timeline['pending_credit_payment']['date']);

        $this->assertArrayHasKey('closed', $timeline);
        $this->assertTrue($timeline['closed']['is_current_status_match']);
        $this->assertTrue($timeline['closed']['is_current_status']);
        $this->assertFalse($timeline['closed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CLOSED_PHASE_AFTER, $timeline['closed']['description']);
        $this->assertNotEmpty($timeline['closed']['date']);
    }

    /**
     * Timeline для сделки в статусе PENDING_CREDIT_PAYMENT
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusPendingCreditPayment()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_PENDING_CREDIT_PAYMENT_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertTrue($timeline['debit_matched']['is_current_status_match']);
        $this->assertFalse($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_AFTER, $timeline['debit_matched']['description']);
        $this->assertNotEmpty($timeline['debit_matched']['date']);

        $this->assertArrayHasKey('delivered', $timeline);
        $this->assertTrue($timeline['delivered']['is_current_status_match']);
        $this->assertFalse($timeline['delivered']['is_current_status']);
        $this->assertFalse($timeline['delivered']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DELIVERED_PHASE_AFTER_U, $timeline['delivered']['description']);
        $this->assertNotEmpty($timeline['delivered']['date']);

        $this->assertArrayHasKey('pending_credit_payment', $timeline);
        $this->assertTrue($timeline['pending_credit_payment']['is_current_status_match']);
        $this->assertTrue($timeline['pending_credit_payment']['is_current_status']);
        $this->assertFalse($timeline['pending_credit_payment']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_AFTER, $timeline['pending_credit_payment']['description']);
        $this->assertNotEmpty($timeline['pending_credit_payment']['date']);

        $this->assertArrayHasKey('closed', $timeline);
        $this->assertFalse($timeline['closed']['is_current_status_match']);
        $this->assertFalse($timeline['closed']['is_current_status']);
        $this->assertTrue($timeline['closed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CLOSED_PHASE_BEFORE, $timeline['closed']['description']);
        $this->assertEmpty($timeline['closed']['date']);
    }

    /**
     * Timeline для сделки в статусе DELIVERED
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusDelivered()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_DELIVERED_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertTrue($timeline['debit_matched']['is_current_status_match']);
        $this->assertFalse($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_AFTER, $timeline['debit_matched']['description']);
        $this->assertNotEmpty($timeline['debit_matched']['date']);

        $this->assertArrayNotHasKey('dispute', $timeline);

        $this->assertArrayHasKey('delivered', $timeline);
        $this->assertTrue($timeline['delivered']['is_current_status_match']);
        $this->assertTrue($timeline['delivered']['is_current_status']);
        $this->assertFalse($timeline['delivered']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DELIVERED_PHASE_AFTER_T, $timeline['delivered']['description']);
        $this->assertNotEmpty($timeline['delivered']['date']);

        $this->assertArrayHasKey('pending_credit_payment', $timeline);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status_match']);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status']);
        $this->assertTrue($timeline['pending_credit_payment']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_BEFORE, $timeline['pending_credit_payment']['description']);
        $this->assertEmpty($timeline['pending_credit_payment']['date']);

        $this->assertArrayHasKey('closed', $timeline);
        $this->assertFalse($timeline['closed']['is_current_status_match']);
        $this->assertFalse($timeline['closed']['is_current_status']);
        $this->assertFalse($timeline['closed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CLOSED_PHASE_BEFORE, $timeline['closed']['description']);
        $this->assertEmpty($timeline['closed']['date']);
    }

    /**
     * Timeline для сделки c закрытым DISPUTE
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineWithClosedDispute()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_CLOSED_DISPUTE_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertTrue($timeline['debit_matched']['is_current_status_match']);
        $this->assertTrue($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_AFTER, $timeline['debit_matched']['description']);
        $this->assertNotEmpty($timeline['debit_matched']['date']);

        $this->assertArrayNotHasKey('dispute', $timeline);

        $this->assertArrayHasKey('delivered', $timeline);
        $this->assertFalse($timeline['delivered']['is_current_status_match']);
        $this->assertFalse($timeline['delivered']['is_current_status']);
        $this->assertTrue($timeline['delivered']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DELIVERED_PHASE_BEFORE, $timeline['delivered']['description']);
        $this->assertEmpty($timeline['delivered']['date']);

        $this->assertArrayHasKey('pending_credit_payment', $timeline);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status_match']);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status']);
        $this->assertFalse($timeline['pending_credit_payment']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_BEFORE, $timeline['pending_credit_payment']['description']);
        $this->assertEmpty($timeline['pending_credit_payment']['date']);

        $this->assertArrayHasKey('closed', $timeline);
        $this->assertFalse($timeline['closed']['is_current_status_match']);
        $this->assertFalse($timeline['closed']['is_current_status']);
        $this->assertFalse($timeline['closed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CLOSED_PHASE_BEFORE, $timeline['closed']['description']);
        $this->assertEmpty($timeline['closed']['date']);
    }

    /**
     * Timeline для сделки в статусе DISPUTE
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusDispute()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_DISPUTE_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertTrue($timeline['debit_matched']['is_current_status_match']);
        $this->assertFalse($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_AFTER, $timeline['debit_matched']['description']);
        $this->assertNotEmpty($timeline['debit_matched']['date']);

        $this->assertArrayHasKey('dispute', $timeline);
        $this->assertTrue($timeline['dispute']['is_current_status_match']);
        $this->assertTrue($timeline['dispute']['is_current_status']);
        $this->assertFalse($timeline['dispute']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DISPUTE_PHASE_IN, $timeline['dispute']['description']);
        $this->assertNotEmpty($timeline['dispute']['date']);

        $this->assertArrayNotHasKey('delivered', $timeline);

        $this->assertArrayHasKey('pending_credit_payment', $timeline);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status_match']);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status']);
        $this->assertFalse($timeline['pending_credit_payment']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_BEFORE, $timeline['pending_credit_payment']['description']);
        $this->assertEmpty($timeline['pending_credit_payment']['date']);

        $this->assertArrayHasKey('closed', $timeline);
        $this->assertFalse($timeline['closed']['is_current_status_match']);
        $this->assertFalse($timeline['closed']['is_current_status']);
        $this->assertFalse($timeline['closed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CLOSED_PHASE_BEFORE, $timeline['closed']['description']);
        $this->assertEmpty($timeline['closed']['date']);
    }

    /**
     * Timeline для сделки в статусе DEBIT_MATCHED
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusDebitMatched()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_DEBIT_MATCHED_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertTrue($timeline['debit_matched']['is_current_status_match']);
        $this->assertTrue($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_AFTER, $timeline['debit_matched']['description']);
        $this->assertNotEmpty($timeline['debit_matched']['date']);

        $this->assertArrayHasKey('delivered', $timeline);
        $this->assertFalse($timeline['delivered']['is_current_status_match']);
        $this->assertFalse($timeline['delivered']['is_current_status']);
        $this->assertTrue($timeline['delivered']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DELIVERED_PHASE_BEFORE, $timeline['delivered']['description']);
        $this->assertEmpty($timeline['delivered']['date']);

        $this->assertArrayHasKey('pending_credit_payment', $timeline);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status_match']);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status']);
        $this->assertFalse($timeline['pending_credit_payment']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_BEFORE, $timeline['pending_credit_payment']['description']);
        $this->assertEmpty($timeline['pending_credit_payment']['date']);

        $this->assertArrayHasKey('closed', $timeline);
        $this->assertFalse($timeline['closed']['is_current_status_match']);
        $this->assertFalse($timeline['closed']['is_current_status']);
        $this->assertFalse($timeline['closed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CLOSED_PHASE_BEFORE, $timeline['closed']['description']);
        $this->assertEmpty($timeline['closed']['date']);
    }

    /**
     * Timeline для сделки в статусе CONFIRMED
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusConfirmed()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertFalse($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertTrue($timeline['confirmed']['is_current_status_match']);
        $this->assertTrue($timeline['confirmed']['is_current_status']);
        $this->assertFalse($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_AFTER, $timeline['confirmed']['description']);
        $this->assertNotEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertFalse($timeline['debit_matched']['is_current_status_match']);
        $this->assertFalse($timeline['debit_matched']['is_current_status']);
        $this->assertTrue($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_BEFORE, $timeline['debit_matched']['description']);
        $this->assertEmpty($timeline['debit_matched']['date']);

        $this->assertArrayHasKey('delivered', $timeline);
        $this->assertFalse($timeline['delivered']['is_current_status_match']);
        $this->assertFalse($timeline['delivered']['is_current_status']);
        $this->assertFalse($timeline['delivered']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DELIVERED_PHASE_BEFORE, $timeline['delivered']['description']);
        $this->assertEmpty($timeline['delivered']['date']);

        $this->assertArrayHasKey('pending_credit_payment', $timeline);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status_match']);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status']);
        $this->assertFalse($timeline['pending_credit_payment']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_BEFORE, $timeline['pending_credit_payment']['description']);
        $this->assertEmpty($timeline['pending_credit_payment']['date']);

        $this->assertArrayHasKey('closed', $timeline);
        $this->assertFalse($timeline['closed']['is_current_status_match']);
        $this->assertFalse($timeline['closed']['is_current_status']);
        $this->assertFalse($timeline['closed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CLOSED_PHASE_BEFORE, $timeline['closed']['description']);
        $this->assertEmpty($timeline['closed']['date']);
    }

    /**
     * Timeline для сделки в статусе NEGOTIATION
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group time-line
     */
    public function testBuildTimelineForStatusNegotiation()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_NEGOTIATION_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $timeline = $this->timelineManager->buildTimeLine($deal);

        $this->assertNotNull($timeline);

        $this->assertArrayHasKey('created', $timeline);
        $this->assertTrue($timeline['created']['is_current_status_match']);
        $this->assertTrue($timeline['created']['is_current_status']);
        $this->assertFalse($timeline['created']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_NEGOTIATION_PHASE_AFTER, $timeline['created']['description']);
        $this->assertNotEmpty($timeline['created']['date']);

        $this->assertArrayHasKey('confirmed', $timeline);
        $this->assertFalse($timeline['confirmed']['is_current_status_match']);
        $this->assertFalse($timeline['confirmed']['is_current_status']);
        $this->assertTrue($timeline['confirmed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CONFIRMED_PHASE_BEFORE, $timeline['confirmed']['description']);
        $this->assertEmpty($timeline['confirmed']['date']);

        $this->assertArrayHasKey('debit_matched', $timeline);
        $this->assertFalse($timeline['debit_matched']['is_current_status_match']);
        $this->assertFalse($timeline['debit_matched']['is_current_status']);
        $this->assertFalse($timeline['debit_matched']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DEBIT_MATCHED_PHASE_BEFORE, $timeline['debit_matched']['description']);
        $this->assertEmpty($timeline['debit_matched']['date']);

        $this->assertArrayHasKey('delivered', $timeline);
        $this->assertFalse($timeline['delivered']['is_current_status_match']);
        $this->assertFalse($timeline['delivered']['is_current_status']);
        $this->assertFalse($timeline['delivered']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_DELIVERED_PHASE_BEFORE, $timeline['delivered']['description']);
        $this->assertEmpty($timeline['delivered']['date']);

        $this->assertArrayHasKey('pending_credit_payment', $timeline);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status_match']);
        $this->assertFalse($timeline['pending_credit_payment']['is_current_status']);
        $this->assertFalse($timeline['pending_credit_payment']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_PENDING_CREDIT_PAYMENT_PHASE_BEFORE, $timeline['pending_credit_payment']['description']);
        $this->assertEmpty($timeline['pending_credit_payment']['date']);

        $this->assertArrayHasKey('closed', $timeline);
        $this->assertFalse($timeline['closed']['is_current_status_match']);
        $this->assertFalse($timeline['closed']['is_current_status']);
        $this->assertFalse($timeline['closed']['is_next_status']);
        $this->assertEquals(TimelineManager::TIMELITE_CLOSED_PHASE_BEFORE, $timeline['closed']['description']);
        $this->assertEmpty($timeline['closed']['date']);
    }
}