<?php

namespace ApplicationTest\Service\Deal;

use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\User;
use ApplicationTest\Bootstrap;
use Core\Service\Base\BaseAuthManager;
use Core\Service\ORMDoctrineUtil;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Zend\Session\SessionManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Service\Deal\DealManager;
use Zend\Stdlib\ArrayUtils;

class DealManagerTest extends AbstractHttpControllerTestCase
{
    const DEAL_IN_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';
    const DEAL_PARTIAL_PAID_BY_BANK_TRANSFER_NAME = 'Сделка Частично оплачено';
    const DEAL_IN_STATUS_NEGOTIATION_NAME = 'Сделка Переговорная';
    const DEAL_IN_STATUS_DEBIT_MATCHED_NAME = 'Сделка Оплаченная';
    const DEAL_FIXTURE = 'Сделка Фикстура';
    const DEAL_NEGOTIATION = 'Сделка Negotiation';
    const DEAL_NEGOTIATION_2 = 'Сделка Переговорная';
    const DEAL_INVITATION = 'Сделка с приглашением';

    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var DealManager
     */
    private $dealManager;
    /**
     * @var string
     */
    private $user_login;
    /**
     * @var string
     */
    private $user_password;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;
        $this->dealManager = null;

        gc_collect_cycles();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testConfirm()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Фикстура']);
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $contractorCivilLawSubject = $deal->getContractor()->getCivilLawSubject();
        $customerCivilLawSubject = $deal->getCustomer()->getCivilLawSubject();

        // Set Customer confirm
        $deal->getCustomer()->setDealAgentConfirm(true);
        $deal->getContractor()->setDealAgentConfirm(true);

        $this->entityManager->flush();

        $result = $this->dealManager->cloneSubordinateEntities($deal);

        $updatedDeal = $this->entityManager->getRepository(Deal::class)->find($deal->getId());
        $updatedPayment = $deal->getPayment();

        $this->assertEquals(true, $result);

        // Проверка что в deal склонировался CivilLawSubject у Contractor и Customer
        if ($contractorCivilLawSubject !== null) {
            $this->assertNotEquals($updatedDeal->getContractor()->getCivilLawSubject()->getId(), $contractorCivilLawSubject->getId());
            $this->assertEquals(false, $updatedDeal->getContractor()->getCivilLawSubject()->getIsPattern());
        }
        if ($customerCivilLawSubject !== null) {
            $this->assertNotEquals($updatedDeal->getCustomer()->getCivilLawSubject()->getId(), $customerCivilLawSubject->getId());
            $this->assertEquals(false, $updatedDeal->getCustomer()->getCivilLawSubject()->getIsPattern());
        }

        // Проверка что в payment склонировался PaymentMethod
        $this->assertNotEquals($updatedPayment->getPaymentMethod()->getId(), $payment);
        $this->assertEquals(false, $updatedPayment->getPaymentMethod()->getIsPattern());

        // Проверка что в PaymentMethod засетился CivilLawSubject Customer
        #$this->assertEquals($updatedPayment->getPaymentMethod()->getCivilLawSubject()->getId(), $updatedDeal->getCustomer()->getCivilLawSubject()->getId());
        // @TODO Почему CivilLawSubject Customer? Нужен CivilLawSubject Contractor?
        // Проверка что в PaymentMethod засетился CivilLawSubject Contractor
        $this->assertEquals($updatedPayment->getPaymentMethod()
            ->getCivilLawSubject()->getId(), $updatedDeal->getContractor()->getCivilLawSubject()->getId());
    }

    /**
     * Для неоплаченной сделки (confirmed) для Кастомера
     * Должен вернуть массив:
     *  'bank_transfer' => true
     *  'acquiring_mandarin' => true
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testGetAvailablePaymentMethodTypesOfConfirmedDealForCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $result = $this->dealManager->getAvailablePaymentMethodTypes($deal, true);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey(PaymentMethodType::BANK_TRANSFER, $result);
        $this->assertTrue( $result[PaymentMethodType::BANK_TRANSFER]);
        $this->assertArrayHasKey(PaymentMethodType::ACQUIRING_MANDARIN, $result);
        $this->assertTrue($result[PaymentMethodType::ACQUIRING_MANDARIN]);
    }

    /**
     * Для неоплаченной сделки (confirmed) для не Кастомера
     * Должен вернуть пустой массив
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testGetAvailablePaymentMethodTypesOfConfirmedDealForNotCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $result = $this->dealManager->getAvailablePaymentMethodTypes($deal, false);

        $this->assertInternalType('array', $result);
        $this->assertEmpty($result);
    }

    /**
     * Для частично оплаченной (confirmed) сделки для кастомера
     * Должен вернуть массив:
     * 'bank_transfer' => true
     * 'acquiring_mandarin' => false
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testGetAvailablePaymentMethodTypesOfPartiallyPaidDealForCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_PARTIAL_PAID_BY_BANK_TRANSFER_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $result = $this->dealManager->getAvailablePaymentMethodTypes($deal, true);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey(PaymentMethodType::BANK_TRANSFER, $result);
        $this->assertTrue( $result[PaymentMethodType::BANK_TRANSFER]);
        $this->assertArrayHasKey(PaymentMethodType::ACQUIRING_MANDARIN, $result);
        $this->assertFalse($result[PaymentMethodType::ACQUIRING_MANDARIN]);
    }

    /**
     * Для частично оплаченной сделки (confirmed) для не Кастомера
     * Должен вернуть пустой массив
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testGetAvailablePaymentMethodTypesOfPartiallyPaidDealForNotCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_PARTIAL_PAID_BY_BANK_TRANSFER_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $result = $this->dealManager->getAvailablePaymentMethodTypes($deal, false);

        $this->assertInternalType('array', $result);
        $this->assertEmpty($result);
    }

    /**
     * Для неподтвержденной сделки (negotiation) сделки для Кастомера
     * Должен вернуть пустой массив
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testGetAvailablePaymentMethodTypesOfNegotiationDealForCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_NEGOTIATION_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $result = $this->dealManager->getAvailablePaymentMethodTypes($deal, true);

        $this->assertInternalType('array', $result);
        $this->assertEmpty($result);
    }

    /**
     * Для неподтвержденной сделки (debit_matched) сделки для Кастомера
     * Должен вернуть пустой массив
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testGetAvailablePaymentMethodTypesOfDebitMatchedDealForCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_IN_STATUS_DEBIT_MATCHED_NAME]);

        $this->assertNotNull($deal);

        $this->dealManager->setDealStatus($deal);

        $result = $this->dealManager->getAvailablePaymentMethodTypes($deal, true);

        $this->assertInternalType('array', $result);
        $this->assertEmpty($result);
    }

    /**
     * тест автоприкрепления User к DealAgent
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     */
    public function testAttachDealAgentsToUser()
    {
        $login = 'test2';
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => $login]);
        /**
         * @var Deal $deal
         * @var Deal $deal2
         * @var Deal $deal3
         * @var Deal $deal4
         */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_FIXTURE]);
        $deal2 = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_NEGOTIATION]);
        $deal3 = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_NEGOTIATION_2]);

        $this->assertNotNull($user);
        $this->assertNotNull($deal);
        $this->assertNotNull($deal2);
        $this->assertNotNull($deal3);

        $contractor = $deal->getContractor();
        $contractor2 = $deal2->getContractor();
        $contractor3 = $deal3->getContractor();

        $this->assertSame($user, $contractor->getUser());
        $this->assertSame($user, $contractor2->getUser());
        $this->assertSame($user, $contractor3->getUser());

        /// цель удалить у DealAgent привязку к пользователю и субъекту права
        /// для того что бы произошла атопривязка слушателем после авторизации
        $contractor->setCivilLawSubject(null);
        $contractor2->setCivilLawSubject(null);
        $contractor3->setCivilLawSubject(null);
        $contractor->setUser(null);
        $contractor2->setUser(null);
        $contractor3->setUser(null);
        $this->entityManager->persist($contractor);
        $this->entityManager->persist($contractor2);
        $this->entityManager->persist($contractor3);
        $this->entityManager->flush();

        //// проверяе что мы отвязали user и civilLawSubject
        $this->assertNull($contractor->getCivilLawSubject());
        $this->assertNull($contractor2->getCivilLawSubject());
        $this->assertNull($contractor3->getCivilLawSubject());
        $this->assertNull($contractor->getUser());
        $this->assertNull($contractor2->getUser());
        $this->assertNull($contractor3->getUser());
        //// проверяе что email принадлежит нашему пользователю
        $this->assertSame($contractor->getEmail(), $user->getEmail()->getEmail());
        $this->assertSame($contractor2->getEmail(), $user->getEmail()->getEmail());
        $this->assertSame($contractor3->getEmail(), $user->getEmail()->getEmail());

        $this->login($login);

        //проверяем что пользователь пригрепиля к DealAgent
        $this->assertSame($user, $contractor->getUser());
        $this->assertSame($user, $contractor2->getUser());
        $this->assertSame($user, $contractor3->getUser());
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if (!$login) {
            $login = $this->user_login;
        }
        if (!$password) {
            $password = $this->user_password;
        }
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();
        $this->baseAuthManager->login($login, $password, 0);
    }
}