<?php

namespace ApplicationTest\Service\Deal;

use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Fixture\DealFixtureLoader;

/**
 * Trait DeadlineOneDayTrait
 * @package ApplicationTest\Service\Deal
 */
trait DeadlineOneDayTrait
{
    public function setOneDayDeadline()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_DEADLINE_EXPIRED_IN_ONE_DAY_NAME]);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $current_day = new \DateTime();

        $newDeFactoDate = $current_day->modify('-'.$deal->getDeliveryPeriod().' days');
        $newDeFactoDate->modify('+20 hours');

        $payment->setDeFactoDate($newDeFactoDate);

        $this->entityManager->persist($payment);
        $this->entityManager->flush();
    }
}