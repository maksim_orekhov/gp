<?php

namespace ApplicationTest\Service\Deal;

use Application\Entity\CivilLawSubject;
use Application\Entity\DealType;
use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Deal\DealTokenManager;
use Application\Service\Deal\DealTypeManager;
use ApplicationTest\Bootstrap;
use Core\Adapter\TokenAdapter;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class DealTokenManagerTest
 * @package ApplicationTest\Service\Deal
 */
class DealTokenManagerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var DealTypeManager
     */
    private $dealTypeManager;

    /**
     * @var DealTokenManager
     */
    private $dealTokenManager;

    /**
     * @var TokenAdapter
     */
    private $tokenAdapter;


    public function setUp()
    {
        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();

        // Add content of global.php and other to ApplicationConfig
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->dealTypeManager = $serviceManager->get(DealTypeManager::class);
        $this->dealTokenManager = $serviceManager->get(DealTokenManager::class);
        $this->tokenAdapter = $serviceManager->get(TokenAdapter::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $config = $this->getApplicationConfig();
    }

    public function tearDown()
    {
        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-token
     *
     * @expectedException \Core\Exception\LogicException
     * @expectedExceptionMessage Invalid token.
     */
    public function testPrepareDataFromInvalidToken()
    {
        // Невалидный токен
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhZ2VudF9lbWFpbCI6InRlc3RAZ3VhcmFudHBheS5jb20iLCJkZWFsX3R5cG';

        // Выкинет Exception
        $decodedToken = $this->tokenAdapter->decodeToken($token);
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-token
     */
    public function testPrepareDataFromToken()
    {
        $data = [
            'agent_email' => 'Test@simple-technology.ru', // преобразование к нижнему регистру
            'deal_type' => 'U',
            'amount' => '10000',
            'counteragent_email' => 'TEST2@SIMPLE-TECHNOLOGY.RU', // преобразование к нижнему регистру
            'counteragent_civil_law_subject' => null,
        ];
        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($data);

        $result = $this->dealTokenManager->prepareDataFromToken($encodedToken);

        $this->assertArrayHasKey('post_data', $result);
        $this->assertArrayHasKey('validation', $result);

        $this->assertArrayHasKey('agent_email', $result['post_data']);
        $this->assertEquals('test@simple-technology.ru', $result['post_data']['agent_email']);

        $this->assertArrayHasKey('deal_type', $result['post_data']);
        $this->assertEquals('U', $result['post_data']['deal_type']);

        $this->assertArrayHasKey('deal_type_id', $result['post_data']);

        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(['ident' => $result['post_data']['deal_type']]);

        $this->assertEquals($dealType->getId(), $result['post_data']['deal_type_id']);

        $this->assertArrayHasKey('amount', $result['post_data']);
        $this->assertEquals('10000', $result['post_data']['amount']);

        $this->assertArrayHasKey('counteragent_email', $result['post_data']);
        $this->assertEquals('test2@simple-technology.ru', $result['post_data']['counteragent_email']);

        $this->assertArrayHasKey('counteragent_civil_law_subject', $result['post_data']);
        $this->assertEquals(null, $result['post_data']['counteragent_civil_law_subject']);

        $this->assertArrayHasKey('is_valid', $result['validation']);
        $this->assertTrue($result['validation']['is_valid']);
        $this->assertNull($result['validation']['agent_email']);
        $this->assertNull($result['validation']['deal_type']);
        $this->assertNull($result['validation']['amount']);
        $this->assertNull($result['validation']['counteragent_email']);
        $this->assertNull($result['validation']['counteragent_civil_law_subject']);
        $this->assertNull($result['validation']['custom']);
    }

    /**
     * @throws \Exception
     *
     *
     *
     * @group deal
     * @group deal-token
     */
    public function testPrepareDataFromTokenWithoutAgentEmail()
    {
        $data = [
            'agent_email' => null, // преобразование к нижнему регистру
            'deal_type' => 'T',
            'amount' => '10000',
            'counteragent_email' => 'test2@simple-technology.ru',
            'counteragent_civil_law_subject' => null,
        ];
        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($data);

        $result = $this->dealTokenManager->prepareDataFromToken($encodedToken);

        $this->assertArrayHasKey('post_data', $result);
        $this->assertArrayHasKey('validation', $result);

        $this->assertArrayHasKey('agent_email', $result['post_data']);
        $this->assertNull($result['post_data']['agent_email']);

        $this->assertArrayHasKey('deal_type', $result['post_data']);
        $this->assertEquals('T', $result['post_data']['deal_type']);

        $this->assertArrayHasKey('deal_type_id', $result['post_data']);

        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(['ident' => $result['post_data']['deal_type']]);

        $this->assertEquals($dealType->getId(), $result['post_data']['deal_type_id']);

        $this->assertArrayHasKey('amount', $result['post_data']);
        $this->assertEquals('10000', $result['post_data']['amount']);

        $this->assertArrayHasKey('counteragent_email', $result['post_data']);
        $this->assertEquals('test2@simple-technology.ru', $result['post_data']['counteragent_email']);

        $this->assertArrayHasKey('counteragent_civil_law_subject', $result['post_data']);
        $this->assertEquals(null, $result['post_data']['counteragent_civil_law_subject']);

        $this->assertArrayHasKey('is_valid', $result['validation']);
        $this->assertFalse($result['validation']['is_valid']);
        $this->assertNotNull($result['validation']['agent_email']);
        $this->assertInternalType('array', $result['validation']['agent_email']);
        $this->assertArrayHasKey('isEmpty', $result['validation']['agent_email']);
        $this->assertEquals('Value is required and can\'t be empty', $result['validation']['agent_email']['isEmpty']);
        $this->assertNull($result['validation']['deal_type']);
        $this->assertNull($result['validation']['amount']);
        $this->assertNull($result['validation']['counteragent_email']);
        $this->assertNull($result['validation']['counteragent_civil_law_subject']);
        $this->assertNull($result['validation']['custom']);
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-token
     */
    public function testPrepareDataFromTokenWithoutCounteragentEmailAndCivilLawSubject()
    {
        $data = [
            'agent_email' => 'Test@Simple-Technology.ru', // преобразование к нижнему регистру
            'deal_type' => 'T',
            'amount' => '10000',
            'counteragent_email' => null,
            'counteragent_civil_law_subject' => null,
        ];
        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($data);

        $result = $this->dealTokenManager->prepareDataFromToken($encodedToken);

        $this->assertArrayHasKey('post_data', $result);
        $this->assertArrayHasKey('validation', $result);

        $this->assertArrayHasKey('agent_email', $result['post_data']);
        $this->assertEquals('test@simple-technology.ru', $result['post_data']['agent_email']);

        $this->assertArrayHasKey('deal_type', $result['post_data']);
        $this->assertEquals('T', $result['post_data']['deal_type']);

        $this->assertArrayHasKey('deal_type_id', $result['post_data']);

        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(['ident' => $result['post_data']['deal_type']]);

        $this->assertEquals($dealType->getId(), $result['post_data']['deal_type_id']);

        $this->assertArrayHasKey('amount', $result['post_data']);
        $this->assertEquals('10000', $result['post_data']['amount']);

        $this->assertArrayHasKey('counteragent_email', $result['post_data']);
        $this->assertNull($result['post_data']['counteragent_email']);

        $this->assertArrayHasKey('counteragent_civil_law_subject', $result['post_data']);
        $this->assertEquals(null, $result['post_data']['counteragent_civil_law_subject']);

        $this->assertArrayHasKey('is_valid', $result['validation']);
        $this->assertFalse($result['validation']['is_valid']);
        $this->assertNull($result['validation']['agent_email']);
        $this->assertNull($result['validation']['deal_type']);
        $this->assertNull($result['validation']['amount']);
        $this->assertNull($result['validation']['counteragent_email']);
        $this->assertNull($result['validation']['counteragent_civil_law_subject']);
        $this->assertNotNull($result['validation']['custom']);
        $this->assertInternalType('array', $result['validation']['custom']);
        $this->assertArrayHasKey('counteragent_email', $result['validation']['custom']);
        $this->assertArrayHasKey('forbiddenCreate', $result['validation']['custom']['counteragent_email']);
        $this->assertInternalType('array', $result['validation']['custom']['counteragent_email']['forbiddenCreate']);
        $this->assertEquals('Некорректно указаны данные контрагента', $result['validation']['custom']['counteragent_email']['forbiddenCreate'][0]);
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-token
     */
    public function testPrepareDataFromTokenWithoutCounteragentEmail()
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);

        /** @var CivilLawSubject $counteragentCivillLawSubject */
        $counteragentCivillLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $data = [
            'agent_email' => 'Test@Simple-Technology.ru', // преобразование к нижнему регистру
            'deal_type' => 'T',
            'amount' => '10000',
            'counteragent_email' => null,
            'counteragent_civil_law_subject' => $counteragentCivillLawSubject->getId(),
        ];
        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($data);

        $result = $this->dealTokenManager->prepareDataFromToken($encodedToken);

        $this->assertArrayHasKey('post_data', $result);
        $this->assertArrayHasKey('validation', $result);

        $this->assertArrayHasKey('agent_email', $result['post_data']);
        $this->assertEquals('test@simple-technology.ru', $result['post_data']['agent_email']);

        $this->assertArrayHasKey('deal_type', $result['post_data']);
        $this->assertEquals('T', $result['post_data']['deal_type']);

        $this->assertArrayHasKey('deal_type_id', $result['post_data']);

        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(['ident' => $result['post_data']['deal_type']]);

        $this->assertEquals($dealType->getId(), $result['post_data']['deal_type_id']);

        $this->assertArrayHasKey('amount', $result['post_data']);
        $this->assertEquals('10000', $result['post_data']['amount']);

        $this->assertArrayHasKey('counteragent_email', $result['post_data']);
        $this->assertEquals($counteragentCivillLawSubject->getUser()->getEmail()->getEmail(), $result['post_data']['counteragent_email']);

        $this->assertArrayHasKey('counteragent_civil_law_subject', $result['post_data']);
        $this->assertEquals($counteragentCivillLawSubject->getId(), $result['post_data']['counteragent_civil_law_subject']);

        $this->assertArrayHasKey('is_valid', $result['validation']);
        $this->assertTrue($result['validation']['is_valid']);
        $this->assertNull($result['validation']['agent_email']);
        $this->assertNull($result['validation']['deal_type']);
        $this->assertNull($result['validation']['amount']);
        $this->assertNull($result['validation']['counteragent_email']);
        $this->assertNull($result['validation']['counteragent_civil_law_subject']);
        $this->assertNull($result['validation']['custom']);
    }

    /**
     * counteragent_email и counteragent_civil_law_subject принадлежат одному пользователю
     *
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-token
     */
    public function testPrepareDataFromTokenWithCounteragentEmailAndCivilLawSubjectMatch()
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);

        /** @var CivilLawSubject $counteragentCivilLLawSubject */
        $counteragentCivilLLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $data = [
            'agent_email' => 'Test@Simple-Technology.ru',
            'deal_type' => 'T',
            'amount' => '10000',
            'counteragent_email' => 'test2@simple-technology.ru',
            'counteragent_civil_law_subject' => $counteragentCivilLLawSubject->getId(),
        ];
        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($data);

        $result = $this->dealTokenManager->prepareDataFromToken($encodedToken);

        $this->assertArrayHasKey('post_data', $result);
        $this->assertArrayHasKey('validation', $result);

        $this->assertArrayHasKey('agent_email', $result['post_data']);
        $this->assertEquals('test@simple-technology.ru', $result['post_data']['agent_email']);

        $this->assertArrayHasKey('deal_type', $result['post_data']);
        $this->assertEquals('T', $result['post_data']['deal_type']);

        $this->assertArrayHasKey('deal_type_id', $result['post_data']);

        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(['ident' => $result['post_data']['deal_type']]);

        $this->assertEquals($dealType->getId(), $result['post_data']['deal_type_id']);

        $this->assertArrayHasKey('amount', $result['post_data']);
        $this->assertEquals('10000', $result['post_data']['amount']);

        $this->assertArrayHasKey('counteragent_email', $result['post_data']);
        $this->assertEquals('test2@simple-technology.ru', $result['post_data']['counteragent_email']);

        $this->assertArrayHasKey('counteragent_civil_law_subject', $result['post_data']);
        $this->assertEquals($counteragentCivilLLawSubject->getId(), $result['post_data']['counteragent_civil_law_subject']);

        $this->assertArrayHasKey('is_valid', $result['validation']);
        $this->assertTrue($result['validation']['is_valid']);
        $this->assertNull($result['validation']['agent_email']);
        $this->assertNull($result['validation']['deal_type']);
        $this->assertNull($result['validation']['amount']);
        $this->assertNull($result['validation']['counteragent_email']);
        $this->assertNull($result['validation']['counteragent_civil_law_subject']);
        $this->assertNull($result['validation']['custom']);
    }

    /**
     * counteragent_email и counteragent_civil_law_subject принадлежат разным пользователям
     *
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-token
     */
    public function testPrepareDataFromTokenWithCounteragentEmailAndCivilLawSubjectNotMatch()
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test']);

        /** @var CivilLawSubject $counteragentCivilLLawSubject */
        $counteragentCivilLLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $data = [
            'agent_email' => 'test@simple-technology.ru',
            'deal_type' => 'T',
            'amount' => '10000',
            'counteragent_email' => 'test2@simple-technology.ru',
            'counteragent_civil_law_subject' => $counteragentCivilLLawSubject->getId(),
        ];
        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($data);

        $result = $this->dealTokenManager->prepareDataFromToken($encodedToken);

        $this->assertArrayHasKey('post_data', $result);
        $this->assertArrayHasKey('validation', $result);

        $this->assertArrayHasKey('agent_email', $result['post_data']);
        $this->assertEquals('test@simple-technology.ru', $result['post_data']['agent_email']);

        $this->assertArrayHasKey('deal_type', $result['post_data']);
        $this->assertEquals('T', $result['post_data']['deal_type']);

        $this->assertArrayHasKey('deal_type_id', $result['post_data']);

        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(['ident' => $result['post_data']['deal_type']]);

        $this->assertEquals($dealType->getId(), $result['post_data']['deal_type_id']);

        $this->assertArrayHasKey('amount', $result['post_data']);
        $this->assertEquals('10000', $result['post_data']['amount']);

        $this->assertArrayHasKey('counteragent_email', $result['post_data']);
        $this->assertEquals('test2@simple-technology.ru', $result['post_data']['counteragent_email']);

        $this->assertArrayHasKey('counteragent_civil_law_subject', $result['post_data']);
        $this->assertEquals($counteragentCivilLLawSubject->getId(), $result['post_data']['counteragent_civil_law_subject']);

        $this->assertArrayHasKey('is_valid', $result['validation']);
        $this->assertFalse($result['validation']['is_valid']);
        $this->assertNull($result['validation']['agent_email']);
        $this->assertNull($result['validation']['deal_type']);
        $this->assertNull($result['validation']['amount']);
        $this->assertNull($result['validation']['counteragent_email']);
        $this->assertNull($result['validation']['counteragent_civil_law_subject']);
        $this->assertNotNull($result['validation']['custom']);
        $this->assertInternalType('array', $result['validation']['custom']);
        $this->assertArrayHasKey('counteragent_email', $result['validation']['custom']);
        $this->assertArrayHasKey('forbiddenCreate', $result['validation']['custom']['counteragent_email']);
        $this->assertInternalType('array', $result['validation']['custom']['counteragent_email']['forbiddenCreate']);
        $this->assertEquals(
            'counteragent_civil_law_subject не принадлежит пользователю с counteragent_email',
            $result['validation']['custom']['counteragent_email']['forbiddenCreate'][0]
        );
    }

    /**
     * указан существующий counteragent_email и несуществующий counteragent_civil_law_subject
     *
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal-token
     */
    public function testPrepareDataFromTokenWithCounteragentEmailAndNonexistentCivilLawSubject()
    {
        /** @var CivilLawSubject $lastCivilLLawSubject */
        $lastCivilLLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(array(), array('id' => 'DESC'));

        $nonexistent_civil_law_subject_id = $lastCivilLLawSubject->getId() + 1;

        $data = [
            'agent_email' => 'Test@Simple-Technology.ru',
            'deal_type' => 'T',
            'amount' => '10000',
            'counteragent_email' => 'test2@simple-technology.ru',
            'counteragent_civil_law_subject' => $nonexistent_civil_law_subject_id, // несущуствующий id
        ];
        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($data);

        $result = $this->dealTokenManager->prepareDataFromToken($encodedToken);

        $this->assertArrayHasKey('post_data', $result);
        $this->assertArrayHasKey('validation', $result);

        $this->assertArrayHasKey('agent_email', $result['post_data']);
        $this->assertEquals('test@simple-technology.ru', $result['post_data']['agent_email']);

        $this->assertArrayHasKey('deal_type', $result['post_data']);
        $this->assertEquals('T', $result['post_data']['deal_type']);

        $this->assertArrayHasKey('deal_type_id', $result['post_data']);

        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(['ident' => $result['post_data']['deal_type']]);

        $this->assertEquals($dealType->getId(), $result['post_data']['deal_type_id']);

        $this->assertArrayHasKey('amount', $result['post_data']);
        $this->assertEquals('10000', $result['post_data']['amount']);

        $this->assertArrayHasKey('counteragent_email', $result['post_data']);
        $this->assertEquals('test2@simple-technology.ru', $result['post_data']['counteragent_email']);

        $this->assertArrayHasKey('counteragent_civil_law_subject', $result['post_data']);
        $this->assertEquals($nonexistent_civil_law_subject_id, $result['post_data']['counteragent_civil_law_subject']);

        $this->assertArrayHasKey('is_valid', $result['validation']);
        $this->assertFalse($result['validation']['is_valid']);
        $this->assertNull($result['validation']['agent_email']);
        $this->assertNull($result['validation']['deal_type']);
        $this->assertNull($result['validation']['amount']);
        $this->assertNull($result['validation']['counteragent_email']);
        $this->assertNotNull($result['validation']['counteragent_civil_law_subject']);
        $this->assertInternalType('array', $result['validation']['counteragent_civil_law_subject']);
        $this->assertArrayHasKey('notInArray', $result['validation']['counteragent_civil_law_subject']);
        $this->assertEquals('The input was not found in the haystack', $result['validation']['counteragent_civil_law_subject']['notInArray']);
    }
}