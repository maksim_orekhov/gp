<?php

namespace ApplicationTest\Listener;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\User;
use Application\EventManager\DealEventProvider as DealEvent;
use Application\Listener\DealListenerAggregate;
use Application\Provider\Token\DealInvitationTokenProvider;
use ApplicationTest\Bootstrap;
use Core\Provider\Token\EncodedTokenProviderInterface;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use \PHPUnit\Framework\TestCase;
use Zend\Mvc\MvcEvent;

/**
 * Class DealListenerAggregateTest
 * @package ApplicationTest\Listener
 */
class DealListenerAggregateTest extends TestCase
{
    /**
     * @var DealListenerAggregate
     */
    private $dealListenerAggregate;

    /**
     * @var EncodedTokenProviderInterface
     */
    private $dealInvitationTokenProvider;


    public function setUp()
    {
        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();

        $this->dealInvitationTokenProvider = $this->prophesize(DealInvitationTokenProvider::class);
        $this->dealListenerAggregate = new DealListenerAggregate($this->dealInvitationTokenProvider->reveal(), $config);
    }

    public function tearDown()
    {
        parent::tearDown();
    }


    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group listener
     * @group deal-listener
     */
    public function testAttach()
    {
        /** @var EventManagerInterface $events */
        $events = $this->prophesize(EventManagerInterface::class);

        $events->attach(
            DealEvent::EVENT_SUCCESS_CREATING,
            [$this->dealListenerAggregate, 'successCreating'],
            9999
        )->willReturn([$this->dealListenerAggregate, 'successCreating'])
            ->shouldBeCalled();

        $this->dealListenerAggregate->attach($events->reveal(), 9999);
    }

//    /**
//     * @throws \Exception
//     * @TODO Доделать?
//     */
//    public function testSuccessCreating()
//    {
//        $deal = $this->prophesize(Deal::class);
//        $customer = $this->prophesize(DealAgent::class);
//        $customer->getUser()->willReturn(new User())->shouldBeCalled();
//        $contractor = $this->prophesize(DealAgent::class);
//        $contractor->getUser()->willReturn(new User())->shouldBeCalled();
//        $deal->getCustomer()->willReturn([$customer->reveal(), 'getId'])->shouldBeCalled();
//        $deal->getContractor()->willReturn([$contractor->reveal(), 'getId'])->shouldBeCalled();
//        $deal->getOwner()->willReturn([$customer->reveal(), 'getId'])->shouldBeCalled();
//
//        /** @var MvcEvent $event */
//        $event = new MvcEvent;
//        $event->setParam('deal', $deal->reveal());
//
//        #$this->dealListenerAggregate->successCreating($event);
//
//        /** @var EventManagerInterface $events */
//        $events = $this->prophesize(EventManagerInterface::class);
//
//        $events->attach(
//            DealEvent::EVENT_SUCCESS_CREATING,
//            [$this->dealListenerAggregate, 'successCreating'],
//            9999
//        )->willReturn([$this->dealListenerAggregate->successCreating($event), 'notifyNewUserAboutInvitationToDealByMai'])
//            ->shouldBeCalled();
//    }
}