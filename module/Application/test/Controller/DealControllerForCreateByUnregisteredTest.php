<?php

namespace ApplicationTest\Controller;

use Application\Controller\DealController;
use Application\Entity\DealAgent;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Entity\Payment;
use Application\Entity\User;
use Application\Form\DealUnregisteredForm;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\UserManager;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseRoleManager;
use Core\Service\ORMDoctrineUtil;
use CoreTest\ViewVarsTrait;
use ModuleFileManager\Service\FileManager;
use Zend\Session\SessionManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use Application\Entity\Deal;
use Application\Service\BankClient\BankClientManager;

/**
 * Class DealControllerForCreateByUnregisteredTest
 * @package ApplicationTest\Controller
 */
class DealControllerForCreateByUnregisteredTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const MINIMUM_DELIVERY_PERIOD = 14;
    /**
     * @var string
     */
    private $user_login;
    /**
     * @var string
     */
    private $user_password;
    /**
     * @var \Application\Entity\User
     */
    private $user;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @var BankClientManager
     */
    public $bankClientManager;
    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;
    /**
     * @var string
     */
    private $deal_upload_folder;
    /**
     * @var FileManager
     */
    private $fileManager;

    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];
        $this->deal_upload_folder = './'.$config['file-management']['main_upload_folder'].'/deal-files';

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->fileManager = $serviceManager->get(FileManager::class);

        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;
        $this->userManager = null;
        $this->baseRoleManager = null;
        $this->civilLawSubjectManager = null;
        $this->fileManager = null;

        gc_collect_cycles();
    }


    /**
     * Тест CreateUnregisteredForm для авторизованного пользователя.
     * Должен сработать редирект на основной экшен создания сделки

     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateUnregisteredForAuthenticatedUser()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $this->dispatch('/deal/create/unregistered-form', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/deal/create');
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * Доступно. Наличие формы dealForm и form_action
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateUnregisteredForNonAuthenticatedUser()
    {
        $this->dispatch('/deal/create/unregistered-form', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealUnregisteredForm::class, $view_vars['dealForm']);
        $this->assertArrayHasKey('form_action', $view_vars);
        /** @var DealUnregisteredForm $form */
        $form = $view_vars['dealForm'];
        $this->assertEquals($form->getAttribute('action'), $view_vars['form_action']);
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * Доступно. Наличие формы dealForm и form_action
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateUnregisteredForNonAuthenticatedUserForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Deal unregistered form', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('form_action', $data['data']);
        $this->assertEquals('/deal/create/unregistered-form', $data['data']['form_action']);
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с валидными POST данными, включающая deal_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateUnregisteredWithValidPostData()
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $amount = 1000500;
        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-1',
            'amount'               => $amount,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertRedirectTo('/deal/show/'.$deal->getId());
        $this->assertEquals($postData['amount'], $deal->getPayment()->getDealValue());

        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertSame($deal->getCustomer(), $deal->getOwner());
        $this->assertEquals($agent_email, $deal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $deal->getContractor()->getEmail());
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с валидными POST данными, включающая deal_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateUnregisteredWithValidPostDataForAjax()
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $amount = 1000500;
        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-1',
            'amount'               => $amount,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($deal->getId(), $data['data']['deal']['id']);
        $this->assertEquals($amount, $data['data']['deal']['amount_without_fee']);

        $this->assertEquals($amount, $payment->getDealValue());
        $this->assertSame($deal->getCustomer(), $deal->getOwner());
        $this->assertEquals($agent_email, $deal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $deal->getContractor()->getEmail());
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с валидными POST данными и File data
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateUnregisteredWithValidPostDataAndFilesData()
    {
        // Переорпеделяем is_uploaded_file
        $this->disablePhpUploadCapabilities();
        $file_name = 'image_file_for_test.png';
        $file_name_2 = 'image_file_2_for_test.png';

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $amount = 1000500;
        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-1',
            'amount'               => $amount,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        // Формируем POST даные
        $this->getRequest()
            ->setMethod('POST')
            ->setPost(new \Zend\Stdlib\Parameters($postData));
        $files = new \Zend\Stdlib\Parameters([
            'files' => [
                [
                    'name' => $file_name,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name,
                    'size' => filesize('./module/Application/test/files/' .$file_name),
                    'error' => 0
                ],
                [
                    'name' => $file_name_2,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name_2,
                    'size' => filesize('./module/Application/test/files/' .$file_name_2),
                    'error' => 0
                ],
            ]
        ]);
        $this->getRequest()->setFiles($files);

        $this->dispatch('/deal/create/unregistered-form', 'POST');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertRedirectTo('/deal/show/'.$deal->getId());
        $this->assertEquals($postData['amount'], $deal->getPayment()->getDealValue());

        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertSame($deal->getCustomer(), $deal->getOwner());
        $this->assertEquals($agent_email, $deal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $deal->getContractor()->getEmail());

        // Проверяем и удаляем полученный файл
        $dealFiles = $deal->getDealFiles();

        $file = $this->fileManager->getFileByOriginName($file_name);
        $this->assertEquals($file_name, $file->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file->getName());
        $this->assertContains($file, $dealFiles);

        $file2 = $this->fileManager->getFileByOriginName($file_name_2);
        $this->assertEquals($file_name_2, $file2->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file2->getName());
        $this->assertContains($file2, $dealFiles);

        unlink($this->deal_upload_folder.'/'.$file->getName());
        unlink($this->deal_upload_folder.'/'.$file2->getName());
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с валидными POST данными и File data
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateUnregisteredWithValidPostDataAndFilesDataForAjax()
    {
        // Переорпеделяем is_uploaded_file
        $this->disablePhpUploadCapabilities();
        $file_name = 'image_file_for_test.png';
        $file_name_2 = 'image_file_2_for_test.png';

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $amount = 1000500;
        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-1',
            'amount'               => $amount,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue()
        ];

        // Формируем POST даные
        $this->getRequest()
            ->setMethod('POST')
            ->setPost(new \Zend\Stdlib\Parameters($postData));
        $files = new \Zend\Stdlib\Parameters([
            'files' => [
                [
                    'name' => $file_name,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name,
                    'size' => filesize('./module/Application/test/files/' .$file_name),
                    'error' => 0
                ],
                [
                    'name' => $file_name_2,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name_2,
                    'size' => filesize('./module/Application/test/files/' .$file_name_2),
                    'error' => 0
                ],
            ]
        ]);
        $this->getRequest()->setFiles($files);

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($deal->getId(), $data['data']['deal']['id']);
        $this->assertEquals($amount, $data['data']['deal']['amount_without_fee']);

        $this->assertEquals($amount, $payment->getDealValue());
        $this->assertSame($deal->getCustomer(), $deal->getOwner());
        $this->assertEquals($agent_email, $deal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $deal->getContractor()->getEmail());

        // Проверяем и удаляем полученный файл
        $dealFiles = $deal->getDealFiles();

        $file = $this->fileManager->getFileByOriginName($file_name);
        $this->assertEquals($file_name, $file->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file->getName());
        $this->assertContains($file, $dealFiles);

        $file2 = $this->fileManager->getFileByOriginName($file_name_2);
        $this->assertEquals($file_name_2, $file2->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file2->getName());
        $this->assertContains($file2, $dealFiles);

        unlink($this->deal_upload_folder.'/'.$file->getName());
        unlink($this->deal_upload_folder.'/'.$file2->getName());
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами Amount
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataAmount
     */
    public function testCreateUnregisteredWithPostDataWithInvalidAmount($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-1',
            'amount'               => $given,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealUnregisteredForm::class, $view_vars['dealForm']);
        /** @var DealUnregisteredForm $form */
        $form = $view_vars['dealForm'];

        $this->assertArrayHasKey($expected['validation_key'], $form->getMessages());
        $this->assertArrayHasKey($expected['validation_reasons'], $form->getMessages()[$expected['validation_key']]);
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами Amount
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataAmount
     */
    public function testCreateUnregisteredWithPostDataWithInvalidAmountForAjax($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-1',
            'amount'               => $given,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey($expected['validation_key'], $data['data']);
        $this->assertArrayHasKey($expected['validation_reasons'], $data['data'][$expected['validation_key']]);
    }

    /**
     * @return array
     */
    public function dataAmount(): array
    {
        return [
            [ null, ['validation_key' => 'amount', 'validation_reasons' => 'isEmpty'] ],
            [ 0,    ['validation_key' => 'amount', 'validation_reasons' => 'regexNotMatch'] ],
        ];
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами Amount
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataName
     */
    public function testCreateUnregisteredWithPostDataWithInvalidName($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => $given,
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealUnregisteredForm::class, $view_vars['dealForm']);
        /** @var DealUnregisteredForm $form */
        $form = $view_vars['dealForm'];

        $this->assertArrayHasKey($expected['validation_key'], $form->getMessages());
        $this->assertArrayHasKey($expected['validation_reasons'], $form->getMessages()[$expected['validation_key']]);
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами Amount
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataName
     */
    public function testCreateUnregisteredWithPostDataWithInvalidNameForAjax($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => $given,
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey($expected['validation_key'], $data['data']);
        $this->assertArrayHasKey($expected['validation_reasons'], $data['data'][$expected['validation_key']]);
    }

    /**
     * @return array
     */
    public function dataName(): array
    {
        return [
            [ null, ['validation_key' => 'name', 'validation_reasons' => 'isEmpty'] ],
            [ '   ', ['validation_key' => 'name', 'validation_reasons' => 'isEmpty'] ],
            [ 4, ['validation_key' => 'name', 'validation_reasons' => 'stringLengthTooShort'] ],
        ];
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами deal_role
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataDealRole
     */
    public function testCreateUnregisteredWithPostDataWithInvalidRole($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Role',
            'amount'               => 505000,
            'deal_role'            => $given,
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealUnregisteredForm::class, $view_vars['dealForm']);
        /** @var DealUnregisteredForm $form */
        $form = $view_vars['dealForm'];

        $this->assertArrayHasKey($expected['validation_key'], $form->getMessages());
        $this->assertArrayHasKey($expected['validation_reasons'], $form->getMessages()[$expected['validation_key']]);
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами deal_role
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataDealRole
     */
    public function testCreateUnregisteredWithPostDataWithDealRoleForAjax($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Role',
            'amount'               => 505000,
            'deal_role'            => $given,
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey($expected['validation_key'], $data['data']);
        $this->assertArrayHasKey($expected['validation_reasons'], $data['data'][$expected['validation_key']]);
    }

    /**
     * @return array
     */
    public function dataDealRole(): array
    {
        return [
            [ null, ['validation_key' => 'deal_role', 'validation_reasons' => 'isEmpty'] ],
            [ '   ', ['validation_key' => 'deal_role', 'validation_reasons' => 'isEmpty'] ],
            [ 'fgdss', ['validation_key' => 'deal_role', 'validation_reasons' => 'stringLengthTooShort'] ],
        ];
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами agent_email
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataAgentEmail
     */
    public function testCreateUnregisteredWithPostDataWithInvalidAgentEmail($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        #$agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Agent Email',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $given,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealUnregisteredForm::class, $view_vars['dealForm']);
        /** @var DealUnregisteredForm $form */
        $form = $view_vars['dealForm'];

        $this->assertArrayHasKey($expected['validation_key'], $form->getMessages());
        $this->assertArrayHasKey($expected['validation_reasons'], $form->getMessages()[$expected['validation_key']]);
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами agent_email
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataAgentEmail
     */
    public function testCreateUnregisteredWithPostDataWithInvalidAgentEmailForAjax($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        #$agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Agent Email',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $given,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey($expected['validation_key'], $data['data']);
        $this->assertArrayHasKey($expected['validation_reasons'], $data['data'][$expected['validation_key']]);
    }

    /**
     * @return array
     */
    public function dataAgentEmail(): array
    {
        return [
            [ null, ['validation_key' => 'agent_email', 'validation_reasons' => 'isEmpty'] ],
            [ '   ', ['validation_key' => 'agent_email', 'validation_reasons' => 'isEmpty'] ],
            [ 'fgdss', ['validation_key' => 'agent_email', 'validation_reasons' => 'emailAddressInvalidFormat'] ],
        ];
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами counteragent_email
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataCounteragentEmail
     */
    public function testCreateUnregisteredWithPostDataWithInvalidCounteragentEmail($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        #$counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Counteragent Email',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $given,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealUnregisteredForm::class, $view_vars['dealForm']);
        /** @var DealUnregisteredForm $form */
        $form = $view_vars['dealForm'];

        $this->assertArrayHasKey($expected['validation_key'], $form->getMessages());
        $this->assertArrayHasKey($expected['validation_reasons'], $form->getMessages()[$expected['validation_key']]);
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами counteragent_email
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataCounteragentEmail
     */
    public function testCreateUnregisteredWithPostDataWithInvalidCounteragentEmailForAjax($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        #$counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Counteragent Email',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $given,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey($expected['validation_key'], $data['data']);
        $this->assertArrayHasKey($expected['validation_reasons'], $data['data'][$expected['validation_key']]);
    }

    /**
     * @return array
     */
    public function dataCounteragentEmail(): array
    {
        return [
            [ null, ['validation_key' => 'counteragent_email', 'validation_reasons' => 'isEmpty'] ],
            [ '   ', ['validation_key' => 'counteragent_email', 'validation_reasons' => 'isEmpty'] ],
            [ 'fgdss', ['validation_key' => 'counteragent_email', 'validation_reasons' => 'emailAddressInvalidFormat'] ],
        ];
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами deal_type
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataDealType
     */
    public function testCreateUnregisteredWithPostDataWithInvalidDealType($given, $expected)
    {
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Deal Type',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $given,
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealUnregisteredForm::class, $view_vars['dealForm']);
        /** @var DealUnregisteredForm $form */
        $form = $view_vars['dealForm'];

        $this->assertArrayHasKey($expected['validation_key'], $form->getMessages());
        $this->assertArrayHasKey($expected['validation_reasons'], $form->getMessages()[$expected['validation_key']]);
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами counteragent_email
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataDealType
     */
    public function testCreateUnregisteredWithPostDataWithInvalidDealTypeForAjax($given, $expected)
    {
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Deal Type',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $given,
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey($expected['validation_key'], $data['data']);
        $this->assertArrayHasKey($expected['validation_reasons'], $data['data'][$expected['validation_key']]);
    }

    /**
     * @return array
     */
    public function dataDealType(): array
    {
        return [
            [ null, ['validation_key' => 'deal_type', 'validation_reasons' => 'isEmpty'] ],
            [ '   ', ['validation_key' => 'deal_type', 'validation_reasons' => 'isEmpty'] ],
            [ 'fgdss', ['validation_key' => 'deal_type', 'validation_reasons' => 'notDigits'] ],
            [ 99999999, ['validation_key' => 'deal_type', 'validation_reasons' => 'notInArray'] ],
        ];
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами fee_payer_option
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataFeePayerOption
     */
    public function testCreateUnregisteredWithPostDataWithInvalidFeePayerOption($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Fee Payer Option',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $given,
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealUnregisteredForm::class, $view_vars['dealForm']);
        /** @var DealUnregisteredForm $form */
        $form = $view_vars['dealForm'];

        $this->assertArrayHasKey($expected['validation_key'], $form->getMessages());
        $this->assertArrayHasKey($expected['validation_reasons'], $form->getMessages()[$expected['validation_key']]);
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами fee_payer_option
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataFeePayerOption
     */
    public function testCreateUnregisteredWithPostDataWithInvalidFeePayerOptionForAjax($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Fee Payer Option',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $given,
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey($expected['validation_key'], $data['data']);
        $this->assertArrayHasKey($expected['validation_reasons'], $data['data'][$expected['validation_key']]);
    }

    /**
     * @return array
     */
    public function dataFeePayerOption(): array
    {
        return [
            [ null, ['validation_key' => 'fee_payer_option', 'validation_reasons' => 'isEmpty'] ],
            [ '   ', ['validation_key' => 'fee_payer_option', 'validation_reasons' => 'isEmpty'] ],
            [ 'fgdss', ['validation_key' => 'fee_payer_option', 'validation_reasons' => 'notDigits'] ],
            [ 99999999, ['validation_key' => 'fee_payer_option', 'validation_reasons' => 'notInArray'] ],
        ];
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами delivery_period
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataDeliveryPeriod
     */
    public function testCreateUnregisteredWithPostDataWithInvalidDeliveryPeriod($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Delivery Period',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => $given,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealUnregisteredForm::class, $view_vars['dealForm']);
        /** @var DealUnregisteredForm $form */
        $form = $view_vars['dealForm'];

        $this->assertArrayHasKey($expected['validation_key'], $form->getMessages());
        $this->assertArrayHasKey($expected['validation_reasons'], $form->getMessages()[$expected['validation_key']]);
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами delivery_period
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataDeliveryPeriod
     */
    public function testCreateUnregisteredWithPostDataWithInvalidDeliveryPeriodForAjax($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Delivery Period',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => $given,
            'csrf'                 => $csrfElement->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey($expected['validation_key'], $data['data']);
        $this->assertArrayHasKey($expected['validation_reasons'], $data['data'][$expected['validation_key']]);
    }

    /**
     * @return array
     */
    public function dataDeliveryPeriod(): array
    {
        return [
            [ null, ['validation_key' => 'delivery_period', 'validation_reasons' => 'isEmpty'] ],
            [ '   ', ['validation_key' => 'delivery_period', 'validation_reasons' => 'isEmpty'] ],
            [ 0, ['validation_key' => 'delivery_period', 'validation_reasons' => 'integerTooSmall'] ]
        ];
    }

    /**
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами csrf
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataCsrf
     */
    public function testCreateUnregisteredWithPostDataWithInvalidCsrf($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Delivery Period',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $given,
        ];

        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealUnregisteredForm::class, $view_vars['dealForm']);
        /** @var DealUnregisteredForm $form */
        $form = $view_vars['dealForm'];

        $this->assertArrayHasKey($expected['validation_key'], $form->getMessages());
        $this->assertArrayHasKey($expected['validation_reasons'], $form->getMessages()[$expected['validation_key']]);
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с невалидными вариантами csrf
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataCsrf
     */
    public function testCreateUnregisteredWithPostDataWithInvalidCsrfForAjax($given, $expected)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'With Invalid Delivery Period',
            'amount'               => 505000,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $given,
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey($expected['validation_key'], $data['data']);
        $this->assertArrayHasKey($expected['validation_reasons'], $data['data'][$expected['validation_key']]);
    }

    /**
     * @return array
     */
    public function dataCsrf(): array
    {
        return [
            [ null, ['validation_key' => 'csrf', 'validation_reasons' => 'isEmpty'] ],
            [ '   ', ['validation_key' => 'csrf', 'validation_reasons' => 'isEmpty'] ],
            [ 'vcdvdvdvv56767grfgfg55454545', ['validation_key' => 'csrf', 'validation_reasons' => 'notSame'] ]
        ];
    }

    /**
     * Ajax
     * Тест CreateUnregisteredForm для НЕавторизованного пользователя.
     * с валидными POST данными и авторегистрацией
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateUnregisteredWithAutoRegistrationUserForAjax()
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');

        $amount = 1000500;
        $agent_email = 'agent@test.net';
        $counteragent_email = 'counteragent@test.net';

        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (testCreateUnregisteredWithAutoRegistrationUserForAjax)',
            'amount'               => $amount,
            'deal_role'            => 'customer',
            'agent_email'          => $agent_email,
            'counteragent_email'   => $counteragent_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create/unregistered-form', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-unregistered-form');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);

        $this->assertNotNull($deal);
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        $this->assertNotNull($customer);
        $this->assertNotNull($contractor);

        $this->assertSame($counteragent_email, $contractor->getEmail());
        $this->assertSame($agent_email, $customer->getEmail());

        $newContractorUsers = $this->entityManager->getRepository(User::class)
            ->findAllUnverifiedUserByEmail($counteragent_email);
        $newCustomerUsers = $this->entityManager->getRepository(User::class)
            ->findAllUnverifiedUserByEmail($agent_email);
        $this->assertCount(1, $newContractorUsers);
        $this->assertCount(1, $newCustomerUsers);
        /** @var User $newContractorUser */
        /** @var User $newCustomerUser */
        $newContractorUser = $newContractorUsers[0];
        $newCustomerUser = $newCustomerUsers[0];
        $this->assertNotNull($newContractorUser);
        $this->assertNotNull($newCustomerUser);
        //user к сделке еше не прикреплен
        $this->assertNull($customer->getUser());
        $this->assertSame($newCustomerUser->getEmail()->getEmail(), $customer->getEmail());

        $this->assertNull($contractor->getUser());
        $this->assertSame($newContractorUser->getEmail()->getEmail(), $contractor->getEmail());
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        $login = $login ?? $this->user_login;
        $password = $password ?? $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}