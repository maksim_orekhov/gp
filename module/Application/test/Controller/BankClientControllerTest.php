<?php

namespace ApplicationTest\Controller;

use Application\Controller\BankClientController;
use Application\Controller\DealController;
use Application\Form\FileUploadForm;
use ApplicationTest\Bootstrap;
use Core\Service\Base\BaseRoleManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\User;
use Application\Entity\Deal;
use Application\Entity\DealType;
use ModuleFileManager\Entity\File;
use Application\Entity\FeePayerOption;
use Application\Service\Parser\BankClientParser;
use Zend\ServiceManager\ServiceManager;
use Application\Entity\Payment;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealAgentManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Payment\PaymentMethodManager;
use Application\Listener\CreditPaymentOrderAvailabilityListener;
use CoreTest\ViewVarsTrait;

/**
 * Class BankClientControllerTest
 * @package ApplicationTest\Controller
 *
 * Важно! На момент запуска этих тестов, папка bank-client-stock не должна содержать файлы.
 * Тесты сами помещают в нее необходимые тестовые файлы и после удаляеют их.
 */
class BankClientControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var string
     */
    public $main_upload_folder;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\BankClient\BankClientManager
     */
    public $bankClientManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \ModuleFileManager\Service\FileManager
     */
    private $fileManager;

    /**
     * @var BankClientParser
     */
    private $bankClientParser;

    /**
     * @var \Application\Service\Deal\DealManager
     */
    private $dealManager;

    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var CreditPaymentOrderAvailabilityListener
     */
    private $creditPaymentOrderAvailabilityListener;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;


    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    /**
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->bankClientManager = $serviceManager->get(\Application\Service\BankClient\BankClientManager::class);
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->fileManager = $serviceManager->get(\ModuleFileManager\Service\FileManager::class);
        $this->bankClientParser = $serviceManager->get(BankClientParser::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $this->dealManager = $serviceManager->get(\Application\Service\Deal\DealManager::class);

        $config = $this->getApplicationConfig();

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->dealAgentManager = $serviceManager->get(DealAgentManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->paymentManager = $serviceManager->get(PaymentManager::class);
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);
        $this->creditPaymentOrderAvailabilityListener = $serviceManager->get(CreditPaymentOrderAvailabilityListener::class);

        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        #$this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown() {
        // Transaction rollback
        #$this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Доступность /bank-client пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testIndexActionCanBeAccessed()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->dispatch('/bank-client', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client');
        #$this->assertQuery('.container');
    }

    /**
     * Недоступность /bank-client не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/bank-client', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/login?redirectUrl=/bank-client');
    }

    /**
     * Недоступность /bank-client авторизованному пользователю без роли Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessed()
    {
        $this->assignRoleToTestUser('Verified');
        $this->login();

        $this->dispatch('/bank-client', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Доступность /bank-client/file-upload пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testFileUploadActionCanBeAccessed()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/bank-client/file-upload', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/file-upload');
        // Что отдается в шаблон
        $this->assertArrayHasKey('fileUploadForm', $view_vars);
        $this->assertInstanceOf(FileUploadForm::class, $view_vars['fileUploadForm']);
        // Возвращается из шаблона
        $this->assertQuery('#bank-client-file-upload');
    }

    /**
     * Недоступность /bank-client не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testFileUploadActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/bank-client/file-upload', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/login?redirectUrl=/bank-client/file-upload');
    }

    /**
     * Недоступность /bank-client авторизованному пользователю без роли Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testFileUploadActionCanNotBeAccessed()
    {
        $this->assignRoleToTestUser('Verified');
        $this->login();

        $this->dispatch('/bank-client/file-upload', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Тест выгрузки файла
     * Кодировка файла должно быть windows-1251
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testFileUploadWithValidPostData()
    {
        // Переорпеделяем is_uploaded_file
        $this->disablePhpUploadCapabilities();

        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $file_name = 'test_bank_file_with_one_order.txt';
        $upload_folder = "./".$this->main_upload_folder.BankClientController::FILE_STORING_FOLDER;

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Формируем POST даные
        $this->getRequest()
            ->setMethod('POST')
            ->setPost(new \Zend\Stdlib\Parameters(array('csrf' => $csrf)));
        $file = new \Zend\Stdlib\Parameters([
            'file' => [
                'name' => $file_name,
                'type' => 'text/plain',
                'tmp_name' => "./module/Application/test/files/".$file_name,
                'size' => filesize("./module/Application/test/files/".$file_name),
                'error' => 0
            ]
        ]);
        $this->getRequest()->setFiles($file);

        $this->dispatch('/bank-client/file-upload', 'POST');

        $file = $this->fileManager->getFileByOriginName($file_name);
        $this->assertEquals($file_name, $file->getOriginName());
        $this->assertFileExists($upload_folder."/".$file->getName());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/file-upload');
        $this->assertRedirectTo('/bank-client');

        // Удаляем полученный файл
        $this->removeFile($upload_folder.'/'.$file->getName());
    }

    /**
     * Доступность /bank-client/file-stock-upload пользователю с ролью Operator
     * Важно! В папке bank-client-stock не должно быть файлов!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testFromStockUploadActionCanBeAccessed()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/bank-client/file-stock-upload', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/file-stock-upload');
    }

    /**
     * Тест загрузки файлов из "склада".
     * Два разных файла, две одинаковые платежки. Должна добавиться 1 платежка.
     * Кодировка файлов должно быть windows-1251.
     * Важно! В папке bank-client-stock не должно быть файлов!
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testFromStockUploadWithValidData()
    {
        // Only user with Operator role can execute upload action
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // Set test deal meet the conditions "Expected payment"
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => 'Сделка Фикстура'));
        // Set Customer confirm
        $deal->getCustomer()->setDealAgentConfirm(true);
        $this->entityManager->flush();

        // File preparation
        $file_name_1 = "payment_order_from_fixture.txt";
        $file_name_2 = "payment_order_from_fixture_repeat.txt";
        // Pathways to files
        $path_1 = "./module/Application/test/files/".$file_name_1;
        $path_2 = "./module/Application/test/files/".$file_name_2;
        // Stock folder
        $stock_source_folder = "./".$this->main_upload_folder.BankClientController::FILE_SOURCE_FOLDER;
        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.BankClientController::FILE_STORING_FOLDER;
        // Copy files to upload/bank-client-stock
        $this->saveFile($path_1, $stock_source_folder."/".$file_name_1);
        $this->saveFile($path_2, $stock_source_folder."/".$file_name_2);
        // Check the files were copied
        $this->assertFileExists($stock_source_folder."/".$file_name_1);
        $this->assertFileExists($stock_source_folder."/".$file_name_2);

        $this->dispatch('/bank-client/file-stock-upload', 'GET');

        $fileOne = $this->fileManager->getFileByOriginName($file_name_1);
        $fileTwo = $this->fileManager->getFileByOriginName($file_name_2);
        // Проверяем, что информация о файлах добавились в базу
        $this->assertEquals($file_name_1, $fileOne->getOriginName());
        $this->assertEquals($file_name_2, $fileTwo->getOriginName());
        // Проверяем, что сами файлы сохранились в нужной папке
        $this->assertFileExists($upload_folder."/".$fileOne->getName());
        $this->assertFileExists($upload_folder."/".$fileTwo->getName());
        // Проверяем, что парсинг отработал
        $this->assertTrue($this->bankClientManager->isFileProcessed($fileOne->getName()));
        $this->assertTrue($this->bankClientManager->isFileProcessed($fileTwo->getName()));
        // Проверка отсутствия повторов одной и той же платежки
        // В тестовых файлах содержатся платежка Номер=$deal->getId() (2 раза)
        $bankClientOrders = $this->entityManager->getRepository(BankClientPaymentOrder::class)
            ->findBy(array('documentNumber' => $deal->getId()));

        // @TODO не проходит. разобраться! В системе нет сделок, готовых к приему платёжек
        #$this->assertCount(1, $bankClientOrders);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/file-stock-upload');

        // Удаляем исходные и созданные файлы из обеих директорий
        $this->removeFile($stock_source_folder."/".$file_name_1);
        $this->removeFile($stock_source_folder."/".$file_name_2);
        $this->removeFile($upload_folder.'/'.$fileOne->getName());
        $this->removeFile($upload_folder.'/'.$fileTwo->getName());
    }

    /**
     * Недоступность /bank-client не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testFromStockUploadActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/bank-client/file-stock-upload', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/login?redirectUrl=/bank-client/file-stock-upload');
    }

    /**
     * Недоступность /bank-client авторизованному пользователю без роли Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     */
    public function testFromStockUploadActionCanNotBeAccessed()
    {
        $this->assignRoleToTestUser('Verified');
        $this->login();

        $this->dispatch('/bank-client/file-stock-upload', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Доступность /bank-client/process пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testPaymentOrderFileProcessingActionCanBeAccessed()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/bank-client/process', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/process');
    }

    /**
     * Недоступность /bank-client не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testPaymentOrderFileProcessingActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/bank-client/process', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/login?redirectUrl=/bank-client/process');
    }

    /**
     * Недоступность /bank-client авторизованному пользователю без роли Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testPaymentOrderFileProcessingActionCanNotBeAccessed()
    {
        $this->assignRoleToTestUser('Verified');
        $this->login();

        $this->dispatch('/bank-client/process', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Доступность /bank-client/show-payment-order пользователю с ролью Operator
     * Проверяем отдачу PDF по косвенному признаку - наличию строки "%PDF-1.7" в getActualOutput
     * В шаблоны добавлен файл show-payment-order.phtml, без него dispatch выдает ошибку 500.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testShowPaymentOrderAction()
    {
        /** @var Deal $deal */ // Set test deal meet the conditions "Expected payment"
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => 'Сделка Фикстура'));
        // Set Customer confirm
        $deal->getCustomer()->setDealAgentConfirm(true);
        $deal->getContractor()->setDealAgentConfirm(true);
        $this->entityManager->flush();
        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($deal);

        // Path
        $path = "./module/Application/test/files/payment_order_from_fixture.txt";
        // Get data as array
        $payment_order_data = $this->bankClientParser->execute($path);

        // Create PaymentOrder
        $paymentOrder = $this->bankClientManager->createNewPaymentOrderFromArray(
            $payment_order_data[0]
        );

        // Only user with Operator role can execute upload action
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->setOutputCallback(function(){
            $output = $this->getActualOutput();
            $this->assertContains("%PDF-1.7", $output );
        });
        $this->dispatch('/bank-client/show-payment-order/'.$paymentOrder->getId());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/show-payment-order');

        /** @var File $file */
        $file = $deal->getDealContractFile()->getFile();

        $upload_folder = "./".$this->main_upload_folder.DealController::DEAL_FILES_STORING_FOLDER;
        $this->removeFile($upload_folder.'/'.$file->getName());
    }

    /**
     * Недоступность /bank-client не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testShowPaymentOrderActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/bank-client/show-payment-order/1', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/login?redirectUrl=/bank-client/show-payment-order/1');
    }

    /**
     * Недоступность /bank-client авторизованному пользователю без роли Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testShowPaymentOrderActionCanNotBeAccessed()
    {
        $this->assignRoleToTestUser('Verified');
        $this->login();

        $this->dispatch('/bank-client/show-payment-order/1', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Доступность /bank-client/credit-unpaid пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testCreditUnpaidActionCanBeAccessedByOperator()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->dispatch('/bank-client/credit-unpaid', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/credit-unpaid');
    }

    /**
     * Недоступность /bank-client/credit-unpaid не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testCreditUnpaidActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/bank-client/credit-unpaid', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/login?redirectUrl=/bank-client/credit-unpaid');
    }

    /**
     * Недоступность /bank-client/credit-unpaid авторизованному пользователю без роли Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testCreditUnpaidActionCanNotBeAccessed()
    {
        $this->assignRoleToTestUser('Verified');
        $this->login();

        $this->dispatch('/bank-client/credit-unpaid', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Доступность /bank-client/credit-unpaid/generate пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testGenerateCreditUnpaidFileActionCanBeAccessedByOperator()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->dispatch('/bank-client/credit-unpaid/generate', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/credit-unpaid/generate');
    }

    /**
     * Недоступность /bank-client/credit-unpaid не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testGenerateCreditUnpaidFileActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/bank-client/credit-unpaid/generate', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/login?redirectUrl=/bank-client/credit-unpaid/generate');
    }

    /**
     * Недоступность /bank-client/credit-unpaid авторизованному пользователю без роли Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testGenerateCreditUnpaidFileActionCanNotBeAccessed()
    {
        $this->assignRoleToTestUser('Verified');
        $this->login();

        $this->dispatch('/bank-client/credit-unpaid/generate', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Создание File и генерация файла txt платёжного поручения из данных сделки.
     * Требует наличия в системе объектов deal, payment, bankClientPaymentOrder, dealAgent, paymentMethod и paymentMethodBankTransfer
     * Успех - отдача файла на загрузку. Как это проверить?
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testGenerateCreditUnpaidFileAction()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $deal_name = 'Сделка Доставка подтверждена';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->creditPaymentOrderAvailabilityListener->subscribe();
        $this->creditPaymentOrderAvailabilityListener->trigger($deal);

        $this->dispatch('/bank-client/credit-unpaid/generate', 'GET');
        #$this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/credit-unpaid/generate');

        // Получаем последний добавленный файл
        $file = $this->entityManager->getRepository('ModuleFileManager\Entity\File')
            ->findOneBy(['path' => '/bank-client'], ['id' => 'DESC']);

        /** @var BankClientPaymentOrder $creditPaymentOrder */
        $creditPaymentOrder = $payment->getPaymentOrders()->first();

        $file_name = $creditPaymentOrder->getBankClientPaymentOrderFile()->getFileName();

        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.BankClientController::FILE_STORING_FOLDER;
        // Проверяем, что сами файлы сохранились в нужной папке
        $this->assertFileExists($upload_folder."/".$file_name);

        // @TODO Хорошо бы еще потестить отдачу файла. Как?

        // Удаляем сгенерированный файл
        $this->removeFile($upload_folder."/".$file_name);
    }

    /**
     * Доступность /bank-client/file-download/id пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testFileDownloadActionCanBeAccessedByOperator()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // Добавить сделку, сгенерировать файл
        $deal_name = 'Тестовая сделка';
        $deal_role = 'customer';
        $amount = 300000;
        // Crete Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);

        /** @var Deal $testDeal */
        $testDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Тестовая сделка']);

        $this->assertEquals($deal_name, $testDeal->getName());

        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($testDeal);

        $this->creditPaymentOrderAvailabilityListener->subscribe();
        $this->creditPaymentOrderAvailabilityListener->trigger($testDeal);

        // Get all ready to payment PaymentOrders
        $paymentOrders = $this->bankClientManager->getReadyOutgoingPaymentOrders();

        /** @var File $file */
        $file = $this->bankClientManager->generateBankClientFile($paymentOrders);

        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.BankClientController::FILE_STORING_FOLDER;
        // Проверяем, что сами файлы сохранились в нужной папке
        $this->assertFileExists($upload_folder."/".$file->getName());

        // Получаем последний добавленный файл
        $file = $this->entityManager->getRepository('ModuleFileManager\Entity\File')
            ->findOneBy(['path' => '/bank-client'], ['id' => 'DESC']);

        $this->dispatch('/bank-client/file-download/'.$file->getId(), 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/file-download');

        // @TODO Хорошо бы еще потестить отдачу файла. Как?

        // Удаляем сгенерированный файл. Не удаляется из-за того, что fclose($file); не закрывается
        fclose($file);
        $this->removeFile($upload_folder."/".$file->getName());
    }

    /**
     * Недоступность /bank-client/file-download/1 не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testFileDownloadActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/bank-client/file-download/1', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/login?redirectUrl=/bank-client/file-download/1');
    }

    /**
     * Недоступность /bank-client/file-download/1 авторизованному пользователю без роли Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testFileDownloadActionCanNotBeAccessed()
    {
        $this->assignRoleToTestUser('Verified');
        $this->login();

        $this->dispatch('/bank-client/file-download/1', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(BankClientController::class);
        $this->assertRedirectTo('/access-denied');
    }


    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     * @throws \Exception
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя test
     * @throws \Exception
     */
    private function login()
    {
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);
    }

    /**
     * @param $source_url
     * @param $destination
     */
    private function saveFile($source_url, $destination)
    {
        copy($source_url, $destination);
    }

    /**
     * @param $path
     */
    private function removeFile($path)
    {
        unlink($path);
    }

    /**
     * @param string $deal_name
     * @param string $deal_role
     * @param $amount
     * @return Deal
     * @throws \Exception
     */
    private function createTestDeal(string $deal_name, string $deal_role, $amount)
    {
        // Create CivilLawSubject
        $params = [
            'first_name' => 'Александр',
            'secondary_name' => 'Александрович',
            'last_name' => 'Александров',
            'birth_date' => date('Y-m-d'),
        ];
        $this->civilLawSubjectManager->createNaturalPerson($this->user, $params);

        // Create paymentMethod и paymentMethodBankTransfer
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user);
        $params = [
            'name' => 'Имя',
            'bank_name' => 'ПАО БАНК САНКТ-ПЕТЕРБУРГ',
            'bik' => '111222',
            'account_number' => '22233344455566677788',
            'payment_recipient_name' => 'Guarant Pay',
            'inn' => '666777',
            'kpp' => '777888',
            'corr_account_number' => '30101810900000000790'
        ];
        $bankTransfer = $this->paymentMethodManager->createPaymentMethodBankTransfer($params, $civilLawSubject);
        $paymentMethod = $bankTransfer->getPaymentMethod();

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $data = [
            'name'                      => $deal_name,
            'amount'                    => $amount,
            'deal_role'                 => $deal_role,
            'deal_civil_law_subject'    => $this->user->getCivilLawSubjects()->last()->getId(),
            'payment_method'            => '',
            'counteragent_email'        => 'test2@test.net',
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => 15,
            'deal_agent_confirm'        => 1
        ];

        // Try to register dill
        // Распределение ролей в сделке взависимости от выбранной инициатором роли
        if($data['deal_role'] === 'customer') {
            $userCustomer   = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $userContractor = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $paramCustomer = $data;
            $paramContractor = [];
        } else {
            $userCustomer   = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $userContractor = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $paramCustomer = [];
            $paramContractor = $data;
        }

        $dealAgentCustomer = $this->dealAgentManager->createDealAgent($userCustomer, $paramCustomer);
        $dealAgentContractor = $this->dealAgentManager->createDealAgent($userContractor, $paramContractor);

        $user = $this->userManager->getUserByLogin($this->authService->getIdentity());

        $dealOwner = $this->dealAgentManager->defineDealOwner($user, $dealAgentCustomer, $dealAgentContractor);

        // Create Deal
        $deal = $this->dealManager->createDeal($data, $dealAgentCustomer, $dealAgentContractor, $dealOwner);

        $deal->getContractor()->setDealAgentConfirm(true);
        $deal->getCustomer()->setDealAgentConfirm(true);

        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($deal);
        $payment->setPaymentMethod($paymentMethod);

        $this->entityManager->flush();

        return $deal;
    }

    /**
     * @param string $deal_name
     * @return Deal
     */
    private function getDealForTest(string $deal_name)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        return $deal;
    }
}