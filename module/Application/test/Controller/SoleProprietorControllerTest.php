<?php

namespace ApplicationTest\Controller;

use Application\Controller\SoleProprietorController;
use Application\Entity\CivilLawSubject;
use Application\Entity\NdsType;
use Application\Entity\Role;
use Application\Entity\SoleProprietor;
use Application\Form\SoleProprietorForm;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\MessagePlugin;
use Core\Exception\LogicException;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseRoleManager;
use Core\Service\ORMDoctrineUtil;
use ModuleRbac\Entity\Interfaces\ModuleRbacRoleInterface;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Zend\Form\Element;
use CoreTest\ViewVarsTrait;

/**
 * Class SoleProprietorControllerTest
 * @package ApplicationTest\Controller
 */
class SoleProprietorControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_SOLE_PROPRIETOR_1_NAME = 'ИП Месси Леонель Аргентинович';
    const TEST_SOLE_PROPRIETOR_2_NAME = 'ИП Роналду Криштиану Португалович';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;
    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// getList

    // Неавторизованный

    /**
     * Недоступность /profile/sole-proprietor (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/sole-proprietor', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');
        $this->assertRedirectTo('/login?redirectUrl=/profile/sole-proprietor');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/sole-proprietor (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/sole-proprietor', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /profile/sole-proprietor (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @dataProvider providerOfSoleProprietorForUser
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUser($given_login, $expected)
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin($given_login);
        // Назначаем роль Operator пользователю
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя
        $this->login($given_login);

        $this->dispatch('/profile/sole-proprietor', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');
        // Что отдается в шаблон
        $this->assertArrayHasKey('soleProprietors', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertEquals($expected['name'], $view_vars['soleProprietors'][0]['name']);
        $this->assertEquals($expected['first_name'], $view_vars['soleProprietors'][0]['first_name']);
        $this->assertEquals($expected['last_name'], $view_vars['soleProprietors'][0]['last_name']);
        $this->assertEquals(false, $view_vars['is_operator']);
        // Возвращается из шаблона
        $this->assertQuery('#sole-proprietor-filter-form');
    }

    /**
     * Ajax
     * Доступность /profile/sole-proprietor (GET на getList) авторизованному с ролью Verified
     *
     * @param $given_login
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @dataProvider providerOfSoleProprietorForUser
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUserForAjax($given_login, $expected)
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin($given_login);
        // Назначаем роль Operator пользователю
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя
        $this->login($given_login);

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $soleProprietor = $data['data']['soleProprietors'][count($data['data']['soleProprietors'])-1];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('SoleProprietor collection', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('soleProprietors', $data['data']);
        $this->assertArrayHasKey('paginator', $data['data']);
        $this->assertArrayHasKey('is_operator', $data['data']);
        $this->assertEquals($expected['name'], $soleProprietor['name']);
        $this->assertEquals($expected['first_name'], $soleProprietor['first_name']);
        $this->assertEquals($expected['last_name'], $soleProprietor['last_name']);
        $this->assertEquals(false, $data['data']['is_operator']);
    }

    /////////////// get

    // Неавторизованный

    /**
     * Недоступность /profile/sole-proprietor/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/sole-proprietor/'.$soleProprietor->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/sole-proprietor/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Не Владелец

    /**
     * Неоступность /profile/sole-proprietor/id (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Ajax
     * Недоступность /profile/sole-proprietor/id (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Доступность /profile/sole-proprietor/id (GET на get) Владельцу
     *
     * @param $given_login
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @dataProvider providerOfSoleProprietorForUser
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOwner($given_login, $expected)
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin($given_login);
        // Назначаем роль Operator пользователю
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя
        $this->login($given_login);

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => $expected['name']));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
    }

    /**
     * Ajax
     * Доступность /profile/sole-proprietor/id (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @dataProvider providerOfSoleProprietorForUser
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOwnerForAjax($given_login, $expected)
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin($given_login);
        // Назначаем роль Operator пользователю
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя
        $this->login($given_login);

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => $expected['name']));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'get', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('SoleProprietor single', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('soleProprietor', $data['data']);
        $this->assertEquals($expected['first_name'], $soleProprietor->getFirstName());
        $this->assertEquals($expected['last_name'], $soleProprietor->getLastName());
    }

    /**
     * @return array
     */
    public function providerOfSoleProprietorForUser(): array
    {
        return [
            ['test', ['name' => self::TEST_SOLE_PROPRIETOR_1_NAME, 'first_name' => 'Леонель', 'last_name' => 'Месси']],
            ['test2', ['name' => self::TEST_SOLE_PROPRIETOR_2_NAME, 'first_name' => 'Криштиану', 'last_name' => 'Роналду']],
        ];
    }

    /////////////// create

    // Неавторизованный

    /**
     * Недоступность /profile/sole-proprietor (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorised()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/sole-proprietor', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');
        $this->assertRedirectTo('/login?redirectUrl=/profile/sole-proprietor');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/sole-proprietor (POST на create) Оператору
     * 404 - метод не поддерживается для HTTP
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/sole-proprietor', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor (POST на create) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Access to the resource is denied to the user.', $data['message']);
    }

    /**
     * Недоступность /profile/sole-proprietor (POST валидный на create) по HTTP (404)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/sole-proprietor', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');
    }

    /**
     * Для Ajax
     * Доступность /profile/sole-proprietor (POST валидный на create) пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerifiedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        /** @var \DateTime $currentDate */
        $currentDate = new \DateTime();

        // Чтобы случайно не совпало, добавляем Timestamp
        $name = 'ИП Супертест Супертестович Супертестов ' . $currentDate->getTimestamp();

        $postData = [
            'name'              => $name,
            'first_name'        => 'Супертест',
            'secondary_name'    => 'Супертестович',
            'last_name'         => 'Супертестов',
            'birth_date'        => '31.12.1980',
            'inn'               => '683103646744',
            'nds_type'          => $ndsType->getType(),
            'csrf'              => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => $name));

        $this->assertEquals($name, $soleProprietor->getName());
        $this->assertEquals($data['data']['soleProprietor']['id'], $soleProprietor->getId());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');

        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных пользователем с ролью Verified /profile/sole-proprietor
     * (POST невалидный на create)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateWithInvalidPostForVerifiedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        /** @var \DateTime $currentDate */
        $currentDate = new \DateTime();

        // Чтобы случайно не совпало, добавляем Timestamp
        $name = 'ИП Супертест Супертестович Супертестов ' . $currentDate->getTimestamp();

        $last_name = 'Супертестов';
        // Invalid POST
        $postData = [
            'name'              => $name,
            'first_name'        => 'Супертест',
            'secondary_name'    => 'Супертестович',
            'last_name'         => 'Супертестов',
            'birth_date'        => '31.12.1980',
            'inn'               => '683103646744',
            'nds_type'          => $ndsType->getType(),
            'csrf'              => '123invalid345' // Wrong csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => $name));

        $this->assertNull($soleProprietor);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /////////////// update

    // Неавторизованный

    /**
     * Недоступность /profile/sole-proprietor/id (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorised()
    {
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/sole-proprietor/'.$soleProprietor->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/sole-proprietor/id (POST на update) Оператору
     * 404 - метод не поддерживается для HTTP
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        // До валидации формы не дойдет (Метод не поддерживается для HTTP)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertArrayHasKey('content', $view_vars);
        $this->assertEquals('Method not supported', $view_vars['content']);
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals('Page not found.', $view_vars['message']);
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id (POST на update) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        // До валидации формы не дойдет
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Не владелец

    /**
     * Недоступность /profile/sole-proprietor/id (POST на update) по HTTP
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * Important! update method is only for Ajax!
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_2_NAME));

        // До валидации формы не дойдет
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id (POST на update) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_2_NAME));

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(),
            'POST', $postData, $isXmlHttpRequest);
        #$data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Для Ajax
     * Доступность /profile/sole-proprietor/id (POST валидный на update) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testUpdateWithValidPostForOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $old_nds_type = $soleProprietor->getCivilLawSubject()->getNdsType()->getType();

        /** @var NdsType $ndsType */
        $newNdsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '0']);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        /** @var \DateTime $currentDate */
        $currentDate = new \DateTime();

        // Чтобы случайно не совпало, добавляем Timestamp
        $new_name = 'ИП Мегатест Мегатестович Мегатестов ' . $currentDate->getTimestamp();

        $postData = [
            'name'              => $new_name,
            'first_name'        => 'Мегатест',
            'secondary_name'    => 'Мегатестович',
            'last_name'         => 'Мегатестов',
            'birth_date'        => '25.12.1980',
            'inn'               => '683103646744',
            'nds_type'          => $newNdsType->getType(),
            'csrf'              => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var SoleProprietor $updatedSoleProprietor */
        $updatedSoleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => $new_name));

        /** @var NdsType $updatedNdsType */
        $updatedNdsType = $updatedSoleProprietor->getCivilLawSubject()->getNdsType();

        $this->assertEquals($new_name, $updatedSoleProprietor->getName());
        $this->assertNotEquals((int)$old_nds_type, (int)$updatedNdsType->getType());
        $this->assertEquals((int)$newNdsType->getType(), (int)$updatedNdsType->getType());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных пользователем с ролью Verified /profile/sole-proprietor/id
     * (POST невалидный на update)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        /** @var \DateTime $currentDate */
        $currentDate = new \DateTime();

        // Чтобы случайно не совпало, добавляем Timestamp
        $new_name = 'ИП Мегатест Мегатестович Мегатестов ' . $currentDate->getTimestamp();

        // Invalid POST
        $postData = [
            'name'              => $new_name,
            'first_name'        => 'Мегатест',
            'secondary_name'    => 'Мегатестович',
            'last_name'         => 'Мегатестов',
            'birth_date'        => '25.12.1980',
            'inn'               => '683103646744',
            'nds_type'          => $ndsType->getType(),
            'csrf'              => '123invalid456'
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var SoleProprietor $updatedSoleProprietor */
        $updatedSoleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('lastName' => $new_name));

        $this->assertNull($updatedSoleProprietor);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /////////////// delete

    // Неавторизованный

    /**
     * Недоступность /profile/sole-proprietor/id (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorised()
    {
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/sole-proprietor/'.$soleProprietor->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Не владелец

    /**
     * Недоступность /profile/sole-proprietor/id (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        // До удаления не дойдет
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /profile/sole-proprietor/id (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        // До удаления не дойдет
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Доступность /profile/sole-proprietor/id (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteForOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var SoleProprietor $newSoleProprietor */ // Создаем новый CivilLawSubject с SoleProprietor для удаления
        $newSoleProprietor = $this->createNewTestSoleProprietor('test');

        // Must be added
        $this->assertEquals('ИП New Sole Proprietor', $newSoleProprietor->getName());

        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/sole-proprietor/'.$newSoleProprietor->getId(), 'POST', $postData);

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => 'ИП New Sole Proprietor'));

        // Must be null (deleted)
        $this->assertNull($soleProprietor);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertRedirectTo('/profile/sole-proprietor');
    }

    /**
     * Для Ajax
     * Доступность /profile/sole-proprietor/id (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteForOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var SoleProprietor $newSoleProprietor */ // Создаем новый CivilLawSubject с SoleProprietor для удаления
        $newSoleProprietor = $this->createNewTestSoleProprietor('test');

        // Must be added
        $this->assertEquals('ИП New Sole Proprietor', $newSoleProprietor->getName());

        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$newSoleProprietor->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => 'ИП New Sole Proprietor'));

        // Must be null (deleted)
        $this->assertNull($soleProprietor);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-single');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(SoleProprietorController::SUCCESS_DELETE, $data['message']);
    }

    /////////////// editForm

    // Неавторизованный

    /**
     * Недоступность /profile/sole-proprietor/id/edit (GET на editFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNonAuthorised()
    {
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/edit');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-edit');
        $this->assertRedirectTo('/login?redirectUrl=/profile/sole-proprietor/'.$soleProprietor->getId().'/edit');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id/edit (GET на editFormAction) по Ajax неавторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/edit',
            null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-edit');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id/edit (GET на editFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/edit', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/sole-proprietor/id/edit (GET на editFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-edit');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_SOLE_PROPRIETOR_MESSAGE, $view_vars['message']);
    }

    /**
     * Недоступность /profile/sole-proprietor/id/edit (GET на editFormAction) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-edit');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_SOLE_PROPRIETOR_MESSAGE, $view_vars['message']);
    }

    /**
     * Доступность /profile/sole-proprietor/id/edit (GET на editFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testEditFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertControllerClass('SoleProprietorController');
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('soleProprietor', $view_vars);
        $this->assertArrayHasKey('soleProprietorForm', $view_vars);
        $this->assertInstanceOf(SoleProprietorForm::class, $view_vars['soleProprietorForm']);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Редактирование SoleProprietor /profile/sole-proprietor/id/edit (POST валидный на editFormAction)
     * пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testEditFormWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $old_nds_type = $soleProprietor->getCivilLawSubject()->getNdsType()->getType();

        /** @var NdsType $newNdsType */
        $newNdsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '0']);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        /** @var \DateTime $currentDate */
        $currentDate = new \DateTime();

        // Чтобы случайно не совпало, добавляем Timestamp
        $new_name = 'ИП Мегатест Мегатестович Мегатестов ' . $currentDate->getTimestamp();

        $postData = [
            'name'              => $new_name,
            'first_name'        => 'Мегатест',
            'secondary_name'    => 'Мегатестович',
            'last_name'         => 'Мегатестов',
            'birth_date'        => '25.12.1980',
            'inn'               => '683103646744',
            'nds_type'          => $newNdsType->getType(),
            'csrf'              => $csrf
        ];

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/edit', 'POST', $postData);

        /** @var SoleProprietor $updatedSoleProprietor */
        $updatedSoleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => $new_name));

        $this->assertEquals($new_name, $updatedSoleProprietor->getName());

        /** @var NdsType $ndsType */
        $ndsType = $updatedSoleProprietor->getCivilLawSubject()->getNdsType();

        $this->assertEquals($newNdsType->getType(), $ndsType->getType());
        $this->assertNotEquals($old_nds_type, $ndsType->getType());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-edit');
        $this->assertRedirectTo('/profile/sole-proprietor');
    }

    /**
     * Отправка заведомо невалидных данных /profile/sole-proprietor/id/edit
     * (POST невалидный на editFormAction) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $nds_type = $soleProprietor->getCivilLawSubject()->getNdsType()->getType();

        /** @var \DateTime $currentDate */
        $currentDate = new \DateTime();

        // Чтобы случайно не совпало, добавляем Timestamp
        $new_name = 'ИП Мегатест Мегатестович Мегатестов ' . $currentDate->getTimestamp();

        // Invalid POST
        $postData = [
            'name'              => $new_name,
            'first_name'        => 'Мегатест',
            'secondary_name'    => 'Мегатестович',
            'last_name'         => 'Мегатестов',
            'birth_date'        => '25.12.1980',
            'inn'               => '683103646744',
            'nds_type'          => $nds_type,
            'csrf'              => '123invalid456'
        ];

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/edit', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var SoleProprietor $updatedSoleProprietor */
        $updatedSoleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => $new_name));

        $this->assertNull($updatedSoleProprietor);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('soleProprietor', $view_vars);
        $this->assertArrayHasKey('soleProprietorForm', $view_vars);
        $this->assertTrue($view_vars['soleProprietorForm'] instanceof SoleProprietorForm);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /////////////// createForm

    // Неавторизованный

    /**
     * Недоступность /profile/sole-proprietor/create (GET на createFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/sole-proprietor/create');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-create');
        $this->assertRedirectTo('/login?redirectUrl=/profile/sole-proprietor/create');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/create (GET на createFormAction) по Ajax не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-create');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/create (GET на createFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/sole-proprietor/create (GET на createFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/profile/sole-proprietor/create', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-create');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Доступность /profile/sole-proprietor/create (GET на createFormAction) пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateFormCanBeAccessedByVerified()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/sole-proprietor/create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertControllerClass('SoleProprietorController');
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('soleProprietorForm', $view_vars);
        $this->assertTrue($view_vars['soleProprietorForm'] instanceof SoleProprietorForm);
    }

    /**
     * Создание SoleProprietor /profile/sole-proprietor/create (POST валидный на create)
     * пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateFormWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        /** @var \DateTime $currentDate */
        $currentDate = new \DateTime();

        // Чтобы случайно не совпало, добавляем Timestamp
        $name = 'ИП Мегатест Мегатестович Мегатестов ' . $currentDate->getTimestamp();

        $postData = [
            'name'              => $name,
            'first_name'        => 'Мегатест',
            'secondary_name'    => 'Мегатестович',
            'last_name'         => 'Мегатестов',
            'birth_date'        => '25.12.1980',
            'inn'               => '683103646744',
            'nds_type'          => $ndsType->getType(),
            'csrf'              => $csrf
        ];

        $this->dispatch('/profile/sole-proprietor/create', 'POST', $postData);

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => $name));

        $this->assertEquals($name, $soleProprietor->getName());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-create');
        $this->assertRedirectTo('/profile');
    }

    /**
     * Создание SoleProprietor
     * Отправка заведомо невалидных данных /profile/sole-proprietor/create
     * (POST невалидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testCreateFormWithInvalidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        /** @var \DateTime $currentDate */
        $currentDate = new \DateTime();

        // Чтобы случайно не совпало, добавляем Timestamp
        $name = 'ИП Мегатест Мегатестович Мегатестов ' . $currentDate->getTimestamp();

        $postData = [
            'name'              => $name,
            'first_name'        => 'Мегатест',
            'secondary_name'    => 'Мегатестович',
            'last_name'         => 'Мегатестов',
            'birth_date'        => '25.12.1980',
            'inn'               => '683103646744',
            'nds_type'          => $ndsType->getType(),
            'csrf'              => 'ghghghghgh65656fdfdfdfgfdewew'
        ];

        $this->dispatch('/profile/sole-proprietor/create', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => $name));

        $this->assertNull($soleProprietor);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertControllerClass('SoleProprietorController');
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('soleProprietorForm', $view_vars);
        $this->assertTrue($view_vars['soleProprietorForm'] instanceof SoleProprietorForm);
    }

    /////////////// deleteForm

    // Неавторизованный

    /**
     * Недоступность /profile/sole-proprietor/id/delete (GET на deleteFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorised()
    {
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/delete');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-delete');
        $this->assertRedirectTo('/login?redirectUrl=/profile/sole-proprietor/'.$soleProprietor->getId().'/delete');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/id/delete (GET на deleteFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/delete',
            'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-delete');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/create (GET на deleteFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/delete',
            'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-delete');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/sole-proprietor/id/delete (GET на deleteFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-delete');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_SOLE_PROPRIETOR_MESSAGE, $view_vars['message']);
    }

    /**
     * Недоступность /profile/sole-proprietor/id/delete (GET на deleteFormAction) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-delete');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_SOLE_PROPRIETOR_MESSAGE, $view_vars['message']);
    }

    /**
     * Доступность /profile/sole-proprietor/id/delete (GET на deleteFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testDeleteFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => self::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/profile/sole-proprietor/'.$soleProprietor->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertControllerClass('SoleProprietorController');
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-delete');
        // Что отдается в шаблон
        $this->assertArrayHasKey('soleProprietor', $view_vars);
    }

    /////////////// formInit (экшен только для Ajax!)

    // Неавторизованный

    /**
     * Недоступность /profile/sole-proprietor/form-init (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByHttp()
    {
        $this->dispatch('/profile/sole-proprietor/form-init', 'GET');

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-init');
    }

    /**
     * Для Ajax
     * Недоступность /profile/sole-proprietor/form-init (GET на formInitAction)
     * не авторизованному пользователю по Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testFormInitCanBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('allowedUser', $data['data']);
        $this->assertArrayHasKey('soleProprietor', $data['data']);
        $this->assertArrayHasKey('allowedSoleProprietors', $data['data']);
        $this->assertArrayHasKey('handbooks', $data['data']);
        $this->assertArrayHasKey('ndsTypes', $data['data']['handbooks']);
    }

    // Авторизованный

    /**
     * Ajax
     * Доступность /profile/sole-proprietor/form-init (GET на formInitAction)
     * авторизованному с ролью Verified
     * Должен отдать коллекцию SoleProprietor, user и нулевые allowedUser и soleProprietor
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithoutParamsForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'soleProprietor' соотвествует нулевое значение
        $this->assertNull($data['data']['soleProprietor']);
        // а ключу коллекции 'allowedSoleProprietors' - не нулевой массив
        $this->assertGreaterThan(0, count($data['data']['allowedSoleProprietors']));
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/sole-proprietor/form-init (GET на formInitAction)
     * с заведома невалидным параметром (idSoleProprietor) авторизованному с ролью Verified
     * Должен отдать коллекции и нулевые объекты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithInvalidParamsForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        //Invalid params
        $getData = [
            'idSoleProprietor' => 99999999,
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'soleProprietor' соотвествует нулевое значение
        $this->assertNull($data['data']['soleProprietor']);
        // а ключу коллекции 'allowedNaturalPersons' - не нулевой массив
        $this->assertGreaterThan(0, count($data['data']['allowedSoleProprietors']));
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/sole-proprietor/form-init (GET на formInitAction)
     * с параметром чужого объекта (idSoleProprietor)
     * Должен отдать коллекции доступных объектов и нулевые объекты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithAlienParamsForAjax()
    {
        // Добавляем SoleProprietor пользователю test
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->createNewTestSoleProprietor('test');

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        //Invalid params
        $getData = [
            'idSoleProprietor' => $soleProprietor->getId()
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test2', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'soleProprietor' соотвествует нулевое значение
        $this->assertNull($data['data']['soleProprietor']);
        // а ключу коллекции 'allowedNaturalPersons' - не нулевой массив
        $this->assertGreaterThan(0, count($data['data']['allowedSoleProprietors']));
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/sole-proprietor/form-init (GET на formInitAction)
     * с валидным параметром Владельцу объектов, id которого передается (idSoleProprietor)
     * Должен отдать коллекции и НЕнулевой объект SoleProprietor
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithValidParamsForAjax()
    {
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $this->createNewTestSoleProprietor('test');
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        //Invalid params
        $getData = [
            'idSoleProprietor' => $soleProprietor->getId()
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/sole-proprietor/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('user-profile/sole-proprietor-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'naturalPerson' соотвествует НЕнулевые значения
        $this->assertNotNull($data['data']['soleProprietor']);
        $this->assertEquals('ИП New Sole Proprietor', $data['data']['soleProprietor']['name']);
        $this->assertEquals('test', $data['data']['user']['login']);
        // и ключу коллекции 'allowedSoleProprietors' тоже
        $this->assertGreaterThan(0, count($data['data']['allowedSoleProprietors']));
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var ModuleRbacRoleInterface $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $login
     * @return SoleProprietor
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createNewTestSoleProprietor($login = 'test'): SoleProprietor
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $login));

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '10%']);

        // Create CivilLawSubject
        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern(true);
        $civilLawSubject->setCreated(new \DateTime());
        $civilLawSubject->setNdsType($ndsType);

        $this->entityManager->persist($civilLawSubject);

        // Create SoleProprietor

        $soleProprietor = new SoleProprietor();
        $soleProprietor->setCivilLawSubject($civilLawSubject);
        $soleProprietor->setName('ИП New Sole Proprietor');
        $soleProprietor->setFirstName('New');
        $soleProprietor->setSecondaryName('Sole');
        $soleProprietor->setLastName('Proprietor');
        $soleProprietor->setBirthDate(\DateTime::createFromFormat('!Y-m-d', '1979-07-25'));
        $soleProprietor->setInn('683103646744');

        $civilLawSubject->setSoleProprietor($soleProprietor);

        $this->entityManager->persist($soleProprietor);

        $this->entityManager->flush();

        return $soleProprietor;
    }
}