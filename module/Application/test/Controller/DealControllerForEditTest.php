<?php

namespace ApplicationTest\Controller;

use Application\Controller\DealController;
use Application\Entity\DealAgent;
use Application\Entity\PaymentMethod;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\User;
use Application\Fixture\PaymentMethodBankTransferFixtureLoader;
use Application\Form\DealForm;
use Application\Service\Payment\PaymentMethodManager;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseRoleManager;
use CoreTest\ViewVarsTrait;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Entity\Role;
use Zend\Form\Form;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\BankClient\BankClientManager;

/**
 * Class DealControllerForEditTest
 * @package ApplicationTest\Controller
 */
class DealControllerForEditTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const MINIMUM_DELIVERY_PERIOD = 14;

    const DEAL_STATUS_NEGOTIATION_NAME = 'Сделка Фикстура';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @var BankClientManager
     */
    public $bankClientManager;

    /**
     * @var bool
     */
    private $disable_rollback = false;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        // Отключаем отправку уведомлений на email
        $this->resetConfigParameters();

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /**
     * Недоступность /deal/edit/[id] не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testEditActionCanNotBeAccessedToNonAuthorised()
    {
        /** @var Deal $deal */
        $deal = $this->getDealInStatusNegotiation();

        $this->dispatch('/deal/edit/'.$deal->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');
        $this->assertRedirectTo('/login?redirectUrl=/deal/edit/'.$deal->getId());
    }

    /**
     * Недоступность /deal/edit/[id] не авторизованному пользователю для ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testEditActionCanNotBeAccessedToNonAuthorisedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealInStatusNegotiation();

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/edit/'.$deal->getId(), 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/deal/edit/'.$deal->getId(), $data['data']['redirectUrl']);
    }

    /**
     * Недоступность /deal/edit/[id] авторизованному пользователю без роли Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testEditActionCanNotBeAccessedUnverifiedUser()
    {
        /** @var Deal $deal */
        $deal = $this->getDealInStatusNegotiation();

        // Залогиниваем пользователя test
        $this->login('test');
        // Присваиваем пользователю test роль Unverified
        $this->assignRoleToTestUser('Unverified');

        $this->dispatch('/deal/edit/'.$deal->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Недоступность /deal/edit/[id] авторизованному пользователю без роли Verified для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testEditActionCanNotBeAccessedUnverifiedUserForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealInStatusNegotiation();

        // Залогиниваем пользователя test
        $this->login('test');
        // Присваиваем пользователю test роль Unverified
        $this->assignRoleToTestUser('Unverified');

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/edit/'.$deal->getId(), 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);

        $this->assertEquals('ERROR',$data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
    }

    /**
     * Доступность /deal/edit/[id] пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testEditActionCanBeAccessedVerifiedUser()
    {
        /** @var Deal $deal */
        $deal = $this->getDealInStatusNegotiation();

        // Залогиниваем пользователя test
        $this->login('test');

        $this->dispatch('/deal/edit/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('dealForm', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#deal-edit-form');
    }

    /**
     * Доступность /deal/edit/[id] пользователю с ролью Verified для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testEditActionCanBeAccessedVerifiedUserForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealInStatusNegotiation();

        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/edit/'.$deal->getId(), null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');
    }

    /**
     * Тест рпедактирования сделки с запрещенными для редактирования данными в Post.
     *
     * 1. Роль contractor. Не владелец, т.е. не модет изменять counteragent_email и deal_role.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @dataProvider dataEditDealWithValidationFailForContractor
     *
     * @group deal
     * @throws \Exception
     */
    public function testEditDealWithValidationFailForContractor($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_STATUS_NEGOTIATION_NAME]);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Нужен Contractor
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();

        // Залогиниваем Contractor'а
        $this->login($contractor->getUser()->getLogin());

        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $new_name = self::DEAL_STATUS_NEGOTIATION_NAME . 'try edit';
        // Post data
        $postData = [
            'name'                 => $new_name,
            'amount'               => '1007043',
            #'deal_role'            => 'contractor',     // не имеет права изменять
            #'counteragent_email'   => 'TeSt2@teSt.neT', // не имеет права изменят
            'deal_type'            => $deal->getDealType()->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),

            'payment_method'       => $payment->getPaymentMethod()->getId(),
            'deal_civil_law_subject' => $contractor->getCivilLawSubject()->getId(),
        ];
        $postData = array_merge($postData, $given);

        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData, true);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('ERROR_INVALID_FORM_DATA', $data['code']);
        $this->assertEquals('The form does not pass validation.', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey($expected['validation_key'], $data['data']);
        $this->assertEquals($expected['validation_reasons'], $data['data'][$expected['validation_key']]);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        // Проверяем, что название не поменялось
        $this->assertNull($deal);
    }

    /**
     * @return array
     */
    public function dataEditDealWithValidationFailForContractor(): array
    {
        return [
            [
                ['deal_role'  => 'customer'],
                ['validation_key' => 'deal_role', 'validation_reasons' => ['forbiddenEdit' => ['Роль запрешено редактировать не владельцу']]]
            ],
            [
                ['counteragent_email' => 'TeSt2@teSt.neT'],
                ['validation_key' => 'counteragent_email', 'validation_reasons' => ['forbiddenEdit' => ['Email запрешено редактировать не владельцу']]]
            ],
        ];
    }

    /**
     * Тест создания сделки с правильными Post данными.
     *
     * 1. С созданием CivilLawSubject (natural-person)
     * 2. С созданием PaymentMethod (bank_transfer)
     * 3. Роль contractor.
     * 4. Проверка приведения email к нижнему регистру.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @TODO Не доделан! Продолжить отсюда!!!
     */
    public function testEditDealWithNaturalPersonAndPaymentMethodBankTransferForContractor()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_STATUS_NEGOTIATION_NAME]);

        // Нужен Contractor
        $contractor_login = $deal->getContractor()->getUser()->getLogin();

        // Залогиниваем Contractor'а
        $this->login($contractor_login);

        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'name'                 => self::DEAL_STATUS_NEGOTIATION_NAME . ' (edit) test-1',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'TeSt2@teSt.neT', // проверка приведения email к нижнему регистру
            'deal_type'            => $deal->getDealType()->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => [
                'payment_method_type'=> 'bank_transfer',
                'name'=> 'SIBBANK test-1',
                'bik'=> '420994241',
                'bank_name'=> 'SIBBANK test-1',
                'account_number'=> '123456789098764123',
                'corr_account_number'=> '123456789098764123'
            ],
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2011',
                'first_name' => 'Перивет test-1',
                'last_name' => 'Привет test-1',
                'secondary_name' => 'Привет test-1',
            ],
        ];

        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());

        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится текущим агентом
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getCustomer()->getEmail());
        $this->assertEquals(strtolower($postData['counteragent_email']), $deal->getCustomer()->getEmail());
    }



    /**
     * @return Deal
     */
    private function getDealInStatusNegotiation()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::DEAL_STATUS_NEGOTIATION_NAME));

        return $deal;
    }

    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        $user = $user ?? $this->user;
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        $login = $login ?? $this->user_login;
        $password = $password ?? $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}