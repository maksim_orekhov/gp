<?php

namespace ApplicationTest\Controller;

use Application\Controller\DealController;
use Application\Entity\CivilLawSubject;
use Application\Entity\CivilLawSubjectChildInterface;
use Application\Entity\DealAgent;
use Application\Entity\Token;
use Application\Entity\User;
use Application\Exception\Code\DealExceptionCodeInterface;
use Application\Form\DealPartnerForm;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\UserManager;
use ApplicationTest\Bootstrap;
use Core\Adapter\TokenAdapter;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseRoleManager;
use Core\Service\ORMDoctrineUtil;
use CoreTest\ViewVarsTrait;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use ModuleFileManager\Service\FileManager;
use Zend\Form\Form;
use Zend\Mvc\MvcEvent;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use Application\Entity\Deal;
use Application\Service\BankClient\BankClientManager;

/**
 * Class DealControllerForCreateByTokenTest
 * @package ApplicationTest\Controller
 */
class DealControllerForCreateByTokenTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const MINIMUM_DELIVERY_PERIOD = 14;
    const NONEXISTENT_TOKEN_KEY = 'gHk65eDg';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @var BankClientManager
     */
    public $bankClientManager;

    /**
     * @var TokenAdapter
     */
    private $tokenAdapter;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;
    /**
     * @var string
     */
    private $deal_upload_folder;
    /**
     * @var FileManager
     */
    private $fileManager;

    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];
        $this->deal_upload_folder = './'.$config['file-management']['main_upload_folder'].'/deal-files';

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);
        $this->tokenAdapter = $serviceManager->get(TokenAdapter::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->fileManager = $serviceManager->get(FileManager::class);

        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;
        $this->userManager = null;
        $this->baseRoleManager = null;
        $this->tokenAdapter = null;
        $this->civilLawSubjectManager = null;
        $this->fileManager = null;

        gc_collect_cycles();
    }

    /**
     * Тест CreatePartnerForm с правильным токеном, которого НЕТ в базе.
     * Есть deal_type, amount, counteragent_email
     * Нет counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormByNonexistentTokenData()
    {
        $url = '/deal/create/partner-form/'.self::NONEXISTENT_TOKEN_KEY;
        $this->dispatch($url, 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_NOT_FOUND_MESSAGE, $view_vars['message']);
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с правильным токеном, которого НЕТ в базе.
     * Есть deal_type, amount, counteragent_email
     * Нет counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormByNonexistentTokenDataForAjax()
    {
        $url = '/deal/create/partner-form/'.self::NONEXISTENT_TOKEN_KEY;
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_NOT_FOUND, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_NOT_FOUND_MESSAGE, $data['message']);
    }

    /**
     * Тест CreatePartnerForm с правильным токеном, который ЕСТЬ в базе.
     * Есть deal_type, amount, counteragent_email
     * Нет counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormByValidTokenData()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $counteragent_civil_law_subject_name = $this->civilLawSubjectManager
            ->getCivilLawSubjectName($counteragentCivilLawSubject);

        $amount = 10000;

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            'Токен для сделок с субъектом пользователя test2', 10000);
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form/'.$token->getTokenKey();
        $this->dispatch($url, 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealPartnerForm::class, $view_vars['dealForm']);
        /** @var Form $form */
        $form = $view_vars['dealForm'];
        // Проверяем, что в елемент формы получилои соотвествующие значения
        $this->assertEquals($url, $form->getAttribute('action'));
        $this->assertEquals($amount, $form->getElements()['amount']->getValue());
        $this->assertArrayNotHasKey('deal_role', $form->getElements());
        $this->assertArrayNotHasKey('deal_type', $form->getElements());

        $this->assertArrayHasKey('form_action', $view_vars);
        $this->assertEquals($url, $view_vars['form_action']);
        $this->assertArrayHasKey('token_data', $view_vars);
        $this->assertInternalType('array', $view_vars['token_data']);
        $this->assertEquals($dealType->getId(), $view_vars['token_data']['deal_type']);
        $this->assertEquals('customer', $view_vars['token_data']['deal_role']);
        $this->assertEquals(10000.0, $view_vars['token_data']['amount']);
        $this->assertEquals($counteragentCivilLawSubject->getUser()->getEmail()->getEmail(),
            $view_vars['token_data']['counteragent_email']);
        $this->assertEquals($counteragentCivilLawSubject->getId(), $view_vars['token_data']['counteragent_civil_law_subject']);
        $this->assertArrayHasKey('counteragent_name', $view_vars['token_data']);
        $this->assertEquals($counteragent_civil_law_subject_name, $view_vars['token_data']['counteragent_name']);
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с правильным токеном.
     * Есть deal_type, amount, counteragent_email
     * Нет counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormByValidTokenDataForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $amount = 10000;

        $counteragent_civil_law_subject_name = $this->civilLawSubjectManager
            ->getCivilLawSubjectName($counteragentCivilLawSubject);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            'Токен для сделок с субъектом пользователя test2', $amount);
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form/'.$token->getTokenKey();
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Deal partner form', $data['message']);
        $this->assertArrayHasKey('form_action', $data['data']);
        $this->assertEquals($url, $data['data']['form_action']);

        $this->assertArrayHasKey('token_data', $data['data']);
        $this->assertArrayHasKey('deal_role', $data['data']['token_data']);
        $this->assertEquals('customer', $data['data']['token_data']['deal_role']);
        $this->assertArrayHasKey('deal_type', $data['data']['token_data']);
        $this->assertEquals($dealType->getId(), $data['data']['token_data']['deal_type']);
        $this->assertArrayHasKey('amount', $data['data']['token_data']);
        $this->assertEquals($amount, $data['data']['token_data']['amount']);
        $this->assertArrayHasKey('counteragent_email', $data['data']['token_data']);
        $this->assertEquals($user->getEmail()->getEmail(), $data['data']['token_data']['counteragent_email']);
        $this->assertArrayHasKey('counteragent_civil_law_subject', $data['data']['token_data']);
        $this->assertEquals($counteragentCivilLawSubject->getId(), $data['data']['token_data']['counteragent_civil_law_subject']);
        $this->assertArrayHasKey('counteragent_name', $data['data']['token_data']);
        $this->assertEquals($counteragent_civil_law_subject_name, $data['data']['token_data']['counteragent_name']);
    }

    /**
     * Тест CreatePartnerForm с НЕВАЛИДНЫМ токеном.
     * Есть deal_type, amount
     * HET counteragent_email, НЕТ counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithNoCounteragentDataInToken()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $amount = 12000;

        $wrongTokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => null,
            'counteragent_civil_law_subject' => null
        ];
        // Создаем токен
        $wrongEncodedToken = $this->tokenAdapter->createTokenForDeal($wrongTokenData);

        $rightTokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => $amount,
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];
        // Создаем токен
        $rightEncodedToken = $this->tokenAdapter->createTokenForDeal($rightTokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $rightEncodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form?counteragent_token='.$wrongEncodedToken;
        $this->dispatch($url, 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_NOT_FOUND_MESSAGE, $view_vars['message']);
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с НЕВАЛИДНЫМ токеном.
     * Есть deal_type, amount
     * HET counteragent_email, НЕТ counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithNoCounteragentDataInTokenForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $amount = 12000;

        $wrongTokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => null,
            'counteragent_civil_law_subject' => null
        ];
        // Создаем токен
        $wrongEncodedToken = $this->tokenAdapter->createTokenForDeal($wrongTokenData);

        $rightTokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => $amount,
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];
        // Создаем токен
        $rightEncodedToken = $this->tokenAdapter->createTokenForDeal($rightTokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $rightEncodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form?counteragent_token='.$wrongEncodedToken;
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_NOT_FOUND, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_NOT_FOUND_MESSAGE, $data['message']);
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с НЕВАЛИДНЫМ токеном В БАЗЕ.
     * Есть deal_type, amount
     * HET counteragent_email, НЕТ counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithNoCounteragentDataInTokenInDBForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);

        $amount = 12000;

        $wrongTokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => $amount,
            'counteragent_email' => null,
            'counteragent_civil_law_subject' => null
        ];
        // Создаем токен
        $wrongEncodedToken = $this->tokenAdapter->createTokenForDeal($wrongTokenData);

        $token = $this->createToken($user, null, $dealType,
            $wrongEncodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form?counteragent_token='.$wrongEncodedToken;
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_DATA_IS_INVALID, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_DATA_IS_INVALID_MESSAGE, $data['message']);
    }

    /**
     * Тест CreatePartnerForm с НЕВАЛИДНЫМ токеном.
     * Есть deal_type, amount
     * HEСУЩЕСТВУЮЩИЙ counteragent_email, НЕТ counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithInvalidEmailInTokenInDB()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);

        $wrongTokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => 'counteragent_email@simple-tech.org', // не существует
            'counteragent_civil_law_subject' => null
        ];

        // Создаем токен
        $wrongEncodedToken = $this->tokenAdapter->createTokenForDeal($wrongTokenData);

        $token = $this->createToken($user, null, $dealType,
            $wrongEncodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form?counteragent_token='.$wrongEncodedToken;
        $this->dispatch($url, 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_DATA_IS_INVALID_MESSAGE, $view_vars['message']);
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с НЕВАЛИДНЫМ токеном.
     * Есть deal_type, amount
     * HEСУЩЕСТВУЮЩИЙ counteragent_email, НЕТ counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithInvalidEmailInTokenInDBForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);

        $wrongTokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => 'counteragent_email@simple-tech.org', // не существует
            'counteragent_civil_law_subject' => null
        ];

        // Создаем токен
        $wrongEncodedToken = $this->tokenAdapter->createTokenForDeal($wrongTokenData);

        $token = $this->createToken($user, null, $dealType,
            $wrongEncodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form?counteragent_token='.$wrongEncodedToken;
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_DATA_IS_INVALID, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_DATA_IS_INVALID_MESSAGE, $data['message']);
    }

    /**
     * Тест CreatePartnerForm с НЕВАЛИДНЫМ токеном.
     * Есть deal_type, amount, есть counteragent_email, но counteragent_civil_law_subject не его
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithInvalidCounteragentDataInTokenInDB()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test']);
        /** @var User $user2 */
        $user2 = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user2, 'isPattern' => true]);

        $wrongTokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $user->getEmail()->getEmail(), // Email user
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId() // CLS user2
        ];

        // Создаем токен
        $wrongEncodedToken = $this->tokenAdapter->createTokenForDeal($wrongTokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $wrongEncodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form?counteragent_token='.$wrongEncodedToken;
        $this->dispatch($url, 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_DATA_IS_INVALID_MESSAGE, $view_vars['message']);
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с НЕВАЛИДНЫМ токеном.
     * Есть deal_type, amount, есть counteragent_email, но counteragent_civil_law_subject не его
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithInvalidCounteragentDataInTokenInDBForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test']);
        /** @var User $user2 */
        $user2 = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user2, 'isPattern' => true]);

        $wrongTokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $user->getEmail()->getEmail(), // Email user
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId() // CLS user2
        ];

        // Создаем токен
        $wrongEncodedToken = $this->tokenAdapter->createTokenForDeal($wrongTokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $wrongEncodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form?counteragent_token='.$wrongEncodedToken;
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_DATA_IS_INVALID, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_DATA_IS_INVALID_MESSAGE, $data['message']);
    }

    /**
     * Тест CreatePartnerForm с валидным токеном без Amount.
     * Есть deal_type, counteragent_email, counteragent_civil_law_subject
     * HET amount
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithNoAmountInToken()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => null,
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        $counteragent_civil_law_subject_name = $this->civilLawSubjectManager
            ->getCivilLawSubjectName($counteragentCivilLawSubject);

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->dispatch($url, 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertInstanceOf(DealPartnerForm::class, $view_vars['dealForm']);
        /** @var Form $form */
        $form = $view_vars['dealForm'];
        // Проверяем, что в елемент формы получилои соотвествующие значения
        $this->assertEquals($url, $form->getAttribute('action'));
        $this->assertEquals(null, $form->getElements()['amount']->getValue());
        $this->assertArrayNotHasKey('deal_role', $form->getElements());
        $this->assertArrayNotHasKey('deal_type', $form->getElements());

        $this->assertArrayHasKey('form_action', $view_vars);
        $this->assertEquals($url, $view_vars['form_action']);
        $this->assertArrayHasKey('token_data', $view_vars);
        $this->assertInternalType('array', $view_vars['token_data']);
        $this->assertNotEquals($tokenData, $view_vars['token_data']);
        $this->assertEquals($dealType->getId(), $view_vars['token_data']['deal_type']);
        $this->assertEquals('customer', $view_vars['token_data']['deal_role']);
        $this->assertEquals(null, $view_vars['token_data']['amount']);
        $this->assertEquals($tokenData['counteragent_email'], $view_vars['token_data']['counteragent_email']);
        $this->assertEquals($tokenData['counteragent_civil_law_subject'], $view_vars['token_data']['counteragent_civil_law_subject']);
        $this->assertArrayHasKey('counteragent_name', $view_vars['token_data']);
        $this->assertEquals($counteragent_civil_law_subject_name, $view_vars['token_data']['counteragent_name']);
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с валидным токеном без Amount.
     * Есть deal_type, counteragent_email, counteragent_civil_law_subject
     * HET amount
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithNoAmountInTokenForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => null,
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        $counteragent_civil_law_subject_name = $this->civilLawSubjectManager
            ->getCivilLawSubjectName($counteragentCivilLawSubject);

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Deal partner form', $data['message']);

        $this->assertArrayHasKey('form_action', $data['data']);
        $this->assertEquals($url, $data['data']['form_action']);

        $this->assertArrayHasKey('token_data', $data['data']);
        $this->assertArrayHasKey('deal_role', $data['data']['token_data']);
        $this->assertEquals('customer', $data['data']['token_data']['deal_role']);
        $this->assertArrayHasKey('deal_type', $data['data']['token_data']);
        $this->assertEquals($dealType->getId(), $data['data']['token_data']['deal_type']);
        $this->assertArrayHasKey('amount', $data['data']['token_data']);
        $this->assertEquals(null, $data['data']['token_data']['amount']);
        $this->assertArrayHasKey('counteragent_email', $data['data']['token_data']);
        $this->assertEquals($user->getEmail()->getEmail(), $data['data']['token_data']['counteragent_email']);
        $this->assertArrayHasKey('counteragent_civil_law_subject', $data['data']['token_data']);
        $this->assertEquals($counteragentCivilLawSubject->getId(), $data['data']['token_data']['counteragent_civil_law_subject']);
        $this->assertArrayHasKey('counteragent_name', $data['data']['token_data']);
        $this->assertEquals($counteragent_civil_law_subject_name, $data['data']['token_data']['counteragent_name']);
    }

    /**
     * Тест CreatePartnerForm с валидным токеном без Amount и валидным POST с Amount
     * Есть deal_type, counteragent_email, counteragent_civil_law_subject
     * В токене HET amount
     * В POST данных есть amount
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithNoAmountInTokenButWithAmountInPostData()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => null,
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка с токеном контрагента';

        $amount = 15000;

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => 'agent_email@gmail.com',
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => $amount,
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId()
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->dispatch($url, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        // Проверяем, что сделка создалась
        /** @var Deal $newDeal */
        $newDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $this->assertRedirectTo('/deal/show/'.$newDeal->getId());

        $this->assertNotNull($newDeal);
        $this->assertEquals($amount, $newDeal->getPayment()->getDealValue());
        $this->assertEquals($deal_name, $newDeal->getName());
        $this->assertEquals($postData['agent_email'], $newDeal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $newDeal->getContractor()->getEmail());
        $this->assertEquals($postData['amount'], $newDeal->getPayment()->getDealValue());
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с валидным токеном без Amount и валидным POST с Amount
     * Есть deal_type, counteragent_email, counteragent_civil_law_subject
     * В токене HET amount
     * В POST данных есть amount
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithNoAmountInTokenButWithAmountInPostDataForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => null,
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка с токеном контрагента';

        $amount = 15000;

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => 'agent_email@gmail.com',
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => $amount,
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId()
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        // Проверяем, что сделка создалась
        /** @var Deal $newDeal */
        $newDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $this->assertNotNull($newDeal);
        $this->assertEquals($deal_name, $newDeal->getName());
        $this->assertEquals($postData['agent_email'], $newDeal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $newDeal->getContractor()->getEmail());

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Deal registered', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($deal_name, $data['data']['deal']['name']);
        $this->assertEquals($amount, $data['data']['deal']['amount_without_fee']);
    }

    /**
     * Тест CreatePartnerForm с вилидным токеном и валидными PostData.
     * Есть deal_type, amount, counteragent_email, counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithValidPostData()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка с токеном контрагента';

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => 'agent_email@gmail.com',
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '10000',
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId()
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->dispatch($url, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        // Проверяем, что сделка создалась
        /** @var Deal $newDeal */
        $newDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        // Проверяем, что сделки связалась с токеном
        $this->assertNotNull($newDeal->getDealToken());
        $this->assertSame($token, $newDeal->getDealToken());

        $this->assertRedirectTo('/deal/show/'.$newDeal->getId());

        $this->assertNotNull($newDeal);
        $this->assertEquals($deal_name, $newDeal->getName());
        $this->assertEquals($postData['agent_email'], $newDeal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $newDeal->getContractor()->getEmail());
        $this->assertEquals($postData['amount'], $newDeal->getPayment()->getDealValue());
    }

    /**
     * Тест CreatePartnerForm с вилидным токеном и валидными PostData и валидными FilesData.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithValidPostDataAndFilesData()
    {
        // Переорпеделяем is_uploaded_file
        $this->disablePhpUploadCapabilities();
        $file_name = 'image_file_for_test.png';
        $file_name_2 = 'image_file_2_for_test.png';

        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка с токеном контрагента';

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => 'agent_email@gmail.com',
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '10000',
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId()
        ];

        // Формируем POST даные
        $this->getRequest()
            ->setMethod('POST')
            ->setPost(new \Zend\Stdlib\Parameters($postData));
        $files = new \Zend\Stdlib\Parameters([
            'files' => [
                [
                    'name' => $file_name,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name,
                    'size' => filesize('./module/Application/test/files/' .$file_name),
                    'error' => 0
                ],
                [
                    'name' => $file_name_2,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name_2,
                    'size' => filesize('./module/Application/test/files/' .$file_name_2),
                    'error' => 0
                ],
            ]
        ]);
        $this->getRequest()->setFiles($files);

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->dispatch($url, 'POST');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        // Проверяем, что сделка создалась
        /** @var Deal $newDeal */
        $newDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $this->assertRedirectTo('/deal/show/'.$newDeal->getId());

        $this->assertNotNull($newDeal);
        $this->assertEquals($deal_name, $newDeal->getName());
        $this->assertEquals($postData['agent_email'], $newDeal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $newDeal->getContractor()->getEmail());
        $this->assertEquals($postData['amount'], $newDeal->getPayment()->getDealValue());

        // Проверяем и удаляем полученный файл
        $dealFiles = $newDeal->getDealFiles();

        $file = $this->fileManager->getFileByOriginName($file_name);
        $this->assertEquals($file_name, $file->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file->getName());
        $this->assertContains($file, $dealFiles);

        $file2 = $this->fileManager->getFileByOriginName($file_name_2);
        $this->assertEquals($file_name_2, $file2->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file2->getName());
        $this->assertContains($file2, $dealFiles);

        unlink($this->deal_upload_folder.'/'.$file->getName());
        unlink($this->deal_upload_folder.'/'.$file2->getName());
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с вилидным токеном и валидными PostData и валидными FilesData.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     */
    public function testCreatePartnerFormWithValidPostDataAndFilesDataForAjax()
    {
        // Переорпеделяем is_uploaded_file
        $this->disablePhpUploadCapabilities();
        $file_name = 'image_file_for_test.png';
        $file_name_2 = 'image_file_2_for_test.png';
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка с токеном контрагента';

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => 'agent_email@gmail.com',
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '10000',
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId()
        ];

        // Формируем POST даные
        $this->getRequest()
            ->setMethod('POST')
            ->setPost(new \Zend\Stdlib\Parameters($postData));
        $files = new \Zend\Stdlib\Parameters([
            'files' => [
                [
                    'name' => $file_name,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name,
                    'size' => filesize('./module/Application/test/files/' .$file_name),
                    'error' => 0
                ],
                [
                    'name' => $file_name_2,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name_2,
                    'size' => filesize('./module/Application/test/files/' .$file_name_2),
                    'error' => 0
                ],
            ]
        ]);
        $this->getRequest()->setFiles($files);

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        // Проверяем, что сделка создалась
        /** @var Deal $newDeal */
        $newDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $this->assertNotNull($newDeal);
        $this->assertEquals($deal_name, $newDeal->getName());
        $this->assertEquals($postData['agent_email'], $newDeal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $newDeal->getContractor()->getEmail());

        // Проверяем и удаляем полученный файл
        $dealFiles = $newDeal->getDealFiles();

        $file = $this->fileManager->getFileByOriginName($file_name);
        $this->assertEquals($file_name, $file->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file->getName());
        $this->assertContains($file, $dealFiles);

        $file2 = $this->fileManager->getFileByOriginName($file_name_2);
        $this->assertEquals($file_name_2, $file2->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file2->getName());
        $this->assertContains($file2, $dealFiles);

        unlink($this->deal_upload_folder.'/'.$file->getName());
        unlink($this->deal_upload_folder.'/'.$file2->getName());

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Deal registered', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($deal_name, $data['data']['deal']['name']);
    }


    /**
     * Ajax
     * Тест CreatePartnerForm с вилидным токеном и валидными PostData.
     * Есть deal_type, amount, counteragent_email, counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithValidPostDataForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка с токеном контрагента';

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => 'agent_email@gmail.com',
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '10000',
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId()
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        // Проверяем, что сделка создалась
        /** @var Deal $newDeal */
        $newDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        // Проверяем, что сделки связалась с токеном
        $this->assertNotNull($newDeal->getDealToken());
        $this->assertSame($token, $newDeal->getDealToken());

        $this->assertNotNull($newDeal);
        $this->assertEquals($deal_name, $newDeal->getName());
        $this->assertEquals($postData['agent_email'], $newDeal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $newDeal->getContractor()->getEmail());

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Deal registered', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($deal_name, $data['data']['deal']['name']);
    }

    /**
     * Тест CreatePartnerForm с вилидным токеном и измененной суммой сделки в PostData.
     * Есть deal_type, amount, counteragent_email, counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithChangedAmountInPostData()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка с токеном контрагента';

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => 'agent_email@gmail.com',
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '12000', // Изменена сумма!
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId()
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->dispatch($url, 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_POST_DATA_IS_INVALID_MESSAGE, $view_vars['message']);

        // Проверяем, что сделка не создалась
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $this->assertNull($deal);
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с вилидным токеном и измененной суммой сделки в PostData.
     * Есть deal_type, amount, counteragent_email, counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithChangedAmountInPostDataForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка с токеном контрагента';

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => 'agent_email@gmail.com',
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '12000', // Изменена сумма!
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId()
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        // Проверяем, что сделка не создалась
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_POST_DATA_IS_INVALID, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_POST_DATA_IS_INVALID_MESSAGE, $data['message']);
    }

    /**
     * Тест CreatePartnerForm с вилидным токеном и одинаковым имайлом.
     * Есть deal_type, amount, counteragent_email совпадает с agent_email
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithSameAgentEmailAndCounteragentEmail()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка с токеном контрагента';

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => $counteragent_email, // Совпадает с 'counteragent_email' из токена
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '10000',
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId()
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->dispatch($url, 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_POST_DATA_IS_INVALID_MESSAGE, $view_vars['message']);

        // Проверяем, что сделка не создалась
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $this->assertNull($deal);
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с вилидным токеном и одинаковым имайлом.
     * Есть deal_type, amount, counteragent_email совпадает с agent_email
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreatePartnerFormWithSameAgentEmailAndCounteragentEmailForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка с токеном контрагента';

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => $counteragent_email, // Совпадает с 'counteragent_email' из токена
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '10000',
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId()
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        // Проверяем, что сделка не создалась
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $this->assertNull($deal);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_POST_DATA_IS_INVALID, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(DealExceptionCodeInterface::DEAL_TOKEN_POST_DATA_IS_INVALID_MESSAGE, $data['message']);
    }

    /**
     * Тест CreatePartnerForm с вилидным токеном и валидными PostData с объектами CivilLawSubject
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataCivilLawSubject
     */
    public function testCreatePartnerFormWithCivilLawSubjectObjectInPostData($given, $expected)
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка тестовоая с субъектом';

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => 'agent_email@gmail.com',
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '10000',
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId(),
            'deal_civil_law_subject' => $given
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->dispatch($url, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        // Проверяем, что сделка создалась
        /** @var Deal $newDeal */
        $newDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $this->assertRedirectTo('/deal/show/'.$newDeal->getId());

        $this->assertNotNull($newDeal);
        $this->assertEquals($deal_name, $newDeal->getName());
        $this->assertEquals($postData['agent_email'], $newDeal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $newDeal->getContractor()->getEmail());

        $civilLawSubject = $newDeal->getCustomer()->getCivilLawSubject();

        $this->assertNotNull($civilLawSubject);
        $this->assertNull($civilLawSubject->getUser());

        /** @var CivilLawSubjectChildInterface $child */
        $child = $this->civilLawSubjectManager->getAssignedCivilLawSubject($civilLawSubject);

        $this->assertInstanceOf(CivilLawSubjectChildInterface::class, $child);
        $this->assertInstanceOf($expected['child_class_name'], $child);

        $name = $this->civilLawSubjectManager->getCivilLawSubjectName($civilLawSubject);

        $this->assertEquals($expected['cls_name'], $name);
    }

    /**
     * Ajax
     * Тест CreatePartnerForm с вилидным токеном и валидными PostData с объектами CivilLawSubject
     *
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     *
     * @dataProvider dataCivilLawSubject
     */
    public function testCreatePartnerFormWithCivilLawSubjectObjectInPostDataForAjax($given, $expected)
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);
        $counteragent_email = $user->getEmail()->getEmail();

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => '10000',
            'counteragent_email' => $counteragent_email,
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $csrfElement = new Element\Csrf('csrf');

        $deal_name = 'Сделка тестовоая с субъектом';

        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => 'agent_email@gmail.com',
            'name'              => $deal_name,
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '10000',
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId(),
            'deal_civil_law_subject' => $given
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);


        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);

        // Проверяем, что сделка создалась
        /** @var Deal $newDeal */
        $newDeal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $this->assertNotNull($newDeal);
        $this->assertEquals($deal_name, $newDeal->getName());
        $this->assertEquals($postData['agent_email'], $newDeal->getCustomer()->getEmail());
        $this->assertEquals($counteragent_email, $newDeal->getContractor()->getEmail());

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($newDeal->getId(), $data['data']['deal']['id']);
        $this->assertEquals($postData['amount'], $data['data']['deal']['amount_without_fee']);

        $civilLawSubject = $newDeal->getCustomer()->getCivilLawSubject();

        $this->assertNotNull($civilLawSubject);
        $this->assertNull($civilLawSubject->getUser());

        /** @var CivilLawSubjectChildInterface $child */
        $child = $this->civilLawSubjectManager->getAssignedCivilLawSubject($civilLawSubject);

        $this->assertInstanceOf(CivilLawSubjectChildInterface::class, $child);
        $this->assertInstanceOf($expected['child_class_name'], $child);

        $name = $this->civilLawSubjectManager->getCivilLawSubjectName($civilLawSubject);

        $this->assertEquals($expected['cls_name'], $name);
    }

    /**
     * @return array
     */
    public function dataCivilLawSubject(): array
    {
        return [
            [
                [
                    'civil_law_subject_type' => 'natural-person',
                    'birth_date' => '01.11.2001',
                    'first_name' => 'Иван',
                    'last_name' => 'Сидоров',
                    'secondary_name' => 'Петрович'
                ],
                ['child_class_name' => \Application\Entity\NaturalPerson::class, 'cls_name' => 'Сидоров Иван Петрович'],
            ],
            [
                [
                    'civil_law_subject_type' => 'sole-proprietor',
                    'birth_date' => '01.11.2001',
                    'first_name' => 'Иван',
                    'last_name' => 'Сидоров',
                    'secondary_name' => 'Петрович',
                    'name' => 'ИП Сидоров Иван',
                    'inn' => '4217030520',
                    'nds_type' => 4
                ],
                ['child_class_name' => \Application\Entity\SoleProprietor::class, 'cls_name' => 'ИП Сидоров Иван'],
            ],
            [
                [
                    'civil_law_subject_type' => 'legal-entity',
                    'name' => 'OOO Legal Entity',
                    'legal_address' => '120000, Москва, Кремль, дом 1',
                    'inn' => '4217030520',
                    'kpp' => '123456',
                    'nds_type' => 3
                ],
                ['child_class_name' => \Application\Entity\LegalEntity::class, 'cls_name' => 'OOO Legal Entity'],
            ],
        ];
    }

    /**
     * Недоступность /deal/create не авторизованному пользователю без данных из токена
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedToNonAuthorisedWithoutDealDataFromEvent()
    {
        $this->dispatch('/deal/create', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/create');
    }

    /**
     * Ajax
     * Недоступность /deal/create не авторизованному пользователю без данных из токена
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedToNonAuthorisedWithoutDealDataFromEventForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'get', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/create');
    }

    /**
     * Доступность /deal/create не авторизованному пользователю с данными из токена
     * Проверяем что просто пускает.
     * Сделка не создается, потому что недостаточно данных (нет Post), валидация формы не пропустит
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanBeAccessedToNonAuthorisedWithDealDataFromEvent()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        $token_deal_data = [
            'deal_type' => $dealType->getId(),
            'amount' => '10000',
            'counteragent_email' => 'test2@simple-technology.ru',
            'counteragent_civil_law_subject' => null
        ];

        /** @var MvcEvent $mvcEvent */
        $mvcEvent = $this->getApplication()->getMvcEvent();

        $mvcEvent->setParam('deal_data', $token_deal_data);

        $this->dispatch('/deal/create', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
    }

    /**
     * Ajax
     * Доступность /deal/create не авторизованному пользователю с данными из токена
     * Проверяем что просто пускает.
     * Сделка не создается, потому что недостаточно данных (нет Post), валидация формы не пропустит
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanBeAccessedToNonAuthorisedWithDealDataFromEventForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        $token_deal_data = [
            'deal_type' => $dealType->getId(),
            'amount' => '10000',
            'counteragent_email' => 'test2@simple-technology.ru',
            'counteragent_civil_law_subject' => null
        ];

        /** @var MvcEvent $mvcEvent */
        $mvcEvent = $this->getApplication()->getMvcEvent();

        $mvcEvent->setParam('deal_data', $token_deal_data);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
    }

    /**
     * Ajax
     * Тест FormInit с правильным токеном.
     * Есть deal_type, amount, counteragent_email
     * Нет counteragent_civil_law_subject
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testFormInitByValidTokenDataForAjax()
    {
        /** @var DealType $dealType */
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test2']);

        /** @var CivilLawSubject $counteragentCivilLawSubject */
        $counteragentCivilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['user' => $user, 'isPattern' => true]);

        $amount = 10000;

        $tokenData = [
            'deal_type' => $dealType->getIdent(),
            'amount' => $amount,
            'counteragent_email' => $user->getEmail()->getEmail(),
            'counteragent_civil_law_subject' => $counteragentCivilLawSubject->getId()
        ];

        $counteragent_civil_law_subject_name = $this->civilLawSubjectManager
            ->getCivilLawSubjectName($counteragentCivilLawSubject);

        // Создаем токен
        $encodedToken = $this->tokenAdapter->createTokenForDeal($tokenData);

        $token = $this->createToken($user, $counteragentCivilLawSubject, $dealType,
            $encodedToken, 'Токен для сделок с субъектом пользователя test2');
        // Проверям, что создался токен
        $this->assertNotNull($token);

        $url = '/deal/form-init?counteragent_token='.$encodedToken;
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-form-init');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);

        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('counteragent_token_data', $data['data']);

        $counteragent_token_data = $data['data']['counteragent_token_data'];
        $this->assertArrayHasKey('deal_role', $counteragent_token_data);
        $this->assertEquals('customer', $counteragent_token_data['deal_role']);

        $this->assertArrayHasKey('deal_type', $counteragent_token_data);
        $this->assertEquals($dealType->getId(), $counteragent_token_data['deal_type']);

        $this->assertArrayHasKey('amount', $counteragent_token_data);
        $this->assertEquals(10000, $counteragent_token_data['amount']);

        $this->assertArrayHasKey('counteragent_email', $counteragent_token_data);
        $this->assertEquals($user->getEmail()->getEmail(), $counteragent_token_data['counteragent_email']);

        $this->assertArrayHasKey('counteragent_name', $counteragent_token_data);
        $this->assertEquals($counteragent_civil_law_subject_name, $counteragent_token_data['counteragent_name']);

        $this->assertArrayHasKey('partner_ident', $counteragent_token_data);
        $this->assertEquals('test_test', $counteragent_token_data['partner_ident']);
    }

    /**
     * Ajax
     * Тест создания сделки с правильными Post данными и авторегистрацией
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     */
    public function testCreateDealByTokenWithAutoRegistrationUserForAjax()
    {
        $new_user_email = 'test2@test.net';
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test']);
        /** @var Token $token */
        $token = $this->entityManager->getRepository(Token::class)
            ->findOneBy(['counteragentUser' => $user]);
        $this->assertNotNull($token);
        $encodedToken = $token->getTokenData();

        $csrfElement = new Element\Csrf('csrf');
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        // Post data
        $postData = [
            'csrf'              => $csrfElement->getValue(),
            'agent_email'       => $new_user_email,
            'name'              => 'Сделка для unit тестов (testCreateDealWithAutoRegistrationUserForAjax)',
            'addon_terms'       => 'Дополнительная информация',
            'amount'            => '10000',
            'delivery_period'   => '7',
            'fee_payer_option'  => $feePayerOption->getId(),
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2001',
                'first_name' => 'Иван',
                'last_name' => 'Сидоров',
                'secondary_name' => 'Петрович'
            ]
        ];

        $url = '/deal/create/partner-form?counteragent_token='.$encodedToken;
        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch($url, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create-partner-form');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);

        $this->assertNotNull($deal);
        // агент является владельцем и в сделке нового пользователя быть не должно
        $this->assertNull($deal->getOwner()->getUser());
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        $this->assertNotNull($customer);
        $this->assertSame($postData['agent_email'], $customer->getEmail());

        $newUsers = $this->entityManager->getRepository(User::class)
            ->findAllUnverifiedUserByEmail($postData['agent_email']);
        $this->assertCount(1, $newUsers);
        /** @var User $newUser */
        $newUser = $newUsers[0];
        $this->assertNotNull($newUser);
        //user к сделке еше не прикреплен
        $this->assertNull($customer->getUser());
        $this->assertSame($newUser->getEmail()->getEmail(), $customer->getEmail());
    }

    /**
     * @return Token
     */
    private function getFixtureToken(): Token
    {
        /** @var Token $token */
        $token = $this->entityManager->getRepository(Token::class)
            ->findOneBy(array('tokenKey' => 'eyJ0eXAi'));

        return $token;
    }

    /**
     * @param User $user
     * @param CivilLawSubject|null $civilLawSubject
     * @param DealType|null $dealType
     * @param string $name
     * @param null $amount
     * @return Token
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createToken(User $user,
                                 CivilLawSubject $civilLawSubject = null,
                                 DealType $dealType = null,
                                 string $name,
                                 $amount = null)
    {
        $token = new Token();
        $token->setName($name);
        $token->setPartnerIdent('test_test');
        $token->setCreated(new \DateTime());
        $token->setIsActive(true);
        $token->setAmount($amount);
        $token->setCounteragentUser($user);
        $token->setCounteragentCivilLawSubject($civilLawSubject);
        $token->setDealName('Сделка с субъектом пользователя test');
        $token->setDealType($dealType);
        $token->setTokenKey('TokenKey');

        $this->entityManager->persist($token);

        $this->entityManager->flush();

        return $token;
    }
}