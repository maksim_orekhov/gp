<?php

namespace Application\Test\Controller;

use Application\Controller\TestController;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;

class TestControllerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    private $main_project_url;
    private $main_upload_folder;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \ModuleFileManager\Service\FileManager
     */
    private $fileManager;


    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                include __DIR__ . '/../../../../config/application.config.php',
                include __DIR__ . '/../../../../config/autoload/global.php'
            ),
            include __DIR__ . '/../../../../config/autoload/local.php'
        ));

        parent::setUp();

        $config = $this->getApplicationConfig();

        $this->main_project_url = $config['main_project_url'];
        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->fileManager = $serviceManager->get(\ModuleFileManager\Service\FileManager::class);
    }

    public function tearDown() {

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Страница списка файлов объекта Test
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testFileListActionCanBeAccessed()
    {
        $this->dispatch('/test/files', 'GET');

        $this->assertModuleName('application');
        $this->assertControllerName(TestController::class);
        $this->assertControllerClass('TestController');
        $this->assertResponseStatusCode(200);
        $this->assertMatchedRouteName('test/files');
        $this->assertQuery('.container');
    }

    /**
     * Страница выгрузки файла для объекта Test c id = 1
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testFileUploadActionCanBeAccessed()
    {
        $test = $this->entityManager->getRepository(\Application\Entity\Test::class)
            ->findOneBy(array('name' => 'Test One'));

        $this->dispatch('/test/file-upload/'.$test->getId(), 'GET');

        $this->assertModuleName('application');
        $this->assertControllerName(TestController::class);
        $this->assertControllerClass('TestController');
        $this->assertResponseStatusCode(200);
        $this->assertMatchedRouteName('test/file-upload');
        $this->assertQuery('.container #file-upload-form');
    }

    /**
     * Выгрузка файла для объекта Test c id = 1 c валидными данными
     */
    public function testFileUploadActionWithValidPostAndFileData()
    {
        // Переорпеделяем is_uploaded_file
        $this->disablePhpUploadCapabilities();

        $upload_tmp_dir = ini_get('upload_tmp_dir');
        $file_name = 'text_file_for_test.txt';
        $source_file = "./module/Application/test/files/".$file_name;
        $upload_dir_path = "./".$this->main_upload_folder.TestController::FILE_STORING_FOLDER;

        // Размещаем файл в директории для временных файлов
        $this->saveFile($source_file, $upload_tmp_dir.'/'.$file_name);

        $this->assertFileExists($upload_tmp_dir.'/'.$file_name);

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Формируем POST даные
        $this->getRequest()
            ->setMethod('POST')
            ->setPost(new \Zend\Stdlib\Parameters(array('csrf' => $csrf)));
        $file = new \Zend\Stdlib\Parameters([
            'file' => [
                'name' => $file_name,
                'type' => 'text/plain',
                'tmp_name' => $upload_tmp_dir.'/'.$file_name,
                'size' => filesize($upload_tmp_dir.'/'.$file_name),
                'error' => 0
            ]
        ]);
        $this->getRequest()->setFiles($file);

        $test = $this->entityManager->getRepository(\Application\Entity\Test::class)
            ->findOneBy(array('name' => 'Test One'));

        $this->dispatch('/test/file-upload/'.$test->getId(), 'POST');

        $file = $this->fileManager->getFileByOriginName($file_name);

        // Удаляем файл из директории для временных файлов
        #$this->removeFile($upload_tmp_dir.'/'.$file_name);

        $this->assertFileNotExists($upload_tmp_dir.'/'.$file->getName());
        $this->assertDirectoryExists($upload_dir_path);
        $this->assertDirectoryIsReadable($upload_dir_path);
        $this->assertDirectoryIsWritable($upload_dir_path);
        $this->assertFileExists($upload_dir_path.'/'.$file->getName());
        $this->assertMatchedRouteName('test/file-upload');
        $this->assertResponseStatusCode(302);
        $this->assertRedirectTo('/test/files');

        return $file->getId();
    }

    /**
     * @param $new_file_id
     * @depends testFileUploadActionWithValidPostAndFileData
     *
     * @expectedException \Exception
     * @expectedExceptionMessage Can not find File in database by Id
     */
    public function testFileDeleteAction($new_file_id)
    {
        $test = $this->entityManager->getRepository(\Application\Entity\Test::class)
            ->findOneBy(['name' => 'Test One']);

        $file = $this->fileManager->getFileById($new_file_id);

        $this->dispatch('/test/file-delete/owner/'.$test->getId().'/file/'.$file->getId());

        $upload_dir_path = "./".$this->main_upload_folder.TestController::FILE_STORING_FOLDER;

        $this->assertFileNotExists($upload_dir_path.'/'.$file->getName());

        // File already deleted. Must return Exception.
        $this->fileManager->getFileById($new_file_id);

        // Проверяем, что файла нет (удален)
        $this->assertFileNotExists($upload_dir_path);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testShowImage()
    {
        $file_name = 'image_file_for_test.png';
        $path = \Application\Controller\TestController::FILE_STORING_FOLDER;

        $params = [
            'file_source'   => "./module/Application/test/files/".$file_name,
            'file_name'     => $file_name,
            'file_type'     => 'image/png',
            'file_size'     => filesize("./module/Application/test/files/".$file_name),
            'path'          => $path
        ];

        $file = $this->fileManager->write($params);
        $content = $this->fileManager->getContent($file);

        $this->dispatch('/test/file-show/'.$file->getId(), 'GET');
        $response = $this->getResponse()->getContent();

        $this->fileManager->delete($file->getId());

        $this->assertEquals($content, $response);
    }

    /**
     * @param $source_url
     * @param $destination
     */
    private function saveFile($source_url, $destination)
    {
        copy($source_url, $destination);
    }

    /**
     * @param $path
     */
    private function removeFile($path)
    {
        unlink($path);
    }
}
