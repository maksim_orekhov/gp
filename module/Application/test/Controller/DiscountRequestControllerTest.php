<?php

namespace ApplicationTest\Controller;

use Application\Controller\DiscountController;
use Application\Controller\DiscountRequestController;
use Application\Entity\DealAgent;
use Application\Entity\Discount;
use Application\Entity\DiscountConfirm;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\Refund;
use Application\Entity\Tribunal;
use Application\Form\DiscountForm;
use Application\Form\DiscountRequestForm;
use Application\Form\RequestConfirmForm;
use Application\Form\RequestRefuseForm;
use Application\Service\Dispute\DisputeManager;
use Core\Service\Base\BaseRoleManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\Deal;
use Application\Entity\User;
use CoreTest\ViewVarsTrait;
use Zend\Form\Element;

/**
 * Class DiscountRequestControllerTest
 * @package ApplicationTest\Controller
 */
class DiscountRequestControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_DEAL_WITHOUT_DISPUTE_NAME = 'Сделка Debit Matched';
    const TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME = 'Сделка Покупатель направил запрос на Скидку';
    const TEST_DEAL_WITH_DISPUTE_NAME = 'Сделка Спор открыт';
    const TEST_DEAL_WITH_REFUSE_NAME = 'Сделка Запрос на скидку отклонен';
    const DISCOUNT_AMOUNT = 50001;
    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        // Отключаем отправку уведомлений на email и телефон
        $this->resetConfigParameters();

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->disputeManager = $serviceManager->get(DisputeManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
    $this->entityManager->getConnection()->beginTransaction();
    }

    private function resetConfigParameters()
    {
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $config['sms_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    public function tearDown() {
        // Transaction rollback
   $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// getList

    // Неавторизованный

    /**
     * Недоступность /discount/request (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/discount/request', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request');
        $this->assertRedirectTo('/login?redirectUrl=/discount/request');
    }

    /**
     * Для Ajax
     * Недоступность /discount/request (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request');
        $this->assertRedirectTo('/login?redirectUrl=/discount/request');
    }

    // Авторизованный

    /**
     * Доступность /discount/request (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetListCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/discount/request', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request');
        // Что отдается в шаблон
        $this->assertArrayHasKey('discount_requests', $view_vars);
        $this->assertGreaterThan(0, count($view_vars['discount_requests']));
    }

    /**
     * Для Ajax
     * Доступность /discount/request (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetListCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('DiscountRequests', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('discount_requests', $data['data']);
        $this->assertArrayHasKey('paginator', $data['data']);
    }

    /**
     * Недоступность /discount/request (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetListCanNotBeAccessedByVerifiedUser()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/discount/request', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Ajax
     * Недоступность /discount/request (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group request-request
     */
    public function testGetListCanNotBeAccessedByVerifiedUserForAjax()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request');
        $this->assertRedirectTo('/not-authorized');
    }


    /////////////// get

    // Неавторизованный

    /**
     * Недоступность /discount/request/[id] (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        $this->dispatch('/discount/request/'.$discountRequest->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-single');
        $this->assertRedirectTo('/login?redirectUrl=/discount/request/'.$discountRequest->getId());
    }

    /**
     * Для Ajax
     * Недоступность /discount/request/[id] (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request/'.$discountRequest->getId(), null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-single');
        $this->assertRedirectTo('/login?redirectUrl=/discount/request/'.$discountRequest->getId());
    }

    // Авторизованный

    /**
     * Недоступность /discount/request/[id] (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        $this->dispatch('/discount/request/'.$discountRequest->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /discount/request/[id] (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request/'.$discountRequest->getId(), 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Не оператор

    /**
     * Доступность /discount/request/[id] (GET на get) не автору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetCanBeAccessedByNotAuthor()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        /** @var DealAgent $author */
        $author = $discountRequest->getAuthor();

        if ($author === $deal->getCustomer()) {
            /** @var DealAgent $notAuthor */
            $notAuthor = $deal->getContractor();
        } else {
            /** @var DealAgent $notAuthor */
            $notAuthor = $deal->getCustomer();
        }

        /** @var User $user */
        $user = $notAuthor->getUser();
        // Назначаем роль Verified не автору
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем
        $this->login($user->getLogin());

        $this->dispatch('/discount/request/'.$discountRequest->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-single');
        // Что отдается в шаблон
        $this->assertArrayHasKey('discountRequest', $view_vars);
        $this->assertEquals($discountRequest->getAmount(), $view_vars['discountRequest']['amount']);
        $this->assertArrayHasKey('discountConfirmForm', $view_vars);
        $this->assertInstanceOf(RequestConfirmForm::class, $view_vars['discountConfirmForm']);
        $this->assertArrayHasKey('discountRefuseForm', $view_vars);
        $this->assertInstanceOf(RequestRefuseForm::class, $view_vars['discountRefuseForm']);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertEquals($dispute->getId(), $view_vars['dispute']['id']);
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertEquals($deal->getNumber(), $view_vars['deal']['number']);
    }

    /**
     * Для Ajax
     * Доступность /discount/request/[id] (GET на get) не автору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetCanBeAccessedByNotAuthorForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        /** @var DealAgent $author */
        $author = $discountRequest->getAuthor();

        if ($author === $deal->getCustomer()) {
            /** @var DealAgent $notAuthor */
            $notAuthor = $deal->getContractor();
        } else {
            /** @var DealAgent $notAuthor */
            $notAuthor = $deal->getCustomer();
        }

        /** @var User $user */
        $user = $notAuthor->getUser();
        // Назначаем роль Verified не автору
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем
        $this->login($user->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request/'.$discountRequest->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('DiscountRequest single', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('discountRequest', $data['data']);
        $this->assertArrayHasKey('dispute', $data['data']);
        $this->assertEquals($dispute->getId(), $data['data']['dispute']['id']);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($deal->getNumber(), $data['data']['deal']['number']);
    }

    /**
     * Доступность /discount/request/[id] (GET на get) не автору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetCanNotBeAccessedByAuthor()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        /** @var DealAgent $author */
        $author = $discountRequest->getAuthor();
        /** @var User $user */
        $user = $author->getUser();

        // Назначаем роль Verified автору
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем
        $this->login($user->getLogin());

        $this->dispatch('/discount/request/'.$discountRequest->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Доступность /discount/request/[id] (GET на get) не автору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testGetCanNotBeAccessedByAuthorForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        /** @var DealAgent $author */
        $author = $discountRequest->getAuthor();
        /** @var User $user */
        $user = $author->getUser();

        // Назначаем роль Verified автору
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем
        $this->login($user->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request/'.$discountRequest->getId(), null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /////////////// create

    // Неавторизованный

    /**
     * Ajax
     * Недоступность /dispute/:idDispute/discount/request/create (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateCanNotBeAccessedByNonAuthorised()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        $this->assertRedirectTo('/login?redirectUrl=/dispute/'.$dispute->getId().'/discount/request/create');
    }

    /**
     * Для Ajax
     * Недоступность /dispute/:idDispute/discount/request/create (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        $this->assertRedirectTo('/login?redirectUrl=/dispute/'.$dispute->getId().'/discount/request/create');
    }

    // Авторизованный

    /**
     * Недоступность /discount/request (POST на create) Оператору по HTTP (404)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        // Экшен только для Ajax, поэтому не важно какие данные - не пропустит
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/discount/request', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request');
    }

    /**
     * Для Ajax
     * Недоступность /discount/request (POST на create) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $amount = 25000;

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $amount,
            'csrf'              => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request', 'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * /dispute/:idDispute/discount/request/create
     * Доступность покупателем сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     * @throws \Exception
     */
    public function testAllowedCreateFormForCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('discountRequestForm', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);
    }

    /**
     * /dispute/:idDispute/discount/request/create
     *
     * Не доступность продовцу сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     * @throws \Exception
     */
    public function testNotAllowedCreateFormForContractor()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        /** @var User $user */
        $user = $deal->getContractor()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
    }

    /**
     * Для Ajax
     * Доступность /discount/request (POST валидный на create) покупателем сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateWithValidPostForCustomerForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $this->assertNotNull($deal->getDispute());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $amount = 25000;

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $amount,
            'csrf'              => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Discount request created', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('discountRequest', $data['data']);
        $this->assertEquals($amount, $data['data']['discountRequest']['amount']);

        // Поверяем, что DiscountRequest не создался
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $deal->getDispute()));
        $this->assertNotNull($discountRequest);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных участником сделки на /discount/request
     * (POST невалидный на create)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateWithInvalidPostForVerifiedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $this->assertNotNull($deal->getDispute());

        $amount = 25000;

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $amount,
            'csrf'              => 'jhjhjhjj6565gfghfg3r3434434'
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid form data', $data['message']);

        // Поверяем, что DiscountRequest не создался
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $deal->getDispute()));
        $this->assertNull($discountRequest);
    }

    /**
     * Для Ajax
     * Отправка на /discount/request (POST валидный на create) НЕ участником сделки
     * У сделки есть Спор!
     *
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateWithValidPostByAlienAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Verified пользователю operator
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        // Есть спор
        $this->assertNotNull($deal->getDispute());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $amount = 25000;

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $amount,
            'csrf'              => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request');
        $this->assertRedirectTo('/not-authorized');

        // Поверяем, что DiscountRequest не создался
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $deal->getDispute()));
        $this->assertNull($discountRequest);
    }

    /**
     * Для Ajax
     * Отправка запроса на скидку, превышающую сумму сделки на /discount/request
     * (POST невалидный на create)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateWithTooBigAmountVerifiedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $this->assertNotNull($deal->getDispute());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Сумма сделки + 500
        $amount = (int)$deal->getPayment()->getDealValue() + 500;

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $amount,
            'csrf'              => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid form data', $data['message']);

        // Поверяем, что DiscountRequest не создался
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $deal->getDispute()));
        $this->assertNull($discountRequest);
    }

    /**
     * Для Ajax
     * Отправка запроса на скидку к сделке без Спора /discount/request
     * (POST невалидный на create)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateForDealWithoutDisputeForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITHOUT_DISPUTE_NAME);

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Нет спора
        $this->assertNull($deal->getDispute());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $amount = 5000;

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $amount,
            'csrf'              => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Deal has no dispute', $data['message']);

        // Поверяем, что DiscountRequest не создался
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $deal->getDispute()));
        $this->assertNull($discountRequest);
    }


    /////////////// createForm

    // Неавторизованный

    /**
     * Недоступность /dispute/:idDispute/discount/request/create (GET на createFormAction)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorised()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        $this->assertRedirectTo('/login?redirectUrl=/dispute/'.$dispute->getId().'/discount/request/create');
    }

    /**
     * Для Ajax
     * Недоступность /dispute/:idDispute/discount/request/create (GET на createFormAction) по Ajax
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $isXmlHttpRequest = true;
        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        $this->assertRedirectTo('/login?redirectUrl=/dispute/'.$dispute->getId().'/discount/request/create');
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /dispute/:idDispute/discount/request/create (GET на createFormAction) по Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $isXmlHttpRequest = true;
        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Доступность /dispute/:idDispute/discount/create (GET на createFormAction) участнику сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateFormCanBeAccessedByOperator()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('discountRequestForm', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertEquals($dispute->getId(), $view_vars['dispute']['id']);
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertEquals($deal->getId(), $view_vars['deal']['id']);
    }

    /**
     * /dispute/:idDispute/discount/create (POST валидный на create) участником сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateFormWithValidPost()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $this->assertNotNull($dispute);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $amount = 25000;

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $amount,
            'csrf'              => $csrf
        ];

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());

        // Поверяем, что DiscountRequest не создался
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $dispute));
        $this->assertNotNull($discountRequest);
        $this->assertEquals($amount, $discountRequest->getAmount());
    }

    /**
     * Отправка заведомо невалидных данных участником сделки на /dispute/:idDispute/discount/create
     * (POST невалидный на create)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateWithInvalidPostForVerified()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $this->assertNotNull($deal->getDispute());

        $amount = 25000;

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $amount,
            'csrf'              => 'jhjhjhjj6565gfghfg3r3434434'
        ];

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('discountRequestForm', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertEquals($dispute->getId(), $view_vars['dispute']['id']);
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertEquals($deal->getId(), $view_vars['deal']['id']);

        // Поверяем, что DiscountRequest не создался
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $deal->getDispute()));
        $this->assertNull($discountRequest);
    }

    /**
     * Для Ajax
     * Отправка на /discount/request (POST валидный на create) НЕ участником сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateWithValidPostByAlien()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Verified пользователю operator
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        // Есть спор
        $this->assertNotNull($deal->getDispute());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $amount = 25000;

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $amount,
            'csrf'              => $csrf
        ];

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        $this->assertRedirectTo('/not-authorized');

        // Поверяем, что DiscountRequest не создался
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $deal->getDispute()));
        $this->assertNull($discountRequest);
    }

    /**
     * Отправка запроса на скидку, превышающую сумму сделки на /discount/request
     * (POST невалидный на create)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testCreateWithTooBigAmountByVerified()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $this->assertNotNull($deal->getDispute());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Сумма сделки + 500
        $amount = (int)$deal->getPayment()->getDealValue() + 500;

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $amount,
            'csrf'              => $csrf
        ];

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('discountRequestForm', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertEquals($dispute->getId(), $view_vars['dispute']['id']);
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertEquals($deal->getId(), $view_vars['deal']['id']);

        // Поверяем, что DiscountRequest не создался
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $deal->getDispute()));
        $this->assertNull($discountRequest);
    }

    /**
     * Попытка создать Request при наличии Discount у спора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group discount-request
     */
    public function testDiscountRequestWithDealDisputeAndDiscount()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        // Проверяем, что спора нет
        $this->assertNotNull($dispute);

        $discount_amount = 1000;

        /** @var Discount $discount */
        $discount = $this->createDiscountForDispute($dispute, $discount_amount);
        // Проверяем, что Discount создан
        $this->assertNotNull($dispute->getId());

        // Обновляем Dispute
        /** @var Dispute $dispute */
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->findOneBy(array('id' => $dispute->getId()));

        // Проверяем, что у Спора уже есть скидка
        $this->assertNotNull($dispute->getDiscount());

        // Кастомер
        $customer = $deal->getCustomer()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $customer);
        $this->login($customer->getLogin());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $discount_amount,
            'csrf'              => $csrf
        ];

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Попытка создать Request при наличии Tribunal у спора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group discount-request
     */
    public function testDiscountRequestWithDealDisputeAndTribunal()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        // Проверяем, что спора нет
        $this->assertNotNull($dispute);

        $discount_amount = 1000;

        // Создаем Tribunal
        /** @var Tribunal $tribunal */
        $tribunal = $this->createTribunalForDispute($dispute);
        // Проверяем, что Tribunal создан
        $this->assertNotNull($tribunal);

        // Обновляем Dispute
        /** @var Dispute $dispute */
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->findOneBy(array('id' => $dispute->getId()));

        // Проверяем, что у Спора уже есть Трибунал
        $this->assertNotNull($dispute->getTribunal());

        // Кастомер
        $customer = $deal->getCustomer()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $customer);
        $this->login($customer->getLogin());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $discount_amount,
            'csrf'              => $csrf
        ];

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'GET', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Попытка создать Request при наличии Refund у спора
     *
     * @group deal
     * @group discount-request
     */
    public function testDiscountRequestWithDealDisputeAndRefund()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        // Проверяем, что спора нет
        $this->assertNotNull($dispute);

        $discount_amount = 1000;

        // Создаем Refund
        /** @var Refund $refund */
        $refund = $this->createRefundForDispute($dispute);
        // Проверяем, что Refund создан
        $this->assertNotNull($refund);

        // Обновляем Dispute
        /** @var Dispute $dispute */
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->findOneBy(array('id' => $dispute->getId()));

        // Проверяем, что у Спора уже есть Возврат
        $this->assertNotNull($dispute->getRefund());

        // Кастомер
        $customer = $deal->getCustomer()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $customer);
        $this->login($customer->getLogin());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal'              => $deal->getId(),
            'type_form'         => DiscountRequestForm::TYPE_FORM,
            'amount_discount'   => $discount_amount,
            'csrf'              => $csrf
        ];

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create', 'GET', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-create');
        $this->assertRedirectTo('/not-authorized');
    }


    /////////////// formInit (экшен только для Ajax!)

    // Неавторизованный

    /**
     * Недоступность /discount/request/form-init (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testFormInitCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/discount/request/form-init', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-init');
        $this->assertRedirectTo('/login?redirectUrl=/discount/request/form-init');
    }

    /**
     * Для Ajax
     * Недоступность /discount/request/form-init (GET на formInitAction)
     * не авторизованному пользователю по Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testFormInitCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request/form-init', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-init');
        $this->assertRedirectTo('/login?redirectUrl=/discount/request/form-init');
    }

    // Авторизованный

    /**
     * Для Ajax
     * Доступность /discount/request/form-init (GET на formInitAction) для Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testFormInitWithParamByOperatorForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Кастомер
        $customer = $deal->getCustomer()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $customer);
        $this->login($customer->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request/form-init', 'GET',
            ['idDispute' => $dispute->getId(), 'idRequest' => $discountRequest->getId()],
            $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertEquals($customer->getLogin(), $data['data']['user']['login']);
        $this->assertArrayHasKey('dispute', $data['data']);
        $this->assertEquals($dispute->getReason(), $data['data']['dispute']['reason']);
        $this->assertArrayHasKey('discountRequest', $data['data']);
        $this->assertEquals($discountRequest->getAmount(), $data['data']['discountRequest']['amount']);
        $this->assertArrayHasKey('max_discount', $data['data']);
    }

    /**
     * Ajax
     * Недоступность /discount/request/form-init (GET на formInitAction)
     * авторизованному с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testFormInitCanNotBeAccessedByVerifiedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request/form-init', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-request-form-init');
        $this->assertRedirectTo('/not-authorized');
    }


    /////////////// discountConfirm

    // Неавторизованный

    /**
     * Недоступность /deal/discount/request/:idRequest/confirm (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountConfirmCanNotBeAccessedByNonAuthorised()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/confirm', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-confirm');
        $this->assertRedirectTo('/login?redirectUrl=/deal/discount/request/'.$discountRequest->getId().'/confirm');
    }

    /**
     * Ajax
     * Недоступность /deal/discount/request/:idRequest/confirm (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountConfirmCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/confirm', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-confirm');
        $this->assertRedirectTo('/login?redirectUrl=/deal/discount/request/'.$discountRequest->getId().'/confirm');
    }

    /**
     * Недоступность /deal/discount/request/:idRequest/confirm (GET на formInitAction)
     * автору запроса по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountConfirmCanNotBeAccessedAuthorOfRequest()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Кастомер
        $customer = $deal->getCustomer()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $customer);
        $this->login($customer->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/confirm', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-confirm');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Ajax
     * Недоступность /deal/discount/request/:idRequest/confirm (GET на formInitAction)
     * автору запроса
     *
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountConfirmCanNotBeAccessedAuthorOfRequestForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Кастомер
        $customer = $deal->getCustomer()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $customer);
        $this->login($customer->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/confirm', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-confirm');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /deal/discount/request/:idRequest/confirm (GET на formInitAction)
     * не автору запроса по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountConfirmCanBeAccessedByNotAuthorOfRequest()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Контрактор
        $contractor = $deal->getContractor()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $contractor);
        $this->login($contractor->getLogin());

        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/confirm');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-confirm');

        // Что отдается в шаблон
        $this->assertArrayHasKey('discountConfirmForm', $view_vars);
        $this->assertInstanceOf(RequestConfirmForm::class, $view_vars['discountConfirmForm']);
        $this->assertArrayHasKey('discountRequest', $view_vars);
        $this->assertEquals($discountRequest->getId(), $view_vars['discountRequest']['id']);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertEquals($dispute->getId(), $view_vars['dispute']['id']);
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertEquals($deal->getId(), $view_vars['deal']['id']);
    }

    /**
     * POST на /deal/discount/request/:idRequest/confirm (GET на formInitAction)
     * не автору запроса по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountConfirmValidPostByNotAuthorOfRequest()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Контрактор
        $contractor = $deal->getContractor()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $contractor);
        $this->login($contractor->getLogin());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'type_form'  => RequestConfirmForm::TYPE_FORM,
            'csrf'       => $csrf
        ];

        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/confirm', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-confirm');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());

        // Поверяем, что DiscountRequest законфёрмился
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $dispute));
        /** @var DiscountConfirm $confirm */
        $confirm = $discountRequest->getConfirm();
        $this->assertInstanceOf(DiscountConfirm::class, $confirm);
        $this->assertTrue($confirm->isStatus());
    }

    /**
     * Ajax
     * POST на /deal/discount/request/:idRequest/confirm (GET на formInitAction)
     * не автору запроса по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountConfirmValidPostByNotAuthorOfRequestByAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Контрактор
        $contractor = $deal->getContractor()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $contractor);
        $this->login($contractor->getLogin());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'type_form'  => RequestConfirmForm::TYPE_FORM,
            'csrf'       => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-confirm');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('DiscountRequest confirmed', $data['message']);

        // Поверяем, что DiscountRequest законфёрмился
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $dispute));
        /** @var DiscountConfirm $confirm */
        $confirm = $discountRequest->getConfirm();
        $this->assertInstanceOf(DiscountConfirm::class, $confirm);
        $this->assertTrue($confirm->isStatus());
    }

    /////////////// discountRefuse

    // Неавторизованный

    /**
     * Недоступность /deal/discount/request/:idRequest/refuse (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountRefuseCanNotBeAccessedByNonAuthorised()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/refuse', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-refuse');
        $this->assertRedirectTo('/login?redirectUrl=/deal/discount/request/'.$discountRequest->getId().'/refuse');
    }

    /**
     * Ajax
     * Недоступность /deal/discount/request/:idRequest/refuse (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountRefuseCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/refuse', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-refuse');
        $this->assertRedirectTo('/login?redirectUrl=/deal/discount/request/'.$discountRequest->getId().'/refuse');
    }

    /**
     * Недоступность /deal/discount/request/:idRequest/refuse (GET на formInitAction)
     * автору запроса по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountRefuseCanNotBeAccessedAuthorOfRequest()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Кастомер
        $customer = $deal->getCustomer()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $customer);
        $this->login($customer->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/refuse', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-refuse');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Ajax
     * Недоступность /deal/discount/request/:idRequest/refuse (GET на formInitAction)
     * автору запроса
     *
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountRefuseCanNotBeAccessedAuthorOfRequestForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Кастомер
        $customer = $deal->getCustomer()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $customer);
        $this->login($customer->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/refuse', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-refuse');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /deal/discount/request/:idRequest/refuse (GET на formInitAction)
     * не автору запроса по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountRefuseCanBeAccessedByNotAuthorOfRequest()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Контрактор
        $contractor = $deal->getContractor()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $contractor);
        $this->login($contractor->getLogin());

        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/refuse');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-refuse');

        // Что отдается в шаблон
        $this->assertArrayHasKey('discountRefuseForm', $view_vars);
        $this->assertInstanceOf(RequestRefuseForm::class, $view_vars['discountRefuseForm']);
        $this->assertArrayHasKey('discountRequest', $view_vars);
        $this->assertEquals($discountRequest->getId(), $view_vars['discountRequest']['id']);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertEquals($dispute->getId(), $view_vars['dispute']['id']);
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertEquals($deal->getId(), $view_vars['deal']['id']);
    }

    /**
     * POST на /deal/discount/request/:idRequest/refuse (GET на formInitAction)
     * не автору запроса по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountRefuseValidPostByNotAuthorOfRequest()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Контрактор
        $contractor = $deal->getContractor()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $contractor);
        $this->login($contractor->getLogin());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'type_form'  => RequestRefuseForm::TYPE_FORM,
            'csrf'       => $csrf
        ];

        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/refuse', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-refuse');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());

        // Поверяем, что DiscountRequest отвергнут
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $dispute));
        /** @var DiscountConfirm $confirm */
        $confirm = $discountRequest->getConfirm();
        $this->assertInstanceOf(DiscountConfirm::class, $confirm);
        $this->assertFalse($confirm->isStatus());
    }

    /**
     * Ajax
     * POST на /deal/discount/request/:idRequest/refuse (GET на formInitAction)
     * не автору запроса по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     */
    public function testDiscountRefuseValidPostByNotAuthorOfRequestByAjax()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Контрактор
        $contractor = $deal->getContractor()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $contractor);
        $this->login($contractor->getLogin());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'type_form'  => RequestRefuseForm::TYPE_FORM,
            'csrf'       => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/refuse', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertMatchedRouteName('deal-discount-refuse');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('DiscountRequest rejected', $data['message']);

        // Поверяем, что DiscountRequest отвергнут
        $discountRequest = $this->entityManager->getRepository(DiscountRequest::class)
            ->findOneBy(array('dispute' => $dispute));
        /** @var DiscountConfirm $confirm */
        $confirm = $discountRequest->getConfirm();
        $this->assertInstanceOf(DiscountConfirm::class, $confirm);
        $this->assertFalse($confirm->isStatus());
    }

    /**
     * 1 раз предложение по скидке было отклонено
     * создаем еще одно предложение по скидке
     * заходим за контрактора в этот раз принимаем
     * заходим за оператора сравниваем, изминилось ли цена в принятом предложении
     * @throws \Exception
     */
    public function testAcceptanceOfaTransactionAfterTheFirstRefusal()
    {
        $discount_amount = self::DISCOUNT_AMOUNT;
        ////1 этап создаем реквест
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_REFUSE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        /** @var DealAgent $author */
        $author = $dispute->getAuthor();
        /** @var Discount $discount */
        $discount = $dispute->getDiscount();

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $this->assertNotNull($deal->getDispute());

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal' => $deal->getId(),
            'type_form' => DiscountRequestForm::TYPE_FORM,
            'amount_discount' => $discount_amount,
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount/request', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountRequestController::class);
        $this->assertEquals('SUCCESS', $data['status']);

        ///2 этап логинимся под контрактором принимаем реквест
        $this->logout();
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_REFUSE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);
        // Контрактор
        $contractor = $deal->getContractor()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $contractor);
        $this->login($contractor->getLogin());

        $this->dispatch('/deal/discount/request/' . $discountRequest->getId() . '/confirm');


        $this->assertResponseStatusCode(200);

        ///3 этап разлогиниваемся и залогиниваемся как оператор и проверяем, что ему пришла верная сумма
        $this->logout();
        $this->login('operator');

        $this->dispatch('/deal/show/' . $deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertTrue($view_vars['data']['discountRequest']['amount'] == self::DISCOUNT_AMOUNT);

    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * Разлогиневаем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function logout()
    {
        $this->baseAuthManager->logout();
    }

    /**
     * @param null $deal_name
     * @param bool $is_dispute_closed
     * @return Dispute
     */
    private function createTestDispute($deal_name = null, $is_dispute_closed = false)
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);

        $dispute = new Dispute();
        $dispute->setReason('Спор по сделке '.$deal->getId().' для тестирования');
        $dispute->setCreated(new \DateTime());
        $dispute->setClosed($is_dispute_closed);
        $dispute->setAuthor($deal->getCustomer());
        $dispute->setDeal($deal);

        $this->entityManager->persist($dispute);

        $deal->setDispute($dispute);

        $this->entityManager->persist($deal);

        $this->entityManager->flush();

        return $dispute;
    }

    /**
     * @param null $deal_name
     * @return Deal
     */
    private function getDealForTest($deal_name = null)
    {
        if (!$deal_name) $deal_name = self::TEXT_DEAL_NAME;

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        return $deal;
    }

    /**
     * @param Dispute $dispute
     * @param $discount_amount
     * @return Discount
     */
    private function createDiscountForDispute(Dispute $dispute, $discount_amount): Discount
    {
        $discount = new Discount();
        $discount->setCreated(new \DateTime());
        $discount->setDispute($dispute);
        $discount->setAmount($discount_amount);

        // @TODO Обязательны ли для теста связанные сущности? Пока не создаю.
        #$discount->setPaymentMethod();
        #$discount->addPaymentOrder();

        $this->entityManager->persist($discount);
        $this->entityManager->flush();

        return $discount;
    }

    /**
     * @param Dispute $dispute
     * @return Refund
     */
    private function createRefundForDispute(Dispute $dispute): Refund
    {
        $refund = new Refund();
        $refund->setCreated(new \DateTime());
        $refund->setDispute($dispute);

        // @TODO Обязательны ли для теста связанные сущности? Пока не создаю.
        #$refund->setPaymentMethod();
        #$refund->addPaymentOrder();

        $this->entityManager->persist($refund);
        $this->entityManager->flush();

        return $refund;
    }

    /**
     * @param Dispute $dispute
     * @return Tribunal
     */
    private function createTribunalForDispute(Dispute $dispute): Tribunal
    {
        $tribunal = new Tribunal();
        $tribunal->setCreated(new \DateTime());
        $tribunal->setDispute($dispute);

        $this->entityManager->persist($tribunal);
        $this->entityManager->flush();

        return $tribunal;
    }

    /**
 * @param Dispute $dispute
 * @param DealAgent $author
 * @param $discount_amount
 * @return DiscountRequest
 */
    private function createDiscountRequestForDispute(Dispute $dispute, DealAgent $author, $discount_amount): DiscountRequest
    {
        /** @var DiscountRequest $discountRequest */
        $discountRequest = new DiscountRequest();
        $discountRequest->setCreated(new \DateTime());
        $discountRequest->setDispute($dispute);
        $discountRequest->setAmount($discount_amount);
        $discountRequest->setAuthor($author);

        $this->entityManager->persist($discountRequest);
        $this->entityManager->flush();

        return $discountRequest;
    }


}