<?php

namespace ApplicationTest\Controller;

use Application\Controller\SoleProprietorController;
use Application\Entity\CivilLawSubject;
use Application\Entity\LegalEntity;
use Application\Entity\Role;
use Application\Entity\SoleProprietor;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseRoleManager;
use ModuleRbac\Entity\Interfaces\ModuleRbacRoleInterface;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Session\SessionManager;
use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use CoreTest\ViewVarsTrait;

/**
 * Class LegalEntityControllerForOperatorTest
 * @package ApplicationTest\Controller
 */
class SoleProprietorControllerForOperatorTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;
    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// getList

    // Неавторизованный

    /**
     * Недоступность /sole-proprietor (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/sole-proprietor', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor');
        $this->assertRedirectTo('/login?redirectUrl=/sole-proprietor');
    }

    /**
     * Для Ajax
     * Недоступность /sole-proprietor (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/sole-proprietor', null, [], $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /sole-proprietor(GET на getList) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNotOperator()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/sole-proprietor', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /sole-proprietor (GET на getList) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNotOperatorForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/sole-proprietor', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor');
        $this->assertRedirectTo('/not-authorized');
    }

    // Оператор

    /**
     * Доступность /sole-proprietor (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $this->dispatch('/sole-proprietor', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor');
        // Что отдается в шаблон
        $this->assertArrayHasKey('soleProprietors', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Для Ajax
     * Доступность /sole-proprietor (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/sole-proprietor', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('SoleProprietor collection', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('soleProprietors', $data['data']);
        $this->assertArrayHasKey('paginator', $data['data']);
        $this->assertGreaterThan(1, $data['data']['soleProprietors']);
    }

    /////////////// get

    // Неавторизованный

    /**
     * Недоступность /sole-proprietor/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => SoleProprietorControllerTest::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/sole-proprietor/'.$soleProprietor->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor-single');
        $this->assertRedirectTo('/login?redirectUrl=/sole-proprietor/'.$soleProprietor->getId());
    }

    /**
     * Для Ajax
     * Недоступность /sole-proprietor/id  (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => SoleProprietorControllerTest::TEST_SOLE_PROPRIETOR_1_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/sole-proprietor/'.$soleProprietor->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Не Оператор

    /**
     * Недоступность /sole-proprietor/id (GET на get) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOperator()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => SoleProprietorControllerTest::TEST_SOLE_PROPRIETOR_1_NAME));

        $this->dispatch('/sole-proprietor/'.$soleProprietor->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /sole-proprietor/id (GET на get) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOperatorByGetForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => SoleProprietorControllerTest::TEST_SOLE_PROPRIETOR_1_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/sole-proprietor/'.$soleProprietor->getId(), null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Оператор

    /**
     * Доступность /sole-proprietor/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOperator()
    {
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => SoleProprietorControllerTest::TEST_SOLE_PROPRIETOR_1_NAME));

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/sole-proprietor/'.$soleProprietor->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor-single');
        // Что отдается в шаблон
        $this->assertArrayHasKey('soleProprietor', $view_vars);
        $this->assertArrayHasKey('paymentMethods', $view_vars);
        $this->assertArrayHasKey('civil_law_subject_id', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Для Ajax
     * Доступность /sole-proprietor/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group sole-proprietor
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOperatorForAjax()
    {
        $soleProprietor = $this->entityManager->getRepository(SoleProprietor::class)
            ->findOneBy(array('name' => SoleProprietorControllerTest::TEST_SOLE_PROPRIETOR_1_NAME));

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/sole-proprietor/'.$soleProprietor->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $soleProprietorFromData = $data['data']['soleProprietor'];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SoleProprietorController::class);
        $this->assertMatchedRouteName('sole-proprietor-single');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('SoleProprietor single', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('soleProprietor', $data['data']);
        $this->assertEquals(SoleProprietorControllerTest::TEST_SOLE_PROPRIETOR_1_NAME, $soleProprietorFromData['name']);
        $this->assertEquals($soleProprietorFromData['id'], $soleProprietor->getId());
    }


    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var ModuleRbacRoleInterface $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $login
     * @return LegalEntity
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createNewTestLegalEntity($login = 'test')
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $login));

        // Create CivilLawSubject
        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern(true);
        $civilLawSubject->setCreated(new \DateTime());

        $this->entityManager->persist($civilLawSubject);

        // Create LegalEntity

        $legalEntity = new LegalEntity();
        $legalEntity->setCivilLawSubject($civilLawSubject);
        $legalEntity->setName('Test Company');
        $legalEntity->setKpp('123456');
        $legalEntity->setInn('123456');
        $legalEntity->setLegalAddress('Ямского поля, дом 2');
        $legalEntity->setOgrn('3456778');
        $legalEntity->setPhone('(495) 123-34-56');

        $civilLawSubject->setLegalEntity($legalEntity);

        $this->entityManager->persist($legalEntity);

        $this->entityManager->flush();

        return $legalEntity;
    }
}