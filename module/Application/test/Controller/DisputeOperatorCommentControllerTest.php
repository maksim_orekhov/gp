<?php

namespace ApplicationTest\Controller;

use Application\Controller\DisputeController;
use Application\Controller\DisputeOperatorCommentController;
use Application\Entity\Dispute;
use Application\Entity\User;
use Application\Form\DisputeCommentForm;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Dispute\DisputeOperatorCommentManager;
use ModuleMessage\Entity\Message;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use Application\Service\Deal\DealManager;
use CoreTest\ViewVarsTrait;

/**
 * Class DisputeOperatorCommentControllerTest
 * @package ApplicationTest\Controller
 */
class DisputeOperatorCommentControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const MINIMUM_DELIVERY_PERIOD = 7;
    const TEXT_DEAL_NAME = 'Сделка Debit Matched';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var DisputeOperatorCommentManager
     */
    private $disputeOperatorCommentManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        // Отключаем отправку уведомлений на email
        $this->resetConfigParameters();

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->disputeManager = $serviceManager->get(DisputeManager::class);
        $this->disputeOperatorCommentManager = $serviceManager->get(DisputeOperatorCommentManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /**
     * Доступность /dispute/:idDispute//operator-comment/create Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group comment
     */
    public function testCreateFormActionCanBeAccessedByOperator()
    {
        /** @var Dispute $dispute */
        $dispute = $this->createTestDispute(self::TEXT_DEAL_NAME);

        $this->login('operator');

        $this->dispatch('/dispute/'.$dispute->getId().'/operator-comment/create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DisputeOperatorCommentController::class);
        $this->assertControllerClass('DisputeOperatorCommentController');
        $this->assertMatchedRouteName('dispute-operator-comment-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('disputeCommentForm', $view_vars);
        $this->assertTrue($view_vars['disputeCommentForm'] instanceof DisputeCommentForm);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-comment-create-form');
    }

    /**
     * Доступность /dispute/operator-comment/:idComment/edit Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group comment
     */
    public function testEditFormActionCanBeAccessedByOperator()
    {
        /** @var Dispute $dispute */
        $dispute = $this->createTestDispute(self::TEXT_DEAL_NAME);

        $this->login('operator');

        /* @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'operator'));

        $text = 'Тестовый комментарий к спору ' . $dispute->getId();

        $data = [
            'text' => $text,
            'dispute_id' => $dispute->getId()
        ];

        /** @var Message $comment */
        $comment = $this->createTestComment($user, $data, $dispute);

        $this->dispatch('/dispute/operator-comment/'.$comment->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DisputeOperatorCommentController::class);
        $this->assertControllerClass('DisputeOperatorCommentController');
        $this->assertMatchedRouteName('dispute-operator-comment-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('comment', $view_vars);
        $this->assertArrayHasKey('disputeCommentForm', $view_vars);
        $this->assertTrue($view_vars['disputeCommentForm'] instanceof DisputeCommentForm);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-comment-edit-form');
    }

    /**
     * Доступность /dispute/operator-comment/:idComment/delete Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group comment
     */
    public function testDeleteFormActionCanBeAccessedByOperator()
    {
        /** @var Dispute $dispute */
        $dispute = $this->createTestDispute(self::TEXT_DEAL_NAME);

        $this->login('operator');

        /* @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => 'operator'));

        $text = 'Тестовый комментарий к спору ' . $dispute->getId();

        $data = [
            'text' => $text,
            'dispute_id' => $dispute->getId()
        ];

        /** @var Message $comment */
        $comment = $this->createTestComment($user, $data, $dispute);

        $this->dispatch('/dispute/operator-comment/'.$comment->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DisputeOperatorCommentController::class);
        $this->assertControllerClass('DisputeOperatorCommentController');
        $this->assertMatchedRouteName('dispute-operator-comment-form-delete');
        // Что отдается в шаблон
        $this->assertArrayHasKey('comment', $view_vars);
        $this->assertArrayHasKey('deal_number', $view_vars);
        $this->assertArrayHasKey('deal_id', $view_vars);
        $this->assertArrayHasKey('dispute_id', $view_vars);
        $this->assertTrue($dispute->getId() == $view_vars['dispute_id']);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-comment-delete-form');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param null $deal_name
     * @return Dispute
     */
    private function createTestDispute($deal_name = null)
    {
        if (!$deal_name) $deal_name = self::TEXT_DEAL_NAME;

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $dispute = new Dispute();
        $dispute->setReason('Спор по сделке '.$deal->getId().' для тестирования');
        $dispute->setCreated(new \DateTime());
        $dispute->setClosed(false);
        $dispute->setAuthor($deal->getCustomer());
        $dispute->setDeal($deal);

        $this->entityManager->persist($dispute);

        $this->entityManager->flush();

        return $dispute;
    }

    private function createTestComment($author, $data, $dispute)
    {
        /** @var Message $comment */
        $comment = $this->disputeOperatorCommentManager
            ->createDisputeComment($author, $data, $dispute);

        return $comment;
    }
}