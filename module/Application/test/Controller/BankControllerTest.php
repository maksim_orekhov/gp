<?php

namespace ApplicationTest\Controller;

use ApplicationTest\Bootstrap;
use Core\Service\Base\BaseAuthManager;
use Zend\Session\SessionManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Controller\BankController;
use Zend\Stdlib\ArrayUtils;
use Zend\Form\Element;

class BankControllerTest extends AbstractHttpControllerTestCase
{
    use \Core\Provider\HttpRequestTrait;

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;
    /**
     * @var string
     */
    private $user_password;

    private $main_project_url;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->main_project_url = $config['main_project_url'];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank
     * @throws \Exception
     */
    public function testBankSearchActionCanNotBeAccessedByUnauthorized()
    {
        $this->dispatch('/bank/search', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(BankController::class);
        $this->assertControllerClass('BankController');
        $this->assertMatchedRouteName('bank');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank
     * @throws \Exception
     */
    public function testBankSearchActionCanNotBeAccessedByUnauthorizedByHttp()
    {
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/bank/search', 'GET');

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(BankController::class);
        $this->assertControllerClass('BankController');
        $this->assertMatchedRouteName('bank');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank
     * @throws \Exception
     */
    public function testBankSearchActionCanNotBeAccessedWithoutPostForAjax()
    {
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/bank/search', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(BankController::class);
        $this->assertMatchedRouteName('bank');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank
     * @throws \Exception
     */
    public function testBankSearchFormWithValidPostAndNonexistentBIK()
    {
        // Залогиниваем пользователя test
        $this->login();

        $csrf = new Element\Csrf('csrf');

        $postData = [
            'bik' => '011111111',
            'csrf' => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/bank/search', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankController::class);
        $this->assertControllerClass('BankController');
        $this->assertMatchedRouteName('bank');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('information on BIK not found', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertNull($data['data']);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank
     * @throws \Exception
     */
    public function testBankSearchFormWithValidPostAndExistentBIK()
    {
        // Залогиниваем пользователя test
        $this->login();

        $csrf = new Element\Csrf('csrf');

        $postData = [
            'bik' => '044525225',
            'csrf' => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/bank/search', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankController::class);
        $this->assertControllerClass('BankController');
        $this->assertMatchedRouteName('bank');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('information on BIK found successfully', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals('044525225', $data['data']['bank_bik']);
        $this->assertEquals('ПАО СБЕРБАНК', $data['data']['bank_name']);
    }

//    В экшене отсутвует какая-либо форма, нет валидации и, следовательно, проверки csrf
//    /**
//     * @runInSeparateProcess
//     * @preserveGlobalState disabled
//     *
//     * @group bank
//     * @throws \Exception
//     */
//    public function testBankSearchFormWithInvalidPostForAjax()
//    {
//        // Залогиниваем пользователя test
//        $this->login();
//
//        $postData = [
//            'bik' => '044525225',
//            #'csrf' => $csrf->getValue()
//        ];
//
//        $this->getRequest()->setContent(json_encode($postData));
//        $isXmlHttpRequest = true;
//        $this->dispatch('/bank/search', 'POST', $postData, $isXmlHttpRequest);
//        $data = json_decode($this->getResponse()->getContent(), true);
//
//        $this->assertResponseStatusCode(200);
//        $this->assertModuleName('application');
//        $this->assertControllerName(BankController::class);
//        $this->assertMatchedRouteName('bank');
//
//        $this->assertEquals('ERROR', $data['status']);
//        $this->assertEquals('Form is not valid', $data['message']);
//    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        $login = $login ?? $this->user_login;
        $password = $password ?? $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}