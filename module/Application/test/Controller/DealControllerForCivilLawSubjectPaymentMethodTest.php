<?php

namespace ApplicationTest\Controller;

use Application\Controller\DealController;
use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethod;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\User;
use Application\Fixture\PaymentMethodBankTransferFixtureLoader;
use Application\Service\Payment\PaymentMethodManager;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseRoleManager;
use CoreTest\ViewVarsTrait;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Entity\Role;
use Doctrine\Common\Collections\ArrayCollection;
use phpDocumentor\Reflection\Types\Array_;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\BankClient\BankClientManager;

/**
 * Class DealControllerForCreateTest
 * @package ApplicationTest\Controller
 */
class DealControllerForCivilLawSubjectPaymentMethodTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const MINIMUM_DELIVERY_PERIOD = 14;

    protected $traceError = true;
    /**
     * @var string
     */
    private $user_login;
    /**
     * @var string
     */
    private $user_password;
    /**
     * @var \Application\Entity\User
     */
    private $user;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;
    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;
    /**
     * @var PaymentMethodManager
     */
    public $paymentMethodManager;
    /**
     * @var BankClientManager
     */
    public $bankClientManager;
    /**
     * @var bool
     */
    private $disable_rollback = false;
    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        // Отключаем отправку уведомлений на email
        $this->resetConfigParameters();

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);
        $this->paymentMethodManager = $serviceManager->get(\Application\Service\Payment\PaymentMethodManager::class);
        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /**
     * Недоступность /deal/id/civil-law-subject_payment-method не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedToNonAuthorised()
    {
        $deal_name = 'Сделка Фикстура';
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        $this->dispatch('/deal/'.$deal->getId().'/civil-law-subject_payment-method', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
    }
    /**
     * Недоступность /deal/id/civil-law-subject_payment-method не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanToNonAuthorised()
    {
        $deal_name = 'Сделка Фикстура';
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        $this->dispatch('/deal/'.$deal->getId().'/civil-law-subject_payment-method', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
    }

    /**
     * Недоступность /deal/id/civil-law-subject_payment-method не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateAndSetThenFormIsNotValid()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self::SetNullsToPaymentAndCivil($deal_name);
        $isXmlHttpRequest = true;
        $postData = [
            'csrf' => '9d6352561d892fc16f7655cab9021c4e-1c0d57c35324971a98a8652e6da9b732',
            'civil_law_subject' =>
                [
                    'csrf' => '9d6352561d892fc16f7655cab9021c4e-1c0d57c35324971a98a8652e6da9b732',
                    'last_name' => 'Привет',
                    'first_name' => 'Ычвфывфыв',
                    'secondary_name' => 'Ыфвфывфыв',
                    'birth_date' => '11.11.1990',
                    'civil_law_subject_type' => 'natural_person',
                    'current_action' => 'form-natural-person',
                ],
            'payment_method' =>
                [
                    'csrf' => '9d6352561d892fc16f7655cab9021c4e-1c0d57c35324971a98a8652e6da9b732',
                    'name' => '',
                    'bik' => '044030653',
                    'bank_name' => 'СЕВЕРО-ЗАПАДНЫЙ БАНК ПАО СБЕРБАНК',
                    'account_number' => '12312433122131231232',
                    'corr_account_number' => '30101810500000000653',
                    'civil_law_subject_id' => '',
                    'payment_method_type' => 'bank_transfer',
                    'id' => '',
                    'current_action' => 'form-bank-transfer',
                ],
        ];
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject_payment-method', 'POST', $postData, $isXmlHttpRequest);
        $this->assertResponseStatusCode(200);
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        $this->assertTrue($deal->getPayment() instanceof Payment);

        $this->assertControllerName(DealController::class);
    }


    /**
     * Недоступность /deal/id/civil-law-subject_payment-method не авторизованному пользователю
     * @dataProvider
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateAndSetThenFormIsNotValidAll()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self::SetNullsToPaymentAndCivil($deal_name);
        $isXmlHttpRequest = true;
        $postData = [
            'csrf' => '9d6352561d892fc16f7655cab9021c4e-1c0d57c35324971a98a8652e6da9b732',
            'civil_law_subject' =>
                [
                    'csrf' => '9d6352561d892fc16f7655cab9021c4e-1c0d57c35324971a98a8652e6da9b732',
                    'last_name' => 'Привет',
                    'birth_date' => '11.11.1990',
                    'civil_law_subject_type' => 'natural_person',
                    'current_action' => 'form-natural-person',
                ],
            'payment_method' =>
                [
                    'csrf' => '9d6352561d892fc16f7655cab9021c4e-1c0d57c35324971a98a8652e6da9b732',
                    'name' => '',
                    'bik' => '044030653',
                    'corr_account_number' => '30101810500000000653',
                    'civil_law_subject_id' => '',
                    'payment_method_type' => 'bank_transfer',
                    'id' => '',
                    'current_action' => 'form-bank-transfer',
                ],
        ];
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject_payment-method', 'POST', $postData, $isXmlHttpRequest);
        $this->assertResponseStatusCode(200);
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        $this->assertTrue($deal->getPayment() instanceof Payment);

        $this->assertControllerName(DealController::class);
    }

    /**
     * Пробую посылать стрингу в ид. жду ошибку
     *
     * @group deal
     * @throws \Exception
     */
    public function testSetCivilLawSubjectToDealNotValidId()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self::SetNullsToCivil($deal_name);
        //   $this->login( $deal->getContractor()->getUser()->getLogin());
        $this->login('test');
        $deal->getContractor()->setCivilLawSubject(null);
        // Prepare object
        $this->entityManager->persist($deal);
        // Save to DB
        $this->entityManager->flush();
        $isXmlHttpRequest = true;
        $csrfElement = new Element\Csrf('csrf');
        $postData = [
            'csrf' =>   $csrfElement->getValue(),
            'civil_law_subject' => 'test'

        ];
        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject', 'POST', $postData, $isXmlHttpRequest);
        // $data = json_decode($this->getResponse()->getContent(), true);
        $data = json_decode($this->getResponse()->getContent(), true);


        $this->assertTrue($data['status']=='ERROR');
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-civil-law-subject');
        $this->assertActionName('createCivilLawSubject');
    }
    /**
     * Пробую посылать не верный массив в ид. жду ошибку
     *
     * @group deal
     * @throws \Exception
     */
    public function testSetCivilLawSubjectToDealNotValidArray()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self::SetNullsToCivil($deal_name);
        //   $this->login( $deal->getContractor()->getUser()->getLogin());
        $this->login('test');
        $deal->getContractor()->setCivilLawSubject(null);
        // Prepare object
        $this->entityManager->persist($deal);
        // Save to DB
        $this->entityManager->flush();
        $isXmlHttpRequest = true;
        $csrfElement = new Element\Csrf('csrf');
        $postData = [
            'csrf' =>   $csrfElement->getValue(),
            'civil_law_subject' => [
                'name' => 'test',
                'test' => 'test'
            ]

        ];
        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject', 'POST', $postData, $isXmlHttpRequest);
        // $data = json_decode($this->getResponse()->getContent(), true);
        $data = json_decode($this->getResponse()->getContent(), true);


        $this->assertTrue($data['status'] === 'ERROR');
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-civil-law-subject');
        $this->assertActionName('createCivilLawSubject');
    }

    /**
     * Недоступность /deal/id/civil-law-subject_payment-method не авторизованному пользователю
     *
     * @group deal
     * @throws \Exception
     */
    public function testSetPaymentMethodToDeal()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self:: SetNullsToPayment($deal_name);
        $user= $deal->getContractor()->getUser();
        $this->login($user->getLogin());

        // Prepare object
        $this->entityManager->persist($deal);
        // Save to DB
        $this->entityManager->flush();
        $csrfElement = new Element\Csrf('csrf');

        $paymentMethodBankTransfers = $this->paymentMethodManager->getAvailablePaymentMethods($user);

        /** @var PaymentMethodBankTransfer $paymentMethodBankTransfer */
        $paymentMethodBankTransfer = end($paymentMethodBankTransfers);

        $postData = [
            'csrf' => $csrfElement->getValue(),
            'payment_method' => $paymentMethodBankTransfer->getPaymentMethod()->getId()
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/' . $deal->getId() . '/payment-method', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        ///снова ищу эту сделку, и убеждаюсь что присетило
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $payment=$deal->getPayment();
        /** @var PaymentMethod $payment_method */
        $payment_method = $payment->getPaymentMethod();
        //я утверждаю, что засетило
        $this->assertTrue(is_array($data));
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertInstanceOf(PaymentMethod::class, $payment_method);
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-payment-method');
        $this->assertActionName('createPaymentMethod');
    }
    /**
     * Недоступность /deal/id/civil-law-subject_payment-method не авторизованному пользователю
     *
     * @group deal
     * @throws \Exception
     */
    public function testSetPaymentMethodToDealNotValid()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self::SetNullsToPayment($deal_name);
        $user= $deal->getContractor()->getUser();
        $this->login($user->getLogin());

        $deal->getContractor()->setCivilLawSubject(null);
        // Prepare object
        $this->entityManager->persist($deal);
        // Save to DB
        $this->entityManager->flush();
        $csrfElement = new Element\Csrf('csrf');
        $isXmlHttpRequest = true;
        /** @var  Array $payment_method */
        $payment_method = $this->paymentMethodManager->getAvailablePaymentMethods($user);
        /** @var PaymentMethod $payment_method */
        $payment_method = end($payment_method);
        $postData = [
            'csrf' => $csrfElement->getValue(),
            'payment_method' => 'test'

        ];
        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/' . $deal->getId() . '/payment-method', 'POST', $postData, $isXmlHttpRequest);
        ///снова ищу эту сделку, и убеждаюсь что присетило
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment=$deal->getPayment();
        /** @var PaymentMethod $payment_method */
        $payment_method = $payment->getPaymentMethod();
        //я утверждаю, что не засетило
        $this->assertNotTrue($payment_method instanceof PaymentMethod);
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-payment-method');
        $this->assertActionName('createPaymentMethod');
    }
    /**
     * Недоступность /deal/id/civil-law-subject_payment-method не авторизованному пользователю
     *
     * @group deal
     * @throws \Exception
     */
    public function testSetPaymentMethodToDealNotValidArraySend()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self::SetNullsToPayment($deal_name);
        $user= $deal->getContractor()->getUser();
        $this->login($user->getLogin());

        $deal->getContractor()->setCivilLawSubject(null);
        // Prepare object
        $this->entityManager->persist($deal);
        // Save to DB
        $this->entityManager->flush();
        $csrfElement = new Element\Csrf('csrf');
        $isXmlHttpRequest = true;
        /** @var  Array $payment_method */
        $payment_method = $this->paymentMethodManager->getAvailablePaymentMethods($user);
        /** @var PaymentMethod $payment_method */
        $payment_method = end($payment_method);
        $postData = [
            'csrf' => $csrfElement->getValue(),
            'payment_method' => [
                'payment_method_type' => 'bank_transfer',
                'account_number' => 30101810600000000786,
                'bank_name' => 'ФИЛИАЛ "САНКТ-ПЕТЕРБУРГСКИЙ" АО "АЛЬФА-БАНК"',
                'bik' => '044030786',
                'civil_law_subject_id' => 85,
                'corr_account_number' => 30101810600000000786,
                'id' ,
                'name',
            ]

        ];

        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/' . $deal->getId() . '/payment-method', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        ///снова ищу эту сделку, и убеждаюсь что присетило
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment=$deal->getPayment();
        /** @var PaymentMethod $payment_method */
        $payment_method = $payment->getPaymentMethod();
        //я утверждаю, что не засетило
        $this->assertNotTrue($payment_method instanceof PaymentMethod);
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-payment-method');
        $this->assertActionName('createPaymentMethod');
    }

    /**
     * Недоступность /deal/id/civil-law-subject_payment-method не авторизованному пользователю
     *
     * @group deal
     * @throws \Exception
     */
    public function testForBugCompletePaymentMetodArraySend()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self::SetNullsToPayment($deal_name);
        $user= $deal->getContractor()->getUser();
        $this->login($user->getLogin());

        $deal->getContractor()->setCivilLawSubject(null);
        // Prepare object
        $this->entityManager->persist($deal);
        // Save to DB
        $this->entityManager->flush();
        $csrfElement = new Element\Csrf('csrf');
        $isXmlHttpRequest = true;
        /** @var  Array $payment_method */
        $payment_method = $this->paymentMethodManager->getAvailablePaymentMethods($user);
        /** @var PaymentMethod $payment_method */
        $payment_method = end($payment_method);
        $postData = [
            'csrf' => $csrfElement->getValue(),
            'payment_method' => [
                'payment_method_type' => 'bank_transfer',
                'account_number' => 30101810600000000786,
                'bank_name' => 'ФИЛИАЛ "САНКТ-ПЕТЕРБУРГСКИЙ" АО "АЛЬФА-БАНК"',
                'bik' => '044030786',
                'civil_law_subject_id' => 85,
                'corr_account_number' => 30101810600000000786,
                'id' ,
                'name',
            ]

        ];

        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/' . $deal->getId() . '/payment-method', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        ///снова ищу эту сделку, и убеждаюсь что присетило
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment=$deal->getPayment();
        /** @var PaymentMethod $payment_method */
        $payment_method = $payment->getPaymentMethod();
        //я утверждаю, что не засетило
        $this->assertNotTrue($payment_method instanceof PaymentMethod);
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-payment-method');
        $this->assertActionName('createPaymentMethod');
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testSetPaymentMethodToDealTypeNotSet()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self:: SetNullsToPayment($deal_name);

        $this->login( $deal->getContractor()->getUser()->getLogin());
        $deal->getContractor()->setCivilLawSubject(null);
        // Prepare object
        $this->entityManager->persist($deal);
        // Save to DB
        $this->entityManager->flush();
        $csrfElement = new Element\Csrf('csrf');
        $isXmlHttpRequest = true;
        $postData = [
            'csrf' =>  $csrfElement->getValue(),
            'payment_method' => ''

        ];

        $this->dispatch('/deal/'.$deal->getId().'/payment-method', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //  var_dump($deal->getContractor()->getCivilLawSubject());
        // $this->assertTrue($deal->getPayment() instanceof Payment);
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-payment-method');
        $this->assertActionName('createPaymentMethod');
    }


    public function testSetPaymentMethodToDealArraySet()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self::SetNullsToPayment($deal_name);

        $this->login( $deal->getContractor()->getUser()->getLogin());
        $deal->getContractor()->setCivilLawSubject(null);
        // Prepare object
        $this->entityManager->persist($deal);
        // Save to DB
        $this->entityManager->flush();
        $csrfElement = new Element\Csrf('csrf');
        $isXmlHttpRequest = true;
        $postData = [
            'csrf' =>  $csrfElement->getValue(),
            'payment_method' => [
                'payment_method_type' => 'bank_transfer',
                'second' => 'test',
            ]

        ];

        $this->dispatch('/deal/'.$deal->getId().'/payment-method', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        $this->assertTrue($deal->getPayment() instanceof Payment);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-payment-method');
        $this->assertActionName('createPaymentMethod');
    }
    /**
     * В общем остальные тесты будут просто с перебором значений массива
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateAndSetThenFormIsNotValidAllFor()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */
        $deal = self::SetNullsToPaymentAndCivil($deal_name);
        $isXmlHttpRequest = true;
        $postData = [
            'csrf' => '9d6352561d892fc16f7655cab9021c4e-1c0d57c35324971a98a8652e6da9b732',
            'civil_law_subject' =>
                [
                    'csrf' => '9d6352561d892fc16f7655cab9021c4e-1c0d57c35324971a98a8652e6da9b732',
                    'last_name' => 'Привет',
                    'birth_date' => '11.11.1990',
                    'civil_law_subject_type' => 'natural_person',
                    'current_action' => 'form-natural-person',
                ],
            'payment_method' => '',
        ];
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject_payment-method', 'POST', $postData, $isXmlHttpRequest);
        $this->assertResponseStatusCode(200);
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        $this->assertTrue($deal->getPayment() instanceof Payment);

        $this->assertControllerName(DealController::class);
    }

    /**
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCivilLawSubjectPaymentMethodCreateForValidData()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */

        $deal = self::SetNullsToPaymentAndCivil($deal_name);
        $user= $deal->getContractor()->getUser();
        $this->login($user->getLogin());

        $paymentMethodBankTransfers = $this->paymentMethodManager->getAvailablePaymentMethods($user);

        /** @var PaymentMethodBankTransfer $paymentMethodBankTransfer */
        $paymentMethodBankTransfer = end($paymentMethodBankTransfers);

        $csrfElement = new Element\Csrf('csrf');
        $postData = [
            'csrf' => $csrfElement->getValue(),
            'civil_law_subject' =>
                [
                    "civil_law_subject_type" => "legal-entity",
                    "name" => "ООО Ёшкин-матрёшкин 2",
                    "legal_address" => "пос.Весёлый",
                    "inn" => "783900874127",
                    "kpp" => "315784700",
                    "nds_code" => "2",
                    "nds_type" => "2"
                ],
            'payment_method' => $paymentMethodBankTransfer->getPaymentMethod()->getId(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject_payment-method', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        
        $this->assertInstanceOf(Payment::class, $deal->getPayment());
        $this->assertInstanceOf(CivilLawSubject::class, $deal->getContractor()->getCivilLawSubject());
        //проверяю, что пришел статус успешно
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertControllerName(DealController::class);
    }

    /**
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCivilLawSubjectPaymentMethodCreateForNotValidData()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */

        $deal = self::SetNullsToPaymentAndCivil($deal_name);
        $user= $deal->getContractor()->getUser();
        $this->login($user->getLogin());
        $isXmlHttpRequest = true;
        $payment_method = $this->paymentMethodManager->getAvailablePaymentMethods($user);
        /** @var PaymentMethod $payment_method */
        $payment_method = end($payment_method);
        $csrfElement = new Element\Csrf('csrf');
        $postData = [
            'csrf' => $csrfElement->getValue(),
            'civil_law_subject' =>
                [
                    "civil_law_subject_type" => "legal-entity",
                    "name" => "ООО Ёшкин-матрёшкин 2",
                    "kpp" => "315784700",

                ],
            'payment_method' => $payment_method->getId(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject_payment-method', 'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);
        $this->assertResponseStatusCode(200);
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment=$deal->getPayment();
        $this->assertNotTrue($payment->getPaymentMethod() instanceof PaymentMethod);
        $this->assertNotTrue($deal->getContractor()->getCivilLawSubject() instanceof CivilLawSubject);
        //проверяю, что пришел статус
        $this->assertTrue($data['status']=='ERROR');
        $this->assertControllerName(DealController::class);
    }

    /**
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCivilLawSubjectPaymentMethodCreateForNotValidDataPayment()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */

        $deal = self::SetNullsToPaymentAndCivil($deal_name);
        $user= $deal->getContractor()->getUser();
        $this->login($user->getLogin());
        $isXmlHttpRequest = true;
        $payment_method = $this->paymentMethodManager->getAvailablePaymentMethods($user);
        /** @var PaymentMethod $payment_method */
        $payment_method = end($payment_method);
        $csrfElement = new Element\Csrf('csrf');
        $postData = [
            'csrf' => $csrfElement->getValue(),
            'civil_law_subject' =>
                [
                    "civil_law_subject_type" => "legal-entity",
                    "name" => "ООО Ёшкин-матрёшкин 2",
                    "kpp" => "315784700",

                ],
            'payment_method' => "test",
        ];
        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject_payment-method', 'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);
        $this->assertResponseStatusCode(200);
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment=$deal->getPayment();
        $this->assertNotTrue($payment->getPaymentMethod() instanceof PaymentMethod);
        $this->assertNotTrue($deal->getContractor()->getCivilLawSubject() instanceof CivilLawSubject);
        //проверяю, что пришел статус
        $this->assertTrue($data['status']=='ERROR');
        $this->assertControllerName(DealController::class);
    }

    /**
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCivilLawSubjectPaymentMethodCreateForNotValidDataCivilLawSubject()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */

        $deal = self::SetNullsToPaymentAndCivil($deal_name);
        $user= $deal->getContractor()->getUser();
        $this->login($user->getLogin());
        $isXmlHttpRequest = true;
        $payment_method = $this->paymentMethodManager->getAvailablePaymentMethods($user);
        /** @var PaymentMethod $payment_method */
        $payment_method = end($payment_method);
        $csrfElement = new Element\Csrf('csrf');
        $postData = [
            'csrf' => $csrfElement->getValue(),
            'civil_law_subject' => "test",
            'payment_method' => $payment_method->getId(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject_payment-method', 'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);
        $this->assertResponseStatusCode(200);
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment=$deal->getPayment();
        $this->assertNotTrue($payment->getPaymentMethod() instanceof PaymentMethod);
        $this->assertNotTrue($deal->getContractor()->getCivilLawSubject() instanceof CivilLawSubject);
        //проверяю, что пришел статус
        $this->assertTrue($data['status']=='ERROR');
        $this->assertControllerName(DealController::class);
    }

    /**
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCivilLawSubjectPaymentMethodCreateForNotValidDataCivilLawSubjectIsEmpty()
    {
        $deal_name = 'Сделка Negotiation';
        /* @var Deal $deal */

        $deal = self::SetNullsToPaymentAndCivil($deal_name);
        $user= $deal->getContractor()->getUser();
        $this->login($user->getLogin());
        $isXmlHttpRequest = true;
        $payment_method = $this->paymentMethodManager->getAvailablePaymentMethods($user);
        /** @var PaymentMethod $payment_method */
        $payment_method = end($payment_method);
        $csrfElement = new Element\Csrf('csrf');
        $postData = [
            'csrf' => $csrfElement->getValue(),
            'civil_law_subject' => "",
            'payment_method' => "",
        ];
        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject_payment-method', 'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);
        $this->assertResponseStatusCode(200);
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment=$deal->getPayment();
        $this->assertNotTrue($payment->getPaymentMethod() instanceof PaymentMethod);
        $this->assertNotTrue($deal->getContractor()->getCivilLawSubject() instanceof CivilLawSubject);
        //проверяю, что пришел статус
        $this->assertEquals('ERROR', $data['status']);
        $this->assertControllerName(DealController::class);
    }

    /**
     * @param $deal_name
     * @return Deal
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function SetNullsToPaymentAndCivil($deal_name)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $payment->setPaymentMethod(null);
        $deal->getContractor()->setCivilLawSubject(null);
        $this->entityManager->persist($deal);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();
        return $deal;
    }
    /**
     * @param $deal_name
     * @return Deal
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function SetNullsToPayment($deal_name)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $payment->setPaymentMethod(null);
        // $deal->getContractor()->setCivilLawSubject(null);
        $this->entityManager->persist($deal);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();
        return $deal;
    }

    /**
     * @param $deal_name
     * @return Deal
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function SetNullsToCivil($deal_name)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $deal->getContractor()->setCivilLawSubject(null);
        $this->entityManager->persist($deal);
        $this->entityManager->persist($payment);
        $this->entityManager->flush();
        return $deal;
    }
    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        $user = $user ?? $this->user;
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        $login = $login ?? $this->user_login;
        $password = $password ?? $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}