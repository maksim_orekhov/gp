<?php

namespace ApplicationTest\Controller;

use Application\Entity\User;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\ORMDoctrineUtil;
use ModuleRbac\Service\RbacManager;
use ModuleRbac\Service\RoleManager;
use Application\Service\SettingManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Controller\SettingController;

/**
 * Class SettingControllerTest
 * @package ApplicationTest\Controller
 */
class SettingControllerTest extends AbstractHttpControllerTestCase
{
    private $user_login;
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * @var \ModuleRbac\Service\RoleManager
     */
    private $roleManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Controller\SettingController
     */
    private $settingController;

    /**
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->rbacManager = $serviceManager->get(RbacManager::class);
        $this->roleManager = $serviceManager->get(RoleManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        $this->settingController = new SettingController(
            $serviceManager->get(SettingManager::class)
        );

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Неавторизованный пользователь не может попасть на страницу
     * Route: setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessedByUnauthorizedUser()
    {
        $this->dispatch('/setting', 'GET');

        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting');
        $this->assertActionName('index');
        $this->assertResponseStatusCode(302);
        $this->assertRedirectTo('/login?redirectUrl=/setting');
    }

    /**
     * Авторизованый пользователь с привилегией Verified не может попасть на страницу
     * Route: setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessedByAuthorizedUserWithVerifiedRole()
    {
        $role_name = 'Verified';
        $this->authUserWithRole($role_name);

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/setting', 'GET');

        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting');
        $this->assertActionName('index');
        $this->assertResponseStatusCode(302);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Авторизованый пользователь с привилегией Operator не может попасть на страницу
     * Route: setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessedByAuthorizedUserWithOperatorRole()
    {
        $role_name = 'Operator';
        $this->authUserWithRole($role_name);

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/setting', 'GET');

        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting');
        $this->assertActionName('index');
        $this->assertResponseStatusCode(302);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Авторизованый пользователь с привилегией Administrator может попасть на страницу
     * Route: setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testIndexActionCanBeAccessedByAuthorizedUserWithAdministratorRole()
    {
        // Tested role name
        $role_name = 'Administrator';
        $this->authUserWithRole($role_name);

        $this->dispatch('/setting', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting');
        $this->assertActionName('index');
    }

    /**
     * Неавторизованный пользователь не может попасть настраницу
     * Route: setting/global-setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testGlobalSettingActionCanNotBeAccessedByUnauthorizedUser()
    {
        $this->dispatch('/setting/global-setting', 'GET');

        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/global-setting');
        $this->assertActionName('globalSetting');
        $this->assertResponseStatusCode(302);
        $this->assertRedirectTo('/login?redirectUrl=/setting/global-setting');
    }

    /**
     * Авторизованый пользователь с привилегией Verified не может попасть на страницу
     * Route: /setting/global-setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testGlobalSettingActionCanNotBeAccessedByAuthorizedUserWithVerifiedRole()
    {
        // Tested role name
        $role_name = 'Verified';
        $this->authUserWithRole($role_name);

        $this->dispatch('/setting/global-setting', 'GET');
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/global-setting');
        $this->assertActionName('globalSetting');
        $this->assertResponseStatusCode(302);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Авторизованый пользователь с привилегией Operator не может попасть на страницу
     * Route: /setting/global-setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testGlobalSettingActionCanNotBeAccessedByAuthorizedUserWithOperatorRole()
    {
        // Tested role name
        $role_name = 'Operator';
        $this->authUserWithRole($role_name);

        $this->dispatch('/setting/global-setting', 'GET');
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/global-setting');
        $this->assertActionName('globalSetting');
        $this->assertResponseStatusCode(302);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Авторизованый пользователь с привилегией Administrator может попасть на страницу
     * Route: /setting/global-setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testGlobalSettingActionCanBeAccessedByAuthorizedUserWithAdministratorRole()
    {
        // Tested role name
        $role_name = 'Administrator';
        $this->authUserWithRole($role_name);

        $this->dispatch('/setting/global-setting', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/global-setting');
        $this->assertActionName('globalSetting');

    }

    /**
     * Неавторизованный пользователь не может попасть настраницу
     * Route: setting/payment-detail
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testPaymentDetailActionCanNotBeAccessedByUnauthorizedUser()
    {
        $this->dispatch('/setting/payment-detail', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/payment-detail');
        $this->assertActionName('paymentDetail');
        $this->assertRedirectTo('/login?redirectUrl=/setting/payment-detail');
    }

    /**
     * Авторизованый пользователь с привилегией Verified не может попасть на страницу
     * Route: /setting/payment-detail
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testPaymentDetailActionCanNotBeAccessedByAuthorizedUserWithVerifiedRole()
    {
        // Tested role name
        $role_name = 'Verified';
        $this->authUserWithRole($role_name);

        $this->dispatch('/setting/payment-detail', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/payment-detail');
        $this->assertActionName('paymentDetail');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Авторизованый пользователь с привилегией Operator не может попасть на страницу
     * Route: /setting/payment-detail
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testPaymentDetailActionCanNotBeAccessedByAuthorizedUserWithOperatorRole()
    {
        // Tested role name
        $role_name = 'Operator';
        $this->authUserWithRole($role_name);

        $this->dispatch('/setting/payment-detail', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/payment-detail');
        $this->assertActionName('paymentDetail');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Авторизованый пользователь с привилегией Administrator может попасть на страницу
     * Route: /setting/payment-detail
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testPaymentDetailActionCanBeAccessedByAuthorizedUserWithAdministratorRole()
    {
        // Tested role name
        $role_name = 'Administrator';
        $this->authUserWithRole($role_name);

        $this->assertResponseStatusCode(200);
        $this->dispatch('/setting/payment-detail', 'GET');
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/payment-detail');
        $this->assertActionName('paymentDetail');
    }

    /**
     * проверка присутствия необходимых переменных в IndexAction
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testIndexActionOnConformanceVariables()
    {
        $resp = $this->settingController->indexAction();
        $variable = $resp->getVariables();
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $resp);
        $this->assertEmpty($variable);
    }

    /**
     * проверка присутствия необходимых переменных в GlobalSettingAction
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testGlobalSettingActionOnConformanceVariables()
    {
        $formFieldNames = [
            'fee_percentage',
            'min_delivery_period',
            'csrf',
            'submit',
        ];

        $resp = $this->settingController->globalSettingAction();
        $form = $resp->getVariable('form');
        $formAttributes = $form->getAttributes();
        $form->isValid();
        $formData = $form->getData();

        $this->assertInstanceOf('Zend\View\Model\ViewModel', $resp);
        $this->assertInstanceOf('Application\Form\GlobalSettingForm', $form);
        $this->assertEquals('post', $formAttributes['method']);
        $this->assertEquals('global-setting', $formAttributes['name']);
        foreach ($formFieldNames as $formFieldName){
            $this->assertArrayHasKey($formFieldName, $formData);
        }
    }

    /**
     * проверка присутствия необходимых переменных в PaymentDetailAction
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testPaymentDetailActionOnConformanceVariables()
    {
        $formFieldNames = [
            'name',
            'bik',
            'account_number',
            'payment_recipient_name',
            'inn',
            'kpp',
            'bank',
            'corr_account_number',
            'csrf',
            'submit',
        ];

        $resp = $this->settingController->paymentDetailAction();
        $form = $resp->getVariable('form');
        $formAttributes = $form->getAttributes();
        $form->isValid();
        $formData = $form->getData();

        $this->assertInstanceOf('Zend\View\Model\ViewModel', $resp);
        $this->assertInstanceOf('Application\Form\SystemPaymentDetailForm', $form);
        $this->assertEquals('post', $formAttributes['method']);
        $this->assertEquals('payment-detail', $formAttributes['name']);
        foreach ($formFieldNames as $formFieldName){
            $this->assertArrayHasKey($formFieldName, $formData);
        }
    }

    /**
     * Неавторизованный пользователь не может получить данные через ajax
     * Route: setting/global-setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testGlobalSettingActionCanNotBeAccessedByUnauthorizedUserForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/setting/global-setting', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/global-setting');
        $this->assertActionName('globalSetting');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/setting/global-setting', $data['data']['redirectUrl']);
    }

    /**
     * Авторизованый пользователь с привилегией Verified не может получить данные через ajax
     * Route: /setting/global-setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testGlobalSettingActionCanNotBeAccessedByAuthorizedUserWithVerifiedRoleForAjax()
    {
        // Tested role name
        $role_name = 'Verified';
        $this->authUserWithRole($role_name);

        $isXmlHttpRequest = true;
        $this->dispatch('/setting/global-setting', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/global-setting');
        $this->assertActionName('globalSetting');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Авторизованый пользователь с привилегией Operator не может получить данные через ajax
     * Route: /setting/global-setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testGlobalSettingActionCanNotBeAccessedByAuthorizedUserWithOperatorRoleForAjax()
    {
        // Tested role name
        $role_name = 'Operator';
        $this->authUserWithRole($role_name);

        $isXmlHttpRequest = true;
        $this->dispatch('/setting/global-setting', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/global-setting');
        $this->assertActionName('globalSetting');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Авторизованый пользователь с привилегией Administrator может получить данные через ajax
     * Route: /setting/global-setting
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testGlobalSettingActionCanBeAccessedByAuthorizedUserWithAdministratorRoleForAjax()
    {
        // Tested role name
        $role_name = 'Administrator';
        $this->authUserWithRole($role_name);

        $isXmlHttpRequest = true;
        $this->dispatch('/setting/global-setting', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/global-setting');
        $this->assertActionName('globalSetting');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals('SUCCESS',$data['status']);
    }

    /**
     * Неавторизованный пользователь не может получить данные через ajax
     * Route: setting/payment-detail
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testPaymentDetailActionCanNotBeAccessedByUnauthorizedUserForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/setting/payment-detail', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/payment-detail');
        $this->assertActionName('paymentDetail');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/setting/payment-detail', $data['data']['redirectUrl']);
    }

    /**
     * Авторизованый пользователь с привилегией Verified не может получить данные через ajax
     * Route: setting/payment-detail
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testPaymentDetailActionCanNotBeAccessedByAuthorizedUserWithVerifiedRoleForAjax()
    {
        // Tested role name
        $role_name = 'Verified';
        $this->authUserWithRole($role_name);

        $isXmlHttpRequest = true;
        $this->dispatch('/setting/payment-detail', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/payment-detail');
        $this->assertActionName('paymentDetail');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Авторизованый пользователь с привилегией Operator не может получить данные через ajax
     * Route: setting/payment-detail
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testPaymentDetailActionCanNotBeAccessedByAuthorizedUserWithOperatorRoleForAjax()
    {
        // Tested role name
        $role_name = 'Operator';
        $this->authUserWithRole($role_name);

        $isXmlHttpRequest = true;
        $this->dispatch('/setting/payment-detail', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/payment-detail');
        $this->assertActionName('paymentDetail');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Авторизованый пользователь с привилегией Administrator может получить данные через ajax
     * Route: setting/payment-detail
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group settings
     * @throws \Exception
     */
    public function testPaymentDetailActionCanBeAccessedByAuthorizedUserWithAdministratorRoleForAjax()
    {
        // Tested role name
        $role_name = 'Administrator';
        $this->authUserWithRole($role_name);

        $isXmlHttpRequest = true;
        $this->dispatch('/setting/payment-detail', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(SettingController::class);
        $this->assertControllerClass('SettingController');
        $this->assertMatchedRouteName('setting/payment-detail');
        $this->assertActionName('paymentDetail');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals('SUCCESS',$data['status']);
    }


    /**
     * @param $role_name
     * @return User
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function authUserWithRole($role_name)
    {
        // Get Role object
        $role = $this->roleManager->getRoleByName($role_name);
        // Get User object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        // Если были роли, удаляем
        $user->getRoles()->clear();
        // Add role to user
        $user->addRole($role);
        // Save
        $this->entityManager->flush();

        // Authorize test user
        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);

        return $user;
    }
}