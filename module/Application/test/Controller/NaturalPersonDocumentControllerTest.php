<?php

namespace ApplicationTest\Controller;

use Application\Controller\NaturalPersonDocumentController;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseRoleManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use CoreTest\ViewVarsTrait;

/**
 * Class NaturalPersonDocumentControllerTest
 * @package ApplicationTest\Controller
 */
class NaturalPersonDocumentControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Application\Service\CivilLawSubject\NaturalPersonDocumentManager
     */
    private $naturalPersonDocumentManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->naturalPersonDocumentManager = $serviceManager->get(NaturalPersonDocumentManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// Collection

    // Неавторизованный

    /**
     * Недоступность natural-person-document не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/natural-person-document', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(NaturalPersonDocumentController::class);
        $this->assertMatchedRouteName('natural-person-document');
        $this->assertRedirectTo('/login?redirectUrl=/natural-person-document');
    }

    /**
     * Для Ajax
     * Недоступность natural-person-document не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person-document', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDocumentController::class);
        $this->assertMatchedRouteName('natural-person-document');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/natural-person-document', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Не доступность /natural-person-document авторизованному без роли Оператора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByVerifiedUser()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/natural-person-document', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDocumentController::class);
        $this->assertMatchedRouteName('natural-person-document');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Для Ajax
     * Не доступность /natural-person-document авторизованному без роли Оператора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByVerifiedUserForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person-document', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDocumentController::class);
        $this->assertMatchedRouteName('natural-person-document');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Доступность /natural-person-document Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/natural-person-document', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDocumentController::class);
        $this->assertMatchedRouteName('natural-person-document');
        // Что отдается в шаблон
        $this->assertArrayHasKey('documents', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
    }

    /**
     * Для Ajax
     * Доступность /natural-person-document Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person-document', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDocumentController::class);
        $this->assertMatchedRouteName('natural-person-document');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('natural person document', $data['message']);
        $this->assertArrayHasKey('documents', $data['data']);
        $this->assertArrayHasKey('paginator', $data['data']);
    }


    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}