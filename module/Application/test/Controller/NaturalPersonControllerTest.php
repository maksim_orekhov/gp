<?php

namespace ApplicationTest\Controller;

use Application\Controller\NaturalPersonController;
use Application\Entity\CivilLawSubject;
use Application\Entity\NaturalPerson;
use Application\Form\NaturalPersonForm;
use ApplicationTest\Bootstrap;
use Core\Service\Base\BaseRoleManager;
use Core\Controller\Plugin\Message\MessagePlugin;
use Core\Exception\LogicException;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Zend\Form\Element;
use CoreTest\ViewVarsTrait;

/**
 * Class NaturalPersonControllerTest
 * @package ApplicationTest\Controller
 */
class NaturalPersonControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_NATURAL_PERSON_LAST_NAME = 'Тестов';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// getList

    // Неавторизованный

    /**
     * Недоступность /profile/natural-person (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/natural-person', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');
        $this->assertRedirectTo('/login?redirectUrl=/profile/natural-person');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/natural-person (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/natural-person', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /profile/natural-person (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUser()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/natural-person', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');
        // Что отдается в шаблон
        $this->assertArrayHasKey('naturalPersons', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-filter-form');
    }

    /**
     * Ajax
     * Доступность /profile/natural-person (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUserForAjax()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $naturalPerson = $data['data']['naturalPersons'][0];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('NaturalPerson collection', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('naturalPersons', $data['data']);
        $this->assertArrayHasKey('paginator', $data['data']);
        $this->assertEquals('Тест', $naturalPerson['first_name']);
        $this->assertEquals('Тестов', $naturalPerson['last_name']);
    }

    /////////////// get

    // Неавторизованный

    /**
     * Недоступность /profile/natural-person/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/natural-person/'.$naturalPerson->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/natural-person/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Не Владелец

    /**
     * Неоступность /profile/natural-person/id (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Ajax
     * Недоступность /profile/natural-person/id (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Доступность /profile/natural-person/id (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertControllerClass('NaturalPersonController');
        $this->assertMatchedRouteName('user-profile/natural-person-single');
    }

    /**
     * Ajax
     * Доступность /profile/natural-person/id (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOwnerForAjax()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'get', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $naturalPerson = $data['data']['naturalPerson'];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('NaturalPerson single', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('naturalPerson', $data['data']);
        $this->assertEquals('Тест', $naturalPerson['first_name']);
        $this->assertEquals('Тестов', $naturalPerson['last_name']);
    }

    /////////////// update

    // Неавторизованный

    /**
     * Недоступность /profile/natural-person/id (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorised()
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/natural-person/'.$naturalPerson->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/natural-person/id (POST на update) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertArrayHasKey('content', $view_vars);
        $this->assertEquals('Method not supported', $view_vars['content']);
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals('Page not found.', $view_vars['message']);
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id (POST на update) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Не владелец

    /**
     * Недоступность /profile/natural-person/id (POST на update) по HTTP
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * Important! update method is only for Ajax!
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id (POST на update) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');

        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Для Ajax
     * Доступность /profile/natural-person/id (POST валидный на update) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testUpdateWithValidPostForOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $new_last_name = 'Супертестов';

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'first_name'      => 'Супертест',
            'secondary_name' => 'Супертестович',
            'last_name' => $new_last_name,
            'birth_date' => '1985-12-31',
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var NaturalPerson $updatedNaturalPerson */
        $updatedNaturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => $new_last_name));

        $this->assertEquals($new_last_name, $updatedNaturalPerson->getLastName());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных пользователем с ролью Verified /profile/natural-person/id
     * (POST невалидный на update)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $new_last_name = 'Супертестов';

        // Invalid POST
        $postData = [
            'first_name'      => 'Супертест',
            'secondary_name' => 'Супертестович',
            'last_name' => $new_last_name,
            'birth_date' => '1985-12-31',
            'csrf'      => '123invalid456'
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var NaturalPerson $updatedNaturalPerson */
        $updatedNaturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => $new_last_name));

        $this->assertNull($updatedNaturalPerson);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /////////////// create

    // Неавторизованный

    /**
     * Недоступность /profile/natural-person (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorised()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/natural-person', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');
        $this->assertRedirectTo('/login?redirectUrl=/profile/natural-person');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/natural-person (POST на create) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/natural-person', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person (POST на create) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Access to the resource is denied to the user.', $data['message']);
    }

    /**
     * Недоступность /profile/natural-person (POST валидный на create) по HTTP (404)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/natural-person', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertControllerClass('NaturalPersonController');
        $this->assertMatchedRouteName('user-profile/natural-person');
    }

    /**
     * Для Ajax
     * Доступность /profile/natural-person (POST валидный на create) пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerifiedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test22
        $this->login('test2');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $last_name = 'Супертестов';

        $postData = [
            'first_name'      => 'Супертест',
            'secondary_name' => 'Супертестович',
            'last_name' => $last_name,
            'birth_date' => '1985-12-31',
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => $last_name));

        $this->assertEquals($last_name, $naturalPerson->getLastName());
        $this->assertEquals($data['data']['naturalPerson']['id'], $naturalPerson->getId());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');

        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных пользователем с ролью Verified /profile/natural-person
     * (POST невалидный на create)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateWithInvalidPostForVerifiedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $last_name = 'Супертестов';
        // Invalid POST
        $postData = [
            'first_name'      => 'Супертест',
            'secondary_name' => 'Супертестович',
            'last_name' => $last_name,
            'birth_date' => '1985-12-31',
            'csrf'      => '123invalid345' // Wrong csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => $last_name));

        $this->assertNull($naturalPerson);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /////////////// delete

    // Неавторизованный

    /**
     * Недоступность /profile/natural-person/id (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorised()
    {
        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/natural-person/'.$naturalPerson->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Не владелец

    /**
     * Недоступность /profile/natural-person/id (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /profile/natural-person/id (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Доступность /profile/natural-person/id (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteForOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var NaturalPerson $newNaturalPerson */ // Создаем новый CivilLawSubject с NaturalPerson для удаления
        $newNaturalPerson = $this->createNewTestNaturalPerson('test');

        // Must be added
        $this->assertEquals('Person', $newNaturalPerson->getLastName());

        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/natural-person/'.$newNaturalPerson->getId(), 'POST', $postData);

        /** @var NaturalPerson $updatedNaturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => 'Person'));

        // Must be null (deleted)
        $this->assertNull($naturalPerson);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertRedirectTo('/profile/natural-person');
    }

    /**
     * Для Ajax
     * Доступность /profile/natural-person/id (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteForOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var NaturalPerson $newNaturalPerson */ // Создаем новый CivilLawSubject с NaturalPerson для удаления
        $newNaturalPerson = $this->createNewTestNaturalPerson('test');

        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$newNaturalPerson->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var NaturalPerson $updatedNaturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => 'Person'));

        // Must be null (deleted)
        $this->assertNull($naturalPerson);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-single');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(NaturalPersonController::SUCCESS_DELETE, $data['message']);
    }

    /////////////// editForm

    // Неавторизованный

    /**
     * Недоступность /profile/natural-person/id/edit (GET на editFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNonAuthorised()
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/edit');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-edit');
        $this->assertRedirectTo('/login?redirectUrl=/profile/natural-person/'.$naturalPerson->getId().'/edit');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id/edit (GET на editFormAction) по Ajax неавторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/edit',
            null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-edit');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id/edit (GET на editFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/edit', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/natural-person/id/edit (GET на editFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-edit');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_NATURAL_PERSON_MESSAGE, $view_vars['message']);
    }

    /**
     * Недоступность /profile/natural-person/id/edit (GET на editFormAction) не Владельцу (500)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-edit');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_NATURAL_PERSON_MESSAGE, $view_vars['message']);
    }

    /**
     * Доступность /profile/natural-person/id/edit (GET на editFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testEditFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertControllerClass('NaturalPersonController');
        $this->assertMatchedRouteName('user-profile/natural-person-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('naturalPerson', $view_vars);
        $this->assertArrayHasKey('naturalPersonForm', $view_vars);
        $this->assertTrue($view_vars['naturalPersonForm'] instanceof NaturalPersonForm);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Редактирование NaturalPerson /profile/natural-person/id/edit (POST валидный на editFormAction)
     * пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testEditFormWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $new_last_name = 'Супертестов';

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'first_name'      => 'Супертест',
            'secondary_name' => 'Супертестович',
            'last_name' => $new_last_name,
            'birth_date' => '1985-12-31',
            'csrf'      => $csrf
        ];

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/edit', 'POST', $postData);

        /** @var NaturalPerson $updatedNaturalPerson */
        $updatedNaturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => $new_last_name));

        $this->assertEquals($new_last_name, $updatedNaturalPerson->getLastName());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-edit');
        $this->assertRedirectTo('/profile/natural-person');
    }

    /**
     * Отправка заведомо невалидных данных /profile/natural-person/id/edit
     * (POST невалидный на editFormAction) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $new_last_name = 'Супертестов';

        // Invalid POST
        $postData = [
            'first_name'      => 'Супертест',
            'secondary_name' => 'Супертестович',
            'last_name' => $new_last_name,
            'birth_date' => '1985-12-31',
            'csrf'      => '123invalid456' // csrf несуществующий
        ];

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/edit', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var NaturalPerson $updatedNaturalPerson */
        $updatedNaturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => $new_last_name));

        $this->assertNull($updatedNaturalPerson);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('naturalPerson', $view_vars);
        $this->assertArrayHasKey('naturalPersonForm', $view_vars);
        $this->assertTrue($view_vars['naturalPersonForm'] instanceof NaturalPersonForm);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /////////////// createForm

    // Неавторизованный

    /**
     * Недоступность /profile/natural-person/create (GET на createFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/natural-person/create');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-create');
        $this->assertRedirectTo('/login?redirectUrl=/profile/natural-person/create');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/create (GET на createFormAction) по Ajax не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-create');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/create (GET на createFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/natural-person/create (GET на createFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/profile/natural-person/create', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-create');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Доступность /profile/natural-person/create (GET на createFormAction) пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateFormCanBeAccessedByVerified()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/natural-person/create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertControllerClass('NaturalPersonController');
        $this->assertMatchedRouteName('user-profile/natural-person-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('naturalPersonForm', $view_vars);
        $this->assertTrue($view_vars['naturalPersonForm'] instanceof NaturalPersonForm);
    }

    /**
     * Создание NaturalPerson /profile/natural-person/create (POST валидный на create)
     * пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateFormWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $last_name = 'Супертестов';

        $postData = [
            'first_name'        => 'Супертест',
            'secondary_name'    => 'Супертестович',
            'last_name'         => $last_name,
            'birth_date'        => '1985-12-31',
            'csrf'              => $csrf
        ];

        $this->dispatch('/profile/natural-person/create', 'POST', $postData);

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => $last_name));

        $this->assertEquals($last_name, $naturalPerson->getLastName());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-create');
        $this->assertRedirectTo('/profile');
    }

    /**
     * Создание NaturalPerson
     * Отправка заведомо невалидных данных /profile/natural-person/create
     * (POST невалидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateFormWithInvalidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $last_name = 'Супертестов';
        // Invalid POST
        $postData = [
            'first_name'      => 'Супертест',
            'secondary_name' => 'Супертестович',
            'last_name' => $last_name,
            'birth_date' => '1985-12-31',
            'csrf'      => '123invalid345' // Wrong csrf
        ];

        $this->dispatch('/profile/natural-person/create', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => $last_name));

        $this->assertNull($naturalPerson);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertControllerClass('NaturalPersonController');
        $this->assertMatchedRouteName('user-profile/natural-person-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('naturalPersonForm', $view_vars);
        $this->assertTrue($view_vars['naturalPersonForm'] instanceof NaturalPersonForm);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-create-form');
    }

    /////////////// deleteForm

    // Неавторизованный

    /**
     * Недоступность /profile/natural-person/id/delete (GET на deleteFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorised()
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/delete');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-delete');
        $this->assertRedirectTo('/login?redirectUrl=/profile/natural-person/'.$naturalPerson->getId().'/delete');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/id/delete (GET на deleteFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/delete',
            'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-delete');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/create (GET на deleteFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/delete',
            'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-delete');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/natural-person/id/delete (GET на deleteFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-delete');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_NATURAL_PERSON_MESSAGE, $view_vars['message']);
    }

    /**
     * Недоступность /profile/natural-person/id/delete (GET на deleteFormAction) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-delete');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_NATURAL_PERSON_MESSAGE, $view_vars['message']);
    }

    /**
     * Доступность /profile/natural-person/id/delete (GET на deleteFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertControllerClass('NaturalPersonController');
        $this->assertMatchedRouteName('user-profile/natural-person-form-delete');
        // Что отдается в шаблон
        $this->assertArrayHasKey('naturalPerson', $view_vars);
    }

    /////////////// formInit (экшен только для Ajax!)

    // Неавторизованный

    /**
     * Недоступность /profile/natural-person/form-init (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/natural-person/form-init', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-init');
        $this->assertRedirectTo('/login?redirectUrl=/profile/natural-person/form-init');
    }

    /**
     * Для Ajax
     * Недоступность /profile/natural-person/form-init (GET на formInitAction)
     * не авторизованному пользователю по Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-init');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals('ERROR_NOT_AUTHORIZED', $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Ajax
     * Доступность /profile/natural-person/form-init (GET на formInitAction)
     * авторизованному с ролью Verified
     * Должен отдать коллекцию NaturalPerson, user и нулевые allowedUser и naturalPerson
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithoutParamsForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'naturalPerson' соотвествует нулевое значение
        $this->assertNull($data['data']['naturalPerson']);
        // а ключу коллекции 'allowedNaturalPersons' - не нулевой массив
        $this->assertGreaterThan(0, count($data['data']['allowedNaturalPersons']));
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/natural-person/form-init (GET на formInitAction)
     * с заведома невалидным параметром (idNaturalPerson) авторизованному с ролью Verified
     * Должен отдать коллекции и нулевые объекты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithInvalidParamsForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        //Invalid params
        $getData = [
            'idNaturalPerson' => 99999999,
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'naturalPerson' соотвествует нулевое значение
        $this->assertNull($data['data']['naturalPerson']);
        // а ключу коллекции 'allowedNaturalPersons' - не нулевой массив
        $this->assertGreaterThan(0, count($data['data']['allowedNaturalPersons']));
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/natural-person/form-init (GET на formInitAction)
     * с параметром чужого объекта (idNaturalPerson)
     * Должен отдать коллекции доступных объектов и нулевые объекты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithAlienParamsForAjax()
    {
        // Добавляем NaturalPerson пользователю test
        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->createNewTestNaturalPerson('test');

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        //Invalid params
        $getData = [
            'idNaturalPerson' => $naturalPerson->getId()
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test2', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'naturalPerson' соотвествует нулевое значение
        $this->assertNull($data['data']['naturalPerson']);
        // а ключу коллекции 'allowedNaturalPersons' - не нулевой массив
        $this->assertGreaterThan(0, count($data['data']['allowedNaturalPersons']));
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/natural-person/form-init (GET на formInitAction)
     * с валидным параметром Владельцу объектов, id которого передается (idNaturalPerson)
     * Должен отдать коллекции и НЕнулевой объект NaturalPerson
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithValidParamsForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->createNewTestNaturalPerson('test');

        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        //Invalid params
        $getData = [
            'idNaturalPerson' => $naturalPerson->getId()
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'naturalPerson' соотвествует НЕнулевые значения
        $this->assertNotNull($data['data']['naturalPerson']);
        $this->assertEquals('Person', $data['data']['naturalPerson']['lastName']);
        $this->assertEquals('test', $data['data']['naturalPerson']['civilLawSubject']['user']['login']);
        $this->assertEquals('Person', $data['data']['naturalPerson']['civilLawSubject']['naturalPerson']['lastName']);
        // и ключу коллекции 'allowedNaturalPersons' тоже
        $this->assertGreaterThan(0, count($data['data']['allowedNaturalPersons']));
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if (!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $login
     * @return NaturalPerson
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createNewTestNaturalPerson($login = 'test')
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $login));

        // Create CivilLawSubject
        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern(true);
        $civilLawSubject->setCreated(new \DateTime());

        $this->entityManager->persist($civilLawSubject);

        // Create NaturalPerson
        $naturalPerson = new NaturalPerson();
        $naturalPerson->setCivilLawSubject($civilLawSubject);
        $naturalPerson->setFirstName('New');
        $naturalPerson->setSecondaryName('Natural');
        $naturalPerson->setLastName('Person');
        $naturalPerson->setBirthDate(\DateTime::createFromFormat('!Y-m-d', '1979-07-25'));

        $civilLawSubject->setNaturalPerson($naturalPerson);

        $this->entityManager->persist($naturalPerson);

        $this->entityManager->flush();

        return $naturalPerson;
    }
}