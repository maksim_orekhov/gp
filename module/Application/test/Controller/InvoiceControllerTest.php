<?php

namespace ApplicationTest\Controller;

use Application\Controller\InvoiceController;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;

/**
 * Class InvoiceControllerTest
 * @package ApplicationTest\Controller
 */
class InvoiceControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const NOT_CONFIRMED_DEAL_NAME = 'Сделка Фикстура';
    const CONFIRMED_DEAL_NAME = 'Сделка Оплаченная';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);


        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// Invoice collection

    /**
     * Недоступность /invoice не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testInvoiceCollectionCanNotBeAccessedByUnverifiedUser()
    {
        // не авторизуемся!!!
        $this->dispatch('/invoice', 'GET');
        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertRedirectTo('/login?redirectUrl=/invoice');
    }

    /**
     * Недоступность /invoice не авторизованному пользователю по ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testInvoiceCollectionCanNotBeAccessedByUnverifiedUserForAjax()
    {
        // не авторизуемся!!!
        $isXmlHttpRequest = true;
        $this->dispatch('/invoice', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/invoice', $data['data']['redirectUrl']);
    }

    /**
     * Доступность коллекции /invoice для участника сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testAvailableInvoiceCollectionByUserPartOfDeal()
    {
        $this->login();

        $this->dispatch('/invoice', 'GET');

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertMatchedRouteName('deal-invoice');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('deals', $view_vars);
        //проверяем что в коллекции что то есть
        $this->assertTrue(\count($view_vars['deals']) > 0);
    }

    /**
     * Доступность коллекции /invoice для участника сделки Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testAvailableInvoiceCollectionByUserPartOfDealForAjax()
    {
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/invoice', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertMatchedRouteName('deal-invoice');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deals', $data['data']);

        $this->assertTrue(\count($data['data']['deals']) > 0);
    }

    /**
     * Доступность коллекции /invoice для оператора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testAvailableInvoiceCollectionByUserOperator()
    {
        $this->login('operator');

        $this->dispatch('/invoice', 'GET');

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertMatchedRouteName('deal-invoice');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertArrayHasKey('deals', $view_vars);
        //проверяем что в коллекции что то есть
        $this->assertTrue(\count($view_vars['deals']) > 0);
    }

    /**
     * Доступность коллекции /invoice для оператора Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testAvailableInvoiceCollectionByUserOperatorForAjax()
    {
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/invoice', 'GET', [], $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertMatchedRouteName('deal-invoice');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deals', $data['data']);

        $this->assertTrue(\count($data['data']['deals']) > 0);
    }

    ///////////// Invoice single

    /**
     * Недоступность /invoice/id не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testInvoiceSingleCanNotBeAccessedByUnverifiedUser()
    {
        // не авторизуемся!!!

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::NOT_CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);
        $this->dispatch('/invoice/'.$deal->getId(), 'GET');

        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertRedirectTo('/login?redirectUrl=/invoice/'.$deal->getId());
    }

    /**
     * Недоступность /invoice/id не авторизованному пользователю по ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testInvoiceSingleCanNotBeAccessedByUnverifiedUserForAjax()
    {
        // не авторизуемся!!!

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::NOT_CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);
        $isXmlHttpRequest = true;
        $this->dispatch('/invoice/'.$deal->getId(), 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/invoice/'.$deal->getId(), $data['data']['redirectUrl']);
    }

    /**
     * Недоступность /invoice/id пользователю с ролью contractor
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testInvoiceSingleCanNotBeAccessedByContractorUser()
    {
        // авторизуемся
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::NOT_CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является contractor
        $this->assertEquals('test2', $deal->getContractor()->getUser()->getLogin());

        $this->dispatch('/invoice/'.$deal->getId(), 'GET');

        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /invoice/id пользователю с ролью contractor Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testInvoiceSingleCanNotBeAccessedByContractorUserForAjax()
    {
        // авторизуемся
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::NOT_CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является contractor
        $this->assertEquals('test2', $deal->getContractor()->getUser()->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/invoice/'.$deal->getId(), 'GET', [], $isXmlHttpRequest);

        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /invoice/id участнику сделки (customer), если сделка не одобрена обоими агентами
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     */
    public function testInvoiceSingleCanNotBeAccessedByCustomerIfDealUnconfirmed()
    {
        // авторизуемся
        $this->login();

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::NOT_CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что сделка не подтверждена обоими участниками
        $this->assertNull($deal->getDateOfConfirm());
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());

        $this->dispatch('/invoice/'.$deal->getId(), 'GET');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        //основная проверка
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertMatchedRouteName('deal-invoice-single');
        $this->assertResponseStatusCode(500);
        #$this->assertArrayHasKey('message', $view_vars);
        #$this->assertEquals(InvoiceController::DEAL_IS_NOT_APPROVED, $view_vars['message']);
    }

    /**
     * Недоступность /invoice/id участнику сделки (customer), если сделка не одобрена обоими агентами Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     *
     * @throws \Exception
     * @expectedExceptionMessage The deal is not approved yet
     */
    public function testInvoiceSingleCanNotBeAccessedByCustomerIfDealUnconfirmedForAjax()
    {
        // авторизуемся
        $this->login();

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::NOT_CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что сделка не подтверждена обоими участниками
        $this->assertNull($deal->getDateOfConfirm());
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/invoice/'.$deal->getId(), 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertMatchedRouteName('deal-invoice-single');

        // Проверка данных в $data
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        #$this->assertEquals(InvoiceController::DEAL_IS_NOT_APPROVED, $data['message']);
    }

    /**
     * Доступность /invoice/id кастомеру, если сделка одобрена обоими агентами
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     * @throws \Exception
     *
     * @TODO Не проходит. Разобраться.
     */
    public function testInvoiceSingleCanBeAccessedByCustomerIfConfirmed()
    {
        // авторизуемся
        $this->login();

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что сделка подтверждена обоими участниками
        $this->assertNotNull($deal->getDateOfConfirm());
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());

        $this->setOutputCallback(function() {
            $output = $this->getActualOutput();
            $this->assertContains("%PDF-1.7", $output );
        });
        $this->dispatch('/invoice/'.$deal->getId(), 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(InvoiceController::class);
        $this->assertMatchedRouteName('deal-invoice-single');
    }

    /////////////// End Invoice


    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}