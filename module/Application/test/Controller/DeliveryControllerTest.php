<?php

namespace ApplicationTest\Controller;

use Application\Controller\DeliveryController;
use Application\Entity\Delivery;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Exception\Code\DeliveryExceptionCodeInterface;
use Application\Form\DeliveryConfirmForm;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Exception\LogicException;
use Core\Service\ORMDoctrineUtil;
use Zend\Form\Form;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;
use Zend\Form\Element;

/**
 * Class DeliveryControllerTest
 * @package ApplicationTest\Controller
 */
class DeliveryControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const NOT_PAID_DEAL_NAME = 'Сделка Фикстура';
    const PAID_DEAL_NAME = 'Сделка Оплаченная';
    const DISPUTE_DEAL_NAME = 'Сделка Спор открыт';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);


        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }


    /////////////// Delivery confirm

    /**
     * Недоступность /deal/:idDeal/delivery/confirm не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionCanNotBeAccessedByUnverifiedUser()
    {
        // не авторизуемся!!!
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'GET');

        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/'.$deal->getId().'/delivery/confirm');
    }

    /**
     * Недоступность /deal/:idDeal/delivery/confirm не авторизованному пользователю Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionCanNotBeAccessedByUnverifiedUserForAjax()
    {
        // не авторизуемся!!!
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery-form-confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/deal/11/delivery/confirm', $data['data']['redirectUrl']);
    }

    /**
     * Недоступность /deal/:idDeal/delivery/confirm для contractor
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionCanNotBeAccessedByContractor()
    {
        // авторизуемся
        $this->login('test2');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является contractor
        $this->assertEquals('test2', $deal->getContractor()->getUser()->getLogin());

        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'GET');

        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Недоступность /deal/:idDeal/delivery/confirm для contractor Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionCanNotBeAccessedByContractorForAjax()
    {
        // авторизуемся
        $this->login('test2');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является contractor
        $this->assertEquals('test2', $deal->getContractor()->getUser()->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'GET', [], $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery-form-confirm');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /deal/:idDeal/delivery/confirm для operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionCanNotBeAccessedByOperator()
    {
        // авторизуемся
        $this->login('operator');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);

        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'GET');

        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Недоступность /deal/:idDeal/delivery/confirm для operator Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionCanNotBeAccessedByOperatorForAjax()
    {
        // авторизуемся
        $this->login('operator');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery-form-confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Недоступность /deal/:idDeal/delivery/confirm для customer если сделка не оплачена
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionCanNotBeAccessedByCustomerIfDealNotPaid()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::NOT_PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        //проверяем что сделка не оплачена
        $this->assertNull($payment->getDeFactoDate());

        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'GET');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery-form-confirm');
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(DeliveryExceptionCodeInterface::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED_MESSAGE,
            $view_vars['message']);
    }

    /**
     * Недоступность /deal/:idDeal/delivery/confirm для customer если по сделке не закрыт спор
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionCanNotBeAccessedByCustomerIfDisputeNotClosed()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        //проверяем что спор не закрыт
        $this->assertNotTrue($dispute->isClosed());

        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'GET');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery-form-confirm');
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(DeliveryExceptionCodeInterface::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED_MESSAGE,
            $view_vars['message']);
    }

    /**
     * Недоступность /deal/:idDeal/delivery/confirm для customer если доставка уже подтверждена
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionCanNotBeAccessedByCustomerIfDeliveryConfirmed()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());

        // Create new Delivery object
        $delivery = new Delivery();
        $delivery->setDeliveryDate(new \DateTime());
        $delivery->setDeliveryStatus(true);
        $delivery->setDeal($deal);

        $this->entityManager->persist($delivery);
        $this->entityManager->flush();

        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'GET');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery-form-confirm');
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(DeliveryExceptionCodeInterface::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED_MESSAGE,
            $view_vars['message']);
    }

    /**
     * Доступность /deal/:idDeal/delivery/confirm для customer
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionCanBeAccessedByCustomer()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());

        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'GET');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery-form-confirm');
        $this->assertNotEmpty($view_vars);
    }

    /**
     * Проверка /deal/:idDeal/delivery/confirm для customer с невалиднами post данными
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionWithInvalidPostDataByCustomer()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());

        $postData = [
            'idDeal' => $deal->getId(),
            'type_form' => DeliveryConfirmForm::TYPE_FORM,
            'csrf' => '1212121212121212', // invalid csrf
        ];

        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery-form-confirm');
        $this->assertArrayHasKey('deliveryConfirmForm', $view_vars);
        /** @var Form $form */
        $form = $view_vars['deliveryConfirmForm'];

        $this->assertEquals('The form submitted did not originate from the expected site',
            $form->getMessages('csrf')['notSame']);
    }

    /**
     * Проверка /deal/:idDeal/delivery/confirm для customer с валиднами post данными
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testDeliveryConfirmFormActionWithValidPostDataByCustomer()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'idDeal' => $deal->getId(),
            'type_form' => DeliveryConfirmForm::TYPE_FORM,
            'csrf' => $csrf,
        ];

        $this->dispatch('/deal/'.$deal->getId().'/delivery/confirm', 'POST', $postData);

        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery-form-confirm');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());
    }

    /////////////// Delivery Create

    /**
     * Недоступность /delivery не авторизованному пользователю Post
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedByUnverifiedUser()
    {
        // не авторизуемся!!!
        $this->dispatch('/delivery', 'POST');

        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertRedirectTo('/login?redirectUrl=/delivery');
    }

    /**
     * Недоступность /delivery не авторизованному пользователю Post Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedByUnverifiedUserForAjax()
    {
        // не авторизуемся!!!
        $isXmlHttpRequest = true;
        $this->dispatch('/delivery', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/delivery', $data['data']['redirectUrl']);
    }

    /**
     * Недоступность /delivery для contractor Post
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     *
     * @TODO Тест неверное написан. Поправить!
     */
    public function testCreateActionCanNotBeAccessedByContractor()
    {
        // авторизуемся
        $this->login('test2');
        $this->dispatch('/delivery', 'POST');
        //основная проверка
        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery');
    }

    /**
     * Недоступность /delivery для contractor Post Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedByContractorForAjax()
    {
        // авторизуемся
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является contractor
        $this->assertEquals('test2', $deal->getContractor()->getUser()->getLogin());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'idDeal' => $deal->getId(),
            'type_form' => DeliveryConfirmForm::TYPE_FORM,
            'csrf' => $csrf,
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/delivery', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Недоступность /delivery для operator Post
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedByOperator()
    {
        // авторизуемся
        $this->login('operator');
        $this->dispatch('/delivery', 'POST');
        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Недоступность /delivery для operator Post Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedByOperatorForAjax()
    {
        // авторизуемся
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является contractor
        $this->assertEquals('test2', $deal->getContractor()->getUser()->getLogin());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'idDeal' => $deal->getId(),
            'type_form' => DeliveryConfirmForm::TYPE_FORM,
            'csrf' => $csrf,
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/delivery', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Недоступность /delivery для customer если сделка не оплачена Post Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedByCustomerIfDealNotPaidForAjax()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::NOT_PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        //проверяем что сделка не оплачена
        $this->assertNull($payment->getDeFactoDate());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'idDeal' => $deal->getId(),
            'type_form' => DeliveryConfirmForm::TYPE_FORM,
            'csrf' => $csrf,
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/delivery', 'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(LogicException::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED, $data['code']);
        $this->assertEquals(LogicException::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED_MESSAGE, $data['message']);
    }

    /**
     * Недоступность /delivery для customer если спор открыт Post Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedByCustomerIfDisputeNotClosedForAjax()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        //проверяем что спор не закрыт
        $this->assertNotTrue($dispute->isClosed());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'idDeal' => $deal->getId(),
            'type_form' => DeliveryConfirmForm::TYPE_FORM,
            'csrf' => $csrf,
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/delivery', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(LogicException::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED, $data['code']);
        $this->assertEquals(LogicException::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED_MESSAGE, $data['message']);
    }

    /**
     * Недоступность /delivery для customer если сделка уже подтверждена Post Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedByCustomerIfDeliveryConfirmed()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());

        // Create new Delivery object
        $delivery = new Delivery();
        $delivery->setDeliveryDate(new \DateTime());
        $delivery->setDeliveryStatus(true);
        $delivery->setDeal($deal);

        $this->entityManager->persist($delivery);
        $this->entityManager->flush();

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'idDeal' => $deal->getId(),
            'type_form' => DeliveryConfirmForm::TYPE_FORM,
            'csrf' => $csrf,
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/delivery', 'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(LogicException::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED, $data['code']);
        $this->assertEquals(LogicException::DELIVERY_CONFIRMATION_CREATION_NOT_ALLOWED_MESSAGE, $data['message']);
    }

    /**
     * Проверка /delivery для customer с не валидными данными Post Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionWithInvalidPostByCustomer()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());

        $postData = [
            'idDeal' => $deal->getId(),
            'type_form' => DeliveryConfirmForm::TYPE_FORM,
            'csrf' => '12331221212', //invalid csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/delivery', 'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals('The form submitted did not originate from the expected site', $data['data']['csrf']['notSame']);
    }

    /**
     * Проверка /delivery для customer с валидными данными Post Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionWithValidPostByCustomer()
    {
        // авторизуемся
        $this->login('test');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);
        // Проверяем что пользователь является customer
        $this->assertEquals('test', $deal->getCustomer()->getUser()->getLogin());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'idDeal' => $deal->getId(),
            'type_form' => DeliveryConfirmForm::TYPE_FORM,
            'csrf' => $csrf,
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/delivery', 'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryController::class);
        $this->assertMatchedRouteName('delivery');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('delivery confirmed', $data['message']);
    }



    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}