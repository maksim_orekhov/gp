<?php

namespace ApplicationTest\Controller;

use Application\Controller\TribunalController;
use Application\Entity\Dispute;
use Application\Form\TribunalForm;
use Application\Service\Dispute\DisputeManager;
use ApplicationTest\Bootstrap;
use Core\Service\Base\BaseRoleManager;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\Deal;
use Application\Entity\User;
use CoreTest\ViewVarsTrait;

/**
 * Class TribunalControllerTest
 * @package ApplicationTest\Controller
 */
class TribunalControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEXT_DEAL_NAME = 'Сделка Debit Matched';
    const TEST_DEAL_WITH_DISPUTE_NAME = 'Сделка Спор открыт';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;
    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->disputeManager = $serviceManager->get(DisputeManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// Get

    /**
     * Доступность /dispute/:idDispute Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     *
     * @throws \Exception
     */
    public function testTribunalCanBeAccessedByOperator()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Operator');
        $this->login('operator');

        $this->dispatch('/dispute/'.$dispute->getId().'/tribunal', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(TribunalController::class);
        $this->assertControllerClass('TribunalController');
        $this->assertMatchedRouteName('dispute-tribunal');
        // Что отдается в шаблон
        $this->assertArrayHasKey('tribunalForm', $view_vars);
        $this->assertInstanceOf(TribunalForm::class, $view_vars['tribunalForm']);
    }

    /**
     * /deal/:idDeal/tribunal/request
     *
     * Доступность покупателем сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group tribunal-request
     *
     * @throws \Exception
     */
    public function testAllowedCreateFormForCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);

        $this->dispatch('/deal/'.$deal->getId().'/tribunal/request', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(TribunalController::class);
        $this->assertMatchedRouteName('deal-tribunal-request');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('tribunalRequestForm', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);
    }

    /**
     * /deal/:idDeal/tribunal/request
     *
     * Не доступность продовцу сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group arbitrage-request
     * @throws \Exception
     */
    public function testNotAllowedCreateFormForContractor()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        /** @var User $user */
        $user = $deal->getContractor()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);

        $this->dispatch('/deal/'.$deal->getId().'/tribunal/request', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(TribunalController::class);
        $this->assertMatchedRouteName('deal-tribunal-request');
    }

    /**
     * @param null $deal_name
     * @return Deal
     */
    private function getDealForTest($deal_name = null)
    {
        if (!$deal_name) $deal_name = self::TEXT_DEAL_NAME;

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        return $deal;
    }

    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var \Application\Entity\Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}