<?php

namespace ApplicationTest\Controller;

use Application\Controller\UserController;
use Application\Entity\User;
use Application\Form\UserSearchForm;
use ApplicationTest\Bootstrap;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Zend\Stdlib\ArrayUtils;
use CoreTest\ViewVarsTrait;
use Zend\Form\Element;

/**
 * Class UserControllerTest
 * @package ApplicationTest\Controller
 */
class UserControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }
//./vendor/bin/doctrine-module orm:generate-proxies
//./vendor/bin/doctrine-module orm:clear-cache:metadata
//./vendor/bin/doctrine-module orm:clear-cache:query
//./vendor/bin/doctrine-module orm:clear-cache:result

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// Index

    /**
     * Недоступность /user не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/user', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertRedirectTo('/login?redirectUrl=/user');
    }

    /**
     * Недоступность /user авторизованному пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessedVerifiedUser()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $this->dispatch('/user', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Доступность /user пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionCanBeAccessedByOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/user', 'GET');

        /** @var array $view_vars */ // Переменные, которые отдаются в twig
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');
        $this->assertQuery('.PageTable form table');

        // Должен быть ключ users
        $this->assertArrayHasKey('users', $view_vars);
        // Количество users должно быть больше 0
        $this->assertGreaterThan(0, count($view_vars['users']));
        // Должен быть ключ userFilterForm
        $this->assertArrayHasKey('userFilterForm', $view_vars);
        // В $view_vars['userFilterForm'] - экземпляр класса UserSearchForm
        $this->assertTrue($view_vars['userFilterForm'] instanceof UserSearchForm);
        // Должен быть ключ paginator
        $this->assertArrayHasKey('paginator', $view_vars);
    }

    /**
     * Доступность /user пользователю с ролью Operator для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionCanBeAccessedByOperatorForAjax()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/user', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');

        $this->assertEquals('SUCCESS', $data['status']);
        // Должен быть ключ data
        $this->assertArrayHasKey('data', $data);
        // Должен быть ключ users
        $this->assertArrayHasKey('users', $data['data']);
        // Количество users должно быть больше 0
        $this->assertGreaterThan(0, count($data['data']['users']));
    }

    /**
     * Фильтрация /user пользователей по ID
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionWithFilterById()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /* @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $this->user_login));

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'user_id' => $user->getId()
            ],
            'csrf' => $csrf
        ];

        $this->dispatch('/user', 'GET', $getData);

        /** @var array $view_vars */ // Переменные, которые отдаются в twig
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');
        $this->assertQuery('.PageTable form table');

        // Должен быть ключ users
        $this->assertArrayHasKey('users', $view_vars);
        // Количество users должно быть равно 1
        $this->assertEquals(1, count($view_vars['users']));
        // Логин пользователя $user совпадает с логином выбранного пользователя из data
        $this->assertEquals($user->getLogin(), $view_vars['users'][0]['login']);
        // Должен быть ключ userFilterForm
        $this->assertArrayHasKey('userFilterForm', $view_vars);
        // В $view_vars['userFilterForm'] - экземпляр класса UserSearchForm
        $this->assertTrue($view_vars['userFilterForm'] instanceof UserSearchForm);
        // Должен быть ключ paginator
        $this->assertArrayHasKey('paginator', $view_vars);
    }

    /**
     * Фильтрация /user пользователей по ID для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionWithFilterByIdForAjax()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /* @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $this->user_login));

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'user_id' => $user->getId()
            ],
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($getData));
        $isXmlHttpRequest = true;
        $this->dispatch('/user', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');

        $this->assertEquals('SUCCESS', $data['status']);
        // Должен быть ключ data
        $this->assertArrayHasKey('data', $data);
        // Должен быть ключ users
        $this->assertArrayHasKey('users', $data['data']);
        // Количество users должно быть равно 1
        $this->assertEquals(1, count($data['data']['users']));
        // Логин пользователя $user совпадает с логином выбранного пользователя из data
        $this->assertEquals($user->getLogin(), $data['data']['users'][0]['login']);
    }

    /**
     * Фильтрация /user пользователей по Login
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionWithFilterByLogin()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /* @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $this->user_login));

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'user_login' => $user->getLogin()
            ],
            'csrf' => $csrf
        ];

        $this->dispatch('/user', 'GET', $getData);

        /** @var array $view_vars */ // Переменные, которые отдаются в twig
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');
        $this->assertQuery('.PageTable form table');

        // Должен быть ключ users
        $this->assertArrayHasKey('users', $view_vars);
        // Количество users должно быть равно 2
        $this->assertEquals(2, count($view_vars['users']));
        // Должен быть ключ userFilterForm
        $this->assertArrayHasKey('userFilterForm', $view_vars);
        // В $view_vars['userFilterForm'] - экземпляр класса UserSearchForm
        $this->assertTrue($view_vars['userFilterForm'] instanceof UserSearchForm);
        // Должен быть ключ paginator
        $this->assertArrayHasKey('paginator', $view_vars);
    }

    /**
     * Фильтрация /user пользователей по Login для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionWithFilterByLoginForAjax()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /* @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $this->user_login));

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'user_login' => $user->getLogin()
            ],
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($getData));
        $isXmlHttpRequest = true;
        $this->dispatch('/user', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');

        $this->assertEquals('SUCCESS', $data['status']);
        // Должен быть ключ data
        $this->assertArrayHasKey('data', $data);
        // Должен быть ключ users
        $this->assertArrayHasKey('users', $data['data']);
        // Количество users должно быть равно 2
        $this->assertEquals(2, count($data['data']['users']));
    }

    /**
     * Фильтрация /user пользователей по RegisterDate Range
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionWithFilterByRegisterDateRange()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /* @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $this->user_login));
        // Change register date to specific fo testing
        $this->changeUserRegisterData($user, '25-07-2016');

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'user_created_from' => '24.07.2016',
                'user_created_till' => '26.07.2016'
            ],
            'csrf' => $csrf
        ];

        $this->dispatch('/user', 'GET', $getData);

        /** @var array $view_vars */ // Переменные, которые отдаются в twig
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');
        $this->assertQuery('.PageTable form table');

        // Должен быть ключ users
        $this->assertArrayHasKey('users', $view_vars);
        // Количество users должно быть равно 1
        $this->assertEquals(1, count($view_vars['users']));
        // Логин пользователя $user совпадает с логином выбранного пользователя из data
        $this->assertEquals($user->getLogin(), $view_vars['users'][0]['login']);
        // Должен быть ключ userFilterForm
        $this->assertArrayHasKey('userFilterForm', $view_vars);
        // В $view_vars['userFilterForm'] - экземпляр класса UserSearchForm
        $this->assertTrue($view_vars['userFilterForm'] instanceof UserSearchForm);
        // Должен быть ключ paginator
        $this->assertArrayHasKey('paginator', $view_vars);
    }

    /**
     * Фильтрация /user пользователей по RegisterDate Range для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionWithFilterByRegisterDateRangeForAjax()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /* @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $this->user_login));
        // Change register date to specific fo testing
        $this->changeUserRegisterData($user, '25-07-2016');

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'user_created_from' => '24.07.2016',
                'user_created_till' => '26.07.2016'
            ],
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($getData));
        $isXmlHttpRequest = true;
        $this->dispatch('/user', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');

        $this->assertEquals('SUCCESS', $data['status']);
        // Должен быть ключ data
        $this->assertArrayHasKey('data', $data);
        // Должен быть ключ users
        $this->assertArrayHasKey('users', $data['data']);
        // Количество users должно быть равно 1
        $this->assertEquals(1, count($data['data']['users']));
        // Логин пользователя $user совпадает с логином выбранного пользователя из data
        $this->assertEquals($user->getLogin(), $data['data']['users'][0]['login']);
    }

    /**
     * Фильтрация /user пользователей по RegisterDate Range (нет попадания в нужные даты)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionWithFilterByRegisterDateRangeNotMatch()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /* @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $this->user_login));
        // Change register date to specific fo testing
        $this->changeUserRegisterData($user, '25-07-2016');

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'user_created_from' => '23.07.2016',
                'user_created_till' => '24.07.2016'
            ],
            'csrf' => $csrf
        ];

        $this->dispatch('/user', 'GET', $getData);

        /** @var array $view_vars */ // Переменные, которые отдаются в twig
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');
        $this->assertQuery('.PageTable form table');

        // Должен быть ключ users
        $this->assertArrayHasKey('users', $view_vars);
        // Количество users должно быть равно 0
        $this->assertEquals(0, count($view_vars['users']));
        // Должен быть ключ userFilterForm
        $this->assertArrayHasKey('userFilterForm', $view_vars);
        // В $view_vars['userFilterForm'] - экземпляр класса UserSearchForm
        $this->assertTrue($view_vars['userFilterForm'] instanceof UserSearchForm);
        // Должен быть ключ paginator
        $this->assertArrayHasKey('paginator', $view_vars);
    }

    /**
     * Фильтрация /user пользователей по RegisterDate Range для Ajax (нет попадания в нужные даты)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group user
     * @throws \Exception
     */
    public function testIndexActionWithFilterByRegisterDateRangeNotMatchForAjax()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /* @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $this->user_login));
        // Change register date to specific fo testing
        $this->changeUserRegisterData($user, '25-07-2016');

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'user_created_from' => '23.07.2016',
                'user_created_till' => '24.07.2016'
            ],
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($getData));
        $isXmlHttpRequest = true;
        $this->dispatch('/user', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');

        $this->assertEquals('SUCCESS', $data['status']);
        // Должен быть ключ data
        $this->assertArrayHasKey('data', $data);
        // Должен быть ключ users
        $this->assertArrayHasKey('users', $data['data']);
        // Количество users должно быть равно 0
        $this->assertEquals(0, count($data['data']['users']));
    }

    /**
     * @param string|null $login
     * @param string|null $password
     *
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param User $user
     * @param string $date
     *
     * @throws \Exception
     */
    private function changeUserRegisterData(User $user, string $date)
    {
        $dateObject = \DateTime::createFromFormat('d-m-Y', $date);

        $user->setCreated($dateObject);

        $this->entityManager->flush($user);
    }
}