<?php

namespace ApplicationTest\Controller;

use Application\Controller\ArbitrageController;
use Application\Entity\ArbitrageRequest;
use Application\Entity\Dispute;
use Application\Service\Dispute\DisputeManager;
use Core\Service\Base\BaseRoleManager;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\Deal;
use Application\Entity\User;
use CoreTest\ViewVarsTrait;

/**
 * Class ArbitrageControllerTest
 * @package ApplicationTest\Controller
 */
class ArbitrageControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEXT_DEAL_NAME = 'Сделка Debit Matched';
    const TEST_DEAL_WITH_DISPUTE_NAME = 'Сделка Спор открыт';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        // Отключаем отправку уведомлений на email
        $this->resetConfigParameters();

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->disputeManager = $serviceManager->get(DisputeManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /**
     * /deal/arbitrage/request/:idRequest/confirm
     *
     * Доступность покупателем сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group arbitrage-request
     * @throws \Exception
     */
    public function testNotAllowedArbitrageConfirmForCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Покупатель направил запрос на Арбитраж']);

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);
        //    var_dump($deal->getDispute());
        /** @var ArrayCollection $requests */
        $requests = $dispute->getArbitrageRequests();
        /** @var ArbitrageRequest $request */
        $request = $requests->last();
        $requestId = $request->getId();


        $this->dispatch('/deal/arbitrage/request/' . $requestId . '/confirm', 'GET');

        $this->assertResponseStatusCode(302);

    }

    /**
     * /deal/arbitrage/request/:idRequest/confirm
     *
     * Доступность покупателем сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group arbitrage-request
     * @throws \Exception
     */
    public function testNotAllowedArbitrageConfirmForContractor()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Покупатель направил запрос на Арбитраж']);

        /** @var User $user */
        $user = $deal->getContractor()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);
        //    var_dump($deal->getDispute());
        /** @var ArrayCollection $requests */
        $requests = $dispute->getArbitrageRequests();
        /** @var ArbitrageRequest $request */
        $request = $requests->last();
        $requestId = $request->getId();


        $this->dispatch('/deal/arbitrage/request/' . $requestId . '/confirm', 'GET');

        $this->assertResponseStatusCode(302);
    }

    /**
     * /deal/:idDeal/arbitrage/request
     *
     * Доступность покупателем сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group arbitrage-request
     * @throws \Exception
     */
    public function testAllowedCreateFormForCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);

        $this->dispatch('/deal/' . $deal->getId() . '/arbitrage/request', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(ArbitrageController::class);
        $this->assertMatchedRouteName('deal-arbitrage-request');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('arbitrageRequestForm', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);
    }

    /**
     * /deal/:idDeal/arbitrage/request
     *
     * Не доступность продовцу сделки
     * У сделки есть Спор!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group arbitrage-request
     * @throws \Exception
     */
    public function testNotAllowedCreateFormForContractor()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);

        /** @var User $user */
        $user = $deal->getContractor()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);

        $this->dispatch('/deal/' . $deal->getId() . '/arbitrage/request', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(ArbitrageController::class);
        $this->assertMatchedRouteName('deal-arbitrage-request');
    }

    /**
     * @param null $deal_name
     * @return Deal
     */
    private function getDealForTest($deal_name = null)
    {
        if (!$deal_name) $deal_name = self::TEXT_DEAL_NAME;

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        return $deal;
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user = null)
    {
        if (!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login = null, string $password = null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param null $deal_name
     * @return Dispute
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createTestDispute($deal_name = null)
    {
        if (!$deal_name) $deal_name = self::TEXT_DEAL_NAME;

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $dispute = new Dispute();
        $dispute->setReason('Спор по сделке ' . $deal->getId() . ' для тестирования');
        $dispute->setCreated(new \DateTime());
        $dispute->setClosed(false);
        $dispute->setAuthor($deal->getCustomer());
        $dispute->setDeal($deal);

        $this->entityManager->persist($dispute);

        $this->entityManager->flush();

        return $dispute;
    }
}