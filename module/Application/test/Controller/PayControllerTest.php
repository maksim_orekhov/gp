<?php

namespace ApplicationTest\Controller;

use Application\Controller\PayController;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use Application\Service\Deal\DealManager;
use CoreTest\ViewVarsTrait;

/**
 * Class PayControllerTest
 * @package ApplicationTest\Controller
 */
class PayControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DEAL_WITH_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->dealManager = $serviceManager->get(DealManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// OptionsForm

    /**
     * Ajax
     * Недоступен для не авторизованного (Отправка POST /deal/:idDeal/pay/options)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group pay
     *
     * @throws \Exception
     */
    public function testOptionsFormForNotAuthorisedForAjax()
    {
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME));

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id' => $deal->getId(),
            'pay_variant' => PayController::BANK_TRANSFER,
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/pay/options', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PayController::class);
        $this->assertMatchedRouteName('deal_pay_options_form');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/deal/'.$deal->getId().'/pay/options', $data['data']['redirectUrl']);
    }

    /**
     * Ajax
     * Недоступен для Operator (Отправка POST /deal/:idDeal/pay/options)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group pay
     *
     * @throws \Exception
     */
    public function testOptionsFormForOperatorForAjax()
    {
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME));

        // Залогиниваем Operator
        $this->login('Operator');

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id' => $deal->getId(),
            'pay_variant' => PayController::BANK_TRANSFER,
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/pay/options', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PayController::class);
        $this->assertMatchedRouteName('deal_pay_options_form');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Ajax
     * Недоступен для Contractor (Отправка POST /deal/:idDeal/pay/options)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group pay
     *
     * @throws \Exception
     */
    public function testOptionsFormForContractorForAjax()
    {
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME));

        // Залогиниваем Контрактора
        $this->login($deal->getContractor()->getUser()->getLogin());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id' => $deal->getId(),
            'pay_variant' => PayController::BANK_TRANSFER,
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/pay/options', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PayController::class);
        $this->assertMatchedRouteName('deal_pay_options_form');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('User can not make payments for this deal', $data['message']);
    }

    /**
     * Отправка POST /deal/:idDeal/pay/options
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group pay
     *
     * @throws \Exception
     */
    public function testOptionsFormForCustomerWithBankTransferSelected()
    {
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME));

        // Залогиниваем Кастомера
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id' => $deal->getId(),
            'pay_variant' => PayController::BANK_TRANSFER,
            'csrf' => $csrf
        ];

        $this->dispatch('/deal/'.$deal->getId().'/pay/options', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PayController::class);
        $this->assertMatchedRouteName('deal_pay_options_form');
        $this->assertRedirectTo('/invoice/'.$deal->getId());
    }

    /**
     * Ajax
     * Отправка POST /deal/:idDeal/pay/options
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group pay
     *
     * @throws \Exception
     */
    public function testOptionsFormForCustomerWithBankTransferSelectedForAjax()
    {
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME));

        // Залогиниваем Кастомера
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id' => $deal->getId(),
            'pay_variant' => PayController::BANK_TRANSFER,
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/pay/options', 'POST', $postData, $isXmlHttpRequest);
        #$data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PayController::class);
        $this->assertMatchedRouteName('deal_pay_options_form');
        $this->assertRedirectTo('/invoice/'.$deal->getId());
    }

    /**
     * Отправка POST /deal/:idDeal/pay/options
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group pay
     *
     * @throws \Exception
     */
    public function testOptionsFormForCustomerWithCreditCardMandarinSelected()
    {
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME));

        // Залогиниваем Кастомера
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id' => $deal->getId(),
            'pay_variant' => PayController::ACQUIRING_MANDARIN,
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/pay/options', 'POST', $postData, $isXmlHttpRequest);
        #$data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PayController::class);
        $this->assertMatchedRouteName('deal_pay_options_form');
        $this->assertRedirectTo('/deal/'.$deal->getId().'/pay/mandarin');
    }

    /**
     * Ajax
     * Отправка POST /deal/:idDeal/pay/options
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group pay
     *
     * @throws \Exception
     */
    public function testOptionsFormForCustomerWithCreditCardMandarinSelectedForAjax()
    {
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME));

        // Залогиниваем Кастомера
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id' => $deal->getId(),
            'pay_variant' => PayController::ACQUIRING_MANDARIN,
            'csrf' => $csrf
        ];

        $this->dispatch('/deal/'.$deal->getId().'/pay/options', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PayController::class);
        $this->assertMatchedRouteName('deal_pay_options_form');
        $this->assertRedirectTo('/deal/'.$deal->getId().'/pay/mandarin');
    }


    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}