<?php

namespace ApplicationTest\Controller;

use Application\Controller\CivilLawSubjectController;
use ApplicationTest\Bootstrap;
use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;

class CivilLawSubjectControllerTest extends AbstractHttpControllerTestCase
{
    use \Core\Provider\HttpRequestTrait;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];
    }


    /**
     * /civil-law-subject/search
     * не доступность не зарегистрированному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Exception
     */
    public function testSearchActionCanNotBeAccessedByNotAuthorized()
    {
        $this->dispatch('/civil-law-subject/search', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(CivilLawSubjectController::class);
        $this->assertMatchedRouteName('civil-law-subject-search');
        $this->assertRedirectTo('/login?redirectUrl=/civil-law-subject/search');
    }

    /**
     * /civil-law-subject/search
     * не доступность зарегистрированному пользователю не по ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Exception
     */
    public function testSearchActionCanNotBeAccessedByNotAjax()
    {
        $this->login();

        $this->dispatch('/civil-law-subject/search', 'GET');

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(CivilLawSubjectController::class);
        $this->assertMatchedRouteName('civil-law-subject-search');
    }

    /**
     * Ajax
     * /civil-law-subject/search
     * отправка пустого запроса
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     *
     * @throws \Exception
     */
    public function testSearchActionWithEmptySearchDataForAjax()
    {
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/civil-law-subject/search', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(CivilLawSubjectController::class);
        $this->assertMatchedRouteName('civil-law-subject-search');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);

        $this->assertSame('ERROR', $data['status']);
        $this->assertSame('Not defined the search parameters', $data['message']);
        $this->assertNull($data['data']);
    }

    // Variants of payment order arrays
    public function providerSearchData(): array
    {
        return [
            ['ООО', true],
            ['ИП', true],
            ['Тестов Тест Тестович', true],
            ['Агафий Петр Кузьмич', false], //результат не существует
        ];
    }

    /**
     * Ajax
     * /civil-law-subject/search
     * отправка валидного запроса
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @param $searchData
     * @param $is_exist
     *
     * @group civil-law-subject
     *
     * @throws \Exception
     *
     * @dataProvider providerSearchData
     */
    public function testSearchActionWithValidSearchDataForAjax($searchData, $is_exist)
    {
        $this->login();
        $postData = [
            'search' => $searchData
        ];
        $isXmlHttpRequest = true;
        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/civil-law-subject/search', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(CivilLawSubjectController::class);
        $this->assertMatchedRouteName('civil-law-subject-search');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);

        $this->assertSame('SUCCESS', $data['status']);
        $this->assertSame('search result', $data['message']);
        $this->assertNotNull($data['data']);
        if ( $is_exist ) {
            $lastSearchResult = end($data['data']);
            $this->assertArrayHasKey('id', $lastSearchResult);
            $this->assertArrayHasKey('name', $lastSearchResult);
        } else {
            $this->assertEmpty($data['data']);
        }
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) {
            $login = $this->user_login;
        }
        if(!$password) {
            $password = $this->user_password;
        }
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

}