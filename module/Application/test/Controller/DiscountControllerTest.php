<?php

namespace ApplicationTest\Controller;

use Application\Controller\DiscountController;
use Application\Entity\Discount;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Form\DiscountForm;
use Application\Service\Dispute\DiscountManager;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Payment\PaymentMethodManager;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseRoleManager;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\Deal;
use Application\Entity\User;
use CoreTest\ViewVarsTrait;
use Zend\Form\Element;

/**
 * Class DiscountControllerTest
 * @package ApplicationTest\Controller
 */
class DiscountControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_DEAL_NAME = 'Сделка Debit Matched';
    const TEST_DEAL_WITH_DISPUTE_NAME = 'Сделка Спор открыт';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var DiscountManager
     */
    private $discountManager;
    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;
    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->disputeManager = $serviceManager->get(DisputeManager::class);
        $this->discountManager = $serviceManager->get(DiscountManager::class);
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// getList

    // Неавторизованный

    /**
     * Недоступность /discount (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/discount', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');
        $this->assertRedirectTo('/login?redirectUrl=/discount');
    }

    /**
     * Для Ajax
     * Недоступность /discount (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/discount', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/discount', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Доступность /discount (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/discount', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');
        // Что отдается в шаблон
        $this->assertArrayHasKey('discounts', $view_vars);
        $this->assertGreaterThan(0, count($view_vars['discounts']));
    }

    /**
     * Для Ajax
     * Доступность /discount (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/discount', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Discounts', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('discounts', $data['data']);
        $this->assertArrayHasKey('paginator', $data['data']);
    }

    /**
     * Недоступность /discount (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByVerifiedUser()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/discount', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Ajax
     * Недоступность /discount (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByVerifiedUserForAjax()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/discount', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /////////////// get

    // Неавторизованный

    /**
     * Недоступность /discount/[id] (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        $deal_name = 'Сделка Спор со Скидкой закрыт без продления';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Discount $discount */
        $discount = $dispute->getDiscount();

        // Проверяем, что Скидка создалась
        $this->assertInstanceOf(Discount::class, $discount);

        $this->dispatch('/discount/'.$discount->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('deal_discount_single');
        $this->assertRedirectTo('/login?redirectUrl=/discount/'.$discount->getId());
    }

    /**
     * Для Ajax
     * Недоступность /discount/[id] (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $deal_name = 'Сделка Спор со Скидкой закрыт без продления';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Discount $discount */
        $discount = $dispute->getDiscount();

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/'.$discount->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('deal_discount_single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/discount/'.$discount->getId(), $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Доступность /discount/[id] (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Спор со Скидкой закрыт без продления';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Discount $discount */
        $discount = $dispute->getDiscount();

        $this->dispatch('/discount/'.$discount->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('deal_discount_single');
        // Что отдается в шаблон
        $this->assertArrayHasKey('discount', $view_vars);
    }

    /**
     * Для Ajax
     * Доступность /discount/[id] (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Спор со Скидкой закрыт без продления';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Discount $discount */
        $discount = $dispute->getDiscount();

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/'.$discount->getId(),null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Discount single', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('discount', $data['data']);
    }

    // Не оператор

    /**
     * Недоступность /discount/[id] (GET на get) пользователю Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByVerified()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $deal_name = 'Сделка Спор со Скидкой закрыт без продления';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Discount $discount */
        $discount = $dispute->getDiscount();

        // Проверяем, что Скидка создалась
        $this->assertInstanceOf(Discount::class, $discount);

        $this->dispatch('/discount/'.$discount->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('deal_discount_single');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Для Ajax
     * Недоступность /discount/[id] (GET на get) пользователю Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByVerifiedForAjax()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $deal_name = 'Сделка Спор со Скидкой закрыт без продления';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Discount $discount */
        $discount = $dispute->getDiscount();

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/'.$discount->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('deal_discount_single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }


    /////////////// create

    // Неавторизованный

    /**
     * Ajax
     * Недоступность /discount (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorised()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/discount', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');
        $this->assertRedirectTo('/login?redirectUrl=/discount');
    }

    /**
     * Для Ajax
     * Недоступность /discount (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/discount', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /discount (POST на create) Оператору по HTTP (404)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        // Экшен только для Ajax, поэтому не важно какие данные - не пропустит
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/discount', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');
    }

    /**
     * Для Ajax
     * Доступность /discount (POST на create) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     * @throws \Core\Exception\LogicException
     * @throws \Exception
     */
    public function testCreateCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        /** @var PaymentMethodBankTransfer $customerPaymentMethodBankTransfer */
        $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($deal);
        $customerPaymentMethodBankTransfer = $customerPaymentMethod ? $customerPaymentMethod->getPaymentMethodBankTransfer() : null;

        $postData = [
            'deal'              => $deal->getId(),
            'dispute_id'        => $dispute->getId(),
            'bank_transfer_id'  => $customerPaymentMethodBankTransfer->getId(),
            'type_form'         => DiscountForm::TYPE_FORM,
            'amount_discount'   => (int)$discountRequest->getAmount(),
            'csrf'              => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Discount created', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('discount', $data['data']);
    }

    /**
     * Для Ajax
     * Доступность /discount (POST валидный на create) пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     * @throws \Core\Exception\LogicException
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerifiedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        /** @var PaymentMethodBankTransfer $customerPaymentMethodBankTransfer */
        $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($deal);
        $customerPaymentMethodBankTransfer = $customerPaymentMethod ? $customerPaymentMethod->getPaymentMethodBankTransfer() : null;

        $postData = [
            'deal'              => $deal->getId(),
            'dispute_id'        => $dispute->getId(),
            'bank_transfer_id'  => $customerPaymentMethodBankTransfer->getId(),
            'type_form'         => DiscountForm::TYPE_FORM,
            'amount_discount'   => (int)$discountRequest->getAmount(),
            'csrf'              => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных пользователем с ролью Verified /profile/legal-entity
     * (POST невалидный на create)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     * @throws \Core\Exception\LogicException
     * @throws \Exception
     */
    public function testCreateWithInvalidPostForVerifiedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        // Invalid POST

        /** @var PaymentMethodBankTransfer $customerPaymentMethodBankTransfer */
        $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($deal);
        $customerPaymentMethodBankTransfer = $customerPaymentMethod ? $customerPaymentMethod->getPaymentMethodBankTransfer() : null;

        $postData = [
            'deal'              => $deal->getId(),
            'dispute_id'        => $dispute->getId(),
            'bank_transfer_id'  => $customerPaymentMethodBankTransfer->getId(),
            'type_form'         => DiscountForm::TYPE_FORM,
            'amount_discount'   => (int)$discountRequest->getAmount(),
            'csrf'              => 'ghghgh6678687gfgfdg3443434'
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/discount', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayNotHasKey('discount', $data['data']);
    }


    /////////////// createForm

    // Неавторизованный

    /**
     * Недоступность /dispute/:idDispute/discount/create (GET на createFormAction)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorised()
    {
        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/create');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-create');
        $this->assertRedirectTo('/login?redirectUrl=/dispute/'.$dispute->getId().'/discount/create');
    }

    /**
     * Для Ajax
     * Недоступность /dispute/:idDispute/discount/create (GET на createFormAction) по Ajax
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $isXmlHttpRequest = true;
        $this->dispatch('/dispute/'.$dispute->getId().'/discount/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/dispute/'.$dispute->getId().'/discount/create', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /dispute/:idDispute/discount/create (GET на createFormAction) по Ajax
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $isXmlHttpRequest = true;
        $this->dispatch('/dispute/'.$dispute->getId().'/discount/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Для Ajax
     * Недоступность /dispute/:idDispute/discount/create (GET на createFormAction) по Ajax
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Verified пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $isXmlHttpRequest = true;
        $this->dispatch('/dispute/'.$dispute->getId().'/discount/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Доступность /dispute/:idDispute/discount/create (GET на createFormAction) Оператору по HTTP
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testCreateFormCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Verified пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/create');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('payment_method_bank_transfer', $view_vars);
        $this->assertArrayHasKey('discountForm', $view_vars);
    }

    /**
     * /dispute/:idDispute/discount/create (POST валидный на create)
     * пользователем с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Core\Exception\LogicException
     * @throws \Exception
     */
    public function testCreateFormWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Verified пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        /** @var PaymentMethodBankTransfer $customerPaymentMethodBankTransfer */
        $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($deal);
        $customerPaymentMethodBankTransfer = $customerPaymentMethod ? $customerPaymentMethod->getPaymentMethodBankTransfer() : null;

        $postData = [
            'deal'              => $deal->getId(),
            'dispute_id'        => $dispute->getId(),
            'bank_transfer_id'  => $customerPaymentMethodBankTransfer->getId(),
            'type_form'         => DiscountForm::TYPE_FORM,
            'amount_discount'   => (int)$discountRequest->getAmount(),
            'csrf'              => $csrf
        ];

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/create','POST', $postData);

        // Проверяем, что у Dispute появился Discount
        $discount = $this->entityManager->getRepository(Discount::class)
            ->findOneBy(array('dispute' => $dispute));
        $this->assertInstanceOf(Discount::class, $discount);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-create');
        $this->assertRedirectTo('/dispute/'.$dispute->getId());
    }

    /**
     * Создание NaturalPerson
     * Отправка заведомо невалидных данных /profile/legal-entity/create
     * (POST невалидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Core\Exception\LogicException
     * @throws \Exception
     */
    public function testCreateFormWithInvalidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Verified пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        // Invalid POST

        /** @var PaymentMethodBankTransfer $customerPaymentMethodBankTransfer */
        $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($deal);
        $customerPaymentMethodBankTransfer = $customerPaymentMethod ? $customerPaymentMethod->getPaymentMethodBankTransfer() : null;

        $postData = [
            'deal'              => $deal->getId(),
            'dispute_id'        => $dispute->getId(),
            'bank_transfer_id'  => $customerPaymentMethodBankTransfer->getId(),
            'type_form'         => DiscountForm::TYPE_FORM,
            'amount_discount'   => (int)$discountRequest->getAmount(),
            'csrf'              => 'jhjhjhjhrt6454545fgfgfgfgfgf'
        ];

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/create','POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        // Проверяем, что у Dispute не появился Discount
        $discount = $this->entityManager->getRepository(Discount::class)
            ->findOneBy(array('dispute' => $dispute));
        $this->assertNull($discount);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('payment_method_bank_transfer', $view_vars);
        $this->assertArrayHasKey('discountForm', $view_vars);
    }


    /////////////// formInit (экшен только для Ajax!)

    // Неавторизованный

    /**
     * Недоступность /discount/form-init (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     *
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/discount/form-init', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-init');
        $this->assertRedirectTo('/login?redirectUrl=/discount/form-init');
    }

    /**
     * Для Ajax
     * Недоступность /discount/form-init (GET на formInitAction)
     * не авторизованному пользователю по Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/discount/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-init');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/discount/form-init', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Доступность /discount/form-init (GET на formInitAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Core\Exception\LogicException
     * @throws \Exception
     */
    public function testFormInitWithParamByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/form-init', 'GET', ['idDispute' => $dispute->getId()], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertEquals('operator', $data['data']['user']['login']);
        $this->assertArrayHasKey('dispute', $data['data']);
        $this->assertEquals($dispute->getReason(), $data['data']['dispute']['reason']);
        $this->assertArrayHasKey('is_discount_done', $data['data']);
        $this->assertEquals($this->discountManager->isDiscountDone($dispute), $data['data']['is_discount_done']);
        $this->assertArrayHasKey('max_discount', $data['data']);
        $this->assertEquals($this->discountManager->getMaxDiscount($dispute->getDeal()), $data['data']['max_discount']);
    }

    /**
     * Ajax
     * Недоступность /discount/form-init  (GET на formInitAction)
     * авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByVerifiedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('test');
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/discount/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DiscountController::class);
        $this->assertMatchedRouteName('dispute-discount-form-init');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }


    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var \Application\Entity\Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param null $deal_name
     * @return Deal
     */
    private function getDealForTest($deal_name = null)
    {
        if (!$deal_name) $deal_name = self::TEST_DEAL_NAME;

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        return $deal;
    }
}