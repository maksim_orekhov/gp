<?php

namespace ApplicationTest\Controller;

use Application\Controller\LegalEntityController;
use Application\Entity\CivilLawSubject;
use Application\Entity\LegalEntity;
use Core\Service\Base\BaseRoleManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use CoreTest\ViewVarsTrait;

/**
 * Class LegalEntityControllerForOperatorTest
 * @package ApplicationTest\Controller
 */
class LegalEntityControllerForOperatorTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;
    /**
     * @var baseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// getList

    // Неавторизованный

    /**
     * Недоступность /legal-entity (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/legal-entity', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity');
        $this->assertRedirectTo('/login?redirectUrl=/legal-entity');
    }

    /**
     * Для Ajax
     * Недоступность /legal-entity (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/legal-entity', null, [], $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /legal-entity (GET на getList) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNotOperator()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/legal-entity', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /legal-entity (GET на getList) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNotOperatorForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/legal-entity', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity');
        $this->assertRedirectTo('/not-authorized');
    }

    // Оператор

    /**
     * Доступность /legal-entity (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/legal-entity', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity');
        // Что отдается в шаблон
        $this->assertArrayHasKey('legalEntities', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#legal-entity-filter-form');
    }

    /**
     * Для Ajax
     * Доступность /legal-entity (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperatorForAjax()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/legal-entity', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $legalEntity = $data['data']['legalEntities'][0];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('LegalEntity collection', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('legalEntities', $data['data']);
        $this->assertArrayHasKey('paginator', $data['data']);
        $this->assertEquals('Test Company', $legalEntity['name']);
    }

    /////////////// get

    // Неавторизованный

    /**
     * Недоступность /legal-entity/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());

        $this->dispatch('/legal-entity/'.$newTestLegalEntity->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity-single');
        $this->assertRedirectTo('/login?redirectUrl=/legal-entity/'.$newTestLegalEntity->getId());
    }

    /**
     * Для Ajax
     * Недоступность /legal-entity/id  (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $isXmlHttpRequest = true;
        $this->dispatch('/legal-entity/'.$newTestLegalEntity->getId(), null, [], $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Не Оператор

    /**
     * Недоступность /legal-entity/id (GET на get) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOperator()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $this->dispatch('/legal-entity/'.$newTestLegalEntity->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /legal-entity/id (GET на get) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOperatorByGetForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $isXmlHttpRequest = true;
        $this->dispatch('/legal-entity/'.$newTestLegalEntity->getId(), null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Оператор

    /**
     * Доступность /legal-entity/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOperator()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/legal-entity/'.$newTestLegalEntity->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity-single');
        // Что отдается в шаблон
        $this->assertArrayHasKey('legalEntity', $view_vars);
        $this->assertArrayHasKey('paymentMethods', $view_vars);
        $this->assertArrayHasKey('civil_law_subject_id', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        // Возвращается из шаблона
    }

    /**
     * Для Ajax
     * Доступность /legal-entity/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOperatorForAjax()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/legal-entity/'.$newTestLegalEntity->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $legalEntity = $data['data']['legalEntity'];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity-single');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('LegalEntity single', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('legalEntity', $data['data']);
        $this->assertEquals('Test Company', $legalEntity['name']);
        $this->assertEquals($legalEntity['id'], $newTestLegalEntity->getId());
    }


    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $login
     * @return LegalEntity
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createNewTestLegalEntity($login = 'test')
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $login));

        // Create CivilLawSubject
        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern(true);
        $civilLawSubject->setCreated(new \DateTime());

        $this->entityManager->persist($civilLawSubject);

        // Create LegalEntity

        $legalEntity = new LegalEntity();
        $legalEntity->setCivilLawSubject($civilLawSubject);
        $legalEntity->setName('Test Company');
        $legalEntity->setKpp('123456');
        $legalEntity->setInn('123456');
        $legalEntity->setLegalAddress('Ямского поля, дом 2');
        $legalEntity->setOgrn('3456778');
        $legalEntity->setPhone('(495) 123-34-56');

        $civilLawSubject->setLegalEntity($legalEntity);

        $this->entityManager->persist($legalEntity);

        $this->entityManager->flush();

        return $legalEntity;
    }
}