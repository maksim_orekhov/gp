<?php

namespace ApplicationTest\Controller;

use Application\Controller\LegalEntityBankDetailController;
use Application\Entity\CivilLawSubject;
use Application\Entity\LegalEntity;
use Application\Entity\LegalEntityBankDetail;
use Application\Entity\LegalEntityDocument;
use Application\Entity\LegalEntityDocumentType;
use Application\Entity\NdsType;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseRoleManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use Zend\Form\Element;
use CoreTest\ViewVarsTrait;

/**
 * Class LegalEntityBankDetailControllerTest
 * @package ApplicationTest\Controller
 */
class LegalEntityBankDetailControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DEFAULT_DOCUMENT_NAME = 'test name for legal entity bank detail';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * тестовый документ LegalEntityBankDetail
     * @var
     */
    private $testLegalEntityBankDetail;

    /**
     * тестое юр лицо LegalEntity
     * @var
     */
    private $testLegalEntity;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();


        // create test document
        $this->testLegalEntityBankDetail = $this->createTestLegalEntityBankDetail($this->user_login);
        $this->testLegalEntity = $this->testLegalEntityBankDetail->getLegalEntityDocument()->getLegalEntity();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    // ******************************
    // ******* method getList *******
    // ******************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail
     * (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/legal-entity-bank-detail', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-bank-detail');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail
     * (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-bank-detail', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail
     * (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity-bank-detail', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail
     * (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /profile/legal-entity-bank-detail
     * (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUser()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/legal-entity-bank-detail', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');
        // Что отдается в шаблон
        $this->assertArrayHasKey('bankDetails', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity-bank-detail
     * (GET на getList) авторизованному с ролью Verified не владелец
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUserNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(true, isset($data['data']['paginator']));
        $this->assertEquals(true, is_array($data['data']['paginator']));
        $this->assertEquals(true, isset($data['data']['bankDetails']));

        //ожидаем пусто т.к единственный документ принадлежит пользователь 'test'
        $this->assertEquals(0, count($data['data']['bankDetails']));
    }

    /**
     * Для Ajax
     * Доступность /profile/legal-entity-bank-detail
     * (GET на getList) авторизованному с ролью Verified владелец
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUserOwnerForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(true, isset($data['data']['paginator']));
        $this->assertEquals(true, is_array($data['data']['paginator']));
        $this->assertEquals(true, isset($data['data']['bankDetails']));

        //ожидаем 1 т.к единственный документ принадлежит текушему пользователю
        $this->assertEquals(1, count($data['data']['bankDetails']));
        $this->assertEquals(self::DEFAULT_DOCUMENT_NAME, $data['data']['bankDetails'][0]['name']);
    }

    // ******************************
    // ********* method get *********
    // ******************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-bank-detail/'.$id);
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-bank-detail/'.$id, $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperator()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperatorForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Не Владелец

    /**
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOwner()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Ajax
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Не проходит. Разобраться!
     */
    public function testGetCanBeAccessedByOwner()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertQuery('.container');
        // Что отдается в шаблон
        $this->assertArrayHasKey('bankDetail', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity-bank-detail/:id
     * (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Не проходит. Разобраться!
     */
    public function testGetCanBeAccessedByOwnerForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');

        $data = json_decode($this->getResponse()->getContent(), true);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(true, isset($data['data']['bankDetail']));
        $this->assertEquals(self::DEFAULT_DOCUMENT_NAME, $data['data']['bankDetail']['name']);
    }

    // *****************************
    // ******* method update *******
    // *****************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-bank-detail/'.$id);
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-bank-detail/'.$id, $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST на update) по HTTP - 404
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByHttp()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST на update) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Возвращает не ту ошибку - ERROR_INVALID_FORM_DATA вместо ERROR_ACCESS_DENIED
     */
    public function testUpdateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Не владелец

    /**
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST на update) по HTTP
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * Important! update method is only for Ajax!
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNotOwner()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(404);
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST на update) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Возвращает ошибку ERROR_INVALID_FORM_DATA вместо ERROR_ACCESS_DENIED
     */
    public function testUpdateCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
    }

    // Владелец

    /**
     * Для Ajax
     * Отправка валидных данных /profile/legal-entity-bank-detail/:id
     * (POST валидный на update) пользователем с ролью Verified Владельцем
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateWithValidPostForOwnerForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $name = 'test post update legal entity bank detail';

        $postData = [
            'name' => $name,
            'checking_account' => '123456789012345',
            'correspondent_account' => '123456789012345',
            'bic' => '123456789',
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(array('name' => $name));

        $this->assertEquals($name, $legalEntityBankDetail->getName());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных /profile/legal-entity-bank-detail/:id
     * (POST невалидный на update) пользователем с ролью Verified Владельцем
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwnerForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $name = 'test post update legal entity bank detail';

        // Invalid POST
        $postData = [
            'name' => $name,
            'checking_account' => '123456789012345',
            'correspondent_account' => '123456789012345',
            'bic' => '123456789',
            'csrf' => '123invalid456'
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(array('name' => $name));

        $this->assertNull($legalEntityBankDetail);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
    }

    // *****************************
    // ******* method create *******
    // *****************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail
     * (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorised()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-bank-detail', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-bank-detail');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail
     * (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-bank-detail', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail
     * (POST на create) по HTTP - 404
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByHttp()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-bank-detail', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail
     * (POST на create) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Возвращает ошибку ERROR_INVALID_FORM_DATA вместо ERROR_ACCESS_DENIED
     */
    public function testCreateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /profile/legal-entity-bank-detail
     * (POST валидный на create) по HTTP (404)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-bank-detail', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');
    }

    /**
     * Для Ajax
     * Доступность /profile/legal-entity-bank-detail
     * (POST валидный на create) пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerifiedForAjax()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $name = 'test create legal entity bank detail';

        $postData = [
            'idLegalEntity' => $idLegalEntity,
            'name' => $name,
            'checking_account' => '123456789012345',
            'correspondent_account' => '123456789012345',
            'bic' => '123456789',
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(array('name' => $name));

        $this->assertEquals($name, $legalEntityBankDetail->getName());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');

        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных /profile/legal-entity-bank-detail
     * (POST невалидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithInvalidPostForVerifiedForAjax()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $name = 'test create legal entity bank detail';

        //invalid Post
        $postData = [
            'idLegalEntity' => $idLegalEntity,
            'name' => $name,
            'checking_account' => '123456789012345',
            'correspondent_account' => '123456789012345',
            'bic' => '123456789',
            'csrf' => '123invalid345'
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(array('name' => $name));

        $this->assertNull($legalEntityBankDetail);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail');

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
    }

    // *****************************
    // ******* method delete *******
    // *****************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-bank-detail/'.$id);
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-bank-detail/'.$id, $data['data']['redirectUrl']);
    }

    // Не владелец

    /**
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwner()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperator()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperatorForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Доступность /profile/legal-entity-bank-detail/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Не проходит. Разобраться!
     */
    public function testDeleteForOwner()
    {
        $testName = 'nameForDelete';
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->createTestLegalEntityBankDetail('test', $testName);
        $id = $legalEntityBankDetail->getId();
        $idLegalEntity = $legalEntityBankDetail->getLegalEntityDocument()->getLegalEntity()->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData);

        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(array('name' => $testName));
        // Must be null (deleted)
        $this->assertNull($legalEntityBankDetail);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertRedirectTo('/profile/legal-entity/'.$idLegalEntity);
    }

    /**
     * Для Ajax
     * Доступность /profile/legal-entity-bank-detail/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Не проходит. Разобраться!
     */
    public function testDeleteForOwnerForAjax()
    {
        $testName = 'nameForDelete';
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->createTestLegalEntityBankDetail('test', $testName);
        $id = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(array('name' => $testName));
        // Must be null (deleted)
        $this->assertNull($legalEntityBankDetail);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-single');
        $this->assertEquals('SUCCESS', $data['status']);
    }
    // @TODO Добавить тесты для попытки удаления субъекта права, когда он используется в сделке
    // После прояснения ситуации в тикете http://jira.simple-tech.org:8080/browse/GP-336

    // *****************************
    // ***** method createForm *****
    // *****************************

    /**
     * Не доступность /profile/legal-entity/:idLegalEntity/bank-detail/create
     * (GET на createFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        $this->dispatch('/profile/legal-entity/'.$idLegalEntity.'/bank-detail/create', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-create');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity/'.$idLegalEntity.'/bank-detail/create');
    }

    /**
     * Для Ajax
     * Не доступность /profile/legal-entity/:idLegalEntity/bank-detail/create
     * (GET на createFormAction) по Ajax не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$idLegalEntity.'/bank-detail/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity/'.$idLegalEntity.'/bank-detail/create', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/:idLegalEntity/bank-detail/create
     * (GET на createFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$idLegalEntity.'/bank-detail/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/legal-entity/:idLegalEntity/bank-detail/create
     * (GET на createFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Возвращвет 500
     */
    public function testCreateFormCanNotBeAccessedByOperator()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity/'.$idLegalEntity.'/bank-detail/create', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-create');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /profile/legal-entity/:idLegalEntity/bank-detail/create
     * (GET на createFormAction) пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormCanBeAccessedByVerified()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/legal-entity/'.$idLegalEntity.'/bank-detail/create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('bankDetailForm', $view_vars);
    }

    /**
     * Создание LegalEntityBankDetail /profile/legal-entity/:idLegalEntity/bank-detail/create
     * (POST валидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormWithValidPostForVerified()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $name = 'test create form legal entity bank detail';

        $postData = [
            'name' => $name,
            'checking_account' => '123456789012345',
            'correspondent_account' => '123456789012345',
            'bic' => '123456789',
            'csrf' => $csrf
        ];

        $this->dispatch('/profile/legal-entity/'.$idLegalEntity.'/bank-detail/create', 'POST', $postData);

        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(array('name' => $name));

        $this->assertEquals($name, $legalEntityBankDetail->getName());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-create');
        $this->assertRedirectTo('/profile/legal-entity/'.$idLegalEntity);
    }

    /**
     * Недоступность LegalEntityBankDetail /profile/legal-entity/:idLegalEntity/bank-detail/create
     * (POST валидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormForAlienLegalEntityWithValidPostForVerified()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity/'.$idLegalEntity.'/bank-detail/create', 'POST', $postData);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-create');
    }

    /**
     * Создание LegalEntityBankDetail
     * Отправка заведомо невалидных данных пользователем с ролью Verified
     * /profile/legal-entity/:idLegalEntity/bank-detail/create
     * (POST невалидный на create)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormWithInvalidPostForVerified()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $name = 'test create form legal entity bank detail';

        $postData = [
            'name' => $name,
            'checking_account' => '123456789012345',
            'correspondent_account' => '123456789012345',
            'bic' => '123456789',
            'csrf' => '123invalid345' // Wrong csrf
        ];

        $this->dispatch('/profile/legal-entity/'.$idLegalEntity.'/bank-detail/create', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(array('name' => $name));

        $this->assertNull($legalEntityBankDetail);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('bankDetailForm', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#legal-entity-bank-detail-crete-form');
    }

    // *****************************
    // ***** method editForm *******
    // *****************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail/:idBankDetail/edit
     * (GET на editFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/edit');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-edit');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-bank-detail/'.$idBankDetail.'/edit');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/:idBankDetail/edit
     * (GET на editFormAction) по Ajax неавторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/edit',null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-bank-detail/'.$idBankDetail.'/edit', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/:idBankDetail/edit
     * (GET на editFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');


        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/edit',null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/legal-entity-bank-detail/:idBankDetail/edit
     * (GET на editFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Возвращает 500 и bad data provided: not found bank detail
     */
    public function testEditFormCanNotBeAccessedByOperator()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/edit', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-edit');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /profile/legal-entity-bank-detail/:idBankDetail/edit
     * (GET на editFormAction) не Владельцу (500)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNotOwner()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/edit', 'GET');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-edit');
    }

    /**
     * Доступность /profile/legal-entity-bank-detail/:idBankDetail/edit
     * (GET на editFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanBeAccessedByOwner()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('bankDetailForm', $view_vars);
    }

    /**
     * Редактирование /profile/legal-entity-bank-detail/:idBankDetail/edit
     * (POST валидный на editFormAction) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormWithValidPostForVerified()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $name = 'test edit form legal entity bank detail 2';

        $postData = [
            'name' => $name,
            'checking_account' => '123456789012345',
            'correspondent_account' => '123456789012345',
            'bic' => '123456789',
            'csrf' => $csrf
        ];

        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/edit', 'POST', $postData);

        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(array('name' => $name));

        $this->assertEquals($name, $legalEntityBankDetail->getName());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-edit');
        $this->assertRedirectTo('/profile/legal-entity/'.$legalEntityBankDetail->getLegalEntityDocument()->getLegalEntity()->getId());
    }

    /**
     * Отправка заведомо невалидных данных /profile/legal-entity-bank-detail/:idBankDetail/edit
     * (POST невалидный на editFormAction) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwner()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Invalid POST
        $name = 'test update form legal entity bank detail 2';

        $postData = [
            'name' => $name,
            'checking_account' => '123456789012345',
            'correspondent_account' => '123456789012345',
            'bic' => '123456789',
            'csrf' => '123invalid456'
        ];

        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/edit', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(array('name' => $name));

        $this->assertNull($legalEntityBankDetail);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('bankDetailForm', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#legal-entity-bank-detail-edit-form');
    }

    // *****************************
    // **** method deleteForm ******
    // *****************************

    // Неавторизованный

    /**
     * Недоступность /legal-entity-bank-detail/:idBankDetail/delete
     * (GET на deleteFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/delete');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-delete');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-bank-detail/'.$idBankDetail.'/delete');
    }

    /**
     * Для Ajax
     * Недоступность /legal-entity-bank-detail/:idBankDetail/delete
     * (GET на deleteFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/delete','GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-delete');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-bank-detail/'.$idBankDetail.'/delete', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /legal-entity-bank-detail/:idBankDetail/delete
     * (GET на deleteFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/delete','GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-delete');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /legal-entity-bank-detail/:idBankDetail/delete
     * (GET на deleteFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Права доступа не работают
     */
    public function testDeleteFormCanNotBeAccessedByOperator()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/delete','GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-delete');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /legal-entity-bank-detail/:idBankDetail/delete
     * (GET на deleteFormAction) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Права доступа не работают
     */
    public function testDeleteFormCanNotBeAccessedByNotOwner()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/delete','GET');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-delete');
    }

    /**
     * Доступность /legal-entity-bank-detail/:idBankDetail/delete
     * (GET на deleteFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormCanBeAccessedByOwner()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $idBankDetail = $legalEntityBankDetail->getId();

        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/legal-entity-bank-detail/'.$idBankDetail.'/delete','GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-delete');
        // Что отдается в шаблон
        $this->assertArrayHasKey('bankDetail', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#legal-entity-bank-detail-delete-form');
    }

    // *****************************
    // ****** method formInit ******
    // *****************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-bank-detail/form-init
     * (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/legal-entity-bank-detail/form-init', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-init');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-bank-detail/form-init');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/form-init
     * (GET на formInitAction)
     * не авторизованному пользователю по Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-init');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-bank-detail/form-init', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-bank-detail/form-init
     * (GET на formInitAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Оператор пускает. Нужно ли?
     */
    public function testFormInitCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-init');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity-bank-detail/form-init
     * (GET на formInitAction) авторизованному с ролью Verified без параметров
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testFormInitCanBeAccessedByVerifiedUserWithoutParamsForAjax()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);

        $this->assertArrayHasKey('legalEntity', $data['data']);
        $this->assertArrayHasKey('allowedLegalEntities', $data['data']);
        $this->assertArrayHasKey('0', $data['data']['allowedLegalEntities']);
        $this->assertArrayHasKey('name', $data['data']['allowedLegalEntities']['0']);

        $this->assertArrayHasKey('bankDetail', $data['data']);
        $this->assertArrayHasKey('allowedBankDetails', $data['data']);
        $this->assertArrayHasKey('0', $data['data']['allowedBankDetails']);
        $this->assertArrayHasKey('name', $data['data']['allowedBankDetails']['0']);

        $this->assertNull($data['data']['legalEntity']);
        $this->assertNull($data['data']['bankDetail']);

       //TODO сделать проверку legal entity
        $this->assertEquals(self::DEFAULT_DOCUMENT_NAME, $data['data']['allowedBankDetails']['0']['name']);
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity-bank-detail/form-init
     * (GET на formInitAction) авторизованному с ролью Verified с ложными параметрами
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testFormInitCanBeAccessedByVerifiedUserWithInvalidParamsForAjax()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        //Invalid params
        $getData = [
            'idLegalEntity' => 989898,
            'idBankDetail' => 'text'
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);

        $this->assertArrayHasKey('legalEntity', $data['data']);
        $this->assertArrayHasKey('bankDetail', $data['data']);

        $this->assertNull($data['data']['legalEntity']);
        $this->assertNull($data['data']['bankDetail']);
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity-bank-detail/form-init
     * (GET на formInitAction) авторизованному с ролью Verified с валидными параметрами
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testFormInitCanBeAccessedByVerifiedUserWithValidParamsForAjax()
    {
        /** @var LegalEntityBankDetail $legalEntityBankDetail */
        $legalEntityBankDetail = $this->testLegalEntityBankDetail;
        $legalEntity = $legalEntityBankDetail->getLegalEntityDocument()->getLegalEntity();

        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        //Valid params
        $getData = [
            'idLegalEntity' => $legalEntity->getId(),
            'idBankDetail' => $legalEntityBankDetail->getId()
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-bank-detail/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityBankDetailController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-bank-detail-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);

        $this->assertArrayHasKey('legalEntity', $data['data']);
        $this->assertArrayHasKey('bankDetail', $data['data']);

        $this->assertArrayHasKey('name', $data['data']['legalEntity']);
        $this->assertArrayHasKey('name', $data['data']['bankDetail']);

        $this->assertEquals($legalEntity->getName(), $data['data']['legalEntity']['name']);
        $this->assertEquals($legalEntityBankDetail->getName(), $data['data']['bankDetail']['name']);
    }


    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $login
     * @param string $name
     * @return LegalEntityBankDetail
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createTestLegalEntityBankDetail($login = 'test', $name = self::DEFAULT_DOCUMENT_NAME)
    {
        //clear all LegalEntityBankDetail

        $removeLegalEntityBankDetails = $this->entityManager->getRepository(LegalEntityBankDetail::class)->findAll();
        foreach ($removeLegalEntityBankDetails as $removeLegalEntityBankDetail){
            /** @var LegalEntityBankDetail $removeLegalEntityBankDetail */
            $removeLegalEntityDocument = $removeLegalEntityBankDetail->getLegalEntityDocument();
            $this->entityManager->remove($removeLegalEntityDocument);
            $this->entityManager->remove($removeLegalEntityBankDetail);
        }

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $login));

        /** @var LegalEntityDocumentType $legalEntityDocumentType */
        $legalEntityDocumentType = $this->entityManager->getRepository(LegalEntityDocumentType::class)
            ->findOneBy(array('name' => 'bank_details'));

        // Create CivilLawSubject
        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern(true);
        $civilLawSubject->setCreated(new \DateTime());
        $civilLawSubject->setNdsType($ndsType);

        $this->entityManager->persist($civilLawSubject);

        // Create LegalEntity
        $legalEntity = new LegalEntity();
        $legalEntity->setCivilLawSubject($civilLawSubject);
        $legalEntity->setName('test name for legal entity');
        $legalEntity->setLegalAddress('test address');
        $legalEntity->setPhone('79000000000');
        $legalEntity->setInn('111111111111');
        $legalEntity->setKpp('222222222');
        $legalEntity->setOgrn('333333333333333');

        $civilLawSubject->setLegalEntity($legalEntity);

        $this->entityManager->persist($legalEntity);

        // Create LegalEntityDocument
        $legalEntityDocument = new LegalEntityDocument();
        $legalEntityDocument->setLegalEntity($legalEntity);
        $legalEntityDocument->setIsActive(true);
        $legalEntityDocument->setLegalEntityDocumentType($legalEntityDocumentType);

        $this->entityManager->persist($legalEntityDocument);

        //Create LegalEntityBankDetail
        $legalEntityBankDetail = new LegalEntityBankDetail();
        $legalEntityBankDetail->setName($name);
        $legalEntityBankDetail->setCheckingAccount('12345678901234567890');
        $legalEntityBankDetail->setCorrespondentAccount('12345678901234567890');
        $legalEntityBankDetail->setBic('123456789');
        $legalEntityBankDetail->setLegalEntityDocument($legalEntityDocument);

        $legalEntityDocument->setLegalEntityBankDetail($legalEntityBankDetail);

        $this->entityManager->persist($legalEntityBankDetail);

        $this->entityManager->flush();

        return $legalEntityBankDetail;
    }
}