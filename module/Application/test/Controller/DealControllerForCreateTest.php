<?php

namespace ApplicationTest\Controller;

use Application\Controller\DealController;
use Application\Entity\CivilLawSubject;
use Application\Entity\DealAgent;
use Application\Entity\PaymentMethod;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\User;
use Application\Fixture\PaymentMethodBankTransferFixtureLoader;
use Application\Service\Payment\PaymentMethodManager;
use Application\Service\UserManager;
use ApplicationTest\Bootstrap;
use Core\Adapter\TokenAdapter;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseRoleManager;
use Core\Service\ORMDoctrineUtil;
use CoreTest\ViewVarsTrait;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Entity\Role;
use Doctrine\ORM\EntityManager;
use ModuleFileManager\Service\FileManager;
use Zend\Mvc\MvcEvent;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\BankClient\BankClientManager;

/**
 * Class DealControllerForCreateTest
 * @package ApplicationTest\Controller
 */
class DealControllerForCreateTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const MINIMUM_DELIVERY_PERIOD = 14;

    /**
     * @var string
     */
    private $user_login;
    /**
     * @var string
     */
    private $user_password;
    /**
     * @var User
     */
    private $user;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;
    /**
     * @var BankClientManager
     */
    public $bankClientManager;
    /**
     * @var string
     */
    private $deal_upload_folder;
    /**
     * @var FileManager
     */
    private $fileManager;

    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }
    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];
        $this->deal_upload_folder = './'.$config['file-management']['main_upload_folder'].'/deal-files';

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);
        $this->fileManager = $serviceManager->get(FileManager::class);

        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;
        $this->userManager = null;
        $this->baseRoleManager = null;
        $this->fileManager = null;

        gc_collect_cycles();
    }

    /**
     * Недоступность /deal/create не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/deal/create', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/create');
    }

    /**
     * Недоступность /deal/create не авторизованному пользователю для ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     */
    public function testCreateActionCanNotBeAccessedToNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/create');
    }

    /**
     * Недоступность /deal/create авторизованному пользователю без роли Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     */
    public function testCreateActionCanNotBeAccessedUnverifiedUser()
    {
        // Залогиниваем пользователя test
        $this->login('test');
        // Присваиваем пользователю test роль Unverified
        $this->assignRoleToTestUser('Unverified');

        $this->dispatch('/deal/create', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/create');
    }

    /**
     * Недоступность /deal/create авторизованному пользователю без роли Verified для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     */
    public function testCreateActionCanNotBeAccessedUnverifiedUserForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');
        // Присваиваем пользователю test роль Unverified
        $this->assignRoleToTestUser('Unverified');

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/create');
    }

    /**
     * Доступность /deal/create пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     */
    public function testCreateActionCanBeAccessedVerifiedUser()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $this->dispatch('/deal/create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('dealForm', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#deal-create-form');
    }

    /**
     * Доступность /deal/create пользователю с ролью Verified для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionCanBeAccessedVerifiedUserForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
    }

    /**
     * Тест создания сделки с правильными Post данными.
     *
     * 1. С созданием CivilLawSubject (natural-person)
     * 2. С созданием PaymentMethod (bank_transfer)
     * 3. Роль contractor.
     * 4. Проверка приведения email к нижнему регистру.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithNaturalPersonAndPaymentMethodBankTransferForContractor()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-1',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'TeSt2@teSt.neT', // проверка приведения email к нижнему регистру
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => [
                'payment_method_type'=> 'bank_transfer',
                'name'=> 'SIBBANK test-1',
                'bik'=> '420994241',
                'bank_name'=> 'SIBBANK test-1',
                'account_number'=> '123456789098764123',
                'corr_account_number'=> '123456789098764123'
            ],
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2011',
                'first_name' => 'Перивет test-1',
                'last_name' => 'Привет test-1',
                'secondary_name' => 'Привет test-1',
            ],
        ];

        $this->dispatch('/deal/create', 'POST', $postData);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        $deal_id = $deal->getId();
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertRedirectTo('/deal/show/'.$deal_id);
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getCustomer()->getEmail());
        $this->assertEquals(strtolower($postData['counteragent_email']), $deal->getCustomer()->getEmail());
    }

    /**
     * Тест создания сделки с правильными Post данными.
     *
     * 1. С созданием CivilLawSubject (natural-person)
     * 2. С созданием PaymentMethod (bank_transfer)
     * 3. Роль customer.
     * 4. Проверка приведения email к нижнему регистру.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithNaturalPersonAndPaymentMethodBankTransferForCustomer()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-2',
            'amount'               => '1007043',
            'deal_role'            => 'customer',
            'counteragent_email'   => 'TeSt2@teSt.neT', // проверка приведения email к нижнему регистру
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => [
                'payment_method_type'=> 'bank_transfer',
                'name'=> 'SIBBANK test-1',
                'bik'=> '420994241',
                'bank_name'=> 'SIBBANK test-1',
                'account_number'=> '123456789098764123',
                'corr_account_number'=> '123456789098764123'
            ],
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2011',
                'first_name' => 'Перивет test-1',
                'last_name' => 'Привет test-1',
                'secondary_name' => 'Привет test-1',
            ],
        ];

        $this->dispatch('/deal/create', 'POST', $postData);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        $deal_id = $deal->getId();
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertRedirectTo('/deal/show/'.$deal_id);
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод не создался!!!
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getContractor()->getEmail());
        $this->assertEquals(strtolower($postData['counteragent_email']), $deal->getContractor()->getEmail());
    }
    /**
     * Тест создания сделки с правильными огда стоимость сделки равна 0
     *
     * 1. С созданием CivilLawSubject (natural-person)
     * 2. С созданием PaymentMethod (bank_transfer)
     * 3. Роль customer.
     * 4. Проверка приведения email к нижнему регистру.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithAmauntEqualToZero()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-2',
            'amount'               => '0',
            'deal_role'            => 'customer',
            'counteragent_email'   => 'TeSt2@teSt.neT', // проверка приведения email к нижнему регистру
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => [
                'payment_method_type'=> 'bank_transfer',
                'name'=> 'SIBBANK test-1',
                'bik'=> '420994241',
                'bank_name'=> 'SIBBANK test-1',
                'account_number'=> '123456789098764123',
                'corr_account_number'=> '123456789098764123'
            ],
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2011',
                'first_name' => 'Перивет test-1',
                'last_name' => 'Привет test-1',
                'secondary_name' => 'Привет test-1',
            ],
        ];
        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/deal/create', 'POST', $postData,true);
        $data = json_decode($this->getResponse()->getContent(), true);
        $this->assertArrayHasKey('amount',$data['data']);
        $this->assertArrayHasKey('regexNotMatch',$data['data']['amount']);
        $this->assertSame(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']); //проверяем что нам пришел ответ с ошибкой
    }
    /**
     * Ajax
     * Тест создания сделки с правильными Post данными.
     *
     * 1. С созданием CivilLawSubject (natural-person)
     * 2. С созданием PaymentMethod (bank_transfer)
     * 3. Роль contractor.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithNaturalPersonAndPaymentMethodBankTransferToContractorForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-3',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.net',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => [
                'payment_method_type'=> 'bank_transfer',
                'name'=> 'SIBBANK',
                'bik'=> '420994241',
                'bank_name'=> 'SIBBANK',
                'account_number'=> '123456789098764123',
                'corr_account_number'=> '123456789098764123'
            ],
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2011',
                'first_name' => 'Перивет',
                'last_name' => 'Привет',
                'secondary_name' => 'Привет',
            ],
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);
        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getCustomer()->getDealAgentConfirm());
    }

    /**
     * Ajax
     * Тест создания сделки с правильными Post данными.
     *
     * 1. С созданием CivilLawSubject (natural-person)
     * 2. С созданием PaymentMethod (bank_transfer)
     * 3. Роль customer.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithNaturalPersonAndPaymentMethodBankTransferToCustomerForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-4',
            'amount'               => '1007043',
            'deal_role'            => 'customer',
            'counteragent_email'   => 'test2@test.net',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => [
                'payment_method_type'=> 'bank_transfer',
                'name'=> 'SIBBANK',
                'bik'=> '420994241',
                'bank_name'=> 'SIBBANK',
                'account_number'=> '123456789098764123',
                'corr_account_number'=> '123456789098764123'
            ],
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2011',
                'first_name' => 'Перивет',
                'last_name' => 'Привет',
                'secondary_name' => 'Привет',
            ],
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);
        $this->assertNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод не создан!!!
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getContractor()->getDealAgentConfirm());
    }

    /**
     * Тест создания сделки с правильными Post данными.
     *
     * 1. С созданием CivilLawSubject (natural-person)
     * 2. PaymentMethod (bank_transfer) добавляем по ID
     * 3. Заводим сделку как contractor.
     *
     *
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithCivilLawSubjectAndPaymentMethodIdToContractor()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-5',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.net',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => $paymentMethodBankTransfer->getPaymentMethod()->getId(),
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2011',
                'first_name' => 'Перивет',
                'last_name' => 'Привет',
                'secondary_name' => 'Привет',
            ],
        ];

        $this->dispatch('/deal/create', 'POST', $postData);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        $deal_id = $deal->getId();
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertRedirectTo('/deal/show/'.$deal_id);
        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertEquals(
            PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1,
            $payment->getPaymentMethod()->getPaymentMethodBankTransfer()->getName()
        );
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getCustomer()->getDealAgentConfirm());
    }

    /**
     * Ajax
     * Тест создания сделки с правильными Post данными.
     *
     * 1. С созданием CivilLawSubject (natural-person)
     * 2. PaymentMethod (bank_transfer) добавляем по ID
     * 3. Заводим сделку как contractor.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithCivilLawSubjectAndPaymentMethodIdToContractorForAjax()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-6',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.net',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => $paymentMethodBankTransfer->getPaymentMethod()->getId(),
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2011',
                'first_name' => 'Перивет',
                'last_name' => 'Привет',
                'secondary_name' => 'Привет',
            ],
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);
        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertEquals(
            PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1,
            $payment->getPaymentMethod()->getPaymentMethodBankTransfer()->getName()
        );
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getCustomer()->getDealAgentConfirm());
    }

    /**
     * Тест создания сделки с правильными Post данными.
     *
     * 1. С созданием CivilLawSubject (natural-person)
     * 2. PaymentMethod (bank_transfer) добавляем по ID
     * 3. Заводим сделку как customer.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithCivilLawSubjectAndPaymentMethodIdToCustomer()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-7',
            'amount'               => '1007043',
            'deal_role'            => 'customer',
            'counteragent_email'   => 'test2@test.net',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => $paymentMethodBankTransfer->getPaymentMethod()->getId(),
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2011',
                'first_name' => 'Перивет',
                'last_name' => 'Привет',
                'secondary_name' => 'Привет',
            ],
        ];

        $this->dispatch('/deal/create', 'POST', $postData);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        $deal_id = $deal->getId();
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertRedirectTo('/deal/show/'.$deal_id);
        $this->assertNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод не создан !!!
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getContractor()->getDealAgentConfirm());
    }

    /**
     * Ajax
     * Тест создания сделки с правильными Post данными.
     *
     * 1. С созданием CivilLawSubject (natural-person)
     * 2. PaymentMethod (bank_transfer) добавляем по ID
     * 3. Заводим сделку как customer.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithCivilLawSubjectAndPaymentMethodIdToCustomerForAjax()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-8',
            'amount'               => '1007043',
            'deal_role'            => 'customer',
            'counteragent_email'   => 'test2@test.net',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => $paymentMethodBankTransfer->getPaymentMethod()->getId(),
            'deal_civil_law_subject' => [
                'civil_law_subject_type' => 'natural-person',
                'birth_date' => '01.11.2011',
                'first_name' => 'Перивет',
                'last_name' => 'Привет',
                'secondary_name' => 'Привет',
            ],
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);
        $this->assertNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод не создан !!!
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getContractor()->getDealAgentConfirm());
    }

    /**
     * Тест создания сделки с правильными Post данными.
     *
     * 1. CivilLawSubject (natural-person) добавляем по ID
     * 2. PaymentMethod (bank_transfer) добавляем по ID
     * 3. Заводим сделку как contractor.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithCivilLawSubjectIdAndPaymentMethodIdToContractor()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-9',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.net',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => $paymentMethodBankTransfer->getPaymentMethod()->getId(),
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->first()->getId(),
        ];

        $this->dispatch('/deal/create', 'POST', $postData);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        $deal_id = $deal->getId();
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertRedirectTo('/deal/show/'.$deal_id);
        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertEquals(
            PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1,
            $payment->getPaymentMethod()->getPaymentMethodBankTransfer()->getName()
        );
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getContractor()->getEmail());
    }

    /**
     * Ajax
     * Тест создания сделки с правильными Post данными.
     *
     * 1. CivilLawSubject (natural-person) добавляем по ID
     * 2. PaymentMethod (bank_transfer) добавляем по ID
     * 3. Заводим сделку как contractor.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithCivilLawSubjectIdAndPaymentMethodIdToContractorForAjax()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-10',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'TeSt2@teSt.neT', // проверка приведения email к нижнему регистру
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => $paymentMethodBankTransfer->getPaymentMethod()->getId(),
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->first()->getId(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);
        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertEquals(
            PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1,
            $payment->getPaymentMethod()->getPaymentMethodBankTransfer()->getName()
        );
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals($this->user->getLogin(), $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getCustomer()->getDealAgentConfirm());
    }

    /**
     * Тест создания сделки с правильными Post данными.
     *
     * 1. поля CivilLawSubject путы
     * 2. поля PaymentMethod путы
     * 3. Заводим сделку как contractor.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithoutCivilLawSubjectIdAndPaymentMethodIdToContractor()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-11',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.net',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => '',
            'deal_civil_law_subject' => '',
        ];

        $this->dispatch('/deal/create', 'POST', $postData);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        $deal_id = $deal->getId();
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertRedirectTo('/deal/show/'.$deal_id);

        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        // Сделка автоматически не конфёрмится т.к не все выбранно для этого
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getContractor()->getEmail());
    }

    /**
     * Ajax
     * Тест создания сделки с правильными Post данными.
     *
     * 1. поля CivilLawSubject путы
     * 2. поля PaymentMethod путы
     * 3. Заводим сделку как contractor.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateDealWithoutCivilLawSubjectIdAndPaymentMethodIdToContractorForAjax()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-12',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'TeSt2@teSt.neT', // проверка приведения email к нижнему регистру
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => '',
            'deal_civil_law_subject' => '',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals($this->user->getLogin(), $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getCustomer()->getDealAgentConfirm());
    }

    /**
     * Тест создания сделки с невалидными данными.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionWithInvalidPostData()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-14',
            'amount'               => '', //поле должно попасть в валидацию
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.neT',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => '',//поле должно попасть в валидацию
            'payment_method'       =>  [
                'payment_method_type'=> 'bank_transfer',
                'name'=> 'SIBBANK',
                'bik'=> '420994241',
                'bank_name'=> 'SIBBANK',
                'account_number'=> '123456789098764123',
                'corr_account_number'=> '123456789098764123'
            ],
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->first()->getId(),
        ];

        $this->dispatch('/deal/create', 'POST', $postData);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        //проверяем что сделка не создана
        $this->assertNull($deal);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
    }

    /**
     * Тест создания сделки с невалидными данными.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionWithInvalidPostDataForAjax()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-14',
            'amount'               => '', //поле должно попасть в валидацию
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.neT',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => '',//поле должно попасть в валидацию
            'payment_method'       =>  [
                'payment_method_type'=> 'bank_transfer',
                'name'=> 'SIBBANK',
                'bik'=> '420994241',
                'bank_name'=> 'SIBBANK',
                'account_number'=> '123456789098764123',
                'corr_account_number'=> '123456789098764123'
            ],
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->first()->getId(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        //проверяем что сделка не создана
        $this->assertNull($deal);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertArrayHasKey('data', $data);

        $this->assertEquals('ERROR',$data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('amount', $data['data']);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /**
     * Проверка автогенерации названия платежного шаблона при созданни сделки если незвание переданно пустым
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionWithAutoGenerateBankTransferName()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-16',
            'amount'               => '199090',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.neT',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       =>  [
                'payment_method_type'=> 'bank_transfer',
                'name'=> '',
                'bik'=> '420994241',
                'bank_name'=> 'SIBBANK',
                'account_number'=> '123456789098764123',
                'corr_account_number'=> '123456789098764123'
            ],
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->first()->getId(),
        ];

        $this->dispatch('/deal/create', 'POST', $postData);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        $deal_id = $deal->getId();
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertRedirectTo('/deal/show/'.$deal_id);
        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertEquals(
            PaymentMethodManager::buildBankTransferNameToDeal(
                $payment->getPaymentMethod()->getPaymentMethodBankTransfer()->getBankName(),
                $deal->getNumber()
            ),
            $payment->getPaymentMethod()->getPaymentMethodBankTransfer()->getName()
        );
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals($this->user->getLogin(), $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getCustomer()->getDealAgentConfirm());
    }

    /**
     * Ajax
     * Проверка автогенерации названия платежного шаблона при созданни сделки если незвание переданно пустым
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testCreateActionWithAutoGenerateBankTransferNameForAjax()
    {
        // Залогиниваем пользователя test
        $this->login($this->user->getLogin());

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-16',
            'amount'               => '199090',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.neT',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       =>  [
                'payment_method_type'=> 'bank_transfer',
                'name' => '',
                'bik'=> '420994241',
                'bank_name'=> 'SIBBANK',
                'account_number'=> '123456789098764123',
                'corr_account_number'=> '123456789098764123'
            ],
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->first()->getId(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        //проверяем что сделка создана
        $this->assertNotNull($deal);

        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);
        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertEquals(
            PaymentMethodManager::buildBankTransferNameToDeal(
                $payment->getPaymentMethod()->getPaymentMethodBankTransfer()->getBankName(),
                $deal->getNumber()
            ),
            $payment->getPaymentMethod()->getPaymentMethodBankTransfer()->getName()
        );
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals($this->user->getLogin(), $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getCustomer()->getDealAgentConfirm());
    }

    /**
     * Тест создания сделки с правильными Post данными и валидными DateFile.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     */
    public function testCreateDealWithWithFileDate()
    {
        // Переорпеделяем is_uploaded_file
        $this->disablePhpUploadCapabilities();
        $file_name = 'image_file_for_test.png';
        $file_name_2 = 'image_file_2_for_test.png';

        $this->login();

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-17',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.net',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => $paymentMethodBankTransfer->getPaymentMethod()->getId(),
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->first()->getId(),
        ];

        // Формируем POST даные
        $this->getRequest()
            ->setMethod('POST')
            ->setPost(new \Zend\Stdlib\Parameters($postData));
        $files = new \Zend\Stdlib\Parameters([
            'files' => [
                [
                    'name' => $file_name,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name,
                    'size' => filesize('./module/Application/test/files/' .$file_name),
                    'error' => 0
                ],
                [
                    'name' => $file_name_2,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name_2,
                    'size' => filesize('./module/Application/test/files/' .$file_name_2),
                    'error' => 0
                ],
            ]
        ]);
        $this->getRequest()->setFiles($files);

        $this->dispatch('/deal/create', 'POST');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        $deal_id = $deal->getId();
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertRedirectTo('/deal/show/'.$deal_id);
        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertEquals(
            PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1,
            $payment->getPaymentMethod()->getPaymentMethodBankTransfer()->getName()
        );
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getContractor()->getEmail());

        // Проверяем и удаляем полученный файл
        $dealFiles = $deal->getDealFiles();

        $file = $this->fileManager->getFileByOriginName($file_name);
        $this->assertEquals($file_name, $file->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file->getName());
        $this->assertContains($file, $dealFiles);

        $file2 = $this->fileManager->getFileByOriginName($file_name_2);
        $this->assertEquals($file_name_2, $file2->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file2->getName());
        $this->assertContains($file2, $dealFiles);

        unlink($this->deal_upload_folder.'/'.$file->getName());
        unlink($this->deal_upload_folder.'/'.$file2->getName());
    }

    /**
     * Ajax
     * Тест создания сделки с правильными Post данными и валидными DateFile.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     */
    public function testCreateDealWithWithFileDateForAjax()
    {
        // Переорпеделяем is_uploaded_file
        $this->disablePhpUploadCapabilities();
        $file_name = 'image_file_for_test.png';
        $file_name_2 = 'image_file_2_for_test.png';

        $this->login();

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-18',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => 'test2@test.net',
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => $paymentMethodBankTransfer->getPaymentMethod()->getId(),
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->first()->getId(),
        ];

        // Формируем POST даные
        $this->getRequest()
            ->setMethod('POST')
            ->setPost(new \Zend\Stdlib\Parameters($postData));
        $files = new \Zend\Stdlib\Parameters([
            'files' => [
                [
                    'name' => $file_name,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name,
                    'size' => filesize('./module/Application/test/files/' .$file_name),
                    'error' => 0
                ],
                [
                    'name' => $file_name_2,
                    'type' => 'text/plain',
                    'tmp_name' => './module/Application/test/files/' .$file_name_2,
                    'size' => filesize('./module/Application/test/files/' .$file_name_2),
                    'error' => 0
                ],
            ]
        ]);
        $this->getRequest()->setFiles($files);

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($deal->getId(), $data['data']['deal']['id']);

        $this->assertNotNull($payment->getPaymentMethod()); // для контрактора проверяем платежный метод
        $this->assertNotNull($payment->getPaymentMethod()->getPaymentMethodBankTransfer());
        $this->assertEquals(
            PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1,
            $payment->getPaymentMethod()->getPaymentMethodBankTransfer()->getName()
        );
        $this->assertEquals($postData['amount'], $payment->getDealValue());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm()); // Сделка автоматически конфёрмится автором
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        $this->assertNotEquals($postData['counteragent_email'], $deal->getContractor()->getEmail());

        // Проверяем и удаляем полученный файл
        $dealFiles = $deal->getDealFiles();

        $file = $this->fileManager->getFileByOriginName($file_name);
        $this->assertEquals($file_name, $file->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file->getName());
        $this->assertContains($file, $dealFiles);

        $file2 = $this->fileManager->getFileByOriginName($file_name_2);
        $this->assertEquals($file_name_2, $file2->getOriginName());
        $this->assertFileExists($this->deal_upload_folder.'/'.$file2->getName());
        $this->assertContains($file2, $dealFiles);

        unlink($this->deal_upload_folder.'/'.$file->getName());
        unlink($this->deal_upload_folder.'/'.$file2->getName());
    }

    /**
     * Ajax
     * Тест создания сделки с правильными Post данными и авторегистрацией
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     */
    public function testCreateDealWithAutoRegistrationUserForAjax()
    {
        $new_user_email = 'test2@test.net';
        $this->login();

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        $csrfElement = new Element\Csrf('csrf');
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => PaymentMethodBankTransferFixtureLoader::TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME_1));
        // Post data
        $postData = [
            'name'                 => 'Сделка для unit тестов (create) test-19',
            'amount'               => '1007043',
            'deal_role'            => 'contractor',
            'counteragent_email'   => $new_user_email,
            'deal_type'            => $dealType->getId(),
            'fee_payer_option'     => $feePayerOption->getId(),
            'addon_terms'          => 'Дополнительные условия',
            'delivery_period'      => self::MINIMUM_DELIVERY_PERIOD,
            'csrf'                 => $csrfElement->getValue(),
            'payment_method'       => $paymentMethodBankTransfer->getPaymentMethod()->getId(),
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->first()->getId(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/create', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $postData['name']));

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal registered', $data['message']);

        $this->assertNotNull($deal);
        $this->assertEquals('test', $deal->getOwner()->getUser()->getLogin());
        /** @var DealAgent $customer */
        $customer = $deal->getCustomer();
        $this->assertNotNull($customer);
        $this->assertSame($postData['counteragent_email'], $customer->getEmail());

        $newUsers = $this->entityManager->getRepository(User::class)
            ->findAllUnverifiedUserByEmail($postData['counteragent_email']);
        $this->assertCount(1, $newUsers);
        /** @var User $newUser */
        $newUser = $newUsers[0];
        $this->assertNotNull($newUser);
        //user к сделке еше не прикреплен
        $this->assertNull($customer->getUser());
        $this->assertSame($newUser->getEmail()->getEmail(), $customer->getEmail());
    }


    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        $user = $user ?? $this->user;
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        $login = $login ?? $this->user_login;
        $password = $password ?? $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}