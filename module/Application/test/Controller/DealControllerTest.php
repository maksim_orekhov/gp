<?php

namespace ApplicationTest\Controller;

use Application\Entity\Email;
use Application\Entity\Phone;
use Application\Form\DealSearchForm;
use Application\Controller\DealController;
use Application\Entity\RepaidOverpay;
use Application\Form\RepaidOverpayForm;
use Application\Service\UserManager;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Exception\LogicException;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseRoleManager;
use Core\Service\ORMDoctrineUtil;
use DateTime;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Form\DisputeForm;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\Authentication\AuthenticationService;
use Zend\Crypt\Password\Bcrypt;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\Deal;
use Application\Entity\User;
use Application\Entity\Payment;
use Application\Entity\SystemPaymentDetails;
use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethod;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealAgentManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\BankClient\BankClientManager;
use Application\Service\Parser\BankClientParser;
use Application\Controller\BankClientController;
use Application\Service\Deal\DealPolitics;
use CoreTest\ViewVarsTrait;

/**
 * Class DealControllerTest
 * @package ApplicationTest\Controller
 */
class DealControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const MINIMUM_DELIVERY_PERIOD = 7;
    const TEXT_DEAL_NAME = 'Сделка Фикстура';
    const TEXT_DISPUTE_DEAL_NAME = 'Сделка Debit Matched'; // Заменить использование на DEAL_DEBIT_MATCHED_NAME
    const DEAL_DEBIT_MATCHED_NAME = 'Сделка Debit Matched';
    const DEAL_WITH_OVERPAY_NAME = 'Сделка Переплата';
    const CONFIRMED_DEAL_NAME = 'Сделка Оплаченная';
    const DEAL_WITH_INVITATION_NAME = 'Сделка с приглашением';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var BankClientManager
     */
    public $bankClientManager;

    /**
     * @var BankClientParser
     */
    private $bankClientParser;

    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * @var string
     */
    public $main_upload_folder;

    /**
     * @var BaseRoleManager
     */
    public $baseRoleManager;
    /**
     * @var BasePhoneManager
     */
    private $basePhoneManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->authService = $serviceManager->get(AuthenticationService::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->dealAgentManager = $serviceManager->get(DealAgentManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->paymentManager = $serviceManager->get(PaymentManager::class);
        $this->bankClientManager = $serviceManager->get(BankClientManager::class);
        $this->bankClientParser = $serviceManager->get(BankClientParser::class);
        $this->dealPolitics = $serviceManager->get(DealPolitics::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);
        $this->basePhoneManager = $serviceManager->get(BasePhoneManager::class);

        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;
        $this->authService = null;
        $this->userManager = null;
        $this->dealManager = null;
        $this->dealAgentManager = null;
        $this->civilLawSubjectManager = null;
        $this->paymentManager = null;
        $this->bankClientManager = null;
        $this->bankClientParser = null;
        $this->dealPolitics = null;
        $this->baseRoleManager = null;
        $this->basePhoneManager = null;

        gc_collect_cycles();
    }

    /////////////// Index

    /**
     * Недоступность /deal не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/deal', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal');
    }

    /**
     * Недоступность /deal авторизованному пользователю без роли Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIndexActionCanNotBeAccessedUnverifiedUser()
    {
        $this->assignRoleToTestUser('Unverified');
        $this->login();

        $this->dispatch('/deal', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /deal пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIndexActionCanBeAccessedVerifiedUser()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $this->dispatch('/deal', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');
        // Что отдается в шаблон
        $this->assertArrayHasKey('dealFilterForm', $view_vars);
        $this->assertArrayHasKey('deals', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertFalse($view_vars['is_operator']);
        // Возвращается из шаблона
        $this->assertQuery('#deal-filter-form');
    }
    /**
     * Доступность /deal пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIndexActionForDealStatusesAndSerchFormInstanceOf()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/deal', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        /** @var DealSearchForm $deal_search_form */
        $deal_search_form = $this->getViewVars();
        /** @var DealSearchForm $view_vars_form */
        $view_vars_form = $view_vars['dealFilterForm'];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');
        // Что отдается в шаблон
        $this->assertArrayHasKey('dealFilterForm', $view_vars);
        $this->assertArrayHasKey('deals', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
       // $dealOptions = $deal_search_form->getOptionsForDealTypeSelect();
        $output= $this->getProvidedData();
         $this->assertTrue($view_vars_form instanceof DealSearchForm);
        // Возвращается из шаблона
        $this->assertQuery('#deal-filter-form');
    }
    /**
     * Доступность /deal пользователю с ролью Verified для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIndexActionCanBeAccessedVerifiedUserForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/deal', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deals', $data['data']);
        $this->assertGreaterThan(0, count($data['data']['deals']));
    }

    /**
     * Отправка GET /deal с параметром фильтрации id
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIndexActionWithFilterById()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Фикстура';
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'deal_id' => $deal->getId()
            ],
            'csrf' => $csrf
        ];

        $this->dispatch('/deal', 'GET', $getData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');
        // Что отдается в шаблон
        $this->assertArrayHasKey('deals', $view_vars);
        $this->assertGreaterThan(0, count($view_vars['deals']));
        $this->assertEquals($deal_name, $view_vars['deals'][0]['name']);
        $this->assertTrue($view_vars['dealFilterForm'] instanceof DealSearchForm);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertTrue($view_vars['is_operator']);
        // Возвращается из шаблона
        $this->assertQuery('#deal-filter-form');
    }

    /**
     * Отправка POST /deal с параметром фильтрации id для Ajax
     *
     * @group deal
     */
    public function testIndexActionWithFilterByIdForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $deal_name = 'Сделка Фикстура';
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'deal_id' => $deal->getId()
            ],
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($getData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deals', $data['data']);
        $this->assertGreaterThan(0, count($data['data']['deals']));
        $this->assertEquals($deal_name, $data['data']['deals'][0]['name']);
        $this->assertArrayHasKey('is_operator', $data['data']);
        $this->assertTrue($data['data']['is_operator']);
    }

    /**
     * Отправка GET /deal с параметром фильтрации name
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIndexActionWithFilterByName()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'deal_name' => 'Фикстура'
            ],
            'csrf' => $csrf
        ];

        $this->dispatch('/deal', 'GET', $getData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');
        // Что отдается в шаблон
        $this->assertArrayHasKey('deals', $view_vars);
        $this->assertGreaterThan(0, count($view_vars['deals']));
        $this->assertEquals('Сделка Фикстура', $view_vars['deals'][0]['name']);
        $this->assertTrue($view_vars['dealFilterForm'] instanceof DealSearchForm);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertTrue($view_vars['is_operator']);
        // Возвращается из шаблона
        $this->assertQuery('#deal-filter-form');
    }

    /**
     * Отправка POST /deal с параметрами сортировки сделок для Ajax
     *
     * @group deal
     */
    public function testIndexActionWithFilterByNameForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'deal_name' => 'Фикстура'
            ],
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($getData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deals', $data['data']);
        $this->assertGreaterThan(0, count($data['data']['deals']));
        $this->assertEquals('Сделка Фикстура', $data['data']['deals'][0]['name']);
        $this->assertArrayHasKey('is_operator', $data['data']);
        $this->assertTrue($data['data']['is_operator']);
    }

    /**
     * Отправка GET /deal с параметром фильтрации amount
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testIndexActionWithFilterByAmount()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Фикстура';
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'deal_amount' => (string)$payment->getExpectedValue() // Правильно обрабатывает только string!
            ],
            'csrf' => $csrf
        ];

        $this->dispatch('/deal', 'GET', $getData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');
        // Что отдается в шаблон
        $this->assertArrayHasKey('deals', $view_vars);
        $this->assertGreaterThan(0, count($view_vars['deals']));
        $this->assertTrue($view_vars['dealFilterForm'] instanceof DealSearchForm);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertTrue($view_vars['is_operator']);
        // Возвращается из шаблона
        $this->assertQuery('#deal-filter-form');
    }

    /**
     * Отправка POST /deal с параметром фильтрации amount для Ajax
     *
     * @group deal
     */
    public function testIndexActionWithFilterByAmountForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $deal_name = 'Сделка Фикстура';
        /* @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $getData = [
            'filter' => [
                'deal_amount' => (string)$payment->getExpectedValue() // Правильно обрабатывает только string!
            ],
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($getData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deals', $data['data']);
        $this->assertGreaterThan(0, count($data['data']['deals']));
        $this->assertArrayHasKey('is_operator', $data['data']);
        $this->assertTrue($data['data']['is_operator']);
    }

    /**
     * Отправка GET /deal без валидного csrf для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     *
     */
    public function testIndexActionWithNoValidFilterParamsForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $getData = [
            'filter' => [
                'deal_name' => 'Фикстура'
            ],
            'csrf' => 'fgfgfgfgfgf4564454545'
        ];

        $this->getRequest()->setContent(json_encode($getData));

        $isXmlHttpRequest = true;
        $this->dispatch('/deal', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['message']);
        // Возвращается из шаблона
        $this->assertQuery('#deal-filter-form');
    }

    /////////////// Edit

    /**
     * Доступность /deal/edit/id одному из участников сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionCanBeAccessedByParticipant()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $deal_name = 'Сделка для unit тестов (edit)';
        $deal_role = 'customer';
        $amount = 1507043;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Get Payment
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        $this->assertEquals($amount, $payment->getDealValue());

        $this->dispatch('/deal/edit/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('dealForm', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#deal-edit-form');
    }

    /**
     * Доступность /deal/edit/id одному из участников сделки для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionCanBeAccessedByOwnerForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $deal_name = 'Сделка для unit тестов (edit)';
        $deal_role = 'customer';
        $amount = 1507043;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Get Payment
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        $this->assertEquals($amount, $payment->getDealValue());

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/edit/'.$deal->getId(), 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Невозможность редактирования подтвержденной сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionCanNotBeAccessedForConfirmedDeal()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        $deal_name = 'Сделка для unit тестов (edit)';
        $deal_role = 'customer';
        $amount = 1507043;
        $confirmed = true;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount, $confirmed);
        // Get Payment
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());

        $this->dispatch('/deal/edit/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('message', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('.Content .MessagePage');
    }

    /**
     * Недоступность /deal/edit/id не участнику сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionCanNotBeAccessedByNotParticipant()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('operator');
        // Назначаем роль Verified пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');
        // Проверяем
        $this->assertEquals('operator', $this->baseAuthManager->getIdentity());

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        $this->dispatch('/deal/edit/'.$deal->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /deal/edit/1 не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/deal/edit/1', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/edit/1');
    }

    /**
     * Недоступность /deal/edit/1 авторизованному пользователю без роли Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionCanNotBeAccessedUnverifiedUser()
    {
        // Назначаем пользователю test роль Unverified
        $this->assignRoleToTestUser('Unverified');
        // Залогинваем его
        $this->login();

        $this->dispatch('/deal/edit/1', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Тест редактирования существующей сделки одним из участников
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionWithValidPostData()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (edit)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Товар'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CONTRACTOR'));

        $customer_id = $deal->getCustomer()->getId();
        $contractor_id = $deal->getContractor()->getId();

        // РЕДАКТИРУЕМ СОЗДАННУЮ СДЕЛКУ
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $new_deal_name = 'Сделка для unit тестов (edited)';
        $new_amount = 400000;
        // Post data
        $postData = [
            'name'                      => $new_deal_name,  // Меняем название
            'amount'                    => $new_amount,     // Меняем сумму
            'deal_role'                 => 'customer',
            'deal_civil_law_subject'    => $this->user->getCivilLawSubjects()->first()->getId(), // именно first(), т.к last() может быть не pattern
            'payment_method'            => '',
            'counteragent_email'        => 'test2@simple-technology.ru',
            'deal_type'                 => $dealType->getId(), // Меняем (выбрали ранее 'Товар')
            'fee_payer_option'          => $feePayerOption->getId(), // Меняем (выбрали ранее 'CONTRACTOR')
            'addon_terms'               => 'Дополнительные условия исправленные', // Меняем текст
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => '0',
            'csrf'                      => $csrf
        ];

        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $new_deal_name));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        $this->assertEquals($new_amount, $payment->getDealValue());
        $this->assertSame($feePayerOption, $deal->getFeePayerOption());
        $this->assertSame($dealType, $deal->getDealType());
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        // Проверяем, что после смены роли, DealAgents не изменились
        $this->assertEquals($customer_id, $deal->getCustomer()->getId());
        $this->assertEquals($contractor_id, $deal->getContractor()->getId());
    }

    /**
     * Тест изменения роли в сделке владельцем сделки
     *
     * Important! Изменять роль в сделке теперь нельзя. Меняем результат теста на fail.
     *
     * @group deal
     */
    public function testEditActionWithDealRolesChangeByOwner()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (Deal Roles Change)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $customer_id = $deal->getCustomer()->getId();
        $contractor_id = $deal->getContractor()->getId();

        // РЕДАКТИРУЕМ СОЗДАННУЮ СДЕЛКУ
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $postData = [
            'name'                      => $deal_name,
            'amount'                    => $amount,
            'deal_role'                 => 'contractor',    // Меняем роль
            'deal_civil_law_subject'    => $this->user->getCivilLawSubjects()->first()->getId(), // именно first(), т.к last() может быть не pattern
            'payment_method'            => '',
            'counteragent_email'        => 'test2@simple-technology.ru',
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => '0',
            'csrf'                      => $csrf
        ];

        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        // Проверяем, что роль не изменилась
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        $this->assertEquals($amount, $payment->getDealValue());
        #$this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        #$this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        // Проверяем, что после смены роли, созданы новые DealAgent -
        #$this->assertNotEquals($customer_id, $deal->getContractor()->getId());
        #$this->assertNotEquals($contractor_id, $deal->getCustomer()->getId());
        #$this->assertTrue($customer_id < $deal->getContractor()->getId());
        #$this->assertTrue($contractor_id < $deal->getCustomer()->getId());
    }

    /**
     * Тест изменения роли в сделке НЕ владельцем сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionWithDealRolesChangeByNotOwner()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (Deal Roles Change)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());
        // Залогиниваем юзера test2
        $this->login('test2', $this->user_password);
        // Проверяем
        $this->assertEquals('test2', $this->baseAuthManager->getIdentity());

        /** @var User $user */
        $user = $this->userManager->getUserByLogin('test2');

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $customer_id = $deal->getCustomer()->getId();
        $contractor_id = $deal->getContractor()->getId();

        // РЕДАКТИРУЕМ СОЗДАННУЮ СДЕЛКУ
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $new_deal_name = $deal_name . " отредактированное";
        $new_amount = $amount+1000;

        // Post data
        $postData = [
            'name'                      => $new_deal_name,  // Меняем
            'amount'                    => $new_amount,     // Меняем
            'deal_role'                 => 'customer',    // Меняем роль (зашли под логином Котрактора)
            'deal_civil_law_subject'    => $user->getCivilLawSubjects()->first()->getId(), // именно first(), т.к last() может быть не pattern
            'payment_method'            => '',
            #'counteragent_email'        => 'test@simple-technology.ru', // (зашли под логином Котрактора)
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => '0',
            'csrf'                      => $csrf
        ];

        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/edit');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $new_deal_name));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        // Проверяем, что роль в сделке пользователя test2 не изменилась
        $this->assertEquals($user->getId(), $deal->getContractor()->getUser()->getId());
        // Проверяем, что изменения внесены
        $this->assertEquals($new_deal_name, $deal->getName());
        $this->assertEquals($new_amount, $payment->getDealValue());
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        // ...но изменений с DealAgents не произошло
        $this->assertEquals($customer_id, $deal->getCustomer()->getId());
        $this->assertEquals($contractor_id, $deal->getContractor()->getId());
    }


    /**
     * Тест изменения роли в сделке НЕ владельцем сделки для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionWithDealRolesChangeByNotOwnerForAjax()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (Deal Roles Change)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());
        // Залогиниваем юзера test2
        $this->login('test2', $this->user_password);
        // Проверяем
        $this->assertEquals('test2', $this->baseAuthManager->getIdentity());

        /** @var User $user */
        $user = $this->userManager->getUserByLogin('test2');

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));
        // РЕДАКТИРУЕМ СОЗДАННУЮ СДЕЛКУ
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $postData = [
            'name'                      => $deal_name,  // не меняем
            'amount'                    => $amount,     // не меняем
            'deal_role'                 => 'customer',    // Меняем роль (зашли под логином Котрактора)
            'deal_civil_law_subject'    => $user->getCivilLawSubjects()->first()->getId(), // именно first(), т.к last() может быть не pattern
            'payment_method'            => '',
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => '0',
            'csrf'                      => $csrf
        ];


        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(DealController::ERROR_INVALID_FORM_DATA, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals($data['data'], DealController::FORBIDDEN_EDIT_ROLE_AND_EMAIL_NOT_OWNER);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        // Проверяем, что роль в сделке пользователя test2 не изменилась
        $this->assertEquals($user->getId(), $deal->getContractor()->getUser()->getId());
    }

    /**
     * Тест изменения контрагента в сделке владельцем сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     *
     * @TODO Не проходит!
     */
    public function testEditActionWithCounteragentChangeByOwner()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (Counteragent change by owner)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $customer_id = $deal->getCustomer()->getId();
        $contractor_id = $deal->getContractor()->getId();

        // РЕДАКТИРУЕМ СОЗДАННУЮ СДЕЛКУ
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $postData = [
            'name'                      => $deal_name,
            'amount'                    => $amount,
            'deal_role'                 => $deal_role,
            'deal_civil_law_subject'    => $this->user->getCivilLawSubjects()->first()->getId(), // именно first(), т.к last() может быть не pattern
            'payment_method'            => '',
            'counteragent_email'        => 'test3@guarantpay.com', // Меняем котрагента
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => '0',
            'csrf'                      => $csrf
        ];

        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/edit');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        $this->assertNull($deal->getContractor()->getUser());
        $this->assertEquals($amount, $payment->getDealValue());
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        // Проверяем, что после смены котрагента, DealAgent владельца не изменился
        $this->assertEquals($customer_id, $deal->getCustomer()->getId());
        // Проверяем, что после смены котрагента, создан новый DealAgent (в данном случае для Кострактора)
        $this->assertNotEquals($contractor_id, $deal->getContractor()->getId());
        $this->assertTrue($contractor_id < $deal->getContractor()->getId());
    }

    /**
     * Тест изменения контрагента в сделке НЕ владельцем сделки.
     * DealAgents не должны измениться.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionWithCounteragentChangeByNotOwner()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (Deal Roles Change)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());
        // Залогиниваем юзера test2
        $this->login('test2', $this->user_password);
        // Проверяем
        $this->assertEquals('test2', $this->baseAuthManager->getIdentity());

        /** @var User $user */
        $user = $this->userManager->getUserByLogin('test2');

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $customer_id = $deal->getCustomer()->getId();
        $contractor_id = $deal->getContractor()->getId();

        // РЕДАКТИРУЕМ СОЗДАННУЮ СДЕЛКУ
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $new_deal_name = $deal_name . " отредактированное";
        $new_amount = $amount+1000;

        // Post data
        $postData = [
            'name'                      => $new_deal_name,  // Меняем
            'amount'                    => $new_amount,     // Меняем
            #'deal_role'                 => 'customer',    // Меняем роль (зашли под логином Котрактора)
            'deal_civil_law_subject'    => $user->getCivilLawSubjects()->first()->getId(), // именно first(), т.к last() может быть не pattern
            'payment_method'            => '',
            'counteragent_email'        => 'test@simple-technology.ru', // (зашли под логином Котрактора)
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => '0',
            'csrf'                      => $csrf
        ];

        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/edit');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $new_deal_name));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        // Проверяем, что роль в сделке пользователя test2 не изменилась
        $this->assertEquals($user->getId(), $deal->getContractor()->getUser()->getId());
        // Проверяем, что изменения внесены
        $this->assertEquals($new_deal_name, $deal->getName());
        $this->assertEquals($new_amount, $payment->getDealValue());
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());
        $this->assertFalse($deal->getCustomer()->getDealAgentConfirm());
        // ...но изменений с DealAgents не произошло
        $this->assertEquals($customer_id, $deal->getCustomer()->getId());
        $this->assertEquals($contractor_id, $deal->getContractor()->getId());
    }

    /**
     * Тест изменения контрагента в сделке НЕ владельцем сделки для Ajax.
     * DealAgents не должны измениться.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionWithCounteragentChangeByNotOwnerForAjax()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (Deal Roles Change)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());
        // Залогиниваем юзера test2
        $this->login('test2', $this->user_password);
        // Проверяем
        $this->assertEquals('test2', $this->baseAuthManager->getIdentity());

        /** @var User $user */
        $user = $this->userManager->getUserByLogin('test2');

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $customer_id = $deal->getCustomer()->getId();
        $contractor_id = $deal->getContractor()->getId();

        // РЕДАКТИРУЕМ СОЗДАННУЮ СДЕЛКУ
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $postData = [
            'name'                      => $deal_name,  // не меняем
            'amount'                    => $amount,     // не меняем
            'deal_role'                 => 'contractor',    //не меняем роль (зашли под логином Котрактора)
            'deal_civil_law_subject'    => $user->getCivilLawSubjects()->first()->getId(), // именно first(), т.к last() может быть не pattern
            'payment_method'            => '',
            'counteragent_email'        => 'test565656@guarantpay.com', //Меняем email (зашли под логином Котрактора)
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => '0',
            'csrf'                      => $csrf
        ];


        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(DealController::ERROR_INVALID_FORM_DATA, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertEquals($data['data'], DealController::FORBIDDEN_EDIT_ROLE_AND_EMAIL_NOT_OWNER);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        // ...но изменений с DealAgents не произошло
        $this->assertEquals($customer_id, $deal->getCustomer()->getId());
        $this->assertEquals($contractor_id, $deal->getContractor()->getId());
    }

    /**
     * Тест редактирования существующей сделки участником сделки для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionWithValidPostDataForAjax()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (edit)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        // РЕДАКТИРУЕМ СОЗДАННУЮ СДЕЛКУ
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $new_deal_name = 'Сделка для unit тестов (edited Ajax)';
        $new_amount = 9000000;
        // Post data
        $postData = [
            'name'                      => $new_deal_name,  // Меняем название
            'amount'                    => $new_amount,     // Меняем сумму
            'deal_role'                 => 'customer',
            'deal_civil_law_subject'    => $this->user->getCivilLawSubjects()->first()->getId(), // именно first(), т.к last() может быть не pattern
            'payment_method'            => '',
            'counteragent_email'        => 'test2@simple-technology.ru',
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => '0',
            'csrf'                      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal successfully edited', $data['message']);
        $this->assertArrayHasKey('data', $data);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $new_deal_name));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        $this->assertEquals($new_amount, $payment->getDealValue());
    }

    /**
     * Тест редактирования существующей сделки участником сделки для Ajax.
     * Отпрвляем не валидные POST дата.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testEditActionWithNotValidPostDataForAjax()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (edit)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        // РЕДАКТИРУЕМ СОЗДАННУЮ СДЕЛКУ
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $new_deal_name = 'Сделка для unit тестов (edited Ajax)';
        $new_amount = 9000000;
        // Post data
        $postData = [
            'name'                      => $new_deal_name,  // Меняем название
            'amount'                    => $new_amount,     // Меняем сумму
            'deal_role'                 => 'customer',
            'deal_civil_law_subject'    => $this->user->getCivilLawSubjects()->first()->getId(), // именно first(), т.к last() может быть не pattern
            'payment_method'            => '',
            'counteragent_email'        => 'test2@simple-technology.ru',
            'deal_type'                 => 'invalid_type_id', // Не пройдет валидацию
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => '0',
            'csrf'                      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/edit/'.$deal->getId(), 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(DealController::ERROR_INVALID_FORM_DATA, $data['message']);
        $this->assertArrayHasKey('data', $data);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(array('deal' => $deal));

        // Проверяем, что ничего не изменилось
        $this->assertEquals($deal_name, $deal->getName());
        $this->assertEquals($amount, $payment->getDealValue());
    }

    /////////////// Show

    /**
     * Доступность /deal/show/id участнику сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testShowActionCanBeAccessedByParticipant()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (show)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/show');
        $this->assertQuery('.Content .DealShowPage');

        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertArrayHasKey('dealFileUploadForm', $view_vars);
        $this->assertArrayHasKey('trackingNumberForm', $view_vars);
        $this->assertArrayHasKey('dealCommentForm', $view_vars);
        $this->assertArrayHasKey('deal_operators_comments', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertFalse($view_vars['is_operator']);
    }

    /**
     * Ajax
     * Доступность /deal/show/id участнику сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testShowActionCanBeAccessedByParticipantForAjax()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (show)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/show/'.$deal->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/show');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal single', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($deal_name, $data['data']['deal']['name']);
        $this->assertTrue($data['data']['deal']['is_customer']);
        $this->assertEquals($amount, $data['data']['deal']['amount_without_fee']);
    }

    /**
     * Доступность /deal/show/id Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testShowActionCanBeAccessedByOperator()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (show)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());

        // Создаем нового юзера, не имеющего отношения к созданной сделке
        $this->createNewUser('testTest', 'test_test@test.net', '+79103300722', 'asdfgh');
        $newUser = $this->userManager->getUserByLogin('testTest');
        // Присваиваем роль 'Operator'
        $this->assignRoleToTestUser('Operator', $newUser);
        // Залогиниваем нового юзера
        $this->login('testTest', 'asdfgh');
        // Проверяем
        $this->assertEquals('testTest', $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/show');
        // Что отдается в шаблон
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertArrayHasKey('dealFileUploadForm', $view_vars);
        $this->assertArrayHasKey('trackingNumberForm', $view_vars);
        $this->assertArrayHasKey('dealCommentForm', $view_vars);
        $this->assertArrayHasKey('deal_operators_comments', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertTrue($view_vars['is_operator']);
        // Возвращается из шаблона
        $this->assertQuery('#deal-comment-form');
    }

    /**
     * Недоступность /deal/show/id не участнику сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testShowActionCanNotBeAccessedByNotParticipant()
    {
        // Присваиваем тестовому юзеру нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (show not owner)';
        $deal_role = 'customer';
        $amount = 400000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());

        // Создаем нового юзера, не имеющего отношения к созданной сделке
        $this->createNewUser('testTest', 'test_test@test.net', '+79103300722', 'asdfgh');
        $newUser = $this->userManager->getUserByLogin('testTest');
        // Присваиваем роль 'Verified'
        $this->assignRoleToTestUser('Verified', $newUser);
        // Залогиниваем нового юзера
        $this->login('testTest', 'asdfgh');
        // Проверяем
        $this->assertEquals('testTest', $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /deal/show/1 не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testShowActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/deal/show/1', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/show/1');
    }

    /**
     * Недоступность /deal/show/1 авторизованному пользователю без роли Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testShowActionCanNotBeAccessedUnverifiedUser()
    {
        // Присваиваем тестовому юзеру нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (show to Unverified)';
        $deal_role = 'customer';
        $amount = 400000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());

        $this->assignRoleToTestUser('Unverified');
        $this->login();

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /////////////// delete

    // Неавторизованный

    /**
     * Недоступность /deal/id (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteCanNotBeAccessedByNonAuthorised()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/deal/'.$deal->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-single');
        $this->assertRedirectTo('/login?redirectUrl=/deal/'.$deal->getId());
    }

    /**
     * Для Ajax
     * Недоступность /deal/id (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId(),'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-single');
        $this->assertRedirectTo('/login?redirectUrl=/deal/'.$deal->getId());
    }

    // Не владелец

    /**
     * Недоступность /deal/id (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/deal/'.$deal->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /deal/id (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->getUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /deal/id (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteCanNotBeAccessedByOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/deal/'.$deal->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /deal/id (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteCanNotBeAccessedByOperatorForAjax()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Удаление неподтвержденной сделки /deal/id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Владельцем
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteForOwner()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // Проверяем
        $this->assertEquals(self::TEXT_DEAL_NAME, $deal->getName());

        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/deal/'.$deal->getId(), 'POST', $postData);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // Must be null (deleted)
        $this->assertNull($deal);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-single');
        $this->assertRedirectTo('/deal');
    }

    /**
     * Для Ajax
     * Удаление неподтвержденной сделки /deal/id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Владельцем
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteForOwnerForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // Проверяем
        $this->assertEquals(self::TEXT_DEAL_NAME, $deal->getName());

        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // Must be null (deleted)
        $this->assertNull($deal);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-single');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(DealController::SUCCESS_DELETE, $data['message']);
    }

    /**
     * Попытка удаления подтвержденной сделки /deal/id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Владельцем
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteConfirmedForOwner()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->confirmAndReturnTestFixtureDeal();

        // Проверяем
        $this->assertEquals(self::TEXT_DEAL_NAME, $deal->getName());

        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/deal/'.$deal->getId(), 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // Проверяем, что сделка не удалена
        $this->assertEquals(self::TEXT_DEAL_NAME, $deal->getName());

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-single');
        $this->assertQuery('.Content .MessagePage');

        $this->assertArrayHasKey('message', $view_vars);
    }

    /**
     * Для Ajax
     * Попытка удаления подтвержденной сделки /deal/id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Владельцем
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteConfirmedForOwnerForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->confirmAndReturnTestFixtureDeal();

        // Проверяем
        $this->assertEquals(self::TEXT_DEAL_NAME, $deal->getName());

        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        // Проверяем, что сделка не удалена
        $this->assertEquals(self::TEXT_DEAL_NAME, $deal->getName());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-single');
        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(DealController::CAN_NOT_BE_DELETED, $data['message']);
    }

    /////////////// deleteForm

    // Неавторизованный

    /**
     * Недоступность /deal/id/delete (GET на deleteFormAction)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorised()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        $this->dispatch('/deal/'.$deal->getId().'/delete');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-form-delete');
        $this->assertRedirectTo('/login?redirectUrl=/deal/'.$deal->getId().'/delete');
    }

    /**
     * Для Ajax
     * Недоступность /deal/id/delete (GET на deleteFormAction)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/delete',
            'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-form-delete');
        $this->assertRedirectTo('/login?redirectUrl=/deal/'.$deal->getId().'/delete');
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /deal/id/delete (GET на deleteFormAction) по Ajax авторизованному пользователю
     * Метод для Ajax не поддерживается!
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteFormCanNotBeAccessedByAuthorisedForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/delete',
            'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-form-delete');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /deal/id/delete (GET на deleteFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteFormCanNotBeAccessedByOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        $this->dispatch('/deal/'.$deal->getId().'/delete', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-form-delete');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /deal/id/delete (GET на deleteFormAction)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteFormCanNotBeAccessedByNotOwner()
    {
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        $this->dispatch('/deal/'.$deal->getId().'/delete', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-form-delete');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /deal/id/delete (GET на deleteFormAction)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteFormCanBeAccessedByOwner()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        $this->dispatch('/deal/'.$deal->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-form-delete');
        // Что отдается в шаблон
        $this->assertArrayHasKey('deal', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#deal-delete-form');
    }

    /**
     * Попытка зайти на форму удаления /deal/id/delete (GET на deleteFormAction)
     * подтвержденной сделки (Владельцем)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDeleteFormForConfirmedCanBeAccessedByOwner()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->confirmAndReturnTestFixtureDeal();

        // Проверяем
        $this->assertEquals(self::TEXT_DEAL_NAME, $deal->getName());

        $this->dispatch('/deal/'.$deal->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-form-delete');
        $this->assertQuery('.Content .MessagePage');
        // Что отдается в шаблон
        $this->assertArrayHasKey('message', $view_vars);
    }

    /////////////// DownloadContract

    /**
     * Доступность /deal/contract/id участнику сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDownloadContractActionCanBeAccessedByOwner()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        /** @var Deal $deal */ // Подтверждаем тестовую фикстурную сделку
        $deal = $this->confirmAndReturnTestFixtureDeal();
        // Проверяем
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm());
        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($deal);

        $this->dispatch('/deal/contract/'.$deal->getId(), 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/contract');

        // @TODO Как проверить отдачу файла на загрузку

        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.DealController::DEAL_CONTRACTS_STORING_FOLDER;
        // Удаляем сгенерированный файл
        $this->removeFile($upload_folder."/".$deal->getDealContractFile()->getFIle()->getName());
    }

    /**
     * Попытка загрузить контракт неподтвержденной сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDownloadContractActionOfUnconfirmedDeal()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Фикстура']);

        // Проверяем
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm());
        $this->assertFalse($deal->getContractor()->getDealAgentConfirm());

        $this->dispatch('/deal/contract/'.$deal->getId(), 'GET');
        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/contract');

        $this->assertQuery('.Content .MessagePage');
    }

    /**
     * Попытка загрузить контракт несуществующей сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDownloadContractActionOfNonexistentDeal()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/contract/999999', 'GET');
        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/contract');

        $this->assertQuery('.Content .MessagePage');
    }

    /**
     * Доступность /deal/contract/id Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDownloadContractActionCanBeAccessedByOperator()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        /** @var Deal $deal */ // Подтверждаем тестовую фикстурную сделку
        $deal = $this->confirmAndReturnTestFixtureDeal();
        // Проверяем
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm());
        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($deal);

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());

        // Создаем нового юзера, не имеющего отношения к созданной сделке
        $this->createNewUser('testTest', 'test_test@test.net', '+79103300722', 'asdfgh');
        $newUser = $this->userManager->getUserByLogin('testTest');
        // Присваиваем роль 'Operator'
        $this->assignRoleToTestUser('Operator', $newUser);
        // Залогиниваем нового юзера
        $this->login('testTest', 'asdfgh');
        // Проверяем
        $this->assertEquals('testTest', $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/contract/'.$deal->getId(), 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/contract');

        // @TODO Как проверить отдачу файла на загрузку

        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.DealController::DEAL_CONTRACTS_STORING_FOLDER;
        // Удаляем сгенерированный файл
        $this->removeFile($upload_folder."/".$deal->getDealContractFile()->getFIle()->getName());
    }

    /**
     * Недоступность /deal/contract/id не участнику сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDownloadContractActionCanNotBeAccessedByNotOwner()
    {
        // Присваиваем тестовому юзеру нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        /** @var Deal $deal */ // Подтверждаем тестовую фикстурную сделку
        $deal = $this->confirmAndReturnTestFixtureDeal();
        // Проверяем
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm());
        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($deal);

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());

        // Создаем нового юзера, не имеющего отношения к созданной сделке
        $this->createNewUser('testTest', 'test_test@test.net', '+79103300722', 'asdfgh');
        $newUser = $this->userManager->getUserByLogin('testTest');
        // Присваиваем роль 'Verified'
        $this->assignRoleToTestUser('Verified', $newUser);
        // Залогиниваем нового юзера
        $this->login('testTest', 'asdfgh');
        // Проверяем
        $this->assertEquals('testTest', $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/contract/'.$deal->getId(), 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/not-authorized');

        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.DealController::DEAL_CONTRACTS_STORING_FOLDER;
        // Удаляем сгенерированный файл
        $this->removeFile($upload_folder."/".$deal->getDealContractFile()->getFIle()->getName());
    }

    /**
     * Недоступность /deal/contract/1 не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDownloadContractActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/deal/contract/1', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/contract/1');
    }

    /**
     * Недоступность /deal/contract/id авторизованному пользователю без роли Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDownloadContractActionCanNotBeAccessedUnverifiedUser()
    {
        // Присваиваем тестовому юзеру нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        /** @var Deal $deal */ // Подтверждаем тестовую фикстурную сделку
        $deal = $this->confirmAndReturnTestFixtureDeal();
        // Проверяем
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm());
        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($deal);

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());

        $this->assignRoleToTestUser('Unverified');
        $this->login();

        $this->dispatch('/deal/contract/'.$deal->getId(), 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/not-authorized');

        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.DealController::DEAL_CONTRACTS_STORING_FOLDER;
        // Удаляем сгенерированный файл
        $this->removeFile($upload_folder."/".$deal->getDealContractFile()->getFIle()->getName());
    }

    /**
     * Список DealTypes для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testGetDealTypesActionForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/deal-types', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Deal types', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Недоступность /deal/expired не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testExpiredActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/deal/expired', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal/expired');
    }

    /**
     * Недоступность /deal/expired не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testExpiredActionCanNotBeAccessedByNotOperator()
    {
        // Присваиваем тестовому юзеру нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/expired');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /deal/expired Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testExpiredActionCanBeAccessedByOperator()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Operator');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/expired');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/expired');
        $this->assertQuery('.PageTable form table');

        $this->assertArrayHasKey('expiredSearchForm', $view_vars);
        $this->assertArrayHasKey('payments', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
    }

    /**
     * Доступность /deal/expired Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     *
     * @TODO Симуляция отправки email не включается
     */
    public function testExpiredActionWithExpiredDeal()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Operator');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (expired)';
        $deal_role = 'customer';
        $amount = 400000;
        $period = self::MINIMUM_DELIVERY_PERIOD + 5;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Set Customer confirm
        $deal->getCustomer()->setDealAgentConfirm(true);
        $currentDate = new \DateTime();
        $deal->getContractor()->setDateOfConfirm($currentDate->modify('-' . ($period + 1) . ' days'));
        $deal->getContractor()->setDealAgentConfirm(true);
        $currentDate = new \DateTime();
        $deal->getContractor()->setDateOfConfirm($currentDate->modify('-' . $period . ' days'));

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $deal->getContractor()->getUser()->getCivilLawSubjects()->last();
        $deal->getContractor()->setCivilLawSubject($civilLawSubject);
        $paymentMethod = $this->entityManager->getRepository(PaymentMethod::class)
            ->findOneBy(array('civilLawSubject' => $civilLawSubject));

        // Set dates expired
        $currentDate = new \DateTime();
        $deal->setCreated($currentDate->modify('-' . $period . ' days'));
        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($deal);
        $payment->setPaymentMethod($paymentMethod);

        $this->entityManager->flush();

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($deal);

        $file_name = 'payment_order_for_debit.txt';
        // Path
        $path = "./module/Application/test/files/".$file_name;

        // Создаем файл платёжки и по нему PaymentOrder
        $this->createPaymentOrderFromFile($deal, $payment, $path);

        // Меняем DeFactoDate, чтобы удовлетворить условиям запроса для expired date
        $currentDate = new \DateTime();
        $payment->setDeFactoDate($currentDate->modify('-' . 22 . ' days'));
        $pannedDate = $payment->getDeFactoDate();
        $payment->setPlannedDate($pannedDate->modify('-2 days'));

        $this->entityManager->flush();

        // Проверка наличия готовых к выплате сделок // Должна быть одна
        $payments = $this->entityManager->getRepository(Payment::class)
            ->selectSortedPaymentsWithExpiredDeals([], 1);

        $this->assertEquals(1, count($payments));
        $this->assertEquals('Сделка для unit тестов (expired)', $payments[0]->getDeal()->getName());

        $this->dispatch('/deal/expired');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/expired');
        $this->assertQuery('.PageTable form table');

        $this->assertArrayHasKey('payments', $view_vars);
        $this->assertGreaterThan(0, count($view_vars['payments']));
        $this->assertTrue($view_vars['dealFilterForm'] instanceof DealSearchForm);

        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.DealController::DEAL_CONTRACTS_STORING_FOLDER;
        // Удаляем сгенерированный файл
        $this->removeFile($upload_folder."/".$deal->getDealContractFile()->getFIle()->getName());
    }

    /**
     * Доступность /deal/expired Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     *
     * @TODO Симуляция отправки email не включается
     */
    public function testExpiredActionWithPaymentOrderCreation()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Operator');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (expired)';
        $deal_role = 'customer';
        $amount = 400000;
        $period = self::MINIMUM_DELIVERY_PERIOD + 5;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Set Customer confirm
        $deal->getCustomer()->setDealAgentConfirm(true);
        $currentDate = new \DateTime();
        $deal->getCustomer()->setDateOfConfirm($currentDate->modify('-' . ($period + 1) . ' days'));
        $deal->getContractor()->setDealAgentConfirm(true);
        $currentDate = new \DateTime();
        $deal->getContractor()->setDateOfConfirm($currentDate->modify('-' . $period . ' days'));

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $deal->getContractor()->getUser()->getCivilLawSubjects()->last();
        $deal->getContractor()->setCivilLawSubject($civilLawSubject);
        $paymentMethod = $this->entityManager->getRepository(PaymentMethod::class)
            ->findOneBy(array('civilLawSubject' => $civilLawSubject));

        // Set dates expired
        $currentDate = new \DateTime();
        $deal->setCreated($currentDate->modify('-' . $period . ' days'));
        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($deal);
        $payment->setPaymentMethod($paymentMethod);

        $this->entityManager->flush();

        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        $this->assertTrue($deal->getCustomer()->getDealAgentConfirm());
        $this->assertTrue($deal->getContractor()->getDealAgentConfirm());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($deal);

        $file_name = 'payment_order_for_debit.txt';
        // Path
        $path = "./module/Application/test/files/".$file_name;

        // Создаем файл платёжки и по нему PaymentOrder
        $this->createPaymentOrderFromFile($deal, $payment, $path);

        // Меняем DeFactoDate, чтобы удовлетворить условиям запроса для expired date
        $payment->setDeFactoDate($currentDate->modify('-' . 22 . ' days'));
        $this->entityManager->flush();

        // Проверка наличия готовых к выплате сделок // Должна быть одна
        $payments = $this->entityManager->getRepository(Payment::class)
            ->selectSortedPaymentsWithExpiredDeals([], 1);

        #var_dump($payments);
        #die();

        $this->assertEquals(1, count($payments));
        $this->assertEquals('Сделка для unit тестов (expired)', $payments[0]->getDeal()->getName());

        $this->dispatch('/deal/expired/'.$deal->getId());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        #$this->assertResponseStatusCode(302);
        #$this->assertRedirectTo('/bank-client/credit-unpaid');

        // Получаем только что добавленный BankClientPaymentOrder по назначению платежа
        $creditPaymentOrder = $this->entityManager->getRepository(BankClientPaymentOrder::class)
            ->findOneBy(array('paymentPurpose' => PayOffManager::PURPOSE_SUBJECT . $deal->getId()));
        // Снова получаем Payment
        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($deal);
        // Проверяем, что CreditPaymentOrder, приписанный к Payment тот же, что и PaymentOrder с creditPaymentOrder
        $this->assertEquals($payment->getCreditPaymentOrders()->ferst(), $creditPaymentOrder);

        // Удаляем результаты работы
        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.DealController::DEAL_CONTRACTS_STORING_FOLDER;
        // Удаляем сгенерированный файл
        $this->removeFile($upload_folder."/".$deal->getDealContractFile()->getFIle()->getName());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testDealPercentageAction()
    {
        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/deal/deal-percentage', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('deal percentage', $data['message']);
        $this->assertEquals(4, $data['data']['percentage']);
    }

    /**
     * Недоступность /deal/abnormal не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testAbnormalActionCanNotBeAccessedByNotOperator()
    {
        // Присваиваем тестовому юзеру нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/abnormal');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /deal/abnormal Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     */
    public function testAbnormalActionCanBeAccessedByOperator()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Operator');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/abnormal');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/abnormal');
        $this->assertQuery('.PageTable form table');

        $this->assertArrayHasKey('dealAbnormalForm', $view_vars);
        $this->assertArrayHasKey('payments', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
    }

    /**
     * Создаем проблемную сделку по DateOfDebit и проверяем ее наличие
     * Сеттим $dateOfDebit (string) заведомо больше, чем DateOfConfirm + DeliveryPeriod
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     *
     * @TODO Симуляция отправки email не включается
     */
    public function testAbnormalActionByDateOfDebit()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Operator');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Фикстура']);

        $period = DealControllerTest::MINIMUM_DELIVERY_PERIOD + 2;
        // Set agents confirm
        $deal->getCustomer()->setDealAgentConfirm(true);
        $currentDate = new \DateTime();
        $deal->getCustomer()->setDateOfConfirm($currentDate->modify('-' . ($period + 1) . ' days'));
        $deal->getContractor()->setDealAgentConfirm(true);
        $currentDate = new \DateTime();
        $deal->getContractor()->setDateOfConfirm($currentDate->modify('-' . $period . ' days'));
        $currentDate = new \DateTime();
        $deal->setDateOfConfirm($currentDate->modify('-' . ($period + 1) . ' days'));

        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($deal);

        $payment->setPlannedDate($currentDate->modify('-2 days'));

        $this->entityManager->flush();

        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($deal);

        $file_name = 'payment_order_for_debit.txt';
        // Path
        $path = "./module/Application/test/files/".$file_name;

        // Создаем файл платёжки и по нему PaymentOrder
        $this->createPaymentOrderFromFile($deal, $payment, $path);

        // Меняем DateOfConfirm
        $currentDate = new \DateTime();
        $deal->setDateOfConfirm($currentDate->modify('-' . 22 . ' days'));
        /** @var BankClientPaymentOrder $debitPaymentOrder */
        $debitPaymentOrder = $payment->getDebitPaymentOrders()->first();
        // Сеттим $dateOfDebit (string) заведомо больше, чем DateOfConfirm + DeliveryPeriod
        $currentDate = new \DateTime();
        $dateOfDebit = $currentDate->modify('-' . 2 . ' days');
        $debitPaymentOrder->setDateOfDebit($dateOfDebit->format('d.m.Y'));

        $this->entityManager->flush();

        // Проверка BankClientPaymentOrder.dateOfDebit > (Deal.dateOfConfirm + Deal.deliveryPeriod)
        #$this->assertTrue($this->dealPolitics->isDealAbnormal($deal));

        $this->dispatch('/deal/abnormal');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/abnormal');
        $this->assertQuery('.PageTable form table');

        $this->assertArrayHasKey('dealAbnormalForm', $view_vars);
        $this->assertArrayHasKey('payments', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);

        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.DealController::DEAL_CONTRACTS_STORING_FOLDER;
        // Удаляем сгенерированный файл
        $this->removeFile($upload_folder."/".$deal->getDealContractFile()->getFile()->getName());
    }

    /**
     * Создаем проблемную сделку по DateOfDebit и проверяем ее наличие
     * Меняем сумму сделки в DebitPaymentOrder
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     *
     * @TODO Симуляция отправки email не включается
     */
    public function testAbnormalActionByAmount()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Operator');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Фикстура']);

        $period = DealControllerTest::MINIMUM_DELIVERY_PERIOD + 2;
        // Set agents confirm
        $deal->getCustomer()->setDealAgentConfirm(true);
        $deal->getCustomer()->setDealAgentOnceConfirm(true);
        $currentDate = new \DateTime();
        $deal->getCustomer()->setDateOfConfirm($currentDate->modify('-' . ($period + 1) . ' days'));
        $deal->getContractor()->setDealAgentConfirm(true);
        $deal->getContractor()->setDealAgentOnceConfirm(true);
        $currentDate = new \DateTime();
        $deal->getContractor()->setDateOfConfirm($currentDate->modify('-' . $period . ' days'));

        $this->entityManager->flush();

        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($deal);

        // Запуск формирования PDF сделки (договора)
        $this->dealManager->createDealPDF($deal);

        $file_name = 'payment_order_for_debit.txt';
        // Path
        $path = "./module/Application/test/files/".$file_name;

        // Создаем файл платёжки и по нему PaymentOrder
        $this->createPaymentOrderFromFile($deal, $payment, $path);

        /** @var BankClientPaymentOrder $debitPaymentOrder */
        $debitPaymentOrder = $payment->getDebitPaymentOrders()->first();
        // Меняем сумму сделки в DebitPaymentOrder
        $debitPaymentOrder->setAmount($payment->getExpectedValue() - 1);
        // Проверка выявляет проблем в тестовой Сделке
        #$this->assertTrue($this->dealPolitics->isDealAbnormal($deal));

        $this->entityManager->flush();

        // Проверка BankClientPaymentOrder.dateOfDebit > (Deal.dateOfConfirm + Deal.deliveryPeriod)
        #$this->assertTrue($this->dealPolitics->isDealAbnormal($deal));

        $this->dispatch('/deal/abnormal');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/abnormal');
        $this->assertQuery('.PageTable form table');

        $this->assertArrayHasKey('dealAbnormalForm', $view_vars);
        $this->assertArrayHasKey('payments', $view_vars);
        $this->assertGreaterThan(0, count($view_vars['payments']));
        $this->assertArrayHasKey('paginator', $view_vars);

        // Upload folder
        $upload_folder = "./".$this->main_upload_folder.DealController::DEAL_CONTRACTS_STORING_FOLDER;
        // Удаляем сгенерированный файл
        $this->removeFile($upload_folder."/".$deal->getDealContractFile()->getFile()->getName());
    }

    /**
     * Недоступность /deal/overpay/id не авторизованному
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanNotBeAccessedByNotAuthorized()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertRedirectTo('/login?redirectUrl=/deal/overpay/'.$deal->getId());
    }

    /**
     * Ajax
     * Недоступность /deal/overpay/id не авторизованному
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanNotBeAccessedByNotAuthorizedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('not_authorized', $data['message']);
    }

    /**
     * Недоступность /deal/overpay/id пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanNotBeAccessedByVerified()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Ajax
     * Недоступность /deal/overpay/id пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanNotBeAccessedByVerifiedForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('access_denied', $data['message']);
    }

    /**
     * Доступность /deal/overpay/id Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanBeAccessedByOperator()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');

        $this->assertArrayHasKey('repaidOverpayForm', $view_vars);
        $this->assertInstanceOf(RepaidOverpayForm::class, $view_vars['repaidOverpayForm']);
        $this->assertEquals($deal->getId(), (int)$view_vars['deal']['id']);
        $this->assertArrayHasKey('overpaid_amount', $view_vars);
        $this->assertArrayHasKey('expected_amount', $view_vars);
        $this->assertEquals($payment->getExpectedValue(), $view_vars['expected_amount']);
        $this->assertArrayHasKey('incomingTransactions', $view_vars);
        $this->assertGreaterThan(0, count($view_vars['incomingTransactions']));
        $this->assertInternalType('array', $view_vars['incomingTransactions'][0]);
    }

    /**
     * Ajax
     * Доступность /deal/overpay/id Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanBeAccessedByOperatorForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/overpay/'.$deal->getId(), 'get', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Overpay', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($deal->getId(), (int)$data['data']['deal']['id']);
        $this->assertArrayHasKey('overpaid_amount', $data['data']);
        $this->assertArrayHasKey('expected_amount', $data['data']);
        $this->assertEquals($payment->getExpectedValue(), $data['data']['expected_amount']);
        $this->assertArrayHasKey('incomingTransactions', $data['data']);
        $this->assertGreaterThan(0, count($data['data']['incomingTransactions']));
        $this->assertInternalType('array', $data['data']['incomingTransactions'][0]);
    }

    /**
     * Неоступность /deal/overpay/id для сделки без переплаты (Оператору)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanNotBeAccessedForDealWithoutOverpay()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */ // Получаем сделку без преплаты!
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DEBIT_MATCHED_NAME]);

        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
    }

    /**
     * Ajax
     * Неоступность /deal/overpay/id для сделки без переплаты (Оператору)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanNotBeAccessedForDealWithoutOverpayForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */ // Получаем сделку без преплаты!
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DEBIT_MATCHED_NAME]);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/overpay/'.$deal->getId(), 'get', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Deal is not overpaid', $data['message']);
    }


    /**
     * Валидный POST на /deal/overpay/id Оператором
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayWithValidPostByOperator()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id'   => $deal->getId(),
            'csrf'      => $csrf
        ];

        $this->dispatch('/deal/overpay/'.$deal->getId(), 'POST', $postData);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Поверяем, что создалась платёжка на выплату
        /** @var RepaidOverpay $repaidOverpay */
        $repaidOverpay = $payment->getRepaidOverpay();

        $is_outgoing_payment_overpaid = false;
        /** @var PaymentOrder $paymentOrder */
        foreach ($repaidOverpay->getPaymentOrders() as $paymentOrder) {

            /** @var BankClientPaymentOrder $bankClientPaymentOrder */
            $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();

            $purpose = $bankClientPaymentOrder->getPaymentPurpose();

            if (strpos($purpose, PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY]) !== false) {

                $is_outgoing_payment_overpaid = true;
            }
        }

        // Проверяем, что outgoing PaymentOverpaid платёжка создалась
        $this->assertTrue($is_outgoing_payment_overpaid);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
    }

    /**
     * Ajax
     * Валидный POST на /deal/overpay/id Оператором
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayWithValidPostByOperatorForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id'   => $deal->getId(),
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/overpay/'.$deal->getId(), 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Поверяем, что создалась платёжка на выплату
        /** @var RepaidOverpay $repaidOverpay */
        $repaidOverpay = $payment->getRepaidOverpay();

        $is_outgoing_payment_overpaid = false;
        /** @var PaymentOrder $paymentOrder */
        foreach ($repaidOverpay->getPaymentOrders() as $paymentOrder) {

            /** @var BankClientPaymentOrder $bankClientPaymentOrder */
            $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();

            $purpose = $bankClientPaymentOrder->getPaymentPurpose();

            if (strpos($purpose, PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY]) !== false) {

                $is_outgoing_payment_overpaid = true;
            }
        }

        // Проверяем, что outgoing PaymentOverpaid платёжка создалась
        $this->assertTrue($is_outgoing_payment_overpaid);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Платёжка на возврат переплаты для сделки ' .$deal->getNumber(). ' сформирована!', $data['message']);
    }







    /**
     * Неоступность /deal/overpay/id при наличии у сделки RepaidOverpay (Оператору)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanNotBeAccessedForDealWithoutRepaidOverpay()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */ // Получаем сделку без преплаты!
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        // Создаем возврат переплаты
        $this->dealManager->overpaidDealsProcessing($deal);

        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
    }

    /**
     * Ajax
     * Неоступность /deal/overpay/id при наличии у сделки RepaidOverpay (Оператору)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanNotBeAccessedForDealWithoutRepaidOverpayForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */ // Получаем сделку без преплаты!
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        // Создаем возврат переплаты
        $this->dealManager->overpaidDealsProcessing($deal);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/overpay/'.$deal->getId(), 'get', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('RepaidOverpay already done', $data['message']);
    }

    // @TODO Отправка POST из формы на создание возарата переплаты (RepaidOverpay)


    /**
     * Доступность формы /deal/$id/dispute/create Заказчику
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     */
    public function testCreateFormActionCanBeAccessedByCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DISPUTE_DEAL_NAME));

        // Присваиваем нужную роль и залогиниваемся
        $this->login('test');
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/'.$deal->getId().'/dispute-create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/dispute-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertArrayHasKey('disputeForm', $view_vars);
        $this->assertTrue($view_vars['disputeForm'] instanceof DisputeForm);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-create-form');
    }

    /**
     * /deal/:idDeal/invitation/resend
     *
     * не доступность не зарегистрированному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     * @group dispute

     */
    public function testDealInvitationResendActionCanNotBeAccessedByNotAuthorized()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_INVITATION_NAME]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/'.$deal->getId().'/invitation/resend', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal-invitation-resend');
        $this->assertRedirectTo('/login?redirectUrl=/deal/'.$deal->getId().'/invitation/resend');
    }

    /**
     * /deal/:idDeal/invitation/resend
     *
     * не доступность зарегистрированному пользователю с ролью Unverified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     * @group dispute

     */
    public function testDealInvitationResendActionCanNotBeAccessedByUnverifiedUser()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_INVITATION_NAME]);
        $this->assertNotNull($deal);

        $this->assignRoleToTestUser('Unverified');
        $this->login();

        $this->dispatch('/deal/'.$deal->getId().'/invitation/resend', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal-invitation-resend');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * /deal/:idDeal/invitation/resend
     * Ajax
     * не доступность зарегистрированному пользователю с ролью Unverified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     * @group dispute

     */
    public function testDealInvitationResendActionCanNotBeAccessedByUnverifiedUserForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_INVITATION_NAME]);
        $this->assertNotNull($deal);

        $this->assignRoleToTestUser('Unverified');
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/invitation/resend', 'GET', [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal-invitation-resend');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * /deal/:idDeal/invitation/resend
     *
     * не доступность зарегистрированному пользователю если он не участник сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     * @group dispute

     */
    public function testDealInvitationResendActionCanNotBeAccessedByUserNotPartDeal()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_INVITATION_NAME]);
        $this->assertNotNull($deal);

        // Создаем нового юзера, не имеющего отношения к созданной сделке
        $this->createNewUser('phpunit_login', 'phpunit_email@test.net', '+79103300722', '123456');
        $newUser = $this->userManager->getUserByLogin('phpunit_login');
        // Присваиваем роль 'Verified'
        $this->assignRoleToTestUser('Verified', $newUser);
        $this->login('phpunit_login', '123456');

        $this->dispatch('/deal/'.$deal->getId().'/invitation/resend', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal-invitation-resend');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * /deal/:idDeal/invitation/resend
     * Ajax
     * не доступность зарегистрированному пользователю если он не участник сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     * @group dispute

     */
    public function testDealInvitationResendActionCanNotBeAccessedByUserNotPartDealForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_INVITATION_NAME]);
        $this->assertNotNull($deal);

        // Создаем нового юзера, не имеющего отношения к созданной сделке
        $this->createNewUser('phpunit_login', 'phpunit_email@test.net', '+79103300722', '123456');
        $newUser = $this->userManager->getUserByLogin('phpunit_login');
        // Присваиваем роль 'Verified'
        $this->assignRoleToTestUser('Verified', $newUser);
        $this->login('phpunit_login', '123456');

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/invitation/resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal-invitation-resend');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('code', $data);
        $this->assertSame('ERROR', $data['status']);
        $this->assertSame('ERROR_ACCESS_DENIED', $data['code']);
    }

    /**
     * /deal/:idDeal/invitation/resend
     * Ajax
     * попытка отправить с невалидным csrf
     *
     * @throws \Exception
     *
     * @group deal
     * @group dispute

     */
    public function testDealInvitationResendActionCanBeAccessedWithoutCsrf()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_INVITATION_NAME]);
        $this->assertNotNull($deal);
        $this->login();
        $postData = ['csrf' => '11111']; //не валидный csrf

        $isXmlHttpRequest = false;
        $this->dispatch('/deal/'.$deal->getId().'/invitation/resend', 'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal-invitation-resend');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertSame(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $view_vars['message']);
    }

    /**
     * /deal/:idDeal/invitation/resend
     * Ajax
     * попытка отправить с невалидным csrf
     *
     * @throws \Exception
     *
     * @group deal
     * @group dispute

     */
    public function testDealInvitationResendActionCanBeAccessedWithoutCsrfForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_INVITATION_NAME]);
        $this->assertNotNull($deal);
        $this->login();
        $postData = ['csrf' => '11111']; //не валидный csrf
        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/invitation/resend', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal-invitation-resend');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('code', $data);
        $this->assertArrayHasKey('data', $data);

        $this->assertSame('ERROR', $data['status']);
        $this->assertSame(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('csrf', $data['data']);
    }


    /**
     * /deal/:idDeal/invitation/resend
     * тест успешной отправки
     *
     * @throws \Exception
     *
     * @group deal
     * @group dispute

     */
    public function testDealInvitationResendActionWithValidPost()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_INVITATION_NAME]);
        $this->assertNotNull($deal);
        $this->login();

        $csrf = new Element\Csrf('csrf');
        $postData = ['csrf' => $csrf->getValue()];

        $isXmlHttpRequest = false;
        $this->dispatch('/deal/'.$deal->getId().'/invitation/resend', 'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal-invitation-resend');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());
    }

    /**
     * /deal/:idDeal/invitation/resend
     * Ajax
     * тест успешной отправки
     *
     * @throws \Exception
     *
     * @group deal
     * @group dispute

     */
    public function testDealInvitationResendActionWithValidPostForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_INVITATION_NAME]);
        $this->assertNotNull($deal);
        $this->login();


        $csrf = new Element\Csrf('csrf');
        $postData = ['csrf' => $csrf->getValue()];
        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/invitation/resend', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal-invitation-resend');

        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertSame('SUCCESS', $data['status']);
        $this->assertSame('invitation resend', $data['message']);
    }


    /**
     * @param Deal $deal
     * @param Payment $payment
     * @param string $path
     * @return BankClientPaymentOrder
     * @throws \Exception
     */
    private function createPaymentOrderFromFile(Deal $deal, Payment $payment, string $path)
    {
//        /** @var PaymentMethodBankTransfer $customerBankTransfer */ // Реквизиты customer'а
//        $customerBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
//            ->findOneBy(array('paymentRecipientName' => 'Тест Тестов'));

        /** @var SystemPaymentDetails $systemPaymentDetails */ // Реквизиты GP
        $systemPaymentDetails = $this->entityManager->getRepository(SystemPaymentDetails::class)
            ->findOneBy(array('name' => 'ООО "Гарант Пэй"'));

        // Creates file if not exists
        $file_with_payment_order = fopen($path, "w") or die("Не удается создать файл");
        // Write file header data
        fwrite($file_with_payment_order, mb_convert_encoding(
                BankClientManager::FILE_HEADER,
                BankClientController::ORIGINAL_ENCODING, 'UTF-8')
        );
        // Main body of PaymentOrder
        $currentDate = new \DateTime();
        $txt = "СекцияДокумент=".BankClientManager::DOCUMENT_TYPE.PHP_EOL;
        $txt .= "Номер=".$deal->getId().PHP_EOL;
        $txt .= "Дата=".$currentDate->format('d.m.Y').PHP_EOL;
        $txt .= "ДатаСписано=".$currentDate->format('d.m.Y').PHP_EOL;
        $txt .= "Сумма=".$payment->getExpectedValue().PHP_EOL;
        $txt .= "ПлательщикСчет=40702810090030001128".PHP_EOL;
        $txt .= "Плательщик=Тест Тестов".PHP_EOL;
        $txt .= "ПлательщикИНН=".PHP_EOL;
        $txt .= "Плательщик1=Тест Тестов".PHP_EOL;
        $txt .= "ПлательщикКПП=".PHP_EOL;
        $txt .= "ПлательщикБанк1=ВТБ".PHP_EOL;
        $txt .= "ПлательщикБИК=044525201".PHP_EOL;
        $txt .= "ПлательщикКорсчет=30101810900000000790".PHP_EOL;
        $txt .= "ПолучательСчет=".$systemPaymentDetails->getAccountNumber().PHP_EOL;
        $txt .= "Получатель=ИНН ".$systemPaymentDetails->getInn()." ".$systemPaymentDetails->getPaymentRecipientName().PHP_EOL;
        $txt .= "ПолучательИНН=".$systemPaymentDetails->getInn().PHP_EOL;
        $txt .= "Получатель1=".$systemPaymentDetails->getPaymentRecipientName().PHP_EOL;
        $txt .= "ПолучательКПП=".$systemPaymentDetails->getKpp().PHP_EOL;
        $txt .= "ПолучательБанк1=".$systemPaymentDetails->getBank().PHP_EOL;
        $txt .= "ПолучательБИК=".$systemPaymentDetails->getBik().PHP_EOL;
        $txt .= "ПолучательКорсчет=".$systemPaymentDetails->getCorrAccountNumber().PHP_EOL;
        $txt .= "НазначениеПлатежа=".BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY.$deal->getId().PHP_EOL;
        $txt .= "ВидОплаты=01".PHP_EOL;
        $txt .= "Очередность=5".PHP_EOL;
        // Write main body
        fwrite($file_with_payment_order, mb_convert_encoding($txt, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
        // Write SECTION_END_POINT
        fwrite($file_with_payment_order, mb_convert_encoding(BankClientParser::SECTION_END_POINT.PHP_EOL,
            BankClientController::ORIGINAL_ENCODING, 'UTF-8'));

        // Генерируем файл с правильным параметром НазначениеПлатежа
        // Get data as array
        $payment_order_data = $this->bankClientParser->execute($path);
        // Create PaymentOrder
        /** @var BankClientPaymentOrder $paymentOrder */
        $paymentOrder = $this->bankClientManager->createNewPaymentOrderFromArray(
            $payment_order_data[0]
        );

        $this->entityManager->flush();

        return $paymentOrder;
    }

    /**
     * @param $login
     * @param $email
     * @param $phone
     * @param $password
     * @return User
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createNewUser($login, $email, $phone, $password)
    {
        $data   = [
            'login'     => $login,
            'phone'     => $phone,
            'email'     => $email,
            'password'  => $password,
        ];

        $checkedUser = $this->entityManager->getRepository(User::class)->findOneBy(['login' => $data['login']]);
        if($checkedUser){
            throw new LogicException(null, LogicException::USER_LOGIN_ALREADY_EXISTS);
        }
        $checkedEmail = $this->entityManager->getRepository(Email::class)
            ->findOneBy(['email' => strtolower($data['email']), 'isVerified' => true]);
        if($checkedEmail){
            throw new LogicException(null, LogicException::EMAIL_ALREADY_EXISTS);
        }
        $checkedPhone = $this->entityManager->getRepository(Phone::class)
            ->findOneBy(['phoneNumber' => $data['phone'], 'isVerified' => true]);
        if($checkedPhone){
            throw new LogicException(null, LogicException::PHONE_NOT_UNIQUE);
        }

        $user = new User();
        $user->setLogin($data['login']);
        $bCrypt = new Bcrypt();
        $user->setPassword($bCrypt->create($data['password']));
        $user->setCreated(new DateTime('now'));

        $email = new Email();
        $email->setEmail($data['email']);

        $phone = $this->basePhoneManager->addNewPhone($data['phone']);

        $this->entityManager->persist($user);
        $this->entityManager->persist($email);

        $user->setEmail($email);

        $this->entityManager->persist($user);

        $user->setPhone($phone);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
        $role = $this->entityManager->getRepository(Role::class)->findOneBy(['name' => 'Unverified']);

        if ($role && !$user->getRoles()->contains( $role )) {

            $user->addRole($role);
            $this->entityManager->persist($user);
            $this->entityManager->flush($user);
        }

        return $user;
    }

    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var \Application\Entity\Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if (!$login) {
            $login = $this->user_login;
        }
        if (!$password) {
            $password = $this->user_password;
        }
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();
        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $deal_name
     * @param string $deal_role
     * @param $amount
     * @param bool $confirmed
     * @return Deal
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createTestDeal(string $deal_name, string $deal_role, $amount, $confirmed=false)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $data = [
            'name'                      => $deal_name,
            'amount'                    => $amount,
            'deal_role'                 => $deal_role,
            'deal_civil_law_subject'    => $this->user->getCivilLawSubjects()->last()->getId(),
            'payment_method'            => '',
            'counteragent_email'        => 'test2@simple-technology.ru',
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => 0
        ];

        // Try to register dill
        // Распределение ролей в сделке взависимости от выбранной инициатором роли
        if($data['deal_role'] == 'customer') {
            $userCustomer   = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $userContractor = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $paramCustomer = $data;
            $paramContractor = [];
        } else {
            $userCustomer   = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $userContractor = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $paramCustomer = [];
            $paramContractor = $data;
        }

        $dealAgentCustomer = $this->dealAgentManager->createDealAgent($userCustomer, $paramCustomer);
        $dealAgentContractor = $this->dealAgentManager->createDealAgent($userContractor, $paramContractor);

        $user = $this->userManager->getUserByLogin($this->authService->getIdentity());

        $dealOwner = $this->dealAgentManager->defineDealOwner($user, $dealAgentCustomer, $dealAgentContractor);

        // Create Deal
        $deal = $this->dealManager->createDeal($data, $dealAgentCustomer, $dealAgentContractor, $dealOwner);

        if($confirmed) {
            $dealAgentCustomer->setDealAgentConfirm(true);
            $dealAgentContractor->setDealAgentConfirm(true);

            $this->entityManager->flush();
        }

        return $deal;
    }

    /**
     * Подтверждение тестовой фикстурной сделки
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function confirmAndReturnTestFixtureDeal()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        $deal->getCustomer()->setDealAgentConfirm(true);
        $deal->getContractor()->setDealAgentConfirm(true);
        $deal->getContractor()->setDateOfConfirm(new \DateTime());

        $this->entityManager->flush();

        return $deal;
    }

    /**
     * @param $path
     */
    private function removeFile($path)
    {
        unlink($path);
    }
}