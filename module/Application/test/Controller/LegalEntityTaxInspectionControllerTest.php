<?php

namespace ApplicationTest\Controller;

use Application\Controller\LegalEntityTaxInspectionController;
use Application\Entity\CivilLawSubject;
use Application\Entity\LegalEntity;
use Application\Entity\LegalEntityTaxInspection;
use Application\Entity\LegalEntityDocument;
use Application\Entity\LegalEntityDocumentType;
use Application\Entity\NdsType;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseRoleManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use Zend\Form\Element;
use CoreTest\ViewVarsTrait;

/**
 * Class LegalEntityTaxInspectionControllerTest
 * @package ApplicationTest\Controller
 */
class LegalEntityTaxInspectionControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DEFAULT_DOCUMENT_NAME = 'test name organisation for legal entity tax inspection';
    const DEFAULT_FILE_NAME = 'image_file_2_for_test.png';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * тестовый документ LegalEntityTaxInspection
     * @var
     */
    private $testLegalEntityTaxInspection;

    /**
     * тестое юр лицо LegalEntity
     * @var
     */
    private $testLegalEntity;

    /**
     * @var
     */
    private $main_project_url;

    /**
     * @var
     */
    private $main_upload_folder;
    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    /**
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->main_project_url = $config['main_project_url'];
        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();

        // create test document
        $this->testLegalEntityTaxInspection = $this->createTestLegalEntityTaxInspection($this->user_login);
        $this->testLegalEntity = $this->testLegalEntityTaxInspection->getLegalEntityDocument()->getLegalEntity();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    // ******************************
    // ******* method getList *******
    // ******************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-tax-inspection
     * (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/legal-entity-tax-inspection', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-tax-inspection');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection
     * (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-tax-inspection', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity-tax-inspection
     * (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity-tax-inspection', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection
     * (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /profile/legal-entity-tax-inspection
     * (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUser()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/legal-entity-tax-inspection', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');
        // Что отдается в шаблон
        $this->assertArrayHasKey('taxInspections', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity-tax-inspection
     * (GET на getList) авторизованному с ролью Verified не владелец
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUserNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(true, isset($data['data']['paginator']));
        $this->assertEquals(true, is_array($data['data']['paginator']));
        $this->assertEquals(true, isset($data['data']['taxInspections']));

        //ожидаем пусто т.к единственный документ принадлежит пользователь 'test'
        $this->assertEquals(0, count($data['data']['taxInspections']));
    }

    /**
     * Для Ajax
     * Доступность /profile/legal-entity-tax-inspection
     * (GET на getList) авторизованному с ролью Verified владелец
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUserOwnerForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(true, isset($data['data']['paginator']));
        $this->assertEquals(true, is_array($data['data']['paginator']));
        $this->assertEquals(true, isset($data['data']['taxInspections']));

        //ожидаем 1 т.к единственный документ принадлежит текушему пользователю
        $this->assertEquals(1, count($data['data']['taxInspections']));
        $this->assertEquals(self::DEFAULT_DOCUMENT_NAME, $data['data']['taxInspections'][0]['issue_organisation']);
    }

    // ******************************
    // ********* method get *********
    // ******************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-tax-inspection/'.$id);
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-tax-inspection/'.$id, $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperator()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperatorForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Не Владелец

    /**
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOwner()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Ajax
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOwner()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity-tax-inspection/:id
     * (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Не проходит. Разобраться.
     */
    public function testGetCanBeAccessedByOwnerForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(true, isset($data['data']['taxInspection']));
        $this->assertEquals(self::DEFAULT_DOCUMENT_NAME, $data['data']['taxInspection']['issue_organisation']);
    }

    // *****************************
    // ******* method update *******
    // *****************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-tax-inspection/'.$id);
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-tax-inspection/'.$id, $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST на update) for HTTP - 404
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByHttp()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST на update) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('bad data provided: not found tax inspection', $data['message']);
    }

    // Не владелец

    /**
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST на update) по HTTP
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * Important! update method is only for Ajax!
     */
    public function testUpdateCanNotBeAccessedByNotOwner()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(404);
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST на update) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('bad data provided: not found tax inspection', $data['message']);
    }

    // Владелец

    /**
     * Для Ajax
     * Отправка валидных данных /profile/legal-entity-tax-inspection/:id
     * (POST валидный на update) пользователем с ролью Verified Владельцем
     *
     *
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateWithValidPostForOwnerForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $issue_organisation = 'test post update legal entity tax inspection';

        $postData = [
            'issue_organisation' => $issue_organisation,
            'serial_number' => '12345678901234567890',
            'date_issued' => '2000-10-11',
            'issue_registrar' => 'issue registrar',
            'date_registered' => '2000-10-11',
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->entityManager->getRepository(LegalEntityTaxInspection::class)
            ->findOneBy(array('issueOrganisation' => $issue_organisation));

        #$this->assertEquals($issue_organisation, $legalEntityTaxInspection->getIssueOrganisation());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных /profile/legal-entity-tax-inspection/:id
     * (POST невалидный на update) пользователем с ролью Verified Владельцем
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwnerForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $issue_organisation = 'test post update legal entity bank detail';

        // Invalid POST
        $postData = [
            'issue_organisation' => $issue_organisation,
            'serial_number' => '12345678901234567890',
            'date_issued' => '2000-10-11',
            'issue_registrar' => 'issue registrar',
            'date_registered' => '2000-10-11',
            'csrf' => '123invalid456'
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->entityManager->getRepository(LegalEntityTaxInspection::class)
            ->findOneBy(array('issueOrganisation' => $issue_organisation));

        $this->assertNull($legalEntityTaxInspection);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
    }

    // *****************************
    // ******* method create *******
    // *****************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-tax-inspection
     * (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorised()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-tax-inspection', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-tax-inspection');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection
     * (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-tax-inspection', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity-tax-inspection
     * (POST на create) по HTTP - 404
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByHttp()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-tax-inspection', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection
     * (POST на create) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Не проходит. Разобраться!
     */
    public function testCreateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Недоступность /profile/legal-entity-tax-inspection
     * (POST валидный на create) по HTTP (404)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity-tax-inspection', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');
    }

    /**
     * Для Ajax
     * Доступность /profile/legal-entity-tax-inspection
     * (POST валидный на create) пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerifiedForAjax()
    {
        // Переорпеделяем is_uploaded_file
        $this->disablePhpUploadCapabilities();

        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $issue_organisation = 'test create legal entity bank detail';
        //Valid post
        $file = $this->getTestFilesParam();
        $postData = [
            'idLegalEntity' => $idLegalEntity,
            'issue_organisation' => $issue_organisation,
            'serial_number' => '12345678901234567890',
            'date_issued' => '2000-10-11',
            'issue_registrar' => 'issue registrar',
            'date_registered' => '2000-10-11',
            'file' => $file,
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->entityManager->getRepository(LegalEntityTaxInspection::class)
            ->findOneBy(array('issueOrganisation' => $issue_organisation));

        $this->assertEquals($issue_organisation, $legalEntityTaxInspection->getIssueOrganisation());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');

        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных /profile/legal-entity-tax-inspection
     * (POST невалидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithInvalidPostForVerifiedForAjax()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $issue_organisation = 'test create legal entity bank detail';
        //Invalid post
        $file = $this->getTestFilesParam();
        $postData = [
            'idLegalEntity' => $idLegalEntity,
            'issue_organisation' => $issue_organisation,
            'serial_number' => '12345678901234567890',
            'date_issued' => '2000-10-11',
            'issue_registrar' => 'issue registrar',
            'date_registered' => '2000-10-11',
            'file' => $file,
            'csrf' => '123invalid456'
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->entityManager->getRepository(LegalEntityTaxInspection::class)
            ->findOneBy(array('issueOrganisation' => $issue_organisation));

        $this->assertNull($legalEntityTaxInspection);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection');

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
    }

    // *****************************
    // ******* method delete *******
    // *****************************

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity-tax-inspection/'.$id);
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/legal-entity-tax-inspection/'.$id, $data['data']['redirectUrl']);
    }

    // Не владелец

    /**
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwner()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperator()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity-tax-inspection/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperatorForAjax()
    {
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Доступность /profile/legal-entity-tax-inspection/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Не проходит. Разобраться.
     */
    public function testDeleteForOwner()
    {
        $issue_organisation = 'nameForDelete';
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->createTestLegalEntityTaxInspection('test', $issue_organisation);
        $id = $legalEntityTaxInspection->getId();
        $idLegalEntity = $legalEntityTaxInspection->getLegalEntityDocument()->getLegalEntity()->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData);

        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->entityManager->getRepository(LegalEntityTaxInspection::class)
            ->findOneBy(array('issueOrganisation' => $issue_organisation));
        // Must be null (deleted)
        $this->assertNull($legalEntityTaxInspection);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertRedirectTo('/profile/legal-entity/'.$idLegalEntity);
    }

    /**
     * Для Ajax
     * Доступность /profile/legal-entity-tax-inspection/:id
     * (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * @TODO Не проходит. Разобраться!
     */
    public function testDeleteForOwnerForAjax()
    {
        $issue_organisation = 'nameForDelete';
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->testLegalEntityTaxInspection;
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->entityManager->getRepository(LegalEntityTaxInspection::class)
            ->findOneBy(array('issueOrganisation' => $issue_organisation));
        // Must be null (deleted)
        $this->assertNull($legalEntityTaxInspection);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertEquals('SUCCESS', $data['status']);
    }

    // *****************************
    // ***** method createForm *****
    // *****************************

    /**
     * Доступность /profile/legal-entity/:idLegalEntity/tax-inspection/create
     * (POST with hidden input name="_method" value="delete" на delete)
     * Virified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormForOwner()
    {
        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->testLegalEntity;
        $idLegalEntity = $legalEntity->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $this->dispatch('/profile/legal-entity/'.$idLegalEntity.'/tax-inspection/create');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('taxInspectionForm', $view_vars);
    }

    // *****************************
    // ***** method editForm *****
    // *****************************

    /**
     * Доступность /profile/legal-entity-tax-inspection/$id/edit
     * (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormForOwner()
    {
        $issue_organisation = 'nameForDelete';
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->createTestLegalEntityTaxInspection('test', $issue_organisation);
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id.'/edit');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('taxInspection', $view_vars);
        $this->assertArrayHasKey('taxInspectionForm', $view_vars);
    }

    // *****************************
    // ***** method deleteForm *****
    // *****************************

    /**
     * Доступность /profile/legal-entity-tax-inspection/$id/delete
     * (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormForOwner()
    {
        $issue_organisation = 'nameForDelete';
        /** @var LegalEntityTaxInspection $legalEntityTaxInspection */
        $legalEntityTaxInspection = $this->createTestLegalEntityTaxInspection('test', $issue_organisation);
        $id = $legalEntityTaxInspection->getId();

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $this->dispatch('/profile/legal-entity-tax-inspection/'.$id.'/delete');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-form-delete');
        // Что отдается в шаблон
        $this->assertArrayHasKey('taxInspection', $view_vars);
    }


    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var \Application\Entity\Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $login
     * @param string $name
     * @return LegalEntityTaxInspection
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createTestLegalEntityTaxInspection($login = 'test', $name = self::DEFAULT_DOCUMENT_NAME)
    {
        //clear all LegalEntityTaxInspection
        $removeLegalEntityTaxInspections = $this->entityManager->getRepository(LegalEntityTaxInspection::class)->findAll();
        foreach ($removeLegalEntityTaxInspections as $removeLegalEntityTaxInspection){
            /** @var LegalEntityTaxInspection $removeLegalEntityTaxInspection */
            $removeLegalEntityDocument = $removeLegalEntityTaxInspection->getLegalEntityDocument();
            $this->entityManager->remove($removeLegalEntityDocument);
            $this->entityManager->remove($removeLegalEntityTaxInspection);
        }

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $login));

        /** @var LegalEntityDocumentType $legalEntityDocumentType */
        $legalEntityDocumentType = $this->entityManager->getRepository(LegalEntityDocumentType::class)
            ->findOneBy(array('name' => 'tax_inspection'));

        // Create CivilLawSubject
        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern(true);
        $civilLawSubject->setCreated(new \DateTime());
        $civilLawSubject->setNdsType($ndsType);

        $this->entityManager->persist($civilLawSubject);

        // Create LegalEntity
        $legalEntity = new LegalEntity();
        $legalEntity->setCivilLawSubject($civilLawSubject);
        $legalEntity->setName('test name for legal entity');
        $legalEntity->setLegalAddress('test address');
        $legalEntity->setPhone('79000000000');
        $legalEntity->setInn('111111111111');
        $legalEntity->setKpp('222222222');
        $legalEntity->setOgrn('333333333333333');

        $civilLawSubject->setLegalEntity($legalEntity);

        $this->entityManager->persist($legalEntity);

        // Create LegalEntityDocument
        $legalEntityDocument = new LegalEntityDocument();
        $legalEntityDocument->setLegalEntity($legalEntity);
        $legalEntityDocument->setIsActive(true);
        $legalEntityDocument->setLegalEntityDocumentType($legalEntityDocumentType);

        $this->entityManager->persist($legalEntityDocument);

        //Create LegalEntityTaxInspection
        $legalEntityTaxInspection = new LegalEntityTaxInspection();
        $legalEntityTaxInspection->setSerialNumber('12345678901234567890');
        $legalEntityTaxInspection->setDateIssued(\DateTime::createFromFormat('!Y-m-d', '2000-10-11'));
        $legalEntityTaxInspection->setIssueOrganisation($name);
        $legalEntityTaxInspection->setIssueRegistrar('issue registrar');
        $legalEntityTaxInspection->setDateRegistered(\DateTime::createFromFormat('!Y-m-d', '2000-10-11'));
        $legalEntityTaxInspection->setLegalEntityDocument($legalEntityDocument);

        $legalEntityDocument->setLegalEntityTaxInspection($legalEntityTaxInspection);

        $this->entityManager->persist($legalEntityTaxInspection);

        $this->entityManager->flush();

        return $legalEntityTaxInspection;
    }

    private function getTestFilesParam()
    {
        $file_name = self::DEFAULT_FILE_NAME;

        // Valid params
        return [
            'name' => $file_name,
            'type' => 'image/png',
            'tmp_name' => "./module/Application/test/files/".$file_name,
            'error' => 0,
            'size' => filesize("./module/Application/test/files/".$file_name)
        ];
    }
}