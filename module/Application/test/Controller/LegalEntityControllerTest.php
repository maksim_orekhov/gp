<?php

namespace ApplicationTest\Controller;

use Application\Controller\LegalEntityController;
use Application\Entity\CivilLawSubject;
use Application\Entity\LegalEntity;
use Application\Entity\NdsType;
use Application\Form\LegalEntityForm;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\MessagePlugin;
use Core\Service\Base\BaseRoleManager;
use Core\Exception\LogicException;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Zend\Form\Element;
use CoreTest\ViewVarsTrait;

/**
 * Class LegalEntityControllerTest
 * @package ApplicationTest\Controller
 */
class LegalEntityControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// getList

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/legal-entity', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $this->dispatch('/profile/legal-entity', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /profile/legal-entity (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUser()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/legal-entity', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');
        // Что отдается в шаблон
        $this->assertArrayHasKey('legalEntities', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUserForAjax()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем, что
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $legalEntity = $data['data']['legalEntities'][0];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('LegalEntity collection', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('legalEntities', $data['data']);
        $this->assertArrayHasKey('paginator', $data['data']);
        $this->assertEquals('Test Company', $legalEntity['name']);
    }

    /////////////// get

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем, что
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity/'.$newTestLegalEntity->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(),
            null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Не Владелец

    /**
     * Неоступность /profile/legal-entity/id (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Ajax
     * Недоступность /profile/legal-entity/id (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(),
            null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Доступность /profile/legal-entity/id (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        // Что отдается в шаблон
        $this->assertArrayHasKey('legalEntity', $view_vars);
        $this->assertArrayHasKey('paymentMethods', $view_vars);
        $this->assertArrayHasKey('civil_law_subject_id', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        // Возвращается из шаблона
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity/id (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOwnerForAjax()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'get', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $naturalPerson = $data['data']['legalEntity'];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('LegalEntity single', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('legalEntity', $data['data']);
        $this->assertEquals('Test Company', $naturalPerson['name']);
    }

    /////////////// update

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity/id (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity/'.$newTestLegalEntity->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity/id (POST на update) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        // До валидации формы не дойдет
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertArrayHasKey('content', $view_vars);
        $this->assertEquals('Method not supported', $view_vars['content']);
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals('Page not found.', $view_vars['message']);
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id (POST на update) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Не владелец

    /**
     * Недоступность /profile/legal-entity/id (POST на update) по HTTP даже владельцу (метод только для Ajax)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNotOwner()
    {
        // Заходим как пользователь test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();

        // До валидации не дойдет. Метод update только дляAjax
        $postData = [
            'name' => 'Не важно'
        ];
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id (POST на update) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNotOwnerForAjax()
    {
        // Заходим какпользователь test2
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');
        // Проверяем
        $this->assertEquals('test2', $this->baseAuthManager->getIdentity());

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity('test2');

        // Разлогиниваемся
        $this->baseAuthManager->logout();

        // Заходим какпользователь test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');
        // Проверяем
        $this->assertEquals('test', $this->baseAuthManager->getIdentity());

        $new_name = 'New Test Company';
        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => $new_name, // меняем только название
            'kpp'       => '123456',
            'inn'       => '4217030520',
            'legal_address' => 'Ямского поля, дом 2',
            'nds_type'  => 2,
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');

        $this->assertRedirectTo('/not-authorized');

    }

    // Владелец

    /**
     * Для Ajax
     * Доступность /profile/legal-entity/id (POST валидный на update) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateWithValidPostForOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $old_nds_type = $newTestLegalEntity->getCivilLawSubject()->getNdsType()->getType();

        /** @var NdsType $ndsType */
        $newNdsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '0']);

        $new_name = 'New Test Company';

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'name' => $new_name, // меняем название
            'kpp'       => '123456',
            'inn'       => '4217030520',
            'legal_address' => 'Ямского поля, дом 2',
            'ogrn'      => '3456778',
            'phone'     => '+74951233456',
            'nds_type'  => $newNdsType->getType(), // меняем НДС
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity ->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntity $updatedTestLegalEntity */
        $updatedTestLegalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => $new_name));

        /** @var NdsType $updatedNdsType */
        $updatedNdsType = $updatedTestLegalEntity->getCivilLawSubject()->getNdsType();

        $this->assertEquals($new_name, $updatedTestLegalEntity->getName());
        $this->assertNotEquals((int)$old_nds_type, (int)$updatedNdsType->getType());
        $this->assertEquals((int)$newNdsType->getType(), (int)$updatedNdsType->getType());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных /profile/legal-entity/id
     * (POST невалидный на update) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $new_name = 'New Test Company';

        // Invalid POST
        $postData = [
            'name'      => $new_name,
            'kpp'       => '123456',
            'inn'       => '123456',
            'legal_address' => 'Ямского поля, дом 2',
            'csrf'      => '123invalid456' // csrf несуществующий
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntity $updatedTestLegalEntity */
        $updatedTestLegalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => $new_name));

        $this->assertNull($updatedTestLegalEntity);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /////////////// create

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorised()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/legal-entity (POST на create) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity (POST на create) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Access to the resource is denied to the user.', $data['message']);
    }

    /**
     * Недоступность /profile/legal-entity (POST валидный на create) по HTTP (404)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/legal-entity', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');
    }

    /**
     * Для Ajax
     * Доступность /profile/legal-entity (POST валидный на create) пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerifiedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $name = 'Number Two Company';

        $postData = [
            'name'      => $name,
            'kpp'       => '123456',
            'inn'       => '683103646744',
            'legal_address' => 'Ямского поля, дом 2',
            'nds_type'  => $ndsType->getType(),
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => $name));

        $this->assertEquals($name, $legalEntity->getName());
        $this->assertEquals($data['data']['legalEntity']['id'], $legalEntity->getId());
        $this->assertArrayHasKey('civilLawSubject', $data['data']);
        $this->assertEquals($ndsType, $legalEntity->getCivilLawSubject()->getNdsType());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');

        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных пользователем с ролью Verified /profile/legal-entity
     * (POST невалидный на create)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateWithInvalidPostForVerifiedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $name = 'Number Two Company';
        // Invalid POST
        $postData = [
            'name'      => $name,
            'kpp'       => '123456',
            'inn'       => '683103646744',
            'legal_address' => 'Ямского поля, дом 2',
            'csrf'      => '123invalid456' // csrf несуществующий и нет nds_type
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => $name));

        $this->assertNull($legalEntity);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('code', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /////////////// delete

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity/id (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity/'.$newTestLegalEntity->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Не владелец

    /**
     * Недоступность /profile/legal-entity/id (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /profile/legal-entity/id (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(),
            'POST', $postData, $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Владелец

    /**
     * Доступность /profile/legal-entity/id (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteForOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(), 'POST', $postData);

        /** @var LegalEntity $updatedLegalEntity */
        $legalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'Test Company'));

        // Must be null (deleted)
        $this->assertNull($legalEntity);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertRedirectTo('/profile/legal-entity');
    }

    /**
     * Для Ajax
     * Доступность /profile/legal-entity/id (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteForOwnerForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var LegalEntity $updatedLegalEntity */
        $legalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => 'Test Company'));

        // Must be null (deleted)
        $this->assertNull($legalEntity);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(LegalEntityController::SUCCESS_DELETE, $data['message']);
    }

    /////////////// editForm

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity/id/edit (GET на editFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/edit');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-edit');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity/'.$newTestLegalEntity->getId().'/edit');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id/edit (GET на editFormAction) по Ajax неавторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/edit',
            null, [], $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-edit');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id/edit (GET на editFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/edit',
            'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/legal-entity/id/edit (GET на editFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-edit');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_LEGAL_ENTITY_MESSAGE, $view_vars['message']);
    }

    /**
     * Недоступность /profile/legal-entity/id/edit (GET на editFormAction) не Владельцу (500)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-edit');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_LEGAL_ENTITY_MESSAGE, $view_vars['message']);
    }

    /**
     * Доступность /profile/legal-entity/id/edit (GET на editFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('legalEntity', $view_vars);
        $this->assertArrayHasKey('legalEntityForm', $view_vars);
        $this->assertTrue($view_vars['legalEntityForm'] instanceof LegalEntityForm);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertFalse($view_vars['is_operator']);
    }

    /**
     * Редактирование NaturalPerson /profile/legal-entity/id/edit (POST валидный на editFormAction)
     * пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testEditFormWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $old_nds_type = $newTestLegalEntity->getCivilLawSubject()->getNdsType()->getType();

        /** @var NdsType $newNdsType */
        $newNdsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '0']);

        $new_name = 'New Test Company';

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'name' => $new_name, // меняем только название
            'kpp'       => '123456',
            'inn'       => '683103646744',
            'legal_address' => 'Ямского поля, дом 2',
            'nds_type'          => $newNdsType->getType(),
            'csrf'      => $csrf
        ];

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/edit',
            'POST', $postData);

        /** @var LegalEntity $updatedLegalEntity */
        $updatedLegalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => $new_name));

        $this->assertEquals($new_name, $updatedLegalEntity->getName());

        /** @var NdsType $ndsType */
        $ndsType = $updatedLegalEntity->getCivilLawSubject()->getNdsType();

        $this->assertEquals($newNdsType->getType(), $ndsType->getType());
        $this->assertNotEquals($old_nds_type, $ndsType->getType());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-edit');
        $this->assertRedirectTo('/profile/legal-entity');
    }

    /**
     * Отправка заведомо невалидных данных /profile/legal-entity/id/edit
     * (POST невалидный на editFormAction) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $nds_type = $newTestLegalEntity->getCivilLawSubject()->getNdsType()->getType();

        $new_name = 'New Test Company';

        // Invalid POST
        $postData = [
            'name'      => $new_name,
            'kpp'       => '123456',
            'inn'       => '123456',
            'legal_address' => 'Ямского поля, дом 2',
            #'nds_type'  => $nds_type,
            'csrf'      => '123invalid456' // csrf несуществующий
        ];

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/edit',
            'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var LegalEntity $updatedLegalEntity */
        $updatedLegalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => $new_name));

        $this->assertNull($updatedLegalEntity);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('legalEntity', $view_vars);
        $this->assertArrayHasKey('legalEntityForm', $view_vars);
        $this->assertTrue($view_vars['legalEntityForm'] instanceof LegalEntityForm);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertFalse($view_vars['is_operator']);

        // Ошибки валидации
        $validation_messages = $view_vars['legalEntityForm']->getMessages();
        $this->assertArrayHasKey('inn', $validation_messages);
        $this->assertArrayHasKey('nds_type', $validation_messages);
        $this->assertArrayHasKey('csrf', $validation_messages);
    }

    /////////////// createForm

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity/create (GET на createFormAction)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/legal-entity/create');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-create');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity/create');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/create (GET на createFormAction) по Ajax
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/create', 'GET', [], $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-create');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/create (GET на createFormAction) по Ajax
     * авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/create (GET на createFormAction) по Ajax
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Verified пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('ERROR_ACCESS_DENIED', $data['code']);
        $this->assertEquals('Access to the resource is denied to the user.', $data['message']);
    }

    /**
     * Недоступность /profile/legal-entity/create (GET на createFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/profile/legal-entity/create', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-create');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Доступность /profile/legal-entity/create (GET на createFormAction)
     * пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormCanBeAccessedByVerified()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/legal-entity/create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('legalEntityForm', $view_vars);
        $this->assertTrue($view_vars['legalEntityForm'] instanceof LegalEntityForm);
        // Возвращается из шаблона
        $this->assertQuery('#legal-entity-create-form');
    }

    /**
     * Создание LegalEntity /profile/legal-entity/create (POST валидный на create)
     * пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        $name = 'New Test Company';

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'name'      => $name,
            'kpp'       => '123456',
            'legal_address' => 'Ямского поля, дом 2',
            'inn'               => '683103646744',
            'nds_type'          => $ndsType->getType(),
            'csrf'      => $csrf
        ];

        $this->dispatch('/profile/legal-entity/create','POST', $postData);

        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => $name));

        $this->assertEquals($name, $legalEntity->getName());
        $this->assertEquals($ndsType, $legalEntity->getCivilLawSubject()->getNdsType());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-create');
        $this->assertRedirectTo('/profile');
    }

    /**
     * Создание LegalEntity
     * Отправка заведомо невалидных данных /profile/legal-entity/create
     * (POST невалидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testCreateFormWithInvalidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '18%']);

        $name = 'Number Two Company';
        // Invalid POST
        $postData = [
            'name'      => $name,
            'kpp'       => '123456',
            'legal_address' => 'Ямского поля, дом 2',
            'inn'       => '683103646744',
            'nds_type'  => $ndsType->getType(),
            'csrf'      => '123invalid456' // csrf несуществующий
        ];

        $this->dispatch('/profile/legal-entity/create', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var LegalEntity $legalEntity */
        $legalEntity = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(array('name' => $name));

        $this->assertNull($legalEntity);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('legalEntityForm', $view_vars);
        $this->assertTrue($view_vars['legalEntityForm'] instanceof LegalEntityForm);
        // Возвращается из шаблона
        $this->assertQuery('#legal-entity-create-form');
    }

    /////////////// deleteForm

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity/id/delete (GET на deleteFormAction)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorised()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/delete');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-delete');
        $this->assertRedirectTo('/login?redirectUrl=/profile/legal-entity/'.$newTestLegalEntity->getId().'/delete');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/id/delete (GET на deleteFormAction)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/delete',
            'GET', [], $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-delete');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/create (GET на deleteFormAction) по Ajax
     * авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/delete',
            'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-delete');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/legal-entity/id/delete (GET на deleteFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-delete');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_LEGAL_ENTITY_MESSAGE, $view_vars['message']);
    }

    /**
     * Недоступность /profile/legal-entity/id/delete (GET на deleteFormAction) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNotOwner()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-delete');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_FOUND_SOLE_PROPRIETOR_MESSAGE, $view_vars['message']);
    }

    /**
     * Доступность /profile/legal-entity/id/delete (GET на deleteFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testDeleteFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $this->dispatch('/profile/legal-entity/'.$newTestLegalEntity->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-delete');
        // Что отдается в шаблон
        $this->assertArrayHasKey('legalEntity', $view_vars);
    }

    /////////////// formInit (экшен только для Ajax!)

    // Неавторизованный

    /**
     * Недоступность /profile/legal-entity/form-init (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/legal-entity/form-init', 'GET');

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-init');
    }

    /**
     * Для Ajax
     * Недоступность /profile/legal-entity/form-init (GET на formInitAction)
     * не авторизованному пользователю по Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     *
     * Теперь разроешаем не авторизованным пользователям
     */
    public function testFormInitCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('formInit', $data['message']);
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity/form-init (GET на formInitAction)
     * авторизованному с ролью Verified
     * Должен отдать коллекцию LegalEntities, user и нулевые allowedUser и legalEntity
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithoutParamsForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        // Создаем LegalEntity, т.к. в системе по умолчанию их нет
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'naturalPerson' соотвествует нулевое значение
        $this->assertNull($data['data']['legalEntity']);
        // а ключу коллекции 'allowedLegalEntities' - не нулевой массив
        $this->assertGreaterThan(0, count($data['data']['allowedLegalEntities']));
    }

    /**
     * Ajax
     * Доступность /profile/legal-entity/form-init (GET на formInitAction)
     * авторизованному с ролью Verified
     * Должен отдать коллекцию LegalEntities, user и нулевые allowedUser и legalEntity
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithInvalidParamsForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        // Создаем LegalEntity, т.к. в системе по умолчанию их нет
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        //Invalid params
        $getData = [
            'idLegalEntity' => 99999999,
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'naturalPerson' соотвествует нулевое значение
        $this->assertNull($data['data']['legalEntity']);
        // а ключу коллекции 'allowedLegalEntities' - не нулевой массив
        $this->assertGreaterThan(0, count($data['data']['allowedLegalEntities']));
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/legal-entity/form-init (GET на formInitAction)
     * с параметром чужого объекта (idLegalEntity)
     * Должен отдать коллекции доступных объектов и нулевые объекты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithAlienParamsForAjax()
    {
        // Создаем LegalEntity, т.к. в системе по умолчанию их нет
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        //Invalid params
        $getData = [
            'idLegalEntity' => $newTestLegalEntity->getId()
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test2', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'legalEntity' соотвествует нулевое значение
        $this->assertNull($data['data']['legalEntity']);
        // а ключу коллекции 'allowedLegalEntities' - нулевой массив (изначально в системе нет LegalEntity для test2)
        $this->assertGreaterThan(0, count($data['data']['allowedLegalEntities']));
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/legal-entity/form-init (GET на formInitAction)
     * с валидным параметром Владельцу объектов, id которого передается (idLegalEntity)
     * Должен отдать коллекции и НЕнулевой объект LegalEntity
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group legal-entity
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithValidParamsForAjax()
    {
        // Создаем LegalEntity, т.к. в системе по умолчанию их нет
        /** @var LegalEntity $newTestLegalEntity */
        $newTestLegalEntity = $this->createNewTestLegalEntity();
        // Проверяем
        $this->assertEquals('Test Company', $newTestLegalEntity->getName());

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        //Invalid params
        $getData = [
            'idLegalEntity' => $newTestLegalEntity->getId()
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/legal-entity/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertNotNull($data['data']['user']);
        $this->assertEquals('test2', $data['data']['user']['login']);
        $this->assertNull($data['data']['allowedUser']);
        // Проверяем, что ключу 'legalEntity' соотвествует нулевое значение
        $this->assertNull($data['data']['legalEntity']);
        // а ключу коллекции 'allowedLegalEntities' - нулевой массив (изначально в системе нет LegalEntity для test2)
        $this->assertGreaterThan(0, count($data['data']['allowedLegalEntities']));
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $login
     * @return LegalEntity
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @throws \Exception
     */
    private function createNewTestLegalEntity($login = 'test')
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)
            ->findOneBy(array('login' => $login));

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(array('name' => '18%'));

        // Create CivilLawSubject
        $civilLawSubject = new CivilLawSubject();
        $civilLawSubject->setUser($user);
        $civilLawSubject->setIsPattern(true);
        $civilLawSubject->setCreated(new \DateTime());
        $civilLawSubject->setNdsType($ndsType);

        $this->entityManager->persist($civilLawSubject);

        // Create LegalEntity

        $legalEntity = new LegalEntity();
        $legalEntity->setCivilLawSubject($civilLawSubject);
        $legalEntity->setName('Test Company');
        $legalEntity->setKpp('123456');
        $legalEntity->setInn('683103646744');
        $legalEntity->setLegalAddress('Ямского поля, дом 2');

        $civilLawSubject->setLegalEntity($legalEntity);

        $this->entityManager->persist($legalEntity);

        $this->entityManager->flush();

        return $legalEntity;
    }
}