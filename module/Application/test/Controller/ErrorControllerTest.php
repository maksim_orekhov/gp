<?php

namespace ApplicationTest\Controller;

use Application\Controller\ErrorController;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Application\Service\BankClient\BankClientManager;

/**
 * Class ErrorControllerTest
 * @package ApplicationTest\Controller
 */
class ErrorControllerTest extends AbstractHttpControllerTestCase
{
    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var BankClientManager
     */
    public $bankClientManager;


    /**
     * @var string
     */
    public $main_upload_folder;


    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));
        // Отключаем отправку уведомлений на email
        $this->resetConfigParameters();
        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();
        parent::tearDown();
        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;
        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /////////////// проверка при всех переданных полях

    /**
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @group error
     * @throws \Exception
     */
    public function testSendAnyErrorForAllJsonData()
    {
        $isXmlHttpRequest = true;
        $data = [
            'message' => 'test',
            'code' => 'test'
        ];
        $this->getRequest()->setContent(json_encode($data));
        $this->dispatch('/error/send', 'POST', $data, $isXmlHttpRequest);
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(ErrorController::class);
        $this->assertMatchedRouteName('error');

    }

    //////// Проверка, если не все поля переданы

    /**
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @group error
     * @throws \Exception
     */
    public function testSendAnyErrorForNotAllJsonData()
    {
        $isXmlHttpRequest = true;

        $data = [
            'message' => 'test'
        ];
        $this->getRequest()->setContent(json_encode($data));
        $this->dispatch('/error/send', 'POST', $data, $isXmlHttpRequest);
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(ErrorController::class);
        $this->assertMatchedRouteName('error');

    }

    //////// Проверка, если вдруг в message прийдет Array , чтобы все обработалось, и отослалось

    /**
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group error
     * @throws \Exception
     */
    public function testSendAnyErrorForAllJsonDataWithArray()
    {
        $isXmlHttpRequest = true;

        $data = [
            'code' => 'test',
            'message' => ['test' => 'super',
                'test2' => 'super2'
            ]
        ];
        $this->getRequest()->setContent(json_encode($data));
        $this->dispatch('/error/send', 'POST', $data, $isXmlHttpRequest);
        $this->assertResponseStatusCode(200);
        $this->assertControllerName(ErrorController::class);
        $this->assertMatchedRouteName('error');

    }


}