<?php

namespace ApplicationTest\Controller;

use Application\Controller\NaturalPersonPassportController;
use Application\Entity\NaturalPerson;
use Application\Entity\NaturalPersonDocument;
use Application\Entity\NaturalPersonDocumentType;
use Application\Entity\NaturalPersonPassport;
use Application\Form\NaturalPersonPassportForm;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseRoleManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Core\Service\ORMDoctrineUtil;
use ModuleFileManager\Entity\File;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use CoreTest\ViewVarsTrait;

/**
 * Class NaturalPersonPassportControllerTest
 * @package ApplicationTest\Controller
 */
class NaturalPersonPassportControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_NATURAL_PERSON_LAST_NAME = 'Тестов';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Application\Service\CivilLawSubject\NaturalPersonDocumentManager
     */
    private $naturalPersonDocumentManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->naturalPersonDocumentManager = $serviceManager->get(NaturalPersonDocumentManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// Collection

    // Неавторизованный

    /**
     * Недоступность /natural-person-passport не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/natural-person-passport', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertControllerName(NaturalPersonPassportController::class);
        $this->assertMatchedRouteName('natural-person-passport');
        $this->assertRedirectTo('/login?redirectUrl=/natural-person-passport');
    }

    /**
     * Для Ajax
     * Недоступность /natural-person-passport не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person-passport', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonPassportController::class);
        $this->assertMatchedRouteName('natural-person-passport');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/natural-person-passport', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Не доступность /natural-person-passport авторизованному без роли Оператора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByVerifiedUser()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/natural-person-passport', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonPassportController::class);
        $this->assertMatchedRouteName('natural-person-passport');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Не доступность /natural-person-passport авторизованному без роли Оператора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByVerifiedUserForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person-passport', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonPassportController::class);
        $this->assertMatchedRouteName('natural-person-passport');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /natural-person-passport Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/natural-person-passport', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonPassportController::class);
        $this->assertMatchedRouteName('natural-person-passport');
        // Что отдается в шаблон
        $this->assertArrayHasKey('passports', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-passport-filter-form');
    }

    /**
     * Для Ajax
     * Доступность /natural-person-passport Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person-passport
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person-passport', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonPassportController::class);
        $this->assertMatchedRouteName('natural-person-passport');

        $data = json_decode($this->getResponse()->getContent(), true);
        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Доступность /profile/natural-person/:idNaturalPerson/passport/create (GET на createFormAction) Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/passport/create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonPassportController::class);
        $this->assertControllerClass('NaturalPersonPassportController');
        $this->assertMatchedRouteName('user-profile/natural-person-passport-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('passportForm', $view_vars);
        $this->assertTrue($view_vars['passportForm'] instanceof NaturalPersonPassportForm);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-passport-create-form');
    }

    /**
     * Доступность /profile/natural-person-passport/:idPassport/edit (GET на editFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testEditFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        /** @var NaturalPersonPassport $passport */
        $passport = $this->createTestPassport($naturalPerson);

        $this->dispatch('/profile/natural-person-passport/'.$passport->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonPassportController::class);
        $this->assertControllerClass('NaturalPersonPassportController');
        $this->assertMatchedRouteName('user-profile/natural-person-passport-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('passport', $view_vars);
        $this->assertArrayHasKey('passportForm', $view_vars);
        $this->assertInstanceOf( NaturalPersonPassportForm::class, $view_vars['passportForm']);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-passport-edit-form');
    }

    /**
     * Доступность /profile/natural-person-passport/:idDriverLicense/delete (GET на deleteFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        /** @var NaturalPersonPassport $passport */
        $passport = $this->createTestPassport($naturalPerson);

        $this->dispatch('/profile/natural-person-passport/'.$passport->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonPassportController::class);
        $this->assertControllerClass('NaturalPersonPassportController');
        $this->assertMatchedRouteName('user-profile/natural-person-passport-form-delete');
        // Что отдается в шаблон
        $this->assertArrayHasKey('passport', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-passport-delete-form');
    }

    /**
     * @param NaturalPerson $naturalPerson
     * @return NaturalPersonPassport
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createTestPassport(NaturalPerson $naturalPerson)
    {
        /** @var NaturalPersonDocument $document */
        $document = $this->createTestDocument();
        /** @var File $file */
        $file = $this->createTestFile();

        $passport = new NaturalPersonPassport();
        $passport->setPassportSerialNumber('123456789');
        $passport->setDateIssued(new \DateTime());
        $passport->setDateExpired(new \DateTime());
        $passport->setNaturalPersonDocument($document);
        $passport->addFile($file);

        $document->setNaturalPersonPassport($passport);

        $this->entityManager->persist($document);
        $this->entityManager->persist($passport);

        $document->setNaturalPerson($naturalPerson);

        $this->entityManager->persist($naturalPerson);

        $this->entityManager->flush();

        return $passport;
    }

    /**
     * @return NaturalPersonDocument
     */
    private function createTestDocument()
    {
        $document = new NaturalPersonDocument();

        /** @var NaturalPersonDocumentType $documentType */
        $documentType = $this->entityManager->getRepository(NaturalPersonDocumentType::class)
            ->findOneBy(['name' => 'passport']);

        $document->setNaturalPersonDocumentType($documentType);
        $document->setIsActive(1);

        return $document;
    }

    /**
     * @return File
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createTestFile()
    {
        $file = new File();
        $file->setCreated(new \DateTime());
        $file->setName('test_passport_file');
        $file->setType('image');
        $file->setOriginName('test_passport_file_origin');
        $file->setPath('/natural-passport');
        $file->setSize(123456);

        $this->entityManager->persist($file);
        $this->entityManager->flush($file);

        return $file;
    }

    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}