<?php

namespace ApplicationTest\Controller;

use Application\Controller\DealController;
use Application\Entity\Dispute;
use Application\Entity\RepaidOverpay;
use Application\Entity\WarrantyExtension;
use Application\Form\RepaidOverpayForm;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Exception\LogicException;
use Core\Service\Base\BaseRoleManager;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Form\DealSearchForm;
use Application\Form\DisputeForm;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\Deal;
use Application\Entity\User;
use Application\Entity\Payment;
use Application\Entity\SystemPaymentDetails;
use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethod;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealAgentManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\BankClient\BankClientManager;
use Application\Service\Parser\BankClientParser;
use Application\Controller\BankClientController;
use Application\Service\Deal\DealPolitics;
use CoreTest\ViewVarsTrait;

/**
 * Class DealControllerTest
 * @package ApplicationTest\Controller
 */
class DealHistoryTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const MINIMUM_DELIVERY_PERIOD = 7;
    const TEXT_DEAL_NAME = 'Сделка Фикстура';
    const TEXT_DISPUTE_DEAL_NAME = 'Сделка Debit Matched'; // Заменить использование на DEAL_DEBIT_MATCHED_NAME
    const DEAL_DEBIT_MATCHED_NAME = 'Сделка Debit Matched';
    const DEAL_WITH_OVERPAY_NAME = 'Сделка Переплата';
    const DEAL_WITH_3_DAY_RENEWAL = 'Сделка Спор закрыт Оператор с продлением 3 дня';
    const PERIOD_DAY_MINUS = 9;
    const DAYS_FOR_RENEWAL = 1;
    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var BankClientManager
     */
    public $bankClientManager;

    /**
     * @var BankClientParser
     */
    private $bankClientParser;

    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * @var string
     */
    public $main_upload_folder;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;


    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        // Отключаем отправку уведомлений на email
        $this->resetConfigParameters();

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->dealAgentManager = $serviceManager->get(DealAgentManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->paymentManager = $serviceManager->get(PaymentManager::class);
        $this->bankClientManager = $serviceManager->get(\Application\Service\BankClient\BankClientManager::class);
        $this->bankClientParser = $serviceManager->get(BankClientParser::class);
        $this->dealPolitics = $serviceManager->get(DealPolitics::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }




    /**
     * НЕ Доступность /deal/show/id участнику Unverified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testShowHistoryActionForUnVerified()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Unverified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (show)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');


        $this->assertResponseStatusCode(302);

    }
    /**
     * Доступность /deal/show/id участнику сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testShowHistoryActionForVerified()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (show)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/show');
        $this->assertQuery('.Content .DealShowPage');

        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertArrayHasKey('deal_history', $view_vars);
        $this->assertArrayHasKey('dealFileUploadForm', $view_vars);
        $this->assertArrayHasKey('trackingNumberForm', $view_vars);
        $this->assertArrayHasKey('dealCommentForm', $view_vars);
        $this->assertArrayHasKey('deal_operators_comments', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertFalse($view_vars['is_operator']);
    }


    public function testShowHistoryActionForAvailabilityEditedOnceArray()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ
        // Основные входные данные
        $deal_name = 'Сделка для unit тестов (show)';
        $deal_role = 'customer';
        $amount = 200000;
        // Crete and get Deal
        $deal = $this->createTestDeal($deal_name, $deal_role, $amount);
        // Проверяем
        $this->assertEquals($this->user->getId(), $deal->getCustomer()->getUser()->getId());
        // END СОЗДАЕМ ТЕСТОВУЮ СДЕЛКУ

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/show');
        $this->assertQuery('.Content .DealShowPage');


        $this->assertArrayHasKey('deal_history', $view_vars);
        //в этой сделке изменений небыло. ничего и не прийдет
        $this->assertNotTrue(end($view_vars['deal_history'])['code'] == 'EDITED_FOR_CUSTOMER');

        $this->assertFalse($view_vars['is_operator']);
    }

    /**
     * Сделка Спор закрыт Оператор с продлением 3 дня
     * @throws \Exception
     */
    public function testShowHistoryActionForAlmostOverdueDeal()
    {
        // Присваиваем нужную роль и залогиниваемся
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_3_DAY_RENEWAL]);
        /** @var Payment $payment */
        $payment = $this->entityManager->getRepository(Payment::class)
        ->findOneBy(['deal' => $deal]);
        /** @var Dispute $dispute */
        $dispute=$deal->getDispute();
        /** @var WarrantyExtension $warrantyExtension */
        $warrantyExtension = $this->entityManager->getRepository(WarrantyExtension::class)
            ->findOneBy(['dispute' => $dispute]);
        /** @var \DateTime $currentDate */
        $currentDate=new \DateTime();
        $currentDate->modify('-'.self::PERIOD_DAY_MINUS.' days');
        $warrantyExtension->setDays(self::DAYS_FOR_RENEWAL);
        $payment->setDeFactoDate($currentDate);
        $this->entityManager->persist($payment);
        $this->entityManager->persist($warrantyExtension);
        $this->entityManager->flush();


        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/show');
        $this->assertQuery('.Content .DealShowPage');
        $dealHistory = $view_vars['deal_history'];
        $lastStatus=end($dealHistory);
        $this->assertArrayHasKey('deal_history', $view_vars);
        //олжен прийти код, что остался один день
        $this->assertTrue($lastStatus['code']==='DEADLINE_EXPIRED_IN_ONE_DAY_FOR_OPERATOR');

    }
    /**
     * Доступность /deal/overpay/id Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanBeAccessedByOperator()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');

        $this->assertArrayHasKey('repaidOverpayForm', $view_vars);
        $this->assertInstanceOf(RepaidOverpayForm::class, $view_vars['repaidOverpayForm']);
        $this->assertEquals($deal->getId(), (int)$view_vars['deal']['id']);
        $this->assertArrayHasKey('overpaid_amount', $view_vars);
        $this->assertArrayHasKey('expected_amount', $view_vars);
        $this->assertEquals($payment->getExpectedValue(), $view_vars['expected_amount']);
        $this->assertArrayHasKey('incomingTransactions', $view_vars);
        $this->assertGreaterThan(0, count($view_vars['incomingTransactions']));
        $this->assertInternalType('array', $view_vars['incomingTransactions'][0]);
    }

    /**
     * Ajax
     * Доступность /deal/overpay/id Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanBeAccessedByOperatorForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/overpay/'.$deal->getId(), 'get', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Overpay', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals($deal->getId(), (int)$data['data']['deal']['id']);
        $this->assertArrayHasKey('overpaid_amount', $data['data']);
        $this->assertArrayHasKey('expected_amount', $data['data']);
        $this->assertEquals($payment->getExpectedValue(), $data['data']['expected_amount']);
        $this->assertArrayHasKey('incomingTransactions', $data['data']);
        $this->assertGreaterThan(0, count($data['data']['incomingTransactions']));
        $this->assertInternalType('array', $data['data']['incomingTransactions'][0]);
    }

    /**
     * Неоступность /deal/overpay/id для сделки без переплаты (Оператору)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanNotBeAccessedForDealWithoutOverpay()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */ // Получаем сделку без преплаты!
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DEBIT_MATCHED_NAME]);

        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
    }

    /**
     * Ajax
     * Неоступность /deal/overpay/id для сделки без переплаты (Оператору)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     *
     * @expectedExceptionMessage Deal is not overpaid
     */
    public function testOverpayCanNotBeAccessedForDealWithoutOverpayForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */ // Получаем сделку без преплаты!
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DEBIT_MATCHED_NAME]);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/overpay/'.$deal->getId(), 'get', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        #$this->assertEquals('Deal is not overpaid', $data['message']);
    }

    /**
     * Валидный POST на /deal/overpay/id Оператором
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayWithValidPostByOperator()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id'   => $deal->getId(),
            'csrf'      => $csrf
        ];

        $this->dispatch('/deal/overpay/'.$deal->getId(), 'POST', $postData);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Поверяем, что создалась платёжка на выплату
        /** @var RepaidOverpay $repaidOverpay */
        $repaidOverpay = $payment->getRepaidOverpay();

        $is_outgoing_payment_overpaid = false;
        /** @var PaymentOrder $paymentOrder */
        foreach ($repaidOverpay->getPaymentOrders() as $paymentOrder) {

            /** @var BankClientPaymentOrder $bankClientPaymentOrder */
            $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();

            $purpose = $bankClientPaymentOrder->getPaymentPurpose();

            if (strpos($purpose, PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY]) !== false) {

                $is_outgoing_payment_overpaid = true;
            }
        }

        // Проверяем, что outgoing PaymentOverpaid платёжка создалась
        $this->assertTrue($is_outgoing_payment_overpaid);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
    }

    /**
     * Ajax
     * Валидный POST на /deal/overpay/id Оператором
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayWithValidPostByOperatorForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'deal_id'   => $deal->getId(),
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/overpay/'.$deal->getId(), 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Поверяем, что создалась платёжка на выплату
        /** @var RepaidOverpay $repaidOverpay */
        $repaidOverpay = $payment->getRepaidOverpay();

        $is_outgoing_payment_overpaid = false;
        /** @var PaymentOrder $paymentOrder */
        foreach ($repaidOverpay->getPaymentOrders() as $paymentOrder) {

            /** @var BankClientPaymentOrder $bankClientPaymentOrder */
            $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();

            $purpose = $bankClientPaymentOrder->getPaymentPurpose();

            if (strpos($purpose, PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY]) !== false) {

                $is_outgoing_payment_overpaid = true;
            }
        }

        // Проверяем, что outgoing PaymentOverpaid платёжка создалась
        $this->assertTrue($is_outgoing_payment_overpaid);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Платёжка на возврат переплаты для сделки ' .$deal->getNumber(). ' сформирована!', $data['message']);
    }

    /**
     * Неоступность /deal/overpay/id при наличии у сделки RepaidOverpay (Оператору)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     */
    public function testOverpayCanNotBeAccessedForDealWithoutRepaidOverpay()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */ // Получаем сделку без преплаты!
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        // Создаем возврат переплаты
        $this->dealManager->overpaidDealsProcessing($deal);

        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
    }

    /**
     * Ajax
     * Неоступность /deal/overpay/id при наличии у сделки RepaidOverpay (Оператору)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     *
     * @expectedExceptionMessage Deal is not overpaid
     */
    public function testOverpayCanNotBeAccessedForDealWithoutRepaidOverpayForAjax()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        /** @var Deal $deal */ // Получаем сделку без преплаты!
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DEBIT_MATCHED_NAME]);

        // Создаем возврат переплаты
        $this->dealManager->overpaidDealsProcessing($deal);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/overpay/'.$deal->getId(), 'get', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        #$this->assertEquals('RepaidOverpay already done', $data['message']);
    }

    // @TODO Отправка POST из формы на создание возарата переплаты (RepaidOverpay)


    /**
     * Доступность формы /deal/$id/dispute/create Заказчику
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     */
    public function testCreateFormActionCanBeAccessedByCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DISPUTE_DEAL_NAME));

        // Присваиваем нужную роль и залогиниваемся
        $this->login('test');
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/'.$deal->getId().'/dispute-create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DealController::class);
        $this->assertControllerClass('DealController');
        $this->assertMatchedRouteName('deal/dispute-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertArrayHasKey('disputeForm', $view_vars);
        $this->assertTrue($view_vars['disputeForm'] instanceof DisputeForm);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-create-form');
    }


    /**
     * @param Deal $deal
     * @param Payment $payment
     * @param string $path
     * @return BankClientPaymentOrder
     */
    private function createPaymentOrderFromFile(Deal $deal, Payment $payment, string $path)
    {
//        /** @var PaymentMethodBankTransfer $customerBankTransfer */ // Реквизиты customer'а
//        $customerBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
//            ->findOneBy(array('paymentRecipientName' => 'Тест Тестов'));

        /** @var SystemPaymentDetails $systemPaymentDetails */ // Реквизиты GP
        $systemPaymentDetails = $this->entityManager->getRepository(SystemPaymentDetails::class)
            ->findOneBy(array('name' => 'ООО "Гарант Пэй"'));

        // Creates file if not exists
        $file_with_payment_order = fopen($path, "w") or die("Не удается создать файл");
        // Write file header data
        fwrite($file_with_payment_order, mb_convert_encoding(
                BankClientManager::FILE_HEADER,
                BankClientController::ORIGINAL_ENCODING, 'UTF-8')
        );
        // Main body of PaymentOrder
        $currentDate = new \DateTime();
        $txt = "СекцияДокумент=".BankClientManager::DOCUMENT_TYPE.PHP_EOL;
        $txt .= "Номер=".$deal->getId().PHP_EOL;
        $txt .= "Дата=".$currentDate->format('d.m.Y').PHP_EOL;
        $txt .= "ДатаСписано=".$currentDate->format('d.m.Y').PHP_EOL;
        $txt .= "Сумма=".$payment->getExpectedValue().PHP_EOL;
        $txt .= "ПлательщикСчет=40702810090030001128".PHP_EOL;
        $txt .= "Плательщик=Тест Тестов".PHP_EOL;
        $txt .= "ПлательщикИНН=".PHP_EOL;
        $txt .= "Плательщик1=Тест Тестов".PHP_EOL;
        $txt .= "ПлательщикКПП=".PHP_EOL;
        $txt .= "ПлательщикБанк1=ВТБ".PHP_EOL;
        $txt .= "ПлательщикБИК=044525201".PHP_EOL;
        $txt .= "ПлательщикКорсчет=30101810900000000790".PHP_EOL;
        $txt .= "ПолучательСчет=".$systemPaymentDetails->getAccountNumber().PHP_EOL;
        $txt .= "Получатель=ИНН ".$systemPaymentDetails->getInn()." ".$systemPaymentDetails->getPaymentRecipientName().PHP_EOL;
        $txt .= "ПолучательИНН=".$systemPaymentDetails->getInn().PHP_EOL;
        $txt .= "Получатель1=".$systemPaymentDetails->getPaymentRecipientName().PHP_EOL;
        $txt .= "ПолучательКПП=".$systemPaymentDetails->getKpp().PHP_EOL;
        $txt .= "ПолучательБанк1=".$systemPaymentDetails->getBank().PHP_EOL;
        $txt .= "ПолучательБИК=".$systemPaymentDetails->getBik().PHP_EOL;
        $txt .= "ПолучательКорсчет=".$systemPaymentDetails->getCorrAccountNumber().PHP_EOL;
        $txt .= "НазначениеПлатежа=".BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY.$deal->getId().PHP_EOL;
        $txt .= "ВидОплаты=01".PHP_EOL;
        $txt .= "Очередность=5".PHP_EOL;
        // Write main body
        fwrite($file_with_payment_order, mb_convert_encoding($txt, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
        // Write SECTION_END_POINT
        fwrite($file_with_payment_order, mb_convert_encoding(BankClientParser::SECTION_END_POINT.PHP_EOL,
            BankClientController::ORIGINAL_ENCODING, 'UTF-8'));

        // Генерируем файл с правильным параметром НазначениеПлатежа
        // Get data as array
        $payment_order_data = $this->bankClientParser->execute($path);
        // Create PaymentOrder
        /** @var BankClientPaymentOrder $paymentOrder */
        $paymentOrder = $this->bankClientManager->createNewPaymentOrderFromArray(
            $payment_order_data[0]
        );

        $this->entityManager->flush();

        return $paymentOrder;
    }

    /**
     * @param $login
     * @param $email
     * @param $phone
     * @param $password
     * @return int|null
     */
    private function createNewUser($login, $email, $phone, $password)
    {
        $data   = [
            'login'     => $login,
            'phone'     => $phone,
            'email'     => $email,
            'password'  => $password,
        ];

        return $this->userManager->addNewUser($data);
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $deal_name
     * @param string $deal_role
     * @param $amount
     * @param bool $confirmed
     * @return Deal
     */
    private function createTestDeal(string $deal_name, string $deal_role, $amount, $confirmed=false)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $data = [
            'name'                      => $deal_name,
            'amount'                    => $amount,
            'deal_role'                 => $deal_role,
            'deal_civil_law_subject'    => $this->user->getCivilLawSubjects()->last()->getId(),
            'payment_method'            => '',
            'counteragent_email'        => 'test2@simple-technology.ru',
            'deal_type'                 => $dealType->getId(),
            'fee_payer_option'          => $feePayerOption->getId(),
            'addon_terms'               => 'Дополнительные условия',
            'delivery_period'           => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm'        => 0
        ];

        // Try to register dill
        // Распределение ролей в сделке взависимости от выбранной инициатором роли
        if($data['deal_role'] == 'customer') {
            $userCustomer   = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $userContractor = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $paramCustomer = $data;
            $paramContractor = [];
        } else {
            $userCustomer   = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $userContractor = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $paramCustomer = [];
            $paramContractor = $data;
        }

        $dealAgentCustomer = $this->dealAgentManager->createDealAgent($userCustomer, $paramCustomer);
        $dealAgentContractor = $this->dealAgentManager->createDealAgent($userContractor, $paramContractor);

        $user = $this->userManager->getUserByLogin($this->authService->getIdentity());

        $dealOwner = $this->dealAgentManager->defineDealOwner($user, $dealAgentCustomer, $dealAgentContractor);

        // Create Deal
        $deal = $this->dealManager->createDeal($data, $dealAgentCustomer, $dealAgentContractor, $dealOwner);

        if($confirmed) {
            $dealAgentCustomer->setDealAgentConfirm(true);
            $dealAgentContractor->setDealAgentConfirm(true);

            $this->entityManager->flush();
        }

        return $deal;
    }

    /**
     * Подтверждение тестовой фикстурной сделки
     */
    private function confirmAndReturnTestFixtureDeal()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        $deal->getCustomer()->setDealAgentConfirm(true);
        $deal->getContractor()->setDealAgentConfirm(true);
        $deal->getContractor()->setDateOfConfirm(new \DateTime());

        $this->entityManager->flush();

        return $deal;
    }

    /**
     * @param $path
     */
    private function removeFile($path)
    {
        unlink($path);
    }
}