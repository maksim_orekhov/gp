<?php

namespace ApplicationTest\Controller;

use Application\Controller\NaturalPersonDriverLicenseController;
use Application\Entity\NaturalPerson;
use Application\Entity\NaturalPersonDocument;
use Application\Entity\NaturalPersonDocumentType;
use Application\Entity\NaturalPersonDriverLicense;
use Application\Form\NaturalPersonDriverLicenseForm;
use Application\Service\CivilLawSubject\NaturalPersonDocumentManager;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseRoleManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Core\Service\ORMDoctrineUtil;
use ModuleFileManager\Entity\File;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use CoreTest\ViewVarsTrait;

/**
 * Class NaturalPersonDriverLicenseControllerTest
 * @package ApplicationTest\Controller
 */
class NaturalPersonDriverLicenseControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_NATURAL_PERSON_LAST_NAME = 'Тестов';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Application\Service\CivilLawSubject\NaturalPersonDocumentManager
     */
    private $naturalPersonDocumentManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->naturalPersonDocumentManager = $serviceManager->get(NaturalPersonDocumentManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// Collection

    // Неавторизованный

    /**
     * Недоступность natural-person-driver-license не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/natural-person-driver-license', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(NaturalPersonDriverLicenseController::class);
        $this->assertMatchedRouteName('natural-person-driver-license');
        $this->assertRedirectTo('/login?redirectUrl=/natural-person-driver-license');
    }

    /**
     * Для Ajax
     * Недоступность natural-person-driver-license не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person-driver-license', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDriverLicenseController::class);
        $this->assertMatchedRouteName('natural-person-driver-license');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/natural-person-driver-license', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Не доступность /natural-person-driver-license авторизованному без роли Оператора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByVerifiedUser()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/natural-person-driver-license', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDriverLicenseController::class);
        $this->assertMatchedRouteName('natural-person-driver-license');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Не доступность /natural-person-driver-license авторизованному без роли Оператора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByVerifiedUserForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person-driver-license', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDriverLicenseController::class);
        $this->assertMatchedRouteName('natural-person-driver-license');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /natural-person-driver-license Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/natural-person-driver-license', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDriverLicenseController::class);
        $this->assertMatchedRouteName('natural-person-driver-license');
        // Что отдается в шаблон
        $this->assertArrayHasKey('driverLicenses', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-driver-license-filter-form');
    }

    /**
     * Для Ajax
     * Доступность /natural-person-driver-license Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person-driver-license', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDriverLicenseController::class);
        $this->assertMatchedRouteName('natural-person-driver-license');

        $data = json_decode($this->getResponse()->getContent(), true);
        $this->assertEquals('SUCCESS', $data['status']);
    }

    /**
     * Доступность /profile/natural-person/:idNaturalPerson/driver-license/create (GET на createFormAction) Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testCreateFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/profile/natural-person/'.$naturalPerson->getId().'/driver-license/create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDriverLicenseController::class);
        $this->assertControllerClass('NaturalPersonDriverLicenseController');
        $this->assertMatchedRouteName('user-profile/natural-person-driver-license-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('driverLicenseForm', $view_vars);
        $this->assertTrue($view_vars['driverLicenseForm'] instanceof NaturalPersonDriverLicenseForm);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-driver-license-create-form');
    }

    /**
     * Доступность /profile/natural-person-driver-license/:idDriverLicense/edit (GET на editFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testEditFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        /** @var NaturalPersonDriverLicense $driverLicense */
        $driverLicense = $this->createTestDriverLicense($naturalPerson);

        $this->dispatch('/profile/natural-person-driver-license/'.$driverLicense->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDriverLicenseController::class);
        $this->assertControllerClass('NaturalPersonDriverLicenseController');
        $this->assertMatchedRouteName('user-profile/natural-person-driver-license-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('driverLicense', $view_vars);
        $this->assertArrayHasKey('driverLicenseForm', $view_vars);
        $this->assertInstanceOf( NaturalPersonDriverLicenseForm::class, $view_vars['driverLicenseForm']);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-driver-license-edit-form');
    }

    /**
     * Доступность /profile/natural-person-driver-license/:idDriverLicense/delete (GET на deleteFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testDeleteFormCanBeAccessedByOwner()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        /** @var NaturalPersonDriverLicense $driverLicense */
        $driverLicense = $this->createTestDriverLicense($naturalPerson);

        $this->dispatch('/profile/natural-person-driver-license/'.$driverLicense->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonDriverLicenseController::class);
        $this->assertControllerClass('NaturalPersonDriverLicenseController');
        $this->assertMatchedRouteName('user-profile/natural-person-driver-license-form-delete');
        // Что отдается в шаблон
        $this->assertArrayHasKey('driverLicense', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-driver-license-delete-form');
    }

    /**
     * @param NaturalPerson $naturalPerson
     * @return NaturalPersonDriverLicense
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createTestDriverLicense(NaturalPerson $naturalPerson)
    {
        /** @var NaturalPersonDocument $document */
        $document = $this->createTestDocument();
        /** @var File $file */
        $file = $this->createTestFile();

        $driverLicense = new NaturalPersonDriverLicense();
        $driverLicense->setDateIssued(new \DateTime());
        $driverLicense->setNaturalPersonDocument($document);
        $driverLicense->addFile($file);

        $document->setNaturalPersonDriverLicense($driverLicense);

        $this->entityManager->persist($document);
        $this->entityManager->persist($driverLicense);

        $document->setNaturalPerson($naturalPerson);

        $this->entityManager->persist($naturalPerson);

        $this->entityManager->flush();

        return $driverLicense;
    }

    /**
     * @return NaturalPersonDocument
     */
    private function createTestDocument()
    {
        $document = new NaturalPersonDocument();

        /** @var NaturalPersonDocumentType $documentType */
        $documentType = $this->entityManager->getRepository(NaturalPersonDocumentType::class)
            ->findOneBy(['name' => 'driver_license']);

        $document->setNaturalPersonDocumentType($documentType);
        $document->setIsActive(1);

        return $document;
    }

    /**
     * @return File
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createTestFile()
    {
        $file = new File();
        $file->setCreated(new \DateTime());
        $file->setName('test_driver_license_file');
        $file->setType('image');
        $file->setOriginName('test_driver_license_file_origin');
        $file->setPath('/natural-person-driver-license');
        $file->setSize(123456);

        $this->entityManager->persist($file);
        $this->entityManager->flush($file);

        return $file;
    }

    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var \Application\Entity\Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}