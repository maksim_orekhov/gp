<?php

namespace ApplicationTest\Controller;

use Application\Controller\DisputeController;
use Application\Entity\Dispute;
use Application\Form\DisputeCloseForm;
use Application\Form\DisputeFileUploadForm;
use Application\Form\DisputeOpenForm;
use Application\Form\DisputeSearchForm;
use Application\Service\Dispute\DisputeManager;
use ApplicationTest\Bootstrap;
use Core\Service\Base\BaseRoleManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\Deal;
use Application\Entity\User;
use Application\Service\Deal\DealManager;
use CoreTest\ViewVarsTrait;

/**
 * Class DisputeControllerTest
 * @package ApplicationTest\Controller
 */
class DisputeControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const MINIMUM_DELIVERY_PERIOD = 7;
    const TEXT_DEAL_NAME = 'Сделка Debit Matched';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig($config);

        // Отключаем отправку уведомлений на email
        $this->resetConfigParameters();

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->disputeManager = $serviceManager->get(DisputeManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /////////////// Get

    /**
     * Доступность /dispute/:idDispute Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     */
    public function testGetActionCanBeAccessedByOperator()
    {
        /** @var Dispute $dispute */
        $dispute = $this->createTestDispute(self::TEXT_DEAL_NAME);

        // Присваиваем нужную роль и залогиниваемся
        #$this->assignRoleToTestUser('Operator');
        $this->login('operator');

        $this->dispatch('/dispute/'.$dispute->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DisputeController::class);
        $this->assertControllerClass('DisputeController');
        $this->assertMatchedRouteName('dispute-single');
        // Что отдается в шаблон
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertArrayHasKey('dispute_operators_comments', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-comment-create-form');
        $this->assertQuery('#dispute-file-upload-form');
    }

    /////////////// GetList

    /**
     * Доступность /dispute Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     */
    public function testGetListActionCanBeAccessedByOperator()
    {
        /** @var Dispute $dispute */
        $dispute = $this->createTestDispute(self::TEXT_DEAL_NAME);

        // Присваиваем нужную роль и залогиниваемся
        #$this->assignRoleToTestUser('Operator');
        $this->login('operator');

        $this->dispatch('/dispute', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DisputeController::class);
        $this->assertControllerClass('DisputeController');
        $this->assertMatchedRouteName('dispute');
        // Что отдается в шаблон
        $this->assertArrayHasKey('disputes', $view_vars);
        $this->assertGreaterThan(0, count($view_vars['disputes']));
        $this->assertEquals($dispute->getId(), $view_vars['disputes'][0]['id']);
        $this->assertArrayHasKey('disputeFilterForm', $view_vars);
        $this->assertTrue($view_vars['disputeFilterForm'] instanceof DisputeSearchForm);
        $this->assertArrayHasKey('paginator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-filter-form');
    }

    /////////////// CloseForm

    /**
     * Доступность /dispute/:idDispute/close Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     */
    public function testCloseFormActionCanBeAccessedByOperator()
    {
        /** @var Dispute $dispute */
        $dispute = $this->createTestDispute(self::TEXT_DEAL_NAME);

        $this->login('operator');

        $this->dispatch('/dispute/'.$dispute->getId().'/close', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DisputeController::class);
        $this->assertControllerClass('DisputeController');
        $this->assertMatchedRouteName('dispute-form-close');
        // Что отдается в шаблон
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('disputeCloseForm', $view_vars);
        $this->assertTrue($view_vars['disputeCloseForm'] instanceof DisputeCloseForm);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertTrue($view_vars['is_operator']);
        $this->assertTrue($view_vars['recommended_number_of_days'] == 1);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-close-form');
    }

    /**
     * Доступность /dispute/:idDispute/close Заказчику
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     */
    public function testCloseFormActionCanBeAccessedByCustomer()
    {
        /** @var Dispute $dispute */
        $dispute = $this->createTestDispute(self::TEXT_DEAL_NAME);

        $this->login('test');

        $this->dispatch('/dispute/'.$dispute->getId().'/close', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DisputeController::class);
        $this->assertControllerClass('DisputeController');
        $this->assertMatchedRouteName('dispute-form-close');
        // Что отдается в шаблон
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('disputeCloseForm', $view_vars);
        $this->assertTrue($view_vars['disputeCloseForm'] instanceof DisputeCloseForm);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertFalse($view_vars['is_operator']);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-close-form');
    }

    /////////////// OpenForm

    /**
     * Доступность /dispute/:idDispute/open Заказчику
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     */
    public function testOpenFormActionCanBeAccessedByCustomer()
    {
        /** @var Dispute $dispute */
        $dispute = $this->createTestDispute(self::TEXT_DEAL_NAME);

        $this->disputeManager->closeDispute([], $dispute, false);

        $this->login('test');

        $this->dispatch('/dispute/'.$dispute->getId().'/open', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DisputeController::class);
        $this->assertControllerClass('DisputeController');
        $this->assertMatchedRouteName('dispute-form-open');
        // Что отдается в шаблон
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('disputeOpenForm', $view_vars);
        $this->assertTrue($view_vars['disputeOpenForm'] instanceof DisputeOpenForm);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-open-form');
    }

    /////////////// FileUpload

    /**
     * Доступность /dispute/file-upload Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group file_upload
     */
    public function testFileUploadActionCanBeAccessedByOperator()
    {
        /** @var Dispute $dispute */
        $dispute = $this->createTestDispute(self::TEXT_DEAL_NAME);

        $this->login('operator');

        // Формируем POST даные
        $this->getRequest()
            ->setMethod('POST')
            ->setPost(new \Zend\Stdlib\Parameters(array('dispute_id' => $dispute->getId())));
        $file = new \Zend\Stdlib\Parameters([
            'file' => [
                'name' => '',
                'type' => '',
                'tmp_name' => '',
                'error' => 4,
                'size' => 0
            ]
        ]);
        $this->getRequest()->setFiles($file);

        $this->dispatch('/dispute/file-upload', 'POST');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(DisputeController::class);
        $this->assertControllerClass('DisputeController');
        $this->assertMatchedRouteName('dispute-file-upload');
        // Что отдается в шаблон
        $this->assertArrayHasKey('fileUploadForm', $view_vars);
        $this->assertTrue($view_vars['fileUploadForm'] instanceof DisputeFileUploadForm);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-file-upload-form');
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param null $deal_name
     * @return Dispute
     */
    private function createTestDispute($deal_name = null)
    {
        if (!$deal_name) $deal_name = self::TEXT_DEAL_NAME;

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        $dispute = new Dispute();
        $dispute->setReason('Спор по сделке '.$deal->getId().' для тестирования');
        $dispute->setCreated(new \DateTime());
        $dispute->setClosed(false);
        $dispute->setAuthor($deal->getCustomer());
        $dispute->setDeal($deal);

        $this->entityManager->persist($dispute);

        $this->entityManager->flush();

        return $dispute;
    }
}