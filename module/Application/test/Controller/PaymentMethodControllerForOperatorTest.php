<?php

namespace ApplicationTest\Controller;

use Application\Controller\PaymentMethodController;
use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethod;
use Application\Entity\PaymentMethodBankTransfer;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Service\Base\BaseRoleManager;
use Core\Service\ORMDoctrineUtil;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use CoreTest\ViewVarsTrait;

/**
 * Class PaymentMethodControllerForOperatorTest
 * @package ApplicationTest\Controller
 */
class PaymentMethodControllerForOperatorTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME = "Счет в ВТБ";

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// getList

    // Неавторизованный

    /**
     * Недоступность /payment-method (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/payment-method', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method');
        $this->assertRedirectTo('/login?redirectUrl=/payment-method');
    }

    /**
     * Для Ajax
     * Недоступность /payment-method (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/payment-method', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/payment-method', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /payment-method (GET на getList) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNotOperator()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/payment-method', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method');

        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $view_vars['message']);
    }

    /**
     * Для Ajax
     * Недоступность /payment-method (GET на getList) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNotOperatorForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/payment-method', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    // Оператор

    /**
     * Доступность /payment-method (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/payment-method', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method');
        // Что отдается в шаблон
        $this->assertArrayHasKey('paymentMethods', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Для Ajax
     * Доступность /payment-method (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperatorForAjax()
    {
        // Обеспечиваем наличие хотя бы одного PaymentMethod у пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $isXmlHttpRequest = true;
        $this->dispatch('/payment-method', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('PaymentMethod collection', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('paginator', $data['data']);
        $this->assertArrayHasKey('paymentMethods', $data['data']);
        $this->assertArrayHasKey('is_operator', $data['data']);

        // Get just added PaymentMethod from collection
        $paymentMethodAjax = $data['data']['paymentMethods'][0];

        $this->assertArrayHasKey('type', $paymentMethodAjax);
        $this->assertArrayHasKey('id', $paymentMethodAjax);
        $this->assertArrayHasKey('bank_transfer', $paymentMethodAjax);
    }

    /////////////// get

    // Неавторизованный

    /**
     * Недоступность /payment-method/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->entityManager->getRepository(PaymentMethod::class)
            ->findOneBy(array('isPattern' => true));
        // Проверяем
        $this->assertNotNull($paymentMethod);

        // Разлогиниваемся
        $this->baseAuthManager->logout();
        // Проверяем, что разлогинились
        $this->assertFalse($this->authService->hasIdentity());

        $this->dispatch('/payment-method/'.$paymentMethod->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method-single');
        $this->assertRedirectTo('/login?redirectUrl=/payment-method/'.$paymentMethod->getId());
    }

    /**
     * Для Ajax
     * Недоступность /payment-method/id  (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        $isXmlHttpRequest = true;
        $this->dispatch('/payment-method/'.$paymentMethod->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/payment-method/'.$paymentMethod->getId(), $data['data']['redirectUrl']);
    }

    // Не Оператор

    /**
     * Недоступность /payment-method/id (GET на get) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOperator()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/payment-method/'.$paymentMethod->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method-single');

        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $view_vars['message']);
    }

    /**
     * Для Ajax
     * Недоступность /payment-method/id (GET на get) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOperatorByGetForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/payment-method/'.$paymentMethod->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    // Оператор

    /**
     * Доступность /payment-method/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOperator()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/payment-method/'.$paymentMethod->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method-single');
        // Что отдается в шаблон
        $this->assertArrayHasKey('paymentMethod', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Для Ajax
     * Доступность /payment-method/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOperatorForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользолвателю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $isXmlHttpRequest = true;
        $this->dispatch('/payment-method/'.$paymentMethod->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('payment-method-single');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('PaymentMethod single', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('paymentMethod', $data['data']);
        $this->assertArrayHasKey('is_operator', $data['data']);

        $this->assertArrayHasKey('type', $data['data']['paymentMethod']);
        $this->assertArrayHasKey('id', $data['data']['paymentMethod']);
        $this->assertArrayHasKey('bank_transfer', $data['data']['paymentMethod']);
    }


    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param User $user
     * @param string $payment_method_type
     * @return PaymentMethod
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createNewTestPaymentMethod(User $user, $payment_method_type = 'bank_transfer')
    {
        /** @var PaymentMethodType $paymentMethodType */
        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(array('name' => $payment_method_type));

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(array('isPattern' => true, 'user' => $user));

        $paymentMethod = new PaymentMethod();
        $paymentMethod->setIsPattern(true);
        $paymentMethod->setCivilLawSubject($civilLawSubject);
        $paymentMethod->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentMethod);

        $this->entityManager->flush();

        $specificPaymentMethod = null;
        if ($payment_method_type === 'bank_transfer') {

            $specificPaymentMethod = new PaymentMethodBankTransfer();
            $specificPaymentMethod->setPaymentMethod($paymentMethod);
            $specificPaymentMethod->setAccountNumber('40700000000000000000');
            $specificPaymentMethod->setBik('044525201');
            $specificPaymentMethod->setCorrAccountNumber('30100000000000000000');
            $specificPaymentMethod->setName('Счет SimpleBank');
            $specificPaymentMethod->setBankName('SimpleBank');

            $paymentMethod->setPaymentMethodBankTransfer($specificPaymentMethod);

        } elseif ($payment_method_type === 'cash') {
            //
        }

        $this->entityManager->persist($specificPaymentMethod);

        $this->entityManager->flush();

        return $paymentMethod;
    }
}