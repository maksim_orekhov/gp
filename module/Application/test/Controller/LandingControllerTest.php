<?php
namespace ApplicationTest\HTMLOutput;

use Application\Controller\LandingController;
use ApplicationTest\Bootstrap;
use Core\Exception\BaseExceptionCodeInterface;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use CoreTest\ViewVarsTrait;

class LandingControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    public function setUp()
    {
        parent::setUp();
        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
    }

    /**
     * тест недоступности роута для GET
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testLandingFeedbackActionWithGetMethod()
    {
        $this->dispatch('/landing-feedback', 'GET');

        $viewVars = $this->getViewVars();

        $this->assertResponseStatusCode(404);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('landing-feedback');
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame(BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE, $viewVars['message']);
    }

    /**
     * тест недоступности роута для POST не через Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testLandingFeedbackActionWithPostMethodWithoutAjax()
    {
        $isXmlHttpRequest = false;
        $this->dispatch('/landing-feedback', 'POST', ['foo'=>'bar'], $isXmlHttpRequest);

        $viewVars = $this->getViewVars();

        $this->assertResponseStatusCode(404);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('landing-feedback');
        $this->assertArrayHasKey('message', $viewVars);
        $this->assertSame(BaseExceptionCodeInterface::PAGE_NOT_FOUND_MESSAGE, $viewVars['message']);
    }

    /**
     * тест отправки данных через роут (POST через Ajax)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testLandingFeedbackActionWithValidPostMethodAndWithAjax()
    {
        $isXmlHttpRequest = true;
        $postData = ['foo'=>'bar'];

        $this->getRequest()->setContent(json_encode($postData));
        $this->dispatch('/landing-feedback', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);


        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('landing-feedback');
        $this->assertArrayHasKey('status', $data);
        $this->assertSame('SUCCESS', $data['status']);
    }
}
