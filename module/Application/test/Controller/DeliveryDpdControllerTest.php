<?php

namespace ApplicationTest\Controller;

use Application\Controller\DeliveryController;
use Application\Controller\DeliveryDpdController;
use Application\Entity\Delivery;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\Role;
use Application\Entity\User;
use Application\Form\DeliveryConfirmForm;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Exception\LogicException;
use Core\Service\Base\BaseRoleManager;
use Zend\Form\Form;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;
use Zend\Form\Element;

/**
 * Class DeliveryControllerTest
 * @package ApplicationTest\Controller
 */
class DeliveryDpdControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const NOT_PAID_DEAL_NAME = 'Сделка Фикстура';
    const PAID_DEAL_NAME = 'Сделка Оплаченная';
    const DISPUTE_DEAL_NAME = 'Сделка Спор открыт';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->getUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// update

    // Неавторизованный

    /**
     * Недоступность /admin/delivery/dpd/update (GET на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery-dpd
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/admin/delivery/dpd/update', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryDpdController::class);
        $this->assertMatchedRouteName('delivery-dpd-update');
        $this->assertRedirectTo('/login?redirectUrl=/admin/delivery/dpd/update');
    }

    /**
     * Ajax
     * Недоступность /admin/delivery/dpd/update (GET на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery-dpd
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/admin/delivery/dpd/update', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryDpdController::class);
        $this->assertMatchedRouteName('delivery-dpd-update');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/admin/delivery/dpd/update', $data['data']['redirectUrl']);
    }


    // Не Administrator

    /**
     * Недоступность /admin/delivery/dpd/update (GET на update) не Administrator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery-dpd
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAdministrator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $this->dispatch('/admin/delivery/dpd/update', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryDpdController::class);
        $this->assertMatchedRouteName('delivery-dpd-update');
        $this->assertRedirectTo('/access-denied');
    }

    /**
     * Ajax
     * Недоступность /admin/delivery/dpd/update (GET на update) не Administrator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery-dpd
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAdministratorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/admin/delivery/dpd/update', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryDpdController::class);
        $this->assertMatchedRouteName('delivery-dpd-update');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    // Administrator

    /**
     * Доступность /admin/delivery/dpd/update (GET на update) для Administrator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery-dpd
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByAdministrator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Administrator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $this->dispatch('/admin/delivery/dpd/update', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryDpdController::class);
        $this->assertMatchedRouteName('delivery-dpd-update');
    }

    /**
     * Ajax
     * Доступность /admin/delivery/dpd/update (GET на update) для Administrator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery-dpd
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByAdministratorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Administrator', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/admin/delivery/dpd/update', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DeliveryDpdController::class);
        $this->assertMatchedRouteName('delivery-dpd-update');
    }


    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}