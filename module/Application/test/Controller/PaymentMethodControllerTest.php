<?php

namespace ApplicationTest\Controller;

use Application\Controller\PaymentMethodController;
use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Exception\Code\PaymentMethodExceptionCodeInterface;
use Application\Form\CivilLawSubjectSelectionForm;
use Application\Form\PaymentMethodTypeSelectionForm;
use ApplicationTest\Bootstrap;
use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Exception\LogicException;
use Core\Service\Base\BaseRoleManager;
use Core\Service\ORMDoctrineUtil;
use ModulePaymentOrder\Entity\PaymentMethodType;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use Application\Entity\PaymentMethod;
use Application\Controller\NaturalPersonController;
use Zend\Form\Element;
use CoreTest\ViewVarsTrait;

/**
 * Class PaymentMethodControllerTest
 * @package ApplicationTest\Controller
 */
class PaymentMethodControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_PAYMENT_METHOD_BANK_TRANSFER_NAME = "Счет в ВТБ";

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Core\Exception\LogicException
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// getList

    // Неавторизованный

    /**
     * Недоступность /profile/payment-method (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/payment-method', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');
        $this->assertRedirectTo('/login?redirectUrl=/profile/payment-method');
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/payment-method', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/payment-method (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/payment-method', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $view_vars['message']);
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Доступность /profile/payment-method (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUser()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/payment-method', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');
        // Что отдается в шаблон
        $this->assertArrayHasKey('paymentMethods', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
    }

    /**
     * Ajax
     * Доступность /profile/payment-method (GET на getList) авторизованному с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByVerifiedUserForAjax()
    {
        // Обеспечиваем наличие хотя бы одного PaymentMethod у пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('PaymentMethod collection', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('paginator', $data['data']);
        $this->assertArrayHasKey('paymentMethods', $data['data']);

        // Get one PaymentMethod
        $paymentMethodAjax = $data['data']['paymentMethods'][count($data['data']['paymentMethods'])-1];

        $this->assertArrayHasKey('type', $paymentMethodAjax);
        $this->assertArrayHasKey('id', $paymentMethodAjax);
        $this->assertArrayHasKey('bank_transfer', $paymentMethodAjax);

//        $this->assertArrayHasKey('civilLawSubject', $paymentMethodAjax);
//        $this->assertArrayHasKey('user', $paymentMethodAjax['civilLawSubject']);
//        $this->assertArrayHasKey('login', $paymentMethodAjax['civilLawSubject']['user']);
//        $this->assertArrayHasKey('naturalPerson', $paymentMethodAjax['civilLawSubject']);
//        $this->assertArrayHasKey('legalEntity',$paymentMethodAjax['civilLawSubject']);
//        $this->assertArrayHasKey('paymentMethodType', $paymentMethodAjax);
//        $this->assertArrayHasKey('name', $paymentMethodAjax['paymentMethodType']);
//        $this->assertArrayHasKey('paymentMethodBankTransfer', $paymentMethodAjax);
//        $this->assertArrayHasKey('name', $paymentMethodAjax['paymentMethodBankTransfer']);
    }

    /////////////// get

    // Неавторизованный

    /**
     * Недоступность /profile/payment-method/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/payment-method/'.$paymentMethod->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/payment-method/'.$paymentMethod->getId(), $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/payment-method/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperator()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $view_vars['message']);
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByOperatorForAjax()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    // Не Владелец

    /**
     * Неоступность /profile/payment-method/id (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByNotOwner()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $view_vars['message']);
    }

    /**
     * Ajax
     * Недоступность /profile/payment-method/id (GET на get) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByNotOwnerForAjax()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    // Владелец

    /**
     * Доступность /profile/payment-method/id (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOwner()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль 'Verified' пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(), 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');
    }

    /**
     * Ajax
     * Доступность /profile/payment-method/id (GET на get) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOwnerForAjax()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль 'Verified' пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'get', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('PaymentMethod single', $data['message']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('paymentMethod', $data['data']);

        $paymentMethodAjax = $data['data']['paymentMethod'];
        $this->assertArrayHasKey('type', $paymentMethodAjax);
        $this->assertArrayHasKey('id', $paymentMethodAjax);
        $this->assertArrayHasKey('bank_transfer', $paymentMethodAjax);
    }

    /////////////// update

    // Неавторизованный

    /**
     * Недоступность /profile/payment-method/id (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorised()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/payment-method/'.$paymentMethod->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id (POST на update) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/payment-method/'.$paymentMethod->getId(), $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/payment-method/id (POST на update) по HTTP
     * Не поддерживается - 404
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByOperatorByHttp()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id (POST на update) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByOperatorForAjax()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    // Не владелец

    /**
     * Недоступность /profile/payment-method/id (POST на update) по HTTP
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * Important! update method is only for Ajax!
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNotOwner()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData);

        $this->assertResponseStatusCode(404);
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id (POST на update) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testUpdateCanNotBeAccessedByNotOwnerForAjax()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываемся как test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    // Владелец

    /**
     * Для Ajax
     * Доступность /profile/payment-method/id (POST валидный на update) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testUpdateWithValidPostForOwnerForAjax()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываемся как test с ролью 'Verified'
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $new_name = 'Счет SimpleBank edited';

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'name'      => $new_name, // Меняем название
            'bank_name' => 'SimpleBank',
            'account_number' => '40700000000000000000',
            'corr_account_number' => '30100000000000000000',
            'bik' => '044525201',
            'kpp' => '781000001',
            'inn' => '7810000000',
            'payment_recipient_name' => 'Симпл Симплов',
            'civil_law_subject_id' => $paymentMethod->getCivilLawSubject()->getId(),
            'payment_method_type' => 'bank_transfer',
            'csrf' => $csrf
        ];

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var PaymentMethodBankTransfer $updatedPaymentMethodBankTransfer */
        $updatedPaymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('id' => $paymentMethod->getPaymentMethodBankTransfer()->getId()));

        $this->assertEquals($new_name, $updatedPaymentMethodBankTransfer->getName());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('PaymentMethod updated', $data['message']);
        $this->assertEquals($paymentMethod->getId(), $data['data']['paymentMethod']['id']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных /profile/payment-method/id
     * (POST невалидный на update) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwnerForAjax()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываемся как test с ролью 'Verified'
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $new_name = 'Счет SimpleBank edited';

        // Invalid POST
        $postData = [
            'name'      => $new_name, // Меняем название
            'bank_name' => 'SimpleBank',
            'account_number' => '40700000000000000000',
            'corr_account_number' => '30100000000000000000',
            'bik' => '044525201',
            'kpp' => '781000001',
            'inn' => '7810000000',
            'payment_recipient_name' => 'Симпл Симплов',
            'civil_law_subject_id' => $paymentMethod->getCivilLawSubject()->getId(),
            'payment_method_type' => 'bank_transfer',
            'csrf' => 'ghghhr56565656hhghghghh' // csrf несуществующий
        ];

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var PaymentMethodBankTransfer $updatedPaymentMethodBankTransfer */
        $updatedPaymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('id' => $paymentMethod->getPaymentMethodBankTransfer()->getId()));

        // Название не изменилось (новое название не сохранилось)
        $this->assertNotEquals($new_name, $updatedPaymentMethodBankTransfer->getName());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /////////////// create

    // Неавторизованный

    /**
     * Недоступность /profile/payment-method (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorised()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/payment-method', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');
        $this->assertRedirectTo('/login?redirectUrl=/profile/payment-method');
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id (POST на create) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/payment-method', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Недоступность /profile/payment-method (POST на create) по HTTP
     * 404
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/payment-method', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method (POST на create) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateCanNotBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До валидации формы не дойдет (Rbac не пропустит)
        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(LogicException::CIVIL_LAW_SUBJECT_NOT_PROPER_PROVIDED_MESSAGE, $data['message']);
    }

    /**
     * Недоступность /profile/payment-method (POST валидный на create) по HTTP (404)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerified()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            'name' => 'Не важно какие данные',
        ];

        $this->dispatch('/profile/payment-method', 'POST', $postData);

        $this->assertResponseStatusCode(404);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');
    }

    /**
     * Для Ajax
     * Доступность /profile/payment-method (POST валидный на create) пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateWithValidPostForVerifiedForAjax()
    {
        $testing_payment_method_type_name = 'bank_transfer';

        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(array('isPattern' => true, 'user' => $user));

        // Авторизовываемся как test с ролью 'Verified'
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $payment_method_name = 'Счет SimpleBank create';

        $postData = [
            'name'      => $payment_method_name,
            'bank_name' => 'SimpleBank',
            'account_number' => '40700000000000000000',
            'corr_account_number' => '30100000000000000000',
            'bik' => '044525201',
            'kpp' => '781000001',
            'inn' => '7810000000',
            'payment_recipient_name' => 'Симпл Симплов',
            'civil_law_subject_id' => $civilLawSubject->getId(),
            'payment_method_type' => $testing_payment_method_type_name,
            'csrf' => $csrf
        ];

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $paymentMethodAjax = $data['data']['paymentMethod'];

        // Проверяем, что тип соответсвует $testing_payment_method_type
        $this->assertEquals($testing_payment_method_type_name, $paymentMethodAjax['paymentMethodType']['name']);

        $this->assertEquals($payment_method_name, $paymentMethodAjax['paymentMethodBankTransfer']['name']);
        $this->assertEquals('test', $paymentMethodAjax['civilLawSubject']['user']['login']);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('PaymentMethod created', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('paymentMethod', $data['data']);
    }

    /**
     * Для Ajax
     * Отправка заведомо невалидных данных /profile/payment-method
     * (POST невалидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateWithInvalidPostForVerifiedForAjax()
    {
        $testing_payment_method_type_name = 'bank_transfer';

        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(array('isPattern' => true, 'user' => $user));

        // Авторизовываемся как test с ролью 'Verified'
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Invalid POST
        $payment_method_name = 'Счет SimpleBank create';

        $postData = [
            'name'      => $payment_method_name,
            'bank_name' => 'SimpleBank',
            'account_number' => '40700000000000000000',
            'corr_account_number' => '30100000000000000000',
            'bik' => '044525201',
            'kpp' => '781000001',
            'inn' => '7810000000',
            'payment_recipient_name' => 'Симпл Симплов',
            'civil_law_subject_id' => $civilLawSubject->getId(),
            'payment_method_type' => $testing_payment_method_type_name,
            'csrf' => 'ghghggg454446tghghghgh' // csrf несуществующий
        ];

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var PaymentMethodBankTransfer $paymentMethodBankTransfer */
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => $payment_method_name));

        $this->assertNull($paymentMethodBankTransfer);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_INVALID_FORM_DATA_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /////////////// delete

    // Неавторизованный

    /**
     * Недоступность /profile/payment-method/id (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorised()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(), 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');
        $this->assertRedirectTo('/login?redirectUrl=/profile/payment-method/'.$paymentMethod->getId());
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id (POST with hidden input name="_method" value="delete" на delete)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/payment-method/'.$paymentMethod->getId(), $data['data']['redirectUrl']);
    }

    // Не владелец

    /**
     * Недоступность /profile/payment-method/id (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwner()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(), 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $view_vars['message']);
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id (POST with hidden input name="_method" value="delete" на delete)
     * не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByNotOwnerForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    /**
     * Недоступность /profile/payment-method/id (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperator()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(), 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $view_vars['message']);
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id (POST with hidden input name="_method" value="delete" на delete)
     * Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteCanNotBeAccessedByOperatorForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        // До удаления не дойдет (Rbac не пропустит)
        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $data['message']);
    }

    // Владелец

    /**
     * Доступность /profile/payment-method/id (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteForOwner()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $newPaymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');
        // Just added PaymentMethod Id
        $new_payment_method_id = $newPaymentMethod->getId();

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            '_method' => 'delete',
        ];

        $this->dispatch('/profile/payment-method/'.$newPaymentMethod->getId(), 'POST', $postData);

        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->entityManager->getRepository(PaymentMethod::class)
            ->findOneBy(array('id' => $new_payment_method_id));

        // Must be null (deleted)
        $this->assertNull($paymentMethod);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');
    }

    /**
     * Для Ajax
     * Доступность /profile/payment-method/id (POST with hidden input name="_method" value="delete" на delete)
     * Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteForOwnerForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $newPaymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');
        // Just added PaymentMethod Id
        $new_payment_method_id = $newPaymentMethod->getId();

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $postData = [
            '_method' => 'delete',
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$newPaymentMethod->getId(),
            'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->entityManager->getRepository(PaymentMethod::class)
            ->findOneBy(array('id' => $new_payment_method_id));

        // Must be null (deleted)
        $this->assertNull($paymentMethod);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-single');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(PaymentMethodController::SUCCESS_DELETE, $data['message']);
    }

    // @TODO Добавить тесты для попытки удаления субъекта права, когда он используется в сделке
    // После прояснения ситуации в тикете http://jira.simple-tech.org:8080/browse/GP-336

    /////////////// editForm

    // Неавторизованный

    /**
     * Недоступность /profile/payment-method/id/edit (GET на editFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNonAuthorised()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/edit');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-edit');
        $this->assertRedirectTo('/login?redirectUrl=/profile/payment-method/'.$paymentMethod->getId().'/edit');
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id/edit (GET на editFormAction) по Ajax неавторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/edit',
            null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/payment-method/'.$paymentMethod->getId().'/edit', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id/edit (GET на editFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByAuthorisedForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/edit',
            'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-edit');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/payment-method/id/edit (GET на editFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByOperator()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-edit');

        $this->assertEquals('bad data provided: payment method not found', $view_vars['message']);
    }

    /**
     * Недоступность /profile/payment-method/id/edit (GET на editFormAction) не Владельцу (500)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testEditFormCanNotBeAccessedByNotOwner()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-edit');
        $this->assertEquals('bad data provided: payment method not found', $view_vars['message']);
    }

    /**
     * Доступность /profile/payment-method/id/edit (GET на editFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testEditFormCanBeAccessedByOwner()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('paymentMethod', $view_vars);
        $this->assertArrayHasKey('paymentMethodForm', $view_vars);
    }

    /**
     * Редактирование NaturalPerson /profile/payment-method/id/edit
     * (POST валидный на editFormAction) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testEditFormWithValidPostForVerified()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $new_name = 'Счет SimpleBank edited';

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'name'      => $new_name, // Меняем название
            'bank_name' => 'SimpleBank',
            'account_number' => '40700000000000000000',
            'corr_account_number' => '30100000000000000000',
            'bik' => '044525201',
            'kpp' => '781000001',
            'inn' => '7810000000',
            'payment_recipient_name' => 'Симпл Симплов',
            'civil_law_subject_id' => $paymentMethod->getCivilLawSubject()->getId(),
            'payment_method_type' => 'bank_transfer',
            'csrf' => $csrf
        ];

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/edit', 'POST', $postData);

        /** @var PaymentMethodBankTransfer $updatedPaymentMethodBankTransfer */
        $updatedPaymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('id' => $paymentMethod->getPaymentMethodBankTransfer()->getId()));

        $this->assertEquals($new_name, $updatedPaymentMethodBankTransfer->getName());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-edit');

        if ($paymentMethod->getCivilLawSubject()->getNaturalPerson()) {
            $url = '/profile/natural-person/' . $paymentMethod->getCivilLawSubject()->getNaturalPerson()->getId();
        } elseif ($paymentMethod->getCivilLawSubject()->getSoleProprietor()) {
            $url = '/profile/sole-proprietor/' . $paymentMethod->getCivilLawSubject()->getSoleProprietor()->getId();
        } else {
            $url = '/profile/legal-entity/'.$paymentMethod->getCivilLawSubject()->getLegalEntity()->getId();
        }
        $this->assertRedirectTo($url);
    }

    /**
     * Отправка заведомо невалидных данных /profile/payment-method/id/edit
     * (POST невалидный на editFormAction) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testUpdateWithInvalidPostForOwner()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль Verified пользолвателю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $new_name = 'Счет SimpleBank edited';

        // Invalid POST
        $postData = [
            'name'      => $new_name, // Меняем название
            'bank_name' => 'SimpleBank',
            'account_number' => '40700000000000000000',
            'corr_account_number' => '30100000000000000000',
            'bik' => '044525201',
            'kpp' => '781000001',
            'inn' => '7810000000',
            'payment_recipient_name' => 'Симпл Симплов',
            'civil_law_subject_id' => $paymentMethod->getCivilLawSubject()->getId(),
            'payment_method_type' => 'bank_transfer',
            'csrf' => 'jhjhj45645645fgfgfgfgg' // csrf несуществующий
        ];

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/edit', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var PaymentMethodBankTransfer $updatedPaymentMethodBankTransfer */
        $updatedPaymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => $new_name));

        $this->assertNull($updatedPaymentMethodBankTransfer);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('paymentMethod', $view_vars);
        $this->assertArrayHasKey('paymentMethodForm', $view_vars);
    }

    /////////////// createForm

    // Неавторизованный

    /**
     * Недоступность /profile/payment-method/create (GET на createFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/payment-method/create');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-create');
        $this->assertRedirectTo('/login?redirectUrl=/profile/payment-method/create');
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/create (GET на createFormAction) по Ajax не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/payment-method/create', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/create (GET на createFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateFormCanNotBeAccessedByAuthorisedForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/create', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-create');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/payment-method/create (GET на createFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     *
     * @TODO Разобьраться!
     */
    public function testCreateFormCanNotBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/payment-method/create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-create');

        #$this->assertEquals(BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE, $view_vars['message']);
    }

    /**
     * Доступность /profile/payment-method/create (GET на createFormAction) без параметров
     * пользователю с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateFormCanBeAccessedByVerified()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/payment-method/create', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('civilLawSubjectSelectForm', $view_vars);
        $this->assertInstanceOf(CivilLawSubjectSelectionForm::class, $view_vars['civilLawSubjectSelectForm']);
    }

    /**
     * Создание PaymentMethod /profile/payment-method/create
     * (POST валидный на create) с параметром civil_law_subject_id
     * пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateFormWithValidCivilLawSubjectForVerified()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(array('isPattern' => true, 'user' => $user));

        // Авторизовываемся как test с ролью 'Verified'
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'civil_law_subject_id' => $civilLawSubject->getId(),
            'csrf' => $csrf
        ];

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $this->dispatch(
            '/profile/civil-law-subject/'.$civilLawSubject->getId().'/payment-method/create',
            'POST', $postData
        );

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('paymentMethodTypeSelectForm', $view_vars);
        $this->assertInstanceOf(PaymentMethodTypeSelectionForm::class, $view_vars['paymentMethodTypeSelectForm']);
    }

    /**
     * Создание PaymentMethod /profile/payment-method/create
     * (Get на create) с параметрами civil_law_subject_id и payment_method_type
     * пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateFormWithValidParamsVerified()
    {
        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(array('isPattern' => true, 'user' => $user));

        // Авторизовываемся как test с ролью 'Verified'
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Valid GET
        $getData = [
            'payment_method_type' => 'bank_transfer',
        ];

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $this->dispatch(
            '/profile/civil-law-subject/'.$civilLawSubject->getId().'/payment-method/create',
            'GET', $getData
        );

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('paymentMethodForm', $view_vars);
        $this->assertArrayHasKey('paymentMethodType', $view_vars);
    }

    /**
     * Создание PaymentMethod /profile/civil-law-subject/:civil_law_subject_id/payment-method/:payment_method_type/create
     * (POST валидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateFormWithValidPostForVerified()
    {
        $testing_payment_method_type_name = 'bank_transfer';

        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(array('isPattern' => true, 'user' => $user));

        // Авторизовываемся как test с ролью 'Verified'
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Valid POST
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $payment_method_name = 'Счет SimpleBank create';

        $postData = [
            'name'      => $payment_method_name,
            'bank_name' => 'SimpleBank',
            'account_number' => '40700000000000000000',
            'corr_account_number' => '30100000000000000000',
            'bik' => '044525201',
            'kpp' => '781000001',
            'inn' => '7810000000',
            'payment_recipient_name' => 'Симпл Симплов',
            'civil_law_subject_id' => $civilLawSubject->getId(),
            'payment_method_type' => $testing_payment_method_type_name,
            'csrf' => $csrf
        ];

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $this->dispatch(
            '/profile/civil-law-subject/'.$civilLawSubject->getId().'/payment-method/'.$testing_payment_method_type_name.'/create',
            'POST', $postData
        );

        /** @var PaymentMethodBankTransfer $paymentMethodBankTransfer */
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => $payment_method_name));
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $paymentMethodBankTransfer->getPaymentMethod();

        // Проверяем, что тип соответсвует $testing_payment_method_type
        $this->assertEquals($testing_payment_method_type_name, $paymentMethod->getPaymentMethodType()->getName());

        $this->assertEquals($payment_method_name, $paymentMethodBankTransfer->getName());
        $this->assertEquals('test', $paymentMethod->getCivilLawSubject()->getUser()->getLogin());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-create');

        if ($paymentMethodBankTransfer->getPaymentMethod()->getCivilLawSubject()->getNaturalPerson()) {
            $url = '/profile/natural-person/' . $paymentMethod->getCivilLawSubject()->getNaturalPerson()->getId();
        } elseif ($paymentMethodBankTransfer->getPaymentMethod()->getCivilLawSubject()->getSoleProprietor()) {
                $url = '/profile/sole-proprietor/'.$paymentMethod->getCivilLawSubject()->getSoleProprietor()->getId();
        } else {
            $url = '/profile/legal-entity/'.$paymentMethod->getCivilLawSubject()->getLegalEntity()->getId();
        }
        $this->assertRedirectTo($url);
    }

    /**
     * Создание NaturalPerson
     * Отправка заведомо невалидных данных
     * /profile/civil-law-subject/:civil_law_subject_id/payment-method/:payment_method_type/create
     * (POST невалидный на create) пользователем с ролью Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testCreateFormWithInvalidPostForVerified()
    {
        $testing_payment_method_type_name = 'bank_transfer';

        // Создаем PaymentMethod для пользователя test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(array('isPattern' => true, 'user' => $user));

        // Авторизовываемся как test с ролью 'Verified'
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        // Invalid POST
        $payment_method_name = 'Счет SimpleBank create';

        $postData = [
            'name'      => $payment_method_name,
            'bank_name' => 'SimpleBank',
            'account_number' => '40700000000000000000',
            'corr_account_number' => '30100000000000000000',
            'bik' => '044525201',
            'kpp' => '781000001',
            'inn' => '7810000000',
            'payment_recipient_name' => 'Симпл Симплов',
            'civil_law_subject_id' => $civilLawSubject->getId(),
            'payment_method_type' => $testing_payment_method_type_name,
            'csrf' => 'hjhjhjhjtuyytytyg545454554545454545' // csrf несуществующий
        ];
        #$this->getRequest()->setContent(json_encode($postData));

        // Important! To avoid LazyLoading
        $this->entityManager->clear();

        $this->dispatch(
            '/profile/civil-law-subject/'.$civilLawSubject->getId().'/payment-method/'.$testing_payment_method_type_name.'/create',
            'POST', $postData
        );

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        /** @var PaymentMethodBankTransfer $paymentMethodBankTransfer */
        $paymentMethodBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
            ->findOneBy(array('name' => $payment_method_name));

        // Проверяем, что PaymentMethod не создан
        $this->assertNull($paymentMethodBankTransfer);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-create');
        // Что отдается в шаблон
        $this->assertArrayHasKey('paymentMethodForm', $view_vars);
        $this->assertArrayHasKey('paymentMethodType', $view_vars);
    }

    /////////////// deleteForm

    // Неавторизованный

    /**
     * Недоступность /profile/payment-method/id/delete (GET на deleteFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorised()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        $this->dispatch('/profile/natural-person/'.$paymentMethod->getId().'/delete');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-delete');
        $this->assertRedirectTo('/login?redirectUrl=/profile/natural-person/'.$paymentMethod->getId().'/delete');
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/id/delete (GET на deleteFormAction) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNonAuthorisedForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/natural-person/'.$paymentMethod->getId().'/delete',
            'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-delete');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/natural-person/'.$paymentMethod->getId().'/delete', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/delete
     * (GET на deleteFormAction) по Ajax авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByAuthorisedForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль Verified пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test');

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/delete',
            'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-delete');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Недоступность /profile/payment-method/id/delete (GET на deleteFormAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByOperator()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Operator'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-delete');

        $this->assertEquals(PaymentMethodExceptionCodeInterface::PAYMENT_METHOD_NOT_DEFINED_MESSAGE, $view_vars['message']);
    }

    /**
     * Недоступность /profile/payment-method/id/delete (GET на deleteFormAction) не Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteFormCanNotBeAccessedByNotOwner()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-delete');

        $this->assertEquals(PaymentMethodExceptionCodeInterface::PAYMENT_METHOD_NOT_DEFINED_MESSAGE, $view_vars['message']);
    }

    /**
     * Доступность /profile/payment-method/id/delete (GET на deleteFormAction) Владельцу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testDeleteFormCanBeAccessedByOwner()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test с ролью 'Verified'
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile/payment-method/'.$paymentMethod->getId().'/delete', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-delete');
        // Что отдается в шаблон
        $this->assertArrayHasKey('paymentMethod', $view_vars);
    }

    /////////////// formInit (экшен только для Ajax!)

    // Неавторизованный

    /**
     * Недоступность /profile/payment-method/form-init (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/profile/payment-method/form-init', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-init');
        $this->assertRedirectTo('/login?redirectUrl=/profile/payment-method/form-init');
    }

    /**
     * Для Ajax
     * Недоступность /profile/payment-method/form-init (GET на formInitAction)
     * не авторизованному пользователю по Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testFormInitCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-init');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED, $data['code']);
        $this->assertEquals(BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE, $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('redirectUrl', $data['data']);
        $this->assertEquals('/profile/payment-method/form-init', $data['data']['redirectUrl']);
    }

    // Авторизованный

    /**
     * Ajax
     * Доступность /profile/payment-method/form-init (GET на formInitAction) без параметров
     * авторизованному с ролью Verified
     * Должен отдать коллекции и нулевые объекты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testFormInitByVerifiedUserWithoutParamsForAjax()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);
        // Проверяем, что ключам объектов соотвествуют нулевые значения, а ключам коллекции - не нулевые массивы
        $this->assertNull($data['data']['civilLawSubject']);
        $this->assertNull($data['data']['paymentMethodType']);
        $this->assertGreaterThan(0, \count($data['data']['availableCivilLawSubjects']));
        $this->assertCount(3, $data['data']['availablePaymentMethodTypes']);
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/payment-method/form-init (GET на formInitAction)
     * с заведома невалидными параметрами авторизованному с ролью Verified
     * Должен отдать коллекции и нулевые объекты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testFormInitCanBeAccessedByVerifiedUserWithInvalidParamsForAjax()
    {
        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        //Invalid params
        $getData = [
            'civil_law_subject_id' => 99999999,
            'payment_method_type' => 'test_test',
            'idPaymentMethod' => 99999999
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);
        // Проверяем, что ключам объектов соотвествуют нулевые значения, а ключам коллекции - не нулевые массивы
        $this->assertNull($data['data']['civilLawSubject']);
        $this->assertNull($data['data']['paymentMethodType']);
        $this->assertNull($data['data']['paymentMethod']);
        $this->assertGreaterThan(0, \count($data['data']['availableCivilLawSubjects']));
        $this->assertCount(3, $data['data']['availablePaymentMethodTypes']);
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/payment-method/form-init (GET на formInitAction)
     * с параметрами чужих объектов (civil_law_subject_id, idPaymentMethod)
     * Должен отдать коллекции доступных объектов и нулевые объекты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testFormInitCanBeAccessedByNotOwnerUserWithAlienParamsForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Авторизовываем пользователя test2 с ролью 'Verified'
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test2');
        // Назначаем роль Verified пользолвателю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login('test2');

        //Invalid params
        $getData = [
            'civil_law_subject_id' =>  $paymentMethod->getCivilLawSubject()->getId(),
            'payment_method_type' => 'bank_transfer',
            'idPaymentMethod' => $paymentMethod->getId()
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);
        // Проверяем, что ключам объектов соотвествуют нулевые значения, а ключам коллекции - не нулевые массивы
        $this->assertNull($data['data']['civilLawSubject']);
        $this->assertNull($data['data']['paymentMethod']);

        $this->assertNotNull($data['data']['paymentMethodType']);
        $this->assertGreaterThan(0, \count($data['data']['availableCivilLawSubjects']));
        $this->assertCount(3, $data['data']['availablePaymentMethodTypes']);
    }

    /**
     * Ajax
     * Отдача данных при запросе /profile/payment-method/form-init (GET на formInitAction)
     * с валидными параметрами Владельцу объектов, id которых передаются (civil_law_subject_id, idPaymentMethod)
     * Должен отдать коллекции и НЕнулевые объекты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payment-method
     *
     * @throws \Exception
     */
    public function testFormInitCanBeAccessedByVerifiedUserWithValidParamsForAjax()
    {
        // Добавляем PaymentMethod пользователю test
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('test');
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->createNewTestPaymentMethod($user, 'bank_transfer');

        // Назначаем роль пользователю test
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test
        $this->login();

        //Invalid params
        $getData = [
            'civil_law_subject_id' =>  $paymentMethod->getCivilLawSubject()->getId(),
            'payment_method_type' => 'bank_transfer',
            'idPaymentMethod' => $paymentMethod->getId()
        ];

        $isXmlHttpRequest = true;
        $this->dispatch('/profile/payment-method/form-init', 'GET', $getData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(PaymentMethodController::class);
        $this->assertMatchedRouteName('user-profile/payment-method-form-init');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);
        // Проверяем, что ключам объектов соотвествуют нулевые значения, а ключам коллекции - не нулевые массивы
        $this->assertNotNull($data['data']['civilLawSubject']);
        //@TODO Если это нужно, то пропустить через hydrator со стретегией (сейчас в $data нет ['natural_person'])
        #$this->assertEquals('Тестов', $data['data']['civilLawSubject']['natural_person']['lastName']);
        $this->assertNotNull($data['data']['paymentMethodType']);
        $this->assertEquals('bank_transfer', $data['data']['paymentMethodType']['name']);
        $this->assertNotNull($data['data']['paymentMethod']);
        $this->assertEquals('Счет SimpleBank', $data['data']['paymentMethod']['paymentMethodBankTransfer']['name']);
        $this->assertEquals('test', $data['data']['paymentMethod']['civilLawSubject']['user']['login']);

        $this->assertGreaterThan(0, \count($data['data']['availableCivilLawSubjects']));
        $this->assertCount(3, $data['data']['availablePaymentMethodTypes']);
    }


    /**
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var \Application\Entity\Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param User $user
     * @param string $payment_method_type
     * @return PaymentMethod
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createNewTestPaymentMethod(User $user, $payment_method_type = 'bank_transfer')
    {
        /** @var PaymentMethodType $paymentMethodType */
        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(array('name' => $payment_method_type));

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(array('isPattern' => true, 'user' => $user));

        $paymentMethod = new PaymentMethod();
        $paymentMethod->setIsPattern(true);
        $paymentMethod->setCivilLawSubject($civilLawSubject);
        $paymentMethod->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentMethod);

        $this->entityManager->flush();

        $specificPaymentMethod = null;
        if ($payment_method_type === 'bank_transfer') {

            $specificPaymentMethod = new PaymentMethodBankTransfer();
            $specificPaymentMethod->setPaymentMethod($paymentMethod);
            $specificPaymentMethod->setAccountNumber('40700000000000000000');
            $specificPaymentMethod->setBik('044525201');
            $specificPaymentMethod->setCorrAccountNumber('30100000000000000000');
            #$specificPaymentMethod->setInn('7810000000');
            #$specificPaymentMethod->setKpp('781000001');
            $specificPaymentMethod->setName('Счет SimpleBank');
            $specificPaymentMethod->setBankName('SimpleBank');
            #$specificPaymentMethod->setPaymentRecipientName('Симпл Симплов');

            $paymentMethod->setPaymentMethodBankTransfer($specificPaymentMethod);

        } elseif ($payment_method_type === 'cash') {
            //
        }

        $this->entityManager->persist($specificPaymentMethod);

        $this->entityManager->flush();

        return $paymentMethod;
    }
}