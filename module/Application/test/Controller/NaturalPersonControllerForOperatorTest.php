<?php

namespace ApplicationTest\Controller;

use Application\Controller\NaturalPersonController;
use Application\Entity\NaturalPerson;
use ApplicationTest\Bootstrap;
use Core\Service\Base\BaseRoleManager;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\User;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use CoreTest\ViewVarsTrait;

/**
 * Class NaturalPersonControllerTest
 * @package ApplicationTest\Controller
 */
class NaturalPersonControllerForOperatorTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_NATURAL_PERSON_LAST_NAME = 'Тестов';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// getList

    // Неавторизованный

    /**
     * Недоступность /natural-person (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/natural-person', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person');
        $this->assertRedirectTo('/login?redirectUrl=/natural-person');
    }

    /**
     * Для Ajax
     * Недоступность /natural-person (GET на getList) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Авторизованный

    /**
     * Недоступность /natural-person (GET на getList) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNotOperator()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/natural-person', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /natural-person (GET на getList) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanNotBeAccessedByNotOperatorForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person', null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person');
        $this->assertRedirectTo('/not-authorized');
    }

    // Оператор

    /**
     * Доступность /natural-person (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/natural-person', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person');
        // Что отдается в шаблон
        $this->assertArrayHasKey('naturalPersons', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#natural-person-filter-form');
    }

    /**
     * Для Ajax
     * Доступность /natural-person (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetListCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $naturalPerson = $data['data']['naturalPersons'][count($data['data']['naturalPersons'])-1];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('NaturalPerson collection', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('naturalPersons', $data['data']);
        $this->assertArrayHasKey('paginator', $data['data']);
        $this->assertEquals('Тест', $naturalPerson['first_name']);
        $this->assertEquals('Тестов', $naturalPerson['last_name']);
    }

    /////////////// get

    // Неавторизованный

    /**
     * Недоступность /natural-person/id (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorised()
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/natural-person/'.$naturalPerson->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person-single');
        $this->assertRedirectTo('/login?redirectUrl=/natural-person/'.$naturalPerson->getId());
    }

    /**
     * Для Ajax
     * Недоступность /natural-person/id  (GET на get) не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person/'.$naturalPerson->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person-single');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    // Не Оператор

    /**
     * Недоступность /natural-person/id (GET на get) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOperator()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/natural-person/'.$naturalPerson->getId(), 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Для Ajax
     * Недоступность /natural-person/id (GET на get) не Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanNotBeAccessedByNotOperatorByGetForAjax()
    {
        // Назначаем роль пользолвателю test
        $this->assignRoleToTestUser('Verified');
        // Залогиниваем пользователя test
        $this->login();

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person/'.$naturalPerson->getId(), null, [], $isXmlHttpRequest);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person-single');
        $this->assertRedirectTo('/not-authorized');
    }

    // Оператор

    /**
     * Доступность /natural-person/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOperator()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $this->dispatch('/natural-person/'.$naturalPerson->getId(), 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person-single');
        #$this->assertQuery('.container');
    }

    /**
     * Для Ajax
     * Доступность /natural-person/id (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civil-law-subject
     * @group natural-person
     *
     * @throws \Exception
     */
    public function testGetCanBeAccessedByOperatorForAjax()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользолвателю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $naturalPerson = $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(array('lastName' => self::TEST_NATURAL_PERSON_LAST_NAME));

        $isXmlHttpRequest = true;
        $this->dispatch('/natural-person/'.$naturalPerson->getId(), null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $naturalPersonAjax = $data['data']['naturalPerson'];

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('natural-person-single');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('NaturalPerson single', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('naturalPerson', $data['data']);
        $this->assertEquals('Тест', $naturalPersonAjax['first_name']);
        $this->assertEquals('Тестов', $naturalPersonAjax['last_name']);
        $this->assertEquals($naturalPersonAjax['id'], $naturalPerson->getId());
    }


    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if (!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}