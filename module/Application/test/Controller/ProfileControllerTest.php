<?php

namespace ApplicationTest\Controller;

use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Controller\ProfileController;
use ApplicationTest\Bootstrap;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class ProfileControllerTest
 * @package ApplicationTest\Controller
 *
 * @TODO Дописать тесты!
 */
class ProfileControllerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    private $user_login;
    private $user_password;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    protected $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    protected $userManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \ModuleRbac\Service\RoleManager
     */
    protected $roleManager;

    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    protected $rbacManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->roleManager = $serviceManager->get(\ModuleRbac\Service\RoleManager::class);
        $this->rbacManager = $serviceManager->get(\ModuleRbac\Service\RbacManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Неавторизованный пользователь не может попасть настраницу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group profile
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessedByUnauthorizedUser()
    {
        $this->dispatch('/profile', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(ProfileController::class);
        $this->assertMatchedRouteName('profile');
        $this->assertRedirectTo('/login?redirectUrl=/profile');
    }

    /**
     * Авторизованый пользователь с нужными привилегиями может попасть на страницу
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group profile
     * @throws \Exception
     */
    public function testIndexActionCanBeAccessedByAuthorizedUserWithVerifiedRole()
    {
        $this->authUser();

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/profile', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(ProfileController::class);
        $this->assertMatchedRouteName('profile');
    }


    /**
     * @return \Application\Entity\User
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function authUser()
    {
        // Tested role name
        $role_name = 'Verified';

        // Get Role object
        $role = $this->roleManager->getRoleByName($role_name);
        // Get User object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        // Если были роли, удаляем
        $user->getRoles()->clear();
        // Add role to user
        $user->addRole($role);
        // Save
        $this->entityManager->flush();

        // Authorize test user
        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);

        return $user;
    }
}
