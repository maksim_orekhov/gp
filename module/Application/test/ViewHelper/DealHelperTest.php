<?php

namespace ApplicationTest\Controller;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Service\Deal\DealManager;
use Application\Service\UserManager;
use Application\View\Helper\DealHelper;
use ApplicationTest\Bootstrap;
use Core\Service\ORMDoctrineUtil;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class DealHelperTest
 * @package ApplicationTest\Controller
 */
class DealHelperTest extends AbstractHttpControllerTestCase
{
    const TEXT_DEAL_NAME = 'Сделка Фикстура';
    const TEXT_DEAL_NAME_2 = 'Сделка Negotiation';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var DealHelper
     */
    private $dealHelper;


    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->dealHelper = new DealHelper($this->dealManager, $this->userManager);
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');

        $this->user_login = $config['tests']['user_login'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;
        $this->dealManager = null;
        $this->userManager = null;
        $this->dealHelper = null;

        gc_collect_cycles();
    }


    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Core\Exception\LogicException
     */
    public function testGetUnansweredDealsNumberWhen0()
    {
        $number = $this->dealHelper->getUnansweredDealsNumber($this->user_login);

        $this->assertEquals(0, $number);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testGetUnansweredDealsNumberWhen1()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TEXT_DEAL_NAME]);
        // Убираем clw из Контрактора
        $this->setCLSToNull($deal->getContractor());

        $number = $this->dealHelper->getUnansweredDealsNumber('test2');

        $this->assertEquals(1, $number);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testGetUnansweredDealsNumberWhen2()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TEXT_DEAL_NAME]);
        // Убираем clw из Контрактора
        $this->setCLSToNull($deal->getContractor());

        /** @var Deal $deal */
        $deal_2 = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TEXT_DEAL_NAME_2]);
        // Убираем clw из Контрактора
        $this->setCLSToNull($deal_2->getContractor());

        $number = $this->dealHelper->getUnansweredDealsNumber('test2');

        $this->assertEquals(2, $number);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\ORMException
     */
    public function testIsUserHasDealsTrue()
    {
        $this->assertTrue($number = $this->dealHelper->isUserHasDeals('test2'));
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     *
     * @throws \Core\Exception\LogicException
     * @throws \Doctrine\ORM\ORMException
     */
    public function testIsUserHasDealsFalse()
    {
        $this->assertFalse($number = $this->dealHelper->isUserHasDeals('operator'));
    }


    /**
     * @param DealAgent $dealAgent
     * @throws \Doctrine\ORM\OptimisticLockException
     * @return void
     */
    private function setCLSToNull(DealAgent $dealAgent)
    {
        $dealAgent->setCivilLawSubject(null);
        $this->entityManager->persist($dealAgent);
        $this->entityManager->flush();
    }
}