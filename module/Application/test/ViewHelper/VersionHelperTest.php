<?php

namespace ApplicationTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\View\Helper\VersionHelper;
use Zend\Stdlib\ArrayUtils;

/**
 * Class VersionHelperTest
 * @package ApplicationTest\Controller
 */
class VersionHelperTest extends AbstractHttpControllerTestCase
{
    /**
     * @var VersionHelper
     */
    private $versionHelper;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $this->versionHelper = new VersionHelper();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    public function testGetVersion()
    {
        $version = $this->versionHelper->getVersion();

        $this->assertNotNull($version);
        $this->assertGreaterThan(40, strlen($version));
    }
}