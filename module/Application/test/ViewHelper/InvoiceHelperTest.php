<?php

namespace ApplicationTest\Controller;

use Core\Service\Base\BaseRoleManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\SettingManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\View\Helper\InvoiceHelper;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\SystemPaymentDetails;

/**
 * Class InvoiceHelperTest
 * @package ApplicationTest\Controller
 */
class InvoiceHelperTest extends AbstractHttpControllerTestCase
{
    /**
     * @var \Application\Service\SettingManager
     */
    private $settingManager;

    /**
     * @var \Application\View\Helper\InvoiceHelper
     */
    private $invoiceHelper;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $serviceManager = $this->getApplicationServiceLocator();
        $this->settingManager = $serviceManager->get(SettingManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);
        $this->invoiceHelper = new InvoiceHelper();
    }

    public function tearDown() {

        parent::tearDown();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     *
     * @dataProvider providerCivilLawSubjects
     *
     * @throws \Exception
     */
    public function testFormatPayerForInvoiceFromArray($given, $expected)
    {
        $result = $this->invoiceHelper->formatPayerForInvoiceFromArray($given);

        $this->assertEquals($result, $expected['payer']);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     *
     * @dataProvider providerCivilLawSubjects
     *
     * @throws \Exception
     */
    public function testFullInformationAboutPayerFromArray($given, $expected)
    {
        $result = $this->invoiceHelper->fullInformationAboutPayerFromArray($given);

        $this->assertEquals($result, $expected['full_payer']);
    }

    /**
     * @return array
     */
    public function providerCivilLawSubjects(): array
    {
        return [
            [   // given
                ['customer' => [
                    'civil_law_subject_id' => 291,
                    'first_name' => 'Тест',
                    'secondary_name' => 'Тестович',
                    'last_name' => 'Тестов',
                    'fio' => 'Тестов Тест Тестович',
                    'birth_date' => '31.12.1980',
                    'phone' => '+79033463906',
                    'email' => 'test@simple-technology.ru',
                    'is_natural_person' => 1,
                    'is_sole_proprietor' => null,
                    'is_legal_entity' => null,
                    'is_confirmed' => 1
                    ]
                ],
                // expected
                [
                    'payer' => 'Тестов Тест Тестович',
                    'full_payer' => 'Тестов Тест Тестович'
                ]
            ],

            [   // given
                ['customer' => [
                    'civil_law_subject_id' => 378,
                    'name' => 'ИП Месси Леонель Аргентинович',
                    'first_name' => 'Леонель',
                    'secondary_name' => 'Аргентинович',
                    'last_name' => 'Месси',
                    'fio' => 'Месси Леонель Аргентинович',
                    'birth_date' => '31.12.1980',
                    'inn' => '683103646744',
                    'email' => 'test@simple-technology.ru',
                    'is_natural_person' => null,
                    'is_sole_proprietor' => 1,
                    'is_legal_entity' => null,
                    'is_confirmed' => 1
                    ]
                ],
                // expected
                [
                    'payer' => 'ИП Месси Леонель Аргентинович',
                    'full_payer' => 'ИП Месси Леонель Аргентинович, ИНН 683103646744'
                ]
            ],

            [   // given
                ['customer' => [
                    'civil_law_subject_id' => 380,
                    'name' => 'OOO Колумбийский сахар',
                    'address' => 'Москва, ул. Авангардная, 4, офис 56',
                    'inn' => '4217030520',
                    'kpp' => '123456',
                    'email' => 'test@simple-technology.ru',
                    'is_natural_person' => null,
                    'is_sole_proprietor' => null,
                    'is_legal_entity' => 1,
                    'is_confirmed' => 1,
                    ]
                ],
                // expected
                [
                    'payer' => 'OOO Колумбийский сахар',
                    'full_payer' => 'OOO Колумбийский сахар, ИНН 4217030520, КПП 123456, Москва, ул. Авангардная, 4, офис 56'
                ]
            ],
        ];
    }

    /**
     * сумму прописью с возможностью сделать первый символ заглавным
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     */
    public function testNumberToString()
    {
        $amount = 5110.34;
        $expectAmount = 'пять тысяч сто десять рублей 34 копейки';
        $expectUppercaseFirstSymbolAmount = 'Пять тысяч сто десять рублей 34 копейки';

        $stringNumber = $this->invoiceHelper->numberToString($amount);
        $uppercaseFirstSymbolStringNumber = $this->invoiceHelper->numberToString($amount, true);

        $this->assertEquals($expectAmount, $stringNumber);
        $this->assertEquals($expectUppercaseFirstSymbolAmount, $uppercaseFirstSymbolStringNumber);
    }

    /**
     * сумму цифрами с единицей измерения валюты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     */
    public function testNumberWithCurrencyUnit()
    {
        $amount = 1;
        $amount2 = 10;
        $amount3 = 22;
        $expectAmount = '1 рубль';
        $expectAmount2 = '10 рублей';
        $expectAmount3 = '22 рубля';

        $unit = $this->invoiceHelper->numberWithCurrencyUnit($amount);
        $unit2 = $this->invoiceHelper->numberWithCurrencyUnit($amount2);
        $unit3 = $this->invoiceHelper->numberWithCurrencyUnit($amount3);

        $this->assertEquals($expectAmount, $unit);
        $this->assertEquals($expectAmount2, $unit2);
        $this->assertEquals($expectAmount3, $unit3);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @throws \Exception
     */
    public function testFullInformationSystemPaymentDetail()
    {
        $param = [
            'name'                   => 'тестовое название реквизитов',
            'bik'                    => '555588834',
            'account_number'         => '22222000098994433222',
            'payment_recipient_name' => 'ООО Guarant-Pay',
            'inn'                    => '900055889943',
            'kpp'                    => '334499338',
            'bank'                   => 'ПАО БАНК САНКТ-ПЕТЕРБУРГ',
            'corr_account_number'    => '30101810300000000811'
        ];
        //устанавливаем свои данные
        $paymentDetail = $this->settingManager->getSystemPaymentDetail();
        $paymentDetail->setName($param['name']);
        $paymentDetail->setBank($param['bank']);
        $paymentDetail->setBik($param['bik']);
        $paymentDetail->setAccountNumber($param['account_number']);
        $paymentDetail->setCorrAccountNumber($param['corr_account_number']);
        $paymentDetail->setPaymentRecipientName($param['payment_recipient_name']);
        $paymentDetail->setInn($param['inn']);
        $paymentDetail->setKpp($param['kpp']);

        /** @var SystemPaymentDetails $paymentDetail */
        $fullPayerSystemPaymentDetail = $this->invoiceHelper->fullInformationSystemPaymentDetail($paymentDetail);
        $expectFullPayerSystemPaymentDetail = $param['payment_recipient_name'].', ИНН '.$param['inn'].', КПП '.$param['kpp'];

        $this->assertEquals($expectFullPayerSystemPaymentDetail, $fullPayerSystemPaymentDetail);
    }
}