<?php

namespace ApplicationTest\HTMLOutput;

use Application\Entity\Delivery;
use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\Deal;

/**
 * Class HTMLOutputDealSingleTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputDealSingleTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DEAL_WITH_DISCOUNT_REQUEST_NAME = 'Сделка Покупатель направил запрос на Скидку';
    const DEAL_WITH_TRACK_NUMBER = 'Сделка Debit Matched';
    const CONFIRMED_DEAL_NAME = 'Сделка Оплаченная';
    const DEAL_WITH_INVITATION_NAME = 'Сделка с приглашением';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Тестируем наличие id для блока вариантов разрешения спора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     * @group HTMLOutput
     */
    public function testCheckAvailabilityBlockForResolvingDispute()
    {
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_DISCOUNT_REQUEST_NAME]);
        // Залогиниваем пользователя test
        $this->login();

        $this->assertNotNull($deal);
        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
        // Проверяем что блок действительно есть в DOM
        $this->assertQuery('#DisputeRequests');
    }

    /**
     * Тестируем наличие id для формы добавления трек номеров для contractor
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     * @group HTMLOutput
     */
    public function testCheckAvailabilityFormOfTrackNumbersForContractor() {
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_TRACK_NUMBER]);
        // Залогиниваем пользователя test
        $this->login('test2');

        $this->assertNotNull($deal);
        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
        // Проверяем что блок действительно есть в DOM
        $this->assertQuery('#track-number');
    }

    /**
     * Тестируем наличие id для формы добавления трек номеров для customer
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     * @group HTMLOutput
     */
    public function testCheckAvailabilityFormOfTrackNumbersForCustomer()
    {
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_DISCOUNT_REQUEST_NAME]);
        // Залогиниваем пользователя test
        $this->login('test');

        $this->assertNotNull($deal);
        $this->dispatch('/deal/show/' . $deal->getId(), 'GET');
        // Проверяем что блока нет в DOM
        $this->assertNotQuery('#track-number');
    }

    /**
     * Тестируем наличие id для списка трек номеров
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group HTMLOutput
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testCheckAvailabilityBlockOfTrackNumbersList() {
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_DISCOUNT_REQUEST_NAME]);
        // Create new Delivery object
        $delivery = new Delivery();
        $delivery->setDeliveryDate(new \DateTime());
        $delivery->setDeliveryStatus(false);
        $delivery->setTrackingNumber('RQ123456CN');
        $delivery->setDeal($deal);

        $this->entityManager->persist($delivery);
        $this->entityManager->flush();


        // Залогиниваем пользователя test
        $this->login();

        $this->assertNotNull($deal);
        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
        // Проверяем что блок действительно есть в DOM
        $this->assertQuery('#track-number-list');
    }

    /**
     * Тестируем Output deal single
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDealOutput()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);

        // Залогиниваем пользователя operator
        $this->login('test');

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');


        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        //check history
        $this->assertArrayHasKey('dispute_history', $view_vars);
    }

//@TODO закаментил 27.07.2018 т.к верстка тоже закоментирована
//    /**
//     * Тестируем наличие id ссылки для скачивания договора по сделке
//     *
//     * @runInSeparateProcess
//     * @preserveGlobalState disabled
//     * @throws \Exception
//     *
//     * @group deal
//     * @group HTMLOutput
//     */
//    public function testCheckAvailabilityContractFileDownloadLink() {
//        $deal = $this->entityManager->getRepository(Deal::class)
//            ->findOneBy(['name' => self::DEAL_WITH_DISCOUNT_REQUEST_NAME]);
//        // Залогиниваем пользователя test
//        $this->login();
//
//        $this->assertNotNull($deal);
//        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
//        // Проверяем что блок действительно есть в DOM
//        $this->assertQuery('#deal-contract-file');
//    }

    /**
     * Тестируем наличие формы повторного приглашения к сделке
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group deal
     * @group HTMLOutput
     */
    public function testCheckAvailabilityFormInvitationToDeal() {
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_INVITATION_NAME]);
        $this->assertNotNull($deal);
        // Залогиниваем пользователя test
        $this->login();
        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
        // Проверяем что блок действительно есть в DOM
        $this->assertQuery('#phpunit-test-invitation-resend');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}