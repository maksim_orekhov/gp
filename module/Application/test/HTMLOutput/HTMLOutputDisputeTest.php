<?php

namespace ApplicationTest\HTMLOutput;

use Application\Entity\Dispute;
use Application\Form\DisputeOpenForm;
use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class HTMLOutputDisputeSingleTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputDisputeTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DISPUTE_DEAL_NAME = 'Сделка Спор открыт';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Тестируем Output dispute single
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDisputeCloseFormOutput()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_DEAL_NAME]);

        $this->assertNotNull($deal);

        $dispute = $deal->getDispute();

        $this->assertNotNull($dispute);

        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/dispute/' . $dispute->getId() . '/close', 'GET');
        $this->assertMatchedRouteName('dispute-form-close');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertQuery('#dispute-close-form');
    }

    /**
     * Тестируем Output dispute single
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDisputeCollectionOutput()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_DEAL_NAME]);

        $this->assertNotNull($deal);

        $dispute = $deal->getDispute();

        $this->assertNotNull($dispute);

        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/dispute', 'GET');
        $this->assertMatchedRouteName('dispute');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertResponseStatusCode(200);
        $this->assertQuery('#TableDealsDispute');
    }


    /**
     * Тестируем Output dispute single
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDisputeFileUploadOutput()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_DEAL_NAME]);

        $this->assertNotNull($deal);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $getData = [
            'deal' => $deal,
            'dispute_id' => $dispute->getId(),

        ];
        $this->assertNotNull($dispute);

        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/dispute/file-upload', 'GET', $getData);
        $this->assertMatchedRouteName('dispute-file-upload');

        $this->assertResponseStatusCode(200);
        $this->assertQuery('#dispute-file-upload-form');
    }

    /**

     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDisputeOpenFormOutput()
    {

        /** @var Dispute $dispute */
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->findOneBy(['closed' => 1]);
        /** @var Deal $deal */
        $deal = $dispute->getDeal();

        $this->login($deal->getCustomer()->getUser()->getLogin());
        $this->assertNotNull($deal);

        $getData = [
            'deal' => $deal,
            'dispute_id' => $dispute->getId(),

        ];
        $this->assertNotNull($dispute);


        $this->dispatch('/dispute/' . $dispute->getId() . '/open', 'GET', $getData);
        $this->assertMatchedRouteName('dispute-form-open');
        $this->assertResponseStatusCode(200);
        $this->assertQuery('#dispute-open-form');
    }
    /**

     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDisputeOpenFormOutputForOperator()
    {

        /** @var Dispute $dispute */
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->findOneBy(['closed' => 1]);
        /** @var Deal $deal */
        $deal = $dispute->getDeal();

        $this->login('operator');
        $this->assertNotNull($deal);

        $getData = [
            'deal' => $deal,
            'dispute_id' => $dispute->getId(),

        ];
        $this->assertNotNull($dispute);


        $this->dispatch('/dispute/' . $dispute->getId() . '/open', 'GET', $getData);
        $this->assertMatchedRouteName('dispute-form-open');
        $this->assertResponseStatusCode(302);

    }
    /**

     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDisputeOpenFormOutputForContractor()
    {

        /** @var Dispute $dispute */
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->findOneBy(['closed' => 1]);
        /** @var Deal $deal */
        $deal = $dispute->getDeal();

        $this->login($deal->getContractor()->getUser()->getLogin());
        $this->assertNotNull($deal);

        $getData = [
            'deal' => $deal,
            'dispute_id' => $dispute->getId(),

        ];
        $this->assertNotNull($dispute);


        $this->dispatch('/dispute/' . $dispute->getId() . '/open', 'GET', $getData);
        $this->assertMatchedRouteName('dispute-form-open');
        $this->assertResponseStatusCode(302);

    }
    /**

     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDisputeSingleOutput()
    {

        /** @var Dispute $dispute */
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->findOneBy(['closed' => 1]);
        /** @var Deal $deal */
        $deal = $dispute->getDeal();

        $this->login($deal->getContractor()->getUser()->getLogin());
        $this->assertNotNull($deal);

        $getData = [
            'deal' => $deal,
            'dispute_id' => $dispute->getId(),

        ];
        $this->assertNotNull($dispute);

        $this->dispatch('/dispute/' . $dispute->getId(), 'GET', $getData);
        $this->assertMatchedRouteName('dispute-single');
        $this->assertResponseStatusCode(302);


    }
    /**

     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDisputeSingleOutputForContractor()
    {

        /** @var Dispute $dispute */
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->findOneBy(['closed' => 1]);
        /** @var Deal $deal */
        $deal = $dispute->getDeal();

        $this->login($deal->getContractor()->getUser()->getLogin());
        $this->assertNotNull($deal);

        $getData = [
            'deal' => $deal,
            'dispute_id' => $dispute->getId(),

        ];
        $this->assertNotNull($dispute);

        $this->dispatch('/dispute/' . $dispute->getId(), 'GET', $getData);
        $this->assertMatchedRouteName('dispute-single');
        $this->assertResponseStatusCode(302);


    }

    /**

     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDisputeSingleOutputForCustomer()
    {

        /** @var Dispute $dispute */
        $dispute = $this->entityManager->getRepository(Dispute::class)
            ->findOneBy(['closed' => 1]);
        /** @var Deal $deal */
        $deal = $dispute->getDeal();

        $this->login($deal->getCustomer()->getUser()->getLogin());
        $this->assertNotNull($deal);

        $getData = [
            'deal' => $deal,
            'dispute_id' => $dispute->getId(),

        ];
        $this->assertNotNull($dispute);

        $this->dispatch('/dispute/' . $dispute->getId(), 'GET', $getData);
        $this->assertMatchedRouteName('dispute-single');
        $this->assertResponseStatusCode(302);

    }
    /**
     * Тестируем Output dispute single
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDisputeOutput()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_DEAL_NAME]);

        $this->assertNotNull($deal);

        $dispute = $deal->getDispute();

        $this->assertNotNull($dispute);

        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/dispute/' . $dispute->getId(), 'GET');


        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertArrayHasKey('dispute', $view_vars);
        $disputeOutput = $view_vars['dispute'];
        //presence of keys
        $this->assertArrayHasKey('is_allowed_arbitrage_request', $disputeOutput);
        $this->assertArrayHasKey('is_allowed_tribunal_request', $disputeOutput);
        $this->assertArrayHasKey('is_allowed_discount_request', $disputeOutput);
        $this->assertArrayHasKey('is_allowed_refund_request', $disputeOutput);
        $this->assertArrayHasKey('is_allowed_arbitrage_confirm', $disputeOutput);
        $this->assertArrayHasKey('is_allowed_tribunal_confirm', $disputeOutput);
        $this->assertArrayHasKey('is_allowed_discount_confirm', $disputeOutput);
        $this->assertArrayHasKey('is_allowed_refund_confirm', $disputeOutput);
        $this->assertArrayHasKey('is_allow_close_dispute', $disputeOutput);
        //check history
        $this->assertArrayHasKey('dispute_history', $view_vars);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login = null, string $password = null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}