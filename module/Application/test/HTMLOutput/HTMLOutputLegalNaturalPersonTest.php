<?php

namespace ApplicationTest\Controller;

use Application\Controller\BankClientController;
use Application\Controller\DealController;
use Application\Controller\LegalEntityBankDetailController;
use Application\Controller\LegalEntityController;
use Application\Controller\LegalEntityTaxInspectionController;
use Application\Controller\NaturalPersonController;
use Application\Entity\CivilLawSubject;
use Application\Entity\LegalEntity;
use Application\Entity\LegalEntityBankDetail;
use Application\Entity\LegalEntityDocument;
use Application\Entity\LegalEntityDocumentType;
use Application\Entity\LegalEntityTaxInspection;
use Application\Entity\NaturalPerson;
use Application\Entity\NaturalPersonDocument;
use Application\Entity\NdsType;
use Application\Form\FileUploadForm;
use Application\Service\BankClient\BankClientManager;
use Core\Service\Base\BaseAuthManager;
use Doctrine\ORM\OptimisticLockException;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use ModuleRbac\Entity\Role;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\User;
use Application\Entity\Deal;
use Application\Entity\DealType;
use ModuleFileManager\Entity\File;
use Application\Entity\FeePayerOption;
use Application\Service\Parser\BankClientParser;
use Zend\ServiceManager\ServiceManager;
use Application\Entity\Payment;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealAgentManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Payment\PaymentMethodManager;
use Application\Listener\CreditPaymentOrderAvailabilityListener;
use CoreTest\ViewVarsTrait;


class HTMLOutputLegalNaturalPersonTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var string
     */
    public $main_upload_folder;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\BankClient\BankClientManager
     */
    public $bankClientManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \ModuleFileManager\Service\FileManager
     */
    private $fileManager;

    /**
     * @var BankClientParser
     */
    private $bankClientParser;

    /**
     * @var \Application\Service\Deal\DealManager
     */
    private $dealManager;

    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var CreditPaymentOrderAvailabilityListener
     */
    private $creditPaymentOrderAvailabilityListener;


    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        parent::setUp();

        $serviceManager = $this->getApplicationServiceLocator();

        $this->configureServiceManager($serviceManager);

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->bankClientManager = $serviceManager->get(\Application\Service\BankClient\BankClientManager::class);
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->fileManager = $serviceManager->get(\ModuleFileManager\Service\FileManager::class);
        $this->bankClientParser = $serviceManager->get(BankClientParser::class);

        $this->dealManager = $serviceManager->get(\Application\Service\Deal\DealManager::class);

        $config = $this->getApplicationConfig();

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->dealAgentManager = $serviceManager->get(DealAgentManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->paymentManager = $serviceManager->get(PaymentManager::class);
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);
        $this->creditPaymentOrderAvailabilityListener = $serviceManager->get(CreditPaymentOrderAvailabilityListener::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
     //   $this->entityManager->getConnection()->beginTransaction();
    }

    protected function configureServiceManager(ServiceManager $services)
    {
        $services->setAllowOverride(true);
        $services->setService('Config', $this->updateConfig($services->get('Config')));
        $services->setAllowOverride(false);
    }

    protected function updateConfig($config)
    {
        $config['email_providers'] = ['message_send_simulation' => true];

        return $config;
    }


    public function tearDown()
    {
        // Transaction rollback
     //   $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }


     /**
     * @throws \Exception
     * only for operator
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testNaturalPersonCreateForm()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();
        $this->dispatch('/profile/natural-person/create', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-create');
        $this->assertQuery('#natural-person-create-form');
    }

    /**
     * @throws \Exception
     * only for operator
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
 /*  public function testLegalEntityTaxInspectionDeleteForm()
    { Вернуться
     //   $this->assignRoleToTestUser('Verifyed');
        $this->login();
        /** @var NaturalPerson $naturalPerson */
 /*       $naturalPerson=self::createNaturalPerson();
        $this->dispatch('/profile/natural-person/' . $naturalPerson->getId() . '/delete', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(NaturalPersonController::class);
        $this->assertMatchedRouteName('user-profile/natural-person-form-delete');
        $this->assertQuery('#natural-person-delete-form');
    }*/

    /**
     * @throws \Exception
     * only for operator
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testLegalEntityTaxInspectionEditeForm()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();
        /** @var LegalEntity $LegalEntity */
        $LegalEntity = self::createLegalEntity();
        $LegalEntityDocumentType = $this->entityManager->getRepository(LegalEntityDocumentType::class)
            ->findOneBy(['name' => 'bank_details']);
        $createLegalEntityDocument=self::createLegalEntityDocument($LegalEntity,$LegalEntityDocumentType);
        $legalEntityTax=self::createTaxLegalEntity($createLegalEntityDocument);
        $this->dispatch('/profile/legal-entity-tax-inspection/' . $legalEntityTax->getId() . '/edit', 'GET');
        // /legal-entity-tax-inspection/:idTaxInspection/delete
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-form-edit');
        $this->assertQuery('#legal-entity-tax-inspection-edit-form');
    }
    /**
     * @throws \Exception
     * only for operator
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testLegalEntityTaxInspectionSingleForm()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();
        /** @var LegalEntity $LegalEntity */
        $LegalEntity = self::createLegalEntity();
        $LegalEntityDocumentType = $this->entityManager->getRepository(LegalEntityDocumentType::class)
            ->findOneBy(['name' => 'bank_details']);
        $createLegalEntityDocument=self::createLegalEntityDocument($LegalEntity,$LegalEntityDocumentType);
        $legalEntityTax=self::createTaxLegalEntity($createLegalEntityDocument);
        $this->dispatch('/profile/legal-entity-tax-inspection/' . $legalEntityTax->getId(), 'GET');
        // /legal-entity-tax-inspection/:idTaxInspection/delete
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityTaxInspectionController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-tax-inspection-single');
        $this->assertQuery('#legal-entity-tax-inspection-single-form');
    }
    /**
     * @param $LegalEntity
     * @throws OptimisticLockException
     */
    public function createNaturalPerson()
    {
        $lastName = 'tests';
        /** @var NaturalPerson $LegalEntityTax */
        $NaturalPerson = new NaturalPerson();
        $NaturalPerson->setLastName($lastName);
        $this->entityManager->persist($NaturalPerson);
        $this->entityManager->flush($NaturalPerson);
        $NaturalPerson= $this->entityManager->getRepository(NaturalPerson::class)
            ->findOneBy(['lastName' => $lastName]);
        var_dump($NaturalPerson);
        return $NaturalPerson;
    }
    /**
     * @param $LegalEntity
     * @throws OptimisticLockException
     */
    public function createTaxLegalEntity($LegalEntityDocument)
    {
        $serialNumber = '111111';
        /** @var LegalEntityTaxInspection $LegalEntityTax */
        $LegalEntityTax = new LegalEntityTaxInspection();
        $LegalEntityTax->setLegalEntityDocument($LegalEntityDocument);
        $LegalEntityTax->setSerialNumber($serialNumber);
        $LegalEntityTax->setDateRegistered(date_create(date('2018-06-03 00:00:00')));
        $LegalEntityTax->setDateIssued(date_create(date('2018-06-01 00:00:00')));
        $this->entityManager->persist($LegalEntityTax);
        $this->entityManager->flush($LegalEntityTax);
        $LegalEntityTax= $this->entityManager->getRepository(LegalEntityTaxInspection::class)
            ->findOneBy(['serialNumber' => $serialNumber]);
        return $LegalEntityTax;
    }

    /**
     * @throws \Exception
     * only for operator
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     */
    private function assignRoleToTestUser(string $role_name, User $user = null)
    {
        if (!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->userManager->addRoleByLogin($user, $role);
    }


    /**
     * @param null $LegalEntity
     * @param $LegalEntityDocumentType
     * @return LegalEntityDocument|null|object
     * @throws OptimisticLockException
     */
    private function createLegalEntityDocument($LegalEntity = null, $LegalEntityDocumentType)
    {
        $LegalEntityDocument = new LegalEntityDocument();
        $LegalEntityDocument->setLegalEntity($LegalEntity);
        $LegalEntityDocument->setLegalEntityDocumentType($LegalEntityDocumentType);
        $LegalEntityDocument->setIsActive(1);
        $this->entityManager->persist($LegalEntityDocument);
        $this->entityManager->flush($LegalEntityDocument);
        /** @var LegalEntityDocument $LegalEntityBankDetail */
        $LegalEntityDocument = $this->entityManager->getRepository(LegalEntityDocument::class)
            ->findOneBy(['isActive' => 1]);
        return $LegalEntityDocument;
    }

    /**
     * @param $LegalEntity
     * @return LegalEntityBankDetail|null|object
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createLegalEntityBankDetail($EntityDocument)
    {

        $bankDetailName = 'test';
        /** @var LegalEntityBankDetail $LegalEntityBankDetail */
        $LegalEntityBankDetail = new LegalEntityBankDetail();
        $LegalEntityBankDetail->setName($bankDetailName);
        $LegalEntityBankDetail->setLegalEntityDocument($EntityDocument);
        $LegalEntityBankDetail->setCheckingAccount('test');
        $LegalEntityBankDetail->setCorrespondentAccount('test');
        $LegalEntityBankDetail->setBic('test');
        $this->entityManager->persist($LegalEntityBankDetail);
        $this->entityManager->flush($LegalEntityBankDetail);
        /** @var LegalEntityBankDetail $LegalEntityBankDetail */
        $LegalEntityBankDetail = $this->entityManager->getRepository(LegalEntityBankDetail::class)
            ->findOneBy(['name' => $bankDetailName]);
        return $LegalEntityBankDetail;
    }

    /**
     * @return LegalEntity
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createLegalEntity()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '0']);
        /** @var  CivilLawSubject $civilLaw */
        $civilLaw = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['id' => '1']);
        $LegalEntyty = new LegalEntity();
        $LegalEntyty->setInn('test');
        $LegalEntyty->setKpp('test');
        $LegalEntyty->setName('test');
        $LegalEntyty->setPhone('8800200600');
        $LegalEntyty->setNdsType($ndsType);
        $LegalEntyty->setLegalAddress('bruansk');
        $LegalEntyty->setCivilLawSubject($civilLaw);
        $this->entityManager->persist($LegalEntyty);
        $this->entityManager->flush();
        /** @var LegalEntity $LegalEntyty */
        $LegalEntyty = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(['name' => 'test']);


        return $LegalEntyty;
    }

    /**
     * Залогиниваем пользователя test
     * @throws \Core\Exception\LogicException
     */
    private function login()
    {
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);
    }


}