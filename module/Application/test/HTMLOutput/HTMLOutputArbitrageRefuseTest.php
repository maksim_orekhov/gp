<?php

namespace ApplicationTest\HTMLOutput;

use Application\Controller\ArbitrageController;
use Application\Entity\ArbitrageRequest;
use Application\Entity\Dispute;
use Application\Service\Dispute\ArbitrageManager;
use Doctrine\Common\Collections\ArrayCollection;
use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\User;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\Deal;
use Application\Service\Dispute\DisputeManager;
use Application\Service\Deal\DealManager;

/**
 * Class HTMLOutputArbitrageRequestTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputArbitrageRefuseTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    /**
     * @var ArbitrageManager
     */
    private $arbitrageManager;
    /**
     * @var DisputeManager
     */
    private $disputeManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->dealManager = $serviceManager->get(\Application\Service\Deal\DealManager::class);
        $this->disputeManager = $serviceManager->get(\Application\Service\Dispute\DisputeManager::class);

    }

    /**
     * Тестируем рендеринг формы спора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     * проверка от имени оператора  (ему должна быть доступна, остальным нет)
     */
    public function testArbitrageRefuseForOperator()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Покупатель направил запрос на Арбитраж']);

        // Залогиниваем участника сжедки
        $this->login('operator');

        // Есть спор
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);
        //    var_dump($deal->getDispute());
        /** @var ArrayCollection $requests */
        $requests = $dispute->getArbitrageRequests();
        /** @var ArbitrageRequest $request */
        $request = $requests->last();
        $requestId = $request->getId();

        $this->dispatch('/deal/arbitrage/request/' . $requestId . '/refuse', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(ArbitrageController::class);
        $this->assertMatchedRouteName('deal-arbitrage-refuse');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('arbitrageRefuseForm', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('arbitrageRequest', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);

        //проверяем отгрузилась ли форма подтверждение запроса суда
        $this->assertQuery('#section_arbitrage_refuse');
    }

    /**
     * @throws \Exception
     * проверка от имени заазчика
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testArbitrageConfirmForCustomer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Покупатель направил запрос на Арбитраж']);

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);
        //    var_dump($deal->getDispute());
        /** @var ArrayCollection $requests */
        $requests = $dispute->getArbitrageRequests();
        /** @var ArbitrageRequest $request */
        $request = $requests->last();
        $requestId = $request->getId();


        $this->dispatch('/deal/arbitrage/request/' . $requestId . '/refuse', 'GET');
        $this->assertResponseStatusCode(302);


    }

    /**
     * @throws \Exception
     * проверка от имени продавца
     */
    public function testArbitrageConfirmForContractor()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Покупатель направил запрос на Арбитраж']);

        /** @var User $user */
        $user = $deal->getContractor()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);
        //    var_dump($deal->getDispute());
        /** @var ArrayCollection $requests */
        $requests = $dispute->getArbitrageRequests();
        /** @var ArbitrageRequest $request */
        $request = $requests->last();
        $requestId = $request->getId();


        $this->dispatch('/deal/arbitrage/request/' . $requestId . '/refuse', 'GET');
        $this->assertResponseStatusCode(302);

    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login = null, string $password = null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}