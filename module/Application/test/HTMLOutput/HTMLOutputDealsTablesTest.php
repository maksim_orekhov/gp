<?php

namespace ApplicationTest\HTMLOutput;

use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class HTMLOutputDealsTablesTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputDealsTablesTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /**
     * Тестируем таблицу сделок
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dealsTable
     * @group HTMLOutput
     */
    public function testDealsTable()
    {
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/deal', 'GET');

        // Проверяем что таблица действительно есть в DOM
        $this->assertQuery('#TableDeals');
    }

    /**
     * Тестируем таблицу сделок со спором
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group disputesTable
     * @group HTMLOutput
     */
    public function testDisputesTable()
    {
        $this->login('operator');

        $this->dispatch('/dispute', 'GET');

        // Проверяем что таблица действительно есть в DOM
        $this->assertQuery('#TableDealsDispute');
    }

    /**
     * Тестируем таблицу сделок с открытым спором
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group disputesTable
     * @group HTMLOutput
     */
    public function testDisputesOpenTable()
    {
        $this->login('operator');

        $this->dispatch('/dispute/open', 'GET');

        // Проверяем что таблица действительно есть в DOM
        $this->assertQuery('#TableDealsDisputeOpen');
    }

    /**
     * Тестируем таблицу проблемных сделок
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group disputesTable
     * @group HTMLOutput
     */
    public function testTableDealsTrouble()
    {
        $this->login('operator');

        $this->dispatch('/deal/abnormal', 'GET');

        // Проверяем что таблица действительно есть в DOM
        $this->assertQuery('#TableDealsTrouble');
    }

    /**
     * Тестируем таблицу сделок с истекшей датой
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group disputesTable
     * @group HTMLOutput
     */
    public function testTableDealsExpired()
    {
        $this->login('operator');

        $this->dispatch('/deal/expired', 'GET');

        // Проверяем что таблица действительно есть в DOM
        $this->assertQuery('#TableDealsExpired');
    }

    /**
     * Тестируем таблицу проблемных платежек
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group disputesTable
     * @group HTMLOutput
     */
    public function testTablePaymentOrdersTrouble()
    {
        $this->login('operator');

        $this->dispatch('/bank-client/payment-orders/trouble', 'GET');

        // Проверяем что таблица действительно есть в DOM
        $this->assertQuery('#TablePaymentOrdersTrouble');
    }

    /**
     * Тестируем таблицу сделок с истекающим сроком
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group disputesTable
     * @group HTMLOutput
     */
    public function testTableDealsDeadline()
    {
        $this->login('operator');

        $this->dispatch('/deal-deadline/notify', 'GET');

        // Проверяем что таблица действительно есть в DOM
        $this->assertQuery('#TableDealsDeadline');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}