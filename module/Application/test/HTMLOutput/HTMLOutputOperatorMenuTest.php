<?php

namespace ApplicationTest\HTMLOutput;

use Application\Controller\BankController;
use Application\Controller\DealController;
use Application\Controller\DealDeadlineController;
use Application\Controller\DisputeController;
use Application\Controller\UserController;
use Application\Form\DeadlineSearchForm;
use Application\Form\DealAbnormalForm;
use Application\Form\DisputeOpenSearchForm;
use Application\Form\DisputeSearchForm;
use Application\Form\ExpiredSearchForm;
use Application\Form\FileUploadForm;
use Application\Form\NotifyDeadlineForm;
use Application\Form\UpdateBankForm;
use Application\Form\UserSearchForm;
use Core\Service\Base\BaseAuthManager;
use ModulePaymentOrder\Controller\BankClientPaymentOrderController;
use ModulePaymentOrder\Form\BankClientPaymentOrderSearchForm;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Controller\BankClientController;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class HTMLOutputOperatorMenuTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputOperatorMenuTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /**
     * Доступность /deal пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group HTMLOutput
     */
    public function testDealIndexAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/deal', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal');
        // Что отдается в шаблон
        $this->assertArrayHasKey('dealFilterForm', $view_vars);
        $this->assertArrayHasKey('deals', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertTrue($view_vars['is_operator']);
        // Возвращается из шаблона
        $this->assertQuery('#deal-filter-form');
        $this->assertQuery('#TableDeals');
    }

    /**
     * Доступность /bank-client пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @group HTMLOutput
     */
    public function testBankClientIndexAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/bank-client', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertMatchedRouteName('bank-client');
        // Что отдается в шаблон
        $this->assertArrayHasKey('files', $view_vars);
        $this->assertArrayHasKey('processed_files', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#PaymentListPage');
        $this->assertQuery('#PageTable');
    }

    /**
     * Доступность /bank-client/file-upload пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @group HTMLOutput
     */
    public function testBankClientFileUploadAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/bank-client/file-upload', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertMatchedRouteName('bank-client/file-upload');
        // Что отдается в шаблон
        $this->assertArrayHasKey('fileUploadForm', $view_vars);
        $this->assertInstanceOf(FileUploadForm::class,$view_vars['fileUploadForm']);
        // Возвращается из шаблона
        $this->assertQuery('#bank-client-file-upload');
    }

    /**
     * Доступность /deal/expired пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group HTMLOutput
     */
    public function testDealExpiredAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/deal/expired', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/expired');
        // Что отдается в шаблон
        $this->assertArrayHasKey('payments', $view_vars);
        #$this->assertGreaterThan(0, count($view_vars['payments']));
        $this->assertTrue($view_vars['expiredSearchForm'] instanceof ExpiredSearchForm);
        $this->assertArrayHasKey('paginator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#TableDealsExpired');
    }

    /**
     * Доступность /deal/abnormal пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group HTMLOutput
     */
    public function testDealAbnormalAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/deal/abnormal', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/abnormal');
        // Что отдается в шаблон
        $this->assertArrayHasKey('payments', $view_vars);
        $this->assertTrue($view_vars['dealAbnormalForm'] instanceof DealAbnormalForm);
        $this->assertArrayHasKey('paginator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#TableDealsTrouble');
        $this->assertQuery('#TableDealsTrouble');
    }

    /**
     * Доступность /dispute пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group HTMLOutput
     */
    public function testDisputeGetListAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/dispute', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DisputeController::class);
        $this->assertMatchedRouteName('dispute');
        // Что отдается в шаблон
        $this->assertArrayHasKey('disputes', $view_vars);
        $this->assertTrue($view_vars['disputeFilterForm'] instanceof DisputeSearchForm);
        $this->assertArrayHasKey('paginator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-filter-form');
        $this->assertQuery('#TableDealsDispute');
    }

    /**
     * Доступность /dispute/open пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group dispute
     * @group HTMLOutput
     */
    public function testDisputeOpenListAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/dispute/open', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DisputeController::class);
        $this->assertMatchedRouteName('dispute-open-list');
        // Что отдается в шаблон
        $this->assertArrayHasKey('disputes', $view_vars);
        $this->assertTrue($view_vars['disputeFilterForm'] instanceof DisputeOpenSearchForm);
        $this->assertArrayHasKey('paginator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-filter-form');
        $this->assertQuery('#TableDealsDisputeOpen');
    }

    /**
     * Доступность /dispute/open пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group HTMLOutput
     */
    public function testDealDeadlineListAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/deal-deadline/notify', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealDeadlineController::class);
        $this->assertMatchedRouteName('deal-deadline-notify');
        // Что отдается в шаблон
        $this->assertArrayHasKey('notifyDeadlineForm', $view_vars);
        $this->assertInstanceOf(NotifyDeadlineForm::class,$view_vars['notifyDeadlineForm']);
        $this->assertArrayHasKey('deadlineSearchForm', $view_vars);
        $this->assertInstanceOf(DeadlineSearchForm::class,$view_vars['deadlineSearchForm']);
        $this->assertArrayHasKey('paginator', $view_vars);
        $this->assertArrayHasKey('is_send_notify', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#notify-deadline-form');
    }

    /**
     * Доступность /bank-client/payment-orders/trouble пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @group HTMLOutput
     */
    public function testBankClientTroubleAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/bank-client/payment-orders/trouble', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModulePaymentOrder');
        $this->assertControllerName(BankClientPaymentOrderController::class);
        $this->assertMatchedRouteName('bank-client-payment-orders-trouble');
        // Что отдается в шаблон
        $this->assertArrayHasKey('paymentOrderSearchForm', $view_vars);
        $this->assertInstanceOf(BankClientPaymentOrderSearchForm::class,$view_vars['paymentOrderSearchForm']);
        $this->assertArrayHasKey('payments', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#trouble-bank-clietn-payment-orders');
        $this->assertQuery('#TablePaymentOrdersTrouble');
    }

    /**
     * Доступность /user пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @group HTMLOutput
     */
    public function testUserGetListAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/user', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Application');
        $this->assertControllerName(UserController::class);
        $this->assertMatchedRouteName('user');
        // Что отдается в шаблон
        $this->assertArrayHasKey('userFilterForm', $view_vars);
        $this->assertInstanceOf(UserSearchForm::class,$view_vars['userFilterForm']);
        $this->assertArrayHasKey('users', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#React-TableUsers');
    }

    /**
     * Доступность /bank/update пользователю с ролью Operator
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank
     * @group HTMLOutput
     */
    public function testBankUpdateAction()
    {
        // Залогиниваем пользователя test
        $this->login('operator');

        $this->dispatch('/bank/update', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Application');
        $this->assertControllerName(BankController::class);
        $this->assertMatchedRouteName('bank-update');
        // Что отдается в шаблон
        $this->assertArrayHasKey('updateBankForm', $view_vars);
        $this->assertInstanceOf(UpdateBankForm::class,$view_vars['updateBankForm']);
        $this->assertArrayHasKey('update_result', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#update-bank-form');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}