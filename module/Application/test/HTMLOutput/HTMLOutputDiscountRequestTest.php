<?php

namespace ApplicationTest\HTMLOutput;

use Application\Entity\DealAgent;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\User;
use Core\Service\Base\BaseRoleManager;
use Core\Service\Base\BaseAuthManager;
use Application\Entity\Role;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class HTMLOutputDiscountRequestTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputDiscountRequestTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_DEAL_NAME = 'Сделка Debit Matched';
    const TEST_DEAL_WITH_DISPUTE_NAME = 'Сделка Спор открыт';
    const TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME = 'Сделка Покупатель направил запрос на Скидку';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;
    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * /discount/request/[id] (GET на get) для не автора звпроса
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     * @group HTMLOutput
     */
    public function testGetDiscountRequestOutput()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        /** @var DealAgent $author */
        $author = $discountRequest->getAuthor();

        if ($author === $deal->getCustomer()) {
            /** @var DealAgent $notAuthor */
            $notAuthor = $deal->getContractor();
        } else {
            /** @var DealAgent $notAuthor */
            $notAuthor = $deal->getCustomer();
        }

        $login = $notAuthor->getUser()->getLogin();
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin($login);
        // Назначаем роль Operator пользователю test2
        $this->assignRoleToTestUser('Verified', $user);
        // Залогиниваем пользователя test2
        $this->login($login);

        $this->dispatch('/discount/request/'.$discountRequest->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        // Что отдается в шаблон
        $this->assertArrayHasKey('discountRequest', $view_vars);
        $this->assertArrayHasKey('discountConfirmForm', $view_vars);
        $this->assertArrayHasKey('discountRefuseForm', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#DisputeRequestPage');
        $this->assertQuery('#request-confirm-form');
        $this->assertQuery('#request-refuse-form');
    }

    /**
     * Доступность /dispute/:idDispute/discount/create (GET на createFormAction) участнику сделки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     * @group HTMLOutput
     */
    public function testCreateForm()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISPUTE_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/request/create');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        // Что отдается в шаблон
        $this->assertArrayHasKey('discountRequestForm', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#discount-request-form');
    }

    /**
     * Доступность /discount/request (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     * @group HTMLOutput
     */
    public function testDiscountRequestTable()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/discount/request', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        // Что отдается в шаблон
        $this->assertArrayHasKey('discount_requests', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#TableDiscountRequests');
    }

    /**
     * Доступность /deal/discount/request/:idRequest/confirm (GET на formInitAction)
     * не автору запроса по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     * @group HTMLOutput
     */
    public function testDiscountRequestConfirmForm()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Контрактор
        $contractor = $deal->getContractor()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $contractor);
        $this->login($contractor->getLogin());

        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/confirm');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        // Что отдается в шаблон
        $this->assertArrayHasKey('discountConfirmForm', $view_vars);
        $this->assertArrayHasKey('discountRequest', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#DisputeRequestConfirmPage');
        $this->assertQuery('#request-confirm-form');
    }

    /**
     * Доступность /deal/discount/request/:idRequest/refuse (GET на formInitAction)
     * не автору запроса по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount-request
     * @group HTMLOutput
     */
    public function testDiscountRefuseForm()
    {
        /** @var Deal $deal */
        $deal = $this->getDealForTest(self::TEST_DEAL_WITH_DISCOUNT_REQUEST_NAME);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();
        // Проверяем, что спора есть
        $this->assertNotNull($dispute);

        // Контрактор
        $contractor = $deal->getContractor()->getUser();
        // Присваиваем нужную роль и залогиниваемся
        $this->assignRoleToTestUser('Verified', $contractor);
        $this->login($contractor->getLogin());

        $this->dispatch('/deal/discount/request/'.$discountRequest->getId().'/refuse');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        // Что отдается в шаблон
        $this->assertArrayHasKey('discountRefuseForm', $view_vars);
        $this->assertArrayHasKey('discountRequest', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#DisputeRequestRefusePage');
        $this->assertQuery('#request-refuse-form');
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param null $deal_name
     * @return Deal
     */
    private function getDealForTest($deal_name = null)
    {
        if (!$deal_name) $deal_name = self::TEST_DEAL_NAME;

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        return $deal;
    }
}