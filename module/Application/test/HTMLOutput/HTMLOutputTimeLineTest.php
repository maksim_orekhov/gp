<?php
namespace ApplicationTest\HTMLOutput;

use ApplicationTest\Bootstrap;
use Core\Service\Base\BaseAuthManager;
use Core\Service\ORMDoctrineUtil;
use Doctrine\ORM\EntityManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;

/**
 * Class HTMLOutputTimeLineTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputTimeLineTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DEAL_NEGOTIATION = 'Сделка Negotiation';
    const DEAL_CONFIRMED = 'Сделка Соглашение достигнуто';
    const DEAL_DEBIT_MATCHED = 'Сделка Оплаченная';
    const DEAL_DISPUTE = 'Сделка Спор открыт';
    const DEAL_DELIVERED = 'Сделка Доставка подтверждена';
    const DEAL_PENDING_CREDIT_PAYMENT = 'Сделка Ожидает выплаты';
    const DEAL_CLOSED = 'Сделка Закрыта';
    const DEAL_PENDING_REFUND_PAYMENT = 'Сделка Ожидает возврата';
    const DEAL_REFUND = 'Сделка Закрыто с возвратом';
    const DEAL_TRIBUNAL = 'Сделка Третейский суд';
    const DEAL_TROUBLE = 'Сделка Без контракта';

    const TIME_LINE_CONTAINER = 'timeline';
    const TIME_LINE_CREATED = 'timeline-created';
    const TIME_LINE_CONFIRMED = 'timeline-confirmed';
    const TIME_LINE_DEBIT_MATCHED = 'timeline-debit-matched';
    const TIME_LINE_DISPUTE = 'timeline-status-dispute';
    const TIME_LINE_DELIVERED = 'timeline-delivered';
    const TIME_LINE_PENDING_CREDIT = 'timeline-pending-credit';
    const TIME_LINE_PENDING_REFUND = 'timeline-status-pending-refund';
    const TIME_LINE_TROUBLE = 'timeline-trouble';
    const TIME_LINE_TRIBUNAL = 'timeline-tribunal';
    const TIME_LINE_CLOSED = 'timeline-closed';
    const TIME_LINE_EMPTY = 'timeline-empty';

    const TIME_LINE_ACTIVE = 'active';
    const TIME_LINE_FAIL = 'fail';
    const TIME_LINE_COMPLETED = 'completed';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();


        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;

        gc_collect_cycles();
    }

    /**
     * Таймлайн: сделка='Ожидает согласования'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealNegotiation()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_NEGOTIATION]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_ACTIVE.'")]');
        $this->assertQuery('#'.self::TIME_LINE_DEBIT_MATCHED);
        $this->assertQuery('#'.self::TIME_LINE_DELIVERED);
        $this->assertQuery('#'.self::TIME_LINE_PENDING_CREDIT);
        $this->assertQuery('#'.self::TIME_LINE_CLOSED);
    }

    /**
     * Таймлайн: сделка='Согласовано'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealConfirmed()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_CONFIRMED]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DEBIT_MATCHED.'" and contains(@class,"'.self::TIME_LINE_ACTIVE.'")]');
        $this->assertQuery('#'.self::TIME_LINE_DELIVERED);
        $this->assertQuery('#'.self::TIME_LINE_PENDING_CREDIT);
        $this->assertQuery('#'.self::TIME_LINE_CLOSED);
    }

    /**
     * Таймлайн: сделка='Оплачено'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealDebitMatched()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DEBIT_MATCHED]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DEBIT_MATCHED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DELIVERED.'" and contains(@class,"'.self::TIME_LINE_ACTIVE.'")]');
        $this->assertQuery('#'.self::TIME_LINE_PENDING_CREDIT);
        $this->assertQuery('#'.self::TIME_LINE_CLOSED);
    }

    /**
     * Таймлайн: сделка='Спор'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealDispute()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISPUTE]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DEBIT_MATCHED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DISPUTE.'" and contains(@class,"'.self::TIME_LINE_FAIL.'")]');
        $this->assertQuery('#'.self::TIME_LINE_PENDING_CREDIT);
        $this->assertQuery('#'.self::TIME_LINE_CLOSED);
    }

    /**
     * Таймлайн: сделка='Товар доставлен'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealDelivered()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DELIVERED]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DEBIT_MATCHED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DELIVERED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_PENDING_CREDIT.'" and contains(@class,"'.self::TIME_LINE_ACTIVE.'")]');
        $this->assertQuery('#'.self::TIME_LINE_CLOSED);
    }

    /**
     * Таймлайн: сделка='Ожидает выплаты'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealPendingCreditPayment()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_PENDING_CREDIT_PAYMENT]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DEBIT_MATCHED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DELIVERED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_PENDING_CREDIT.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CLOSED.'" and contains(@class,"'.self::TIME_LINE_ACTIVE.'")]');
    }

    /**
     * Таймлайн: сделка='Закрыто'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealClosed()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_CLOSED]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DEBIT_MATCHED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DELIVERED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_PENDING_CREDIT.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CLOSED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
    }

    /**
     * Таймлайн: сделка='Ожидает возврата'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealPendingRefundPayment()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_PENDING_REFUND_PAYMENT]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DEBIT_MATCHED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DISPUTE.'" and contains(@class,"'.self::TIME_LINE_FAIL.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_PENDING_REFUND.'" and contains(@class,"'.self::TIME_LINE_FAIL.'")]');
    }

    /**
     * Таймлайн: сделка='Закрыто с возвратом'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealRefund()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_REFUND]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DEBIT_MATCHED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DISPUTE.'" and contains(@class,"'.self::TIME_LINE_FAIL.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_PENDING_REFUND.'" and contains(@class,"'.self::TIME_LINE_FAIL.'")]');
    }

    /**
     * Таймлайн: сделка='Суд'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealTribunal()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_TRIBUNAL]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DEBIT_MATCHED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_DISPUTE.'" and contains(@class,"'.self::TIME_LINE_FAIL.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_TRIBUNAL.'" and contains(@class,"'.self::TIME_LINE_FAIL.'")]');
    }

    /**
     * Таймлайн: сделка='Проблемная'
     *
     * @throws \Exception
     *
     * @group time-line
     * @group deal
     * @group HTMLOutput
     */
    public function testTimeLineStatusWithDealTrouble()
    {
        // Залогиниваем пользователя test
        $this->login();

        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_TROUBLE]);
        $this->assertNotNull($deal);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        // TimeLine
        $this->assertQuery('#'.self::TIME_LINE_CONTAINER);

        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CREATED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_CONFIRMED.'" and contains(@class,"'.self::TIME_LINE_COMPLETED.'")]');
        $this->assertXpathQuery('//div[@id="'.self::TIME_LINE_TROUBLE.'" and contains(@class,"'.self::TIME_LINE_FAIL.'")]');
        $this->assertQuery('#'.self::TIME_LINE_DELIVERED);
        $this->assertQuery('#'.self::TIME_LINE_PENDING_CREDIT);
        $this->assertQuery('#'.self::TIME_LINE_CLOSED);
    }

    /**
     * Залогиниваем пользователя
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) {
            $login = $this->user_login;
        }
        if(!$password) {
            $password = $this->user_password;
        }
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}