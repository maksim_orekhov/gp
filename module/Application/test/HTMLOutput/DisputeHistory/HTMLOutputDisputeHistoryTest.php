<?php
namespace ApplicationTest\HTMLOutput\DisputeHistory;

use Application\Entity\Deal;
use ApplicationTest\Bootstrap;
use ApplicationTest\Service\Dispute\DisputeHistoryBaseTestTrait;
use CoreTest\ViewVarsTrait;
use Doctrine\ORM\EntityManager;
use Core\Service\Base\BaseAuthManager;
use Zend\Session\SessionManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class HTMLOutputDisputeHistoryTest
 * @package ApplicationTest\HTMLOutput\DisputeHistory
 */
class HTMLOutputDisputeHistoryTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;
    use DisputeHistoryBaseTestTrait;

    //1. открытие спора
    const DEAL_DISPUTE_OPEN = 'Сделка Спор открыт';
    //2.1 запрос на скидку
    const DEAL_DISCOUNT_REQUEST = 'Сделка Покупатель направил запрос на Скидку';
    //2.2 предложение скидки принято
    const DISCOUNT_REQUEST_ACCEPTED = 'Сделка Предложение скидки принято';
    //2.3 предложение скидки отклонено
    const DISCOUNT_REQUEST_REJECTED = 'Сделка Запрос на скидку отклонен';
    //3.1 запрос на возврат
    const REFUND_REQUEST = 'Сделка Покупатель направил запрос на Возврат';
    //3.2 предложение возврата принято
    const REFUND_REQUEST_ACCEPTED = 'Сделка Предложение возврата принято';
    //3.3 предложение возврата отклонено
    const REFUND_REQUEST_REJECTED = 'Сделка Предложение возврата отклонено';
    //4.1 запрос на арбитраж
    const ARBITRAGE_REQUEST = 'Сделка Покупатель направил запрос на Арбитраж';
    //4.2 запрос на арбитраж принят
    const ARBITRAGE_REQUEST_ACCEPTED = 'Сделка Запрос на Арбитраж принят';
    //4.3 запрос на арбитраж отклонен
    const ARBITRAGE_REQUEST_REJECTED = 'Сделка Запрос на Арбитраж отклонен';
    //5. запрос на скидку отклонен 3 раза
    const ARBITRAGE_REQUEST_WITH_DISCOUNT = 'Сделка Запросы на скидку отклонены 3 раза';
    //6. запрос на арбитраж после отклонения возврата
    const ARBITRAGE_REQUEST_WITH_REFUND = 'Сделка Запрос на Возврат отклонен - запрос на Арбитраж';
    //7. запрос на арбитраж после отклонения запроса на суд
    const ARBITRAGE_REQUEST_WITH_TRIBUNAL = 'Сделка Запрос на Суд отклонен - запрос на Арбитраж';
    //8.1 запрос на суд
    const TRIBUNAL_REQUEST = 'Сделка Продавец направил запрос на суд';
    //8.2 запрос на суд принят
    const TRIBUNAL_REQUEST_ACCEPTED = 'Сделка Запрос на суд принят';
    //8.3 запрос на суд отклонен
    const TRIBUNAL_REQUEST_REJECTED = 'Сделка Запроса на суд отклонен';
    //9. скидка предоставлена
    const DISCOUNT = 'Сделка Скидка создана';
    //10. возврат осуществлен
    const REFUND = 'Сделка Возврат создан';
    //11. третейский суд
    const TRIBUNAL = 'Сделка Третейский суд';
    //12. спор отменен покупателем
    const DISPUTE_CANCELED = 'Сделка Спор отозван';
    //13. спор закрыт без добавления дней
    const DISPUTE_CLOSED = 'Сделка Спор закрыт Оператором с продлением 0 дней';
    //14. оператор создает возврат
    const DISPUTE_CLOSED_REFUND = 'Сделка Закрыто с возвратом';
    //15. оператор создает скидку и не добавляет дни
    const DISPUTE_CLOSED_DISCOUNT = 'Сделка Спор со Скидкой закрыт без продления';
    //16. оператор создает скидку и добавляет дни
    const DISPUTE_CLOSED_DISCOUNT_WITH_DAYS = 'Сделка Спор со Скидкой закрыт с продлением 4 дня';
    //17. оператор закрывает спор и добавляет дни
    const DISPUTE_CLOSED_WITH_DAYS = 'Сделка Спор закрыт Оператор с продлением 3 дня';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();

        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollBack();
        $this->entityManager->getConnection()->close();
        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;

        gc_collect_cycles();
    }

    ///////////////////////////////////////////
    /// основные тесты для dispute history ///
    //////////////////////////////////////////

    /////////// 1. открытие спора ////////////
    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeOpenForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISPUTE_OPEN]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#dispute-open-for-operator');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeOpenForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISPUTE_OPEN]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#dispute-open-for-contractor');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeOpenForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISPUTE_OPEN]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#dispute-open-for-customer');
    }

    /////////// 2.1. запрос скидки ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISCOUNT_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISCOUNT_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISCOUNT_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request');
    }

    /////////// 2.2. предложение скидки принято ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestAcceptedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestAcceptedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestAcceptedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-accepted');
    }

    /////////// 2.3. предложение скидки отклонено ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestRejectedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestRejectedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestRejectedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-rejected');
    }

    /////////// 3.1. запрос на возврат ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request');
    }

    /////////// 3.2. предложение возврата принято ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestAcceptedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestAcceptedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestAcceptedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-accepted');
    }

    /////////// 3.3. предложение возврата отклонено ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestRejectedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestRejectedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestRejectedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-rejected');
    }

    /////////// 4.1. запрос на арбитраж ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request');
    }

    /////////// 4.2. запрос на арбитраж принят ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAcceptedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAcceptedForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAcceptedForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-accepted');
    }

    /////////// 4.3. запрос на арбитраж отклонен ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestRejectedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestRejectedForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestRejectedForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-rejected');
    }

    /////////// 5. запрос на скидку отклонен 3 раза ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAfterDiscountDeclineForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_WITH_DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-after-discount-decline');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAfterDiscountDeclineForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_WITH_DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-after-discount-decline');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAfterDiscountDeclineForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_WITH_DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-after-discount-decline');
    }

    /////////// 6. запрос на арбитраж после отклонения возврата ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAfterRefundDeclineForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_WITH_REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-after-refund-decline');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAfterRefundDeclineForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_WITH_REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-after-refund-decline');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAfterRefundDeclineForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_WITH_REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-after-refund-decline');
    }

    /////////// 7. запрос на арбитраж после отклонения запроса на суд ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAfterTribunalDeclinedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_WITH_TRIBUNAL]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-after-tribunal-decline');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAfterTribunalDeclinedForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_WITH_TRIBUNAL]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-after-tribunal-decline');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithArbitrageRequestAfterTribunalDeclinedForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::ARBITRAGE_REQUEST_WITH_TRIBUNAL]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-arbitrage-request-after-tribunal-decline');
    }

    /////////// 8.1. запрос на суд ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request');
    }

    /////////// 8.2. запрос на суд принят ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestAcceptedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestAcceptedForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestAcceptedForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-accepted');
    }

    /////////// 8.3. запрос на суд отклонен ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestRejectedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestRejectedForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestRejectedForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-rejected');
    }

    /////////// 9. скидка предоставлена ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount');
    }

    /////////// 10. возврат осуществлен ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund');
    }

    /////////// 11. третейский суд ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal');
    }

    /**
 * @runInSeparateProcess
 * @preserveGlobalState disabled
 *
 * @group dispute
 * @group dispute_history
 * @throws \Exception
 * @throws \Psr\Container\ContainerExceptionInterface
 * @throws \Psr\Container\NotFoundExceptionInterface
 */
    public function testHistoryWithTribunalForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal');
    }

    /////////// 12. спор отменен покупателем ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeCanceledForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CANCELED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-dispute-canceled');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeCanceledForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CANCELED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-dispute-canceled');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeCanceledForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CANCELED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-dispute-canceled');
    }

    /////////// 13. спор закрыт без добавления дней ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-dispute-closed');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-dispute-closed');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-dispute-closed');
    }

    /////////// 14. оператор создает возврат ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterRefundForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-is-made');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterRefundForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-is-made');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterRefundForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-is-made');
    }

    /////////// 15. оператор создает скидку и не добавляет дни ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-after-discount');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-after-discount');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-after-discount');
    }

    /////////// 16. оператор создает скидку и добавляет дни ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountWithDaysForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-discount-with-days');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountWithDaysForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-discount-with-days');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountWithDaysForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-discount-with-days');
    }

    /////////// 17. оператор закрывает спор и добавляет дни ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedWithDaysForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-with-days');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedWithDaysForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-with-days');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedWithDaysForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-with-days');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}