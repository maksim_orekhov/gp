<?php
namespace ApplicationTest\HTMLOutput\DisputeHistory;

use Application\Entity\Deal;
use ApplicationTest\Service\Dispute\DisputeHistoryBaseTestTrait;
use CoreTest\ViewVarsTrait;
use Doctrine\ORM\EntityManager;
use Core\Service\Base\BaseAuthManager;
use Zend\Session\SessionManager;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class HTMLOutputDisputeClosedTest
 * @package ApplicationTest\HTMLOutput\DisputeHistory
 */
class HTMLOutputDisputeClosedTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;
    use DisputeHistoryBaseTestTrait;

    //13. спор закрыт без добавления дней
    const DISPUTE_CLOSED = 'Сделка Спор закрыт Оператором с продлением 0 дней';
    //14. оператор создает возврат
    const DISPUTE_CLOSED_REFUND = 'Сделка Закрыто с возвратом';
    //15. оператор создает скидку и не добавляет дни
    const DISPUTE_CLOSED_DISCOUNT = 'Сделка Спор со Скидкой закрыт без продления';
    //16. оператор создает скидку и добавляет дни
    const DISPUTE_CLOSED_DISCOUNT_WITH_DAYS = 'Сделка Спор со Скидкой закрыт с продлением 4 дня';
    //17. оператор закрывает спор и добавляет дни
    const DISPUTE_CLOSED_WITH_DAYS = 'Сделка Спор закрыт Оператор с продлением 3 дня';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            include __DIR__ . '/../../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollBack();
        $this->entityManager->getConnection()->close();
        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;

        gc_collect_cycles();
    }

    /////////// 13. спор закрыт без добавления дней ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-dispute-closed');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-dispute-closed');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-dispute-closed');
    }

    /////////// 14. оператор создает возврат ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterRefundForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-is-made');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterRefundForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-is-made');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterRefundForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-is-made');
    }

    /////////// 15. оператор создает скидку и не добавляет дни ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-after-discount');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-after-discount');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-after-discount');
    }

    /////////// 16. оператор создает скидку и добавляет дни ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountWithDaysForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-discount-with-days');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountWithDaysForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-discount-with-days');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedAfterDiscountWithDaysForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_DISCOUNT_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-discount-with-days');
    }

    /////////// 17. оператор закрывает спор и добавляет дни ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedWithDaysForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-with-days');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedWithDaysForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-with-days');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDisputeClosedWithDaysForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_CLOSED_WITH_DAYS]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-closed-with-days');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}