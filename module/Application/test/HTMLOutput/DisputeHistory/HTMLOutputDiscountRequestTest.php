<?php
namespace ApplicationTest\HTMLOutput\DisputeHistory;

use Application\Entity\Deal;
use ApplicationTest\Service\Dispute\DisputeHistoryBaseTestTrait;
use CoreTest\ViewVarsTrait;
use Doctrine\ORM\EntityManager;
use Core\Service\Base\BaseAuthManager;
use Zend\Session\SessionManager;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class HTMLOutputDiscountRequestTest
 * @package ApplicationTest\HTMLOutput\DisputeHistory
 */
class HTMLOutputDiscountRequestTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;
    use DisputeHistoryBaseTestTrait;

    //2.1 запрос на скидку
    const DEAL_DISCOUNT_REQUEST = 'Сделка Покупатель направил запрос на Скидку';
    //2.2 предложение скидки принято
    const DISCOUNT_REQUEST_ACCEPTED = 'Сделка Предложение скидки принято';
    //2.3 предложение скидки отклонено
    const DISCOUNT_REQUEST_REJECTED = 'Сделка Запрос на скидку отклонен';
    //9. скидка предоставлена
    const DISCOUNT = 'Сделка Скидка создана';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            include __DIR__ . '/../../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollBack();
        $this->entityManager->getConnection()->close();
        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;

        gc_collect_cycles();
    }

    /////////// 2.1. запрос скидки ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISCOUNT_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISCOUNT_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_DISCOUNT_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request');
    }

    /////////// 2.2. предложение скидки принято ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestAcceptedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestAcceptedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestAcceptedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-accepted');
    }

    /////////// 2.3. предложение скидки отклонено ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestRejectedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestRejectedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountRequestRejectedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount-request-rejected');
    }

    /////////// 9. скидка предоставлена ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDiscountForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-discount');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}