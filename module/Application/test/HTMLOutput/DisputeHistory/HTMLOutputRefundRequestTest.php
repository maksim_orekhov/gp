<?php
namespace ApplicationTest\HTMLOutput\DisputeHistory;

use Application\Entity\Deal;
use ApplicationTest\Service\Dispute\DisputeHistoryBaseTestTrait;
use CoreTest\ViewVarsTrait;
use Doctrine\ORM\EntityManager;
use Core\Service\Base\BaseAuthManager;
use Zend\Session\SessionManager;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class HTMLOutputRefundRequestTest
 * @package ApplicationTest\HTMLOutput\DisputeHistory
 */
class HTMLOutputRefundRequestTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;
    use DisputeHistoryBaseTestTrait;

    //3.1 запрос на возврат
    const REFUND_REQUEST = 'Сделка Покупатель направил запрос на Возврат';
    //3.2 предложение возврата принято
    const REFUND_REQUEST_ACCEPTED = 'Сделка Предложение возврата принято';
    //3.3 предложение возврата отклонено
    const REFUND_REQUEST_REJECTED = 'Сделка Предложение возврата отклонено';
    //10. возврат осуществлен
    const REFUND = 'Сделка Возврат создан';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            include __DIR__ . '/../../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollBack();
        $this->entityManager->getConnection()->close();
        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;

        gc_collect_cycles();
    }

    /////////// 3.1. запрос на возврат ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request');
    }

    /////////// 3.2. предложение возврата принято ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestAcceptedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestAcceptedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestAcceptedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-accepted');
    }

    /////////// 3.3. предложение возврата отклонено ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestRejectedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestRejectedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundRequestRejectedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund-request-rejected');
    }

    /////////// 10. возврат осуществлен ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithRefundForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-refund');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}