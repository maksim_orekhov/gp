<?php
namespace ApplicationTest\HTMLOutput\DisputeHistory;

use Application\Entity\Deal;
use ApplicationTest\Service\Dispute\DisputeHistoryBaseTestTrait;
use CoreTest\ViewVarsTrait;
use Doctrine\ORM\EntityManager;
use Core\Service\Base\BaseAuthManager;
use Zend\Session\SessionManager;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class HTMLOutputTribunalRequestTest
 * @package ApplicationTest\HTMLOutput\DisputeHistory
 */
class HTMLOutputTribunalRequestTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;
    use DisputeHistoryBaseTestTrait;

    //8.1 запрос на суд
    const TRIBUNAL_REQUEST = 'Сделка Продавец направил запрос на суд';
    //8.2 запрос на суд принят
    const TRIBUNAL_REQUEST_ACCEPTED = 'Сделка Запрос на суд принят';
    //8.3 запрос на суд отклонен
    const TRIBUNAL_REQUEST_REJECTED = 'Сделка Запроса на суд отклонен';
    //11. третейский суд
    const TRIBUNAL = 'Сделка Третейский суд';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            include __DIR__ . '/../../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollBack();
        $this->entityManager->getConnection()->close();
        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;

        gc_collect_cycles();
    }

    /////////// 8.1. запрос на суд ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request');
    }

    /////////// 8.2. запрос на суд принят ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestAcceptedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestAcceptedForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-accepted');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestAcceptedForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_ACCEPTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-accepted');
    }

    /////////// 8.3. запрос на суд отклонен ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestRejectedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestRejectedForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-rejected');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalRequestRejectedForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL_REQUEST_REJECTED]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal-request-rejected');
    }

    /////////// 11. третейский суд ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalForContractor()
    {
        // Залогиниваем пользователя operator
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group dispute_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithTribunalForCustomer()
    {
        // Залогиниваем пользователя operator
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TRIBUNAL]);

        $this->getDisputeHistoryBaseTest($deal);

        $this->assertQuery('#history-tribunal');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}