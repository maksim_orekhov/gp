<?php
namespace ApplicationTest\HTMLOutput\DisputeHistory;

use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;
use ApplicationTest\Service\Deal\DeadlineOneDayTrait;
use Doctrine\ORM\EntityManager;
use Core\Service\Base\BaseAuthManager;
use Zend\Session\SessionManager;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class HTMLOutputDealHistoryTest
 * @package ApplicationTest\HTMLOutput\DealHistory
 */
class HTMLOutputDealHistoryTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;
    use DeadlineOneDayTrait;

    //1. сделка создана
    const CREATED = 'Сделка Negotiation';
    //2. сделка подтверждена
    const CONFIRMED = 'Сделка Частично оплачено';
    //3. сделка оплачена
    const DEBIT_MATCHED = 'Сделка Debit Matched';
    //4. сделка выполнена
    const DELIVERED = 'Сделка Доставка подтверждена';
    //5. деньги по сделке выплачены
    const PENDING_CREDIT_PAYMENT = 'Сделка Ожидает выплаты';
    //6. проблемная сделка
    const TROUBLE = 'Сделка Без контракта';
    //7. сделка просрочена
    const DEADLINE_EXPIRED = 'Сделка Deadline expired';
    //8. сделка будет просрочена через 1 день
    const DEADLINE_EXPIRED_IN_ONE_DAY = 'Сделка Deadline one day';
    //9. оплата по карте прошла
    const MANDARIN_ACQUIRING_SUCCESS = 'Сделка Оплата Мандарин success';
    //10. платеж обрабатывается
    const MANDARIN_ACQUIRING_PROCESSING = 'Сделка Оплата Мандарин processing';
    //11. оплата по карте не прошла
    const MANDARIN_ACQUIRING_FAILED = 'Сделка Оплата Мандарин processing';
    //12. сделка закрыта
    const CLOSED = 'Сделка Закрыта';


    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        $this->entityManager->getConnection()->rollBack();
        $this->entityManager->getConnection()->close();
        parent::tearDown();

        $this->entityManager = null;
        $this->baseAuthManager = null;

        gc_collect_cycles();
    }

    ///////////////////////////////////////////
    /// основные тесты для deal history ///
    //////////////////////////////////////////

    /////////// 1. сделка создана ////////////
    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealCreatedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CREATED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#your-civil-law-subject-and-payment-method-last');
        $this->assertQuery('#wait-for-counteragent-confirmation-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealCreatedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CREATED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#your-civil-law-subject-and-payment-method-last');
        $this->assertQuery('#confirmation-requiment-last');
        $this->assertQuery('#if-conditions-change-counteragent-reconsider-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealCreatedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CREATED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#you-created-deal');
        $this->assertQuery('#your-civil-law-subject-and-payment-method-last');
        $this->assertQuery('#wait-for-counteragent-confirmation');
        $this->assertQuery('#you-can-change-conditions-last');
    }

    /////////// 2. сделка подтверждена ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealConfirmedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CONFIRMED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions-last');
        $this->assertQuery('#wait-for-payment-from-customer-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealConfirmedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CONFIRMED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions-last');
        $this->assertQuery('#wait-for-payment-from-customer-last');
        $this->assertQuery('#warning-not-yet-confirmed-payment-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealConfirmedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CONFIRMED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#you-created-deal');
        $this->assertQuery('#deal-confirmed');
        $this->assertQuery('#deal-confirmed-last');
        $this->assertQuery('#payment-required');
        $this->assertQuery('#bank-payment-success-last');
        $this->assertQuery('#guarant-pay-security');
    }

    /////////// 3. сделка оплачена ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDebitMatchedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEBIT_MATCHED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#msg-payment-status-success');
        $this->assertQuery('#msg-payment-status-success-last');
        $this->assertQuery('#info-msg-contractor-must-fulfill-obligations-last');
        $this->assertQuery('#info-msg-money-transfer-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDebitMatchedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEBIT_MATCHED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#msg-payment-status-success');
        $this->assertQuery('#msg-payment-status-success-last');
        $this->assertQuery('#info-msg-contractor-must-fulfill-obligations-last');
        $this->assertQuery('#info-msg-money-transfer-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDebitMatchedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEBIT_MATCHED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#you-created-deal');
        $this->assertQuery('#deal-confirmed');
        $this->assertQuery('#msg_payment-status-success');
        $this->assertQuery('#msg_payment-status-success-last');
        $this->assertQuery('#contractor-must-fulfill-obligations-debit-matched-last');
        $this->assertQuery('#contractor-must-fulfill-obligations-debit-matched-last-after-check');
        $this->assertQuery('#you-can-open-dispute-customer-last');
        $this->assertQuery('#info_msg-automatic-timeout-close-debit-matched-last');
    }

    /////////// 4. сделка выполнена ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDeliveredForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DELIVERED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#msg-payment-status-success');
        $this->assertQuery('#customer-confirmed-obligations-fulfillment');
        $this->assertQuery('#customer-confirmed-obligations-fulfillment-last');
        $this->assertQuery('#need-to-prepare-payment-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDeliveredForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DELIVERED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#msg-payment-status-success');
        $this->assertQuery('#customer-confirmed-obligations-fulfillment');
        $this->assertQuery('#customer-confirmed-obligations-fulfillment-last');
        $this->assertQuery('#check-requisites-and-send-charge-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDeliveredForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DELIVERED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#you-created-deal');
        $this->assertQuery('#deal-confirmed');
        $this->assertQuery('#msg_payment-status-success');
        $this->assertQuery('#you-confirmed-obligations-fulfillment');
        $this->assertQuery('#you-confirmed-obligations-fulfillment-last');
        $this->assertQuery('#check-requisites-and-send-charge-last');
    }

    /////////// 5. деньги по сделке выплачены ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealPendingCreditPaymentForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PENDING_CREDIT_PAYMENT]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#deal-pending-credit-payment');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealPendingCreditPaymentForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PENDING_CREDIT_PAYMENT]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#deal-pending-credit-payment');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealPendingCreditPaymentForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PENDING_CREDIT_PAYMENT]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#you-created-deal');
        $this->assertQuery('#deal-confirmed');
        $this->assertQuery('#msg_payment-status-success');
        $this->assertQuery('#you-confirmed-obligations-fulfillment');
        $this->assertQuery('#deal-pending-credit-payment');
    }

    /////////// 6. проблемная сделка ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealTroubleForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TROUBLE]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealTroubleForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TROUBLE]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealTroubleForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TROUBLE]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#you-created-deal');
        $this->assertQuery('#deal-confirmed');
    }

    /////////// 7. сделка просрочена ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDeadlineExpiredForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEADLINE_EXPIRED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#msg-payment-status-success');
        $this->assertQuery('#deal-deadline-expired');
        $this->assertQuery('#deal-deadline-expired-last');
        $this->assertQuery('#deal-will-close-automatically-last');
        $this->assertQuery('#need-to-prepare-payment-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDeadlineExpiredForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEADLINE_EXPIRED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#msg-payment-status-success');
        $this->assertQuery('#deal-deadline-expired');
        $this->assertQuery('#deal-deadline-expired-last');
        $this->assertQuery('#deal-will-close-automatically-last');
        $this->assertQuery('#check-requisites-and-send-charge-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDeadlineExpiredForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEADLINE_EXPIRED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#you-created-deal');
        $this->assertQuery('#deal-confirmed');
        $this->assertQuery('#msg_payment-status-success');
        $this->assertQuery('#deal-deadline-expired');
        $this->assertQuery('#deal-deadline-expired-last');
        $this->assertQuery('#automatic-close-warning-last');
        $this->assertQuery('#check-requisites-and-send-charge-last');
    }

    /////////// 8. сделка будет просрочена через 1 день ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDeadlineExpiredInOneDayForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEADLINE_EXPIRED_IN_ONE_DAY]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
//        $this-> setOneDayDeadline();

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#msg-payment-status-success');
        $this->assertQuery('#deal-deadline-expired-in-one-day-last');
        $this->assertQuery('#automatic-deal-close-warning-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDeadlineExpiredInOneDayForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEADLINE_EXPIRED_IN_ONE_DAY]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#msg-payment-status-success');
        $this->assertQuery('#deal-deadline-expired-in-one-day-last');
        $this->assertQuery('#automatic-deal-close-warning-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealDeadlineExpiredInOneDayForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEADLINE_EXPIRED_IN_ONE_DAY]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#you-created-deal');
        $this->assertQuery('#deal-confirmed');
        $this->assertQuery('#msg_payment-status-success');
        $this->assertQuery('#deal-deadline-expired-in-one-day-last');
        $this->assertQuery('#automatic-deal-close-warning');
    }

    /////////// 9. оплата по карте прошла ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealMandarinAcquiringSuccessForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::MANDARIN_ACQUIRING_SUCCESS]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#mandarin-acquiring-success');
        $this->assertQuery('#mandarin-acquiring-success-last');
        $this->assertQuery('#info-msg-contractor-must-fulfill-obligations-last');
        $this->assertQuery('#info-msg-money-transfer-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealMandarinAcquiringSuccessForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::MANDARIN_ACQUIRING_SUCCESS]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#mandarin-acquiring-success');
        $this->assertQuery('#mandarin-acquiring-success-last');
        $this->assertQuery('#info-msg-contractor-must-fulfill-obligations-last');
        $this->assertQuery('#info-msg-money-transfer-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealMandarinAcquiringSuccessForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::MANDARIN_ACQUIRING_SUCCESS]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#you-created-deal');
        $this->assertQuery('#deal-confirmed');
        $this->assertQuery('#mandarin-acquiring-success');
        $this->assertQuery('#mandarin-acquiring-success-last');
        $this->assertQuery('#contractor-must-fulfill-obligations-mandarin-last');
        $this->assertQuery('#you-can-open-dispute-customer-last');
        $this->assertQuery('#info_msg-automatic-timeout-close-mandarin-last');
    }

    /////////// 10. платеж обрабатывается ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
//    public function testHistoryWithDealMandarinAcquiringProcessingForOperator()
//    {
//        // Залогиниваем пользователя operator
//        $this->login('operator');
//
//        /** @var Deal $deal */
//        $deal = $this->entityManager->getRepository(Deal::class)
//            ->findOneBy(['name' => self::MANDARIN_ACQUIRING_PROCESSING]);
//
//        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
//
//        $this->assertQuery('#counteragent-created-deal');
//        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
//        $this->assertQuery('#mandarin-acquiring-processing-last');
//    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
//    public function testHistoryWithDealMandarinAcquiringProcessingForContractor()
//    {
//        // Залогиниваем пользователя contractor
//        $this->login('test2');
//
//        /** @var Deal $deal */
//        $deal = $this->entityManager->getRepository(Deal::class)
//            ->findOneBy(['name' => self::MANDARIN_ACQUIRING_PROCESSING]);
//
//        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
//
//        $this->assertQuery('#mandarin-acquiring-processing');
//    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
//    public function testHistoryWithDealMandarinAcquiringProcessingForCustomer()
//    {
//        // Залогиниваем пользователя customer
//        $this->login('test');
//
//        /** @var Deal $deal */
//        $deal = $this->entityManager->getRepository(Deal::class)
//            ->findOneBy(['name' => self::MANDARIN_ACQUIRING_PROCESSING]);
//
//        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
//
//        $this->assertQuery('#you-created-deal');
//        $this->assertQuery('#counteragent-confirmed');
//        $this->assertQuery('#deal-confirmed');
//        $this->assertQuery('#MandarinPaymentStatusProcessing');
//    }

    /////////// 11. оплата по карте не прошла ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
//    public function testHistoryWithDealMandarinAcquiringFailedForOperator()
//    {
//        // Залогиниваем пользователя operator
//        $this->login('operator');
//
//        /** @var Deal $deal */
//        $deal = $this->entityManager->getRepository(Deal::class)
//            ->findOneBy(['name' => self::MANDARIN_ACQUIRING_FAILED]);
//
//        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
//
//        $this->assertQuery('#counteragent-created-deal');
//        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
//        $this->assertQuery('#mandarin-acquiring-failed-last');
//        $this->assertQuery('#wait-for-payment-mandarin-from-customer-last');
//        $this->assertQuery('#contact-hotline-last');
//    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
//    public function testHistoryWithDealMandarinAcquiringFailedForContractor()
//    {
//        // Залогиниваем пользователя contractor
//        $this->login('test2');
//
//        /** @var Deal $deal */
//        $deal = $this->entityManager->getRepository(Deal::class)
//            ->findOneBy(['name' => self::MANDARIN_ACQUIRING_FAILED]);
//
//        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
//
//        $this->assertQuery('#mandarin-acquiring-failed');
//    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
//    public function testHistoryWithDealMandarinAcquiringFailedForCustomer()
//    {
//        // Залогиниваем пользователя customer
//        $this->login('test');
//
//        /** @var Deal $deal */
//        $deal = $this->entityManager->getRepository(Deal::class)
//            ->findOneBy(['name' => self::MANDARIN_ACQUIRING_FAILED]);
//
//        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
//
//        $this->assertQuery('#you-created-deal');
//        $this->assertQuery('#deal-confirmed');
//        $this->assertQuery('#mandarin-acquiring-failed-last');
//        $this->assertQuery('#payment-required');
//        $this->assertQuery('#call-bank-hotline-last');
//    }

    /////////// 12. сделка закрыта ////////////

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealClosedForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CLOSED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#msg-payment-status-success');
        $this->assertQuery('#customer-confirmed-obligations-fulfillment');
        $this->assertQuery('#deal-closed');
        $this->assertQuery('#payment-sent');
        $this->assertQuery('#deal-closed-last');
        $this->assertQuery('#payment-sent-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealClosedForContractor()
    {
        // Залогиниваем пользователя contractor
        $this->login('test2');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CLOSED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#counteragent-created-deal');
        $this->assertQuery('#contractor-and-customer-confirmed-conditions');
        $this->assertQuery('#msg-payment-status-success');
        $this->assertQuery('#customer-confirmed-obligations-fulfillment');
        $this->assertQuery('#deal-closed');
        $this->assertQuery('#deal-closed-last');
        $this->assertQuery('#payment-sent-last');
        $this->assertQuery('#gratitude-for-using-service-last');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group deal_history
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testHistoryWithDealClosedForCustomer()
    {
        // Залогиниваем пользователя customer
        $this->login('test');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CLOSED]);

        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');

        $this->assertQuery('#you-created-deal');
        $this->assertQuery('#deal-confirmed');
        $this->assertQuery('#msg_payment-status-success');
        $this->assertQuery('#you-confirmed-obligations-fulfillment');
        $this->assertQuery('#deal-closed');
        $this->assertQuery('#deal-closed-last');
        $this->assertQuery('#payment-sent-last');
        $this->assertQuery('#gratitude-for-using-service');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}