<?php

namespace ApplicationTest\HTMLOutput;

use Application\Entity\Deal;
use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;


/**
 * Class HTMLOutputFormInitFromPagesTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputFormInitFromPagesTest extends AbstractHttpControllerTestCase
{
    const PAID_DEAL_NAME = 'Сделка Оплаченная';

    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Тестируем output form init для /deal/form-init
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testCheckFormInitOutputFromDeal()
    {
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/form-init', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        //основная проверка
        $this->assertResponseStatusCode(200);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal_statuses', $data['data']);
        $this->assertArrayHasKey('dispute_statuses', $data['data']);
        $this->assertArrayHasKey('deal_types', $data['data']);
        $this->assertArrayHasKey('fee_payer_options', $data['data']);
        $this->assertArrayHasKey('naturalPersons', $data['data']);
        $this->assertArrayHasKey('legalEntities', $data['data']);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertArrayHasKey('deal_form', $data['data']);
    }

    /**
     * Тестируем output form init для /deal/form-init?idDeal={id}
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testCheckFormInitOutputFromDealWithDealId()
    {
        // Залогиниваем пользователя test
        $this->login();

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PAID_DEAL_NAME]);

        $this->assertNotNull($deal);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/form-init?idDeal='.$deal->getId(), 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        //основная проверка
        $this->assertResponseStatusCode(200);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal_statuses', $data['data']);
        $this->assertArrayHasKey('dispute_statuses', $data['data']);
        $this->assertArrayHasKey('deal_types', $data['data']);
        $this->assertArrayHasKey('fee_payer_options', $data['data']);
        $this->assertArrayHasKey('naturalPersons', $data['data']);
        $this->assertArrayHasKey('legalEntities', $data['data']);
        $this->assertArrayHasKey('deal', $data['data']);
        //output deal_form
        $this->assertArrayHasKey('deal_form', $data['data']);
        $deal_form = $data['data']['deal_form'];
        $this->assertNotNull($deal_form);
        $this->assertArrayHasKey('id', $deal_form);
        $this->assertArrayHasKey('deal_type', $deal_form);
        $this->assertArrayHasKey('name', $deal_form);
        $this->assertArrayHasKey('addon_terms', $deal_form);
        $this->assertArrayHasKey('amount', $deal_form);
        $this->assertArrayHasKey('fee_payer_option', $deal_form);
        $this->assertArrayHasKey('delivery_period', $deal_form);
        $this->assertArrayHasKey('deal_civil_law_subject', $deal_form);
        $this->assertArrayHasKey('payment_method', $deal_form);
        $this->assertArrayHasKey('counteragent_email', $deal_form);
        $this->assertArrayHasKey('deal_number', $deal_form);
        $this->assertArrayHasKey('is_owner', $deal_form);
    }

    /**
     * Тестируем output form init для /dispute/form-init
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testCheckFormInitOutputFromDispute()
    {
        // Залогиниваем пользователя test
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/dispute/form-init', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);
        //основная проверка
        $this->assertResponseStatusCode(200);

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('deal_statuses', $data['data']);
        $this->assertArrayHasKey('dispute_statuses', $data['data']);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertArrayHasKey('dispute', $data['data']);
        $this->assertArrayHasKey('customer', $data['data']);
        $this->assertArrayHasKey('deal', $data['data']);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}