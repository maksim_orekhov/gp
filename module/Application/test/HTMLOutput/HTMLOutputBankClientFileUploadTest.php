<?php

namespace ApplicationTest\Controller;

use Application\Controller\BankClientController;
use Application\Controller\DealController;
use Application\Form\FileUploadForm;
use Application\Service\BankClient\BankClientManager;
use Core\Service\Base\BaseRoleManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\User;
use Application\Entity\Deal;
use Application\Entity\DealType;
use ModuleFileManager\Entity\File;
use Application\Entity\FeePayerOption;
use Application\Service\Parser\BankClientParser;
use Zend\ServiceManager\ServiceManager;
use Application\Entity\Payment;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealAgentManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Payment\PaymentMethodManager;
use Application\Listener\CreditPaymentOrderAvailabilityListener;
use CoreTest\ViewVarsTrait;

/**
 * Class BankClientControllerTest
 * @package ApplicationTest\Controller
 *
 * Важно! На момент запуска этих тестов, папка bank-client-stock не должна содержать файлы.
 * Тесты сами помещают в нее необходимые тестовые файлы и после удаляеют их.
 */
class HTMLOutputBankClientFileUploadTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var string
     */
    public $main_upload_folder;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\BankClient\BankClientManager
     */
    public $bankClientManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \ModuleFileManager\Service\FileManager
     */
    private $fileManager;

    /**
     * @var BankClientParser
     */
    private $bankClientParser;

    /**
     * @var \Application\Service\Deal\DealManager
     */
    private $dealManager;

    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var CreditPaymentOrderAvailabilityListener
     */
    private $creditPaymentOrderAvailabilityListener;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;


    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        parent::setUp();

        $serviceManager = $this->getApplicationServiceLocator();

        $this->configureServiceManager($serviceManager);

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->bankClientManager = $serviceManager->get(\Application\Service\BankClient\BankClientManager::class);
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->fileManager = $serviceManager->get(\ModuleFileManager\Service\FileManager::class);
        $this->bankClientParser = $serviceManager->get(BankClientParser::class);
        $this->dealManager = $serviceManager->get(\Application\Service\Deal\DealManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $config = $this->getApplicationConfig();

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->dealAgentManager = $serviceManager->get(DealAgentManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->paymentManager = $serviceManager->get(PaymentManager::class);
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);
        $this->creditPaymentOrderAvailabilityListener = $serviceManager->get(CreditPaymentOrderAvailabilityListener::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        #$this->entityManager->getConnection()->beginTransaction();
    }

    protected function configureServiceManager(ServiceManager $services)
    {
        $services->setAllowOverride(true);
        $services->setService('Config', $this->updateConfig($services->get('Config')));
        $services->setAllowOverride(false);
    }

    protected function updateConfig($config)
    {
        $config['email_providers'] = ['message_send_simulation' => true];

        return $config;
    }


    public function tearDown()
    {
        // Transaction rollback
        #$this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Доступность /credit-unpaid контроллера bankClient
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testBankClientFileStockUploadForOperator()
    {
        // доступно только оператору по +bank-client.execute
        $this->assignRoleToTestUser('Operator');
        $this->login();
        /** @var BankClientManager $paymentTransactions */
        $paymentTransactions = $this->bankClientManager->getReadyOutgoingPaymentOrders();
        $this->dispatch('/bank-client/file-upload', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankClientController::class);
        $this->assertControllerClass('BankClientController');
        $this->assertMatchedRouteName('bank-client/file-upload');
        $this->assertQuery('#bank-client-file-upload');
        $this->assertQuery('#section_bank_client_file_upload');

    }

    /**
     * Недоступность   не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testBankClientFileStockUploadToNonAuthorised()
    {

        $this->dispatch('/bank-client/file-upload', 'GET');
        $this->assertMatchedRouteName('bank-client/file-upload');
        $this->assertResponseStatusCode(302);

    }

    /**
     * Недоступность  авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testBankClientFileStockUploadToAuthorised()
    {
        // доступно только оператору по +bank-client.execute
        $this->assignRoleToTestUser('Verified');
        $this->login();
        $this->dispatch('/bank-client/file-upload', 'GET');
        $this->assertMatchedRouteName('bank-client/file-upload');
        $this->assertResponseStatusCode(302);

    }

    /**
     * Недоступность  Unverified пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group bank-client
     * @throws \Exception
     */
    public function testBankClientFileStockUploadToUnverified()
    {
        // доступно только оператору по +bank-client.execute
        $this->assignRoleToTestUser('Unverified');
        $this->login();
        $this->dispatch('/bank-client/file-upload', 'GET');
        $this->assertMatchedRouteName('bank-client/file-upload');
        $this->assertResponseStatusCode(302);

    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     */
    private function assignRoleToTestUser(string $role_name, User $user = null)
    {
        if (!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var \Application\Entity\Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя test
     */
    private function login()
    {
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);
    }

    /**
     * @param $source_url
     * @param $destination
     */
    private function saveFile($source_url, $destination)
    {
        copy($source_url, $destination);
    }

    /**
     * @param $path
     */
    private function removeFile($path)
    {
        unlink($path);
    }

    /**
     * @param string $deal_name
     * @param string $deal_role
     * @param $amount
     * @return Deal
     */
    private function createTestDeal(string $deal_name, string $deal_role, $amount)
    {
        // Create CivilLawSubject
        $params = [
            'first_name' => 'Александр',
            'secondary_name' => 'Александрович',
            'last_name' => 'Александров',
            'birth_date' => date('Y-m-d'),
        ];
        $this->civilLawSubjectManager->createNaturalPerson($this->user, $params);

        // Create paymentMethod и paymentMethodBankTransfer
        $civilLawSubject = $this->civilLawSubjectManager->create($this->user);
        $params = [
            'name' => 'Имя',
            'bank_name' => 'ПАО БАНК САНКТ-ПЕТЕРБУРГ',
            'bik' => '111222',
            'account_number' => '22233344455566677788',
            'payment_recipient_name' => 'Guarant Pay',
            'inn' => '666777',
            'kpp' => '777888',
            'corr_account_number' => '30101810900000000790'
        ];
        $bankTransfer = $this->paymentMethodManager->createPaymentMethodBankTransfer($params, $civilLawSubject);
        $paymentMethod = $bankTransfer->getPaymentMethod();

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $data = [
            'name' => $deal_name,
            'amount' => $amount,
            'deal_role' => $deal_role,
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->last()->getId(),
            'payment_method' => '',
            'counteragent_email' => 'test2@test.net',
            'deal_type' => $dealType->getId(),
            'fee_payer_option' => $feePayerOption->getId(),
            'addon_terms' => 'Дополнительные условия',
            'delivery_period' => 15,
            'deal_agent_confirm' => 1
        ];

        // Try to register dill
        // Распределение ролей в сделке взависимости от выбранной инициатором роли
        if ($data['deal_role'] == 'customer') {
            $userCustomer = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $userContractor = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $paramCustomer = $data;
            $paramContractor = [];
        } else {
            $userCustomer = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $userContractor = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $paramCustomer = [];
            $paramContractor = $data;
        }

        $dealAgentCustomer = $this->dealAgentManager->createDealAgent($userCustomer, $paramCustomer);
        $dealAgentContractor = $this->dealAgentManager->createDealAgent($userContractor, $paramContractor);

        $user = $this->userManager->selectUserByLogin($this->authService->getIdentity());

        $dealOwner = $this->dealAgentManager->defineDealOwner($user, $dealAgentCustomer, $dealAgentContractor);

        // Create Deal
        $deal = $this->dealManager->createDeal($data, $dealAgentCustomer, $dealAgentContractor, $dealOwner);

        $deal->getContractor()->setDealAgentConfirm(true);
        $deal->getCustomer()->setDealAgentConfirm(true);

        /** @var Payment $payment */
        $payment = $this->paymentManager->getPaymentByDeal($deal);
        $payment->setPaymentMethod($paymentMethod);

        $this->entityManager->flush();

        return $deal;
    }
}