<?php

namespace ApplicationTest\HTMLOutput;

use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\Deal;

/**
 * Class HTMLOutputDealTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputDealDeadlineTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DEAL_WITH_DISCOUNT_REQUEST_NAME = 'Сделка Покупатель направил запрос на Скидку';
    const DEAL_WITH_OVERPAY_NAME = 'Сделка Переплата';
    const DEAL_WITH_OVERTIME_NAME = 'Сделки с истекающим сроком';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /**
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @group overpay
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testDealDeadline()
    {
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);


        $this->login('operator');

        $this->assertNotNull($deal);
        $this->dispatch('/deal-deadline/notify', 'GET');

        $this->assertMatchedRouteName('deal-deadline-notify');

        $this->assertQuery('#section-deal-deadline');
        $this->assertQuery('#notify-deadline-form');
        $this->assertQuery('#TableDealsDeadline');
    }


    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login = null, string $password = null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}