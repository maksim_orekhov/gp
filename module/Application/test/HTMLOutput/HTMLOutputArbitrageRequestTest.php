<?php

namespace ApplicationTest\HTMLOutput;

use Application\Controller\ArbitrageController;
use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\User;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\Deal;

/**
 * Class HTMLOutputArbitrageRequestTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputArbitrageRequestTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /**
     * Тестируем рендеринг формы спора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testArbitrageRequest()
    {
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Спор открыт']);


        /** @var User $user */
        $user = $deal->getCustomer()->getUser();
        // Залогиниваем участника сжедки
        $this->login($user->getLogin());

        // Есть спор
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);

        $this->dispatch('/deal/' . $deal->getId() . '/arbitrage/request', 'GET');

      //  $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(ArbitrageController::class);
        $this->assertMatchedRouteName('deal-arbitrage-request');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('arbitrageRequestForm', $view_vars);
        $this->assertArrayHasKey('dispute', $view_vars);
        $this->assertArrayHasKey('deal', $view_vars);
        //проверяем отгрузилась ли форма
        $this->assertQuery('#section_arbitrage_request');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login = null, string $password = null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}