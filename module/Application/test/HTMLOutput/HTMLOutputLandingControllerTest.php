<?php
namespace ApplicationTest\HTMLOutput;

use Application\Controller\LandingController;
use ApplicationTest\Bootstrap;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class HTMLOutputLandingControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testLandingAction()
    {
        $this->dispatch('/', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('home');
        // Контент
        $this->assertQuery('#phpunit-test-landing');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testPolicyAction()
    {
        $this->dispatch('/policy', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('policy');

        // Контент
        $this->assertQuery('#phpunit-test-policy');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testConnectionContractAction()
    {
        $this->dispatch('/connection-contract', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('connection-contract');
        // Контент
        $this->assertQuery('#phpunit-test-connection-contract');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testTermsOfUseAction()
    {
        $this->dispatch('/terms-of-use', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('terms-of-use');
        // Контент
        $this->assertQuery('#phpunit-test-terms-of-use');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testHowItWorksAction()
    {
        $this->dispatch('/how-it-works', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('how-it-works');
        // Контент
        $this->assertQuery('#phpunit-test-how-it-works');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testForWhomAction()
    {
        $this->dispatch('/for-whom', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('for-whom');
        // Контент
        $this->assertQuery('#phpunit-test-for-whom');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testWhatCanBuyAction()
    {
        $this->dispatch('/what-can-buy', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('what-can-buy');
        // Контент
        $this->assertQuery('#phpunit-test-what-can-buy');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testDisputeResolutionAction()
    {
        $this->dispatch('/dispute-resolution', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('dispute-resolution');
        // Контент
        $this->assertQuery('#phpunit-test-dispute-resolution');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testDisputeConsiderationRulesAction()
    {
        $this->dispatch('/dispute-consideration-rules', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('dispute-consideration-rules');
        // Контент
        $this->assertQuery('#phpunit-test-dispute-consideration-rules');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testContactsAction()
    {
        $this->dispatch('/contacts', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('contacts');
        // Контент
        $this->assertQuery('#phpunit-test-contacts');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testForCustomersAction()
    {
        $this->dispatch('/for-customers', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('for-customers');
        // Контент
        $this->assertQuery('#phpunit-test-for-customers');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testForContractorsAction()
    {
        $this->dispatch('/for-contractors', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('for-contractors');
        // Контент
        $this->assertQuery('#phpunit-test-for-contractors');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testForBusinessAction()
    {
        $this->dispatch('/for-business', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('for-business');
        // Контент
        $this->assertQuery('#phpunit-test-for-business');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testECommerceAction()
    {
        $this->dispatch('/e-commerce', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('e-commerce');
        // Контент
        $this->assertQuery('#phpunit-test-e-commerce');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testForPartnersAction()
    {
        $this->dispatch('/for-partners', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('for-partners');
        // Контент
        $this->assertQuery('#phpunit-test-for-partners');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     * @throws \Exception
     *
     * @group landing
     * @group HTMLOutput
     */
    public function testInformationAction()
    {
        $this->dispatch('/information', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(LandingController::class);
        $this->assertMatchedRouteName('information');
        // Контент
        $this->assertQuery('#phpunit-test-information');
    }
}
