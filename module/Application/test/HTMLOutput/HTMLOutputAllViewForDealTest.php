<?php

namespace ApplicationTest\Controller;

use Application\Controller\DealController;
use Application\Entity\RepaidOverpay;
use Application\Form\RepaidOverpayForm;
use Core\Service\Base\BaseRoleManager;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Form\DealSearchForm;
use Application\Form\DisputeForm;
use ModulePaymentOrder\Entity\PaymentOrder;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use Application\Entity\Deal;
use Application\Entity\User;
use Application\Entity\Payment;
use Application\Entity\SystemPaymentDetails;
use Application\Entity\CivilLawSubject;
use Application\Entity\PaymentMethod;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealAgentManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\BankClient\BankClientManager;
use Application\Service\Parser\BankClientParser;
use Application\Controller\BankClientController;
use Application\Service\Deal\DealPolitics;
use CoreTest\ViewVarsTrait;

/**
 * Class DealControllerTest
 * @package ApplicationTest\Controller
 */
class HTMLOutputAllViewForDealTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const MINIMUM_DELIVERY_PERIOD = 7;
    const TEXT_DEAL_NAME = 'Сделка Фикстура';
    const TEXT_DEAL_PAYED_NAME = 'Сделка Оплаченная';
    const TEXT_DISPUTE_DEAL_NAME = 'Сделка Debit Matched'; // Заменить использование на DEAL_DEBIT_MATCHED_NAME
    const DEAL_DEBIT_MATCHED_NAME = 'Сделка Debit Matched';
    const DEAL_WITH_OVERPAY_NAME = 'Сделка Переплата';
    const DEAL_WITH_CONVERSATION_NAME = 'Сделка Переговорная';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var BankClientManager
     */
    public $bankClientManager;

    /**
     * @var BankClientParser
     */
    private $bankClientParser;

    /**
     * @var DealPolitics
     */
    private $dealPolitics;

    /**
     * @var string
     */
    public $main_upload_folder;

    /**
     * @var BaseRoleManager
     */
    public $baseRoleManager;


    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        // Отключаем отправку уведомлений на email
        $this->resetConfigParameters();

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->dealAgentManager = $serviceManager->get(DealAgentManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->paymentManager = $serviceManager->get(PaymentManager::class);
        $this->bankClientManager = $serviceManager->get(\Application\Service\BankClient\BankClientManager::class);
        $this->bankClientParser = $serviceManager->get(BankClientParser::class);
        $this->dealPolitics = $serviceManager->get(DealPolitics::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /////////////// Index

    /**
     * Недоступность /deal не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessedToNonAuthorised()
    {
        $this->dispatch('/deal', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/login?redirectUrl=/deal');
    }

    /**
     * Недоступность /deal авторизованному пользователю без роли Verified
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group deal
     * @throws \Exception
     */
    public function testIndexActionCanNotBeAccessedUnverifiedUser()
    {
        $this->assignRoleToTestUser('Unverified');
        $this->login();

        $this->dispatch('/deal', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertControllerName(DealController::class);
        $this->assertRedirectTo('/not-authorized');
    }


    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testDealAbnornal()
    {

        $this->login('operator');
        $this->dispatch('/deal/abnormal', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/abnormal');
        $this->assertQuery('#TableDealsTrouble');
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testDealConfirmConditions()
    {
        // доступно только оператору по +bank-client.execute
        $this->assignRoleToTestUser('Verified');
        // $this->login();
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_CONVERSATION_NAME]);

        $this->login('test');
        $this->dispatch('/deal/' . $deal->getId() . '/confirm-conditions', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-confirm-conditions');
        $this->assertQuery('#deal_confirm_conditions');
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testDealCreate()
    {
        // доступно только оператору по +bank-client.execute
        $this->assignRoleToTestUser('Verified');

        $this->login('test');
        $this->dispatch('/deal/create', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/create');
        $this->assertQuery('#deal_create_section');
        $this->assertQuery('#deal-create-form');

    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testCreateCivilLawSubject()
    {

        $this->assignRoleToTestUser('Verified');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TEXT_DEAL_PAYED_NAME]);

        $this->login('test');
        $this->dispatch('/deal/' . $deal->getId() . '/civil-law-subject', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-civil-law-subject');
        $this->assertQuery('#FormNaturalPersonSingle1');
        $this->assertQuery('#FormLegalEntity1');

    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testPaymentMethod()
    {

        $this->assignRoleToTestUser('Verified');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Оплаченная']);

        $this->login('test');
        $this->dispatch('/deal/' . $deal->getId() . '/payment-method', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-payment-method');
        $this->assertQuery('#form_payment_method');
        $this->assertQuery('#section_payment_method');
        $this->assertQuery('#form-bank-transfer');

    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testTrackingNumber()
    {

        $this->assignRoleToTestUser('Operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => 'Сделка Оплаченная']);

        $this->login('test');
        $this->dispatch('/deal/' . $deal->getId() . '/tracking-number', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-create-tracking-number');
        $this->assertQuery('#section_tracking_number');
        $this->assertQuery('#tracking-number-form');

    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testDealDeleteForm()
    {

        $this->assignRoleToTestUser('Verified');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TEXT_DEAL_NAME]);

        $this->login($deal->getCustomer()->getUser()->getLogin());

        $this->dispatch('/deal/' . $deal->getId() . '/delete', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal-form-delete');
        $this->assertQuery('#section-deal-form-delete');
        $this->assertQuery('#deal-delete-form');

    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testDealEditeForm()
    {

        $this->assignRoleToTestUser('Verified');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TEXT_DEAL_NAME]);

        $this->login($deal->getCustomer()->getUser()->getLogin());

        $this->dispatch('/deal/edit/' . $deal->getId(), 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/edit');
        $this->assertQuery('#section-edit-dial');
        $this->assertQuery('#deal-edit-form');

    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testDealExpired()
    {

        $this->assignRoleToTestUser('Operator');
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/deal/expired', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/expired');
        $this->assertQuery('#section-deal-expried');
        $this->assertQuery('#TableDealsExpired');

    }
    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testDealFileUpload()
    {

        $this->assignRoleToTestUser('Verified');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TEXT_DEAL_NAME]);

        $this->login($deal->getCustomer()->getUser()->getLogin());

       $postData = [
            'deal_id' => $deal->getId(),

        ];
        $this->dispatch('/deal/file-upload', 'GET',$postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/file-upload');
        $this->assertQuery('#section-deal-file-upload');
        $this->assertQuery('#deal-file-upload');

    }
    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testDealOverpay()
    {

        $this->assignRoleToTestUser('Operator');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $this->login($deal->getCustomer()->getUser()->getLogin());


        $this->dispatch('/deal/overpay/'.$deal->getId(), 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/overpay');
        $this->assertQuery('#OverpayPage');
        $this->assertQuery('#repaid-overpay-form');

    }
    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testDealShowFormForOperator()
    {

        $this->assignRoleToTestUser('Operator');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $this->login($deal->getCustomer()->getUser()->getLogin());


        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/show');
        $this->assertQuery('#section-deal-show-page');
        $this->assertQuery('#deal-comment-form');

    }
    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testDealShowFormForVerified()
    {

        $this->assignRoleToTestUser('Verified');
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_OVERPAY_NAME]);

        $this->login($deal->getCustomer()->getUser()->getLogin());
        $this->dispatch('/deal/show/'.$deal->getId(), 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(DealController::class);
        $this->assertMatchedRouteName('deal/show');
        $this->assertQuery('#section-deal-show-page');
        //not show this form
        $this->assertNotQuery('#deal-comment-form');

    }
    /**
     * @param Deal $deal
     * @param Payment $payment
     * @param string $path
     * @return BankClientPaymentOrder
     */
    private function createPaymentOrderFromFile(Deal $deal, Payment $payment, string $path)
    {
//        /** @var PaymentMethodBankTransfer $customerBankTransfer */ // Реквизиты customer'а
//        $customerBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
//            ->findOneBy(array('paymentRecipientName' => 'Тест Тестов'));

        /** @var SystemPaymentDetails $systemPaymentDetails */ // Реквизиты GP
        $systemPaymentDetails = $this->entityManager->getRepository(SystemPaymentDetails::class)
            ->findOneBy(array('name' => 'ООО "Гарант Пэй"'));

        // Creates file if not exists
        $file_with_payment_order = fopen($path, "w") or die("Не удается создать файл");
        // Write file header data
        fwrite($file_with_payment_order, mb_convert_encoding(
                BankClientManager::FILE_HEADER,
                BankClientController::ORIGINAL_ENCODING, 'UTF-8')
        );
        // Main body of PaymentOrder
        $currentDate = new \DateTime();
        $txt = "СекцияДокумент=" . BankClientManager::DOCUMENT_TYPE . PHP_EOL;
        $txt .= "Номер=" . $deal->getId() . PHP_EOL;
        $txt .= "Дата=" . $currentDate->format('d.m.Y') . PHP_EOL;
        $txt .= "ДатаСписано=" . $currentDate->format('d.m.Y') . PHP_EOL;
        $txt .= "Сумма=" . $payment->getExpectedValue() . PHP_EOL;
        $txt .= "ПлательщикСчет=40702810090030001128" . PHP_EOL;
        $txt .= "Плательщик=Тест Тестов" . PHP_EOL;
        $txt .= "ПлательщикИНН=" . PHP_EOL;
        $txt .= "Плательщик1=Тест Тестов" . PHP_EOL;
        $txt .= "ПлательщикКПП=" . PHP_EOL;
        $txt .= "ПлательщикБанк1=ВТБ" . PHP_EOL;
        $txt .= "ПлательщикБИК=044525201" . PHP_EOL;
        $txt .= "ПлательщикКорсчет=30101810900000000790" . PHP_EOL;
        $txt .= "ПолучательСчет=" . $systemPaymentDetails->getAccountNumber() . PHP_EOL;
        $txt .= "Получатель=ИНН " . $systemPaymentDetails->getInn() . " " . $systemPaymentDetails->getPaymentRecipientName() . PHP_EOL;
        $txt .= "ПолучательИНН=" . $systemPaymentDetails->getInn() . PHP_EOL;
        $txt .= "Получатель1=" . $systemPaymentDetails->getPaymentRecipientName() . PHP_EOL;
        $txt .= "ПолучательКПП=" . $systemPaymentDetails->getKpp() . PHP_EOL;
        $txt .= "ПолучательБанк1=" . $systemPaymentDetails->getBank() . PHP_EOL;
        $txt .= "ПолучательБИК=" . $systemPaymentDetails->getBik() . PHP_EOL;
        $txt .= "ПолучательКорсчет=" . $systemPaymentDetails->getCorrAccountNumber() . PHP_EOL;
        $txt .= "НазначениеПлатежа=" . BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY . $deal->getId() . PHP_EOL;
        $txt .= "ВидОплаты=01" . PHP_EOL;
        $txt .= "Очередность=5" . PHP_EOL;
        // Write main body
        fwrite($file_with_payment_order, mb_convert_encoding($txt, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
        // Write SECTION_END_POINT
        fwrite($file_with_payment_order, mb_convert_encoding(BankClientParser::SECTION_END_POINT . PHP_EOL,
            BankClientController::ORIGINAL_ENCODING, 'UTF-8'));

        // Генерируем файл с правильным параметром НазначениеПлатежа
        // Get data as array
        $payment_order_data = $this->bankClientParser->execute($path);
        // Create PaymentOrder
        /** @var BankClientPaymentOrder $paymentOrder */
        $paymentOrder = $this->bankClientManager->createNewPaymentOrderFromArray(
            $payment_order_data[0]
        );

        $this->entityManager->flush();

        return $paymentOrder;
    }

    /**
     * @param $login
     * @param $email
     * @param $phone
     * @param $password
     * @return int|null
     */
    private function createNewUser($login, $email, $phone, $password)
    {
        $data = [
            'login' => $login,
            'phone' => $phone,
            'email' => $email,
            'password' => $password,
        ];

        return $this->userManager->addNewUser($data);
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     */
    private function assignRoleToTestUser(string $role_name, User $user = null)
    {
        if (!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login = null, string $password = null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param string $deal_name
     * @param string $deal_role
     * @param $amount
     * @param bool $confirmed
     * @return Deal
     */
    private function createTestDeal(string $deal_name, string $deal_role, $amount, $confirmed = false)
    {
        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));
        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $data = [
            'name' => $deal_name,
            'amount' => $amount,
            'deal_role' => $deal_role,
            'deal_civil_law_subject' => $this->user->getCivilLawSubjects()->last()->getId(),
            'payment_method' => '',
            'counteragent_email' => 'test2@simple-technology.ru',
            'deal_type' => $dealType->getId(),
            'fee_payer_option' => $feePayerOption->getId(),
            'addon_terms' => 'Дополнительные условия',
            'delivery_period' => self::MINIMUM_DELIVERY_PERIOD + 5,
            'deal_agent_confirm' => 0
        ];

        // Try to register dill
        // Распределение ролей в сделке взависимости от выбранной инициатором роли
        if ($data['deal_role'] == 'customer') {
            $userCustomer = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $userContractor = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $paramCustomer = $data;
            $paramContractor = [];
        } else {
            $userCustomer = $this->dealAgentManager->getUserForDealAgent($data['counteragent_email']);
            $userContractor = $this->dealAgentManager->getUserForDealAgent($this->authService->getIdentity());
            $paramCustomer = [];
            $paramContractor = $data;
        }

        $dealAgentCustomer = $this->dealAgentManager->createDealAgent($userCustomer, $paramCustomer);
        $dealAgentContractor = $this->dealAgentManager->createDealAgent($userContractor, $paramContractor);

        $user = $this->userManager->selectUserByLogin($this->authService->getIdentity());

        $dealOwner = $this->dealAgentManager->defineDealOwner($user, $dealAgentCustomer, $dealAgentContractor);

        // Create Deal
        $deal = $this->dealManager->createDeal($data, $dealAgentCustomer, $dealAgentContractor, $dealOwner);

        if ($confirmed) {
            $dealAgentCustomer->setDealAgentConfirm(true);
            $dealAgentContractor->setDealAgentConfirm(true);

            $this->entityManager->flush();
        }

        return $deal;
    }

    /**
     * Подтверждение тестовой фикстурной сделки
     */
    private function confirmAndReturnTestFixtureDeal()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => self::TEXT_DEAL_NAME));

        $deal->getCustomer()->setDealAgentConfirm(true);
        $deal->getContractor()->setDealAgentConfirm(true);
        $deal->getContractor()->setDateOfConfirm(new \DateTime());

        $this->entityManager->flush();

        return $deal;
    }

    /**
     * @param $path
     */
    private function removeFile($path)
    {
        unlink($path);
    }
}