<?php

namespace ApplicationTest\Controller;

use Application\Controller\BankClientController;
use Application\Controller\DealController;
use Application\Controller\LegalEntityController;
use Application\Entity\CivilLawSubject;
use Application\Entity\LegalEntity;
use Application\Entity\NdsType;
use Application\Form\FileUploadForm;
use Application\Service\BankClient\BankClientManager;
use Core\Service\Base\BaseRoleManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Application\Entity\Role;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Application\Entity\User;
use Application\Entity\Deal;
use Application\Entity\DealType;
use ModuleFileManager\Entity\File;
use Application\Entity\FeePayerOption;
use Application\Service\Parser\BankClientParser;
use Zend\ServiceManager\ServiceManager;
use Application\Entity\Payment;
use Application\Service\Deal\DealManager;
use Application\Service\Deal\DealAgentManager;
use Application\Service\CivilLawSubject\CivilLawSubjectManager;
use Application\Service\Payment\PaymentManager;
use Application\Service\Payment\PaymentMethodManager;
use Application\Listener\CreditPaymentOrderAvailabilityListener;
use CoreTest\ViewVarsTrait;

/**
 * Class BankClientControllerTest
 * @package ApplicationTest\Controller
 *
 * Важно! На момент запуска этих тестов, папка bank-client-stock не должна содержать файлы.
 * Тесты сами помещают в нее необходимые тестовые файлы и после удаляеют их.
 */
class HTMLOutputLegalEntityTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var string
     */
    public $main_upload_folder;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\BankClient\BankClientManager
     */
    public $bankClientManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \ModuleFileManager\Service\FileManager
     */
    private $fileManager;

    /**
     * @var BankClientParser
     */
    private $bankClientParser;

    /**
     * @var \Application\Service\Deal\DealManager
     */
    private $dealManager;

    /**
     * @var DealAgentManager
     */
    private $dealAgentManager;

    /**
     * @var CivilLawSubjectManager
     */
    private $civilLawSubjectManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var CreditPaymentOrderAvailabilityListener
     */
    private $creditPaymentOrderAvailabilityListener;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;


    public function disablePhpUploadCapabilities()
    {
        require_once __DIR__ . '/../TestAsset/DisablePhpUploadChecks.php';
    }

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        parent::setUp();

        $serviceManager = $this->getApplicationServiceLocator();

        $this->configureServiceManager($serviceManager);

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->bankClientManager = $serviceManager->get(\Application\Service\BankClient\BankClientManager::class);
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->fileManager = $serviceManager->get(\ModuleFileManager\Service\FileManager::class);
        $this->bankClientParser = $serviceManager->get(BankClientParser::class);
        $this->dealManager = $serviceManager->get(\Application\Service\Deal\DealManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $config = $this->getApplicationConfig();

        $this->main_upload_folder = $config['file-management']['main_upload_folder'];
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->dealManager = $serviceManager->get(DealManager::class);
        $this->dealAgentManager = $serviceManager->get(DealAgentManager::class);
        $this->civilLawSubjectManager = $serviceManager->get(CivilLawSubjectManager::class);
        $this->paymentManager = $serviceManager->get(PaymentManager::class);
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);
        $this->creditPaymentOrderAvailabilityListener = $serviceManager->get(CreditPaymentOrderAvailabilityListener::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    protected function configureServiceManager(ServiceManager $services)
    {
        $services->setAllowOverride(true);
        $services->setService('Config', $this->updateConfig($services->get('Config')));
        $services->setAllowOverride(false);
    }

    protected function updateConfig($config)
    {
        $config['email_providers'] = ['message_send_simulation' => true];

        return $config;
    }


    public function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }


    /**
     * @throws \Exception
     * only for operator
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testLegalEntityCollection()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();
        $this->dispatch('/legal-entity', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity');
        $this->assertQuery('#section-legal-entity');
    }

    /**
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testLegalEntityCollectionForVerified()
    {

        $this->assignRoleToTestUser('Verified');
        $this->login();
        $this->dispatch('/legal-entity', 'GET');
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('legal-entity');

    }

    /**
     * @throws \Exception
     * only for operator
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testLegalEntityCreateForm()
    {
        $this->assignRoleToTestUser('Operator');
        $this->login();
        $this->dispatch('/profile/legal-entity/create', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-create');
        $this->assertQuery('#legal-entity-create-form');
    }

    /**
     * @throws \Exception
     * only for operator
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testLegalEntityDeleteForm()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '0']);
        /** @var  CivilLawSubject $civilLaw */
        $civilLaw = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['id' => '1']);
        $LegalEntyty = new LegalEntity();
        $LegalEntyty->setInn('test');
        $LegalEntyty->setKpp('test');
        $LegalEntyty->setName('test');
        $LegalEntyty->setPhone('8800200600');
        $civilLaw->setNdsType($ndsType);
        $LegalEntyty->setLegalAddress('bruansk');
        $LegalEntyty->setCivilLawSubject($civilLaw);
        $this->entityManager->persist($LegalEntyty);
        $this->entityManager->flush();
        /** @var LegalEntity $LegalEntyty */
        $LegalEntyty = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(['name' => 'test']);
        $entity_id = $LegalEntyty->getId();
        $this->assignRoleToTestUser('Operator');
        $this->login();
        $this->dispatch('/profile/legal-entity/' . $entity_id . '/delete', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-delete');
        $this->assertQuery('#section-legal-entity-delete');
        $this->assertQuery('#legal-entity-delete-form');

    }

    /**
     * @throws \Exception
     * only for operator
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testLegalEntityEditeForm()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '0']);
        /** @var  CivilLawSubject $civilLaw */
        $civilLaw = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['id' => '1']);
        $LegalEntyty = new LegalEntity();
        $LegalEntyty->setInn('test');
        $LegalEntyty->setKpp('test');
        $LegalEntyty->setName('test');
        $LegalEntyty->setPhone('8800200600');
        $civilLaw->setNdsType($ndsType);
        $LegalEntyty->setLegalAddress('bruansk');
        $LegalEntyty->setCivilLawSubject($civilLaw);
        $this->entityManager->persist($LegalEntyty);
        $this->entityManager->flush();
        /** @var LegalEntity $LegalEntyty */
        $LegalEntyty = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(['name' => 'test']);
        $entity_id = $LegalEntyty->getId();
        $this->assignRoleToTestUser('Operator');
        $this->login();
        $this->dispatch('/profile/legal-entity/' . $entity_id . '/edit', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-form-edit');
        $this->assertQuery('#section-legal-entity-edit');
        $this->assertQuery('#legal-entity-edit-form');
    }

    /**
     * @throws \Exception
     * only for operator
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testLegalEntitySingleForm()
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => '0']);
        /** @var  CivilLawSubject $civilLaw */
        $civilLaw = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['id' => '1']);
        $LegalEntyty = new LegalEntity();
        $LegalEntyty->setInn('test');
        $LegalEntyty->setKpp('test');
        $LegalEntyty->setName('test');
        $LegalEntyty->setPhone('8800200600');
        $civilLaw->setNdsType($ndsType);
        $LegalEntyty->setLegalAddress('bruansk');
        $LegalEntyty->setCivilLawSubject($civilLaw);
        $this->entityManager->persist($LegalEntyty);
        $this->entityManager->flush();
        /** @var LegalEntity $LegalEntyty */
        $LegalEntyty = $this->entityManager->getRepository(LegalEntity::class)
            ->findOneBy(['name' => 'test']);
        $entity_id = $LegalEntyty->getId();
        $this->assignRoleToTestUser('Operator');
        $this->login();
        $this->dispatch('/profile/legal-entity/' . $entity_id , 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(LegalEntityController::class);
        $this->assertMatchedRouteName('user-profile/legal-entity-single');
        $this->assertQuery('#section-legal-entity-single');
        $this->assertQuery('#legal-entity-single-form');
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     */
    private function assignRoleToTestUser(string $role_name, User $user = null)
    {
        if (!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя test
     */
    private function login()
    {
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);
    }


}