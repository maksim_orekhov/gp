<?php

namespace ApplicationTest\HTMLOutput;

use Application\Controller\ArbitrageController;
use Application\Controller\BankController;
use Application\Form\UpdateBankForm;
use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\User;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\Deal;

/**
 * Class HTMLOutputArbitrageRequestTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputBankUpdateTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /**
     * Тестируем рендеринг формы спора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     */
    public function testBankUpdateOperator()
    {

        // Залогиниваем участника сжедки
        $this->login('operator');

        /** @var UpdateBankForm $updateBankForm */
        $updateBankForm = new UpdateBankForm();

        // Проверяем, является ли пост POST-запросом.
        $this->dispatch('/bank/update', 'POST');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName(BankController::class);
        $this->assertMatchedRouteName('bank-update');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('updateBankForm', $view_vars);
        $this->assertArrayHasKey('update_result', $view_vars);
        //проверяем отгрузилась ли форма
        $this->assertQuery('#section_bank_update');
    }


    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login = null, string $password = null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}