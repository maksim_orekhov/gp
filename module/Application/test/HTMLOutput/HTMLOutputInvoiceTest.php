<?php

namespace ApplicationTest\HTMLOutput;

use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class HTMLOutputInvoiceTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputInvoiceTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const CONFIRMED_DEAL_NAME = 'Сделка Оплаченная';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Тестируем доступность invoice/id
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testAvailabilityInvoiceSingle()
    {
        // Залогиниваем пользователя
        $this->login();

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);

        $this->setOutputCallback(function(){
            $output = $this->getActualOutput();
            $this->assertContains("%PDF-1.7", $output );
        });

        $this->dispatch('/invoice/'.$deal->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertArrayHasKey('system_payment_detail', $view_vars);
    }

    /**
     * Тестируем доступность invoice оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testAvailabilityInvoiceSingleForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CONFIRMED_DEAL_NAME]);

        $this->assertNotNull($deal);

        $this->setOutputCallback(function () {
            $output = $this->getActualOutput();
            $this->assertContains("%PDF-1.7", $output);

        });

        $this->dispatch('/invoice/' . $deal->getId(), 'GET');


        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertArrayHasKey('deal', $view_vars);
        $this->assertArrayHasKey('system_payment_detail', $view_vars);
    }

    /**
     * Тестируем доступность invoice коллекции и переменных
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testAvailabilityInvoiceCollectionAndViewVars()
    {
        // Залогиниваем пользователя
        $this->login();

        $this->dispatch('/invoice', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertQuery('#invoice-collection');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertArrayHasKey('deals', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
    }

    /**
     * Тестируем доступность invoice коллекции и переменных для оператора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group invoice
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testAvailabilityInvoiceCollectionAndViewVarsForOperator()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/invoice', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertQuery('#invoice-collection');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();
        $this->assertArrayHasKey('deals', $view_vars);
        $this->assertArrayHasKey('is_operator', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}