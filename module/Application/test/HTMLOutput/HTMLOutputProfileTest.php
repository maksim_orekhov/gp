<?php

namespace ApplicationTest\HTMLOutput;

use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class HTMLOutputProfileTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputProfileTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /**
     * Тестируем формы изменения телефона и email в профиле
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group profile
     * @group HTMLOutput
     */
    public function testPhoneAndEmailEditForms()
    {
        // Залогиниваем пользователя test
        $this->login();

        $this->dispatch('/profile', 'GET');

        // Проверяем что форма смены телефона действительно есть в DOM
        $this->assertQuery('#FormPhoneEdit');

        // Проверяем что форма смены email действительно есть в DOM
        $this->assertQuery('#FormEmailEdit');

        // Проверяем что форма подтверждения email действительно есть в DOM
        $this->assertQuery('#FormEmailConfirm');

        // Проверяем что форма подтверждения телефона действительно есть в DOM
        $this->assertQuery('#FormPhoneConfirm');

        // Проверяем, что таблица индивидуальных предпринимателей действительно есть в DOM
        $this->assertQuery('#sole-proprietor');

        // Проверяем, что сообщение о приглашениях действительно есть в DOM
        $this->assertQuery('#unhandled-invitations-message');

        // Проверяем, что кнопка перехода к приглашениям действительно есть в DOM
        $this->assertQuery('#redirect-to-invitations-button');

        // Проверяем, что кнопка создания сделки действительно есть в DOM
        $this->assertQuery('#create-deal-button');

        // Проверяем, что контроллер таблиц cls действительно есть в DOM
        $this->assertQuery('#profile-page-content');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}