<?php

namespace ApplicationTest\HTMLOutput;

use Application\Entity\Discount;
use Application\Entity\Dispute;
use Application\Entity\User;
use Core\Service\Base\BaseRoleManager;
use Core\Service\Base\BaseAuthManager;
use Application\Entity\Role;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class HTMLOutputDiscountTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputDiscountTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_DEAL_NAME = 'Сделка Debit Matched';
    const TEST_DEAL_WITH_DISPUTE_NAME = 'Сделка Спор открыт';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }


    /**
     * Доступность /discount/[id] (GET на get) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     * @group HTMLOutput
     */
    public function testGetDiscountOutput()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Спор со Скидкой закрыт без продления';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var Discount $discount */
        $discount = $dispute->getDiscount();

        $this->dispatch('/discount/'.$discount->getId(), 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        // Что отдается в шаблон
        $this->assertArrayHasKey('discount', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#DiscountPage');
        // Если страница будет развиваться, дописать проверки других id
    }

    /**
     * Доступность /discount (GET на getList) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     * @group HTMLOutput
     */
    public function testDiscountTable()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Operator пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/discount', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        // Что отдается в шаблон
        $this->assertArrayHasKey('discounts', $view_vars);
        $this->assertArrayHasKey('paginator', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#TableDiscounts');
    }

    /**
     * Доступность /dispute/:idDispute/discount/create (GET на createFormAction) Оператору по HTTP
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     */
    public function testCreateForm()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Verified пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $this->dispatch('/dispute/'.$dispute->getId().'/discount/create');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        // Что отдается в шаблон
        $this->assertArrayHasKey('payment_method_bank_transfer', $view_vars);
        $this->assertArrayHasKey('discountForm', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#dispute-discount-create-form');
    }

    /**
     * наличие формы FormDiscountPaymentMethod в карточке спора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     */
    public function testFormDiscountPaymentMethod()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Verified пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $this->dispatch('/dispute/'.$dispute->getId());

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        // Что отдается в шаблон
        $this->assertArrayHasKey('payment_method_bank_transfer', $view_vars);
        $this->assertArrayHasKey('discountForm', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#discount-form');
    }

    /**
     * наличие формы FormDiscountPaymentMethod в карточке спора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dispute
     * @group discount
     */
    public function testFormRefundPaymentMethod()
    {
        /** @var User $user */
        $user = $this->userManager->selectUserByLogin('operator');
        // Назначаем роль Verified пользователю operator
        $this->assignRoleToTestUser('Operator', $user);
        // Залогиниваем пользователя operator
        $this->login('operator');

        $deal_name = 'Сделка Предложение скидки принято';
        /** @var Deal $deal */
        $deal = $this->getDealForTest($deal_name);
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        $this->dispatch('/dispute/'.$dispute->getId());

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        // Что отдается в шаблон
        $this->assertArrayHasKey('payment_method_bank_transfer', $view_vars);
        $this->assertArrayHasKey('discountForm', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#refund_form');
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param null $deal_name
     * @return Deal
     */
    private function getDealForTest($deal_name = null)
    {
        if (!$deal_name) $deal_name = self::TEST_DEAL_NAME;

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(array('name' => $deal_name));

        return $deal;
    }
}