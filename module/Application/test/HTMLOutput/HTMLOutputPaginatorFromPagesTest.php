<?php

namespace ApplicationTest\HTMLOutput;

use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class HTMLOutputPaginatorFromPagesTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputPaginatorFromPagesTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DISPUTE_DEAL_NAME = 'Сделка Спор открыт';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Тестируем paginator output vars для /bank-client
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group paginator
     * @group HTMLOutput
     */
    public function testPaginatorOutputFromBankClient()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/bank-client', 'GET');


        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertArrayHasKey('paginator', $view_vars);

        $paginatorOutputs = $view_vars['paginator'];
        //presence of keys
        $this->assertArrayHasKey('page_count', $paginatorOutputs);
        $this->assertArrayHasKey('item_count_per_page', $paginatorOutputs);
        $this->assertArrayHasKey('first', $paginatorOutputs);
        $this->assertArrayHasKey('current', $paginatorOutputs);
        $this->assertArrayHasKey('last', $paginatorOutputs);
        $this->assertArrayHasKey('pages_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('first_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('last_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('current_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('total_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('last_item_number', $paginatorOutputs);

        $this->assertQuery('#paginator-bank-client');
    }

    /**
     * Тестируем paginator output vars для /dispute/open
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group paginator
     * @group HTMLOutput
     */
    public function testPaginatorOutputFromDisputeOpen()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/dispute/open', 'GET');


        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertArrayHasKey('paginator', $view_vars);

        $paginatorOutputs = $view_vars['paginator'];
        //presence of keys
        $this->assertArrayHasKey('page_count', $paginatorOutputs);
        $this->assertArrayHasKey('item_count_per_page', $paginatorOutputs);
        $this->assertArrayHasKey('first', $paginatorOutputs);
        $this->assertArrayHasKey('current', $paginatorOutputs);
        $this->assertArrayHasKey('last', $paginatorOutputs);
        $this->assertArrayHasKey('pages_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('first_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('last_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('current_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('total_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('last_item_number', $paginatorOutputs);

        $this->assertQuery('#paginator-dispute-open');
    }

    /**
     * Тестируем paginator output vars для /deal/expired
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group paginator
     * @group HTMLOutput
     */
    public function testPaginatorOutputFromDealExpired()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/deal/expired', 'GET');


        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertArrayHasKey('paginator', $view_vars);

        $paginatorOutputs = $view_vars['paginator'];
        //presence of keys
        $this->assertArrayHasKey('page_count', $paginatorOutputs);
        $this->assertArrayHasKey('item_count_per_page', $paginatorOutputs);
        $this->assertArrayHasKey('first', $paginatorOutputs);
        $this->assertArrayHasKey('current', $paginatorOutputs);
        $this->assertArrayHasKey('last', $paginatorOutputs);
        $this->assertArrayHasKey('pages_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('first_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('last_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('current_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('total_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('last_item_number', $paginatorOutputs);

        $this->assertQuery('#paginator-deal-expired');
    }

    /**
     * Тестируем paginator output vars для /user
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group paginator
     * @group HTMLOutput
     */
    public function testPaginatorOutputFromUser()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/user', 'GET');


        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertArrayHasKey('paginator', $view_vars);

        $paginatorOutputs = $view_vars['paginator'];
        //presence of keys
        $this->assertArrayHasKey('page_count', $paginatorOutputs);
        $this->assertArrayHasKey('item_count_per_page', $paginatorOutputs);
        $this->assertArrayHasKey('first', $paginatorOutputs);
        $this->assertArrayHasKey('current', $paginatorOutputs);
        $this->assertArrayHasKey('last', $paginatorOutputs);
        $this->assertArrayHasKey('pages_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('first_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('last_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('current_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('total_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('last_item_number', $paginatorOutputs);

        $this->assertQuery('#paginator-user');
    }

    /**
     * Тестируем paginator output vars для /deal/abnormal
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group paginator
     * @group HTMLOutput
     */
    public function testPaginatorOutputFromDealAbnormal()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/deal/abnormal', 'GET');


        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertArrayHasKey('paginator', $view_vars);

        $paginatorOutputs = $view_vars['paginator'];
        //presence of keys
        $this->assertArrayHasKey('page_count', $paginatorOutputs);
        $this->assertArrayHasKey('item_count_per_page', $paginatorOutputs);
        $this->assertArrayHasKey('first', $paginatorOutputs);
        $this->assertArrayHasKey('current', $paginatorOutputs);
        $this->assertArrayHasKey('last', $paginatorOutputs);
        $this->assertArrayHasKey('pages_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('first_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('last_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('current_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('total_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('last_item_number', $paginatorOutputs);

        $this->assertQuery('#paginator-deal-abnormal');
    }

    /**
     * Тестируем paginator output vars для /invoice
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group paginator
     * @group HTMLOutput
     */
    public function testPaginatorOutputFromInvoice()
    {
        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->dispatch('/invoice', 'GET');


        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertArrayHasKey('paginator', $view_vars);

        $paginatorOutputs = $view_vars['paginator'];
        //presence of keys
        $this->assertArrayHasKey('page_count', $paginatorOutputs);
        $this->assertArrayHasKey('item_count_per_page', $paginatorOutputs);
        $this->assertArrayHasKey('first', $paginatorOutputs);
        $this->assertArrayHasKey('current', $paginatorOutputs);
        $this->assertArrayHasKey('last', $paginatorOutputs);
        $this->assertArrayHasKey('pages_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('first_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('last_page_in_range', $paginatorOutputs);
        $this->assertArrayHasKey('current_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('total_item_count', $paginatorOutputs);
        $this->assertArrayHasKey('last_item_number', $paginatorOutputs);

        $this->assertQuery('#paginator-invoice');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}