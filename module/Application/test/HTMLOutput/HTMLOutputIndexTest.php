<?php

namespace ApplicationTest\HTMLOutput;

use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class HTMLOutputDealHintsTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputIndexTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dealHints
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testIndexContacts()
    {


        $this->dispatch('/contacts', 'GET');

        // Возвращается из шаблона
        $this->assertQuery('#section-contacts');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dealHints
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testIndexConnectionContract()
    {


        $this->dispatch('/connection-contract', 'GET');

        // Возвращается из шаблона
        $this->assertQuery('#section-connection-contract');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dealHints
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testIndexDisputeConsiderationRule()
    {


        $this->dispatch('/dispute-consideration-rules', 'GET');

        // Возвращается из шаблона
        $this->assertQuery('#section-consideration-rules');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dealHints
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testIndexDisputeResolution()
    {


        $this->dispatch('/dispute-resolution', 'GET');

        // Возвращается из шаблона
        $this->assertQuery('#section-dispute-resolution');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dealHints
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testIndexForWhorm()
    {


        $this->dispatch('/for-whom', 'GET');

        // Возвращается из шаблона
        $this->assertQuery('#section-for-whom');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dealHints
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testIndexHowItWorks()
    {


        $this->dispatch('/how-it-works', 'GET');


        $this->assertQuery('#section-how-it-works');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dealHints
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testIndexPolicy()
    {


        $this->dispatch('/policy', 'GET');


        $this->assertQuery('#section-index-policy');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dealHints
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testIndexTermsOfUse()
    {


        $this->dispatch('/terms-of-use', 'GET');


        $this->assertQuery('#section-terms-of-use');
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group dealHints
     * @group deal
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testIndexWhatCanBuy()
    {


        $this->dispatch('/what-can-buy', 'GET');


        $this->assertQuery('#section-what-can-buy');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login = null, string $password = null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}