<?php

namespace ApplicationTest\HTMLOutput;

use Application\Entity\CivilLawSubject;
use Application\Entity\NaturalPerson;
use Application\Entity\User;
use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;

/**
 * Class HTMLOutputCivilLawSubjectPageTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputCivilLawSubjectPageTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /**
     * Тестируем поведение таймлайн в статусе сделки 'pending_refund_payment'
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civilLawSubjectPage
     * @group HTMLOutput
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testNaturalPersonPaymentMethodsTable()
    {
//        $deal = $this->entityManager->getRepository(Deal::class)
//            ->findOneBy(['name' => 'Сделка Ожидает возврата']);

        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['isPattern' => true]);

//        if($civilLawSubject->getNaturalPerson())

        /** @var User $user */
        $user = $civilLawSubject->getUser();

        // Залогиниваем пользователя
        $this->login($user->getLogin());

        $this->dispatch('/profile/natural-person/'.$civilLawSubject->getNaturalPerson()->getId(), 'GET');

        // Возвращается из шаблона
        // Карточка NaturalPerson
        $this->assertQuery('#CardNaturalPerson');
        // Таблица методов оплаты
        $this->assertQuery('#table-payment-methods');
    }

    /**
     * Тестируем наличие блока CardLegalEntity на странице профиля legal-entity
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group civilLawSubjectPage
     * @group HTMLOutput
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testLegalEntityPaymentMethodsTable()
    {
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $this->entityManager->getRepository(CivilLawSubject::class)
            ->findOneBy(['isPattern' => true]);

        /** @var User $user */
        $user = $civilLawSubject->getUser();

        // Залогиниваем пользователя
        $this->login($user->getLogin());

        $this->dispatch('/profile/legal-entity/'.$civilLawSubject->getLegalEntity()->getId(), 'GET');

        // Возвращается из шаблона
        // Карточка LegalEntity
        $this->assertQuery('#CardLegalEntity');
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}