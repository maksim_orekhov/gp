<?php

namespace ApplicationTest;

use Application\Module;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;

class ModuleTest extends AbstractHttpControllerTestCase
{
    const CONFIG_FILES_NAMES = [
        'module.config.php',
        'router.config.php',
        'landing.router.config.php',
        'assets.config.php',
        'rbac.config.php',
        'doctrine.config.php',
        'controllers.config.php',
        'service.config.php',
        'views.config.php'
    ];

    private $moduleRoot = null;

    protected function setUp()
    {
        $this->moduleRoot = realpath(__DIR__ . '/../');

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../config/application.config.php',
            include __DIR__ . '/../../../config/autoload/global.php'
        ));
    }

    /**
     * @group module
     */
    public function testGetConfig()
    {
        $expectedConfig = [];
        foreach(self::CONFIG_FILES_NAMES as $file_name) {
            $config = include $this->moduleRoot . '/config/' . $file_name;
            $expectedConfig = array_replace_recursive($expectedConfig, $config);
        }

        $module = new Module();
        $configData = $module->getConfig();

        $this->assertEquals($expectedConfig, $configData);
    }

    /**
     * @group module
     */
    public function testHasConfigKeys()
    {
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');

        $this->assertArrayHasKey('hide_exceptions', $config);
        $this->assertArrayHasKey('session_validation_failed', $config['hide_exceptions']);
    }
}