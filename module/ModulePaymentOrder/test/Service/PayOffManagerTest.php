<?php

namespace ModulePaymentOrderTest\Service;

use Application\Entity\Deal;
use Application\Entity\Discount;
use Application\Entity\Dispute;
use Application\Entity\DisputeCycle;
use Application\Entity\FeePayerOption;
use Application\Entity\Payment;
use Application\Service\Payment\PaymentMethodManager;
use Core\Exception\LogicException;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\ServiceManager\ServiceManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use PHPUnit\Framework\MockObject\Generator;

/**
 * Class PayOffManagerTest
 * @package ModulePaymentOrderTest\Service
 */
class PayOffManagerTest extends AbstractHttpControllerTestCase
{
    const DISPUTE_OPEN_DEAL_NAME = 'Сделка Спор открыт';

    /**
     * @var Generator
     */
    private $generator;
    /**
     * @var PayOffManager
     */
    private $payOffManager;
    /**
     * @var PaymentOrderManager
     */
    private $paymentOrderManager;
    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    /**
     * @var ServiceManager
     */
    private $serviceManager;


    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        //$configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                include __DIR__ . '/../../../../config/application.config.php',
                include __DIR__ . '/../../../../config/autoload/global.php'
            ),
            include __DIR__ . '/../../../../config/autoload/local.php'
        ));

        parent::setUp();

        $this->generator = new Generator;
        $serviceManager = $this->getApplicationServiceLocator();
        $this->serviceManager = $serviceManager;
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->payOffManager = $serviceManager->get(PayOffManager::class);
        $this->paymentOrderManager = $serviceManager->get(PaymentOrderManager::class);
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Попытка создать платежку сумма которой превышает баланс
     *
     * @throws \Core\Exception\LogicException
     * @throws \Exception
     */
    public function testCreatePayOffWithAmountExceedsBalance()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_OPEN_DEAL_NAME]);
        $this->assertNotNull($deal);
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);
        $disputeCycles = $dispute->getDisputeCycles();
        $this->assertNotEmpty($disputeCycles);
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $disputeCycles->last();
        $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($dispute->getDeal());

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $deal->getFeePayerOption();
        ///// 1 ожидаем что проценты возложены на CUSTOMER /////
        $this->assertEquals('CUSTOMER', $feePayerOption->getName());
        ///// 2 проверяем что сделка оплачена (вычисляем по DeFactoDate) /////
        $this->assertNotNull($payment->getDeFactoDate()); /////
        ///// 3 проверяем что нет переплаты
        $this->assertNull($payment->getRepaidOverpay());
        // при соблюдении 3-х вышестояших пунктов ожидаемый баланс сделки
        // будет равен ReleasedValue так как никаких выплат не произведенно
        // и проценты заложенны в оплате кастомера
        $expected_balance = $payment->getReleasedValue();
        ///// 4 имитируем превышение баланса на 1 рубль
        $amount = $expected_balance + 1;
        $deal_balance = $this->paymentOrderManager->getDealBalanceByPayment($deal->getPayment());
        $this->assertTrue($amount > $deal_balance);
        ///// 5 создаем сушность на скидку с суммой привышаюшей баланс
        $discount = new Discount();
        $discount->setCreated(new \DateTime());
        $discount->setDispute($dispute);
        $discount->setAmount($amount);
        $discount->setPaymentMethod($customerPaymentMethod);
        $discount->setDisputeCycle($disputeCycle);
        $this->entityManager->persist($discount);
        $dispute->setDiscount($discount);
        $disputeCycle->setDiscount($discount);
        $this->entityManager->persist($dispute);
        $this->entityManager->persist($disputeCycle);
        $this->entityManager->flush();
        ///// Генерация платежки на выплату скидки /////
        try {
            $this->payOffManager->createPayOff($deal, PayOffManager::PURPOSE_TYPE_DISCOUNT);
            $logicException = null;
        }catch (LogicException $exception){
            $logicException = $exception;
        }

        // ожидаем что произошел exception причем LogicException
        $this->assertNotNull($logicException);
        $this->assertEquals(LogicException::DEAL_BALANCE_DOES_NOT_ALLOW_PAYOFF_MESSAGE, $logicException->getMessage());
    }

    /**
     * Создаем платежку сумма которой не превышает баланс
     * тестируем что проверка баланса не блокирует создание платежки
     *
     * @throws \Core\Exception\LogicException
     * @throws \Exception
     */
    public function testCreatePayOffWithAmountNotExceedsBalance()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISPUTE_OPEN_DEAL_NAME]);
        $this->assertNotNull($deal);
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $this->assertNotNull($dispute);
        $disputeCycles = $dispute->getDisputeCycles();
        $this->assertNotEmpty($disputeCycles);
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $disputeCycles->last();
        $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($dispute->getDeal());

        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $deal->getFeePayerOption();
        ///// 1 ожидаем что проценты возложены на CUSTOMER /////
        $this->assertEquals('CUSTOMER', $feePayerOption->getName());
        ///// 2 проверяем что сделка оплачена (вычисляем по DeFactoDate) /////
        $this->assertNotNull($payment->getDeFactoDate()); /////
        ///// 3 проверяем что нет переплаты
        $this->assertNull($payment->getRepaidOverpay());
        // при соблюдении 3-х вышестояших пунктов ожидаемый баланс сделки
        // будет равен ReleasedValue так как никаких выплат не произведенно
        // и проценты заложенны в оплате кастомера
        $expected_balance = $payment->getReleasedValue();
        ///// 4 заведомо делаем сумму меньше чем баланс на 1 рубль
        $amount = $expected_balance - 1;
        $deal_balance = $this->paymentOrderManager->getDealBalanceByPayment($deal->getPayment());
        $this->assertTrue($amount < $deal_balance);
        ///// 5 создаем сушность на скидку с суммой меньше чем баланс
        $discount = new Discount();
        $discount->setCreated(new \DateTime());
        $discount->setDispute($dispute);
        $discount->setAmount($amount);
        $discount->setPaymentMethod($customerPaymentMethod);
        $discount->setDisputeCycle($disputeCycle);
        $this->entityManager->persist($discount);
        $dispute->setDiscount($discount);
        $disputeCycle->setDiscount($discount);
        $this->entityManager->persist($dispute);
        $this->entityManager->persist($disputeCycle);
        $this->entityManager->flush();
        ///// Генерация платежки на выплату скидки /////
        /** @var PaymentOrder $paymentOrder */
        try {
            $paymentOrder = $this->payOffManager->createPayOff($deal, PayOffManager::PURPOSE_TYPE_DISCOUNT);
            $logicException = null;
        }catch (LogicException $exception){
            $logicException = $exception;
            $paymentOrder = null;
        }

        // ожидаем что проверка баланса не выдала exception
        $this->assertNull($logicException);
        // сущность paymentOrder должна создаться
        $this->assertNotNull($paymentOrder);
        $this->assertSame($paymentOrder->getPaymentMethodType(), $customerPaymentMethod->getPaymentMethodType());
        //ожидаем что выплата производится по реквизитам
        $this->assertNotNull($paymentOrder->getBankClientPaymentOrder());
        $this->assertSame($paymentOrder->getBankClientPaymentOrder()->getAmount(), $amount);
    }
}