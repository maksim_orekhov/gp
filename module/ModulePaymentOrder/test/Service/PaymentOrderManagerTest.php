<?php

namespace ModulePaymentOrderTest\Service;

use Application\Entity\Deal;
use Application\Entity\Dispute;
use Application\Entity\FeePayerOption;
use Application\Entity\Payment;
use Application\Service\Payment\PaymentMethodManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\ServiceManager\ServiceManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use PHPUnit\Framework\MockObject\Generator;

/**
 * Class PaymentOrderManagerTest
 * @package ModulePaymentOrderTest\Service
 */
class PaymentOrderManagerTest extends AbstractHttpControllerTestCase
{
    const CONFIRMED_DEAL_NAME = 'Сделка Соглашение достигнуто';
    const DEBIT_MATCHED_DEAL_NAME = 'Сделка Оплаченная';
    const OVERPAID_DEAL_NAME = 'Сделка Переплата';
    const DISCOUNT_CREATE_DEAL_NAME = 'Сделка Скидка создана';
    const REFUND_CREATE_DEAL_NAME = 'Сделка Возврат создан';
    const CLOSED_DEAL_NAME = 'Сделка Закрыта';

    /**
     * @var Generator
     */
    private $generator;
    /**
     * @var PayOffManager
     */
    private $payOffManager;
    /**
     * @var PaymentOrderManager
     */
    private $paymentOrderManager;
    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;
    /**
     * @var ServiceManager
     */
    private $serviceManager;


    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        //$configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                include __DIR__ . '/../../../../config/application.config.php',
                include __DIR__ . '/../../../../config/autoload/global.php'
            ),
            include __DIR__ . '/../../../../config/autoload/local.php'
        ));

        parent::setUp();

        $this->generator = new Generator;
        $serviceManager = $this->getApplicationServiceLocator();
        $this->serviceManager = $serviceManager;
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->payOffManager = $serviceManager->get(PayOffManager::class);
        $this->paymentOrderManager = $serviceManager->get(PaymentOrderManager::class);
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group balance
     * @throws \Exception
     */
    public function testGetDealBalanceByPaymentWithConfirmedDeal()
    {
        ///// 1. сделка создана и подтверждена обоими уч. оплаты небыло
        ///// ожидаем баланс 0 так как средств не поступало
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CONFIRMED_DEAL_NAME]);
        $this->assertNotNull($deal);
        $deal_balance = $this->paymentOrderManager->getDealBalanceByPayment($deal->getPayment());
        $this->assertSame(0, $deal_balance);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group balance
     * @throws \Exception
     */
    public function testGetDealBalanceByPaymentWithDebitMatchedDeal()
    {
        ///// 2. сделку оплатили
        ///// 2.1 ожидаем что проценты возложены на CUSTOMER
        ///// 2.2 проверяем что сделка оплачена (вычисляем по DeFactoDate)
        ///// 2.3 проверяем что нет переплаты
        /**
         * при соблюдении 3-х вышестояших пунктов ожидаемый баланс сделки
         * будет равен ReleasedValue так как никаких выплат не произведенно
         * и проценты заложенны в оплате кастомера
         */
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEBIT_MATCHED_DEAL_NAME]);
        $this->assertNotNull($deal);
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $deal->getFeePayerOption();
        $this->assertEquals('CUSTOMER', $feePayerOption->getName());
        $this->assertNotNull($payment->getDeFactoDate());
        $this->assertNull($payment->getRepaidOverpay());
        $expected_balance = $payment->getReleasedValue();
        $deal_balance = $this->paymentOrderManager->getDealBalanceByPayment($deal->getPayment());
        $this->assertSame($expected_balance, $deal_balance);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group balance
     * @throws \Exception
     */
    public function testGetDealBalanceByPaymentWithOverpaidDeal()
    {
        ///// 3. сделку переплатили сущность переплаты как и платежка еще не созданы
        ///// 3.1 ожидаем что проценты возложены на CUSTOMER
        ///// 3.2 проверяем что сделка оплачена (вычисляем по DeFactoDate)
        ///// 3.3 проверяем что есть переплата
        /**
         * при соблюдении 3-х вышестояших пунктов ожидаемый баланс сделки
         * будет равен ReleasedValue + сумма переплаты так как никаких выплат не произведенно
         * и проценты заложенны в оплате кастомера
         */
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::OVERPAID_DEAL_NAME]);
        $this->assertNotNull($deal);
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $deal->getFeePayerOption();
        $this->assertEquals('CUSTOMER', $feePayerOption->getName());
        $this->assertNotNull($payment->getDeFactoDate());
        $this->assertNull($payment->getRepaidOverpay());
        $expected_balance = $payment->getReleasedValue() + $this->paymentOrderManager->getTotalOverpaidAmountByPayment($payment);
        $deal_balance = $this->paymentOrderManager->getDealBalanceByPayment($deal->getPayment());
        $this->assertSame($expected_balance, $deal_balance);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group balance
     * @throws \Exception
     */
    public function testGetDealBalanceByPaymentWithDiscountCreate()
    {
        ///// 4. в сделке создали как платежку так и сущность скидки
        ///// 4.1 ожидаем что проценты возложены на CUSTOMER
        ///// 4.2 проверяем что сделка оплачена (вычисляем по DeFactoDate)
        ///// 4.3 проверяем что нет переплаты
        ///// 4.4 проверяем что есть платежка и сущность скидки
        /**
         * при соблюдении 4-х вышестояших пунктов ожидаемый баланс сделки
         * будет равен ReleasedValue - сумма скидки так как создана платежка на скидку
         */
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_CREATE_DEAL_NAME]);
        $this->assertNotNull($deal);
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $deal->getFeePayerOption();
        $this->assertEquals('CUSTOMER', $feePayerOption->getName());
        $this->assertNotNull($payment->getDeFactoDate());
        $this->assertNull($payment->getRepaidOverpay());
        $this->assertNotNull($dispute);
        $this->assertNotNull($dispute->getDiscount());
        $expected_balance = $payment->getReleasedValue() - $dispute->getDiscount()->getAmount();
        $deal_balance = $this->paymentOrderManager->getDealBalanceByPayment($deal->getPayment());
        $this->assertSame($expected_balance, $deal_balance);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group balance
     * @throws \Exception
     */
    public function testGetDealBalanceByPaymentWithRefundCreate()
    {
        ///// 5. в сделке создали как платежку так и сущность возврата
        ///// 5.1 ожидаем что проценты возложены на CUSTOMER
        ///// 5.2 проверяем что сделка оплачена (вычисляем по DeFactoDate)
        ///// 5.3 проверяем что нет переплаты
        ///// 5.4 проверяем что есть платежка и сущность возврата
        /**
         * при соблюдении 4-х вышестояших пунктов ожидаемый баланс сделки
         * будет равен ReleasedValue - сумма возврата так как создана платежка на возврат
         * и баланс должен быть 0!!!
         */
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::REFUND_CREATE_DEAL_NAME]);
        $this->assertNotNull($deal);
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var FeePayerOption $feePayerOption */
        $feePayerOption = $deal->getFeePayerOption();
        $this->assertEquals('CUSTOMER', $feePayerOption->getName());
        $this->assertNotNull($payment->getDeFactoDate());
        $this->assertNull($payment->getRepaidOverpay());
        $this->assertNotNull($dispute);
        $this->assertNotNull($dispute->getRefund());
        $expected_balance = $payment->getReleasedValue() - $this->payOffManager->getRefundAmount($deal);
        $deal_balance = $this->paymentOrderManager->getDealBalanceByPayment($deal->getPayment());
        $this->assertSame($expected_balance, $deal_balance);
        //проверяем что точно 0
        $this->assertSame((double) 0 ,$deal_balance);
    }

    /**
     * @throws \Exception
     */
    public function testGetDealBalanceByPaymentWithDealClosed()
    {
        ///// 5. сделка закрыта
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CLOSED_DEAL_NAME]);
        $this->assertNotNull($deal);
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $deal_balance = $this->paymentOrderManager->getDealBalanceByPayment($payment);
        //проверяем что 0 так как все выплаты должны быть произведены
        $this->assertSame((double) 0 ,$deal_balance);
    }
}