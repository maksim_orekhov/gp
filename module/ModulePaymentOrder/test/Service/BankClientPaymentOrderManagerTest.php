<?php

namespace ModulePaymentOrderTest\Service;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\Delivery;
use Application\Entity\Discount;
use Application\Entity\DiscountRequest;
use Application\Entity\Dispute;
use Application\Entity\DisputeCycle;
use Application\Entity\NdsType;
use Application\Entity\Payment;
use Application\Entity\Refund;
use Application\Entity\RepaidOverpay;
use Application\Fixture\DealFixtureLoader;
use Application\Service\Deal\DealManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentMethodManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Entity\PaymentTransactionInterface;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use PHPUnit\Framework\MockObject\Generator;

/**
 * Class BankClientPaymentOrderManagerTest
 * @package ModulePaymentOrderTest\Service
 */
class BankClientPaymentOrderManagerTest extends AbstractHttpControllerTestCase
{
    use \ApplicationTest\TestFixture\FixtureTrait;

    const CLOSED_DEAL_NAME = 'Сделка Закрыта';
    const PENDING_PAY_DEAL_NAME = 'Сделка Ожидает выплаты';
    const CONFIRMED_DEAL_NAME = 'Сделка Соглашение достигнуто';
    const DEBIT_MATCHED_DEAL_NAME = 'Сделка Оплаченная';
    #const DELIVERED_DEAL_NAME = 'Сделка Доставка подтверждена';
    const DISCOUNT_ACCEPTED_DEAL_NAME = 'Сделка Предложение скидки принято';
    const OVERPAID_DEAL_NAME = 'Сделка Переплата';

    /**
     * @var Generator
     */
    private $generator;

    /**
     * @var BankClientPaymentOrderManager $bankClientPaymentOrderManager
     */
    private $bankClientPaymentOrderManager;

    /**
     * @var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var DealManager
     */
    private $dealManager;

    private $serviceManager;

    /**
     * @var PaymentMethodManager
     */
    private $paymentMethodManager;

    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        //$configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                include __DIR__ . '/../../../../config/application.config.php',
                include __DIR__ . '/../../../../config/autoload/global.php'
            ),
            include __DIR__ . '/../../../../config/autoload/local.php'
        ));

        parent::setUp();

        $this->generator = new Generator;
        $serviceManager = $this->getApplicationServiceLocator();
        $this->serviceManager = $serviceManager;
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->bankClientPaymentOrderManager = $serviceManager->get(BankClientPaymentOrderManager::class);
        $this->paymentOrderManager = $serviceManager->get(PaymentOrderManager::class);
        $this->paymentMethodManager = $serviceManager->get(PaymentMethodManager::class);
        $this->dealManager = $serviceManager->get(DealManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Тест создания выплаты по Сделке
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payoff
     */
    public function testMakePayOffPaymentByBankTransfer()
    {
        $eventManager = $this->entityManager->getEventManager();
        // Отписываемся от события, чтобы автоматически не вызвать создание платёжки при createDelivery()
        $eventManager->removeEventListener(
            array(\Doctrine\ORM\Events::onFlush, \Doctrine\ORM\Events::postFlush),
            $this->serviceManager->get(\Application\Listener\DeliveryListener::class)
        );

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEBIT_MATCHED_DEAL_NAME]);

        $this->assertNotNull($deal);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        // Проверяем, что у payment еще нет исходящих платежек
        $outgoingPaymentOrder = $this->paymentOrderManager->getPaymentOutgoingPaymentTransaction($payment);
        $this->assertNull($outgoingPaymentOrder);

        $this->createDelivery($deal);

        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $this->bankClientPaymentOrderManager->makePayOff($deal, $payment,
                               PayOffManager::PURPOSE_TYPE_PAYOFF, $payment->getReleasedValue());

        $this->assertNotNull($paymentOrder);
        /** @var BankClientPaymentOrder $bankClientPaymentOrder */
        $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();
        $this->assertNotNull($bankClientPaymentOrder);
        $this->assertEquals(PaymentOrderManager::TYPE_OUTGOING, $bankClientPaymentOrder->getType());
        $this->assertEquals($payment->getReleasedValue(), $bankClientPaymentOrder->getAmount());

        /** @var PaymentTransactionInterface $outgoingPaymentOrder */
        $outgoingPaymentOrder = $this->paymentOrderManager->getPaymentOutgoingPaymentTransaction($payment);
        $this->assertInstanceOf(BankClientPaymentOrder::class, $outgoingPaymentOrder);
        $this->assertEquals(PaymentOrderManager::TYPE_OUTGOING, $outgoingPaymentOrder->getType());
        $this->assertEquals($payment->getReleasedValue(), $outgoingPaymentOrder->getAmount());
    }

    /**
     * Тест создания выплаты по Скидке
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payoff
     */
    public function testMakePayOffDiscountByBankTransfer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_ACCEPTED_DEAL_NAME]);

        $this->assertNotNull($deal);

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var DiscountRequest $discountRequest */
        $discountRequest = $dispute->getDiscountRequests()->last();

        /** @var Discount $discount */
        $discount = $this->createDiscount($deal, $discountRequest->getAmount());

        // Проверяем, что у payment еще нет исходящих платежек
        $outgoingPaymentOrder = $this->paymentOrderManager->getPaymentOutgoingPaymentTransaction($payment);
        $this->assertNull($outgoingPaymentOrder);

        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $this->bankClientPaymentOrderManager->makePayOff($deal, $discount,
            PayOffManager::PURPOSE_TYPE_DISCOUNT, $discountRequest->getAmount());

        $this->assertNotNull($paymentOrder);
        /** @var BankClientPaymentOrder $bankClientPaymentOrder */
        $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();
        $this->assertNotNull($bankClientPaymentOrder);
        $this->assertEquals(PaymentOrderManager::TYPE_OUTGOING, $bankClientPaymentOrder->getType());
        $this->assertEquals($discount->getAmount(), $bankClientPaymentOrder->getAmount());

        /** @var PaymentTransactionInterface $outgoingPaymentOrder */
        $outgoingPaymentOrder = $this->paymentOrderManager->getDiscountOutgoingPaymentTransaction($discount);
        $this->assertInstanceOf(BankClientPaymentOrder::class, $outgoingPaymentOrder);
        $this->assertEquals(PaymentOrderManager::TYPE_OUTGOING, $outgoingPaymentOrder->getType());
        $this->assertEquals($discount->getAmount(), $outgoingPaymentOrder->getAmount());
    }

    /**
     * Тест создания выплаты по Возврату
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payoff
     */
    public function testMakePayOffRefundByBankTransfer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DISCOUNT_ACCEPTED_DEAL_NAME]);

        $this->assertNotNull($deal);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        /** @var Refund $refund */
        $refund = $this->createRefund($deal);

        // Проверяем, что у payment еще нет исходящих платежек
        $outgoingPaymentOrder = $this->paymentOrderManager->getPaymentOutgoingPaymentTransaction($payment);
        $this->assertNull($outgoingPaymentOrder);

        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $this->bankClientPaymentOrderManager->makePayOff($deal, $refund,
            PayOffManager::PURPOSE_TYPE_DISCOUNT, $payment->getReleasedValue());

        $this->assertNotNull($paymentOrder);
        /** @var BankClientPaymentOrder $bankClientPaymentOrder */
        $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();
        $this->assertNotNull($bankClientPaymentOrder);
        $this->assertEquals(PaymentOrderManager::TYPE_OUTGOING, $bankClientPaymentOrder->getType());
        $this->assertEquals($payment->getReleasedValue(), $bankClientPaymentOrder->getAmount());

        /** @var PaymentTransactionInterface $outgoingPaymentOrder */
        $outgoingPaymentOrder = $this->paymentOrderManager->getRefundOutgoingPaymentTransaction($refund);
        $this->assertInstanceOf(BankClientPaymentOrder::class, $outgoingPaymentOrder);
        $this->assertEquals(PaymentOrderManager::TYPE_OUTGOING, $outgoingPaymentOrder->getType());
        $this->assertEquals($payment->getReleasedValue(), $outgoingPaymentOrder->getAmount());
    }

    /**
     * Тест создания выплаты по Возврату переплаты
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group payoff
     */
    public function testMakePayOffRepaidOverpayByBankTransfer()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::OVERPAID_DEAL_NAME]);

        $this->assertNotNull($deal);

        $debit = $this->dealManager->getCurrentDealDebit($deal->getPayment());

        $this->assertArrayHasKey('overpayment', $debit);
        $this->assertNotNull($debit['overpayment']);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        /** @var Refund $refund */
        $refund = $this->createRepaidOverpay($deal, $debit['overpayment']);

        // Проверяем, что у payment еще нет исходящих платежек
        $outgoingPaymentOrder = $this->paymentOrderManager->getPaymentOutgoingPaymentTransaction($payment);
        $this->assertNull($outgoingPaymentOrder);

        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $this->bankClientPaymentOrderManager->makePayOff($deal, $refund,
            PayOffManager::PURPOSE_TYPE_DISCOUNT, $debit['overpayment']);

        $this->assertNotNull($paymentOrder);
        /** @var BankClientPaymentOrder $bankClientPaymentOrder */
        $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();
        $this->assertNotNull($bankClientPaymentOrder);
        $this->assertEquals(PaymentOrderManager::TYPE_OUTGOING, $bankClientPaymentOrder->getType());
        $this->assertEquals($debit['overpayment'], $bankClientPaymentOrder->getAmount());

        /** @var PaymentTransactionInterface $outgoingPaymentOrder */
        $outgoingPaymentOrder = $this->paymentOrderManager->getRepaidOverpayOutgoingPaymentTransaction($refund);
        $this->assertInstanceOf(BankClientPaymentOrder::class, $outgoingPaymentOrder);
        $this->assertEquals(PaymentOrderManager::TYPE_OUTGOING, $outgoingPaymentOrder->getType());
        $this->assertEquals($debit['overpayment'], $outgoingPaymentOrder->getAmount());
    }

    /**
     * доступность массива с проблемами
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testGetBankClientPaymentOrderTroubleTypes()
    {
        $troubleTypes = BankClientPaymentOrderManager::getBankClientPaymentOrderTroubleTypes();

        $this->assertInternalType('array', $troubleTypes);
        $this->assertNotEmpty($troubleTypes);
    }

    /**
     * для OUTGOING есть INCOMING платежка
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testIsPayOffWithPaymentOrderConfirming()
    {
        /**
         * @var Deal $deal
         * @var Payment $payment
         */
        $bankClientPaymentOrder = null;
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::CLOSED_DEAL_NAME]);

        $this->assertNotNull($deal);

        $payment = $deal->getPayment();

        $this->assertNotNull($payment);

        $paymentOrders = $payment->getPaymentOrders();
        /** @var PaymentOrder $paymentOrder */
        foreach ($paymentOrders as $paymentOrder){
            if ($paymentOrder->getBankClientPaymentOrder()->getType() === PaymentOrderManager::TYPE_OUTGOING) {
                $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();
            }
        }
        //проверяем что мы нашли OUTGOING платежку
        $this->assertNotNull($bankClientPaymentOrder);
        $result = $this->bankClientPaymentOrderManager->isPayOff($bankClientPaymentOrder);
        //проверяем что для OUTGOING нашлась INCOMING платежка
        $this->assertTrue($result);
    }

    /**
     * для OUTGOING нет INCOMING платежки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testIsPayOffWithoutPaymentOrderConfirming()
    {
        /**
         * @var Deal $deal
         * @var Payment $payment
         */
        $bankClientPaymentOrder = null;
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::PENDING_PAY_DEAL_NAME]);

        $this->assertNotNull($deal);

        $payment = $deal->getPayment();

        $this->assertNotNull($payment);

        $paymentOrders = $payment->getPaymentOrders();
        /** @var PaymentOrder $paymentOrder */
        foreach ($paymentOrders as $paymentOrder){
            if ($paymentOrder->getBankClientPaymentOrder()->getType() === PaymentOrderManager::TYPE_OUTGOING) {
                $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();
            }
        }
        //проверяем что мы нашли OUTGOING платежку
        $this->assertNotNull($bankClientPaymentOrder);
        $result = $this->bankClientPaymentOrderManager->isPayOff($bankClientPaymentOrder);
        //проверяем что для OUTGOING не нашлась INCOMING платежка
        $this->assertFalse($result);
    }

    /**
     * коллекция проблемных платежек
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testGetTroubleBankClientPaymentOrderMethod()
    {
        $result = $this->bankClientPaymentOrderManager
            ->getTroubleBankClientPaymentOrder([]);

        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('trouble_bank_client_payment_orders', $result);
        $this->assertNotEmpty($result['trouble_bank_client_payment_orders']);
        $this->assertArrayHasKey('paginator', $result);
        $this->assertNotEmpty($result['paginator']);
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @preserveGlobalState disabled
     *
     * @group nds
     *
     * @dataProvider dataGeneratePaymentPurposeForPayOff
     *
     * @throws \Exception
     */
    public function testGeneratePaymentPurposeForPayOff($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $payoff_payment_purpose = $this->bankClientPaymentOrderManager
            ->generatePayOffPaymentPurpose($deal, PayOffManager::PURPOSE_TYPE_PAYOFF, $payment->getReleasedValue());

        $this->assertEquals(
            $expected['pre'] . ' ' . PayOffManager::PURPOSE_SUBJECT . ' ' . $deal->getNumber() . '. ' . $expected['post'] . '.',
            $payoff_payment_purpose
        );
    }

    /**
     * @return array
     */
    public function dataGeneratePaymentPurposeForPayOff(): array
    {
        return [
            [null, [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_PAYOFF],
                'post' => NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY['0']
            ]],
            ['0', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_PAYOFF],
                'post' => NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY['1']
            ]],
            ['без НДС', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_PAYOFF],
                'post' => NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY['2']
            ]],
            ['10%', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_PAYOFF],
                'post' => NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY['3']
            ]],
            ['18%', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_PAYOFF],
                'post' => NdsTypeManager::NDS_TYPES_DECLARATIONS_FOR_OUTGOING_PAY['4']
            ]],
        ];
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @preserveGlobalState disabled
     *
     * @group nds
     *
     * @dataProvider dataGeneratePaymentPurposeForRefund
     *
     * @throws \Exception
     */
    public function testGeneratePaymentPurposeForRefund($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $refund_payment_purpose = $this->bankClientPaymentOrderManager
            ->generatePayOffPaymentPurpose($deal, PayOffManager::PURPOSE_TYPE_REFUND, $payment->getReleasedValue());

        $this->assertEquals(
            $expected['pre'] . ' ' . PayOffManager::PURPOSE_SUBJECT . ' ' . $deal->getNumber() . '. ' . $expected['post'] . '.',
            $refund_payment_purpose
        );
    }

    /**
     * @return array
     */
    public function dataGeneratePaymentPurposeForRefund(): array
    {
        return [
            [null, [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_REFUND],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['0', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_REFUND],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['без НДС', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_REFUND],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['10%', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_REFUND],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['18%', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_REFUND],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
        ];
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @preserveGlobalState disabled
     *
     * @group nds
     *
     * @dataProvider dataGeneratePaymentPurposeForDiscount
     *
     * @throws \Exception
     */
    public function testGeneratePaymentPurposeForDiscount($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $discount_payment_purpose = $this->bankClientPaymentOrderManager
            ->generatePayOffPaymentPurpose($deal, PayOffManager::PURPOSE_TYPE_DISCOUNT, $payment->getReleasedValue());

        $this->assertEquals(
            $expected['pre'] . ' ' . PayOffManager::PURPOSE_SUBJECT . ' ' . $deal->getNumber() . '. ' . $expected['post'] . '.',
            $discount_payment_purpose
        );
    }

    /**
     * @return array
     */
    public function dataGeneratePaymentPurposeForDiscount(): array
    {
        return [
            [null, [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_DISCOUNT],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['0', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_DISCOUNT],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['без НДС', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_DISCOUNT],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['10%', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_DISCOUNT],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['18%', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_DISCOUNT],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
        ];
    }

    /**
     * @param $given
     * @param $expected
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @preserveGlobalState disabled
     *
     * @group nds
     *
     * @dataProvider dataGeneratePaymentPurposeForOverpay
     *
     * @throws \Exception
     */
    public function testGeneratePaymentPurposeForOverpay($given, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => DealFixtureLoader::DEAL_BETWEEN_LEGAL_ENTITIES_NAME]);

        /** @var NdsType $ndsType */
        $ndsType = $this->entityManager->getRepository(NdsType::class)
            ->findOneBy(['name' => $given]);

        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var DealAgent $contractor */
        $contractor = $deal->getContractor();
        /** @var CivilLawSubject $contractorCivilLawSubject */
        $contractorCivilLawSubject = $contractor->getCivilLawSubject();
        // Подменяем ndsType
        $this->setNdsType($contractorCivilLawSubject, $ndsType);

        $overpay_payment_purpose = $this->bankClientPaymentOrderManager
            ->generatePayOffPaymentPurpose($deal, PayOffManager::PURPOSE_TYPE_OVERPAY, $payment->getReleasedValue());

        $this->assertEquals(
            $expected['pre'] . ' ' . PayOffManager::PURPOSE_SUBJECT . ' ' . $deal->getNumber() . '. ' . $expected['post'] . '.',
            $overpay_payment_purpose
        );
    }

    /**
     * @return array
     */
    public function dataGeneratePaymentPurposeForOverpay(): array
    {
        return [
            [null, [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['0', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['без НДС', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['10%', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
            ['18%', [
                'pre' => PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY],
                'post' => NdsTypeManager::NOT_SUBJECT_FOR_NDS
            ]],
        ];
    }

     /**
     * @preserveGlobalState disabled
     * @throws \ReflectionException
     */
    public function testGetOutgoingPurposeTypeWithPayment()
    {
        $paymentOrder = new PaymentOrder;
        $paymentOrders = new ArrayCollection;
        $paymentOrders->add($paymentOrder);

        $mockDeal = $this->generator->getMock(Deal::class, ['getPayment', 'getDispute']);

        $mockPayment = $this->generator->getMock(Payment::class, ['getPaymentOrders']);

        $mockDeal->expects($this->any())->method('getPayment')->will($this->returnValue($mockPayment));

        $mockPayment->expects($this->any())->method('getPaymentOrders')->will($this->returnValue($paymentOrders));

        //call private method
        $result = $this->invokeMethod($this->bankClientPaymentOrderManager, 'getOutgoingPurposeType', [$mockDeal, $paymentOrder]);

        $this->assertEquals(PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_PAYOFF], $result);
    }

    /**
     * @preserveGlobalState disabled
     * @throws \ReflectionException
     */
    public function testGetOutgoingPurposeTypeWithDiscount()
    {
        $paymentOrder = new PaymentOrder;
        $paymentOrders = new ArrayCollection;
        $paymentOrders->add($paymentOrder);

        $mockDeal = $this->generator->getMock(Deal::class, ['getPayment', 'getDispute']);
        $mockDispute = $this->generator->getMock(Dispute::class, ['getDiscount','getRefund']);

        $mockPayment = $this->generator->getMock(Payment::class, ['getPaymentOrders']);
        $mockDiscount = $this->generator->getMock(Discount::class, ['getPaymentOrders']);

        $mockDeal->expects($this->any())->method('getPayment')->will($this->returnValue($mockPayment));
        $mockDeal->expects($this->any())->method('getDispute')->will($this->returnValue($mockDispute));
        $mockDispute->expects($this->any())->method('getDiscount')->will($this->returnValue($mockDiscount));

        $mockPayment->expects($this->any())->method('getPaymentOrders')->will($this->returnValue(new ArrayCollection));
        $mockDiscount->expects($this->any())->method('getPaymentOrders')->will($this->returnValue($paymentOrders));

        //call private method
        $result = $this->invokeMethod($this->bankClientPaymentOrderManager, 'getOutgoingPurposeType', [$mockDeal, $paymentOrder]);

        $this->assertEquals(PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_DISCOUNT], $result);
    }

    /**
     * @preserveGlobalState disabled
     * @throws \ReflectionException
     */
    public function testGetOutgoingPurposeTypeWithRefund()
    {
        $paymentOrder = new PaymentOrder;
        $paymentOrders = new ArrayCollection;
        $paymentOrders->add($paymentOrder);

        $mockDeal = $this->generator->getMock(Deal::class, ['getPayment', 'getDispute']);
        $mockDispute = $this->generator->getMock(Dispute::class, ['getDiscount','getRefund']);

        $mockPayment = $this->generator->getMock(Payment::class, ['getPaymentOrders']);
        $mockDiscount = $this->generator->getMock(Discount::class, ['getPaymentOrders']);
        $mockRefund = $this->generator->getMock(Refund::class, ['getPaymentOrders']);

        $mockDeal->expects($this->any())->method('getPayment')->will($this->returnValue($mockPayment));
        $mockDeal->expects($this->any())->method('getDispute')->will($this->returnValue($mockDispute));
        $mockDispute->expects($this->any())->method('getDiscount')->will($this->returnValue($mockDiscount));
        $mockDispute->expects($this->any())->method('getRefund')->will($this->returnValue($mockRefund));

        $mockPayment->expects($this->any())->method('getPaymentOrders')->will($this->returnValue(new ArrayCollection));
        $mockDiscount->expects($this->any())->method('getPaymentOrders')->will($this->returnValue(new ArrayCollection));
        $mockRefund->expects($this->any())->method('getPaymentOrders')->will($this->returnValue($paymentOrders));

        //call private method
        $result = $this->invokeMethod($this->bankClientPaymentOrderManager, 'getOutgoingPurposeType', [$mockDeal, $paymentOrder]);

        $this->assertEquals(PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_REFUND], $result);
    }

    /**
     * @preserveGlobalState disabled
     * @throws \ReflectionException
     * @expectedException \Exception
     * @expectedExceptionMessage No PaymentOrder owner found
     */
    public function testGetOutgoingPurposeTypeWithNoPaymentOrder()
    {
        $paymentOrder = new PaymentOrder;

        $mockDeal = $this->generator->getMock(Deal::class, ['getPayment', 'getDispute']);
        $mockDispute = $this->generator->getMock(Dispute::class, ['getDiscount','getRefund']);

        $mockPayment = $this->generator->getMock(Payment::class, ['getPaymentOrders']);
        $mockDiscount = $this->generator->getMock(Discount::class, ['getPaymentOrders']);
        $mockRefund = $this->generator->getMock(Refund::class, ['getPaymentOrders']);

        $mockDeal->expects($this->any())->method('getPayment')->will($this->returnValue($mockPayment));
        $mockDeal->expects($this->any())->method('getDispute')->will($this->returnValue($mockDispute));
        $mockDispute->expects($this->any())->method('getDiscount')->will($this->returnValue($mockDiscount));
        $mockDispute->expects($this->any())->method('getRefund')->will($this->returnValue($mockRefund));

        $mockPayment->expects($this->any())->method('getPaymentOrders')->will($this->returnValue(new ArrayCollection));
        $mockDiscount->expects($this->any())->method('getPaymentOrders')->will($this->returnValue(new ArrayCollection));
        $mockRefund->expects($this->any())->method('getPaymentOrders')->will($this->returnValue(new ArrayCollection));

        //call private method
        $this->invokeMethod($this->bankClientPaymentOrderManager, 'getOutgoingPurposeType', [$mockDeal, $paymentOrder]);
    }


    /**
     * @param $object
     * @param $methodName
     * @param array $parameters
     * @return mixed
     * @throws \ReflectionException
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(\get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * @param $repository
     * @param $repositoryName
     * @param $repositoryMethod
     * @param $repositoryMethodReturnVal
     * @return \PHPUnit\Framework\MockObject\MockObject
     * @throws \ReflectionException
     */
    public function mockedDoctrineRepository($repository, $repositoryName, $repositoryMethod, $repositoryMethodReturnVal) {
        $generator = new Generator;
        $mockEM=$generator->getMock('\Doctrine\ORM\EntityManager',
            array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);
        $mockSVRepo=$generator->getMock($repository,array($repositoryMethod),array(),'',false);

        $mockEM->expects($this->any())
            ->method('getClassMetadata')
            ->will($this->returnValue((object)array('name' => 'aClass')));
        $mockEM->expects($this->any())
            ->method('persist')
            ->will($this->returnValue(null));
        $mockEM->expects($this->any())
            ->method('flush')
            ->will($this->returnValue(null));

        $mockSVRepo
            ->expects($this->once())
            ->method($repositoryMethod)
            ->will($this->returnValue($repositoryMethodReturnVal));

        $mockEM->expects($this->once())
            ->method('getRepository')
            ->with($repositoryName)
            ->will($this->returnValue($mockSVRepo));

        return $mockEM;
    }

    /**
     * @param Deal $deal
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createDelivery(Deal $deal)
    {
        $delivery = new Delivery();
        $delivery->setDeal($deal);
        $delivery->setDeliveryDate(new \DateTime());
        $delivery->setDeliveryStatus(true);

        $this->entityManager->persist($delivery);

        $deal->addDelivery($delivery);

        $this->entityManager->persist($deal);

        $this->entityManager->flush();
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return Discount
     * @throws \Core\Exception\LogicException
     */
    private function createDiscount(Deal $deal, $amount): Discount
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();

        $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($deal);

        $discount = new Discount();
        $discount->setPaymentMethod($customerPaymentMethod);
        $discount->setCreated(new \DateTime());
        $discount->setDispute($dispute);
        $discount->setAmount($amount);
        $discount->setDisputeCycle($disputeCycle);

        return $discount;
    }

    /**
     * @param Deal $deal
     * @return Refund
     * @throws \Core\Exception\LogicException
     */
    private function createRefund(Deal $deal): Refund
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        /** @var DisputeCycle $disputeCycle */
        $disputeCycle = $dispute->getDisputeCycles()->last();

        $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($deal);

        $refund = new Refund();
        $refund->setPaymentMethod($customerPaymentMethod);
        $refund->setCreated(new \DateTime());
        $refund->setDisputeCycle($disputeCycle);

        return $refund;
    }

    /**
     * @param Deal $deal
     * @param $amount
     * @return RepaidOverpay
     * @throws \Core\Exception\LogicException
     */
    private function createRepaidOverpay(Deal $deal, $amount): RepaidOverpay
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $customerPaymentMethod = $this->paymentMethodManager->getCustomerPaymentMethod($deal);

        $repaidOverpay = new RepaidOverpay();
        $repaidOverpay->setPaymentMethod($customerPaymentMethod);
        $repaidOverpay->setAmount($amount);
        $repaidOverpay->setCreated(new \DateTime());
        $repaidOverpay->setPayment($payment);

        return $repaidOverpay;
    }

    /**
     * @param CivilLawSubject $civilLawSubject
     * @param NdsType|null $ndsType
     * @return CivilLawSubject
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function setNdsType(CivilLawSubject $civilLawSubject, $ndsType): CivilLawSubject
    {
        $civilLawSubject->setNdsType($ndsType);

        $this->entityManager->persist($civilLawSubject);
        $this->entityManager->flush();

        return $civilLawSubject;
    }
}