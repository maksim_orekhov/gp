<?php

namespace ApplicationTest\Controller;

use Application\Controller\InvoiceController;
use Application\Entity\Payment;
use ModulePaymentOrder\Controller\BankClientPaymentOrderController;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Zend\Session\SessionManager;
use Application\Entity\Deal;
use CoreTest\ViewVarsTrait;

/**
 * Class BankClientPaymentOrderControllerTest
 * @package ApplicationTest\Controller
 */
class BankClientPaymentOrderControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TROUBLE_DEAL_NAME = 'Сделка Проблемная платежка';

    protected $traceError = true;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var \Application\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);


        $user = $this->userManager->selectUserByLogin($this->user_login);
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Недоступность /bank-client/payment-orders/trouble не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testTroubleActionCanNotBeAccessedByUnverifiedUser()
    {
        // не авторизуемся!!!
        $this->dispatch('/bank-client/payment-orders/trouble', 'GET');
        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('modulePaymentOrder');
        $this->assertControllerName(BankClientPaymentOrderController::class);
        $this->assertRedirectTo('/login?redirectUrl=/bank-client/payment-orders/trouble');
    }

    /**
     * Недоступность /bank-client/payment-orders/trouble не авторизованному пользователю Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testTroubleActionCanNotBeAccessedByUnverifiedUserForAjax()
    {
        // не авторизуемся!!!
        $isXmlHttpRequest = true;
        $this->dispatch('/bank-client/payment-orders/trouble', 'GET', [], $isXmlHttpRequest);
        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('modulePaymentOrder');
        $this->assertControllerName(BankClientPaymentOrderController::class);
        $this->assertRedirectTo('/login?redirectUrl=/bank-client/payment-orders/trouble');
    }

    /**
     * Недоступность /bank-client/payment-orders/trouble авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testTroubleActionCanNotBeAccessedByVerifiedUser()
    {
        // авторизуемся
        $this->login();

        $this->dispatch('/bank-client/payment-orders/trouble', 'GET');
        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('modulePaymentOrder');
        $this->assertControllerName(BankClientPaymentOrderController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Недоступность /bank-client/payment-orders/trouble авторизованному пользователю Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testTroubleActionCanNotBeAccessedByVerifiedUserForAjax()
    {
        // авторизуемся
        $this->login();

        $isXmlHttpRequest = true;
        $this->dispatch('/bank-client/payment-orders/trouble', 'GET', [], $isXmlHttpRequest);
        //основная проверка
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('modulePaymentOrder');
        $this->assertControllerName(BankClientPaymentOrderController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Доступность /bank-client/payment-orders/trouble и коллекции оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testTroubleActionCanBeAccessedByOperator()
    {
        // авторизуемся под оператором
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TROUBLE_DEAL_NAME]);

        $this->assertNotNull($deal);

        $this->dispatch('/bank-client/payment-orders/trouble', 'GET');
        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('modulePaymentOrder');
        $this->assertControllerName(BankClientPaymentOrderController::class);
        $this->assertMatchedRouteName('bank-client-payment-orders-trouble');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('payments', $view_vars);
        //проверяем что в коллекции что то есть
        $this->assertTrue(\count($view_vars['payments']) > 0);
    }

    /**
     * Доступность /bank-client/payment-orders/trouble и коллекции оператору Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testTroubleActionCanBeAccessedByOperatorForAjax()
    {
        // авторизуемся под оператором
        $this->login('operator');

        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::TROUBLE_DEAL_NAME]);

        $this->assertNotNull($deal);

        $isXmlHttpRequest = true;
        $this->dispatch('/bank-client/payment-orders/trouble', 'GET', [], $isXmlHttpRequest);

        $data = json_decode($this->getResponse()->getContent(), true);
        //основная проверка
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('modulePaymentOrder');
        $this->assertControllerName(BankClientPaymentOrderController::class);
        $this->assertMatchedRouteName('bank-client-payment-orders-trouble');

        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);

        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('payments', $data['data']);
        //проверяем что в коллекции что то есть
        $this->assertTrue(\count($data['data']['payments']) > 0);
    }


    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}