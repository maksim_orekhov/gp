<?php

namespace ModulePaymentOrder\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\BankClientPaymentOrderFile;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * @package ModulePaymentOrder\Entity
 * @ORM\Table(name="bank_client_payment_order")
 * @ORM\Entity(repositoryClass="ModulePaymentOrder\Entity\Repository\BankClientPaymentOrderRepository")
 */
class BankClientPaymentOrder implements PaymentTransactionInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=8, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="document_type", type="string", length=255, nullable=false)
     */
    private $documentType; // СекцияДокумент

    /**
     * @var integer
     *
     * @ORM\Column(name="document_number", type="integer", length=11, nullable=false)
     */
    private $documentNumber; // Номер

    /**
     * @var string
     *
     * @ORM\Column(name="document_date", type="string", length=10, nullable=false)
     */
    private $documentDate; // Дата

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", precision=12, scale=2, nullable=false)
     */
    private $amount; // Сумма

    /**
     * @var string
     *
     * @ORM\Column(name="recipient_account", type="string", length=20, nullable=false)
     */
    private $recipientAccount; // ПолучательСчет

    /**
     * @var string
     *
     * @ORM\Column(name="recipient", type="string", length=255, nullable=true)
     */
    private $recipient; // Получатель

    /**
     * @var string
     *
     * @ORM\Column(name="recipient_inn", type="string", length=12, nullable=true)
     */
    private $recipientInn; // ПолучательИНН

    /**
     * @var string
     *
     * @ORM\Column(name="recipient_1", type="string", length=255, nullable=false)
     */
    private $recipient1; // Получатель1

    /**
     * @var string
     *
     * @ORM\Column(name="recipient_kpp", type="string", length=9, nullable=true)
     */
    private $recipientKpp; // ПолучательКПП

    /**
     * @var string
     *
     * @ORM\Column(name="recipient_bank_1", type="string", length=255, nullable=false)
     */
    private $recipientBank1; // ПолучательБанк1

    /**
     * @var string
     *
     * @ORM\Column(name="recipient_bik", type="string", length=9, nullable=false)
     */
    private $recipientBik; // ПолучательБИК

    /**
     * @var string
     *
     * @ORM\Column(name="recipient_corr_account", type="string", length=20, nullable=true)
     */
    private $recipientCorrAccount; // ПолучательКорсчет

    /**
     * @var string
     *
     * @ORM\Column(name="payer_account", type="string", length=20, nullable=false)
     */
    private $payerAccount; // ПлательщикСчет

    /**
     * @var string
     *
     * @ORM\Column(name="payer", type="string", length=255, nullable=true)
     */
    private $payer; // Плательщик

    /**
     * @var string
     *
     * @ORM\Column(name="payer_inn", type="string", length=12, nullable=true)
     */
    private $payerInn; // ПлательщикИНН

    /**
     * @var string
     *
     * @ORM\Column(name="payer_1", type="string", length=255, nullable=false)
     */
    private $payer1; // Плательщик1

    /**
     * @var string
     *
     * @ORM\Column(name="payer_kpp", type="string", length=9, nullable=true)
     */
    private $payerKpp; // ПлательщикКПП

    /**
     * @var string
     *
     * @ORM\Column(name="payer_bank_1", type="string", length=255, nullable=false)
     */
    private $payerBank1; // ПлательщикБанк1

    /**
     * @var string
     *
     * @ORM\Column(name="payer_bik", type="string", length=9, nullable=false)
     */
    private $payerBik; // ПлательщикБИК

    /**
     * @var string
     *
     * @ORM\Column(name="payer_corr_account", type="string", length=20, nullable=false)
     */
    private $payerCorrAccount; // ПлательщикКорсчет

    /**
     * @var string
     *
     * @ORM\Column(name="payment_purpose", type="string", length=255, nullable=false)
     */
    private $paymentPurpose; // НазначениеПлатежа

    /**
     * @var string
     *
     * @ORM\Column(name="payment_type", type="string", nullable=false)
     */
    private $paymentType; // ВидОплаты

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer", nullable=false)
     */
    private $priority; // Очередность

    /**
     * @var string
     *
     * @ORM\Column(name="date_of_receipt", type="string", length=10, nullable=true)
     */
    private $dateOfReceipt; // ДатаПоступило

    /**
     * @var string
     *
     * @ORM\Column(name="date_of_debit", type="string", length=10, nullable=true)
     */
    private $dateOfDebit; // ДатаСписано

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, nullable=false)
     */
    private $hash;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * Many BankClientPaymentOrders have One BankClientPaymentOrderFile.
     * @ORM\ManyToOne(targetEntity="Application\Entity\BankClientPaymentOrderFile", inversedBy="bankClientPaymentOrders")
     * @ORM\JoinColumn(name="bank_client_payment_order_file_id", referencedColumnName="id", nullable=true)
     */
    private $bankClientPaymentOrderFile;

    /**
     * One BankClientPaymentOrder has One PaymentOrder.
     * @ORM\OneToOne(targetEntity="PaymentOrder", inversedBy="bankClientPaymentOrder")
     * @ORM\JoinColumn(name="payment_order_id", referencedColumnName="id")
     */
    private $paymentOrder;


    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        // @TODO Перенести в менеджер
        if (!in_array($type, array(PaymentOrderManager::TYPE_INCOMING, PaymentOrderManager::TYPE_OUTGOING))) {

            throw new \InvalidArgumentException("Invalid type");
        }

        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDocumentType(): string
    {
        return $this->documentType;
    }

    /**
     * @param string $documentType
     */
    public function setDocumentType(string $documentType)
    {
        $this->documentType = $documentType;
    }

    /**
     * @return int
     */
    public function getDocumentNumber(): int
    {
        return $this->documentNumber;
    }

    /**
     * @param int $documentNumber
     */
    public function setDocumentNumber(int $documentNumber)
    {
        $this->documentNumber = $documentNumber;
    }

    /**
     * @return string
     */
    public function getDocumentDate(): string
    {
        return $this->documentDate;
    }

    /**
     * @param string $documentDate
     */
    public function setDocumentDate(string $documentDate)
    {
        $this->documentDate = $documentDate;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getRecipientAccount(): string
    {
        return $this->recipientAccount;
    }

    /**
     * @param string $recipientAccount
     */
    public function setRecipientAccount(string $recipientAccount)
    {
        $this->recipientAccount = $recipientAccount;
    }

    /**
     * @return string|null
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param string|null $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return string|null
     */
    public function getRecipientInn()
    {
        return $this->recipientInn;
    }

    /**
     * @param string|null $recipientInn
     */
    public function setRecipientInn($recipientInn)
    {
        $this->recipientInn = $recipientInn;
    }

    /**
     * @return string
     */
    public function getRecipient1(): string
    {
        return $this->recipient1;
    }

    /**
     * @param string $recipient1
     */
    public function setRecipient1(string $recipient1)
    {
        $this->recipient1 = $recipient1;
    }

    /**
     * @return string|null
     */
    public function getRecipientKpp()
    {
        return $this->recipientKpp;
    }

    /**
     * @param string|null $recipientKpp
     */
    public function setRecipientKpp($recipientKpp)
    {
        $this->recipientKpp = $recipientKpp;
    }

    /**
     * @return string
     */
    public function getRecipientBank1(): string
    {
        return $this->recipientBank1;
    }

    /**
     * @param string $recipientBank1
     */
    public function setRecipientBank1(string $recipientBank1)
    {
        $this->recipientBank1 = $recipientBank1;
    }

    /**
     * @return string
     */
    public function getRecipientBik(): string
    {
        return $this->recipientBik;
    }

    /**
     * @param string $recipientBik
     */
    public function setRecipientBik(string $recipientBik)
    {
        $this->recipientBik = $recipientBik;
    }

    /**
     * @return string
     */
    public function getRecipientCorrAccount()
    {
        return $this->recipientCorrAccount;
    }

    /**
     * @param string|null $recipientCorrAccount
     */
    public function setRecipientCorrAccount($recipientCorrAccount)
    {
        $this->recipientCorrAccount = $recipientCorrAccount;
    }

    /**
     * @return string
     */
    public function getPayerAccount(): string
    {
        return $this->payerAccount;
    }

    /**
     * @param string $payerAccount
     */
    public function setPayerAccount(string $payerAccount)
    {
        $this->payerAccount = $payerAccount;
    }

    /**
     * @return string|null
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * @param string|null $payer
     */
    public function setPayer($payer)
    {
        $this->payer = $payer;
    }

    /**
     * @return string|null
     */
    public function getPayerInn()
    {
        return $this->payerInn;
    }

    /**
     * @param string|null $payerInn
     */
    public function setPayerInn($payerInn)
    {
        $this->payerInn = $payerInn;
    }

    /**
     * @return string
     */
    public function getPayer1(): string
    {
        return $this->payer1;
    }

    /**
     * @param string $payer1
     */
    public function setPayer1(string $payer1)
    {
        $this->payer1 = $payer1;
    }

    /**
     * @return string|null
     */
    public function getPayerKpp()
    {
        return $this->payerKpp;
    }

    /**
     * @param string|null $payerKpp
     */
    public function setPayerKpp($payerKpp)
    {
        $this->payerKpp = $payerKpp;
    }

    /**
     * @return string
     */
    public function getPayerBank1(): string
    {
        return $this->payerBank1;
    }

    /**
     * @param string $payerBank1
     */
    public function setPayerBank1(string $payerBank1)
    {
        $this->payerBank1 = $payerBank1;
    }

    /**
     * @return string
     */
    public function getPayerBik(): string
    {
        return $this->payerBik;
    }

    /**
     * @param string $payerBik
     */
    public function setPayerBik(string $payerBik)
    {
        $this->payerBik = $payerBik;
    }

    /**
     * @return string
     */
    public function getPayerCorrAccount(): string
    {
        return $this->payerCorrAccount;
    }

    /**
     * @param string $payerCorrAccount
     */
    public function setPayerCorrAccount(string $payerCorrAccount)
    {
        $this->payerCorrAccount = $payerCorrAccount;
    }

    /**
     * @return string
     */
    public function getPaymentPurpose(): string
    {
        return $this->paymentPurpose;
    }

    /**
     * @param string $paymentPurpose
     */
    public function setPaymentPurpose(string $paymentPurpose)
    {
        $this->paymentPurpose = $paymentPurpose;
    }

    /**
     * @return string
     */
    public function getPaymentType(): string
    {
        return $this->paymentType;
    }

    /**
     * @param string $paymentType
     */
    public function setPaymentType(string $paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return string
     */
    public function getDateOfReceipt()
    {
        return $this->dateOfReceipt;
    }

    /**
     * @param string|null $dateOfReceipt
     */
    public function setDateOfReceipt(string $dateOfReceipt = null)
    {
        $this->dateOfReceipt = $dateOfReceipt;
    }

    /**
     * @return string
     */
    public function getDateOfDebit()
    {
        return $this->dateOfDebit;
    }

    /**
     * @param string|null $dateOfDebit
     */
    public function setDateOfDebit(string $dateOfDebit = null)
    {
        $this->dateOfDebit = $dateOfDebit;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return mixed
     */
    public function getBankClientPaymentOrderFile()
    {
        return $this->bankClientPaymentOrderFile;
    }

    /**
     * @param mixed $bankClientPaymentOrderFile
     */
    public function setBankClientPaymentOrderFile($bankClientPaymentOrderFile)
    {
        $this->bankClientPaymentOrderFile = $bankClientPaymentOrderFile;
    }

    /**
     * @return mixed
     */
    public function getPaymentOrder()
    {
        return $this->paymentOrder;
    }

    /**
     * @param $paymentOrder
     * @return mixed
     */
    public function setPaymentOrder($paymentOrder)
    {
        $this->paymentOrder = $paymentOrder;
    }
}
