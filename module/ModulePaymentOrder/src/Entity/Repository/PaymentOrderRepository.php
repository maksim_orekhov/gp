<?php

namespace ModulePaymentOrder\Entity\Repository;

use Application\Entity\Payment;
use Doctrine\ORM\EntityRepository;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Service\PaymentOrderManager;

/**
 * Class PaymentOrderRepository
 * @package ModulePaymentOrder\Entity\Repository
 */
class PaymentOrderRepository extends EntityRepository
{
    /**
     * @param array $payment_orders_ids
     * @return mixed|null
     *
     * @TODO Не используется. Удалить!
     */
    public function findPaymentOrderByOutgoingPaymentTransaction(array $payment_orders_ids)
    {
        $qb = $this->createQueryBuilder('po')
            ->innerJoin(BankClientPaymentOrder::class, 'bcpo')
            ->andWhere('po.id IN (:payment_orders_ids)')
            ->andWhere('bcpo.paymentOrderId = po.id')
            ->andWhere('bcpo.type = :type')
            ->setParameter('payment_orders_ids', $payment_orders_ids['bank_transfer'])
            ->setParameter('type', PaymentOrderManager::TYPE_OUTGOING);

        try {

            $result = $qb->getQuery()->getSingleResult();
        }
        catch (\Throwable $t){

            $result = null;
        }

        return $result;
    }

    /**
     * @param array $payment_orders_ids
     * @return array
     *
     * @TODO Не используется. Удалить!
     */
    public function findPaymentOrdersByIncomingPaymentTransactions(array $payment_orders_ids): array
    {
        $qb = $this->createQueryBuilder('po')
            ->innerJoin(BankClientPaymentOrder::class, 'bcpo')
            ->andWhere('bcpo.paymentOrderId = po.id')
            ->andWhere('po.id IN (:payment_orders_ids)')
            ->andWhere('bcpo.type = :type')
            ->setParameter('payment_orders_ids', $payment_orders_ids['bank_transfer'])
            ->setParameter('type', PaymentOrderManager::TYPE_INCOMING);

        return $qb->getQuery()->getResult();
    }
}