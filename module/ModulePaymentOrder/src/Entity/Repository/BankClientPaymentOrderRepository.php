<?php

namespace ModulePaymentOrder\Entity\Repository;

use Application\Entity\Repository\BaseRepository;
use Application\Entity\Interfaces\PaymentOrderOwnerInterface;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Doctrine\ORM\QueryBuilder;

/**
 * Class BankClientPaymentOrder
 * @package ModulePaymentOrder\Entity\Repository
 */
class BankClientPaymentOrderRepository extends BaseRepository
{
    const PER_PAGE_DEFAULT = 10;

    const ALIASES = [
        'payment_id'                => 'bcpo.id',
        'payment_purpose'           => 'bcpo.paymentPurpose',
        'payment_amount'            => 'bcpo.amount',
        'payment_created_from'      => 'bcpo.created',
        'payment_created_till'      => 'bcpo.created',
        'payment_recipient_account' => 'bcpo.recipientAccount',
        'payment_payer'             => 'bcpo.payer',
        'payment_payer_account'     => 'bcpo.payerAccount',
        'trouble_type'              => 'custom'
    ];

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @param string $deal_number
     * @param string $purpose_type
     * @return mixed|null
     */
    public function findConfirmationBankClientPaymentOrder(BankClientPaymentOrder $bankClientPaymentOrder,
                                                           PaymentOrderOwnerInterface $paymentOrderOwner,
                                                           string $deal_number,
                                                           string $purpose_type)
    {
        $qb = $this->createQueryBuilder('bcpo')
            ->andWhere('bcpo.paymentOrder IN (:paymentOrderOwnerCollection)')
            ->andWhere('bcpo.amount = :amount')
            ->andWhere('bcpo.payerAccount = :payerAccount')
            #->andWhere('bcpo.recipientAccount = :recipientAccount') // Отключаем по GP-1656
            ->andWhere('bcpo.type = :payment_order_type')
            ->andWhere('bcpo.paymentPurpose LIKE :deal_number')
            ->andWhere('bcpo.paymentPurpose LIKE :purpose')
        ;

        $qb->setParameter('amount', $bankClientPaymentOrder->getAmount())
            ->setParameter('payerAccount', $bankClientPaymentOrder->getPayerAccount())
            #->setParameter('recipientAccount', $bankClientPaymentOrder->getRecipientAccount())
            ->setParameter('payment_order_type', PaymentOrderManager::TYPE_INCOMING)
            ->setParameter('deal_number', '%'.$deal_number.'%')
            ->setParameter('purpose', '%'.$purpose_type.'%')
            ->setParameter('paymentOrderOwnerCollection', $paymentOrderOwner->getPaymentOrders())
        ;

        try {

            $result = $qb->getQuery()->getSingleResult();
        }
        catch (\Throwable $t){

            $result = null;
        }

        return $result;
    }

    /**
     * @param $params
     * @return mixed|null
     */
    public function getTroubleBankClientPaymentOrders($params)
    {
        $qb = $this->createQueryBuilder('bcpo');

        // Добавляем к запросу параметры фильтрации
        if (array_key_exists('filter', $params)) {
            // Другие параметры
            $this->addQueryBuilderFilterParams($qb, $params['filter']);
        } else {
            $qb->andWhere('bcpo.paymentOrder is null OR (bcpo.bankClientPaymentOrderFile is null AND bcpo.type = :incoming)');
            $qb->setParameter('incoming', PaymentOrderManager::TYPE_INCOMING);
        }

        // Добавляем к запросу параметры сортировки
        if (array_key_exists('order', $params)) {
            $this->addQueryBuilderSortParams($qb, $params['order']);
        } else {
            $qb->addOrderBy('bcpo.id', 'DESC');
        }

        $page = isset($params['page'])
            ? $params['page']
            : 1
        ;

        $per_page = isset($params['per_page'])
            ? $params['per_page']
            : self::PER_PAGE_DEFAULT
        ;

        return $this->paginate($qb, $page, $per_page);
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderFilterParams(QueryBuilder $qb, array $params)
    {
        $trouble_type_used = false;

        foreach ($params as $item) {
            if ($item['property'] === 'trouble_type') {
                if ($item['value'] === 'no file binding') {

                    $qb->andWhere('bcpo.bankClientPaymentOrderFile is null AND bcpo.type = :incoming');
                    $qb->setParameter('incoming', PaymentOrderManager::TYPE_INCOMING);

                    $trouble_type_used = true;
                    break;

                } elseif ($item['value'] === 'not included in any collection') {

                    $qb->andWhere('bcpo.paymentOrder is null');

                    $trouble_type_used = true;
                    break;
                }
            }

            // common cases
            $qb->andWhere(
                self::ALIASES[$item['property']] . ' ' . self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['operator'] . ' :' . $item['property'])
                ->setParameter($item['property'],
                    self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['pre_item'] . $item['value'] . self::OPERATORS_FOR_FILTER_RULES[$item['rule']]['post_item']
                );
        }

        if (!$trouble_type_used) {
            $qb->andWhere('bcpo.paymentOrder is null OR (bcpo.bankClientPaymentOrderFile is null AND bcpo.type = :incoming)');
            $qb->setParameter('incoming', PaymentOrderManager::TYPE_INCOMING);
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param array $params
     */
    private function addQueryBuilderSortParams(QueryBuilder $qb, array $params)
    {
        foreach ($params as $key=>$value) {
            switch ($key) {
                case 'id':
                    $qb->addOrderBy('bcpo.id', $value);
                    break;
                case 'payment_purpose':
                    $qb->addOrderBy('bcpo.paymentPurpose', $value);
                    break;
                case 'amount':
                    $qb->addOrderBy('bcpo.amount', $value);
                    break;
                case 'created':
                    $qb->addOrderBy('bcpo.created', $value);
                    break;
                case 'recipient_account':
                    $qb->addOrderBy('bcpo.recipientAccount', $value);
                    break;
                case 'payer':
                    $qb->addOrderBy('bcpo.payer', $value);
                    break;
                case 'payer_account':
                    $qb->addOrderBy('bcpo.payerAccount', $value);
                    break;
                default:
                    $qb->addOrderBy('bcpo.id', 'DESC');
            }
        }
    }
}