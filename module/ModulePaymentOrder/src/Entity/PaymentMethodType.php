<?php

namespace ModulePaymentOrder\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentMethodType
 *
 * @ORM\Table(name="payment_method_type")
 * @ORM\Entity
 */
class PaymentMethodType
{
    const BANK_TRANSFER = 'bank_transfer';
    const ACQUIRING_MANDARIN = 'acquiring_mandarin';
    const EMONEY = 'emoney';

    const FIELD_BANK_TRANSFER = [
        'bank_name',
        'bik',
        'account_number',
        'corr_account_number',
    ];

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="output_name", type="string", length=45, nullable=false, unique=true)
     */
    private $outputName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=500, nullable=true, unique=false)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true, unique=false)
     */
    private $isActive;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PaymentMethodType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getOutputName(): string
    {
        return $this->outputName;
    }

    /**
     * @param string $outputName
     */
    public function setOutputName(string $outputName)
    {
        $this->outputName = $outputName;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PaymentMethodType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return PaymentMethodType
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}

