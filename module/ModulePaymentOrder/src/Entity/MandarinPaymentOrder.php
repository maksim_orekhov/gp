<?php

namespace ModulePaymentOrder\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @package ModulePaymentOrder\Entity
 * @ORM\Table(name="mandarin_payment_order")
 * @ORM\Entity()
 */
class MandarinPaymentOrder implements PaymentTransactionInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=8, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="text", length=500, nullable=true)
     */
    private $orderId; // зашифрованный токен (deal_id, timestamp и назначение платежа)

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="decimal", precision=12, scale=2, nullable=false)
     */
    private $amount; // Сумма ("price" в системе Mandarin)

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=6, nullable=false)
     */
    private $action; // (pay, payout) Действие (оплата или выплата на карту)

    /**
     * @var string
     *
     * @ORM\Column(name="transaction", type="string", length=64, nullable=true)
     */
    private $transaction; // ID транзакции в системе Mandarin

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16, nullable=false)
     */
    private $status; // (success, failed, payout-only) Статус операции

    /**
     * @var string
     *
     * @ORM\Column(name="customer_email", type="string", length=64, nullable=true)
     */
    private $customerEmail; // Email пользователя

    /**
     * @var string
     *
     * @ORM\Column(name="customer_phone", type="string", length=64, nullable=true)
     */
    private $customerPhone; // Телефон пользователя

    /**
     * @var string
     *
     * @ORM\Column(name="card_holder", type="string", length=64, nullable=true)
     */
    private $cardHolder; // Держатель карты

    /**
     * @var string
     *
     * @ORM\Column(name="card_number", type="string", length=16, nullable=true)
     */
    private $cardNumber; // Номер карты

    /**
     * @var string
     *
     * @ORM\Column(name="card_expiration_year", type="string", length=4, nullable=true)
     */
    private $cardExpirationYear; // Год срока действия карты

    /**
     * @var string
     *
     * @ORM\Column(name="card_expiration_month", type="string", length=2, nullable=true)
     */
    private $cardExpirationMonth; // Месяц срока действия карты

    /**
     * @var integer
     *
     * @ORM\Column(name="error_code", type="integer", length=2, nullable=true)
     */
    private $errorCode; // Код ошибки

    /**
     * @var string
     *
     * @ORM\Column(name="error_description", type="text", length=500, nullable=true)
     */
    private $errorDescription; // Описание ошибки

    /**
     * @var string
     *
     * @ORM\Column(name="payment_purpose", type="string", length=255, nullable=false)
     */
    private $paymentPurpose; // Назначение Платежа

//    /**
//     * @var string
//     *
//     * @ORM\Column(name="hash", type="string", length=255, nullable=false)
//     */
//    private $hash;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

//    /**
//     * Many BankClientPaymentOrders have One BankClientPaymentOrderFile.
//     * @ORM\ManyToOne(targetEntity="Application\Entity\BankClientPaymentOrderFile", inversedBy="bankClientPaymentOrders")
//     * @ORM\JoinColumn(name="bank_client_payment_order_file_id", referencedColumnName="id", nullable=true)
//     */
//    private $bankClientPaymentOrderFile;

    /**
     * One MandarinPaymentOrder has One PaymentOrder.
     * @ORM\OneToOne(targetEntity="PaymentOrder", inversedBy="mandarinPaymentOrder")
     * @ORM\JoinColumn(name="payment_order_id", referencedColumnName="id")
     */
    private $paymentOrder;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     * @param string $orderId
     */
    public function setOrderId(string $orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction(string $action)
    {
        $this->action = $action;
    }

    /**
     * @return string|null
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param string|null $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @param string|null $customerEmail
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;
    }

    /**
     * @return string|null
     */
    public function getCustomerPhone()
    {
        return $this->customerPhone;
    }

    /**
     * @param string $customerPhone
     */
    public function setCustomerPhone(string $customerPhone)
    {
        $this->customerPhone = $customerPhone;
    }

    /**
     * @return string|null
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * @param string|null $cardHolder
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
    }

    /**
     * @return string|null
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string|null $cardNumber
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return string|null
     */
    public function getCardExpirationYear()
    {
        return $this->cardExpirationYear;
    }

    /**
     * @param string|null $cardExpirationYear
     */
    public function setCardExpirationYear($cardExpirationYear)
    {
        $this->cardExpirationYear = $cardExpirationYear;
    }

    /**
     * @return string|null
     */
    public function getCardExpirationMonth()
    {
        return $this->cardExpirationMonth;
    }

    /**
     * @param string|null $cardExpirationMonth
     */
    public function setCardExpirationMonth($cardExpirationMonth)
    {
        $this->cardExpirationMonth = $cardExpirationMonth;
    }

    /**
     * @return int|null
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param int|null $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return string|null
     */
    public function getErrorDescription()
    {
        return $this->errorDescription;
    }

    /**
     * @param string|null $errorDescription
     */
    public function setErrorDescription($errorDescription)
    {
        $this->errorDescription = $errorDescription;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getPaymentOrder()
    {
        return $this->paymentOrder;
    }

    /**
     * @param $paymentOrder
     * @return mixed|void
     */
    public function setPaymentOrder($paymentOrder)
    {
        $this->paymentOrder = $paymentOrder;
    }

    /**
     * @return string
     */
    public function getPaymentPurpose(): string
    {
        return $this->paymentPurpose;
    }

    /**
     * @param string $paymentPurpose
     */
    public function setPaymentPurpose(string $paymentPurpose)
    {
        $this->paymentPurpose = $paymentPurpose;
    }
}
