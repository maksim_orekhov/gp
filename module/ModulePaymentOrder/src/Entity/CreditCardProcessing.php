<?php

namespace ModulePaymentOrder\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Это просто заготовка класса
 * @TODO Когда оформится в сущность, прописать аннотации для ORM!
 * Должден реализовывать PaymentTransactionInterface
 */
class CreditCardProcessing
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
