<?php
namespace ModulePaymentOrder\Entity;

/**
 * Interface PaymentTransactionInterface
 * @package ModulePaymentOrder\Entity
 */
interface PaymentTransactionInterface
{
    /**
     * @return float
     */
    public function getAmount(): float;

    /**
     * @return mixed
     */
    public function getPaymentOrder();

    /**
     * @param $paymentOrder
     * @return mixed
     */
    public function setPaymentOrder($paymentOrder);

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @param string $type
     */
    public function setType(string $type);

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime;

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created);
}