<?php

namespace ModulePaymentOrder\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentOrder
 *
 * @ORM\Table(name="payment_order")
 * @ORM\Entity(repositoryClass="ModulePaymentOrder\Entity\Repository\PaymentOrderRepository")
 */
class PaymentOrder
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var PaymentMethodType
     *
     * @ORM\ManyToOne(targetEntity="PaymentMethodType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_method_type_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $paymentMethodType;

    /**
     * One BankClientPaymentOrder has One PaymentOrder.
     * @ORM\OneToOne(targetEntity="BankClientPaymentOrder", mappedBy="paymentOrder")
     */
    private $bankClientPaymentOrder;

    /**
     * One MandarinPaymentOrder has One PaymentOrder.
     * @ORM\OneToOne(targetEntity="MandarinPaymentOrder", mappedBy="paymentOrder")
     */
    private $mandarinPaymentOrder;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return PaymentMethodType
     */
    public function getPaymentMethodType(): PaymentMethodType
    {
        return $this->paymentMethodType;
    }

    /**
     * @param PaymentMethodType $paymentMethodType
     */
    public function setPaymentMethodType(PaymentMethodType $paymentMethodType)
    {
        $this->paymentMethodType = $paymentMethodType;
    }

    /**
     * @return mixed
     */
    public function getBankClientPaymentOrder()
    {
        return $this->bankClientPaymentOrder;
    }

    /**
     * @param mixed $bankClientPaymentOrder
     */
    public function setBankClientPaymentOrder($bankClientPaymentOrder)
    {
        $this->bankClientPaymentOrder = $bankClientPaymentOrder;
    }

    /**
     * @return mixed
     */
    public function getMandarinPaymentOrder()
    {
        return $this->mandarinPaymentOrder;
    }

    /**
     * @param mixed $mandarinPaymentOrder
     */
    public function setMandarinPaymentOrder($mandarinPaymentOrder)
    {
        $this->mandarinPaymentOrder = $mandarinPaymentOrder;
    }
}

