<?php
namespace ModulePaymentOrder\Exception;

interface PayOffExceptionCodeInterface
{
    /**
     * префикс PAYOFF, для сообшений суфикс _MESSAGE
     */
    const PAYOFF_TYPE_NOT_DEFINED = 'PAYOFF_TYPE_NOT_DEFINED';
    const PAYOFF_TYPE_NOT_DEFINED_MESSAGE = 'The type of payoff is not defined';

    const PAYOFF_CALCULATED_AMOUNT_ERROR = 'PAYOFF_CALCULATED_AMOUNT_ERROR';
    const PAYOFF_CALCULATED_AMOUNT_ERROR_MESSAGE = 'An error occurred while calculating the amount';

    const PAYOFF_PAYMENT_METHOD_NOT_DEFINED = 'PAYOFF_PAYMENT_METHOD_NOT_DEFINED';
    const PAYOFF_PAYMENT_METHOD_NOT_DEFINED_MESSAGE = 'Payoff payment method not defined';

    const PAYOFF_CREATE_METHOD_NOT_DEFINED = 'PAYOFF_CREATE_METHOD_NOT_DEFINED';
    const PAYOFF_CREATE_METHOD_NOT_DEFINED_MESSAGE = 'Payoff create method not defined';
}