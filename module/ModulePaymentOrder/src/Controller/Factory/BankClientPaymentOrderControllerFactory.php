<?php
namespace ModulePaymentOrder\Controller\Factory;

use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Controller\BankClientPaymentOrderController;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * This is the factory for AuthController
 */
class BankClientPaymentOrderControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $bankClientPaymentOrderManager = $container->get(BankClientPaymentOrderManager::class);
        $config = $container->get('Config');

        return new BankClientPaymentOrderController(
            $bankClientPaymentOrderManager,
            $config
        );
    }
}