<?php

namespace ModulePaymentOrder\Controller;

use ModulePaymentOrder\Form\BankClientPaymentOrderSearchForm;
use Application\Util\DataFilter;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use Application\Service\PaginatorOutputTrait;

/**
 * Class BankController
 * @package Application\Controller
 */
class BankClientPaymentOrderController extends AbstractRestfulController
{
    use PaginatorOutputTrait;

    /**
     * @var BankClientPaymentOrderManager
     */
    private $bankClientPaymentOrderManager;

    /**
     * @var array
     */
    private $config;

    /**
     * BankClientPaymentOrderController constructor.
     * @param BankClientPaymentOrderManager $bankClientPaymentOrderManager
     * @param array $config
     */
    public function __construct(BankClientPaymentOrderManager $bankClientPaymentOrderManager,
                                array $config)
    {
        $this->bankClientPaymentOrderManager = $bankClientPaymentOrderManager;
        $this->config = $config;
    }

    /**
     * @return TwigViewModel
     */
    public function troubleAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        // Data
        $data = $this->collectIncomingData();

        $paramsFilterAndSort = $this->getPaginationParams($this->config);

        try {
            /** @var BankClientPaymentOrderSearchForm $bankClientPaymentOrderSearchForm */
            $bankClientPaymentOrderSearchForm = new BankClientPaymentOrderSearchForm();

            if (isset($data['filter'])) {
                /** @var DataFilter $dataFilter */
                $dataFilter = new DataFilter();

                $data['filter']['csrf'] = $data['csrf'];

                // Set action
                $bankClientPaymentOrderSearchForm->setAttribute(
                    'action',
                    $this->url()->fromRoute('bank-client-payment-orders-trouble')
                );

                $filterResult = $dataFilter->filterData($bankClientPaymentOrderSearchForm, $data['filter']);

                if ($filterResult['filter']) {
                    $paramsFilterAndSort['filter'] = $filterResult['filter'];
                } else {
                    $bankClientPaymentOrderSearchForm = $filterResult['form'];
                }
            }
            /** @var array $bankClientPaymentOrdersOutput */
            $bankClientPaymentOrdersOutput = $this->bankClientPaymentOrderManager
                ->getTroubleBankClientPaymentOrder($paramsFilterAndSort);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        $paginatorOutput = null;
        if ($bankClientPaymentOrdersOutput['paginator']) {
            $paginatorOutput = $this->getPaginatorOutput($bankClientPaymentOrdersOutput['paginator']);
        }

        // SUCCESS Response with CSRF
        // Ajax - csrf token
        if($isAjax){
            $csrf   = $bankClientPaymentOrderSearchForm->get('csrf')->getValue();
            $data   = [
                'csrf'      => $csrf,
                'payments'  => $bankClientPaymentOrdersOutput['trouble_bank_client_payment_orders'],
                'paginator' => $paginatorOutput
            ];

            return $this->message()->success("CSRF token return", $data);
        }

        // Html -
        $view = new TwigViewModel([
            'paymentOrderSearchForm' => $bankClientPaymentOrderSearchForm,
            'payments'          => $bankClientPaymentOrdersOutput['trouble_bank_client_payment_orders'],
            'paginator'         => $paginatorOutput,
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }
}