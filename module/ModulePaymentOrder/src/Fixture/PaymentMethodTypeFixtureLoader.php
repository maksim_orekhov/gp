<?php

namespace ModulePaymentOrder\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use ModulePaymentOrder\Entity\PaymentMethodType;

class PaymentMethodTypeFixtureLoader implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $types = [
            PaymentMethodType::BANK_TRANSFER => "Банковский перевод",
            PaymentMethodType::ACQUIRING_MANDARIN => "Эквайринг Мандарин",
            PaymentMethodType::EMONEY => "Электронный кошелек",
        ];

        foreach ($types as $key=>$output_name) {
            $paymentMethodType = new PaymentMethodType();
            $paymentMethodType->setName($key);
            $paymentMethodType->setOutputName($output_name);
            $paymentMethodType->setDescription($output_name);
            $paymentMethodType->setIsActive(1);
            $entityManager->persist($paymentMethodType);
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 50;
    }
}