<?php

namespace ModulePaymentOrder\Fixture;

use Application\Entity\BankClientPaymentOrderFile;
use ModuleFileManager\Entity\File;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\SystemPaymentDetails;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Controller\BankClientController;
use Application\Service\Parser\BankClientParser;
use Application\Service\BankClient\BankClientManager;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Entity\PaymentOrder;
use Doctrine\ORM\EntityManager;
use ModulePaymentOrder\Service\PaymentOrderManager;

class BankClientPaymentOrderFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \DateTime
     */
    private $currentDate;

    public function getOrder()
    {
        return 130;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function load(ObjectManager $manager)
    {
//        $this->entityManager = $this->container->get('doctrine.entitymanager.orm_default');
//        $this->currentDate = new \DateTime();
//
//        // Проблемная сделка
//
//        /** @var Deal $deal */
//        $deal = $this->entityManager->getRepository(Deal::class)
//            ->findOneBy(array('name' => 'Сделка Проблемная'));
//
//        /** @var Payment $payment */
//        $payment = $this->entityManager->getRepository(Payment::class)
//            ->findOneBy(array('deal' => $deal));
//
//        /** @var PaymentOrder $paymentOrder */
//        $paymentOrder = $this->createPaymentOrder();
//
//        $payment->addPaymentOrder($paymentOrder);
//
//        $documentDate = clone $this->currentDate;
//        $documentDate->modify('-8 days');
//
//        // Создание $bankClientPaymentOrder
//        $bankClientPaymentOrder = new BankClientPaymentOrder();
//        $bankClientPaymentOrder->setCreated($this->currentDate);
//        $bankClientPaymentOrder->setType(PaymentOrderManager::TYPE_INCOMING);
//        $bankClientPaymentOrder->setPaymentOrder($paymentOrder);
//        $bankClientPaymentOrder->setDocumentType('Платежное поручение');
//        $bankClientPaymentOrder->setDocumentNumber(4);
//        $bankClientPaymentOrder->setDocumentDate(date_format($documentDate, 'd.m.Y'));
//        $bankClientPaymentOrder->setAmount(103000); // ошибочный
//        $bankClientPaymentOrder->setRecipientAccount('40702810590030001259');
//        $bankClientPaymentOrder->setRecipient('ИНН 7813271867 ООО "Гарант Пэй"');
//        $bankClientPaymentOrder->setRecipientInn('7813271867');
//        $bankClientPaymentOrder->setRecipient1('ООО "Гарант Пэй"');
//        $bankClientPaymentOrder->setRecipientKpp('781301001');
//        $bankClientPaymentOrder->setRecipientBank1('ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" Г. САНКТ-ПЕТЕРБУРГ');
//        $bankClientPaymentOrder->setRecipientBank1('ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" Г. САНКТ-ПЕТЕРБУРГ');
//        $bankClientPaymentOrder->setRecipientBik('044030790');
//        $bankClientPaymentOrder->setRecipientCorrAccount('30101810900000000790');
//        $bankClientPaymentOrder->setPayerAccount('40702810900000027373');
//        $bankClientPaymentOrder->setPayer('ИНН 4217030520 Test');
//        $bankClientPaymentOrder->setPayerInn('4217030520');
//        $bankClientPaymentOrder->setPayer1('Test');
//        #$bankClientPaymentOrder->setPayerKpp('325701001');
//        $bankClientPaymentOrder->setPayerKpp(null);
//        $bankClientPaymentOrder->setPayerBank1('АО "РАЙФФАЙЗЕНБАНК"');
//        $bankClientPaymentOrder->setPayerBik('044525700');
//        $bankClientPaymentOrder->setPayerCorrAccount('30101810200000000700');
//        $bankClientPaymentOrder->setPayerCorrAccount('30101810200000000700');
//        $bankClientPaymentOrder->setPaymentPurpose('Оплата по сделке #'.$deal->getId());
//        $bankClientPaymentOrder->setPaymentType('01');
//        $bankClientPaymentOrder->setPriority('5');
//        $bankClientPaymentOrder->setDateOfReceipt(date_format($documentDate, 'd.m.Y'));
//        $bankClientPaymentOrder->setDateOfDebit(date_format($documentDate, 'd.m.Y'));
//        $bankClientPaymentOrder->setHash('0faeda34e4c8d8c463d2e4644ba020a4');
//        /** @var BankClientPaymentOrderFile $bankClientPaymentOrderFile */
//        $bankClientPaymentOrderFile = $this->creteBankClientPaymentOrderFile($deal);
//        $bankClientPaymentOrder->setBankClientPaymentOrderFile($bankClientPaymentOrderFile);
//
//        $this->entityManager->persist($bankClientPaymentOrder);
//        $this->entityManager->flush();
//        // Crete PaymentOrder file
//        #$this->cretePaymentOrderFile($payment->getDeal());
    }

    /**
     * @return PaymentOrder
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createPaymentOrder()
    {
        /** @var PaymentMethodType $paymentMethodType */
        $paymentMethodType = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(array('name' => PaymentMethodType::BANK_TRANSFER));

        $paymentOrderCreateDate = clone $this->currentDate;

        // Создаём PaymentOrder
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = new PaymentOrder();
        $paymentOrder->setCreated($paymentOrderCreateDate->modify('-18 days'));
        $paymentOrder->setPaymentMethodType($paymentMethodType);

        $this->entityManager->persist($paymentOrder);
        $this->entityManager->flush($paymentOrder);

        return $paymentOrder;
    }

    /**
     * @param Deal $deal
     * @return BankClientPaymentOrderFile
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function creteBankClientPaymentOrderFile(Deal $deal)
    {


        $file = new File();
        $file->setCreated($this->currentDate);
        $file->setName('payment_order_for_deal_'.$deal->getId());
        $file->setType('txt');
        $file->setOriginName('payment_order_for_deal_'.$deal->getId());
        $file->setPath('/bank-client');
        $file->setSize(123456);

        $this->entityManager->persist($file);
        $this->entityManager->flush($file);

        $bankClientPaymentOrderFile = new BankClientPaymentOrderFile();
        $bankClientPaymentOrderFile->setFile($file);
        $bankClientPaymentOrderFile->setFileName($file->getName());
        $bankClientPaymentOrderFile->setHash(microtime());

        $this->entityManager->persist($bankClientPaymentOrderFile);
        $this->entityManager->flush($bankClientPaymentOrderFile);

        return $bankClientPaymentOrderFile;
    }
    /**
     * Создание файла платёжки по имеющейся сделке "Сделка Фикстура".
     * Метод в этой фикстуре потому, что Payment по сделке создается в последнюю очередь после всех необходимых объектов.
     * Объект Deal появляется только к моменту создания Payment.
     *
     * @param Deal $deal
     */
//    private function cretePaymentOrderFile(Deal $deal)
//    {
//        /** @var Payment $payment */
//        $payment = $deal->getPayment();
//        /** @var PaymentMethodBankTransfer $customerBankTransfer */ // Реквизиты customer'а
//        $customerBankTransfer = $this->entityManager->getRepository(PaymentMethodBankTransfer::class)
//            ->findOneBy(array('paymentRecipientName' => 'Тест Тестов'));
//        /** @var SystemPaymentDetails $systemPaymentDetails */ // Реквизиты GP
//        $systemPaymentDetails = $this->entityManager->getRepository(SystemPaymentDetails::class)
//            ->findOneBy(array('name' => 'ООО "Гарант Пэй"'));
//
//        // Full path to file
//        $path = "./module/Application/test/files/payment_order_from_fixture.txt";
//        // Creates file if not exists
//        $file_with_payment_order = fopen($path, "w") or die("Не удается создать файл");
//        // Write file header data
//        fwrite($file_with_payment_order, mb_convert_encoding(
//                BankClientManager::FILE_HEADER,
//                BankClientController::ORIGINAL_ENCODING, 'UTF-8')
//        );
//        // Main body of PaymentOrder
//        $currentDate = new \DateTime();
//        $txt = "СекцияДокумент=".BankClientManager::DOCUMENT_TYPE.PHP_EOL;
//        $txt .= "Номер=".$deal->getId().PHP_EOL;
//        $txt .= "Дата=".$currentDate->format('d.m.Y').PHP_EOL;
//        $txt .= "ДатаСписано=".$currentDate->format('d.m.Y').PHP_EOL;
//        $txt .= "Сумма=".$payment->getExpectedValue().PHP_EOL;
//        $txt .= "ПлательщикСчет=".$customerBankTransfer->getAccountNumber().PHP_EOL;
//        $txt .= "Плательщик=ИНН ".$customerBankTransfer->getInn()." ".$customerBankTransfer->getPaymentRecipientName().PHP_EOL;
//        $txt .= "ПлательщикИНН=".$customerBankTransfer->getInn().PHP_EOL;
//        $txt .= "Плательщик1=".$customerBankTransfer->getPaymentRecipientName().PHP_EOL;
//        $txt .= "ПлательщикКПП=".PHP_EOL;
//        $txt .= "ПлательщикБанк1=".$customerBankTransfer->getBankName().PHP_EOL;
//        $txt .= "ПлательщикБИК=".$customerBankTransfer->getBik().PHP_EOL;
//        $txt .= "ПлательщикКорсчет=".$customerBankTransfer->getCorrAccountNumber().PHP_EOL;
//        $txt .= "ПолучательСчет=".$systemPaymentDetails->getAccountNumber().PHP_EOL;
//        $txt .= "Получатель=ИНН ".$systemPaymentDetails->getInn()." ".$systemPaymentDetails->getPaymentRecipientName().PHP_EOL;
//        $txt .= "ПолучательИНН=".$systemPaymentDetails->getInn().PHP_EOL;
//        $txt .= "Получатель1=".$systemPaymentDetails->getPaymentRecipientName().PHP_EOL;
//        $txt .= "ПолучательКПП=".$systemPaymentDetails->getKpp().PHP_EOL;
//        $txt .= "ПолучательБанк1=".$systemPaymentDetails->getBank().PHP_EOL;
//        $txt .= "ПолучательБИК=".$systemPaymentDetails->getBik().PHP_EOL;
//        $txt .= "ПолучательКорсчет=".$systemPaymentDetails->getCorrAccountNumber().PHP_EOL;
//        $txt .= "НазначениеПлатежа=".BankClientManager::INCOMING_PAYMENT_PURPOSE_ENTRY.$deal->getId().PHP_EOL;
//        $txt .= "ВидОплаты=01".PHP_EOL;
//        $txt .= "Очередность=5".PHP_EOL;
//        // Write main body
//        fwrite($file_with_payment_order, mb_convert_encoding($txt, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
//        // Write SECTION_END_POINT
//        fwrite($file_with_payment_order, mb_convert_encoding(BankClientParser::SECTION_END_POINT.PHP_EOL,
//            BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
//
//        // Дубликат платёжки в другом файле для тестирования повторной выгрузки
//        $path_for_repeat = "./module/Application/test/files/payment_order_from_fixture_repeat.txt";
//        // Creates file if not exists
//        $file_with_payment_order_repeat = fopen($path_for_repeat, "w") or die("Не удается создать файл");
//        // Write file header data
//        fwrite($file_with_payment_order_repeat, mb_convert_encoding(
//                BankClientManager::FILE_HEADER,
//                BankClientController::ORIGINAL_ENCODING, 'UTF-8')
//        );
//        // Write main body
//        fwrite($file_with_payment_order_repeat, mb_convert_encoding($txt, BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
//        // Write SECTION_END_POINT
//        fwrite($file_with_payment_order_repeat, mb_convert_encoding(BankClientParser::SECTION_END_POINT.PHP_EOL,
//            BankClientController::ORIGINAL_ENCODING, 'UTF-8'));
//    }
}