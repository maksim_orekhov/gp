<?php
namespace ModulePaymentOrder\Service;

use Application\Entity\CivilLawSubject;
use Application\Entity\Deal;
use Application\Entity\Discount;
use Application\Entity\Dispute;
use Application\Entity\LegalEntity;
use Application\Entity\NaturalPerson;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\PaymentMethodBankTransfer;
use Application\Entity\Interfaces\PaymentOrderOwnerInterface;
use Application\Entity\Refund;
use Application\Entity\RepaidOverpay;
use Application\Entity\SoleProprietor;
use Application\Entity\SystemPaymentDetails;
use Application\Service\BankClient\BankClientManager;
use Application\Service\NdsTypeManager;
use Application\Service\SettingManager;
use Doctrine\ORM\EntityManager;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Entity\PaymentOrder;
use Zend\Paginator\Paginator;

/**
 * Class BankClientPaymentOrderManager
 * @package ModulePaymentOrder\Service
 */
class BankClientPaymentOrderManager
{
    const TROUBLE_NO_FILE_BINDING = 'no file binding';
    const TROUBLE_NOT_INCLUDED_IN_ANY_COLLECTION = 'not included in any collection';

    const BANK_CLIENT_PAYMENT_ORDER_TROUBLE_TYPES = [
        self::TROUBLE_NO_FILE_BINDING => 'Нет привязки к файлу',
        self::TROUBLE_NOT_INCLUDED_IN_ANY_COLLECTION => 'Не вошла ни в одну коллекцию',
    ];

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * @var SettingManager
     */
    private $settingManager;

    /**
     * @var BankClientPaymentOrderHandler
     */
    private $bankClientPaymentOrderHandler;

    /**
     * BankClientPaymentOrderManager constructor.
     * @param EntityManager $entityManager
     * @param NdsTypeManager $ndsTypeManager
     * @param SettingManager $settingManager
     */
    public function __construct(EntityManager $entityManager,
                                NdsTypeManager $ndsTypeManager,
                                SettingManager $settingManager,
                                BankClientPaymentOrderHandler $bankClientPaymentOrderHandler)
    {
        $this->entityManager    = $entityManager;
        $this->ndsTypeManager   = $ndsTypeManager;
        $this->settingManager   = $settingManager;
        $this->bankClientPaymentOrderHandler = $bankClientPaymentOrderHandler;
    }

    /**
     * @return array
     */
    public static function getBankClientPaymentOrderTroubleTypes(): array
    {
        return self::BANK_CLIENT_PAYMENT_ORDER_TROUBLE_TYPES;
    }

    /**
     * @param Deal $deal
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @param string $payment_purpose_type
     * @param $amount
     * @return PaymentOrder
     * @throws \Exception
     */
    public function makePayOff(Deal $deal, PaymentOrderOwnerInterface $paymentOrderOwner,
                               string $payment_purpose_type, $amount): PaymentOrder
    {
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $paymentOrderOwner->getPaymentMethod();

        try {
            $payment_purpose = $this->generatePayOffPaymentPurpose($deal, $payment_purpose_type, $amount);

            /** @var BankClientPaymentOrder $bankClientPaymentOrder */ // Can throw Exception
            $bankClientPaymentOrder = $this->createBankClientPaymentOrder($deal, $paymentMethod, $payment_purpose, $amount);

            // Создаем PaymentOrder
            $paymentOrder = new PaymentOrder();
            $paymentOrder->setPaymentMethodType($paymentMethod->getPaymentMethodType());
            $paymentOrder->setBankClientPaymentOrder($bankClientPaymentOrder);
            $paymentOrder->setCreated(new \DateTime());
            $this->entityManager->persist($paymentOrder);

            $bankClientPaymentOrder->setPaymentOrder($paymentOrder);
            $paymentOrderOwner->addPaymentOrder($paymentOrder);
            $this->entityManager->persist($bankClientPaymentOrder);
            $this->entityManager->persist($paymentOrderOwner);

            $this->entityManager->flush();

            if (!$paymentOrder->getId()) {

                throw new \Exception('Can not create PaymentOrder');
            }
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return $paymentOrder;
    }

    /**
     * @param Deal $deal
     * @param string $payment_purpose_type
     * @param $amount
     * @return string
     * @throws \Exception
     *
     * Не private, т.к. используется в фикстурах
     */
    public function generatePayOffPaymentPurpose(Deal $deal, string $payment_purpose_type, $amount): string
    {
        $payment_purpose = PayOffManager::PURPOSE_TYPE_TRANSLATIONS[$payment_purpose_type];
        $payment_purpose .= ' ' . PayOffManager::PURPOSE_SUBJECT . ' ' . $deal->getNumber();
        $payment_purpose .= '. ' . $this->getNdsTypeDeclaration($deal, $payment_purpose_type, $amount) . '.';

        return $payment_purpose;
    }

    /**
     * @param Deal $deal
     * @param string $payment_purpose_type
     * @param $amount
     * @return mixed|null|string
     * @throws \Exception
     */
    private function getNdsTypeDeclaration(Deal $deal, string $payment_purpose_type, $amount)
    {
        $nds_declaration = null;

        switch ($payment_purpose_type) {

            case PayOffManager::PURPOSE_TYPE_PAYOFF: // Выплата
                // В том числе НДС процент - сумма
                $nds_declaration = $this->ndsTypeManager->getPayOffNdsTypeDeclaration($deal, $amount);
                break;

            case PayOffManager::PURPOSE_TYPE_REFUND: // Возврат
                // НДС не облагается
                $nds_declaration = $this->ndsTypeManager->getRefundNdsTypeDeclaration($deal, $amount);
                break;

            case PayOffManager::PURPOSE_TYPE_DISCOUNT: // Скидка
                // НДС не облагается
                $nds_declaration = $this->ndsTypeManager->getDiscountNdsTypeDeclaration($deal, $amount);
                break;

            case PayOffManager::PURPOSE_TYPE_OVERPAY: // Возврат переплаты
                // НДС не облагается
                $nds_declaration = $this->ndsTypeManager->getOverpayNdsTypeDeclaration($deal, $amount);
                break;
        }

        return $nds_declaration;
    }

    /**
     * @param Deal $deal
     * @param PaymentMethod $paymentMethod
     * @param string $payment_purpose
     * @param $payment_amount
     * @return BankClientPaymentOrder
     * @throws \Exception
     */
    private function createBankClientPaymentOrder(Deal $deal, PaymentMethod $paymentMethod,
                                                  string $payment_purpose, $payment_amount)
    {
        /** @var PaymentMethodBankTransfer $paymentMethodBankTransfer */
        $paymentMethodBankTransfer = $paymentMethod->getPaymentMethodBankTransfer();
        /** @var CivilLawSubject $civilLawSubject */
        $civilLawSubject = $paymentMethod->getCivilLawSubject();
        /** @var LegalEntity $legalEntity */
        $legalEntity = $civilLawSubject->getLegalEntity();
        /** @var NaturalPerson $naturalPerson */
        $naturalPerson = $civilLawSubject->getNaturalPerson();
        /** @var SoleProprietor $soleProprietor */
        $soleProprietor = $civilLawSubject->getSoleProprietor();

        if ($legalEntity) {
            $recipientInn = $legalEntity->getInn();
            $recipientKpp = $legalEntity->getKpp();
            $recipient1 = $legalEntity->getName();
            $recipient = 'ИНН ' . $recipientInn . ' ' . $recipient1;
        } elseif ($soleProprietor) {
            $recipientInn = $soleProprietor->getInn();
            $recipientKpp = '';
            $recipient1 = $soleProprietor->getName();
            $recipient = 'ИНН ' . $recipientInn . ' ' . $recipient1;
        } else {
            $recipientInn = '';
            $recipientKpp = '';
            $recipient1 = $naturalPerson->getLastName() . ' ' . $naturalPerson->getFirstName() . ' '
                . $naturalPerson->getSecondaryName();
            $recipient = $recipient1;
        }

        /** @var SystemPaymentDetails $systemPaymentDetail */
        $systemPaymentDetail = $this->settingManager->getSystemPaymentDetail();

        $currentDate = new \DateTime();
        // Create new BankClientPaymentOrder
        $bankClientPaymentOrder = new BankClientPaymentOrder();
        // Set created date
        $bankClientPaymentOrder->setCreated($currentDate);
        // Fill it with required data
        $bankClientPaymentOrder->setType(PaymentOrderManager::TYPE_OUTGOING);
        $bankClientPaymentOrder->setAmount($payment_amount);
        $bankClientPaymentOrder->setDocumentNumber($deal->getId());
        $bankClientPaymentOrder->setDocumentType(BankClientManager::DOCUMENT_TYPE);
        $bankClientPaymentOrder->setDocumentDate($currentDate->format('d.m.Y'));
        // Payer data
        $bankClientPaymentOrder->setPayer('ИНН ' . $systemPaymentDetail->getInn() . ' '
            . $systemPaymentDetail->getPaymentRecipientName());
        $bankClientPaymentOrder->setPayer1($systemPaymentDetail->getPaymentRecipientName());
        $bankClientPaymentOrder->setPayerAccount($systemPaymentDetail->getAccountNumber());
        $bankClientPaymentOrder->setPayerBank1($systemPaymentDetail->getBank());
        $bankClientPaymentOrder->setPayerBik($systemPaymentDetail->getBik());
        $bankClientPaymentOrder->setPayerCorrAccount($systemPaymentDetail->getCorrAccountNumber());
        $bankClientPaymentOrder->setPayerInn($systemPaymentDetail->getInn());
        $bankClientPaymentOrder->setPayerKpp($systemPaymentDetail->getKpp());
        // Recipient data
        $bankClientPaymentOrder->setRecipient($recipient);
        $bankClientPaymentOrder->setRecipient1($recipient1);
        $bankClientPaymentOrder->setRecipientAccount($paymentMethodBankTransfer->getAccountNumber());
        $bankClientPaymentOrder->setRecipientBank1($paymentMethodBankTransfer->getBankName());
        $bankClientPaymentOrder->setRecipientBik($paymentMethodBankTransfer->getBik());
        $bankClientPaymentOrder->setRecipientCorrAccount($paymentMethodBankTransfer->getCorrAccountNumber());
        $bankClientPaymentOrder->setRecipientInn($recipientInn);
        $bankClientPaymentOrder->setRecipientKpp($recipientKpp);

        $bankClientPaymentOrder->setPaymentType('01');
        $bankClientPaymentOrder->setPriority(5);
        $bankClientPaymentOrder->setPaymentPurpose($payment_purpose);

        // Get PaymentOrder data as array (используем для валидации и для получения хэша платежки)
        $payment_order_array = BankClientPaymentOrderHandler::getBankClientPaymentOrderAsArray($bankClientPaymentOrder);

        $bankClientPaymentOrder->setHash($this->bankClientPaymentOrderHandler->getHashForPaymentOrder($payment_order_array));

        try {
            $this->bankClientPaymentOrderHandler->validationBankClientPaymentOrderData($payment_order_array);
        }
        catch (\Throwable $t) {

            throw new \Exception('Validation error on BankClientPaymentOrder creation: ' . $t->getMessage());
        }

        // Check if BankClientPaymentOrder with such hash already exists
        if (true === $this->bankClientPaymentOrderHandler
                ->checkPaymentOrderWithProvidedHashExists($bankClientPaymentOrder->getHash())) {

            throw new \Exception('Such BankClientPaymentOrder is already exists in the system');
        }

        // Save BankClientPaymentOrder
        $this->entityManager->persist($bankClientPaymentOrder);
        $this->entityManager->flush($bankClientPaymentOrder);

        if (!$bankClientPaymentOrder->getId()) {

            throw new \Exception('Can not create BankClientPaymentOrder');
        }

        $bankClientPaymentOrder->setDocumentNumber($bankClientPaymentOrder->getId());

        $this->entityManager->persist($bankClientPaymentOrder);
        $this->entityManager->flush($bankClientPaymentOrder);

        return $bankClientPaymentOrder;
    }

    /**
     * Для outgoing есть подтверждающия платежка (incoming)
     *
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @return bool
     * @throws \Exception
     */
    public function isPayOff(BankClientPaymentOrder $bankClientPaymentOrder): bool
    {
        $deal_id = static::getDealNumberFromPaymentPurpose($bankClientPaymentOrder);

        if ($deal_id) {
            /** @var Deal $deal */
            $deal = $this->entityManager->getRepository(Deal::class)
                ->findOneBy(['id' => $deal_id]);
            /** @var PaymentOrder $paymentOrder */
            $paymentOrder = $bankClientPaymentOrder->getPaymentOrder();

            try {
                // Can throw Exception
                $payment_order_purpose_type = $this->getOutgoingPurposeType($deal, $paymentOrder);
            }
            catch (\Throwable $t) {

                throw new \Exception($t->getMessage());
            }

            /** @var PaymentOrderOwnerInterface $paymentOrderOwner */
            $paymentOrderOwner = $this->getPaymentOrderOwnerForBankClientPaymentOrder($deal, $bankClientPaymentOrder);

            /** Если найденный $bankClientPaymentOrder не находится в нужной коллекции и если в коллекции меньше двух элементов */
            if (!$paymentOrderOwner->getPaymentOrders()->contains($paymentOrder)
                && $paymentOrderOwner->getPaymentOrders()->count() < 2) {

                return false;
            }

            /** @var BankClientPaymentOrder $confirmationPaymentOrder */
            $confirmationPaymentOrder = $this->entityManager
                ->getRepository(BankClientPaymentOrder::class)
                ->findConfirmationBankClientPaymentOrder(
                    $bankClientPaymentOrder,
                    $paymentOrderOwner,
                    $deal->getNumber(),
                    $payment_order_purpose_type
                );

            if ($confirmationPaymentOrder) {

                return true;
            }
        }

        return false;
    }

    /**
     * @param $params
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function getTroubleBankClientPaymentOrder($params): array
    {
        /** @var Paginator $troubleBankClientPaymentOrders */
        $troubleBankClientPaymentOrders = $this->entityManager
            ->getRepository(BankClientPaymentOrder::class)
            ->getTroubleBankClientPaymentOrders($params);

        $troubleBankClientPaymentOrdersOutput =
            $this->extractBankClientPaymentOrderCollection($troubleBankClientPaymentOrders, true);

        return [
            'trouble_bank_client_payment_orders' => $troubleBankClientPaymentOrdersOutput,
            'paginator' => $troubleBankClientPaymentOrders->getPages()
        ];
    }

    /**
     * @param Deal $deal
     * @param PaymentOrder $paymentOrder
     * @return string
     * @throws \Exception
     */
    private function getOutgoingPurposeType(Deal $deal, PaymentOrder $paymentOrder)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();

        if ($payment->getPaymentOrders()->contains($paymentOrder)) {

            return PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_PAYOFF];
        }

        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        /** @var Discount $discount */
        $discount = $dispute ? $dispute->getDiscount() : null;

        if ($discount && $discount->getPaymentOrders()->contains($paymentOrder)) {

            return PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_DISCOUNT];
        }

        /** @var Refund $refund */
        $refund = $dispute ? $dispute->getRefund() : null;

        if ($refund && $refund->getPaymentOrders()->contains($paymentOrder)) {

            return PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_REFUND];
        }

        /** @var RepaidOverpay $repaidOverpay */
        $repaidOverpay = $payment->getRepaidOverpay();

        if ($repaidOverpay && $repaidOverpay->getPaymentOrders()->contains($paymentOrder)) {

            return PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY];
        }

        throw new \Exception('No PaymentOrder owner found');
    }

    /**
     * @param Deal $deal
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @return PaymentOrderOwnerInterface|null
     */
    public function getPaymentOrderOwnerForBankClientPaymentOrder(Deal $deal, BankClientPaymentOrder $bankClientPaymentOrder)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var string $payment_purpose */
        $payment_purpose = $bankClientPaymentOrder->getPaymentPurpose();
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();

        // Если Выплата
        if (strpos($payment_purpose, PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_PAYOFF]) !== false) {

            return $payment;
        }
        // Если Скидка
        if (strpos($payment_purpose, PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_DISCOUNT]) !== false) {

            if (null !== $dispute && null !== $dispute->getDiscount()) {

                return $dispute->getDiscount();
            }
        }
        // Если Возврат
        if (strpos($payment_purpose, PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_REFUND]) !== false) {

            if (null !== $dispute && null !== $dispute->getRefund()) {

                return $dispute->getRefund();
            }
        }
        // Если Возврат переплаты
        if (strpos($payment_purpose, PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_OVERPAY]) !== false) {

            if (null !== $payment->getRepaidOverpay()) {

                return $payment->getRepaidOverpay();
            }
        }

        return null;
    }

    /**
     * @param $bankClientPaymentOrders
     * @return array
     */
    public function extractBankClientPaymentOrderCollection($bankClientPaymentOrders, $with_trouble_reason = false)
    {
        $bankClientPaymentOrdersOutput = [];

        foreach ($bankClientPaymentOrders as $bankClientPaymentOrder) {

            if (!$with_trouble_reason) {
                $bankClientPaymentOrdersOutput[] = $this->getBankClientPaymentOrderOutput($bankClientPaymentOrder);
            } else {
                $bankClientPaymentOrdersOutput[] = $this->getTroubleBankClientPaymentOrderOutput($bankClientPaymentOrder);
            }

        }

        return $bankClientPaymentOrdersOutput;
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @return array
     */
    public function getTroubleBankClientPaymentOrderOutput(BankClientPaymentOrder $bankClientPaymentOrder): array
    {
        $bankClientPaymentOrderOutput = $this->getBankClientPaymentOrderOutput($bankClientPaymentOrder);

        $bankClientPaymentOrderOutput['trouble_reasons'] = $this->getBankClientPaymentOrderTroubleReason($bankClientPaymentOrder);

        return $bankClientPaymentOrderOutput;
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @return array
     */
    private function getBankClientPaymentOrderTroubleReason(BankClientPaymentOrder $bankClientPaymentOrder): array
    {
        $trouble_reasons = [];

        if ($bankClientPaymentOrder->getType() === PaymentOrderManager::TYPE_INCOMING
            && null === $bankClientPaymentOrder->getBankClientPaymentOrderFile()) {

            $trouble_reasons[] = self::BANK_CLIENT_PAYMENT_ORDER_TROUBLE_TYPES[self::TROUBLE_NO_FILE_BINDING];
        }

        if (null === $bankClientPaymentOrder->getPaymentOrder()) {

            $trouble_reasons[] = self::BANK_CLIENT_PAYMENT_ORDER_TROUBLE_TYPES[self::TROUBLE_NOT_INCLUDED_IN_ANY_COLLECTION];
        }

        return $trouble_reasons;
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @return array
     * @TODO Переделать на static?
     */
    public function getBankClientPaymentOrderOutput(BankClientPaymentOrder $bankClientPaymentOrder): array
    {
        $bankClientPaymentOrderOutput = [];

        $bankClientPaymentOrderOutput['id'] = $bankClientPaymentOrder->getId();
        $bankClientPaymentOrderOutput['payment_method_type'] = PaymentMethodType::BANK_TRANSFER;
        $bankClientPaymentOrderOutput['created'] = $bankClientPaymentOrder->getCreated()->format('d.m.Y');
        $bankClientPaymentOrderOutput['document_type'] = $bankClientPaymentOrder->getDocumentType();
        $bankClientPaymentOrderOutput['document_number'] = $bankClientPaymentOrder->getDocumentNumber();
        $bankClientPaymentOrderOutput['document_date'] = $bankClientPaymentOrder->getDocumentDate();
        $bankClientPaymentOrderOutput['amount'] = $bankClientPaymentOrder->getAmount();
        $bankClientPaymentOrderOutput['recipient_account'] = $bankClientPaymentOrder->getRecipientAccount();
        $bankClientPaymentOrderOutput['recipient'] = $bankClientPaymentOrder->getRecipient();
        $bankClientPaymentOrderOutput['recipient_inn'] = $bankClientPaymentOrder->getRecipientInn();
        $bankClientPaymentOrderOutput['recipient_1'] = $bankClientPaymentOrder->getRecipient1();
        $bankClientPaymentOrderOutput['recipient_kpp'] = $bankClientPaymentOrder->getRecipientKpp();
        $bankClientPaymentOrderOutput['recipient_bank_1'] = $bankClientPaymentOrder->getRecipientBank1();
        $bankClientPaymentOrderOutput['recipient_bik'] = $bankClientPaymentOrder->getRecipientBik();
        $bankClientPaymentOrderOutput['recipient_corr_account'] = $bankClientPaymentOrder->getRecipientCorrAccount();
        $bankClientPaymentOrderOutput['payer_account'] = $bankClientPaymentOrder->getPayerAccount();
        $bankClientPaymentOrderOutput['payer'] = $bankClientPaymentOrder->getPayer();
        $bankClientPaymentOrderOutput['payer_inn'] = $bankClientPaymentOrder->getPayerInn();
        $bankClientPaymentOrderOutput['payer_1'] = $bankClientPaymentOrder->getPayer1();
        $bankClientPaymentOrderOutput['payer_kpp'] = $bankClientPaymentOrder->getPayerKpp();
        $bankClientPaymentOrderOutput['payer_bank_1'] = $bankClientPaymentOrder->getPayerBank1();
        $bankClientPaymentOrderOutput['payer_bik'] = $bankClientPaymentOrder->getPayerBik();
        $bankClientPaymentOrderOutput['payer_corr_account'] = $bankClientPaymentOrder->getPayerCorrAccount();
        $bankClientPaymentOrderOutput['payment_purpose'] = $bankClientPaymentOrder->getPaymentPurpose();
        $bankClientPaymentOrderOutput['payment_type'] = $bankClientPaymentOrder->getPaymentType();
        $bankClientPaymentOrderOutput['priority'] = $bankClientPaymentOrder->getPriority();
        $bankClientPaymentOrderOutput['date_of_receipt'] = $bankClientPaymentOrder->getDateOfReceipt();
        $bankClientPaymentOrderOutput['date_of_debit'] = $bankClientPaymentOrder->getDateOfDebit();

        return $bankClientPaymentOrderOutput;
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @return int|null
     */
    public static function getDealNumberFromPaymentPurpose(BankClientPaymentOrder $bankClientPaymentOrder)
    {
        preg_match_all(BankClientManager::DEAL_ID_CREDIT_PAYMENT_PURPOSE_ENTRY_STRONG,
            $bankClientPaymentOrder->getPaymentPurpose(), $matches);

        return isset($matches[1][0]) ? $matches[1][0] : null;
    }
}