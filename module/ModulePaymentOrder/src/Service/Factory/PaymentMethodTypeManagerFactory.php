<?php
namespace ModulePaymentOrder\Service\Factory;

use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Service\PaymentMethodTypeManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class PaymentMethodTypeManagerFactory
 * @package ModulePaymentOrder\Service\Factory
 */
class PaymentMethodTypeManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PaymentMethodTypeManager
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new PaymentMethodTypeManager($entityManager);
    }
}