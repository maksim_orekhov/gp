<?php
namespace ModulePaymentOrder\Service\Factory;

use Application\Service\Payment\CalculatedDataProvider;
use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Application\Service\SettingManager;

/**
 * Class PaymentOrderManagerFactory
 * @package Application\Service\Factory
 */
class PaymentOrderManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PaymentOrderManager
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $bankClientPaymentOrderManager = $container->get(BankClientPaymentOrderManager::class);
        $settingManager = $container->get(SettingManager::class);
        $calculatedDataProvider = $container->get(CalculatedDataProvider::class);

        return new PaymentOrderManager(
            $entityManager,
            $bankClientPaymentOrderManager,
            $settingManager,
            $calculatedDataProvider
        );
    }
}