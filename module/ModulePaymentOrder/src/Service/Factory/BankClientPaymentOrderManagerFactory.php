<?php
namespace ModulePaymentOrder\Service\Factory;

use Application\Service\NdsTypeManager;
use Application\Service\SettingManager;
use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Service\BankClientPaymentOrderHandler;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;

/**
 * Class PaymentOrderManagerFactory
 * @package Application\Service\Factory
 */
class BankClientPaymentOrderManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BankClientPaymentOrderManager
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $ndsTypeManager = $container->get(NdsTypeManager::class);
        $settingManager = $container->get(SettingManager::class);
        $bankClientPaymentOrderHandler = $container->get(BankClientPaymentOrderHandler::class);

        return new BankClientPaymentOrderManager(
            $entityManager,
            $ndsTypeManager,
            $settingManager,
            $bankClientPaymentOrderHandler
        );
    }
}