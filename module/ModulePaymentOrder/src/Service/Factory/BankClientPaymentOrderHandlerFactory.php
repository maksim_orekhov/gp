<?php
namespace ModulePaymentOrder\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModulePaymentOrder\Service\BankClientPaymentOrderHandler;

/**
 * Class BankClientPaymentOrderHandlerFactory
 * @package ModulePaymentOrder\Service\Factory
 */
class BankClientPaymentOrderHandlerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BankClientPaymentOrderHandler
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new BankClientPaymentOrderHandler($entityManager);
    }
}