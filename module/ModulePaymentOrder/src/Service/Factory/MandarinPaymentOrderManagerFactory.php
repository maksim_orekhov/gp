<?php
namespace ModulePaymentOrder\Service\Factory;

use Application\Provider\Mail\PaymentReceiptSender;
use Application\Service\Payment\PaymentManager;
use Core\Service\TwigRenderer;
use Interop\Container\ContainerInterface;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentMethodTypeManager;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Adapter\TokenAdapter;

/**
 * Class MandarinPaymentOrderManagerFactory
 * @package ModulePaymentOrder\Service\Factory
 */
class MandarinPaymentOrderManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return MandarinPaymentOrderManager
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $paymentOrderManager = $container->get(PaymentOrderManager::class);
        $paymentMethodTypeManager = $container->get(PaymentMethodTypeManager::class);
        $tokenAdapter = $container->get(TokenAdapter::class);
        $paymentManager = $container->get(PaymentManager::class);
        $bankClientPaymentOrderManager = $container->get(BankClientPaymentOrderManager::class);
        $config = $container->get('config');
        $twigRenderer = $container->get(TwigRenderer::class);
        $paymentReceiptSender = new PaymentReceiptSender($twigRenderer, $config);
        $deliveryEventProvider = $container->get(DeliveryEventProvider::class);

        return new MandarinPaymentOrderManager(
            $entityManager,
            $paymentOrderManager,
            $paymentMethodTypeManager,
            $tokenAdapter,
            $paymentManager,
            $bankClientPaymentOrderManager,
            $paymentReceiptSender,
            $deliveryEventProvider->getEventManager(),
            $config
        );
    }
}