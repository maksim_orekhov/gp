<?php
namespace ModulePaymentOrder\Service\Factory;

use Application\Service\Payment\CalculatedDataProvider;
use Interop\Container\ContainerInterface;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class PayOffManagerFactory
 * @package ModulePaymentOrder\Service\Factory
 */
class PayOffManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return PayOffManager
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $paymentOrderManager = $container->get(PaymentOrderManager::class);
        $bankClientPaymentOrderManager = $container->get(BankClientPaymentOrderManager::class);
        $mandarinPaymentOrderManager = $container->get(MandarinPaymentOrderManager::class);
        $calculatedDataProvider = $container->get(CalculatedDataProvider::class);

        return new PayOffManager(
            $paymentOrderManager,
            $bankClientPaymentOrderManager,
            $mandarinPaymentOrderManager,
            $calculatedDataProvider
        );
    }
}