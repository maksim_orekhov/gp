<?php

namespace ModulePaymentOrder\Service;

use ModulePaymentOrder\Entity\PaymentMethodType;
use Doctrine\ORM\EntityManager;

class PaymentMethodTypeManager
{
    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * PaymentMethodManager constructor.
     * @param $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @param $id
     * @return PaymentMethodType|null
     */
    public function findType($id)
    {
        return $this->entityManager->getRepository(PaymentMethodType::class)->find($id);
    }

    /**
     * @param string $payment_method_type
     * @return null|object
     */
    public function getPaymentMethodTypeByName(string $payment_method_type)
    {
        $paymentMethod = $this->entityManager->getRepository(PaymentMethodType::class)
            ->findOneBy(['name' => $payment_method_type]);

        return $paymentMethod;
    }
}