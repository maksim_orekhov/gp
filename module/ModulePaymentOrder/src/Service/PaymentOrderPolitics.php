<?php

namespace ModulePaymentOrder\Service;

use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\PaymentTransactionInterface;

/**
 * Class PaymentOrderPolitics
 * @package ModulePaymentOrder\Service\Payment
 */
class PaymentOrderPolitics
{
    /**
     * @param PaymentTransactionInterface $outgoingPaymentTransaction
     * @return bool
     */
    public static function isOutgoingPaymentTransactionReadyToPay(PaymentTransactionInterface $outgoingPaymentTransaction): bool
    {
        // Если экземпляр BankClientPaymentOrder и платёжка не попала в файл на выплату
        if ($outgoingPaymentTransaction instanceof BankClientPaymentOrder
            && !$outgoingPaymentTransaction->getBankClientPaymentOrderFile()) {

            return false;
        }

        // @TODO Проверка для других типов метода оплаты

        return true;
    }
}