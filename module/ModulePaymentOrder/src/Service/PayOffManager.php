<?php

namespace ModulePaymentOrder\Service;

use Application\Entity\Deal;
use Application\Entity\Discount;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\PaymentMethod;
use Application\Entity\Interfaces\PaymentOrderOwnerInterface;
use Application\Entity\Refund;
use Application\Entity\RepaidOverpay;
use Application\Service\Payment\CalculatedDataProvider;
use Core\Exception\LogicException;
use ModulePaymentOrder\Entity\PaymentMethodType;

class PayOffManager
{
    const PURPOSE_TYPE_REFUND = 'refund';
    const PURPOSE_TYPE_DISCOUNT = 'discount';
    const PURPOSE_TYPE_OVERPAY = 'overpay';
    const PURPOSE_TYPE_PAYOFF = 'payoff';

    const PURPOSE_TYPE_TRANSLATIONS = [
        self::PURPOSE_TYPE_REFUND   => 'Возврат',
        self::PURPOSE_TYPE_DISCOUNT => 'Частичный возврат',
        self::PURPOSE_TYPE_OVERPAY  => 'Переплата',
        self::PURPOSE_TYPE_PAYOFF   => 'Выплата',
    ];

    const PURPOSE_SUBJECT = 'по сделке';

    /**
     * @var BankClientPaymentOrderManager
     */
    private $bankClientPaymentOrderManager;
    /**
     * @var MandarinPaymentOrderManager
     */
    private $mandarinPaymentOrderManager;
    /**
     * @var PaymentOrderManager
     */
    private $paymentOrderManager;
    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider;

    /**
     * PayOffManager constructor.
     * @param PaymentOrderManager $paymentOrderManager
     * @param BankClientPaymentOrderManager $bankClientPaymentOrderManager
     * @param MandarinPaymentOrderManager $mandarinPaymentOrderManager
     * @param CalculatedDataProvider $calculatedDataProvider
     */
    public function __construct(PaymentOrderManager $paymentOrderManager,
                                BankClientPaymentOrderManager $bankClientPaymentOrderManager,
                                MandarinPaymentOrderManager $mandarinPaymentOrderManager,
                                CalculatedDataProvider $calculatedDataProvider)
    {

        $this->paymentOrderManager = $paymentOrderManager;
        $this->bankClientPaymentOrderManager = $bankClientPaymentOrderManager;
        $this->mandarinPaymentOrderManager = $mandarinPaymentOrderManager;
        $this->calculatedDataProvider = $calculatedDataProvider;
    }

    /**
     * @param Deal $deal
     * @param $purpose_type
     * @return null
     * @throws LogicException
     * @throws \Exception
     */
    public function createPayOff(Deal $deal, $purpose_type)
    {
        /**
         * @var PaymentOrderOwnerInterface $paymentOrderOwner
         */
        $payoff_settings = $this->getPayOffSettingsByPurposeType($deal, $purpose_type);
        $paymentOrderOwner = $payoff_settings['paymentOrderOwner'];
        $payment_method_type_name = $payoff_settings['payment_method_type_name'];
        $amount = $payoff_settings['amount'];

        ///// всегда проверяем баланс сделки /////
        $deal_balance = $this->paymentOrderManager->getDealBalanceByPayment($deal->getPayment());
        if ($deal_balance === 0 || $deal_balance < $amount) {

            throw new LogicException(null, LogicException::DEAL_BALANCE_DOES_NOT_ALLOW_PAYOFF);
        }

        ///// выбираем через что будем делаем выплату /////
        switch ($payment_method_type_name) {
            case PaymentMethodType::BANK_TRANSFER:
                return $this->bankClientPaymentOrderManager->makePayOff($deal, $paymentOrderOwner, $purpose_type, $amount);
            case PaymentMethodType::ACQUIRING_MANDARIN:
                return $this->mandarinPaymentOrderManager->makePayOff($deal, $paymentOrderOwner, $purpose_type, $amount);
            case PaymentMethodType::EMONEY:
                throw new LogicException(null, LogicException::PAYOFF_CREATE_METHOD_NOT_DEFINED);
            default:
                throw new LogicException(null, LogicException::PAYOFF_CREATE_METHOD_NOT_DEFINED);
        }
    }

    /**
     * @param Deal $deal
     * @param string $purpose_type
     * @return mixed
     * @throws LogicException
     */
    public function getPayOffSettingsByPurposeType(Deal $deal, string $purpose_type)
    {
        switch ($purpose_type)
        {
            case self::PURPOSE_TYPE_REFUND:
                return $this->getRefundSettings($deal);

            case self::PURPOSE_TYPE_DISCOUNT:
                return $this->getDiscountSettings($deal);

            case self::PURPOSE_TYPE_OVERPAY:
                return $this->getRepaidOverpaySettings($deal);

            case self::PURPOSE_TYPE_PAYOFF:
                return $this->getPayOffSettings($deal);

            default:
                throw new LogicException(null, LogicException::PAYOFF_TYPE_NOT_DEFINED);
        }
    }

    /**
     * @param Deal $deal
     * @return array
     * @throws LogicException
     */
    public function getRefundSettings(Deal $deal)
    {
        /**
         * @var Dispute $dispute
         * @var Refund $refund
         * @var PaymentMethodType $paymentMethodType
         * @var PaymentMethod $paymentMethod
         */

        $dispute = $deal->getDispute();
        $refund = $dispute ? $dispute->getRefund() : null;
        $paymentMethod = $refund ? $refund->getPaymentMethod() : null;
        $paymentMethodType = $paymentMethod ? $paymentMethod->getPaymentMethodType() : null;

        return $this->buildSettingsResultToArray($refund, $paymentMethodType, $this->getRefundAmount($deal));
    }

    /**
     * @param Deal $deal
     * @return array
     * @throws LogicException
     */
    public function getDiscountSettings(Deal $deal)
    {
        /**
         * @var Dispute $dispute
         * @var Discount $discount
         * @var PaymentMethodType $paymentMethodType
         * @var PaymentMethod $paymentMethod
         */

        $dispute = $deal->getDispute();
        $discount = $dispute ? $dispute->getDiscount() : null;
        $paymentMethod = $discount ? $discount->getPaymentMethod() : null;
        $paymentMethodType = $paymentMethod ? $paymentMethod->getPaymentMethodType() : null;

        return $this->buildSettingsResultToArray($discount, $paymentMethodType, $this->getDiscountAmount($deal));
    }

    /**
     * @param Deal $deal
     * @return array
     * @throws LogicException
     */
    public function getRepaidOverpaySettings(Deal $deal)
    {
        /**
         * @var Payment $payment
         * @var RepaidOverpay $repaidOverpay
         * @var PaymentMethodType $paymentMethodType
         * @var PaymentMethod $paymentMethod
         */

        $payment = $deal->getPayment();
        $repaidOverpay = $payment->getRepaidOverpay();
        $paymentMethod = $repaidOverpay ? $repaidOverpay->getPaymentMethod() : null;
        $paymentMethodType = $paymentMethod ? $paymentMethod->getPaymentMethodType() : null;

        return $this->buildSettingsResultToArray($repaidOverpay, $paymentMethodType, $this->getOverpayAmount($deal));
    }

    /**
     * @param Deal $deal
     * @return array
     * @throws LogicException
     */
    public function getPayOffSettings(Deal $deal)
    {
        /**
         * @var Payment $payment
         * @var PaymentMethodType $paymentMethodType
         * @var PaymentMethod $paymentMethod
         */

        $payment = $deal->getPayment();
        $paymentMethod = $payment ? $payment->getPaymentMethod() : null;
        $paymentMethodType = $paymentMethod ? $paymentMethod->getPaymentMethodType() : null;

        return $this->buildSettingsResultToArray($payment, $paymentMethodType, $this->getPayOffAmount($deal));
    }

    /**
     * @param Deal $deal
     * @return mixed
     * @throws LogicException
     */
    public function getRefundAmount(Deal $deal)
    {
        try {
            /** @var Payment $payment */
            $payment = $deal->getPayment();
            /** @var Dispute $dispute */
            $dispute = $deal->getDispute();
            $refund = $dispute ? $dispute->getRefund() : null;
            $refundAmount = 0;
            if ( $refund ) {
                $total_incoming_amount = $this->paymentOrderManager->getTotalIncomingAmountByPayment($payment, true);
                $refundAmount = $this->calculatedDataProvider->getCalculatedRefundAmount($payment, $total_incoming_amount);
            }
        }
        catch (\Throwable $t){

            throw new LogicException(null, LogicException::PAYOFF_CALCULATED_AMOUNT_ERROR);
        }

        return $refundAmount;
    }

    /**
     * @param Deal $deal
     * @return int
     */
    public function getDiscountAmount(Deal $deal)
    {
        /** @var Dispute $dispute */
        $dispute = $deal->getDispute();
        $discount = $dispute ? $dispute->getDiscount() : null;

        return $discount ? $discount->getAmount() : 0;
    }

    /**
     * @param Deal $deal
     * @return float|int
     */
    public function getOverpayAmount(Deal $deal)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        /** @var RepaidOverpay $repaidOverpay */
        $repaidOverpay = $payment->getRepaidOverpay();

        return $repaidOverpay ? $repaidOverpay->getAmount() : 0;
    }

    /**
     * @param Deal $deal
     * @return float|int
     */
    public function getPayOffAmount(Deal $deal)
    {
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $discount_amount = $this->getDiscountAmount($deal);
        $payoff_amount = $payment->getReleasedValue();

        if ($discount_amount > 0) {
            $payoff_amount -= $discount_amount;
        }

        return CalculatedDataProvider::roundAmount($payoff_amount);
    }

    /**
     * @param PaymentOrderOwnerInterface|null $paymentOrderOwner
     * @param PaymentMethodType|null $paymentMethodType
     * @param int $amount
     * @return array
     * @throws LogicException
     */
    public function buildSettingsResultToArray(
        PaymentOrderOwnerInterface $paymentOrderOwner = null,
        PaymentMethodType $paymentMethodType = null,
        $amount = 0
    )
    {
        if (null === $paymentOrderOwner || null === $paymentOrderOwner->getPaymentMethod()) {

            throw new LogicException(null, LogicException::PAYOFF_PAYMENT_METHOD_NOT_DEFINED);
        }

        return [
            'paymentOrderOwner' => $paymentOrderOwner,
            'payment_method_type_name' => $paymentMethodType ? $paymentMethodType->getName() : null,
            'amount' => $amount
        ];
    }
}