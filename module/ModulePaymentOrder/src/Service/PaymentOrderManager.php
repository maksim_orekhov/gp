<?php
namespace ModulePaymentOrder\Service;

use Application\Entity\Deal;
use Application\Entity\Discount;
use Application\Entity\Dispute;
use Application\Entity\Payment;
use Application\Entity\Interfaces\PaymentOrderOwnerInterface;
use Application\Entity\Refund;
use Application\Entity\RepaidOverpay;
use Application\Entity\SystemPaymentDetails;
use Application\Service\BankClient\BankClientManager;
use Application\Service\Payment\CalculatedDataProvider;
use Application\Service\SettingManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use ModuleAcquiringMandarin\Controller\CallbackHandleController;
use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Entity\PaymentTransactionInterface;

/**
 * Class PaymentOrderManager
 * @package ModulePaymentOrder\Service
 */
class PaymentOrderManager
{
    const TYPE_INCOMING = 'incoming';
    const TYPE_OUTGOING = 'outgoing';

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var BankClientPaymentOrderManager
     */
    private $bankClientPaymentOrderManager;

    /**
     * @var SettingManager
     */
    private $settingManager;
    /**
     * @var CalculatedDataProvider
     */
    private $calculatedDataProvider;

    /**
     * PaymentOrderManager constructor.
     * @param EntityManager $entityManager
     * @param BankClientPaymentOrderManager $bankClientPaymentOrderManager
     * @param SettingManager $settingManager
     * @param CalculatedDataProvider $calculatedDataProvider
     */
    public function __construct(EntityManager $entityManager,
                                BankClientPaymentOrderManager $bankClientPaymentOrderManager,
                                SettingManager $settingManager,
                                CalculatedDataProvider $calculatedDataProvider)
    {
        $this->entityManager = $entityManager;
        $this->bankClientPaymentOrderManager = $bankClientPaymentOrderManager;
        $this->settingManager = $settingManager;
        $this->calculatedDataProvider = $calculatedDataProvider;
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return bool
     * @throws \Exception
     */
    public function isPayOff(PaymentOrder $paymentOrder): bool
    {
        try {
            // BANK_TRANSFER
            if ($this->checkPaymentOrderHasBankClientConfirmationTransaction($paymentOrder)) {

                return true;
            }

            //@TODO Проверка других типов методов оплаты
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        return false;
    }

    /**
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @param PaymentMethodType $paymentMethodType
     * @param PaymentTransactionInterface $paymentTransaction
     * @return PaymentOrder
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createPaymentOrderAndSetRelations(PaymentOrderOwnerInterface $paymentOrderOwner,
                                                      PaymentMethodType $paymentMethodType,
                                                      PaymentTransactionInterface $paymentTransaction): PaymentOrder
    {
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = new PaymentOrder();
        $paymentOrder->setCreated(new \DateTime());
        $paymentOrder->setPaymentMethodType($paymentMethodType);

        // Add PaymentOrder to $transactionOwner->paymentOrders collection
        $paymentOrderOwner->addPaymentOrder($paymentOrder);
        // Set PaymentOrder id to $paymentTransaction
        $paymentTransaction->setPaymentOrder($paymentOrder);

        $this->entityManager->persist($paymentOrder);
        $this->entityManager->persist($paymentTransaction);

        $this->entityManager->flush();

        return $paymentOrder;
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return bool
     * @throws \Exception
     */
    private function checkPaymentOrderHasBankClientConfirmationTransaction(PaymentOrder $paymentOrder): bool
    {
        /** @var BankClientPaymentOrder $bankClientPaymentOrder */
        $bankClientPaymentOrder = $paymentOrder->getBankClientPaymentOrder();

        if ($bankClientPaymentOrder->getType() !== self::TYPE_OUTGOING) {

            throw new \Exception('Only outgoing BankClientPaymentOrder can be confirmed');
        }

        return $paymentOrder->getPaymentMethodType()->getName() === PaymentMethodType::BANK_TRANSFER
            && $this->bankClientPaymentOrderManager->isPayOff($paymentOrder->getBankClientPaymentOrder());
    }

    /**
     * Возвращает INCOMING (полученные в результате парсинга) платёжки от Кастомера (без подтверждающих)
     *
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @return array
     *
     * @TODO Переделать на использование метода getIncomingPaymentTransactions по аналогии с getOutgoingPaymentTransactions
     */
    public function getIncomingPaymentTransactions(PaymentOrderOwnerInterface $paymentOrderOwner) :array
    {
        $this->entityManager->refresh($paymentOrderOwner);

        $paymentTransactions = [];

        /** @var PaymentOrder $paymentOrder */
        foreach ($paymentOrderOwner->getPaymentOrders() as $paymentOrder) {

            if ($this->checkPaymentOrderHasBankClientPaymentOrderOfSpecificType($paymentOrder, self::TYPE_INCOMING)
                && $this->isRecipientGP($paymentOrder->getBankClientPaymentOrder())) {
                // Get BankClientPaymentOrder
                $paymentTransactions[] = $paymentOrder->getBankClientPaymentOrder();
            }

            // @TODO Тут еще проверять, что получатель GP, чтобы исключить подтверждающую платёжкуку. Пока непонятно как.
            if ($this->checkPaymentOrderHasMandarinPaymentOrderOfSpecificType($paymentOrder, self::TYPE_INCOMING)
                && $this->isMandarinPaymentOrderHasSuccessStatus($paymentOrder->getMandarinPaymentOrder())) {
                // Get MandarinPaymentOrder
                $paymentTransactions[] = $paymentOrder->getMandarinPaymentOrder();
            }
        }

        //@TODO Выборка других типов методов оплаты

        return $paymentTransactions;
    }

    /**
     * Возвращает INCOMING (полученные в результате парсинга) платёжки от Кастомера (без подтверждающих)
     *
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @return array
     *
     * @TODO Переделать на использование метода getIncomingPaymentTransactions по аналогии с getOutgoingPaymentTransactions
     */
    public function getIncomingPaymentAllTransactions(PaymentOrderOwnerInterface $paymentOrderOwner) :array
    {
        $this->entityManager->refresh($paymentOrderOwner);

        $paymentTransactions = [];

        /** @var PaymentOrder $paymentOrder */
        foreach ($paymentOrderOwner->getPaymentOrders() as $paymentOrder) {

            if ($this->checkPaymentOrderHasBankClientPaymentOrderOfSpecificType($paymentOrder, self::TYPE_INCOMING)
                && $this->isRecipientGP($paymentOrder->getBankClientPaymentOrder())) {
                // Get BankClientPaymentOrder
                $paymentTransactions[] = $paymentOrder->getBankClientPaymentOrder();
            }

            // @TODO Тут еще проверять, что получатель GP, чтобы исключить подтверждающую платёжкуку. Пока непонятно как.
            if ($this->checkPaymentOrderHasMandarinPaymentOrderOfSpecificType($paymentOrder, self::TYPE_INCOMING)) {
                // Get MandarinPaymentOrder
                $paymentTransactions[] = $paymentOrder->getMandarinPaymentOrder();
            }
        }

        //@TODO Выборка других типов методов оплаты

        return $paymentTransactions;
    }

    /**
     * @param MandarinPaymentOrder $mandarinPaymentOrder
     * @return bool
     */
    private function isMandarinPaymentOrderHasSuccessStatus(MandarinPaymentOrder $mandarinPaymentOrder): bool
    {
        return MandarinPaymentOrderManager::STATUS_SUCCESS === $mandarinPaymentOrder->getStatus();
    }


    /**
     * Возвращает все outgoing paymentTransactions
     *
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @return array
     */
    public function getOutgoingPaymentTransactions(PaymentOrderOwnerInterface $paymentOrderOwner) :array
    {
        return $this->getAllPaymentTransactionsByType($paymentOrderOwner, self::TYPE_OUTGOING);
    }

    /**
     * Возвращает все paymentTransactions определнного типа (incoming или outgoing)
     * В incoming попадают и подтверждающие
     *
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @param string $type
     * @return array|null
     */
    public function getAllPaymentTransactionsByType(PaymentOrderOwnerInterface $paymentOrderOwner, string $type) :array
    {
        $this->entityManager->refresh($paymentOrderOwner);

        $paymentTransactions = [];

        /** @var PaymentOrder $paymentOrder */
        foreach ($paymentOrderOwner->getPaymentOrders() as $paymentOrder) {

            if ($this->checkPaymentOrderHasBankClientPaymentOrderOfSpecificType($paymentOrder, $type)) {
                // Get BankClientPaymentOrders
                $paymentTransactions[] = $paymentOrder->getBankClientPaymentOrder();
            }
            // @TODO Тут еще проврять, что получатель GP, чтобы исключить подтверждающую платёжкуку. Пока непонятно как.
            if ($this->checkPaymentOrderHasMandarinPaymentOrderOfSpecificType($paymentOrder, $type)
                && $this->isMandarinPaymentOrderHasSuccessStatus($paymentOrder->getMandarinPaymentOrder())) {
                // Get MandarinPaymentOrder
                $paymentTransactions[] = $paymentOrder->getMandarinPaymentOrder();
            }
        }

        //@TODO Выборка других типов методов оплаты

        return $paymentTransactions;
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @param string $type
     * @return bool
     */
    private function checkPaymentOrderHasBankClientPaymentOrderOfSpecificType(PaymentOrder $paymentOrder,
                                                                              string $type): bool
    {
        return $paymentOrder->getPaymentMethodType()->getName() === PaymentMethodType::BANK_TRANSFER
            && $paymentOrder->getBankClientPaymentOrder()->getType() === $type;
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @param string $type
     * @return bool
     */
    public function checkPaymentOrderHasMandarinPaymentOrderOfSpecificType(PaymentOrder $paymentOrder,
                                                                              string $type): bool
    {
        return $paymentOrder->getPaymentMethodType()->getName() === PaymentMethodType::ACQUIRING_MANDARIN
            && $paymentOrder->getMandarinPaymentOrder()->getType() === $type;
    }

    /**
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @return array
     */
    public function getArrayOfPaymentOrdersIds(PaymentOrderOwnerInterface $paymentOrderOwner): array
    {
        $payment_orders_ids = [];

        $payment_orders_ids[PaymentMethodType::BANK_TRANSFER] = [];
        // @TODO Создавать подмассивы для всех типов
        #$payment_orders_ids[PaymentMethodType::CREDIT_CARD] = [];
        #$payment_orders_ids[PaymentMethodType::EMONEY] = [];

        /** @var PaymentOrder $paymentOrder */
        foreach ($paymentOrderOwner->getPaymentOrders() as $paymentOrder) {

            $payment_orders_ids[$paymentOrder->getPaymentMethodType()->getName()][] = $paymentOrder->getId();
        }

        return $payment_orders_ids;
    }

    /**
     * @param Payment $payment
     * @param bool $with_overpay
     * @return float|int
     * @throws \Exception
     */
    public function getTotalIncomingAmountByPayment(Payment $payment, $with_overpay = false)
    {
        try {
            // Can throw Exception
            $incomingPaymentTransactions = $this->getIncomingPaymentTransactions($payment);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        $total_amount = 0;

        /** @var PaymentTransactionInterface $paymentTransaction */
        foreach ($incomingPaymentTransactions as $paymentTransaction) {
            // Проверка на то, что платёжка входящая и получатель GP
            if ($this->isRecipientGP($paymentTransaction)) {

                $total_amount += $paymentTransaction->getAmount();
            }
        }

        if ($with_overpay && $payment->getRepaidOverpay()) {
            $total_amount -= $payment->getRepaidOverpay()->getAmount();
        }

        return $total_amount;
    }

    /**
     * @param Payment $payment
     * @return float|int
     * @throws \Exception
     */
    public function getTotalOverpaidAmountByPayment(Payment $payment)
    {
        $total_overpaid_amount = 0;
        $total_incoming_amount = $this->getTotalIncomingAmountByPayment($payment, false);

        if ($payment->getExpectedValue() < $total_incoming_amount) {
            $total_overpaid_amount = $total_incoming_amount - $payment->getExpectedValue();
        }

        return $total_overpaid_amount;
    }

    /**
     * Возвращает OUTGOING (сгенерированную нами) платёжку на ВЫПЛАТУ Контрактору
     *
     * @param Payment $payment
     * @return PaymentTransactionInterface|null
     * @throws \Exception
     */
    public function getPaymentOutgoingPaymentTransaction(Payment $payment)
    {
        try {
            // Can throw Exception
            return $this->getOutgoingPaymentTransactionForPaymentOrderOwner($payment);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage().' (Payment)');
        }
    }

    /**
     * Возвращает OUTGOING (сгенерированную нами) платёжку на ВОЗВРАТ Кастомеру
     *
     * @param Refund $refund
     * @return PaymentTransactionInterface|null
     * @throws \Exception
     */
    public function getRefundOutgoingPaymentTransaction(Refund $refund)
    {
        try {
            // Can throw Exception
            return $this->getOutgoingPaymentTransactionForPaymentOrderOwner($refund);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage().' (Refund)');
        }
    }

    /**
     * Возвращает OUTGOING (сгенерированную нами) платёжку на СКИДКУ Кастомеру
     *
     * @param Discount $discount
     * @return PaymentTransactionInterface|null
     * @throws \Exception
     */
    public function getDiscountOutgoingPaymentTransaction(Discount $discount)
    {
        try {
            // Can throw Exception
            return $this->getOutgoingPaymentTransactionForPaymentOrderOwner($discount);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage().' (Discount)');
        }
    }

    /**
     * Возвращает OUTGOING (сгенерированную нами) платёжку на ВОЗВРАТ ПЕРЕПЛАТЫ Кастомеру
     *
     * @param RepaidOverpay $repaidOverpay
     * @return PaymentTransactionInterface|null
     * @throws \Exception
     */
    public function getRepaidOverpayOutgoingPaymentTransaction(RepaidOverpay $repaidOverpay)
    {
        try {
            // Can throw Exception
            return $this->getOutgoingPaymentTransactionForPaymentOrderOwner($repaidOverpay);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage().' (RepaidOverpay)');
        }
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return PaymentTransactionInterface
     * @throws \Exception
     */
    private function getPaymentTransactionByPaymentOrderAndType(PaymentOrder $paymentOrder): PaymentTransactionInterface
    {
        if ($paymentOrder->getPaymentMethodType()->getName() === PaymentMethodType::BANK_TRANSFER) {
            // Если выплата через BANK_TRANSFER
            return $paymentOrder->getBankClientPaymentOrder();
        }

        if ($paymentOrder->getPaymentMethodType()->getName() === PaymentMethodType::ACQUIRING_MANDARIN) {
            // Если выплата через ACQUIRING_MANDARIN
            return $paymentOrder->getMandarinPaymentOrder();
        }

        // @TODO Другие методы оплаты

        if ($paymentOrder->getPaymentMethodType()->getName() === PaymentMethodType::EMONEY) {

            // Если выплата через EMONEY
        }

        throw new \Exception('PaymentTransaction to PaymentOrder not fount');
    }

    /**
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @return PaymentTransactionInterface|null
     * @throws \Exception
     */
    private function getOutgoingPaymentTransactionForPaymentOrderOwner(PaymentOrderOwnerInterface $paymentOrderOwner)
    {
        foreach ($paymentOrderOwner->getPaymentOrders() as $paymentOrder) {
            try {
                /** @var PaymentTransactionInterface $paymentTransaction */ // Can throw Exception
                $paymentTransaction = $this->getPaymentTransactionByPaymentOrderAndType($paymentOrder);
            }
            catch (\Throwable $t) {

                throw new \Exception($t->getMessage());
            }

            if ($paymentTransaction->getType() === self::TYPE_OUTGOING) {

                return $paymentTransaction;
            }
        }

        return null;
    }

    /**
     * @param Payment $payment
     * @return BankClientPaymentOrder|null
     * @throws \Exception
     */
    public function getPaymentConfirmationPaymentOrder(Payment $payment)
    {
        try {
            /** @var PaymentOrder $outgoingPaymentTransaction */ // Can throw Exception
            $outgoingPaymentTransaction = $this->getOutgoingPaymentTransactionForPaymentOrderOwner($payment);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        // BANK_TRANSFER
        if ($outgoingPaymentTransaction instanceof BankClientPaymentOrder) {

            $deal_number = BankClientPaymentOrderManager::getDealNumberFromPaymentPurpose($outgoingPaymentTransaction);

            if ($deal_number) {
                /** @var BankClientPaymentOrder $confirmationPaymentTransaction */
                $confirmationPaymentTransaction = $this->entityManager
                    ->getRepository(BankClientPaymentOrder::class)
                    ->findConfirmationBankClientPaymentOrder(
                        $outgoingPaymentTransaction,
                        $payment,
                        $deal_number,
                        PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_PAYOFF]
                    );

                return $confirmationPaymentTransaction;
            }
        }

        // @TODO Другие типы методов оплаты

        return null;
    }

    /**
     * @param Discount $discount
     * @return BankClientPaymentOrder|null
     * @throws \Exception
     */
    public function getDiscountConfirmationPaymentOrder(Discount $discount)
    {
        try {
            /** @var PaymentOrder $discountTransaction */ // Can throw Exception
            $discountTransaction = $this->getOutgoingPaymentTransactionForPaymentOrderOwner($discount);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        // BANK_TRANSFER
        if ($discountTransaction instanceof BankClientPaymentOrder) {

            $deal_number = BankClientPaymentOrderManager::getDealNumberFromPaymentPurpose($discountTransaction);

            if ($deal_number) {
                /** @var BankClientPaymentOrder $confirmationDiscountTransaction */
                $confirmationDiscountTransaction = $this->entityManager
                    ->getRepository(BankClientPaymentOrder::class)
                    ->findConfirmationBankClientPaymentOrder(
                        $discountTransaction,
                        $discount,
                        $deal_number,
                        PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_DISCOUNT]
                    );

                return $confirmationDiscountTransaction;
            }
        }

        // @TODO Другие типы методов оплаты

        return null;
    }

    /**
     * @param Refund $refund
     * @return BankClientPaymentOrder|null
     * @throws \Exception
     */
    public function getRefundConfirmationPaymentOrder(Refund $refund)
    {
        try {
            /** @var PaymentOrder $refundTransaction */ // Can throw Exception
            $refundTransaction = $this->getOutgoingPaymentTransactionForPaymentOrderOwner($refund);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        // BANK_TRANSFER
        if ($refundTransaction instanceof BankClientPaymentOrder) {

            $deal_number = BankClientPaymentOrderManager::getDealNumberFromPaymentPurpose($refundTransaction);

            if ($deal_number) {
                /** @var BankClientPaymentOrder $confirmationRefundTransaction */
                $confirmationRefundTransaction = $this->entityManager
                    ->getRepository(BankClientPaymentOrder::class)
                    ->findConfirmationBankClientPaymentOrder(
                        $refundTransaction,
                        $refund,
                        $deal_number,
                        PayOffManager::PURPOSE_TYPE_TRANSLATIONS[PayOffManager::PURPOSE_TYPE_REFUND]
                    );

                return $confirmationRefundTransaction;
            }
        }

        // @TODO Другие типы методов оплаты

        return null;
    }

    /**
     * @return array|null
     */
    public function getOutgoingBankClientPaymentOrderReadyToPayOff()
    {
        $outgoingReadyBankClientPaymentOrders = $this->entityManager
            ->getRepository(BankClientPaymentOrder::class)
            ->findBy(['type' => self::TYPE_OUTGOING, 'bankClientPaymentOrderFile' => null]);

        return $outgoingReadyBankClientPaymentOrders;
    }

    /**
     * @param PaymentTransactionInterface $paymentTransaction
     * @return bool
     */
    public function isRecipientGP(PaymentTransactionInterface $paymentTransaction): bool
    {
        if ($paymentTransaction instanceof BankClientPaymentOrder
            && $paymentTransaction->getType() === self::TYPE_INCOMING
            && $this->isBankClientPaymentOrderRecipientGP($paymentTransaction)
        ) {

            return true;
        }

        // @TODO Тут еще проверять, что получатель GP, чтобы исключить подтверждающую платёжкуку. Пока по Action.
        if ($paymentTransaction instanceof MandarinPaymentOrder
            && $paymentTransaction->getType() === self::TYPE_INCOMING
            && $paymentTransaction->getAction() === CallbackHandleController::ACTION_FOR_PAYMENT
        ) {
            return true;
        }

        //@TODO Другие типы PaymentTransaction

        return false;
    }

    public function isPayerGP(PaymentTransactionInterface $paymentTransaction): bool
    {
        if ($paymentTransaction instanceof BankClientPaymentOrder
            && $paymentTransaction->getType() === self::TYPE_OUTGOING
            && $this->isBankClientPaymentOrderPayerGP($paymentTransaction)) {

            return true;
        }

        //@TODO Другие типы PaymentTransaction

        return false;
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @return bool
     */
    public function isBankClientPaymentOrderRecipientGP(BankClientPaymentOrder $bankClientPaymentOrder): bool
    {
        /** @var SystemPaymentDetails $paymentDetails */
        $paymentDetails = $this->settingManager->getGuarantPayBankTransferDetails();

        if ($paymentDetails->getAccountNumber() === $bankClientPaymentOrder->getRecipientAccount()
            && $paymentDetails->getInn() === $bankClientPaymentOrder->getRecipientInn()) {

            return true;
        }

        return false;
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @return bool
     */
    public function isBankClientPaymentOrderPayerGP(BankClientPaymentOrder $bankClientPaymentOrder): bool
    {
        /** @var SystemPaymentDetails $paymentDetails */
        $paymentDetails = $this->settingManager->getGuarantPayBankTransferDetails();

        if ($paymentDetails->getAccountNumber() === $bankClientPaymentOrder->getPayerAccount()
            && $paymentDetails->getCorrAccountNumber() === $bankClientPaymentOrder->getPayerCorrAccount()
            && $paymentDetails->getBik() === $bankClientPaymentOrder->getPayerBik()
            && $paymentDetails->getInn() === $bankClientPaymentOrder->getPayerInn()
            && $paymentDetails->getKpp() === $bankClientPaymentOrder->getPayerKpp()) {

            return true;
        }

        return false;
    }

    /**
     * @param array $paymentTransactions
     * @return array
     */
    public function getPaymentTransactionCollectionOutput(array $paymentTransactions) :array
    {
        $paymentTransactionOutput = [];

        foreach ($paymentTransactions as $paymentTransaction) {

            $paymentTransactionOutput[] = $this->bankClientPaymentOrderManager->getBankClientPaymentOrderOutput($paymentTransaction);
        }

        return $paymentTransactionOutput;
    }

    /**
     * @param Payment $payment
     * @return float|int
     * @throws \Exception
     */
    public function getDealBalanceByPayment(Payment $payment)
    {
        $total_incoming_amount = $this->getTotalIncomingAmountByPayment($payment);
        if ($total_incoming_amount === 0) {
            //делаем return что бы не уходить в минус
            return 0;
        }
        ///// вычитаем из общей суммы выплаты /////
        // 1. вычитаем комиссию сделки
        $total_incoming_amount -= $this->calculatedDataProvider->getCalculateFeeAmount($payment->getDealValue(), $payment->getFee());
        // 2. проверяем платежки на переплату
        $total_incoming_amount -= $this->getTotalOutgoingRepaidOverpayAmountByPayment($payment);
        // 3. проверяем платежки на скидки
        $total_incoming_amount -= $this->getTotalOutgoingDiscountAmountByPayment($payment);
        // 4. проверяем платежки на возврат
        $total_incoming_amount -= $this->getTotalOutgoingRefundAmountByPayment($payment);
        // 4. проверяем платежки на выплату
        $total_incoming_amount -= $this->getTotalOutgoingPaymentAmountByPayment($payment);

        return $total_incoming_amount;
    }

    /**
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @return int
     */
    public function getTotalAmountOutgoingPaymentTransactions(PaymentOrderOwnerInterface $paymentOrderOwner = null)
    {
        /**
         * @var BankClientPaymentOrder | MandarinPaymentOrder $outgoingTransaction
         * @var ArrayCollection $outgoingTransactions
         */
        $total_amount = 0;
        if (null === $paymentOrderOwner) {
            return $total_amount;
        }

        $outgoingTransactions = $this->getOutgoingPaymentTransactions($paymentOrderOwner);
        foreach ($outgoingTransactions as $outgoingTransaction) {
            $total_amount += $outgoingTransaction->getAmount();
        }

        return $total_amount;
    }

    /**
     * @param Payment $payment
     * @return float|int
     */
    public function getTotalOutgoingPaymentAmountByPayment(Payment $payment)
    {
        return $this->getTotalAmountOutgoingPaymentTransactions($payment);
    }

    /**
     * @param Payment $payment
     * @return float|int
     */
    public function getTotalOutgoingRepaidOverpayAmountByPayment(Payment $payment)
    {
        $repaidOverpay = $payment->getRepaidOverpay();

        return $this->getTotalAmountOutgoingPaymentTransactions($repaidOverpay);
    }

    /**
     * @param Payment $payment
     * @return float|int
     */
    public function getTotalOutgoingDiscountAmountByPayment(Payment $payment)
    {
        /**
         * @var Deal $deal
         * @var Dispute $dispute
         */
        $deal = $payment->getDeal();
        $dispute = $deal->getDispute();
        $discount = $dispute ? $dispute->getDiscount() : null;

        return $this->getTotalAmountOutgoingPaymentTransactions($discount);
    }

    /**
     * @param Payment $payment
     * @return float|int
     */
    public function getTotalOutgoingRefundAmountByPayment(Payment $payment)
    {
        /**
         * @var Deal $deal
         * @var Dispute $dispute
         */
        $deal = $payment->getDeal();
        $dispute = $deal->getDispute();
        $refund = $dispute ? $dispute->getRefund() : null;

        return $this->getTotalAmountOutgoingPaymentTransactions($refund);
    }
}