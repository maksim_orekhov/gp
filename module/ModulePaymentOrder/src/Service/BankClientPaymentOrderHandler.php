<?php
namespace ModulePaymentOrder\Service;

use ModulePaymentOrder\Entity\BankClientPaymentOrder;
use ModulePaymentOrder\Form\BankClientPaymentOrderForm;
use Doctrine\ORM\EntityManager;

/**
 * Class BankClientPaymentOrderHandler
 * @package ModulePaymentOrder\Service
 */
class BankClientPaymentOrderHandler
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * BankClientPaymentOrderHandler constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $transfer_order_array
     * @return string
     */
    public function getHashForPaymentOrder($transfer_order_array): string
    {
        return md5(serialize($transfer_order_array));
    }

    /**
     * @param array $payment_order_array
     * @throws \Exception
     */
    public function validationBankClientPaymentOrderData(array $payment_order_array)
    {
        // Create BankClientPaymentOrderForm for Validation purpose
        $form = new BankClientPaymentOrderForm();
        // Set data
        $form->setData($payment_order_array);
        // Validate
        if( !$form->isValid() ) {
            $message = null;
            foreach ($form->getMessages() as $key => $value) {
                $message .= $key . " (";
                foreach ($value as $m) {
                    $message .= $m . " / ";
                }
                $message .= ") ";
            }
            throw new \Exception($message);
        }
    }

    /**
     * @param $hash
     * @return bool
     */
    public function checkPaymentOrderWithProvidedHashExists($hash): bool
    {
        $bankClientPaymentOrder = $this->entityManager->getRepository(BankClientPaymentOrder::class)
            ->findOneBy(array('hash' => $hash));

        return !(null === $bankClientPaymentOrder);
    }

    /**
     * @param BankClientPaymentOrder $bankClientPaymentOrder
     * @return array
     */
    public static function getBankClientPaymentOrderAsArray(BankClientPaymentOrder $bankClientPaymentOrder): array
    {
        $array = [];

        $array['document_type'] = $bankClientPaymentOrder->getDocumentType();
        $array['document_number'] = $bankClientPaymentOrder->getDocumentNumber();
        $array['amount'] = $bankClientPaymentOrder->getAmount();
        $array['payer_account'] = $bankClientPaymentOrder->getPayerAccount();
        $array['payer'] = $bankClientPaymentOrder->getPayer();
        $array['payer_inn'] = $bankClientPaymentOrder->getPayerInn();
        $array['payer_1'] = $bankClientPaymentOrder->getPayer1();
        $array['payer_kpp'] = $bankClientPaymentOrder->getPayerKpp();
        $array['payer_bank_1'] = $bankClientPaymentOrder->getPayerBank1();
        $array['payer_bik'] = $bankClientPaymentOrder->getPayerBik();
        $array['payer_corr_account'] = $bankClientPaymentOrder->getPayerCorrAccount();
        $array['recipient_account'] = $bankClientPaymentOrder->getRecipientAccount();
        $array['recipient'] = $bankClientPaymentOrder->getRecipient();
        $array['recipient_inn'] = $bankClientPaymentOrder->getRecipientInn();
        $array['recipient_1'] = $bankClientPaymentOrder->getRecipient1();
        $array['recipient_kpp'] = $bankClientPaymentOrder->getRecipientKpp();
        $array['recipient_bank_1'] = $bankClientPaymentOrder->getRecipientBank1();
        $array['recipient_bik'] = $bankClientPaymentOrder->getRecipientBik();
        $array['recipient_corr_account'] = $bankClientPaymentOrder->getRecipientCorrAccount();
        $array['payment_purpose'] = $bankClientPaymentOrder->getPaymentPurpose();
        $array['payment_type'] = $bankClientPaymentOrder->getPaymentType();
        $array['priority'] = $bankClientPaymentOrder->getPriority();

        return $array;
    }
}