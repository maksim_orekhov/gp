<?php

namespace ModulePaymentOrder\Service;

use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\Interfaces\PaymentOrderOwnerInterface;
use Application\Provider\Mail\PaymentReceiptSender;
use Application\Service\Payment\PaymentManager;
use Core\Adapter\TokenAdapter;
use Doctrine\ORM\EntityManager;
use ModuleAcquiringMandarin\Controller\CallbackHandleController;
use ModuleCode\Service\SmsStrategyContext;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModulePaymentOrder\Entity\PaymentOrder;
use Zend\EventManager\EventManager;

/**
 * Class MandarinPaymentOrderManager
 * @package ModulePaymentOrder\Service
 */
class MandarinPaymentOrderManager
{
    use \Application\Service\PrepareConfigNotifyTrait;
    use \Application\Service\SmsNotifyTrait;

    private $email_notify_mandarin_payment_receipt_full;
    private $sms_notify_mandarin_payment_receipt_full;

    const STATUS_PROCESSING = 'processing';
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILED = 'failed';
    const STATUS_PAYOUT_ONLY = 'payout-only';

    const MAX_RECURSION_LEVEL = 10;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var PaymentOrderManager
     */
    private $paymentOrderManager;

    /**
     * @var PaymentMethodTypeManager
     */
    private $paymentMethodTypeManager;

    /**
     * @var TokenAdapter
     */
    private $tokenAdapter;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var BankClientPaymentOrderManager
     */
    private $bankClientPaymentOrderManager;

    /**
     * @var PaymentReceiptSender
     */
    private $paymentReceiptSender;

    /**
     * @var EventManager
     */
    private $deliveryEventManager;

    /**
     * @var array
     */
    private $config;


    /**
     * MandarinPaymentOrderManager constructor.
     * @param EntityManager $entityManager
     * @param PaymentOrderManager $paymentOrderManager
     * @param PaymentMethodTypeManager $paymentMethodTypeManager
     * @param TokenAdapter $tokenAdapter
     * @param PaymentManager $paymentManager
     * @param BankClientPaymentOrderManager $bankClientPaymentOrderManager
     * @param PaymentReceiptSender $paymentReceiptSender
     * @param array $config
     */
    function __construct(EntityManager $entityManager,
                         PaymentOrderManager $paymentOrderManager,
                         PaymentMethodTypeManager $paymentMethodTypeManager,
                         TokenAdapter $tokenAdapter,
                         PaymentManager $paymentManager,
                         BankClientPaymentOrderManager $bankClientPaymentOrderManager,
                         PaymentReceiptSender $paymentReceiptSender,
                         EventManager $eventManager,
                         array $config)
    {
        $this->entityManager            = $entityManager;
        $this->paymentOrderManager      = $paymentOrderManager;
        $this->paymentMethodTypeManager = $paymentMethodTypeManager;
        $this->tokenAdapter             = $tokenAdapter;
        $this->paymentManager           = $paymentManager;
        $this->bankClientPaymentOrderManager = $bankClientPaymentOrderManager;
        $this->paymentReceiptSender = $paymentReceiptSender;
        $this->deliveryEventManager = $eventManager;
        $this->config = $config;

        $this->sms_notify_mandarin_payment_receipt_full = $this->getDealNotifyConfig($this->config, 'mandarin_payment_receipt_full', 'sms');
        $this->email_notify_mandarin_payment_receipt_full = $this->getDealNotifyConfig($this->config, 'mandarin_payment_receipt_full', 'email');

    }

    /**
     * @param Deal $deal
     * @param PaymentOrderOwnerInterface $paymentOrderOwner
     * @param string $payment_purpose_type
     * @param $amount
     * @return PaymentOrder
     * @throws \Exception
     */
    public function makePayOff(Deal $deal, PaymentOrderOwnerInterface $paymentOrderOwner,
                               string $payment_purpose_type, $amount): PaymentOrder
    {
        return $this->bankClientPaymentOrderManager->makePayOff($deal, $paymentOrderOwner, $payment_purpose_type, $amount);
    }

    /**
     * @param Deal $deal
     * @param MandarinPaymentOrder $mandarinPaymentOrder
     * @param array $data
     * @return MandarinPaymentOrder
     * @throws \Exception
     */
    public function manageSuccessMandarinPaymentOrder(Deal $deal,
                                                      MandarinPaymentOrder $mandarinPaymentOrder,
                                                      array $data): MandarinPaymentOrder
    {
        $mandarinPaymentOrder->setOrderId($data['orderId']);
        $mandarinPaymentOrder->setAmount($data['price']);
        $mandarinPaymentOrder->setAction($data['action']);
        $mandarinPaymentOrder->setTransaction($data['transaction']);
        $mandarinPaymentOrder->setStatus($data['status']);
        $mandarinPaymentOrder->setCustomerEmail($data['customer_email']);
        $mandarinPaymentOrder->setCustomerPhone($data['customer_phone']);
        $mandarinPaymentOrder->setCardHolder($data['card_holder']);
        $mandarinPaymentOrder->setCardNumber($data['card_number']);
        $mandarinPaymentOrder->setCardExpirationYear($data['card_expiration_year']);
        $mandarinPaymentOrder->setCardExpirationMonth($data['card_expiration_month']);
        if (isset($data['error_code'])) {
            $mandarinPaymentOrder->setErrorCode($data['error_code']);
        }
        if (isset($data['error_description'])) {
            $mandarinPaymentOrder->setErrorDescription($data['error_description']);
        }

        $this->entityManager->persist($mandarinPaymentOrder);
        $this->entityManager->flush();

        // Записываем DeFactoDate и отправляем уведомления (открыть и доделать)
        /** @var Payment $payment */
        $payment = $deal->getPayment();
        $payment_status = $this->paymentManager->getPaymentDebitStatus($payment);

        if ($payment_status === 'matched' ||  $payment_status === 'overpayment') {
            // Записываем дату фактической оплаты
            $payment->setDeFactoDate(new \DateTime());
            // Сохранение банковских реквизитов customer
            #$this->paymentMethodManager->savingBankDetailsCustomerFromPaymentOrder($bankClientPaymentOrder, $payment);
            //// *** доставка *** ////
            /// @TODO доставка временно реализована без автоматики
            // Вызываем событие Создания заказа на доставку в выбранном сервисе доставки
            #$this->deliveryEventManager->trigger(
            #    DeliveryEventProvider::EVENT_CREATE_DELIVERY_SERVICE_ORDER, $this, [
            #    'create_delivery_service_order_params' => ['deliveryOrder' => $deal->getDeliveryOrder()]
            #]);

            $this->entityManager->flush($payment);
        }

        // Уведомления
        // Если сделка полностью оплачена Покупателем и нет переплаты
        if ($payment_status === 'matched') {
            //уведомить агентов сделки о получении оплаты в полном объеме
            $this->notifyDealAgentsAboutReceiptMandarinPayment($mandarinPaymentOrder, $payment);
        }

        return $mandarinPaymentOrder;
    }

    /**
     * @param MandarinPaymentOrder $mandarinPaymentOrder
     * @param Payment $payment
     */
    public function notifyDealAgentsAboutReceiptMandarinPayment(MandarinPaymentOrder $mandarinPaymentOrder, Payment $payment)
    {
        if($this->sms_notify_mandarin_payment_receipt_full){
            try {
                $this->notifyCustomerAboutReceiptPaymentFullBySms($mandarinPaymentOrder, $payment);
                $this->notifyContractorAboutReceiptPaymentFullBySms($mandarinPaymentOrder, $payment);
            } catch (\Throwable $t) {
                logException($t, 'sms-error');
            }
        }
        if($this->email_notify_mandarin_payment_receipt_full){
            try {
                $this->notifyCustomerAboutReceiptPaymentFullByEmail($payment, $mandarinPaymentOrder->getAmount());
                $this->notifyContractorAboutReceiptPaymentFullByEmail($payment, $mandarinPaymentOrder->getAmount());
            } catch (\Throwable $t) {
                logException($t, 'email-error');
            }
        }
    }

    /**
     * @param Payment $payment
     * @param $pay_amount
     * @return bool
     * @throws \Exception
     */
    public function notifyCustomerAboutReceiptPaymentFullByEmail(Payment $payment, $pay_amount)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        $email = $deal->getCustomer()->getEmail();
        $data = [
            'deal' => $deal,
            'agent_user_login' => $deal->getCustomer()->getUser()->getLogin(),
            'counteragent_login' => $deal->getContractor()->getUser()->getLogin(),
            PaymentReceiptSender::TYPE_NOTIFY_KEY => PaymentReceiptSender::MANDARIN_PAYMENT_RECEIPT_CUSTOMER,
            'payment_receipt_type'  => PaymentReceiptSender::PAYMENT_FULL,
            'pay_amount' => $pay_amount
        ];
        // Send notification mail to Contractor
        $this->paymentReceiptSender->sendMail($email, $data);

        return true;
    }

    /**
     * @param Payment $payment
     * @param $pay_amount
     * @return bool
     * @throws \Exception
     */
    public function notifyContractorAboutReceiptPaymentFullByEmail(Payment $payment, $pay_amount)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        $email = $deal->getContractor()->getEmail();
        $data = [
            'deal' => $deal,
            'agent_user_login' => $deal->getContractor()->getUser()->getLogin(),
            'counteragent_login' => $deal->getCustomer()->getUser()->getLogin(),
            PaymentReceiptSender::TYPE_NOTIFY_KEY => PaymentReceiptSender::MANDARIN_PAYMENT_RECEIPT_CONTRACTOR,
            'payment_receipt_type'  => PaymentReceiptSender::PAYMENT_FULL,
            'pay_amount' => $pay_amount
        ];
        // Send notification mail to Contractor
        $this->paymentReceiptSender->sendMail($email, $data);

        return true;
    }

    /**
     * @param MandarinPaymentOrder $mandarinPaymentOrder
     * @param Payment $payment
     * @throws \Exception
     */
    public function notifyCustomerAboutReceiptPaymentFullBySms(MandarinPaymentOrder $mandarinPaymentOrder, Payment $payment)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        $dealAgent = $deal->getCustomer();


        $userPhone = $dealAgent->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getCustomerMandarinPaymentReceiptFullSmsMessage($deal, $mandarinPaymentOrder->getAmount());
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }

    /**
     * @param MandarinPaymentOrder $mandarinPaymentOrder
     * @param Payment $payment
     * @throws \Exception
     */
    public function notifyContractorAboutReceiptPaymentFullBySms(MandarinPaymentOrder $mandarinPaymentOrder, Payment $payment)
    {
        /** @var Deal $deal */
        $deal = $payment->getDeal();
        $dealAgent = $deal->getContractor();


        $userPhone = $dealAgent->getUser()->getPhone();
        $smsStrategy = new SmsStrategyContext($userPhone, $this->config['sms_providers']);
        if ( $smsStrategy->isAllowedSmsNotify() ) {
            // Sms provider selection (by provider balance and so on...)
            $smsProvider = $smsStrategy->getSmsProvider();
            // Message create
            $smsMessage = $this->getContractorMandarinPaymentReceiptFullSmsMessage($deal, $mandarinPaymentOrder->getAmount());
            // Send message
            $smsProvider->smsSend($userPhone->getPhoneNumber(), $smsMessage);
        } else {
            throw new \Exception('Forbidden by policy sms notification');
        }
    }


    /**
     * @param MandarinPaymentOrder $mandarinPaymentOrder
     * @param array $data
     * @return MandarinPaymentOrder
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function manageFailedMandarinPaymentOrder(MandarinPaymentOrder $mandarinPaymentOrder, array $data)
    {
        $mandarinPaymentOrder->setStatus(self::STATUS_FAILED);

        #$mandarinPaymentOrder->setAmount($data['price']);
        #$mandarinPaymentOrder->setAction($data['action']);
        $mandarinPaymentOrder->setTransaction($data['transaction']);
        $mandarinPaymentOrder->setCustomerEmail($data['customer_email']);
        $mandarinPaymentOrder->setCustomerPhone($data['customer_phone']);
        $mandarinPaymentOrder->setCardHolder($data['card_holder']);
        $mandarinPaymentOrder->setCardNumber($data['card_number']);
        $mandarinPaymentOrder->setCardExpirationYear($data['card_expiration_year']);
        $mandarinPaymentOrder->setCardExpirationMonth($data['card_expiration_month']);
        if (isset($data['error_code'])) {
            $mandarinPaymentOrder->setErrorCode($data['error_code']);
        }
        if (isset($data['error_description'])) {
            $mandarinPaymentOrder->setErrorDescription($data['error_description']);
        }

        $this->entityManager->persist($mandarinPaymentOrder);
        $this->entityManager->flush();

        // @TODO Уведомления?

        return $mandarinPaymentOrder;
    }

    /**
     * @param Deal $deal
     * @param $data
     * @param string $payment_order_type
     * @return MandarinPaymentOrder
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createMandarinPaymentOrder(Deal $deal,
                                               $data,
                                               string $payment_order_type) :MandarinPaymentOrder
    {
        /** @var PaymentMethodType $paymentMethodType */
        $paymentMethodType = $this->paymentMethodTypeManager
            ->getPaymentMethodTypeByName('acquiring_mandarin');

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $mandarinPaymentOrder = new MandarinPaymentOrder();
        $mandarinPaymentOrder->setType($payment_order_type);
        $mandarinPaymentOrder->setOrderId($data['order_id']);
        $mandarinPaymentOrder->setAmount($data['price']);
        $mandarinPaymentOrder->setAction(CallbackHandleController::ACTION_FOR_PAYMENT);
        #$mandarinPaymentOrder->setTransaction($data['transaction']);
        $mandarinPaymentOrder->setStatus(self::STATUS_PROCESSING);
        #$mandarinPaymentOrder->setCustomerEmail($data['customer_email']);
        #$mandarinPaymentOrder->setCustomerPhone($data['customer_phone']);
        #$mandarinPaymentOrder->setCardHolder($data['card_holder']);
        #$mandarinPaymentOrder->setCardNumber($data['card_number']);
        #$mandarinPaymentOrder->setCardExpirationYear($data['card_expiration_year']);
        #$mandarinPaymentOrder->setCardExpirationMonth($data['card_expiration_month']);
        $mandarinPaymentOrder->setPaymentPurpose($data['payment_purpose']);
        $mandarinPaymentOrder->setCreated(new \DateTime());

        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $this->paymentOrderManager
            ->createPaymentOrderAndSetRelations($payment, $paymentMethodType, $mandarinPaymentOrder);

        $paymentOrder->setMandarinPaymentOrder($mandarinPaymentOrder);

        $this->entityManager->persist($paymentOrder);
        $this->entityManager->flush();

        return $mandarinPaymentOrder;
    }

    /**
     * @param string $orderId
     * @return object
     * @throws \Exception
     */
    public function decodeEncodedOrderId(string $orderId)
    {
        try {
            /*
             * Возвращает
             * object(stdClass)
             *  public 'deal_id'
             *  public 'timestamp'
             *  public 'payment_purpose'
             */
            return $this->tokenAdapter->decodeToken($orderId);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param MandarinPaymentOrder $mandarinPaymentOrder
     * @return array
     */
    public static function getMandarinPaymentOrderOutput(MandarinPaymentOrder $mandarinPaymentOrder): array
    {
        $mandarinPaymentOrderOutput = [];

        $mandarinPaymentOrderOutput['id'] = $mandarinPaymentOrder->getId();
        $mandarinPaymentOrderOutput['payment_method_type'] = PaymentMethodType::ACQUIRING_MANDARIN;
        #$mandarinPaymentOrderOutput['order_id'] = $mandarinPaymentOrder->getOrderId();
        $mandarinPaymentOrderOutput['amount'] = $mandarinPaymentOrder->getAmount();
        $mandarinPaymentOrderOutput['action'] = $mandarinPaymentOrder->getAction();
        $mandarinPaymentOrderOutput['transaction'] = $mandarinPaymentOrder->getTransaction();
        $mandarinPaymentOrderOutput['status'] = $mandarinPaymentOrder->getStatus();
        $mandarinPaymentOrderOutput['customer_email'] = $mandarinPaymentOrder->getCustomerEmail();
        $mandarinPaymentOrderOutput['customer_phone'] = $mandarinPaymentOrder->getCustomerPhone();
        $mandarinPaymentOrderOutput['card_holder'] = $mandarinPaymentOrder->getCardHolder();
        $mandarinPaymentOrderOutput['card_number'] = $mandarinPaymentOrder->getCardNumber();
        $mandarinPaymentOrderOutput['card_expiration_year'] = $mandarinPaymentOrder->getCardExpirationYear();
        $mandarinPaymentOrderOutput['card_expiration_month'] = $mandarinPaymentOrder->getCardExpirationMonth();
        $mandarinPaymentOrderOutput['error_code'] = $mandarinPaymentOrder->getErrorCode();
        $mandarinPaymentOrderOutput['error_description'] = $mandarinPaymentOrder->getErrorDescription();
        $mandarinPaymentOrderOutput['payment_purpose'] = $mandarinPaymentOrder->getPaymentPurpose();
        $mandarinPaymentOrderOutput['created'] = $mandarinPaymentOrder->getCreated()->format('d.m.Y');

        return $mandarinPaymentOrderOutput;
    }


    /**
     * @param string $orderId
     * @param int $recursion_level
     * @return null|object
     */
    public function getIncomingMandarinPaymentOrderByOrderIdWithRecursion(string $orderId, $recursion_level = 0)
    {
        if ($recursion_level >= self::MAX_RECURSION_LEVEL) {

            return null;
        }

        $mandarinPaymentOrder = $this->getIncomingMandarinPaymentOrderByOrderId($orderId);

        if ($mandarinPaymentOrder === null) {
            ++$recursion_level;
            logMessage('MandarinPaymentOrder with orderId not found: ' . $orderId . "\n" . 'recursion_level_' . $recursion_level, 'acquiring-mandarin');
            //ждем перед итерацией 0.5 сек
            usleep(500000);
            return $this->getIncomingMandarinPaymentOrderByOrderIdWithRecursion($orderId, $recursion_level);
        }

        return $mandarinPaymentOrder;
    }

    /**
     * @param string $orderId'
     * @return null|object
     */
    public function getIncomingMandarinPaymentOrderByOrderId(string $orderId)
    {
        $mandarinPaymentOrder = $this->entityManager->getRepository(MandarinPaymentOrder::class)
            ->findOneBy(['orderId' => $orderId, 'type' => PaymentOrderManager::TYPE_INCOMING]);

        // @TODO Если нет то Exception

        return $mandarinPaymentOrder;
    }
    public function getMandarinOrderByDeal($deal)
    {

        $payment  = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(['deal' => $deal ]);
        return $payment;
    }

    public function getMandarinOrderById($paymentOrders)
    {
        $paymentOrdersMandarin=[];
        /** @var PaymentOrder $paymentOrder */
        foreach ($paymentOrders as $paymentOrder) {
            if ($this->paymentOrderManager->checkPaymentOrderHasMandarinPaymentOrderOfSpecificType($paymentOrder, PaymentOrderManager::TYPE_INCOMING)) {
                // Get MandarinPaymentOrder
                $paymentOrdersMandarin[] = $paymentOrder->getMandarinPaymentOrder();
            }
        }

        return  end($paymentOrdersMandarin);
    }
}