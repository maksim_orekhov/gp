<?php
namespace ModulePaymentOrder\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class BankClientPaymentOrderForm
 * @package Application\Form
 *
 * Используется только для валидации данных. Форма, как таковая, по прямому назначению не применяется.
 */
class BankClientPaymentOrderForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('bank-client-payment-order');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'document_type',
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'document_number',
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'document_date',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'date_of_receipt',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'date_of_debit',
        ]);

        // @TODO определиться с типом
        $this->add([
            'type' => 'text',
            'name' => 'amount',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'payer_account',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'payer',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'payer_inn',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'payer_1',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'payer_kpp',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'payer_bank_1',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'payer_bik',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'payer_corr_account',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'recipient_account',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'recipient',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'recipient_inn',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'recipient_1',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'recipient_kpp',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'recipient_bank_1',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'recipient_bik',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'recipient_corr_account',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'payment_purpose',
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'payment_type',
        ]);

        // @TODO определиться с типом
        $this->add([
            'type' => 'text',
            'name' => 'priority',
        ]);

        // Add the CSRF field
        /*
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);
        */

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        /*
        $inputFilter->add([
            'name'     => 'document_type',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
                ['name' => 'StripNewlines'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 255]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'document_number',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 11]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'document_date',
            'required' => true, // @TODO Уточнить!
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 9, 'max' => 10]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'date_of_receipt',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 9, 'max' => 10]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'date_of_debit',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 9, 'max' => 10]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'amount',
            'required' => true,
            'validators' => [
                ['name' => 'Float'],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'payer_account',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'payer',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 255]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'payer_inn',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            //валидный инн для теста юр лиц: 4217030520
            //валидный инн для теста физ лиц: 683103646744
            'validators' => [
                ['name' => \Application\Form\Validator\InnValidator::class],
            ],
//            'validators' => [
//                ['name' => 'Digits'],
//                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 12]],
//            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'payer_1',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 255]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'payer_kpp',
            'required' => false, // В некоторых платёжках нет
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 9]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'payer_bank_1',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 255]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'payer_bik',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 9]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'payer_corr_account',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'recipient_account',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
            ],
        ]);
        */

        $inputFilter->add([
            'name'     => 'recipient',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 255]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'recipient_inn',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            //валидный инн для теста юр лиц: 4217030520
            //валидный инн для теста физ лиц: 683103646744
            'validators' => [
                ['name' => \Application\Form\Validator\InnValidator::class],
            ],
//            'validators' => [
//                ['name' => 'Digits'],
//                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 12]],
//            ],
        ]);
        /*
        $inputFilter->add([
            'name'     => 'recipient_1',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 255]],
            ],
        ]);
        */

        $inputFilter->add([
            'name'     => 'recipient_kpp',
            'required' => false, // В некоторых платёжках нет
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 9]],
            ],
        ]);

        /*
        $inputFilter->add([
            'name'     => 'recipient_bank_1',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 255]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'recipient_bik',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 9]],
            ],
        ]);
        */

        /*
        $inputFilter->add([
            'name'     => 'recipient_corr_account',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 15, 'max' => 20]],
            ],
        ]);
        */

        $inputFilter->add([
            'name'     => 'payment_purpose',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 5, 'max' => 255]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'payment_type',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 3]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'priority',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                ['name' => 'Digits'],
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 3]],
            ],
        ]);
    }
}