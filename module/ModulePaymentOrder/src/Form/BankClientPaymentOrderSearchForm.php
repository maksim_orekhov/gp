<?php
namespace ModulePaymentOrder\Form;

use Application\Form\FilterForm;
use ModulePaymentOrder\Service\BankClientPaymentOrderManager;
use Zend\InputFilter\InputFilter;

class BankClientPaymentOrderSearchForm extends FilterForm
{
    use \Application\Provider\FormFieldsetTrait;

    const PROPERTIES_FILTER_RULES = [
        'payment_purpose'           => 'like',
        'payment_amount'            => 'equal',
        'payment_created_from'      => 'date_greater',
        'payment_created_till'      => 'date_smaller',
        'payment_recipient_account' => 'equal',
        'payment_payer'             => 'like',
        'payment_payer_account'     => 'equal',
        'trouble_type'              => 'custom',
    ];

    /**
     * DisputeSearchForm constructor.
     */
    public function __construct()
    {
        parent::__construct('bank-client-payment-order-search-form');

        // Set POST method for this form
        $this->setAttribute('method', 'get');

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return array
     */
    public function getFilterRules()
    {
        return self::PROPERTIES_FILTER_RULES;
    }

    protected function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'filter[payment_purpose]',
            'attributes' => [
                'id' => 'payment_purpose',
                'placeholder'=>'Назначение платежа',
            ],
            'options' => array(
                'label' => 'Назначение платежа',
            )
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[payment_amount]',
            'attributes' => [
                'id' => 'payment_amount',
                'class'=>'filter-input',
                'placeholder'=>'Сумма',
            ],
            'options' => [
                'label' => 'Сумма',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[payment_created_from]',
            'options' => [
                'label' => 'Дата от',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'filter-date__item datepicker-here',
                'placeholder'=>'Дата от',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'filter[payment_created_till]',
            'options' => [
                'label' => 'Дата до',
            ],
            'attributes' => [
                'step' => 'any',
                'class'=>'filter-date__item datepicker-here',
                'placeholder'=>'Дата до',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => "filter[payment_recipient_account]",
            'attributes' => [
                'id' => 'payment_recipient_account',
                'class'=>'filter-input',
                'placeholder'=>'Счет получателя',
            ],
            'options' => [
                'label' => 'Счет получателя',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => "filter[payment_payer]",
            'attributes' => [
                'id' => 'payment_payer',
                'class'=>'filter-input',
                'placeholder'=>'Плательщик',
            ],
            'options' => [
                'label' => 'Плательщик',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => "filter[payment_payer_account]",
            'attributes' => [
                'id' => 'payment_payer_account',
                'class'=>'filter-input',
                'placeholder'=>'Счет плетельщика',
            ],
            'options' => [
                'label' => 'Счет плетельщика',
            ],
        ]);

        $this->add([
            'name' => 'filter[trouble_type]',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => [
                'id' => 'abnormal_type',
                'class'=>'form-control',
            ],
            'options' => array(
                'label' => 'Тип проблемы',
                'value_options' => array_merge(
                    ['' => 'Все проблемы'],
                    BankClientPaymentOrderManager::BANK_CLIENT_PAYMENT_ORDER_TROUBLE_TYPES
                )
            )
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Search',
                'class'=>'btn btn-primary'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'filter[payment_purpose]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['max' => 100]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[payment_amount]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['max' => 11]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[payment_created_from]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'Regex',
                    'options' => [
                        'pattern' => '/\d{1}/',
                        #'message' => 'Сообщение'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[payment_created_till]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'Regex',
                    'options' => [
                        'pattern' => '/\d{1}/',
                        #'message' => 'Сообщение'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[payment_recipient_account]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['max' => 20]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[payment_payer]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['max' => 100]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[payment_payer_account]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['max' => 20]],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'filter[trouble_type]',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [],
        ]);
    }
}