<?php
namespace ModulePaymentOrder;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'bank-client-payment-orders-trouble' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/bank-client/payment-orders/trouble',
                    'defaults' => [
                        'controller' => Controller\BankClientPaymentOrderController::class,
                        'action'     => 'trouble',
                    ],
                ],
            ],
        ],
    ],
];