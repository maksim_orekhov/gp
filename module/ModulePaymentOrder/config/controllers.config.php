<?php
namespace ModulePaymentOrder;

return [
    'controllers' => [
        'factories' => [
            Controller\BankClientPaymentOrderController::class => Controller\Factory\BankClientPaymentOrderControllerFactory::class,
        ],
    ],
];