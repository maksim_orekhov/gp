<?php
namespace ModulePaymentOrder;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'service_manager' => [
        'factories' => [
            Service\PayOffManager::class => Service\Factory\PayOffManagerFactory::class,
            Service\PaymentOrderManager::class => Service\Factory\PaymentOrderManagerFactory::class,
            Service\PaymentMethodTypeManager::class => Service\Factory\PaymentMethodTypeManagerFactory::class,
            Service\BankClientPaymentOrderManager::class => Service\Factory\BankClientPaymentOrderManagerFactory::class,
            Service\BankClientPaymentOrderHandler::class => Service\Factory\BankClientPaymentOrderHandlerFactory::class,
            Service\MandarinPaymentOrderManager::class => Service\Factory\MandarinPaymentOrderManagerFactory::class,
            Service\MandarinPaymentOrderManager::class => Service\Factory\MandarinPaymentOrderManagerFactory::class,
        ],
    ],
];