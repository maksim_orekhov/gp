<?php
namespace ModulePaymentOrder;

return [
    // The 'access_filter' key is used by the Application module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            'mode' => 'permissive',     // все, что не запрещено, разрешено
            //'mode' => 'restrictive',  // все, что не разрешено, запрещено
        ],
        'controllers' => [
            Controller\BankClientPaymentOrderController::class => [
                [
                    'actions' => [
                        'trouble',
                    ],
                    'allows' => ['#Operator']
                ],
            ],
        ],
    ],
];