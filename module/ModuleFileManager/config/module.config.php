<?php
namespace ModuleFileManager;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'file-single' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/file/:id',
                    'constraints' => [
                        'id' => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Controller\FileController::class,
                    ],
                ]
            ],
            'common-file-download' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/common-file/download/:file_name',
                    'constraints' => [
                        'file_name' => '[\w0-9_-]*\.[\w]{1,4}',
                    ],
                    'defaults' => [
                        'controller' => Controller\FileController::class,
                        'action' => 'downloadCommonFile',
                    ],
                ]
            ],
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\FileController::class => Controller\Factory\FileControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\FileManager::class => Service\Factory\FileManagerFactory::class
        ],
    ],
    'controller_plugins' => [
    ],
    // The 'access_filter' key is used by the File module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            'mode' => 'permissive',     // все, что не запрещено, разрешено
            //'mode' => 'restrictive',  // все, что не разрешено, запрещено
        ],
    ],
    'view_manager' => [
        'template_map' => [

        ],
        'template_path_stack' => [
            'ModuleFile' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Entity')
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' =>  __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
];
