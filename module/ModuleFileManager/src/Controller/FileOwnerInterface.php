<?php

namespace ModuleFileManager\Controller;

use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Interface FileOwnerInterface
 * @package ModuleFileManager\Controller
 */
interface FileOwnerInterface
{
    /**
     * @return \Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function fileListAction();

    /**
     * @return \Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function fileUploadAction();

    /**
     * @return mixed
     * @throws \Exception
     */
    public function fileShowAction();

    /**
     * @return \Zend\Http\Response|JsonModel
     * @throws \Exception
     */
    public function fileDeleteAction();
}