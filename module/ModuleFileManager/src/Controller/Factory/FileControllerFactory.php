<?php
namespace ModuleFileManager\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleFileManager\Controller\FileController;

/**
 * Class FileControllerFactory
 * @package ModuleFileManager\Controller\Factory
 */
class FileControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $fileManager = $container->get(\ModuleFileManager\Service\FileManager::class);
        $config = $container->get('Config');

        return new FileController($fileManager, $config);
    }
}