<?php

namespace ModuleFileManager\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Exception\LogicException;
use Core\Service\TwigViewModel;
use Zend\View\Model\ViewModel;
use ModuleFileManager\Service\FileManager;

/**
 * Class FileController
 * @package ModuleFileManager\Controller
 */
class FileController extends AbstractRestfulController
{
    const ERROR_CREATE_FILE       = 'Could not create file object';
    const ERROR_CREATE_FOLDER     = 'Could not create folder';
    const ERROR_SAVE_FILE_TO_DISC = 'Could not save file to disc';
    const ERROR_DATA_PROVIDE      = 'Data was not provided';
    const ERROR_NOT_FOUND         = 'File not found';
    const ERROR_FILE_DISPLAY      = 'File can not be displayed';
    const PATH_COMMON             = '/common';

    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * @var array
     */
    private $config;

    /**
     * FileController constructor.
     * @param FileManager $fileManager
     * @param array $config
     */
    public function __construct(FileManager $fileManager,
                                array $config)
    {
        $this->fileManager = $fileManager;
        $this->config      = $config;
    }

    /**
     * @param mixed $id
     * @return bool|\Core\Service\TwigViewModel|mixed|\Zend\Http\Response|\Zend\Stdlib\ResponseInterface|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function get($id)
    {
        try {
            $file = $this->fileManager->getFileById($id);
            $display = false;
            if ( $file && $this->fileManager->isFileReadable($file)) {
                $display = $this->fileManager->isFileTypeDisplayable($file->getType());
            }

            if ($display) {

                return $this->fileManager->fileDisplay($file);
            }

            return $this->fileManager->download($file->getId(), $file->getPath());
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Exception
     */
    public function downloadCommonFileAction()
    {
        try {
            $file_name = $this->params()->fromRoute('file_name', null);
            if (! $file_name) {

                throw new LogicException(null, LogicException::FILE_NAME_NOT_FOUND_FROM_ROUTE);
            }
            $path_file = $this->config['file-management']['main_upload_folder'] . self::PATH_COMMON. '/' .$file_name;
            if (! file_exists($path_file) ) {

                throw new LogicException(null, LogicException::FILE_NOT_FOUND_IN_COMMON_DIRECTORY);
            }
            $data = $this->collectIncomingData();
            $out_name = $file_name;

            if ($data && array_key_exists('out_file_name', $data) && preg_match("/^.*\.[\w]{1,4}/iu", $data['out_file_name'])) {
                $out_name = $data['out_file_name'];
            }

            $file = [
                'path' => $path_file,
                'name' => $file_name,
                'out_name' => $out_name,
                'type' => mime_content_type($path_file),
                'size' => filesize($path_file),
            ];
            return $this->fileManager->downloadFile($file);
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }
    }
}
