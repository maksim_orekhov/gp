<?php
namespace ModuleFileManager\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\Paginator\Paginator;
use Doctrine\ORM\QueryBuilder;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

/**
 * Class FileRepository
 * @package ModuleFileManager\Entity\File
 */
class FileRepository extends EntityRepository
{
    /**
     * @param string $file_storing_folder
     * @param array $params
     * @return array|Paginator
     */
    public function getAllBankClientFilesByPath(string $file_storing_folder, array $params)
    {
        $qb = $this->createQueryBuilder('f')
            ->andWhere('f.path = :file_storing_folder')
            ->setParameter('file_storing_folder', $file_storing_folder)
        ;

        if (array_key_exists('per_page', $params) && array_key_exists('page', $params)) {
            $fileCollection = $this->paginate($qb, $params['page'], $params['per_page']);
        } else {
            $fileCollection = $qb->getQuery()->getResult();
        }

        return $fileCollection;
    }

    /**
     * @param QueryBuilder $query
     * @param $limit
     * @param $page
     * @return Paginator
     */
    public function paginate(QueryBuilder $query, $page=1, $limit=10)
    {
        $page = (abs((int) $page) > 0) ? $page : 1;
        $limit = (abs((int) $limit) > 0) ? $limit : 10;

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);
        $paginator->setDefaultItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);

        return $paginator;
    }
}