<?php
namespace ModuleFileManager\Service;

use ModuleFileManager\Entity\File;
use DateTime;

/**
 * Class FileManager
 * @package ModuleFileManager\Service
 */
class FileManager
{
    const ERROR_CREATE_FILE       = 'Could not create file object';
    const ERROR_CREATE_FOLDER     = 'Could not create folder';
    const ERROR_SAVE_FILE_TO_DISC = 'Could not save file to disc';
    const ERROR_DATA_PROVIDE      = 'Data was not provided';
    const ERROR_NOT_FOUND         = 'File not found';
    const ERROR_FILE_DISPLAY      = 'File can not be displayed';

    /**
     * @var \Zend\Http\Response
     */
    private $response;

    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var $config
     */
    private $config;

    /**
     * FileManager constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param \Zend\Http\Response $response
     * @param $config
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                \Zend\Http\Response $response = null,
                                $config)
    {
        $this->entityManager = $entityManager;
        $this->response = $response;
        $this->config = $config;
    }

    /**
     * @param $id
     * @return File
     * @throws \Exception
     */
    public function getFileById($id): File
    {
        /** @var File $file */
        $file = $this->entityManager->getRepository(File::class)
            ->findOneById($id);

        if(!$file) {

            throw new \Exception("Can not find File in database by Id");
        }

        return $file;
    }

    /**
     * @param $name
     * @return File
     * @throws \Exception
     */
    public function getFileByName($name): File
    {
        /** @var File $file */
        $file = $this->entityManager->getRepository(File::class)
            ->findOneBy(['name' => $name]);

        if(!$file) {

            throw new \Exception("Can not find file " . $name . " in database");
        }

        return $file;
    }

    /**
     * @param $origin_name
     * @return File
     */
    public function getFileByOriginName($origin_name)
    {
        /** @var File $file */
        $file = $this->entityManager->getRepository(File::class)
            ->findOneBy(['originName' => $origin_name]);

        return $file;
    }

    /**
     * @param string $name
     * @param string $origin_name
     * @param string $path
     * @param string $type
     * @param integer $size
     * @return File
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveFileInfoToDB($name, $origin_name, $path, $type, $size)
    {
        $file = new File();
        $file->setName($name);
        $file->setOriginName($origin_name);
        $file->setPath($path);
        $file->setType($type);
        $file->setSize($size);
        $file->setCreated(new DateTime("now"));
        // Prepare object
        $this->entityManager->persist($file);
        // Save to DB
        $this->entityManager->flush();

        return $file;
    }

    /**
     * Remove file
     * @param File $file
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeFileInfoFormDB(File $file)
    {
        $this->entityManager->remove($file);

        $this->entityManager->flush();
    }

    /**
     * @param array $params
     * @param bool $from_string
     * @return File
     * @throws \Exception
     */
    public function write(array $params, $from_string = false)
    {
        if ($params && isset($params['file_source']) && isset($params['file_name'])
            && isset($params['file_type']) && isset($params['path'])) {

            // Break the file name into parts by '.' to get extension
            $pieces = explode(".", $params['file_name']);
            // Folder to save file
            $destination_folder = "./".$this->config['file-management']['main_upload_folder'].$params['path'];
            // Generate new name
            $unique_name = $this->getUniqueFileName($params['file_name'], $destination_folder);
            // New file name ($unique_name with extension)
            $new_file_name = $unique_name . "." . end($pieces);

            $destination = $destination_folder."/".$new_file_name;

            // Save file
            $this->saveFile($params['file_source'], $destination_folder, $destination, $from_string);

            // Create, save and return File object
            $file = $this->saveFileInfoToDB($new_file_name, $params['file_name'], $params['path'],
                $params['file_type'], $params['file_size']);

            if( !$file || !$file instanceof File) {

                throw new \Exception(self::ERROR_CREATE_FILE);
            }

        } else {

            throw new \Exception(self::ERROR_DATA_PROVIDE);
        }

        return $file;
    }

    /**
     * @param String $file_path
     * @return String
     * @throws \Exception
     */
    public function read($file_path)
    {
        if (file_exists($file_path)) {
            $file = file_get_contents($file_path, FILE_USE_INCLUDE_PATH);

            if( !$file ) {
                throw new \Exception('Failed to read file contents');
            }
        } else {
            throw new \Exception('Data was not provided');
        }

        return $file;
    }

    /**
     * @param File $file
     * @return bool|string
     */
    public function getContent(File $file)
    {
        $path = "./".$this->config['file-management']['main_upload_folder'].$file->getPath()."/".$file->getName();

        return file_get_contents($path);
    }

    /**
     * @param File $file
     * @return bool|string
     * @throws \Exception
     */
    public function readToEntityFile(File $file)
    {
        if (!empty($file)) {
            $path = "./" . $this->config['file-management']['main_upload_folder'] . $file->getPath() . "/" . $file->getName();

            $file_content = file_get_contents($path, FILE_USE_INCLUDE_PATH);

            if (!$file_content) {
                throw new \Exception('Failed to read file contents');
            }
        } else {

            throw new \Exception('Data was not provided');
        }

        return $file_content;
    }

    /**
     * @param File $file
     * @return bool
     */
    public function isFileReadable(File $file)
    {
        try {
            $file = $this->readToEntityFile($file);
            if($file) {
                return true;
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param integer $file_id
     * @return bool
     * @throws \Exception
     */
    public function delete($file_id)
    {
        $file = $this->getFileById($file_id);

        if( !$file || !$file instanceof File) {

            throw new \Exception(self::ERROR_NOT_FOUND);
        }

        // Remove from disk
        $path = './'.$this->config['file-management']['main_upload_folder'].$file->getPath().'/'. $file->getName();

        // Remove File from DB
        $this->removeFileInfoFormDB($file);

        if ( file_exists($path) ) {
            unlink($path);

            return true;
        }

        return false;
    }

    /**
     * Создание директории, если нет
     * @param $folder
     * @throws \Exception
     */
    public function createFolderIfNotExists($folder)
    {
        if(! is_dir($folder)) {
            // По умолчанию 0777. Ставим 0755 rw-----.
            if (false == mkdir($folder, 0755, true)) {
                throw new \Exception(self::ERROR_CREATE_FOLDER);
            }
        }
    }

    /**
     * @param $file_id
     * @param $file_storing_folder
     * @return \Zend\Stdlib\ResponseInterface
     * @throws \Exception
     */
    public function download($file_id, $file_storing_folder)
    {
        try {
            // Get file
            $file = $this->getFileById($file_id);

            if($file->getPath() === '/deal') {
                // Если запрашивается файл контракта сделки, то отдаем с красивым названием, которое сгенерировалось при создании файла
                $file_name = $file->getOriginName();
            } else {
                $file_name = $file->getName();
            }

            $file = [
                'path' => $this->config['file-management']['main_upload_folder'].$file_storing_folder. '/' .$file->getName(),
                'name' => $file->getName(),
                'out_name' => $file_name,
                'type' => $file->getType(),
                'size' => $file->getSize(),
            ];
            return $this->downloadFile($file);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param array $file
     * @return \Zend\Http\Response
     */
    public function downloadFile(array $file): \Zend\Http\Response
    {
        $fileContents = file_get_contents($file['path']);

        $response = $this->response;
        $response->setContent($fileContents);

        $headers = $response->getHeaders();
        $headers->clearHeaders()
            ->addHeaderLine('Content-Type', $file['type'])
            ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $file['out_name'] . '"')
            ->addHeaderLine('Content-Length', $file['size']);

        return $this->response;
    }

    /**
     * Save file
     * If $from_string = true, save provided string to file, else save from file
     * @param string $source
     * @param string $destination_folder
     * @param string $destination
     * @param bool $from_string
     * @throws \Exception
     */
    private function saveFile(string $source, string $destination_folder, string $destination, bool $from_string)
    {
        // Создание директории, если нет
        $this->createFolderIfNotExists($destination_folder);
        // If $from_string = true, save provided string to file, else save from file
        // file_put_contents возвращает количество байт или false, если неудачно
        if($from_string) {
            $result = file_put_contents($destination, $source);
        } else {
            $result = copy($source, $destination);
        }

        if(false == $result) {
            throw new \Exception(self::ERROR_SAVE_FILE_TO_DISC);
        }
    }

    /**
     * @param $file_name
     * @param $destination_folder
     * @return string
     */
    private function getUniqueFileName($file_name, $destination_folder)
    {
        $random_string = $this->getRandomString(8);
        $unique_name = md5($file_name. $random_string . time());
        // Check if file with new name already exists
        if(file_exists($destination_folder.'/'.$unique_name)) {
            // If exists try again
            $this->getUniqueFileName($file_name, $destination_folder);
        }

        return $unique_name;
    }

    /**
     * @param $length
     * @return string
     */
    private function getRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $len = strlen($characters);
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, $len - 1)];
        }

        return $string;
    }

    /**
     * @param $type
     * @return bool|mixed
     */
    public function isFileTypeDisplayable($type) {

        $type_display = array(
            'text/plain' => false,
            'text/html'  => false,
            'text/css' => false,
            'application/javascript' => false,
            'application/json' => false,
            'application/xml' => false,
            'application/x-shockwave-flash' => false,
            'video/x-flv' => false,

            // images
            'image/png' => true,
            'image/jpeg'  => true,
            'image/gif'  => true,
            'image/bmp'  => true,
            'image/vnd.microsoft.icon'  => true,
            'image/tiff'  => true,
            'image/svg+xml'  => true,

            // archives
            'application/zip'  => false,
            'application/x-rar-compressed'  => false,
            'application/x-msdownload'  => false,
            'application/vnd.ms-cab-compressed'  => false,

            // audio/video
            'audio/mpeg'  => false,
            'video/quicktime'  => false,
            'video/mp4'  => false,

            // adobe
            'application/pdf' => false,
            'image/vnd.adobe.photoshop' => false,
            'application/postscript' => false,

            // ms office
            'application/msword' => false,
            'application/rtf' => false,
            'application/vnd.ms-excel' => false,
            'application/vnd.ms-powerpoint' => false,

            // open office
            'application/vnd.oasis.opendocument.text' => false,
            'application/vnd.oasis.opendocument.spreadsheet' => false,
        );

        if (key_exists($type, $type_display)) {

            return $type_display[$type];
        }

        return false;
    }

    /**
     * @param File $file
     * @return \Zend\Http\Response
     * @throws \Exception
     */
    public function fileDisplay(File $file)
    {
        if (!$this->isFileTypeDisplayable($file->getType())) {
            throw new \Exception(self::ERROR_FILE_DISPLAY);
        }

        $content= $this->getContent($file);
        $response = $this->response;
        $response->setContent($content);

        $response
            ->getHeaders()
            ->addHeaderLine('Content-Transfer-Encoding', 'binary')
            ->addHeaderLine('Content-Type', $file->getType());

        return $response;
    }
}