<?php
namespace ModuleFileManager\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleFileManager\Service\FileManager;
use Zend\Http\Response;

/**
 * Class FileManagerFactory
 * @package ModuleFileManager\Service\Factory
 */
class FileManagerFactory implements FactoryInterface
{
    /**
     * This method creates the FileManager service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $response = $container->get('Response');
        $config = $container->get('Config');

        // Если $response не экземпляр \Zend\Http\Response - обнуляем.
        if( !$response instanceof Response) {
            $response = null;
        }

        return new FileManager(
            $entityManager,
            $response,
            $config
        );
    }
}