<?php
namespace ModuleRbac;

use Core\EventManager\AuthEventProvider;
use ModuleRbac\Listener\RoleListenerAggregate;

return [
    'controller_plugins' => [
        'factories' => [
            Controller\Plugin\AccessPlugin::class => Controller\Plugin\Factory\AccessPluginFactory::class,
        ],
        'aliases' => [
            'access' => Controller\Plugin\AccessPlugin::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            // Rbac
            Service\RoleManager::class => Service\Factory\RoleManagerFactory::class,
            Service\PermissionManager::class => Service\Factory\PermissionManagerFactory::class,
            Service\RbacManager::class => Service\Factory\RbacManagerFactory::class,
            Listener\RoleListenerAggregate::class => Listener\Factory\RoleListenerAggregateFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            View\Helper\Access::class => View\Helper\Factory\AccessFactory::class,
        ],
        'aliases' => [
            'access' => View\Helper\Access::class,
        ],
    ],
    'event_provider' => [
        //provider class => listeners
        AuthEventProvider::class => [
            //listener class => priority
            RoleListenerAggregate::class => 9999,
        ],
    ],
];
