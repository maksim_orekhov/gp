<?php
namespace ModuleRbac\Service\Factory;

use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RoleManager;
use ModuleRbac\Service\RbacManager;

/**
 * This is the factory class for RoleManager service
 */
class RoleManagerFactory
{
    /**
     * This method creates the UserManager service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $rbacManager = $container->get(RbacManager::class);

        return new RoleManager($entityManager, $rbacManager);
    }
}