<?php
namespace ModuleRbac\Service\Factory;

use Interop\Container\ContainerInterface;
use ModuleRbac\Service\PermissionManager;
use ModuleRbac\Service\RbacManager as RbacManager;
/**
 * This is the factory class for PermissionManager service
 */
class PermissionManagerFactory
{
    /**
     * This method creates the UserManager service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $rbacManager = $container->get(RbacManager::class);

        return new PermissionManager($entityManager, $rbacManager);
    }
}