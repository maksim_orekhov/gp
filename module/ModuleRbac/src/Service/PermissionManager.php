<?php
namespace ModuleRbac\Service;

use Application\Entity\Permission;

/**
 * This service is responsible for adding/editing permissions.
 */
class PermissionManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * RBAC manager.
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * PermissionManager constructor.
     * @param \Doctrine\ORM\EntityManager   $entityManager
     * @param RbacManager                   $rbacManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                \ModuleRbac\Service\RbacManager $rbacManager)
    {
        $this->entityManager    = $entityManager;
        $this->rbacManager      = $rbacManager;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getPermissionByName(string $name)
    {
        $permission = $this->entityManager->getRepository(Permission::class)->findOneByName($name);

        return $permission;
    }

    /**
     * Adds a new permission
     * @param $data
     * @throws \Exception
     */
    public function addPermission($data)
    {
        $existingPermission = $this->entityManager->getRepository(Permission::class)
            ->findOneByName($data['name']);
        if ($existingPermission!=null) {
            throw new \Exception('Permission with such name already exists');
        }

        $permission = new Permission();
        $permission->setName($data['name']);
        $permission->setDescription($data['description']);

        $this->entityManager->persist($permission);

        $this->entityManager->flush();

        // Reload RBAC container.
        $this->rbacManager->init(true);
    }

    /**
     * Updates an existing permission
     * @param Permission    $permission
     * @param array         $data
     * @throws \Exception
     */
    public function updatePermission(Permission $permission, array $data)
    {
        $existingPermission = $this->getPermissionByName($data['name']);
        // If permission with new name already exists
        if ($existingPermission != null && $existingPermission instanceof Permission && $existingPermission != $permission) {
            throw new \Exception('Another permission with such name already exists');
        }

        $permission->setName($data['name']);
        $permission->setDescription($data['description']);

        $this->entityManager->flush();

        // Reload RBAC container.
        $this->rbacManager->init(true);
    }

    /**
     * Deletes the given permission
     *
     * @param $permission
     */
    public function deletePermission($permission)
    {
        $this->entityManager->remove($permission);
        $this->entityManager->flush();

        // Reload RBAC container.
        $this->rbacManager->init(true);
    }
}
