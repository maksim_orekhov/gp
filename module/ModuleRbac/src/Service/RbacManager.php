<?php
namespace ModuleRbac\Service;

use Application\Entity\User;
use Core\Entity\Interfaces\UserInterface;
use Core\Service\Auth\AuthService;
use Doctrine\ORM\EntityManager;
use Zend\Cache\Storage\StorageInterface;
use Application\Entity\Role;
use Zend\Permissions\Rbac\RoleInterface as RbacRoleInterface;

/**
 * This service is responsible for initializing RBAC (Role-Based Access Control).
 */
class RbacManager
{
    /**
     * Doctrine entity manager.
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Rbac
     */
    private $rbac;

    /**
     * Auth service.
     * @var AuthService
     */
    private $authService;

    /**
     * Filesystem cache.
     * @var \Zend\Cache\Storage\StorageInterface
     */
    private $cache;

    /**
     * Assertion managers.
     * @var array
     */
    private $assertionManagers = [];

    /**
     * RbacManager constructor.
     * @param EntityManager $entityManager
     * @param AuthService $authService
     * @param StorageInterface $cache
     * @param array $assertionManagers
     */
    public function __construct(EntityManager $entityManager,
                                AuthService $authService,
                                StorageInterface $cache,
                                array $assertionManagers)
    {
        $this->entityManager        = $entityManager;
        $this->authService          = $authService;
        $this->cache                = $cache;
        $this->assertionManagers    = $assertionManagers;
    }

    /**
     * Initializes the RBAC container.
     * @param bool $forceCreate
     */
    public function init($forceCreate = true)
    {
        if ($this->rbac !== null && !$forceCreate) {
            // Already initialized; do nothing.
            return;
        }

        // If user wants us to reinit RBAC container, clear cache now.
        if ($forceCreate) {
            $this->cache->removeItem('rbac_container');
        }

        // Try to load Rbac container from cache.
        $result = false;
        $this->rbac = $this->cache->getItem('rbac_container', $result);

        if (!$result) {
            // Create Rbac container.
            $rbac = new Rbac();
            $this->rbac = $rbac;

            // Construct role hierarchy by loading roles and permissions from database
            $rbac->setCreateMissingRoles(true);

            $roles = $this->entityManager->getRepository(Role::class)
                ->findBy([], ['id'=>'ASC']);

            foreach ($roles as $role) {

                $roleName = $role->getName();

                $parentRoleNames = [];
                foreach ($role->getParentRoles() as $parentRole) {
                    $parentRoleNames[] = $parentRole->getName();
                }

                $rbac->addRole($roleName, $parentRoleNames);

                foreach ($role->getPermissions() as $permission) {
                    $rbac->getRole($roleName)->addPermission($permission->getName());
                }
            }
            /** @var UserInterface $user */
            $user = $this->entityManager->getRepository(User::class)
                ->findOneByLogin($this->authService->getIdentity());

            $rbac->addUser($user);

            // Save Rbac container to cache.
            $this->cache->setItem('rbac_container', $rbac);
        }
    }

    /**
     * Checks whether the given user has role
     *
     * @param User         $user
     * @param string       $role_name
     * @return bool
     * @throws \Exception
     */
    public function hasRole($user, $role_name): bool
    {
        if ($this->rbac === null) {
            $this->init();
        }

        $rbacRole = $this->rbac->getRole($role_name);
        if(!$rbacRole || !($rbacRole instanceof RbacRoleInterface)) {

            return false;
        }

        if ($user === null) {
            // Get login from Identity
            $identity = $this->authService->getIdentity();

            if ($identity === null) {
                return false;
            }

            // Get user object by login from Identity
            $user = $this->rbac->getUser();

            // Identity presents in session, but there is no such user in database
            if ($user === null) {

                return false;
            }
        }

        $roles = $user->getRoles();
        /** @var Role $userRole */
        foreach ($roles as $userRole) {
            if ($userRole->getName() === $rbacRole->getName()) {

                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the given user has permission
     *
     * @param User         $user
     * @param string        $permission
     * @param null|array    $params
     * @return bool
     * @throws \Exception
     */
    public function isGranted($user, $permission, $params = null): bool
    {
        if ($this->rbac === null) {
            $this->init();
        }

        if ($user === null) {

            // Get login from Identity
            $identity = $this->authService->getIdentity();

            if ($identity === null) {
                return false;
            }

            $user = $this->rbac->getUser();

            // Identity presents in session, but there is no such user in database
            if ($user === null) {

                return false;
            }
        }

        $roles = $user->getRoles();

        foreach ($roles as $role) {

            if ($this->rbac->isGranted($role->getName(), $permission)) {

                if ($params === null) {

                    return true;
                }
                foreach ($this->assertionManagers as $assertionManager) {

                    if ($assertionManager->assert($this->rbac, $permission, $params)) {

                        return true;
                    }
                }

                return false;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasIdentity(): bool
    {
        if ($this->rbac === null) {
            $this->init();
        }

        $identity = $this->authService->getIdentity();

        if ($identity === null) {

            return false;
        }

        $user = $this->rbac->getUser();

        if ($user === null) {
            $this->authService->clearIdentity();

            return false;
        }

        return true;
    }
}