<?php
namespace ModuleRbac\Service;

use Application\Entity\Role;

/**
 * This service is responsible for adding/editing roles.
 */
class RoleManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * RBAC manager.
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * RoleManager constructor.
     * @param \Doctrine\ORM\EntityManager           $entityManager
     * @param \ModuleRbac\Service\RbacManager $rbacManager
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager,
                                \ModuleRbac\Service\RbacManager $rbacManager)
    {
        $this->entityManager    = $entityManager;
        $this->rbacManager      = $rbacManager;
    }

    /**
     * @param string $name
     * @return Role|null
     */
    public function getRoleByName(string $name)
    {
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)->findOneBy(['name' => $name]);

        return $role ?? null;
    }

    /**
     * Adds a new role
     *
     * @param $data
     * @throws \Exception
     */
    public function addRole($data)
    {
        $existingRole = $this->getRoleByName($data['name']);
        if ($existingRole != null) {
            throw new \Exception('Role with such name already exists');
        }

        $role = new Role;
        $role->setName($data['name']);
        $role->setDescription($data['description']);

        $this->entityManager->persist($role);

        // Apply changes to database.
        $this->entityManager->flush();

        // Reload RBAC container.
        $this->rbacManager->init(true);
    }

    /**
     * Updates an existing role
     *
     * @param $role
     * @param $data
     * @throws \Exception
     */
    public function updateRole($role, $data)
    {
        $existingRole = $this->entityManager->getRepository(Role::class)
            ->findOneByName($data['name']);
        if ($existingRole!=null && $existingRole!=$role) {
            throw new \Exception('Another role with such name already exists');
        }

        $role->setName($data['name']);
        $role->setDescription($data['description']);

        $this->entityManager->flush();

        // Reload RBAC container.
        $this->rbacManager->init(true);
    }

    /**
     * Deletes the given role
     *
     * @param $role
     */
    public function deleteRole($role)
    {
        $this->entityManager->remove($role);
        $this->entityManager->flush();

        // Reload RBAC container.
        $this->rbacManager->init(true);
    }

    /**
     * Retrieves all permissions from the given role and its child roles
     *
     * @param $role
     * @return array
     */
    public function getEffectivePermissions($role)
    {
        $effectivePermissions = [];

        foreach ($role->getChildRoles() as $childRole)
        {
            $childPermissions = $this->getEffectivePermissions($childRole);
            foreach ($childPermissions as $name=>$inherited) {
                $effectivePermissions[$name] = 'inherited';
            }
        }

        foreach ($role->getPermissions() as $permission)
        {
            if (!isset($effectivePermissions[$permission->getName()])) {
                $effectivePermissions[$permission->getName()] = 'own';
            }
        }

        return $effectivePermissions;
    }
}
