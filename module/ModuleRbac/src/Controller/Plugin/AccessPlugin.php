<?php
namespace ModuleRbac\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * This controller plugin is used for role-based access control (RBAC).
 */
class AccessPlugin extends AbstractPlugin
{
    /**
     * @var \ModuleRbac\Service\RbacManager
     */
    private $rbacManager;

    /**
     * AccessPlugin constructor.
     * @param \ModuleRbac\Service\RbacManager $rbacManager
     */
    public function __construct(\ModuleRbac\Service\RbacManager $rbacManager)
    {
        $this->rbacManager = $rbacManager;
    }

    /**
     * Checks whether the currently logged in user has the given permission.
     * @param string $permission Permission name.
     * @param array $params Optional params (used only if an assertion is associated with permission).
     * @return bool
     * @throws \Exception
     */
    public function __invoke($permission, $params = [])
    {
        return $this->rbacManager->isGranted(null, $permission, $params);
    }
}

