<?php
namespace ModuleRbac\Listener\Factory;

use Core\Service\Base\BaseUserManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Listener\RoleListenerAggregate;
use ModuleRbac\Service\RoleManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class RoleListenerAggregateFactory
 * @package ModuleRbac\Listener\Factory
 */
class RoleListenerAggregateFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RoleListenerAggregate|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $roleManager = $container->get(RoleManager::class);
        $baseUserManager = $container->get(BaseUserManager::class);

        return new RoleListenerAggregate(
            $roleManager,
            $baseUserManager
        );
    }
}