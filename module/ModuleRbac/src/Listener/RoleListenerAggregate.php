<?php
namespace ModuleRbac\Listener;

use Core\Entity\Interfaces\UserInterface;
use Core\EventManager\AuthEventProvider as AuthEvent;
use Core\Exception\LogicException;
use Core\Listener\ListenerAggregateTrait;
use Core\Service\Base\BaseUserManager;
use Core\Service\CodeOperationInterface;
use ModuleRbac\Service\RoleManager;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

class RoleListenerAggregate implements ListenerAggregateInterface, CodeOperationInterface
{
    use ListenerAggregateTrait;
    /**
     * @var RoleManager
     */
    private $roleManager;
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;

    /**
     * RoleListenerAggregate constructor.
     * @param RoleManager $roleManager
     * @param BaseUserManager $baseUserManager
     */
    public function __construct(RoleManager $roleManager,
                                BaseUserManager $baseUserManager)
    {
        $this->roleManager = $roleManager;
        $this->baseUserManager = $baseUserManager;
    }

    /**
     * @param EventManagerInterface $events
     * @param int $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(AuthEvent::EVENT_SET_ROLE_TO_USER, [$this, 'setRoleToUser'], $priority);
    }

    /**
     * @param EventInterface $event
     * @return void
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setRoleToUser(EventInterface $event)
    {
        if (!$event->getParam('user') || !$event->getParam('role')) {

            throw new LogicException(null, LogicException::EVENT_REQUIRED_PARAMETERS_MISSING);
        }
        /** @var UserInterface $user */
        $user = $event->getParam('user');
        $role_value = strtolower($event->getParam('role'));

        // create new role and set to user
        $role = $this->roleManager->getRoleByName($role_value);
        if ($role) {

            $this->baseUserManager->setRoleToUser($user, $role);
        }
    }
}