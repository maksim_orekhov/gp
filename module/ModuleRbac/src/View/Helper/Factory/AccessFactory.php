<?php
namespace ModuleRbac\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleRbac\Service\RbacManager;
use ModuleRbac\View\Helper\Access;

/**
 * This is the factory for Access view helper
 */
class AccessFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $rbacManager = $container->get(RbacManager::class);

        return new Access($rbacManager);
    }
}

