<?php
namespace ModuleRbac\View\Helper;

use ModuleRbac\Service\RbacManager;
use Zend\View\Helper\AbstractHelper;

/**
 * This view helper is used to check user permissions.
 */
class Access extends AbstractHelper
{
    /**
     * @var RbacManager
     */
    private $rbacManager;

    public function __construct(RbacManager $rbacManager)
    {
        $this->rbacManager = $rbacManager;
    }

    /**
     * @param $permission
     * @param $params
     * @return bool
     * @throws \Exception
     */
    public function __invoke($permission, $params = null)
    {
        try{
            return $this->rbacManager->isGranted(null, $permission, $params);
        } catch (\Throwable $throwable){
            return false;
        }
    }
}

