<?php

namespace ModuleAuthV2\HTMLOutputs;

use Application\Entity\Deal;
use Application\Entity\DealAgent;
use Application\Entity\DealType;
use Application\Entity\FeePayerOption;
use Application\Entity\Payment;
use Application\Entity\User;
use Core\Service\Base\BaseRoleManager;
use ModuleAuthV2\Controller\UserRegistrationController;
use ModuleAuthV2\Form\UserRegistrationForm;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Application\Entity\Role;
use Zend\Session\SessionManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use CoreTest\ViewVarsTrait;

/**
 * Class HTMLOutputRegistration
 * @package ModuleAuthV2\HTMLOutputs
 */
class HTMLOutputRegistration extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var
     */
    private $user;

    /**
     * @var array
     */
    private $config;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Zend\Stdlib\Exception\LogicException
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();
        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $serviceManager = $this->getApplicationServiceLocator();
        $this->config = $this->getApplicationConfig();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(UserManager::class);
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);

        $user = $this->userManager->selectUserByLogin('test');
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Проверка что поле редактирования email выключено при посещении /register с токеном
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @group HTMLOutput
     * @throws \Exception
     */
    public function testCheckDisabledEmailWhenRegisterByInvitationToken()
    {
        $token = $this->getInvitationToken();

        $this->dispatch('/register?token='.$token, 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleAuth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertMatchedRouteName('register');
        $this->assertQuery('#inputEmail');
        $this->assertArrayHasKey('userRegistrationForm', $view_vars);
        /** @var UserRegistrationForm $userRegistrationForm */
        $userRegistrationForm = $view_vars['userRegistrationForm'];

        //проверка что поле не доступно для редактирования
        $this->assertTrue($userRegistrationForm->get('email')->getAttribute('disabled'));
    }

    /**
     * Проверка доступности Администратору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @group HTMLOutput
     */
    public function testDeleteAction()
    {
        /** @var User $user */
        $user = $this->user;
        // Присвоение роли 'Administrator' тестовому пользователю
        $this->assignRoleToTestUser('Administrator', $this->user);
        // Залогиниваем пользователя
        $this->login($user->getLogin());

        $this->dispatch('/register/delete', 'GET');

        // Проверяем что нужные блоки есть в DOM
        $this->assertQuery('#user-list');
        $this->assertQuery('#delete-user-'.$user->getId());
    }

    /**
     * @throws \Exception
     */
    private function getInvitationToken()
    {
        $email = 'test@test.com';
        $email2 = 'test2@test.com';

        $dealAgent = new DealAgent();
        $dealAgent->setEmail($email);
        $dealAgent->setUser($this->user);
        $dealAgent->setDealAgentConfirm(false);
        $this->entityManager->persist($dealAgent);

        $dealAgent2 = new DealAgent();
        $dealAgent2->setEmail($email2);
        $dealAgent2->setDealAgentConfirm(false);
        $this->entityManager->persist($dealAgent2);

        $dealType = $this->entityManager->getRepository(DealType::class)
            ->findOneBy(array('name' => 'Услуга'));

        $feePayerOption = $this->entityManager->getRepository(FeePayerOption::class)
            ->findOneBy(array('name' => 'CUSTOMER'));

        $deal = new Deal();
        $deal->setCreated(new \DateTime());
        $deal->setName('тест пригласителного токена');
        $deal->setUpdated(new \DateTime());
        $deal->setDeliveryPeriod(14);
        $deal->setFeePayerOption($feePayerOption);
        $deal->setDealType($dealType);


        $deal->setCustomer($dealAgent);
        $deal->setContractor($dealAgent2);
        $deal->setOwner($dealAgent);

        $this->entityManager->persist($deal);

        $payment = new Payment();
        $payment->setDeal($deal);
        $payment->setDealValue(100000);
        $payment->setExpectedValue(104000);
        $payment->setReleasedValue(100000);
        $payment->setFee(2);

        $this->entityManager->persist($payment);

        $deal->setPayment($payment);
        $this->entityManager->persist($deal);

        $this->entityManager->flush();

        $encoded_token = $this->userManager->createInvitationUserToken($email2, $deal->getId(), $dealAgent2->getId());

        return $encoded_token;
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param User|null $user
     */
    private function assignRoleToTestUser(string $role_name, User $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var \Application\Entity\Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login, string $password = 'qwerty')
    {
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}