<?php

namespace ModuleAuthV2Test\Controller;

use ModuleAuthV2\Controller\PasswordResetController;
use Core\Form\OperationConfirmForm;
use Core\Service\OperationConfirmManager;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use ModuleCode\Service\CodeManager;
use CoreTest\ViewVarsTrait;

class PasswordResetControllerWithCodeConfirmTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    private $main_project_url;

    /**
     * @var string
     */
    private $user_email;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var CodeManager
     */
    private $codeManager;

    /**
     * $sessionContainerManager
     * @var \Core\Service\SessionContainerManager
     */
    private $sessionContainerManager;


    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $this->resetConfigParameters();

        $serviceManager = $this->getApplicationServiceLocator();

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->codeManager = $serviceManager->get(CodeManager::class);
        $this->sessionContainerManager = $serviceManager->get(\Core\Service\SessionContainerManager::class);

        $config = $this->getApplicationConfig();

        $this->user_email = $config['tests']['user_email'];
        $this->user_login = $config['tests']['user_login'];
        $this->main_project_url = $config['main_project_url'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_password_reset
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['multifactor_password_reset'] = true; // включаем подтверждение через SMS
        $config['email_providers']['message_send_simulation'] = true;
        $config['sms_providers']['message_send_simulation'] = true;
        $config['tokens']['password_resetting_token_request_period'] = 0;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /**
     * Валидный Email и Csrf в Post для смены пароля с подтверждением через SMS
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testResetPasswordActionWithValidPost()
    {
        // Get Email object
        $email = $this->userManager->selectEmailByEmailValue($this->user_email);

        // If Email is not verified, verify
        if( !$email->getIsVerified() ) {
            $email->setIsVerified(true);
            $this->entityManager->flush();
        }

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'email' => $this->user_email,
            'csrf'  => $csrf->getValue(),
        ];

        $this->dispatch('/forgot/password/reset', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-reset');
        $this->assertEquals($this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(PasswordResetController::CODE_OPERATION_TYPE_NAME));
        // Что отдается в шаблон
        $this->assertArrayHasKey('operationConfirmForm', $view_vars);
        $this->assertInstanceOf(OperationConfirmForm::class, $view_vars['operationConfirmForm']);
        // Возвращается из шаблона
        $this->assertQuery('#operation-code-confirm-form');
    }

    /**
     * For Ajax
     * Валидный Email и Csrf в Post для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testResetPasswordActionWithValidPostForAjax()
    {
        // Get Email object
        $email = $this->userManager->selectEmailByEmailValue($this->user_email);
        // If Email is not verified, verify
        if( !$email->getIsVerified() ) {
            $email->setIsVerified(true);
            $this->entityManager->flush();
        }

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'email' => $this->user_email,
            'csrf'  => $csrf->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/reset', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/password-reset');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertEquals($this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(PasswordResetController::CODE_OPERATION_TYPE_NAME));

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(OperationConfirmManager::SUCCESS_CONFIRMATION_CODE_SENT, $data['message']);
    }

    /**
     * For Ajax
     * По GET отдает CSRF токен для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeActionByGetForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        $this->assertEquals($this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(PasswordResetController::CODE_OPERATION_TYPE_NAME));

        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/reset/confirm', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/reset-confirm');
        $this->assertControllerName(PasswordResetController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Подтверждение кода, который есть в БД
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeActionWithValidCode()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        $this->assertEquals($this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(PasswordResetController::CODE_OPERATION_TYPE_NAME));

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/reset/confirm', 'POST', $postData);

        try {
            $this->sessionContainerManager
                ->getLoginByOperationType(PasswordResetController::CODE_OPERATION_TYPE_NAME);
        }
        catch (\Throwable $t) {
            $this->assertEquals('Initiator with key PASSWORD_RESET_CONFIRMATION not found', $t->getMessage());
        }

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/reset-confirm');
        $this->assertControllerName(PasswordResetController::class);
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Подтверждение кода, который есть в БД для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeActionWithValidCodeForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        $this->assertEquals($this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(PasswordResetController::CODE_OPERATION_TYPE_NAME));

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/reset/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        try {
            $this->sessionContainerManager
                ->getLoginByOperationType(PasswordResetController::CODE_OPERATION_TYPE_NAME);
        }
        catch (\Throwable $t) {
            $this->assertEquals('Initiator with key PASSWORD_RESET_CONFIRMATION not found', $t->getMessage());
        }
        $this->assertMatchedRouteName('forgot/reset-confirm');
        $this->assertControllerName(PasswordResetController::class);

        $this->assertModuleName('moduleauth');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(PasswordResetController::SUCCESS_TOKEN_SENT, $data['message']);
    }

    /**
     * Попытка подтверждение кода, которого нет в БД
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeActionWithInvalidCode()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => '333333', // Такого кода нет в базе
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/reset/confirm', 'POST', $postData);

        $this->assertEquals($this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(PasswordResetController::CODE_OPERATION_TYPE_NAME));

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/reset-confirm');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Попытка подтверждение кода, которого нет в БД для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeActionWithInvalidCodeForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => '333333', // Такого кода нет в базе
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/reset/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals($this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(PasswordResetController::CODE_OPERATION_TYPE_NAME));


        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/reset-confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid code entered', $data['message']);
    }

    /**
     * Попытка подтверждение кода для пользователя, которого нет в БД
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeActionWithInvalidLogin()
    {
        $confirmation_code = '445566';
        // Подготовка данных. Записываем в сессию логин несуществубщего пользователя
        $this->dataPrepare($confirmation_code, 'not_exists_login');

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST и ожидаем Exception
        $this->dispatch('/forgot/password/reset/confirm', 'POST', $postData);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertEquals(null, $this->baseAuthManager->getIdentity());
        $this->assertMatchedRouteName('forgot/reset-confirm');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Попытка подтверждение кода для пользователя, которого нет в БД Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeActionWithInvalidLoginForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных. Записываем в сессию логин несуществубщего пользователя
        $this->dataPrepare($confirmation_code, 'not_exists_login');

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/reset/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals(null, $this->baseAuthManager->getIdentity());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/reset-confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Process initiator user not found', $data['message']);
    }

    /**
     * Повторная отправка кода
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendAction()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Посылаем GET запрос
        $this->dispatch('/forgot/password/reset/code-resend');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/reset-code-resend');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertRedirectTo('/forgot/password/reset/confirm');
    }

    /**
     * For Ajax
     * Повторная отправка кода для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionForAjax()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/reset/code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/reset-code-resend');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(OperationConfirmManager::SUCCESS_NEW_CONFIRMATION_CODE_SENT, $data['message']);
    }

    /**
     * Повторная отправка кода
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithNoLogin()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, null);

        // Посылаем GET запрос
        $this->dispatch('/forgot/password/reset/code-resend');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/reset-code-resend');
        $this->assertControllerName(PasswordResetController::class);
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Повторная отправка кода c "нулевым" логином в сессии для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithNoLoginForAjax()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, null);

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/reset/code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);


        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/reset-code-resend');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Process initiator user not found', $data['message']);
    }

    /**
     * Повторная отправка кода при неистекшим периодом ['tokens']['confirmation_code_request_period']
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithTimePeriodNotPassed()
    {
        $confirmation_code = "888999";
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Посылаем GET запрос
        $this->dispatch('/forgot/password/reset/code-resend');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/reset-code-resend');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Повторная отправка кода при неистекшим периодом ['tokens']['confirmation_code_request_period'] для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithTimePeriodNotPassedForAjax()
    {
        $confirmation_code = "888999";
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/reset/code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/reset-code-resend');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('User not allowed request code', $data['message']);
    }


    /**
     * @param $confirmation_code
     * @param $user_login
     */
    private function dataPrepare($confirmation_code, $user_login)
    {
        if($confirmation_code) {
            $code_operation_type_name = PasswordResetController::CODE_OPERATION_TYPE_NAME;
            // Prepare objects
            // Get User by login
            $user = $this->userManager->selectUserByLogin($this->user_login);
            // Get CodeOperationType by name
            $codeOperationType = $this->codeManager->getCodeOperationTypeByName($code_operation_type_name);
            // Save code with value $confirmation_code to DB
            $this->codeManager->saveCode($user, $codeOperationType, $confirmation_code);
        }
        // Session init
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        // Session start
        $sessionManager->start();
        // Set session vars
        $this->setSessionVars($user_login);
    }

    /**
     * @param $login
     */
    private function setSessionVars($login)
    {
        $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
            PasswordResetController::CODE_OPERATION_TYPE_NAME,
            $login
        );
    }
}
