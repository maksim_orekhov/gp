<?php

namespace ModuleAuthV2Test\Controller;

use ModuleAuthV2\Form\LoginForm;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use ModuleAuthV2\Controller\AuthController;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;

class AuthControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    private $user_login;
    private $user_password;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;


    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $this->resetConfigParameters();

        $serviceManager = $this->getApplicationServiceLocator();

        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);

        $config = $this->getApplicationConfig();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['multifactor_authorization'] = false;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Проверка доступности страницы и наличия формы
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionCanBeAccessed()
    {
        $this->dispatch('/login');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(AuthController::class);
        $this->assertMatchedRouteName('login');
        // Что отдается в шаблон
        $this->assertArrayHasKey('loginForm', $view_vars);
        $this->assertInstanceOf(LoginForm::class, $view_vars['loginForm']);
        // Возвращается из шаблона
        $this->assertQuery('#login-form');
    }

    /**
     * For Ajax
     * Проверка доступности страницы и json ответа для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionCanBeAccessedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/login', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(AuthController::class);
        $this->assertControllerClass('AuthController');
        $this->assertMatchedRouteName('login');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Простая авторизация
     * Valid Post with multifactor_authorization = false
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSimpleLoginActionWithValidPost()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'login'         => $this->user_login,
            'password'      => $this->user_password,
            'remember_me'   => 0,
            'csrf'          => $csrf->getValue(),
        ];
        // Посылаем форму методом POST
        $this->dispatch('/login', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());
        $this->assertRedirectTo('/profile');
    }

    /**
     * For Ajax
     * Простая авторизация через Ajax
     * Valid Post with multifactor_authorization = false
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSimpleLoginActionWithValidPostForAjax()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'login'         => $this->user_login,
            'password'      => $this->user_password,
            'remember_me'   => 0,
            'csrf'          => $csrf->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/login', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->assertModuleName('moduleauth');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals($this->user_login, $data['login']);
    }

    /**
     * Простая авторизация с заведомо неверными данными (несуществующий логин)
     * Valid Post with multifactor_authorization = false
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSimpleLoginActionWithInvalidLogin()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'login'         => 'not_exists_login',
            'password'      => $this->user_password,
            'remember_me'   => 0,
            'csrf'          => $csrf->getValue(),
        ];

        // Посылаем форму методом POST
        $this->dispatch('/login', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('message', $view_vars);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertEquals(null, $this->baseAuthManager->getIdentity());
        $this->assertMatchedRouteName('login');
    }

    /**
     * For Ajax
     * Простая авторизация с заведомо неверными данными (несуществующий логин) для Ajax
     * Valid Post with multifactor_authorization = false
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSimpleLoginActionWithBadCredentialsInPostForAjax()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'login'         => 'not_exists_login',
            'password'      => $this->user_password,
            'remember_me'   => 0,
            'csrf'          => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/login', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals(null, $this->baseAuthManager->getIdentity());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('User with such login not found', $data['message']);
    }

    /**
     * Попытка логина с неверными Post данными (заведомо неверный логин)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionWithBadCredentialsInPost()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'login'         => 'notvalid',
            'password'      => $this->user_password,
            'remember_me'   => 0,
            'csrf'          => $csrf->getValue()
        ];

        $this->dispatch('/login', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('message', $view_vars);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login');
    }

    /**
     * For Ajax
     * Попытка логина с неверными Post данными (заведомо неверный логин) для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionWithBadCredentialsInPostForAjax()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'login'         => 'notvalid',
            'password'      => $this->user_password,
            'remember_me'   => 0,
            'csrf'          => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/login', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('User with such login not found', $data['message']);
    }

    /**
     * Logout тест. Предварительно залогиниваем пользователя напрямую через authManager->login.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLogoutAction()
    {
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/logout');

        $this->assertEquals(null, $this->baseAuthManager->getIdentity());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('logout');
        $this->assertControllerName(AuthController::class);
        $this->assertRedirectTo('/login');
    }

    /**
     * For Ajax
     * Logout тест. Предварительно залогиниваем пользователя напрямую через authManager->login для Ajax.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLogoutActionForAjax()
    {
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);

        //$this->request = new Request();

        $isXmlHttpRequest = true;
        $this->dispatch('/logout', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals(null, $this->baseAuthManager->getIdentity());

        $this->assertModuleName('moduleauth');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Log out done', $data['message']);
    }
}