<?php

namespace ModuleAuthV2Test\Controller;

use ModuleAuthV2\Controller\PasswordResetController;
use ModuleAuthV2\Form\PasswordChangeForm;
use ModuleAuthV2\Form\PasswordResetForm;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use CoreTest\ViewVarsTrait;

class PasswordResetControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    private $main_project_url;

    /**
     * @var string
     */
    private $user_email;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Core\Adapter\TokenAdapter
     */
    private $tokenAdapter;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;


    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.

        parent::setUp();


        $configOverrides = [
            'multifactor_password_reset' => false
        ];

        // Add content of global.php and other to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    ArrayUtils::merge(
                        include __DIR__ . '/../../../../config/application.config.php',
                        include __DIR__ . '/../../../../config/autoload/global.php'
                    ),
                    include __DIR__ . '/../../../../config/autoload/development.local.php'
                ),
                include __DIR__ . '/../../../../config/autoload/local.php'
            ),
            $configOverrides
        ));

        $this->resetConfigParameters();

        $serviceManager = $this->getApplicationServiceLocator();

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->tokenAdapter = $serviceManager->get(\Core\Adapter\TokenAdapter::class);

        $config = $this->getApplicationConfig();

        $this->user_email = $config['tests']['user_email'];
        $this->user_login = $config['tests']['user_login'];
        $this->main_project_url = $config['main_project_url'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_password_reset
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['multifactor_password_reset'] = false; // отключаем подтверждение через SMS
        $config['email_providers']['message_send_simulation'] = true;
        $config['sms_providers']['message_send_simulation'] = true;
        $config['tokens']['password_resetting_token_request_period'] = 0;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /**
     * Проверка доступности страницы
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testResetPasswordActionCanBeAccessed()
    {
        $this->dispatch('/forgot/password/reset');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-reset');
        // Что отдается в шаблон
        $this->assertArrayHasKey('passwordResetForm', $view_vars);
        $this->assertInstanceOf(PasswordResetForm::class, $view_vars['passwordResetForm']);
        // Возвращается из шаблона
        $this->assertQuery('#password-reset-form');
    }

    /**
     * For Ajax
     * Проверка доступности страницы для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testResetPasswordActionCanBeAccessedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/reset', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-reset');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Валидный Email и Csrf в Post
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testResetPasswordActionWithValidPost()
    {
        // Get Email object
        $email = $this->userManager->selectEmailByEmailValue($this->user_email);

        // If Email is not verified, verify
        if( !$email->getIsVerified() ) {
            $email->setIsVerified(true);
            $this->entityManager->flush();
        }

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'email' => $this->user_email,
            'csrf'  => $csrf->getValue(),
        ];

        $this->dispatch('/forgot/password/reset', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/password-reset');
        $this->assertControllerName(PasswordResetController::class);
        #$this->assertQuery('.container');

        $user = $this->userManager->getUserByLoginOrEmail($this->user_email);

        $this->assertNotNull($user->getPasswordResetToken());
        $this->assertNotNull($user->getPasswordResetTokenCreationDate());
    }

    /**
     * For Ajax
     * Валидный Email и Csrf в Post для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testResetPasswordActionWithValidPostForAjax()
    {
        // Get Email object
        $email = $this->userManager->selectEmailByEmailValue($this->user_email);
        // If Email is not verified, verify
        if( !$email->getIsVerified() ) {
            $email->setIsVerified(true);
            $this->entityManager->flush();
        }

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'email' => $this->user_email,
            'csrf'  => $csrf->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/reset', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/password-reset');
        $this->assertControllerName(PasswordResetController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(PasswordResetController::SUCCESS_TOKEN_SENT, $data['message']);
    }

    /**
     * Попытка отправить заведомо неверный email
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testResetPasswordActionWithInvalidEmail()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'email' => 'invalid_email@test.net',
            'csrf'  => $csrf->getValue(),
        ];

        $this->dispatch('/forgot/password/reset', 'POST', $postData);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/password-reset');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Попытка отправить заведомо неверный email для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testResetPasswordActionWithInvalidEmailForAjax()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'email' => 'invalid_email@test.net',
            'csrf'  => $csrf->getValue(),
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/reset', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Can not find user with this email', $data['message']);
    }

    /**
     * Попытка отправить заведомо неверный csrf
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testResetPasswordActionWithInvalidCsrf()
    {
        // Post data
        $postData = [
            'email' => $this->user_email,
            'csrf'  => 'e2ec1b3b1ab7407f4cdfb84458addf50-e172c96408bc89412eee5c60a9b71af3',
        ];

        $this->dispatch('/forgot/password/reset', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('forgot/password-reset');
    }

    /**
     * For Ajax
     * Попытка отправить заведомо неверный csrf для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testResetPasswordActionWithInvalidCsrfForAjax()
    {
        // Post data
        $postData = [
            'email' => $this->user_email,
            'csrf'  => 'e2ec1b3b1ab7407f4cdfb84458addf50-e172c96408bc89412eee5c60a9b71af3',
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/reset', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid form data', $data['message']);
        $this->assertArrayHasKey('csrf', $data['data']);
    }

    /**
     * SetPasswordAction без зашифрованного токена отправляент а страницу ошибки
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionCanNotBeAccessedWithoutToken()
    {
        $this->dispatch('/forgot/password/set');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * SetPasswordAction без зашифрованного токена отправляент а страницу ошибки для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionCanNotBeAccessedWithoutTokenForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/forgot/password/set', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * SetPasswordAction with valid Get
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithValidGet()
    {
        // Simple token
        $simple_token = '333333ab';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        $this->dispatch('/forgot/password/set?token='.$encodedToken);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');
        // Что отдается в шаблон
        $this->assertArrayHasKey('passwordChangeForm', $view_vars);
        $this->assertInstanceOf(PasswordChangeForm::class, $view_vars['passwordChangeForm']);
        // Возвращается из шаблона
        $this->assertQuery('#password-change-form');
    }

    /**
     * For Ajax
     * SetPasswordAction with valid Get для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithValidGetForAjax()
    {
        // Simple token
        $simple_token = '33333333';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
    }

    /**
     * Попытка передать в SetPasswordAction неверный simple token (в составе зашифрованного токена)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidSimpleToken()
    {
        // Simple token
        $simple_token = '33333333';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, '33333332');

        $this->dispatch('/forgot/password/set?token='.$encodedToken);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Попытка передать в SetPasswordAction неверный simple token (в составе зашифрованного токена) для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidSimpleTokenForAjax()
    {
        // Simple token
        $simple_token = '33333333';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, '33333332');

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('User with given token not found', $data['message']);
    }

    /**
     * Попытка передать в SetPasswordAction неверный зашифрованный токен
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidEncodedToken()
    {
        // Simple token
        $simple_token = '33333333';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        $this->dispatch('/forgot/password/set?token='.$encodedToken.'12');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Попытка передать в SetPasswordAction неверный зашифрованный токен для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidEncodedTokenForAjax()
    {
        // Simple token
        $simple_token = '33333333';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken.'128', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid encoded token', $data['message']);
    }

    /**
     * SetPasswordAction с правильными Post данными
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithValidPost()
    {
        // Simple token
        $simple_token = '44444444';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test_test',
            'csrf'                  => $csrf->getValue()
        ];

        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');
        $this->assertQuery('.container');

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_new_pass = $user->getPassword();
        // Password has changed
        $this->assertNotEquals($user_old_pass, $user_new_pass);
    }

    /**
     * For Ajax
     * SetPasswordAction с правильными Post данными для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithValidPostForAjax()
    {
        // Simple token
        $simple_token = '44444444';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test_ajax',
            'confirm_new_password'  => 'test_test_ajax',
            'csrf'                  => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PasswordResetController::class);
        $this->assertMatchedRouteName('forgot/password-set');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('Password changed', $data['message']);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_new_pass = $user->getPassword();
        // Password has changed
        $this->assertNotEquals($user_old_pass, $user_new_pass);
    }

    /**
     * Передаем в SetPasswordAction неверный Csrf
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidCsrf()
    {
        // Simple token
        $simple_token = '44444444';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test_test',
            'csrf'                  => '7779ffebfc787475e9c6fcc1f13191bb-2738e4964056a4b7b2800350670d31e8'
        ];

        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertQuery('.container #password-change-form');

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }

    /**
     * For Ajax
     * Передаем в SetPasswordAction неверный Csrf для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidCsrfForAjax()
    {
        // Simple token
        $simple_token = '44444444';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test_test',
            'csrf'                  => '7779ffebfc787475e9c6fcc1f13191bb-2738e4964056a4b7b2800350670d31e8'
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid form data', $data['message']);
        $this->assertArrayHasKey('csrf', $data['data']);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }

    /**
     * Передаем в SetPasswordAction неверные Post данные (пароли не совпадают)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidPassword()
    {
        // Simple token
        $simple_token = '44444444';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test',
            'csrf'                  => $csrf->getValue()
        ];

        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertQuery('.container #password-change-form');

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }

    /**
     * For Ajax
     * Передаем в SetPasswordAction неверные Post данные (пароли не совпадают) для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithInvalidPasswordForAjax()
    {
        // Simple token
        $simple_token = '44444444';

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $user->setPasswordResetTokenCreationDate(new \DateTime());
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test_ajax',
            'confirm_new_password'  => 'test_test',
            'csrf'                  => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid form data', $data['message']);
        $this->assertArrayHasKey('confirm_new_password', $data['data']);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }

    /**
     * Попытка смены пароля с просроченным simple token
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithExpiredToken()
    {
        // Simple token
        $simple_token = '44444444';

        $config = $this->getApplication()
            ->getServiceManager()
            ->get('Configuration');
        $token_expiration_time = $config['tokens']['password_resetting_token_expiration_time'];

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $currentDate = new \DateTime();
        $user->setPasswordResetTokenCreationDate($currentDate->modify('-' . ($token_expiration_time*2) . ' seconds'));
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test',
            'csrf'                  => $csrf->getValue()
        ];

        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertQuery('.container');

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }

    /**
     * For Ajax
     * Попытка смены пароля с просроченным simple token для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testSetPasswordActionWithExpiredTokenForAjax()
    {
        // Simple token
        $simple_token = '44444444';

        $config = $this->getApplication()
            ->getServiceManager()
            ->get('Configuration');
        $token_expiration_time = $config['tokens']['password_resetting_token_expiration_time'];

        // Get Email object
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_old_pass = $user->getPassword();
        $user->setPasswordResetToken($simple_token);
        $currentDate = new \DateTime();
        $user->setPasswordResetTokenCreationDate($currentDate->modify('-' . ($token_expiration_time*2) . ' seconds'));
        // Save it to DB
        $this->entityManager->flush();

        // Encoded token
        $encodedToken = $this->tokenAdapter->createToken($this->user_email, $simple_token);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'new_password'          => 'test_test',
            'confirm_new_password'  => 'test',
            'csrf'                  => $csrf->getValue()
        ];

        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/forgot/password/set?token='.$encodedToken, 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Simple token has expired', $data['message']);

        $user = $this->userManager->selectUserByLogin($this->user_login);
        $user_new_pass = $user->getPassword();
        // Password has not changed
        $this->assertEquals($user_old_pass, $user_new_pass);
    }
}
