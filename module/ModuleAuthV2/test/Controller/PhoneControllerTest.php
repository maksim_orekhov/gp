<?php
namespace ModuleAuthV2Test\Controller;

use Core\Service\Base\BasePhoneManager;
use Core\Service\CodeOperationInterface;
use ModuleAuthV2\Controller\PhoneController;
use Core\Form\OperationConfirmForm;
use Core\Form\PhoneEditForm;
use ModuleCode\Entity\CodeOperationType;
use ModuleCode\Service\CodeManager;
use Application\Entity\User;
use Core\Service\OperationConfirmManager;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;

class PhoneControllerTest extends AbstractHttpControllerTestCase implements CodeOperationInterface
{
    use ViewVarsTrait;

    protected $traceError = true;

    private $user_login;
    private $user_password;
    private $user_phone;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Core\Service\Auth\AuthService
     */
    private $authService;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var BasePhoneManager
     */
    private $basePhoneManager;

    /**
     * @var CodeManager
     */
    private $codeManager;

    /**
     * $sessionContainerManager
     * @var \Core\Service\SessionContainerManager
     */
    private $sessionContainerManager;


    public function setUp()
    {
        parent::setUp();

        // Add content of global.php and other to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                ArrayUtils::merge(
                    include __DIR__ . '/../../../../config/application.config.php',
                    include __DIR__ . '/../../../../config/autoload/global.php'
                ),
                include __DIR__ . '/../../../../config/autoload/development.local.php'
            ),
            include __DIR__ . '/../../../../config/autoload/local.php'
        ));

        $this->resetConfigParameters();

        $config = $this->getApplicationConfig();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];
        $this->user_phone = $config['tests']['user_phone'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Core\Service\Auth\AuthService::class);
        $this->basePhoneManager = $serviceManager->get(BasePhoneManager::class);
        $this->codeManager = $serviceManager->get(CodeManager::class);
        $this->sessionContainerManager = $serviceManager->get(\Core\Service\SessionContainerManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_password_reset
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $config['sms_providers']['message_send_simulation'] = true;
        $config['tokens']['password_resetting_token_request_period'] = 0;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Недоступностсь неавторизованному пользователю без дополнительных данных
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionRedirectForUnauthorized()
    {
        $this->dispatch('/phone/confirm', 'GET');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Получение csrf для Ajax и ничего больше
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionCanBeAccessedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/confirm', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PhoneController::class);
        $this->assertMatchedRouteName('phone/confirm');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Авторизованный пользователь с верифицированным телефоном не может попасть на эту форму
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionCanNotBeAccessedByAuthorizedUserWithVerifiedPhone()
    {
        // В базе телефон, добавленный фикстурами, может оказаться не верифицированным.
        // Проверяем и, если так, то верифицируем.
        $phone = $this->basePhoneManager->getPhoneByNumber($this->user_phone);
        if($phone && $phone->getIsVerified() == 0) {
            $phone->setIsVerified(1);
            $this->entityManager->flush();
        }
        // Log in
        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);

        $this->dispatch('/phone/confirm', 'GET');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PhoneController::class);
        $this->assertMatchedRouteName('phone/confirm');
        #$this->assertQuery('.container');
    }

    /**
     * Авторизованный пользователь с неподтвержденным телефоном может попасть на страницу и увидеть форму.
     * Также предполагается, что инициирован процесс подтверждения телефона,
     * а значит в перерменной сесии $this->initiator находится логин пользователя
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionCanBeAccessedByAuthorizedUser()
    {
        // В базе телефон, добавленный фикстурами, может оказаться верифицированным.
        // Проверяем и, если так, то снимаем верификацию.
        $phone = $this->basePhoneManager->getPhoneByNumber($this->user_phone);
        if($phone && $phone->getIsVerified() == 1) {
            $phone->setIsVerified(0);
            $this->entityManager->flush();
        }

        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        // Session start
        $sessionManager->start();

        // Set initiator to session
        $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
            self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION,
            $this->user_login
        );

        // Log in
        $this->baseAuthManager->login($this->user_login, $this->user_password, 0);

        $this->dispatch('/phone/confirm', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PhoneController::class);
        $this->assertMatchedRouteName('phone/confirm');
        // Что отдается в шаблон
        $this->assertArrayHasKey('form', $view_vars);
        $this->assertInstanceOf(OperationConfirmForm::class, $view_vars['form']);
        $this->assertArrayHasKey('resend_url', $view_vars);
        $this->assertArrayHasKey('server_error_messages', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#operation-code-confirm-form');
    }

    /**
     * Неавторизованный пользователь с неподтвержденным телефоном может попасть на страницу и увидеть форму.
     * Также предполагается, что инициирован процесс подтверждения телефона,
     * а значит в перерменной сесии $this->initiator находится логин пользователя
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionCanBeAccessedByUnauthorizedUser()
    {
        // В базе телефон, добавленный фикстурами, может оказаться верифицированным.
        // Проверяем и, если так, то снимаем верификацию.
        $phone = $this->basePhoneManager->getPhoneByNumber($this->user_phone);
        if($phone && $phone->getIsVerified() == 1) {
            $phone->setIsVerified(0);
            $this->entityManager->flush();
        }

        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        // Session start
        $sessionManager->start();

        // Set initiator to session
        $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
            self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION,
            $this->user_login
        );

        $this->dispatch('/phone/confirm', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PhoneController::class);
        $this->assertMatchedRouteName('phone/confirm');
        // Что отдается в шаблон
        $this->assertArrayHasKey('form', $view_vars);
        $this->assertInstanceOf(OperationConfirmForm::class, $view_vars['form']);
        $this->assertArrayHasKey('resend_url', $view_vars);
        $this->assertArrayHasKey('server_error_messages', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#operation-code-confirm-form');
    }

    /**
     * For Ajax
     * По GET отдает CSRF токен для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionByGetForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION)
        );

        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/confirm', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/confirm');
        $this->assertControllerName(PhoneController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Подтверждение кода, который есть в БД
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionWithValidCode()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION)
        );

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        // Посылаем форму методом POST
        $this->dispatch('/phone/confirm', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/confirm');
        $this->assertControllerName(PhoneController::class);
        $this->assertRedirectTo('/phone/confirm/success');
        $this->assertFalse(
            $this->sessionContainerManager
                ->isProcessInitiatorExists(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION)
        );
    }

    /**
     * For Ajax
     * Подтверждение кода, который есть в БД для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionWithValidCodeForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION)
        );

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertFalse(
            $this->sessionContainerManager
                ->isProcessInitiatorExists(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION)
        );

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/confirm');
        $this->assertControllerName(PhoneController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(PhoneController::SUCCESS_PHONE_CONFIRMED, $data['message']);
    }

    /**
     * Попытка подтверждение кода, которого нет в БД
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionWithInvalidCode()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => '333333', // Такого кода нет в базе
            'csrf' => $csrf->getValue(),
        ];
        // Посылаем форму методом POST
        $this->dispatch('/phone/confirm', 'POST', $postData);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION)
        );

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/confirm');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Попытка подтверждение кода, которого нет в БД для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionWithInvalidCodeForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => '333333', // Такого кода нет в базе
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION)
        );

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid code entered', $data['message']);
    }

    /**
     * Попытка подтверждение кода для пользователя, которого нет в БД
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionWithInvalidLogin()
    {
        $confirmation_code = '445566';
        // Подготовка данных. Записываем в сессию логин несуществубщего пользователя
        $this->dataPrepare($confirmation_code, 'not_exists_login', self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        // Посылаем форму методом POST и ожидаем Exception
        $this->dispatch('/phone/confirm', 'POST', $postData);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertEquals(null, $this->baseAuthManager->getIdentity());
        $this->assertMatchedRouteName('phone/confirm');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Попытка подтверждение кода для пользователя, которого нет в БД Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmActionWithInvalidLoginForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных. Записываем в сессию логин несуществубщего пользователя
        $this->dataPrepare($confirmation_code, 'not_exists_login', self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals(null, $this->baseAuthManager->getIdentity());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Process initiator user not found', $data['message']);
    }

    /**
     * Повторная отправка кода
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendAction()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        // Посылаем GET запрос
        $this->dispatch('/phone/confirm/code-resend');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/code-resend');
        $this->assertControllerName(PhoneController::class);
        $this->assertControllerClass('PhoneController');
        $this->assertRedirectTo('/phone/confirm');
    }

    /**
     * For Ajax
     * Повторная отправка кода для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionForAjax()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION, false);

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/confirm/code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/code-resend');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(OperationConfirmManager::SUCCESS_NEW_CONFIRMATION_CODE_SENT, $data['message']);
    }

    /**
     * Повторная отправка кода
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithNoLogin()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, null, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        // Посылаем GET запрос
        $this->dispatch('/phone/confirm/code-resend');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/code-resend');
        $this->assertControllerName(PhoneController::class);
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Повторная отправка кода c "нулевым" логином в сессии для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithNoLoginForAjax()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, null, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/confirm/code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);


        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/code-resend');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Process initiator user not found', $data['message']);
    }

    /**
     * Повторная отправка кода при неистекшим периодом ['tokens']['confirmation_code_request_period']
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithTimePeriodNotPassed()
    {
        $confirmation_code = "888999";
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        // Посылаем GET запрос
        $this->dispatch('/phone/confirm/code-resend');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/code-resend');
        $this->assertControllerName(PhoneController::class);
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Повторная отправка кода при неистекшим периодом ['tokens']['confirmation_code_request_period'] для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithTimePeriodNotPassedForAjax()
    {
        $confirmation_code = "888999";
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION);

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/confirm/code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/code-resend');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('User not allowed request code', $data['message']);
    }

    /**
     * Неавторизованный пользователь не может редактировать номер телефона
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testEditActionCanNotBeAccessedByNonAuthorised()
    {
        $this->dispatch('/phone/edit', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertRedirect();
        $this->assertRedirectTo('/login?redirectUrl=/phone/edit');
    }

    /**
     * For Ajax
     * Неавторизованный пользователь не может редактировать номер телефона для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testEditActionCanNotBeAccessedByNonAuthorisedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/edit', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PhoneController::class);
        $this->assertMatchedRouteName('phone/edit');
        // @TODO Возврат 'Access is denied' или 'Authorization required' для аякса
        #$this->assertEquals('Access is denied', $data['message']);
        $this->assertEquals(null, $data);
    }

    /**
     * Авторизованный пользователь может изменять номер своего телефона
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testEditActionCanBeAccessedByAuthorizedUser()
    {
        // Залогиниваем тестового юзера
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $this->dispatch('/phone/edit', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PhoneController::class);
        $this->assertMatchedRouteName('phone/edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('formPhone', $view_vars);
        $this->assertInstanceOf(PhoneEditForm::class, $view_vars['formPhone']);
        $this->assertArrayHasKey('server_error_messages', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#phone-edit-form');
    }

    /**
     * For Ajax
     * Авторизованный пользователь может изменять номер своего телефона для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testEditActionCanBeAccessedByAuthorizedUserForAjax()
    {
        // Залогиниваем тестового юзера
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        $isXmlHttpRequest = true;
        $this->dispatch('/phone/edit', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit');
        $this->assertControllerName(PhoneController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Изменение номера телефона
     * Отправляем правильный POST
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testEditActionWithValidPost()
    {
        $new_phone = '79991112244';
        // Залогиниваем тестового юзера
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'current_phone' => $this->user_phone,
            'new_phone' => $new_phone,
            'csrf' => $csrf->getValue(),
        ];

        $this->dispatch('/phone/edit', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PhoneController::class);
        $this->assertMatchedRouteName('phone/edit');
        // Что отдается в шаблон
        $this->assertArrayHasKey('formPhone', $view_vars);
        $this->assertInstanceOf(PhoneEditForm::class, $view_vars['formPhone']);
        $this->assertArrayHasKey('server_error_messages', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#phone-edit-form');

        $user=$this->userManager->selectUserByLogin($this->user_login);
        // Тут телефон еще не изменяется
        $this->assertNotEquals($user->getPhone()->getPhoneNumber(), $new_phone);
        // Проверяем записался ли инициатор в сессию
//        $this->assertEquals($this->user_login,
//            $this->sessionContainerManager
//                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE));
//        // Проверяем записался ли новый номер телефона в сессиию
//        $this->assertEquals($new_phone, $this->sessionContainerManager->getSessionVar('new_phone_number'));
    }

    /**
     * For Ajax
     * Изменение номера телефона (правильный POST) для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testEditActionWithValidPostForAjax()
    {
        $new_phone = '79991112244';

        // Залогиниваем тестового юзера
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'current_phone' => $this->user_phone,
            'new_phone' => '+'.$new_phone,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        $this->dispatch('/phone/edit', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit');
        $this->assertControllerName(PhoneController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(OperationConfirmManager::SUCCESS_CONFIRMATION_CODE_SENT_BY_EMAIL, $data['message']);

        $user=$this->userManager->selectUserByLogin($this->user_login);
        // Тут телефон еще не изменяется
        $this->assertNotEquals($user->getPhone()->getPhoneNumber(), $new_phone);

        // Проверяем записался ли инициатор в сессию
        $this->assertEquals($this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE));
        // Проверяем записался ли новый номер телефона в сессиию
        $this->assertEquals($new_phone, $this->sessionContainerManager->getSessionVar('new_phone_number'));
    }

    /**
     * For Ajax
     * Изменение номера телефона для Ajax
     * Отправляем POST с незарегистрированным в системе current_phone
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testEditActionWithNonexistentCurrentPhoneForAjax()
    {
        $current_phone = '79991112255';
        $new_phone = '79991112244';

        // Залогиниваем тестового юзера
        $this->login();
        // Проверяем
        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'current_phone' => '+'.$current_phone,
            'new_phone' => '+'.$new_phone,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        $this->dispatch('/phone/edit', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit');
        $this->assertControllerName(PhoneController::class);

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Введенный текущий телефон не соответствует телефону, указанному в профайле', $data['message']);

        $user=$this->userManager->selectUserByLogin($this->user_login);
        // Тут телефон еще не изменяется
        $this->assertNotEquals($user->getPhone()->getPhoneNumber(), $new_phone);

        // Проверяем, что новый номер телефона не записался в сессиию
        $this->assertEquals(null, $this->sessionContainerManager->getSessionVar('new_phone_number'));

        // Инициатор не должен записаться в сессию
        $this->assertFalse(
            $this->sessionContainerManager
                ->isProcessInitiatorExists(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );
    }

    /**
     * Авторизованный пользователь может попасть на страницу и увидеть форму.
     * Также предполагается, что инициирован процесс подтверждения телефона,
     * а значит в перерменной сесии $this->initiator находится логин пользователя
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationEditCodeActionCanBeAccessedByAuthorizedUser()
    {
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        // Session start
        $sessionManager->start();

        // Set initiator to session
        $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
            self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE,
            $this->user_login
        );

        // Залогиниваем тестового юзера
        $this->login();

        $this->dispatch('/phone/edit-confirm', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(PhoneController::class);
        $this->assertMatchedRouteName('phone/edit-confirm');
        // Что отдается в шаблон
        $this->assertArrayHasKey('form', $view_vars);
        $this->assertInstanceOf(OperationConfirmForm::class, $view_vars['form']);
        $this->assertArrayHasKey('resend_url', $view_vars);
        $this->assertArrayHasKey('server_error_messages', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#operation-code-confirm-form');
    }

    /**
     * For Ajax
     * По GET отдает CSRF токен для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testConfirmationEditCodeActionByGetForAjax()
    {
        $confirmation_code = '445566';
        // Залогиниваем тестового юзера
        $this->login();
        // Подготовка данных
        $this->dataPrepare(
            $confirmation_code,
            $this->user_login,
            self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE,
            true
        );

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/edit-confirm', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit-confirm');
        $this->assertControllerName(PhoneController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Подтверждение кода, который есть в БД
     * Предполагаем, что в сессию записан новый номер телефона(new_phone_number) и инициатор процесса
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testConfirmationEditCodeActionWithValidCode()
    {
        $confirmation_code = '445566';
        $new_phone = '79991112244';

        // Залогиниваем тестового юзера
        $this->login();
        // Подготовка данных
        $this->dataPrepare(
            $confirmation_code,
            $this->user_login,
            self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE,
            true);

        $this->sessionContainerManager->setSessionVar('new_phone_number', '+'.$new_phone);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        // Посылаем форму методом POST
        $this->dispatch('/phone/edit-confirm', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit-confirm');
        $this->assertControllerName(PhoneController::class);
        $this->assertRedirectTo('/phone/confirm');

        // Проверяем, что инициатор удален из сессии
        $this->assertFalse(
            $this->sessionContainerManager
                ->isProcessInitiatorExists(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );
        // Проверяем, что новый номер удален из сессии
        $this->assertEquals(null, $this->sessionContainerManager->getSessionVar('new_phone_number'));
    }

    /**
     * For Ajax
     * Подтверждение кода, который есть в БД для Ajax
     * Предполагаем, что в сессию записан новый номер телефона(new_phone_number) и инициатор процесса
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testConfirmationEditCodeActionWithValidCodeForAjax()
    {
        $confirmation_code = '445566';
        $new_phone = '79991112244';

        // Залогиниваем тестового юзера
        $this->login();
        // Подготовка данных
        $this->dataPrepare(
            $confirmation_code,
            $this->user_login,
            self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE,
            true);

        $this->sessionContainerManager->setSessionVar('new_phone_number', '+'.$new_phone);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/edit-confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertFalse(
            $this->sessionContainerManager
                ->isProcessInitiatorExists(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit-confirm');
        $this->assertControllerName(PhoneController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(PhoneController::SUCCESS_OPERATION_CONFIRMED, $data['message']);

        // Проверяем, что инициатор удален из сессии
        $this->assertFalse(
            $this->sessionContainerManager
                ->isProcessInitiatorExists(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );
        // Проверяем, что новый номер удален из сессии
        $this->assertEquals(null, $this->sessionContainerManager->getSessionVar('new_phone_number'));
    }

    /**
     * Попытка подтверждения кода, которого нет в БД
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationEditCodeActionWithInvalidCode()
    {
        $confirmation_code = '445566';
        $new_phone = '79991112244';

        // Залогиниваем тестового юзера
        $this->login();
        // Подготовка данных
        $this->dataPrepare(
            $confirmation_code,
            $this->user_login,
            self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE,
            true);

        $this->sessionContainerManager->setSessionVar('new_phone_number', '+'.$new_phone);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => '333333', // Такого кода нет в базе
            'csrf' => $csrf->getValue(),
        ];
        // Посылаем форму методом POST
        $this->dispatch('/phone/edit-confirm', 'POST', $postData);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit-confirm');
        #$this->assertQuery('.container');

        // Проверяем, что инициатор НЕ удален из сессии
        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );
        // Проверяем, что новый номер НЕ удален из сессии
        $this->assertEquals($new_phone, $this->sessionContainerManager->getSessionVar('new_phone_number'));
    }

    /**
     * For Ajax
     * Попытка подтверждение кода, которого нет в БД для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testConfirmationEditCodeActionWithInvalidCodeForAjax()
    {
        $confirmation_code = '445566';
        $new_phone = '79991112244';

        // Залогиниваем тестового юзера
        $this->login();
        // Подготовка данных
        $this->dataPrepare(
            $confirmation_code,
            $this->user_login,
            self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE,
            true);

        $this->sessionContainerManager->setSessionVar('new_phone_number', '+'.$new_phone);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => '333333', // Такого кода нет в базе
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/edit-confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit-confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid code entered', $data['message']);

        // Проверяем, что инициатор НЕ удален из сессии
        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );
        // Проверяем, что новый номер НЕ удален из сессии
        $this->assertEquals($new_phone, $this->sessionContainerManager->getSessionVar('new_phone_number'));
    }

    /**
     * Отправка повтрного кода подтверждения при смене телефона
     * Повторная отправка кода
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationEditCodeResendActionCanNotBeAccessedByUnauthorizedUser()
    {
        $new_phone = '79991112244';
        $confirmation_code = null;

        // Не залогиниваем тестового юзера!
        #$this->login();

        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE);

        $this->sessionContainerManager->setSessionVar('new_phone_number', '+'.$new_phone);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        // Посылаем GET запрос
        $this->dispatch('/phone/confirm/edit-code-resend');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit-code-resend');
        $this->assertControllerName(PhoneController::class);

        // Ожидаем, что сработает Rbac и перенаправит на страницу авторизации
        $this->assertRedirectTo('/login?redirectUrl=/phone/confirm/edit-code-resend');
    }

    /**
     * Отправка повтрного кода подтверждения при смене телефона
     * Повторная отправка кода
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationEditCodeResendAction()
    {
        $new_phone = '79991112244';
        $confirmation_code = null;

        // Залогиниваем тестового юзера
        $this->login();
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE);

        $this->sessionContainerManager->setSessionVar('new_phone_number', '+'.$new_phone);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        // Посылаем GET запрос
        $this->dispatch('/phone/confirm/edit-code-resend');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit-code-resend');
        $this->assertControllerName(PhoneController::class);
        $this->assertRedirectTo('/phone/edit-confirm');
    }

    /**
     * For Ajax
     * Повторная отправка кода для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationEditCodeResendActionForAjax()
    {
        $new_phone = '79991112244';
        $confirmation_code = null;

        // Залогиниваем тестового юзера
        $this->login();
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE);

        $this->sessionContainerManager->setSessionVar('new_phone_number', '+'.$new_phone);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/confirm/edit-code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit-code-resend');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(OperationConfirmManager::SUCCESS_NEW_CONFIRMATION_CODE_SENT, $data['message']);
    }

    /**
     * Повторная отправка кода при неистекшим периодом ['tokens']['confirmation_code_request_period']
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationEditCodeResendActionWithTimePeriodNotPassed()
    {
        $confirmation_code = "888999";
        $new_phone = '79991112244';

        // Залогиниваем тестового юзера
        $this->login();
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE);

        $this->sessionContainerManager->setSessionVar('new_phone_number', '+'.$new_phone);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        // Посылаем GET запрос
        $this->dispatch('/phone/confirm/edit-code-resend');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit-code-resend');
        $this->assertControllerName(PhoneController::class);
        #$this->assertQuery('.container');

        /** @var User $user */
        $user = $this->userManager->selectUserByLogin($this->user_login);
        /** @var CodeOperationType $operationType */
        $operationType = $this->entityManager->getRepository(CodeOperationType::class)
            ->findOneBy(array('name' => self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE));
        // Получаем количество кодов данного типа у пользователя
        $codes = $this->userManager->getCountUserCodesByOperationType($user, $operationType);
        // Проверяем, что второй код не добавился
        $this->assertEquals(1, $codes);
    }

    /**
     * For Ajax
     * Повторная отправка кода при неистекшим периодом ['tokens']['confirmation_code_request_period'] для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testConfirmationEditCodeResendActionWithTimePeriodNotPassedForAjax()
    {
        $confirmation_code = "888999";
        $new_phone = '79991112244';

        // Залогиниваем тестового юзера
        $this->login();
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login, self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE);

        $this->sessionContainerManager->setSessionVar('new_phone_number', '+'.$new_phone);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE)
        );

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/phone/confirm/edit-code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('phone/edit-code-resend');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('User not allowed request code', $data['message']);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }

    /**
     * @param $confirmation_code
     * @param $user_login
     * @param $code_operation_type_name
     * @param bool $isVerified
     */
    private function dataPrepare($confirmation_code, $user_login, $code_operation_type_name, $isVerified=false)
    {
        // Get User by login
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $phone = $user->getPhone();
        $phone->setIsVerified($isVerified);

        if($confirmation_code) {
            // Prepare objects
            // Get CodeOperationType by name
            $codeOperationType = $this->codeManager->getCodeOperationTypeByName($code_operation_type_name);
            // Save code with value $confirmation_code to DB
            $this->codeManager->saveCode($user, $codeOperationType, $confirmation_code);
        }
        // Session init
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        // Session start
        $sessionManager->start();
        // Set session vars
        $this->setSessionVars($user_login, $code_operation_type_name);
    }

    private function setSessionVars($login, $code_operation_type_name)
    {
        $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
            $code_operation_type_name,
            $login
        );
    }
}
