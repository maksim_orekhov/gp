<?php

namespace ModuleAuthV2Test\Controller;

use Core\Service\CodeOperationInterface;
use ModuleAuthV2\Form\LoginForm;
use Core\Form\OperationConfirmForm;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use ModuleAuthV2\Controller\AuthController;
use Zend\Form\Element;
use Zend\Session\SessionManager;
use Core\Service\OperationConfirmManager;
use ModuleCode\Entity\CodeOperationType;
use ModuleCode\Service\CodeManager;
use CoreTest\ViewVarsTrait;

class AuthControllerWithCodeConfirmTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    private $user_login;
    private $user_password;

    /**
     * @var \Core\Service\Base\BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var CodeManager
     */
    private $codeManager;

    /**
     * $sessionContainerManager
     * @var \Core\Service\SessionContainerManager
     */
    private $sessionContainerManager;

    /**
     * @var string
     */
    private $process_initiator_var_name;


    public function setUp()
    {
        parent::setUp();

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $this->resetConfigParameters();

        $serviceManager = $this->getApplicationServiceLocator();

        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
        $this->authService = $serviceManager->get(\Zend\Authentication\AuthenticationService::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->codeManager = $serviceManager->get(CodeManager::class);
        $this->sessionContainerManager = $serviceManager->get(\Core\Service\SessionContainerManager::class);

        $config = $this->getApplicationConfig();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');

        $this->process_initiator_var_name = strtolower(CodeOperationInterface::CODE_OPERATION_TYPE_NAME).'_initiator_login';

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();
        // Clear up codes
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $code_operation_type_name = CodeOperationInterface::CODE_OPERATION_TYPE_NAME;
        /** @var CodeOperationType $codeOperationType */
        $codeOperationType = $this->codeManager->getCodeOperationTypeByName($code_operation_type_name);
        $this->codeManager->removeAllUserCodesByCodeType($user, $codeOperationType);

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['multifactor_authorization'] = true;
        $config['email_providers']['message_send_simulation'] = true;
        $config['sms_providers']['message_send_simulation'] = true;
        $config['tokens']['max_number_of_specific_code_for_user'] = 20;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /**
     * Мультифакторная авторизация
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionWithValidPost()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'login'         => $this->user_login,
            'password'      => $this->user_password,
            'remember_me'   => 0,
            'csrf'          => $csrf->getValue(),
        ];
        // Посылаем форму методом POST
        $this->dispatch('/login', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(AuthController::class);
        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(CodeOperationInterface::CODE_OPERATION_TYPE_NAME)
        );

        // Что отдается в шаблон
        $this->assertArrayHasKey('operationConfirmForm', $view_vars);
        $this->assertInstanceOf(OperationConfirmForm::class, $view_vars['operationConfirmForm']);
        $this->assertArrayHasKey('resend_url', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#operation-code-confirm-form');
    }

    /**
     * For Ajax
     * Мультифакторная авторизация для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionWithValidPostForAjax()
    {
        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'login'         => $this->user_login,
            'password'      => $this->user_password,
            'remember_me'   => 0,
            'csrf'          => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/login', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertControllerName(AuthController::class);
        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(CodeOperationInterface::CODE_OPERATION_TYPE_NAME)
        );

        $this->assertModuleName('moduleauth');
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(OperationConfirmManager::SUCCESS_CONFIRMATION_CODE_SENT, $data['message']);
    }

    /**
     * For Ajax
     * По GET отдает CSRF токен для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionByGetForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(CodeOperationInterface::CODE_OPERATION_TYPE_NAME)
        );

        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/login/confirm', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/confirm');
        $this->assertControllerName(AuthController::class);

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Подтверждение кода, который есть в БД
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionWithValidCode()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(CodeOperationInterface::CODE_OPERATION_TYPE_NAME)
        );

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $this->dispatch('/login/confirm', 'POST', $postData);

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());
        $this->assertFalse(
            $this->sessionContainerManager
                ->isProcessInitiatorExists(CodeOperationInterface::CODE_OPERATION_TYPE_NAME)
        );
        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/confirm');
        $this->assertControllerName(AuthController::class);
        $this->assertControllerClass('AuthController');
        $this->assertRedirectTo('/');
    }

    /**
     * For Ajax
     * Подтверждение кода, который есть в БД для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionWithValidCodeForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(CodeOperationInterface::CODE_OPERATION_TYPE_NAME)
        );

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/login/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals($this->user_login, $this->baseAuthManager->getIdentity());
        $this->assertEquals(null, $this->sessionContainerManager->getSessionVar($this->process_initiator_var_name));

        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/confirm');
        $this->assertControllerName(AuthController::class);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals($this->user_login, $data['login']);
    }

    /**
     * Попытка подтверждение кода, которого нет в БД
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionWithInvalidCode()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => '333333', // Такого кода нет в базе
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $this->dispatch('/login/confirm', 'POST', $postData);

        $this->assertEquals(null, $this->baseAuthManager->getIdentity());
        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(CodeOperationInterface::CODE_OPERATION_TYPE_NAME)
        );
        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/confirm');
    }

    /**
     * For Ajax
     * Попытка подтверждение кода, которого нет в БД для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionWithInvalidCodeForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => '333333', // Такого кода нет в базе
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/login/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals(null, $this->baseAuthManager->getIdentity());
        $this->assertEquals(
            $this->user_login,
            $this->sessionContainerManager
                ->getLoginByOperationType(CodeOperationInterface::CODE_OPERATION_TYPE_NAME)
        );

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid code entered', $data['message']);
    }

    /**
     * Попытка подтверждение кода для пользователя, которого нет в БД
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionWithInvalidLogin()
    {
        $confirmation_code = '445566';
        // Подготовка данных. Записываем в сессию логин несуществубщего пользователя
        $this->dataPrepare($confirmation_code, 'not_exists_login');

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST и ожидаем Exception
        $this->dispatch('/login/confirm', 'POST', $postData);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertEquals(null, $this->baseAuthManager->getIdentity());
        $this->assertMatchedRouteName('login/confirm');
    }

    /**
     * For Ajax
     * Попытка подтверждение кода для пользователя, которого нет в БД Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testLoginActionWithInvalidLoginForAjax()
    {
        $confirmation_code = '445566';
        // Подготовка данных. Записываем в сессию логин несуществубщего пользователя
        $this->dataPrepare($confirmation_code, 'not_exists_login');

        // Get CSRF
        $csrf = new Element\Csrf('csrf');
        // Post data
        $postData = [
            'code' => $confirmation_code,
            'csrf' => $csrf->getValue(),
        ];
        $this->getRequest()->setContent(json_encode($postData));
        // Посылаем форму методом POST
        $isXmlHttpRequest = true;
        $this->dispatch('/login/confirm', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals(null, $this->baseAuthManager->getIdentity());

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/confirm');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Process initiator user not found', $data['message']);
    }

    /**
     * Повторная отправка кода
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendAction()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Посылаем GET запрос
        $this->dispatch('/login/code-resend');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/code-resend');
        $this->assertControllerName(AuthController::class);
        $this->assertControllerClass('AuthController');
        $this->assertRedirectTo('/login/confirm');
    }

    /**
     * For Ajax
     * Повторная отправка кода для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionForAjax()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/login/code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/code-resend');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals(OperationConfirmManager::SUCCESS_NEW_CONFIRMATION_CODE_SENT, $data['message']);
    }

    /**
     * Повторная отправка кода
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithNoLoginAndCode()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, null);

        // Посылаем GET запрос
        $this->dispatch('/login/code-resend');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/code-resend');
        $this->assertControllerName(AuthController::class);
        $this->assertControllerClass('AuthController');
        #$this->assertRedirectTo('/login/confirm');
    }

    /**
     * For Ajax
     * Повторная отправка кода c "нулевым" логином в сессии для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithNoLoginAndCodeForAjax()
    {
        $confirmation_code = null;
        // Подготовка данных
        $this->dataPrepare($confirmation_code, null);

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/login/code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/code-resend');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals("Process initiator user not found", $data['message']);
    }

    /**
     * Повторная отправка кода при неистекшим периодом ['tokens']['confirmation_code_request_period']
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithTimePeriodNotPassed()
    {
        $confirmation_code = "888999";
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Посылаем GET запрос
        $this->dispatch('/login/code-resend');

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/code-resend');
        $this->assertControllerName(AuthController::class);
        $this->assertControllerClass('AuthController');
        #$this->assertRedirectTo('/login/confirm');
    }

    /**
     * For Ajax
     * Повторная отправка кода при неистекшим периодом ['tokens']['confirmation_code_request_period'] для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testConfirmationCodeResendActionWithTimePeriodNotPassedForAjax()
    {
        $confirmation_code = "888999";
        // Подготовка данных
        $this->dataPrepare($confirmation_code, $this->user_login);

        // Посылаем GET запрос
        $isXmlHttpRequest = true;
        $this->dispatch('/login/code-resend', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('login/code-resend');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('User not allowed request code', $data['message']);
    }


    private function dataPrepare($confirmation_code, $user_login)
    {
        if ($confirmation_code) {
            $code_operation_type_name = CodeOperationInterface::CODE_OPERATION_TYPE_NAME;
            // Prepare objects
            // Get User by login
            $user = $this->userManager->selectUserByLogin($this->user_login);
            // Get CodeOperationType by name
            $codeOperationType = $this->codeManager->getCodeOperationTypeByName($code_operation_type_name);
            // Save code with value $confirmation_code to DB
            $this->codeManager->saveCode($user, $codeOperationType, $confirmation_code);
        }
        // Session init
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        // Session start
        $sessionManager->start();
        // Set session vars
        $this->setSessionVars($user_login, 0, '/');
    }

    private function setSessionVars($login, $remember_me = null, $back_redirect_url = null)
    {
        // Set to session just registered User login
        $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
            CodeOperationInterface::CODE_OPERATION_TYPE_NAME,
            $login
        );

        if($remember_me) {
            $this->sessionContainerManager->setSessionVar('remember_me', 0);
        }

        if($back_redirect_url) {
            $this->sessionContainerManager->setSessionVar('back_redirect_url', '/');
        }
    }
}