<?php
namespace ModuleAuthV2Test\Controller;

use Core\Entity\Interfaces\UserInterface;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseRoleManager;
use Core\Service\Base\BaseUserManager;
use ModuleAuthV2\Controller\UserRegistrationController;
use Application\Entity\Email;
use ModuleAuthV2\Form\UserRegistrationForm;
use Core\Service\Base\BaseAuthManager;
use Application\Entity\Role;
use Zend\Session\SessionManager;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;
use CoreTest\ViewVarsTrait;

class UserRegistrationControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    protected $traceError = true;

    private $user_login;
    private $user_password;
    private $user_email;
    private $user_phone;

    /**
     * @var
     */
    private $user;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;

    /**
     * @var BasePhoneManager
     */
    private $basePhoneManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var bool
     */
    private $disable_rollback = false;

    /**
     * $sessionContainerManager
     * @var \Core\Service\SessionContainerManager
     */
    private $sessionContainerManager;

    /**
     * @var BaseRoleManager
     */
    private $baseRoleManager;
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;

    public function setUp()
    {
        parent::setUp();
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        //$configOverrides = [];

        // Add content of global.php and other to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $this->resetConfigParameters();

        $config = $this->getApplicationConfig();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];
        $this->user_email = $config['tests']['user_email'];
        $this->user_phone = $config['tests']['user_phone'];

        $serviceManager = $this->getApplicationServiceLocator();

        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->baseEmailManager = $serviceManager->get(BaseEmailManager::class);
        $this->basePhoneManager = $serviceManager->get(BasePhoneManager::class);
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->sessionContainerManager = $serviceManager->get(\Core\Service\SessionContainerManager::class);
        $this->baseRoleManager = $serviceManager->get(BaseRoleManager::class);
        $this->baseUserManager = $serviceManager->get(BaseUserManager::class);

        $user = $this->baseUserManager->getUserByLogin('test');
        $this->user = $user;

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_password_reset
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $config['sms_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    protected function tearDown() {
        if (!$this->disable_rollback) {
            // Transaction rollback
            $this->entityManager->getConnection()->rollBack();
        } else {
            $this->disable_rollback = false;
        }

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * Проверка доступности страницы регистрации и наличия формы
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationIndexActionCanBeAccessed()
    {
        $this->dispatch('/register', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(UserRegistrationController::class); // as specified in router's controller name alias
        $this->assertMatchedRouteName('register');
        // Что отдается в шаблон
        $this->assertArrayHasKey('userRegistrationForm', $view_vars);
        $this->assertInstanceOf(UserRegistrationForm::class, $view_vars['userRegistrationForm']);
        $this->assertArrayHasKey('messageData', $view_vars);
        $this->assertArrayHasKey('isRegistered', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#user-registration-form');
    }

    /**
     * For Ajax
     * Проверка отдачи CSRF токена для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationIndexActionCanBeAccessedForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatch('/register', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertMatchedRouteName('register');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('CSRF token return', $data['message']);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Регистрация с valid Post
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithValidPostAndUppercaseEmail()
    {
        $user_login = 'testValidPost';

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $user_email = 'VVZONE1@yandex.ru';

        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => '+79103300811',
            'email'     => $user_email,
            'password'  => '123456',
            'csrf'      => $csrf
        ];

        $this->dispatch('/register', 'POST', $testUserData );

        /** @var UserInterface $registeredUser */
        $registeredUser = $this->baseUserManager->getUserByLogin($user_login);

        /** @var Email $email */
        $email = $registeredUser->getEmail();

        $this->assertEquals($user_login, $registeredUser->getLogin());

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');

        $this->assertNotEquals($user_email, $email->getEmail());
        $this->assertEquals(strtolower($user_email), $email->getEmail());

        // Удаление (в контроллере трансакция с фиксацией точки отката)
        #$this->baseUserManager->deleteUserRegistration($registeredUser->getId());
        $this->disable_rollback = true;
    }

    /**
     * For Ajax
     * Регистрация с valid Post для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testUserRegistrationWithValidPostForAjax()
    {
        $user_login = 'testValidPost';

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => '+79103300811',
            'email'     => 'vvzone1@yandex.ru',
            'password'  => '123456',
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($testUserData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $registeredUser = $this->baseUserManager->getUserByLogin($user_login);
        $this->assertEquals($user_login, $registeredUser->getLogin());

        $this->assertModuleName('moduleauth');

        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertEquals('User registered', $data['message']);

        // Удаление (в контроллере трансакция с фиксацией точки отката)
        $this->baseUserManager->deleteUserRegistrationByUserId($registeredUser->getId());
        $this->disable_rollback = true;
    }

    /**
     * Регистрация с неверным форматом email
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithInvalidEmail()
    {
        $user_login = 'testValidPost';

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => '+79103300811',
            'email'     => 'vvzone1@yandex', // без домена
            'password'  => '123456',
            'csrf'      => $csrf
        ];

        $this->dispatch('/register', 'POST', $testUserData );

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertControllerClass('UserRegistrationController');
        $this->assertMatchedRouteName('register');
        // Что отдается в шаблон
        $this->assertArrayHasKey('userRegistrationForm', $view_vars);
        $this->assertInstanceOf(UserRegistrationForm::class, $view_vars['userRegistrationForm']);
        $this->assertArrayHasKey('messageData', $view_vars);
        $this->assertArrayHasKey('isRegistered', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#user-registration-form');
        //$registeredUser = $this->baseUserManager->getUserByLogin($user_login);
        //$this->assertEquals(null, $registeredUser);
    }

    /**
     * For Ajax
     * Регистрация с неверным форматом email для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithInvalidEmailForAjax()
    {
        $user_login = 'testValidPost';

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => '+79103300811',
            'email'     => 'vvzone1yandex.ru', // без собаки
            'password'  => '123456',
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($testUserData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid form data', $data['message']);
        $this->assertArrayHasKey('email', $data['data']);
        $this->assertArrayHasKey('emailAddressInvalidFormat', $data['data']['email']);

        //$registeredUser = $this->baseUserManager->getUserByLogin($user_login);
        //$this->assertEquals(null, $registeredUser);
    }

    // Variant of invalid numbers for tests
    public function invalidNumberProvider()
    {
        return [
            ['79103300811', 'Invalid phone number'],
            ['+79103300811678956', 'Invalid phone number'],
            ['yytytytytyt', 'Invalid phone number']
        ];
    }

    /**
     * Регистрация с неверным форматом телефона
     *
     * @dataProvider invalidNumberProvider
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithInvalidPhone($phone_number, $expected_error)
    {
        $user_login = 'testValidPost';

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => $phone_number,
            'email'     => 'vvzone1@yandex.ru',
            'password'  => '123456',
            'csrf'      => $csrf
        ];

        $this->dispatch('/register', 'POST', $testUserData );

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertMatchedRouteName('register');
        // Что отдается в шаблон
        $this->assertArrayHasKey('userRegistrationForm', $view_vars);
        $this->assertInstanceOf(UserRegistrationForm::class, $view_vars['userRegistrationForm']);
        $this->assertArrayHasKey('messageData', $view_vars);
        $this->assertArrayHasKey('isRegistered', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#user-registration-form');

        //$registeredUser = $this->baseUserManager->getUserByLogin($user_login);
        //$this->assertEquals(null, $registeredUser);
    }

    /**
     * For Ajax
     * Регистрация с неверным форматом phone для Ajax
     *
     * @dataProvider invalidNumberProvider
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithInvalidPhoneForAjax($phone_number, $expected_error)
    {
        $user_login = 'testValidPost';

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $testUserData   = [
            'login'     => $user_login,
            'phone'     => $phone_number,
            'email'     => 'vvzone1@yandex.ru',
            'password'  => '123456',
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($testUserData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('Invalid form data', $data['message']);
        $this->assertArrayHasKey('phone', $data['data']);
        $this->assertArrayHasKey('invalidPhone', $data['data']['phone']);
        $this->assertEquals($expected_error, $data['data']['phone']['invalidPhone']);

        //$registeredUser = $this->baseUserManager->getUserByLogin($user_login);
        //$this->assertEquals(null, $registeredUser);
    }

    /**
     * Регистрация по данным уже существующего пользователя
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithValidPostAndExistingUserData()
    {
        // Номер "нового" пользователя, которого пытаемся зарегистрировать.
        // Он не должен совпасть с номером уже существующего пользователя.
        $user_phone = '+79103300811';

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $testUserData   = [
            'login'     => $this->user_login,
            'phone'     => $user_phone,
            'email'     => $this->user_email,
            'password'  => '123456',
            'csrf'      => $csrf
        ];

        $this->dispatch('/register', 'POST', $testUserData );

        $registeredUser = $this->baseUserManager->getUserByLogin($this->user_login);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('moduleauth');
        $this->assertNotEquals($user_phone, $registeredUser->getPhone()->getPhoneNumber());
        $this->assertMatchedRouteName('register');
        #$this->assertQuery('.container');
    }

    /**
     * For Ajax
     * Регистрация по данным уже существующего пользователя для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithValidPostAndExistingUserDataForAjax()
    {
        // Номер "нового" пользователя, которого пытаемся зарегистрировать.
        // Он не должен совпасть с номером уже существующего пользователя.
        $user_phone = '+79103300811';

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        // Post data
        $testUserData   = [
            'login'     => $this->user_login,
            'phone'     => $user_phone,
            'email'     => $this->user_email,
            'password'  => '123456',
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($testUserData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $registeredUser = $this->baseUserManager->getUserByLogin($this->user_login);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertNotEquals($user_phone, $registeredUser->getPhone()->getPhoneNumber());

        $this->assertEquals('ERROR', $data['status']);
        $this->assertEquals('User login already exists', $data['message']);
    }

    /**
     * Попытка регистрации с заведомо неверным Csrf токеном
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithNotValidCsrf()
    {
        $testUserData   = [
            'login'     => 'testCsrf',
            'phone'     => '+79103300822',
            'email'     => 'vvzone2@yandex.ru',
            'password'  => '123456',
            'csrf'      => 'be04a5a7a198a35b97218b9c01dc9d7e-cae84c0c28877ada5ea617783e02733b'
        ];

        $this->dispatch('/register', 'POST', $testUserData );

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('register');
        // Что отдается в шаблон
        $this->assertArrayHasKey('userRegistrationForm', $view_vars);
        $this->assertInstanceOf(UserRegistrationForm::class, $view_vars['userRegistrationForm']);
        $this->assertArrayHasKey('messageData', $view_vars);
        $this->assertArrayHasKey('isRegistered', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#user-registration-form');
    }

    /**
     * For Ajax
     * Попытка регистрации с заведомо неверным Csrf токеном для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithNotValidCsrfForAjax()
    {
        $testUserData   = [
            'login'     => 'testCsrf',
            'phone'     => '+79103300822',
            'email'     => 'vvzone2@yandex.ru',
            'password'  => '123456',
            'csrf'      => 'be04a5a7a198a35b97218b9c01dc9d7e-cae84c0c28877ada5ea617783e02733b'
        ];

        $this->getRequest()->setContent(json_encode($testUserData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertEquals('ERROR', $data['status']);
    }

    /**
     * Попытка регистрации с коротким паролем
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithShortPassword()
    {
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $testUserData   = [
            'login'     => 'testPass',
            'phone'     => '+79103300833',
            'email'     => 'vvzone3@yandex.ru',
            'password'  => '1234',
            'csrf'      => $csrf
        ];

        $this->dispatch('/register', 'POST', $testUserData );

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertMatchedRouteName('register');
        // Что отдается в шаблон
        $this->assertArrayHasKey('userRegistrationForm', $view_vars);
        $this->assertInstanceOf(UserRegistrationForm::class, $view_vars['userRegistrationForm']);
        $this->assertArrayHasKey('messageData', $view_vars);
        $this->assertArrayHasKey('isRegistered', $view_vars);
        // Возвращается из шаблона
        $this->assertQuery('#user-registration-form');
    }

    /**
     * For Ajax
     * Попытка регистрации с коротким паролем для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testUserRegistrationWithShortPasswordForAjax()
    {
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $testUserData   = [
            'login'     => 'testPass',
            'phone'     => '+79103300833',
            'email'     => 'vvzone3@yandex.ru',
            'password'  => '1234',
            'csrf'      => $csrf
        ];

        $this->getRequest()->setContent(json_encode($testUserData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertEquals('ERROR', $data['status']);
    }


    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testIndexActionViewModelTemplateRenderedWithinLayout()
    {
        $this->dispatch('/register', 'GET');
        $this->assertQuery('form');
    }

    /**
     * Проверка редиректа при запросе к экшену checkIsLoginUniqueAction через Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testCheckIsLoginUniqueAction()
    {
        $this->dispatch('/register/check/login');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertRedirectTo('/');
    }

    // Variant of user logins
    public function userLoginProvider()
    {
        return [
            ['unique_login', 'SUCCESS', 'Login is available'],  // логин, которого нет в базе
            ['not_unique', 'ERROR', 'Login already taken']      // логин, который есть в базе
        ];
    }

    /**
     * For Ajax
     * Проверка уникальности логина для Ajax
     * @dataProvider userLoginProvider
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testCheckIsLoginUniqueActionForAjax($login, $status, $message)
    {
        // dataProvider не может принимать динамические переменные, поэтому существующий номер ('not_unique') подменяем здесь
        if('not_unique' == $login) {
            $login = $this->user_login;
        }

        $testUserData   = [
            'login' => $login,
        ];

        $this->getRequest()->setContent(json_encode($testUserData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register/check/login', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');

        $this->assertEquals($status, $data['status']);
        $this->assertEquals($message, $data['message']);
    }

    /**
     * Проверка редиректа при запросе к экшену checkIsPhoneUniqueAction через Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testCheckIsPhoneUniqueAction()
    {
        $this->dispatch('/register/check/phone');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertRedirectTo('/');
    }

    // Variant of user logins
    public function userPhoneNumberProvider()
    {
        return [
            ['+79031087823', 'SUCCESS', 'Phone is available'],  // телефон, которого нет в базе
            ['not_unique', 'ERROR', 'Phone already taken']      // телефон, который есть в базе
        ];
    }

    /**
     * For Ajax
     * Проверка уникальности телефона для Ajax
     *
     * @dataProvider userPhoneNumberProvider
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testCheckIsPhoneUniqueActionForAjax($phone_number, $status, $message)
    {
        // dataProvider не может принимать динамические переменные, поэтому существующий номер ('not_unique') подменяем здесь
        if('not_unique' == $phone_number) {
            $phone_number = $this->user_phone;
        }
        // В базе телефон, добавленный фикстурами, может оказаться не верифицированным.
        // Проверяем и, если так, то верифицируем.
        $phone = $this->basePhoneManager->getPhoneByNumber($phone_number);
        if($phone && $phone->getIsVerified() == 0) {
            $phone->setIsVerified(1);
            $this->entityManager->flush();
        }

        $testUserData   = [
            'phone' => $phone_number,
        ];

        $this->getRequest()->setContent(json_encode($testUserData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register/check/phone', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertEquals($status, $data['status']);
        $this->assertEquals($message, $data['message']);
    }

    /**
     * Проверка редиректа при запросе к экшену checkIsEmailUniqueAction через Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testCheckIsEmailUniqueAction()
    {
        $this->dispatch('/register/check/email');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertRedirectTo('/');
    }

    // Variant of user logins
    public function userEmailProvider()
    {
        return [
            ['unique@email.com', 'SUCCESS', 'Email is available'],  // email, которого нет в базе
            ['Unique@email.com', 'SUCCESS', 'Email is available'],  // email, которого нет в базе Caps
            ['test@simple-technology.ru', 'ERROR', 'Email already taken'], // email который есть в базе
            ['Test@simple-technology.ru', 'ERROR', 'Email already taken'] // email который есть в базе Caps
        ];
    }

    /**
     * ForAjax
     * Проверка уникальности email для Ajax
     *
     * @dataProvider userEmailProvider
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     * @throws \Exception
     */
    public function testCheckIsEmailUniqueActionForAjax($email_value, $status, $message)
    {
        // В базе email, добавленный фикстурами, может оказаться не верифицированным.
        // Проверяем и, если так, то верифицируем.
        $email = $this->baseEmailManager->getEmailByValue($email_value);
        if($email && $email->getIsVerified() === 0) {
            $email->setIsVerified(1);
            $this->entityManager->flush();
        }

        $testUserData   = [
            'email' => $email_value,
        ];

        $this->getRequest()->setContent(json_encode($testUserData));

        $isXmlHttpRequest = true;
        // Посылаем форму методом POST
        $this->dispatch('/register/check/email', 'POST', $testUserData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertModuleName('moduleauth');

        $this->assertEquals($status, $data['status']);
        $this->assertEquals($message, $data['message']);
    }

    /**
     * Проверка доступности /register/delete неавторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testDeleteActionCanNotBeAccessedByNotAuthorized()
    {
        $this->dispatch('/register/delete', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertRedirectTo('/login?redirectUrl=/register/delete');
    }

    /**
     * Проверка доступности /register/delete обычному пользователю (Verified)
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testDeleteActionCanNotBeAccessedByVerified()
    {
        /** @var UserInterface $user */
        $user = $this->user;
        // Присвоение роли 'Verified' тестовому пользователю
        $this->assignRoleToTestUser('Verified', $this->user);
        // Залогиниваем пользователя
        $this->login($user->getLogin());

        $this->dispatch('/register/delete', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Проверка недоступности /register/delete Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testDeleteActionCanNotBeAccessedByOperator()
    {
        /** @var UserInterface $user */
        $user = $this->user;
        // Присвоение роли 'Operator' тестовому пользователю
        $this->assignRoleToTestUser('Operator', $this->user);
        // Залогиниваем пользователя
        $this->login($user->getLogin());

        $this->dispatch('/register/delete', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Проверка доступности /register/delete Администратору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group auth
     */
    public function testDeleteActionCanBeAccessedByAdministrator()
    {
        /** @var UserInterface $user */
        $user = $this->user;
        // Присвоение роли 'Administrator' тестовому пользователю
        $this->assignRoleToTestUser('Administrator', $this->user);
        // Залогиниваем пользователя
        $this->login($user->getLogin());

        $this->dispatch('/register/delete', 'GET');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('moduleauth');
        $this->assertControllerName(UserRegistrationController::class);
        $this->assertMatchedRouteName('register/default');
    }

    /**
     * Присвоение роли тестовому пользователю
     *
     * @param string $role_name
     * @param UserInterface|null $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function assignRoleToTestUser(string $role_name, UserInterface $user=null)
    {
        if(!$user) {
            $user = $this->user;
        }
        // Если были роли, удаляем
        $user->getRoles()->clear();
        /** @var Role $role */
        $role = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => $role_name));

        $this->baseRoleManager->addRoleByLogin($user, $role);
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login, string $password = 'qwerty')
    {
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}