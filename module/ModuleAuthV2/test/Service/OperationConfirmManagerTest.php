<?php

namespace ModuleAuthV2Test\Service;

use Core\Service\CodeOperationInterface;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Application\Entity\User;
use ModuleCode\Entity\CodeOperationType;
use ModuleCode\Service\CodeManager;

class OperationConfirmManagerTest extends AbstractHttpControllerTestCase implements CodeOperationInterface
{
    protected $traceError = true;

    private $user_login;
    private $user_password;

    /**
     * @var \Core\Service\OperationConfirmManager
     */
    private $operationConfirmManager;

    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var CodeManager
     */
    private $codeManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php and other to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                include __DIR__ . '/../../../../config/application.config.php',
                include __DIR__ . '/../../../../config/autoload/global.php'
            ),
            include __DIR__ . '/../../../../config/autoload/development.local.php'
        ));

        $serviceManager = $this->getApplicationServiceLocator();

        $this->enableMessageSendSimulation();

        $this->operationConfirmManager = $serviceManager->get(\Core\Service\OperationConfirmManager::class);
        $this->codeManager = $serviceManager->get(CodeManager::class);
        $this->userManager = $serviceManager->get(\Application\Service\UserManager::class);
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');

        $config = $this->getApplicationConfig();

        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    private function enableMessageSendSimulation()
    {
        // Override config var ['sms_providers']['message_send_simulation']
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('config');
        $config['sms_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('config', $config);
        $services->setAllowOverride(false);
    }

    protected function tearDown()
    {
        // Transaction rollback
        $this->entityManager->getConnection()->rollback();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
    }

    /**
     * Подтверждение кода, который есть в базе
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testConfirmCode()
    {
        $confirmation_code = '445566';
        $code_operation_type_name = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION;

        // Prepare Objects
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $codeOperationType = $this->codeManager->getCodeOperationTypeByName($code_operation_type_name);
        // Save code with value $confirmation_code to DB
        $this->codeManager->saveCode($user, $codeOperationType, $confirmation_code);

        $this->assertTrue($this->operationConfirmManager->checkCode($user, $confirmation_code, $code_operation_type_name));
    }

    /**
     * Подтверждение кода, которого нет в базе. Провоцирем Exception.
     *
     * @expectedException \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testConfirmCodeThrowsException()
    {
        $confirmation_code = '445566';
        $code_operation_type_name = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION;

        $user = $this->userManager->selectUserByLogin($this->user_login);

        // Exception expected
        $this->operationConfirmManager->checkCode($user, $confirmation_code, $code_operation_type_name);
    }

    /**
     * Подтверждение кода c логином пользователя, которого нет в базе. Провоцирем Exception.
     *
     * @expectedException \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testConfirmCodeWithNotExistsUser()
    {
        $user = new User();
        $user->setLogin('Not_exists');
        $confirmation_code = '445566';
        $code_operation_type_name = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION;
        // Exception expected
        $this->operationConfirmManager->checkCode($user, $confirmation_code, $code_operation_type_name);
    }

    /**
     * Отправка кода
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testResendCode()
    {
        $code_operation_type_name = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION;
        // Генерируем код и тестируем, что сработало
        $this->assertTrue($this->operationConfirmManager->sendCode($this->user_login, $code_operation_type_name));
        /** @var CodeOperationType $codeOperationType */
        $codeOperationType = $this->codeManager->getCodeOperationTypeByName($code_operation_type_name);
        $user = $this->userManager->selectUserByLogin($this->user_login);
        // Получаем код из БД
        $confirmation_code = $this->codeManager->getLastUserCodeByOperationType($user, $codeOperationType);
        // Подтверждаем код и тестируем
        $this->assertTrue($this->operationConfirmManager->checkCode($user, $confirmation_code->getValue(), $code_operation_type_name));
    }

    /**
     * Запрашиваем отправку кода при неистекшем confirmation_code_request_period. Провоцирем Exception.
     *
     * @expectedException \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     */
    public function testResendCodeThrowsException()
    {
        $confirmation_code = '445566';
        $code_operation_type_name = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION;
        // Prepare Objects
        $user = $this->userManager->selectUserByLogin($this->user_login);
        $codeOperationType = $this->codeManager->getCodeOperationTypeByName($code_operation_type_name);
        // Save code with value $confirmation_code to DB
        $this->codeManager->saveCode($user, $codeOperationType, $confirmation_code);
        // Подтверждаем код и тестируем
        $this->assertTrue($this->operationConfirmManager->sendCode($user, $code_operation_type_name));
    }
}