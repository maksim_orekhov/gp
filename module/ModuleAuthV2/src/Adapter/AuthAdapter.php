<?php
namespace ModuleAuthV2\Adapter;

use Core\Entity\Interfaces\EmailInterface;
use Core\Entity\Interfaces\PhoneInterface;
use Core\Entity\Interfaces\UserInterface;
use Core\Service\Auth\AuthAdapterInterface;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseUserManager;
use Zend\Authentication\Result;
use Zend\Crypt\Password\Bcrypt;

class AuthAdapter implements AuthAdapterInterface
{
    const SUCCESS_AUTHENTICATION = 'Authenticated successfully';
    const ERROR_AUTHENTICATION = 'Invalid credentials';
    /**
     * User login.
     * @var string
     */
    private $login;
    /**
     * User password.
     * @var string
     */
    private $password;

    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;
    /**
     * @var BasePhoneManager
     */
    private $basePhoneManager;
    /**
     * @var bool
     */
    private $verify_password = true;

    /**
     * AuthAdapter constructor.
     * @param BaseUserManager $baseUserManager
     * @param BaseEmailManager $baseEmailManager
     * @param BasePhoneManager $basePhoneManager
     */
    public function __construct(BaseUserManager $baseUserManager,
                                BaseEmailManager $baseEmailManager,
                                BasePhoneManager $basePhoneManager)
    {
        $this->baseUserManager = $baseUserManager;
        $this->baseEmailManager = $baseEmailManager;
        $this->basePhoneManager = $basePhoneManager;
    }

    /**
     * @param $login
     * @return UserInterface|null
     */
    private function getUserByLoginOrEmailOrPhone($login)
    {
        /** @var EmailInterface $email */
        $email = $this->baseEmailManager->getEmailVerifiedByValue($login);
        if ($email !== null) {

            return $this->baseUserManager->getUserByEmail($email);
        }

        /** @var PhoneInterface $phone */
        $phone = $this->basePhoneManager->getPhoneVerifiedByNumber($login);
        if ($phone !== null) {
            return $this->baseUserManager->getUserByPhone($phone);
        }

        $user = $this->baseUserManager->getUserByLogin($login);
        if ($user !== null) {

            return $user;
        }

        return null;
    }

    /**
     * Performs an authentication attempt
     * Используется в AuthService
     *
     * @return Result
     */
    public function authenticate()
    {
        $user = $this->getUserByLoginOrEmailOrPhone($this->login);

        // If there is no such user, return 'Identity Not Found' status.
        if ($user === null) {
            return new Result(
                Result::FAILURE_IDENTITY_NOT_FOUND,
                null,
                [self::ERROR_AUTHENTICATION]);
        }

        if ($this->verify_password === false) {
            // не проверяем парол просто логинем
            return new Result(
                Result::SUCCESS,
                $user->getLogin(),
                [self::SUCCESS_AUTHENTICATION]);
        }

        // Now we need to calculate hash based on user-entered password and compare
        // it with the password hash stored in database.
        $bCrypt = new Bcrypt();
        $bCrypt->create('qwerty');
        $passwordHash = $user->getPassword();

        if ($bCrypt->verify($this->password, $passwordHash)) {
            // Great! The password hash matches. Return user identity (login).
            return new Result(
                Result::SUCCESS,
                $user->getLogin(),
                [self::SUCCESS_AUTHENTICATION]);
        }

        // If password check didn't pass return 'Invalid Credential' failure status.
        return new Result(
            Result::FAILURE_CREDENTIAL_INVALID,
            null,
            [self::ERROR_AUTHENTICATION]);
    }

    /**
     * @throws \Error
     */
    public function checkCredentials()
    {
        throw new \Error('Attempt to use checkCredentials with wrong AuthAdapter');
    }

    /**
     * @return bool
     */
    public function checkLoginExist() :bool
    {
        return (bool) $this->getUserByLoginOrEmailOrPhone($this->login);
    }


    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function getLogin()
    {
        return $this->login;
    }
    public function setVerifyPassword($verify_password = true)
    {
        $this->verify_password = $verify_password;
    }
}