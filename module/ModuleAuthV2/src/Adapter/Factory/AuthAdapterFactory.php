<?php
namespace ModuleAuthV2\Adapter\Factory;

use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseUserManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleAuthV2\Adapter\AuthAdapter;

/**
 * This is the factory class for AuthAdapter service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class AuthAdapterFactory implements FactoryInterface
{
    /**
     * This method creates the AuthAdapter service and returns its instance.
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return AuthAdapter|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseUserManager = $container->get(BaseUserManager::class);
        $baseEmailManager = $container->get(BaseEmailManager::class);
        $basePhoneManager = $container->get(BasePhoneManager::class);

        return new AuthAdapter(
            $baseUserManager,
            $baseEmailManager,
            $basePhoneManager
        );
    }
}