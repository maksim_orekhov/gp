<?php
namespace ModuleAuthV2;

use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;

class Module
{
    const MODULE_NAME = 'auth_v2';

    private $is_not_active_module = false;

    /**
     * @return void
     */
    public function init()
    {
        $config = array_replace_recursive(
            require __DIR__ . '/../../../config/autoload/auth.global.php',
            require __DIR__ . '/../../../config/environment/'. (\getenv('APP_ENV') ?: 'production') . '.config.php'
        );

        if (isset($config['auth_service']['provider']) && $config['auth_service']['provider'] !== __NAMESPACE__) {

            $this->is_not_active_module = true;
        }
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        if ( $this->is_not_active_module ) {
            return [];
        }

        return array_replace_recursive(
            require __DIR__ . '/../config/module.config.php',
            require __DIR__ . '/../config/router.config.php',
            require __DIR__ . '/../config/assets.config.php',
            require __DIR__ . '/../config/controllers.config.php',
            require __DIR__ . '/../config/service.config.php',
            require __DIR__ . '/../config/views.config.php'
        );
    }

    /**
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {

    }
}
