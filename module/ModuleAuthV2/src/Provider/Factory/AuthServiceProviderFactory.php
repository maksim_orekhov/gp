<?php
namespace ModuleAuthV2\Provider\Factory;

use Interop\Container\ContainerInterface;
use ModuleAuthV2\Provider\AuthServiceProvider;
use Zend\ServiceManager\Factory\FactoryInterface;


class AuthServiceProviderFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return AuthServiceProvider|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AuthServiceProvider($container);
    }
}