<?php
namespace ModuleAuthV2\Provider;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;


class AuthServiceProvider
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $adapter;
    private $config;

    /**
     * AuthServiceProvider constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->setConfig();
        $this->setAdapter();
    }

    /**
     * @return mixed
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * @return mixed
     */
    public function setConfig()
    {
        if ($this->config !== null) {
            return $this->config;
        }

        if (! $this->container->has('config')) {
            $this->config = [];
            return $this->config;
        }

        $config = $this->container->get('config');
        if (! isset($config['auth_service'])
            || ! \is_array($config['auth_service'])
        ) {
            $this->config = [];
            return $this->config;
        }

        $config = $config['auth_service'];
        if (! isset($config['adapter']) ) {
            $this->config = [];
            return $this->config;
        }
        $this->config = $config;

        return $this->config;
    }

    /**
     * @param null $adapter_name
     * @return mixed
     */
    public function setAdapter($adapter_name = null)
    {
        if ($adapter_name !== null && $this->container->has($adapter_name)) {

            $this->adapter = $this->container->get($adapter_name);

            return $this->adapter;
        }

        $config = $this->config;

        if (empty($config)
            || !array_key_exists('adapter', $config)
            || empty($config['adapter'])
            || !\is_string($config['adapter']))
        {
            throw new ServiceNotCreatedException(sprintf(
                'Auth adapter class does not found in "%s"',
                __CLASS__
            ));
        }

        if (! $this->container->has($config['adapter'])) {
            throw new ServiceNotCreatedException(sprintf(
                'Service with name "%s" could not be created.',
                $config['adapter']
            ));
        }

        $this->adapter = $this->container->get($config['adapter']);

        return $this->adapter;
    }
}