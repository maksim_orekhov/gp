<?php

namespace ModuleAuthV2\Form;

use Core\Form\Captcha\ReCaptchaV2;
use ModuleAuthV2\Form\Validator\IdenticalCallback;
use ModuleAuthV2\Module;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class UserRegistrationForm
 * @package ModuleAuthV2\Form
 */
class UserRegistrationForm extends Form
{
    const CLASS_PREFIX = Module::MODULE_NAME;
    const REGEXP = '/^[^#@А-Яа-я,"][\.0-9A-Za-z-_]{0,}\@(([0-9A-Za-z-_]{2,18}\.[A-Za-z-]{2,5})|([0-9A-Za-z-_]{2,18}\.[0-9A-Za-z-_]{2,18}\.[-A-Za-z]{2,5}))/';
    /**
     * @var mixed
     */
    private $user_email;

    /**
     * @var null
     */
    private $email_pattern;
    /**
     * @var array
     */
    private $config;

    /**
     * Конструктор.
     * @param null $user_email
     * @param array $config
     */
    public function __construct($user_email = null, array $config)
    {
        parent::__construct(self::CLASS_PREFIX.'-user-registration-form');

        $this->user_email = strtolower($user_email);
        $this->email_pattern = self::REGEXP;
        if ( $user_email ) {
            $this->email_pattern = str_replace(['@', '.'], ['\@', '\.'], '/^'.$user_email.'/');
        }
        $this->config = $config;

        $this->setAttribute('method', 'post');
        $this->setAttribute('id', self::CLASS_PREFIX.'-user-registration-form');
        $this->addElements();
        $this->addInputFilter();

    }

    protected function addElements()
    {
        $this->add([
            'type' => 'captcha',
            'name' => 'captcha',
            'attributes' => [
                'id' => 'g-recaptcha-response',
            ],
            'options' => [
                'captcha' => [
                    'class' => ReCaptchaV2::class,
                    'options' => [
                        'site_key' => $this->config['recaptcha_v2']['site_key'],
                        'secret_key' => $this->config['recaptcha_v2']['secret_key'],
                        'bypass' => $this->config['recaptcha_v2']['bypass'],
                        'hl' => 'ru'
                    ]
                ],
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'email',
            'attributes' => [
                'id' => self::CLASS_PREFIX.'-email',
                'class'=>'form-control',
                'placeholder'=>'Введите email',
                'value' => $this->user_email,
                'readonly' => (bool) $this->user_email
            ],
            'options' => [
                'label' => 'Email',
            ],
        ]);

        // Добавляем поле "password"
        $this->add([
            'type'  => 'password',
            'name' => 'password',
            'attributes' => [
                'id' => self::CLASS_PREFIX.'-password',
                'class' => 'form-control',
                'placeholder'=>'Введите пароль'
            ],
            'options' => [
                'label' => 'Пароль',
            ],
        ]);

        $this->add([
            'type'  => 'password',
            'name' => 'confirm_password',
            'attributes' => [
                'id' => self::CLASS_PREFIX.'-confirm_password',
                'class' => 'form-control',
                'placeholder'=>'Подтвердите пароль'
            ],
            'options' => [
                'label' => 'Подтверждение пароля',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'hidden',
            'name' => 'token',
            'attributes' => ['value' => '']
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Зарегистрироваться',
                'id' => self::CLASS_PREFIX.'-submit',
                'class'=>'btn btn-blue'
            ],
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name' => 'captcha',
            'required' => !$this->config['recaptcha_v2']['bypass'],
            'filters' => [
                ['name' => 'StringTrim'],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'email',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => $this->email_pattern,
                        'message' => 'Недопустимый email'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'password',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                ['name'    => 'StringLength', 'options' => ['min' => 6, 'max' => 64]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[a-zA-Z0-9]$/',
                        'message' => 'Недопустимые символы в пароле'
                    ]
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'confirm_password',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name' => IdenticalCallback::class,
                    'options' => [
                        'callback' => [$this, 'identicalPassword'],
                        'message' => 'Пароли не совпадают!'
                    ]
                ]
            ]
        ]);
    }

    /**
     * @param $value
     * @return bool
     */
    public function identicalPassword($value)
    {
        if (! \is_array($this->data) || ! \array_key_exists('password', $this->data)) {
            return false;
        }

        return $this->data['password'] === $value;
    }
}
