<?php
namespace ModuleAuthV2\Form;

use ModuleAuthV2\Module;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class LoginEditForm
 * @package ModuleAuthV2\Form
 */
class LoginEditForm extends Form
{
    const CLASS_PREFIX = Module::MODULE_NAME;
    /**
     * @var string
     */
    private $login;

    /**
     * LoginEditForm constructor.
     * @param $login
     */
    public function __construct(string $login)
    {
        // Define form name
        parent::__construct(self::CLASS_PREFIX.'-login-edit-form');

        $this->login = $login;

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', self::CLASS_PREFIX.'-login-edit-form');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        // Add "login" field
        $this->add([
            'type' => 'text',
            'name' => 'login',
            'attributes' => [
                'id' => self::CLASS_PREFIX.'-login',
                'class'=>'form-control',
                'placeholder'=>'Введите логин',
                'value' => $this->login,
            ],
            'options' => [
                'label' => 'Логин',
            ],
        ]);
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Изменить',
                'id' => self::CLASS_PREFIX.'-submit',
                'class'=>'btn btn-blue'
            ],
        ]);
    }

    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'login',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3, 'max' => 16]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/^[a-zA-Z][a-zA-Z0-9-_\.]{2,15}$/',
                        'message' => 'Неверный формат ввода']
                ],
            ],
        ]);
    }
}