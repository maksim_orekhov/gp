<?php
namespace ModuleAuthV2\Form;

use ModuleAuthV2\Module;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class PhoneCreateForm
 * @package ModuleAuthV2\Form
 */
class PhoneCreateForm extends Form
{
    const CLASS_PREFIX = Module::MODULE_NAME;

    public function __construct()
    {
        // Define form name
        parent::__construct(self::CLASS_PREFIX.'phone-create-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', self::CLASS_PREFIX.'-phone-create-form');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'phone',
            'attributes' => [
                'id' => self::CLASS_PREFIX.'-phone',
                'class' => 'form-control',
                'placeholder' => 'Телефон',
            ],
            'options' => [
                'label' => 'Введите телефон',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Добавить',
                'id' => self::CLASS_PREFIX.'-submit',
            ],
        ]);
    }

    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'phone',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                [
                    'name' =>  'PregReplace',
                    'options' => [
                        'pattern' => '/(\()|(\))|(\-)/',
                        'replacement' => ''
                    ]
                ],
            ],
            'validators' => [
                [
                    'name' => \Core\Form\Validator\PhoneValidator::class,
                ],
            ],
        ]);
    }
}