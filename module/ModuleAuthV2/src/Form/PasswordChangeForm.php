<?php
namespace ModuleAuthV2\Form;

use Core\Entity\Interfaces\UserInterface;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class PasswordChangeForm
 * @package ModuleAuthV2\Form
 */
class PasswordChangeForm extends Form
{
    /**
     * @var UserInterface
     */
    private $user;

    /**
     * PasswordChangeForm constructor.
     * @param UserInterface|null $user
     */
    public function __construct(UserInterface $user = null)
    {
        // Define form name
        parent::__construct('password-change-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->user = $user;

        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "new_password" field
        $this->add([
            'type'  => 'password',
            'name' => 'new_password',
            'attributes' => [
                'id' => 'New password',
                'class'=>'form-control',
                //'placeholder'=>'Enter new password'
            ],
            'options' => [
                'label' => 'New Password',
            ],
        ]);

        // Add "confirm_new_password" field
        $this->add([
            'type'  => 'password',
            'name' => 'confirm_new_password',
            'attributes' => [
                'id' => 'New password again',
                'class'=>'form-control',
                //'placeholder'=>'Enter new password again'
            ],
            'options' => [
                'label' => 'Confirm new password',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Change Password',
                'id' => 'submit',
                'class'=>'btn btn-blue'
            ]
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $new_password_validators = [
            ['name' => 'StringLength', 'options' => ['min' => 6, 'max' => 64]],
            ['name'    => 'Regex',
                'options' => [
                    'pattern' => '/[a-zA-Z0-9]$/',
                    'message' => 'Недопустимые символы в пароле']
            ],
        ];
        if (null !== $this->user) {
            $new_password_validators[] = [
                'name' => \ModuleAuthV2\Form\Validator\IdenticalPasswordValidator::class,
                'options' => ['current_password_hash' => $this->user->getPassword()]
            ];
        }

        // Add input for "new_password" field
        $inputFilter->add([
            'name'     => 'new_password',
            'required' => true,
            'filters'  => [
            ],
            'validators' => $new_password_validators
        ]);

        // Add input for "confirm_new_password" field
        $inputFilter->add([
            'name'     => 'confirm_new_password',
            'required' => true,
            'filters'  => [
            ],
            'validators' => [
                [
                    'name'    => 'Identical',
                    'options' => [
                        'token' => 'new_password',
                    ],
                ],
            ],
        ]);
    }
}