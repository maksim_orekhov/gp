<?php
namespace ModuleAuthV2\Form;

use Core\Exception\BaseExceptionCodeInterface;
use Core\Form\Captcha\ReCaptchaV2;
use Core\Service\Base\BaseAuthManager;
use ModuleAuthV2\Form\Validator\LoginValidator;
use ModuleAuthV2\Module;
use Zend\Captcha\ReCaptcha;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class LoginForm
 * @package ModuleAuthV2\Form
 */
class LoginForm extends Form
{
    const CLASS_PREFIX = Module::MODULE_NAME;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var array
     */
    private $config;
    /**
     * @var bool
     */
    private $captcha_enable;

    /**
     * There can be two scenarios - 'with auth confirm code' or 'without auth confirm code'.
     *
     *
     * @param BaseAuthManager $baseAuthManager
     * @param array $config
     * @param bool $captcha_enable
     */
    #private $authorization_confirm;

    public function __construct(BaseAuthManager $baseAuthManager, array $config, bool $captcha_enable)
    {
        // Define form name
        parent::__construct(self::CLASS_PREFIX.'-login-form');

        $this->baseAuthManager = $baseAuthManager;
        $this->config = $config;
        $this->captcha_enable = $captcha_enable;

        $this->setAttribute('method', 'post');
        $this->setAttribute('id', self::CLASS_PREFIX.'-login-form');
        $this->addElements();
        $this->addInputFilter();

    }

    public function getCaptcha()
    {
        return [
            'type' => 'captcha',
            'name' => 'captcha',
            'attributes' => [
                'id' => 'g-recaptcha-response',
            ],
            'options' => [
                'captcha' => [
                    'class' => ReCaptchaV2::class,
                    'options' => [
                        'site_key' => $this->config['recaptcha_v2']['site_key'],
                        'secret_key' => $this->config['recaptcha_v2']['secret_key'],
                        'bypass' => $this->config['recaptcha_v2']['bypass'],
                        'hl' => 'ru',
                    ]
                ],
            ],
        ];
    }

    protected function addElements()
    {
        if ($this->captcha_enable) {
            $this->add($this->getCaptcha());
        } else {
            $this->add([
                'type' => 'hidden',
                'name' => 'captcha',
            ]);
        }

        $this->add([
            'type' => 'text',
            'name' => 'login',
            'attributes' => [
                'id' => self::CLASS_PREFIX.'-login',
                'class'=>'form-control',
                'placeholder'=>'Введите логин, email или телефон'
            ],
            'options' => [
                'label' => 'E-mail/Телефон/Логин',
            ],
        ]);

        // Add "password" field
        $this->add([
            'type'  => 'password',
            'name' => 'password',
            'attributes' => [
                'id' => self::CLASS_PREFIX.'-password',
                'class'=>'form-control',
                'placeholder'=>'Введите пароль'
            ],
            'options' => [
                'label' => 'Пароль',
            ],
        ]);

        $this->add([
            'type'  => 'checkbox',
            'name' => 'remember_me',
            'options' => [
                'label' => 'Запомнить меня',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'id' => self::CLASS_PREFIX.'-submit',
                'value' => 'Войти',
                'class'=>'btn btn-blue'
            ],
        ]);
    }

    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        if ($this->captcha_enable) {
            $inputFilter->add([
                'name' => 'captcha',
                'required' => !$this->config['recaptcha_v2']['bypass'],
                'filters' => [
                    ['name' => 'StringTrim'],
                ],
            ]);
        } else {
            $inputFilter->add([
                'name' => 'captcha',
                'required' => false,
                'filters' => [],
            ]);
        }

        $inputFilter->add([
            'name'     => 'login',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 3]],
                [
                    'name' => LoginValidator::class,
                    'options' => [
                        'callback' => [$this, 'loginValidator'],
                        'message' => BaseExceptionCodeInterface::AUTHENTICATION_FAILED_MESSAGE
                    ]
                ]
            ]
        ]);

        $inputFilter->add([
            'name'     => 'password',
            'required' => true,
            'filters'  => [
            ],
            'validators' => [
                ['name'    => 'StringLength', 'options' => ['min' => 6, 'max' => 64]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[a-zA-Z0-9]$/',
                        'message' => 'Недопустимые символы в пароле']
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'remember_me',
            'required' => false,
            'filters'  => [],
            'validators' => [
                [
                    'name'    => 'InArray',
                    'options' => [
                        'haystack' => [0, 1],
                    ]
                ],
            ],
        ]);
    }

    /**
     * @param $value
     * @return bool
     */
    public function loginValidator($value): bool
    {
        return $this->baseAuthManager->checkLoginExist($value);
    }
}