<?php
namespace ModuleAuthV2\Form\Validator;

use Zend\Crypt\Password\Bcrypt;
use Zend\Validator\AbstractValidator;

/**
 * Class IdenticalPasswordValidator
 * @package ModuleAuthV2\Form\Validator
 */
class IdenticalPasswordValidator extends AbstractValidator
{
    const PASSWORD_IDENTICAL_TO_CURRENT = 'passwordIdenticalToCurrent';

    protected $options = [
        'current_password_hash' => null,
    ];

    protected $messageTemplates = [
        self::PASSWORD_IDENTICAL_TO_CURRENT  => 'Новый пароль должен отличаться от текущего',
    ];

    /**
     * IdenticalPasswordValidator constructor.
     * @param array $options
     */
    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    /**
     * @param mixed $new_password
     * @return bool
     */
    public function isValid($new_password)
    {
        if (!$new_password) {

           return false;
        }

        $bCrypt = new Bcrypt();
        $bCrypt->create('qwerty');
        if ($bCrypt->verify($new_password, $this->options['current_password_hash'])) {

            $this->error(self::PASSWORD_IDENTICAL_TO_CURRENT);
            return false;
        }

        return true;
    }
}