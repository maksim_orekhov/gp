<?php
namespace ModuleAuthV2\Form\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class IdenticalCallback extends AbstractValidator
{
    /**
     * Invalid value
     */
    const NOT_IDENTICAL = 'notIdentical';
    const INVALID_CALLBACK = 'callbackInvalid';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_IDENTICAL    => 'Not identical',
        self::INVALID_CALLBACK => 'An exception has been raised within the callback',
    ];

    /**
     * Default options to set for the validator
     *
     * @var mixed
     */
    protected $options = [
        'callback' => null,
    ];

    /**
     * Constructor
     *
     * @param array|callable $options
     */
    public function __construct($options = null)
    {
        if (\is_callable($options)) {
            $options = ['callback' => $options];
        }

        parent::__construct($options);
    }

    /**
     * Returns the set callback
     *
     * @return mixed
     */
    public function getCallback()
    {
        return $this->options['callback'];
    }

    /**
     * Sets the callback
     *
     * @param  string|array|callable $callback
     * @return Callback Provides a fluent interface
     * @throws Exception\InvalidArgumentException
     */
    public function setCallback($callback)
    {
        if (! \is_callable($callback)) {
            throw new Exception\InvalidArgumentException('Invalid callback given');
        }

        $this->options['callback'] = $callback;
        return $this;
    }

    /**
     * Returns true if and only if the set callback returns
     * for the provided $value
     *
     * @param  mixed $value
     * @param  mixed $context Additional context to provide to the callback
     * @return bool
     * @throws Exception\InvalidArgumentException
     */
    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $callback = $this->getCallback();
        if (empty($callback)) {
            throw new Exception\InvalidArgumentException('No callback given');
        }

        $args = [$value];
        if (! empty($context)) {
            $args[] = $context;
        }

        try {
            if (! \call_user_func_array($callback, $args)) {
                $this->error(self::NOT_IDENTICAL);
                return false;
            }
        } catch (\Exception $e) {
            $this->error(self::INVALID_CALLBACK);
            return false;
        }

        return true;
    }
}
