<?php
namespace ModuleAuthV2\Form;

use ModuleAuthV2\Module;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class PhoneEditForm
 * @package ModuleAuthV2\Form
 */
class PhoneEditForm extends Form
{
    const CLASS_PREFIX = Module::MODULE_NAME;

    public function __construct()
    {
        // Define form name
        parent::__construct(self::CLASS_PREFIX.'phone-edit-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', self::CLASS_PREFIX.'-phone-edit-form');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Current_phone
        $this->add([
            'type'  => 'hidden',
            'name' => 'current_phone',
        ]);

        // New phone
        $this->add([
            'type' => 'text',
            'name' => 'new_phone',
            'attributes' => [
                'id' => self::CLASS_PREFIX.'-new-phone',
                'class' => 'form-control',
                'placeholder' => 'Новый номер',
            ],
            'options' => [
                'label' => 'Введите новый номер',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Confirm',
                'id' => self::CLASS_PREFIX.'-submit',
            ],
        ]);
    }

    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'current_phone',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name'    => \Core\Form\Validator\PhoneValidator::class,
                ],
            ],
        ]);
        $inputFilter->add([
            'name'     => 'new_phone',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                [
                    'name' =>  'PregReplace',
                    'options' => [
                        'pattern' => '/(\()|(\))|(\-)/',
                        'replacement' => ''
                    ]
                ],
            ],
            'validators' => [
                [
                    'name'    => 'Regex',
                    'options' => [
                        'pattern' => '/\d{11}/',
                        'message' => 'Номер телефона заполнен не полностью'
                    ]
                ],
            ],
        ]);
    }
}