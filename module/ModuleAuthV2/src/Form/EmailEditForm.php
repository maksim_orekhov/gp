<?php
namespace ModuleAuthV2\Form;

use ModuleAuthV2\Module;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class EmailEditForm
 * @package ModuleAuthV2\Form
 */
class EmailEditForm extends Form
{
    const CLASS_PREFIX = Module::MODULE_NAME;

    public function __construct()
    {
        // Define form name
        parent::__construct(self::CLASS_PREFIX.'-email-edit-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', self::CLASS_PREFIX.'-email-edit-form');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Current_phone
        $this->add([
            'type'  => 'hidden',
            'name' => 'current_email',
        ]);

        // New phone
        $this->add([
            'type' => 'text',
            'name' => 'new_email',
            'attributes' => [
                'id' => self::CLASS_PREFIX.'-new_email',
                'class' => 'form-control',
                'placeholder' => 'Новый email',
            ],
            'options' => [
                'label' => 'Введите новый email',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Confirm',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'current_email',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name'    => 'EmailAddress',
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'new_email',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name'    => 'EmailAddress',
                ],
            ],
        ]);
    }
}