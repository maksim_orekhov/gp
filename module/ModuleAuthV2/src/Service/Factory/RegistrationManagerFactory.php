<?php
namespace ModuleAuthV2\Service\Factory;

use Core\EventManager\AuthEventProvider;
use Core\Service\Base\BaseEmailManager;
use ModuleAuthV2\Service\RegistrationManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Application\Service\UserManager;

/**
 * Class RegistrationManagerFactory
 * @package ModuleAuthV2\Service\Factory
 */
class RegistrationManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserManager|object
     */
    public function __invoke(ContainerInterface $container,
                             $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $authEventProvider = $container->get(AuthEventProvider::class);
        $baseEmailManager = $container->get(BaseEmailManager::class);

        return new RegistrationManager(
            $entityManager,
            $authEventProvider->getEventManager(),
            $baseEmailManager
        );
    }
}
