<?php
namespace ModuleAuthV2\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Entity\Interfaces\UserInterface;
use Core\Exception\LogicException;
use Core\Provider\Token\EncodedTokenProviderInterface;
use Core\Service\Base\BaseAuthManager;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BaseTokenManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\TwigViewModel;
use Zend\View\Model\ViewModel;
use ModuleAuthV2\Form\PasswordResetForm;
use ModuleAuthV2\Form\PasswordChangeForm;
use Zend\View\Model\JsonModel;

/**
 * Class PasswordResetController
 * @package ModuleAuthV2\Controller
 */
class PasswordResetController extends AbstractRestfulController
{
    const CODE_OPERATION_TYPE_NAME  = 'PASSWORD_RESET_CONFIRMATION';
    const SUCCESS_TOKEN_SENT        = 'Ссылка для сброса пароля, отправлена на ваш электронный адрес';
    const ERROR_TOKEN_SENT          = 'Token sending fails';
    const ERROR_TOO_FREQUENT_RESET  = 'Not enough time has passed since the date of the last password reset';
    const SUCCESS_PASSWORD_CHANGED  = 'Password changed';
    const ERROR_NO_USER_DEFINED     = 'No user defined';

    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var BaseTokenManager
     */
    private $baseTokenManager;
    /**
     * @var EncodedTokenProviderInterface
     */
    private $encodedTokenProvider;
    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;


    /**
     * PasswordResetController constructor.
     * @param BaseUserManager $baseUserManager
     * @param BaseTokenManager $baseTokenManager
     * @param EncodedTokenProviderInterface $encodedTokenProvider
     * @param BaseEmailManager $baseEmailManager
     * @param BaseAuthManager $baseAuthManager
     */
    public function __construct(BaseUserManager $baseUserManager,
                                BaseTokenManager $baseTokenManager,
                                EncodedTokenProviderInterface $encodedTokenProvider,
                                BaseEmailManager $baseEmailManager,
                                BaseAuthManager $baseAuthManager)
    {
        $this->baseUserManager      = $baseUserManager;
        $this->baseTokenManager     = $baseTokenManager;
        $this->encodedTokenProvider = $encodedTokenProvider;
        $this->baseEmailManager     = $baseEmailManager;
        $this->baseAuthManager      = $baseAuthManager;
    }

    /**
     * @return TwigViewModel|JsonModel|ViewModel
     * @throws \Exception
     */
    public function resetPasswordAction()
    {
        $isAjax =  $this->getRequest()->isXmlHttpRequest();
        try {
            // Create form
            $form = new PasswordResetForm();
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);
                if ( $form->isValid() ) {
                    $data = $form->getData();
                    $email = $this->baseEmailManager->getEmailByValue($data['email']);
                    if ($email === null ) {
                        throw new LogicException(null, LogicException::EMAIL_NOT_FOUND);
                    }
                    $user = $this->baseUserManager->getUserByEmail($email);
                    if ($user === null) {
                        throw new LogicException(null, LogicException::USER_NOT_FOUND);
                    }
                    // Create token and send mail
                    $this->encodedTokenProvider->provideToken($user);

                    return $this->message()->success(self::SUCCESS_TOKEN_SENT);
                }
                if($isAjax){

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
            if($isAjax){
                $data = ['csrf' => $form->get('csrf')->getValue()];

                return $this->message()->success('CSRF token return', $data);
            }
        } catch (\Throwable $throwable) {

            return $this->message()->exception($throwable);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'passwordResetForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Set new password
     *
     * @return \Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function setPasswordAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        $isPost = $this->getRequest()->isPost();
        try {
            // Get user provided token from link
            $user_token = trim($this->params()->fromQuery('token', null));
            // If no token provided
            if(!$user_token) {
                return $this->message()->error('No token provided');
            }
            // Decode token
            $decoded_token = $this->baseTokenManager->decodePasswordResetToken($user_token);
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByPasswordResetToken($decoded_token['simple_token']);
            // Create form
            $form = new PasswordChangeForm($user);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);
                if($form->isValid()) {
                    $data = $form->getData();
                    $this->baseTokenManager->validatePasswordResetToken($decoded_token, $user);
                    $this->baseUserManager->setUserPasswordByToken($user, $data['new_password']);
                    // Автоматически залогиниваем
                    $this->baseAuthManager->userAutoLogin($user);

                    return $this->message()->success(self::SUCCESS_PASSWORD_CHANGED);
                }
                if($isAjax) {
                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
            if($isAjax) {
                $data = ['csrf' => $form->get('csrf')->getValue()];
                return $this->message()->success('CSRF token return', $data);
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'passwordChangeForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        return $view;
    }
}
