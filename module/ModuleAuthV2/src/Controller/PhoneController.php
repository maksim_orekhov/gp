<?php
namespace ModuleAuthV2\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Entity\Interfaces\UserInterface;
use Core\Exception\LogicException;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseUserManager;
use Core\Service\CodeOperationInterface;
use Core\Service\SessionContainerManager;
use Core\Service\TwigViewModel;
use ModuleAuthV2\Form\PhoneCreateForm;
use ModuleAuthV2\Form\PhoneEditForm;
use Core\Service\Base\BaseAuthManager;
use Zend\EventManager\EventManager;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Core\Service\OperationConfirmManager;
use Core\Form\OperationConfirmForm;
use Core\EventManager\AuthEventProvider as AuthEvent;

/**
 * Class PhoneController
 * @package ModuleAuthV2\Controller
 */
class PhoneController extends AbstractRestfulController implements CodeOperationInterface
{
    // Messages
    const ERROR_NO_VALID_DATA_PROVIDED      = 'No valid data provided';
    const SUCCESS_PHONE_CONFIRMED           = 'Phone confirmed';
    const SUCCESS_OPERATION_CONFIRMED       = 'Operation confirmed';
    const ERROR_CONFIRMATION_FAILED         = 'Confirmation failed';
    const ERROR_NO_USER_DEFINED             = 'No user defined';

    /**
     * @var BaseUserManager
     */
    private $baseUserManager;

    /**
     * @var BasePhoneManager
     */
    private $basePhoneManager;

    /**
     * Auth manager.
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * $sessionContainerManager
     * @var SessionContainerManager
     */
    private $sessionContainerManager;

    /**
     * $operationConfirmManager
     * @var OperationConfirmManager
     */
    private $operationConfirmManager;
    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * PhoneController constructor.
     * @param BaseUserManager $baseUserManager
     * @param BasePhoneManager $basePhoneManager
     * @param BaseAuthManager $baseAuthManager
     * @param SessionContainerManager $sessionContainerManager
     * @param OperationConfirmManager $operationConfirmManager
     * @param EventManager $eventManager
     */
    public function __construct(BaseUserManager $baseUserManager,
                                BasePhoneManager $basePhoneManager,
                                BaseAuthManager $baseAuthManager,
                                SessionContainerManager $sessionContainerManager,
                                OperationConfirmManager $operationConfirmManager,
                                EventManager $eventManager)
    {
        $this->baseUserManager         = $baseUserManager;
        $this->basePhoneManager        = $basePhoneManager;
        $this->baseAuthManager         = $baseAuthManager;
        $this->sessionContainerManager = $sessionContainerManager;
        $this->operationConfirmManager = $operationConfirmManager;
        $this->eventManager            = $eventManager;
    }

    /**
     * create phone
     *
     * @return \Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function createAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            $codeType = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION;
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if ($user === null) {
                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            if ( $user->getPhone() !== null) {

                throw new LogicException(null, LogicException::PHONE_ALREADY_EXISTS);
            }

            $form = new PhoneCreateForm();

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);

                if($form->isValid()) {
                    $data = $form->getData();
                    // If phone is not unique
                    if (!$this->basePhoneManager->checkPhoneUnique($data['phone'])) {

                        throw new LogicException(null, LogicException::PHONE_NOT_UNIQUE);
                    }
                    $phone = $this->basePhoneManager->addNewPhone($data['phone']);
                    $user = $this->baseUserManager->setPhoneToUser($user, $phone);

                    $this->eventManager->trigger(AuthEvent::EVENT_VERIFY_USER_CONFIRMS, $this, [
                        'identity' => $user->getLogin()
                    ]);
                    // Set initiator to session array
                    $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
                        $codeType,
                        $user->getLogin()
                    );
                    $this->operationConfirmManager->sendCode(
                        $user->getLogin(),
                        $codeType
                    );

                    if ($isAjax) {

                        return $this->message()->success('Phone created');
                    }

                    return $this->redirect()->toRoute('phone/confirm');
                }
                // Ajax -
                if($isAjax){

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
            if($isAjax){

                return $this->message()->success('CSRF token return', ['csrf' => $form->get('csrf')->getValue()]);
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'createPhoneForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Edit phone
     *
     * @return \Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function editAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            $codeType = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE;
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if ($user === null) {
                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            $phone = $user->getPhone();
            if ($phone === null) {

                throw new LogicException(null, LogicException::PHONE_NOT_FOUND);
            }
            $form = new PhoneEditForm();
            $form->setData([
                'current_phone' => '+'.$phone->getPhoneNumber(),
            ]);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);

                if($postData['current_phone'] !== $postData['new_phone'] && $form->isValid()) {
                    $data = $form->getData();
                    // If phone is not unique
                    if (!$this->basePhoneManager->checkPhoneUnique($data['new_phone'])) {

                        throw new LogicException(null, LogicException::PHONE_NOT_UNIQUE);
                    }
                    // Get fon by provided current phone number
                    $currentPhone = $this->basePhoneManager->getPhoneByNumber($data['current_phone']);
                    // Check whether user provide correct phone number
                    if ($user->getPhone() !== $currentPhone) {

                        throw new LogicException(null, LogicException::PHONE_ENTERED_NOT_BELONG_USER);
                    }
                    // Save phone number to session
                    $this->sessionContainerManager->setSessionVar('new_phone_number', $data['new_phone']);
                    // Set initiator to session array
                    $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
                        $codeType,
                        $user->getLogin()
                    );
                    // Send confirmation code by email
                    $this->operationConfirmManager->sendCodeByEmail(
                        $user->getLogin(),
                        $codeType
                    );

                    if ($isAjax) {

                        return $this->message()->success('Email with confirmation code has been send');
                    }
                    // Generate view from changeConfirmationCodeAction
                    return $this->forward()->dispatch(self::class, array(
                        'action' => 'confirmationEditCode'
                    ));
                }
                // Ajax -
                if($isAjax){

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
            $user_allowed_request_code_status = $this->operationConfirmManager->userAllowedRequestCodeStatus(
                $user->getLogin(),
                $codeType
            );
            if($isAjax){
                $data = [
                    'csrf' => $form->get('csrf')->getValue(),
                    'user_allowed_request_code_status' => $user_allowed_request_code_status
                ];
                return $this->message()->success('CSRF token return', $data);
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'formPhone' => $form,
            'user_allowed_request_code_status' => $user_allowed_request_code_status
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function confirmationEditCodeAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            $codeType = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE;
            if (!$this->sessionContainerManager->isProcessInitiatorExists($codeType) ) {

                throw new LogicException(null, LogicException::SESSION_CODE_OPERATION_TYPE_NOT_FOUND);
            }
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if ($user === null) {

                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            $phone = $user->getPhone();
            if ($phone === null) {

                throw new LogicException(null, LogicException::PHONE_NOT_FOUND);
            }

            $form = new OperationConfirmForm();
            $form->setAttribute('action', $this->url()->fromRoute('phone/edit-confirm'));

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);
                if($form->isValid()){
                    $data = $form->getData();
                    $current_phone_value = $phone->getPhoneNumber();
                    $new_phone_value = $this->sessionContainerManager->getSessionVar('new_phone_number');
                    if ( !$new_phone_value ) {

                        throw new LogicException(null, LogicException::SESSION_VARIABLE_NOT_FOUND);
                    }
                    $this->operationConfirmManager->checkCode($user, $data['code'], $codeType);
                    $this->basePhoneManager->editUsersPhone($current_phone_value, $new_phone_value);
                    $this->eventManager->trigger(AuthEvent::EVENT_VERIFY_USER_CONFIRMS, $this, [
                        'identity' => $user->getLogin()
                    ]);
                    // Remove session data
                    $this->sessionContainerManager->deleteProcessInitiatorForCurrentCodeOperationType($codeType);
                    $this->sessionContainerManager->deleteSessionVar('new_phone_number');

                    //// create new session cookie /////
                    // Set initiator to session array
                    $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
                        self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION,
                        $user->getLogin()
                    );
                    $this->operationConfirmManager->sendCode(
                        $user->getLogin(),
                        self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION
                    );

                    if ($isAjax) {
                        return $this->message()->success('Operation confirmed');
                    }

                    return $this->redirect()->toRoute('phone/confirm');
                }
                if($isAjax) {

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
            $user_allowed_request_code_status = $this->operationConfirmManager->userAllowedRequestCodeStatus(
                $user->getLogin(),
                $codeType
            );
            if($isAjax){
                $data   = [
                    'csrf' => $form->get('csrf')->getValue(),
                    'user_allowed_request_code_status' => $user_allowed_request_code_status
                ];

                return $this->message()->success('CSRF token return', $data);
            }

        } catch (\Throwable $throwable) {

            return $this->message()->exception($throwable);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'form' => $form,
            'resend_url' => $this->url()->fromRoute('phone/edit-code-resend'),
            'user_allowed_request_code_status' => $user_allowed_request_code_status
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('module-auth-v2/operation-confirm/confirm');

        return $view;
    }

    /**
     * Edit phone confirmation code resend
     * @throws \Exception
     */
    public function confirmationEditCodeResendAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            $codeType = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CHANGE;
            $login = $this->sessionContainerManager->getLoginByOperationType($codeType);
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($login);
            if ($user === null) {
                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            // Send confirmation code by email
            $this->operationConfirmManager->sendCodeByEmail($login, $codeType);

            if ($isAjax) {

                return $this->message()->success('SMS with new confirmation code has been send');
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->redirect()->toRoute('phone/edit-confirm');
    }

    /**
     * @return \Zend\Http\Response|\Zend\EventManager\ResponseCollection|ViewModel
     * @throws \Exception
     */
    public function confirmAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            $codeType = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION;
            if (!$this->sessionContainerManager->isProcessInitiatorExists($codeType) ) {

                throw new LogicException(null, LogicException::SESSION_CODE_OPERATION_TYPE_NOT_FOUND);
            }
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if ($user === null) {
                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            $phone = $user->getPhone();
            if ($phone === null) {

                throw new LogicException(null, LogicException::PHONE_NOT_FOUND);
            }

            // Form creation
            $form = new OperationConfirmForm();
            $form->setAttribute('action', $this->url()->fromRoute('phone/confirm'));

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                // Fill form with incoming data
                $form->setData($postData);
                if($form->isValid()) {
                    $postData = $form->getData();
                    // Code confirmation
                    $this->operationConfirmManager->checkCode($user, $postData['code'], $codeType);
                    $this->basePhoneManager->setPhoneConfirmed($phone);
                    $this->eventManager->trigger(AuthEvent::EVENT_VERIFY_USER_CONFIRMS, $this, [
                        'identity' => $user->getLogin()
                    ]);

                    $this->sessionContainerManager->deleteProcessInitiatorForCurrentCodeOperationType($codeType);

                    if($isAjax){

                        return $this->message()->success(self::SUCCESS_PHONE_CONFIRMED);
                    }

                    return $this->redirect()->toRoute('profile');
                }
                if($isAjax){
                    return $this->message()->invalidFormData($form->getMessages());
                }
            }

            /** @var array $user_allowed_request_code_status */
            $user_allowed_request_code_status = $this->operationConfirmManager->userAllowedRequestCodeStatus(
                $user->getLogin(),
                $codeType
            );
            if($isAjax){
                $data   = [
                    'csrf' => $form->get('csrf')->getValue(),
                    'user_allowed_request_code_status' => $user_allowed_request_code_status
                ];
                return $this->message()->success('CSRF token return', $data);
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'form' => $form,
            'resend_url' => $this->url()->fromRoute('phone/code-resend'),
            'user_allowed_request_code_status' => $user_allowed_request_code_status
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        $view->setTemplate('module-auth-v2/operation-confirm/confirm');

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function confirmationCodeResendAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            $codeType = self::CODE_OPERATION_TYPE_NAME_FOR_PHONE_CONFIRMATION;
            // If no $encoded_token passed
            if ($this->baseAuthManager->getIdentity()) {
                // Если авторизован
                $login = $this->baseAuthManager->getIdentity();
            } else {
                $login = $this->sessionContainerManager->getLoginByOperationType($codeType);
            }
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($login);

            if ($user === null) {
                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            $phone = $user->getPhone();
            if ($phone === null) {

                throw new LogicException(null, LogicException::PHONE_NOT_FOUND);
            }

            $this->sessionContainerManager->addProcessInitiatorWithCurrentCodeOperationType(
                $codeType,
                $user->getLogin()
            );
            // Resend confirmation code
            $this->operationConfirmManager->sendCode($user->getLogin(), $codeType);

            if ($isAjax) {

                return $this->message()->success('SMS with new confirmation code has been send');
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->redirect()->toRoute('phone/confirm');
    }
}