<?php
namespace ModuleAuthV2\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Service\Base\BaseEmailManager;
use Core\Service\Base\BasePhoneManager;
use Core\Service\Base\BaseUserManager;
use Zend\View\Model\JsonModel;
use Zend\Json\Json;

/**
 * Class UserRegistrationCheckedController
 * @package ModuleAuthV2\Controller
 */
class UserRegistrationCheckedController extends AbstractRestfulController
{
    const ERROR_CHECK_EMAIL_EXISTS = 'Email does not exists';
    const SUCCESS_CHECK_EMAIL_EXISTS = 'Email exists';
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var BasePhoneManager
     */
    private $basePhoneManager;
    /**
     * @var BaseEmailManager
     */
    private $baseEmailManager;

    /**
     * PhoneController constructor.
     * @param BaseUserManager $baseUserManager
     * @param BasePhoneManager $basePhoneManager
     * @param BaseEmailManager $baseEmailManager
     */
    public function __construct(BaseUserManager $baseUserManager,
                                BasePhoneManager $basePhoneManager,
                                BaseEmailManager $baseEmailManager)
    {
        $this->baseUserManager = $baseUserManager;
        $this->basePhoneManager = $basePhoneManager;
        $this->baseEmailManager = $baseEmailManager;
    }

    /**
     * Точка проверки уникальности Email
     * /register/check/email
     * @return \Zend\Http\Response|JsonModel
     */
    public function checkIsEmailUniqueAction()
    {
        //only ajax and post
        $this->checkedOnlyAjaxAndPost();
        $content = $this->getRequest()->getContent();
        try {
            $data = Json::decode($content, Json::TYPE_ARRAY);

            if(array_key_exists('email', $data)) {
                $data['email'] = strtolower($data['email']);
            }

            if ($this->baseEmailManager->checkEmailUnique($data['email'])) {
                return new JsonModel([
                    'status' => 'SUCCESS',
                    'code' => null,
                    'message' => 'Email is available',
                    'request' => $data,
                ]);
            }

            return new JsonModel([
                'status' => 'ERROR',
                'code' => null,
                'message' => 'Email already taken',
                'request' => $data,
            ]);

        } catch (\Throwable $t) {

            return new JsonModel([
                'status' => 'ERROR',
                'code' => null,
                'server_error'  => true,
                'message' => $t->getMessage(),
                'request' => $content,
            ]);
        }
    }

    /**
     * @return array|void
     */
    private function checkedOnlyAjaxAndPost()
    {
        //only ajax and post
        /** @noinspection PhpUndefinedMethodInspection */
        if (!$this->getRequest()->isXmlHttpRequest() || !$this->getRequest()->isPost()) {
            /** @noinspection PhpUndefinedMethodInspection */
            $this->response->setStatusCode(404);
            /** @noinspection PhpInconsistentReturnPointsInspection */
            return ['content' => 'Method not supported'];
        }
        /** @noinspection UselessReturnInspection */
        return;
    }
}