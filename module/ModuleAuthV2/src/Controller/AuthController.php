<?php
namespace ModuleAuthV2\Controller;

use Core\Controller\AbstractRestfulController;
use Core\Entity\Interfaces\UserInterface;
use Core\Controller\Plugin\Message\MessagePlugin;
use Core\EventManager\AuthEventProvider;
use Core\Exception\LogicException;
use Core\Service\Base\BaseUserManager;
use Core\Service\CodeOperationInterface;
use Core\Service\SessionContainerManager;
use Core\Service\TwigViewModel;
use Core\Service\Base\BaseAuthManager;
use ModuleAuthV2\Form\LoginEditForm;
use Core\EventManager\AuthEventProvider as AuthEvent;
use Zend\Form\Element\Captcha;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use ModuleAuthV2\Form\LoginForm;
use Zend\View\Model\JsonModel;

/**
 * This controller is responsible for letting the user to log in and log out.
 */
class AuthController extends AbstractRestfulController implements CodeOperationInterface
{
    const MAX_LOGIN_COUNT_ATTEMPTS_WITHOUT_CAPTCHA = 3;

    const SUCCESS_USER_LOG_IN           = 'Log in successfully';
    const SUCCESS_USER_AUTHENTICATED    = 'Authenticated successfully';
    const ERROR_INVALID_DATA_PROVIDED   = 'Invalid data provided';
    const ERROR_NO_USER_DEFINED         = 'No user defined';

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;
    /**
     * @var SessionContainerManager
     */
    private $sessionContainerManager;
    /**
     * @var AuthEventProvider
     */
    private $authEventProvider;
    /**
     * @var array
     */
    private $config;

    /**
     * AuthController constructor.
     * @param BaseAuthManager $baseAuthManager
     * @param BaseUserManager $baseUserManager
     * @param SessionContainerManager $sessionContainerManager
     * @param AuthEventProvider $authEventProvider
     * @param array $config
     */
    public function __construct(BaseAuthManager $baseAuthManager,
                                BaseUserManager $baseUserManager,
                                SessionContainerManager $sessionContainerManager,
                                AuthEventProvider $authEventProvider,
                                array $config)
    {
        $this->baseAuthManager         = $baseAuthManager;
        $this->baseUserManager         = $baseUserManager;
        $this->sessionContainerManager = $sessionContainerManager;
        $this->authEventProvider = $authEventProvider;
        $this->config = $config;
    }

    /**
     * @return TwigViewModel|JsonModel|ViewModel
     */
    public function csrfTokenAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        $isGet = $this->getRequest()->isGet();

        if ($isAjax && $isGet) {
            $csrf = new \Zend\Form\Element\Csrf('csrf');
            $csrf->setCsrfValidatorOptions([
                'timeout' => 3600
            ]);
            $data   = ['csrf' => $csrf->getValue()];

            return $this->message()->success('CSRF token return', $data);
        }

        $this->getResponse()->setStatusCode(404);
        return;
    }

    /**
     * @return TwigViewModel|mixed|JsonModel|ViewModel
     * @throws \Exception
     */
    public function loginAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        $logic_exception = null;
        $captcha_enable = $this->cookie()->get('login_captcha_enable') ?? false;

        try {
            // Автоматический редирект
            $this->authEventProvider->getEventManager()->trigger(AuthEvent::EVENT_BEFORE_AUTH, $this);
            /** @var MvcEvent $mvcEvent */
            $mvcEvent = $this->getEvent();
            $logged_in_user_redirection_url = $mvcEvent->getParam('logged_in_user_redirection_url', null);
            if (null !== $logged_in_user_redirection_url) {
                if ($isAjax) {
                    $data = [
                        'redirect_url' => $logged_in_user_redirection_url
                    ];

                    return $this->message()->success(self::SUCCESS_USER_AUTHENTICATED, $data);
                }
                // Http success
                return $this->redirect()->toUrl($logged_in_user_redirection_url);
            }

            $form = new LoginForm($this->baseAuthManager, $this->config, $captcha_enable);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                $form->setData($postData);

                if ($form->isValid()) {
                    $data = $form->getData();
                    try {
                        $this->baseAuthManager->login($data['login'], $data['password'], $data['remember_me']);

                        $this->cookie()->set('count_failed_login_attempts', null);
                        $this->cookie()->set('login_captcha_enable', null);

                        $this->authEventProvider->getEventManager()->trigger(AuthEvent::EVENT_AFTER_AUTH, $this);
                        $redirect_url = $mvcEvent->getParam('redirect_url', $this->url()->fromRoute('profile'));

                        // Ajax success
                        if ($isAjax) {
                            $data = [
                                'login' => $data['login'],
                                'redirect_url' => $redirect_url
                            ];

                            return $this->message()->success(self::SUCCESS_USER_AUTHENTICATED, $data);
                        }
                        // Http success
                        return $this->redirect()->toUrl($redirect_url);
                    }
                    catch (LogicException $exception) {
                        $logic_exception = $exception;
                    }
                }
                $count_failed_login_attempts = $this->cookie()->get('count_failed_login_attempts') ?? 0;
                $this->cookie()->set('count_failed_login_attempts', ++$count_failed_login_attempts, 60);

                if ($count_failed_login_attempts >= self::MAX_LOGIN_COUNT_ATTEMPTS_WITHOUT_CAPTCHA) {
                    $this->cookie()->set('login_captcha_enable', true, 60);
                    $captcha = $form->get('captcha');

                    if (!$captcha instanceof Captcha) {
                        //remove check to file required
                        $form->add($form->getCaptcha());
                        $captchaFilter = $form->getInputFilter()->get('captcha');
                        $captchaFilter->setRequired(!$this->config['recaptcha_v2']['bypass']);
                    }
                }
                if ($isAjax) {
                    if ($logic_exception) {
                        return $this->message()->exception($logic_exception);
                    }
                    return $this->message()->invalidFormData($form->getMessages());
                }
            }

            // set cookie
            $this->cookie()->set('login-form-data', $postData, 15);
            $this->cookie()->set('login-form-message', $form->getMessages(), 15);

        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'loginForm' => $form,
            'auth_error_message' => $logic_exception ? $logic_exception->getExceptionMessage() : null
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * The "logout" action performs logout operation
     *
     * @return \Zend\Http\Response|JsonModel
     */
    public function logoutAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $this->baseAuthManager->logout();
            // Response for Ajax request
            if($isAjax) {
                return $this->message()->success('Log out done');
            }

            return $this->redirect()->toRoute('login');
        }
        catch (\Throwable $t) {
            return $this->message()->error($t->getMessage());
        }
    }

    /**
     * @return bool|TwigViewModel|\Zend\Http\Response|JsonModel|ViewModel
     * @throws \Exception
     */
    public function editLoginAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();
        try {
            /** @var UserInterface $user */
            $user = $this->baseUserManager->getUserByLogin($this->baseAuthManager->getIdentity());
            if ($user === null) {

                throw new LogicException(null, LogicException::USER_NOT_FOUND);
            }
            $old_login = $user->getLogin();
            $form = new LoginEditForm($user->getLogin());
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            if ($postData) {
                if ($old_login === $postData['login']) {

                    throw new LogicException(null, LogicException::USER_LOGIN_IS_SAME_AS_LOGIN_IN_PROFILE);
                }

                $form->setData($postData);
                if ($form->isValid()) {
                    $data = $form->getData();
                    // Проверка уникальности login на сервере
                    if(!$this->baseUserManager->checkLoginUnique($data['login'])){

                        throw new LogicException(null, LogicException::USER_LOGIN_ALREADY_EXISTS);
                    }
                    //set new login
                    $user = $this->baseUserManager->setLoginValueToUser($user, $data['login']);
                    //////////////////////////////////////////////////////////
                    // если в сессии есть не подтвержденный пользователь    //
                    // совпадаюший со старым логином то меняем его на новый //
                    //////////////////////////////////////////////////////////
                    $unconfirmed_user = $this->sessionContainerManager->getSessionVar('unconfirmed_user');
                    if ( $unconfirmed_user &&  $unconfirmed_user === $old_login) {
                        $this->sessionContainerManager->setSessionVar('unconfirmed_user', $user->getLogin());
                    }
                    // перелогиниваем пользователя с новым логином
                    if (! $this->baseAuthManager->userAutoLogin($user) ) {

                        throw new LogicException(null, LogicException::USER_AUTHORIZATION_FAILED);
                    }

                    // Ajax success
                    if ($isAjax) {
                        $data = [
                            'login' => $user->getLogin(),
                            'redirect_url' => '/profile'
                        ];
                        return $this->message()->success('edit login success', $data);
                    }
                    // Http success
                    return $this->redirect()->toUrl('/profile');
                }
                if($isAjax){

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
        } catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $form->prepare();
        $view = new TwigViewModel([
            'loginEditForm' => $form
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * Not Authorized page.
     * @throws \Exception
     */
    public function notAuthorizedAction()
    {
        $this->getResponse()->setStatusCode(403);
        if(! $this->getRequest()->isXmlHttpRequest()){

            $view = new TwigViewModel([]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        }else{

            return $this->message()->error(null, ['redirectUrl' => null], MessagePlugin::ERROR_NOT_AUTHORIZED);
        }

        return $view;
    }

    /**
     * @return TwigViewModel|JsonModel|ViewModel
     */
    public function accessDeniedAction()
    {
        if (! $this->getRequest()->isXmlHttpRequest()) {

            $view = new TwigViewModel();
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        } else {

            return $this->message()->error(null, null, MessagePlugin::ERROR_ACCESS_DENIED);
        }

        return $view;
    }
}