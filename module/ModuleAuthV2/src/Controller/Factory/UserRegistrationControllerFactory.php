<?php
namespace ModuleAuthV2\Controller\Factory;

use Core\EventManager\AuthEventProvider;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use ModuleAuthV2\Service\RegistrationManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleAuthV2\Controller\UserRegistrationController;

/**
 * Class UserRegistrationControllerFactory
 * @package ModuleAuthV2\Controller\Factory
 */
class UserRegistrationControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserRegistrationController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $userRegistrationManager = $container->get(RegistrationManager::class);
        $authEventProvider = $container->get(AuthEventProvider::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $config = $container->get('config');

        return new UserRegistrationController(
            $entityManager,
            $authEventProvider->getEventManager(),
            $userRegistrationManager,
            $baseAuthManager,
            $config
        );
    }
}
