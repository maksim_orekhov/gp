<?php
namespace ModuleAuthV2\Controller;

use Core\Controller\AbstractRestfulController;
use Core\EventManager\AuthEventProvider as AuthEvent;
use Core\Exception\LogicException;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use Doctrine\ORM\EntityManager;
use ModuleAuthV2\Service\RegistrationManager;
use Zend\EventManager\EventManager;
use Zend\Mvc\MvcEvent;
use ModuleAuthV2\Form\UserRegistrationForm;

/**
 * Class UserRegistrationController
 * @package ModuleAuthV2\Controller
 */
class UserRegistrationController extends AbstractRestfulController
{
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var EventManager
     */
    private $eventManager;
    /**
     * @var RegistrationManager
     */
    private $userRegistrationManager;
    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var array
     */
    private $config;

    /**
     * UserRegistrationController constructor.
     * @param EntityManager $entityManager
     * @param EventManager $eventManager
     * @param RegistrationManager $userRegistrationManager
     * @param BaseAuthManager $baseAuthManager
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                EventManager $eventManager,
                                RegistrationManager $userRegistrationManager,
                                BaseAuthManager $baseAuthManager,
                                array $config)
    {
        $this->entityManager           = $entityManager;
        $this->eventManager            = $eventManager;
        $this->userRegistrationManager = $userRegistrationManager;
        $this->baseAuthManager         = $baseAuthManager;
        $this->config = $config;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Zend\Form\Exception\DomainException
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function createAction()
    {
        try {
            // DB Transaction start
            $this->entityManager->getConnection()->beginTransaction();

            $this->eventManager->trigger(AuthEvent::EVENT_BEFORE_REGISTRATION, $this);

            /** @var MvcEvent $mvcEvent */
            $mvcEvent = $this->getEvent();
            $form = new UserRegistrationForm($mvcEvent->getParam('user_email', null), $this->config);

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];

            if ($postData) {
                if (array_key_exists('email', $postData)) {
                    $postData['email'] = strtolower($postData['email']);
                }
                // Заполняем форму данными.
                $form->setData($postData);

                if ($form->isValid()) {
                    // Получаем валидированные данные формы.
                    $formData = $form->getData();
                    // Add user to DB
                    $user = $this->userRegistrationManager->addNewUser($formData);
                    //set event
                    $this->eventManager->trigger(AuthEvent::EVENT_SUCCESS_REGISTRATION, $this, ['user' => $user]);
                    // DB Transaction commit
                    $this->entityManager->getConnection()->commit();
                    // user auto login
                    if (! $this->baseAuthManager->userAutoLogin($user) ) {

                        throw new LogicException(null, LogicException::USER_AUTHORIZATION_FAILED);
                    }

                    $redirect_url = $mvcEvent->getParam('redirect_url', $this->url()->fromRoute('profile'));

                    //ajax
                    if ($this->getRequest()->isXmlHttpRequest()) {

                        return $this->message()->success('success create', [
                            'redirect_url' => $redirect_url,
                        ]);
                    }
                    //http
                    return $this->redirect()->toUrl($redirect_url);
                }
                if ($this->getRequest()->isXmlHttpRequest()) {

                    return $this->message()->invalidFormData($form->getMessages());
                }
            }
        }catch (\Throwable $t) {
            // DB Transaction rollback
            $this->entityManager->getConnection()->rollBack();
            $this->entityManager->getConnection()->close();

            return $this->message()->exception($t);
        }

        if($this->getRequest()->isXmlHttpRequest()){

            return $this->message()->success('CSRF token return', ['csrf' => $form->get('csrf')->getValue()]);
        }

        // HTTP -
        $form->prepare();
        $view = new TwigViewModel([
            'userRegistrationForm' => $form,
        ]);
        $view->setTemplate('module-auth-v2/user-registration/create');
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        //для переопределения View модели
        $this->eventManager->trigger(AuthEvent::EVENT_VIEW_MODEL_REGISTRATION, $this, [
            'view' => $view,
        ]);

        return $view;
    }
}
