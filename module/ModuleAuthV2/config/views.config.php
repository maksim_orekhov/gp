<?php
namespace ModuleAuthV2;

return [
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
            __DIR__ . '/../viewTwig',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];