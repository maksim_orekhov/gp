<?php
namespace ModuleAuthV2;

return [
    // The multifactor authorization on/off
    'multifactor_authorization' => false,
    // The multifactor password resetting on/off
    'multifactor_password_reset' => false,
    // роут удаления пользователя on/off
    'delete_user_route' => false,
    'auth_service' => [
        'adapter' => \ModuleAuthV2\Adapter\AuthAdapter::class,
    ],
];
