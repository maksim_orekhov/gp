<?php
namespace Core\Adapter;

use Core\Exception\LogicException;
use \Firebase\JWT\JWT;

/**
 * Class TokenAdapter
 * @package Core\Adapter
 */
class TokenAdapter
{
    /**
     * @var array
     */
    private $config;

    /**
     * TokenAdapter constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @param array $data
     * @return string
     * @throws LogicException
     */
    public function createToken(array $data)
    {
        if (!is_array($data) || empty($data)) {

            throw new LogicException(null, LogicException::TOKEN_DATA_FOR_CREATION_IS_EMPTY);
        }

        return JWT::encode($data, $this->config['tokens']['token_encryption_key']);
    }

    /**
     * Decode data from token
     * @param $encoded_token
     * @return object
     * @throws LogicException
     *
     * Возвращает:
     * object(stdClass) // набор свойств показан для токена подтверждения email
     *  public 'email' => string 'r.akhmetov@sim-t.org'
     *  public 'simple_token' => string 's6JaYjaC'
     */
    public function decodeToken($encoded_token)
    {
        try {
            // Decode encoded token
            $decoded_token = JWT::decode($encoded_token, $this->config['tokens']['token_encryption_key'], array('HS256'));
        }
        catch (\Throwable $t){

            throw new LogicException(null, LogicException::TOKEN_NOT_VALID);
        }

        return $decoded_token;
    }
}