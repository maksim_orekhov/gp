<?php
namespace Core\Adapter;

use Core\Exception\LogicException;
use libphonenumber\NumberParseException;
use \libphonenumber\PhoneNumberUtil;
use \libphonenumber\PhoneNumber;

/**
 * Class PhoneNumberUtilAdapter
 * @package Core\Adapter
 */
class PhoneNumberUtilAdapter
{
    /**
     * @var PhoneNumberUtil
     */
    private $phoneNumberUtil;

    /**
     * PhoneNumberUtilAdapter constructor.
     */
    public function __construct()
    {
        $this->phoneNumberUtil = PhoneNumberUtil::getInstance();
    }

    /**
     * @param $phone_value
     * @param int $attempt
     * @return PhoneNumber
     * @throws LogicException
     * @throws NumberParseException
     * @throws \Throwable
     */
    public function getPhoneNumberObj($phone_value, $attempt = 1)
    {
        $phone_value = trim($phone_value);

        if(!preg_match('/^[\+]/', $phone_value, $matches)){

            throw new LogicException(null, LogicException::PHONE_PLUS_SYMBOL_ABSENT);
        }

        // Если приходит российский номер, начинающтйся с 8, то getPhoneNumberObj вернет exception
        // Поэтому после первого exception, если номер начинался на 8, меняем на 7 и повторно пытаемся получить объект getPhoneNumberObj
        try {
            $phoneNumber = $this->phoneNumberUtil->parse($phone_value, null);
        }
        catch (\Throwable $t) {
            if ($attempt === 1 ) {
                $phone_value_new = preg_replace('/[^0-9]/', '', $phone_value);
                if (\strlen($phone_value_new) === 11 && (int)$phone_value_new[0] === 8) {
                    $phone_value_new = '+7'.substr($phone_value_new, 1);
                }

                return $this->getPhoneNumberObj($phone_value_new, 2);
            }

            throw $t;
        }

        return $phoneNumber;
    }

    /**
     * @param $phone_value
     * @return bool
     * @throws LogicException
     * @throws NumberParseException
     * @throws \Throwable
     */
    public function isValidNumber($phone_value)
    {
        $phoneNumberObj = $this->getPhoneNumberObj($phone_value);

        return $this->phoneNumberUtil->isValidNumber($phoneNumberObj);
    }

    /**
     * @param $phoneNumberObj
     * @return null|string
     */
    public function getCountryCode($phoneNumberObj)
    {
        return $this->phoneNumberUtil->getRegionCodeForNumber($phoneNumberObj);
    }

    /**
     * @param $phoneNumberObj
     * @return mixed
     * Пока не используем
     */
    public function getNationalPhoneNumber(PhoneNumber $phoneNumberObj)
    {
        return $phoneNumberObj->getNationalNumber();
    }
}