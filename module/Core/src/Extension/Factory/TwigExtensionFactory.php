<?php
namespace Core\Extension\Factory;

use Core\Extension\TwigExtension;
use ZendTwig\Extension\Extension;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class TwigExtensionFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return TwigExtension|Extension
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new TwigExtension($container->get('config'));
    }
}
