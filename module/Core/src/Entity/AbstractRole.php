<?php
namespace Core\Entity;

use Core\Entity\Interfaces\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractRole
 * @package Core\Entity
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractRole implements RoleInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    protected $description;

    /**
     * Many Roles have Many Roles
     * @ORM\ManyToMany(targetEntity="Application\Entity\Role", inversedBy="parentRoles")
     * @ORM\JoinTable(name="role_hierarchy")
     */
    protected $childRoles;

    /**
     * Many Roles have Many Roles
     * @ORM\ManyToMany(targetEntity="Application\Entity\Role", mappedBy="childRoles")
     */
    protected $parentRoles;

    /**
     * @ORM\ManyToMany(targetEntity="Application\Entity\Permission", inversedBy="roles")
     * @ORM\JoinTable(name="role_permission")
     */
    protected $permissions;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->parentRoles  = new ArrayCollection();
        $this->childRoles   = new ArrayCollection();
        $this->permissions  = new ArrayCollection();
    }

    /**
     * Returns role ID.
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return ArrayCollection
     */
    public function getParentRoles()
    {
        return $this->parentRoles;
    }

    /**
     * Assigns a parent Role to role
     * @param $parentRole
     */
    public function addParentRole($parentRole)
    {
        if (!$this->parentRoles->contains($parentRole)) {
            $this->parentRoles->add($parentRole);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getChildRoles()
    {
        return $this->childRoles;
    }

    /**
     * Assigns a child Role to role
     * @param $childRole
     */
    public function addChildRole($childRole)
    {
        if (!$this->childRoles->contains($childRole)) {
            $this->childRoles->add($childRole);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Assigns a permission to role
     * @param $permission
     */
    public function addPermission($permission)
    {
        if (!$this->permissions->contains($permission)) {
            $this->permissions->add($permission);
        }
    }
}