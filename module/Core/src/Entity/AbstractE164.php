<?php
namespace Core\Entity;

use Application\Entity\E164;
use Core\Entity\Interfaces\E164Interface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractE164
 * @package Core\Entity
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractE164 implements E164Interface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="country_code", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $countryCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="region_code", type="smallint", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $regionCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="phone_number", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $phoneNumber;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countryCode
     *
     * @param integer $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * Get countryCode
     *
     * @return integer
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set regionCode
     *
     * @param integer $regionCode
     */
    public function setRegionCode($regionCode)
    {
        $this->regionCode = $regionCode;
    }

    /**
     * Get regionCode
     *
     * @return integer
     */
    public function getRegionCode()
    {
        return $this->regionCode;
    }

    /**
     * @return int
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param int $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }
}

