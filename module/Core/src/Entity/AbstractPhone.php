<?php
namespace Core\Entity;

use Application\Entity\Country;
use Application\Entity\E164;
use Application\Entity\Phone;
use Core\Entity\Interfaces\PhoneInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Phone
 * @package Core\Entity
 *
 * @ORM\MappedSuperclass
 */
abstract class AbstractPhone implements PhoneInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="user_enter", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    protected $userEnter;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=45, nullable=true)
     */
    protected $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    protected $comment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_verified", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    protected $isVerified;

    /**
     * @var E164
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\E164", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="e164_id", referencedColumnName="id", nullable=true)
     * })
     */
    protected $e164;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    protected $country;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userEnter
     *
     * @param string $userEnter
     */
    public function setUserEnter($userEnter)
    {
        $this->userEnter = $userEnter;
    }

    /**
     * Get userEnter
     *
     * @return string
     */
    public function getUserEnter()
    {
        return $this->userEnter;
    }

    /**
     * Set comment
     *
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set isVerified
     *
     * @param boolean $isVerified
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
    }

    /**
     * Get isVerified
     *
     * @return boolean
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * Set e164
     *
     * @param E164 $e164
     */
    public function setE164(E164 $e164 = null)
    {
        $this->e164 = $e164;
    }

    /**
     * Get e164
     *
     * @return E164
     */
    public function getE164()
    {
        return $this->e164;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
}

