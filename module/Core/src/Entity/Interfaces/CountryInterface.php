<?php
namespace Core\Entity\Interfaces;

/**
 * Interface CountryInterface
 * @package Core\Entity
 */
interface CountryInterface
{
    /**
     * @return int
     */
    public function getId();
    /**
     * @param int $id
     * For fixtures creation
     */
    public function setId($id);
    /**
     * @return string
     */
    public function getName();
    /**
     * @param string $name
     */
    public function setName($name);
    /**
     * @return string
     */
    public function getCode();
    /**
     * @param string $code
     */
    public function setCode($code);
    /**
     * @return string
     */
    public function getCode3();
    /**
     * @param string $code3
     */
    public function setCode3($code3);
    /**
     * @return int
     */
    public function getPhoneCode();
    /**
     * @param int $phoneCode
     */
    public function setPhoneCode($phoneCode);
    /**
     * @return bool
     */
    public function isPostcodeRequired();
    /**
     * @param bool $postcodeRequired
     */
    public function setPostcodeRequired($postcodeRequired);
    /**
     * @return bool
     */
    public function isEu();
    /**
     * @param bool $isEu
     */
    public function setIsEu($isEu);
    /**
     * @return int
     */
    public function getWeight();
    /**
     * @param int $weight
     */
    public function setWeight($weight);
}