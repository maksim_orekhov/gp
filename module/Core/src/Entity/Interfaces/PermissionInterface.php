<?php
namespace Core\Entity\Interfaces;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Interface PermissionInterface
 * @package Core\Entity\Interfaces
 */
interface PermissionInterface
{
    public function __construct();
    /**
     * Returns permission ID.
     * @return integer
     */
    public function getId();
    /**
     * @return string
     */
    public function getName();
    /**
     * @param string $name
     */
    public function setName($name);
    /**
     * @return string
     */
    public function getDescription();
    /**
     * @param string $description
     */
    public function setDescription($description);
    /**
     * @return ArrayCollection
     */
    public function getRoles();
    /**
     * Assigns a role to permission
     * @param $role
     */
    public function addRole($role);
}