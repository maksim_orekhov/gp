<?php
namespace Core\Entity\Interfaces;

use Application\Entity\Country;
use Application\Entity\E164;

/**
 * Interface BasePhoneInterface
 * @package Core\Entity
 */
interface PhoneInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();
    /**
     * Set userEnter
     *
     * @param string $userEnter
     */
    public function setUserEnter($userEnter);
    /**
     * Get userEnter
     *
     * @return string
     */
    public function getUserEnter();
    /**
     * Set comment
     *
     * @param string $comment
     */
    public function setComment($comment);
    /**
     * Get comment
     *
     * @return string
     */
    public function getComment();
    /**
     * Set isVerified
     *
     * @param boolean $isVerified
     */
    public function setIsVerified($isVerified);
    /**
     * Get isVerified
     *
     * @return boolean
     */
    public function getIsVerified();
    /**
     * Set e164
     *
     * @param E164 $e164
     */
    public function setE164(E164 $e164 = null);
    /**
     * Get e164
     *
     * @return E164
     */
    public function getE164();
    /**
     * @return string
     */
    public function getPhoneNumber();
    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber);
    /**
     * @return Country
     */
    public function getCountry();
    /**
     * @param Country $country
     */
    public function setCountry($country);
}