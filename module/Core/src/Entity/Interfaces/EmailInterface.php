<?php
namespace Core\Entity\Interfaces;

use Application\Entity\Email;

/**
 * Interface BaseEmailInterface
 * @package Core\Entity
 */
interface EmailInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();
    /**
     * Set email
     *
     * @param string $email
     *
     * @return Email
     */
    public function setEmail($email);
    /**
     * Get email
     *
     * @return string
     */
    public function getEmail();
    /**
     * Set isVerified
     *
     * @param boolean $isVerified
     *
     * @return Email
     */
    public function setIsVerified($isVerified);
    /**
     * Get isVerified
     *
     * @return boolean
     */
    public function getIsVerified();
    /**
     * @return string
     */
    public function getConfirmationToken();
    /**
     * @param string $token
     */
    public function setConfirmationToken($token);
    /**
     * @return string
     */
    public function getConfirmationTokenCreationDate();
    /**
     * @param string $date
     */
    public function setConfirmationTokenCreationDate($date);
}