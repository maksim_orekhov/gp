<?php
namespace Core\Entity\Interfaces;

/**
 * Interface E164Interface
 * @package Core\Entity\Interfaces
 */
interface E164Interface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Set countryCode
     *
     * @param integer $countryCode
     */
    public function setCountryCode($countryCode);

    /**
     * Get countryCode
     *
     * @return integer
     */
    public function getCountryCode();

    /**
     * Set regionCode
     *
     * @param integer $regionCode
     */
    public function setRegionCode($regionCode);

    /**
     * Get regionCode
     *
     * @return integer
     */
    public function getRegionCode();

    /**
     * Set phoneNumber
     *
     * @param integer $phoneNumber
     */
    public function setPhoneNumber($phoneNumber);

    /**
     * Get phoneNumber
     *
     * @return integer
     */
    public function getPhoneNumber();
}