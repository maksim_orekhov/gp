<?php
namespace Core\Listener\Factory;

use Core\EventManager\AuthEventProvider;
use Core\EventManager\NotifyEventProvider;
use Core\Listener\BaseAuthListenerAggregate;
use Core\Service\Base\BaseUserManager;
use Core\Service\SessionContainerManager;
use Interop\Container\ContainerInterface;
use Core\Provider\Token\EmailConfirmationTokenProvider;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class BaseAuthListenerAggregateFactory
 * @package Core\Listener\Factory
 */
class BaseAuthListenerAggregateFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BaseAuthListenerAggregate|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $sessionContainerManager = $container->get(SessionContainerManager::class);
        $encodedTokenProvider = $container->get(EmailConfirmationTokenProvider::class);
        $baseUserManager = $container->get(BaseUserManager::class);
        $notifyEventManager = $container->get(NotifyEventProvider::class)->getEventManager();
        $authEventManager = $container->get(AuthEventProvider::class)->getEventManager();
        $config = $container->get('config');
        $namespace_module_auth = $config['auth_service']['provider'];
        $registrationManager = $container->get($namespace_module_auth.'\Service\RegistrationManager');

        return new BaseAuthListenerAggregate(
            $sessionContainerManager,
            $encodedTokenProvider,
            $baseUserManager,
            $registrationManager,
            $notifyEventManager,
            $authEventManager
        );
    }
}