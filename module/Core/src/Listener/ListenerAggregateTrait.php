<?php
namespace Core\Listener;

use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

/**
 * Provides logic to easily create aggregate listeners, without worrying about
 * manually detaching events
 */
trait ListenerAggregateTrait
{
    /**
     * @var callable[]
     */
    protected $listeners = [];
    /**
     * @var MvcEvent
     */
    protected $mvcEvent;

    /**
     * @param MvcEvent $mvcEvent
     */
    public function setMvcEvent(MvcEvent $mvcEvent)
    {
        $this->mvcEvent = $mvcEvent;
    }

    /**
     * {@inheritDoc}
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $callback) {
            $events->detach($callback);
            unset($this->listeners[$index]);
        }
    }

    /**
     * @param $model
     * @return null
     */
    public function runViewModel($model)
    {
        /** @var ViewModel $viewModel */
        $viewModel = $this->mvcEvent->getViewModel()->addChild($model);
        $this->mvcEvent->setViewModel($viewModel);
        $this->mvcEvent->setResult($viewModel);
        $this->mvcEvent->stopPropagation(true);

        return null;
    }
}