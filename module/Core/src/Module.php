<?php
namespace Core;

use Application\Form\ErrorReportForm;
use Core\Exception\BaseExceptionCodeInterface as ExceptionCode;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TimeZone;
use Core\Service\TwigViewModel;
use Fig\Http\Message\StatusCodeInterface;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\JsonModel;

class Module
{
    use \Core\Controller\Plugin\Message\MessageJsonModelTrait;

    public function getConfig()
    {
        return array_replace_recursive(
            require __DIR__ . '/../config/module.config.php',
            require __DIR__ . '/../config/doctrine.config.php',
            require __DIR__ . '/../config/controllers.config.php',
            require __DIR__ . '/../config/service.config.php'
        );
    }

    /**
     * This method is called once the MVC bootstrapping is complete and allows
     * to register event listeners.
     *
     * @param MvcEvent $event
     * @throws \Exception
     */
    public function onBootstrap(MvcEvent $event)
    {
        // *** Log PHP errors ***
        \Core\Service\LogMessage::registerErrorHandler(true);
        \Core\Service\LogMessage::registerFatalErrorShutdownFunction();
//        \Core\Service\LogMessage::registerExceptionHandler();
        // *** End log PHP errors ***

        $application = $event->getApplication();
        // Get service manager
        $serviceManager = $application->getServiceManager();
        // Get event manager
        $eventManager = $application->getEventManager();

        $eventManager->getSharedManager()->attach('*', MvcEvent::EVENT_DISPATCH, array($this, 'onDispatchError'), -100);
        $eventManager->getSharedManager()->attach('*', MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), -100);
        $eventManager->getSharedManager()->attach('*', MvcEvent::EVENT_RENDER_ERROR, array(
            $this,
            'onDispatchError'
        ), - 100);

        $sharedEventManager = $eventManager->getSharedManager();
        if ($sharedEventManager) {
            // Register the event listener method.
            $sharedEventManager->attach(AbstractActionController::class,
                MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 100);
        }

        //Set time zone
        $serviceManager->get(TimeZone::class)->apply();

        $eventProviderConfig = $this->getEventProviderConfig($serviceManager->get('config'));
        foreach ($eventProviderConfig as $eventProviderClass => $listeners) {
            if (\is_array($listeners)) {
                //получаем eventManager
                $eventProviderManager = $serviceManager->get($eventProviderClass)->getEventManager();
                foreach ($listeners as $listenerClass => $priority) {
                    //слушаем события
                    $listener = $serviceManager->get($listenerClass);
                    $listener->setMvcEvent($event);
                    $listener->attach($eventProviderManager, $priority);
                }
            }
        }
    }

    /**
     * @param MvcEvent $event
     * @return TwigViewModel|null|JsonModel
     */
    public function onDispatchError(MvcEvent $event)
    {

        $application = $event->getApplication();
        $serviceManager = $application->getServiceManager();
        $config = $serviceManager->get('config');
        $config_view_manager = $config['view_manager'] ?? false;

        /** @var Response $response */
        $response = $event->getResponse();
        /** @var Request $request */
        $request = $event->getRequest();
        if ($event->getParam('exception')) {
            /** @var \Throwable $exception */
            $exception = $event->getParam('exception');
            //log errors
            $this->exceptionLogging($exception, $event);
            //error 500
            if ($response->getStatusCode() === StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR) {
                if(!$request->isXmlHttpRequest()) {
                    $errorReportForm = new ErrorReportForm();
                    $errorReportForm->get('error_message')->setValue($exception->getMessage());
                    $errorReportForm->get('error_tracing')
                        ->setValue(formExceptionTrace(5, $exception->getTraceAsString(), $exception->getLine()));
                    $errorReportForm->prepare();
                    $model = new TwigViewModel([
                        'message'            => ExceptionCode::INTERNAL_SERVER_ERROR_MESSAGE,
                        'exception'          => $exception,
                        'display_exceptions' => $config_view_manager['display_exceptions'] ?? false,
                        'errorReportForm'    => $errorReportForm
                    ]);
                    $model->setTemplate('twig/error/500');
                    $event->setViewModel($model);

                }else{
                    //Ajax response
                    $model = $this->JsonErrorModel(
                        ExceptionCode::INTERNAL_SERVER_ERROR_MESSAGE,
                        ExceptionCode::INTERNAL_SERVER_ERROR,
                        APP_ENV_DEV === true
                            ? [
                                'message' => $exception->getMessage(),
                                'file'    => $exception->getFile(),
                                'line'    => $exception->getLine(),
                                'trace'   => $exception->getTrace()
                            ]
                            :null
                    );
                    $event->setViewModel($model);
                    $event->setResult($model);
                    $event->stopPropagation(true);

                    return $model;
                }
            }
        }
        //error 404
        // Проверка существования метода method_exists($response, 'getStatusCode') нужна,
        // т.к. в случае запуска консольных комманд $response содержит экземпляр Zend\Console\Request),
        // в котором нет метода getStatusCode.
        if (method_exists($response, 'getStatusCode') && $response->getStatusCode() === StatusCodeInterface::STATUS_NOT_FOUND) {
            if(!$request->isXmlHttpRequest()) {
                //Http response
                $model = new TwigViewModel();
                $model->setTemplate('twig/error/404');
                $event->setViewModel($model);

            } else {
                //Ajax response
                $model = $this->JsonErrorModel(
                    ExceptionCode::PAGE_NOT_FOUND_MESSAGE,
                    ExceptionCode::PAGE_NOT_FOUND,
                    [
                        'page' => $request->getUri()->getPath()
                    ]
                );
                $event->setViewModel($model);
                $event->setResult($model);
                $event->stopPropagation(true);

                return $model;
            }
        }

        return null;
    }

    /**
     * @param $exception
     * @param MvcEvent $event
     * @return bool
     */
    private function exceptionLogging($exception, MvcEvent $event)
    {
        if ($exception instanceof \Core\Exception\CriticalException) {
            $application = $event->getApplication();
            /** @var ServiceLocatorInterface $serviceManager */
            $serviceManager = $application->getServiceManager();
            $messagePlugin = $serviceManager->get(\Core\Controller\Plugin\Message\MessagePlugin::class);

            //пишем логи
            logException($exception,'critical-errors',\Zend\Log\Logger::CRIT);
            //уведомляем на почту
            $messagePlugin->sendCriticalExceptionAlert($exception);
        } else {
            //пишем логи
            logException($exception,'errors',\Zend\Log\Logger::ERR);
        }

        return true;
    }

    /**
     * @param array $config
     * @return array|mixed
     */
    private function getEventProviderConfig(array $config)
    {
        return array_key_exists('event_provider', $config) ? $config['event_provider'] : [];
    }

    /**
     * Event listener method for the 'Dispatch' event. We listen to the Dispatch
     * event to call the access filter. The access filter allows to determine if
     * the current visitor is allowed to see the page or not. If he/she
     * is not authenticated and is not allowed to see the page, we redirect the user
     * to the login page.
     * @param MvcEvent $event
     * @return
     */
    public function onDispatch(MvcEvent $event)
    {
        // Get the instance of BaseAuthManager service
        $baseAuthManager = $event->getApplication()->getServiceManager()->get(BaseAuthManager::class);

        $result = $baseAuthManager->dispatchAccess($event);

        if ($result) {

            return $result;
        }
    }
}
