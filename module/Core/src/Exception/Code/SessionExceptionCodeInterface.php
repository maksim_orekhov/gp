<?php
namespace Core\Exception\Code;

/**
 * Interface SessionExceptionCodeInterface
 * @package Core\Exception
 */
interface SessionExceptionCodeInterface
{
    /**
     * префикс SESSION, для сообшений суфикс _MESSAGE
     */
    const SESSION_CODE_OPERATION_TYPE_NOT_FOUND = 'SESSION_CODE_OPERATION_TYPE_NOT_FOUND';
    const SESSION_CODE_OPERATION_TYPE_NOT_FOUND_MESSAGE = 'Session code operation type not found.';

    const SESSION_VARIABLE_NOT_FOUND = 'SESSION_VARIABLE_NOT_FOUND';
    const SESSION_VARIABLE_NOT_FOUND_MESSAGE = 'Session variable not found.';
}