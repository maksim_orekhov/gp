<?php
namespace Core\Exception\Code;

/**
 * Interface TokenExceptionCodeInterface
 * @package Core\Exception
 */
interface TokenExceptionCodeInterface
{
    /**
     * префикс TOKEN, для сообшений суфикс _MESSAGE
     */
    const TOKEN_NOT_GENERATED_NOT_ENOUGH_TIMER_PASSED  = 'TOKEN_NOT_GENERATED_NOT_ENOUGH_TIMER_PASSED';
    const TOKEN_NOT_GENERATED_NOT_ENOUGH_TIMER_PASSED_MESSAGE = 'Token not generated not enough timer passed.';

    const TOKEN_GENERATING_FAILED  = 'TOKEN_GENERATING_FAILED';
    const TOKEN_GENERATING_FAILED_MESSAGE = 'Token generating failed.';

    const TOKEN_NOT_VALID  = 'TOKEN_NOT_VALID';
    const TOKEN_NOT_VALID_MESSAGE = 'Invalid token.';

    const TOKEN_DATA_FOR_CREATION_IS_EMPTY  = 'TOKEN_DATA_FOR_CREATION_IS_EMPTY';
    const TOKEN_DATA_FOR_CREATION_IS_EMPTY_MESSAGE = 'Data for creation token is empty.';

    const TOKEN_IS_EXPIRED  = 'TOKEN_IS_EXPIRED';
    const TOKEN_IS_EXPIRED_MESSAGE = 'Token is expired.';
}