<?php
namespace Core\Exception\Code;

/**
 * Interface UserExceptionCodeInterface
 * @package Core\Exception
 */
interface UserExceptionCodeInterface
{
    /**
     * префикс USER, для сообшений суфикс _MESSAGE
     */
    const USER_NOT_FOUND  = 'USER_NOT_FOUND';
    const USER_NOT_FOUND_MESSAGE  = 'User not found.';

    const USER_GENERATION_UNIQUE_LOGIN_FAILED  = 'USER_GENERATION_UNIQUE_LOGIN_FAILED';
    const USER_GENERATION_UNIQUE_LOGIN_FAILED_MESSAGE  = 'User generation unique login failed.';

    const USER_AUTHORIZATION_FAILED = 'USER_AUTHORIZATION_FAILED';
    const USER_AUTHORIZATION_FAILED_MESSAGE  = 'User authorization failed.';

    const USER_LOGIN_ALREADY_EXISTS  = 'USER_LOGIN_ALREADY_EXISTS';
    const USER_LOGIN_ALREADY_EXISTS_MESSAGE = 'Такой логин уже существует.';

    const USER_LOGIN_IS_SAME_AS_LOGIN_IN_PROFILE  = 'USER_LOGIN_IS_SAME_AS_LOGIN_IN_PROFILE';
    const USER_LOGIN_IS_SAME_AS_LOGIN_IN_PROFILE_MESSAGE = 'Введенный логин совпадает с логинон, указанным в профайле.';

    const USER_CREATED_FAILED  = 'USER_CREATED_FAILED';
    const USER_CREATED_FAILED_MESSAGE = 'User created failed.';

    const USER_NOT_MATCHING_SIMPLE_TOKEN  = 'USER_NOT_MATCHING_SIMPLE_TOKEN';
    const USER_NOT_MATCHING_SIMPLE_TOKEN_MESSAGE = 'User not matching simple token.';

    const USER_WITH_UNCONFIRMED_EMAIL_CANNOT_DELETE_DUPLICATE_USERS  = 'USER_WITH_UNCONFIRMED_EMAIL_CANNOT_DELETE_DUPLICATE_USERS';
    const USER_WITH_UNCONFIRMED_EMAIL_CANNOT_DELETE_DUPLICATE_USERS_MESSAGE = 'User with unconfirmed email cannot delete duplicate users.';
}