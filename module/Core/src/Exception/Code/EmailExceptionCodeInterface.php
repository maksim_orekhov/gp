<?php
namespace Core\Exception\Code;

/**
 * Interface EmailExceptionCodeInterface
 * @package Core\Exception\Code
 */
interface EmailExceptionCodeInterface
{
    /**
     * префикс EMAIL, для сообшений суфикс _MESSAGE
     */
    const EMAIL_NOT_FOUND = 'EMAIL_NOT_FOUND';
    const EMAIL_NOT_FOUND_MESSAGE = 'Email not found.';

    const EMAIL_ENTITY_EXPECTED_BUT_ACTUAL_NULL = 'EMAIL_ENTITY_EXPECTED_BUT_ACTUAL_NULL';
    const EMAIL_ENTITY_EXPECTED_BUT_ACTUAL_NULL_MESSAGE = 'Email entity expected but actual null.';

    const EMAIL_ALREADY_EXISTS = 'EMAIL_ALREADY_EXISTS';
    const EMAIL_ALREADY_EXISTS_MESSAGE = 'Такой email уже существует.';

    const EMAIL_NOT_UNIQUE = 'EMAIL_NOT_UNIQUE';
    const EMAIL_NOT_UNIQUE_MESSAGE = 'Такой email уже используется.';

    const EMAIL_IS_ALREADY_CONFIRMED = 'EMAIL_IS_ALREADY_CONFIRMED';
    const EMAIL_IS_ALREADY_CONFIRMED_MESSAGE = 'Email is already confirmed.';

    const EMAIL_NOT_MATCHING_SIMPLE_TOKEN = 'EMAIL_NOT_MATCHING_SIMPLE_TOKEN';
    const EMAIL_NOT_MATCHING_SIMPLE_TOKEN_MESSAGE = 'Email not matching simple token.';

    const EMAIL_IS_SAME_AS_EMAIL_IN_PROFILE = 'EMAIL_IS_SAME_AS_EMAIL_IN_PROFILE';
    const EMAIL_IS_SAME_AS_EMAIL_IN_PROFILE_MESSAGE = 'Введенный email совпадает с email, указанным в профайле.';

    const EMAIL_ENTERED_NOT_BELONG_USER = 'EMAIL_ENTERED_NOT_BELONG_USER';
    const EMAIL_ENTERED_NOT_BELONG_USER_MESSAGE = 'Введенный текущий email не соответствует email, указанному в профайле.';

    const EMAIL_VALUE_NOT_PROVIDED = 'EMAIL_VALUE_NOT_PROVIDED';
    const EMAIL_VALUE_NOT_PROVIDED_MESSAGE = 'The email value is not provided.';
}