<?php
namespace Core\Exception\Code;

/**
 * Interface EventExceptionCodeInterface
 * @package Core\Exception
 */
interface EventExceptionCodeInterface
{
    /**
     * префикс EVENT, для сообшений суфикс _MESSAGE
     */
    const EVENT_REQUIRED_PARAMETERS_MISSING  = 'EVENT_REQUIRED_PARAMETERS_MISSING';
    const EVENT_REQUIRED_PARAMETERS_MISSING_MESSAGE = 'Event required parameters missing!';
}