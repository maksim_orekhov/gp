<?php
namespace Core\Exception\Code;

/**
 * Interface FileExceptionCodeInterface
 * @package Core\Exception\Code
 */
interface FileExceptionCodeInterface
{
    /**
     * префикс FILE, для сообшений суфикс _MESSAGE
     */

    const FILE_NAME_NOT_FOUND_FROM_ROUTE = 'FILE_NAME_NOT_FOUND_FROM_ROUTE';
    const FILE_NAME_NOT_FOUND_FROM_ROUTE_MESSAGE = 'Not found file name from route.';

    const FILE_NOT_FOUND_IN_COMMON_DIRECTORY = 'FILE_NOT_FOUND_IN_COMMON_DIRECTORY';
    const FILE_NOT_FOUND_IN_COMMON_DIRECTORY_MESSAGE = 'Not found file in common directory.';
}