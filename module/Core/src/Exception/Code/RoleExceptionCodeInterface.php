<?php
namespace Core\Exception\Code;

/**
 * Interface RoleExceptionCodeInterface
 * @package Core\Exception
 */
interface RoleExceptionCodeInterface
{
    /**
     * префикс USER, для сообшений суфикс _MESSAGE
     */
    const ROLE_NOT_FOUND  = 'ROLE_NOT_FOUND';
    const ROLE_NOT_FOUND_MESSAGE  = 'Role not found.';
}