<?php
namespace Core\Exception;

use Core\Exception\Code\EmailExceptionCodeInterface;
use Core\Exception\Code\EventExceptionCodeInterface;
use Core\Exception\Code\PhoneExceptionCodeInterface;
use Core\Exception\Code\RoleExceptionCodeInterface;
use Core\Exception\Code\SessionExceptionCodeInterface;
use Core\Exception\Code\TokenExceptionCodeInterface;
use Core\Exception\Code\UserExceptionCodeInterface;
use Core\Exception\Code\FileExceptionCodeInterface;

/**
 * Interface BaseExceptionCodeInterface
 * @package Core\Exception
 */
interface BaseExceptionCodeInterface extends
    TokenExceptionCodeInterface,
    EmailExceptionCodeInterface,
    PhoneExceptionCodeInterface,
    UserExceptionCodeInterface,
    EventExceptionCodeInterface,
    SessionExceptionCodeInterface,
    RoleExceptionCodeInterface,
    FileExceptionCodeInterface
{
    /**
     * Только для общих кодов, которые могут быть использованы везде!!!
     *
     * без префикса, для сообшений суфикс _MESSAGE
     */
    const PAGE_NOT_FOUND = 'PAGE_NOT_FOUND';
    const PAGE_NOT_FOUND_MESSAGE = 'Routing is not defined for this page.';

    const INTERNAL_SERVER_ERROR = 'INTERNAL_SERVER_ERROR';
    const INTERNAL_SERVER_ERROR_MESSAGE = 'An error occurred during execution; please try again later.';

    const AUTHENTICATION_FAILED = 'AUTHENTICATION_FAILED';
    const AUTHENTICATION_FAILED_MESSAGE = 'Неправильно ввели логин или пароль!';

    const ALREADY_AUTHORIZED = 'ALREADY_AUTHORIZED';
    const ALREADY_AUTHORIZED_MESSAGE = 'Пользователь уже авторизован!';
}