<?php
namespace Core\Exception;

use Application\Exception\ApplicationExceptionCodeInterface;

/**
 * Class CriticalException
 * @package Core\Exception
 */
class CriticalException extends AbstractBaseException
    implements
    BaseExceptionCodeInterface,
    ModuleExceptionCodeInterface,
    ApplicationExceptionCodeInterface
{
    use BaseExceptionMethodsTrait;
    /**
     * CriticalException constructor.
     * @param null $exceptionMessage
     * @param null $exceptionCode
     * @throws \RuntimeException
     */
    public function __construct($exceptionMessage = null, $exceptionCode = null)
    {
        error_reporting(E_ALL & ~E_WARNING);

        $this->setExceptionCode($exceptionCode);
        $this->setExceptionMessage($exceptionMessage);
        $this->setExceptionType();

        parent::__construct($this->exceptionMessage, 0, null);
    }
}