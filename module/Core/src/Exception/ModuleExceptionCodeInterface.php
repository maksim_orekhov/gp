<?php
namespace Core\Exception;

use ModuleDelivery\Exception\ModuleDeliveryExceptionCodeInterface;
use ModuleDeliveryDpd\Exception\ModuleDeliveryDpdExceptionCodeInterface;
use ModuleAcquiringMandarin\Exception\MandarinExceptionCodeInterface;
use ModulePaymentOrder\Exception\PayOffExceptionCodeInterface;

/**
 * Interface BaseExceptionCodeInterface
 * @package Core\Exception
 */
interface ModuleExceptionCodeInterface extends
    ModuleDeliveryExceptionCodeInterface,
    ModuleDeliveryDpdExceptionCodeInterface,
    MandarinExceptionCodeInterface,
    PayOffExceptionCodeInterface
{
    /**
     * Только для общих кодов модулей, которые могут быть использованы везде!!!
     *
     * без префикса, для сообшений суфикс _MESSAGE
     */
}