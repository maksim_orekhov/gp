<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Core\Controller;

use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use Zend\Http\Request as HttpRequest;
use Zend\Json\Json;
use Zend\Mvc\Exception;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;
use Zend\Mvc\Controller\AbstractController;

/**
 * Abstract RESTful controller
 *
 * @method \Core\Controller\Plugin\Message\MessagePlugin message()
 * @method \Core\Controller\Plugin\CookiePlugin cookie()
 */
abstract class AbstractRestfulController extends AbstractController
{
    const CONTENT_TYPE_JSON = 'json';

    /**
     * {@inheritDoc}
     */
    protected $eventIdentifier = __CLASS__;

    /**
     * @var array
     */
    protected $contentTypes = [
        self::CONTENT_TYPE_JSON => [
            'application/hal+json',
            'application/json'
        ]
    ];

    /**
     * Name of request or query parameter containing identifier
     *
     * @var string
     */
    protected $identifierName = 'id';

    /**
     * Flag to pass to json_decode and/or Zend\Json\Json::decode.
     *
     * The flags in Zend\Json\Json::decode are integers, but when evaluated
     * in a boolean context map to the flag passed as the second parameter
     * to json_decode(). As such, you can specify either the Zend\Json\Json
     * constant or the boolean value. By default, starting in v3, we use
     * the boolean value, and cast to integer if using Zend\Json\Json::decode.
     *
     * Default value is boolean true, meaning JSON should be cast to
     * associative arrays (vs objects).
     *
     * Override the value in an extending class to set the default behavior
     * for your class.
     *
     * @var int|bool
     */
    protected $jsonDecodeType = true;

    /**
     * Map of custom HTTP methods and their handlers
     *
     * @var array
     */
    protected $customHttpMethodsMap = [];

    /**
     * @param array $data
     * @param string|null $template
     * @return TwigViewModel
     */
    public function view(array $data = [], string $template = null): TwigViewModel
    {
        $view = new TwigViewModel($data);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
        if ($template) {
            $view->setTemplate($template);
        }
        return $view;
    }

    /**
     * Set the route match/query parameter name containing the identifier
     *
     * @param  string $name
     * @return self
     */
    public function setIdentifierName($name)
    {
        $this->identifierName = (string) $name;
        return $this;
    }

    /**
     * Retrieve the route match/query parameter name containing the identifier
     *
     * @return string
     */
    public function getIdentifierName()
    {
        return $this->identifierName;
    }

    /**
     * Create a new resource
     *
     * @param  mixed $data
     * @return mixed
     */
    public function create($data)
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Delete an existing resource
     *
     * @param  mixed $id
     * @return mixed
     */
    public function delete($id)
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Delete the entire resource collection
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @return mixed
     */
    public function deleteList($data)
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Return single resource
     *
     * @param  mixed $id
     * @return mixed
     */
    public function get($id)
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Return list of resources
     *
     * @return mixed
     */
    public function getList()
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Retrieve HEAD metadata for the resource
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @param  null|mixed $id
     * @return mixed
     */
    public function head($id = null)
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Respond to the OPTIONS method
     *
     * Typically, set the Allow header with allowed HTTP methods, and
     * return the response.
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @return mixed
     */
    public function options()
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Respond to the PATCH method
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @param  $id
     * @param  $data
     * @return array
     */
    public function patch($id, $data)
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Replace an entire resource collection
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @param  mixed $data
     * @return mixed
     */
    public function replaceList($data)
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Modify a resource collection without completely replacing it
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.2.0); instead, raises an exception if not implemented.
     *
     * @param  mixed $data
     * @return mixed
     */
    public function patchList($data)
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Update an existing resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return mixed
     */
    public function update($id, $data)
    {
        $this->response->setStatusCode(405);

        return [
            'content' => 'Method Not Allowed'
        ];
    }

    /**
     * Basic functionality for when a page is not available
     *
     * @return array
     */
    public function notFoundAction()
    {
        $this->response->setStatusCode(404);

        return [
            'content' => 'Page not found'
        ];
    }

    /**
     * Dispatch a request
     *
     * If the route match includes an "action" key, then this acts basically like
     * a standard action controller. Otherwise, it introspects the HTTP method
     * to determine how to handle the request, and which method to delegate to.
     *
     * @events dispatch.pre, dispatch.post
     * @param  Request $request
     * @param  null|Response $response
     * @return mixed|Response
     * @throws Exception\InvalidArgumentException
     */
    public function dispatch(Request $request, Response $response = null)
    {
        if (! $request instanceof HttpRequest) {
            throw new Exception\InvalidArgumentException('Expected an HTTP request');
        }

        return parent::dispatch($request, $response);
    }

    /**
     * Handle the request
     *
     * @todo   try-catch in "patch" for patchList should be removed in the future
     * @param  MvcEvent $e
     * @return mixed
     * @throws Exception\DomainException if no route matches in event or invalid HTTP method
     */
    public function onDispatch(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();
        if (! $routeMatch) {
            /**
             * @todo Determine requirements for when route match is missing.
             *       Potentially allow pulling directly from request metadata?
             */
            throw new Exception\DomainException('Missing route matches; unsure how to retrieve action');
        }

        // Get the instance of BaseAuthManager service
        $baseAuthManager = $e->getApplication()->getServiceManager()->get(BaseAuthManager::class);

        $request = $e->getRequest();

        // Was an "action" requested?
        $action  = $routeMatch->getParam('action', false);
        if ($action) {
            // Handle arbitrary methods, ending in Action
            $method = static::getMethodFromAction($action);
            if (! method_exists($this, $method)) {
                $method = 'notFoundAction';
            }

            // Important!
            $result = $baseAuthManager->dispatchAccess($e);
            if($result) return $result;
            $return = $this->$method();
            $e->setResult($return);
            return $return;
        }


        // RESTful methods
        $method = strtolower($request->getMethod());
        if($request->getPost()->get('_method', null) !== null){
            $method = strtolower($request->getPost()->get('_method', null));
        }

        $action = $this->getActionName($method, $routeMatch, $request);
        if ($action === null) {
            $response = $e->getResponse();
            $response->setStatusCode(405);
            return $response;
        }
        $routeMatch->setParam('action', $action);

        // Important!
        $result = $baseAuthManager->dispatchAccess($e);
        if($result) return $result;

        switch ($action) {
            // DELETE
            case 'delete':
                $id = $this->getIdentifier($routeMatch, $request);
                $return = $this->delete($id);
                break;
            // DELETE LIST
            case 'deleteList':
                $data = $this->processBodyContent($request);
                $return = $this->deleteList($data);
                break;
            // GET
            case 'get':
                $id = $this->getIdentifier($routeMatch, $request);
                $return = $this->get($id);
                break;
            // GET LIST
            case 'getList':
                $return = $this->getList();
                break;
            // HEAD
            case 'head':
                $id = $this->getIdentifier($routeMatch, $request);
                if ($id === false) {
                    $id = null;
                }
                $headResult = $this->head($id);
                $response = ($headResult instanceof Response) ? clone $headResult : $e->getResponse();
                $response->setContent('');
                $return = $response;
                break;
            // OPTIONS
            case 'options':
                $this->options();
                $return = $e->getResponse();
                break;
            // PATCH
            case 'patch':
                $id = $this->getIdentifier($routeMatch, $request);
                $data = $this->processBodyContent($request);
                $return = $this->patch($id, $data);
                break;
            // PATCH LIST
            case 'patchList':
                $data = $this->processBodyContent($request);
                try {
                    $return = $this->patchList($data);
                } catch (Exception\RuntimeException $ex) {
                    $response = $e->getResponse();
                    $response->setStatusCode(405);
                    return $response;
                }
                break;
            // UPDATE
            case 'update':
                $id = $this->getIdentifier($routeMatch, $request);
                $data = $this->processPostData($request);
                $return = $this->update($id, $data);
                break;
            // CREATE
            case 'create':
                $data = $this->processPostData($request);
                $return = $this->create($data);
                break;
            // PUT
            case 'replaceList':
                $data = $this->processBodyContent($request);
                $return = $this->replaceList($data);
                break;
            // All others...
            default:
                $response = $e->getResponse();
                $response->setStatusCode(405);
                return $response;
        }

        $e->setResult($return);

        return $return;
    }

    /**
     * Process post data
     *
     * @param Request $request
     * @return mixed
     * @throws Exception\DomainException If a JSON request was made, but no
     *    method for parsing JSON is available.
     */
    public function processPostData(Request $request)
    {
        if ($this->getRequest()->isXmlHttpRequest() && !empty($this->getRequest()->getContent())) {
            $content = $this->getRequest()->getContent();

            return Json::decode($content, Json::TYPE_ARRAY);
        }

        return $request->getPost()->toArray();
    }

    /**
     * Check if request has certain content type
     *
     * @param  Request $request
     * @param  string|null $contentType
     * @return bool
     */
    public function requestHasContentType(Request $request, $contentType = '')
    {
        /** @var $headerContentType \Zend\Http\Header\ContentType */
        $headerContentType = $request->getHeaders()->get('content-type');
        if (! $headerContentType) {
            return false;
        }

        $requestedContentType = $headerContentType->getFieldValue();
        if (false !== strpos($requestedContentType, ';')) {
            $headerData = explode(';', $requestedContentType);
            $requestedContentType = array_shift($headerData);
        }
        $requestedContentType = trim($requestedContentType);
        if (array_key_exists($contentType, $this->contentTypes)) {
            foreach ($this->contentTypes[$contentType] as $contentTypeValue) {
                if (stripos($contentTypeValue, $requestedContentType) === 0) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Register a handler for a custom HTTP method
     *
     * This method allows you to handle arbitrary HTTP method types, mapping
     * them to callables. Typically, these will be methods of the controller
     * instance: e.g., array($this, 'foobar'). The typical place to register
     * these is in your constructor.
     *
     * Additionally, as this map is checked prior to testing the standard HTTP
     * methods, this is a way to override what methods will handle the standard
     * HTTP methods. However, if you do this, you will have to retrieve the
     * identifier and any request content manually.
     *
     * Callbacks will be passed the current MvcEvent instance.
     *
     * To retrieve the identifier, you can use "$id =
     * $this->getIdentifier($routeMatch, $request)",
     * passing the appropriate objects.
     *
     * To retrieve the body content data, use "$data = $this->processBodyContent($request)";
     * that method will return a string, array, or, in the case of JSON, an object.
     *
     * @param  string $method
     * @param  Callable $handler
     * @return AbstractRestfulController
     */
    public function addHttpMethodHandler($method, /* Callable */ $handler)
    {
        if (! \is_callable($handler)) {
            throw new Exception\InvalidArgumentException(sprintf(
                'Invalid HTTP method handler: must be a callable; received "%s"',
                (\is_object($handler) ? \get_class($handler) : \gettype($handler))
            ));
        }
        $method = strtolower($method);
        $this->customHttpMethodsMap[$method] = $handler;
        return $this;
    }

    /**
     * Retrieve the identifier, if any
     *
     * Attempts to see if an identifier was passed in either the URI or the
     * query string, returning it if found. Otherwise, returns a boolean false.
     *
     * @param  \Zend\Router\RouteMatch $routeMatch
     * @param  Request $request
     * @return false|mixed
     */
    protected function getIdentifier($routeMatch, $request)
    {
        $identifier = $this->getIdentifierName();
        $id = $routeMatch->getParam($identifier, false);
        if ($id !== false) {
            return $id;
        }

        $id = $request->getQuery()->get($identifier, false);
        if ($id !== false) {
            return $id;
        }

        return false;
    }

    /**
     * Process the raw body content
     *
     * If the content-type indicates a JSON payload, the payload is immediately
     * decoded and the data returned. Otherwise, the data is passed to
     * parse_str(). If that function returns a single-member array with a empty
     * value, the method assumes that we have non-urlencoded content and
     * returns the raw content; otherwise, the array created is returned.
     *
     * @param  mixed $request
     * @return object|string|array
     * @throws Exception\DomainException If a JSON request was made, but no
     *    method for parsing JSON is available.
     */
    protected function processBodyContent($request)
    {
        $content = $request->getContent();

        // JSON content? decode and return it.
        if ($this->requestHasContentType($request, self::CONTENT_TYPE_JSON)) {
            return $this->jsonDecode($request->getContent());
        }

        parse_str($content, $parsedParams);

        // If parse_str fails to decode, or we have a single element with empty value
        if (! is_array($parsedParams) || empty($parsedParams)
            || (1 == count($parsedParams) && '' === reset($parsedParams))
        ) {
            return $content;
        }

        return $parsedParams;
    }

    /**
     * Decode a JSON string.
     *
     * Uses json_decode by default. If that is not available, checks for
     * availability of Zend\Json\Json, and uses that if present.
     *
     * Otherwise, raises an exception.
     *
     * Marked protected to allow usage from extending classes.
     *
     * @param string
     * @return mixed
     * @throws Exception\DomainException if no JSON decoding functionality is
     *     available.
     */
    protected function jsonDecode($string)
    {
        if (function_exists('json_decode')) {
            return json_decode($string, (bool) $this->jsonDecodeType);
        }

        if (class_exists(Json::class)) {
            return Json::decode($string, (int) $this->jsonDecodeType);
        }

        throw new Exception\DomainException(sprintf(
            'Unable to parse JSON request, due to missing ext/json and/or %s',
            Json::class
        ));
    }

    private function getActionName($method, $routeMatch, $request)
    {
        switch ($method) {
            // DELETE
            case 'delete':
                $id = $this->getIdentifier($routeMatch, $request);
                if ($id !== false) {
                    $action = 'delete';
                    break;
                }
                $action = 'deleteList';
                break;
            // GET
            case 'get':
                $id = $this->getIdentifier($routeMatch, $request);
                if ($id !== false) {
                    $action = 'get';
                    break;
                }
                $action = 'getList';
                break;
            // HEAD
            case 'head':
                $id = $this->getIdentifier($routeMatch, $request);
                if ($id === false) {
                    $id = null;
                }
                $action = 'head';
                break;
            // OPTIONS
            case 'options':
                $action = 'options';
                break;
            // PATCH
            case 'patch':
                $id = $this->getIdentifier($routeMatch, $request);
                if ($id !== false) {
                    $action = 'patch';
                    break;
                }
                $action = 'patchList';
                break;
            // POST
            case 'post':
                $id = $this->getIdentifier($routeMatch, $request);
                if ($id !== false) {
                    $action = 'update';
                    break;
                }
                $action = 'create';
                break;
            // PUT
            case 'put':
                $action = 'replaceList';
                break;
            // All others...
            default:
                return null;
        }

        return $action;
    }

    /**
     * @return array
     */
    public function collectIncomingData()
    {
        $routeData = $this->params()->fromRoute();
        $queryData = $this->params()->fromQuery();
        $filesData = $this->getRequest()->getFiles()->toArray();
        $data = array_merge($queryData, $routeData);
        if ($this->getRequest()->isPost()) {
            if ($this->getRequest()->isXmlHttpRequest() && !empty($this->getRequest()->getContent())) {
                $content = $this->getRequest()->getContent();
                $ajaxData = Json::decode($content, Json::TYPE_ARRAY);
                $postData = $ajaxData;
                $data = array_merge($data, $ajaxData);
            } else {
                $postData = $this->params()->fromPost();
                $data = array_merge($data, $postData);
            }
        }

        $data['routeData'] = (isset($routeData) && !empty($routeData)) ? $routeData : null;
        $data['queryData'] = (isset($queryData) && !empty($queryData)) ? $queryData : null;
        $data['ajaxData'] = (isset($ajaxData) && !empty($ajaxData)) ? $ajaxData : null;
        $data['postData'] = (isset($postData) && !empty($postData)) ? $postData : null;
        $data['filesData'] = (isset($filesData) && !empty($filesData)) ? $filesData : null;

        return $data;
    }

    /**
     * @param null $config
     * @return array
     */
    public function getPaginationParams($config = null)
    {
        $queryData = $this->collectIncomingData()['queryData'];
        $default_number_per_page = $config ? $config['default_pagination_preferences']['number_per_page'] : 20;
        $default_sorting_order = $config ? $config['default_pagination_preferences']['sorting_order'] : 'DESC';

        $page = isset($queryData['page']) ? $queryData['page'] : 1;
        $perPage = isset($queryData['per_page']) ? $queryData['per_page'] : $default_number_per_page;
        $order = isset($queryData['order']) ? $queryData['order'] : null;
        $sort = isset($queryData['sort']) ? $queryData['sort'] : $default_sorting_order;

        $params = [];
        $params['order'] = $order ? [$order => $sort] : [ 'id' => $default_sorting_order ];
        $params['per_page'] = $perPage;
        $params['page'] = $page;

        return $params;
    }
}
