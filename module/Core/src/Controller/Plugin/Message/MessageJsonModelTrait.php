<?php
namespace Core\Controller\Plugin\Message;

use Zend\View\Model\JsonModel;

Trait MessageJsonModelTrait
{
    /**
     * @param null $message
     * @param null $data
     * @param null $code
     * @return JsonModel
     */
    private function JsonErrorModel($message = null, $code = null, $data = null)
    {
        return new JsonModel([
            'status'    => 'ERROR',
            'code'      => $code,
            'message'   => $message,
            'data'      => $data
        ]);
    }

    /**
     * @param null $message
     * @param null $data
     * @param null $code
     * @return JsonModel
     */
    private function JsonSuccessModel($message = null, $code = null, $data = null)
    {
        return new JsonModel([
            'status'    => 'SUCCESS',
            'code'      => $code,
            'message'   => $message,
            'data'      => $data
        ]);
    }
}
