<?php
namespace Core\Controller\Plugin\Message\Factory;

use Core\Provider\Mail\MessagePluginSender;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Controller\Plugin\Message\MessagePlugin;

/**
 * Class MessagePluginFactory
 * @package Core\Controller\Plugin\Message\Factory
 */
class MessagePluginFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $request = $container->get('request');
        $messagePluginSender = $container->get(MessagePluginSender::class);
        $config = $container->get('Config');

        return new MessagePlugin(
            $request,
            $messagePluginSender,
            $config
        );
    }
}
