<?php
namespace Core\Controller\Plugin\Message;


interface BaseMessageCodeInterface
{
    /**
     * success code
     *
     * префикс SUCCESS, для сообшений суфикс _MESSAGE
     */
    //code for 200 status
    const SUCCESS_RESPONSE = 'SUCCESS_RESPONSE';
    const SUCCESS_RESPONSE_MESSAGE = 'Response successfully received.';

    /**
     * error code
     *
     * префикс ERROR, для сообшений суфикс _MESSAGE
     */
    const ERROR_RESPONSE = 'ERROR_RESPONSE';
    const ERROR_RESPONSE_MESSAGE = 'Response failed.';

    const ERROR_NOT_AUTHORIZED = 'ERROR_NOT_AUTHORIZED';
    const ERROR_NOT_AUTHORIZED_MESSAGE = 'User not authorized.';

    const ERROR_ACCESS_DENIED = 'ERROR_ACCESS_DENIED';
    const ERROR_ACCESS_DENIED_MESSAGE = 'Access to the resource is denied to the user.';

    const ERROR_INVALID_FORM_DATA = 'ERROR_INVALID_FORM_DATA';
    const ERROR_INVALID_FORM_DATA_MESSAGE = 'The form does not pass validation.';
}
