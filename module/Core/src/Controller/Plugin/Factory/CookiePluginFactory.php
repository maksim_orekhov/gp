<?php
namespace Core\Controller\Plugin\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Controller\Plugin\CookiePlugin;

/**
 * Class CookiePluginFactory
 * @package Core\Controller\Plugin\Factory
 */
class CookiePluginFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $response = $container->get('response');
        $request = $container->get('request');

        return new CookiePlugin($response, $request);
    }
}

