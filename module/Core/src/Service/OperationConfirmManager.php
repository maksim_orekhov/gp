<?php
namespace Core\Service;

use Core\Entity\Interfaces\UserInterface;
use Application\Entity\User;
use Core\Exception\LogicException;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use ModuleCode\Entity\Code;
use ModuleCode\Entity\CodeOperationType;
use ModuleCode\Listener\OperationConfirmCodeDemandListener;
use ModuleCode\Listener\OperationConfirmCodeReceptionListener;
use Core\Provider\Mail\MailSenderInterface;
use ModuleCode\Api\CodeController as ConfirmationCode;
use \Doctrine\ORM\EntityManager;

/**
 * Class OperationConfirmManager
 * @package Core\Service
 */
class OperationConfirmManager
{
    const SUCCESS_CONFIRMATION_CODE_SENT        = "SMS with confirmation code has been send";
    const SUCCESS_NEW_CONFIRMATION_CODE_SENT    = "SMS with new confirmation code has been send";
    const SUCCESS_CSRF_TOKEN                    = "CSRF token return";
    const ERROR_USER_NOT_DEFINED                = "User not defined";
    const ERROR_CODE_CAN_NOT_BE_SENT            = "A new sms message can not be sent";
    const SUCCESS_CONFIRMATION_CODE_SENT_BY_EMAIL = "Email with confirmation code has been send";

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var OperationConfirmCodeDemandListener
     */
    private $operationConfirmCodeDemandListener;

    /**
     * @var OperationConfirmCodeReceptionListener
     */
    private $operationConfirmCodeReceptionListener;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var MailSenderInterface
     */
    private $confirmationCodeMailSender;

    /**
     * @var ConfirmationCode
     */
    private $confirmationCode;

    /**
     * @var array
     */
    private $config;

    /**
     * OperationConfirmManager constructor.
     * @param EntityManager $entityManager
     * @param OperationConfirmCodeDemandListener $operationConfirmCodeDemandListener
     * @param OperationConfirmCodeReceptionListener $operationConfirmCodeReceptionListener
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param MailSenderInterface $confirmationCodeMailSender
     * @param ConfirmationCode $confirmationCode
     */
    public function __construct(EntityManager $entityManager,
                                OperationConfirmCodeDemandListener $operationConfirmCodeDemandListener,
                                OperationConfirmCodeReceptionListener $operationConfirmCodeReceptionListener,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                MailSenderInterface $confirmationCodeMailSender,
                                ConfirmationCode $confirmationCode,
                                $config)
    {
        $this->entityManager                            = $entityManager;
        $this->operationConfirmCodeDemandListener       = $operationConfirmCodeDemandListener;
        $this->operationConfirmCodeReceptionListener    = $operationConfirmCodeReceptionListener;
        $this->userManager                              = $userManager;
        $this->baseAuthManager                          = $baseAuthManager;
        $this->confirmationCodeMailSender               = $confirmationCodeMailSender;
        $this->confirmationCode                         = $confirmationCode;
        $this->config                                   = $config;
    }


    /**
     * @param $login
     * @param $password
     * @return \Zend\Authentication\Result
     * @throws \Exception
     */
    public function checkPassword($login, $password)
    {
        return $this->baseAuthManager->checkPassword($login, $password);
    }

    /**
     * @param UserInterface $user
     * @param string $code_value
     * @param string $code_operation_type_name
     * @return bool
     * @throws \Exception
     */
    public function checkCode(UserInterface $user, $code_value, $code_operation_type_name)
    {
        // Subscribe on event "onOperationConfirmationCodeReception"
        $this->operationConfirmCodeReceptionListener->subscribe();
        // Тригер события "onPhoneConfirmationCodeReception"
        // Check user submitted code
        try {
            $result = $this->operationConfirmCodeReceptionListener->trigger($user->getLogin(), $code_value, $code_operation_type_name);
            // If no SUCCESS
            if($result['status'] !== 'SUCCESS') {
                // If no such user's code - throw Exception
                throw new LogicException(null, LogicException::PHONE_INVALID_CODE);
            }
        }
        catch (\Throwable $t) {

            throw new LogicException($t->getMessage(), LogicException::PHONE_INVALID_CODE);
        }

        return true;
    }

    /**
     * @param $user_login
     * @param $code_operation_type_name
     * @return bool
     * @throws \Exception
     */
    public function sendCode($user_login, $code_operation_type_name)
    {
        // Provide new confirmation code
        // Subscribe to onOperationConfirmationCodeDemand event
        $this->operationConfirmCodeDemandListener->subscribe();
        // If provided data are valid and the user allowed request a confirmation code code will be sent
        // Trigger event "onOperationConfirmationCodeDemand"
        $result = $this->operationConfirmCodeDemandListener->trigger($user_login, $code_operation_type_name);

        if($result['status'] !== 'SUCCESS') {

            throw new LogicException($this->getExceptionMessage($result), LogicException::SMS_PROVIDER_ERROR);
        }

        return true;
    }

    /**
     * Send code by email
     *
     * @param $user_login
     * @param $code_operation_type_name
     * @return bool
     * @throws \Exception
     */
    public function sendCodeByEmail($user_login, $code_operation_type_name)
    {
        // Provide new confirmation code
        // Subscribe to onOperationConfirmationCodeDemand event
        $this->operationConfirmCodeDemandListener->subscribe();

        // If provided data are valid and the user allowed request a confirmation code code will be sent by email
        // Trigger event "onOperationConfirmationCodeDemand"
        $result = $this->operationConfirmCodeDemandListener->trigger($user_login, $code_operation_type_name, $this->confirmationCodeMailSender);

        if($result['status'] !== 'SUCCESS') {

            throw new \Exception($this->getExceptionMessage($result));
        }

        return true;
    }

    /**
     * @param $user_login
     * @param $code_operation_type_name
     * @return array|bool|\Zend\View\Model\JsonModel
     */
    public function userAllowedRequestCodeStatus($user_login, $code_operation_type_name)
    {
        $params = [
            'user_login' => $user_login,
            'code_operation_type_name' => $code_operation_type_name
        ];

        return $this->confirmationCode->userAllowedRequestCodeStatus($params);
    }

    /**
     * @param User $user
     * @param string $code_operation_type_name
     * @return bool
     */
    public function isUserHasActiveCodeOfSpecifiedType(User $user, string $code_operation_type_name): bool
    {
        /** @var CodeOperationType $codeOperationType */
        $codeOperationType = $this->entityManager->getRepository(CodeOperationType::class)
            ->findOneBy(
                array('name' => $code_operation_type_name)
            );

        /** @var \DateTime $currentDate */// Current date
        $currentDate = new \DateTime();
        $expiration_period = $this->config['tokens']['confirmation_code_expiration_time'];

        $codes = $this->userManager->getUserCodesByOperationType($user, $codeOperationType);
        /** @var Code $code */
        foreach ($codes as $code) {
            /** @var \DateTime $codeCreated */
            $codeCreatedDate = clone $code->getCreated();

            if ($codeCreatedDate->modify('+'.$expiration_period.' seconds') > $currentDate) {

                return true;
            }
        }

        return false;
    }

    /**
     * @param array $result
     * @return string
     */
    private function getExceptionMessage(array $result):string
    {
        $message = 'Providing confirmation code failed';
        if (isset($result['message'])) {
            $message .= '. ' . $result['message'];
        }
        if (isset($result['data']['violation_message'])) {
            $message .= '. ' . $result['data']['violation_message'];
        }
        $message .= '.';

        return $message;
    }
}