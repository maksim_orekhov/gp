<?php
namespace Core\Service;

use ZendTwig\View\TwigModel;

/**
 * Class TwigViewModel
 * @package Core\Service
 */
class TwigViewModel extends TwigModel
{
    const DEFAULT_LAYOUT = 'twigTemplate';


    public function __construct($variables = null, $options = null)
    {
        parent::__construct($variables, $options);
        //TODO сделать дефолтный layout
    }
}
