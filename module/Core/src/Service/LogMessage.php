<?php
namespace Core\Service;
use Throwable;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use ErrorException;


/**
 * Class LoggerManager
 * @package Core\Service
 */
class LogMessage
{
    const MESSAGE_LOG_FILE = __DIR__ . '/../../../../logs/';

    const DEFAULT_MAX_LEVEL = 5;
    const MAX_LEVEL_OBJECT = 3;
    const MAX_LEVEL_ARRAY = self::DEFAULT_MAX_LEVEL;

    /**
     * @const int defined from the BSD Syslog message severities
     * @link http://tools.ietf.org/html/rfc3164
     */
    const EMERG  = 0;
    const ALERT  = 1;
    const CRIT   = 2;
    const ERR    = 3;
    const WARN   = 4;
    const NOTICE = 5;
    const INFO   = 6;
    const DEBUG  = 7;

    /**
     * Map native PHP errors to priority
     *
     * @var array
     */
    public static $errorPriorityMap = [
        E_NOTICE            => self::NOTICE,
        E_USER_NOTICE       => self::NOTICE,
        E_WARNING           => self::WARN,
        E_CORE_WARNING      => self::WARN,
        E_USER_WARNING      => self::WARN,
        E_ERROR             => self::ERR,
        E_USER_ERROR        => self::ERR,
        E_CORE_ERROR        => self::ERR,
        E_RECOVERABLE_ERROR => self::ERR,
        E_PARSE             => self::ERR,
        E_COMPILE_ERROR     => self::ERR,
        E_COMPILE_WARNING   => self::ERR,
        E_STRICT            => self::DEBUG,
        E_DEPRECATED        => self::DEBUG,
        E_USER_DEPRECATED   => self::DEBUG,
    ];

    /**
     * @var string
     */
    private static $stackTraceTemplate = <<<'EOT'
%s raised in file %s line %d:
------------------
>>> Message: %s
>>> Stack Trace:
%s
------------------

EOT;

    /**
     * Registered shutdown error handler
     *
     * @var bool
     */
    protected static $registeredFatalErrorShutdownFunction = false;

    /**
     * Registered error handler
     *
     * @var bool
     */
    protected static $registeredErrorHandler = false;

    /**
     * Registered exception handler
     *
     * @var bool
     */
    protected static $registeredExceptionHandler = false;

    /**
     * priority
     * EMERG   = 0;  // Emergency: system is unusable
     * ALERT   = 1;  // Alert: action must be taken immediately
     * CRIT    = 2;  // Critical: critical conditions
     * ERR     = 3;  // Error: error conditions
     * WARN    = 4;  // Warning: warning conditions
     * NOTICE  = 5;  // Notice: normal but significant condition
     * INFO    = 6;  // Informational: informational messages
     * DEBUG   = 7;  // Debug: debug messages
     *
     * @param null $message
     * @param null $file_name
     * @param null $priority
     * @param string $after_message_string
     * @return bool
     */
    public static function setLogMessage($message = null, $file_name = null, $priority = null, $after_message_string = ''): bool
    {
        switch (\gettype($message)) {
            case 'array':
                $message = self::print_r_level($message, self::MAX_LEVEL_ARRAY);
                break;
            case 'object':
                $message = self::print_r_level($message, self::MAX_LEVEL_OBJECT);
                break;
            case 'string':
                $message = ('' === trim($message)) ? 'Пусто' : $message;
                break;
            default:
                $message = var_export($message, true);
                break;
        }

        ignore_user_abort(true);
        $logger = self::getLogger($file_name);
        if (empty($priority) || $priority === null) {
            $priority = 7;
        } else {
            $priority = (int)$priority;
        }

        $logger->log($priority, "\nMessage:\n".$message.$after_message_string."\n----------\n");

        ignore_user_abort(false);

        return true;
    }

    /**
     * @param Throwable $exception|Exception $exception
     * @param null $file_name
     * @param null $priority
     * @return bool
     */
    public static function setLogException($exception, $file_name = null, $priority = null): bool
    {
        ignore_user_abort(true);
        $logger = self::getLogger($file_name);
        if (empty($priority) || $priority === null) {
            $priority = 3;
        } else {
            $priority = (int)$priority;
        }
//        $message = '';
//        do {
//            $message .= sprintf(
//                self::$stackTraceTemplate,
//                \get_class($exception),
//                $exception->getFile(),
//                $exception->getLine(),
//                $exception->getMessage(),
//                $exception->getTraceAsString()
//            );
//        } while ($e = $exception->getPrevious());
        $message = sprintf(
            self::$stackTraceTemplate,
            \get_class($exception),
            $exception->getFile(),
            $exception->getLine(),
            $exception->getMessage(),
            $exception->getTraceAsString()
        );

        $logger->log($priority, $message, []);

        ignore_user_abort(false);

        return true;
    }

    /**
     * @param null $file_name
     * @return Logger
     */
    private static function getLogger($file_name = null): Logger
    {
        if(empty($file_name) || $file_name === null){
            $path = self::MESSAGE_LOG_FILE . 'message.log';
        }else{
            $path = self::MESSAGE_LOG_FILE . $file_name.'.log';
        }
        // На случай если файл был создан под пользователем root и прочим (например из под крона)
        // Если файл не доступен для записи (например был создан из под root) то удаляем его что бы он пересоздался.
        if (file_exists($path) && !is_writable($path)) {
            @unlink($path);
        }

        $logger = new Logger;
        $writer = new Stream($path);

        return $logger->addWriter($writer);
    }

    /**
     * @param $data
     * @param int $level
     * @return string
     */
    private static function print_r_level($data, $level = self::DEFAULT_MAX_LEVEL): string
    {
        static $innerLevel = 1;

        static $tabLevel = 1;

        static $cache = array();

        $output     = '';
        $type       = \gettype($data);
        $tabs       = str_repeat('    ', $tabLevel);
        $quoteTabs = str_repeat('    ', $tabLevel - 1);

        $recursiveType = array('object', 'array');

        // Recursive
        if (\in_array($type, $recursiveType, true))
        {
            $elements = array();

            // If type is object, try to get properties by Reflection.
            if ($type === 'object')
            {
                if (self::in_array_recursive($data, $cache)) {
                    return "\n{$quoteTabs}*RECURSION*\n";
                }

                // Cache the data
                $cache[] = $data;

                $output     = \get_class($data) . ' ' . ucfirst($type);
                $ref        = new \ReflectionObject($data);
                $properties = $ref->getProperties();

                foreach ($properties as $property)
                {
                    $property->setAccessible(true);

                    $pType = '\''.$property->getName().'\'';

                    if ($property->isProtected())
                    {
                        $pType = 'protected:' .$pType;
                    }
                    elseif ($property->isPrivate())
                    {
                        $pType = 'private:' .$pType;
                    }

                    if ($property->isStatic())
                    {
                        $pType = 'static:' .$pType;
                    }

                    $elements[$pType] = $property->getValue($data);
                }
            }
            // If type is array, just return it's value.
            elseif ($type === 'array')
            {
                $output = ucfirst($type);
                $elements = $data;
            }

            // Start dumping data
            if ($level === 0 || $innerLevel < $level)
            {
                // Start recursive print
                $output .= "\n{$quoteTabs}(";

                foreach ($elements as $key => $element)
                {
                    $output .= "\n{$tabs}{$key} => ";

                    // Increment level
                    $tabLevel += 2;
                    $innerLevel++;

                    $output  .= \in_array(\gettype($element), $recursiveType, true) ? self::print_r_level($element, $level) : $element;

                    // Decrement level
                    $tabLevel -= 2;
                    $innerLevel--;
                }

                $output .= "\n{$quoteTabs})\n";
            }
            else
            {
                $output .= "\n{$quoteTabs}*MAX LEVEL*\n";
            }
        }

        // Clean cache
        if($innerLevel === 1)
        {
            $cache = array();
        }

        return $output;
    }

    /**
     * решает проблемму "Nesting level too deep"
     * @param $needle
     * @param $haystack
     * @return bool
     */
    private static function in_array_recursive($needle, $haystack): bool
    {

        foreach($haystack AS $element) {
            if($element === $needle) {
                return true;
            }
        }

        return false;
    }

    /**
     * Register a shutdown handler to log fatal errors
     *
     * @link http://www.php.net/manual/function.register-shutdown-function.php
     * @return bool
     */
    public static function registerFatalErrorShutdownFunction(): bool
    {
        // Only register once per instance
        if (static::$registeredFatalErrorShutdownFunction) {
            return false;
        }

        $errorPriorityMap = static::$errorPriorityMap;

        $logger = self::getLogger('errors');

        register_shutdown_function(function () use ($logger, $errorPriorityMap) {
            $error = error_get_last();
            if (null === $error
                || ! \in_array(
                    $error['type'],
                    [
                        E_ERROR,
                        E_PARSE,
                        E_CORE_ERROR,
                        E_CORE_WARNING,
                        E_COMPILE_ERROR,
                        E_COMPILE_WARNING
                    ],
                    true
                )
            ) {
                return;
            }
            if (isset($errorPriorityMap[$error['type']])) {
                $priority = $errorPriorityMap[$error['type']];
            } else {
                $priority = self::INFO;
            }
            $logger->log($priority, $error['message'], []);
        });

        static::$registeredFatalErrorShutdownFunction = true;

        return true;
    }

    /**
     * Register logging system as an error handler to log PHP errors
     *
     * @link http://www.php.net/manual/function.set-error-handler.php
     * @param  bool   $continueNativeHandler
     * @return mixed  Returns result of set_error_handler
     * @throws \Zend\Log\Exception\InvalidArgumentException if logger is null
     */
    public static function registerErrorHandler($continueNativeHandler = false)
    {
        // Only register once per instance
        if (static::$registeredErrorHandler) {
            return false;
        }

        $errorPriorityMap = static::$errorPriorityMap;

        $logger = self::getLogger('errors');

        $previous = set_error_handler(
            function ($level, $message, $file, $line) use ($logger, $errorPriorityMap, $continueNativeHandler) {
                $iniLevel = error_reporting();

                if ($iniLevel & $level) {
                    if (isset($errorPriorityMap[$level])) {
                        $priority = $errorPriorityMap[$level];
                    } else {
                        $priority = self::INFO;
                    }

                    try {
                        throw new \ErrorException(
                            $message,
                            $priority,
                            $level,
                            $file,
                            $line
                        );
                    }
                    catch (\ErrorException $exception){
                        $message = sprintf(
                            self::$stackTraceTemplate,
                            \get_class($exception),
                            $exception->getFile(),
                            $exception->getLine(),
                            $exception->getMessage(),
                            $exception->getTraceAsString()
                        );

                        $logger->log($priority, $message, []);
                    }
                }

                return ! $continueNativeHandler;
            }
        );

        static::$registeredErrorHandler = true;
        return $previous;
    }

    /**
     * Register logging system as an exception handler to log PHP exceptions
     *
     * @link http://www.php.net/manual/en/function.set-exception-handler.php
     * @return bool
     * @throws \Zend\Log\Exception\InvalidArgumentException if logger is null
     */
    public static function registerExceptionHandler()
    {
        // Only register once per instance
        if (static::$registeredExceptionHandler) {
            return false;
        }

        $logger = self::getLogger('errors');

        $errorPriorityMap = static::$errorPriorityMap;

        set_exception_handler(function ($exception) use ($logger, $errorPriorityMap) {
            $logMessages = [];

            do {
                $priority = self::ERR;

                if ($exception instanceof ErrorException && isset($errorPriorityMap[$exception->getSeverity()])) {
                    $priority = $errorPriorityMap[$exception->getSeverity()];
                }

                $extra = [
                    'file'  => $exception->getFile(),
                    'line'  => $exception->getLine(),
                    #'trace' => $exception->getTrace(),//слишком много информации
                ];
//                if (isset($exception->xdebug_message)) {
                    #$extra['xdebug'] = $exception->xdebug_message;//слишком много информации
//                }

                $logMessages[] = [
                    'priority' => $priority,
                    'message'  => $exception->getMessage(),
                    'extra'    => $extra,
                ];
                $exception = $exception->getPrevious();
            } while ($exception);

            foreach (array_reverse($logMessages) as $logMessage) {
                $logger->log($logMessage['priority'], $logMessage['message'], $logMessage['extra']);
            }
        });

        static::$registeredExceptionHandler = true;
        return true;
    }
}

