<?php
namespace Core\Service\Auth;

use Zend\Authentication\Storage\StorageInterface;
use Zend\Authentication\Storage\Session;
use Zend\Authentication\Result;
use Zend\Authentication\Exception\RuntimeException;
use Zend\Authentication\AuthenticationServiceInterface;

class AuthService implements AuthenticationServiceInterface
{
    /**
     * Persistent storage handler
     *
     * @var StorageInterface
     */
    protected $storage = null;

    /**
     * Authentication adapter
     *
     * @var AuthAdapterInterface
     */
    protected $adapter = null;

    /**
     * Constructor
     *
     * @param  StorageInterface $storage
     * @param  AuthAdapterInterface $adapter
     */
    public function __construct(StorageInterface $storage = null, AuthAdapterInterface $adapter = null)
    {
        if (null !== $storage) {
            $this->setStorage($storage);
        }
        if (null !== $adapter) {
            $this->setAdapter($adapter);
        }
    }

    /**
     * Returns the authentication adapter
     *
     * The adapter does not have a default if the storage adapter has not been set.
     *
     * @return AuthAdapterInterface|null
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * Sets the authentication adapter
     *
     * @param  AuthAdapterInterface $adapter
     * @return AuthService Provides a fluent interface
     */
    public function setAdapter(AuthAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        return $this;
    }

    /**
     * Returns the persistent storage handler
     *
     * Session storage is used by default unless a different storage adapter has been set.
     *
     * @return StorageInterface
     */
    public function getStorage()
    {
        if (null === $this->storage) {
            $this->setStorage(new Session());
        }

        return $this->storage;
    }

    /**
     * Sets the persistent storage handler
     *
     * @param  StorageInterface $storage
     * @return AuthService Provides a fluent interface
     */
    public function setStorage(StorageInterface $storage)
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * Authenticates against the supplied adapter
     *
     * @param  AuthAdapterInterface $adapter
     * @return Result
     * @throws RuntimeException
     */
    public function authenticate(AuthAdapterInterface $adapter = null)
    {
        if (!$adapter) {
            if (!$adapter = $this->getAdapter()) {
                throw new RuntimeException('An adapter must be set or passed prior to calling authenticate()');
            }
        }
        $result = $adapter->authenticate();

        /**
         * ZF-7546 - prevent multiple successive calls from storing inconsistent results
         * Ensure storage has clean state
         */
        if ($this->hasIdentity()) {
            $this->clearIdentity();
        }

        if ($result->isValid()) {
            $this->getStorage()->write($result->getIdentity());
        }

        return $result;
    }

    /**
     * Returns true if and only if an identity is available from storage
     *
     * @return bool
     */
    public function hasIdentity()
    {
        return !$this->getStorage()->isEmpty();
    }

    /**
     * Returns the identity from storage or null if no identity is available
     *
     * @return mixed|null
     */
    public function getIdentity()
    {
        $storage = $this->getStorage();

        if ($storage->isEmpty()) {

            return null;
        }

        return $storage->read();
    }

    /**
     * Clears the identity from persistent storage
     *
     * @return void
     */
    public function clearIdentity()
    {
        $this->getStorage()->clear();
    }
}