<?php
namespace Core\Service\Auth;

use Zend\Authentication\Adapter\AdapterInterface;

interface AuthAdapterInterface extends AdapterInterface
{
    public function setPassword($password);
    public function setVerifyPassword($verify_password = true);
    public function setLogin($login);
    public function getLogin();
    public function checkLoginExist():bool;

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
     */
    public function authenticate();

    /**
     * @return \Zend\Authentication\Result
     * @throws \Error if current adapter can't checkCredentials of user (wrong adapter set)
     */
    public function checkCredentials();
}