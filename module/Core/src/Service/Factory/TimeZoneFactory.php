<?php
namespace Core\Service\Factory;

use Core\Service\TimeZone;
use DateTimeZone;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;


/**
 * Class TwigRendererFactory
 * @package Core\Service
 */
class TimeZoneFactory implements FactoryInterface
{
    const DEFAULT_TIME_ZONE = 'Europe/Moscow';

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $timeZone = self::DEFAULT_TIME_ZONE;

        // get config
        if (array_key_exists('time_zone', $config) && array_key_exists('time_zone', $config['time_zone'])) {

            $timeZone = $config['time_zone']['time_zone'];
        }

        return new TimeZone($timeZone);
    }
}
