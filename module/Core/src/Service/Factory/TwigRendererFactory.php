<?php
namespace Core\Service\Factory;

use Core\Service\TwigRenderer;
use ZendTwig\View\HelperPluginManager as TwigHelperPluginManager;
use Interop\Container\ContainerInterface;
use Twig_Environment;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\View;
use ZendTwig\Resolver\TwigResolver;

/**
 * Class TwigRendererFactory
 * @package Core\Service
 */
class TwigRendererFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $env      = $container->get(Twig_Environment::class);
        $view     = $container->get(View::class);
        $resolver = $container->get(TwigResolver::class);
        $renderer = new TwigRenderer($view, $env, $resolver);

        $renderer->setTwigHelpers($container->get(TwigHelperPluginManager::class));
        $renderer->setZendHelpers($container->get('ViewHelperManager'));

        if (!empty($options['force_standalone'])) {
            $renderer = $renderer->setForceStandalone(true);
        }

        return $renderer;
    }
}
