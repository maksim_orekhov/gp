<?php
namespace Core\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Service\SessionContainerManager;

/**
 * This is the factory class for SessionContainerManager service
 */
class SessionContainerManagerFactory implements FactoryInterface
{
    /**
     * This method creates the SessionContainerManager service and returns its instance.
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return SessionContainerManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $sessionContainer = $container->get('ContainerNamespace');

        return new SessionContainerManager($sessionContainer);
    }
}