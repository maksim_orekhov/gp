<?php
namespace Core\Service\Factory;

use ModuleCode\Api\CodeController;
use ModuleCode\Listener\OperationConfirmCodeDemandListener;
use ModuleCode\Listener\OperationConfirmCodeReceptionListener;
use Core\Provider\Mail\ConfirmationCodeSender;
use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Service\OperationConfirmManager;

/**
 * This is the factory class for OperationConfirmManager
 */
class OperationConfirmManagerFactory implements FactoryInterface
{
    /**
     * This method creates the BaseAuthManager service and returns its instance.
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return OperationConfirmManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $operationConfirmCodeDemandListener = $container->get(OperationConfirmCodeDemandListener::class);
        $operationConfirmCodeReceptionListener = $container->get(OperationConfirmCodeReceptionListener::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $confirmationCodeMailSender = $container->get(ConfirmationCodeSender::class);
        $confirmationCode = $container->get(CodeController::class);
        $config = $container->get('Config');

        return new OperationConfirmManager(
            $entityManager,
            $operationConfirmCodeDemandListener,
            $operationConfirmCodeReceptionListener,
            $userManager,
            $baseAuthManager,
            $confirmationCodeMailSender,
            $confirmationCode,
            $config
        );
    }
}