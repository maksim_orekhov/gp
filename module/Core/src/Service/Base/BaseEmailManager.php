<?php
namespace Core\Service\Base;

use Core\Entity\Interfaces\EmailInterface;
use Application\Entity\Email;
use Doctrine\ORM\EntityManager;

class BaseEmailManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * BaseEmailManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $email_value
     * @return \Application\Entity\Email
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addNewEmail($email_value)
    {
        $email = new Email();
        $email->setEmail($email_value);

        $this->entityManager->persist($email);
        $this->entityManager->flush();

        return $email;
    }

    /**
     * @param $email_value
     * @return bool
     */
    public function checkEmailUnique($email_value)
    {
        /** @var EmailInterface $email */
        $email = $this->getEmailVerifiedByValue($email_value);
        //если найден email то не унекально
        return $email ? false : true;
    }

    /**
     * @param $email_value
     * @return bool
     */
    public function checkEmailExist($email_value)
    {
        return (bool) $this->getEmailVerifiedByValue($email_value);
    }

    /**
     * @param $email_value
     * @return EmailInterface|null
     */
    public function getEmailVerifiedByValue($email_value)
    {
        /** @var EmailInterface $email */
        $email = $this->entityManager->getRepository(Email::class)->findOneBy(['email' => strtolower($email_value), 'isVerified' => true]);

        return $email ?? null;
    }

    /**
     * @param $email_value
     * @return EmailInterface|null
     */
    public function getEmailByValue($email_value)
    {
        /** @var EmailInterface $email */
        $email = $this->entityManager->getRepository(Email::class)->findOneBy(['email' => strtolower($email_value)]);

        return $email;
    }

    /**
     * @param EmailInterface $email
     * @return EmailInterface
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setEmailConfirmed(EmailInterface $email)
    {
        $email->setIsVerified(1);
        $email->setConfirmationToken(null);
        $this->entityManager->persist($email);
        $this->entityManager->flush();

        return $email;
    }

    /**
     * @param $email_value
     * @param $simple_token
     * @return EmailInterface|null
     */
    public function getEmailByValueAndSimpleToken($email_value, $simple_token)
    {
        /** @var EmailInterface $email */
        $email = $this->entityManager->getRepository(Email::class)->findOneBy([
            'email' => strtolower($email_value),
            'confirmationToken' => $simple_token
        ]);

        return $email;
    }

    /**
     * @param EmailInterface $email
     * @param $email_value
     * @return EmailInterface
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setNewValueToEmail(EmailInterface $email, $email_value)
    {
        $email->setEmail(strtolower($email_value));
        $email->setIsVerified(false);

        $this->entityManager->persist($email);
        $this->entityManager->flush($email);

        return $email;
    }
}