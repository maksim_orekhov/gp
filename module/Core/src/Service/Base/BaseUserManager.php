<?php
namespace Core\Service\Base;

use Core\Entity\Interfaces\EmailInterface;
use Core\Entity\Interfaces\PhoneInterface;
use Core\EventManager\AuthEventProvider as AuthEvent;
use ModuleRbac\Entity\Interfaces\ModuleRbacRoleInterface;
use Core\Entity\Interfaces\UserInterface;
use Application\Entity\Role;
use Application\Entity\User;
use Core\Exception\LogicException;
use Core\Service\CodeOperationInterface;
use Zend\Crypt\Password\Bcrypt;
use Doctrine\ORM\EntityManager;
use ModuleCode\Entity\Code;
use Zend\EventManager\EventManager;

class BaseUserManager implements CodeOperationInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * BaseUserManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param UserInterface $user
     * @param EmailInterface $email
     * @return UserInterface
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setEmailToUser(UserInterface $user, EmailInterface $email)
    {
        $user->setEmail($email);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param UserInterface $user
     * @param PhoneInterface $phone
     * @return UserInterface
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setPhoneToUser(UserInterface $user, PhoneInterface $phone)
    {
        $user->setPhone($phone);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param UserInterface $user
     * @param string $login
     * @return UserInterface
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setLoginValueToUser(UserInterface $user, string $login)
    {
        $user->setLogin($login);

        $this->entityManager->persist($user);
        $this->entityManager->flush($user);

        return $user;
    }

    /**
     * @param UserInterface $user
     * @param \ModuleRbac\Entity\Interfaces\ModuleRbacRoleInterface $role
     * @return UserInterface
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setRoleToUser(UserInterface $user, ModuleRbacRoleInterface $role)
    {
        //// если роли нет в коллекции то добавляем ////
        if (!$user->getRoles()->contains( $role )) {

            $user->addRole($role);
            $this->entityManager->persist($user);
            $this->entityManager->flush($user);
        }

        return $user;
    }

    /**
     * @param UserInterface $user
     * @param \ModuleRbac\Entity\Interfaces\ModuleRbacRoleInterface $role
     * @return UserInterface
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeRoleToUser(UserInterface $user, ModuleRbacRoleInterface $role)
    {
        //// если роли есть в коллекции то удаляем ////
        if ($user->getRoles()->contains( $role )) {

            $user->getRoles()->removeElement($role);

            $this->entityManager->persist($user);
            $this->entityManager->flush($user);
        }

        return $user;
    }

    /**
     * @param $login string
     * @return bool
     */
    public function checkLoginUnique($login)
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['login' => $login]);
        //если найден пользователь то не унекально
        return $user ? false : true;
    }

    /**
     * @param int $id
     * Удаление новой регистрации;
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteUserRegistrationByUserId($id)
    {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->find((int) $id);
        if ($user !== null) {
            $this->deleteUserRegistration($user);
        }
    }

    /**
     * @param User $user
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteUserRegistration(User $user)
    {
        $email = $user->getEmail();
        $phone = $user->getPhone();

        $codes = $this->entityManager->getRepository(Code::class)
            ->selectAllUserCodes($user);

        foreach ($codes as $code) {
            $this->entityManager->remove($code);
        }
        if ($email) {
            $this->entityManager->remove($email);
        }
        if ($phone) {
            $this->entityManager->remove($phone);
        }
        $this->entityManager->remove($user);

        $this->entityManager->flush();
    }

    /**
     * @param $login
     * @return UserInterface|null
     */
    public function getUserByLogin($login)
    {
        /** @var UserInterface $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(array('login' => $login));

        return $user ?? null;
    }

    /**
     * @return array
     */
    public function getAllUsers()
    {
        return $this->entityManager->getRepository(User::class)->findAll();
    }

    /**
     * @param EmailInterface $email
     * @return UserInterface|null
     */
    public function getUserByEmail(EmailInterface $email)
    {
        /** @var UserInterface $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(array('email' => $email));

        return $user;
    }

    /**
     * @param PhoneInterface $phone
     * @return UserInterface|null
     */
    public function getUserByPhone(PhoneInterface $phone)
    {
        /** @var UserInterface $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(array('phone' => $phone));

        return $user;
    }

    /**
     * @param $simple_token
     * @return UserInterface|null
     */
    public function getUserByPasswordResetToken($simple_token)
    {
        /** @var UserInterface|null $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['passwordResetToken' => $simple_token]);

        return $user;
    }

    /**
     * @param UserInterface $user
     * @param $new_password
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setUserPasswordByToken(UserInterface $user, $new_password)
    {
        $password = $this->createPassword($new_password);
        $user->setPassword($password);
        // Remove password reset token
        $user->setPasswordResetToken(null);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @param $password
     * @return string
     */
    public function createPassword($password){
        $bCrypt = new Bcrypt();

        return $bCrypt->create($password);
    }

    /**
     * @param UserInterface $user
     * @param EventManager $authEventManager
     * @return void
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function verifyUserConfirms(UserInterface $user, EventManager $authEventManager)
    {
        $is_phone_verified = $user->getPhone() ? $user->getPhone()->getIsVerified() : false;
        $is_email_verified = $user->getEmail() ? $user->getEmail()->getIsVerified() : false;

        //переключаем роль пользователя по подтверждению телефона и email
        $is_user_verified = $this->checkAndSwitchUserRoleByPhoneAndEmailVerification($user, $is_phone_verified, $is_email_verified);

        $authEventManager->trigger(AuthEvent::EVENT_VERIFY_USER, $this, [
            'verify_user_params' => [
                'is_user_verified' => $is_user_verified,
                'user' => $user
            ]
        ]);

        $authEventManager->trigger(AuthEvent::EVENT_VERIFY_PHONE, $this, [
            'verify_phone_params' => [
                'is_phone_verified' => $is_phone_verified,
                'user' => $user
            ]
        ]);

        $authEventManager->trigger(AuthEvent::EVENT_VERIFY_EMAIL, $this, [
            'verify_email_params' => [
                'is_email_verified' => $is_email_verified,
                'user' => $user
            ]
        ]);
    }

    /**
     * @param UserInterface $user
     * @param bool $phoneIsVerified
     * @param bool $emailIsVerified
     * @return bool
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function checkAndSwitchUserRoleByPhoneAndEmailVerification(UserInterface $user, $phoneIsVerified = false, $emailIsVerified = false): bool
    {
        /** @var ModuleRbacRoleInterface $roleVerified */
        $roleVerified = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => 'Verified'));
        /** @var ModuleRbacRoleInterface $roleUnverified */
        $roleUnverified = $this->entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => 'Unverified'));

        if( ! $roleUnverified || ! $roleVerified ) {
            throw new LogicException(null, LogicException::ROLE_NOT_FOUND);
        }

        if($phoneIsVerified && $emailIsVerified) {
            if ($user->getRoles()->contains( $roleUnverified )) {
                //добавляем роль Verified
                $this->setRoleToUser($user, $roleVerified);
                //удаляем роль Unverified
                $this->removeRoleToUser($user, $roleUnverified);
            }

            return true;
        }

        //добавляем роль Unverified
        $this->setRoleToUser($user, $roleUnverified);
        //удаляем роль Verified
        $this->removeRoleToUser($user, $roleVerified);

        return false;
    }

    /**
     * @param $length
     * @param string $key_space
     * @return string
     * @throws \Exception
     */
    public function randomPassword(
        $length,
        $key_space = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ): string
    {
        $str = '';
        $max = mb_strlen($key_space, '8bit') - 1;
        if ($max < 1) {
            throw new \Exception('$key_space must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $key_space[random_int(0, $max)];
        }
        return $str;
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public function removeDuplicateUserRegistrations(User $user)
    {
        if (!$user->getEmail()->getIsVerified()) {

            throw new LogicException(null, LogicException::USER_WITH_UNCONFIRMED_EMAIL_CANNOT_DELETE_DUPLICATE_USERS);
        }

        $allUser = $this->entityManager->getRepository(User::class)
            ->findAllUnverifiedUserByEmail($user->getEmail()->getEmail());

        foreach ($allUser as $oneUser) {
            $this->deleteUserRegistration($oneUser);
        }
    }
}