<?php
namespace Core\Service\Base;

use ModuleRbac\Entity\Interfaces\ModuleRbacRoleInterface;
use Core\Entity\Interfaces\UserInterface;
use Doctrine\ORM\EntityManager;

class BaseRoleManager
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * BaseUserManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param UserInterface $user
     * @param \ModuleRbac\Entity\Interfaces\ModuleRbacRoleInterface $role
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addRoleByLogin(UserInterface $user, ModuleRbacRoleInterface $role)
    {
        if( !$user->getRoles()->contains($role) ) {

            $user->addRole($role);
        }

        $this->entityManager->flush();

        return true;
    }
}