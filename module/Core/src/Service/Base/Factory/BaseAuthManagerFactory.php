<?php
namespace Core\Service\Base\Factory;

use Core\EventManager\AuthEventProvider;
use Core\Service\Base\BaseAuthManager;
use ModuleRbac\Service\RbacManager;
use Core\Service\Auth\AuthService;
use Interop\Container\ContainerInterface;
use Zend\Http\Response;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Session\SessionManager;

/**
 * Class BaseAuthManagerFactory
 * @package Core\Service\Factory
 */
class BaseAuthManagerFactory implements FactoryInterface
{
    /**
     * This method creates the BaseAuthManager service and returns its instance
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BaseAuthManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        // Instantiate dependencies.
        /** @var \Core\Service\Auth\AuthService $authService */
        $authService = $container->get(AuthService::class);
        $sessionManager = $container->get(SessionManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $registrationEventProvider = $container->get(AuthEventProvider::class);
        $application = $container->get('Application');
        $response = $application->getResponse();
        // При запуске консольных команд $response имеет тип Zend\Console\Response.
        // Если $response не экземпляр \Zend\Http\Response - обнуляем.
        if( !$response instanceof Response) {
            $response = null;
        }
        $config = $container->get('Config');

        // Instantiate the BaseAuthManager service and inject dependencies to its constructor.
        return new BaseAuthManager(
            $authService,
            $sessionManager,
            $rbacManager,
            $registrationEventProvider->getEventManager(),
            $response,
            $config
        );
    }
}