<?php
namespace Core\Service\Base\Factory;

use Core\Service\Base\BaseEmailManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class BaseEmailManagerFactory
 * @package Core\Service
 */
class BaseEmailManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BaseEmailManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new BaseEmailManager(
            $entityManager
        );
    }
}
