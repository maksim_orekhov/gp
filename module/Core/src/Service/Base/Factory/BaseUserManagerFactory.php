<?php
namespace Core\Service\Base\Factory;

use Core\Service\Base\BaseUserManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class BaseUserManagerFactory
 * @package Core\Service
 */
class BaseUserManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BaseUserManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new BaseUserManager(
            $entityManager
        );
    }
}
