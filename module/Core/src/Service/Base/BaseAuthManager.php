<?php
namespace Core\Service\Base;

use Core\Controller\Plugin\Message\BaseMessageCodeInterface;
use Core\Exception\LogicException;
use Core\Service\Auth\AuthAdapterInterface;
use Core\Service\Auth\AuthService;
use ModuleAuth\Controller\AuthController;
use Application\Entity\User;
use ModuleRbac\Service\RbacManager;
use Zend\Authentication\Result;
use Zend\EventManager\EventManager;
use Zend\Http\Header\SetCookie;
use Zend\Http\Response;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;
use Core\EventManager\AuthEventProvider as AuthEvent;

/**
 * Class BaseAuthManager
 * @package Core\Service
 */
class BaseAuthManager
{
    use \Core\Controller\Plugin\Message\MessageJsonModelTrait;

    const USER_AUTHORIZATION_FAILED = 'User authorization failed';

    // Constants returned by the access filter.
    const ACCESS_GRANTED = 1; // Access to the page is granted.
    const AUTH_REQUIRED  = 2; // Authentication is required to see the page.
    const ACCESS_DENIED  = 3; // Access to the page is denied.

    /**
     * @var AuthService
     */
    private $authService;

    /**
     * @var SessionManager
     */
    private $sessionManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var EventManager
     */
    private $eventManager;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var array
     */
    private $config;

    /**
     * BaseAuthManager constructor.
     * @param AuthService $authService
     * @param SessionManager $sessionManager
     * @param RbacManager $rbacManager
     * @param EventManager $eventManager
     * @param Response|null $response
     * @param $config
     */
    public function __construct(AuthService $authService,
                                SessionManager $sessionManager,
                                RbacManager $rbacManager,
                                EventManager $eventManager,
                                Response $response = null,
                                $config)
    {
        $this->authService       = $authService;
        $this->sessionManager    = $sessionManager;
        $this->rbacManager       = $rbacManager;
        $this->eventManager      = $eventManager;
        $this->response          = $response;
        $this->config            = $config ;
    }

    /**
     * @param User|null $user
     * @return bool
     * @throws \Exception
     */
    public function userAutoLogin(User $user = null)
    {
        if($user === null){

            return false;
        }
        // If user not logged in, log in
        $old_identity = $this->getIdentity();
        if(!$old_identity) {
            $this->login($user->getLogin(), 'no_pass', 0, false);
        }
        if ($old_identity && strtolower($old_identity) !== strtolower($user->getLogin())) {
            // If logged in other user (другой пользователь забыл разлогиниться)
            $this->logout();
            $this->sessionManager->start();
            $this->sessionManager->regenerateId();
            $this->login($user->getLogin(), 'no_pass', 0, false);
        }
        $new_identity = $this->getIdentity();

        return strtolower($new_identity) === strtolower($user->getLogin());
    }

    /**
     * Performs a login attempt. If $rememberMe argument is true, it forces the session
     * to last for one month (otherwise the session expires on one hour).
     *
     * @param $login
     * @param $password
     * @param $remember_me
     * @param bool $verify_password
     * @return \Zend\Authentication\Result
     * @throws LogicException
     * @throws \Exception
     */
    public function login($login, $password, $remember_me, $verify_password = true): Result
    {
        if ($this->authService->getIdentity() !== null) {

            throw new LogicException(null, LogicException::ALREADY_AUTHORIZED);
        }
        /** @var AuthAdapterInterface $authAdapter */
        $authAdapter = $this->authService->getAdapter();
        $authAdapter->setLogin($login);
        $authAdapter->setPassword($password);
        $authAdapter->setVerifyPassword($verify_password);

        // Attempt authentication, saving the result:
        $result = $this->authService->authenticate();

        if($result->isValid()) {
            if($this->response) {
                $this->setCookie(time() + $this->config['tokens']['guarant_pay_login_cookie_lifetime'], $login);
            }
            if ($remember_me) {
                // Session cookie will expire in time specified in $config['cookie_lifetime_with_remember_me']
                $this->sessionManager->rememberMe($this->config['cookie_lifetime_with_remember_me']);
            }
            $this->eventManager->trigger(AuthEvent::EVENT_VERIFY_USER_CONFIRMS, $this, [
                'identity' => $result->getIdentity()
            ]);
        } else {

            throw new LogicException(null, LogicException::AUTHENTICATION_FAILED);
        }

        return $result;
    }

    /**
     * @param $login
     * @return bool
     */
    public function checkLoginExist($login): bool
    {
        /** @var AuthAdapterInterface $authAdapter */
        $authAdapter = $this->authService->getAdapter();
        $authAdapter->setLogin($login);

        return $authAdapter->checkLoginExist();
    }

    /**
     * Check password
     *
     * @param $login
     * @param $password
     * @return Result
     * @throws \Exception
     */
    public function checkPassword($login, $password)
    {
        // Authenticate with login/password.
        /** @var AuthAdapterInterface $authAdapter */
        $authAdapter = $this->authService->getAdapter();
        $authAdapter->setLogin($login);
        $authAdapter->setPassword($password);
        // Check credentials
        $result = $authAdapter->checkCredentials();
        // If authentication fails
        if ($result->getCode() !== Result::SUCCESS) {

            throw new \Exception("Check password failed");
        }

        return $result;
    }

    /**
     * Performs user logout.
     * @throws \Exception
     */
    public function logout()
    {
        // Allow to log out only when user is logged in.
        if ($this->authService->getIdentity() === null) {

            throw new \Exception('Can not log out. The user is not logged in.');
        }

        // Remove identity from session.
        $this->authService->clearIdentity();

        // Cookie delete
        $this->setCookie(time() - $this->config['tokens']['guarant_pay_login_cookie_lifetime']);

        $this->sessionManager->destroy();
    }

    /**
     * @param $expire_time
     * @param string $login
     */
    private function setCookie($expire_time, $login='')
    {
        $cookie = new SetCookie(
            'guarant-pay-login',
            $login,
            $expire_time,
            '/'
        );
        $this->response->getHeaders()->addHeader($cookie);
    }

    /**
     * This is a simple access control filter. It is able to restrict unauthorized
     * users to visit certain pages.
     *
     * This method uses the 'access_filter' key in the config file and determines
     * whenther the current visitor is allowed to access the given controller action
     * or not. It returns true if allowed; otherwise false.
     * @param $controllerName
     * @param $actionName
     * @return int
     * @throws \Exception
     */
    public function filterAccess($controllerName, $actionName)
    {
        // Определяем режим – 'ограничительный' (по умолчанию) или 'разрешительный'. В ограничительном
        // режиме все действия контроллера должны быть явно перечислены под ключом конфигурации 'access_filter',
        // и доступ к любому не указанному действию для неавторизованных пользователей запрещен.
        // В разрешительном режиме, если действие не указано под ключом 'access_filter', доступ к нему
        // разрешен для всех (даже для незалогиненных пользователей).
        // Ограничительный режим является более безопасным, и рекомендуется использовать его.
        $mode = $this->config['access_filter']['options']['mode'] ?? 'restrictive';
        if ($mode !== 'restrictive' && $mode !== 'permissive') {

            throw new \RuntimeException('Invalid access filter mode (expected either restrictive or permissive mode');
        }
        if (isset($this->config['access_filter']['controllers'][$controllerName])) {

            $items = $this->config['access_filter']['controllers'][$controllerName];
            foreach ($items as $item) {
                $actionList = $item['actions'];
                if ($actionList === '*' || (\is_array($actionList) && \in_array($actionName, $actionList, true))) {
                    $access_status = null;
                    foreach ($item['allows'] as $allow) {
                        if ($allow === '*') {
                            // Все могут просматривать эту страницу
                            return self::ACCESS_GRANTED;
                        }
                        if (!$this->rbacManager->hasIdentity()) {
                            // Только аутентифицированные пользователи могут просматривать страницу
                            // Если не '*' и пользователь не авторизован, то перенаправляем на страницу авторизации
                            return self::AUTH_REQUIRED;
                        }
                        if ($allow === '@') {
                            // Любой аутентифицированный пользователь может просматривать страницу
                            return self::ACCESS_GRANTED;
                        }
                        if ($allow[0] === '@') {
                            // Только конкретный пользователь может просматривать страницу
                            $identity = substr($allow, 1);
                            if ($this->getIdentity() === $identity) {
                                return self::ACCESS_GRANTED;
                            }
                            $access_status = self::ACCESS_DENIED;
                        } else if ($allow[0] === '#') {
                            // Только пользователи с этой ролью могут просматривать страницу
                            $role_name = substr($allow, 1);
                            if ($this->rbacManager->hasRole(null, $role_name)) {
                                return self::ACCESS_GRANTED;
                            }
                            $access_status = self::ACCESS_DENIED;
                        } else if ($allow[0] === '+') {
                            // Только пользователи с этой привилегией могут просматривать страницу
                            $permission = substr($allow, 1);
                            if ($this->rbacManager->isGranted(null, $permission)) {
                                return self::ACCESS_GRANTED;
                            }
                            $access_status = self::ACCESS_DENIED;
                        }
                    }
                    if($access_status === null && !empty($item['allows'])) {
                        throw new \RuntimeException('Unexpected value for "allow" - expected ' .
                            'either "?", "@", "@identity", "#role" or "+permission"');
                    }

                    return $access_status;
                }
            }
        }

        // В ограничительном режиме мы требуем аутентификации для любого действия, не
        // перечисленного под ключом 'access_filter' и отказываем в доступе авторизованным пользователям
        // (из соображений безопасности).
        if ($mode === 'restrictive') {
            if(!$this->rbacManager->hasIdentity()) {

                return self::AUTH_REQUIRED;
            }
            return self::ACCESS_DENIED;
        }

        return self::ACCESS_GRANTED;
    }

    /**
     * Returns current user identity (login) or null if user is not authenticated
     *
     * @return mixed|null
     */
    public function getIdentity()
    {
        if($this->authService->hasIdentity()) {

            return $this->authService->getIdentity();
        }

        return null;
    }

    /**
     * @param MvcEvent $e
     * @return mixed
     *
     * @throws \Exception
     */
    public function dispatchAccess(MvcEvent $e)
    {
        // Get controller and action to which the HTTP request was dispatched
        $controller = $e->getTarget();
        $controllerName = $e->getRouteMatch()->getParam('controller', null);
        $actionName = $e->getRouteMatch()->getParam('action', null);

        // Layout toggle
        if ($this->getIdentity()) {
            $e->getViewModel()->setTemplate('layout/application');
        } else {
            $e->getViewModel()->setTemplate('layout/authentication');
        }

        // Convert dash-style action name to camel-case
        $actionName = str_replace('-', '', lcfirst(ucwords($actionName, '-')));

        // Execute the access filter on every controller except AuthController (to avoid infinite redirect)
        if ($controllerName !== AuthController::class) {
            $result = $this->filterAccess($controllerName, $actionName);
            /** @var Response $response */
            $response = $e->getResponse();
            /** @var Request $request */
            $request = $e->getRequest();

            if ($result === self::AUTH_REQUIRED) {
                // Remember the URL of the page the user tried to access. We will
                // redirect the user to that URL after successful login.
                $uri = $request->getUri();
                // Make the URL relative (remove scheme, user info, host name and port)
                // to avoid redirecting to other domain by a malicious user.
                $uri->setScheme(null)
                    ->setHost(null)
                    ->setPort(null)
                    ->setUserInfo(null);
                $redirectUrl = $uri->toString();
                //ajax response
                if ($request->isXmlHttpRequest()) {
                    $response->setStatusCode(200);
                    $viewModel = $this->JsonErrorModel(
                        BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED_MESSAGE,
                        BaseMessageCodeInterface::ERROR_NOT_AUTHORIZED,
                        ['redirectUrl' => $redirectUrl]
                    );
                    $e->setViewModel($viewModel);
                    $e->setResult($viewModel);
                    $e->stopPropagation(true);

                    return $viewModel;
                }

                // Redirect the user to the "Login" page.
                return $controller->redirect()->toRoute('login', [], ['query'=>['redirectUrl'=>$redirectUrl]]);

            }
            if ($result === self::ACCESS_DENIED) {
                //ajax response
                if ($request->isXmlHttpRequest()) {
                    $response->setStatusCode(200);
                    $viewModel = $this->JsonErrorModel(
                        BaseMessageCodeInterface::ERROR_ACCESS_DENIED_MESSAGE,
                        BaseMessageCodeInterface::ERROR_ACCESS_DENIED,
                        null
                    );
                    $e->setViewModel($viewModel);
                    $e->setResult($viewModel);
                    $e->stopPropagation(true);

                    return $viewModel;
                }

                // Redirect the user to the "Not Authorized" page.
                return $controller->redirect()->toRoute('access-denied');
            }
        }

        return null;
    }
}