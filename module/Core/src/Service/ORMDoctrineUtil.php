<?php
namespace Core\Service;

use Doctrine\ORM\EntityManager;

class ORMDoctrineUtil
{
    /**
     * @see https://github.com/doctrine/doctrine2/issues/5906 (Proposed solution (part 4 of 4))
     * @param EntityManager $em
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public static function rollBackTransaction(EntityManager $em)
    {
        // DB Transaction rollback
        if ( $em->getConnection()->isTransactionActive() ) {
            $em->getConnection()->beginTransaction();
            $em->getConnection()->rollBack();
            $em->getConnection()->close();
        }
    }
}
