<?php
namespace Core\Service;
use Core\Exception\LogicException;

/**
 * Class SessionContainerManager
 * @package Core\Service
 */
class SessionContainerManager
{
    /**
     * SessionContainer
     * @var \Zend\Session\Container
     */
    private $sessionContainer;

    /**
     * SessionContainerManager constructor.
     * @param \Zend\Session\Container $sessionContainer
     */
    public function __construct(\Zend\Session\Container $sessionContainer)
    {
        $this->sessionContainer = $sessionContainer;
    }

    /**
     * Set variable to session
     *
     * @param string $var_name
     * @param null   $var_value
     */
    public function setSessionVar($var_name, $var_value=null)
    {
        if ($var_name) {
            $this->sessionContainer->{$var_name} = $var_value;
        }
    }

    /**
     * Get variable from session
     *
     * @param $var_name
     * @return mixed
     */
    public function getSessionVar($var_name)
    {
        return $this->sessionContainer->{$var_name};
    }

    /**
     * @param $code_operation_type
     * @return mixed
     * @throws LogicException
     */
    public function getLoginByOperationType($code_operation_type)
    {
        if( !array_key_exists($code_operation_type, $this->sessionContainer->initiator)) {

            throw new LogicException(null, LogicException::SESSION_CODE_OPERATION_TYPE_NOT_FOUND);
        }

        return $this->sessionContainer->initiator[$code_operation_type]['login'];
    }

    /**
     * Add element to session array
     *
     * @param string $arr_name
     * @param string $key
     * @param array|string $var_value
     */
    public function addElementToSessionArray($arr_name, $key, $var_value)
    {
        if ($arr_name) {
            $this->sessionContainer->{$arr_name}[$key] = $var_value;
        }
    }

    /**
     * @param $code_operation_type_name
     * @param $login
     */
    public function addProcessInitiatorWithCurrentCodeOperationType($code_operation_type_name, $login)
    {
        $this->addElementToSessionArray('initiator', $code_operation_type_name, ['login'=>$login]);
    }

    /**
     * Remove element from session array
     *
     * @param string $arr_name
     * @param string $arr_vars
     */
    public function removeElementFromSessionArray($arr_name, $arr_vars)
    {
        $arr = $this->sessionContainer->{$arr_name};
        unset($arr[$arr_vars]);
        $this->sessionContainer->{$arr_name} = $arr;
    }

    /**
     * @param $code_operation_type_name
     */
    public function deleteProcessInitiatorForCurrentCodeOperationType($code_operation_type_name)
    {
        $this->removeElementFromSessionArray('initiator', $code_operation_type_name);
    }

    /**
     * @param $code_operation_type_name
     * @return bool
     */
    public function isProcessInitiatorExists($code_operation_type_name)
    {
        if( !array_key_exists($code_operation_type_name, $this->sessionContainer->initiator) ) {

            return false;
        }

        return true;
    }

    /**
     * Remove variable from session
     *
     * @param string $var_name
     */
    public function deleteSessionVar($var_name)
    {
        $this->sessionContainer->offsetUnset($var_name);
    }
}