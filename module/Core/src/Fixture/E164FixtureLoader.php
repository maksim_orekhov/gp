<?php

namespace ModuleAuth\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\E164;

class E164FixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $phone = new E164();
        $phone->setCountryCode(7);
        $phone->setRegionCode(999);
        $phone->setphoneNumber(1112233);

        $entityManager->persist($phone);
        $entityManager->flush();
    }

    public function getOrder()
    {
        return 60;
    }
}