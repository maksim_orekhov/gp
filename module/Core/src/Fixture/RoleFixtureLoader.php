<?php

namespace ModuleRbac\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Role;
use Application\Entity\Permission;

class RoleFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const OPERATOR_PERMISSIONS = [
        'profile.own.edit',
        'bank-client.view',
        'bank-client.execute',
        'deal.all.view',
        'message.all.view',
        'message.own.edit',
        'message.own.delete'
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        //profile
        $profilePermission = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'profile.any.view'));
        $profilePermission1 = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'profile.own.edit'));
        //deal
        $dealPermission = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'deal.any'));
        $dealPermission1 = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'deal.own.view'));
        $dealPermission2 = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'deal.own.edit'));
        $dealPermission3 = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'deal.own.customer.invoice'));
        $dealPermission4 = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'deal.own.contract'));
        $dealPermission5 = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'deal.own.customer'));
        $dealPermission6 = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'deal.own.contractor'));
        $disputPermission = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'dispute.own.toggle'));

        //admin
        $administrator_permission = $entityManager->getRepository(Permission::class)
            ->findOneBy(array('name' => 'settings.edit'));

        $role_1 = new Role();
        $role_1->setName('Unverified');
        $entityManager->persist($role_1);

        $role_2 = new Role();
        $role_2->setName('Verified');
        $role_2->addPermission($profilePermission);
        $role_2->addPermission($profilePermission1);
        $role_2->addPermission($dealPermission);
        $role_2->addPermission($dealPermission1);
        $role_2->addPermission($dealPermission2);
        $role_2->addPermission($dealPermission3);
        $role_2->addPermission($dealPermission4);
        $role_2->addPermission($dealPermission5);
        $role_2->addPermission($dealPermission6);
        $role_2->addPermission($disputPermission);
        $entityManager->persist($role_2);

        $role_3 = new Role();
        $role_3->setName('Operator');
        foreach (self::OPERATOR_PERMISSIONS as $operator_permission) {
            $operator_permission = $entityManager->getRepository(Permission::class)
                ->findOneBy(array('name' => $operator_permission));
            $role_3->addPermission($operator_permission);
        }
        $role_3->addPermission($dealPermission4); // 'deal.own.contract'
        $entityManager->persist($role_3);

        $role_4 = new Role();
        $role_4->setName('Administrator');
        $role_4->addPermission($profilePermission);
        $role_4->addPermission($profilePermission1);
        $role_4->addPermission($administrator_permission);
        $entityManager->persist($role_4);

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 50;
    }
}