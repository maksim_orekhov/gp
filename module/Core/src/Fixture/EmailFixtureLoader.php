<?php
namespace ModuleAuth\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Email;

class EmailFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    const EMAIL_VALUES = [
        'test@simple-technology.ru',
        'test2@simple-technology.ru',
        'operator@guarantpay.com',
        'admin@guarantpay.com'
    ];

    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        foreach (self::EMAIL_VALUES as $email_value) {
            $email = new Email();
            $email->setEmail($email_value);
            $email->setIsVerified(true);
            $entityManager->persist($email);
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 50;
    }
}