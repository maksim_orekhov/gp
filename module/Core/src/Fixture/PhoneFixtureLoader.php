<?php

namespace ModuleAuth\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Phone;
use Application\Entity\Country;

class PhoneFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    const PHONE_VALUES = ['79033463906', '79033463907', '79033463909', '79033463911'];

    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $country = $entityManager->getRepository(Country::class)
            ->findOneBy(array('code' => 'RU'));

        foreach (self::PHONE_VALUES as $phone_value) {
            $phone = new Phone();
            $phone->setUserEnter($phone_value);
            $phone->setPhoneNumber($phone_value);
            $phone->setIsVerified(true);
            $phone->setCountry($country);

            $entityManager->persist($phone);
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 50;
    }
}