<?php
namespace ModuleAuth\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\User;
use Application\Entity\Phone;
use Application\Entity\Email;
use Application\Entity\Role;
use Zend\Crypt\Password\Bcrypt;

class UserFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        // Relations
        $phone = $entityManager->getRepository(Phone::class)
            ->findOneBy(array('phoneNumber' => '79033463906'));
        $email = $entityManager->getRepository(Email::class)
            ->findOneBy(array('email' => 'test@simple-technology.ru'));

        $phone2 = $entityManager->getRepository(Phone::class)
        ->findOneBy(array('phoneNumber' => '79033463907'));
        $email2 = $entityManager->getRepository(Email::class)
            ->findOneBy(array('email' => 'test2@simple-technology.ru'));

        $phone3 = $entityManager->getRepository(Phone::class)
            ->findOneBy(array('phoneNumber' => '79033463909'));
        $email3 = $entityManager->getRepository(Email::class)
            ->findOneBy(array('email' => 'operator@guarantpay.com'));

        $phone4 = $entityManager->getRepository(Phone::class)
            ->findOneBy(array('phoneNumber' => '79033463911'));
        $email4 = $entityManager->getRepository(Email::class)
            ->findOneBy(array('email' => 'admin@guarantpay.com'));

        $role = $entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => 'Verified'));

        $role2 = $entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => 'Operator'));

        $role3 = $entityManager->getRepository(Role::class)
            ->findOneBy(array('name' => 'Administrator'));

        $user = new User();
        $user->setLogin('test');
        // Encrypt password and store the password in encrypted state.
        $bcrypt = new Bcrypt();
        $passwordHash = $bcrypt->create('qwerty');
        $user->setPassword($passwordHash);
        // Current date
        $user->setCreated(new \DateTime(date('Y-m-d H:i:s')));
        $user->setIsBanned(false);
        $user->setPhone($phone);
        $user->setEmail($email);
        $user->addRole($role);

        $entityManager->persist($user);

        $user2 = new User();
        $user2->setLogin('test2');
        // Encrypt password and store the password in encrypted state.
        $bcrypt = new Bcrypt();
        $passwordHash = $bcrypt->create('qwerty');
        $user2->setPassword($passwordHash);
        // Current date
        $user2->setCreated(new \DateTime(date('Y-m-d H:i:s')));
        $user2->setIsBanned(false);
        $user2->setPhone($phone2);
        $user2->setEmail($email2);
        $user2->addRole($role);

        $entityManager->persist($user2);

        $user3 = new User();
        $user3->setLogin('operator');
        // Encrypt password and store the password in encrypted state.
        $bcrypt = new Bcrypt();
        $passwordHash = $bcrypt->create('qwerty');
        $user3->setPassword($passwordHash);
        // Current date
        $user3->setCreated(new \DateTime(date('Y-m-d H:i:s')));
        $user3->setIsBanned(false);
        $user3->setPhone($phone3);
        $user3->setEmail($email3);
        $user3->addRole($role2);

        $entityManager->persist($user3);

        $user4 = new User();
        $user4->setLogin('admin');
        // Encrypt password and store the password in encrypted state.
        $bcrypt = new Bcrypt();
        $passwordHash = $bcrypt->create('qwerty');
        $user4->setPassword($passwordHash);
        // Current date
        $user4->setCreated(new \DateTime(date('Y-m-d H:i:s')));
        $user4->setIsBanned(false);
        $user4->setPhone($phone4);
        $user4->setEmail($email4);
        $user4->addRole($role3);

        $entityManager->persist($user4);

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 60;
    }
}