<?php

namespace ModuleAuth\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Country;

class CountryFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        /* Шаблон
        $country = new Country();
        $country->setName('United States');
        $country->setCode('US');
        $country->setCode3('USA');
        $country->setPhoneCode(1);
        $country->setPostcodeRequired(0);
        $country->setIsEu(0);
        $country->setWeight(0);

        $entityManager->persist($country);
        */

// United States
        $country1 = new Country();
        $country1->setId(1);
        $country1->setName("United States");
        $country1->setCode("US");
        $country1->setCode3("USA");
        $country1->setPhoneCode(1);
        $country1->setPostcodeRequired(0);
        $country1->setIsEu(0);
        $country1->setWeight(0);

        $entityManager->persist($country1);

// Canada
        $country2 = new Country();
        $country2->setId(2);
        $country2->setName("Canada");
        $country2->setCode("CA");
        $country2->setCode3("CAN");
        $country2->setPhoneCode(1);
        $country2->setPostcodeRequired(0);
        $country2->setIsEu(0);
        $country2->setWeight(0);

        $entityManager->persist($country2);

// Afghanistan
        $country3 = new Country();
        $country3->setId(3);
        $country3->setName("Afghanistan");
        $country3->setCode("AF");
        $country3->setCode3("AFG");
        $country3->setPhoneCode(93);
        $country3->setPostcodeRequired(0);
        $country3->setIsEu(0);
        $country3->setWeight(0);

        $entityManager->persist($country3);

// Albania
        $country4 = new Country();
        $country4->setId(4);
        $country4->setName("Albania");
        $country4->setCode("AL");
        $country4->setCode3("ALB");
        $country4->setPhoneCode(355);
        $country4->setPostcodeRequired(0);
        $country4->setIsEu(0);
        $country4->setWeight(0);

        $entityManager->persist($country4);

// Algeria
        $country5 = new Country();
        $country5->setId(5);
        $country5->setName("Algeria");
        $country5->setCode("DZ");
        $country5->setCode3("DZA");
        $country5->setPhoneCode(213);
        $country5->setPostcodeRequired(0);
        $country5->setIsEu(0);
        $country5->setWeight(0);

        $entityManager->persist($country5);

// American Samoa
        $country6 = new Country();
        $country6->setId(6);
        $country6->setName("American Samoa");
        $country6->setCode("DS");
        $country6->setCode3("ASM");
        $country6->setPhoneCode(44);
        $country6->setPostcodeRequired(0);
        $country6->setIsEu(0);
        $country6->setWeight(0);

        $entityManager->persist($country6);

// Andorra
        $country7 = new Country();
        $country7->setId(7);
        $country7->setName("Andorra");
        $country7->setCode("AD");
        $country7->setCode3("AND");
        $country7->setPhoneCode(376);
        $country7->setPostcodeRequired(0);
        $country7->setIsEu(0);
        $country7->setWeight(0);

        $entityManager->persist($country7);

// Angola
        $country8 = new Country();
        $country8->setId(8);
        $country8->setName("Angola");
        $country8->setCode("AO");
        $country8->setCode3("AGO");
        $country8->setPhoneCode(244);
        $country8->setPostcodeRequired(0);
        $country8->setIsEu(0);
        $country8->setWeight(0);

        $entityManager->persist($country8);

// Anguilla
        $country9 = new Country();
        $country9->setId(9);
        $country9->setName("Anguilla");
        $country9->setCode("AI");
        $country9->setCode3("AIA");
        $country9->setPhoneCode(1264);
        $country9->setPostcodeRequired(0);
        $country9->setIsEu(0);
        $country9->setWeight(0);

        $entityManager->persist($country9);

// Antarctica
        $country10 = new Country();
        $country10->setId(10);
        $country10->setName("Antarctica");
        $country10->setCode("AQ");
        $country10->setCode3("ATA");
        $country10->setPhoneCode(672);
        $country10->setPostcodeRequired(0);
        $country10->setIsEu(0);
        $country10->setWeight(0);

        $entityManager->persist($country10);

// Antigua and Barbuda
        $country11 = new Country();
        $country11->setId(11);
        $country11->setName("Antigua and Barbuda");
        $country11->setCode("AG");
        $country11->setCode3("ATG");
        $country11->setPhoneCode(1268);
        $country11->setPostcodeRequired(0);
        $country11->setIsEu(0);
        $country11->setWeight(0);

        $entityManager->persist($country11);

// Argentina
        $country12 = new Country();
        $country12->setId(12);
        $country12->setName("Argentina");
        $country12->setCode("AR");
        $country12->setCode3("ARG");
        $country12->setPhoneCode(54);
        $country12->setPostcodeRequired(0);
        $country12->setIsEu(0);
        $country12->setWeight(0);

        $entityManager->persist($country12);

// Armenia
        $country13 = new Country();
        $country13->setId(13);
        $country13->setName("Armenia");
        $country13->setCode("AM");
        $country13->setCode3("ARM");
        $country13->setPhoneCode(374);
        $country13->setPostcodeRequired(0);
        $country13->setIsEu(0);
        $country13->setWeight(0);

        $entityManager->persist($country13);

// Aruba
        $country14 = new Country();
        $country14->setId(14);
        $country14->setName("Aruba");
        $country14->setCode("AW");
        $country14->setCode3("ABW");
        $country14->setPhoneCode(297);
        $country14->setPostcodeRequired(0);
        $country14->setIsEu(0);
        $country14->setWeight(0);

        $entityManager->persist($country14);

// Australia
        $country15 = new Country();
        $country15->setId(15);
        $country15->setName("Australia");
        $country15->setCode("AU");
        $country15->setCode3("AUS");
        $country15->setPhoneCode(61);
        $country15->setPostcodeRequired(0);
        $country15->setIsEu(0);
        $country15->setWeight(0);

        $entityManager->persist($country15);

// Austria
        $country16 = new Country();
        $country16->setId(16);
        $country16->setName("Austria");
        $country16->setCode("AT");
        $country16->setCode3("AUT");
        $country16->setPhoneCode(43);
        $country16->setPostcodeRequired(0);
        $country16->setIsEu(0);
        $country16->setWeight(0);

        $entityManager->persist($country16);

// Azerbaijan
        $country17 = new Country();
        $country17->setId(17);
        $country17->setName("Azerbaijan");
        $country17->setCode("AZ");
        $country17->setCode3("AZE");
        $country17->setPhoneCode(994);
        $country17->setPostcodeRequired(0);
        $country17->setIsEu(0);
        $country17->setWeight(0);

        $entityManager->persist($country17);

// Bahamas
        $country18 = new Country();
        $country18->setId(18);
        $country18->setName("Bahamas");
        $country18->setCode("BS");
        $country18->setCode3("BHS");
        $country18->setPhoneCode(1242);
        $country18->setPostcodeRequired(0);
        $country18->setIsEu(0);
        $country18->setWeight(0);

        $entityManager->persist($country18);

// Bahrain
        $country19 = new Country();
        $country19->setId(19);
        $country19->setName("Bahrain");
        $country19->setCode("BH");
        $country19->setCode3("BHR");
        $country19->setPhoneCode(973);
        $country19->setPostcodeRequired(0);
        $country19->setIsEu(0);
        $country19->setWeight(0);

        $entityManager->persist($country19);

// Bangladesh
        $country20 = new Country();
        $country20->setId(20);
        $country20->setName("Bangladesh");
        $country20->setCode("BD");
        $country20->setCode3("BGD");
        $country20->setPhoneCode(880);
        $country20->setPostcodeRequired(0);
        $country20->setIsEu(0);
        $country20->setWeight(0);

        $entityManager->persist($country20);

// Barbados
        $country21 = new Country();
        $country21->setId(21);
        $country21->setName("Barbados");
        $country21->setCode("BB");
        $country21->setCode3("BRB");
        $country21->setPhoneCode(1246);
        $country21->setPostcodeRequired(0);
        $country21->setIsEu(0);
        $country21->setWeight(0);

        $entityManager->persist($country21);

// Belarus
        $country22 = new Country();
        $country22->setId(22);
        $country22->setName("Belarus");
        $country22->setCode("BY");
        $country22->setCode3("BLR");
        $country22->setPhoneCode(375);
        $country22->setPostcodeRequired(0);
        $country22->setIsEu(0);
        $country22->setWeight(0);

        $entityManager->persist($country22);

// Belgium
        $country23 = new Country();
        $country23->setId(23);
        $country23->setName("Belgium");
        $country23->setCode("BE");
        $country23->setCode3("BEL");
        $country23->setPhoneCode(32);
        $country23->setPostcodeRequired(0);
        $country23->setIsEu(0);
        $country23->setWeight(0);

        $entityManager->persist($country23);

// Belize
        $country24 = new Country();
        $country24->setId(24);
        $country24->setName("Belize");
        $country24->setCode("BZ");
        $country24->setCode3("BLZ");
        $country24->setPhoneCode(501);
        $country24->setPostcodeRequired(0);
        $country24->setIsEu(0);
        $country24->setWeight(0);

        $entityManager->persist($country24);

// Benin
        $country25 = new Country();
        $country25->setId(25);
        $country25->setName("Benin");
        $country25->setCode("BJ");
        $country25->setCode3("BEN");
        $country25->setPhoneCode(229);
        $country25->setPostcodeRequired(0);
        $country25->setIsEu(0);
        $country25->setWeight(0);

        $entityManager->persist($country25);

// Bermuda
        $country26 = new Country();
        $country26->setId(26);
        $country26->setName("Bermuda");
        $country26->setCode("BM");
        $country26->setCode3("BMU");
        $country26->setPhoneCode(1441);
        $country26->setPostcodeRequired(0);
        $country26->setIsEu(0);
        $country26->setWeight(0);

        $entityManager->persist($country26);

// Bhutan
        $country27 = new Country();
        $country27->setId(27);
        $country27->setName("Bhutan");
        $country27->setCode("BT");
        $country27->setCode3("BTN");
        $country27->setPhoneCode(975);
        $country27->setPostcodeRequired(0);
        $country27->setIsEu(0);
        $country27->setWeight(0);

        $entityManager->persist($country27);

// Bolivia
        $country28 = new Country();
        $country28->setId(28);
        $country28->setName("Bolivia");
        $country28->setCode("BO");
        $country28->setCode3("BOL");
        $country28->setPhoneCode(591);
        $country28->setPostcodeRequired(0);
        $country28->setIsEu(0);
        $country28->setWeight(0);

        $entityManager->persist($country28);

// Bosnia and Herzegovina
        $country29 = new Country();
        $country29->setId(29);
        $country29->setName("Bosnia and Herzegovina");
        $country29->setCode("BA");
        $country29->setCode3("BIH");
        $country29->setPhoneCode(387);
        $country29->setPostcodeRequired(0);
        $country29->setIsEu(0);
        $country29->setWeight(0);

        $entityManager->persist($country29);

// Botswana
        $country30 = new Country();
        $country30->setId(30);
        $country30->setName("Botswana");
        $country30->setCode("BW");
        $country30->setCode3("BWA");
        $country30->setPhoneCode(267);
        $country30->setPostcodeRequired(0);
        $country30->setIsEu(0);
        $country30->setWeight(0);

        $entityManager->persist($country30);

// Bouvet Island
        $country31 = new Country();
        $country31->setId(31);
        $country31->setName("Bouvet Island");
        $country31->setCode("BV");
        $country31->setCode3("");
        $country31->setPhoneCode(44);
        $country31->setPostcodeRequired(0);
        $country31->setIsEu(0);
        $country31->setWeight(0);

        $entityManager->persist($country31);

// Brazil
        $country32 = new Country();
        $country32->setId(32);
        $country32->setName("Brazil");
        $country32->setCode("BR");
        $country32->setCode3("BRA");
        $country32->setPhoneCode(55);
        $country32->setPostcodeRequired(0);
        $country32->setIsEu(0);
        $country32->setWeight(0);

        $entityManager->persist($country32);

// British lndian Ocean Territory
        $country33 = new Country();
        $country33->setId(33);
        $country33->setName("British lndian Ocean Territory");
        $country33->setCode("IO");
        $country33->setCode3("IOT");
        $country33->setPhoneCode(0);
        $country33->setPostcodeRequired(0);
        $country33->setIsEu(0);
        $country33->setWeight(0);

        $entityManager->persist($country33);

// Brunei Darussalam
        $country34 = new Country();
        $country34->setId(34);
        $country34->setName("Brunei Darussalam");
        $country34->setCode("BN");
        $country34->setCode3("BRN");
        $country34->setPhoneCode(673);
        $country34->setPostcodeRequired(0);
        $country34->setIsEu(0);
        $country34->setWeight(0);

        $entityManager->persist($country34);

// Bulgaria
        $country35 = new Country();
        $country35->setId(35);
        $country35->setName("Bulgaria");
        $country35->setCode("BG");
        $country35->setCode3("BGR");
        $country35->setPhoneCode(359);
        $country35->setPostcodeRequired(0);
        $country35->setIsEu(0);
        $country35->setWeight(0);

        $entityManager->persist($country35);

// Burkina Faso
        $country36 = new Country();
        $country36->setId(36);
        $country36->setName("Burkina Faso");
        $country36->setCode("BF");
        $country36->setCode3("BFA");
        $country36->setPhoneCode(226);
        $country36->setPostcodeRequired(0);
        $country36->setIsEu(0);
        $country36->setWeight(0);

        $entityManager->persist($country36);

// Burundi
        $country37 = new Country();
        $country37->setId(37);
        $country37->setName("Burundi");
        $country37->setCode("BI");
        $country37->setCode3("BDI");
        $country37->setPhoneCode(257);
        $country37->setPostcodeRequired(0);
        $country37->setIsEu(0);
        $country37->setWeight(0);

        $entityManager->persist($country37);

// Cambodia
        $country38 = new Country();
        $country38->setId(38);
        $country38->setName("Cambodia");
        $country38->setCode("KH");
        $country38->setCode3("KHM");
        $country38->setPhoneCode(855);
        $country38->setPostcodeRequired(0);
        $country38->setIsEu(0);
        $country38->setWeight(0);

        $entityManager->persist($country38);

// Cameroon
        $country39 = new Country();
        $country39->setId(39);
        $country39->setName("Cameroon");
        $country39->setCode("CM");
        $country39->setCode3("CMR");
        $country39->setPhoneCode(237);
        $country39->setPostcodeRequired(0);
        $country39->setIsEu(0);
        $country39->setWeight(0);

        $entityManager->persist($country39);

// Cape Verde
        $country40 = new Country();
        $country40->setId(40);
        $country40->setName("Cape Verde");
        $country40->setCode("CV");
        $country40->setCode3("CPV");
        $country40->setPhoneCode(238);
        $country40->setPostcodeRequired(0);
        $country40->setIsEu(0);
        $country40->setWeight(0);

        $entityManager->persist($country40);

// Cayman Islands
        $country41 = new Country();
        $country41->setId(41);
        $country41->setName("Cayman Islands");
        $country41->setCode("KY");
        $country41->setCode3("CYM");
        $country41->setPhoneCode(1345);
        $country41->setPostcodeRequired(0);
        $country41->setIsEu(0);
        $country41->setWeight(0);

        $entityManager->persist($country41);

// Central African Republic
        $country42 = new Country();
        $country42->setId(42);
        $country42->setName("Central African Republic");
        $country42->setCode("CF");
        $country42->setCode3("CAF");
        $country42->setPhoneCode(236);
        $country42->setPostcodeRequired(0);
        $country42->setIsEu(0);
        $country42->setWeight(0);

        $entityManager->persist($country42);

// Chad
        $country43 = new Country();
        $country43->setId(43);
        $country43->setName("Chad");
        $country43->setCode("TD");
        $country43->setCode3("TCD");
        $country43->setPhoneCode(235);
        $country43->setPostcodeRequired(0);
        $country43->setIsEu(0);
        $country43->setWeight(0);

        $entityManager->persist($country43);

// Chile
        $country44 = new Country();
        $country44->setId(44);
        $country44->setName("Chile");
        $country44->setCode("CL");
        $country44->setCode3("CHL");
        $country44->setPhoneCode(56);
        $country44->setPostcodeRequired(0);
        $country44->setIsEu(0);
        $country44->setWeight(0);

        $entityManager->persist($country44);

// China
        $country45 = new Country();
        $country45->setId(45);
        $country45->setName("China");
        $country45->setCode("CN");
        $country45->setCode3("CHN");
        $country45->setPhoneCode(86);
        $country45->setPostcodeRequired(0);
        $country45->setIsEu(0);
        $country45->setWeight(0);

        $entityManager->persist($country45);

// Christmas Island
        $country46 = new Country();
        $country46->setId(46);
        $country46->setName("Christmas Island");
        $country46->setCode("CX");
        $country46->setCode3("CXR");
        $country46->setPhoneCode(61);
        $country46->setPostcodeRequired(0);
        $country46->setIsEu(0);
        $country46->setWeight(0);

        $entityManager->persist($country46);

// Cocos (Keeling) Islands
        $country47 = new Country();
        $country47->setId(47);
        $country47->setName("Cocos (Keeling) Islands");
        $country47->setCode("CC");
        $country47->setCode3("CCK");
        $country47->setPhoneCode(61);
        $country47->setPostcodeRequired(0);
        $country47->setIsEu(0);
        $country47->setWeight(0);

        $entityManager->persist($country47);

// Colombia
        $country48 = new Country();
        $country48->setId(48);
        $country48->setName("Colombia");
        $country48->setCode("CO");
        $country48->setCode3("COL");
        $country48->setPhoneCode(57);
        $country48->setPostcodeRequired(0);
        $country48->setIsEu(0);
        $country48->setWeight(0);

        $entityManager->persist($country48);

// Comoros
        $country49 = new Country();
        $country49->setId(49);
        $country49->setName("Comoros");
        $country49->setCode("KM");
        $country49->setCode3("COM");
        $country49->setPhoneCode(269);
        $country49->setPostcodeRequired(0);
        $country49->setIsEu(0);
        $country49->setWeight(0);

        $entityManager->persist($country49);

// Congo
        $country50 = new Country();
        $country50->setId(50);
        $country50->setName("Congo");
        $country50->setCode("CG");
        $country50->setCode3("COG");
        $country50->setPhoneCode(242);
        $country50->setPostcodeRequired(0);
        $country50->setIsEu(0);
        $country50->setWeight(0);

        $entityManager->persist($country50);

// Cook Islands
        $country51 = new Country();
        $country51->setId(51);
        $country51->setName("Cook Islands");
        $country51->setCode("CK");
        $country51->setCode3("COK");
        $country51->setPhoneCode(682);
        $country51->setPostcodeRequired(0);
        $country51->setIsEu(0);
        $country51->setWeight(0);

        $entityManager->persist($country51);

// Costa Rica
        $country52 = new Country();
        $country52->setId(52);
        $country52->setName("Costa Rica");
        $country52->setCode("CR");
        $country52->setCode3("CRC");
        $country52->setPhoneCode(506);
        $country52->setPostcodeRequired(0);
        $country52->setIsEu(0);
        $country52->setWeight(0);

        $entityManager->persist($country52);

// Croatia (Hrvatska)
        $country53 = new Country();
        $country53->setId(53);
        $country53->setName("Croatia (Hrvatska)");
        $country53->setCode("HR");
        $country53->setCode3("HRV");
        $country53->setPhoneCode(385);
        $country53->setPostcodeRequired(0);
        $country53->setIsEu(0);
        $country53->setWeight(0);

        $entityManager->persist($country53);

// Cuba
        $country54 = new Country();
        $country54->setId(54);
        $country54->setName("Cuba");
        $country54->setCode("CU");
        $country54->setCode3("CUB");
        $country54->setPhoneCode(53);
        $country54->setPostcodeRequired(0);
        $country54->setIsEu(0);
        $country54->setWeight(0);

        $entityManager->persist($country54);

// Cyprus
        $country55 = new Country();
        $country55->setId(55);
        $country55->setName("Cyprus");
        $country55->setCode("CY");
        $country55->setCode3("CYP");
        $country55->setPhoneCode(357);
        $country55->setPostcodeRequired(0);
        $country55->setIsEu(0);
        $country55->setWeight(0);

        $entityManager->persist($country55);

// Czech Republic
        $country56 = new Country();
        $country56->setId(56);
        $country56->setName("Czech Republic");
        $country56->setCode("CZ");
        $country56->setCode3("CZE");
        $country56->setPhoneCode(420);
        $country56->setPostcodeRequired(0);
        $country56->setIsEu(0);
        $country56->setWeight(0);

        $entityManager->persist($country56);

// Denmark
        $country57 = new Country();
        $country57->setId(57);
        $country57->setName("Denmark");
        $country57->setCode("DK");
        $country57->setCode3("DNK");
        $country57->setPhoneCode(45);
        $country57->setPostcodeRequired(0);
        $country57->setIsEu(0);
        $country57->setWeight(0);

        $entityManager->persist($country57);

// Djibouti
        $country58 = new Country();
        $country58->setId(58);
        $country58->setName("Djibouti");
        $country58->setCode("DJ");
        $country58->setCode3("DJI");
        $country58->setPhoneCode(253);
        $country58->setPostcodeRequired(0);
        $country58->setIsEu(0);
        $country58->setWeight(0);

        $entityManager->persist($country58);

// Dominica
        $country59 = new Country();
        $country59->setId(59);
        $country59->setName("Dominica");
        $country59->setCode("DM");
        $country59->setCode3("DMA");
        $country59->setPhoneCode(1767);
        $country59->setPostcodeRequired(0);
        $country59->setIsEu(0);
        $country59->setWeight(0);

        $entityManager->persist($country59);

// Dominican Republic
        $country60 = new Country();
        $country60->setId(60);
        $country60->setName("Dominican Republic");
        $country60->setCode("DO");
        $country60->setCode3("DOM");
        $country60->setPhoneCode(1809);
        $country60->setPostcodeRequired(0);
        $country60->setIsEu(0);
        $country60->setWeight(0);

        $entityManager->persist($country60);

// East Timor
        $country61 = new Country();
        $country61->setId(61);
        $country61->setName("East Timor");
        $country61->setCode("TP");
        $country61->setCode3("");
        $country61->setPhoneCode(44);
        $country61->setPostcodeRequired(0);
        $country61->setIsEu(0);
        $country61->setWeight(0);

        $entityManager->persist($country61);

// Ecuador
        $country62 = new Country();
        $country62->setId(62);
        $country62->setName("Ecuador");
        $country62->setCode("EC");
        $country62->setCode3("ECU");
        $country62->setPhoneCode(593);
        $country62->setPostcodeRequired(0);
        $country62->setIsEu(0);
        $country62->setWeight(0);

        $entityManager->persist($country62);

// Egypt
        $country63 = new Country();
        $country63->setId(63);
        $country63->setName("Egypt");
        $country63->setCode("EG");
        $country63->setCode3("EGY");
        $country63->setPhoneCode(20);
        $country63->setPostcodeRequired(0);
        $country63->setIsEu(0);
        $country63->setWeight(0);

        $entityManager->persist($country63);

// El Salvador
        $country64 = new Country();
        $country64->setId(64);
        $country64->setName("El Salvador");
        $country64->setCode("SV");
        $country64->setCode3("SLV");
        $country64->setPhoneCode(503);
        $country64->setPostcodeRequired(0);
        $country64->setIsEu(0);
        $country64->setWeight(0);

        $entityManager->persist($country64);

// Equatorial Guinea
        $country65 = new Country();
        $country65->setId(65);
        $country65->setName("Equatorial Guinea");
        $country65->setCode("GQ");
        $country65->setCode3("GNQ");
        $country65->setPhoneCode(240);
        $country65->setPostcodeRequired(0);
        $country65->setIsEu(0);
        $country65->setWeight(0);

        $entityManager->persist($country65);

// Eritrea
        $country66 = new Country();
        $country66->setId(66);
        $country66->setName("Eritrea");
        $country66->setCode("ER");
        $country66->setCode3("ERI");
        $country66->setPhoneCode(291);
        $country66->setPostcodeRequired(0);
        $country66->setIsEu(0);
        $country66->setWeight(0);

        $entityManager->persist($country66);

// Estonia
        $country67 = new Country();
        $country67->setId(67);
        $country67->setName("Estonia");
        $country67->setCode("EE");
        $country67->setCode3("EST");
        $country67->setPhoneCode(372);
        $country67->setPostcodeRequired(0);
        $country67->setIsEu(0);
        $country67->setWeight(0);

        $entityManager->persist($country67);

// Ethiopia
        $country68 = new Country();
        $country68->setId(68);
        $country68->setName("Ethiopia");
        $country68->setCode("ET");
        $country68->setCode3("ETH");
        $country68->setPhoneCode(251);
        $country68->setPostcodeRequired(0);
        $country68->setIsEu(0);
        $country68->setWeight(0);

        $entityManager->persist($country68);

// Falkland Islands (Malvinas)
        $country69 = new Country();
        $country69->setId(69);
        $country69->setName("Falkland Islands (Malvinas)");
        $country69->setCode("FK");
        $country69->setCode3("FLK");
        $country69->setPhoneCode(500);
        $country69->setPostcodeRequired(0);
        $country69->setIsEu(0);
        $country69->setWeight(0);

        $entityManager->persist($country69);

// Faroe Islands
        $country70 = new Country();
        $country70->setId(70);
        $country70->setName("Faroe Islands");
        $country70->setCode("FO");
        $country70->setCode3("FRO");
        $country70->setPhoneCode(298);
        $country70->setPostcodeRequired(0);
        $country70->setIsEu(0);
        $country70->setWeight(0);

        $entityManager->persist($country70);

// Fiji
        $country71 = new Country();
        $country71->setId(71);
        $country71->setName("Fiji");
        $country71->setCode("FJ");
        $country71->setCode3("FJI");
        $country71->setPhoneCode(679);
        $country71->setPostcodeRequired(0);
        $country71->setIsEu(0);
        $country71->setWeight(0);

        $entityManager->persist($country71);

// Finland
        $country72 = new Country();
        $country72->setId(72);
        $country72->setName("Finland");
        $country72->setCode("FI");
        $country72->setCode3("FIN");
        $country72->setPhoneCode(358);
        $country72->setPostcodeRequired(0);
        $country72->setIsEu(0);
        $country72->setWeight(0);

        $entityManager->persist($country72);

// France
        $country73 = new Country();
        $country73->setId(73);
        $country73->setName("France");
        $country73->setCode("FR");
        $country73->setCode3("FRA");
        $country73->setPhoneCode(33);
        $country73->setPostcodeRequired(0);
        $country73->setIsEu(0);
        $country73->setWeight(0);

        $entityManager->persist($country73);

// France, Metropolitan
        $country74 = new Country();
        $country74->setId(74);
        $country74->setName("France, Metropolitan");
        $country74->setCode("FX");
        $country74->setCode3("");
        $country74->setPhoneCode(44);
        $country74->setPostcodeRequired(0);
        $country74->setIsEu(0);
        $country74->setWeight(0);

        $entityManager->persist($country74);

// French Guiana
        $country75 = new Country();
        $country75->setId(75);
        $country75->setName("French Guiana");
        $country75->setCode("GF");
        $country75->setCode3("");
        $country75->setPhoneCode(44);
        $country75->setPostcodeRequired(0);
        $country75->setIsEu(0);
        $country75->setWeight(0);

        $entityManager->persist($country75);

// French Polynesia
        $country76 = new Country();
        $country76->setId(76);
        $country76->setName("French Polynesia");
        $country76->setCode("PF");
        $country76->setCode3("PYF");
        $country76->setPhoneCode(689);
        $country76->setPostcodeRequired(0);
        $country76->setIsEu(0);
        $country76->setWeight(0);

        $entityManager->persist($country76);

// French Southern Territories
        $country77 = new Country();
        $country77->setId(77);
        $country77->setName("French Southern Territories");
        $country77->setCode("TF");
        $country77->setCode3("");
        $country77->setPhoneCode(44);
        $country77->setPostcodeRequired(0);
        $country77->setIsEu(0);
        $country77->setWeight(0);

        $entityManager->persist($country77);

// Gabon
        $country78 = new Country();
        $country78->setId(78);
        $country78->setName("Gabon");
        $country78->setCode("GA");
        $country78->setCode3("GAB");
        $country78->setPhoneCode(241);
        $country78->setPostcodeRequired(0);
        $country78->setIsEu(0);
        $country78->setWeight(0);

        $entityManager->persist($country78);

// Gambia
        $country79 = new Country();
        $country79->setId(79);
        $country79->setName("Gambia");
        $country79->setCode("GM");
        $country79->setCode3("GMB");
        $country79->setPhoneCode(220);
        $country79->setPostcodeRequired(0);
        $country79->setIsEu(0);
        $country79->setWeight(0);

        $entityManager->persist($country79);

// Georgia
        $country80 = new Country();
        $country80->setId(80);
        $country80->setName("Georgia");
        $country80->setCode("GE");
        $country80->setCode3("GEO");
        $country80->setPhoneCode(995);
        $country80->setPostcodeRequired(0);
        $country80->setIsEu(0);
        $country80->setWeight(0);

        $entityManager->persist($country80);

// Germany
        $country81 = new Country();
        $country81->setId(81);
        $country81->setName("Germany");
        $country81->setCode("DE");
        $country81->setCode3("DEU");
        $country81->setPhoneCode(49);
        $country81->setPostcodeRequired(0);
        $country81->setIsEu(0);
        $country81->setWeight(0);

        $entityManager->persist($country81);

// Ghana
        $country82 = new Country();
        $country82->setId(82);
        $country82->setName("Ghana");
        $country82->setCode("GH");
        $country82->setCode3("GHA");
        $country82->setPhoneCode(233);
        $country82->setPostcodeRequired(0);
        $country82->setIsEu(0);
        $country82->setWeight(0);

        $entityManager->persist($country82);

// Gibraltar
        $country83 = new Country();
        $country83->setId(83);
        $country83->setName("Gibraltar");
        $country83->setCode("GI");
        $country83->setCode3("GIB");
        $country83->setPhoneCode(350);
        $country83->setPostcodeRequired(0);
        $country83->setIsEu(0);
        $country83->setWeight(0);

        $entityManager->persist($country83);

// Greece
        $country84 = new Country();
        $country84->setId(84);
        $country84->setName("Greece");
        $country84->setCode("GR");
        $country84->setCode3("GRC");
        $country84->setPhoneCode(30);
        $country84->setPostcodeRequired(0);
        $country84->setIsEu(0);
        $country84->setWeight(0);

        $entityManager->persist($country84);

// Greenland
        $country85 = new Country();
        $country85->setId(85);
        $country85->setName("Greenland");
        $country85->setCode("GL");
        $country85->setCode3("GRL");
        $country85->setPhoneCode(299);
        $country85->setPostcodeRequired(0);
        $country85->setIsEu(0);
        $country85->setWeight(0);

        $entityManager->persist($country85);

// Grenada
        $country86 = new Country();
        $country86->setId(86);
        $country86->setName("Grenada");
        $country86->setCode("GD");
        $country86->setCode3("GRD");
        $country86->setPhoneCode(1473);
        $country86->setPostcodeRequired(0);
        $country86->setIsEu(0);
        $country86->setWeight(0);

        $entityManager->persist($country86);

// Guadeloupe
        $country87 = new Country();
        $country87->setId(87);
        $country87->setName("Guadeloupe");
        $country87->setCode("GP");
        $country87->setCode3("");
        $country87->setPhoneCode(44);
        $country87->setPostcodeRequired(0);
        $country87->setIsEu(0);
        $country87->setWeight(0);

        $entityManager->persist($country87);

// Guam
        $country88 = new Country();
        $country88->setId(88);
        $country88->setName("Guam");
        $country88->setCode("GU");
        $country88->setCode3("GUM");
        $country88->setPhoneCode(1671);
        $country88->setPostcodeRequired(0);
        $country88->setIsEu(0);
        $country88->setWeight(0);

        $entityManager->persist($country88);

// Guatemala
        $country89 = new Country();
        $country89->setId(89);
        $country89->setName("Guatemala");
        $country89->setCode("GT");
        $country89->setCode3("GTM");
        $country89->setPhoneCode(502);
        $country89->setPostcodeRequired(0);
        $country89->setIsEu(0);
        $country89->setWeight(0);

        $entityManager->persist($country89);

// Guinea
        $country90 = new Country();
        $country90->setId(90);
        $country90->setName("Guinea");
        $country90->setCode("GN");
        $country90->setCode3("GIN");
        $country90->setPhoneCode(224);
        $country90->setPostcodeRequired(0);
        $country90->setIsEu(0);
        $country90->setWeight(0);

        $entityManager->persist($country90);

// Guinea-Bissau
        $country91 = new Country();
        $country91->setId(91);
        $country91->setName("Guinea-Bissau");
        $country91->setCode("GW");
        $country91->setCode3("GNB");
        $country91->setPhoneCode(245);
        $country91->setPostcodeRequired(0);
        $country91->setIsEu(0);
        $country91->setWeight(0);

        $entityManager->persist($country91);

// Guyana
        $country92 = new Country();
        $country92->setId(92);
        $country92->setName("Guyana");
        $country92->setCode("GY");
        $country92->setCode3("GUY");
        $country92->setPhoneCode(592);
        $country92->setPostcodeRequired(0);
        $country92->setIsEu(0);
        $country92->setWeight(0);

        $entityManager->persist($country92);

// Haiti
        $country93 = new Country();
        $country93->setId(93);
        $country93->setName("Haiti");
        $country93->setCode("HT");
        $country93->setCode3("HTI");
        $country93->setPhoneCode(509);
        $country93->setPostcodeRequired(0);
        $country93->setIsEu(0);
        $country93->setWeight(0);

        $entityManager->persist($country93);

// Heard and Mc Donald Islands
        $country94 = new Country();
        $country94->setId(94);
        $country94->setName("Heard and Mc Donald Islands");
        $country94->setCode("HM");
        $country94->setCode3("");
        $country94->setPhoneCode(44);
        $country94->setPostcodeRequired(0);
        $country94->setIsEu(0);
        $country94->setWeight(0);

        $entityManager->persist($country94);

// Honduras
        $country95 = new Country();
        $country95->setId(95);
        $country95->setName("Honduras");
        $country95->setCode("HN");
        $country95->setCode3("HND");
        $country95->setPhoneCode(504);
        $country95->setPostcodeRequired(0);
        $country95->setIsEu(0);
        $country95->setWeight(0);

        $entityManager->persist($country95);

// Hong Kong
        $country96 = new Country();
        $country96->setId(96);
        $country96->setName("Hong Kong");
        $country96->setCode("HK");
        $country96->setCode3("HKG");
        $country96->setPhoneCode(852);
        $country96->setPostcodeRequired(0);
        $country96->setIsEu(0);
        $country96->setWeight(0);

        $entityManager->persist($country96);

// Hungary
        $country97 = new Country();
        $country97->setId(97);
        $country97->setName("Hungary");
        $country97->setCode("HU");
        $country97->setCode3("HUN");
        $country97->setPhoneCode(36);
        $country97->setPostcodeRequired(0);
        $country97->setIsEu(0);
        $country97->setWeight(0);

        $entityManager->persist($country97);

// Iceland
        $country98 = new Country();
        $country98->setId(98);
        $country98->setName("Iceland");
        $country98->setCode("IS");
        $country98->setCode3("IS");
        $country98->setPhoneCode(354);
        $country98->setPostcodeRequired(0);
        $country98->setIsEu(0);
        $country98->setWeight(0);

        $entityManager->persist($country98);

// India
        $country99 = new Country();
        $country99->setId(99);
        $country99->setName("India");
        $country99->setCode("IN");
        $country99->setCode3("IND");
        $country99->setPhoneCode(91);
        $country99->setPostcodeRequired(0);
        $country99->setIsEu(0);
        $country99->setWeight(0);

        $entityManager->persist($country99);

// Indonesia
        $country100 = new Country();
        $country100->setId(100);
        $country100->setName("Indonesia");
        $country100->setCode("ID");
        $country100->setCode3("IDN");
        $country100->setPhoneCode(62);
        $country100->setPostcodeRequired(0);
        $country100->setIsEu(0);
        $country100->setWeight(0);

        $entityManager->persist($country100);

// Iran (Islamic Republic of)
        $country101 = new Country();
        $country101->setId(101);
        $country101->setName("Iran (Islamic Republic of)");
        $country101->setCode("IR");
        $country101->setCode3("IRN");
        $country101->setPhoneCode(98);
        $country101->setPostcodeRequired(0);
        $country101->setIsEu(0);
        $country101->setWeight(0);

        $entityManager->persist($country101);

// Iraq
        $country102 = new Country();
        $country102->setId(102);
        $country102->setName("Iraq");
        $country102->setCode("IQ");
        $country102->setCode3("IRQ");
        $country102->setPhoneCode(964);
        $country102->setPostcodeRequired(0);
        $country102->setIsEu(0);
        $country102->setWeight(0);

        $entityManager->persist($country102);

// Ireland
        $country103 = new Country();
        $country103->setId(103);
        $country103->setName("Ireland");
        $country103->setCode("IE");
        $country103->setCode3("IRL");
        $country103->setPhoneCode(353);
        $country103->setPostcodeRequired(0);
        $country103->setIsEu(0);
        $country103->setWeight(0);

        $entityManager->persist($country103);

// Israel
        $country104 = new Country();
        $country104->setId(104);
        $country104->setName("Israel");
        $country104->setCode("IL");
        $country104->setCode3("ISR");
        $country104->setPhoneCode(972);
        $country104->setPostcodeRequired(0);
        $country104->setIsEu(0);
        $country104->setWeight(0);

        $entityManager->persist($country104);

// Italy
        $country105 = new Country();
        $country105->setId(105);
        $country105->setName("Italy");
        $country105->setCode("IT");
        $country105->setCode3("ITA");
        $country105->setPhoneCode(39);
        $country105->setPostcodeRequired(0);
        $country105->setIsEu(0);
        $country105->setWeight(0);

        $entityManager->persist($country105);

// Ivory Coast
        $country106 = new Country();
        $country106->setId(106);
        $country106->setName("Ivory Coast");
        $country106->setCode("CI");
        $country106->setCode3("CIV");
        $country106->setPhoneCode(225);
        $country106->setPostcodeRequired(0);
        $country106->setIsEu(0);
        $country106->setWeight(0);

        $entityManager->persist($country106);

// Jamaica
        $country107 = new Country();
        $country107->setId(107);
        $country107->setName("Jamaica");
        $country107->setCode("JM");
        $country107->setCode3("JAM");
        $country107->setPhoneCode(1876);
        $country107->setPostcodeRequired(0);
        $country107->setIsEu(0);
        $country107->setWeight(0);

        $entityManager->persist($country107);

// Japan
        $country108 = new Country();
        $country108->setId(108);
        $country108->setName("Japan");
        $country108->setCode("JP");
        $country108->setCode3("JPN");
        $country108->setPhoneCode(81);
        $country108->setPostcodeRequired(0);
        $country108->setIsEu(0);
        $country108->setWeight(0);

        $entityManager->persist($country108);

// Jordan
        $country109 = new Country();
        $country109->setId(109);
        $country109->setName("Jordan");
        $country109->setCode("JO");
        $country109->setCode3("JOR");
        $country109->setPhoneCode(962);
        $country109->setPostcodeRequired(0);
        $country109->setIsEu(0);
        $country109->setWeight(0);

        $entityManager->persist($country109);

// Kazakhstan
        $country110 = new Country();
        $country110->setId(110);
        $country110->setName("Kazakhstan");
        $country110->setCode("KZ");
        $country110->setCode3("KAZ");
        $country110->setPhoneCode(7);
        $country110->setPostcodeRequired(0);
        $country110->setIsEu(0);
        $country110->setWeight(0);

        $entityManager->persist($country110);

// Kenya
        $country111 = new Country();
        $country111->setId(111);
        $country111->setName("Kenya");
        $country111->setCode("KE");
        $country111->setCode3("KEN");
        $country111->setPhoneCode(254);
        $country111->setPostcodeRequired(0);
        $country111->setIsEu(0);
        $country111->setWeight(0);

        $entityManager->persist($country111);

// Kiribati
        $country112 = new Country();
        $country112->setId(112);
        $country112->setName("Kiribati");
        $country112->setCode("KI");
        $country112->setCode3("KIR");
        $country112->setPhoneCode(686);
        $country112->setPostcodeRequired(0);
        $country112->setIsEu(0);
        $country112->setWeight(0);

        $entityManager->persist($country112);

// Korea, Democratic People's Republic of
        $country113 = new Country();
        $country113->setId(113);
        $country113->setName("Korea, Democratic People's Republic of");
        $country113->setCode("KP");
        $country113->setCode3("PRK");
        $country113->setPhoneCode(850);
        $country113->setPostcodeRequired(0);
        $country113->setIsEu(0);
        $country113->setWeight(0);

        $entityManager->persist($country113);

// Korea, Republic of
        $country114 = new Country();
        $country114->setId(114);
        $country114->setName("Korea, Republic of");
        $country114->setCode("KR");
        $country114->setCode3("KOR");
        $country114->setPhoneCode(82);
        $country114->setPostcodeRequired(0);
        $country114->setIsEu(0);
        $country114->setWeight(0);

        $entityManager->persist($country114);

// Kuwait
        $country115 = new Country();
        $country115->setId(115);
        $country115->setName("Kuwait");
        $country115->setCode("KW");
        $country115->setCode3("KWT");
        $country115->setPhoneCode(965);
        $country115->setPostcodeRequired(0);
        $country115->setIsEu(0);
        $country115->setWeight(0);

        $entityManager->persist($country115);

// Kyrgyzstan
        $country116 = new Country();
        $country116->setId(116);
        $country116->setName("Kyrgyzstan");
        $country116->setCode("KG");
        $country116->setCode3("KGZ");
        $country116->setPhoneCode(996);
        $country116->setPostcodeRequired(0);
        $country116->setIsEu(0);
        $country116->setWeight(0);

        $entityManager->persist($country116);

// Lao People's Democratic Republic
        $country117 = new Country();
        $country117->setId(117);
        $country117->setName("Lao People's Democratic Republic");
        $country117->setCode("LA");
        $country117->setCode3("LAO");
        $country117->setPhoneCode(856);
        $country117->setPostcodeRequired(0);
        $country117->setIsEu(0);
        $country117->setWeight(0);

        $entityManager->persist($country117);

// Latvia
        $country118 = new Country();
        $country118->setId(118);
        $country118->setName("Latvia");
        $country118->setCode("LV");
        $country118->setCode3("LVA");
        $country118->setPhoneCode(371);
        $country118->setPostcodeRequired(0);
        $country118->setIsEu(0);
        $country118->setWeight(0);

        $entityManager->persist($country118);

// Lebanon
        $country119 = new Country();
        $country119->setId(119);
        $country119->setName("Lebanon");
        $country119->setCode("LB");
        $country119->setCode3("LBN");
        $country119->setPhoneCode(961);
        $country119->setPostcodeRequired(0);
        $country119->setIsEu(0);
        $country119->setWeight(0);

        $entityManager->persist($country119);

// Lesotho
        $country120 = new Country();
        $country120->setId(120);
        $country120->setName("Lesotho");
        $country120->setCode("LS");
        $country120->setCode3("LSO");
        $country120->setPhoneCode(266);
        $country120->setPostcodeRequired(0);
        $country120->setIsEu(0);
        $country120->setWeight(0);

        $entityManager->persist($country120);

// Liberia
        $country121 = new Country();
        $country121->setId(121);
        $country121->setName("Liberia");
        $country121->setCode("LR");
        $country121->setCode3("LBR");
        $country121->setPhoneCode(231);
        $country121->setPostcodeRequired(0);
        $country121->setIsEu(0);
        $country121->setWeight(0);

        $entityManager->persist($country121);

// Libyan Arab Jamahiriya
        $country122 = new Country();
        $country122->setId(122);
        $country122->setName("Libyan Arab Jamahiriya");
        $country122->setCode("LY");
        $country122->setCode3("LBY");
        $country122->setPhoneCode(218);
        $country122->setPostcodeRequired(0);
        $country122->setIsEu(0);
        $country122->setWeight(0);

        $entityManager->persist($country122);

// Liechtenstein
        $country123 = new Country();
        $country123->setId(123);
        $country123->setName("Liechtenstein");
        $country123->setCode("LI");
        $country123->setCode3("LIE");
        $country123->setPhoneCode(423);
        $country123->setPostcodeRequired(0);
        $country123->setIsEu(0);
        $country123->setWeight(0);

        $entityManager->persist($country123);

// Lithuania
        $country124 = new Country();
        $country124->setId(124);
        $country124->setName("Lithuania");
        $country124->setCode("LT");
        $country124->setCode3("LTU");
        $country124->setPhoneCode(370);
        $country124->setPostcodeRequired(0);
        $country124->setIsEu(0);
        $country124->setWeight(0);

        $entityManager->persist($country124);

// Luxembourg
        $country125 = new Country();
        $country125->setId(125);
        $country125->setName("Luxembourg");
        $country125->setCode("LU");
        $country125->setCode3("LUX");
        $country125->setPhoneCode(352);
        $country125->setPostcodeRequired(0);
        $country125->setIsEu(0);
        $country125->setWeight(0);

        $entityManager->persist($country125);

// Macau
        $country126 = new Country();
        $country126->setId(126);
        $country126->setName("Macau");
        $country126->setCode("MO");
        $country126->setCode3("MAC");
        $country126->setPhoneCode(853);
        $country126->setPostcodeRequired(0);
        $country126->setIsEu(0);
        $country126->setWeight(0);

        $entityManager->persist($country126);

// Macedonia
        $country127 = new Country();
        $country127->setId(127);
        $country127->setName("Macedonia");
        $country127->setCode("MK");
        $country127->setCode3("MKD");
        $country127->setPhoneCode(389);
        $country127->setPostcodeRequired(0);
        $country127->setIsEu(0);
        $country127->setWeight(0);

        $entityManager->persist($country127);

// Madagascar
        $country128 = new Country();
        $country128->setId(128);
        $country128->setName("Madagascar");
        $country128->setCode("MG");
        $country128->setCode3("MDG");
        $country128->setPhoneCode(261);
        $country128->setPostcodeRequired(0);
        $country128->setIsEu(0);
        $country128->setWeight(0);

        $entityManager->persist($country128);

// Malawi
        $country129 = new Country();
        $country129->setId(129);
        $country129->setName("Malawi");
        $country129->setCode("MW");
        $country129->setCode3("MWI");
        $country129->setPhoneCode(265);
        $country129->setPostcodeRequired(0);
        $country129->setIsEu(0);
        $country129->setWeight(0);

        $entityManager->persist($country129);

// Malaysia
        $country130 = new Country();
        $country130->setId(130);
        $country130->setName("Malaysia");
        $country130->setCode("MY");
        $country130->setCode3("MYS");
        $country130->setPhoneCode(60);
        $country130->setPostcodeRequired(0);
        $country130->setIsEu(0);
        $country130->setWeight(0);

        $entityManager->persist($country130);

// Maldives
        $country131 = new Country();
        $country131->setId(131);
        $country131->setName("Maldives");
        $country131->setCode("MV");
        $country131->setCode3("MDV");
        $country131->setPhoneCode(960);
        $country131->setPostcodeRequired(0);
        $country131->setIsEu(0);
        $country131->setWeight(0);

        $entityManager->persist($country131);

// Mali
        $country132 = new Country();
        $country132->setId(132);
        $country132->setName("Mali");
        $country132->setCode("ML");
        $country132->setCode3("MLI");
        $country132->setPhoneCode(223);
        $country132->setPostcodeRequired(0);
        $country132->setIsEu(0);
        $country132->setWeight(0);

        $entityManager->persist($country132);

// Malta
        $country133 = new Country();
        $country133->setId(133);
        $country133->setName("Malta");
        $country133->setCode("MT");
        $country133->setCode3("MLT");
        $country133->setPhoneCode(356);
        $country133->setPostcodeRequired(0);
        $country133->setIsEu(0);
        $country133->setWeight(0);

        $entityManager->persist($country133);

// Marshall Islands
        $country134 = new Country();
        $country134->setId(134);
        $country134->setName("Marshall Islands");
        $country134->setCode("MH");
        $country134->setCode3("MHL");
        $country134->setPhoneCode(692);
        $country134->setPostcodeRequired(0);
        $country134->setIsEu(0);
        $country134->setWeight(0);

        $entityManager->persist($country134);

// Martinique
        $country135 = new Country();
        $country135->setId(135);
        $country135->setName("Martinique");
        $country135->setCode("MQ");
        $country135->setCode3("");
        $country135->setPhoneCode(44);
        $country135->setPostcodeRequired(0);
        $country135->setIsEu(0);
        $country135->setWeight(0);

        $entityManager->persist($country135);

// Mauritania
        $country136 = new Country();
        $country136->setId(136);
        $country136->setName("Mauritania");
        $country136->setCode("MR");
        $country136->setCode3("MRT");
        $country136->setPhoneCode(222);
        $country136->setPostcodeRequired(0);
        $country136->setIsEu(0);
        $country136->setWeight(0);

        $entityManager->persist($country136);

// Mauritius
        $country137 = new Country();
        $country137->setId(137);
        $country137->setName("Mauritius");
        $country137->setCode("MU");
        $country137->setCode3("MUS");
        $country137->setPhoneCode(230);
        $country137->setPostcodeRequired(0);
        $country137->setIsEu(0);
        $country137->setWeight(0);

        $entityManager->persist($country137);

// Mayotte
        $country138 = new Country();
        $country138->setId(138);
        $country138->setName("Mayotte");
        $country138->setCode("TY");
        $country138->setCode3("MYT");
        $country138->setPhoneCode(262);
        $country138->setPostcodeRequired(0);
        $country138->setIsEu(0);
        $country138->setWeight(0);

        $entityManager->persist($country138);

// Mexico
        $country139 = new Country();
        $country139->setId(139);
        $country139->setName("Mexico");
        $country139->setCode("MX");
        $country139->setCode3("MEX");
        $country139->setPhoneCode(52);
        $country139->setPostcodeRequired(0);
        $country139->setIsEu(0);
        $country139->setWeight(0);

        $entityManager->persist($country139);

// Micronesia, Federated States of
        $country140 = new Country();
        $country140->setId(140);
        $country140->setName("Micronesia, Federated States of");
        $country140->setCode("FM");
        $country140->setCode3("FSM");
        $country140->setPhoneCode(691);
        $country140->setPostcodeRequired(0);
        $country140->setIsEu(0);
        $country140->setWeight(0);

        $entityManager->persist($country140);

// Moldova, Republic of
        $country141 = new Country();
        $country141->setId(141);
        $country141->setName("Moldova, Republic of");
        $country141->setCode("MD");
        $country141->setCode3("MDA");
        $country141->setPhoneCode(373);
        $country141->setPostcodeRequired(0);
        $country141->setIsEu(0);
        $country141->setWeight(0);

        $entityManager->persist($country141);

// Monaco
        $country142 = new Country();
        $country142->setId(142);
        $country142->setName("Monaco");
        $country142->setCode("MC");
        $country142->setCode3("MCO");
        $country142->setPhoneCode(377);
        $country142->setPostcodeRequired(0);
        $country142->setIsEu(0);
        $country142->setWeight(0);

        $entityManager->persist($country142);

// Mongolia
        $country143 = new Country();
        $country143->setId(143);
        $country143->setName("Mongolia");
        $country143->setCode("MN");
        $country143->setCode3("MNG");
        $country143->setPhoneCode(976);
        $country143->setPostcodeRequired(0);
        $country143->setIsEu(0);
        $country143->setWeight(0);

        $entityManager->persist($country143);

// Montserrat
        $country144 = new Country();
        $country144->setId(144);
        $country144->setName("Montserrat");
        $country144->setCode("MS");
        $country144->setCode3("MSR");
        $country144->setPhoneCode(1664);
        $country144->setPostcodeRequired(0);
        $country144->setIsEu(0);
        $country144->setWeight(0);

        $entityManager->persist($country144);

// Morocco
        $country145 = new Country();
        $country145->setId(145);
        $country145->setName("Morocco");
        $country145->setCode("MA");
        $country145->setCode3("MAR");
        $country145->setPhoneCode(212);
        $country145->setPostcodeRequired(0);
        $country145->setIsEu(0);
        $country145->setWeight(0);

        $entityManager->persist($country145);

// Mozambique
        $country146 = new Country();
        $country146->setId(146);
        $country146->setName("Mozambique");
        $country146->setCode("MZ");
        $country146->setCode3("MOZ");
        $country146->setPhoneCode(258);
        $country146->setPostcodeRequired(0);
        $country146->setIsEu(0);
        $country146->setWeight(0);

        $entityManager->persist($country146);

// Myanmar
        $country147 = new Country();
        $country147->setId(147);
        $country147->setName("Myanmar");
        $country147->setCode("MM");
        $country147->setCode3("MMR");
        $country147->setPhoneCode(95);
        $country147->setPostcodeRequired(0);
        $country147->setIsEu(0);
        $country147->setWeight(0);

        $entityManager->persist($country147);

// Namibia
        $country148 = new Country();
        $country148->setId(148);
        $country148->setName("Namibia");
        $country148->setCode("NA");
        $country148->setCode3("NAM");
        $country148->setPhoneCode(264);
        $country148->setPostcodeRequired(0);
        $country148->setIsEu(0);
        $country148->setWeight(0);

        $entityManager->persist($country148);

// Nauru
        $country149 = new Country();
        $country149->setId(149);
        $country149->setName("Nauru");
        $country149->setCode("NR");
        $country149->setCode3("NRU");
        $country149->setPhoneCode(674);
        $country149->setPostcodeRequired(0);
        $country149->setIsEu(0);
        $country149->setWeight(0);

        $entityManager->persist($country149);

// Nepal
        $country150 = new Country();
        $country150->setId(150);
        $country150->setName("Nepal");
        $country150->setCode("NP");
        $country150->setCode3("NPL");
        $country150->setPhoneCode(977);
        $country150->setPostcodeRequired(0);
        $country150->setIsEu(0);
        $country150->setWeight(0);

        $entityManager->persist($country150);

// Netherlands
        $country151 = new Country();
        $country151->setId(151);
        $country151->setName("Netherlands");
        $country151->setCode("NL");
        $country151->setCode3("NLD");
        $country151->setPhoneCode(31);
        $country151->setPostcodeRequired(0);
        $country151->setIsEu(0);
        $country151->setWeight(0);

        $entityManager->persist($country151);

// Netherlands Antilles
        $country152 = new Country();
        $country152->setId(152);
        $country152->setName("Netherlands Antilles");
        $country152->setCode("AN");
        $country152->setCode3("ANT");
        $country152->setPhoneCode(599);
        $country152->setPostcodeRequired(0);
        $country152->setIsEu(0);
        $country152->setWeight(0);

        $entityManager->persist($country152);

// New Caledonia
        $country153 = new Country();
        $country153->setId(153);
        $country153->setName("New Caledonia");
        $country153->setCode("NC");
        $country153->setCode3("NCL");
        $country153->setPhoneCode(687);
        $country153->setPostcodeRequired(0);
        $country153->setIsEu(0);
        $country153->setWeight(0);

        $entityManager->persist($country153);

// New Zealand
        $country154 = new Country();
        $country154->setId(154);
        $country154->setName("New Zealand");
        $country154->setCode("NZ");
        $country154->setCode3("NZL");
        $country154->setPhoneCode(64);
        $country154->setPostcodeRequired(0);
        $country154->setIsEu(0);
        $country154->setWeight(0);

        $entityManager->persist($country154);

// Nicaragua
        $country155 = new Country();
        $country155->setId(155);
        $country155->setName("Nicaragua");
        $country155->setCode("NI");
        $country155->setCode3("NIC");
        $country155->setPhoneCode(505);
        $country155->setPostcodeRequired(0);
        $country155->setIsEu(0);
        $country155->setWeight(0);

        $entityManager->persist($country155);

// Niger
        $country156 = new Country();
        $country156->setId(156);
        $country156->setName("Niger");
        $country156->setCode("NE");
        $country156->setCode3("NER");
        $country156->setPhoneCode(227);
        $country156->setPostcodeRequired(0);
        $country156->setIsEu(0);
        $country156->setWeight(0);

        $entityManager->persist($country156);

// Nigeria
        $country157 = new Country();
        $country157->setId(157);
        $country157->setName("Nigeria");
        $country157->setCode("NG");
        $country157->setCode3("NGA");
        $country157->setPhoneCode(234);
        $country157->setPostcodeRequired(0);
        $country157->setIsEu(0);
        $country157->setWeight(0);

        $entityManager->persist($country157);

// Niue
        $country158 = new Country();
        $country158->setId(158);
        $country158->setName("Niue");
        $country158->setCode("NU");
        $country158->setCode3("NIU");
        $country158->setPhoneCode(683);
        $country158->setPostcodeRequired(0);
        $country158->setIsEu(0);
        $country158->setWeight(0);

        $entityManager->persist($country158);

// Norfork Island
        $country159 = new Country();
        $country159->setId(159);
        $country159->setName("Norfork Island");
        $country159->setCode("NF");
        $country159->setCode3("");
        $country159->setPhoneCode(44);
        $country159->setPostcodeRequired(0);
        $country159->setIsEu(0);
        $country159->setWeight(0);

        $entityManager->persist($country159);

// Northern Mariana Islands
        $country160 = new Country();
        $country160->setId(160);
        $country160->setName("Northern Mariana Islands");
        $country160->setCode("MP");
        $country160->setCode3("MNP");
        $country160->setPhoneCode(1670);
        $country160->setPostcodeRequired(0);
        $country160->setIsEu(0);
        $country160->setWeight(0);

        $entityManager->persist($country160);

// Norway
        $country161 = new Country();
        $country161->setId(161);
        $country161->setName("Norway");
        $country161->setCode("NO");
        $country161->setCode3("NOR");
        $country161->setPhoneCode(47);
        $country161->setPostcodeRequired(0);
        $country161->setIsEu(0);
        $country161->setWeight(0);

        $entityManager->persist($country161);

// Oman
        $country162 = new Country();
        $country162->setId(162);
        $country162->setName("Oman");
        $country162->setCode("OM");
        $country162->setCode3("OMN");
        $country162->setPhoneCode(968);
        $country162->setPostcodeRequired(0);
        $country162->setIsEu(0);
        $country162->setWeight(0);

        $entityManager->persist($country162);

// Pakistan
        $country163 = new Country();
        $country163->setId(163);
        $country163->setName("Pakistan");
        $country163->setCode("PK");
        $country163->setCode3("PAK");
        $country163->setPhoneCode(92);
        $country163->setPostcodeRequired(0);
        $country163->setIsEu(0);
        $country163->setWeight(0);

        $entityManager->persist($country163);

// Palau
        $country164 = new Country();
        $country164->setId(164);
        $country164->setName("Palau");
        $country164->setCode("PW");
        $country164->setCode3("PLW");
        $country164->setPhoneCode(680);
        $country164->setPostcodeRequired(0);
        $country164->setIsEu(0);
        $country164->setWeight(0);

        $entityManager->persist($country164);

// Panama
        $country165 = new Country();
        $country165->setId(165);
        $country165->setName("Panama");
        $country165->setCode("PA");
        $country165->setCode3("PAN");
        $country165->setPhoneCode(507);
        $country165->setPostcodeRequired(0);
        $country165->setIsEu(0);
        $country165->setWeight(0);

        $entityManager->persist($country165);

// Papua New Guinea
        $country166 = new Country();
        $country166->setId(166);
        $country166->setName("Papua New Guinea");
        $country166->setCode("PG");
        $country166->setCode3("PNG");
        $country166->setPhoneCode(675);
        $country166->setPostcodeRequired(0);
        $country166->setIsEu(0);
        $country166->setWeight(0);

        $entityManager->persist($country166);

// Paraguay
        $country167 = new Country();
        $country167->setId(167);
        $country167->setName("Paraguay");
        $country167->setCode("PY");
        $country167->setCode3("PRY");
        $country167->setPhoneCode(595);
        $country167->setPostcodeRequired(0);
        $country167->setIsEu(0);
        $country167->setWeight(0);

        $entityManager->persist($country167);

// Peru
        $country168 = new Country();
        $country168->setId(168);
        $country168->setName("Peru");
        $country168->setCode("PE");
        $country168->setCode3("PER");
        $country168->setPhoneCode(51);
        $country168->setPostcodeRequired(0);
        $country168->setIsEu(0);
        $country168->setWeight(0);

        $entityManager->persist($country168);

// Philippines
        $country169 = new Country();
        $country169->setId(169);
        $country169->setName("Philippines");
        $country169->setCode("PH");
        $country169->setCode3("PHL");
        $country169->setPhoneCode(63);
        $country169->setPostcodeRequired(0);
        $country169->setIsEu(0);
        $country169->setWeight(0);

        $entityManager->persist($country169);

// Pitcairn
        $country170 = new Country();
        $country170->setId(170);
        $country170->setName("Pitcairn");
        $country170->setCode("PN");
        $country170->setCode3("PCN");
        $country170->setPhoneCode(870);
        $country170->setPostcodeRequired(0);
        $country170->setIsEu(0);
        $country170->setWeight(0);

        $entityManager->persist($country170);

// Poland
        $country171 = new Country();
        $country171->setId(171);
        $country171->setName("Poland");
        $country171->setCode("PL");
        $country171->setCode3("POL");
        $country171->setPhoneCode(48);
        $country171->setPostcodeRequired(0);
        $country171->setIsEu(0);
        $country171->setWeight(0);

        $entityManager->persist($country171);

// Portugal
        $country172 = new Country();
        $country172->setId(172);
        $country172->setName("Portugal");
        $country172->setCode("PT");
        $country172->setCode3("PRT");
        $country172->setPhoneCode(351);
        $country172->setPostcodeRequired(0);
        $country172->setIsEu(0);
        $country172->setWeight(0);

        $entityManager->persist($country172);

// Puerto Rico
        $country173 = new Country();
        $country173->setId(173);
        $country173->setName("Puerto Rico");
        $country173->setCode("PR");
        $country173->setCode3("PRI");
        $country173->setPhoneCode(1);
        $country173->setPostcodeRequired(0);
        $country173->setIsEu(0);
        $country173->setWeight(0);

        $entityManager->persist($country173);

// Qatar
        $country174 = new Country();
        $country174->setId(174);
        $country174->setName("Qatar");
        $country174->setCode("QA");
        $country174->setCode3("QAT");
        $country174->setPhoneCode(974);
        $country174->setPostcodeRequired(0);
        $country174->setIsEu(0);
        $country174->setWeight(0);

        $entityManager->persist($country174);

// Reunion
        $country175 = new Country();
        $country175->setId(175);
        $country175->setName("Reunion");
        $country175->setCode("RE");
        $country175->setCode3("");
        $country175->setPhoneCode(44);
        $country175->setPostcodeRequired(0);
        $country175->setIsEu(0);
        $country175->setWeight(0);

        $entityManager->persist($country175);

// Romania
        $country176 = new Country();
        $country176->setId(176);
        $country176->setName("Romania");
        $country176->setCode("RO");
        $country176->setCode3("ROU");
        $country176->setPhoneCode(40);
        $country176->setPostcodeRequired(0);
        $country176->setIsEu(0);
        $country176->setWeight(0);

        $entityManager->persist($country176);

// Russian Federation
        $country177 = new Country();
        $country177->setId(177);
        $country177->setName("Russian Federation");
        $country177->setCode("RU");
        $country177->setCode3("RUS");
        $country177->setPhoneCode(7);
        $country177->setPostcodeRequired(0);
        $country177->setIsEu(0);
        $country177->setWeight(0);

        $entityManager->persist($country177);

// Rwanda
        $country178 = new Country();
        $country178->setId(178);
        $country178->setName("Rwanda");
        $country178->setCode("RW");
        $country178->setCode3("RWA");
        $country178->setPhoneCode(250);
        $country178->setPostcodeRequired(0);
        $country178->setIsEu(0);
        $country178->setWeight(0);

        $entityManager->persist($country178);

// Saint Kitts and Nevis
        $country179 = new Country();
        $country179->setId(179);
        $country179->setName("Saint Kitts and Nevis");
        $country179->setCode("KN");
        $country179->setCode3("KNA");
        $country179->setPhoneCode(1869);
        $country179->setPostcodeRequired(0);
        $country179->setIsEu(0);
        $country179->setWeight(0);

        $entityManager->persist($country179);

// Saint Lucia
        $country180 = new Country();
        $country180->setId(180);
        $country180->setName("Saint Lucia");
        $country180->setCode("LC");
        $country180->setCode3("LCA");
        $country180->setPhoneCode(1758);
        $country180->setPostcodeRequired(0);
        $country180->setIsEu(0);
        $country180->setWeight(0);

        $entityManager->persist($country180);

// Saint Vincent and the Grenadines
        $country181 = new Country();
        $country181->setId(181);
        $country181->setName("Saint Vincent and the Grenadines");
        $country181->setCode("VC");
        $country181->setCode3("VCT");
        $country181->setPhoneCode(1784);
        $country181->setPostcodeRequired(0);
        $country181->setIsEu(0);
        $country181->setWeight(0);

        $entityManager->persist($country181);

// Samoa
        $country182 = new Country();
        $country182->setId(182);
        $country182->setName("Samoa");
        $country182->setCode("WS");
        $country182->setCode3("WSM");
        $country182->setPhoneCode(685);
        $country182->setPostcodeRequired(0);
        $country182->setIsEu(0);
        $country182->setWeight(0);

        $entityManager->persist($country182);

// San Marino
        $country183 = new Country();
        $country183->setId(183);
        $country183->setName("San Marino");
        $country183->setCode("SM");
        $country183->setCode3("SMR");
        $country183->setPhoneCode(378);
        $country183->setPostcodeRequired(0);
        $country183->setIsEu(0);
        $country183->setWeight(0);

        $entityManager->persist($country183);

// Sao Tome and Principe
        $country184 = new Country();
        $country184->setId(184);
        $country184->setName("Sao Tome and Principe");
        $country184->setCode("ST");
        $country184->setCode3("STP");
        $country184->setPhoneCode(239);
        $country184->setPostcodeRequired(0);
        $country184->setIsEu(0);
        $country184->setWeight(0);

        $entityManager->persist($country184);

// Saudi Arabia
        $country185 = new Country();
        $country185->setId(185);
        $country185->setName("Saudi Arabia");
        $country185->setCode("SA");
        $country185->setCode3("SAU");
        $country185->setPhoneCode(966);
        $country185->setPostcodeRequired(0);
        $country185->setIsEu(0);
        $country185->setWeight(0);

        $entityManager->persist($country185);

// Senegal
        $country186 = new Country();
        $country186->setId(186);
        $country186->setName("Senegal");
        $country186->setCode("SN");
        $country186->setCode3("SEN");
        $country186->setPhoneCode(221);
        $country186->setPostcodeRequired(0);
        $country186->setIsEu(0);
        $country186->setWeight(0);

        $entityManager->persist($country186);

// Seychelles
        $country187 = new Country();
        $country187->setId(187);
        $country187->setName("Seychelles");
        $country187->setCode("SC");
        $country187->setCode3("SYC");
        $country187->setPhoneCode(248);
        $country187->setPostcodeRequired(0);
        $country187->setIsEu(0);
        $country187->setWeight(0);

        $entityManager->persist($country187);

// Sierra Leone
        $country188 = new Country();
        $country188->setId(188);
        $country188->setName("Sierra Leone");
        $country188->setCode("SL");
        $country188->setCode3("SLE");
        $country188->setPhoneCode(232);
        $country188->setPostcodeRequired(0);
        $country188->setIsEu(0);
        $country188->setWeight(0);

        $entityManager->persist($country188);

// Singapore
        $country189 = new Country();
        $country189->setId(189);
        $country189->setName("Singapore");
        $country189->setCode("SG");
        $country189->setCode3("SGP");
        $country189->setPhoneCode(65);
        $country189->setPostcodeRequired(0);
        $country189->setIsEu(0);
        $country189->setWeight(0);

        $entityManager->persist($country189);

// Slovakia
        $country190 = new Country();
        $country190->setId(190);
        $country190->setName("Slovakia");
        $country190->setCode("SK");
        $country190->setCode3("SVK");
        $country190->setPhoneCode(421);
        $country190->setPostcodeRequired(0);
        $country190->setIsEu(0);
        $country190->setWeight(0);

        $entityManager->persist($country190);

// Slovenia
        $country191 = new Country();
        $country191->setId(191);
        $country191->setName("Slovenia");
        $country191->setCode("SI");
        $country191->setCode3("SVN");
        $country191->setPhoneCode(386);
        $country191->setPostcodeRequired(0);
        $country191->setIsEu(0);
        $country191->setWeight(0);

        $entityManager->persist($country191);

// Solomon Islands
        $country192 = new Country();
        $country192->setId(192);
        $country192->setName("Solomon Islands");
        $country192->setCode("SB");
        $country192->setCode3("SLB");
        $country192->setPhoneCode(677);
        $country192->setPostcodeRequired(0);
        $country192->setIsEu(0);
        $country192->setWeight(0);

        $entityManager->persist($country192);

// Somalia
        $country193 = new Country();
        $country193->setId(193);
        $country193->setName("Somalia");
        $country193->setCode("SO");
        $country193->setCode3("SOM");
        $country193->setPhoneCode(252);
        $country193->setPostcodeRequired(0);
        $country193->setIsEu(0);
        $country193->setWeight(0);

        $entityManager->persist($country193);

// South Africa
        $country194 = new Country();
        $country194->setId(194);
        $country194->setName("South Africa");
        $country194->setCode("ZA");
        $country194->setCode3("ZAF");
        $country194->setPhoneCode(27);
        $country194->setPostcodeRequired(0);
        $country194->setIsEu(0);
        $country194->setWeight(0);

        $entityManager->persist($country194);

// South Georgia South Sandwich Islands
        $country195 = new Country();
        $country195->setId(195);
        $country195->setName("South Georgia South Sandwich Islands");
        $country195->setCode("GS");
        $country195->setCode3("");
        $country195->setPhoneCode(44);
        $country195->setPostcodeRequired(0);
        $country195->setIsEu(0);
        $country195->setWeight(0);

        $entityManager->persist($country195);

// Spain
        $country196 = new Country();
        $country196->setId(196);
        $country196->setName("Spain");
        $country196->setCode("ES");
        $country196->setCode3("ESP");
        $country196->setPhoneCode(34);
        $country196->setPostcodeRequired(0);
        $country196->setIsEu(0);
        $country196->setWeight(0);

        $entityManager->persist($country196);

// Sri Lanka
        $country197 = new Country();
        $country197->setId(197);
        $country197->setName("Sri Lanka");
        $country197->setCode("LK");
        $country197->setCode3("LKA");
        $country197->setPhoneCode(94);
        $country197->setPostcodeRequired(0);
        $country197->setIsEu(0);
        $country197->setWeight(0);

        $entityManager->persist($country197);

// St. Helena
        $country198 = new Country();
        $country198->setId(198);
        $country198->setName("St. Helena");
        $country198->setCode("SH");
        $country198->setCode3("SHN");
        $country198->setPhoneCode(290);
        $country198->setPostcodeRequired(0);
        $country198->setIsEu(0);
        $country198->setWeight(0);

        $entityManager->persist($country198);

// St. Pierre and Miquelon
        $country199 = new Country();
        $country199->setId(199);
        $country199->setName("St. Pierre and Miquelon");
        $country199->setCode("PM");
        $country199->setCode3("SPM");
        $country199->setPhoneCode(508);
        $country199->setPostcodeRequired(0);
        $country199->setIsEu(0);
        $country199->setWeight(0);

        $entityManager->persist($country199);

// Sudan
        $country200 = new Country();
        $country200->setId(200);
        $country200->setName("Sudan");
        $country200->setCode("SD");
        $country200->setCode3("SDN");
        $country200->setPhoneCode(249);
        $country200->setPostcodeRequired(0);
        $country200->setIsEu(0);
        $country200->setWeight(0);

        $entityManager->persist($country200);

// Suriname
        $country201 = new Country();
        $country201->setId(201);
        $country201->setName("Suriname");
        $country201->setCode("SR");
        $country201->setCode3("SUR");
        $country201->setPhoneCode(597);
        $country201->setPostcodeRequired(0);
        $country201->setIsEu(0);
        $country201->setWeight(0);

        $entityManager->persist($country201);

// Svalbarn and Jan Mayen Islands
        $country202 = new Country();
        $country202->setId(202);
        $country202->setName("Svalbarn and Jan Mayen Islands");
        $country202->setCode("SJ");
        $country202->setCode3("SJM");
        $country202->setPhoneCode(0);
        $country202->setPostcodeRequired(0);
        $country202->setIsEu(0);
        $country202->setWeight(0);

        $entityManager->persist($country202);

// Swaziland
        $country203 = new Country();
        $country203->setId(203);
        $country203->setName("Swaziland");
        $country203->setCode("SZ");
        $country203->setCode3("SWZ");
        $country203->setPhoneCode(268);
        $country203->setPostcodeRequired(0);
        $country203->setIsEu(0);
        $country203->setWeight(0);

        $entityManager->persist($country203);

// Sweden
        $country204 = new Country();
        $country204->setId(204);
        $country204->setName("Sweden");
        $country204->setCode("SE");
        $country204->setCode3("SWE");
        $country204->setPhoneCode(46);
        $country204->setPostcodeRequired(0);
        $country204->setIsEu(0);
        $country204->setWeight(0);

        $entityManager->persist($country204);

// Switzerland
        $country205 = new Country();
        $country205->setId(205);
        $country205->setName("Switzerland");
        $country205->setCode("CH");
        $country205->setCode3("CHE");
        $country205->setPhoneCode(41);
        $country205->setPostcodeRequired(0);
        $country205->setIsEu(0);
        $country205->setWeight(0);

        $entityManager->persist($country205);

// Syrian Arab Republic
        $country206 = new Country();
        $country206->setId(206);
        $country206->setName("Syrian Arab Republic");
        $country206->setCode("SY");
        $country206->setCode3("SYR");
        $country206->setPhoneCode(963);
        $country206->setPostcodeRequired(0);
        $country206->setIsEu(0);
        $country206->setWeight(0);

        $entityManager->persist($country206);

// Taiwan
        $country207 = new Country();
        $country207->setId(207);
        $country207->setName("Taiwan");
        $country207->setCode("TW");
        $country207->setCode3("TWN");
        $country207->setPhoneCode(886);
        $country207->setPostcodeRequired(0);
        $country207->setIsEu(0);
        $country207->setWeight(0);

        $entityManager->persist($country207);

// Tajikistan
        $country208 = new Country();
        $country208->setId(208);
        $country208->setName("Tajikistan");
        $country208->setCode("TJ");
        $country208->setCode3("TJK");
        $country208->setPhoneCode(992);
        $country208->setPostcodeRequired(0);
        $country208->setIsEu(0);
        $country208->setWeight(0);

        $entityManager->persist($country208);

// Tanzania, United Republic of
        $country209 = new Country();
        $country209->setId(209);
        $country209->setName("Tanzania, United Republic of");
        $country209->setCode("TZ");
        $country209->setCode3("TZA");
        $country209->setPhoneCode(255);
        $country209->setPostcodeRequired(0);
        $country209->setIsEu(0);
        $country209->setWeight(0);

        $entityManager->persist($country209);

// Thailand
        $country210 = new Country();
        $country210->setId(210);
        $country210->setName("Thailand");
        $country210->setCode("TH");
        $country210->setCode3("THA");
        $country210->setPhoneCode(66);
        $country210->setPostcodeRequired(0);
        $country210->setIsEu(0);
        $country210->setWeight(0);

        $entityManager->persist($country210);

// Togo
        $country211 = new Country();
        $country211->setId(211);
        $country211->setName("Togo");
        $country211->setCode("TG");
        $country211->setCode3("TGO");
        $country211->setPhoneCode(228);
        $country211->setPostcodeRequired(0);
        $country211->setIsEu(0);
        $country211->setWeight(0);

        $entityManager->persist($country211);

// Tokelau
        $country212 = new Country();
        $country212->setId(212);
        $country212->setName("Tokelau");
        $country212->setCode("TK");
        $country212->setCode3("TKL");
        $country212->setPhoneCode(690);
        $country212->setPostcodeRequired(0);
        $country212->setIsEu(0);
        $country212->setWeight(0);

        $entityManager->persist($country212);

// Tonga
        $country213 = new Country();
        $country213->setId(213);
        $country213->setName("Tonga");
        $country213->setCode("TO");
        $country213->setCode3("TON");
        $country213->setPhoneCode(676);
        $country213->setPostcodeRequired(0);
        $country213->setIsEu(0);
        $country213->setWeight(0);

        $entityManager->persist($country213);

// Trinidad and Tobago
        $country214 = new Country();
        $country214->setId(214);
        $country214->setName("Trinidad and Tobago");
        $country214->setCode("TT");
        $country214->setCode3("TTO");
        $country214->setPhoneCode(1868);
        $country214->setPostcodeRequired(0);
        $country214->setIsEu(0);
        $country214->setWeight(0);

        $entityManager->persist($country214);

// Tunisia
        $country215 = new Country();
        $country215->setId(215);
        $country215->setName("Tunisia");
        $country215->setCode("TN");
        $country215->setCode3("TUN");
        $country215->setPhoneCode(216);
        $country215->setPostcodeRequired(0);
        $country215->setIsEu(0);
        $country215->setWeight(0);

        $entityManager->persist($country215);

// Turkey
        $country216 = new Country();
        $country216->setId(216);
        $country216->setName("Turkey");
        $country216->setCode("TR");
        $country216->setCode3("TUR");
        $country216->setPhoneCode(90);
        $country216->setPostcodeRequired(0);
        $country216->setIsEu(0);
        $country216->setWeight(0);

        $entityManager->persist($country216);

// Turkmenistan
        $country217 = new Country();
        $country217->setId(217);
        $country217->setName("Turkmenistan");
        $country217->setCode("TM");
        $country217->setCode3("TKM");
        $country217->setPhoneCode(993);
        $country217->setPostcodeRequired(0);
        $country217->setIsEu(0);
        $country217->setWeight(0);

        $entityManager->persist($country217);

// Turks and Caicos Islands
        $country218 = new Country();
        $country218->setId(218);
        $country218->setName("Turks and Caicos Islands");
        $country218->setCode("TC");
        $country218->setCode3("TCA");
        $country218->setPhoneCode(1649);
        $country218->setPostcodeRequired(0);
        $country218->setIsEu(0);
        $country218->setWeight(0);

        $entityManager->persist($country218);

// Tuvalu
        $country219 = new Country();
        $country219->setId(219);
        $country219->setName("Tuvalu");
        $country219->setCode("TV");
        $country219->setCode3("TUV");
        $country219->setPhoneCode(688);
        $country219->setPostcodeRequired(0);
        $country219->setIsEu(0);
        $country219->setWeight(0);

        $entityManager->persist($country219);

// Uganda
        $country220 = new Country();
        $country220->setId(220);
        $country220->setName("Uganda");
        $country220->setCode("UG");
        $country220->setCode3("UGA");
        $country220->setPhoneCode(256);
        $country220->setPostcodeRequired(0);
        $country220->setIsEu(0);
        $country220->setWeight(0);

        $entityManager->persist($country220);

// Ukraine
        $country221 = new Country();
        $country221->setId(221);
        $country221->setName("Ukraine");
        $country221->setCode("UA");
        $country221->setCode3("UKR");
        $country221->setPhoneCode(380);
        $country221->setPostcodeRequired(0);
        $country221->setIsEu(0);
        $country221->setWeight(0);

        $entityManager->persist($country221);

// United Arab Emirates
        $country222 = new Country();
        $country222->setId(222);
        $country222->setName("United Arab Emirates");
        $country222->setCode("AE");
        $country222->setCode3("ARE");
        $country222->setPhoneCode(971);
        $country222->setPostcodeRequired(0);
        $country222->setIsEu(0);
        $country222->setWeight(0);

        $entityManager->persist($country222);

// United Kingdom
        $country223 = new Country();
        $country223->setId(223);
        $country223->setName("United Kingdom");
        $country223->setCode("GB");
        $country223->setCode3("GBR");
        $country223->setPhoneCode(44);
        $country223->setPostcodeRequired(0);
        $country223->setIsEu(0);
        $country223->setWeight(0);

        $entityManager->persist($country223);

// United States minor outlying islands
        $country224 = new Country();
        $country224->setId(224);
        $country224->setName("United States minor outlying islands");
        $country224->setCode("UM");
        $country224->setCode3("");
        $country224->setPhoneCode(44);
        $country224->setPostcodeRequired(0);
        $country224->setIsEu(0);
        $country224->setWeight(0);

        $entityManager->persist($country224);

// Uruguay
        $country225 = new Country();
        $country225->setId(225);
        $country225->setName("Uruguay");
        $country225->setCode("UY");
        $country225->setCode3("URY");
        $country225->setPhoneCode(598);
        $country225->setPostcodeRequired(0);
        $country225->setIsEu(0);
        $country225->setWeight(0);

        $entityManager->persist($country225);

// Uzbekistan
        $country226 = new Country();
        $country226->setId(226);
        $country226->setName("Uzbekistan");
        $country226->setCode("UZ");
        $country226->setCode3("UZB");
        $country226->setPhoneCode(998);
        $country226->setPostcodeRequired(0);
        $country226->setIsEu(0);
        $country226->setWeight(0);

        $entityManager->persist($country226);

// Vanuatu
        $country227 = new Country();
        $country227->setId(227);
        $country227->setName("Vanuatu");
        $country227->setCode("VU");
        $country227->setCode3("VUT");
        $country227->setPhoneCode(678);
        $country227->setPostcodeRequired(0);
        $country227->setIsEu(0);
        $country227->setWeight(0);

        $entityManager->persist($country227);

// Vatican City State
        $country228 = new Country();
        $country228->setId(228);
        $country228->setName("Vatican City State");
        $country228->setCode("VA");
        $country228->setCode3("VAT");
        $country228->setPhoneCode(39);
        $country228->setPostcodeRequired(0);
        $country228->setIsEu(0);
        $country228->setWeight(0);

        $entityManager->persist($country228);

// Venezuela
        $country229 = new Country();
        $country229->setId(229);
        $country229->setName("Venezuela");
        $country229->setCode("VE");
        $country229->setCode3("VEN");
        $country229->setPhoneCode(58);
        $country229->setPostcodeRequired(0);
        $country229->setIsEu(0);
        $country229->setWeight(0);

        $entityManager->persist($country229);

// Vietnam
        $country230 = new Country();
        $country230->setId(230);
        $country230->setName("Vietnam");
        $country230->setCode("VN");
        $country230->setCode3("VNM");
        $country230->setPhoneCode(84);
        $country230->setPostcodeRequired(0);
        $country230->setIsEu(0);
        $country230->setWeight(0);

        $entityManager->persist($country230);

// Virigan Islands (British)
        $country231 = new Country();
        $country231->setId(231);
        $country231->setName("Virigan Islands (British)");
        $country231->setCode("VG");
        $country231->setCode3("VGB");
        $country231->setPhoneCode(1284);
        $country231->setPostcodeRequired(0);
        $country231->setIsEu(0);
        $country231->setWeight(0);

        $entityManager->persist($country231);

// Virgin Islands (U.S.)
        $country232 = new Country();
        $country232->setId(232);
        $country232->setName("Virgin Islands (U.S.)");
        $country232->setCode("VI");
        $country232->setCode3("VIR");
        $country232->setPhoneCode(1340);
        $country232->setPostcodeRequired(0);
        $country232->setIsEu(0);
        $country232->setWeight(0);

        $entityManager->persist($country232);

// Wallis and Futuna Islands
        $country233 = new Country();
        $country233->setId(233);
        $country233->setName("Wallis and Futuna Islands");
        $country233->setCode("WF");
        $country233->setCode3("WLF");
        $country233->setPhoneCode(681);
        $country233->setPostcodeRequired(0);
        $country233->setIsEu(0);
        $country233->setWeight(0);

        $entityManager->persist($country233);

// Western Sahara
        $country234 = new Country();
        $country234->setId(234);
        $country234->setName("Western Sahara");
        $country234->setCode("EH");
        $country234->setCode3("ESH");
        $country234->setPhoneCode(0);
        $country234->setPostcodeRequired(0);
        $country234->setIsEu(0);
        $country234->setWeight(0);

        $entityManager->persist($country234);

// Yemen
        $country235 = new Country();
        $country235->setId(235);
        $country235->setName("Yemen");
        $country235->setCode("YE");
        $country235->setCode3("YEM");
        $country235->setPhoneCode(967);
        $country235->setPostcodeRequired(0);
        $country235->setIsEu(0);
        $country235->setWeight(0);

        $entityManager->persist($country235);

// Yugoslavia
        $country236 = new Country();
        $country236->setId(236);
        $country236->setName("Yugoslavia");
        $country236->setCode("YU");
        $country236->setCode3("");
        $country236->setPhoneCode(44);
        $country236->setPostcodeRequired(0);
        $country236->setIsEu(0);
        $country236->setWeight(0);

        $entityManager->persist($country236);

// Zaire
        $country237 = new Country();
        $country237->setId(237);
        $country237->setName("Zaire");
        $country237->setCode("ZR");
        $country237->setCode3("");
        $country237->setPhoneCode(44);
        $country237->setPostcodeRequired(0);
        $country237->setIsEu(0);
        $country237->setWeight(0);

        $entityManager->persist($country237);

// Zambia
        $country238 = new Country();
        $country238->setId(238);
        $country238->setName("Zambia");
        $country238->setCode("ZM");
        $country238->setCode3("ZMB");
        $country238->setPhoneCode(260);
        $country238->setPostcodeRequired(0);
        $country238->setIsEu(0);
        $country238->setWeight(0);

        $entityManager->persist($country238);

// Zimbabwe
        $country239 = new Country();
        $country239->setId(239);
        $country239->setName("Zimbabwe");
        $country239->setCode("ZW");
        $country239->setCode3("ZWE");
        $country239->setPhoneCode(263);
        $country239->setPostcodeRequired(0);
        $country239->setIsEu(0);
        $country239->setWeight(0);

        $entityManager->persist($country239);

// Kosovo
        $country240 = new Country();
        $country240->setId(240);
        $country240->setName("Kosovo");
        $country240->setCode("XK");
        $country240->setCode3("");
        $country240->setPhoneCode(381);
        $country240->setPostcodeRequired(0);
        $country240->setIsEu(0);
        $country240->setWeight(0);

        $entityManager->persist($country240);

// Montenegro
        $country241 = new Country();
        $country241->setId(241);
        $country241->setName("Montenegro");
        $country241->setCode("ME");
        $country241->setCode3("MNE");
        $country241->setPhoneCode(382);
        $country241->setPostcodeRequired(0);
        $country241->setIsEu(0);
        $country241->setWeight(0);

        $entityManager->persist($country241);

// Serbia
        $country386 = new Country();
        $country386->setId(386);
        $country386->setName("Serbia");
        $country386->setCode("RS");
        $country386->setCode3("SRB");
        $country386->setPhoneCode(381);
        $country386->setPostcodeRequired(0);
        $country386->setIsEu(0);
        $country386->setWeight(0);

        $entityManager->persist($country386);



        $entityManager->flush();
    }

    public function getOrder()
    {
        return 40;
    }
}