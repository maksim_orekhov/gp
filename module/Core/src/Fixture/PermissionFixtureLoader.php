<?php

namespace ModuleRbac\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use Application\Entity\Permission;

class PermissionFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const PERMISSION_NAMES =[
        'profile.own.edit',
        'profile.any.view',
        'bank-client.view',     // for Operator
        'bank-client.execute',  // for Operator
        'deal.all.view',        // for Operator
        'deal.any',
        'deal.own.view',
        'deal.own.edit',
        'deal.own.customer.invoice',
        'deal.own.customer',
        'deal.own.contractor',
        'deal.own.contract',
        'settings.edit',
        'message.all.view',
        'message.own.edit',
        'message.own.delete',
        'dispute.own.toggle'
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        foreach (self::PERMISSION_NAMES as $permission_name) {
            $permission = new Permission();
            $permission->setName($permission_name);
            $entityManager->persist($permission);
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 40;
    }
}