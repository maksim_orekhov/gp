<?php
/**
 * Call Trace mode of application: on(true)/off(false).
 */
defined('CALL_TRACE__DUMP') or define('CALL_TRACE__DUMP', false);
defined('CALL_TRACE__LOG_MESSAGE') or define('CALL_TRACE__LOG_MESSAGE', true);

/**
 * @param null $message
 * @param null $file_name
 * @param null $priority
 */
function logMessage($message = null, $file_name = null, $priority = null)
{
    $trace = '';
    if (CALL_TRACE__LOG_MESSAGE) {
        $trace = "\n----------\n**** Trace information of the previous call ****\n" . generateCallTrace(1);
    }
    \Core\Service\LogMessage::setLogMessage($message, $file_name, $priority, $trace);
}

/**
 * @param Throwable $exception|Exception $exception
 * @param null $file_name
 * @param null $priority
 */
function logException($exception, $file_name = null, $priority = null)
{
    \Core\Service\LogMessage::setLogException($exception, $file_name, $priority);
}

/**
 * Dump with die
 *
 * @param $var
 * @param null $label
 */
function dump($var, $label = null)
{
    $dump = \Zend\Debug\Debug::dump($var, $label, false);

    echo $dump;
    if (CALL_TRACE__DUMP) {
        echo '<pre>'."\n----------\n**** Trace information of the previous call ****\n" .generateCallTrace(5).'</pre>';
    }
    die();
}

/**
 * Doctrine Dump with die
 *
 * @param $var
 * @param int $maxDepth
 * @param bool $stripTags
 */
function sql_dump($var, $maxDepth = 2, $stripTags = false)
{
    $dump = \Doctrine\Common\Util\Debug::dump($var, $maxDepth, $stripTags, false);

    echo $dump;
    if (CALL_TRACE__DUMP) {
        echo '<pre>'."\n----------\n**** Trace information of the previous call ****\n" .generateCallTrace(5).'</pre>';
    }
    die();
}

/**
 * start microtime
 */
function timer_start()
{
    Core\Service\Timer::start();
}

/**
 * stop microtime
 * @return float
 */
function timer_stop()
{
    return Core\Service\Timer::finish();
}

/**
 * @param $date_diff
 * @return float|int
 */
function hour_date_diff($date_diff)
{
    $total_days = $date_diff->days;
    $hours      = $date_diff->h;
    if ($total_days !== FALSE) {
        $hours += 24 * $total_days;
    }
    if ($date_diff->invert) {
        return -1 * $hours;
    }

    return $hours;
}

/**
 * @return null|string
 */
function get_version_git()
{
    #last commit
    #$cmd = git show -s --format="%H"
    // last commit in master
    $cmd = 'git show master -s --format="%H"';
    $version = null;
    $descriptor = array(
        1 => array('pipe', 'w'),
        2 => array('pipe', 'w'),
    );
    $process = proc_open($cmd, $descriptor, $pipes, null, null, array('suppress_errors' => true));
    if (\is_resource($process)) {
        $version = trim(stream_get_contents($pipes[1]));
        fclose($pipes[1]);
        fclose($pipes[2]);
        proc_close($process);
    }

    return $version;
}

/**
 * @param bool $maxRows
 * @return string
 */
function generateCallTrace($maxRows = false)
{
    $e = new Exception();
    $trace = explode("\n", $e->getTraceAsString());

    // reverse array to make steps line up chronologically
    // $trace = array_reverse($trace);
    array_shift($trace); // remove {main}
    array_pop($trace); // remove call to this method
    $length = count($trace);
    $result = array();

    for ($i = 0; $i < $length; $i++)
    {
        if ($maxRows && (int) $maxRows > 0 && (int) $maxRows === $i) {
            break;
        }
        $result[] = ($i + 1) . ')' . substr($trace[$i], strpos($trace[$i], ' ')); // replace '#someNum' with '$i)', set the right ordering
    }

    return implode("\n", $result);
}

/**
 * @param bool $maxRows
 * @param string $exception_trace
 * @param int|null $line
 * @return string
 */
function formExceptionTrace($maxRows = false, string $exception_trace, int $line = null)
{
    $trace = explode("\n", $exception_trace);

    array_pop($trace); // remove call to this method
    $length = count($trace);
    $result = array();

    for ($i = 0; $i < $length; $i++)
    {
        if ($maxRows && (int) $maxRows > 0 && (int) $maxRows === $i) {
            break;
        }
        $trace_line = ($i + 1) . ')' . substr($trace[$i], strpos($trace[$i], ' ')); // replace '#someNum' with '$i)', set the right ordering
        if (null !== $line && $i === 0) {
            $trace_line .= ' (строка <b>' . $line . '</b>)';
        }
        $result[] = $trace_line;

    }

    return implode("<br>", $result);
}
