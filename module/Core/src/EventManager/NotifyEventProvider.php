<?php
namespace Core\EventManager;

class NotifyEventProvider extends AbstractEventProvider
{
    const REGISTRATION_AFTER_CREATE_DEAL_FOR_OWNER = 'REGISTRATION_AFTER_CREATE_DEAL_FOR_OWNER';
    const REGISTRATION_AFTER_CREATE_DEAL_FOR_COUNTER_AGENT = 'REGISTRATION_AFTER_CREATE_DEAL_FOR_COUNTER_AGENT';
    const INVITATION_TO_DEAL_FOR_OWNER = 'INVITATION_TO_DEAL_FOR_OWNER';
    const INVITATION_TO_DEAL_FOR_COUNTER_AGENT = 'INVITATION_TO_DEAL_FOR_COUNTER_AGENT';

    const INVITATION_TO_DEAL_FOR_OWNER_BY_SMS = 'INVITATION_TO_DEAL_FOR_OWNER_BY_SMS';
    const INVITATION_TO_DEAL_FOR_COUNTER_AGENT_BY_SMS = 'INVITATION_TO_DEAL_FOR_COUNTER_AGENT_BY_SMS';

    const DEAL_CREATE = 'DEAL_CREATE';
}

