<?php
namespace Core\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class OperationConfirmForm
 * @package Core\Form
 */
class OperationConfirmForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('operation-confirm');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'operation-code-confirm-form');
        #$this->setAttribute('class', 'form-signin');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'code',
            'attributes' => [
                'placeholder'=> 'Код подтверждения',
                'class' => 'form-control',
                'id' => 'confirmation_code',
            ],
            'options' => [
                'label' => 'код подтверждения',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Confirm',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        // Add input for "code" field
        $inputFilter->add([
            'name'     => 'code',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                /*
                [
                    'name' => 'Regex',
                    'options' => [
                        'pattern' => '/[0-9a-zA-Z\s._-]+/',
                        'messages' => array(
                            \Zend\Validator\Regex::INVALID => "Invalid characters in login"
                        )
                    ],
                ],
                */
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 3,
                        'max' => 16
                    ],
                ],
            ],
        ]);
    }
}