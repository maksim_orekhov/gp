<?php
namespace Core\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class EmailEditForm
 * @package Core\Form
 */
class EmailEditForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('phone-edit');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'email-edit-form');
        $this->setAttribute('class', 'form-signin');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Current_phone
        $this->add([
            'type'  => 'hidden',
            'name' => 'current_email',
        ]);

        // New phone
        $this->add([
            'type' => 'text',
            'name' => 'new_email',
            'attributes' => [
                'id' => 'email',
                'class' => 'form-control',
                'placeholder' => 'Новый email',
            ],
            'options' => [
                'label' => 'Новый email',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Confirm',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'current_email',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name'    => 'EmailAddress',
                    'options' => [
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'new_email',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name'    => 'EmailAddress',
                    'options' => [
                    ],
                ],
            ],
        ]);
    }
}