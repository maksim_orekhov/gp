<?php
namespace Core\Form\Validator;

use Zend\Validator\AbstractValidator;
use \Core\Adapter\PhoneNumberUtilAdapter;

class PhoneValidator extends AbstractValidator
{
    const INVALID_PHONE = 'invalidPhone';

    protected $messageTemplates = [
        self::INVALID_PHONE  => 'Invalid phone number',
    ];

    /**
     * @var \Core\Adapter\PhoneNumberUtilAdapter
     */
    private $phoneNumberAdapter;

    public function __construct()
    {
        $this->phoneNumberAdapter = new PhoneNumberUtilAdapter();
        parent::__construct();
    }

    /**
     * @param mixed $phone_number
     * @return bool
     */
    public function isValid($phone_number)
    {
        try {
            $result =  $this->phoneNumberAdapter->isValidNumber($phone_number);
            if(!$result) {
                $this->error(self::INVALID_PHONE);
                return false;
            }

            return true;
        }
        catch (\Throwable $t) {
            $this->error(self::INVALID_PHONE);
            return false;
        }
    }
}