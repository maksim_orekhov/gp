<?php
namespace Core\Form\Captcha;

use Zend\Captcha\AbstractAdapter;
use Zend\Captcha\ReCaptcha;
use Traversable;
use ZendService\ReCaptcha\ReCaptcha as ReCaptchaService;

/**
 * ReCaptcha adapter
 *
 * Allows to insert captchas driven by ReCaptcha service
 *
 * @see http://recaptcha.net/apidocs/captcha/
 */
class ReCaptchaV2 extends ReCaptcha
{
    /**
     * @var bool skip checking the CAPTCHA
     */
    private $captchaBypassed = false;

    /**
     * @param bool $captchaBypassed
     */
    public function setCaptchaBypassed($captchaBypassed)
    {
        $this->captchaBypassed = $captchaBypassed;
    }

    /**
     * @return bool
     */
    public function isCaptchaBypassed()
    {
        return $this->captchaBypassed;
    }

    /**
     * Constructor
     *
     * @param  null|array|Traversable $options
     */
    public function __construct($options = null)
    {
        $this->setService(new ReCaptchaService());
        $this->serviceParams  = $this->getService()->getParams();
        $this->serviceOptions = $this->getService()->getOptions();

        parent::__construct($options);

        if (! empty($options)) {
            //skip checking the CAPTCHA setting
            if (array_key_exists('bypass', $options)) {
                $this->setCaptchaBypassed($options['bypass']);
            }

            $this->setOptions($options);
        }
    }

    /**
     * Validate captcha.
     *
     * The value should contain the name of the key within the context that
     * contains the ReCaptcha data. The default within the ReCaptcha service
     * for this is "g-recaptcha-response"
     *
     * @see    \Zend\Validator\ValidatorInterface::isValid()
     * @param  mixed $value
     * @param  mixed $context
     * @return bool
     */
    public function isValid($value, $context = null)
    {
        if ($this->isCaptchaBypassed()) {

            return true;
        }

        return parent::isValid($value, $context);
    }
}
