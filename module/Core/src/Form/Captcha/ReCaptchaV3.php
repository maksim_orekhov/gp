<?php
namespace Core\Form\Captcha;

use Zend\Captcha\AbstractAdapter;

class ReCaptchaV3 extends AbstractAdapter
{
    /**
     * Recaptcha service object
     *
     * @var ReCaptchaV3Service
     */
    protected $service;

    /**
     * Parameters defined by the service
     *
     * @var array
     */
    protected $serviceParams = [];

    /**
     * Options defined by the service
     *
     * @var array
     */
    protected $serviceOptions = [];

    /**#@+
     * Error codes
     */
    const MISSING_VALUE = 'missingValue';
    const ERR_CAPTCHA   = 'errCaptcha';
    const BAD_CAPTCHA   = 'badCaptcha';
    /**#@-*/

    /**
     * Error messages
     * @var array
     */
    protected $messageTemplates = [
        self::MISSING_VALUE => 'Missing captcha fields',
        self::ERR_CAPTCHA   => 'Failed to validate captcha',
        self::BAD_CAPTCHA   => 'Captcha value is wrong',
    ];

    /**
     * Retrieve ReCaptcha Secret key
     *
     * @return string
     */
    public function getSecretKey():string
    {
        return $this->getService()->getSecretKey();
    }

    /**
     * Retrieve ReCaptcha Site key
     *
     * @return string
     */
    public function getSiteKey():string
    {
        return $this->getService()->getSiteKey();
    }

    /**
     * Set ReCaptcha private key
     *
     * @param  string $secretKey
     * @return ReCaptchaV3
     */
    public function setSecretKey($secretKey):self
    {
        $this->getService()->setSecretKey($secretKey);
        return $this;
    }

    /**
     * @param $siteKey
     * @return ReCaptchaV3
     */
    public function setSiteKey($siteKey) :self
    {
        $this->getService()->setSiteKey($siteKey);
        return $this;
    }

    /**
     * @return string
     */
    public function getPrivKey() :string
    {
        return $this->getSecretKey();
    }

    /**
     * @return string
     */
    public function getPubKey() :string
    {
        return $this->getSiteKey();
    }

    /**
     * Set ReCaptcha secret key (BC version)
     *
     * @param  string $key
     * @return ReCaptchaV3
     * @deprecated
     */
    public function setPrivKey($key) :self
    {
        return $this->setSecretKey($key);
    }

    /**
     * Set ReCaptcha site key (BC version)
     *
     * @param  string $key
     * @return ReCaptchaV3
     * @deprecated
     */
    public function setPubKey($key) :self
    {
        return $this->setSiteKey($key);
    }

    /**
     * ReCaptchaV3 constructor.
     * @param null $options
     * @throws \Exception
     */
    public function __construct($options = null)
    {
        $this->setService(new ReCaptchaV3Service());
        $this->serviceParams  = $this->getService()->getParams();
        $this->serviceOptions = $this->getService()->getOptions();

        parent::__construct($options);

        if (! empty($options)) {
            if (array_key_exists('secret_key', $options)) {
                $this->getService()->setSecretKey($options['secret_key']);
            }
            if (array_key_exists('site_key', $options)) {
                $this->getService()->setSiteKey($options['site_key']);
            }

            // Support pubKey and pubKey for BC
            if (array_key_exists('privKey', $options)) {
                $this->getService()->setSecretKey($options['privKey']);
            }
            if (array_key_exists('pubKey', $options)) {
                $this->getService()->setSiteKey($options['pubKey']);
            }

            $this->setOptions($options);
        }
    }

    /**
     * Set service object
     *
     * @param  ReCaptchaV3Service $service
     * @return ReCaptchaV3
     */
    public function setService(ReCaptchaV3Service $service):self
    {
        $this->service = $service;
        return $this;
    }

    /**
     * Retrieve ReCaptcha service object
     *
     * @return ReCaptchaV3Service
     */
    public function getService() :ReCaptchaV3Service
    {
        return $this->service;
    }

    /**
     * @param string $key
     * @param string $value
     * @return $this|AbstractAdapter
     */
    public function setOption($key, $value)
    {
        $service = $this->getService();
        if (array_key_exists($key, $this->serviceParams)) {
            $service->setParam($key, $value);
            return $this;
        }
        if (array_key_exists($key, $this->serviceOptions)) {
            $service->setOption($key, $value);
            return $this;
        }
        return parent::setOption($key, $value);
    }

    /**
     * @return string
     */
    public function generate() :string
    {
        return '';
    }

    /**
     * @param mixed $value
     * @param null $context
     * @return bool
     * @throws \Exception
     */
    public function isValid($value, $context = null) :bool
    {

        if (empty($value) && ! \is_array($context)) {
            $this->error(self::MISSING_VALUE);
            return false;
        }

        $service = $this->getService();
        if ((\is_string($value) || \is_int($value)) && array_key_exists($value, $context)) {
            $res = $service->verify($context[$value]);
        } else {
            $res = $service->verify($value);
        }

        if (! $res) {
            $this->error(self::ERR_CAPTCHA);
            return false;
        }

        if (! $res->isValid()) {
            $this->error(self::BAD_CAPTCHA);
            return false;
        }

        return true;
    }

    /**
     * Get helper name used to render captcha
     *
     * @return string
     */
    public function getHelperName() :string
    {
        return 'recaptchaV3Helper';
    }
}
