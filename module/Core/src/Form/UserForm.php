<?php
namespace Core\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class UserForm
 * @package Core\Form
 */
class UserForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('user');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'ava-change-form js-input-file__container second-left-menu__ava-form');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type' => 'file',
            'name' => 'avatar',
            'options' => [
                'label' => 'File',
                'class'=>'js-input-file',
                'accept'=>'image/*',
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
            'attributes' => [
                'id' => 'csrf',
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Сохранить',
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'avatar',
            'required' => false,
            'filters' => [],
            'validators' => [
                [
                    'name' => \Zend\Validator\File\IsImage::class,
                    'options' => [],
                ],
                [
                    'name' => \Zend\Validator\File\Size::class,
                    'options' => [
                        'max' => '10MB'
                    ],
                ]
            ],
        ]);
    }
}