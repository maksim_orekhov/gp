<?php
namespace Core\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class PhoneEditForm
 * @package Core\Form
 */
class PhoneEditForm extends Form
{
    public function __construct()
    {
        // Define form name
        parent::__construct('phone-edit');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'phone-edit-form');
        $this->setAttribute('class', 'form-signin');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Current_phone
        $this->add([
            'type'  => 'hidden',
            'name' => 'current_phone',
        ]);
        /*
        $this->add([
            'type' => 'text',
            'name' => 'current_phone',
            'attributes' => [
                'id' => 'phone',
                'class' => 'form-control',
                'placeholder' => 'Current phone number',
            ],
            'options' => [
                'label' => 'Current phone number',
            ],
        ]);
        */
        // New phone
        $this->add([
            'type' => 'text',
            'name' => 'new_phone',
            'attributes' => [
                'id' => 'phone',
                'class' => 'form-control',
                'placeholder' => 'Новый номер',
            ],
            'options' => [
                'label' => 'Новый номер',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Confirm',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter()
    {
        // Create main input filter
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'current_phone',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name'    => \Core\Form\Validator\PhoneValidator::class,
                    'options' => [
                    ],
                ],
            ],
        ]);
        $inputFilter->add([
            'name'     => 'new_phone',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                [
                    'name' =>  'PregReplace',
                    'options' => [
                        'pattern' => '/(\()|(\))|(\-)/',
                        'replacement' => ''
                    ]
                ],
            ],
            'validators' => [
                [
                    'name'    => 'Regex',
                    'options' => [
                        'pattern' => '/\d{11}/',
                        'message' => 'Номер телефона заполнен не полностью'
                    ]
                ],
            ],
        ]);
    }
}