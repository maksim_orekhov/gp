<?php
namespace Core\View\Helper;

use Core\Entity\Interfaces\UserInterface;
use Core\Service\Base\BaseUserManager;
use Core\Service\Base\BaseAuthManager;

class UserHelper
{
    /**
     * @var BaseUserManager
     */
    private $baseUserManager;

    /**
     * @var array
     */
    private $config;

    /**
     * @var UserInterface
     */
    private $user = null;

    /**
     * UserHelper constructor.
     * @param BaseUserManager $baseUserManager
     * @param BaseAuthManager $baseAuthManager
     * @param $config
     * @throws \Exception
     */
    public function __construct(BaseUserManager $baseUserManager,
                                BaseAuthManager $baseAuthManager,
                                $config)
    {
        $this->baseUserManager = $baseUserManager;
        $this->config = $config;

        if ($this->user === null || ($this->user !== null
                && strtolower($this->user->getLogin()) !== strtolower($baseAuthManager->getIdentity()))){
            $this->setCurrentUser($baseAuthManager->getIdentity());
        }
    }

    public function __invoke()
    {
        return $this;
    }

    /**
     * @param $login
     * @return bool
     * @throws \Exception
     */
    public function setCurrentUser($login){
        try{
            $user = $this->baseUserManager->getUserByLogin($login);
            $this->user = $user;
        }catch(\Throwable $t){
            $this->user = null;
        }
        return true;
    }

    /**
     * @return UserInterface|null
     */
    public function getCurrentUser(){
        return $this->user;
    }

    /**
     * @return string|null
     */
    public function getLogin(){
        return $this->getCurrentUser() ? $this->getCurrentUser()->getLogin() : null;
    }

    /**
     * //TODO надобы дописать
     * @return null
     */
    public function getAvatar(){
        return null;
    }

    /**
     * Get user Phone indicator
     *
     * @return null|string
     */
    public function unconfirmedPhoneIndicator()
    {
        if($this->isUnconfirmedPhone()) {

            $code_expiration_time_in_hours = $this->config['tokens']['confirmation_code_expiration_time']/3600;

            $phoneIndicator = '<div class="alert alert-warning">
                                <span class="glyphicon glyphicon-phone"></span>
                                На ваш номер телефона (' . $this->user->getPhone()->getPhoneNumber(). ') отправлен код подтверждения.
                                Код действителен в течение ' . $code_expiration_time_in_hours . ' часов.
                                <a href="/phone/confirm">Подтвердить</a>.
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                </div>';

            return $phoneIndicator;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isUnconfirmedPhone()
    {
        if ($this->user && !$this->user->getPhone()) {

            return true;
        }
        if ($this->user && $this->user->getPhone() && !$this->user->getPhone()->getIsVerified()) {

            return true;
        }

        return false;
    }

    /**
     * Get user Email indicator
     *
     * @return null|string
     */
    public function unconfirmedEmailIndicator()
    {
        if($this->isUnconfirmedEmail()) {

            $token_expiration_time_in_hours = $this->config['tokens']['email_confirmation_token_expiration_time']/3600;

            $emailIndicator = '<div class="alert alert-warning">
                                <span class="glyphicon glyphicon-envelope"></span>
                                На ваш email (' . $this->user->getEmail()->getEmail() . ') отправлена ссылка для подтверждения.
                                Ссылка действительна в течение ' . $token_expiration_time_in_hours . ' часов. 
                                <a href="/email/confirm">Подтвердить</a>.
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                </div>';

            return $emailIndicator;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isUnconfirmedEmail()
    {
        return $this->user && !$this->user->getEmail()->getIsVerified();
    }

    /**
     * @return bool
     */
    public function isPhoneExists()
    {
        return $this->user && $this->user->getPhone();
    }
}