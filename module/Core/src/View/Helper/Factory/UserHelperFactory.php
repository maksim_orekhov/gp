<?php
namespace Core\View\Helper\Factory;

use Core\Service\Base\BaseUserManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\View\Helper\UserHelper;
use Core\Service\Base\BaseAuthManager;

/**
 * Class UserHelperFactory
 * @package Core\View\Helper\Factory
 */
class UserHelperFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserHelper|object
     * @throws \Exception
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $baseUserManager = $container->get(BaseUserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $config = $container->get('Config');

        return new UserHelper(
            $baseUserManager,
            $baseAuthManager,
            $config
        );
    }
}