<?php
namespace Core\View\Helper\Captcha;

use Core\Form\Captcha\ReCaptchaV3;
use Zend\Form\Element\Captcha;
use Zend\Form\ElementInterface;
use Zend\Form\Exception;
use Zend\Form\View\Helper\FormInput;

class ReCaptchaV3Helper extends FormInput
{
    /**
     * @param ElementInterface|null $element
     * @return $this|string|FormInput
     */
    public function __invoke(ElementInterface $element = null)
    {
        if (! $element) {
            return $this;
        }

        return $this->render($element);
    }

    /**
     * @param ElementInterface $element
     * @return string
     */
    public function render(ElementInterface $element) :string
    {
        /**
         * @var Captcha $element
         * @var ReCaptchaV3 $captcha
         */
        $captcha = $element->getCaptcha();
        $name = $element->getName();
        if ($captcha === null || ! $captcha instanceof ReCaptchaV3) {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has a "captcha" attribute implementing Core\Form\Captcha\ReCaptchaV3; '
                . 'none found',
                __METHOD__
            ));
        }

        $markup = '
        <script 
            type="text/javascript"
            src="https://www.google.com/recaptcha/api.js?render='.$captcha->getService()->getSiteKey().'"></script>
        <script type="text/javascript">
            grecaptcha.ready(function() {
                grecaptcha.execute("' . $captcha->getService()->getSiteKey() . '").then(function(token) {
                    if (document.getElementById("'.$name.'")) {  
                        document.getElementById("' . $name . '").value = token;
                    }
                });
            });
        </script>
        ';

        $hidden = $this->renderHiddenInput($name);

        return $hidden . $markup;
    }

    protected function renderHiddenInput($name)
    {
        /**
         * @var Captcha $element
         * @var ReCaptchaV3 $captcha
         */
        if ($name === 'g-recaptcha-response') {
            return '';
        }

        $pattern        = '<input type="hidden" %s%s';
        $closingBracket = $this->getInlineClosingBracket();

        $attributes = $this->createAttributesString([
            'name'  => $name,
            'id'  => $name,
            'value' => 'g-recaptcha-response',
        ]);

        return sprintf($pattern, $attributes, $closingBracket);
    }

    /**
     * No longer used with v2 of Recaptcha API
     *
     * @deprecated
     *
     * @param  string $challengeId
     * @param  string $responseId
     * @return string
     */
    protected function renderJsEvents($challengeId, $responseId)
    {
        return '';
    }
}
