<?php
namespace Core\Provider\Mail;

use Core\Service\TwigRenderer;
use Core\Provider\Mail\BaseMailSender;

/**
 * Class PasswordResettingTokenSender
 * @package Code\Provider\Mail
 */
class PasswordResettingTokenSender extends BaseMailSender
{
    const ERROR_TOKEN_IS_ABSENT = 'Trying send password-recovery email without token data provided';

    private $twigRenderer;

    /**
     * PasswordResettingTokenSender constructor.
     * @param TwigRenderer $twigRenderer
     * @param array $config
     */
    public function __construct(
        TwigRenderer $twigRenderer,
        array $config)
    {
        parent::__construct($config);

        $this->twigRenderer = $twigRenderer;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        //checking data
        $this->checkValidityData($data);

        $token = $data['encoded_token'];
        $subject = self::SUBJECT_TITLE.'Сброс пароля';
        // Get main_project_url from config
        $main_project_url = $this->config['main_project_url'];
        //@TODO Как тут получить путь для ссылок через имя роута?
        $password_reset_url = $main_project_url . 'forgot/password/set?token=' . $token;

        // Html body
        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'user_login' => $data['user_login'],
            'subject' => $subject,
            'main_project_url' => $main_project_url,
            'password_reset_url' => $password_reset_url
        ]);
        $twigRenderer->setTemplate('twig/password_reset');
        $html = $twigRenderer->getHtml();

        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function checkValidityData($data)
    {
        if (!array_key_exists('encoded_token', $data)) {

            throw new \Exception(self::ERROR_TOKEN_IS_ABSENT);
        }

        return true;
    }
}