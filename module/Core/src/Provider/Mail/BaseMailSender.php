<?php
namespace Core\Provider\Mail;

use Zend\Mail\Transport\SmtpOptions;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Mime;
use Zend\Mail\Transport\Smtp as SmtpTransport;

/**
 * Class BaseMailSender
 * @package Core\Provider\Mail
 */
abstract class BaseMailSender implements MailSenderInterface
{
    const SUBJECT_TITLE = '💰 GP: ';
    /**
     * @var array
     */
    protected $config;

    /**
     * BaseMailSender constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param $email
     * @param $subject
     * @param null $htmlBody
     * @param null $textBody
     * @param array|null $file
     * @return bool
     * @throws \Exception
     */
    protected function send($email, $subject, $htmlBody=null, $textBody=null, array $file=null)
    {
        $body = null;

        if($htmlBody) {
            // Create HTML message
            $html           = new MimePart($htmlBody);
            $html->type     = 'text/html';
            $html->charset  = 'UTF-8';
            $body           = new MimeMessage();
            $body->addPart($html);

            if($file) {
                $attachment = new MimePart($file['file_content']);
                $attachment->type = $file['file_type'];
                $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
                $attachment->encoding = Mime::ENCODING_BASE64;
                $attachment->filename = $file['file_name'];
                $body->addPart($attachment);
            }

        } elseif ($textBody) {
            // Create Text message
            $body = $textBody;
        }

        if($body) {
            // Create mail
            $message = new Message();
            $message->addTo($email);
            $message->addFrom($this->config['mail_options']['from_email'], $this->config['mail_options']['from_name']);
            $message->setSubject($subject);
            $message->setBody($body);
            $message->setEncoding('UTF-8');

            // Setup transport using LOGIN authentication
            $transport = new SmtpTransport();
            $options   = new SmtpOptions([
                'name'              => $this->config['mail_options']['name'],
                'host'              => $this->config['mail_options']['host'],
                'port'              => $this->config['mail_options']['port'],
                'connection_class'  => $this->config['mail_options']['connection_class'],
                'connection_config' => [
                    'username' => $this->config['mail_options']['connection_config']['username'],
                    'password' => $this->config['mail_options']['connection_config']['password'],
                ],
            ]);

            // Send mail
            $transport->setOptions($options);
            if( !$this->config['email_providers']['message_send_simulation'] ) {
                // If message_send_simulation == false try send mail
                try {
                    $transport->send($message);
                } catch (\Throwable $exception) {
                    logException($exception,'errors',\Zend\Log\Logger::ERR);
                    return false;
                }
            }
        }

        return true;
    }
}