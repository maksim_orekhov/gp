<?php
namespace Core\Provider\Mail;

use Core\Provider\QrCodeProvider;
use Core\Service\TwigRenderer;
use Core\Provider\Mail\BaseMailSender;

/**
 * Class EmailConfirmationTokensSender
 * @package Core\Provider\Mail
 */
class EmailConfirmationTokensSender extends BaseMailSender
{
    const ERROR_TOKEN_IS_ABSENT = 'Trying send email without token data provided';

    private $twigRenderer;
    /**
     * @var QrCodeProvider
     */
    private $qrCodeProvider;

    /**
     * EmailConfirmationTokensSender constructor.
     * @param TwigRenderer $twigRenderer
     * @param array $config
     * @param QrCodeProvider $qrCodeProvider
     */
    public function __construct(
                    TwigRenderer $twigRenderer,
                    array $config,
                    QrCodeProvider $qrCodeProvider)
    {
        parent::__construct($config);

        $this->twigRenderer = $twigRenderer;
        $this->qrCodeProvider = $qrCodeProvider;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function sendMail($email_value, $data)
    {
        //checking data
        $this->checkValidityData($data);

        $token = $data['encoded_token'];
        $subject = self::SUBJECT_TITLE.'Подтверждение почты';
        $main_project_url = $this->config['main_project_url'];
        // Email confirmation Url //@TODO Как тут получить пути для ссылок через имена роутов?
        $email_confirmation_url = $main_project_url . 'email/confirm?token=' . $token;
        // Email confirmation Page
        $email_confirmation_page = $main_project_url . 'email/confirm';
        // Crete QR code
        $qrCodeData = $this->qrCodeProvider->createDataUri($email_confirmation_url);
        // Html body
        $twigRenderer = $this->twigRenderer;
        $twigRenderer->initTwigModel([
            'user_login' => $data['user_login'],
            'subject' => $subject,
            'main_project_url' => $main_project_url,
            'email_confirmation_url' => $email_confirmation_url,
            'email_confirmation_page'  => $email_confirmation_page,
            'email_confirmation_token' => $token,
            'qrCodeData'               => $qrCodeData
        ]);
        $twigRenderer->setTemplate('twig/email_confirm');
        $html = $twigRenderer->getHtml();

        $this->send($email_value, $subject, $html, null);
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    private function checkValidityData($data)
    {
        if ( !array_key_exists('encoded_token', $data) ) {

            throw new \Exception(self::ERROR_TOKEN_IS_ABSENT);
        }

        return true;
    }
}