<?php
namespace Core\Provider\Mail\Factory;

use Core\Service\TwigRenderer;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Provider\Mail\ConfirmationCodeSender;

/**
 * Class ConfirmationCodeSenderFactory
 * @package Core\Provider\Mail\Factory
 */
class ConfirmationCodeSenderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        // Get config
        $config = $container->get('Config');
        $twigRenderer = $container->get(TwigRenderer::class);

        return new ConfirmationCodeSender($twigRenderer, $config);
    }
}