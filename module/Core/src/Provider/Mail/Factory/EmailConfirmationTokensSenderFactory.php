<?php
namespace Core\Provider\Mail\Factory;

use Core\Provider\Mail\EmailConfirmationTokensSender;
use Core\Provider\QrCodeProvider;
use Core\Service\TwigRenderer;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class EmailConfirmationTokensSenderFactory
 * @package Core\Provider\Mail\Factory
 */
class EmailConfirmationTokensSenderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $qrCodeProvider = $container->get(QrCodeProvider::class);
        $twigRenderer = $container->get(TwigRenderer::class);
        $config = $container->get('Config');

        return new EmailConfirmationTokensSender(
            $twigRenderer,
            $config,
            $qrCodeProvider
        );
    }
}