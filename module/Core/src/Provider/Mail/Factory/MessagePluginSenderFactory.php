<?php
namespace Core\Provider\Mail\Factory;

use Core\Provider\Mail\MessagePluginSender;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Service\TwigRenderer;

class MessagePluginSenderFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $twigRenderer = $container->get(TwigRenderer::class);
        $config = $container->get('Config');

        return new MessagePluginSender(
            $twigRenderer,
            $config
        );
    }
}