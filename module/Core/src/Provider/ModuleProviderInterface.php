<?php
namespace Core\Provider;

use Interop\Container\ContainerInterface;

interface ModuleProviderInterface
{
    /**
     * @return array
     */
    public function getConfig() :array;

    /**
     * @param ContainerInterface $container
     * @return mixed
     */
    public function getAuthAdapter(ContainerInterface $container);
}