<?php
namespace Core\Provider\Token;

use Zend\Math\Rand;

/**
 * Generates and returns simple randomized token of specified length
 *
 * Trait SimpleTokenGeneratorTrait
 * @package Core\Provider\Token
 */
trait SimpleTokenGeneratorTrait
{
    /**
     * @param int $length
     * @return string
     */
    public function tokenGenerate($length=32)
    {
        return Rand::getString($length, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    }

    /**
     * @return \DateTime
     */
    public function tokenCreationDate()
    {
        return new \DateTime(date('Y-m-d H:i:s'));
    }
}