<?php
namespace Core\Provider\Token;

use Core\Entity\Interfaces\UserInterface;
use Core\Provider\Mail\MailSenderInterface;
use Core\Service\Base\BaseTokenManager;

/**
 * Class EmailConfirmationTokenProvider
 * @package Code\Provider\Token
 */
class EmailConfirmationTokenProvider implements EncodedTokenProviderInterface
{
    /**
     * @var MailSenderInterface
     */
    private $mailSender;
    /**
     * @var BaseTokenManager
     */
    private $baseTokenManager;

    /**
     * EmailConfirmationTokenProvider constructor.
     * @param MailSenderInterface|null $mailSender
     * @param BaseTokenManager $baseTokenManager
     */
    public function __construct(MailSenderInterface $mailSender = null,
                                BaseTokenManager $baseTokenManager)
    {
        $this->mailSender = $mailSender;
        $this->baseTokenManager = $baseTokenManager;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function provideToken($user, array $param = [])
    {
        try {
            // Generate encoded token
            $encoded_token = $this->encodedToken($user, $param);

            $this->send($user->getEmail()->getEmail(), [
                'encoded_token' => $encoded_token,
                'user_login'    => $user->getLogin()
            ]);
        }
        catch (\Throwable $t) {
            // No token generated
            throw new \Exception($t->getMessage());
        }

        return $encoded_token;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function encodedToken($user, array $param = [])
    {
        return $this->baseTokenManager->createEmailConfirmationToken($user->getEmail());
    }

    /**
     * @param string $email
     * @param array $param
     * @return mixed
     */
    public function send(string $email, array $param = [])
    {
        $this->mailSender->sendMail($email, $param);

        return true;
    }
}