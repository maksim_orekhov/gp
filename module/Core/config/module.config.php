<?php
namespace Core;

use Core\EventManager\AuthEventProvider;
use Core\Listener\BaseAuthListenerAggregate;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'session_containers' => [
        'ContainerNamespace'
    ],
    'time_zone' => [
        'time_zone' => 'Europe/Moscow',
    ],
    'view_manager' => [
        'template_map' => [
            'twigTemplate' => __DIR__ . '/../view/twig/template.php',
            'twig/exception_alert' => __DIR__ . '/../viewTwig/email/exception_alert.twig',
            'twig/special_mail' => __DIR__ . '/../viewTwig/email/special_mail.twig',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
            __DIR__ . '/../viewTwig',
        ],
        'factories' => [],
        'aliases' => [],
    ],
    'view_helpers' => [
        'factories' => [
            View\Helper\UserHelper::class => View\Helper\Factory\UserHelperFactory::class,
            View\Helper\Captcha\ReCaptchaV3Helper::class => InvokableFactory::class,
        ],
        'aliases' => [
            'userHelper' => View\Helper\UserHelper::class,
            'recaptchaV3Helper' => View\Helper\Captcha\ReCaptchaV3Helper::class,
        ],
    ],
    'event_provider' => [
        //provider class => listeners
        AuthEventProvider::class => [
            //listener class => priority
            BaseAuthListenerAggregate::class => 9999,
        ],
    ],
];
