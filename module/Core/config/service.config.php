<?php
namespace Core;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'service_manager' => [
        'factories' => [
            Adapter\TokenAdapter::class => Adapter\Factory\TokenAdapterFactory::class,
            Adapter\PhoneNumberUtilAdapter::class => InvokableFactory::class,

            Provider\Mail\MessagePluginSender::class => Provider\Mail\Factory\MessagePluginSenderFactory::class,
            Provider\Mail\ConfirmationCodeSender::class => Provider\Mail\Factory\ConfirmationCodeSenderFactory::class,
            Provider\Mail\EmailConfirmationTokensSender::class => Provider\Mail\Factory\EmailConfirmationTokensSenderFactory::class,
            Provider\Token\EmailConfirmationTokenProvider::class => Provider\Token\Factory\EmailConfirmationTokenProviderFactory::class,
            Provider\QrCodeProvider::class => Provider\Factory\QrCodeProviderFactory::class,

            Controller\Plugin\Message\MessagePlugin::class => Controller\Plugin\Message\Factory\MessagePluginFactory::class,

            \Zend\Authentication\AuthenticationService::class => Service\Auth\Factory\AbstractAuthServiceFactory::class,
            Service\Auth\AuthService::class => Service\Auth\Factory\AbstractAuthServiceFactory::class,
            Service\SessionContainerManager::class => Service\Factory\SessionContainerManagerFactory::class,
            Service\TimeZone::class => Service\Factory\TimeZoneFactory::class,
            Service\TwigViewModel::class => InvokableFactory::class,
            Service\TwigRenderer::class => Service\Factory\TwigRendererFactory::class,
            Service\OperationConfirmManager::class => Service\Factory\OperationConfirmManagerFactory::class,
            Service\Base\BaseTokenManager::class => Service\Base\Factory\BaseTokenManagerFactory::class,
            Service\Base\BaseUserManager::class => Service\Base\Factory\BaseUserManagerFactory::class,
            Service\Base\BaseEmailManager::class => Service\Base\Factory\BaseEmailManagerFactory::class,
            Service\Base\BaseRoleManager::class => Service\Base\Factory\BaseRoleManagerFactory::class,
            Service\Base\BaseAuthManager::class => Service\Base\Factory\BaseAuthManagerFactory::class,
            Service\Base\BasePhoneManager::class => Service\Base\Factory\BasePhoneManagerFactory::class,
            Extension\TwigExtension::class => Extension\Factory\TwigExtensionFactory::class,
            // EventManagers
            EventManager\AuthEventProvider::class => InvokableFactory::class,
            EventManager\NotifyEventProvider::class => InvokableFactory::class,
            //listeners
            Listener\BaseAuthListenerAggregate::class => Listener\Factory\BaseAuthListenerAggregateFactory::class,
        ],
    ],
];