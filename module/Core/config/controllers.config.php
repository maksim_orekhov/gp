<?php
namespace Core;

return [
    'controller_plugins' => [
        'factories' => [
            Controller\Plugin\Message\MessagePlugin::class => Controller\Plugin\Message\Factory\MessagePluginFactory::class,
            Controller\Plugin\CookiePlugin::class => Controller\Plugin\Factory\CookiePluginFactory::class,
        ],
        'aliases' => [
            'message' => Controller\Plugin\Message\MessagePlugin::class,
            'cookie' => Controller\Plugin\CookiePlugin::class,
        ],
    ],
];
