<?php
namespace CoreTest\Exception;

use Core\Exception\LogicException;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class LogicExceptionTest extends TestCase
{
    const TEST_MESSAGE_TEXT = 'test message text';
    /**
     * Тест конструктора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group exception
     * @throws \ReflectionException
     */
    public function testExceptionConstruct()
    {
        // now call the constructor
        $reflectedClass = new ReflectionClass(LogicException::class);
        $constructor = $reflectedClass->getConstructor();
        $parameters = $constructor->getParameters();
        //тетс что в конструкторе 2 параметра
        $this->assertCount(2, $parameters);
        $this->assertEquals('exceptionMessage', $parameters[0]->getName());
        $this->assertEquals('exceptionCode', $parameters[1]->getName());

    }

    /**
     * Тест с несушествуюшей константой и с незаданным текстом сообшения
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group exception
     */
    public function testExceptionWithInvalidCodeAndWithoutMessage()
    {
        try {
            throw new LogicException(null, 'invalid code');
        }
        catch (\Throwable $t){
            /** @var LogicException $t */
            $this->assertInstanceOf(\RuntimeException::class, $t);
            $this->assertEquals('constant "invalid code" not found', $t->getMessage());
        }
    }

    /**
     * Тест когда сообшение совподает с константой кода
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group exception
     */
    public function testExceptionWithMessageEqualsCode()
    {
        try {
            throw new LogicException(LogicException::PAGE_NOT_FOUND, null);
        }
        catch (\Throwable $t){
            /** @var LogicException $t */
            $this->assertInstanceOf(\RuntimeException::class, $t);
            $this->assertEquals('Message "PAGE_NOT_FOUND" matching with constant for code', $t->getMessage());
        }
    }

    /**
     * Тест с незаданным текстом сообшения и с незаданной константой
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group exception

     */
    public function testExceptionWithoutCodeAndWithoutMessage()
    {
        try {
            throw new LogicException(null, null);
        }
        catch (\Throwable $t){
            /** @var LogicException $t */
            $this->assertInstanceOf(LogicException::class, $t);
            $this->assertTrue(method_exists($t, 'getExceptionType'));
            $this->assertTrue(method_exists($t, 'getExceptionCode'));
            $this->assertTrue(method_exists($t, 'getExceptionMessage'));
            $this->assertEquals('LogicException', $t->getExceptionType());
            $this->assertEquals(null, $t->getExceptionCode());
            $this->assertEquals(null, $t->getExceptionMessage());
            $this->assertEquals('', $t->getMessage());
        }
    }

    /**
     * Тест с сушествуюшей константой и с незаданным текстом сообшения
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group exception

     */
    public function testExceptionWithValidCodeAndWithoutMessage()
    {
        try {
            throw new LogicException(null, LogicException::PAGE_NOT_FOUND);
        }
        catch (\Throwable $t){
            /** @var LogicException $t */
            $this->assertInstanceOf(LogicException::class, $t);
            $this->assertTrue(method_exists($t, 'getExceptionType'));
            $this->assertTrue(method_exists($t, 'getExceptionCode'));
            $this->assertTrue(method_exists($t, 'getExceptionMessage'));
            $this->assertEquals('LogicException', $t->getExceptionType());
            $this->assertEquals('PAGE_NOT_FOUND', $t->getExceptionCode());
            $this->assertEquals(LogicException::PAGE_NOT_FOUND_MESSAGE, $t->getExceptionMessage());
            $this->assertEquals(LogicException::PAGE_NOT_FOUND_MESSAGE, $t->getMessage());
        }
    }

    /**
     * Тест с заданным текстом сообшения и с незаданной константой
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group exception

     */
    public function testExceptionWithoutCodeAndWithMessage()
    {
        try {
            throw new LogicException(self::TEST_MESSAGE_TEXT, null);
        }
        catch (\Throwable $t){
            /** @var LogicException $t */
            $this->assertInstanceOf(LogicException::class, $t);
            $this->assertTrue(method_exists($t, 'getExceptionType'));
            $this->assertTrue(method_exists($t, 'getExceptionCode'));
            $this->assertTrue(method_exists($t, 'getExceptionMessage'));
            $this->assertEquals('LogicException', $t->getExceptionType());
            $this->assertEquals(null, $t->getExceptionCode());
            $this->assertEquals(self::TEST_MESSAGE_TEXT, $t->getExceptionMessage());
            $this->assertEquals(self::TEST_MESSAGE_TEXT, $t->getMessage());
        }
    }

    /**
     * Тест с заданным текстом сообшения и с заданной константой
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group exception

     */
    public function testExceptionWithCodeAndWithMessage()
    {
        try {
            throw new LogicException(self::TEST_MESSAGE_TEXT, LogicException::PAGE_NOT_FOUND);
        }
        catch (\Throwable $t){
            /** @var LogicException $t */
            $this->assertInstanceOf(LogicException::class, $t);
            $this->assertTrue(method_exists($t, 'getExceptionType'));
            $this->assertTrue(method_exists($t, 'getExceptionCode'));
            $this->assertTrue(method_exists($t, 'getExceptionMessage'));
            $this->assertEquals('LogicException', $t->getExceptionType());
            $this->assertEquals('PAGE_NOT_FOUND', $t->getExceptionCode());
            $this->assertEquals(self::TEST_MESSAGE_TEXT, $t->getExceptionMessage());
            $this->assertNotEquals(LogicException::PAGE_NOT_FOUND_MESSAGE, $t->getMessage());
            $this->assertEquals(self::TEST_MESSAGE_TEXT, $t->getMessage());
        }
    }
}