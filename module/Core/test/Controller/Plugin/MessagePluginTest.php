<?php
namespace CoreTest\Controller\Plugin;

use Core\Controller\Plugin\Message\MessagePlugin;
use Core\Exception\CriticalException;
use Core\Exception\LogicException;
use CoreTest\ViewVarsTrait;
use Zend\Mvc\Controller\PluginManager;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractControllerTestCase;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Validator\ValidatorPluginManager;

/**
 * Class MessagePluginTest
 * @package CoreTest\Controller\Plugin
 */
class MessagePluginTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const TEST_MESSAGE_TEXT = 'test_message_text';
    /**
     * @var MessagePlugin
     */
    private $messagePlugin;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            include __DIR__ . '/../../../../../config/autoload/global.php'
        ));

        // Отключаем отправку уведомлений на email
        $this->resetConfigParameters();
    }

    public function tearDown() {

        parent::tearDown();
        $this->messagePlugin = null;

        gc_collect_cycles();
    }

    /**
     * @param bool $isXmlHttpRequest
     * @throws \Exception
     */
    public function dispatchWithMessagePlugin($isXmlHttpRequest = false)
    {
        //идем на любой роут для получения плагина для контроллера
        $this->dispatch('/', 'GET', [], $isXmlHttpRequest);
        $serviceManager = $this->getApplicationServiceLocator();
        $pluginManager = $serviceManager->get(PluginManager::class);
        $this->messagePlugin = $pluginManager->get('message');
    }

    private function resetConfigParameters()
    {
        // Override config var multifactor_authorization
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');
        $config['email_providers']['message_send_simulation'] = true;
        $services->setAllowOverride(true);
        $services->setService('Config', $config);
        $services->setAllowOverride(false);
    }

    /**
     * тест error message
     *
     * @throws \Exception
     */
    public function testErrorMessage()
    {
        $isXmlHttpRequest = false;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->error(self::TEST_MESSAGE_TEXT);
         /** @var array $view_vars */
        $view_vars = $view->getVariables();
        $view_template = $view->getTemplate();
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(self::TEST_MESSAGE_TEXT, $view_vars['message']);
        $this->assertEquals(MessagePlugin::$twig_model_templates[MessagePlugin::TEMPLATE_ERROR], $view_template);
    }

    /**
     * тест success message
     *
     * @throws \Exception
     */
    public function testSuccessMessage()
    {
        $isXmlHttpRequest = false;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->success(self::TEST_MESSAGE_TEXT);
        /** @var array $view_vars */
        $view_vars = $view->getVariables();
        $view_template = $view->getTemplate();
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(self::TEST_MESSAGE_TEXT, $view_vars['message']);
        $this->assertEquals(MessagePlugin::$twig_model_templates[MessagePlugin::TEMPLATE_SUCCESS], $view_template);
    }

    /**
     * тест underConstruction message
     *
     * @throws \Exception
     */
    public function testUnderConstructionMessage()
    {
        $isXmlHttpRequest = false;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->underConstruction(self::TEST_MESSAGE_TEXT);
        /** @var array $view_vars */
        $view_vars = $view->getVariables();
        $view_template = $view->getTemplate();
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(self::TEST_MESSAGE_TEXT, $view_vars['message']);
        $this->assertEquals(MessagePlugin::$twig_model_templates[MessagePlugin::TEMPLATE_UNDER_CONSTRUCTION], $view_template);
    }

    /**
     * тест exception message с LogicException
     *
     * @throws \Exception
     */
    public function testExceptionMessage()
    {
        $isXmlHttpRequest = false;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);
        try{
            $this->messagePlugin->exception(new \Exception(self::TEST_MESSAGE_TEXT));
        }catch (\Throwable $t){

            $this->assertEquals(self::TEST_MESSAGE_TEXT, $t->getMessage());
        }
    }

    /**
     * тест exception message с LogicException
     *
     * @throws \Exception
     */
    public function testExceptionMessageWithLogicException()
    {
        $isXmlHttpRequest = false;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->exception(new LogicException(self::TEST_MESSAGE_TEXT));
        /** @var array $view_vars */
        $view_vars = $view->getVariables();
        $view_template = $view->getTemplate();
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(self::TEST_MESSAGE_TEXT, $view_vars['message']);
        $this->assertEquals(MessagePlugin::$twig_model_templates[MessagePlugin::TEMPLATE_ERROR], $view_template);
    }

    /**
     * тест exception message с LogicException (ajax)
     *
     * @throws \Exception
     */
    public function testExceptionMessageWithLogicExceptionForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->exception(new LogicException(self::TEST_MESSAGE_TEXT));
        /** @var array $view_vars */
        $view_vars = $view->getVariables();

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertArrayHasKey('status', $view_vars);
        $this->assertArrayHasKey('code', $view_vars);
        $this->assertArrayHasKey('data', $view_vars);
        $this->assertEquals(self::TEST_MESSAGE_TEXT, $view_vars['message']);
        $this->assertEquals('ERROR', $view_vars['status']);
        $this->assertEquals(null, $view_vars['code']);
        $this->assertNotNull($view_vars['data']);
        $this->assertArrayHasKey('message', $view_vars['data']);
        $this->assertArrayHasKey('file', $view_vars['data']);
        $this->assertArrayHasKey('line', $view_vars['data']);
        $this->assertArrayHasKey('trace', $view_vars['data']);
    }

    /**
     * тест exception message с LogicException и с кодом (ajax)
     *
     * @throws \Exception
     */
    public function testExceptionMessageWithLogicExceptionWithCodeForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->exception(new LogicException(null, LogicException::INTERNAL_SERVER_ERROR));
        /** @var array $view_vars */
        $view_vars = $view->getVariables();

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertArrayHasKey('status', $view_vars);
        $this->assertArrayHasKey('code', $view_vars);
        $this->assertArrayHasKey('data', $view_vars);
        $this->assertEquals(LogicException::INTERNAL_SERVER_ERROR_MESSAGE, $view_vars['message']);
        $this->assertEquals('ERROR', $view_vars['status']);
        $this->assertEquals(LogicException::INTERNAL_SERVER_ERROR, $view_vars['code']);
        $this->assertNotNull($view_vars['data']);
        $this->assertArrayHasKey('message', $view_vars['data']);
        $this->assertArrayHasKey('file', $view_vars['data']);
        $this->assertArrayHasKey('line', $view_vars['data']);
        $this->assertArrayHasKey('trace', $view_vars['data']);
    }

    /**
     * тест exception message с CriticalException
     *
     * @throws \Exception
     */
    public function testExceptionMessageWithCriticalException()
    {
        $isXmlHttpRequest = false;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->exception(new CriticalException(self::TEST_MESSAGE_TEXT));
        /** @var array $view_vars */
        $view_vars = $view->getVariables();
        $view_template = $view->getTemplate();
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(self::TEST_MESSAGE_TEXT, $view_vars['message']);
        $this->assertEquals(MessagePlugin::$twig_model_templates[MessagePlugin::TEMPLATE_ERROR], $view_template);
    }

    /**
     * тест exception message с CriticalException (ajax)
     *
     * @throws \Exception
     */
    public function testExceptionMessageWithCriticalExceptionForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->exception(new CriticalException(self::TEST_MESSAGE_TEXT));
        /** @var array $view_vars */
        $view_vars = $view->getVariables();

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertArrayHasKey('status', $view_vars);
        $this->assertArrayHasKey('code', $view_vars);
        $this->assertArrayHasKey('data', $view_vars);
        $this->assertEquals(self::TEST_MESSAGE_TEXT, $view_vars['message']);
        $this->assertEquals('ERROR', $view_vars['status']);
        $this->assertEquals(null, $view_vars['code']);
        $this->assertNotNull($view_vars['data']);
        $this->assertArrayHasKey('message', $view_vars['data']);
        $this->assertArrayHasKey('file', $view_vars['data']);
        $this->assertArrayHasKey('line', $view_vars['data']);
        $this->assertArrayHasKey('trace', $view_vars['data']);
    }

    /**
     * тест exception message с CriticalException (ajax)
     *
     * @throws \Exception
     */
    public function testExceptionMessageWithCriticalExceptionWithCodeForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->exception(new CriticalException(null, CriticalException::INTERNAL_SERVER_ERROR));
        /** @var array $view_vars */
        $view_vars = $view->getVariables();

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertArrayHasKey('status', $view_vars);
        $this->assertArrayHasKey('code', $view_vars);
        $this->assertArrayHasKey('data', $view_vars);
        $this->assertEquals(CriticalException::INTERNAL_SERVER_ERROR_MESSAGE, $view_vars['message']);
        $this->assertEquals('ERROR', $view_vars['status']);
        $this->assertEquals(CriticalException::INTERNAL_SERVER_ERROR, $view_vars['code']);
        $this->assertNotNull($view_vars['data']);
        $this->assertArrayHasKey('message', $view_vars['data']);
        $this->assertArrayHasKey('file', $view_vars['data']);
        $this->assertArrayHasKey('line', $view_vars['data']);
        $this->assertArrayHasKey('trace', $view_vars['data']);
    }

    /**
     * тест invalidFormData message
     *
     * @throws \Exception
     */
    public function testInvalidFormDataMessage()
    {
        $isXmlHttpRequest = false;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);
        $formData = [
            'field' => [
                'invalid'=>'invalid message'
            ]
        ];
        $view = $this->messagePlugin->invalidFormData($formData);
        /** @var array $view_vars */
        $view_vars = $view->getVariables();
        $view_template = $view->getTemplate();
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA_MESSAGE, $view_vars['message']);
        $this->assertEquals(MessagePlugin::$twig_model_templates[MessagePlugin::TEMPLATE_ERROR], $view_template);
    }

    /**
     * тест invalidFormData message (ajax)
     *
     * @throws \Exception
     */
    public function testInvalidFormDataMessageForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);
        $formData = [
            'field' => [
                'invalid'=>'invalid message'
            ]
        ];
        $view = $this->messagePlugin->invalidFormData($formData);
        /** @var array $view_vars */
        $view_vars = $view->getVariables();

        $this->assertArrayHasKey('status', $view_vars);
        $this->assertArrayHasKey('code', $view_vars);
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertArrayHasKey('data', $view_vars);
        $this->assertEquals('ERROR', $view_vars['status']);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA, $view_vars['code']);
        $this->assertEquals(MessagePlugin::ERROR_INVALID_FORM_DATA_MESSAGE, $view_vars['message']);
        $this->assertEquals($formData, $view_vars['data']);
    }

    /**
     * тест error message с кодом но без сообшения
     *
     * @throws \Exception
     */
    public function testErrorMessageWithCodeAndWithoutMessage()
    {
        $isXmlHttpRequest = false;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->error(null, null, MessagePlugin::ERROR_ACCESS_DENIED);
        /** @var array $view_vars */
        $view_vars = $view->getVariables();
        $view_template = $view->getTemplate();
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(MessagePlugin::ERROR_ACCESS_DENIED_MESSAGE, $view_vars['message']);
        $this->assertEquals(MessagePlugin::$twig_model_templates[MessagePlugin::TEMPLATE_ERROR], $view_template);
    }

    /**
     * тест error message с кодом но без сообшения (ajax)
     *
     * @throws \Exception
     */
    public function testErrorMessageWithCodeAndWithoutMessageForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->error(null, null, MessagePlugin::ERROR_ACCESS_DENIED);
        /** @var array $view_vars */
        $view_vars = $view->getVariables();

        $this->assertArrayHasKey('status', $view_vars);
        $this->assertArrayHasKey('code', $view_vars);
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertArrayHasKey('data', $view_vars);
        $this->assertEquals('ERROR', $view_vars['status']);
        $this->assertEquals(MessagePlugin::ERROR_ACCESS_DENIED, $view_vars['code']);
        $this->assertEquals(MessagePlugin::ERROR_ACCESS_DENIED_MESSAGE, $view_vars['message']);
        $this->assertEquals(null, $view_vars['data']);
    }

    /**
     * тест success message с кодом но без сообшения
     *
     * @throws \Exception
     */
    public function testSuccessMessageWithCodeAndWithoutMessage()
    {
        $isXmlHttpRequest = false;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->success(null, null, MessagePlugin::SUCCESS_RESPONSE);
        /** @var array $view_vars */
        $view_vars = $view->getVariables();
        $view_template = $view->getTemplate();
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(MessagePlugin::SUCCESS_RESPONSE_MESSAGE, $view_vars['message']);
        $this->assertEquals(MessagePlugin::$twig_model_templates[MessagePlugin::TEMPLATE_SUCCESS], $view_template);
    }

    /**
     * тест success message с кодом но без сообшения (ajax)
     *
     * @throws \Exception
     */
    public function testSuccessMessageWithCodeAndWithoutMessageForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->success(null, null, MessagePlugin::SUCCESS_RESPONSE);
        /** @var array $view_vars */
        $view_vars = $view->getVariables();

        $this->assertArrayHasKey('status', $view_vars);
        $this->assertArrayHasKey('code', $view_vars);
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertArrayHasKey('data', $view_vars);
        $this->assertEquals('SUCCESS', $view_vars['status']);
        $this->assertEquals(MessagePlugin::SUCCESS_RESPONSE, $view_vars['code']);
        $this->assertEquals(MessagePlugin::SUCCESS_RESPONSE_MESSAGE, $view_vars['message']);
        $this->assertEquals(null, $view_vars['data']);
    }

    /**
     * тест under construction message с кодом но без сообшения
     *
     * @throws \Exception
     */
    public function testUnderConstructionMessageWithCodeAndWithoutMessage()
    {
        $isXmlHttpRequest = false;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->underConstruction(null, null, MessagePlugin::SUCCESS_RESPONSE);
        /** @var array $view_vars */
        $view_vars = $view->getVariables();
        $view_template = $view->getTemplate();
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals(MessagePlugin::SUCCESS_RESPONSE_MESSAGE, $view_vars['message']);
        $this->assertEquals(MessagePlugin::$twig_model_templates[MessagePlugin::TEMPLATE_UNDER_CONSTRUCTION], $view_template);
    }

    /**
     * тест under construction message с кодом но без сообшения (ajax)
     *
     * @throws \Exception
     */
    public function testUnderConstructionMessageWithCodeAndWithoutMessageForAjax()
    {
        $isXmlHttpRequest = true;
        $this->dispatchWithMessagePlugin($isXmlHttpRequest);

        $view = $this->messagePlugin->underConstruction(null, null, MessagePlugin::SUCCESS_RESPONSE);
        /** @var array $view_vars */
        $view_vars = $view->getVariables();

        $this->assertArrayHasKey('status', $view_vars);
        $this->assertArrayHasKey('code', $view_vars);
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertArrayHasKey('data', $view_vars);
        $this->assertEquals('SUCCESS', $view_vars['status']);
        $this->assertEquals(MessagePlugin::SUCCESS_RESPONSE, $view_vars['code']);
        $this->assertEquals(MessagePlugin::SUCCESS_RESPONSE_MESSAGE, $view_vars['message']);
        $this->assertEquals(null, $view_vars['data']);
    }
}