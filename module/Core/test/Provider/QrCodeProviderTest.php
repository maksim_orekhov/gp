<?php
namespace CoreTest\Provider;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use Core\Provider\QrCodeProvider;
use Zxing\Qrcode\QRCodeReader;
use Zxing\QrReader;

/**
 * Class QrCodeProviderTest
 * @package ApplicationTest\Provider
 */
class QrCodeProviderTest extends AbstractHttpControllerTestCase
{
    /**
     * @var QrCodeProvider
     */
    private $qrCodeProvider;

    /**
     * @var array
     */
    private $config;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $this->config = $this->getApplicationConfig();

        $this->qrCodeProvider = new QrCodeProvider($this->config);
    }


    public function typeDataProvider()
    {
        return [
            ['png', 'data:image/png;base64'],
            ['svg', 'data:image/svg+xml;base64'],
            ['debug', 'data:text/plain;base64'],
        ];
    }

    /**
     * @dataProvider typeDataProvider
     * @param $format
     * @param $expected
     * @throws \Endroid\QrCode\Exception\InvalidWriterException
     */
    public function testCreate($format, $expected)
    {
        $qrCodeData = $this->qrCodeProvider->createDataUri('test', null, null, $format);

        $this->assertInternalType('string', $qrCodeData);
        $this->assertContains($expected, $qrCodeData);
    }

    /**
     * Генерируем QR код, сохраняем. Считываем ридером, достаем текст и сравниваем с исходным
     */
    public function testCreteAndReadCode()
    {
        $text = 'http://alpha.guarantpay.com/';

        $create_result = $this->qrCodeProvider->createFile($text, './upload/test/qrcode.png', 300);
        $this->assertTrue($create_result);

        #$reader = new QrReader('./' . $this->config['file-management']['main_upload_folder'] . '/test/qrcode.png');
        #$this->assertEquals($text, $reader->text());
    }
}