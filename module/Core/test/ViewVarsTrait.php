<?php
namespace CoreTest;

use Zend\Mvc\MvcEvent;

/**
 * Trait ViewVarsTrait
 * @package CoreTest
 */
trait ViewVarsTrait
{
    /**
     * Возвращает переменные, которые передаются в шаблон
     *
     * @return array
     */
    public function getViewVars()
    {
        /** @var MvcEvent $events */
        $events = $this->getApplication()->getMvcEvent();
        /** @var array $view_vars */
        $view_vars = $events->getResult()->getVariables();

        return $view_vars;
    }
}