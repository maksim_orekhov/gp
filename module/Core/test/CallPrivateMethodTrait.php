<?php
namespace CoreTest;

/**
 * Trait CallPrivateMethodTrait
 * @package CoreTest
 */
trait CallPrivateMethodTrait
{
    /**
     * Call protected/private method of a class.
     *
     * @param object &$object
     * @param string
     * @param array
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}