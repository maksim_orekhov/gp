<?php
namespace CoreTest\Adapter;

use Core\Adapter\TokenAdapter;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use ApplicationTest\Bootstrap;

/**
 * Class QrCodeProviderTest
 * @package CoreTest\Adapter
 */
class QrCodeProviderTest extends AbstractHttpControllerTestCase
{
    /**
     * @var TokenAdapter
     */
    private $tokenAdapter;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->config = $this->getApplicationConfig();

        $this->tokenAdapter = $serviceManager->get(TokenAdapter::class);
    }

    /**
     * @throws \Core\Exception\LogicException
     *
     * @expectedException \Core\Exception\LogicException
     * @expectedExceptionMessage Data for creation token is empty.
     */
    public function testCreateTokenFromEmptyArray()
    {
        $data = [];

        $this->tokenAdapter->createToken($data);
    }

    /**
     * @throws \Core\Exception\LogicException
     */
    public function testCreateTokenFromValidData()
    {
        $data = ['email' => 'rtyu@emaiol.com' , 'dimple_token' => 'yty565656'];

        $token = $this->createToken($data);

        $this->assertNotNull($token);
        $this->assertInternalType('string', $token);
    }

    /**
     * @throws \Core\Exception\LogicException
     *
     * @expectedException \Core\Exception\LogicException
     */
    public function testDecodeTokenInvalid()
    {
        // Создаем токен
        $data = ['email' => 'rtyu@emaiol.com' , 'dimple_token' => 'yty565656'];
        $encoded_token = $this->createToken($data);

        $this->assertNotNull($encoded_token);

        // Делаем полученный токен невалидным
        $encoded_token .= '56';

        // Должен выбросить Exception
        $this->tokenAdapter->decodeToken($encoded_token);
    }

    /**
     * @throws \Core\Exception\LogicException
     */
    public function testDecodeTokenValid()
    {
        // Создаем токен
        $data = ['email' => 'rtyu@emaiol.com' , 'dimple_token' => 'yty565656'];
        $encoded_token = $this->createToken($data);

        $this->assertNotNull($encoded_token);

        /** @var \stdClass $decoded_token */
        $decodedToken = $this->tokenAdapter->decodeToken($encoded_token);

        $this->assertNotNull($decodedToken);
        $this->assertInstanceOf(\stdClass::class, $decodedToken);
        $this->assertEquals($data['email'], $decodedToken->email);
        $this->assertEquals($data['dimple_token'], $decodedToken->dimple_token);
    }

    /**
     * @param array $data
     * @return string
     * @throws \Core\Exception\LogicException
     */
    private function createToken(array $data)
    {
        return $this->tokenAdapter->createToken($data);
    }
}