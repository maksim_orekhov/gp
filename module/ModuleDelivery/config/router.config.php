<?php
namespace ModuleDelivery;

use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            // DeliveryCost
            'delivery-cost' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/delivery/cost',
                    'defaults' => [
                        'controller' => Controller\DeliveryCostController::class,
                        'action' => 'getCost',
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            // Delivery Order
            'delivery-order-single' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/delivery/order/:id',
                    'constraints' => [
                        'id'     => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Controller\DeliveryOrderController::class,
                    ],
                ]
            ],
            'delivery-order-form-init' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/delivery/order/form-init',
                    'defaults' => [
                        'controller' => Controller\DeliveryOrderController::class,
                        'action' => 'formInit',
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            'delivery-order-create-order' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/delivery/order/:idDeliveryOrder/create-order',
                    'constraints' => [
                        'idDeliveryOrder' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\DeliveryOrderController::class,
                        'action' => 'createServiceOrder',
                    ],
                ],
            ],
            'delivery-order-creation-status' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/delivery/order/:idDeliveryOrder/creation-status',
                    'constraints' => [
                        'idDeliveryOrder' => '\d+',
                    ],
                    'defaults' => [
                        'controller' => Controller\DeliveryOrderController::class,
                        'action' => 'updateServiceOrderCreationStatus',
                    ],
                ],
            ],
        ]
    ],
];
