<?php
namespace ModuleDelivery;

return [
    'view_manager' => [
        'template_map' => [

        ],
        'template_path_stack' => [
            __DIR__ . '/../viewTwig',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
