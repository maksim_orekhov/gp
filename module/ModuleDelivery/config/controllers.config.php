<?php
namespace ModuleDelivery;

return [
    'controllers' => [
        'factories' => [
            Controller\DeliveryOrderController::class => Controller\Factory\DeliveryOrderControllerFactory::class,
            Controller\DeliveryCostController::class => Controller\Factory\DeliveryCostControllerFactory::class,
        ],
    ],
];
