<?php
namespace ModuleDelivery;

use Application\Listener\DeliveryListenerAggregate;
use ModuleDelivery\Listener\DeliveryOrderListenerAggregate;
use ModuleDelivery\EventManager\DeliveryEventProvider;

return [
    'event_provider' => [
        //provider class => listeners
        DeliveryEventProvider::class => [
            //listener class => priority
            DeliveryOrderListenerAggregate::class => 9998,
            DeliveryListenerAggregate::class => 9998,
        ],
    ],
];
