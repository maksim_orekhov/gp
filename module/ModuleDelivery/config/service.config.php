<?php
namespace ModuleDelivery;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'service_manager' => [
        'factories' => [
            Service\DeliveryOrderManager::class => Service\Factory\DeliveryOrderManagerFactory::class,
            Service\DeliveryTrackingNumberManager::class => Service\Factory\DeliveryTrackingNumberManagerFactory::class,
            Service\DeliveryOrderDataValidator::class => InvokableFactory::class,
            EventManager\DeliveryEventProvider::class => InvokableFactory::class,
            Listener\DeliveryOrderListenerAggregate::class => Listener\Factory\DeliveryOrderListenerAggregateFactory::class,
            Service\DeliveryOrderPolitics::class => InvokableFactory::class,
        ],
    ],
];
