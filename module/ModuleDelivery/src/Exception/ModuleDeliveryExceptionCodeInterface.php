<?php
namespace ModuleDelivery\Exception;

use ModuleDelivery\Exception\Code\DeliveryExceptionCodeInterface;

/**
 * Interface BaseExceptionCodeInterface
 * @package Core\Exception
 */
interface ModuleDeliveryExceptionCodeInterface extends
    DeliveryExceptionCodeInterface
{
    /**
     * Только для общих кодов, которые могут быть использованы везде!!!
     *
     * без префикса, для сообшений суфикс _MESSAGE
     */
}