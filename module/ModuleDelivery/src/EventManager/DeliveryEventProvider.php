<?php
namespace ModuleDelivery\EventManager;

use Core\EventManager\AbstractEventProvider;

/**
 * Class DeliveryEventProvider
 * @package Application\EventManager
 */
class DeliveryEventProvider extends AbstractEventProvider
{
    const EVENT_CREATE_DELIVERY_ORDER = 'EVENT_CREATE_DELIVERY_ORDER';
    const EVENT_CREATE_DELIVERY_SERVICE_ORDER = 'EVENT_CREATE_DELIVERY_SERVICE_ORDER';
    const EVENT_DELIVERY_SERVICE_ORDER_CREATED = 'EVENT_DELIVERY_SERVICE_ORDER_CREATED';
    const EVENT_PARCELS_DELIVERED = 'EVENT_PARCELS_DELIVERED';
    const EVENT_PARCELS_DELIVERY_FAILED = 'EVENT_PARCELS_DELIVERY_FAILED';

    //edit
    const EVENT_EDIT_DELIVERY_ORDER = 'EVENT_EDIT_DELIVERY_ORDER';
}