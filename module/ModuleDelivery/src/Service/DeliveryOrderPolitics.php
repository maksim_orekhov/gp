<?php
namespace ModuleDelivery\Service;

use Application\Entity\Deal;
use ModuleDelivery\Entity\DeliveryOrder;

/**
 * Class DeliveryOrderPolitics
 * @package ModuleDelivery\Service
 */
class DeliveryOrderPolitics
{
    public function isServiceOrderCreationAllowed($user, DeliveryOrder $deliveryOrder): bool
    {
        /** @var Deal $deal */
        $deal = $deliveryOrder->getDeal();

        return $user === $deal->getContractor()->getUser();
    }
}