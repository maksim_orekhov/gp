<?php
namespace ModuleDelivery\Service;

use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\Entity\DeliveryTrackingNumber;

/**
 * Class DeliveryTrackingNumberManager
 * @package ModuleDelivery\Service
 */
class DeliveryTrackingNumberManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * DeliveryTrackingNumberManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $id
     * @return DeliveryTrackingNumber
     */
    public function getDeliveryTrackingNumber(int $id): DeliveryTrackingNumber
    {
        /** @var DeliveryTrackingNumber $deliveryTrackingNumber */
        $deliveryTrackingNumber = $this->entityManager
            ->getRepository(DeliveryTrackingNumber::class)->find($id);

        return $deliveryTrackingNumber;
    }

    /**
     * @param string $tracking_number
     * @return DeliveryTrackingNumber|null
     */
    public function getDeliveryTrackingNumberByNumber(string $tracking_number)
    {
        /** @var DeliveryTrackingNumber $deliveryTrackingNumber */
        $deliveryTrackingNumber = $this->entityManager->getRepository(DeliveryTrackingNumber::class)
            ->findOneBy(['trackingNumber' => $tracking_number]);

        return $deliveryTrackingNumber;
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @param string $tracking_number
     * @return DeliveryTrackingNumber
     * @throws LogicException
     */
    public function createDeliveryTrackingNumber(DeliveryOrder $deliveryOrder, string $tracking_number): DeliveryTrackingNumber
    {
        $deliveryTrackingNumber = $this->getDeliveryTrackingNumberByNumber($tracking_number);

        if (null !== $deliveryTrackingNumber) {

            throw new LogicException(null, LogicException::DELIVERY_TRACKING_NUMBER_ALREADY_EXISTS);
        }

        $deliveryTrackingNumber = new DeliveryTrackingNumber();
        $deliveryTrackingNumber->setCreated(new \DateTime());
        $deliveryTrackingNumber->setTrackingNumber($tracking_number);
        $deliveryTrackingNumber->setDeliveryOrder($deliveryOrder);

        return $deliveryTrackingNumber;
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @param string $tracking_number
     * @return DeliveryTrackingNumber
     * @throws LogicException
     * @throws \Exception
     */
    public function createCustomDeliveryTrackingNumber(DeliveryOrder $deliveryOrder, string $tracking_number)
    {
        $deliveryTrackingNumber = $this->createDeliveryTrackingNumber($deliveryOrder, $tracking_number);
        $deliveryOrder->addDeliveryTrackingNumber($deliveryTrackingNumber);

        $this->entityManager->persist($deliveryTrackingNumber);
        $this->entityManager->persist($deliveryOrder);
        $this->entityManager->flush();

        return $deliveryTrackingNumber;
    }
}