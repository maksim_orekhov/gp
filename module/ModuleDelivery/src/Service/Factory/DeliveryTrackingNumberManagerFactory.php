<?php
namespace ModuleDelivery\Service\Factory;

use Interop\Container\ContainerInterface;
use ModuleDelivery\Service\DeliveryTrackingNumberManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryTrackingNumberManagerFactory
 * @package ModuleDelivery\Service\Factory
 */
class DeliveryTrackingNumberManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryTrackingNumberManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new DeliveryTrackingNumberManager(
            $entityManager
        );
    }
}