<?php
namespace ModuleDelivery\Service\Factory;

use Core\Controller\Plugin\Message\MessagePlugin;
use Interop\Container\ContainerInterface;
use ModuleDelivery\Service\DeliveryOrderDataValidator;
use ModuleDelivery\Service\DeliveryTrackingNumberManager;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdManager;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdValidator;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleDelivery\Service\DeliveryOrderManager;

/**
 * Class DeliveryOrderManagerFactory
 * @package ModuleDeliveryDpd\Service\Factory
 */
class DeliveryOrderManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryOrderManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $deliveryOrderDataValidator   = $container->get(DeliveryOrderDataValidator::class);
        $deliveryOrderDpdManager      = $container->get(DeliveryOrderDpdManager::class);
        $deliveryOrderDpdValidator    = $container->get(DeliveryOrderDpdValidator::class);
        $deliveryDpdManager           = $container->get(DeliveryDpdManager ::class);
        $deliveryTrackingNumberManager = $container->get(DeliveryTrackingNumberManager ::class);
        $messagePlugin = $container->get(MessagePlugin ::class);
        $config = $container->get('config');

        return new DeliveryOrderManager(
            $entityManager,
            $deliveryOrderDataValidator,
            $deliveryOrderDpdManager,
            $deliveryOrderDpdValidator,
            $deliveryDpdManager,
            $deliveryTrackingNumberManager,
            $messagePlugin,
            $config
        );
    }
}