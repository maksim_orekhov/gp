<?php
namespace ModuleDelivery\Service;

use ModuleDelivery\Entity\DeliveryServiceType;
use ModuleDelivery\Form\DeliveryOrderForm;

/**
 * Class DeliveryOrderManager
 * @package ModuleDelivery\Service
 */
class DeliveryOrderDataValidator
{
    /**
     * @var DeliveryOrderValidatorInterface|null
     */
    private $deliveryOrderValidator;

    /**
     * @param DeliveryOrderValidatorInterface $deliveryOrderValidator
     * @return void
     */
    public function setDeliveryOrderValidator(DeliveryOrderValidatorInterface $deliveryOrderValidator)
    {
        $this->deliveryOrderValidator = $deliveryOrderValidator;
    }

    /**
     * @param array $deliveryServiceTypes
     * @param array $data
     * @return array|null|\Traversable
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function validate(array $deliveryServiceTypes, array $data)
    {
        // Валидируем данные для DeliveryOrder
        /** @var DeliveryOrderForm $deliveryOrderForm */
        $deliveryOrderForm = new DeliveryOrderForm($deliveryServiceTypes);
        $deliveryOrderForm->setData($data);
        if (!$deliveryOrderForm->isValid()) {
            // Если первый уровень не проходит валидацию, сразу возвращаем сообщения валидации
            return $deliveryOrderForm->getMessages();
        }

        $validation_massages = null;

        if ($data['service_type'] !== DeliveryServiceType::NOT_REQUIRED && $data['service_type'] !== DeliveryServiceType::NO_SERVICE) {
            // Не установлен необходимый валидатор данных сервиса доставки
            // Exception, потому что в этом случае это ошибка разработчика
            if (null === $data['service']) {

                return ['service' => ['isEmpty' => 'Service data is required']];
            }

            $data['service']['csrf'] = $data['csrf'];
            // Валидация данных второго уровня (Данные для конкретного сервиса доставки. Например, DPD)
            $validation_massages = $this->deliveryOrderValidator->validate($data['service']);
        }

        return $validation_massages;
    }
}