<?php
namespace ModuleDelivery\Service;

use Application\Entity\Deal;
use Application\Entity\Payment;
use Core\Controller\Plugin\Message\MessagePlugin;
use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\Entity\DeliveryTrackingNumber;
use ModuleDeliveryDpd\Entity\DpdCity;
use ModuleDeliveryDpd\Entity\DpdDeliveryOrder;
use ModuleDelivery\Entity\DeliveryServiceType;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdManager;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdValidator;

/**
 * Class DeliveryOrderManager
 * @package ModuleDelivery\Service
 */
class DeliveryOrderManager
{
    const MAX_RECURSION_LEVEL = 20;

    const DELIVERY_SERVICE_TYPE_NAMES = [
        'no_service' => DeliveryServiceType::NO_SERVICE,
        'not_required' => DeliveryServiceType::NOT_REQUIRED,
        'dpd' => DeliveryServiceType::DPD,
    ];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var DeliveryOrderDataValidator
     */
    private $deliveryOrderDataValidator;

    /**
     * @var DeliveryOrderDpdManager
     */
    private $deliveryOrderDpdManager;

    /**
     * @var DeliveryOrderDpdValidator
     */
    private $deliveryOrderDpdValidator;

    /**
     * @var DeliveryDpdManager
     */
    private $deliveryDpdManager;

    /**
     * @var DeliveryTrackingNumberManager
     */
    private $deliveryTrackingNumberManager;

    /**
     * @var MessagePlugin
     */
    private $messagePlugin;

    /**
     * @var array
     */
    private $config;

    /**
     * DeliveryOrderManager constructor.
     * @param EntityManager $entityManager
     * @param DeliveryOrderDataValidator $deliveryOrderDataValidator
     * @param DeliveryOrderDpdManager $deliveryOrderDpdManager
     * @param DeliveryOrderDpdValidator $deliveryOrderDpdValidator
     * @param DeliveryDpdManager $deliveryDpdManager
     * @param DeliveryTrackingNumberManager $deliveryTrackingNumberManager
     * @param MessagePlugin $messagePlugin
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                DeliveryOrderDataValidator $deliveryOrderDataValidator,
                                DeliveryOrderDpdManager $deliveryOrderDpdManager,
                                DeliveryOrderDpdValidator $deliveryOrderDpdValidator,
                                DeliveryDpdManager $deliveryDpdManager,
                                DeliveryTrackingNumberManager $deliveryTrackingNumberManager,
                                MessagePlugin $messagePlugin,
                                array $config)
    {
        $this->entityManager                    = $entityManager;
        $this->deliveryOrderDataValidator       = $deliveryOrderDataValidator;
        $this->deliveryOrderDpdManager          = $deliveryOrderDpdManager;
        $this->deliveryOrderDpdValidator        = $deliveryOrderDpdValidator;
        $this->deliveryDpdManager               = $deliveryDpdManager;
        $this->deliveryTrackingNumberManager    = $deliveryTrackingNumberManager;
        $this->messagePlugin                    = $messagePlugin;
        $this->config = $config;
    }

    /**
     * @param string $name
     * @return DeliveryServiceType|null
     */
    public function getDeliveryServiceTypeByName(string $name)
    {
        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $this->entityManager->getRepository(DeliveryServiceType::class)
            ->findOneBy(['name' => trim($name)]);

        return $deliveryServiceType;
    }

    /**
     * @param int $id
     * @return DeliveryServiceType|null
     */
    public function getDeliveryServiceType($id)
    {
        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $this->entityManager->getRepository(DeliveryServiceType::class)
            ->findOneBy(['id' => (int) $id]);

        return $deliveryServiceType;
    }

    /**
     * @param $id
     * @return DpdCity|null
     */
    public function getDpdCityByCityId($id)
    {
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByCityId($id);

        return $city ?? null;
    }

    /**
     * @return array
     */
    public function getActiveDeliveryServiceTypes(): array
    {
        $deliveryServiceTypes = $this->entityManager->getRepository(DeliveryServiceType::class)
            ->findBy(['isActive' => true]);

        return $deliveryServiceTypes;
    }

    /**
     * @param int $id
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getDeliveryOrder(int $id)
    {
        return $this->entityManager->find(DeliveryOrder::class, $id);
    }

    /**
     * @param string $number
     * @return null|object
     */
    public function getDeliveryOrderByNumber(string $number)
    {
        return $this->entityManager->getRepository(DeliveryOrder::class)
            ->findOneBy(['orderNumber' => $number]);
    }

    /**
     * @param array $delivery_data
     * @param string|null $csrf
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function prepareDeliveryData(array $delivery_data, string $csrf = null): array
    {
        if (isset($delivery_data['service_type'])) {
            /** @var DeliveryServiceType $serviceType */
            $serviceType = $this->getDeliveryServiceType($delivery_data['service_type']);
            if (null !== $serviceType) {
                $service_type = $serviceType->getName();
            }

            $delivery_data['service_type'] = $service_type;

        } elseif (isset($delivery_data['id'])) {
            /** @var DeliveryOrder $deliveryOrder */
            $deliveryOrder = $this->getDeliveryOrder((int)$delivery_data['id']);
            $delivery_data['service_type'] = $deliveryOrder->getDeliveryServiceType()->getName();
        }

        if (null !== $csrf) {
            $delivery_data['csrf'] = $csrf;
        }

        return $delivery_data;
    }

    /**
     * @param array $data
     * @return array|null|\Traversable
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function validateDeliveryOrderData(array $data)
    {
        if (array_key_exists('service_type', $data)) {

            $deliveryOrderValidator = null;

            if ($data['service_type'] === self::DELIVERY_SERVICE_TYPE_NAMES['dpd']) {
                $deliveryOrderValidator = $this->deliveryOrderDpdValidator;
            }

            if ($deliveryOrderValidator instanceof DeliveryOrderValidatorInterface) {

                $this->deliveryOrderDataValidator->setDeliveryOrderValidator($deliveryOrderValidator);
            }
        }

        $deliveryServiceTypes = $this->getActiveDeliveryServiceTypes();

        return $this->deliveryOrderDataValidator->validate($deliveryServiceTypes, $data);
    }

    /**
     * @param Deal $deal
     * @param array $data
     * @return DeliveryOrder
     * @throws LogicException
     * @throws \Exception
     */
    public function createDeliveryOrder(Deal $deal, array $data): DeliveryOrder
    {
        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $this->getDeliveryServiceTypeByName($data['service_type']);

        $deliveryOrder = new DeliveryOrder();
        // Генерация уникального номера заказа c проверкой на уникальность
        $order_number = $this->generateUniqueOrderNumber();
        $deliveryOrder->setOrderNumber($order_number);
        $deliveryOrder->setCreated(new \DateTime());
        if (isset($data['terms_of_delivery'])) {
            $deliveryOrder->setTermsOfDelivery($data['terms_of_delivery']);
        }
        $deliveryOrder->setDeal($deal);
        $deliveryOrder->setDeliveryServiceType($deliveryServiceType);

        $this->entityManager->persist($deliveryOrder);
        $deal->setDeliveryOrder($deliveryOrder);
        $this->entityManager->persist($deal);
        $this->entityManager->flush();

        //создаем сервис доставки
        $this->createDeliveryService($deal, $deliveryOrder, $deliveryServiceType->getName(), $data);

        return $deliveryOrder;
    }

    /**
     * @param Deal $deal
     * @param DeliveryOrder $deliveryOrder
     * @param string $delivery_service_name
     * @param array $data
     * @throws \Exception
     */
    public function createDeliveryService(Deal $deal, DeliveryOrder $deliveryOrder, $delivery_service_name, array $data)
    {
        switch ($delivery_service_name) {
            case self::DELIVERY_SERVICE_TYPE_NAMES['dpd']:
                /** @var Payment $payment */
                $payment = $deal->getPayment();
                $data['service']['declared_value'] = $payment->getDealValue();
                /** @var DpdDeliveryOrder $deliveryOrderDpd */
                $deliveryOrderDpd = $this->deliveryOrderDpdManager->createDeliveryOrderDpd($data['service']);
                $deliveryOrderDpd->setDeliveryOrder($deliveryOrder);

                $deliveryOrder->setDpdDeliveryOrder($deliveryOrderDpd);
                $this->entityManager->persist($deliveryOrder);
                $this->entityManager->persist($deliveryOrderDpd);
                $this->entityManager->flush();
            break;
        }
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @param array $data
     * @throws \Exception
     */
    public function updateDeliveryOrder(DeliveryOrder $deliveryOrder, array $data)
    {
        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $this->getDeliveryServiceTypeByName($data['service_type']);
        /** @var DeliveryServiceType $oldDeliveryServiceType */
        $oldDeliveryServiceType = $deliveryOrder->getDeliveryServiceType();
        if (isset($data['terms_of_delivery'])) {
            $deliveryOrder->setTermsOfDelivery($data['terms_of_delivery']);
        }
        $deliveryOrder->setDeliveryServiceType($deliveryServiceType);
        $this->entityManager->persist($deliveryOrder);
        $this->entityManager->flush();

        ///// если тип доставки сменился удаляем старую доставку и создаем новую /////
        if ($oldDeliveryServiceType !== $deliveryServiceType) {
            // удаляем старую доставку
            $this->removeDeliveryService($deliveryOrder, $oldDeliveryServiceType->getName());
            //создаем сервис доставки
            $this->createDeliveryService($deliveryOrder->getDeal(), $deliveryOrder, $data['service_type'], $data);
        } else {
            $service = isset($data['service']) ? $data['service'] : null;
            ///// если тип доставки не сменился /////
            $this->updateDeliveryService($deliveryOrder, $data['service_type'], $service);
        }
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @param $delivery_service_name
     * @param $data
     * @throws \Exception
     */
    public function updateDeliveryService(DeliveryOrder $deliveryOrder, $delivery_service_name, $data)
    {
        switch ($delivery_service_name) {
            case self::DELIVERY_SERVICE_TYPE_NAMES['dpd']:
                /** @var DpdDeliveryOrder $deliveryOrderDpd */
                $deliveryOrderDpd = $deliveryOrder->getDpdDeliveryOrder();
                $deal = $deliveryOrder->getDeal();
                /** @var Payment $payment */
                $payment = $deal->getPayment();
                $data['service']['declared_value'] = $payment->getDealValue();
                if (null === $deliveryOrderDpd) {
                    //создаем сервис доставки
                    $this->createDeliveryService($deliveryOrder->getDeal(), $deliveryOrder, self::DELIVERY_SERVICE_TYPE_NAMES['dpd'], $data);
                } else {
                    // обновляем
                    $this->deliveryOrderDpdManager->updateDeliveryOrderDpd($deliveryOrderDpd, $data);
                }
            break;
        }
    }


    /**
     * @param DeliveryOrder $deliveryOrder
     * @param $delivery_service_name
     * @throws \Exception
     */
    public function removeDeliveryService(DeliveryOrder $deliveryOrder, $delivery_service_name)
    {
        switch ($delivery_service_name) {
            case self::DELIVERY_SERVICE_TYPE_NAMES['dpd']:
                /** @var DpdDeliveryOrder $dpdDeliveryOrder */
                $dpdDeliveryOrder = $deliveryOrder->getDpdDeliveryOrder();
                if ($dpdDeliveryOrder) {
                    $this->deliveryOrderDpdManager->removeDeliveryOrderDpd($dpdDeliveryOrder);
                }
            break;
        }
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @param array $postData
     * @return mixed|null
     * @throws \Throwable
     */
    public function getDeliveryCost(DeliveryOrder $deliveryOrder, array $postData)
    {
        $delivery_cost = null;

        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();

        try {
            if ($deliveryServiceType->getName() === self::DELIVERY_SERVICE_TYPE_NAMES['dpd']
                && null !== $deliveryOrder->getDpdDeliveryOrder()) {

                /** @var DpdDeliveryOrder $deliveryOrderDpd */
                $deliveryOrderDpd = $deliveryOrder->getDpdDeliveryOrder();
                $delivery_cost = $this->deliveryDpdManager->getServiceCost($deliveryOrderDpd, $postData);
            }
        }
        catch (\Throwable $t) {

            throw $t;
        }

        return $delivery_cost;
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @return array|null
     * @throws \Throwable
     */
    public function createDeliveryServiceOrder(DeliveryOrder $deliveryOrder)
    {
        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();

        try {
            if (null !== $deliveryOrder->getDpdDeliveryOrder() &&
                $deliveryServiceType->getName() === self::DELIVERY_SERVICE_TYPE_NAMES['dpd']) {

                /** @var DpdDeliveryOrder $deliveryOrderDpd */
                $deliveryOrderDpd = $deliveryOrder->getDpdDeliveryOrder();
                $result = $this->deliveryDpdManager->createServiceOrder($deliveryOrderDpd);

                if ($result['status'] !== 'OK') {

                    return ['status' => $result['status'], 'error_message' => $result['error_message']];
                }

                if (!isset($result['order_number'])) {
                    // Отправляем уведомление админу
                    $this->sendSpecialMail(
                        $deliveryOrder,
                        'У принятого заказа на доставку отсутствует номер в системе сервиса доставки (трек-номер)'
                    );

                    return ['status' => 'ERROR', 'error_message' => 'service order number missing'];
                }

                return ['status' => 'OK', 'error_message' => null];
            }
        }
        catch (\Throwable $t) {

            throw $t;
        }

        return null;
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @return mixed|null
     * @throws \Throwable
     */
    public function getCurrentServiceOrderCreationStatus(DeliveryOrder $deliveryOrder)
    {
        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();

        try {
            if ($deliveryServiceType->getName() === self::DELIVERY_SERVICE_TYPE_NAMES['dpd']
                && null !== $deliveryOrder->getDpdDeliveryOrder()) {

                /** @var DpdDeliveryOrder $deliveryOrderDpd */
                $deliveryOrderDpd = $deliveryOrder->getDpdDeliveryOrder();
                $status = $this->deliveryDpdManager->getCurrentDpdOrderCreationStatus($deliveryOrderDpd);

                return $status;
            }
        }
        catch (\Throwable $t) {

            throw $t;
        }

        return null;
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @return mixed|null
     * @throws \Throwable
     */
    public function updateServiceOrderCreationStatus(DeliveryOrder $deliveryOrder)
    {
        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();

        try {
            if ($deliveryServiceType->getName() === self::DELIVERY_SERVICE_TYPE_NAMES['dpd']
                && null !== $deliveryOrder->getDpdDeliveryOrder()) {

                /** @var DpdDeliveryOrder $deliveryOrderDpd */
                $deliveryOrderDpd = $deliveryOrder->getDpdDeliveryOrder();
                $status = $this->deliveryDpdManager->updateDpdOrderCreationStatus($deliveryOrderDpd);

//                if ($status['status'] === 'OK') {
//                    // Создаем трекер номер
//                    $this->createDeliveryTrackingNumber($deliveryOrder);
//                }

                return $status;
            }
        }
        catch (\Throwable $t) {

            throw $t;
        }

        return null;
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @return DeliveryTrackingNumber
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Throwable
     */
    public function createDeliveryTrackingNumber(DeliveryOrder $deliveryOrder): DeliveryTrackingNumber
    {
        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();

        $tracking_number = null;

        try {
            if ($deliveryServiceType->getName() === self::DELIVERY_SERVICE_TYPE_NAMES['dpd']
                && null !== $deliveryOrder->getDpdDeliveryOrder()) {

                /** @var DpdDeliveryOrder $deliveryOrderDpd */
                $deliveryOrderDpd = $deliveryOrder->getDpdDeliveryOrder();
                $tracking_number = $this->deliveryDpdManager->getIdentifierForTrackingNumber($deliveryOrderDpd);
            }

            if (null === $tracking_number) {

                throw new LogicException(null,
                    LogicException::DELIVERY_ORDER_NUMBER_RETURNS_NULL);
            }

            /** @var DeliveryTrackingNumber $deliveryTrackingNumber */
            $deliveryTrackingNumber = $this->deliveryTrackingNumberManager
                ->createDeliveryTrackingNumber($deliveryOrder, $tracking_number);
        }
        catch (\Throwable $t) {

            throw $t;
        }

        $deliveryOrder->addDeliveryTrackingNumber($deliveryTrackingNumber);

        $this->entityManager->persist($deliveryTrackingNumber);
        $this->entityManager->persist($deliveryOrder);
        $this->entityManager->flush();

        return $deliveryTrackingNumber;
    }

    /**
     * @return string|null
     */
    public function getServiceTracePageUrl(DeliveryOrder $deliveryOrder)
    {
        $trace_page_url = null;
        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();

        if ($deliveryServiceType->getName() === self::DELIVERY_SERVICE_TYPE_NAMES['dpd']
            && null !== $deliveryOrder->getDpdDeliveryOrder()) {

            $trace_page_url = $this->deliveryDpdManager->getServiceTracePageUrl();
        }

        return $trace_page_url;
    }

    // Outputs and private

    /**
     * @param array $deliveryServiceTypes
     * @return array
     */
    public function getDeliveryServiceTypesCollectionForOutput(array $deliveryServiceTypes): array
    {
        $deliveryServiceTypesOutput = [];
        foreach ($deliveryServiceTypes as $deliveryServiceType) {
            $deliveryServiceTypesOutput[$deliveryServiceType->getId()] = $this->getDeliveryServiceTypesForOutput($deliveryServiceType);
        }

        return $deliveryServiceTypesOutput;
    }

    /**
     * @param DeliveryServiceType $deliveryServiceType
     * @return array
     */
    public function getDeliveryServiceTypesForOutput(DeliveryServiceType $deliveryServiceType): array
    {
        $deliveryServiceTypeOutput = [];
        $deliveryServiceTypeOutput['id'] = $deliveryServiceType->getId();
        $deliveryServiceTypeOutput['name'] = $deliveryServiceType->getName();
        $deliveryServiceTypeOutput['name_for_output'] = $deliveryServiceType->getOutputName();
        $deliveryServiceTypeOutput['priority'] = $deliveryServiceType->getPriority();
        $deliveryServiceTypeOutput['description'] = $deliveryServiceType->getDescription();

        return $deliveryServiceTypeOutput;
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @return array
     */
    public function getDeliveryOrderOutput(DeliveryOrder $deliveryOrder): array
    {
        $deliveryOrderOutput = [];
        $deliveryOrderOutput['id'] = $deliveryOrder->getId();
        $deliveryOrderOutput['order_number'] = $deliveryOrder->getOrderNumber();
        $deliveryOrderOutput['created'] = $deliveryOrder->getCreated()->format('d.m.Y');
        $deliveryOrderOutput['terms_of_delivery'] = $deliveryOrder->getTermsOfDelivery();
        /** @var  DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();
        $deliveryOrderOutput['service_type'] = $deliveryServiceType ? $deliveryServiceType->getId() : null;
        $deliveryOrderOutput['service_type_name'] = $deliveryServiceType ? $deliveryServiceType->getName() : null;
        $deliveryOrderOutput['dpd_order'] = null;

        switch ($deliveryOrderOutput['service_type_name']){
            case DeliveryServiceType::NO_SERVICE:
                $deliveryOrderOutput['service'] = null;
                break;
            case DeliveryServiceType::NOT_REQUIRED:
                $deliveryOrderOutput['service'] = null;
                break;
            case DeliveryServiceType::DPD:
                $deliveryOrderOutput['service'] = null;
                $dpdDeliveryOrder = $deliveryOrder->getDpdDeliveryOrder();
                if (null !== $dpdDeliveryOrder) {
                    $deliveryOrderOutput['service'] = $this->deliveryOrderDpdManager
                        ->getDeliveryOrderDpdOutput($dpdDeliveryOrder);
                    $trace_page_url = array_key_exists('delivery_dpd_settings', $this->config) && array_key_exists('trace_page_url', $this->config['delivery_dpd_settings']) ? $this->config['delivery_dpd_settings']['trace_page_url'] : null;

                    $service = $deliveryOrderOutput['service'];
                    $deliveryOrderOutput['dpd_order']['trace_page_url'] = $trace_page_url;
                    $deliveryOrderOutput['dpd_order']['trace_page_url_full'] = $trace_page_url && $service['order_number']
                        ? $trace_page_url.'?orderNum='.$service['order_number']
                        : null;
                    $deliveryOrderOutput['dpd_order']['is_status_ok'] = $service
                        ? $service['dpd_order_creation_status'] === DpdDeliveryOrder::OK
                        : false;
                    $deliveryOrderOutput['dpd_order']['is_status_error'] = $service
                        ? $service['dpd_order_creation_status'] === DpdDeliveryOrder::ERROR
                        : false;
                    $deliveryOrderOutput['dpd_order']['is_status_pending'] = $service
                        ? $service['dpd_order_creation_status'] === DpdDeliveryOrder::PENDING
                        : false;
                    $deliveryOrderOutput['dpd_order']['is_status_duplicate'] = $service
                        ? $service['dpd_order_creation_status'] === DpdDeliveryOrder::DUPLICATE
                        : false;
                    $deliveryOrderOutput['dpd_order']['message'] = $service
                        ? $service['dpd_order_creation_error']
                        : null;
                    $deliveryOrderOutput['dpd_order']['status'] = $service && array_key_exists(
                        $service['dpd_order_creation_status'],
                        DeliveryDpdManager::DPD_ORDER_CREATION_STATUSES
                    )
                        ? DeliveryDpdManager::DPD_ORDER_CREATION_STATUSES[$service['dpd_order_creation_status']]
                        : null;
                }
                break;
            default:
                $deliveryOrderOutput['service'] = null;
        }

        return $deliveryOrderOutput;
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     * @param string $message
     * @throws \Exception
     */
    public function sendSpecialMail(DeliveryOrder $deliveryOrder, string $message)
    {
        /** @var DeliveryServiceType $deliveryServiceType */
        $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();
        $message .= '. Номер ' . $deliveryOrder->getOrderNumber() . '. Сервис '. $deliveryServiceType->getName() . '.';
        // Отправляем уведомление админу
        $this->messagePlugin->sendSpecialMail($message, null, 'Непредвиденная ошибка создания заказа на доставку');
    }

    /**
     * @param int $recursion_level
     * @return string
     * @throws LogicException
     */
    private function generateUniqueOrderNumber($recursion_level = 0): string
    {
        if ($recursion_level >= self::MAX_RECURSION_LEVEL) {

            throw new LogicException(null, LogicException::DELIVERY_GENERATION_UNIQUE_ORDER_NUMBER_FAILED);
        }

        $order_number = $this->generateRandOrderNumber();
        $deliveryOrder = $this->entityManager->getRepository(DeliveryOrder::class)
            ->findOneBy(['orderNumber' => $order_number]);

        if ($deliveryOrder !== null) {
            //// если DeliveryOrder с таким orderNumber существует запускаем повторную генерацию ////
            return $this->generateUniqueOrderNumber(++$recursion_level);
        }

        return $order_number;
    }

    /**
     * @return string
     */
    private function generateRandOrderNumber(): string
    {
        return uniqid('GP-', false);
    }

    /**
     * @param $receiverAddress
     * @return bool
     */
    public function isNotFillDpdAddress($receiverAddress):bool
    {
        return $this->deliveryDpdManager->isNotFillDpdAddress($receiverAddress);
    }

    /**
     * @param $parcels
     * @return bool
     */
    public function isNotFillDpdParcel($parcels):bool
    {
        return $this->deliveryDpdManager->isNotFillDpdParcel($parcels);
    }

    /**
     * @param $id
     * @return DeliveryOrder|null
     */
    public function getDeliveryOrderByAddressId($id)
    {
        /** @var DpdDeliveryOrder $dpdDeliveryOrder */
        $dpdDeliveryOrder = $this->deliveryDpdManager->getDpdDeliveryOrderByAddressId($id);

        return $dpdDeliveryOrder ? $dpdDeliveryOrder->getDeliveryOrder() : null;
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     */
    public function remove(DeliveryOrder $deliveryOrder)
    {
        if (null !== $deliveryOrder->getDpdDeliveryOrder()) {
            /** @var DpdDeliveryOrder $dpdDeliveryOrder */
            $dpdDeliveryOrder = $deliveryOrder->getDpdDeliveryOrder();
            $this->deliveryDpdManager->remove($dpdDeliveryOrder);
        }

        $this->entityManager->remove($deliveryOrder);
    }
}