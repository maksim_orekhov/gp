<?php
namespace ModuleDelivery\Service;

/**
 * Interface DeliveryOrderValidatorInterface
 * @package ModuleDelivery\Service
 */
interface DeliveryOrderValidatorInterface
{
    /**
     * @param array $data
     * @return array|null|\Traversable
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function validate(array $data);
}