<?php

namespace ModuleDelivery\Controller;

use Core\Form\CsrfForm;
use ModuleDelivery\Service\DeliveryOrderManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;

/**
 * Class DeliveryCostController
 * @package ModuleDelivery\Controller
 */
class DeliveryCostController extends AbstractRestfulController
{
    /**
     * @var DeliveryOrderManager
     */
    private $deliveryOrderManager;

    /**
     * DeliveryOrderController constructor.
     * @param DeliveryOrderManager $deliveryOrderManager
     */
    public function __construct(DeliveryOrderManager $deliveryOrderManager)
    {
        $this->deliveryOrderManager = $deliveryOrderManager;
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getCostAction()
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        // Проверяем, является ли пост POST-запросом.
        $postData = $this->collectIncomingData()['postData'];

        if (!$postData || !isset($postData['delivery_order_id'])) {

            return $this->message()->error('No data provided');
        }

        /** @var \ModuleDelivery\Entity\DeliveryOrder $deliveryOrder */
        $deliveryOrder = $this->deliveryOrderManager->getDeliveryOrder($postData['delivery_order_id']);

        if (null === $deliveryOrder) {

            return $this->message()->error('No deliveryOrder found');
        }

        try {
            $result = $this->deliveryOrderManager->getDeliveryCost($deliveryOrder, $postData);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        if (null !== $result) {
            // DPD возвращает результат как stbClass.
            // После появления второго сервиса ввести адаптер, который будет приводить данные к единому виду и формату
            $result->serviceType = $deliveryOrder->getDeliveryServiceType()->getName();
            return $this->message()->success('DeliveryCost', $result);
        }

        return $this->message()->error('No result returned', $result);
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $incomingData = $this->collectIncomingData();
        $queryData = $incomingData['queryData'];

        $queryData = [
            'csrf' => isset($queryData['csrf']) ? trim($queryData['csrf']) : null
        ];

        $csrfForm = new CsrfForm();
        $csrfForm->setData($queryData);
        if (!$csrfForm->isValid()) {

            return $this->message()->error($csrfForm->getMessages());
        }

        $formDataInit = $this->processFormDataInit();

        if (isset($formDataInit['delivery_service_types'])) {
            $formDataInit['delivery_service_types'] = $this->deliveryOrderManager
                ->getDeliveryServiceTypesCollectionForOutput($formDataInit['delivery_service_types']);
        }

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit(): array
    {
        $deliveryServiceTypes = $this->deliveryOrderManager->getActiveDeliveryServiceTypes();

        return [
            'delivery_service_types' => $deliveryServiceTypes,
        ];
    }
}