<?php
namespace ModuleDelivery\Controller\Factory;

use ModuleDelivery\Controller\DeliveryCostController;
use ModuleDelivery\Service\DeliveryOrderManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryCostControllerFactory
 * @package ModuleDelivery\Controller\Factory
 */
class DeliveryCostControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryCostController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $deliveryOrderManager = $container->get(DeliveryOrderManager::class);

        return new DeliveryCostController(
            $deliveryOrderManager
        );
    }
}