<?php
namespace ModuleDelivery\Controller\Factory;

use Application\Service\UserManager;
use Core\Service\Base\BaseAuthManager;
use ModuleDelivery\Controller\DeliveryOrderController;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use ModuleDelivery\Service\DeliveryOrderManager;
use Interop\Container\ContainerInterface;
use ModuleDelivery\Service\DeliveryOrderPolitics;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryOrderControllerFactory
 * @package ModuleDelivery\Controller\Factory
 */
class DeliveryOrderControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryOrderController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $deliveryOrderManager = $container->get(DeliveryOrderManager::class);
        $deliveryEventProvider = $container->get(DeliveryEventProvider::class);
        $deliveryOrderPolitics = $container->get(DeliveryOrderPolitics::class);

        return new DeliveryOrderController(
            $userManager,
            $baseAuthManager,
            $deliveryOrderManager,
            $deliveryEventProvider->getEventManager(),
            $deliveryOrderPolitics
        );
    }
}