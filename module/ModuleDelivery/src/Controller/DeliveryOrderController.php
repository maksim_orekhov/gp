<?php

namespace ModuleDelivery\Controller;

use Application\Entity\User;
use Application\Service\UserManager;
use Core\Exception\LogicException;
use Core\Form\CsrfForm;
use Core\Service\Base\BaseAuthManager;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\Entity\DeliveryServiceType;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use ModuleDelivery\Service\DeliveryOrderManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use ModuleDelivery\Service\DeliveryOrderPolitics;
use Zend\View\Model\ViewModel;
use Zend\EventManager\EventManager;

/**
 * Class DeliveryOrderController
 * @package ModuleDelivery\Controller
 */
class DeliveryOrderController extends AbstractRestfulController
{
    /**
     * User manager
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var DeliveryOrderManager
     */
    private $deliveryOrderManager;

    /**
     * @var EventManager
     */
    private $deliveryEventManager;

    /**
     * @var DeliveryOrderPolitics
     */
    private $deliveryOrderPolitics;

    /**
     * DeliveryOrderController constructor.
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param DeliveryOrderManager $deliveryOrderManager
     * @param EventManager $eventManager
     * @param DeliveryOrderPolitics $deliveryOrderPolitics
     */
    public function __construct(UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                DeliveryOrderManager $deliveryOrderManager,
                                EventManager $eventManager,
                                DeliveryOrderPolitics $deliveryOrderPolitics)
    {
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->deliveryOrderManager = $deliveryOrderManager;
        $this->deliveryEventManager = $eventManager;
        $this->deliveryOrderPolitics = $deliveryOrderPolitics;
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function update($id, $data)
    {
        //only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            $formDataInit = $this->processFormDataInit();

            if (!isset($formDataInit['user'])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            if ( !isset($formDataInit['delivery_order']) || !$formDataInit['delivery_order'] instanceof DeliveryOrder) {

                throw new LogicException(null, LogicException::DELIVERY_ORDER_NOT_FOUND);
            }

            if (!isset($formDataInit['user'])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            // @TODO Запрет редактирвоания после оплаты сделки

            /** @var User $user */
            $user = $formDataInit['user'];
            /** @var DeliveryOrder $deliveryOrder */
            $deliveryOrder = $formDataInit['delivery_order'];

            // Check permissions // Important! Only participants can edit Delivery order
            if (false === $this->access('deal.own.edit', ['user' => $user, 'delivery_order' => $deliveryOrder])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $delivery_data = $this->deliveryOrderManager->prepareDeliveryData($data);
            $validation_messages = $this->deliveryOrderManager->validateDeliveryOrderData($delivery_data);

            if (null === $validation_messages) {
                $this->deliveryOrderManager->updateDeliveryOrder($deliveryOrder, $delivery_data);
                $deliveryOrderOutput = $this->deliveryOrderManager->getDeliveryOrderOutput($deliveryOrder);

//                if ($this->deliveryOrderPolitics->isServiceOrderCreationAllowed($user, $deliveryOrder)) {
//                    $this->deliveryEventManager->trigger(
//                        DeliveryEventProvider::EVENT_CREATE_DELIVERY_SERVICE_ORDER, $this, [
//                        'create_delivery_service_order_params' => ['deliveryOrder' => $deliveryOrder]
//                    ]);
//                }

                return $this->message()->success('update deliveryOrder', $deliveryOrderOutput);
            }
            // Form is not valid
            return $this->message()->invalidFormData($validation_messages);
        }
        catch (\Throwable $t){

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $incomingData = $this->collectIncomingData();
        $queryData = $incomingData['queryData'];

        $queryData = [
            'csrf' => isset($queryData['csrf']) ? trim($queryData['csrf']) : null
        ];

        $csrfForm = new CsrfForm();
        $csrfForm->setData($queryData);
        if (!$csrfForm->isValid()) {

            return $this->message()->error($csrfForm->getMessages());
        }

        $formDataInit = $this->processFormDataInit();

        if (isset($formDataInit['delivery_service_types'])) {
            $formDataInit['delivery_service_types'] = $this->deliveryOrderManager
                ->getDeliveryServiceTypesCollectionForOutput($formDataInit['delivery_service_types']);
        }

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|ViewModel
     * @throws \Throwable
     */
    public function updateServiceOrderCreationStatusAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            $formDataInit = $this->processFormDataInit();

            if (!isset($formDataInit['user'])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            if (!isset($formDataInit['delivery_order']) || !$formDataInit['delivery_order'] instanceof DeliveryOrder) {

                throw new LogicException(null, LogicException::DELIVERY_ORDER_NOT_FOUND);
            }

            /** @var User $user */
            $user = $formDataInit['user'];
            /** @var DeliveryOrder $deliveryOrder */
            $deliveryOrder = $formDataInit['delivery_order'];

            // Check permissions // Important! Only participants can edit Delivery order
            if (false === $this->access('deal.own.edit', ['user' => $user, 'delivery_order' => $deliveryOrder])) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $current_service_order_creation_status = $this->deliveryOrderManager
                ->getCurrentServiceOrderCreationStatus($deliveryOrder);

            if ($current_service_order_creation_status['status'] === 'OK') {

                throw new LogicException(null, LogicException::DELIVERY_SERVICE_ORDER_CREATION_CURRENT_STATUS_IS_OK);
            }

            $new_service_order_creation_status = $this->deliveryOrderManager
                ->updateServiceOrderCreationStatus($formDataInit['delivery_order']);

            /** @var DeliveryServiceType $deliveryServiceType */
            $deliveryServiceType = $deliveryOrder->getDeliveryServiceType();

            $data =[
                'order_number' => $deliveryOrder->getOrderNumber(),
                'old_creation_status' => $current_service_order_creation_status,
                'new_creation_status' => $new_service_order_creation_status,
                'service_name' => $deliveryServiceType->getOutputName()
            ];

            if ($isAjax) {

                return $this->message()->success('Creation status', $data);
            }

            $view = new TwigViewModel($data);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);
            $view->setTemplate('module-delivery/delivery-order/service-order-creation-status');

            return $view;
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit(): array
    {
        $incomingData = $this->collectIncomingData();

        $user = null;
        if (null !== $this->baseAuthManager->getIdentity()) {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        }

        //process data for update
        $token = null;
        if (isset($incomingData['idDeliveryOrder']) || isset($incomingData['id'])) {
            $id = $incomingData['id'] ?? $incomingData['idDeliveryOrder'];
            /** @var DeliveryOrder $deliveryOrder */
            $deliveryOrder = $this->deliveryOrderManager->getDeliveryOrder((int) $id);
        }

        #$deliveryServiceTypes = $this->deliveryOrderManager->getActiveDeliveryServiceTypes();

        return [
            'user' => $user,
            'delivery_order' => $deliveryOrder,
            #'delivery_service_types' => $deliveryServiceTypes,
        ];
    }
}