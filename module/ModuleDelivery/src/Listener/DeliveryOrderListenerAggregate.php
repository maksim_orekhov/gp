<?php
namespace ModuleDelivery\Listener;

use Application\Entity\Deal;
use Core\Exception\LogicException;
use Core\Listener\ListenerAggregateTrait;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\Entity\DeliveryTrackingNumber;
use ModuleDelivery\EventManager\DeliveryEventProvider;
use ModuleDelivery\Service\DeliveryOrderManager;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManager;

/**
 * Class DeliveryOrderListenerAggregate
 * @package ModuleDelivery\Listener
 */
class DeliveryOrderListenerAggregate implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /**
     * @var DeliveryOrderManager
     */
    private $deliveryOrderManager;

    /**
     * @var EventManager
     */
    private $deliveryEventManager;

    /**
     * DeliveryOrderListenerAggregate constructor.
     * @param DeliveryOrderManager $deliveryOrderManager
     * @param EventManager $eventManager
     */
    public function __construct(DeliveryOrderManager $deliveryOrderManager,
                                EventManager $eventManager)
    {
        $this->deliveryOrderManager = $deliveryOrderManager;
        $this->deliveryEventManager = $eventManager;
    }

    /**
     * @param EventManagerInterface $events
     * @param int $priority
     * @return void
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        // Событие Создание DeliveryOrder в нашей системе
        $this->listeners[] = $events->attach(
            DeliveryEventProvider::EVENT_CREATE_DELIVERY_ORDER,
            [$this, 'createDeliveryOrder'],
            $priority
        );
        // Событие Создание DeliveryOrder в нашей системе
        $this->listeners[] = $events->attach(
            DeliveryEventProvider::EVENT_EDIT_DELIVERY_ORDER,
            [$this, 'editDeliveryOrder'],
            $priority
        );
        // Событие Создание DeliveryOrder в системе конкретного сервиса доставки
        $this->listeners[] = $events->attach(
            DeliveryEventProvider::EVENT_CREATE_DELIVERY_SERVICE_ORDER,
            [$this, 'createDeliveryServiceOrder'],
            $priority
        );
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     * @return void
     */
    public function createDeliveryOrder(EventInterface $event)
    {
        $params = $event->getParam('create_delivery_order_params', null);
        $event->setParam('create_delivery_order_params', null);

        if (!\is_array($params) ||
            !array_key_exists('deal', $params) ||
            !array_key_exists('delivery_data', $params)) {

            return;
        }

        /** @var Deal $deal */
        $deal = $params['deal'];
        /** @var array $delivery_data */
        $delivery_data = $params['delivery_data'];

        if (null === $delivery_data || empty($delivery_data)) {

            throw new LogicException(null, LogicException::DELIVERY_ORDER_NO_DATA_PROVIDED_FOR_CREATION);
        }

        try {
            // Создаем DeliveryOrder
            $this->deliveryOrderManager->createDeliveryOrder($deal, $delivery_data);
        }
        catch (\Throwable $t) {

            logMessage($t->getMessage(), 'delivery-errors');

            throw new LogicException(null, LogicException::DELIVERY_ORDER_NOT_CREATED);
        }
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     * @return void
     */
    public function editDeliveryOrder(EventInterface $event)
    {
        $params = $event->getParam('edit_delivery_order_params', null);
        $event->setParam('edit_delivery_order_params', null);

        if (!\is_array($params) ||
            !array_key_exists('delivery_order', $params) ||
            !array_key_exists('delivery_data', $params)) {

            return;
        }

        /** @var DeliveryOrder $deliveryOrder */
        $deliveryOrder = $params['delivery_order'];
        /** @var array $delivery_data */
        $delivery_data = $params['delivery_data'];

        if (! $deliveryOrder instanceof DeliveryOrder || null === $delivery_data || empty($delivery_data)) {

            throw new LogicException(null, LogicException::DELIVERY_ORDER_NO_DATA_PROVIDED_FOR_CREATION);
        }

        try {
            // Редактируем DeliveryOrder
            $this->deliveryOrderManager->updateDeliveryOrder($deliveryOrder, $delivery_data);
        }
        catch (\Throwable $t) {

            logMessage($t->getMessage(), 'delivery-errors');

            throw new LogicException(null, LogicException::DELIVERY_ORDER_NOT_CREATED);
        }
    }

    /**
     * @param EventInterface $event
     * @throws \Exception
     * @return void
     */
    public function createDeliveryServiceOrder(EventInterface $event)
    {
        $params = $event->getParam('create_delivery_service_order_params', null);
        $event->setParam('create_delivery_service_order_params', null);

        if (!\is_array($params) || !array_key_exists('deliveryOrder', $params)) {

            return;
        }

        /** @var DeliveryOrder $deliveryOrder */
        $deliveryOrder = $params['deliveryOrder'];

        if (null === $deliveryOrder || !$deliveryOrder instanceof DeliveryOrder) {

            throw new LogicException(null, LogicException::DELIVERY_SERVICE_ORDER_NO_DATA_PROVIDED_FOR_CREATION);
        }

        try {
            $result = $this->deliveryOrderManager->createDeliveryServiceOrder($deliveryOrder);

            if (isset($result['status']) && $result['status'] === 'OK') {
                /** @var DeliveryTrackingNumber $deliveryTrackingNumber */
                $deliveryTrackingNumber = $this->deliveryOrderManager->createDeliveryTrackingNumber($deliveryOrder);
                // Тригерим событие, которые будет слушает слушатель в модуле бизнес-логики(Application) и рассылает уведомления
                $this->deliveryEventManager->trigger(
                    DeliveryEventProvider::EVENT_DELIVERY_SERVICE_ORDER_CREATED, $this, [
                    'delivery_service_order_created' => [
                        'deliveryTrackingNumber' => $deliveryTrackingNumber,
                        'trace_page_url' => $this->deliveryOrderManager->getServiceTracePageUrl($deliveryOrder)
                    ]
                ]);
            }
        }
        catch (\Throwable $t) {

            logException($t);

            throw new LogicException(null, LogicException::DELIVERY_ORDER_NOT_CREATED);
        }
    }
}