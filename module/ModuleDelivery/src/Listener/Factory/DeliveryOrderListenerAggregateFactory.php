<?php
namespace ModuleDelivery\Listener\Factory;

use ModuleDelivery\EventManager\DeliveryEventProvider;
use ModuleDelivery\Listener\DeliveryOrderListenerAggregate;
use Interop\Container\ContainerInterface;
use ModuleDelivery\Service\DeliveryOrderManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryOrderListenerAggregateFactory
 * @package ModuleDelivery\Listener\Factory
 */
class DeliveryOrderListenerAggregateFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $deliveryOrderManager = $container->get(DeliveryOrderManager::class);
        $deliveryEventProvider = $container->get(DeliveryEventProvider::class);

        return new DeliveryOrderListenerAggregate(
            $deliveryOrderManager,
            $deliveryEventProvider->getEventManager()
        );
    }
}