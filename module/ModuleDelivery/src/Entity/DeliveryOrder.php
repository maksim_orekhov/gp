<?php

namespace ModuleDelivery\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use ModuleDeliveryDpd\Entity\DpdDeliveryOrder;

/**
 * DeliveryOrder
 *
 * @ORM\Table(name="delivery_order")
 * @ORM\Entity()
 */
class DeliveryOrder
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="order_number", type="string", length=45, nullable=false, unique=true)
     */
    private $orderNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false, unique=false)
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="terms_of_delivery", type="text", length=2000, nullable=true)
     */
    private $termsOfDelivery;

    /**
     * One DeliveryOrder has One Deal.
     * @ORM\OneToOne(targetEntity="Application\Entity\Deal", inversedBy="deliveryOrder")
     * @ORM\JoinColumn(name="deal_id", referencedColumnName="id")
     */
    private $deal;

    /**
     * @ORM\ManyToOne(targetEntity="DeliveryServiceType")
     * @ORM\JoinColumn(name="delivery_service_type_id", referencedColumnName="id")
     */
    private $deliveryServiceType;

    /**
     * @var DpdDeliveryOrder
     *
     * @ORM\OneToOne(targetEntity="ModuleDeliveryDpd\Entity\DpdDeliveryOrder", mappedBy="deliveryOrder", cascade={"persist", "remove"})
     */
    private $dpdDeliveryOrder;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="DeliveryTrackingNumber", mappedBy="deliveryOrder")
     */
    private $deliveryTrackingNumbers;

    /**
     * Deal constructor.
     */
    public function __construct()
    {
        $this->deliveryTrackingNumbers = new ArrayCollection;
    }

    public function __toString()
    {
        return $this->orderNumber;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     */
    public function setOrderNumber(string $orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return string|null
     */
    public function getTermsOfDelivery()
    {
        return $this->termsOfDelivery;
    }

    /**
     * @param string|null $termsOfDelivery
     */
    public function setTermsOfDelivery($termsOfDelivery)
    {
        $this->termsOfDelivery = $termsOfDelivery;
    }

    /**
     * @return mixed
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * @param mixed $deal
     */
    public function setDeal($deal)
    {
        $this->deal = $deal;
    }

    /**
     * @return mixed
     */
    public function getDeliveryServiceType()
    {
        return $this->deliveryServiceType;
    }

    /**
     * @param mixed $deliveryServiceType
     */
    public function setDeliveryServiceType($deliveryServiceType)
    {
        $this->deliveryServiceType = $deliveryServiceType;
    }

    /**
     * @return DpdDeliveryOrder|null
     */
    public function getDpdDeliveryOrder()
    {
        return $this->dpdDeliveryOrder;
    }

    /**
     * @param DpdDeliveryOrder|null $dpdDeliveryOrder
     */
    public function setDpdDeliveryOrder($dpdDeliveryOrder)
    {
        $this->dpdDeliveryOrder = $dpdDeliveryOrder;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDeliveryTrackingNumbers(): \Doctrine\Common\Collections\Collection
    {
        return $this->deliveryTrackingNumbers;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $deliveryTrackingNumbers
     */
    public function setDeliveryTrackingNumbers(\Doctrine\Common\Collections\Collection $deliveryTrackingNumbers)
    {
        $this->deliveryTrackingNumbers = $deliveryTrackingNumbers;
    }

    /**
     * @param DeliveryTrackingNumber $deliveryTrackingNumber
     */
    public function addDeliveryTrackingNumber(DeliveryTrackingNumber $deliveryTrackingNumber)
    {
        $this->deliveryTrackingNumbers->add($deliveryTrackingNumber);
    }
}

