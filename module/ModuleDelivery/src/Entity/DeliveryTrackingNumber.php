<?php

namespace ModuleDelivery\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DeliveryTrackingNumber
 *
 * @ORM\Table(name="delivery_tracking_number")
 * @ORM\Entity()
 */
class DeliveryTrackingNumber
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true, unique=false)
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="tracking_number", type="string", length=255, nullable=false, unique=true)
     */
    private $trackingNumber;

    /**
     * @var DeliveryOrder
     *
     * @ORM\ManyToOne(targetEntity="DeliveryOrder", inversedBy="deliveryTrackingNumbers", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="delivery_order_id", referencedColumnName="id")
     * })
     */
    private $deliveryOrder;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getTrackingNumber(): string
    {
        return $this->trackingNumber;
    }

    /**
     * @param string $trackingNumber
     */
    public function setTrackingNumber(string $trackingNumber)
    {
        $this->trackingNumber = $trackingNumber;
    }

    /**
     * @return DeliveryOrder
     */
    public function getDeliveryOrder(): DeliveryOrder
    {
        return $this->deliveryOrder;
    }

    /**
     * @param DeliveryOrder $deliveryOrder
     */
    public function setDeliveryOrder(DeliveryOrder $deliveryOrder)
    {
        $this->deliveryOrder = $deliveryOrder;
    }
}

