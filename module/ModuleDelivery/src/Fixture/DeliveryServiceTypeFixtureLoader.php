<?php

namespace ModuleDelivery\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use ModuleDelivery\Entity\DeliveryServiceType;

/**
 * Class DeliveryServiceTypeFixtureLoader
 * @package ModuleDelivery\Fixture
 */
class DeliveryServiceTypeFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const SERVICE_TYPES = [
        [
            'name' => DeliveryServiceType::NOT_REQUIRED,
            'output_name' => 'Доставка не требуется',
            'priority' => 10,
            'is_active' => true,
        ],
        [
            'name' => DeliveryServiceType::NO_SERVICE,
            'output_name' => 'Собственная доставка',
            'priority' => 1,
            'is_active' => true,
        ],
        [
            'name' => DeliveryServiceType::DPD,
            'output_name' => 'DPD доставка',
            'priority' => 5,
            'is_active' => true,
        ],
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        foreach (self::SERVICE_TYPES as $service_type) {
            $dpdServiceType = new DeliveryServiceType();
            $dpdServiceType->setName($service_type['name']);
            $dpdServiceType->setOutputName($service_type['output_name']);
            $dpdServiceType->setPriority($service_type['priority']);
            $dpdServiceType->setIsActive(true);

            $entityManager->persist($dpdServiceType);
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 180;
    }
}