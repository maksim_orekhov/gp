<?php
namespace ModuleDelivery\Form;

use ModuleDelivery\Entity\DeliveryServiceType;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class DeliveryOrderForm
 * @package ModuleDelivery\Form
 */
class DeliveryOrderForm extends Form
{
    const TYPE_FORM = 'delivery-order-form';

    /**
     * @var array
     */
    private $deliveryServiceTypes;

    /**
     * DeliveryOrderForm constructor.
     * @param array $deliveryServiceTypes
     */
    public function __construct(array $deliveryServiceTypes)
    {
        $this->deliveryServiceTypes = $deliveryServiceTypes;

        // Define form name
        parent::__construct(self::TYPE_FORM);

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return array
     */
    public function getOptionsForServiceType(): array
    {
        $service_type_array = [];

        /** @var DeliveryServiceType $serviceType */
        foreach ($this->deliveryServiceTypes as $serviceType) {
            $service_type_array[$serviceType->getName()] = $serviceType->getOutputName();
        }

        return $service_type_array;
    }

    protected function addElements()
    {
        $this->add([
            'type'  => Element\Textarea::class,
            'name' => 'terms_of_delivery',
            'attributes' => [
                'id' => 'terms_of_delivery',
                'class'=>'form-control',
                'placeholder'=>'Условия доставки'
            ],
            'options' => [
                'label' => 'Условия доставки',
            ],
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'service_type',
            'attributes' => [
                'id' => 'service_type',
                'placeholder'=>'Тип сервиса'
            ],
            'options' => array(
                'label' => 'Тип сервиса',
                'empty_option' => 'Выберите тип сервиса',
                'value_options' => $this->getOptionsForServiceType()
            )
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'class'=>'ButtonApply'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'terms_of_delivery',
            'required' => false,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags']
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 5,
                        'max' => 2000
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'service_type',
            'required' => true,
            'validators' => [
            ],
        ]);
    }
}