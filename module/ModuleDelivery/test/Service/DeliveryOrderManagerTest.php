<?php

namespace ModuleDeliveryTest\Service;

use ApplicationTest\Bootstrap;
use CoreTest\CallPrivateMethodTrait;
use ModuleDelivery\Service\DeliveryOrderManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class DeliveryOrderManagerTest
 * @package ModuleDeliveryDpdTest\Service
 */
class DeliveryOrderManagerTest extends AbstractHttpControllerTestCase
{
    use CallPrivateMethodTrait;

    /**
     * @var DeliveryOrderManager;
     */
    private $deliveryOrderManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->deliveryOrderManager = $serviceManager->get(DeliveryOrderManager::class);
    }



    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     *
     * @throws \Exception
     */
    public function testGenerateUniqueOrderNumber()
    {
        $order_number = $this->invokeMethod(
            $this->deliveryOrderManager,
            'generateUniqueOrderNumber'
        );

        $this->assertNotNull($order_number);
        $this->assertGreaterThan(5, \strlen($order_number));
    }
}