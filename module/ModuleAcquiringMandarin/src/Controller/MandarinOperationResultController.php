<?php

namespace ModuleAcquiringMandarin\Controller;

use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\User;
use Application\Service\Deal\DealManager;
use Application\Service\InvoiceManager;
use Application\Service\NdsTypeManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class MandarinOperationResultController
 * @package ModuleAcquiringMandarin\Controller
 */
class MandarinOperationResultController extends AbstractRestfulController
{
    const ERROR_INVALID_DATA = 'Invalid data';

    /**
     * @var dealManager
     */
    private $dealManager;

    /**
     * @var MandarinPayManager
     */
    private $mandarinPayManager;

    /**
     * @var MandarinPaymentOrderManager
     */
    private $mandarinPaymentOrderManager;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var InvoiceManager
     */
    private $invoiceManager;


    /**
     * MandarinOperationResultController constructor.
     * @param DealManager $dealManager
     * @param MandarinPayManager $mandarinPayManager
     * @param MandarinPaymentOrderManager $mandarinPaymentOrderManager
     * @param NdsTypeManager $ndsTypeManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param RbacManager $rbacManager
     * @param InvoiceManager $invoiceManager
     */
    public function __construct(DealManager $dealManager,
                                MandarinPayManager $mandarinPayManager,
                                MandarinPaymentOrderManager $mandarinPaymentOrderManager,
                                NdsTypeManager $ndsTypeManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                RbacManager $rbacManager,
                                InvoiceManager $invoiceManager)
    {
        $this->dealManager = $dealManager;
        $this->mandarinPayManager = $mandarinPayManager;
        $this->mandarinPaymentOrderManager = $mandarinPaymentOrderManager;
        $this->ndsTypeManager = $ndsTypeManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->rbacManager = $rbacManager;
        $this->invoiceManager = $invoiceManager;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     *
     * На вход получаем:
     * 'id' => string 'be96dbbd9fdf48c6b9b659fa4d804dac'
     * 'status' => string 'success' (length=7)
     * 'orderId' => string 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9....'
     * 'type' => string 'pay'
     */
    public function resultAction()
    {
        try {
            $incoming_data = $this->processFormDataInit();

            if (!isset($incoming_data['status']) || $incoming_data['status'] !== 'success') {

                throw new \Exception('Operation failed');
            }

            if (!isset($incoming_data['type']) || $incoming_data['type'] !== 'pay') {

                // @TODO Что делать, если type не 'pay'?
            }

            /** @var Deal|null $deal */
            $deal = $incoming_data['deal'];

            if (null === $deal) {

                throw new \Exception('Operation failed');
            }

            /** @var User $user */
            $user = $incoming_data['user'];

            if ($user !== $deal->getCustomer()->getUser()) {

                return $this->message()->error('Forbidden create PayOff for deal ' . $deal->getNumber());
            }

            if ($deal->status['status'] !== 'confirmed') {

                return $this->message()->error('По сделке ' . $deal->getNumber() . ' платежи не ожидаются');
            }

            /** @var Payment $payment */
            $payment = $deal->getPayment();

            // Проверка наличия платёжки с $incoming_data['orderId'] в системе
            /** @var MandarinPaymentOrder $mandarinPaymentOrder */
            $mandarinPaymentOrder = $this->mandarinPaymentOrderManager
                ->getIncomingMandarinPaymentOrderByOrderId($incoming_data['orderId']);

            // Если платёжка есть, ничего не делаем
            if (null !== $mandarinPaymentOrder) {

                logMessage('MandarinPaymentOrder with id ' . $mandarinPaymentOrder->getId() . ' already created by CallbackHandleController','acquiring-mandarin');

            } else {

                $payment_purpose = $this->invoiceManager->generateInvoicePaymentPurpose($deal, true);

                $data = [
                    'order_id' => $incoming_data['orderId'],
                    'price' => $payment->getExpectedValue(),
                    'payment_purpose' => $payment_purpose,
                ];

                // ... и создаем MandarinPaymentOrder

                // @TODO Добавить проверку, что сделка ожидает входящий платёж?
                // Create MandarinPaymentOrder
                $mandarinPaymentOrder = $this->mandarinPaymentOrderManager->createMandarinPaymentOrder(
                    $deal,
                    $data,
                    PaymentOrderManager::TYPE_INCOMING
                );

                logMessage('mandarinPaymentOrder created by MandarinOperationResultController','acquiring-mandarin');
            }

            return $this->redirect()->toRoute('deal/show', ['id' => $deal->getId()]);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        // До этого момента не дойдет при success и failed

        $dealOutput = $this->dealManager->getDealOutputForSingle($deal);

        $view = new TwigViewModel([
            'deal'    => $dealOutput,
            'status'  => $incoming_data['status'],
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return array
     * @throws \Exception
     *
     * В случае успешной операции по карте:
     * GET параметры status=success&orderId=496-1528111587&type=pay
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        if (!array_key_exists('orderId', $incomingData) || null === $incomingData['orderId']) {

            throw new \Exception('No orderId given');
        }

        $deal = null;

        // Get current user // Can throw Exception
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        // Декодируем orderId
        $decoded_orderId = $this->mandarinPayManager->decodeEncodedOrderId($incomingData['orderId']);
        /** @var $deal $deal */ // Can throw Exception
        $deal = $this->dealManager->getDealById((int)$decoded_orderId->deal_id);
        // Check if $deal belongs to current user
        if ($deal && !$this->dealManager->isUserPartOfDial($deal, $user) && !$this->rbacManager->hasRole(null, 'Operator')) {
            $deal = null;
        }

        return [
            'deal' => $deal,
            'status' => $incomingData['status'],
            'orderId' => $incomingData['orderId'],
            'type' => $incomingData['type'],
            'user' => $user
        ];
    }
}