<?php

namespace ModuleAcquiringMandarin\Controller;

use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\User;
use Application\Service\Deal\DealManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModuleRbac\Service\RbacManager;

/**
 * Class MandarinOperationStatusController
 * @package ModuleAcquiringMandarin\Controller
 */
class MandarinOperationStatusController extends AbstractRestfulController
{
    const ERROR_INVALID_DATA = 'Invalid data';
    const ERROR_ACCESS_DENIED = 'ERROR_ACCESS_DENIED';
    /**
     * @var dealManager
     */
    private $dealManager;

    /**
     * @var MandarinPayManager
     */
    private $mandarinPayManager;

    /**
     * @var MandarinPaymentOrderManager
     */
    private $mandarinPaymentOrderManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;
    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * MandarinOperationStatusController constructor.
     * @param DealManager $dealManager
     * @param MandarinPayManager $mandarinPayManager
     * @param MandarinPaymentOrderManager $mandarinPaymentOrderManager
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     */
    public function __construct(DealManager $dealManager,
                                MandarinPayManager $mandarinPayManager,
                                MandarinPaymentOrderManager $mandarinPaymentOrderManager,
                                UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                RbacManager $rbacManager)
    {
        $this->dealManager = $dealManager;
        $this->mandarinPayManager = $mandarinPayManager;
        $this->mandarinPaymentOrderManager = $mandarinPaymentOrderManager;
        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->rbacManager = $rbacManager;
    }

    /**
     * @return TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     *
     * На вход
     * POST orderId=496-1528111587
     */
    public function checkAction()
    {
        $isAjax = $this->getRequest()->isXmlHttpRequest();

        try {
            /** @var array $incoming_data */
            $incoming_data = $this->processFormDataInit();
        } catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }

        if (null === $incoming_data['deal']) {

            throw new \Exception('Operation failed');
        }

        /** @var Deal|null $deal */
        $deal = $incoming_data['deal'];
        /** @var User $user */
        $user = $incoming_data['user'];

        if ($user !== $deal->getCustomer()->getUser()) {

            return $this->message()->error('Forbidden check MandarinPaymentOrder status for this orderId');
        }

        // Достаем POST-дата
        $postData = $this->collectIncomingData()['postData'];

        if ($postData) {
            /** @var MandarinPaymentOrder $mandarinPaymentOrder */
            $mandarinPaymentOrder = $this->mandarinPaymentOrderManager
                ->getIncomingMandarinPaymentOrderByOrderId($postData['orderId']);

            if (null === $mandarinPaymentOrder) {

                throw new \Exception('No MandarinPaymentOrder found for this orderId');
            }

            // Ajax
            if ($isAjax) {
                return $this->message()->success('MandarinPaymentOrder status',
                    ['status' => $mandarinPaymentOrder->getStatus()]);
            }

            /** @var array $mandarinPaymentOrderOutput */
            $mandarinPaymentOrderOutput = MandarinPaymentOrderManager::getMandarinPaymentOrderOutput($mandarinPaymentOrder);

            // Html -
            $view = new TwigViewModel([
                'mandarinPaymentOrder' => $mandarinPaymentOrderOutput
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

            return $view;
        }

        return $this->message()->error('No Post data given');
    }

    /**
     * @return array
     * @throws \Exception
     *
     * На вход
     * POST orderId=496-1528111587
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        if (!array_key_exists('orderId', $incomingData) || null === $incomingData['orderId']) {

            throw new \Exception('No orderId given');
        }

        $deal = null;

        // Get current user // Can throw Exception
        $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
        // Декодируем orderId
        $decoded_orderId = $this->mandarinPayManager->decodeEncodedOrderId($incomingData['orderId']);
        /** @var $deal $deal */ // Can throw Exception
        $deal = $this->dealManager->getDealById((int)$decoded_orderId->deal_id);
        // Check if $deal belongs to current user
        if ($deal && !$this->dealManager->isUserPartOfDial($deal, $user) && !$this->rbacManager->hasRole(null, 'Operator')) {
            $deal = null;
        }

        return [
            'deal' => $deal,
            'orderId' => $incomingData['orderId'],
            'user' => $user
        ];
    }

    /**
     * @return TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel|array
     * @throws \Exception
     *
     * На вход
     * POST
     */
    public function orderIdAction()
    {

        if (!$this->getRequest()->isXmlHttpRequest()) {

            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }
        $postData = $this->collectIncomingData()['postData']['id'];
        if (!$postData || !preg_match('/^[1-9][0-9]*$/', $postData)) {
            return $this->message()->error('not found deal');
        }
        /** @var User $user */ // Can throw Exception
        $login = $this->baseAuthManager->getIdentity();
       // $login = 'test';
        $user = $this->userManager->getUserByLogin($login);
        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole($user, 'Operator');
        /** @var Deal $deal */ // Can throe Exception
        $deal = $this->dealManager->getDealById($postData);
        // Check permissions only for customer
        $isCustomer=($deal->getCustomer()->getUser()->getLogin()==$login) ? true :false ;
        if (!$this->access('deal.own.view', ['user' => $user, 'deal' => $deal]) && !$isOperator && $isCustomer) {
            return $this->message()->error(self::ERROR_ACCESS_DENIED);
        }

        try {
            $payment = $this->mandarinPaymentOrderManager
                ->getMandarinOrderByDeal($deal);
            /** @var Payment $paymentOrder */
            $paymentOrders = $payment->getPaymentOrders();
            $mandarinPaymentOrder = $this->mandarinPaymentOrderManager
                ->getMandarinOrderById($paymentOrders);
            $mandarinPaymentOrder = $mandarinPaymentOrder->getOrderId();
            return $this->message()->success('success', [
                'orderId' => $mandarinPaymentOrder,
                'userLogin' => $login
            ]);

        } catch (\Throwable $t) {
            return $this->message()->error(self::ERROR_INVALID_DATA);
        }
        return false;
    }
}