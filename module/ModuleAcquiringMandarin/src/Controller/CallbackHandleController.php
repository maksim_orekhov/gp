<?php

namespace ModuleAcquiringMandarin\Controller;

use Application\Entity\Deal;
use Application\Service\Deal\DealManager;
use Application\Service\InvoiceManager;
use Core\Controller\AbstractRestfulController;
use ModuleAcquiringMandarin\Service\CallbackHandlePolitics;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\View\Model\JsonModel;

/**
 * Class CallbackHandleController
 * @package ModuleAcquiringMandarin\Controller
 */
class CallbackHandleController extends AbstractRestfulController
{
    const ERROR_INVALID_DATA = 'Invalid data';
    const LOG_FILE_NAME = 'acquiring-mandarin';
    const ACTION_FOR_PAYMENT = 'pay';

    /**
     * @var dealManager
     */
    private $dealManager;

    /**
     * @var MandarinPaymentOrderManager
     */
    private $mandarinPaymentOrderManager;

    /**
     * @var CallbackHandlePolitics
     */
    private $callbackHandlePolitics;

    /**
     * @var InvoiceManager
     */
    private $invoiceManager;


    /**
     * CallbackHandleController constructor.
     * @param DealManager $dealManager
     * @param MandarinPaymentOrderManager $mandarinPaymentOrderManager
     * @param CallbackHandlePolitics $callbackHandlePolitics
     */
    public function __construct(DealManager $dealManager,
                                MandarinPaymentOrderManager $mandarinPaymentOrderManager,
                                CallbackHandlePolitics $callbackHandlePolitics,
                                InvoiceManager $invoiceManager)
    {
        $this->dealManager  = $dealManager;
        $this->mandarinPaymentOrderManager = $mandarinPaymentOrderManager;
        $this->callbackHandlePolitics = $callbackHandlePolitics;
        $this->invoiceManager = $invoiceManager;
    }

//    public function getList()
//    {
//    }

//    public function get($id)
//    {
//    }

//    public function create($data)
//    {
//    }

//    public function update($id, $data)
//    {
//    }

//    public function delete($id)
//    {
//    }

    /**
     * @return JsonModel
     * @throws \Exception
     *
     * Асинхронный callback от Мандарина:
     * merchantId => 940
     * orderId => eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZWFsX2lkIjo0OTYG9uI309dCPrc2PMIlxc_KTpBVAZ_Va_Yxl-qc.....
     * email => test@simple-technology.ru
     * orderActualTill => 2018-06-08 16:26:14Z
     * price => 102000
     * callbackUrl => http://g-pay.org/mandarin/callback/deal/pay
     * returnUrl => http://g-pay.org/mandarin/operation-result
     * action => pay
     * customName0 => Назначение платежа
     * customValue0 => Оплата по сделке #GP496U
     * customer_fullName =>
     * customer_phone => +79033463906
     * customer_email => test@simple-technology.ru
     * transaction => 75cafc984840435fb1b0cd764292ec24
     * object_type => transaction
     * status => success
     * payment_system => mandarinpayv1
     * sandbox => true
     * cb_processed_at => 2018-06-06T16:26:29.9256630
     * card_number => 492950XXXXXX6878
     * cb_customer_creditcard_number => 492950XXXXXX6878
     * card_holder => CARD HOLDER
     * card_expiration_year => 2018
     * card_expiration_month => 07
     * 3dsecure => false
     * gw_id => 292c4709-113c-4450-8463-587b35f5303f
     * 295811af-63bb-4390-9038-65fe3ac6c941 => f4a317a8-22e5-46b7-81f4-36e5fb3f6a62
     * sign => 68cae865bb11ffab63da5c9b0fa62580a60bc5d3311077cf1181ac3e519c02c5
     */
    public function handlePayAction()
    {
        $incoming_data = $this->processFormDataInit();

        /** @var Deal|null $deal */
        $deal = $incoming_data['deal'];

        if ($deal->status['status'] !== 'confirmed') {

            logMessage(
                'По сделке ' . $deal->getNumber() .  ' платежей не ожидается',
                self::LOG_FILE_NAME
            );

            // Говорим Мандарину, что callback принят и обработан и ничего не делаем
            return new JsonModel(['content' => 'OK']);
        }

        // Достаем POST-дата
        $postData = $this->collectIncomingData()['postData'];

        // Important! Не убирать.
        // Помогает решать проблемы, если MandarinPaymentOrder не создался или не заполнился.
        logMessage($postData,'acquiring-mandarin-callback');

        if($postData) {

            if (false === $this->callbackHandlePolitics
                    ->isCallbackDataCorrect($postData, self::ACTION_FOR_PAYMENT)) {

                // Говорим Мандарину, что callback принят и обработан и ничего не делаем
                return new JsonModel(['content' => 'OK']);
            }

            $orderId_data = $incoming_data['orderId_data'];

            try {
                /** @var MandarinPaymentOrder $mandarinPaymentOrder */
                $mandarinPaymentOrder = $this->mandarinPaymentOrderManager
                    ->getIncomingMandarinPaymentOrderByOrderIdWithRecursion($incoming_data['orderId']);

                if (null === $mandarinPaymentOrder) {
                    $payment_purpose = $this->invoiceManager->generateInvoicePaymentPurpose($deal, true);
                    $data = [
                        'order_id' => $incoming_data['orderId'],
                        'price' => $deal->getPayment()->getExpectedValue(),
                        'payment_purpose' => $payment_purpose,
                    ];
                    // ... и создаем MandarinPaymentOrder
                    // Create MandarinPaymentOrder
                    $mandarinPaymentOrder = $this->mandarinPaymentOrderManager->createMandarinPaymentOrder(
                        $deal,
                        $data,
                        PaymentOrderManager::TYPE_INCOMING
                    );

                    logMessage('mandarinPaymentOrder created by CallbackHandleController','acquiring-mandarin');
                }

                // Проверка статуса транзакции
                if ($postData['status'] !== MandarinPaymentOrderManager::STATUS_SUCCESS) {
                    // Update MandarinPaymentOrder with failed data
                    $this->mandarinPaymentOrderManager->manageFailedMandarinPaymentOrder($mandarinPaymentOrder, $postData);

                    // Говорим Мандарину, что callback принят и обработан
                    return new JsonModel(['content' => 'OK']);
                }

                // @TODO Добавить проверку, что сделка ожидает входящий платёж?

                // @TODO Пока не создаем платёжку, если данные в OrderId не идеальные. Продумать!
                if (false === $this->callbackHandlePolitics->isOrderIdDataCorrect($deal, $orderId_data)) {
                    // Конкретная ошибка запишется в логи в методе isOrderIdDataCorrect
                    // Говорим Мандарину, что callback принят и обработан
                    return new JsonModel(['content' => 'OK']);
                }

                // @TODO Добавить проверку правильности информации в назначении платежа (customValue0 => Оплата по сделке #GP496U)

                // Create MandarinPaymentOrder
                $this->mandarinPaymentOrderManager->manageSuccessMandarinPaymentOrder(
                    $deal,
                    $mandarinPaymentOrder,
                    $postData
                );
            }
            catch (\Throwable $t) {

                logMessage($t->getMessage(), self::LOG_FILE_NAME);
            }

            // Говорим Мандарину, что callback принят и обработан
            return new JsonModel(['content' => 'OK']);
        }

        // Если нет Post data
        // Пишем лог
        logMessage('No Post data in callback',self::LOG_FILE_NAME);

        // Говорим Мандарину, что callback принят и обработан
        return new JsonModel(['content' => 'OK']);
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        $deal = null;
        $callback_deal_id = null;
        $callback_payment_purpose_type = null;
        $callback_nds_type = null;
        $orderId = null;

        try {
            if (isset($incomingData['orderId'])) {

                $orderId = $incomingData['orderId'];

                try {
                    $decoded_orderId = $this->mandarinPaymentOrderManager->decodeEncodedOrderId($incomingData['orderId']);
                }
                catch (\Throwable $t) {

                    throw new \Exception($t->getMessage());
                }

                $callback_deal_id               = (int)$decoded_orderId->deal_id;
                $callback_payment_purpose_type  = $decoded_orderId->payment_purpose_type;
                $callback_nds_type              = $decoded_orderId->nds_type;

                try {
                    /** @var $deal $deal */ // Can throw Exception
                    $deal = $this->dealManager->getDealById($callback_deal_id);
                }
                catch (\Throwable $t) {

                    throw new \Exception('Deal for orderId ' . $incomingData['orderId'] . ' not found (transaction - ' . $incomingData['transaction'] . ')');
                }
            }
        }
        catch (\Throwable $t) {

            logMessage($t->getMessage(),self::LOG_FILE_NAME);
        }

        $orderId_data = [
            'callback_deal_id'              => $callback_deal_id, // если нет $incomingData['orderId'], то null
            'callback_payment_purpose_type' => $callback_payment_purpose_type, // если нет $incomingData['orderId'], то null
            'callback_nds_type'             => $callback_nds_type, // если нет $incomingData['orderId'], то null
        ];

        return [
            'deal'          => $deal,
            'orderId'       => $orderId,
            'orderId_data'  => $orderId_data,
        ];
    }

//    private function saveData($dispute, $formData): array
//    {
//    }
}