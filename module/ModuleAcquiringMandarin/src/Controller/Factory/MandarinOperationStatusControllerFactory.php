<?php
namespace ModuleAcquiringMandarin\Controller\Factory;

use Application\Service\Deal\DealManager;
use Interop\Container\ContainerInterface;
use ModuleAcquiringMandarin\Controller\MandarinOperationStatusController;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class MandarinOperationStatusControllerFactory
 * @package ModuleAcquiringMandarin\Controller\Factory
 */
class MandarinOperationStatusControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealManager = $container->get(DealManager::class);
        $mandarinPayManager = $container->get(MandarinPayManager::class);
        $mandarinPaymentOrderManager = $container->get(MandarinPaymentOrderManager::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $rbacManager = $container->get(RbacManager::class);

        return new MandarinOperationStatusController(
            $dealManager,
            $mandarinPayManager,
            $mandarinPaymentOrderManager,
            $userManager,
            $baseAuthManager,
            $rbacManager
        );
    }
}