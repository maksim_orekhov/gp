<?php
namespace ModuleAcquiringMandarin\Controller\Factory;

use Application\Service\Deal\DealManager;
use Application\Service\InvoiceManager;
use Application\Service\NdsTypeManager;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use ModuleAcquiringMandarin\Service\MandarinPayPolitics;
use Application\Service\UserManager;
use Interop\Container\ContainerInterface;
use ModuleAcquiringMandarin\Controller\MandarinPayController;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\Service\Base\BaseAuthManager;

/**
 * Class MandarinPayControllerFactory
 * @package ModuleAcquiringMandarin\Controller\Factory
 */
class MandarinPayControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealManager = $container->get(DealManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $userManager = $container->get(UserManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $mandarinPayManager = $container->get(MandarinPayManager::class);
        $mandarinPayPolitics = $container->get(MandarinPayPolitics::class);
        $ndsTypeManager = $container->get(NdsTypeManager::class);
        $invoiceManager = $container->get(InvoiceManager::class);

        $config = $container->get('Config');

        return new MandarinPayController(
            $dealManager,
            $baseAuthManager,
            $userManager,
            $rbacManager,
            $mandarinPayManager,
            $mandarinPayPolitics,
            $ndsTypeManager,
            $invoiceManager,
            $config
        );
    }
}