<?php
namespace ModuleAcquiringMandarin\Controller\Factory;

use Application\Service\Deal\DealManager;
use Application\Service\InvoiceManager;
use Application\Service\NdsTypeManager;
use Interop\Container\ContainerInterface;
use ModuleAcquiringMandarin\Controller\MandarinOperationResultController;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class CallbackHandleControllerFactory
 * @package ModuleAcquiringMandarin\Controller\Factory
 */
class MandarinOperationResultControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealManager = $container->get(DealManager::class);
        $mandarinPayManager = $container->get(MandarinPayManager::class);
        $mandarinPaymentOrderManager = $container->get(MandarinPaymentOrderManager::class);
        $ndsTypeManager = $container->get(NdsTypeManager::class);
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $invoiceManager = $container->get(InvoiceManager::class);

        return new MandarinOperationResultController(
            $dealManager,
            $mandarinPayManager,
            $mandarinPaymentOrderManager,
            $ndsTypeManager,
            $userManager,
            $baseAuthManager,
            $rbacManager,
            $invoiceManager
        );
    }
}