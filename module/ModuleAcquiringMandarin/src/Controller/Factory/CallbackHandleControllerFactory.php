<?php
namespace ModuleAcquiringMandarin\Controller\Factory;

use Application\Service\Deal\DealManager;
use Application\Service\InvoiceManager;
use ModuleAcquiringMandarin\Service\CallbackHandlePolitics;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use Interop\Container\ContainerInterface;
use ModuleAcquiringMandarin\Controller\CallbackHandleController;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class CallbackHandleControllerFactory
 * @package ModuleAcquiringMandarin\Controller\Factory
 */
class CallbackHandleControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dealManager = $container->get(DealManager::class);
        $mandarinPaymentOrderManager = $container->get(MandarinPaymentOrderManager::class);
        $callbackHandlePolitics = $container->get(CallbackHandlePolitics::class);
        $invoiceManager = $container->get(InvoiceManager::class);

        return new CallbackHandleController(
            $dealManager,
            $mandarinPaymentOrderManager,
            $callbackHandlePolitics,
            $invoiceManager
        );
    }
}