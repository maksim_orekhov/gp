<?php

namespace ModuleAcquiringMandarin\Controller;

use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\User;
use Application\Service\Deal\DealManager;
use Application\Service\InvoiceManager;
use Application\Service\NdsTypeManager;
use Application\Service\Payment\PaymentPolitics;
use Core\Controller\AbstractRestfulController;
use Core\Exception\LogicException;
use Core\Service\TwigViewModel;
use ModuleAcquiringMandarin\Form\InitialFormForMandarinPay;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use ModuleAcquiringMandarin\Service\MandarinPayPolitics;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use ModulePaymentOrder\Entity\PaymentMethodType;
use ModuleRbac\Service\RbacManager;

/**
 * Class MandarinPayController
 * @package ModuleAcquiringMandarin\Controller
 */
class MandarinPayController extends AbstractRestfulController
{
    const ERROR_INVALID_FORM_DATA = 'Invalid form data';
    const ERROR_ACCESS_DENIED = 'ERROR_ACCESS_DENIED';
    /**
     * @var dealManager
     */
    private $dealManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var MandarinPayManager
     */
    private $mandarinPayManager;

    /**
     * @var MandarinPayPolitics
     */
    private $mandarinPayPolitics;

    /**
     * @var PaymentPolitics
     */
    private $paymentPolitics;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * @var InvoiceManager
     */
    private $invoiceManager;

    /**
     * @var array
     */
    private $config;

    /**
     * MandarinPayController constructor.
     * @param DealManager $dealManager
     * @param BaseAuthManager $baseAuthManager
     * @param UserManager $userManager
     * @param RbacManager $rbacManager
     * @param MandarinPayManager $mandarinPayManager
     * @param MandarinPayPolitics $mandarinPayPolitics
     * @param NdsTypeManager $ndsTypeManager
     * @param InvoiceManager $invoiceManager
     * @param $config
     */
    public function __construct(DealManager $dealManager,
                                BaseAuthManager $baseAuthManager,
                                UserManager $userManager,
                                RbacManager $rbacManager,
                                MandarinPayManager $mandarinPayManager,
                                MandarinPayPolitics $mandarinPayPolitics,
                                NdsTypeManager $ndsTypeManager,
                                InvoiceManager $invoiceManager,
                                $config)
    {
        $this->dealManager  = $dealManager;
        $this->baseAuthManager  = $baseAuthManager;
        $this->userManager  = $userManager;
        $this->rbacManager  = $rbacManager;
        $this->mandarinPayManager = $mandarinPayManager;
        $this->mandarinPayPolitics = $mandarinPayPolitics;
        $this->ndsTypeManager = $ndsTypeManager;
        $this->invoiceManager = $invoiceManager;
        $this->config       = $config;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     * @throws \Exception
     */
    public function payAction()
    {
        //only Http
        if ($this->getRequest()->isXmlHttpRequest()) {

            return $this->message()->error('Method not supported');
        }

        try {
            $incoming_data = $this->processFormDataInit();
            /** @var Deal|null $deal */
            $deal = $incoming_data['deal'];

            if (null === $deal) {

                return $this->message()->error('No deal for card pay specified');
            }

            /** @var User $user */
            $user = $incoming_data['user'];

            if ($user !== $deal->getCustomer()->getUser()) {

                return $this->message()->error('Forbidden create PayOff for deal ' . $deal->getNumber());
            }

            if ($deal->status['status'] !== 'confirmed') {

                return $this->message()->error('По сделке ' . $deal->getNumber() .  ' платежей не ожидается');
            }

            // @TODO Перенести в processFormDataInit?

            // Проверяем, что сделка может принять платёж через Мандарин
            if (false === PaymentPolitics::isPaymentMethodTypeAvailable($deal, PaymentMethodType::ACQUIRING_MANDARIN)) {

                throw new LogicException(null, LogicException::PAYMENT_BY_ACQUIRING_MANDARIN_CAN_NOT_BE_MADE);
            }

            // Сумма, выплаченная по сделке на данный момент
            $paid_amount = $this->dealManager->getDealPaidAmount($deal);

            // Проверяем, что сделка может принять платёж через Мандарин
            if (false === $this->mandarinPayPolitics->isDealAvailableForMandarinPay($deal, $paid_amount)) {

                throw new LogicException(null, LogicException::PAYMENT_BY_ACQUIRING_MANDARIN_CAN_NOT_BE_MADE);
            }

            $payment_purpose = $this->invoiceManager->generateInvoicePaymentPurpose($deal, true);

            /** @var Payment $payment */
            $payment = $deal->getPayment();

            $price = $payment->getExpectedValue();
            // Создаем orderId, как зашифрованную комбинацию
            $order_id = $this->mandarinPayManager->createOrderId($deal, 'payoff');
            $custom_values = [
                ['name' => 'Назначение платежа', 'value' => $payment_purpose]
            ];
            // stdClass
            $result = $this->mandarinPayManager->createPaymentTransaction($order_id, $price, $user, $custom_values);

            if (!isset($result->jsOperationId)) {

                throw new LogicException(null, LogicException::MANDARIN_TRANSACTION_NOT_CREATED);
            }

            // / @TODO до сих пор перенести в processFormDataInit?

            $dealOutput = $this->dealManager->getDealOutputForSingle($deal);

            $mandarinPayForm = null;

            if (isset($result->userWebLink) && true !== $this->config['mandarin']['hosted_fields']) {

                $data_for_pay = $this->mandarinPayManager->getDataForPay($price, $order_id, $dealOutput);

                $mandarinPayForm = new InitialFormForMandarinPay($result->userWebLink, $data_for_pay);
                $mandarinPayForm->get('sign')
                    ->setAttribute('value', $this->mandarinPayManager->calculateSignFromFormObject($mandarinPayForm));

                $mandarinPayForm->prepare();
            }
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        $view = new TwigViewModel([
            'deal'              => $dealOutput,
            'mandarinPayForm'   => $mandarinPayForm,
            'jsOperationId'     => $result->jsOperationId,
            'hosted_fields'     => $this->config['mandarin']['hosted_fields']
        ]);
        $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

        return $view;
    }

    /**
     * @return TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel|array
     * @throws \Exception
     *
     * Создание transaction в Mandarin по запросу из react
     *
     * На вход
     * POST
     */
    public function hashOperationIdAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {

            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $postData = $this->collectIncomingData()['postData']['id'];
        if (!$postData || !preg_match('/^[1-9][0-9]*$/', $postData)) {
            return $this->message()->error('not id deal send');
        }

        /** @var User $user */ // Can throw Exception
        $login = $this->baseAuthManager->getIdentity();
        // $login = 'test';
        $user = $this->userManager->getUserByLogin($login);
        // Whether the currant user has role Operator
        $isOperator = $this->rbacManager->hasRole($user, 'Operator');
        /** @var Deal $deal */ // Can throe Exception
        $deal = $this->dealManager->getDealById($postData);
        // Check permissions only for customer
        $isCustomer=($deal->getCustomer()->getUser()->getLogin()==$login) ? true :false ;

        if (!$this->access('deal.own.view', ['user' => $user, 'deal' => $deal]) && !$isOperator && $isCustomer) {
            return $this->message()->error(self::ERROR_ACCESS_DENIED);
        }

        try {
            /** @var Payment $payment */
            $payment = $deal->getPayment();
            $payment_purpose = $this->invoiceManager->generateInvoicePaymentPurpose($deal, true);
            $price = $payment->getExpectedValue();
            // Создаем orderId, как зашифрованную комбинацию
            $order_id = $this->mandarinPayManager->createOrderId($deal, 'payment');
            $custom_values = [
                ['name' => 'Назначение платежа', 'value' => $payment_purpose]
            ];
            // stdClass
            $result = $this->mandarinPayManager->createPaymentTransaction($order_id, $price, $user, $custom_values);
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->message()->success('success', [
            'jsOperationId' => $result->jsOperationId,
            'userLogin' => $login
        ]);
    }

    /**
     * @return array|bool|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        // only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        try {
            $formDataInit = $this->processFormDataInit();

            if (!is_array($formDataInit)) {

                return $this->message()->error('formInit failed');
            }

            $formDataInit['user'] = $this->userManager->extractUser($formDataInit['user']);
            $formDataInit['deal'] = $formDataInit['deal'] ? $this->dealManager->getDealOutputForSingle($formDataInit['deal']) : null;
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $incomingData = $this->collectIncomingData();

        //check
        try {
            // Get current user // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());
            //process data for update or delete
            $deal = null;
            if (isset($incomingData['idDeal'])){
                /** @var $deal $deal */ // Can throw Exception
                $deal = $this->dealManager->getDealById((int) $incomingData['idDeal']);
                // Check if $deal belongs to current user
                if ($deal && !$this->dealManager->isUserPartOfDial($deal, $user) && !$this->rbacManager->hasRole(null, 'Operator')) {

                    $deal = null;
                }
            }

        } catch (\Throwable $t){

            throw new \Exception($t);
        }

        return [
            'user' => $user,
            'deal' => $deal,
        ];
    }

//    private function saveFormData($dispute, $formData): array
//    {
//
//    }
}