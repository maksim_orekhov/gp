<?php
namespace ModuleAcquiringMandarin\Exception;

interface MandarinExceptionCodeInterface
{
    /**
     * префикс MANDARIN, для сообшений суфикс _MESSAGE
     */
    const MANDARIN_TRANSACTION_NOT_CREATED = 'MANDARIN_TRANSACTION_NOT_CREATED';
    const MANDARIN_TRANSACTION_NOT_CREATED_MESSAGE = 'Transaction not created';
}
