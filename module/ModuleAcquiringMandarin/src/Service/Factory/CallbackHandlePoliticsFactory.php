<?php
namespace ModuleAcquiringMandarin\Service\Factory;

use ModuleAcquiringMandarin\Service\CallbackHandlePolitics;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class CallbackHandlePoliticsFactory
 * @package ModuleAcquiringMandarin\Service\Factory
 */
class CallbackHandlePoliticsFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        return new CallbackHandlePolitics(
            $config['mandarin']['merchant_id'],
            $config['mandarin']['secret_key']
        );
    }
}