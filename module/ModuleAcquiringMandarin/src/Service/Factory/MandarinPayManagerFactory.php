<?php
namespace ModuleAcquiringMandarin\Service\Factory;

use Application\Service\NdsTypeManager;
use Core\Adapter\TokenAdapter;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class MandarinPayManagerFactory
 * @package ModuleAcquiringMandarin\Controller\Factory
 */
class MandarinPayManagerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $tokenAdapter = $container->get(TokenAdapter::class);
        $ndsTypeManager = $container->get(NdsTypeManager::class);

        $config = $container->get('Config');

        return new MandarinPayManager(
            $tokenAdapter,
            $ndsTypeManager,
            $config['mandarin']['merchant_id'],
            $config['mandarin']['secret_key'],
            $config['mandarin']['gateway_host'],
            $config['main_project_url']
        );
    }
}