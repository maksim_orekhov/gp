<?php

namespace ModuleAcquiringMandarin\Service;

use Application\Entity\Deal;
use Application\Entity\NdsType;
use Application\Entity\User;
use Application\Service\NdsTypeManager;
use Core\Adapter\TokenAdapter;
use Zend\Form\Form;
use Zend\Form\Element;

/**
 * Class MandarinPayManager
 * @package ModuleAcquiringMandarin\Service
 */
class MandarinPayManager
{
    use \Core\Provider\HttpRequestTrait;

    const CARD_BINDING_SUB_URL = 'api/card-bindings';
    const CARD_TRANSACTION_SUB_URL = 'api/transactions';

    /**
     * @var TokenAdapter
     */
    private $tokenAdapter;

    private $merchant_id;

    private $secret_key;

    private $mandarin_base_url;

    private $main_project_url;

    /**
     * @var NdsTypeManager
     */
    private $ndsTypeManager;

    /**
     * MandarinPayManager constructor.
     * @param TokenAdapter $tokenAdapter
     * @param NdsTypeManager $ndsTypeManager
     * @param $merchant_id
     * @param $secret_key
     * @param $mandarin_base_url
     * @param $main_project_url
     */
    public function __construct(TokenAdapter $tokenAdapter,
                                NdsTypeManager $ndsTypeManager,
                                $merchant_id,
                                $secret_key,
                                $mandarin_base_url,
                                $main_project_url)
    {
        $this->tokenAdapter     = $tokenAdapter;
        $this->ndsTypeManager   = $ndsTypeManager;
        $this->merchant_id      = $merchant_id;
        $this->secret_key       = $secret_key;
        $this->mandarin_base_url = $mandarin_base_url;
        $this->main_project_url = $main_project_url;
    }

    /**
     * @param Form $form
     * @return string
     */
    public function calculateSignFromFormObject(Form $form) :string
    {
        $fields = $this->getFormFieldsArray($form);

        return $this->calculateSign($fields);
    }

    /**
     * @param Form $form
     * @return array
     */
    public function getFormFieldsArray(Form $form) :array
    {
        $fields = [];
        /** @var Element $value */
        foreach ($form->getElements() as $key => $value) {
            $fields[$key] = $value->getValue();
        }

        return $fields;
    }

    /**
     * @param $fields
     * @return string
     */
    public function calculateSign($fields) :string
    {
        ksort($fields);
        unset($fields['sign']);
        $secret_t = '';
        foreach ($fields as $key => $val) {
            $secret_t = $secret_t . '-' . $val;
        }
        $secret_t = substr($secret_t, 1) . '-' . $this->secret_key;

        return hash('sha256', $secret_t);
    }

    /**
     * @param $order_id
     * @param $price
     * @param User $user
     * @param array $custom_values
     * @return mixed
     *
     * В случае успешного создания транзакции синхронно возвращает:
     * HTTP 200
     * object(stdClass) с public свойствами:
     *  'id' => string 'a7bffc4639604f50ab60ee04515b1e62'
     *  'userWebLink' => string 'https://secure.mandarinpay.com/Pay/Select?transaction=a7bffc4639604f50ab60ee04515b1e62&language=ru'
     *  'jsOperationId' => string 'a7bffc4639604f50ab60ee04515b1e62'
     *
     * ... асинхронно на callback url:
     * @TODO расписать
     *
     * @see http://docs.mandarinbank.com/api_v2.html#purchase
     */
    public function createPaymentTransaction($order_id, $price, User $user, $custom_values = [])
    {
        $payment = $this->genPayment($order_id, $price);

        // @TODO Проверка, что значение $price соответствует оплате по сделке

        $costumer_info = $this->toArrayCostumerInfo($user);

        $array_content = array_merge($payment, $costumer_info);
        // Important! Название ключа "customValues" не менять!
        $array_content['customValues'] = $custom_values;
        // Important! Название ключа "customValues" не менять!
        $array_content['urls'] = [
            'callback' => $this->main_project_url . 'mandarin/callback/deal/pay',
            'return' => $this->main_project_url . 'mandarin/operation-result'
        ];

        $json_content = json_encode($array_content);

        $url_transaction = $this->mandarin_base_url . self::CARD_TRANSACTION_SUB_URL;

        $result = $this->curlMandarinRequest($url_transaction, $json_content, array(
            'Content-Type: application/json',
            'X-Auth:' . $this->generateAuth()
        ));

        return $result;
    }

    /**
     * @param User $user
     * @return mixed
     *
     * В случае успешного создания транзакции синхронно возвращает:
     * HTTP 200
     * object(stdClass) с public свойствами:
     *  "id": "43913ddc000c4d3990fddbd3980c1725",
     *  "userWebLink": "https://secure.mandarinpay.com/Pay?transaction=0eb51e74-e704-4c36-b5cb-8f0227621518",
     *  "jsOperationId": "9874694yr87y73e7ey39ed80"
     *
     * не успешного:
     * object(stdClass) с public свойством:
     * "error": "Invalid request"
     *
     * @TODO Пока не используется
     *
     * @see http://docs.mandarinbank.com/api_v2.html#full-binding
     */
    public function newCardBinding(User $user, string $return_url)
    {
        $url_transaction = $this->mandarin_base_url . self::CARD_BINDING_SUB_URL;
        $array_content = $this->toArrayCostumerInfo($user);
        $array_content['urls'] = [
            'return' => $return_url
        ];
        $json_content = json_encode($array_content);

        $result = $this->curlMandarinRequest($url_transaction, $json_content, array(
            'Content-Type: application/json',
            'X-Auth:' . $this->generateAuth(),
        ));

        return $result;
    }

    /**
     * @param $order_id
     * @param $price
     * @param $id_card_number
     * @return mixed
     *
     * @TODO Пока не используется
     */
    public function payFromCardBinding($order_id, $price, $id_card_number)
    {
        $payout = $this->genPayment($order_id, $price);
        $payout['target']['card'] = $id_card_number;
        $json_content = json_encode($payout);

        $url_transaction = $this->mandarin_base_url . self::CARD_TRANSACTION_SUB_URL;

        $result = $this->curlMandarinRequest($url_transaction, $json_content, array(
            'Content-Type: application/json',
            'X-Auth:' . $this->generateAuth(),
        ));

        return $result;
    }

    /**
     * @param $price
     * @param $order_id
     * @param $dealOutput
     * @return array
     */
    public function getDataForPay($price, $order_id, $dealOutput) :array
    {
        $data_for_pay = [
            'merchantId' => $this->merchant_id,
            'price' => $price,
            'orderId' => $order_id,
            'customer_email' => $dealOutput['customer']['email'],
            'customName1' => 'client name 1', // Имя физика или Название юрика
            'customValue1' => 'Оплата по сделке ' . $dealOutput['number'],
            'callbackUrl' => '', // ссылка на обработку результатов запроса (фиксация зачисления средтств или привязки карты)
            // @TODO Куда перенаправлять?
            'returnUrl' => $this->main_project_url . 'mandarin/operation-result', // ссылка, куда будет перенаправлен пользователь.
            'orderActualTill' => '2018-10-29 12:34:56+00:00'
        ];

        return $data_for_pay;
    }

    /**
     * @param Deal $deal
     * @param string $payment_purpose_type
     * @return string
     * @throws \Core\Exception\LogicException
     *
     * [id_сделки][timestamp][refund/payment/discount/overpay/payoff]+[nds_type]
     */
    public function createOrderId(Deal $deal, string $payment_purpose_type) :string
    {
        /** @var NdsType $ndsType */
        $ndsType = $this->ndsTypeManager->getNdsTypeOfDealAgent($deal->getCustomer());

        $current_date = new \DateTime();
        $data = array(
            'deal_id'               => $deal->getId(),
            'timestamp'             => $current_date->getTimestamp(),
            'payment_purpose_type'  => $payment_purpose_type,
            'nds_type'              => $ndsType ? $ndsType->getType() : 0
        );

        return $this->tokenAdapter->createToken($data);
    }

    /**
     * @param string $orderId
     * @return object
     * @throws \Exception
     */
    public function decodeEncodedOrderId(string $orderId)
    {
        try {
            /*
             * Возвращает
             * object(stdClass)
             *  public 'deal_id'
             *  public 'timestamp'
             *  public 'payment_purpose'
             */
            return $this->tokenAdapter->decodeToken($orderId);
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage());
        }
    }

    /**
     * @param User $user
     * @return mixed
     */
    private function toArrayCostumerInfo(User $user)
    {
        $array['customerInfo'] = [
            'email' => $user->getEmail()->getEmail(),
            'phone' => '+'.$user->getPhone()->getPhoneNumber()
        ];

        return ($array);
    }

    /**
     * Generate  array payment
     *
     * @param $order_id
     * @param $price
     * @return mixed
     */
    private function genPayment($order_id, $price)
    {
        $array['payment'] = [
            'orderId'   => $order_id,
            'action'    => 'pay',
            'price'     => $price
        ];

        return $array;
    }

    /**
     * @return string
     */
    private function reqidCalc()
    {
        // this is calk reqid this is function need to registr Auth
        return time() . '_' . microtime(true) . '_' . mt_rand();
    }

    /**
     * "merchantId-SHA256(merchantId-requestId-secret)-requestId"
     *
     * @return string
     */
    private function generateAuth() :string
    {
        $reqid = $this->reqidCalc();
        $hash = hash('sha256', $this->merchant_id . '-' . $reqid . '-' . $this->secret_key);

        return $this->merchant_id . '-' . $hash . '-' . $reqid;
    }
}