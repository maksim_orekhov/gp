<?php

namespace ModuleAcquiringMandarin\Service;

use Application\Entity\Deal;

/**
 * Class MandarinPayPolitics
 * @package ModuleAcquiringMandarin\Service
 */
class MandarinPayPolitics
{
    /**
     * @param Deal $deal
     * @param $paid_amount
     * @return bool
     */
    public function isDealAvailableForMandarinPay(Deal $deal, $paid_amount) :bool
    {
        if ($deal->status['status'] !== 'confirmed') {

            return false;
        }

        // Если сделку уже частично оплатили другим методом оплаты
        if ($paid_amount > 0) {

            return false;
        }

        return true;
    }
}