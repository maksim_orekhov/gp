<?php

namespace ModuleAcquiringMandarin\Service;
use Application\Entity\Deal;
use ModuleAcquiringMandarin\Controller\CallbackHandleController;

/**
 * Class CallbackHandlePolitics
 * @package ModuleAcquiringMandarin\Service
 */
class CallbackHandlePolitics
{
    const SUCCESS_STATUS = 'success';
    const FAILED_STATUS = 'failed';

    const REQUIRED_DATA = [
        'status',
        'merchantId',
        'action',
        'orderId'
    ];

    /**
     * @var int
     */
    private $merchant_id;

    /**
     * @var string
     */
    private $secret_key;

    /**
     * CallbackHandlePolitics constructor.
     * @param int $merchant_id
     * @param string $secret_key
     */
    public function __construct(int $merchant_id, string $secret_key)
    {
        $this->merchant_id  = $merchant_id;
        $this->secret_key   = $secret_key;
    }

    /**
     * @param array $data
     * @param string $expected_action
     * @return bool
     */
    public function isCallbackDataCorrect(array $data, string $expected_action) :bool
    {
        foreach (self::REQUIRED_DATA as $required) {
            if (!array_key_exists($required, $data)) {
                // Пишем лог
                logMessage(
                    'Callback does not have required parameter ' . $required,
                    CallbackHandleController::LOG_FILE_NAME
                );

                return false;
            }
        }

//        // Проверка статуса транзакции
//        if (false === $this->checkStatus($data['status'], $data['orderId'])) {
//
//            return false;
//        }

        // Проверка merchantId
        if (false === $this->checkMerchantId($data['merchantId'], $data['orderId'])) {

            return false;
        }

        // Проверка action
        if (false === $this->checkAction($data['action'], $expected_action, $data['orderId'])) {

            return false;
        }

        // Расчитываем и проверяем sign
        if (false === $this->checkSign($data)) {

            return false;
        }

        return true;
    }

    /**
     * @param Deal $deal
     * @param $orderId_data
     * @return bool
     *
     * @TODO Тесты для метода не пишу, т.к., вероятно, он изменится кардинально
     */
    public function isOrderIdDataCorrect(Deal $deal, $orderId_data) :bool
    {
        // Если нет deal_id в callback (установлена переменная со значением, отличным от NULL)
        if (!isset($orderId_data['callback_deal_id'])) {
            // Пишем лог
            logMessage(
                'No deal_id in encoded orderId',
                CallbackHandleController::LOG_FILE_NAME
            );

            return false;
        }

        // Если deal_id в callback есть, но сделки с таким id нет в системе
        if (null === $deal) {
            // Пишем лог
            logMessage(
                'Deal for deal_id ' . $orderId_data['callback_deal_id'] . ' does not exists',
                CallbackHandleController::LOG_FILE_NAME
            );

            return false;
        }

        // Если нет назначения платежа (установлена переменная со значением, отличным от NULL)
        if (!isset($orderId_data['callback_payment_purpose_type'])) {
            // Пишем лог
            logMessage(
                'No payment_purpose_type in encoded orderId for Deal ' . $orderId_data['callback_deal_id'],
                CallbackHandleController::LOG_FILE_NAME
            );

            return false;
        }

        // Если нет кода НДС (установлена переменная со значением, отличным от NULL)
        if (!isset($orderId_data['callback_nds_type'])) {
            // Пишем лог
            logMessage(
                'No nds_type in encoded orderId for Deal ' . $orderId_data['callback_deal_id'],
                CallbackHandleController::LOG_FILE_NAME
            );

            return false;
        }

        return true;
    }

    /**
     * @param string $status
     * @param string $order_id
     * @return bool
     */
    private function checkStatus(string $status, string $order_id) :bool
    {
        if (self::SUCCESS_STATUS === $status) {

            return true;
        }

        // Пишем лог
        logMessage(
            'Attempt to make a transaction for orderId ' . $order_id . ' returned status ' . $status,
            CallbackHandleController::LOG_FILE_NAME
        );

        return false;
    }

    /**
     * @param $merchant_id
     * @param string $order_id
     * @return bool
     */
    private function checkMerchantId($merchant_id, string $order_id) :bool
    {
        if ((int)$merchant_id === $this->merchant_id) {

            return true;
        }

        // Пишем лог
        logMessage(
            'Wrong merchantId for orderId ' . $order_id . ' ('.$merchant_id.')',
            CallbackHandleController::LOG_FILE_NAME
        );

        return false;
    }

    /**
     * @param string $callback_action
     * @param string $expected_action
     * @param string $order_id
     * @return bool
     */
    private function checkAction(string $callback_action, string $expected_action, string $order_id) :bool
    {
        if ($callback_action === $expected_action) {

            return true;
        }

        // Пишем лог
        logMessage(
            'Wrong action for orderId ' . $order_id . ' ('.$callback_action.')',
            CallbackHandleController::LOG_FILE_NAME
        );

        return false;
    }

    /**
     * @param array $data
     * @return bool
     */
    private function checkSign(array $data) :bool
    {
        $sign = $data['sign'];
        unset($data['sign']);
        $to_hash = '';
        if (null !== $data && is_array($data)) {
            ksort($data);
            $to_hash = implode('-', $data);
        }
        $to_hash = $to_hash . '-' . $this->secret_key;
        $calculated_sign = hash('sha256', $to_hash);

        // Если вычесленный calculated_sign совпадает с полученным из callback sign
        if ($calculated_sign === $sign) {

            return true;
        }

        // Если не совпадают
        // Пишем лог
        logMessage(
            'Sign does not matched for orderId '.$data['orderId'],
            CallbackHandleController::LOG_FILE_NAME
        );

        return false;
    }
}