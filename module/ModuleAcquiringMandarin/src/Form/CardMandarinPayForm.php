<?php
namespace ModuleAcquiringMandarin\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class CardMandarinPayForm
 * @package ModuleAcquiringMandarin\Form
 *
 * @TODO Пока не используется. Проверить и удалить!
 */
class CardMandarinPayForm extends Form
{
    /**
     * CartPayForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('card-pay-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'card-pay-form');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => 'text',
            'name' => 'card_number',
            'attributes' => [
                'id' => 'card_number',
                'placeholder'=>'Номер карты'
            ],
            'options' => [
                'label' => 'Номер карты',
            ],
        ]);

        $this->add([
            'type'  => 'text',
            'name' => 'card_holder_name',
            'attributes' => [
                'id' => 'card_holder_name',
                'placeholder'=>'Имя держателя карты'
            ],
            'options' => [
                'label' => 'Имя держателя карты',
            ],
        ]);

        $this->add([
            'type'  => Element\Number::class,
            'name' => 'card_сvv_code', // Card Verification Value
            'attributes' => [
                'id' => 'card_сvv_code',
                'placeholder'=>'СVV код'
            ],
            'options' => [
                'label' => 'СVV код',
            ],
        ]);

        // Add the CSRF field
        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }


    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'card_number',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
                ['name'    =>  'StripNewlines'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\CreditCard::class,
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'card_holder_name',
            'required' => true,
            'filters'  => [
                ['name'    =>  'StringTrim'],
                ['name'    =>  'StripTags'],
                ['name'    =>  'StripNewlines'],
            ],
            'validators' => [
                ['name'    => 'StringLength', 'options' => ['min' => 3, 'max' => 61]],
                ['name'    => 'Regex',
                    'options' => [
                        'pattern' => '/[a-zA-Z-]{1,30}\s{1}[a-zA-Z-]{1,30}/', // два слова, только латиница
                        'message' => 'Неверный формат ввода имени владельца карты']
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'card_сvv_code',
            'required' => true,
            'filters'  => [
                ['name'    =>  'StringTrim'],
                ['name'    =>  'StripTags'],
                ['name'    =>  'StripNewlines'],
            ],
            'validators' => [
                [
                    'name' => 'Digits'
                ],
            ],
        ]);
    }
}