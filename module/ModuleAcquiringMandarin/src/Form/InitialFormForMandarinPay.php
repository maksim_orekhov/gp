<?php
namespace ModuleAcquiringMandarin\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;
use Zend\Form\Element\Hidden;

/**
 * Class InitialFormForMandarinPay
 * @package ModuleAcquiringMandarin\Form
 */
class InitialFormForMandarinPay extends Form
{
    protected $data_for_pay;
    protected $user_web_link;

    /**
     * InitialFormForMandarinPay constructor.
     * @param string $user_web_link
     * @param array $data_for_pay
     */
    public function __construct(string $user_web_link, array $data_for_pay)
    {
        $this->data_for_pay = $data_for_pay;
        $this->user_web_link = $user_web_link;

        // Define form name
        parent::__construct('initial-form-for-card-bindings');

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', $this->user_web_link);
        $this->setAttribute('id', 'initial-form-for-card-bindings');
        $this->addElements();
        #$this->addInputFilter();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        $this->add([
            'type'  => Hidden::class,
            'name' => 'merchantId',
            'attributes' => array(
                'value' => $this->data_for_pay['merchantId']
            ),
        ]);

        $this->add([
            'type'  => Hidden::class,
            'name' => 'price',
            'attributes' => array(
                'value' => $this->data_for_pay['price']
            ),
        ]);

        $this->add([
            'type'  => Hidden::class,
            'name' => 'orderId',
            'attributes' => array(
                'value' => $this->data_for_pay['orderId']
            ),
        ]);

        $this->add([
            'type'  => Hidden::class,
            'name' => 'customer_email',
            'attributes' => array(
                'value' => $this->data_for_pay['customer_email']
            ),
        ]);

        $this->add([
            'type'  => Hidden::class,
            'name' => 'customName1',
            'attributes' => array(
                'value' => $this->data_for_pay['customName1']
            ),
        ]);

        $this->add([
            'type'  => Hidden::class,
            'name' => 'customValue1',
            'attributes' => array(
                'value' => $this->data_for_pay['customValue1']
            ),
        ]);

        $this->add([
            'type'  => Hidden::class,
            'name' => 'callbackUrl',
            'attributes' => array(
                'value' => $this->data_for_pay['callbackUrl']
            ),
        ]);

        $this->add([
            'type'  => Hidden::class,
            'name' => 'returnUrl',
            'attributes' => array(
                'value' => $this->data_for_pay['returnUrl']
            ),
        ]);

        $this->add([
            'type'  => Hidden::class,
            'name' => 'sign'
        ]);

        $this->add([
            'type'  => Hidden::class,
            'name' => 'orderActualTill',
            'attributes' => array(
                'value' => $this->data_for_pay['orderActualTill']
            ),
        ]);

        // Add the Submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'id' => 'submit',
            ],
        ]);
    }

    /**
     * Этот метод создает фильтр входных данных (используется для фильтрации/валидации).
     */
    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

//        $inputFilter->add([
//            'name'     => 'card_number',
//            'required' => true,
//            'filters'  => [
//                ['name' => 'StringTrim'],
//                ['name' => 'StripTags'],
//                ['name'    =>  'StripNewlines'],
//            ],
//            'validators' => [
//                [
//                    'name' => \Zend\Validator\CreditCard::class,
//                ],
//            ],
//        ]);
    }
}