<?php
namespace ModuleAcquiringMandarin;

use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return array_replace_recursive(
            require __DIR__ . '/../config/assets.config.php',
            require __DIR__ . '/../config/mandarin.config.php',
            require __DIR__ . '/../config/module.config.php',
            require __DIR__ . '/../config/router.config.php',
            require __DIR__ . '/../config/views.config.php'
        );
    }

    /**
     * This method is called once the MVC bootstrapping is complete and allows
     * to register event listeners.
     *
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {
    }
}
