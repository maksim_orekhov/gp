<?php

namespace ModuleAcquiringMandarinTest\HTMLOutput;

use Application\Entity\Payment;
use ModuleAcquiringMandarin\Controller\MandarinPayController;
use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\Deal;
use Zend\Form\Element;

/**
 * Class MandarinPayControllerTest
 * @package ModuleAcquiringMandarinTest\HTMLOutput
 */
class MandarinPayControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DEAL_WITH_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';
    const DEAL_WITH_STATUS_NEGOTIATION_NAME = 'Сделка Negotiation';
    const DEAL_WITH_STATUS_DEBIT_MATCHED_NAME = 'Сделка Debit Matched';
    const DEAL_WITH_PARTIAL_PAYMENT_NAME = 'Сделка Частично оплачено';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /////////////// poyAction (экшен только для Ajax!)

    /**
     * Тестируем недоступность PayAction неавторизованному Пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testPayActionForDealInConfirmedStatusNotPaidForNotAutorized()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'csrf' => $csrf
        ];

        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin');
        $this->assertRedirectTo('/login?redirectUrl=/deal/'.$deal->getId().'/pay/mandarin');
    }

    /**
     * Тестируем недоступность PayAction неавторизованному Пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testPayActionForDealInConfirmedStatusNotPaidForOperator()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        // Залогиниваем пользователя Operator
        $this->login('operator');

        $this->assertNotNull($deal);

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'csrf' => $csrf
        ];

        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin');
        $this->assertRedirectTo('/not-authorized');
    }

    /**
     * Тестируем доступность PayAction Пользователю
     * @Important! Только на неоплаченную сделку (неизмененные фикстуры). Иначе - mandarinPayPolitics не пропустит
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testPayActionForDealInConfirmedStatusNotPaid()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        // Залогиниваем пользователя test
        $this->login('test');

        $this->assertNotNull($deal);

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'csrf' => $csrf
        ];

        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin');
    }

    /**
     * Ajax
     * Тестируем недоступность PayAction Пользователю по ajax.
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testPayActionForDealInConfirmedStatusNotPaidForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        // Залогиниваем пользователя test
        $this->login('test');

        $this->assertNotNull($deal);

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'csrf' => $csrf
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin', 'POST', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Method not supported', $data['message']);
    }

    /**
     * Тестируем недоступность createForm Кастомеру, если сделка в статсе Debit Matched
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testPayActionForDealWithImproperStatus()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_DEBIT_MATCHED_NAME]);

        // Залогиниваем пользователя от customer
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $this->assertNotNull($deal);

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'csrf' => $csrf
        ];

        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin');

        // Проверяем что нет блока в DOM
        $this->assertNotQuery('#CardMandarinPayPage');
        // Проверяем что нет формы 'hosted_fields' в DOM
        $this->assertNotQuery('#form-hosted-pay');
        // error-page
        $this->assertQuery('#message-error-page');
    }

    /**
     * Тестируем недоступность createForm Контрактору, если сделка в статсе Negotiation
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testPayActionForDealWithImproperStatus2()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_NEGOTIATION_NAME]);

        // Залогиниваем пользователя от customer
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $this->assertNotNull($deal);

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'csrf' => $csrf
        ];

        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin');

        // Проверяем что нет блока в DOM
        $this->assertNotQuery('#CardMandarinPayPage');
        // Проверяем что нет формы 'hosted_fields' в DOM
        $this->assertNotQuery('#form-hosted-pay');
        // error-page
        $this->assertQuery('#message-error-page');
    }

    /**
     * Тестируем недоступности формы PayAction для частично оплаченной сделки "Сделка Частично оплачено"
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testPayActionForDealWithPartialPayment()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_PARTIAL_PAYMENT_NAME]);

        // Залогиниваем пользователя от customer
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $this->assertNotNull($deal);

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'csrf' => $csrf
        ];

        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin', 'POST', $postData);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin');

        // Проверяем что нет блока в DOM
        $this->assertNotQuery('#CardMandarinPayPage');
        // Проверяем что нет формы 'hosted_fields' в DOM
        $this->assertNotQuery('#form-hosted-pay');
        // error-page
        $this->assertQuery('#message-error-page');
    }

    /////////////// formInit (экшен только для Ajax!)

    // Неавторизованный

    /**
     * Недоступность /deal/[deal_id]/pay/mandarin/form-init (GET на formInitAction)
     * не авторизованному пользователю по Http
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testFormInitCanNotBeAccessedByNonAuthorised()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_PARTIAL_PAYMENT_NAME]);

        $this->assertNotNull($deal);

        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin/form-init', 'GET');

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin-form-init');
        $this->assertRedirectTo('/login?redirectUrl=/deal/'.$deal->getId().'/pay/mandarin/form-init');
    }

    /**
     * Для Ajax
     * Недоступность /deal/[deal_id]/pay/mandarin/form-init (GET на formInitAction)
     * не авторизованному пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testFormInitCanNotBeAccessedByNonAuthorisedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_PARTIAL_PAYMENT_NAME]);

        $this->assertNotNull($deal);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin-form-init');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    /**
     * Для Ajax
     * Недоступность /deal/[deal_id]/pay/mandarin/form-init (GET на formInitAction) Оператору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testFormInitCanNotBeAccessedByOperatorForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_PARTIAL_PAYMENT_NAME]);

        // Залогиниваем пользователя operator
        $this->login('operator');

        $this->assertNotNull($deal);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin-form-init');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Access to the resource is denied to the user.', $data['message']);
    }

    /**
     * Доступность /deal/[deal_id]/pay/mandarin/form-init (GET на formInitAction) для Verified
     *
     *
     * @group acquiring-mandarin
     */
    public function testFormInitCanBeAccessedByVerifiedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_PARTIAL_PAYMENT_NAME]);

        $customer_login = $deal->getCustomer()->getUser()->getLogin();
        // Залогиниваем пользователя от customer
        $this->login($customer_login);

        $this->assertNotNull($deal);

        $isXmlHttpRequest = true;
        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin/form-init', null, [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinPayController::class);
        $this->assertMatchedRouteName('deal-pay-mandarin-form-init');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('formInit', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('user', $data['data']);
        $this->assertEquals($customer_login, $data['data']['user']['login']);
        $this->assertArrayHasKey('deal', $data['data']);
        $this->assertEquals(self::DEAL_WITH_PARTIAL_PAYMENT_NAME, $data['data']['deal']['name']);
    }


    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}