<?php

namespace ModuleAcquiringMandarinTest\Service;

use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\User;
use Application\Service\InvoiceManager;
use Core\Service\ORMDoctrineUtil;
use ModuleAcquiringMandarin\Controller\CallbackHandleController;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use ModuleAcquiringMandarinTest\Bootstrap;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Class CallbackHandleControllerTest
 * @package ModulePaymentOrderTest\Service
 */
class CallbackHandleControllerTest extends AbstractHttpControllerTestCase
{
    const DEAL_WITH_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';

    const CARD_NUMBER_FOR_SUCCESS_TRANSACTION = '4929509947106878';
    const CARD_NUMBER_FOR_FAILED_TRANSACTION = '4485913187374384';

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var MandarinPayManager
     */
    private $mandarinPayManager;

    /**
     * @var MandarinPaymentOrderManager
     */
    private $mandarinPaymentOrderManager;

    /**
     * @var InvoiceManager
     */
    private $invoiceManager;

    /**
     * @var string
     */
    private $gateway_host;

    /**
     * @var integer
     */
    private $merchant_id;


    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $config = $serviceManager->get('config');

        $this->gateway_host = $config['mandarin']['gateway_host'];
        $this->merchant_id = $config['mandarin']['merchant_id'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->mandarinPayManager = $serviceManager->get(MandarinPayManager::class);
        $this->mandarinPaymentOrderManager = $serviceManager->get(MandarinPaymentOrderManager::class);
        $this->invoiceManager = $serviceManager->get(InvoiceManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // DB Transaction rollback
        ORMDoctrineUtil::rollBackTransaction($this->entityManager);

        parent::tearDown();

        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group pay
     * @group acquiring-mandarin
     */
    public function testHandlePayForSuccessTransaction()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertNotNull($payment);

        $this->assertNotNull($deal->getDateOfConfirm());

        // Создаём MandarinPaymentOrder для выплаты по сделке
        $price = $payment->getExpectedValue();
        $payment_purpose = $this->invoiceManager->generateInvoicePaymentPurpose($deal, true);

        // Создаем orderId, как зашифрованную комбинацию
        $order_id = $this->mandarinPayManager->createOrderId($deal, 'payment');
        $data = [
            'order_id' => $order_id,
            'price' => $price,
            'payment_purpose' => $payment_purpose,
        ];
        // Create MandarinPaymentOrder
        $this->mandarinPaymentOrderManager->createMandarinPaymentOrder(
            $deal,
            $data,
            PaymentOrderManager::TYPE_INCOMING
        );

        // Проверка созданных \ не созданных сущностей
        $paymentCreated = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(['deal' => $deal]);

        $this->assertGreaterThan(0, $paymentCreated->getPaymentOrders()->count());
        $this->assertInstanceOf(PaymentOrder::class, $paymentCreated->getPaymentOrders()->last());
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $paymentCreated->getPaymentOrders()->last();
        $this->assertInstanceOf(MandarinPaymentOrder::class, $paymentOrder->getMandarinPaymentOrder());
        /** @var MandarinPaymentOrder $mandarinPaymentOrder */
        $mandarinPaymentOrder = $paymentOrder->getMandarinPaymentOrder();
        $this->assertEquals(MandarinPaymentOrderManager::STATUS_PROCESSING, $mandarinPaymentOrder->getStatus());
        $this->assertEquals($paymentCreated->getExpectedValue(), $mandarinPaymentOrder->getAmount());
        $this->assertEquals(PaymentOrderManager::TYPE_INCOMING, $mandarinPaymentOrder->getType());
        $this->assertEquals(CallbackHandleController::ACTION_FOR_PAYMENT, $mandarinPaymentOrder->getAction());
        $this->assertNull($mandarinPaymentOrder->getTransaction());
        $this->assertNull($mandarinPaymentOrder->getCardHolder());
        $this->assertNull($mandarinPaymentOrder->getCardNumber());

        /** @var User $customerUser */
        $customerUser = $deal->getCustomer()->getUser();

        $postData = [
            'merchantId' => $this->merchant_id,
            'orderId' => $order_id, // Уникальный номер заказа в системе Продавца.
            'price' => $price, // Сумма платежа. Обязательна к передаче в текущей версии системы.
            'action' => 'pay', // (pay, payout) Действие (покупка или выплата на карту) - актуально только для транзакций
            'transaction' => '05991602ed7c47d996ca7ab119b07f66', // ID транзакции в системе - актуально только для транзакций
            'status' => 'success', // (success, failed, payout-only) Статус операции
            'customer_email' => $customerUser->getEmail()->getEmail(), // Email пользователя
            'customer_phone' => '+'.$customerUser->getPhone()->getPhoneNumber(), // Телефон пользователя
            'card_holder' => 'CARD HOLDER', // Держатель карты
            'card_number' => self::CARD_NUMBER_FOR_SUCCESS_TRANSACTION, // Номер карты
            'card_expiration_year' => '18', // Год срока действия карты
            'card_expiration_month' => '11', // Месяц срока действия карты
            #'error_code' => '59', // Код ошибки
            #'error_description' => 'Transaction Declined', // Описание ошибки
            'transaction_rrn' => '704191744222', // Retrieval Reference Number - уникальный идентифкатор транзакции в системе Банка-эмитента банковской карты
            'sign' => 'will_be_replaced'
        ];
        // Расчитываем и меняем sign
        $postData['sign'] = $this->mandarinPayManager->calculateSign($postData);

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/mandarin/callback/deal/pay', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        // Проверка ответа
        $this->assertArrayHasKey('content', $data);
        $this->assertEquals('OK', $data['content']);
        // Проверка созданных \ не созданных сущностей
        $paymentUpdated = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(['deal' => $deal]);

        $this->assertGreaterThan(0, $paymentUpdated->getPaymentOrders()->count());
        $this->assertInstanceOf(PaymentOrder::class, $paymentUpdated->getPaymentOrders()->last());
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $paymentUpdated->getPaymentOrders()->last();
        $this->assertInstanceOf(MandarinPaymentOrder::class, $paymentOrder->getMandarinPaymentOrder());
        /** @var MandarinPaymentOrder $mandarinPaymentOrder */
        $mandarinPaymentOrder = $paymentOrder->getMandarinPaymentOrder();
        $this->assertEquals(MandarinPaymentOrderManager::STATUS_SUCCESS, $mandarinPaymentOrder->getStatus());
        $this->assertEquals($paymentUpdated->getExpectedValue(), $mandarinPaymentOrder->getAmount());
        $this->assertEquals(PaymentOrderManager::TYPE_INCOMING, $mandarinPaymentOrder->getType());
        $this->assertEquals(CallbackHandleController::ACTION_FOR_PAYMENT, $mandarinPaymentOrder->getAction());
        $this->assertNotNull($mandarinPaymentOrder->getTransaction());
        $this->assertNotNull($mandarinPaymentOrder->getCardHolder());
        $this->assertNotNull($mandarinPaymentOrder->getCardNumber());
        $this->assertEquals($customerUser->getEmail()->getEmail(),$mandarinPaymentOrder->getCustomerEmail());
        $this->assertEquals('+'.$customerUser->getPhone()->getPhoneNumber(),$mandarinPaymentOrder->getCustomerPhone());
    }

    /**
     * Случай, когда асинхронный ответ приходит раньше, чем синхронный.
     * CallbackHandleController должен сайм создать MandarinPaymentOrder, если не находит по order_id
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group pay
     * @group acquiring-mandarin
     */
    public function testHandlePayForSuccessTransactionIfNoMandarinPaymentOrderExists()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertNotNull($payment);

        $this->assertNotNull($deal->getDateOfConfirm());

        // Создаём MandarinPaymentOrder для выплаты по сделке
        $price = $payment->getExpectedValue();

        // Создаем orderId, как зашифрованную комбинацию
        $order_id = $this->mandarinPayManager->createOrderId($deal, 'payment');


        /** @var User $customerUser */
        $customerUser = $deal->getCustomer()->getUser();

        $postData = [
            'merchantId' => $this->merchant_id,
            'orderId' => $order_id, // Уникальный номер заказа в системе Продавца.
            'price' => $price, // Сумма платежа. Обязательна к передаче в текущей версии системы.
            'action' => 'pay', // (pay, payout) Действие (покупка или выплата на карту) - актуально только для транзакций
            'transaction' => '05991602ed7c47d996ca7ab119b07f66', // ID транзакции в системе - актуально только для транзакций
            'status' => 'success', // (success, failed, payout-only) Статус операции
            'customer_email' => $customerUser->getEmail()->getEmail(), // Email пользователя
            'customer_phone' => '+'.$customerUser->getPhone()->getPhoneNumber(), // Телефон пользователя
            'card_holder' => 'CARD HOLDER', // Держатель карты
            'card_number' => self::CARD_NUMBER_FOR_SUCCESS_TRANSACTION, // Номер карты
            'card_expiration_year' => '2021', // Год срока действия карты
            'card_expiration_month' => '11', // Месяц срока действия карты
            #'error_code' => '59', // Код ошибки
            #'error_description' => 'Transaction Declined', // Описание ошибки
            'transaction_rrn' => '704191744222', // Retrieval Reference Number - уникальный идентифкатор транзакции в системе Банка-эмитента банковской карты
            'sign' => 'will_be_replaced'
        ];
        // Расчитываем и меняем sign
        $postData['sign'] = $this->mandarinPayManager->calculateSign($postData);

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/mandarin/callback/deal/pay', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        // Проверка ответа
        $this->assertArrayHasKey('content', $data);
        $this->assertEquals('OK', $data['content']);
        // Проверка созданных \ не созданных сущностей
        $paymentCreated = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(['deal' => $deal]);

        $this->assertGreaterThan(0, $paymentCreated->getPaymentOrders()->count());
        $this->assertInstanceOf(PaymentOrder::class, $paymentCreated->getPaymentOrders()->last());
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $paymentCreated->getPaymentOrders()->last();
        $this->assertInstanceOf(MandarinPaymentOrder::class, $paymentOrder->getMandarinPaymentOrder());
        /** @var MandarinPaymentOrder $mandarinPaymentOrder */
        $mandarinPaymentOrder = $paymentOrder->getMandarinPaymentOrder();
        $this->assertEquals(MandarinPaymentOrderManager::STATUS_SUCCESS, $mandarinPaymentOrder->getStatus());
        $this->assertEquals($paymentCreated->getExpectedValue(), $mandarinPaymentOrder->getAmount());
        $this->assertEquals(PaymentOrderManager::TYPE_INCOMING, $mandarinPaymentOrder->getType());
        $this->assertEquals(CallbackHandleController::ACTION_FOR_PAYMENT, $mandarinPaymentOrder->getAction());
        $this->assertNotNull($mandarinPaymentOrder->getTransaction());
        $this->assertNotNull($mandarinPaymentOrder->getCardHolder());
        $this->assertNotNull($mandarinPaymentOrder->getCardNumber());
        $this->assertEquals($customerUser->getEmail()->getEmail(),$mandarinPaymentOrder->getCustomerEmail());
        $this->assertEquals('+'.$customerUser->getPhone()->getPhoneNumber(),$mandarinPaymentOrder->getCustomerPhone());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group pay
     * @group acquiring-mandarin
     */
    public function testHandlePayForFailedTransaction()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertNotNull($payment);

        // Создаём MandarinPaymentOrder для выплаты по сделке
        $price = $payment->getExpectedValue();
        $payment_purpose = 'Оплата по сделке '.$deal->getNumber();
        // Создаем orderId, как зашифрованную комбинацию
        $order_id = $this->mandarinPayManager->createOrderId($deal, 'payoff');
        $data = [
            'order_id' => $order_id,
            'price' => $price,
            'payment_purpose' => $payment_purpose,
        ];
        // Create MandarinPaymentOrder
        $this->mandarinPaymentOrderManager->createMandarinPaymentOrder(
            $deal,
            $data,
            PaymentOrderManager::TYPE_INCOMING
        );

        // Проверка созданных \ не созданных сущностей
        $paymentCreated = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(['deal' => $deal]);

        $this->assertGreaterThan(0, $paymentCreated->getPaymentOrders()->count());
        $this->assertInstanceOf(PaymentOrder::class, $paymentCreated->getPaymentOrders()->last());
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $paymentCreated->getPaymentOrders()->last();
        $this->assertInstanceOf(MandarinPaymentOrder::class, $paymentOrder->getMandarinPaymentOrder());
        /** @var MandarinPaymentOrder $mandarinPaymentOrder */
        $mandarinPaymentOrder = $paymentOrder->getMandarinPaymentOrder();
        $this->assertEquals(MandarinPaymentOrderManager::STATUS_PROCESSING, $mandarinPaymentOrder->getStatus());
        $this->assertEquals($paymentCreated->getExpectedValue(), $mandarinPaymentOrder->getAmount());
        $this->assertEquals(PaymentOrderManager::TYPE_INCOMING, $mandarinPaymentOrder->getType());
        $this->assertEquals(CallbackHandleController::ACTION_FOR_PAYMENT, $mandarinPaymentOrder->getAction());
        $this->assertNull($mandarinPaymentOrder->getTransaction());
        $this->assertNull($mandarinPaymentOrder->getCardHolder());
        $this->assertNull($mandarinPaymentOrder->getCardNumber());

        $customerUser = $deal->getCustomer()->getUser();

        $postData = [
            'merchantId' => $this->merchant_id,
            'orderId' => $order_id, // Уникальный номер заказа в системе Продавца.
            'price' => $price, // Сумма платежа. Обязательна к передаче в текущей версии системы.
            'action' => 'pay', // (pay, payout) Действие (покупка или выплата на карту) - актуально только для транзакций
            'transaction' => '05991602ed7c47d996ca7ab119b07f66', // ID транзакции в системе - актуально только для транзакций
            'status' => 'failed', // (success, failed, payout-only) Статус операции
            'customer_email' => $customerUser->getEmail()->getEmail(), // Email пользователя
            'customer_phone' => '+'.$customerUser->getPhone()->getPhoneNumber(), // Телефон пользователя
            'card_holder' => 'CARD HOLDER', // Держатель карты
            'card_number' => self::CARD_NUMBER_FOR_FAILED_TRANSACTION, // Номер карты
            'card_expiration_year' => '18', // Год срока действия карты
            'card_expiration_month' => '11', // Месяц срока действия карты
            'error_code' => '59', // Код ошибки
            'error_description' => 'Transaction Declined', // Описание ошибки
            'transaction_rrn' => '704191744222', // Retrieval Reference Number - уникальный идентифкатор транзакции в системе Банка-эмитента банковской карты
            'sign' => 'will_be_replaced'
        ];
        // Расчитываем и меняем sign
        $postData['sign'] = $this->mandarinPayManager->calculateSign($postData);

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/mandarin/callback/deal/pay', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        // Проверка ответа
        $this->assertArrayHasKey('content', $data);
        $this->assertEquals('OK', $data['content']);

        // Проверка созданных \ не созданных сущностей
        $paymentUpdated = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(['deal' => $deal]);

        $this->assertGreaterThan(0, $paymentUpdated->getPaymentOrders()->count());
        $this->assertInstanceOf(PaymentOrder::class, $paymentUpdated->getPaymentOrders()->last());
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $paymentUpdated->getPaymentOrders()->last();
        $this->assertInstanceOf(MandarinPaymentOrder::class, $paymentOrder->getMandarinPaymentOrder());
        /** @var MandarinPaymentOrder $mandarinPaymentOrder */
        $mandarinPaymentOrder = $paymentOrder->getMandarinPaymentOrder();
        $this->assertEquals(MandarinPaymentOrderManager::STATUS_FAILED, $mandarinPaymentOrder->getStatus());
        $this->assertEquals($paymentUpdated->getExpectedValue(), $mandarinPaymentOrder->getAmount());
        $this->assertEquals(PaymentOrderManager::TYPE_INCOMING, $mandarinPaymentOrder->getType());
        $this->assertEquals(CallbackHandleController::ACTION_FOR_PAYMENT, $mandarinPaymentOrder->getAction());
        $this->assertNotNull($mandarinPaymentOrder->getTransaction());
        $this->assertNotNull($mandarinPaymentOrder->getCardHolder());
        $this->assertNotNull($mandarinPaymentOrder->getCardNumber());
        $this->assertEquals($customerUser->getEmail()->getEmail(), $mandarinPaymentOrder->getCustomerEmail());
        $this->assertEquals('+'.$customerUser->getPhone()->getPhoneNumber(), $mandarinPaymentOrder->getCustomerPhone());
        $this->assertNotNull($mandarinPaymentOrder->getErrorCode());
        $this->assertNotNull($mandarinPaymentOrder->getErrorDescription());
    }
}