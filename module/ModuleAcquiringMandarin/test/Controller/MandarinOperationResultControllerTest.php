<?php

namespace ModuleAcquiringMandarinTest\HTMLOutput;

use Application\Entity\Payment;
use ModuleAcquiringMandarin\Controller\CallbackHandleController;
use ModuleAcquiringMandarin\Controller\MandarinOperationResultController;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use Core\Service\Base\BaseAuthManager;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Entity\PaymentOrder;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\Deal;
use Zend\Form\Element;

/**
 * Class MandarinOperationResultControllerTest
 * @package ModuleAcquiringMandarinTest\HTMLOutput
 */
class MandarinOperationResultControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DEAL_WITH_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';
    const DEAL_WITH_STATUS_NEGOTIATION_NAME = 'Сделка Negotiation';
    const DEAL_WITH_STATUS_DEBIT_MATCHED_NAME = 'Сделка Debit Matched';
    const DEAL_WITH_PARTIAL_PAYMENT_NAME = 'Сделка Частично оплачено';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var MandarinPayManager
     */
    private $mandarinPayManager;

    /**
     * @var MandarinPaymentOrderManager
     */
    private $mandarinPaymentOrderManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->mandarinPayManager = $serviceManager->get(MandarinPayManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// ResultAction

    /**
     * ResultAction со status='success' НЕ авторизованному Пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testResultActionForNotAuthorized()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        $orderId = $this->mandarinPayManager->createOrderId($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);

        $getData = [
            'id' => 'be96dbbd9fdf48c6b9b659fa4d804dac',
            'status' => 'success',
            'orderId' => $orderId,
            'type' => CallbackHandleController::ACTION_FOR_PAYMENT
        ];

        $this->dispatch('/mandarin/operation-result', 'GET', $getData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationResultController::class);
        $this->assertMatchedRouteName('mandarin-operation-result');
        $this->assertRedirectTo('/login?redirectUrl=/mandarin/operation-result');

        // Проверка созданных \ не созданных сущностей
        $paymentUpdated = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(['deal' => $deal]);

        $this->assertEquals(0, $paymentUpdated->getPaymentOrders()->count());
    }

    /**
     * ResultAction со status='success' авторизованному Пользователю
     * В итоге должна создасться платёжка MandarinPaymentOrder со статусом 'processing'
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testResultActionWithSuccessResult()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        // Залогиниваем Кастомера
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $orderId = $this->mandarinPayManager->createOrderId($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);

        $getData = [
            'id' => 'be96dbbd9fdf48c6b9b659fa4d804dac',
            'status' => 'success',
            'orderId' => $orderId,
            'type' => CallbackHandleController::ACTION_FOR_PAYMENT
        ];

        $this->dispatch('/mandarin/operation-result', 'GET', $getData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationResultController::class);
        $this->assertMatchedRouteName('mandarin-operation-result');
        $this->assertRedirectTo('/deal/show/'.$deal->getId());

        // Проверка созданных \ не созданных сущностей
        $paymentUpdated = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(['deal' => $deal]);

        $this->assertGreaterThan(0, $paymentUpdated->getPaymentOrders()->count());
        $this->assertInstanceOf(PaymentOrder::class, $paymentUpdated->getPaymentOrders()->last());
        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $paymentUpdated->getPaymentOrders()->last();
        $this->assertInstanceOf(MandarinPaymentOrder::class, $paymentOrder->getMandarinPaymentOrder());
        /** @var MandarinPaymentOrder $mandarinPaymentOrder */
        $mandarinPaymentOrder = $paymentOrder->getMandarinPaymentOrder();
        $this->assertEquals(MandarinPaymentOrderManager::STATUS_PROCESSING, $mandarinPaymentOrder->getStatus());
        $this->assertEquals($paymentUpdated->getExpectedValue(), $mandarinPaymentOrder->getAmount());
        $this->assertEquals(PaymentOrderManager::TYPE_INCOMING, $mandarinPaymentOrder->getType());
        $this->assertEquals(CallbackHandleController::ACTION_FOR_PAYMENT, $mandarinPaymentOrder->getAction());
        $this->assertNull($mandarinPaymentOrder->getTransaction());
        $this->assertNull($mandarinPaymentOrder->getCardHolder());
        $this->assertNull($mandarinPaymentOrder->getCardNumber());
    }

    /**
     * ResultAction со status='failed' авторизованному Пользователю
     * В итоге НЕ должна создасться платёжка MandarinPaymentOrder
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     *
     * @expectedExceptionMessage
     */
    public function testResultActionWithFailedResult()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        // Залогиниваем Кастомера
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $orderId = $this->mandarinPayManager->createOrderId($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);

        $getData = [
            'id' => 'be96dbbd9fdf48c6b9b659fa4d804dac',
            'status' => 'failed',
            'orderId' => $orderId,
            'type' => CallbackHandleController::ACTION_FOR_PAYMENT
        ];

        $this->dispatch('/mandarin/operation-result', 'GET', $getData);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationResultController::class);
        $this->assertMatchedRouteName('mandarin-operation-result');

        // Проверка созданных \ не созданных сущностей
        $paymentUpdated = $this->entityManager->getRepository(Payment::class)
            ->findOneBy(['deal' => $deal]);

        $this->assertEquals(0, $paymentUpdated->getPaymentOrders()->count());
    }

    // @TODO Добавить тест для попытки повторного добавления MandarinPaymentOrder с тем же orderId


    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}