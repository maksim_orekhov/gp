<?php

namespace ModuleAcquiringMandarinTest\HTMLOutput;

use Application\Entity\Payment;
use ModuleAcquiringMandarin\Controller\MandarinOperationStatusController;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use Core\Service\Base\BaseAuthManager;
use ModulePaymentOrder\Entity\MandarinPaymentOrder;
use ModulePaymentOrder\Service\MandarinPaymentOrderManager;
use ModulePaymentOrder\Service\PaymentOrderManager;
use ModulePaymentOrder\Service\PayOffManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\Deal;
use Zend\Form\Element;

/**
 * Class MandarinOperationStatusControllerTest
 * @package ModuleAcquiringMandarinTest\HTMLOutput
 *
 * Тесты запускать на чистых (не измененных) фикстурах!
 */
class MandarinOperationStatusControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DEAL_WITH_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';
    const DEAL_WITH_STATUS_NEGOTIATION_NAME = 'Сделка Negotiation';
    const DEAL_WITH_STATUS_DEBIT_MATCHED_NAME = 'Сделка Debit Matched';
    const DEAL_WITH_PARTIAL_PAYMENT_NAME = 'Сделка Частично оплачено';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var MandarinPayManager
     */
    private $mandarinPayManager;

    /**
     * @var MandarinPaymentOrderManager
     */
    private $mandarinPaymentOrderManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(BaseAuthManager::class);
        $this->mandarinPayManager = $serviceManager->get(MandarinPayManager::class);
        $this->mandarinPaymentOrderManager = $serviceManager->get(MandarinPaymentOrderManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /////////////// CheckAction

    /**
     * CheckAction НЕ авторизованному Пользователю
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testCheckActionForNotAuthorized()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        $orderId = $this->mandarinPayManager->createOrderId($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);

        $postData = [
            'orderId' => $orderId,
        ];

        $this->dispatch('/mandarin/check', 'POST', $postData);

        $this->assertResponseStatusCode(302);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationStatusController::class);
        $this->assertMatchedRouteName('mandarin-operation-check');
        $this->assertRedirectTo('/login?redirectUrl=/mandarin/check');
    }

    /**
     * Ajax
     * CheckAction НЕ авторизованному Пользователю для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testCheckActionForNotAuthorizedForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        $orderId = $this->mandarinPayManager->createOrderId($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);

        $postData = [
            'orderId' => $orderId,
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/mandarin/check', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationStatusController::class);
        $this->assertMatchedRouteName('mandarin-operation-check');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('User not authorized.', $data['message']);
    }

    /**
     * CheckAction Кастомеру без POST
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     *
     * @expectedExceptionMessage No orderId given
     */
    public function testCheckActionWithoutPostData()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        // Залогиниваем Кастомера
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $this->dispatch('/mandarin/check', 'GET');

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationStatusController::class);
        $this->assertMatchedRouteName('mandarin-operation-check');

        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals('An error occurred during execution; please try again later.', $view_vars['message']);
        $this->assertArrayHasKey('exception', $view_vars);
        /** @var \Throwable $exception */
        $exception = $view_vars['exception'];
        $this->assertEquals('No orderId given', $exception->getMessage());
    }

    /**
     * Ajax
     * CheckAction Кастомеру без POST для Ajax
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testCheckActionWithoutPostDataForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        // Залогиниваем Кастомера
        $this->login($deal->getCustomer()->getUser()->getLogin());

        $isXmlHttpRequest = true;
        $this->dispatch('/mandarin/check', 'GET', [], $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(500);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationStatusController::class);
        $this->assertMatchedRouteName('mandarin-operation-check');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('An error occurred during execution; please try again later.', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('message', $data['data']);
        $this->assertEquals('No orderId given', $data['data']['message']);
    }

    /**
     * CheckAction Котрактору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testCheckActionForContractor()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        // Залогиниваем Контрактора
        $this->login($deal->getContractor()->getUser()->getLogin());

        $orderId = $this->mandarinPayManager->createOrderId($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);

        $postData = [
            'orderId' => $orderId,
        ];

        $this->dispatch('/mandarin/check', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationStatusController::class);
        $this->assertMatchedRouteName('mandarin-operation-check');
        $this->assertArrayHasKey('message', $view_vars);
        $this->assertEquals('Forbidden check MandarinPaymentOrder status for this orderId', $view_vars['message']);
    }

    /**
     * Ajax
     * CheckAction Котрактору
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     */
    public function testCheckActionForContractorForAjax()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        // Залогиниваем Контрактора
        $this->login($deal->getContractor()->getUser()->getLogin());

        $orderId = $this->mandarinPayManager->createOrderId($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);

        $postData = [
            'orderId' => $orderId,
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/mandarin/check', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationStatusController::class);
        $this->assertMatchedRouteName('mandarin-operation-check');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('ERROR', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('Forbidden check MandarinPaymentOrder status for this orderId', $data['message']);
    }


    /**
     * @return array
     */
    public function givenMandarinPaymentOrderStatuses(): array
    {
        return [
            [MandarinPaymentOrderManager::STATUS_PROCESSING, true],
            [MandarinPaymentOrderManager::STATUS_SUCCESS, true],
            [MandarinPaymentOrderManager::STATUS_FAILED, true],
        ];
    }

    /**
     * CheckAction для разных статусов платежа
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     *
     * @dataProvider givenMandarinPaymentOrderStatuses
     */
    public function testCheckActionForVariousStatuses($given_status, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertNotNull($payment);

        // Залогиниваем Кастомера
        $this->login($deal->getCustomer()->getUser()->getLogin());

        // Создаём MandarinPaymentOrder для выплаты по сделке
        $price = $payment->getExpectedValue();
        $payment_purpose = 'Оплата по сделке '.$deal->getNumber();
        // Создаем orderId, как зашифрованную комбинацию
        $order_id = $this->mandarinPayManager->createOrderId($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);
        $data = [
            'order_id' => $order_id,
            'price' => $price,
            'payment_purpose' => $payment_purpose,
        ];
        // Create MandarinPaymentOrder
        /** @var MandarinPaymentOrder $mandarinPaymentOrder */
        $mandarinPaymentOrder = $this->mandarinPaymentOrderManager->createMandarinPaymentOrder(
            $deal,
            $data,
            PaymentOrderManager::TYPE_INCOMING
        );

        $this->assertNotNull($mandarinPaymentOrder);

        if ($given_status !== MandarinPaymentOrderManager::STATUS_PROCESSING) {
            $this->setStatus($mandarinPaymentOrder, $given_status);
        }

        $postData = [
            'orderId' => $mandarinPaymentOrder->getOrderId(),
        ];

        $this->dispatch('/mandarin/check', 'POST', $postData);

        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationStatusController::class);
        $this->assertMatchedRouteName('mandarin-operation-check');
        // Что отдается в шаблон
        $this->assertArrayHasKey('mandarinPaymentOrder', $view_vars);
        #$this->assertEquals($mandarinPaymentOrder->getOrderId(), $view_vars['mandarinPaymentOrder']['orderId']);
        $this->assertEquals($given_status, $view_vars['mandarinPaymentOrder']['status']);
        // Возвращается из шаблона
        $this->assertQuery('#MandarinOperationCheckPage');
    }

    /**
     * Ajax
     * CheckAction для разных статусов платежа
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     *
     * @dataProvider givenMandarinPaymentOrderStatuses
     */
    public function testCheckActionForSuccessStatusForAjax($given_status, $expected)
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertNotNull($payment);

        // Залогиниваем Кастомера
        $this->login($deal->getCustomer()->getUser()->getLogin());

        // Создаём MandarinPaymentOrder для выплаты по сделке
        $price = $payment->getExpectedValue();
        $payment_purpose = 'Оплата по сделке '.$deal->getNumber();
        // Создаем orderId, как зашифрованную комбинацию
        $order_id = $this->mandarinPayManager->createOrderId($deal, PayOffManager::PURPOSE_TYPE_PAYOFF);
        $data = [
            'order_id' => $order_id,
            'price' => $price,
            'payment_purpose' => $payment_purpose,
        ];
        // Create MandarinPaymentOrder
        /** @var MandarinPaymentOrder $mandarinPaymentOrder */
        $mandarinPaymentOrder = $this->mandarinPaymentOrderManager->createMandarinPaymentOrder(
            $deal,
            $data,
            PaymentOrderManager::TYPE_INCOMING
        );

        $this->assertNotNull($mandarinPaymentOrder);

        if ($given_status !== MandarinPaymentOrderManager::STATUS_PROCESSING) {
            $this->setStatus($mandarinPaymentOrder, $given_status);
        }

        $postData = [
            'orderId' => $mandarinPaymentOrder->getOrderId(),
        ];

        $this->getRequest()->setContent(json_encode($postData));
        $isXmlHttpRequest = true;
        $this->dispatch('/mandarin/check', 'POST', $postData, $isXmlHttpRequest);
        $data = json_decode($this->getResponse()->getContent(), true);

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleAcquiringMandarin');
        $this->assertControllerName(MandarinOperationStatusController::class);
        $this->assertMatchedRouteName('mandarin-operation-check');
        $this->assertArrayHasKey('status', $data);
        $this->assertEquals('SUCCESS', $data['status']);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('MandarinPaymentOrder status', $data['message']);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('status', $data['data']);
        $this->assertEquals($given_status, $data['data']['status']);
    }

    /**
     * @param MandarinPaymentOrder $mandarinPaymentOrder
     * @param string $status
     */
    private function setStatus(MandarinPaymentOrder $mandarinPaymentOrder, string $status)
    {
        $mandarinPaymentOrder->setStatus($status);

        $this->entityManager->persist($mandarinPaymentOrder);
        $this->entityManager->flush();
    }

    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}