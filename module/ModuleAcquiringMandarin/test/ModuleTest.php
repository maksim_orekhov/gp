<?php

namespace ModuleAcquiringMandarinTest;

use ModuleAcquiringMandarin\Module;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;

class ModuleTest extends AbstractHttpControllerTestCase
{
    const CONFIG_FILES_NAMES = [
        'assets.config.php',
        'mandarin.config.php',
        'module.config.php',
        'router.config.php',
        'views.config.php',
    ];

    private $moduleRoot = null;

    protected function setUp()
    {
        $this->moduleRoot = realpath(__DIR__ . '/../');
    }

    /**
     * @group module
     */
    public function testGetConfig()
    {
        $expectedConfig = [];
        foreach(self::CONFIG_FILES_NAMES as $file_name) {
            $config = include $this->moduleRoot . '/config/' . $file_name;
            $expectedConfig = array_replace_recursive($expectedConfig, $config);
        }

        $module = new Module();
        $configData = $module->getConfig();

        $this->assertEquals($expectedConfig, $configData);
    }
}