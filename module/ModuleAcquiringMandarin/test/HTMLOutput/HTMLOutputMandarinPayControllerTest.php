<?php

namespace ModuleAcquiringMandarinTest\HTMLOutput;

use Application\Entity\Payment;
use Core\Service\Base\BaseAuthManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;
use Zend\Stdlib\ArrayUtils;
use Application\Entity\Deal;
use Zend\Form\Element;

/**
 * Class HTMLOutputMandarinPayControllerTest
 * @package ApplicationTest\HTMLOutput
 */
class HTMLOutputMandarinPayControllerTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    const DEAL_WITH_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            include __DIR__ . '/../../../../config/autoload/global.php'
        ));

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /**
     * Тестируем доступность PayAction Пользователю
     * @Important! Только на неоплаченную сделку (неизмененные фикстуры). Иначе - mandarinPayPolitics не пропустит
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group acquiring-mandarin
     * @group HTMLOutput
     */
    public function testPayAction()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        // Залогиниваем пользователя test
        $this->login('test');

        $this->assertNotNull($deal);

        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $postData = [
            'csrf' => $csrf
        ];

        $this->dispatch('/deal/'.$deal->getId().'/pay/mandarin', 'POST', $postData);

        // Проверяем что нужные блоки есть в DOM
        $this->assertQuery('#CardMandarinPayPage');
        // При включенном 'hosted_fields' в конфиге
        $this->assertQuery('#form-hosted-pay');
    }


    /**
     * Залогиниваем пользователя
     *
     * @param string|null $login
     * @param string|null $password
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    private function login(string $login=null, string $password=null)
    {
        if(!$login) $login = $this->user_login;
        if(!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}