<?php

namespace ModulePaymentOrderTest\Service;

use ModuleAcquiringMandarin\Controller\CallbackHandleController;
use ModuleAcquiringMandarin\Service\CallbackHandlePolitics;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;
use CoreTest\CallPrivateMethodTrait;

/**
 * Class CallbackHandlePoliticsTest
 * @package ModulePaymentOrderTest\Service
 */
class CallbackHandlePoliticsTest extends AbstractHttpControllerTestCase
{
    use CallPrivateMethodTrait;

    const DEAL_WITH_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var MandarinPayManager
     */
    private $mandarinPayManager;

    /**
     * @var CallbackHandlePolitics
     */
    private $callbackHandlePolitics;

    /**
     * @var string
     */
    private $gateway_host;

    /**
     * @var integer
     */
    private $merchant_id;

    /**
     * @var string
     */
    private $secret_key;


    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        //$configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                include __DIR__ . '/../../../../config/application.config.php',
                include __DIR__ . '/../../../../config/autoload/global.php'
            ),
            include __DIR__ . '/../../config/mandarin.config.php'
        ));

        parent::setUp();

        $config = $this->getApplicationConfig();
        $this->secret_key = $config['mandarin']['secret_key'];
        $this->gateway_host = $config['mandarin']['gateway_host'];
        $this->merchant_id = $config['mandarin']['merchant_id'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->callbackHandlePolitics = $serviceManager->get(CallbackHandlePolitics::class);
        $this->mandarinPayManager = $serviceManager->get(MandarinPayManager::class);

        // Transaction start
        #$this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        #$this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group pay
     * @group acquiring-mandarin
     */
    public function testCheckStatus()
    {
        $orderId = 'Unit_test_eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';

        $failed_status = CallbackHandlePolitics::FAILED_STATUS;
        $success_status = CallbackHandlePolitics::SUCCESS_STATUS;

        $result_1 = $this->invokeMethod($this->callbackHandlePolitics, 'checkStatus', array($failed_status, $orderId));
        $result_2 = $this->invokeMethod($this->callbackHandlePolitics, 'checkStatus', array($success_status, $orderId));

        $this->assertFalse($result_1);
        $this->assertTrue($result_2);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group pay
     * @group acquiring-mandarin
     */
    public function testCheckMerchantId()
    {
        $orderId = 'Unit_test_eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';

        $incorrect_merchant_id = 111;
        $correct_merchant_id = $this->merchant_id;

        $result_1 = $this->invokeMethod($this->callbackHandlePolitics, 'checkMerchantId', array($incorrect_merchant_id, $orderId));
        $result_2 = $this->invokeMethod($this->callbackHandlePolitics, 'checkMerchantId', array($correct_merchant_id, $orderId));

        $this->assertFalse($result_1);
        $this->assertTrue($result_2);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group pay
     * @group acquiring-mandarin
     */
    public function testCheckAction()
    {
        $orderId = 'Unit_test_eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';

        $expected_action = CallbackHandleController::ACTION_FOR_PAYMENT;
        $incorrect_callback_action = 'payout';
        $correct_callback_action = CallbackHandleController::ACTION_FOR_PAYMENT;

        $result_1 = $this->invokeMethod(
            $this->callbackHandlePolitics,
            'checkAction',
            [$incorrect_callback_action, $expected_action, $orderId]
        );
        $result_2 = $this->invokeMethod(
            $this->callbackHandlePolitics,
            'checkAction',
            [$correct_callback_action, $expected_action, $orderId]
        );

        $this->assertFalse($result_1);
        $this->assertTrue($result_2);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group pay
     * @group acquiring-mandarin
     */
    public function testCheckSign()
    {
        $data_1 = [
            'f' => 6,
            'с' => 3,
            'a' => 1,
            'd' => 4,
            'g' => 7,
            'e' => 5,
            'b' => 2,
            'orderId' => 'Unit_test_eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9'
        ];

        $data_2 = $data_1;

        // Получаем sign БЕЗ СОРТИРОВКИ ключей по алфавиту
        $secret_t = '';
        foreach ($data_1 as $key => $val) {
            $secret_t = $secret_t . '-' . $val;
        }
        $secret_t = substr($secret_t, 1) . '-' . $this->secret_key;
        $sign_1 =  hash('sha256', $secret_t);
        $data_1['sign'] = $sign_1;

        // Получаем sign С СОРТИРОВКОЙ ключей по алфавиту
        ksort($data_2);
        $sign_2 = $this->mandarinPayManager->calculateSign($data_2);
        $data_2['sign'] = $sign_2;

        $result_1 = $this->invokeMethod($this->callbackHandlePolitics, 'checkSign', array($data_1));
        $result_2 = $this->invokeMethod($this->callbackHandlePolitics, 'checkSign', array($data_2));

        $this->assertFalse($result_1);
        $this->assertTrue($result_2);
    }
}