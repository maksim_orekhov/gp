<?php

namespace ModulePaymentOrderTest\Service;

use Application\Entity\Deal;
use Application\Entity\Payment;
use Application\Entity\User;
use ModuleAcquiringMandarin\Service\MandarinPayManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;

/**
 * Class MandarinPayManagerTest
 * @package ModulePaymentOrderTest\Service
 */
class MandarinPayManagerTest extends AbstractHttpControllerTestCase
{
    const DEAL_WITH_STATUS_CONFIRMED_NAME = 'Сделка Соглашение достигнуто';

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var MandarinPayManager
     */
    private $mandarinPayManager;

    /**
     * @var string
     */
    private $gateway_host;

    /**
     * @var string
     */
    private $secret_key;


    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        //$configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            ArrayUtils::merge(
                include __DIR__ . '/../../../../config/application.config.php',
                include __DIR__ . '/../../../../config/autoload/global.php'
            ),
            include __DIR__ . '/../../config/mandarin.config.php'
        ));

        parent::setUp();

        $config = $this->getApplicationConfig();
        $this->secret_key = $config['mandarin']['secret_key'];
        $this->gateway_host = $config['mandarin']['gateway_host'];

        $serviceManager = $this->getApplicationServiceLocator();
        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->mandarinPayManager = $serviceManager->get(MandarinPayManager::class);

        // Transaction start
        $this->entityManager->getConnection()->beginTransaction();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function tearDown() {
        // Transaction rollback
        $this->entityManager->getConnection()->rollBack();

        parent::tearDown();

        // Закрываем соединение (чтобы избежать ошибки "to many connections" - GP-135)
        $this->entityManager->getConnection()->close();
        $this->entityManager = null;

        gc_collect_cycles();
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group pay
     * @group acquiring-mandarin
     */
    public function testCreatePaymentTransaction()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertNotNull($payment);

        $price = $payment->getExpectedValue();
        $current_day = new \DateTime();
        $order_id = $deal->getId().'-'.$current_day->getTimestamp();
        /** @var User $customerUser */
        $customerUser = $deal->getCustomer()->getUser();

        $result = $this->mandarinPayManager->createPaymentTransaction($order_id, $price, $customerUser);

        $this->assertNotNull($result->id);
        $this->assertNotNull($result->userWebLink);
        $this->assertEquals($this->gateway_host . 'Pay/Select?transaction=' . $result->id, $result->userWebLink);
        $this->assertNotNull($result->jsOperationId);
        $this->assertEquals($result->id, $result->jsOperationId);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group pay
     * @group acquiring-mandarin
     */
    public function testCreateOrderId()
    {
        /** @var Deal $deal */
        $deal = $this->entityManager->getRepository(Deal::class)
            ->findOneBy(['name' => self::DEAL_WITH_STATUS_CONFIRMED_NAME]);

        $this->assertNotNull($deal);

        $current_date = new \DateTime();
        $current_date_timestamp = $current_date->getTimestamp();

        /** @var Payment $payment */
        $payment = $deal->getPayment();

        $this->assertNotNull($payment);

        $order_id = $this->mandarinPayManager->createOrderId($deal, 'payment');

        $decoded_order_id = $this->mandarinPayManager->decodeEncodedOrderId($order_id);

        $this->assertEquals($deal->getId(), $decoded_order_id->deal_id);
        $this->assertEquals($current_date_timestamp, (int)$decoded_order_id->timestamp);
        $this->assertEquals('payment', $decoded_order_id->payment_purpose_type);
        $this->assertEquals(0, $decoded_order_id->nds_type);
    }
}