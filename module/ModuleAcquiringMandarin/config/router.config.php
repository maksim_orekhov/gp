<?php
namespace ModuleAcquiringMandarin;

use ModuleAcquiringMandarin\Controller\CallbackHandleController;
use ModuleAcquiringMandarin\Controller\MandarinOperationResultController;
use ModuleAcquiringMandarin\Controller\MandarinOperationStatusController;
use ModuleAcquiringMandarin\Controller\MandarinPayController;
use Zend\Router\Http\Segment;

// CallbackHandleController
$mandarin_callback_pay = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/mandarin/callback/deal/pay',
        'defaults' => [
            'controller' => CallbackHandleController::class,
            'action' => 'handlePay',
        ],
    ]
];
// MandarinOperationResultController
$mandarin_operation_result = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/mandarin/operation-result',
        'defaults' => [
            'controller' => MandarinOperationResultController::class,
            'action' => 'result',
        ],
    ]
];
// MandarinOperationStatusController
$mandarin_operation_check = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/mandarin/check',
        'defaults' => [
            'controller' => MandarinOperationStatusController::class,
            'action' => 'check',
        ],
    ]
];
// MandarinGetOrderId
$mandarin_get_order_id = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/mandarin/order-id',
        'defaults' => [
            'controller' => MandarinOperationStatusController::class,
            'action' => 'orderId',
        ],
    ]
];
// MandarinGetOrderId
$mandarin_get_hash_operation_id = [
    'type' => Segment::class,
    'options' => [
        'route'    => '/mandarin/hash-operation-id',
        'defaults' => [
            'controller' => MandarinPayController::class,
            'action' => 'hashOperationId',
        ],
    ]
];
return [
    'router' => [
        'routes' => [
            'mandarin-operation-result' => $mandarin_operation_result,
            'mandarin-callback-pay' => $mandarin_callback_pay,
            'mandarin-operation-check' => $mandarin_operation_check,
            'mandarin-get-order-id' => $mandarin_get_order_id,
            'mandarin-get-hash-operation-id' => $mandarin_get_hash_operation_id,
        ],
    ],
];