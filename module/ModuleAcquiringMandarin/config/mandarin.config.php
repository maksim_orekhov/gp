<?php
namespace ModuleAcquiringMandarin;

return [
    'mandarin' => [
        #'merchant_id'   => '940', // в конфигах окружения congif/environment/
        #'secret_key'    => 'Cm0uUN6L0H', // в конфигах окружения congif/environment/
        'gateway_host'  => 'https://secure.mandarinpay.com/',
        'hosted_fields' => true, // Форма отображается в интерфейсе GP, иначе перекидывает на страницу Mandarin
        'api_request_simulation' => false
    ],
];