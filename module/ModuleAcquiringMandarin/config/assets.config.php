<?php
namespace ModuleAcquiringMandarin;

return [
    // The 'access_filter' key is used by the Application module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            'mode' => 'permissive',     // все, что не запрещено, разрешено
            //'mode' => 'restrictive',  // все, что не разрешено, запрещено
        ],
        'controllers' => [
            Controller\MandarinPayController::class => [
                [
                    'actions' => [
                        'pay',
                        'formInit'
                    ],
                    'allows' => ['#Verified']
                ],
            ],
            Controller\MandarinOperationResultController::class => [
                [
                    'actions' => [
                        'result',
                    ],
                    'allows' => ['#Verified']
                ],
            ],
            Controller\MandarinOperationStatusController::class => [
                [
                    'actions' => [
                        'check',
                        'orderId',
                    ],
                    'allows' => ['#Verified']
                ],
            ],
        ],
    ],
];