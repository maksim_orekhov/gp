<?php
namespace ModuleAcquiringMandarin;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\MandarinPayController::class => Controller\Factory\MandarinPayControllerFactory::class,
            Controller\MandarinOperationResultController::class => Controller\Factory\MandarinOperationResultControllerFactory::class,
            Controller\MandarinOperationStatusController::class => Controller\Factory\MandarinOperationStatusControllerFactory::class,
            Controller\CallbackHandleController::class => Controller\Factory\CallbackHandleControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\MandarinPayManager::class => Service\Factory\MandarinPayManagerFactory::class,
            Service\MandarinPayPolitics::class => InvokableFactory::class,
            Service\CallbackHandlePolitics::class => Service\Factory\CallbackHandlePoliticsFactory::class,
        ],
    ],
];
