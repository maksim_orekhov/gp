<?php
namespace ModuleBank;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\BankController::class => Controller\Factory\BankControllerFactory::class,
        ],
    ],
    'controller_plugins' => [
        'factories' => [
            // ...code
        ],
        'aliases' => [
            // ...code
        ],
    ],
];
