<?php
namespace ModuleBank;


return [
    'service_manager' => [
        'factories' => [
            Service\BankManager::class => Service\Factory\BankManagerFactory::class,
        ],
    ],
];
