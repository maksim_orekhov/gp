<?php
namespace ModuleBank\Controller\Factory;

use Interop\Container\ContainerInterface;
use ModuleBank\Controller\BankController;
use ModuleBank\Service\BankManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class BankControllerFactory
 * @package ModuleBank\Controller\Factory
 */
class BankControllerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BankController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $bankManager = $container->get(BankManager::class);

        return new BankController($bankManager);
    }
}