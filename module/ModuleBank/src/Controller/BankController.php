<?php

namespace ModuleBank\Controller;

use ModuleBank\Service\BankManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;



/**
 * Class BankController
 * @package ModuleBank\Controller
 */
class BankController extends AbstractActionController
{
    /**
     * @var BankManager
     */
    private $bankManager;

    /**
     * BankController constructor.
     * @param BankManager $bankManager
     */
    public function __construct(BankManager $bankManager)
    {
        $this->bankManager = $bankManager;
    }

    /**
     * @return JsonModel
     */
    public function bankUpdateAction()
    {
        try {
            $result = $this->bankManager->updateBankBase();
        }catch (\Throwable $t){
            return new JsonModel([
                'status'    => 'ERROR',
                'message'   => $t->getMessage(),
                'data'      => null
            ]);
        }
        return new JsonModel($result);
    }
    /**
     * @return JsonModel
     */
    public function bankInfoAction()
    {
        try {
            $bikQueryData = $this->params()->fromQuery('bik', null);

            $bank = $this->bankManager->getBankByBik($bikQueryData);
            $bankOutput = [];
            if($bank) {
                $bankOutput = $this->bankManager->getBankOutput($bank);
            }

        }catch (\Throwable $t){
            return new JsonModel([
                'status'    => 'ERROR',
                'message'   => $t->getMessage(),
                'data'      => null
            ]);
        }

        if(empty($bankOutput)) {
            $result = [
                'status'    => 'ERROR',
                'message'   => 'information on BIK not found',
                'data'      => null
            ];
        } else {
            $result = [
                'status'    => 'SUCCESS',
                'message'   => 'information on BIK found successfully',
                'data'      => $bankOutput
            ];
        }

        return new JsonModel($result);
    }
}