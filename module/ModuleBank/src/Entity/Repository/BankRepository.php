<?php
namespace ModuleBank\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class BankRepository
 * @package ModuleBank\Entity\Repository
 */
class BankRepository extends EntityRepository
{
    public function selectBankWithMaxDate()
    {
        $qb = $this->createQueryBuilder('b');
        $qb->orderBy('b.created', 'desc');
        $qb->setMaxResults(1);
        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}