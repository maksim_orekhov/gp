<?php
namespace ModuleBank\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Bank
 *
 * @ORM\Table(name="bank")
 * @ORM\Entity(repositoryClass="ModuleBank\Entity\Repository\BankRepository");
 */
class Bank
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reality", type="string", length=255, nullable=true)
     */
    private $reality;//real

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name; //namep

    /**
     * @var string
     *
     * @ORM\Column(name="name_mini", type="string", length=255, nullable=true)
     */
    private $nameMini; //namen

    /**
     * @var string
     *
     * @ORM\Column(name="bik", type="string", length=255, nullable=true, unique=true)
     */
    private $bik; //newnum

    /**
     * @var string
     *
     * @ORM\Column(name="ks", type="string", length=255, nullable=true)
     */
    private $ks; //ksnp

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=255, nullable=true)
     */
    private $postcode; //ind

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city; //nnp

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address; //adr

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone; //telef

    /**
     * @var string
     *
     * @ORM\Column(name="okato", type="string", length=255, nullable=true)
     */
    private $okato; //rgn

    /**
     * @var string
     *
     * @ORM\Column(name="okpo", type="string", length=255, nullable=true)
     */
    private $okpo; //okpo

    /**
     * @var string
     *
     * @ORM\Column(name="reg_num", type="string", length=255, nullable=true)
     */
    private $regNum; //regn

    /**
     * @var string
     *
     * @ORM\Column(name="srok", type="string", length=255, nullable=true)
     */
    private $srok; //srok

    /**
     * @var string
     *
     * @ORM\Column(name="date_add", type="string", length=255, nullable=true)
     */
    private $dateAdd; //date_in

    /**
     * @var string
     *
     * @ORM\Column(name="date_change", type="string", length=255, nullable=true)
     */
    private $dateChange; //dt_izm

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getReality()
    {
        return $this->reality;
    }

    /**
     * @param string $reality
     */
    public function setReality($reality)
    {
        $this->reality = $reality;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNameMini()
    {
        return $this->nameMini;
    }

    /**
     * @param string $nameMini
     */
    public function setNameMini($nameMini)
    {
        $this->nameMini = $nameMini;
    }

    /**
     * @return string
     */
    public function getBik()
    {
        return $this->bik;
    }

    /**
     * @param string $bik
     */
    public function setBik($bik)
    {
        $this->bik = $bik;
    }

    /**
     * @return string
     */
    public function getKs()
    {
        return $this->ks;
    }

    /**
     * @param string $ks
     */
    public function setKs($ks)
    {
        $this->ks = $ks;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getOkato()
    {
        return $this->okato;
    }

    /**
     * @param string $okato
     */
    public function setOkato($okato)
    {
        $this->okato = $okato;
    }

    /**
     * @return string
     */
    public function getOkpo()
    {
        return $this->okpo;
    }

    /**
     * @param string $okpo
     */
    public function setOkpo($okpo)
    {
        $this->okpo = $okpo;
    }

    /**
     * @return string
     */
    public function getRegNum()
    {
        return $this->regNum;
    }

    /**
     * @param string $regNum
     */
    public function setRegNum($regNum)
    {
        $this->regNum = $regNum;
    }

    /**
     * @return string
     */
    public function getSrok()
    {
        return $this->srok;
    }

    /**
     * @param string $srok
     */
    public function setSrok($srok)
    {
        $this->srok = $srok;
    }

    /**
     * @return string
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * @param string $dateAdd
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;
    }

    /**
     * @return string
     */
    public function getDateChange()
    {
        return $this->dateChange;
    }

    /**
     * @param string $dateChange
     */
    public function setDateChange($dateChange)
    {
        $this->dateChange = $dateChange;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }
}

