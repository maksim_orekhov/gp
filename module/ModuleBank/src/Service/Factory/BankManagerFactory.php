<?php

namespace ModuleBank\Service\Factory;

use ModuleBank\Service\BankManager;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class BankManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return BankManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager      =   $container->get('doctrine.entitymanager.orm_default');
        $config = $container->get('Config');

        return new BankManager(
            $entityManager,
            $config
        );
    }
}