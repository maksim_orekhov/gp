<?php

namespace ModuleBank\Service;

use Doctrine\ORM\EntityManager;
use Exception;
use ModuleBank\Entity\Bank;
use ModuleBank\XBase\Table;
use ZipArchive;

/**
 * Class BankManager
 * @package ModuleBank\Service
 */
class BankManager
{
    const PATTERN_NAME = '#current_date#';
    const CBR_ZIP_FILE_URL = 'http://www.cbr.ru/vfs/mcirabis/BIK/bik_db_'.self::PATTERN_NAME.'.zip';
    const NAME_BANK_ZIP_FILE = self::PATTERN_NAME.'.zip';
    const NAME_BANK_FILE = 'bnkseek.dbf';
    const BANK_FILE_ENCODING = 'CP866';
    const MESSAGE_UPDATES_SUCCESS = 'updates_success';
    const NUMBER_OF_ATTEMPTS = 3;

    const BANK_FILE_FIELD = [
       'bik'=> 'newnum',
       'reality'=> 'real',
       'name'=> 'namep',
       'name_mini'=> 'namen',
       'ks'=> 'ksnp',
       'postcode'=> 'ind',
       'city'=> 'nnp',
       'address'=> 'adr',
       'phone'=> 'telef',
       'okato'=> 'rgn',
       'okpo'=> 'okpo',
       'reg_num'=> 'regn',
       'srok'=> 'srok',
       'date_add'=> 'date_in',
       'date_change'=> 'dt_izm',
    ];

    const POPULAR_BANKS = [
        '044525225',// Сбер
        '044525593',// Альфа
        '044525974',// Тинькоф
        '044525745',// ВТБ
        '044525823',// Газпром
        '044525111',// Россельхоз
        '044525985',// Открытие
        '044525659',// МКБ
        '044525117',// Бинбанк
        '044525545',// Юникредит
        '044525555',// Промсвязь
        '044525256',// Росбанк
        '044525700',// Райф
        '044030861',// Россия
        '043469743',// Совком
        '044030790',// СПБ
        '044525635',// Траст
        '044525880',// ВБРР
        '044525202',// ситибанк
        '044030653',// Сбербанк северо-западный
        '044525937',// Рост Банк
        '044525062',// БМ-Банк
        '044525521',// Московский областной банк
        '044525787',// Уралсиб
        '049205805',// Ак Барс
        '044525151',// Русский стандарт
        '044525503',// СМП Банк
        '044525162',// Новикомбанк
        '044525266',// Российский капитал
        '046577795',// Уральский банк реконструкции и развития
        '044525716',// ВТБ24
        '044525214',// Почта Банк
        '044525848',// Связь-Банк
        '044525275',// Пересвет
        '041012718',// Восточный Банк
        '044525600',// Московский Индустриальный Банк
        '044525976',// Абсолют Банк
        '044525245',// Хоум Кредит Банк
        '044525181',// Возрождение
        '047144709',// Сургутнефтегазбанк
        '044525272',// Банк Зенит
        '044525000',// РосЕвроБанк
        '044525595',// Рокет ПАО Банка ФК Открытие
        '044525311',// АО ОТП Банк
        '044030832',// ВТБ северо-западный
        '044030786',// Альфа-банк санкт-петербургский
        '044030723',// Райффайзен Северная Столица
        '045004641',// Сбербанк Сибирский банк
        '046015602',// Сбербанк юго-западный
        '049205603' // Сбербанк Банк Татарстан
    ];

    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var \DateTime
     */
    private $current_date;
    /**
     * @var string
     */
    private $destination_path;

    /**
     * BankManager constructor.
     * @param EntityManager $entityManager
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                array $config)
    {

        $this->entityManager = $entityManager;
        $this->current_date = new \DateTime();
        $this->destination_path = './'.$config['file-management']['main_upload_folder'].'/module-bank/';
    }

    /**
     * @return array
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws Exception
     */
    public function updateBankBase()
    {
        $start = microtime(true);

        //отчишаем папку перед update
        $this->clearDestinationFolder();

        //даем несколько попыток на скачивание
        for ($i = 0; $i < self::NUMBER_OF_ATTEMPTS; $i++) {
            $archive_path = $this->downloadBankZipFile();

            if ($archive_path && file_exists($archive_path)) {
                break;
            }
            //ждем перед итерацией 1 сек
            usleep(1000000);
        }

        /** @var string $archive_path */
        if (!$archive_path || !file_exists($archive_path)) {
            if(\function_exists('logMessage')) {
                logMessage('ZIP Archive not download', 'module-bank');
            }

            throw new \Exception('ZIP Archive not download');
        }

        $bank_file_path = $this->extractBankFile($archive_path);

        if (!$bank_file_path || !file_exists($bank_file_path)) {
            if(\function_exists('logMessage')) {
                logMessage('Bank file not extract', 'module-bank');
            }

            throw new \Exception('Bank file not extract');
        }


        $bank_file_array = $this->convertBankFileToArray($bank_file_path);

        if ( empty($bank_file_array) && \function_exists('logMessage')){
            logMessage('Bank array empty', 'module-bank');
        }


        $result = $this->saveBankFileArrayToDatabase($bank_file_array);

        if($result['status'] === 'SUCCESS' && $result['message'] === self::MESSAGE_UPDATES_SUCCESS) {
            //все банки присутствующие в файле получат сегодняшнюю дату
            //значит остальные банки с иной датой удаляем
            $this->removeOldBank();
        }
        //отчишаем папку после всех операций
        $this->clearDestinationFolder();

        $finish = microtime(true);

        $delta = $finish - $start;

        if(\function_exists('logMessage')) {
            logMessage('update success time: ' . $delta . ' сек.', 'module-bank');
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getBankWithMaxDate()
    {
        return $this->entityManager->getRepository(Bank::class)->selectBankWithMaxDate();
    }

    /**
     * @param $bik
     * @return mixed
     * @throws \Exception
     */
    public function getBankByBik($bik)
    {
        return $this->entityManager->getRepository(Bank::class)->findOneByBik($bik);
    }

    /**
     * @return array
     */
    public function getPopularBanks()
    {
        $banks = $this->entityManager->getRepository(Bank::class)->findBy(['bik'=>self::POPULAR_BANKS]);
        $sortedBanks = [];
        if( !empty($banks) ){
            /** @var Bank $bank */
            foreach ($banks as $bank){
                $sortedBanks[array_search($bank->getBik(), self::POPULAR_BANKS, true)] = $bank;
            }
        }
        return $sortedBanks;
    }

    /**
     * @param Bank $bank
     * @return array
     */
    public function getBankOutput(Bank $bank)
    {
        $bankOutput = [];

        $bankOutput['bank_bik'] = $bank->getBik();
        $bankOutput['bank_ks'] = $bank->getKs();
        $bankOutput['bank_name'] = $bank->getName();
        $bankOutput['bank_name_mini'] = $bank->getNameMini();
        $bankOutput['bank_postcode'] = $bank->getPostcode();
        $bankOutput['bank_city'] = $bank->getCity();
        $bankOutput['bank_address'] = $bank->getAddress();
        $bankOutput['bank_phone'] = $bank->getPhone();
        $bankOutput['bank_okato'] = $bank->getOkato();
        $bankOutput['bank_okpo'] = $bank->getOkpo();
        $bankOutput['bank_reg_num'] = $bank->getRegNum();
        $bankOutput['bank_srok'] = $bank->getSrok();
        $bankOutput['bank_date_add'] = $bank->getDateAdd();
        $bankOutput['bank_date_change'] = $bank->getDateChange();

        return $bankOutput;
    }

    /**
     * @param array $banks
     * @param bool $key_bik
     * @return array
     */
    public function extractBankCollection(array $banks, $key_bik = false)
    {
        $banksOutput = [];

        /** @var Bank $bank */
        foreach ($banks as $bank) {
            if($key_bik) {
                $banksOutput[$bank->getBik()] = $this->getBankOutput($bank);
            }else{
                $banksOutput[] = $this->getBankOutput($bank);
            }
        }

        return $banksOutput;
    }

    /**
     * @return bool
     */
    private function removeOldBank()
    {
        //@TODO пока не реализованно.
        return true;
    }

    /**
     * @param array $dataBanks
     * @return array
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function saveBankFileArrayToDatabase(array $dataBanks)
    {
        if(!empty($dataBanks)){
            $batchSize = 20;
            $i=0;
            foreach ($dataBanks as $bankData) {

                $existingBank = $this->entityManager->getRepository(Bank::class)->findOneByBik($bankData[self::BANK_FILE_FIELD['bik']]);
                /** @var Bank $bank */
                $bank = $this->setBankData($existingBank, $bankData);
                $this->entityManager->persist($bank);
                // flush everything to the database every 20 inserts
                if (($i % $batchSize) === 0) {
                    $i=0;
                    $this->entityManager->flush();
                    $this->entityManager->clear();
                }
                $i++;
            }
            // flush the remaining objects
            $this->entityManager->flush();
            $this->entityManager->clear();

            return [
                'status'    => 'SUCCESS',
                'message'   => self::MESSAGE_UPDATES_SUCCESS,
                'data'      => null
            ];
        }

        return [
            'status'    => 'ERROR',
            'message'   => 'bank file empty',
            'data'      => null
        ];
    }

    /**
     * @param Bank|null $existingBank
     * @param $bankData
     * @return Bank
     */
    private function setBankData($existingBank, $bankData)
    {
        if ($existingBank instanceOf Bank) {//update
            $bank = $existingBank;
        } else {//create
            $bank = new Bank();
        }

        $bank->setReality($bankData[self::BANK_FILE_FIELD['reality']]);
        $bank->setName($bankData[self::BANK_FILE_FIELD['name']]);
        $bank->setNameMini($bankData[self::BANK_FILE_FIELD['name_mini']]);
        $bank->setBik($bankData[self::BANK_FILE_FIELD['bik']]);
        $bank->setKs($bankData[self::BANK_FILE_FIELD['ks']]);
        $bank->setPostcode($bankData[self::BANK_FILE_FIELD['postcode']]);
        $bank->setCity($bankData[self::BANK_FILE_FIELD['city']]);
        $bank->setAddress($bankData[self::BANK_FILE_FIELD['address']]);
        $bank->setPhone($bankData[self::BANK_FILE_FIELD['phone']]);
        $bank->setOkato($bankData[self::BANK_FILE_FIELD['okato']]);
        $bank->setOkpo($bankData[self::BANK_FILE_FIELD['okpo']]);
        $bank->setRegNum($bankData[self::BANK_FILE_FIELD['reg_num']]);
        $bank->setSrok($bankData[self::BANK_FILE_FIELD['srok']]);
        $bank->setDateAdd($bankData[self::BANK_FILE_FIELD['date_add']]);
        $bank->setDateChange($bankData[self::BANK_FILE_FIELD['date_change']]);
        $bank->setCreated($this->current_date);

        return $bank;
    }

    /**
     * @return null|string
     * @throws Exception
     */
    private function downloadBankZipFile()
    {
        if (!file_exists($this->destination_path) && !mkdir($this->destination_path, 0777, true) && !is_dir($this->destination_path)) {
            if (\function_exists('logMessage')) {
                logMessage('Failed to create directory...', 'module-bank');
            }

            throw new Exception('Failed to create directory...');
        }

        $zip_file_url = preg_filter('/'.self::PATTERN_NAME.'/', $this->current_date->format('dmY') , self::CBR_ZIP_FILE_URL);
        $fileName = preg_filter('/'.self::PATTERN_NAME.'/', $this->current_date->format('dmY') , self::NAME_BANK_ZIP_FILE);

        $zip_file = $this->get_file_contents($zip_file_url);

        if (!$zip_file && \function_exists('logMessage')) {
            logMessage('file_get_contents not download files...', 'module-bank');
        }
        if($zip_file && file_put_contents($this->destination_path . $fileName, $zip_file, LOCK_EX)) {

            return $this->destination_path . $fileName;
        }

        return null;
    }

    /**
     * @param $url
     * @return mixed
     */
    public function get_file_contents($url) {
        $useragent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36';
        $timeout= 120;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_AUTOREFERER, true );
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout );
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout );
        curl_setopt($ch, CURLOPT_MAXREDIRS, 100 );
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com/');
        $content = curl_exec($ch);
        if(!$content && \function_exists('logMessage')) {

            logMessage('Ошибка curl: ' . curl_error($ch), 'module-bank');
        }

        curl_close($ch);
        return $content;
    }

    /**
     * @param $archive
     * @return string
     */
    private function extractBankFile($archive)
    {
        $zip = new ZipArchive();
        $name_bank_file = self::NAME_BANK_FILE;

        $zip->open($archive);
        $zip->extractTo($this->destination_path,[$name_bank_file]);
        if (!file_exists($this->destination_path . $name_bank_file)) {
            $name_bank_file = strtoupper($name_bank_file);
            $zip->extractTo($this->destination_path,[$name_bank_file]);
        }
        $zip->close();

        if(file_exists($archive)) {
            unlink($archive);
        }

        return $this->destination_path . $name_bank_file;
    }

    /**
     * @param $db_file_path
     * @return array
     */
    private function convertBankFileToArray($db_file_path)
    {
        /** @var Table $table */
        $table = new Table($db_file_path);
        $bankArray = [];
        while ($record = $table->nextRecord()) {
            $line = [];
            $columns = $record->getColumns();
            /** @var array $columns */
            foreach ($columns as $column) {
                switch($column->type) {
                    case 'C':
                        $line[$column->name] = mb_convert_encoding($record->getObject($column), 'UTF-8', self::BANK_FILE_ENCODING);
                        break;
                    case 'D':	// Date
                        $line[$column->name]= strftime('%d.%m.%Y', $record->getObject($column) );
                        break;
                }

            }
            $bankArray[$record->getObject($record->getColumn(self::BANK_FILE_FIELD['bik']))] = $line;
        }
        $table->close();

        return $bankArray;
    }

    /**
     * @return bool
     */
    private function clearDestinationFolder()
    {
        if ( file_exists($this->destination_path) ){
            $files = glob($this->destination_path.'*'); // get all file names
            foreach($files as $file){ // iterate files
                if (is_file($file)) {
                    unlink($file); // delete file
                }
            }
        }
        return true;
    }
}