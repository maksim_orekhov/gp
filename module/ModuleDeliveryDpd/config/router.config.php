<?php
namespace ModuleDeliveryDpd;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            // DeliveryDpdCatalogController
            'delivery-dpd-update' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/admin/delivery/dpd/update',
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCatalogController::class,
                        'action'     => 'dpdUpdate',
                    ],
                ],
            ],
            'delivery-dpd-city-update' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/admin/delivery/dpd/update/city',
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCatalogController::class,
                        'action'     => 'dpdCityUpdate',
                    ],
                ],
            ],
            'delivery-dpd-point-update' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/admin/delivery/dpd/update/point',
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCatalogController::class,
                        'action'     => 'dpdPointUpdate',
                    ],
                ],
            ],
            'delivery-dpd-production-city-update' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/admin/delivery/dpd/update/production/city',
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCatalogController::class,
                        'action'     => 'dpdProductionCityUpdate',
                    ],
                ],
            ],
            'delivery-dpd-production-point-update' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/admin/delivery/dpd/update/production/point',
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCatalogController::class,
                        'action'     => 'dpdProductionPointUpdate',
                    ],
                ],
            ],
            // DeliveryDpdCityController
            'delivery-dpd-city-single' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/delivery/dpd/city/:id',
                    'constraints' => [
                        'id'     => '\d+'
                    ],
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCityController::class,
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            'delivery-dpd-city-form-init' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/delivery/dpd/city/form-init',
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCityController::class,
                        'action' => 'formInit',
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            'delivery-dpd-search-city' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/delivery/dpd/city',
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCityController::class,
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            'delivery-dpd-city-auto-complete' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/dpd/city/autocomplete',
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCityController::class,
                        'action' => 'autoComplete'
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            'delivery-dpd-to-city-cost' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/delivery/dpd/city/cost',
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCityController::class,
                        'action' => 'getDeliveryToCityCost',
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            // DeliveryDpdPointController
            'delivery-dpd-point-single' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/delivery/dpd/point/:pointCode',
                    'constraints' => [
                        'pointCode'     => '[0-9A-Za-z]{2,6}'
                    ],
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdPointController::class,
                        'action' => 'getPoint'
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
            // DeliveryDpdCostController
            'delivery-dpd-cost' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/delivery/dpd/cost',
                    'defaults' => [
                        'controller' => Controller\DeliveryDpdCostController::class,
                        'action' => 'getCost',
                    ],
                    'strategies' => [
                        'ViewJsonStrategy',
                    ],
                ]
            ],
        ],
    ],
];
