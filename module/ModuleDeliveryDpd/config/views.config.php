<?php
namespace ModuleDeliveryDpd;

return [
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../viewTwig',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
