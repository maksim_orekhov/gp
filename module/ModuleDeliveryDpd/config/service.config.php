<?php
namespace ModuleDeliveryDpd;

use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'service_manager' => [
        'factories' => [
            Service\DeliveryDpdManager::class => Service\Factory\DeliveryDpdManagerFactory::class,
            Service\DeliveryDpdCatalogManager::class => Service\Factory\DeliveryDpdCatalogManagerFactory::class,
            Service\DeliveryOrderDpdManager::class => Service\Factory\DeliveryOrderDpdManagerFactory::class,
            Service\DeliveryOrderDpdValidator::class => Service\Factory\DeliveryOrderDpdValidatorFactory::class,
            Service\DeliveryDpdStatusManager::class => Service\Factory\DeliveryDpdStatusManagerFactory::class,
        ],
    ],
];
