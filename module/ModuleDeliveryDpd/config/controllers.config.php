<?php
namespace ModuleDeliveryDpd;

return [
    'controllers' => [
        'factories' => [
            Controller\DeliveryDpdCityController::class => Controller\Factory\DeliveryDpdCityControllerFactory::class,
            Controller\DeliveryDpdPointController::class => Controller\Factory\DeliveryDpdPointControllerFactory::class,
            Controller\DeliveryDpdCostController::class => Controller\Factory\DeliveryDpdCostControllerFactory::class,
            Controller\DeliveryDpdCatalogController::class => Controller\Factory\DeliveryDpdCatalogControllerFactory::class,
        ],
    ],
];