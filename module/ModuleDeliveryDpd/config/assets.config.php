<?php
namespace ModuleDeliveryDpd;

return [
    // The 'access_filter' key is used by the Application module to restrict or permit
    // access to certain controller actions for unauthorized visitors.
    'access_filter' => [
        'options' => [
            'mode' => 'permissive',     // все, что не запрещено, разрешено
            //'mode' => 'restrictive',  // все, что не разрешено, запрещено
        ],
        'controllers' => [
            Controller\DeliveryDpdCatalogController::class => [
                [
                    'actions' => [
                        'dpdUpdate',
                        'dpdCityUpdate',
                        'dpdPointUpdate',
                        'dpdProductionCityUpdate',
                        'dpdProductionPointUpdate',
                    ],
                    'allows' => ['#Administrator']
                ]
            ],
            Controller\DeliveryDpdPointController::class => [
                [
                    'actions' => [
                        'getPoint',
                    ],
                    'allows' => ['#Verified']
                ]
            ],
        ],
    ],
];
