<?php

namespace ModuleDeliveryDpdTest;

use ModuleDeliveryDpd\Module;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Stdlib\ArrayUtils;

class ModuleTest extends AbstractHttpControllerTestCase
{
    const CONFIG_FILES_NAMES = [
        'assets.config.php',
        'controllers.config.php',
        'doctrine.config.php',
        'module.config.php',
        'router.config.php',
        'service.config.php',
        'views.config.php'
    ];

    private $moduleRoot = null;

    protected function setUp()
    {
        $this->moduleRoot = realpath(__DIR__ . '/../');

        // Add content of global.php to ApplicationConfig
        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../config/application.config.php',
            include __DIR__ . '/../../../config/autoload/global.php'
        ));
    }

    /**
     * @group module
     */
    public function testGetConfig()
    {
        $expectedConfig = [];
        foreach(self::CONFIG_FILES_NAMES as $file_name) {
            $config = include $this->moduleRoot . '/config/' . $file_name;
            $expectedConfig = array_replace_recursive($expectedConfig, $config);
        }

        $module = new Module();
        $configData = $module->getConfig();

        $this->assertEquals($expectedConfig, $configData);
    }

    /**
     * @group module
     */
    public function testHasConfigKeys()
    {
        $services = $this->getApplicationServiceLocator();
        $config = $services->get('Config');

        $this->assertArrayHasKey('delivery_dpd_settings', $config);
        $this->assertArrayHasKey('account', $config['delivery_dpd_settings']);
        $this->assertArrayHasKey('secret_key', $config['delivery_dpd_settings']);
        $this->assertArrayHasKey('gateway_host', $config['delivery_dpd_settings']);
    }
}