<?php

namespace ModuleDeliveryDpdTest\Service;

use ApplicationTest\Bootstrap;
use ModuleDeliveryDpd\Entity\DpdCityCandidate;
use ModuleDeliveryDpd\Entity\DpdParcelShopLimitsCandidate;
use ModuleDeliveryDpd\Entity\DpdParcelShopState;
use ModuleDeliveryDpd\Entity\DpdParcelShopType;
use ModuleDeliveryDpd\Entity\DpdPointCandidate;
use ModuleDeliveryDpd\Entity\DpdScheduleCandidate;
use ModuleDeliveryDpd\Entity\DpdTimetableCandidate;
use ModuleDeliveryDpd\Service\DeliveryDpdCatalogManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use CoreTest\CallPrivateMethodTrait;

/**
 * Class DeliveryDpdCatalogManagerTest
 * @package ModuleDeliveryDpdTest\HTMLOutput
 *
 * Тесты запускать только с заполенными таблицами DPD!
 */
class DeliveryDpdCatalogManagerTest extends AbstractHttpControllerTestCase
{
    use CallPrivateMethodTrait;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var DeliveryDpdCatalogManager;
     */
    private $deliveryDpdCatalogManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->deliveryDpdCatalogManager = $serviceManager->get(DeliveryDpdCatalogManager::class);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testCreateDpdCity()
    {
        $city = new \stdClass();
        $city->cityId = 99080419708;
        $city->countryCode = 'RU';
        $city->countryName = 'Russia';
        $city->regionCode = '99';
        $city->regionName = 'Солнечная';
        $city->cityCode = '99000000001';
        $city->cityName = 'Цветочный';
        $city->abbreviation = 'г';

        /** @var DpdCityCandidate $dpdCity */
        $dpdCity = $this->invokeMethod(
            $this->deliveryDpdCatalogManager,
            'createDpdCity',
            array($city, true)
        );

        $this->assertNotNull($dpdCity);
        $this->assertInstanceOf(DpdCityCandidate::class, $dpdCity);
        $this->assertEquals($city->cityId, $dpdCity->getCityId());
        $this->assertEquals($city->countryCode, $dpdCity->getCountryCode());
        $this->assertEquals($city->countryName, $dpdCity->getCountryName());
        $this->assertEquals($city->regionCode, $dpdCity->getRegionCode());
        $this->assertEquals($city->regionName, $dpdCity->getRegionName());
        $this->assertEquals($city->regionName, $dpdCity->getRegionName());
        $this->assertEquals($city->cityCode, $dpdCity->getCityCode());
        $this->assertEquals($city->cityName, $dpdCity->getCityName());
        $this->assertEquals($city->abbreviation, $dpdCity->getAbbreviation());
        $this->assertTrue($dpdCity->isPaymentOnDelivery());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testCreateDpdCityFromAddress()
    {
        $address = $this->createAddress();

        /** @var DpdCityCandidate $dpdCity */
        $dpdCity = $this->invokeMethod(
            $this->deliveryDpdCatalogManager,
            'createDpdCityFromAddress',
            array($address, false)
        );

        $this->assertNotNull($dpdCity);
        $this->assertInstanceOf(DpdCityCandidate::class, $dpdCity);
        $this->assertEquals($address->cityId, $dpdCity->getCityId());
        $this->assertEquals($address->countryCode, $dpdCity->getCountryCode());
        $this->assertEquals($address->regionCode, $dpdCity->getRegionCode());
        $this->assertEquals($address->regionName, $dpdCity->getRegionName());
        $this->assertEquals($address->regionName, $dpdCity->getRegionName());
        $this->assertEquals($address->cityCode, $dpdCity->getCityCode());
        $this->assertEquals($address->cityName, $dpdCity->getCityName());
        $this->assertFalse($dpdCity->isPaymentOnDelivery());
    }

    /**
     *  @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testCreateDpdParcelShopsPoint()
    {
        $parcelShop = new \stdClass();
        $parcelShop->code = '001H';
        $parcelShop->state = 'Open';
        $parcelShop->address = $this->createAddress();
        $parcelShop->brand = 'КОПИЦЕНТР';
        $parcelShop->geoCoordinates = $this->createGeoCoordinates();
        $parcelShop->limits = $this->createLimits();

        /** @var DpdParcelShopType $dpdParcelShopType */
        $dpdParcelShopType = $this->entityManager
            ->getRepository(DpdParcelShopType::class)->findOneBy(['code' => 'PARCELSHOP']);

        /** @var DpdPointCandidate $dpdParcelShop */
        $dpdParcelShop = $this->invokeMethod(
            $this->deliveryDpdCatalogManager,
            'createDpdParcelShopsPoint',
            array($dpdParcelShopType, $parcelShop)
        );

        $this->assertNotNull($dpdParcelShop);
        $this->assertInstanceOf(DpdPointCandidate::class, $dpdParcelShop);
        $this->assertInstanceOf(DpdCityCandidate::class, $dpdParcelShop->getCity());
        $this->assertInstanceOf(DpdParcelShopLimitsCandidate::class, $dpdParcelShop->getDpdParcelShopLimits());
        $this->assertInstanceOf(DpdParcelShopState::class, $dpdParcelShop->getDpdParcelShopState());
        $this->assertInstanceOf(DpdParcelShopType::class, $dpdParcelShop->getDpdParcelShopType());
        $this->assertEquals('PARCELSHOP', $dpdParcelShop->getDpdParcelShopType()->getCode());
        $this->assertEquals($parcelShop->code, $dpdParcelShop->getCode());
        $this->assertEquals($parcelShop->brand, $dpdParcelShop->getName());
        $this->assertEquals($parcelShop->geoCoordinates->latitude, $dpdParcelShop->getLatitude());
        $this->assertEquals($parcelShop->geoCoordinates->longitude, $dpdParcelShop->getLongitude());
        $this->assertEquals($parcelShop->address->street, $dpdParcelShop->getStreet());
        $this->assertEquals($parcelShop->address->streetAbbr, $dpdParcelShop->getStreetAbr());
        $this->assertEquals($parcelShop->address->houseNo, $dpdParcelShop->getHouse());
        $this->assertEquals($parcelShop->address->structure, $dpdParcelShop->getStructure());
        $this->assertEquals($parcelShop->address->ownership, $dpdParcelShop->getOwnership());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testCreateDpdTerminalsSelfDeliveryPoint()
    {
        $terminal = new \stdClass();
        $terminal->terminalCode = 'O11';
        $terminal->terminalName = 'ОРЕНБУРГ - O11 ОГУ';
        $terminal->address = $this->createAddress();
        $terminal->geoCoordinates = $this->createGeoCoordinates();
        $terminal->limits = $this->createLimits();

        /** @var DpdParcelShopType $dpdParcelShopType */
        $dpdParcelShopType = $this->entityManager
            ->getRepository(DpdParcelShopType::class)->findOneBy(['code' => 'TERMINAL']);

        /** @var DpdPointCandidate $dpdParcelShop */
        $dpdParcelShop = $this->invokeMethod(
            $this->deliveryDpdCatalogManager,
            'createDpdTerminalsSelfDeliveryPoint',
            array($dpdParcelShopType, $terminal)
        );

        $this->assertNotNull($dpdParcelShop);
        $this->assertInstanceOf(DpdPointCandidate::class, $dpdParcelShop);
        $this->assertInstanceOf(DpdCityCandidate::class, $dpdParcelShop->getCity());
        $this->assertInstanceOf(DpdParcelShopType::class, $dpdParcelShop->getDpdParcelShopType());
        $this->assertEquals('TERMINAL', $dpdParcelShop->getDpdParcelShopType()->getCode());
        $this->assertEquals($terminal->terminalCode, $dpdParcelShop->getCode());
        $this->assertEquals($terminal->terminalName, $dpdParcelShop->getName());
        $this->assertEquals($terminal->geoCoordinates->latitude, $dpdParcelShop->getLatitude());
        $this->assertEquals($terminal->geoCoordinates->longitude, $dpdParcelShop->getLongitude());
        $this->assertEquals($terminal->address->street, $dpdParcelShop->getStreet());
        $this->assertEquals($terminal->address->streetAbbr, $dpdParcelShop->getStreetAbr());
        $this->assertEquals($terminal->address->houseNo, $dpdParcelShop->getHouse());
        $this->assertEquals($terminal->address->structure, $dpdParcelShop->getStructure());
        $this->assertEquals($terminal->address->ownership, $dpdParcelShop->getOwnership());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testCreateSchedulesForParcelShop()
    {
        $name = 'Брянск';
        /** @var DpdCityCandidate $city */ // Get City by Id
        $city = $this->entityManager->getRepository(DpdCityCandidate::class)
            ->findOneBy(['cityName' => $name]);

        $points = $city->getDpdPoints();

        /** @var DpdPointCandidate $point */
        $point = $points->first();

        $operations = ['Payment', 'PaymentByBankCard', 'SelfDelivery', 'SelfPickup'];

        $schedules = [];
        foreach ($operations as $operation) {
            $schedules[] = $this->createSchedule($operation);
        }

        $dpdSchedules = $this->invokeMethod(
            $this->deliveryDpdCatalogManager,
            'createSchedulesForParcelShop',
            array($point, $schedules)
        );

        $this->assertInternalType('array', $dpdSchedules);
        /** @var DpdScheduleCandidate $dpdSchedule */
        $dpdSchedule = $dpdSchedules[0];
        $this->assertEquals($operations[0], $dpdSchedule->getType());
        $this->assertInstanceOf(DpdPointCandidate::class, $dpdSchedule->getDpdPoint());
        /** @var DpdScheduleCandidate $dpdSchedule */
        $dpdSchedule = $dpdSchedules[1];
        $this->assertEquals($operations[1], $dpdSchedule->getType());
        $this->assertInstanceOf(DpdPointCandidate::class, $dpdSchedule->getDpdPoint());
        /** @var DpdScheduleCandidate $dpdSchedule */
        $dpdSchedule = $dpdSchedules[2];
        $this->assertEquals($operations[2], $dpdSchedule->getType());
        $this->assertInstanceOf(DpdPointCandidate::class, $dpdSchedule->getDpdPoint());
        /** @var DpdScheduleCandidate $dpdSchedule */
        $dpdSchedule = $dpdSchedules[3];
        $this->assertEquals($operations[3], $dpdSchedule->getType());
        $this->assertInstanceOf(DpdPointCandidate::class, $dpdSchedule->getDpdPoint());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testCreateTimetablesForSchedule()
    {
        /** @var DpdScheduleCandidate $dpdSchedule */ // Get City by Id
        $dpdSchedule = $this->entityManager->getRepository(DpdScheduleCandidate::class)
            ->findOneBy(['type' => 'Payment']);

        $timetable = $this->createTimetables();

        $dpdTimetables = $this->invokeMethod(
            $this->deliveryDpdCatalogManager,
            'createTimetablesForSchedule',
            array($dpdSchedule, $timetable)
        );

        $this->assertInternalType('array', $dpdTimetables);
        /** @var DpdTimetableCandidate $dpdTimetable */
        $dpdTimetable = $dpdTimetables[0];
        $this->assertInstanceOf(DpdScheduleCandidate::class, $dpdTimetable->getSchedule());
        $this->assertEquals('Пн,Вт,Ср,Чт,Пт', $dpdTimetable->getWeekDays());
        $this->assertEquals('09:00 - 19:00', $dpdTimetable->getWorkTime());
        /** @var DpdTimetableCandidate $dpdTimetable */
        $dpdTimetable = $dpdTimetables[1];
        $this->assertInstanceOf(DpdScheduleCandidate::class, $dpdTimetable->getSchedule());
        $this->assertEquals('Сб', $dpdTimetable->getWeekDays());
        $this->assertEquals('10:00 - 15:00', $dpdTimetable->getWorkTime());
        /** @var DpdTimetableCandidate $dpdTimetable */
        $dpdTimetable = $dpdTimetables[2];
        $this->assertInstanceOf(DpdScheduleCandidate::class, $dpdTimetable->getSchedule());
        $this->assertEquals('Вс', $dpdTimetable->getWeekDays());
        $this->assertEquals('выходной', $dpdTimetable->getWorkTime());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testCreateDpdParcelShopLimits()
    {
        $limits = $this->createLimits();

        /** @var DpdParcelShopLimitsCandidate $dpdParcelShopLimits */
        $dpdParcelShopLimits = $this->invokeMethod(
            $this->deliveryDpdCatalogManager,
            'createDpdParcelShopLimits',
            array($limits)
        );

        $this->assertInstanceOf(DpdParcelShopLimitsCandidate::class, $dpdParcelShopLimits);
        $this->assertEquals(30, $dpdParcelShopLimits->getWeight());
        $this->assertEquals(200, $dpdParcelShopLimits->getWidth());
        $this->assertEquals(200, $dpdParcelShopLimits->getLength());
        $this->assertEquals(200, $dpdParcelShopLimits->getHeight());
        $this->assertEquals(200, $dpdParcelShopLimits->getDimensionSum());
    }

    /**
     * @return \stdClass
     */
    private function createAddress(): \stdClass
    {
        $address = new \stdClass();
        $address->cityId = 195554857;
        $address->countryCode = 'RU';
        $address->regionCode = '56';
        $address->regionName = 'Оренбургская';
        $address->cityCode = '56000001000';
        $address->cityName = 'Оренбург';
        $address->index = '460052';
        $address->street = 'Дружбы';
        $address->streetAbbr = 'ул';
        $address->houseNo = '45А';
        $address->structure = '2';
        $address->ownership = '8';

        return $address;
    }

    /**
     * @return \stdClass
     */
    private function createGeoCoordinates(): \stdClass
    {
        $geoCoordinates = new \stdClass();
        $geoCoordinates->latitude = '55.739839';
        $geoCoordinates->longitude = '52.380899';

        return $geoCoordinates;
    }

    /**
     * @return \stdClass
     */
    private function createLimits(): \stdClass
    {
        $limits = new \stdClass();
        $limits->maxShipmentWeight = '400';
        $limits->maxWeight = '30';
        $limits->maxLength = '200';
        $limits->maxWidth = '200';
        $limits->maxHeight = '200';
        $limits->dimensionSum = '200';

        return $limits;
    }

    /**
     * @param string $operation
     * @return \stdClass
     */
    private function createSchedule(string $operation = 'Payment')
    {
        $schedule = new \stdClass();
        $schedule->operation = $operation;
        $schedule->timetable = $this->createTimetables();

        return $schedule;
    }

    /**
     * @return array
     */
    private function createTimetables()
    {
        $timetables = [];

        $timetable_1 = new \stdClass();
        $timetable_1->weekDays = 'Пн,Вт,Ср,Чт,Пт';
        $timetable_1->workTime  = '09:00 - 19:00';
        $timetables[] = $timetable_1;

        $timetable_2 = new \stdClass();
        $timetable_2->weekDays = 'Сб';
        $timetable_2->workTime  = '10:00 - 15:00';
        $timetables[] = $timetable_2;

        $timetable_3 = new \stdClass();
        $timetable_3->weekDays = 'Вс';
        $timetable_3->workTime  = 'выходной';
        $timetables[] = $timetable_3;

        return $timetables;
    }
}