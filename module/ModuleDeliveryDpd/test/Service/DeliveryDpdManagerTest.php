<?php

namespace ModuleDeliveryDpdTest\Service;

use ApplicationTest\Bootstrap;
use ModuleDeliveryDpd\Entity\DpdCity;
use ModuleDeliveryDpd\Entity\DpdParcelShopLimits;
use ModuleDeliveryDpd\Entity\DpdPoint;
use ModuleDeliveryDpd\Entity\DpdSchedule;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Form\Element;

/**
 * Class DeliveryDpdManagerTest
 * @package ModuleDeliveryDpdTest\HTMLOutput
 *
 * Тесты запускать только с заполенными таблицами DPD!
 */
class DeliveryDpdManagerTest extends AbstractHttpControllerTestCase
{
    /**
     * @var DeliveryDpdManager;
     */
    private $deliveryDpdManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $this->deliveryDpdManager = $serviceManager->get(DeliveryDpdManager::class);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     *
     * @throws \Exception
     */
    public function testGetCity()
    {
        $id = 1;
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCity($id);

        $this->assertInstanceOf(DpdCity::class, $city);
        $this->assertEquals($id, $city->getId());
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testGetCityByName()
    {
        $name = 'Оренбург';
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByName($name);

        $this->assertInstanceOf(DpdCity::class, $city);
        $this->assertEquals($name, $city->getCityName());
    }

//    /**
//     * @runInSeparateProcess
//     * @preserveGlobalState disabled
//     *
//     * @group delivery
//     * @group delivery-dpd
//     *
//     * Долго исполняется, если много городо в БД
//     */
//    public function testGetAllCities()
//    {
//        $cities = $this->deliveryDpdManager->getAllCities();
//
//        $this->assertInternalType('array', $cities);
//        $this->assertGreaterThan(0, \count($cities));
//    }

    /**
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     *
     * @dataProvider providerGetBiggestCities
     */
    public function testGetBiggestCities($given, $expected)
    {
        $cities = $this->deliveryDpdManager->getBiggestCities($given);

        $this->assertCount($expected, $cities);
    }

    public function providerGetBiggestCities(): array
    {
        return [
            [1, 1],
            [5, 5],
            [10, 10],
            [50, 50]
        ];
    }

    /**
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     *
     * @dataProvider providerGetCitiesBySearchRequest
     */
    public function testGetCitiesBySearchRequest($given, $expected)
    {
        $cities = $this->deliveryDpdManager->getCitiesBySearchRequest($given);

        $this->assertCount($expected, $cities);
    }

    public function providerGetCitiesBySearchRequest(): array
    {
        return [
            ['Оренбург', 2],
            ['Брянск', 1],
            ['Москва', 1],
            ['Химки', 2],
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testGetNumberOfDpdCities()
    {
        $number = $this->deliveryDpdManager->getNumberOfDpdCities();
        // В фикстурах пока 4 города, поэтому провенрка на "больше 3"
        // Если база DPD заполнена, то городов больше 17000
        $this->assertGreaterThan(3, $number);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testGetNumberOfDpdPoints()
    {
        $number = $this->deliveryDpdManager->getNumberOfDpdPoints();
        // Если база DPD заполнена, то пунктов ннесколько тысяч
        // Фикстур для пунктов нет
        $this->assertGreaterThan(0, $number);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     *
     * @throws \Exception
     */
    public function testGetDeliveryDpdCostUsingServiceCost2()
    {
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $data = [
            'pickup_city_id' => 49694102,
            'pickup_index' => null,
            'pickup_city_name' => null,
            'pickup_region_code' => null,
            'pickup_country_code' => null,

	        'delivery_city_id' => 195554857,
	        'delivery_index' => null,
	        'delivery_city_name' => null,
	        'delivery_region_code' => null,
	        'delivery_country_code' => null,

            'self_pickup' => true,
            'self_delivery' => true,

            'weight' => 10,
            'volume' => null,
            'service_code' => null,
            'pickup_date' => null,
            'max_days' => null,
            'max_cost' => null,
            'declared_value' => null,

            'parcel_weight' => null,
            'parcel_length' => null,
            'parcel_width' => null,
            'parcel_height' => null,
            'parcel_quantity' => null,

	        'csrf' => $csrf
        ];

        /** @var array $costs */
        $costs = $this->deliveryDpdManager->getDeliveryDpdCost($data);

        $this->assertInternalType('array', $costs);
        $this->assertGreaterThan(4, \count($costs));

        foreach ($costs as $cost) {
            $this->assertInstanceOf(\stdClass::class, $cost);
            $this->assertTrue(isset(DeliveryDpdManager::SERVICE_CODES[$cost->serviceCode]));
            $this->assertTrue(in_array($cost->serviceName,DeliveryDpdManager::SERVICE_CODES));
            $this->assertNotNull($cost->cost);
            $this->assertGreaterThan(0, $cost->days);
        }
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     *
     * @throws \Exception
     */
    public function testGetDeliveryDpdCostUsingServiceCostByParcels()
    {
        $csrfElement = new Element\Csrf('csrf');
        $csrf = $csrfElement->getValue();

        $data = [
            'pickup_city_id' => 49694102,
            'pickup_index' => null,
            'pickup_city_name' => null,
            'pickup_region_code' => null,
            'pickup_country_code' => null,

            'delivery_city_id' => 195554857,
            'delivery_index' => null,
            'delivery_city_name' => null,
            'delivery_region_code' => null,
            'delivery_country_code' => null,

            'self_pickup' => true,
            'self_delivery' => true,

            'weight' => 10,
            'volume' => null,
            'service_code' => null,
            'pickup_date' => null,
            'max_days' => null,
            'max_cost' => null,
            'declared_value' => null,

            'parcel_weight' => 10,  // кг
            'parcel_length' => 50,  // см
            'parcel_width' => 30,   // см
            'parcel_height' => 20,  // см
            'parcel_quantity' => 1,

            'csrf' => $csrf
        ];

        /** @var array $costs */
        $costs = $this->deliveryDpdManager->getDeliveryDpdCost($data);

        $this->assertInternalType('array', $costs);
        $this->assertGreaterThan(4, \count($costs));

        foreach ($costs as $cost) {
            $this->assertInstanceOf(\stdClass::class, $cost);
            $this->assertTrue(isset(DeliveryDpdManager::SERVICE_CODES[$cost->serviceCode]));
            $this->assertTrue(in_array($cost->serviceName,DeliveryDpdManager::SERVICE_CODES));
            $this->assertNotNull($cost->cost);
            $this->assertGreaterThan(0, $cost->days);
        }
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testGetCityCollectionForOutput()
    {
        // Выбираем 5 самых больших городов (по количеству пунктов)
        $cities = $this->deliveryDpdManager->getBiggestCities(5);

        $citiesOutput = $this->deliveryDpdManager->getCityCollectionForOutput($cities);

        // В выборке обязательно должны быть Москва и Санкт-Петербург
        $is_moscow = false;
        $is_piter = false;
        foreach ($citiesOutput as $cityOutput) {
            if ($cityOutput['city_name'] === 'Москва') {
                $is_moscow = true;
            }
            if ($cityOutput['city_name'] === 'Санкт-Петербург') {
                $is_piter = true;
            }
        }

        $this->assertCount(5, $citiesOutput);
        $this->assertTrue($is_moscow);
        $this->assertTrue($is_piter);
    }

    /**
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     *
     * @dataProvider providerGetCityForOutput
     */
    public function testGetCityForOutput($given, $expected)
    {
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByName($given);

        $cityOutput = $this->deliveryDpdManager->getCityForOutput($city);

        $this->assertEquals($expected['city_name'], $cityOutput['city_name']);
        $this->assertEquals($expected['region_code'], $cityOutput['region_code']);
        $this->assertEquals($expected['region_name'], $cityOutput['region_name']);
    }

    public function providerGetCityForOutput(): array
    {
        return [
            ['Оренбург', [
                'city_name' => 'Оренбург',
                'region_code' => '56',
                'region_name' => DeliveryDpdManager::REGION_NAMES_RU['56']
                ]
            ],
            ['Брянск', [
                'city_name' => 'Брянск',
                'region_code' => '32',
                'region_name' => DeliveryDpdManager::REGION_NAMES_RU['32']
                ]
            ],
            ['Симферополь', [
                'city_name' => 'Симферополь',
                'region_code' => '91',
                'region_name' => DeliveryDpdManager::REGION_NAMES_RU['91']
                ]
            ],
            ['Нальчик', [
                'city_name' => 'Нальчик',
                'region_code' => '7',
                'region_name' => DeliveryDpdManager::REGION_NAMES_RU['7']
                ]
            ],
        ];
    }

    /**
     * @param $given
     * @param $expected
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     *
     * @dataProvider providerGetRegionName
     */
    public function testGetRegionName($given, $expected)
    {
        $region_name = DeliveryDpdManager::REGION_NAMES_RU[$given];

        $this->assertEquals($expected, $region_name);
    }

    public function providerGetRegionName(): array
    {
        return [
            ['56', 'Оренбургская обл.'],
            ['32', 'Брянская обл.'],
            ['91', 'Крым'],
            ['7', 'Кабардино-Балкария'],
        ];
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testGetPointCollectionOutput()
    {
        $name = 'Москва';
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByName($name);

        $points = $city->getDpdPoints();

        $pointsOutput = $this->deliveryDpdManager->getPointCollectionOutput($points);

        $this->assertInternalType('array', $pointsOutput);
        $this->assertGreaterThan(100, \count($pointsOutput));
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testGetPointForOutput()
    {
        $name = 'Брянск';
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByName($name);

        $points = $city->getDpdPoints();

        /** @var DpdPoint $point */
        $point = $points->first();

        $this->assertEquals($name, $point->getCity()->getCityName());

        $pointOutput = $this->deliveryDpdManager->getPointForOutput($point);

        $this->assertInternalType('array', $pointOutput);
        $this->assertNotNull($pointOutput['name']);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testGetLimitsForOutput()
    {
        $name = 'Брянск';
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByName($name);

        $points = $city->getDpdPoints();

        /** @var DpdPoint $point */
        $point = $points->first();
        /** @var DpdParcelShopLimits $limits */
        $limits = $point->getDpdParcelShopLimits();

        $limitsOutput = $this->deliveryDpdManager->getLimitsForOutput($limits);

        $this->assertArrayHasKey('weight', $limitsOutput);
        $this->assertArrayHasKey('width', $limitsOutput);
        $this->assertArrayHasKey('length', $limitsOutput);
        $this->assertArrayHasKey('height', $limitsOutput);
        $this->assertArrayHasKey('dimension_sum', $limitsOutput);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testGetScheduleCollectionForOutput()
    {
        $name = 'Брянск';
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByName($name);

        $points = $city->getDpdPoints();

        /** @var DpdPoint $point */
        $point = $points->first();

        $schedules = $point->getDpdSchedules();

        $schedulesOutput = $this->deliveryDpdManager->getScheduleCollectionForOutput($schedules);

        $this->assertInternalType('array', $schedulesOutput);
        $this->assertGreaterThan(0, \count($schedulesOutput));
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testGetScheduleForOutput()
    {
        $name = 'Брянск';
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByName($name);

        $points = $city->getDpdPoints();

        /** @var DpdPoint $point */
        $point = $points->first();

        $schedules = $point->getDpdSchedules();

        /** @var DpdSchedule $schedule */
        $schedule = $schedules->first();

        $scheduleOutput = $this->deliveryDpdManager->getScheduleForOutput($schedule);

        $this->assertArrayHasKey('type', $scheduleOutput);
        $this->assertArrayHasKey('timetables', $scheduleOutput);
        $this->assertInternalType('array', $scheduleOutput['timetables']);
        $this->assertGreaterThan(0, \count($scheduleOutput['timetables']));
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @group delivery
     * @group delivery-dpd
     */
    public function testGetTimetableCollectionForOutput()
    {
        $name = 'Брянск';
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByName($name);

        $points = $city->getDpdPoints();

        /** @var DpdPoint $point */
        $point = $points->first();

        $schedules = $point->getDpdSchedules();

        /** @var DpdSchedule $schedule */
        $schedule = $schedules->first();

        $timetables = $schedule->getDpdTimetables();

        $timetablesOutput = $this->deliveryDpdManager->getTimetableCollectionForOutput($timetables);

        $this->assertInternalType('array', $timetablesOutput);
        $this->assertCount(7, $timetablesOutput);
        $this->assertInternalType('array', $timetablesOutput[0]);
        $this->assertArrayHasKey('week_day', $timetablesOutput[0]);
        $this->assertArrayHasKey('work_time', $timetablesOutput[0]);
        $this->assertEquals('Пн', $timetablesOutput[0]['week_day']);
        $this->assertEquals('Вт', $timetablesOutput[1]['week_day']);
        $this->assertEquals('Ср', $timetablesOutput[2]['week_day']);
        $this->assertEquals('Чт', $timetablesOutput[3]['week_day']);
        $this->assertEquals('Пт', $timetablesOutput[4]['week_day']);
        $this->assertEquals('Сб', $timetablesOutput[5]['week_day']);
        $this->assertEquals('Вс', $timetablesOutput[6]['week_day']);
    }
}