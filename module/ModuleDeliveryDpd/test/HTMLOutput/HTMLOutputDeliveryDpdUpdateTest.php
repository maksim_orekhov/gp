<?php

namespace ModuleDeliveryDpdTest\HTMLOutput;

use ApplicationTest\Bootstrap;
use Core\Service\Base\BaseAuthManager;
use ModuleDeliveryDpd\Controller\DeliveryDpdCatalogController;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\SessionManager;
use CoreTest\ViewVarsTrait;

/**
 * Class HTMLOutputDeliveryDpdUpdateTest
 * @package ModuleDeliveryDpdTest\HTMLOutput
 */
class HTMLOutputDeliveryDpdUpdateTest extends AbstractHttpControllerTestCase
{
    use ViewVarsTrait;

    /**
     * @var string
     */
    private $user_login;

    /**
     * @var string
     */
    private $user_password;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $bootstrap = Bootstrap::getInstance();
        $config = $bootstrap::getConfig();
        $this->setApplicationConfig($config);
        $serviceManager = $this->getApplicationServiceLocator();

        $config = $this->getApplicationConfig();
        $this->user_login = $config['tests']['user_login'];
        $this->user_password = $config['tests']['user_password'];

        $this->entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
        $this->baseAuthManager = $serviceManager->get(\Core\Service\Base\BaseAuthManager::class);
    }

    /**
     * Тестируем рендеринг формы спора
     *
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @throws \Exception
     */
    public function testDpdUpdateActionForAdministrator()
    {
        // Залогиниваем участника сжедки
        $this->login('admin');

        // Проверяем, является ли пост POST-запросом.
        $this->dispatch('/admin/delivery/dpd/update', 'POST');

        $this->assertResponseStatusCode(200);
        $this->assertModuleName('ModuleDeliveryDpd');
        $this->assertControllerName(DeliveryDpdCatalogController::class);
        $this->assertMatchedRouteName('delivery-dpd-update');
        /** @var array $view_vars */
        $view_vars = $this->getViewVars();

        $this->assertArrayHasKey('updateDeliveryDpdFormCity', $view_vars);
        $this->assertArrayHasKey('updateDeliveryDpdFormPoint', $view_vars);
        $this->assertArrayHasKey('updateDeliveryDpdFormCityTableReplace', $view_vars);
        $this->assertArrayHasKey('updateDeliveryDpdFormPointTablesReplace', $view_vars);
        $this->assertArrayHasKey('number_of_cities', $view_vars);
        $this->assertArrayHasKey('number_of_points', $view_vars);
        $this->assertArrayHasKey('update_info', $view_vars);
        //проверяем отгрузилась ли форма
        $this->assertQuery('#section_delivery_dpd_update');
    }

    /**
     * @param string|null $login
     * @param string|null $password
     * @throws \Core\Exception\LogicException
     */
    private function login(string $login = null, string $password = null)
    {
        if (!$login) $login = $this->user_login;
        if (!$password) $password = $this->user_password;
        // Session start
        $serviceManager = $this->getApplication()->getServiceManager();
        // Instantiate SessionManager as default one
        $sessionManager = $serviceManager->get(SessionManager::class);
        #$sessionManager = new SessionManager($sessionConfig);
        $sessionManager->start();

        $this->baseAuthManager->login($login, $password, 0);
    }
}