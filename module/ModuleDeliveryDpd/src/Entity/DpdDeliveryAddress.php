<?php

namespace ModuleDeliveryDpd\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DpdDeliveryAddress
 *
 * @ORM\Table(name="dpd_delivery_address")
 * @ORM\Entity()
 */
class DpdDeliveryAddress
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="terminal_code", type="string", length=16, nullable=true)
     */
    private $terminalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="post_index", type="string", length=6, nullable=true)
     */
    private $index;

    /**
     * @var integer
     *
     * @ORM\Column(name="city_id", type="bigint", length=11, nullable=false, unique=false, options={"unsigned":true})
     */
    private $cityId;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=64, nullable=false)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="street_abr", type="string", length=40, nullable=true)
     */
    private $streetAbr;

    /**
     * @var string
     *
     * @ORM\Column(name="house", type="string", length=16, nullable=true)
     */
    private $house;

    /**
     * @var string
     *
     * @ORM\Column(name="house_korpus", type="string", length=16, nullable=true)
     */
    private $houseKorpus;

    /**
     * @var string
     *
     * @ORM\Column(name="str", type="string", length=16, nullable=true)
     */
    private $str;

    /**
     * @var string
     *
     * @ORM\Column(name="vlad", type="string", length=16, nullable=true)
     */
    private $vlad;

    /**
     * @var string
     *
     * @ORM\Column(name="extra_info", type="string", length=90, nullable=true)
     */
    private $extraInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="office", type="string", length=10, nullable=true)
     */
    private $office;

    /**
     * @var string
     *
     * @ORM\Column(name="flat", type="string", length=10, nullable=true)
     */
    private $flat;

    /**
     * @var string
     *
     * @ORM\Column(name="work_time_from", type="string", length=6, nullable=true)
     */
    private $workTimeFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="work_time_to", type="string", length=6, nullable=true)
     */
    private $workTimeTo;

    /**
     * @var string
     *
     * @ORM\Column(name="dinner_time_from", type="string", length=6, nullable=true)
     */
    private $dinnerTimeFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="dinner_time_to", type="string", length=6, nullable=true)
     */
    private $dinnerTimeTo;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_fio", type="string", length=255, nullable=true)
     */
    private $contactFio;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_phone", type="string", length=16, nullable=true)
     */
    private $contactPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_email", type="string", length=60, nullable=true)
     */
    private $contactEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="instructions", type="string", length=255, nullable=true)
     */
    private $instructions;

    /**
     * @var boolean
     *
     * @ORM\Column(name="need_pass", type="boolean", nullable=true)
     */
    private $needPass;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getTerminalCode()
    {
        return $this->terminalCode;
    }

    /**
     * @param string|null $terminalCode
     */
    public function setTerminalCode($terminalCode)
    {
        $this->terminalCode = $terminalCode;
    }

    /**
     * @return string|null
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param string|null $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->cityId;
    }

    /**
     * @param int $cityId
     */
    public function setCityId(int $cityId)
    {
        $this->cityId = $cityId;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street)
    {
        $this->street = $street;
    }

    /**
     * @return string|null
     */
    public function getStreetAbr()
    {
        return $this->streetAbr;
    }

    /**
     * @param string|null $streetAbr
     */
    public function setStreetAbr($streetAbr)
    {
        $this->streetAbr = $streetAbr;
    }

    /**
     * @return string|null
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param string|null $house
     */
    public function setHouse($house)
    {
        $this->house = $house;
    }

    /**
     * @return string|null
     */
    public function getHouseKorpus()
    {
        return $this->houseKorpus;
    }

    /**
     * @param string|null $houseKorpus
     */
    public function setHouseKorpus($houseKorpus)
    {
        $this->houseKorpus = $houseKorpus;
    }

    /**
     * @return string|null
     */
    public function getStr()
    {
        return $this->str;
    }

    /**
     * @param string|null $str
     */
    public function setStr($str)
    {
        $this->str = $str;
    }

    /**
     * @return string|null
     */
    public function getVlad()
    {
        return $this->vlad;
    }

    /**
     * @param string|null $vlad
     */
    public function setVlad($vlad)
    {
        $this->vlad = $vlad;
    }

    /**
     * @return string|null
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * @param string|null $extraInfo
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;
    }

    /**
     * @return string|null
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * @param string|null $office
     */
    public function setOffice($office)
    {
        $this->office = $office;
    }

    /**
     * @return string|null
     */
    public function getFlat()
    {
        return $this->flat;
    }

    /**
     * @param string|null $flat
     */
    public function setFlat($flat)
    {
        $this->flat = $flat;
    }

    /**
     * @return string|null
     */
    public function getWorkTimeFrom()
    {
        return $this->workTimeFrom;
    }

    /**
     * @param string|null $workTimeFrom
     */
    public function setWorkTimeFrom($workTimeFrom)
    {
        $this->workTimeFrom = $workTimeFrom;
    }

    /**
     * @return string|null
     */
    public function getWorkTimeTo()
    {
        return $this->workTimeTo;
    }

    /**
     * @param string|null $workTimeTo
     */
    public function setWorkTimeTo($workTimeTo)
    {
        $this->workTimeTo = $workTimeTo;
    }

    /**
     * @return string|null
     */
    public function getDinnerTimeFrom()
    {
        return $this->dinnerTimeFrom;
    }

    /**
     * @param string|null $dinnerTimeFrom
     */
    public function setDinnerTimeFrom($dinnerTimeFrom)
    {
        $this->dinnerTimeFrom = $dinnerTimeFrom;
    }

    /**
     * @return string|null
     */
    public function getDinnerTimeTo()
    {
        return $this->dinnerTimeTo;
    }

    /**
     * @param string|null $dinnerTimeTo
     */
    public function setDinnerTimeTo($dinnerTimeTo)
    {
        $this->dinnerTimeTo = $dinnerTimeTo;
    }

    /**
     * @return string|null
     */
    public function getContactFio()
    {
        return $this->contactFio;
    }

    /**
     * @param string|null $contactFio
     */
    public function setContactFio($contactFio)
    {
        $this->contactFio = $contactFio;
    }

    /**
     * @return string|null
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param string|null $contactPhone
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return string|null
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param string|null $contactEmail
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;
    }

    /**
     * @return string|null
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * @param string|null $instructions
     */
    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;
    }

    /**
     * @return bool|null
     */
    public function isNeedPass()
    {
        return $this->needPass;
    }

    /**
     * @param bool|null $needPass
     */
    public function setNeedPass($needPass)
    {
        $this->needPass = $needPass;
    }
}