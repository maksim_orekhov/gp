<?php
namespace ModuleDeliveryDpd\Entity\Repository;

use Core\Exception\LogicException;
use Doctrine\ORM\EntityRepository;
use ModuleDeliveryDpd\Entity\DpdCity;
use ModuleDeliveryDpd\Entity\DpdCityCandidate;
use ModuleDeliveryDpd\Entity\DpdCityPrevious;
use PDO;

/**
 * Class DpdCityRepository
 * @package ModuleDeliveryDpd\Entity\Repository
 */
class DpdCityRepository extends EntityRepository
{
    use \Application\Entity\EntityMetadataTrait;
    use \Application\Entity\EntityTableReplaceTrait;


//    /**
//     * Населенный пункт вместе с dpdPoints и dpdSchedules
//     *
//     * @param int $city_id
//     * @return mixed
//     * @throws \Doctrine\ORM\NoResultException
//     * @throws \Doctrine\ORM\NonUniqueResultException
//     */
//    public function getCityWithRelations(int $city_id)
//    {
//        $qb = $this->createQueryBuilder('c')
//            ->addSelect('p')
//            ->innerJoin('c.dpdPoints', 'p')
//            ->addSelect('sh')
//            ->innerJoin('p.dpdSchedules', 'sh')
//            ->andWhere('c.cityId = :city_id')
//            ->setParameter('city_id', $city_id)
//        ;
//
//        return $qb->getQuery()->getSingleResult();
//    }


    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('c')
            ->select('count(c.id)');

        return $qb->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $search_word
     * @return array
     */
    public function gitCities($search_word)
    {
        $search_word =  mb_strtolower($search_word, 'UTF-8');

        $qb = $this->createQueryBuilder('c')
            ->leftJoin('c.dpdPoints', 'p')
            ->addSelect('COUNT(p.id) AS HIDDEN pointsCount')
            ->andWhere('c.cityName LIKE :search_word')
            ->groupBy('c.id')
            ->addOrderBy('pointsCount', 'DESC')
            ->setParameter('search_word', $search_word.'%')
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getBiggestCities($limit = 20)
    {
        $qb = $this->createQueryBuilder('c')
            ->leftJoin('c.dpdPoints', 'p')
            ->addSelect('COUNT(p.id) AS HIDDEN pointsCount')
            ->groupBy('c.id')
            ->addOrderBy('pointsCount', 'DESC')
            ->setMaxResults($limit)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return bool
     * @throws LogicException
     */
    public function renameTable()
    {
        $entityManager = $this->getEntityManager();

        try {
            $production_table_name = $this->getEntityTableName($entityManager, DpdCity::class);
            $candidate_table_name = $this->getEntityTableName($entityManager, DpdCityCandidate::class);
            $previous_table_name = $this->getEntityTableName($entityManager, DpdCityPrevious::class);
            $this->rename($production_table_name, $candidate_table_name, $previous_table_name);
        }
        catch (\Throwable $t) {

            throw new LogicException($t->getMessage());
        }

        return true;
    }

    /**
     * @param string $production_table_name
     * @param string $candidate_table_name
     * @param string $previous_table_name
     * @throws \Doctrine\DBAL\DBALException
     */
    public function rename(string $production_table_name, string $candidate_table_name, string $previous_table_name)
    {

        try {
            $entityManager = $this->getEntityManager();

            $connection = $entityManager->getConnection();

            // Копируем содержимое table_name_1 в table_name_previous
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $connection->query('TRUNCATE TABLE '.$previous_table_name);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->query('INSERT INTO '.$previous_table_name.' SELECT * FROM '.$production_table_name);

            // Копируем содержимое table_name_1 в table_name_2
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $connection->query('TRUNCATE TABLE '.$production_table_name);
            //дропаем индексы что бы повысить скорость вставки
            $connection->query('DROP INDEX city_ft_index ON '.$production_table_name);
            $connection->query('DROP INDEX full_city_ft_index ON '.$production_table_name);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            //вставка
            $connection->query('INSERT INTO '.$production_table_name.' SELECT * FROM '.$candidate_table_name);
            //возврашаем индексы на место
            $connection->query('CREATE FULLTEXT INDEX city_ft_index ON '.$production_table_name.' (city_name)');
            $connection->query('CREATE FULLTEXT INDEX full_city_ft_index ON '.$production_table_name.' (full_city_name)');
        }
        catch (\Throwable $t){

            logMessage($t->getMessage(), 'dpd-errors');
        }
    }

    /**
     * @param null $search
     * @param int $limit
     * @return array
     * @throws \Exception
     */
    public function getAutoCompleteCities($search = null, $limit = 20)
    {
        $entityManager = $this->getEntityManager();
        $connection = $entityManager->getConnection();
        $result = [];
        if (empty($search)) {
            /** @noinspection SqlDialectInspection */
            /** @noinspection SqlNoDataSourceInspection */
            $cities = $connection->query('SELECT ct.city_id, ct.full_city_name 
                                      FROM 
                                      (SELECT dc.city_id, dc.full_city_name, COUNT(dp.id) as point
                                        FROM dpd_point AS dp
                                        INNER JOIN dpd_city as dc on dc.id = dp.city_id
                                        GROUP BY dp.city_id
                                        ORDER BY point DESC
                                        LIMIT ' . $limit . ') as ct')->fetchAll();


        } else {
            $min_word_len = $connection->query('select @@ft_min_word_len')->fetchColumn();
            $ar_search = $this->getFullTextSearchPhrase($search, (int) $min_word_len);
            $rel_a = '(CASE abbreviation WHEN \'г\' THEN 2 WHEN \'д\' THEN 1 WHEN \'с\' THEN 1 WHEN \'п\' THEN 1 ELSE 0 END)';
            /** @noinspection SqlDialectInspection */
            /** @noinspection SqlNoDataSourceInspection */
            $sql = 'SELECT city_id, region_name, full_city_name, REL_A, REL_C, REL_F
                  FROM
                    ((SELECT *, '.$this->getFullTextMatchString('city_name', $ar_search).' as REL_C, 0 as REL_F, '.$rel_a.' as REL_A
                     FROM dpd_city WHERE '.$this->getFullTextMatchString('city_name', $ar_search).'
                     ORDER BY REL_C DESC, REL_A DESC, city_name ASC
                     LIMIT '.$limit.')
                    UNION
                    (SELECT *, 0 as REL_C, '.$this->getFullTextMatchString('full_city_name', $ar_search).' as REL_F, '.$rel_a.' as REL_A
                     FROM dpd_city WHERE '.$this->getFullTextMatchString('full_city_name', $ar_search).'
                     ORDER BY REL_F DESC, REL_A DESC, full_city_name ASC 
                     LIMIT '.$limit.')) as c
                GROUP BY city_id
                ORDER BY REL_C DESC, REL_F DESC, REL_A DESC, city_name ASC, full_city_name ASC
                LIMIT '.$limit;
            $sth = $connection->prepare($sql);
            $sth->execute();
            $cities = $sth->fetchAll();
        }

        foreach ($cities as $key => $city) {
            $result[$key] = [
                'city_id' => $city['city_id'],
                'full_city_name' => str_ireplace('%', ' ', $city['full_city_name'])
            ];
        }
        return $result;
    }

    /**
     * @param $search
     * @param int $min_word_len
     * @return array
     */
    private function getFullTextSearchPhrase($search, $min_word_len = 4): array
    {
        $ar_search = [
            'phrase' => '',
            'group' => '',
        ];
        // вырезаем из строки нежелательные символы
        $search = str_ireplace(
            ['<','>','\\','!','$','(',')','*','+','?','[',']','^','{','|','}','select','insert','update'],'',
            trim($search));
        //для поиска по полной фразе
        $ar_search['phrase'] = '>"'.$search.'"';
        //для поиска по словам
        //разбиваем фразу на слова (по пробелу)
        $words = explode(' ', $search);
        foreach ($words as $word) {
            //минимальная длинна слова
            if (mb_strlen($word) >= $min_word_len) {
                $ar_search['group'] .= ' +'.$word;
            }
        }
        if (!empty($ar_search['group'])) {
            $ar_search['group'] = ' <('.trim($ar_search['group']).'*)';
        }

        return $ar_search;
    }

    private function getFullTextMatchString($column, $searchPhrase): string
    {
        return 'MATCH('.$column.') AGAINST(\''.$searchPhrase['phrase'].$searchPhrase['group'].'\' IN BOOLEAN MODE)';
    }
}