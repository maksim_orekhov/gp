<?php
namespace ModuleDeliveryDpd\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use ModuleDeliveryDpd\Entity\DpdCity;
use ModuleDeliveryDpd\Entity\DpdParcelShopLimits;
use ModuleDeliveryDpd\Entity\DpdParcelShopLimitsCandidate;
use ModuleDeliveryDpd\Entity\DpdParcelShopLimitsPrevious;
use ModuleDeliveryDpd\Entity\DpdPoint;
use ModuleDeliveryDpd\Entity\DpdPointCandidate;
use ModuleDeliveryDpd\Entity\DpdPointPrevious;
use ModuleDeliveryDpd\Entity\DpdSchedule;
use ModuleDeliveryDpd\Entity\DpdScheduleCandidate;
use ModuleDeliveryDpd\Entity\DpdSchedulePrevious;
use ModuleDeliveryDpd\Entity\DpdTimetable;
use ModuleDeliveryDpd\Entity\DpdTimetableCandidate;
use ModuleDeliveryDpd\Entity\DpdTimetablePrevious;

/**
 * Class DpdPointRepository
 * @package ModuleDeliveryDpd\Entity\Repository
 */
class DpdPointRepository extends EntityRepository
{
    use \Application\Entity\EntityMetadataTrait;
    #use \Application\Entity\EntityTableReplaceTrait;

    /**
     * @param DpdCity $city
     * @param string|null $schedule_type
     * @return array
     */
    public function getPointsForCity(DpdCity $city, string $schedule_type = null): array
    {
        $qb = $this->createQueryBuilder('p')
            ->addSelect('c')
            ->innerJoin('p.city', 'c')
            ->addSelect('sh')
            ->innerJoin('p.dpdSchedules', 'sh')
            ->addSelect('lm')
            ->innerJoin('p.dpdParcelShopLimits', 'lm')
            ->andWhere('p.city = :city')
            ->setParameter('city', $city)
        ;
        if (null !== $schedule_type) {
            $qb ->andWhere('sh.type = :schedule_type')
                ->setParameter('schedule_type', $schedule_type)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('p')
            ->select('count(p.id)');

        return $qb->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $result
     * @return mixed
     */
    public function renameTables(&$result)
    {
        $entityManager = $this->getEntityManager();

        try {
            $production_table_name = $this->getEntityTableName($entityManager, DpdPoint::class);
            $candidate_table_name = $this->getEntityTableName($entityManager, DpdPointCandidate::class);
            $table_name_previous = $this->getEntityTableName($entityManager, DpdPointPrevious::class);
            $this->rename($production_table_name, $candidate_table_name, $table_name_previous);

            $result['message'][] = 'DpdPoint replaced by DpdPointCandidate';

            $production_table_name_2 = $this->getEntityTableName($entityManager, DpdParcelShopLimits::class);
            $candidate_table_name_2 = $this->getEntityTableName($entityManager, DpdParcelShopLimitsCandidate::class);
            $previous_table_name_2 = $this->getEntityTableName($entityManager, DpdParcelShopLimitsPrevious::class);
            $this->rename($production_table_name_2, $candidate_table_name_2, $previous_table_name_2);

            $result['message'][] = 'DpdParcelShopLimits replaced by DpdParcelShopLimitsCandidate';

            $production_table_name_3 = $this->getEntityTableName($entityManager, DpdSchedule::class);
            $candidate_table_name_3 = $this->getEntityTableName($entityManager, DpdScheduleCandidate::class);
            $previous_table_name_3 = $this->getEntityTableName($entityManager, DpdSchedulePrevious::class);
            $this->rename($production_table_name_3, $candidate_table_name_3, $previous_table_name_3);

            $result['message'][] = 'DpdSchedule replaced by DpdScheduleCandidate';

            $production_table_name_4 = $this->getEntityTableName($entityManager, DpdTimetable::class);
            $candidate_table_name_4 = $this->getEntityTableName($entityManager, DpdTimetableCandidate::class);
            $previous_table_name_4 = $this->getEntityTableName($entityManager, DpdTimetablePrevious::class);
            $this->rename($production_table_name_4, $candidate_table_name_4, $previous_table_name_4);

            $result['message'][] = 'DpdTimetable replaced by DpdTimetableCandidate';

            return $result;
        }
        catch (\Throwable $t) {

            throw new \LogicException($t->getMessage());
        }
    }

    /**
     * @param string $production_table_name
     * @param string $candidate_table_name
     * @param string|null $previous_table_name
     * @throws \Doctrine\DBAL\DBALException
     */
    public function rename(string $production_table_name, string $candidate_table_name, string $previous_table_name)
    {
        $entityManager = $this->getEntityManager();

        $connection = $entityManager->getConnection();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');

        // Копируем содержимое table_name_1 в table_name_previous
        $connection->query('TRUNCATE TABLE '.$previous_table_name);
        $connection->query('INSERT INTO '.$previous_table_name.' SELECT * FROM '.$production_table_name);

        // Копируем содержимое table_name_1 в table_name_2
        $connection->query('TRUNCATE TABLE '.$production_table_name);
        $connection->query('INSERT INTO '.$production_table_name.' SELECT * FROM '.$candidate_table_name);

        $connection->query('SET FOREIGN_KEY_CHECKS=1');
    }
}