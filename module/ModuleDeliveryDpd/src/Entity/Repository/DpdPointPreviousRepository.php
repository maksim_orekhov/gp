<?php
namespace ModuleDeliveryDpd\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class DpdPointPreviousRepository
 * @package ModuleDeliveryDpd\Entity\Repository
 */
class DpdPointPreviousRepository extends EntityRepository
{
    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('p')
            ->select('count(p.id)');

        return $qb->getQuery()
            ->getSingleScalarResult();
    }
}