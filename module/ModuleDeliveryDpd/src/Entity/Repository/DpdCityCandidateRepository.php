<?php
namespace ModuleDeliveryDpd\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use ModuleDeliveryDpd\Entity\DpdCityCandidate;

/**
 * Class DpdCityCandidateRepository
 * @package ModuleDeliveryDpd\Entity\Repository
 */
class DpdCityCandidateRepository extends EntityRepository
{
    use \Application\Entity\EntityMetadataTrait;
    use \Application\Entity\EntityTableTruncateTrait;

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('c')
            ->select('count(c.id)');

        return $qb->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     * @return void
     */
    public function truncateTable()
    {
        $entityManager = $this->getEntityManager();

        $table_name = $this->getEntityTableName($entityManager, DpdCityCandidate::class);

        $this->truncate($entityManager, $table_name);
    }

    /**
     * @param $file_path
     * @throws \Doctrine\DBAL\DBALException
     */
    public function loadDataInFile($file_path)
    {
        if (!file_exists($file_path)) {

            throw new \RuntimeException('sql execution error: file not found at specified path');
        }
        $entityManager = $this->getEntityManager();
        $table_name = $this->getEntityTableName($entityManager, DpdCityCandidate::class);
        /** @var \Doctrine\DBAL\Connection $connection */
        $connection = $entityManager->getConnection();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $connection->query('TRUNCATE `'.$table_name.'`');
        $connection->query('SET FOREIGN_KEY_CHECKS=1');

        $connection->query('SET AUTOCOMMIT=0');
        $connection->query("LOAD DATA LOCAL INFILE '".$file_path."' 
                            INTO TABLE ".$table_name."
                            CHARACTER SET utf8 
                            FIELDS TERMINATED BY ';'
                            ENCLOSED BY '\"'
                            LINES TERMINATED BY '\\n'
                            (city_id, country_code, country_name, region_code, region_name, city_code, city_name, abbreviation, index_min, index_max, payment_on_delivery, full_city_name)");
        $connection->query('COMMIT');
    }
}