<?php
namespace ModuleDeliveryDpd\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use ModuleDeliveryDpd\Entity\DpdParcelShopLimitsCandidate;
use ModuleDeliveryDpd\Entity\DpdPointCandidate;
use ModuleDeliveryDpd\Entity\DpdScheduleCandidate;
use ModuleDeliveryDpd\Entity\DpdTimetableCandidate;

/**
 * Class DpdPointCandidateRepository
 * @package ModuleDeliveryDpd\Entity\Repository
 */
class DpdPointCandidateRepository extends EntityRepository
{
    use \Application\Entity\EntityMetadataTrait;
    use \Application\Entity\EntityTableTruncateTrait;

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('p')
            ->select('count(p.id)');

        return $qb->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     * @return void
     */
    public function truncateTables()
    {
        $entityManager = $this->getEntityManager();

        // Обнуляем таблицу DpdPointCandidate
        $table_name = $this->getEntityTableName($entityManager, DpdPointCandidate::class);
        $this->truncate($entityManager, $table_name);
        // Обнуляем таблицу DpdParcelShopLimitsCandidate
        $table_name = $this->getEntityTableName($entityManager, DpdParcelShopLimitsCandidate::class);
        $this->truncate($entityManager, $table_name);
        // Обнуляем таблицу DpdScheduleCandidate
        $table_name = $this->getEntityTableName($entityManager, DpdScheduleCandidate::class);
        $this->truncate($entityManager, $table_name);
        // Обнуляем таблицу DpdTimetableCandidate
        $table_name = $this->getEntityTableName($entityManager, DpdTimetableCandidate::class);
        $this->truncate($entityManager, $table_name);
    }
}