<?php
namespace ModuleDeliveryDpd\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class DpdCityPreviousRepository
 * @package ModuleDeliveryDpd\Entity\Repository
 */
class DpdCityPreviousRepository extends EntityRepository
{
    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countAll()
    {
        $qb = $this->createQueryBuilder('c')
            ->select('count(c.id)');

        return $qb->getQuery()
            ->getSingleScalarResult();
    }
}