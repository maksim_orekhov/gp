<?php

namespace ModuleDeliveryDpd\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DpdTimetable
 *
 * @ORM\Table(name="dpd_timetable")
 * @ORM\Entity();
 */
class DpdTimetable
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="week_days", type="string", length=20, nullable=false, unique=false)
     */
    private $weekDays;

    /**
     * @var string
     *
     * @ORM\Column(name="work_time", type="string", length=20, nullable=false, unique=false)
     */
    private $workTime;

    /**
     * Many DpdTimetable have One DpdSchedule.
     * @ORM\ManyToOne(targetEntity="DpdSchedule", inversedBy="dpdTimetables")
     * @ORM\JoinColumn(name="schedule_id", referencedColumnName="id")
     */
    private $schedule;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getWeekDays(): string
    {
        return $this->weekDays;
    }

    /**
     * @param string $weekDays
     */
    public function setWeekDays(string $weekDays)
    {
        $this->weekDays = $weekDays;
    }

    /**
     * @return string
     */
    public function getWorkTime(): string
    {
        return $this->workTime;
    }

    /**
     * @param string $workTime
     */
    public function setWorkTime(string $workTime)
    {
        $this->workTime = $workTime;
    }

    /**
     * @return mixed
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * @param mixed $schedule
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;
    }
}