<?php

namespace ModuleDeliveryDpd\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DpdSystemInfo
 *
 * @ORM\Table(name="dpd_update_info")
 * @ORM\Entity
 */
class DpdUpdateInfo
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=25, nullable=false, unique=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_dpd_city_candidate_update", type="datetime", nullable=true)
     */
    private $lastDpdCityCandidateUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_dpd_point_candidate_update", type="datetime", nullable=true)
     */
    private $lastDpdPointCandidateUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_dpd_city_production_update", type="datetime", nullable=true)
     */
    private $lastDpdCityProductionUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_dpd_point_production_update", type="datetime", nullable=true)
     */
    private $lastDpdPointProductionUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_dpd_city_previous_update", type="datetime", nullable=true)
     */
    private $lastDpdCityPreviousUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_dpd_point_previous_update", type="datetime", nullable=true)
     */
    private $lastDpdPointPreviousUpdate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastDpdCityCandidateUpdate()
    {
        return $this->lastDpdCityCandidateUpdate;
    }

    /**
     * @param \DateTime|null $lastDpdCityCandidateUpdate
     */
    public function setLastDpdCityCandidateUpdate($lastDpdCityCandidateUpdate)
    {
        $this->lastDpdCityCandidateUpdate = $lastDpdCityCandidateUpdate;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastDpdPointCandidateUpdate()
    {
        return $this->lastDpdPointCandidateUpdate;
    }

    /**
     * @param \DateTime|null $lastDpdPointCandidateUpdate
     */
    public function setLastDpdPointCandidateUpdate($lastDpdPointCandidateUpdate)
    {
        $this->lastDpdPointCandidateUpdate = $lastDpdPointCandidateUpdate;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastDpdCityProductionUpdate()
    {
        return $this->lastDpdCityProductionUpdate;
    }

    /**
     * @param \DateTime|null $lastDpdCityProductionUpdate
     */
    public function setLastDpdCityProductionUpdate($lastDpdCityProductionUpdate)
    {
        $this->lastDpdCityProductionUpdate = $lastDpdCityProductionUpdate;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastDpdPointProductionUpdate()
    {
        return $this->lastDpdPointProductionUpdate;
    }

    /**
     * @param \DateTime|null $lastDpdPointProductionUpdate
     */
    public function setLastDpdPointProductionUpdate($lastDpdPointProductionUpdate)
    {
        $this->lastDpdPointProductionUpdate = $lastDpdPointProductionUpdate;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastDpdCityPreviousUpdate()
    {
        return $this->lastDpdCityPreviousUpdate;
    }

    /**
     * @param \DateTime|null $lastDpdCityPreviousUpdate
     */
    public function setLastDpdCityPreviousUpdate($lastDpdCityPreviousUpdate)
    {
        $this->lastDpdCityPreviousUpdate = $lastDpdCityPreviousUpdate;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastDpdPointPreviousUpdate()
    {
        return $this->lastDpdPointPreviousUpdate;
    }

    /**
     * @param \DateTime|null $lastDpdPointPreviousUpdate
     */
    public function setLastDpdPointPreviousUpdate($lastDpdPointPreviousUpdate)
    {
        $this->lastDpdPointPreviousUpdate = $lastDpdPointPreviousUpdate;
    }
}