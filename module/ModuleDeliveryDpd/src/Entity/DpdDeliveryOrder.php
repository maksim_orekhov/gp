<?php

namespace ModuleDeliveryDpd\Entity;

use Doctrine\ORM\Mapping as ORM;
use ModuleDelivery\Entity\DeliveryOrder;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * DpdDeliveryOrder
 *
 * @ORM\Table(name="dpd_delivery_order")
 * @ORM\Entity()
 */
class DpdDeliveryOrder
{
    const DUPLICATE = 'OrderDuplicate';
    const PENDING = 'OrderPending';
    const ERROR = 'OrderError';
    const OK = 'OK';


    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="order_number", type="string", length=45, nullable=true)
     */
    private $orderNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_pickup", type="datetime", nullable=true)
     */
    private $datePickup;

    /**
     * @var string
     *
     * @ORM\Column(name="pickup_time_period", type="string", length=6, nullable=true)
     */
    private $pickupTimePeriod;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_time_period", type="string", length=6, nullable=true)
     */
    private $deliveryTimePeriod;

    /**
     * @var string
     *
     * @ORM\Column(name="service_code", type="string", length=60, nullable=false)
     */
    private $serviceCode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_cargo_valuable", type="boolean", nullable=true)
     */
    private $isCargoValuable;

    /**
     * @var float
     *
     * @ORM\Column(name="declared_value", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $declaredValue;

    /**
     * @var string
     *
     * @ORM\Column(name="cargo_category", type="string", length=128, nullable=true)
     */
    private $cargoCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_type", type="string", length=3, nullable=true)
     */
    private $paymentType;

    /**
     * @var string
     *
     * @ORM\Column(name="dpd_order_creation_status", type="string", length=20, nullable=true)
     */
    private $dpdOrderCreationStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="dpd_order_creation_error", type="text", length=500, nullable=true)
     */
    private $dpdOrderCreationError;

    /**
     * @var DeliveryOrder
     *
     * @ORM\OneToOne(targetEntity="ModuleDelivery\Entity\DeliveryOrder", inversedBy="dpdDeliveryOrder")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="delivery_order_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $deliveryOrder;

    /**
     * @ORM\OneToOne(targetEntity="DpdDeliveryAddress", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="sender_address_id", referencedColumnName="id")
     */
    private $senderAddress;

    /**
     * @ORM\OneToOne(targetEntity="DpdDeliveryAddress", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="receiver_address_id", referencedColumnName="id")
     */
    private $receiverAddress;

    /**
     * One DpdDeliveryOrder has Many DpdDeliveryParcels.
     * @ORM\OneToMany(targetEntity="DpdDeliveryParcel", mappedBy="dpdDeliveryOrder", cascade={"persist", "remove"})
     */
    private $parcels;


    public function __construct()
    {
        $this->parcels = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string|null $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return \DateTime|null
     */
    public function getDatePickup()
    {
        return $this->datePickup;
    }

    /**
     * @param \DateTime|null $datePickup
     */
    public function setDatePickup($datePickup)
    {
        $this->datePickup = $datePickup;
    }

    /**
     * @return string|null
     */
    public function getPickupTimePeriod()
    {
        return $this->pickupTimePeriod;
    }

    /**
     * @param string|null $pickupTimePeriod
     */
    public function setPickupTimePeriod($pickupTimePeriod)
    {
        $this->pickupTimePeriod = $pickupTimePeriod;
    }

    /**
     * @return string|null
     */
    public function getDeliveryTimePeriod()
    {
        return $this->deliveryTimePeriod;
    }

    /**
     * @param string|null $deliveryTimePeriod
     */
    public function setDeliveryTimePeriod($deliveryTimePeriod)
    {
        $this->deliveryTimePeriod = $deliveryTimePeriod;
    }

    /**
     * @return string|null
     */
    public function getServiceCode()
    {
        return $this->serviceCode;
    }

    /**
     * @param string|null $serviceCode
     */
    public function setServiceCode($serviceCode)
    {
        $this->serviceCode = $serviceCode;
    }

    /**
     * @return bool
     */
    public function isCargoValuable(): bool
    {
        return $this->isCargoValuable;
    }

    /**
     * @param bool $isCargoValuable
     */
    public function setIsCargoValuable(bool $isCargoValuable)
    {
        $this->isCargoValuable = $isCargoValuable;
    }

    /**
     * @return int|null
     */
    public function getDeclaredValue()
    {
        return $this->declaredValue;
    }

    /**
     * @param int|null $declaredValue
     */
    public function setDeclaredValue($declaredValue)
    {
        $this->declaredValue = $declaredValue;
    }

    /**
     * @return string|null
     */
    public function getCargoCategory()
    {
        return $this->cargoCategory;
    }

    /**
     * @param string|null
     */
    public function setCargoCategory($cargoCategory)
    {
        $this->cargoCategory = $cargoCategory;
    }

    /**
     * @return string|null
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param string|null $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return string|null
     */
    public function getDpdOrderCreationStatus()
    {
        return $this->dpdOrderCreationStatus;
    }

    /**
     * @param string|null $dpdOrderCreationStatus
     */
    public function setDpdOrderCreationStatus($dpdOrderCreationStatus)
    {
        $this->dpdOrderCreationStatus = $dpdOrderCreationStatus;
    }

    /**
     * @return string|null
     */
    public function getDpdOrderCreationError()
    {
        return $this->dpdOrderCreationError;
    }

    /**
     * @param string|null $dpdOrderCreationError
     */
    public function setDpdOrderCreationError($dpdOrderCreationError)
    {
        $this->dpdOrderCreationError = $dpdOrderCreationError;
    }

    /**
     * @return DeliveryOrder|null
     */
    public function getDeliveryOrder()
    {
        return $this->deliveryOrder;
    }

    /**
     * @param DeliveryOrder|null $deliveryOrder
     */
    public function setDeliveryOrder($deliveryOrder)
    {
        $this->deliveryOrder = $deliveryOrder;
    }

    /**
     * @return mixed
     */
    public function getSenderAddress()
    {
        return $this->senderAddress;
    }

    /**
     * @param mixed $senderAddress
     */
    public function setSenderAddress($senderAddress)
    {
        $this->senderAddress = $senderAddress;
    }

    /**
     * @return mixed
     */
    public function getReceiverAddress()
    {
        return $this->receiverAddress;
    }

    /**
     * @param mixed $receiverAddress
     */
    public function setReceiverAddress($receiverAddress)
    {
        $this->receiverAddress = $receiverAddress;
    }

    /**
     * @return mixed
     */
    public function getParcels()
    {
        return $this->parcels;
    }

    /**
     * @param mixed $parcels
     */
    public function setParcels($parcels)
    {
        $this->parcels = $parcels;
    }

    /**
     * @param DpdDeliveryParcel $parcel
     */
    public function addParcel(DpdDeliveryParcel $parcel)
    {
        $this->parcels->add($parcel);
    }
}

