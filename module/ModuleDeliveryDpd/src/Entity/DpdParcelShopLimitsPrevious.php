<?php

namespace ModuleDeliveryDpd\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DpdParcelShopStatePrevious
 *
 * @ORM\Table(name="dpd_parcel_shop_limits_previous")
 * @ORM\Entity
 */
class DpdParcelShopLimitsPrevious
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float", nullable=true, unique=false)
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer", nullable=true, unique=false)
     */
    private $width;

    /**
     * @var integer
     *
     * @ORM\Column(name="length", type="integer", nullable=true, unique=false)
     */
    private $length;

    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer", nullable=true, unique=false)
     */
    private $height;

    /**
     * @var integer
     *
     * @ORM\Column(name="dimension_sum", type="integer", nullable=true, unique=false)
     */
    private $dimensionSum;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float|null
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float|null $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return int|null
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int|null $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int|null
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param int|null $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * @return int|null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int|null $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return int|null
     */
    public function getDimensionSum()
    {
        return $this->dimensionSum;
    }

    /**
     * @param int|null $dimensionSum
     */
    public function setDimensionSum($dimensionSum)
    {
        $this->dimensionSum = $dimensionSum;
    }
}