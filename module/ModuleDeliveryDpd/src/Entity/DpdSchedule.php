<?php

namespace ModuleDeliveryDpd\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DpdSchedule
 *
 * @ORM\Table(name="dpd_schedule")
 * @ORM\Entity();
 */
class DpdSchedule
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=false, unique=false)
     */
    private $type;

    /**
     * Many DpdSchedule have One DpdPoint
     * @ORM\ManyToOne(targetEntity="DpdPoint", inversedBy="dpdSchedules")
     * @ORM\JoinColumn(name="dpd_point_id", referencedColumnName="id")
     */
    private $dpdPoint;

    /**
     * One DpdSchedule has Many DpdTimetables.
     * @ORM\OneToMany(targetEntity="DpdTimetable", mappedBy="schedule")
     */
    private $dpdTimetables;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDpdPoint()
    {
        return $this->dpdPoint;
    }

    /**
     * @param mixed $dpdPoint
     */
    public function setDpdPoint($dpdPoint)
    {
        $this->dpdPoint = $dpdPoint;
    }

    /**
     * @return mixed
     */
    public function getDpdTimetables()
    {
        return $this->dpdTimetables;
    }

    /**
     * @param mixed $dpdTimetables
     */
    public function setDpdTimetables($dpdTimetables)
    {
        $this->dpdTimetables = $dpdTimetables;
    }
}