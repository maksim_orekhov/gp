<?php

namespace ModuleDeliveryDpd\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * DpdCityPrevious
 *
 * @ORM\Table(name="dpd_city_previous")
 * @ORM\Entity(repositoryClass="ModuleDeliveryDpd\Entity\Repository\DpdCityPreviousRepository");
 */
class DpdCityPrevious
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="city_id", type="bigint", length=11, nullable=false, unique=true, options={"unsigned":true})
     */
    private $cityId;

    /**
     * @var string
     *
     * @ORM\Column(name="country_code", type="string", length=2, nullable=false, unique=false)
     */
    private $countryCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country_name", type="string", length=16, nullable=true, unique=false)
     */
    private $countryName;

    /**
     * @var string
     *
     * @ORM\Column(name="region_code", type="string", length=3, nullable=false, unique=false)
     */
    private $regionCode;

    /**
     * @var string
     *
     * @ORM\Column(name="region_name", type="string", length=128, nullable=false, unique=false)
     */
    private $regionName;

    /**
     * @var string
     *
     * @ORM\Column(name="city_code", type="string", length=11, nullable=false, unique=false)
     */
    private $cityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city_name", type="string", length=64, nullable=false, unique=false)
     */
    private $cityName;

    /**
     * @var string
     *
     * @ORM\Column(name="abbreviation", type="string", length=20, nullable=true, unique=false)
     */
    private $abbreviation;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_min", type="integer", length=11, nullable=true, unique=false)
     */
    private $indexMin;

    /**
     * @var integer
     *
     * @ORM\Column(name="index_max", type="integer", length=11, nullable=true, unique=false)
     */
    private $indexMax;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payment_on_delivery", type="boolean", nullable=false, unique=false)
     */
    private $paymentOnDelivery;

    /**
     * @var string
     *
     * @ORM\Column(name="full_city_name", type="string", length=192, nullable=true, unique=false)
     */
    private $fullCityName;

    /**
     * One DpdCity has Many DpdPoints.
     * @ORM\OneToMany(targetEntity="DpdPointPrevious", mappedBy="city")
     */
    private $dpdPoints;


    /**
     * Deal constructor.
     */
    public function __construct()
    {
        $this->dpdPoints = new ArrayCollection;
    }

    public function __toString()
    {
        return $this->cityName;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->cityId;
    }

    /**
     * @param int $cityId
     */
    public function setCityId(int $cityId)
    {
        $this->cityId = $cityId;
    }

    /**
     * @return string
     */
    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode(string $countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return string|null
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * @param string|null $countryName
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;
    }

    /**
     * @return string
     */
    public function getRegionCode()
    {
        return $this->regionCode;
    }

    /**
     * @param string $regionCode
     */
    public function setRegionCode($regionCode)
    {
        $this->regionCode = $regionCode;
    }

    /**
     * @return string
     */
    public function getRegionName(): string
    {
        return $this->regionName;
    }

    /**
     * @param string $regionName
     */
    public function setRegionName(string $regionName)
    {
        $this->regionName = $regionName;
    }

    /**
     * @return string
     */
    public function getCityCode(): string
    {
        return $this->cityCode;
    }

    /**
     * @param string $cityCode
     */
    public function setCityCode(string $cityCode)
    {
        $this->cityCode = $cityCode;
    }

    /**
     * @return string
     */
    public function getCityName(): string
    {
        return $this->cityName;
    }

    /**
     * @param string $cityName
     */
    public function setCityName(string $cityName)
    {
        $this->cityName = $cityName;
    }

    /**
     * @return string|null
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * @param string|null $abbreviation
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;
    }

    /**
     * @return int|null
     */
    public function getIndexMin()
    {
        return $this->indexMin;
    }

    /**
     * @param int|null $indexMin
     */
    public function setIndexMin($indexMin)
    {
        $this->indexMin = $indexMin;
    }

    /**
     * @return int|null
     */
    public function getIndexMax()
    {
        return $this->indexMax;
    }

    /**
     * @param int|null $indexMax
     */
    public function setIndexMax($indexMax)
    {
        $this->indexMax = $indexMax;
    }

    /**
     * @return bool
     */
    public function isPaymentOnDelivery(): bool
    {
        return $this->paymentOnDelivery;
    }

    /**
     * @param bool $paymentOnDelivery
     */
    public function setPaymentOnDelivery(bool $paymentOnDelivery)
    {
        $this->paymentOnDelivery = $paymentOnDelivery;
    }

    /**
     * @return string|null
     */
    public function getFullCityName()
    {
        return $this->fullCityName;
    }

    /**
     * @param string|null $fullCityName
     */
    public function setFullCityName($fullCityName)
    {
        $this->fullCityName = $fullCityName;
    }

    /**
     * @return mixed
     */
    public function getDpdPoints()
    {
        return $this->dpdPoints;
    }

    /**
     * @param mixed $dpdPoints
     */
    public function setDpdPoints($dpdPoints)
    {
        $this->dpdPoints = $dpdPoints;
    }
}