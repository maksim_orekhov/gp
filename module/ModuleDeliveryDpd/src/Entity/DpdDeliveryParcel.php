<?php

namespace ModuleDeliveryDpd\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DpdDeliveryParcel
 *
 * @ORM\Table(name="dpd_delivery_parcel")
 * @ORM\Entity()
 */
class DpdDeliveryParcel
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="decimal", precision=6, scale=2, nullable=true)
     */
    private $weight;

    /**
     * @var float
     *
     * @ORM\Column(name="length", type="decimal", precision=7, scale=3, nullable=true)
     */
    private $length;

    /**
     * @var float
     *
     * @ORM\Column(name="width", type="decimal", precision=7, scale=3, nullable=true)
     */
    private $width;

    /**
     * @var float
     *
     * @ORM\Column(name="parcel_height", type="decimal", precision=7, scale=3, nullable=true)
     */
    private $height;

    /**
     * Many DpdDeliveryParcels have One DpdDeliveryOrder.
     * @ORM\ManyToOne(targetEntity="DpdDeliveryOrder", inversedBy="parcels", cascade={"persist"})
     * @ORM\JoinColumn(name="dpd_delivery_order_id", referencedColumnName="id")
     */
    private $dpdDeliveryOrder;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float|null
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float|null $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return float|null
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @param float|null $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * @return float|null
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param float|null $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return float|null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param float|null $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getDpdDeliveryOrder()
    {
        return $this->dpdDeliveryOrder;
    }

    /**
     * @param mixed $dpdDeliveryOrder
     */
    public function setDpdDeliveryOrder($dpdDeliveryOrder)
    {
        $this->dpdDeliveryOrder = $dpdDeliveryOrder;
    }
}