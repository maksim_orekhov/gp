<?php

namespace ModuleDeliveryDpd\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DpdPointCandidate
 *
 * @ORM\Table(name="dpd_point_candidate")
 * @ORM\Entity(repositoryClass="ModuleDeliveryDpd\Entity\Repository\DpdPointCandidateRepository");
 */
class DpdPointCandidate
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue (strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=16, nullable=false, unique=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true, unique=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cashpay", type="boolean", nullable=true, unique=false)
     */
    private $cashpay;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=64, nullable=false, unique=false)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="street_abr", type="string", length=40, nullable=true, unique=false)
     */
    private $streetAbr;

    /**
     * @var string
     *
     * @ORM\Column(name="house", type="string", length=16, nullable=true, unique=false)
     */
    private $house;

    /**
     * @var string
     *
     * @ORM\Column(name="structure", type="string", length=16, nullable=true, unique=false)
     */
    private $structure;

    /**
     * @var string
     *
     * @ORM\Column(name="ownership", type="string", length=16, nullable=true, unique=false)
     */
    private $ownership;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=10, nullable=false, unique=false)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=10, nullable=false, unique=false)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true, unique=false)
     */
    private $description;

    /**
     * Many DpdPoint have One DpdCity.
     * @ORM\ManyToOne(targetEntity="DpdCityCandidate", inversedBy="dpdPoints")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    /**
     * Unidirectional
     * @ORM\ManyToOne(targetEntity="DpdParcelShopType")
     * @ORM\JoinColumn(name="dpd_parcel_shop_type_id", referencedColumnName="id")
     */
    private $dpdParcelShopType;

    /**
     * Unidirectional
     * @ORM\ManyToOne(targetEntity="DpdParcelShopState")
     * @ORM\JoinColumn(name="dpd_parcel_shop_state_id", referencedColumnName="id")
     */
    private $dpdParcelShopState;

    /**
     * Unidirectional
     * @ORM\ManyToOne(targetEntity="DpdParcelShopLimitsCandidate")
     * @ORM\JoinColumn(name="dpd_parcel_shop_limits_id", referencedColumnName="id")
     */
    private $dpdParcelShopLimits;

    /**
     * One DpdPoint has Many DpdSchedules.
     * @ORM\OneToMany(targetEntity="DpdScheduleCandidate", mappedBy="dpdPoint")
     */
    private $dpdSchedules;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string|null $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return bool|null
     */
    public function isCashpay()
    {
        return $this->cashpay;
    }

    /**
     * @param bool|null $cashpay
     */
    public function setCashpay($cashpay)
    {
        $this->cashpay = $cashpay;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street)
    {
        $this->street = $street;
    }

    /**
     * @return string|null
     */
    public function getStreetAbr()
    {
        return $this->streetAbr;
    }

    /**
     * @param string|null $streetAbr
     */
    public function setStreetAbr($streetAbr)
    {
        $this->streetAbr = $streetAbr;
    }

    /**
     * @return string|null
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * @param string|null $house
     */
    public function setHouse($house)
    {
        $this->house = $house;
    }

    /**
     * @return string|null
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @param string|null $structure
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;
    }

    /**
     * @return string|null
     */
    public function getOwnership()
    {
        return $this->ownership;
    }

    /**
     * @param string|null $ownership
     */
    public function setOwnership($ownership)
    {
        $this->ownership = $ownership;
    }

    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude(string $latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude(string $longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return DpdCityCandidate
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity(DpdCityCandidate $city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getDpdParcelShopType()
    {
        return $this->dpdParcelShopType;
    }

    /**
     * @param mixed $dpdParcelShopType
     */
    public function setDpdParcelShopType($dpdParcelShopType)
    {
        $this->dpdParcelShopType = $dpdParcelShopType;
    }

    /**
     * @return mixed
     */
    public function getDpdParcelShopState()
    {
        return $this->dpdParcelShopState;
    }

    /**
     * @param mixed $dpdParcelShopState
     */
    public function setDpdParcelShopState($dpdParcelShopState)
    {
        $this->dpdParcelShopState = $dpdParcelShopState;
    }

    /**
     * @return mixed
     */
    public function getDpdParcelShopLimits()
    {
        return $this->dpdParcelShopLimits;
    }

    /**
     * @param mixed $dpdParcelShopLimits
     */
    public function setDpdParcelShopLimits($dpdParcelShopLimits)
    {
        $this->dpdParcelShopLimits = $dpdParcelShopLimits;
    }

    /**
     * @return mixed
     */
    public function getDpdSchedules()
    {
        return $this->dpdSchedules;
    }

    /**
     * @param mixed $dpdSchedules
     */
    public function setDpdSchedules($dpdSchedules)
    {
        $this->dpdSchedules = $dpdSchedules;
    }
}