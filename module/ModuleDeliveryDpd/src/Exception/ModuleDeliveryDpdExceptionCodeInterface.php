<?php
namespace ModuleDeliveryDpd\Exception;

use ModuleDeliveryDpd\Exception\Code\DeliveryDpdExceptionCodeInterface;

/**
 * Interface ModuleDeliveryDpdExceptionCodeInterface
 * @package ModuleDeliveryDpd\Exception
 */
interface ModuleDeliveryDpdExceptionCodeInterface extends
    DeliveryDpdExceptionCodeInterface
{
    /**
     * Только для общих кодов, которые могут быть использованы везде!!!
     *
     * без префикса, для сообшений суфикс _MESSAGE
     */

    const SENDER_CITY_NOT_FOUND = 'SENDER_CITY_NOT_FOUND';
    const SENDER_CITY_NOT_FOUND_MESSAGE = 'Sender city not found.';

    const RECEIVER_CITY_NOT_FOUND = 'RECEIVER_CITY_NOT_FOUND';
    const RECEIVER_CITY_NOT_FOUND_MESSAGE = 'Receiver city not found.';
}