<?php

namespace ModuleDeliveryDpd\Command;

use Application\Service\DeliveryManager;
use Core\Exception\LogicException;
use ModuleDeliveryDpd\Service\DeliveryDpdStatusManager;
use Zend\Console\Adapter\AdapterInterface;
use ZF\Console\Route;

/**
 * Class DeliveryDpdStatusesUpdateCommand
 * @package ModuleDeliveryDpd\Command
 */
class DeliveryDpdStatusesUpdateCommand
{
    /**
     * @var DeliveryDpdStatusManager
     */
    private $deliveryDpdStatusManager;

    /**
     * @var DeliveryManager
     */
    private $deliveryManager;

    /**
     * DeliveryDpdStatusesUpdateCommand constructor.
     * @param DeliveryDpdStatusManager $deliveryDpdStatusManager
     * @param DeliveryManager $deliveryManager
     */
    public function __construct(DeliveryDpdStatusManager $deliveryDpdStatusManager,
                                DeliveryManager $deliveryManager)
    {
        $this->deliveryDpdStatusManager = $deliveryDpdStatusManager;
        $this->deliveryManager = $deliveryManager;
    }

    /**
     * Команда для запуска & bin/console delivery dpd statuses update
     *
     * @param Route $route
     * @param AdapterInterface $console
     */
    public function __invoke(Route $route, AdapterInterface $console)
    {
        $console->writeLine('Delivery DPD statuses updating...');

        try {
            // Получаем информацию по посылкам, изменившим свой статус
            $states = $this->deliveryDpdStatusManager->getDataOfParcelsWithChangedStatus();
            // Симуляциии ответа DPD с исмененными статусами посылок
            #$states = $this->testResponse();

            if (null === $states) {
                // Exception, т.к. DPD должен вернуть ответ, даже если нет посылок с измененным состоянием:
                /*
                 * stdClass Object
                 *   (
                 *       'docId' => 13291702587
                 *       'docDate' => 2018-09-28T13:47:50
                 *       'clientNumber' => 1001049312
                 *       'resultComplete' => 1
                 *       'states' - Массив состояний посылок (измененных с момента предыдущего запроса)
                 *   )
                 */
                throw new LogicException(null,
                    LogicException::DELIVERY_DPD_REQUEST_FOR_CHANGED_STATES_RETURNED_NULL);
            }

            // Получаем отфильтрованный массив по заказам, перешедшим в финальные статусы ('delivered' и 'failed')
            $result = $this->deliveryDpdStatusManager->getDataWithFinalStatuses($states);
            if (null === $result) {
                $console->writeLine('Nothing to update.');
                return;
            }

            $console->writeLine('The response from the DPD was received. Processing in progress...');

            if (\count($result['delivered']) > 0) {
                // Запускаем процесс изменения статусов заказов
                $this->deliveryManager->manageSuccessfulDeliveries($result['delivered']);
            }

            if (\count($result['failed']) > 0) {
                // Запускаем процесс изменения статусов заказов
                $this->deliveryManager->manageUnsuccessfulDeliveries($result['failed'], $this->deliveryDpdStatusManager->getServiceTracePageUrl());
            }

        }
        catch (\Throwable $t) {

            logMessage($t->getMessage(), 'delivery-dpd');
            $console->writeLine('Error! ' . $t->getMessage());
        }

        $console->writeLine('Changed DPD parcels delivery statuses successfully received and processed.');
    }

    /**
     * Метод для симуляциии ответа DPD с исмененными статусами посылок
     * Нужен только для отладки
     *
     * @return \stdClass
     *
     */
    public function testResponse()
    {
        $response = new \stdClass();
        $response->docId = '13291702587';
        $response->docDate = '2018-10-23T13:47:50';
        $response->clientNumber = '1001049312';
        $response->resultComplete = 1;
        $response->states = [
            [
                'clientOrderNr' => 'GP-9acf0t5b90f7a',      // изменить для конкретного заказа
                'clientParcelNr' => 'GP-9acf0t5b90f7a-1',   // изменить для конкретного заказа
                'dpdOrderNr' => 'RU008505284',              // изменить для конкретного заказа
                'dpdParcelNr' => 'RU008505284-1',           // изменить для конкретного заказа

                'newState' => 'Delivered', // success - Delivered; fail - NewOrderByClient, NotDone, Lost, NewOrderByDPD
                'transitionTime' => '2018-10-23T14:10:15',

                // Нужен только для статуса Delivered
                'consignee' => 'Иванов И.И.',
            ]
        ];

        return $response;
    }
}