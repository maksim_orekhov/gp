<?php

namespace ModuleDeliveryDpd\Command;

use Application\Service\DeliveryManager;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDelivery\Entity\DeliveryTrackingNumber;
use ModuleDelivery\Service\DeliveryOrderManager;
use ModuleDeliveryDpd\Entity\DpdDeliveryOrder;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdManager;
use Zend\Console\Adapter\AdapterInterface;
use ZF\Console\Route;

/**
 * Class DeliveryDpdOrderCreationStatusesUpdateCommand
 * @package ModuleDeliveryDpd\Command
 */
class DeliveryDpdOrderCreationStatusesUpdateCommand
{
    /**
     * @var DeliveryOrderManager
     */
    private $deliveryOrderManager;

    /**
     * @var DeliveryOrderDpdManager
     */
    private $deliveryOrderDpdManager;

    /**
     * @var DeliveryManager
     */
    private $deliveryManager;

    /**
     * DeliveryDpdOrderCreationStatusesUpdateCommand constructor.
     * @param DeliveryOrderManager $deliveryOrderManager
     * @param DeliveryOrderDpdManager $deliveryOrderDpdManager
     * @param DeliveryManager $deliveryManager
     */
    public function __construct(DeliveryOrderManager $deliveryOrderManager,
                                DeliveryOrderDpdManager $deliveryOrderDpdManager,
                                DeliveryManager $deliveryManager)
    {
        $this->deliveryOrderManager = $deliveryOrderManager;
        $this->deliveryOrderDpdManager = $deliveryOrderDpdManager;
        $this->deliveryManager = $deliveryManager;
    }

    /**
     * @param Route $route
     * @param AdapterInterface $console
     *
     * Команда для запуска & bin/console delivery dpd order creation statuses update
     */
    public function __invoke(Route $route, AdapterInterface $console)
    {
        $console->writeLine('Delivery DPD pending order check and updating...');

        try {
            // Получаем DpdDeliveryOrders со статусом создания заказа (dpdOrderCreationStatus) = 'OrderPending'
            $pendingDpdDeliveryOrders = $this->deliveryOrderDpdManager->getPendingDpdDeliveryOrder();

            if (empty($pendingDpdDeliveryOrders)) {
                $console->writeLine('There are no pending orders.');
                return;
            }

            /** @var DpdDeliveryOrder $dpdDeliveryOrder */
            foreach ($pendingDpdDeliveryOrders as $dpdDeliveryOrder) {
                /** @var DeliveryOrder $deliveryOrder */
                $deliveryOrder = $dpdDeliveryOrder->getDeliveryOrder();
                // Получаем статус создания конкретного заказа
                $result = $this->deliveryOrderManager->updateServiceOrderCreationStatus($deliveryOrder);
                if ($result['status'] === 'OK') {
                    $console->writeLine('Order ' . $deliveryOrder->getOrderNumber() . ' changed status to ' . $result['status']);
                    $console->writeLine('Order number ' . $result['order_number']);
                    if (!isset($result['order_number'])) {
                        // Отправляем уведомление админу
                        $this->deliveryOrderManager->sendSpecialMail(
                            $deliveryOrder,
                            'У принятого заказа на доставку отсутствует номер в системе сервиса доставки (трек-номер)'
                        );
                    } else {
                        /** @var DeliveryTrackingNumber $deliveryTrackingNumber */
                        $deliveryTrackingNumber = $this->deliveryOrderManager->createDeliveryTrackingNumber($deliveryOrder);
                        // Отправляем уведомления
                        $this->deliveryManager->notifyDealAgentsAboutDeliveryServiceOrderCreation(
                            $deliveryTrackingNumber,
                            $this->deliveryOrderManager->getServiceTracePageUrl($deliveryOrder)
                        );
                    }
                }
            }
        }
        catch (\Throwable $t) {

            logMessage($t->getMessage(), 'delivery-dpd');
            $console->writeLine('Error! ' . $t->getMessage());
        }

        $console->writeLine('Checking pending order creation statuses finished.');
    }
}