<?php
namespace ModuleDeliveryDpd;

use Zend\Mvc\MvcEvent;

class Module
{
    public function getConfig()
    {
        return array_replace_recursive(
            require __DIR__ . '/../config/assets.config.php',
            require __DIR__ . '/../config/controllers.config.php',
            require __DIR__ . '/../config/doctrine.config.php',
            require __DIR__ . '/../config/module.config.php',
            require __DIR__ . '/../config/router.config.php',
            require __DIR__ . '/../config/service.config.php',
            require __DIR__ . '/../config/views.config.php'
        );
    }

    /**
     * @param MvcEvent $event
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function onBootstrap(MvcEvent $event)
    {
        // ... code
    }
}
