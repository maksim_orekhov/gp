<?php

namespace ModuleDeliveryDpd\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use ModuleDeliveryDpd\Entity\DpdParcelShopState;

/**
 * Class DpdParcelShopStateFixtureLoader
 * @package ModuleDeliveryDpd\Fixture
 */
class DpdParcelShopStateFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const DPD_PARCEL_SHOP_STATE = ['open', 'full'];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        foreach (self::DPD_PARCEL_SHOP_STATE as $shop_state) {
            $dpdParcelShopState = new DpdParcelShopState();
            $dpdParcelShopState->setCode($shop_state);
            $entityManager->persist($dpdParcelShopState);
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 170;
    }
}