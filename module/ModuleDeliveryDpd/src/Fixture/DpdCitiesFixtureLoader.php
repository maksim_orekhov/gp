<?php

namespace ModuleDeliveryDpd\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use ModuleDeliveryDpd\Entity\DpdCity;

/**
 * Class DpdParcelShopTypeFixtureLoader
 * @package ModuleDeliveryDpd\Fixture
 *
 * @TODO Временно, убрать после настройки заполнения из API
 */
class DpdCitiesFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const CITIES = [
        [
            'city_id' => 1,
            'country_code' => 'RU',
            'country_name' => 'Russia',
            'region_code' => 77,
            'region_name' => 'Московская область',
            'city_code' => 77,
            'city_name' => 'Москва',
            'abbreviation' => 'МСК',
            'index_min' => 135000,
            'index_max' => 135999,
        ],
        [
            'city_id' => 2,
            'country_code' => 'RU',
            'country_name' => 'Russia',
            'region_code' => 78,
            'region_name' => 'Ленинградская область',
            'city_code' => 78,
            'city_name' => 'Санкт-Петербург',
            'abbreviation' => 'СПБ',
            'index_min' => 145000,
            'index_max' => 145999,
        ],
        [
            'city_id' => 3,
            'country_code' => 'RU',
            'country_name' => 'Russia',
            'region_code' => 32,
            'region_name' => 'Брянская область',
            'city_code' => 79,
            'city_name' => 'Брянск',
            'abbreviation' => 'БРН',
            'index_min' => 155000,
            'index_max' => 155999,
        ],
        [
            'city_id' => 4,
            'country_code' => 'RU',
            'country_name' => 'Russia',
            'region_code' => 56,
            'region_name' => 'Оренбургская область',
            'city_code' => 80,
            'city_name' => 'Оренбург',
            'abbreviation' => 'ОРБ',
            'index_min' => 165000,
            'index_max' => 165999,
        ],
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        foreach (self::CITIES as $city) {
            $dpdCity = new DpdCity();
            $dpdCity->setCityId($city['city_id']);
            $dpdCity->setCountryCode($city['country_code']);
            $dpdCity->setCountryName($city['country_name']);
            $dpdCity->setRegionCode($city['region_code']);
            $dpdCity->setRegionName($city['region_name']);
            $dpdCity->setCityCode($city['city_code']);
            $dpdCity->setCityName($city['city_name']);
            $dpdCity->setAbbreviation($city['abbreviation']);
            $dpdCity->setIndexMin($city['index_min']);
            $dpdCity->setIndexMax($city['index_max']);
            $dpdCity->setPaymentOnDelivery(true);

            $entityManager->persist($dpdCity);
        }

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 180;
    }
}