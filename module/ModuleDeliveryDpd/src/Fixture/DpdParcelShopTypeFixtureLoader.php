<?php

namespace ModuleDeliveryDpd\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use ModuleDeliveryDpd\Entity\DpdParcelShopType;

/**
 * Class DpdParcelShopTypeFixtureLoader
 * @package ModuleDeliveryDpd\Fixture
 */
class DpdParcelShopTypeFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');

        $dpdParcelShopType = new DpdParcelShopType();
        $dpdParcelShopType->setCode('PARCELSHOP');
        $entityManager->persist($dpdParcelShopType);

        $dpdParcelShopType = new DpdParcelShopType();
        $dpdParcelShopType->setCode('TERMINAL');
        $entityManager->persist($dpdParcelShopType);

        $entityManager->flush();
    }

    public function getOrder()
    {
        return 170;
    }
}