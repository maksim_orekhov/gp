<?php
namespace ModuleDeliveryDpd\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class CityCostForm
 * @package ModuleDeliveryDpd\Form
 */
class CityCostForm extends Form
{
    const TYPE_FORM = 'city-cost-form';

    /**
     * CitySearchForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        // Set POST method for this form
        $this->setAttribute('method', 'get');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type' => Element\Text::class,
            'name' => 'sender_city_id',
            'attributes' => [
                'id' => 'sender_city',
                'placeholder'=>'Идентификатор города отправки',
            ],
            'options' => array(
                'label' => 'Идентификатор города отправки',
            )
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'receiver_city_id',
            'attributes' => [
                'id' => 'receiver_city',
                'placeholder'=>'Идентификатор города получения',
            ],
            'options' => array(
                'label' => 'Идентификатор города получения',
            )
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'weight',
            'attributes' => [
                'id' => 'weight',
                'placeholder'=>'Вес посылки',
            ],
            'options' => array(
                'label' => 'Вес посылки',
            )
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'self_pickup',
            'attributes' => [
                'id' => 'self_pickup',
                'placeholder'=>'Самопривоз на терминал'
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Отправить',
                'class'=>'ButtonApply'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'sender_city_id',
            'required' => true,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'receiver_city_id',
            'required' => true,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'weight',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'self_pickup',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\InArray::class,
                    'options' => [
                        'haystack' => [0,1],
                        'messages' => [
                            'notInArray' => 'Недопустимое значение!',
                        ],
                    ],
                ],
            ],
        ]);
    }
}