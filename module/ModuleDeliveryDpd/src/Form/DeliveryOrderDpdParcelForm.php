<?php
namespace ModuleDeliveryDpd\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class DeliveryOrderDpdParcelForm
 * @package ModuleDeliveryDpd\Form
 */
class DeliveryOrderDpdParcelForm extends Form
{
    const TYPE_FORM = 'delivery-order-dpd-parcel-form';

    /**
     * DeliveryOrderDpdParcelForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type' => Element\Number::class,
            'name' => 'weight',
            'attributes' => [
                'id' => 'weight',
                'placeholder'=>'Вес посылки, кг'
            ],
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'length',
            'attributes' => [
                'id' => 'length',
                'placeholder'=>'Длина посылки, см'
            ],
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'width',
            'attributes' => [
                'id' => 'width',
                'placeholder'=>'Ширина посылки, см'
            ],
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'height',
            'attributes' => [
                'id' => 'height',
                'placeholder'=>'Высота посылки, см'
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'class'=>'ButtonApply'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'weight',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'length',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'width',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'height',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);
    }
}