<?php
namespace ModuleDeliveryDpd\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class DeliveryOrderDpdAddressForm
 * @package ModuleDeliveryDpd\Form
 */
class DeliveryOrderDpdAddressForm extends Form
{
    const TYPE_FORM = 'delivery-order-dpd-address-form';

    /**
     * DeliveryOrderDpdAddressForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type' => Element\Text::class,
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'placeholder'=>'Название отправителя/получателя'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'terminal_code',
            'attributes' => [
                'id' => 'terminal_code',
                'placeholder'=>'Код терминала'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'country_name',
            'attributes' => [
                'id' => 'country_name',
                'placeholder'=>'Название страны'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'index',
            'attributes' => [
                'id' => 'index',
                'placeholder'=>'Индекс'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'region',
            'attributes' => [
                'id' => 'region',
                'placeholder'=>'Регион'
            ],
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'city_id',
            'attributes' => [
                'id' => 'city_id',
                'placeholder'=>'Идентификатор города отправления'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'city',
            'attributes' => [
                'id' => 'city',
                'placeholder'=>'Город'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'street',
            'attributes' => [
                'id' => 'street',
                'placeholder'=>'Улица'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'street_abr',
            'attributes' => [
                'id' => 'street_abr',
                'placeholder'=>'Сокращения типа улицы (ул, пр-т, б-р и т.д.)'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'house',
            'attributes' => [
                'id' => 'house',
                'placeholder'=>'Дом'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'house_korpus',
            'attributes' => [
                'id' => 'house_korpus',
                'placeholder'=>'Корпус'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'str',
            'attributes' => [
                'id' => 'str',
                'placeholder'=>'Строение'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'vlad',
            'attributes' => [
                'id' => 'vlad',
                'placeholder'=>'Владение'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'extra_info',
            'attributes' => [
                'id' => 'extra_info',
                'placeholder'=>'Доп. Информация'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'office',
            'attributes' => [
                'id' => 'office',
                'placeholder'=>'Офис'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'flat',
            'attributes' => [
                'id' => 'flat',
                'placeholder'=>'Квартира'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'work_time_from',
            'attributes' => [
                'id' => 'work_time_from',
                'placeholder'=>'Время работы от'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'work_time_to',
            'attributes' => [
                'id' => 'work_time_to',
                'placeholder'=>'Время работы до'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'dinner_time_from',
            'attributes' => [
                'id' => 'dinner_time_from',
                'placeholder'=>'Время обеда от'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'dinner_time_to',
            'attributes' => [
                'id' => 'dinner_time_to',
                'placeholder'=>'Время обеда до'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'contact_fio',
            'attributes' => [
                'id' => 'contact_fio',
                'placeholder'=>'Контактное лицо'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'contact_phone',
            'attributes' => [
                'id' => 'contact_phone',
                'placeholder'=>'Контактный телефон'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'contact_email',
            'attributes' => [
                'id' => 'contact_email',
                'placeholder'=>'Контактный e-mail'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'instructions',
            'attributes' => [
                'id' => 'instructions',
                'placeholder'=>'Инструкции для курьера'
            ],
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'need_pass',
            'attributes' => [
                'id' => 'need_pass',
                'placeholder'=>'Требуется пропуск'
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'class'=>'ButtonApply'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'name',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'terminal_code',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'country_name',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'index',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'region',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'city_id',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'city',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'street_abr',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'street',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'house',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'house_korpus',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'str',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'vlad',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'extra_info',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'office',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'flat',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'work_time_from',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'work_time_to',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'dinner_time_from',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'dinner_time_to',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'contact_fio',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'contact_phone',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'contact_email',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'instructions',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'need_pass',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Boolean::class],
            ],
        ]);
    }
}