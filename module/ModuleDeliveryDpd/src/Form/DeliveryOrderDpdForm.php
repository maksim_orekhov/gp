<?php
namespace ModuleDeliveryDpd\Form;

use ModuleDeliveryDpd\Service\DeliveryOrderDpdManager;
use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class DeliveryOrderDpdForm
 * @package ModuleDeliveryDpd\Form
 */
class DeliveryOrderDpdForm extends Form
{
    const TYPE_FORM = 'delivery-order-dpd-form';

    /**
     * DeliveryOrderDpdForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    /**
     * @return array
     */
    public function getOptionsForPickupTimePeriod(): array
    {
        $pickup_time_periods_array = [];
        foreach (DeliveryOrderDpdManager::RECEIPT_TIME_INTERVALS as $interval) {
            $pickup_time_periods_array[$interval['name']] = $interval['description'];
        }

        return $pickup_time_periods_array;
    }

    /**
     * @return array
     */
    public function getOptionsForDeliveryTimePeriod(): array
    {
        $delivery_time_periods_array = [];
        foreach (DeliveryOrderDpdManager::DELIVERY_TIME_INTERVALS as $interval) {
            $delivery_time_periods_array[$interval['name']] = $interval['description'];
        }

        return $delivery_time_periods_array;
    }

    /**
     * @return array
     */
    public function getOptionsForPaymentType(): array
    {
        $delivery_time_periods_array = [];
        foreach (DeliveryOrderDpdManager::PAYMENT_METHOD_OPTIONS as $key=>$value) {
            $delivery_time_periods_array[$key] = $value;
        }

        return $delivery_time_periods_array;
    }

    protected function addElements()
    {
        $this->add([
            'type' => Element\Date::class,
            'name' => 'date_pickup',
            'attributes' => [
                'id' => 'date_pickup',
                'placeholder'=>'Дата приёма груза'
            ],
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'pickup_time_period',
            'attributes' => [
                'id' => 'pickup_time_period',
                'placeholder'=>'Интервал времени приёма груза'
            ],
            'options' => array(
                'label' => 'Интервал времени приёма груза',
                'empty_option' => 'Выберите интервал времени приёма груза',
                'value_options' => $this->getOptionsForPickupTimePeriod()
            )
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'delivery_time_period',
            'attributes' => [
                'id' => 'delivery_time_period',
                'placeholder'=>'Интервал времени доставки груза'
            ],
            'options' => array(
                'label' => 'Интервал времени доставки груза',
                'empty_option' => 'Выберите интервал времени доставки груза',
                'value_options' => $this->getOptionsForDeliveryTimePeriod()
            )
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'service_code',
            'attributes' => [
                'id' => 'service_code',
                'placeholder'=>'Список кодов услуг DPD'
            ],
        ]);

//        $this->add([
//            'type' => Element\Checkbox::class,
//            'name' => 'is_cargo_valuable',
//            'attributes' => [
//                'id' => 'is_cargo_valuable',
//                'placeholder'=>'Ценный груз'
//            ],
//        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'declared_value',
            'attributes' => [
                'id' => 'declared_value',
                'placeholder'=>'Сумма объявленной ценности, руб'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'cargo_category',
            'attributes' => [
                'id' => 'cargo_category',
                'placeholder'=>'Содержимое отправки'
            ],
        ]);

        $this->add([
            'type' => Element\Select::class,
            'name' => 'payment_type',
            'attributes' => [
                'id' => 'payment_type',
                'placeholder'=>'Форма оплаты'
            ],
            'options' => array(
                'label' => 'Форма оплаты',
                'empty_option' => 'Выберите форму оплаты',
                'value_options' => $this->getOptionsForPaymentType()
            )
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'class'=>'ButtonApply'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'date_pickup',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\DateSelect::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Date::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'pickup_time_period',
            'required' => false,
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'delivery_time_period',
            'required' => false,
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'service_code',
            'required' => true,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\Regex::class,
                    'options' => [
                        'pattern' => '/[A-Z][A-Z],*/',
                        'message' => 'Неверный формат ввода']
                ],
            ],
        ]);

//        $inputFilter->add([
//            'name'     => 'is_cargo_valuable',
//            'required' => true,
//            'filters'  => [
//                ['name' => 'StringTrim'],
//            ],
//            'validators' => [
//                [
//                    'name' => \Zend\Validator\InArray::class,
//                    'options' => [
//                        'haystack' => [0,1],
//                        'messages' => [
//                            'notInArray' => 'Недопустимое значение!',
//                        ],
//                    ],
//                ],
//            ],
//        ]);

        $inputFilter->add([
            'name'     => 'declared_value',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'cargo_category',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
            ],
        ]);

        $inputFilter->add([
            'name'     => 'payment_type',
            'required' => false,
            'validators' => [
            ],
        ]);
    }
}