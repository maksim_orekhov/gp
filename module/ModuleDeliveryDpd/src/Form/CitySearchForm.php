<?php
namespace ModuleDeliveryDpd\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * Class CitySearchForm
 * @package ModuleDeliveryDpd\Form
 */
class CitySearchForm extends Form
{
    const TYPE_FORM = 'city-search-form';

    /**
     * CitySearchForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        // Set POST method for this form
        $this->setAttribute('method', 'get');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type' => 'text',
            'name' => 'city_name',
            'attributes' => [
                'id' => 'city_name',
                'placeholder'=>'Город',
            ],
            'options' => array(
                'label' => 'Город',
            )
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Найти город',
                'class'=>'ButtonApply'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'city_name',
            'required' => false,
            'filters'  => [
                ['name' =>  'StringTrim'],
            ],
            'validators' => [
                ['name' => 'StringLength', 'options' => ['min' => 1, 'max' => 25]],
            ],
        ]);
    }
}