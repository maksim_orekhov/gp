<?php
namespace ModuleDeliveryDpd\Form;

use Zend\Form\Form;

/**
 * Class UpdateDeliveryDpdCatalogForm
 * @package ModuleDeliveryDpd\Form
 */
class UpdateDeliveryDpdCatalogForm extends Form
{
    const TYPE_FORM = 'update-delivery-dpd-catalog-form';

    /**
     * @var string
     */
    private $type;

    /**
     * UpdateDeliveryDpdForm constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        $this->type = $type;

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', self::TYPE_FORM . '_' . $this->type);

        switch ($type) {
            case 'city':
                $action = '/admin/delivery/dpd/update/city';
                break;
            case 'point':
                $action = '/admin/delivery/dpd/update/point';
                break;
            case 'city_table_replace':
                $action = '/admin/delivery/dpd/update/production/city';
                break;
            case 'point_tables_replace':
                $action = '/admin/delivery/dpd/update/production/point';
                break;
        }
        $this->setAttribute('action', $action);

        $this->addElements();
    }

    protected function addElements()
    {
        $this->add([
            'type'  => 'hidden',
            'name' => 'form_type',
            'attributes' => [
                'value' => $this->type
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Обновить информацию о DPD',
                'class'=>'ButtonApply'
            ],
        ]);
    }
}