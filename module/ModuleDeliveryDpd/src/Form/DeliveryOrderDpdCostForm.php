<?php
namespace ModuleDeliveryDpd\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Form\Element;

/**
 * Class DeliveryOrderDpdCostForm
 * @package ModuleDeliveryDpd\Form
 */
class DeliveryOrderDpdCostForm extends Form
{
    const TYPE_FORM = 'delivery-order-dpd-cost-form';

    /**
     * DeliveryOrderDpdCostForm constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct(self::TYPE_FORM);

        // Set POST method for this form
        $this->setAttribute('method', 'post');
        $this->addElements();
        $this->addInputFilter();
    }

    protected function addElements()
    {
        $this->add([
            'type' => Element\Number::class,
            'name' => 'pickup_city_id',
            'attributes' => [
                'id' => 'pickup_city_id',
                'placeholder'=>'Идентификатор города отправления'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'pickup_index',
            'attributes' => [
                'id' => 'pickup_index',
                'placeholder'=>'Индекс места отправления'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'pickup_city_name',
            'attributes' => [
                'id' => 'pickup_city_name',
                'placeholder'=>'Город отправления'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'pickup_region_code',
            'attributes' => [
                'id' => 'pickup_region_code',
                'placeholder'=>'Код региона отправления'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'pickup_country_code',
            'attributes' => [
                'id' => 'pickup_country_code',
                'placeholder'=>'Код страны отправления'
            ],
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'delivery_city_id',
            'attributes' => [
                'id' => 'delivery_city_id',
                'placeholder'=>'Идентификатор города доставки'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'delivery_index',
            'attributes' => [
                'id' => 'delivery_index',
                'placeholder'=>'Индекс места доставки'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'delivery_city_name',
            'attributes' => [
                'id' => 'delivery_city_name',
                'placeholder'=>'Город доставки'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'delivery_region_code',
            'attributes' => [
                'id' => 'delivery_region_code',
                'placeholder'=>'Код региона доставки'
            ],
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'delivery_country_code',
            'attributes' => [
                'id' => 'delivery_country_code',
                'placeholder'=>'Код страны доставки'
            ],
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'self_pickup',
            'attributes' => [
                'id' => 'self_pickup',
                'placeholder'=>'Самопривоз на терминал'
            ],
        ]);

        $this->add([
            'type' => Element\Checkbox::class,
            'name' => 'self_delivery',
            'attributes' => [
                'id' => 'self_delivery',
                'placeholder'=>'Доставка до терминала. Самовывоз с терминала.'
            ],
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'weight',
            'attributes' => [
                'id' => 'weight',
                'placeholder'=>'Вес отправки, кг'
            ],
        ]);

        $this->add([
            'type' => Element\Number::class,
            'name' => 'volume',
            'attributes' => [
                'id' => 'volume',
                'placeholder'=>'Объём, м3'
            ],
        ]);

        // Если параметр задан, то сервис возвращает стоимость только заданных услуг. Если он не задан – всех доступных услуг.
        // BZP,ECN
        $this->add([
            'type' => Element\Text::class,
            'name' => 'service_code',
            'attributes' => [
                'id' => 'service_code',
                'placeholder'=>'Список кодов услуг DPD'
            ],
        ]);

        // Стоимость будет считаться на заданную дату. Если параметр не задан – будет считаться на текущую дату.
        // 2014-05-21
        $this->add([
            'type' => Element\Date::class,
            'name' => 'pickup_date',
            'attributes' => [
                'id' => 'pickup_date',
                'placeholder'=>'Предполагаемая дата приёма груза'
            ],
        ]);

        // Если параметр задан, то все услуги с большим сроком не будут показываться. Параметр не имеет смысла, если задавать конкретную услугу.
        // Целое число
        $this->add([
            'type' => Element\Number::class,
            'name' => 'max_days',
            'attributes' => [
                'id' => 'max_days',
                'placeholder'=>'Максимально допустимый срок'
            ],
        ]);

        // Если параметр задан, то все услуги с большей стоимостью не будут показываться. Параметр не имеет смысла, если задавать конкретную услугу.
        // (после запятой не более 2-х знаков)
        $this->add([
            'type' => Element\Number::class,
            'name' => 'max_cost',
            'attributes' => [
                'id' => 'max_cost',
                'placeholder'=>'Максимально допустимая стоимость'
            ],
        ]);

        // 1000 (после запятой не более 2-х знаков)
        $this->add([
            'type' => Element\Number::class,
            'name' => 'declared_value',
            'attributes' => [
                'id' => 'declared_value',
                'placeholder'=>'Объявленная ценность груза'
            ],
        ]);

        // 5 (После запятой может быть более 2-х знаков)
        $this->add([
            'type' => Element\Number::class,
            'name' => 'parcel_weight',
            'attributes' => [
                'id' => 'parcel_weight',
                'placeholder'=>'Вес посылки, кг'
            ],
        ]);

        // 20 (После запятой может быть более 2-х знаков)
        $this->add([
            'type' => Element\Number::class,
            'name' => 'parcel_length',
            'attributes' => [
                'id' => 'parcel_length',
                'placeholder'=>'Длина посылки, см'
            ],
        ]);

        // 20 (После запятой может быть более 2-х знаков)
        $this->add([
            'type' => Element\Number::class,
            'name' => 'parcel_width',
            'attributes' => [
                'id' => 'parcel_width',
                'placeholder'=>'Ширина посылки, см'
            ],
        ]);

        // 30 (После запятой может быть более 2-х знаков)
        $this->add([
            'type' => Element\Number::class,
            'name' => 'parcel_height',
            'attributes' => [
                'id' => 'parcel_height',
                'placeholder'=>'Высота посылки, см'
            ],
        ]);

        // 3
        $this->add([
            'type' => Element\Number::class,
            'name' => 'parcel_quantity',
            'attributes' => [
                'id' => 'parcel_quantity',
                'placeholder'=>'Количество посылок'
            ],
        ]);

        $this->add([
            'type' => 'csrf',
            'name' => 'csrf',
            'options' => [
                'csrf_options' => [
                    'timeout' => 3600
                ]
            ],
        ]);

        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Расчитать стоимость',
                'class'=>'ButtonApply'
            ],
        ]);
    }

    private function addInputFilter()
    {
        $inputFilter = new InputFilter();
        $this->setInputFilter($inputFilter);

        $inputFilter->add([
            'name'     => 'pickup_city_id',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'pickup_index',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 6,
                        'max' => 6
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'pickup_city_name',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 1,
                        'max' => 100
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'pickup_region_code',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 1,
                        'max' => 2
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'pickup_country_code',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 2
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'delivery_city_id',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'delivery_index',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 6,
                        'max' => 6
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'delivery_city_name',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 1,
                        'max' => 100
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'delivery_region_code',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 1,
                        'max' => 2
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'delivery_country_code',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class],
            ],
            'validators' => [
                [
                    'name'    => \Zend\Validator\StringLength::class,
                    'options' => [
                        'min' => 2,
                        'max' => 2
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'self_pickup',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\InArray::class,
                    'options' => [
                        'haystack' => [0,1],
                        'messages' => [
                            'notInArray' => 'Недопустимое значение!',
                        ],
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'self_delivery',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\InArray::class,
                    'options' => [
                        'haystack' => [0,1],
                        'messages' => [
                            'notInArray' => 'Недопустимое значение!',
                        ],
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'weight',
            'required' => true,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'volume',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'service_code',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\StringTrim::class],
                ['name' => \Zend\Filter\StripTags::class]
            ],
            'validators' => [
                [
                    'name' => \Zend\Validator\Regex::class,
                    'options' => [
                        'pattern' => '/[A-Z][A-Z],*/',
                        'message' => 'Неверный формат ввода']
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'pickup_date',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\DateSelect::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Date::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'max_days',
            'required' => false,
//            'filters'  => [
//                ['name' => \Zend\Filter\ToInt::class],
//            ],
            'validators' => [
                ['name' => \Zend\I18n\Validator\IsInt::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'max_cost',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'declared_value',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'parcel_weight',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'parcel_length',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'parcel_width',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'parcel_height',
            'required' => false,
            'filters'  => [
                ['name' => \Zend\Filter\Digits::class],
            ],
            'validators' => [
                ['name' => \Zend\Validator\Digits::class],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'parcel_quantity',
            'required' => false,
//            'filters'  => [
//                ['name' => \Zend\Filter\ToInt::class],
//            ],
            'validators' => [
                ['name' => \Zend\I18n\Validator\IsInt::class]
            ],
        ]);
    }
}