<?php

namespace ModuleDeliveryDpd\Controller;

use Application\Entity\User;
use Application\EventManager\ApplicationEventProvider;
use ModuleDeliveryDpd\Form\UpdateDeliveryDpdCatalogForm;
use ModuleDeliveryDpd\Service\DeliveryDpdCatalogManager;
use Application\Service\UserManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\Base\BaseAuthManager;
use Core\Service\TwigViewModel;
use ModuleRbac\Service\RbacManager;
use Zend\EventManager\EventManager;

/**
 * Class DeliveryDpdCatalogController
 * @package ModuleDeliveryDpd\Controller
 */
class DeliveryDpdCatalogController extends AbstractRestfulController
{
    /**
     * User manager
     * @var UserManager
     */
    private $userManager;

    /**
     * @var BaseAuthManager
     */
    private $baseAuthManager;

    /**
     * @var DeliveryDpdCatalogManager
     */
    private $deliveryDpdCatalogManager;

    /**
     * @var RbacManager
     */
    private $rbacManager;

    /**
     * @var EventManager
     */
    private $applicationEventManager;

    /**
     * DeliveryDpdCatalogController constructor.
     * @param UserManager $userManager
     * @param BaseAuthManager $baseAuthManager
     * @param DeliveryDpdCatalogManager $deliveryDpdCatalogManager
     * @param RbacManager $rbacManager
     * @param EventManager $dpdCatalogEventManager
     */
    public function __construct(UserManager $userManager,
                                BaseAuthManager $baseAuthManager,
                                DeliveryDpdCatalogManager $deliveryDpdCatalogManager,
                                RbacManager $rbacManager,
                                EventManager $applicationEventManager)
    {
        set_time_limit(900);

        $this->userManager = $userManager;
        $this->baseAuthManager = $baseAuthManager;
        $this->deliveryDpdCatalogManager = $deliveryDpdCatalogManager;
        $this->rbacManager = $rbacManager;
        $this->applicationEventManager = $applicationEventManager;
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function dpdUpdateAction()
    {
        try {
            // Если вдруг RBAC не сработал
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            if(!$this->rbacManager->hasRole($user, 'Administrator') ) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $updateDeliveryDpdFormCity = new UpdateDeliveryDpdCatalogForm('city');
            $updateDeliveryDpdFormPoint = new UpdateDeliveryDpdCatalogForm('point');
            $updateDeliveryDpdFormCityTableReplace = new UpdateDeliveryDpdCatalogForm('city_table_replace');
            $updateDeliveryDpdFormPointTablesReplace = new UpdateDeliveryDpdCatalogForm('point_tables_replace');

            $number_of_cities = $this->deliveryDpdCatalogManager->getNumbersOfDpdCities();
            $number_of_points = $this->deliveryDpdCatalogManager->getNumbersOfDpdPoints();

            $dpdUpdateInfo = $this->deliveryDpdCatalogManager->getOrCreateDpdUpdateInfo();
            $dpdUpdateInfoOutput = $this->deliveryDpdCatalogManager->getDpdUpdateInfoOutput($dpdUpdateInfo);

            $operations_availability = $this->deliveryDpdCatalogManager
                ->getOperationsAvailability($dpdUpdateInfo, $number_of_cities, $number_of_points);

            $view = new TwigViewModel([
                'updateDeliveryDpdFormCity' => $updateDeliveryDpdFormCity,
                'updateDeliveryDpdFormPoint' => $updateDeliveryDpdFormPoint,
                'updateDeliveryDpdFormCityTableReplace' => $updateDeliveryDpdFormCityTableReplace,
                'updateDeliveryDpdFormPointTablesReplace' => $updateDeliveryDpdFormPointTablesReplace,
                'number_of_cities' => $number_of_cities,
                'number_of_points' => $number_of_points,
                'update_info' => $dpdUpdateInfoOutput,
                'operations_availability' => $operations_availability,
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

            return $view;
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function dpdCityUpdateAction()
    {
        try {
            // Если вдруг RBAC не сработал
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            if(!$this->rbacManager->hasRole($user, 'Administrator') ) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $updateDeliveryDpdCityForm = new UpdateDeliveryDpdCatalogForm('city');

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            $update_result = null;
            if ($postData) {
                //set data to form
                $updateDeliveryDpdCityForm->setData($postData);
                //validate form
                if ($updateDeliveryDpdCityForm->isValid()) {
                    try {
                        // Отключаем слушатели debug панели
                        $this->applicationEventManager->trigger(ApplicationEventProvider::EVENT_DEVELOPER_TOOL_LISTENERS_DISABLE, $this);
                        $update_result = $this->deliveryDpdCatalogManager->updateDpdCityCandidateTable();
                    } catch (\Throwable $t) {
                        $update_result = [
                            'status' => 'ERROR',
                            'message' => $t->getMessage(),
                            'data' => null
                        ];
                    }
                    //success ajax
                    if ($this->getRequest()->isXmlHttpRequest()) {

                        return $this->message()->success($update_result['status'], $update_result['message']);
                    }

                    // Redirect на dpdUpdateAction
                    #return $this->redirect()->toRoute('delivery-dpd-update');
                }
                // If not valid
                if ($this->getRequest()->isXmlHttpRequest()) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($updateDeliveryDpdCityForm->getMessages());
                }
            }

            $number_of_cities = $this->deliveryDpdCatalogManager->getNumbersOfDpdCities();

            $dpdUpdateInfo = $this->deliveryDpdCatalogManager->getOrCreateDpdUpdateInfo();
            $dpdUpdateInfoOutput = $this->deliveryDpdCatalogManager->getDpdUpdateInfoOutput($dpdUpdateInfo);

            $view = new TwigViewModel([
                'updateDeliveryDpdCityForm' => $updateDeliveryDpdCityForm,
                'number_of_cities' => $number_of_cities,
                'update_info' => $dpdUpdateInfoOutput,
                'update_result' => $update_result,
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

            return $view;
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function dpdPointUpdateAction()
    {
        try {
            // Если вдруг RBAC не сработал
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            if(!$this->rbacManager->hasRole($user, 'Administrator') ) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $updateDeliveryDpdPointForm = new UpdateDeliveryDpdCatalogForm('point');

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            $update_result = null;
            if ($postData) {
                //set data to form
                $updateDeliveryDpdPointForm->setData($postData);
                //validate form
                if ($updateDeliveryDpdPointForm->isValid()) {
                    try {
                        // Отключаем слушатели debug панели
                        $this->applicationEventManager->trigger(ApplicationEventProvider::EVENT_DEVELOPER_TOOL_LISTENERS_DISABLE, $this);
                        $update_result = $this->deliveryDpdCatalogManager->updateDpdPointCandidateTable();
                    } catch (\Throwable $t) {
                        $update_result = [
                            'status' => 'ERROR',
                            'message' => $t->getMessage(),
                            'data' => null
                        ];
                    }
                    //success ajax
                    if ($this->getRequest()->isXmlHttpRequest()) {

                        return $this->message()->success($update_result['status'], $update_result['message']);
                    }

                    // Redirect на dpdUpdateAction
                    return $this->redirect()->toRoute('delivery-dpd-update');
                }
                // If not valid
                if ($this->getRequest()->isXmlHttpRequest()) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($updateDeliveryDpdPointForm->getMessages());
                }
            }

            $number_of_points = $this->deliveryDpdCatalogManager->getNumbersOfDpdPoints();

            $dpdUpdateInfo = $this->deliveryDpdCatalogManager->getOrCreateDpdUpdateInfo();
            $dpdUpdateInfoOutput = $this->deliveryDpdCatalogManager->getDpdUpdateInfoOutput($dpdUpdateInfo);

            $view = new TwigViewModel([
                'updateDeliveryDpdPointForm' => $updateDeliveryDpdPointForm,
                'number_of_points' => $number_of_points,
                'update_info' => $dpdUpdateInfoOutput,
                'update_result' => $update_result,
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

            return $view;
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function dpdProductionCityUpdateAction()
    {
        try {
            // Если вдруг RBAC не сработал
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            if(!$this->rbacManager->hasRole($user, 'Administrator') ) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $updateDeliveryDpdProductionForm = new UpdateDeliveryDpdCatalogForm('city_table_replace');

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            $update_result = null;
            if ($postData) {
                //set data to form
                $updateDeliveryDpdProductionForm->setData($postData);
                //validate form
                if ($updateDeliveryDpdProductionForm->isValid()) {
                    try {
                        $update_result = $this->deliveryDpdCatalogManager->replaceDpdCityTable();
                    } catch (\Throwable $t) {
                        $update_result = [
                            'status' => 'ERROR',
                            'message' => $t->getMessage(),
                            'data' => null
                        ];
                    }
                    //success ajax
                    if ($this->getRequest()->isXmlHttpRequest()) {

                        return $this->message()->success($update_result['status'], $update_result['message']);
                    }

                    // Redirect на dpdUpdateAction
                    return $this->redirect()->toRoute('delivery-dpd-update');
                }
                // If not valid
                if ($this->getRequest()->isXmlHttpRequest()) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($updateDeliveryDpdProductionForm->getMessages());
                }
            }

            $number_of_cities = $this->deliveryDpdCatalogManager->getNumbersOfDpdCities();

            $dpdUpdateInfo = $this->deliveryDpdCatalogManager->getOrCreateDpdUpdateInfo();
            $dpdUpdateInfoOutput = $this->deliveryDpdCatalogManager->getDpdUpdateInfoOutput($dpdUpdateInfo);

            $view = new TwigViewModel([
                'updateDeliveryDpdProductionForm' => $updateDeliveryDpdProductionForm,
                'number_of_cities' => $number_of_cities,
                'update_info' => $dpdUpdateInfoOutput,
                'update_result' => $update_result,
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

            return $view;
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }
    }

    /**
     * @return TwigViewModel|\Zend\Http\Response
     */
    public function dpdProductionPointUpdateAction()
    {
        try {
            // Если вдруг RBAC не сработал
            /** @var User $user */ // Can throw Exception
            $user = $this->userManager->getUserByLogin($this->baseAuthManager->getIdentity());

            if(!$this->rbacManager->hasRole($user, 'Administrator') ) {

                return $this->redirect()->toRoute('not-authorized');
            }

            $updateDeliveryDpdProductionForm = new UpdateDeliveryDpdCatalogForm('point');

            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            $update_result = null;
            if ($postData) {
                //set data to form
                $updateDeliveryDpdProductionForm->setData($postData);
                //validate form
                if ($updateDeliveryDpdProductionForm->isValid()) {
                    try {
                        $update_result = $this->deliveryDpdCatalogManager->replaceDpdPointTables();
                    } catch (\Throwable $t) {
                        $update_result = [
                            'status' => 'ERROR',
                            'message' => $t->getMessage(),
                            'data' => null
                        ];
                    }
                    //success ajax
                    if ($this->getRequest()->isXmlHttpRequest()) {

                        return $this->message()->success($update_result['status'], $update_result['message']);
                    }

                    // Redirect на dpdUpdateAction
                    return $this->redirect()->toRoute('delivery-dpd-update');
                }
                // If not valid
                if ($this->getRequest()->isXmlHttpRequest()) {
                    // Return Json with ERROR status
                    return $this->message()->invalidFormData($updateDeliveryDpdProductionForm->getMessages());
                }
            }

            $number_of_points = $this->deliveryDpdCatalogManager->getNumbersOfDpdPoints();

            $dpdUpdateInfo = $this->deliveryDpdCatalogManager->getOrCreateDpdUpdateInfo();
            $dpdUpdateInfoOutput = $this->deliveryDpdCatalogManager->getDpdUpdateInfoOutput($dpdUpdateInfo);

            $view = new TwigViewModel([
                'updateDeliveryDpdProductionForm' => $updateDeliveryDpdProductionForm,
                'number_of_points' => $number_of_points,
                'update_info' => $dpdUpdateInfoOutput,
                'update_result' => $update_result,
            ]);
            $this->layout()->setTemplate(TwigViewModel::DEFAULT_LAYOUT);

            return $view;
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }
    }
}