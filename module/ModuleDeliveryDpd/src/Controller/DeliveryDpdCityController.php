<?php

namespace ModuleDeliveryDpd\Controller;

use Core\Form\CsrfForm;
use ModuleDeliveryDpd\Entity\DpdCity;
use ModuleDeliveryDpd\Form\CityCostForm;
use ModuleDeliveryDpd\Form\CitySearchForm;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use Doctrine\ORM\EntityManager;
use Zend\Form\Element;

/**
 * Class DeliveryDpdCityController
 * @package ModuleDeliveryDpd\Controller
 */
class DeliveryDpdCityController extends AbstractRestfulController
{
    const CITY_NAME_BY_DEFAULT = 'Москва';

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var DeliveryDpdManager
     */
    private $deliveryDpdManager;

    /**
     * DeliveryDpdCityController constructor.
     * @param EntityManager $entityManager
     * @param DeliveryDpdManager $deliveryDpdManager
     */
    public function __construct(EntityManager $entityManager,
                                DeliveryDpdManager $deliveryDpdManager)
    {
        $this->entityManager = $entityManager;
        $this->deliveryDpdManager = $deliveryDpdManager;
    }

    /**
     * @return array|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function getList()
    {
        $this->setOnlyAjax();
        // return only json
        $this->message()->setJsonStrategy(true);

        $incomingData = $this->collectIncomingData();
        $queryData = $incomingData['queryData'];

        $queryData = [
            'city_name' => isset($queryData['search']) ? trim($queryData['search']) : null,
            'csrf' => isset($queryData['csrf']) ? trim($queryData['csrf']) : null
        ];

        $citySearchForm = new CitySearchForm();

        $citySearchForm->setData($queryData);

        if (!$citySearchForm->isValid()) {

            return $this->message()->invalidFormData($citySearchForm->getMessages());
        }

        $citiesOutput = null;

        // Если пришел поисковый запрос, валидируем и ищем по нему
        if (null !== $queryData['city_name']) {
            $cities = $this->deliveryDpdManager->getCitiesBySearchRequest($queryData['city_name']);
        } else {
            // Выбираем самые крупные города, оринетируясь по количеству пунктов в них
            $cities = $this->deliveryDpdManager->getBiggestCities();
        }

        if (\count($cities) > 0) {
            $citiesOutput = $this->deliveryDpdManager->getCityCollectionForOutput($cities);
        }

        return $this->message()->success('Cities', $citiesOutput);
    }

    /**
     * @param mixed $id
     * @return array|TwigViewModel|mixed|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function get($id)
    {
        $this->setOnlyAjax();
        // return only json
        $this->message()->setJsonStrategy(true);

        $cityOutput = null;
        $dpdPointCollectionOutput = null;

        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByCityId($id);

        if (null !== $city) {
            $points = $this->deliveryDpdManager->getCityDpdPointsWithSpecificType($city, 'SelfPickup');
            // DpdCity Output
            $cityOutput = $this->deliveryDpdManager->getCityForOutput($city);
            // DpdPoints Output
            $dpdPointCollectionOutput = $this->deliveryDpdManager->getPointCollectionOutput($points);
        }

        return $this->message()->success(
            'DpdCity single',
            [
                'city' => $cityOutput,
                'dpd_points' => \count($dpdPointCollectionOutput) > 0 ? $dpdPointCollectionOutput : null,
            ]
        );
    }

    /**
     * @return TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function autoCompleteAction()
    {
        \Core\Service\Timer::start();
        // return only json
        $this->message()->setJsonStrategy(true);
        $queryData = $this->collectIncomingData()['queryData'];

        $queryData = [
            'search' => isset($queryData['search']) ? trim($queryData['search']) : null,
            'csrf' => isset($queryData['csrf']) ? trim($queryData['csrf']) : null
        ];

        $csrf = new Element\Csrf('csrf');
        $validator = $csrf->getCsrfValidator();
        if ( !$validator->isValid($queryData['csrf'])) {

            return $this->message()->invalidFormData($validator->getMessages());
        }

        // Выбираем самые крупные города, оринетируясь по количеству пунктов в них
        return $this->message()->success('Cities | time: '.\Core\Service\Timer::finish(false).'s', $this->deliveryDpdManager->getAutoCompleteCities($queryData['search']));
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function getDeliveryToCityCostAction()
    {
        //return only json
        $this->message()->setJsonStrategy(true);

        try {
            // Проверяем, является ли пост POST-запросом.
            $postData = $this->collectIncomingData()['postData'];
            $queryData = $this->collectIncomingData()['queryData'];
            $data = array_merge_recursive($postData ?? [], $queryData ?? []);

            /** @var CityCostForm $cityCostForm */
            $cityCostForm = new CityCostForm();
            $cityCostForm->setData($data);
            if (!$cityCostForm->isValid()) {

                return $this->message()->invalidFormData($cityCostForm->getMessages());
            }
            $data = $cityCostForm->getData();

            if ($result = $this->deliveryDpdManager->getServiceCostByCityData($data)) {

                return $this->message()->success('DeliveryDpdCost', $result);
            }

            return $this->message()->error('No result returned');
        }
        catch (\Throwable $t) {

            return $this->message()->exception($t);
        }
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Exception
     */
    public function formInitAction()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        $incomingData = $this->collectIncomingData();
        $queryData = $incomingData['queryData'];

        $queryData = [
            'csrf' => isset($queryData['csrf']) ? trim($queryData['csrf']) : null
        ];

        $csrfForm = new CsrfForm();
        $csrfForm->setData($queryData);
        if (!$csrfForm->isValid()) {

            return $this->message()->invalidFormData($csrfForm->getMessages());
        }

        $formDataInit = $this->processFormDataInit();

        return $this->message()->success('formInit', $formDataInit);
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function processFormDataInit()
    {
        $cityOutput = null;
        $dpdPointCollectionOutput = null;

        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityMoscow();

        if (null !== $city) {
            $points = $this->deliveryDpdManager->getCityDpdPointsWithSpecificType($city, 'SelfPickup');
            // DpdCity Output
            $cityOutput = $this->deliveryDpdManager->getCityForOutput($city);
            // DpdPoints Output
            $dpdPointCollectionOutput = $this->deliveryDpdManager->getPointCollectionOutput($points);
        }

        return [
            'city' => $cityOutput,
            'dpd_points' => $dpdPointCollectionOutput,
            'regions_ru' => DeliveryDpdManager::getRussiaRegions(),
        ];
    }

    /**
     * @return array
     */
    private function setOnlyAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}