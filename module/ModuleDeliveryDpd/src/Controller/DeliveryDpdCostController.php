<?php

namespace ModuleDeliveryDpd\Controller;

use ModuleDeliveryDpd\Entity\DpdDeliveryOrder;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdManager;

/**
 * Class DeliveryDpdCostController
 * @package ModuleDeliveryDpd\Controller
 */
class DeliveryDpdCostController extends AbstractRestfulController
{
    /**
     * @var DeliveryDpdManager
     */
    private $deliveryDpdManager;

    /**
     * @var DeliveryOrderDpdManager
     */
    private $deliveryOrderDpdManager;

    /**
     * DeliveryDpdCostController constructor.
     * @param DeliveryDpdManager $deliveryDpdManager
     * @param DeliveryOrderDpdManager $deliveryOrderDpdManager
     */
    public function __construct(DeliveryDpdManager $deliveryDpdManager,
                                DeliveryOrderDpdManager $deliveryOrderDpdManager)
    {
        $this->deliveryDpdManager = $deliveryDpdManager;
        $this->deliveryOrderDpdManager = $deliveryOrderDpdManager;
    }

    /**
     * @return array|TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getCostAction()
    {
        //only ajax
        if (! $this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Method not supported'
            ];
        }

        // Проверяем, является ли пост POST-запросом.
        $postData = $this->collectIncomingData()['postData'];

        if (!$postData || !isset($postData['delivery_order_dpd_id'])) {

            return $this->message()->error('No data provided');
        }

        /** @var DpdDeliveryOrder $deliveryOrderDpd */
        $deliveryOrderDpd = $this->deliveryOrderDpdManager->getDeliveryOrderDpd($postData['delivery_order_dpd_id']);

        if (null === $deliveryOrderDpd) {

            return $this->message()->error('No deliveryOrderDpd found');
        }

        try {
            $result = $this->deliveryDpdManager->getServiceCost($deliveryOrderDpd, $postData);
        }
        catch (\Throwable $t) {

            return $this->message()->error($t->getMessage());
        }

        if (null !== $result) {

            return $this->message()->success('DeliveryDpdCost', $result);
        }

        return $this->message()->error('No result returned', $result);
    }
}