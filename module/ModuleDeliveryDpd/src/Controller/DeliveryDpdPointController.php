<?php

namespace ModuleDeliveryDpd\Controller;

use ModuleDeliveryDpd\Entity\DpdPoint;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use Core\Controller\AbstractRestfulController;
use Core\Service\TwigViewModel;

/**
 * Class DeliveryDpdPointController
 * @package ModuleDeliveryDpd\Controller
 */
class DeliveryDpdPointController extends AbstractRestfulController
{
    /**
     * @var DeliveryDpdManager
     */
    private $deliveryDpdManager;

    /**
     * DeliveryDpdPointController constructor.
     * @param DeliveryDpdManager $deliveryDpdManager
     */
    public function __construct(DeliveryDpdManager $deliveryDpdManager)
    {
        $this->deliveryDpdManager = $deliveryDpdManager;
    }

    /**
     * @return TwigViewModel|\Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getPointAction()
    {
        $this->setOnlyAjax();
        // return only json
        $this->message()->setJsonStrategy(true);
        $incomingData = $this->collectIncomingData();
        $code = isset($incomingData['pointCode']) ? trim($incomingData['pointCode']) : null;

        $pointOutput = null;

        /** @var DpdPoint $point */
        $point = $this->deliveryDpdManager->getPointByCode($code);

        if (null !== $point) {
            // DpdPoint Output
            $pointOutput = $this->deliveryDpdManager->getPointForOutput($point);
        }

        return $this->message()->success(
            'DpdPoint single',
            [
                'dpd_point' => \count($pointOutput) > 0 ? $pointOutput : null,
            ]
        );
    }

    /**
     * @return array
     */
    private function setOnlyAjax()
    {
        // only ajax
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->response->setStatusCode(404);
            return [
                'content' => 'Page not found'
            ];
        }
    }
}