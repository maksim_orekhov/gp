<?php
namespace ModuleDeliveryDpd\Controller\Factory;

use ModuleDeliveryDpd\Controller\DeliveryDpdPointController;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryDpdCityControllerFactory
 * @package ModuleDeliveryDpd\Controller\Factory
 */
class DeliveryDpdPointControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryDpdPointController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $deliveryDpdManager = $container->get(DeliveryDpdManager::class);

        return new DeliveryDpdPointController(
            $deliveryDpdManager
        );
    }
}