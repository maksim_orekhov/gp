<?php
namespace ModuleDeliveryDpd\Controller\Factory;

use Application\EventManager\ApplicationEventProvider;
use ModuleDeliveryDpd\Controller\DeliveryDpdCatalogController;
use ModuleDeliveryDpd\Service\DeliveryDpdCatalogManager;
use Core\Service\Base\BaseAuthManager;
use Application\Service\UserManager;
use Interop\Container\ContainerInterface;
use ModuleRbac\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryDpdCatalogControllerFactory
 * @package ModuleDeliveryDpd\Controller\Factory
 */
class DeliveryDpdCatalogControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryDpdCatalogController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $userManager = $container->get(UserManager::class);
        $baseAuthManager = $container->get(BaseAuthManager::class);
        $deliveryDpdCatalogManager = $container->get(DeliveryDpdCatalogManager::class);
        $rbacManager = $container->get(RbacManager::class);
        $applicationEventProvider = $container->get(ApplicationEventProvider::class);

        return new DeliveryDpdCatalogController(
            $userManager,
            $baseAuthManager,
            $deliveryDpdCatalogManager,
            $rbacManager,
            $applicationEventProvider->getEventManager()
        );
    }
}