<?php
namespace ModuleDeliveryDpd\Controller\Factory;

use ModuleDeliveryDpd\Controller\DeliveryDpdCityController;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryDpdCityControllerFactory
 * @package ModuleDeliveryDpd\Controller\Factory
 */
class DeliveryDpdCityControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryDpdCityController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager  = $container->get('doctrine.entitymanager.orm_default');
        $deliveryDpdManager = $container->get(DeliveryDpdManager::class);

        return new DeliveryDpdCityController(
            $entityManager,
            $deliveryDpdManager
        );
    }
}