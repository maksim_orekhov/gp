<?php
namespace ModuleDeliveryDpd\Controller\Factory;

use ModuleDeliveryDpd\Controller\DeliveryDpdCostController;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use Interop\Container\ContainerInterface;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryDpdCostControllerFactory
 * @package ModuleDeliveryDpd\Controller\Factory
 */
class DeliveryDpdCostControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryDpdCostController|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $deliveryDpdManager = $container->get(DeliveryDpdManager::class);
        $deliveryOrderDpdManager = $container->get(DeliveryOrderDpdManager::class);

        return new DeliveryDpdCostController(
            $deliveryDpdManager,
            $deliveryOrderDpdManager
        );
    }
}