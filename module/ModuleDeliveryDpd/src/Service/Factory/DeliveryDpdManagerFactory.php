<?php
namespace ModuleDeliveryDpd\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;

/**
 * Class DeliveryDpdManagerFactory
 * @package ModuleDeliveryDpd\Service\Factory
 */
class DeliveryDpdManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryDpdManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager  = $container->get('doctrine.entitymanager.orm_default');
        $config         = $container->get('Config');

        return new DeliveryDpdManager(
            $entityManager,
            $config
        );
    }
}