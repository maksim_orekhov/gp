<?php
namespace ModuleDeliveryDpd\Service\Factory;

use Interop\Container\ContainerInterface;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryOrderDpdManagerFactory
 * @package ModuleDeliveryDpd\Service\Factory
 */
class DeliveryOrderDpdManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryOrderDpdManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager  = $container->get('doctrine.entitymanager.orm_default');
        $deliveryDpdManager = $container->get(DeliveryDpdManager::class);

        return new DeliveryOrderDpdManager(
            $entityManager,
            $deliveryDpdManager
        );
    }
}