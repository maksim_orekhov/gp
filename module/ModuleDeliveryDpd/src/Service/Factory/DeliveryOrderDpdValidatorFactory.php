<?php
namespace ModuleDeliveryDpd\Service\Factory;

use Interop\Container\ContainerInterface;
use ModuleDeliveryDpd\Service\DeliveryDpdManager;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdValidator;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryOrderDpdValidatorFactory
 * @package ModuleDeliveryDpd\Service\Factory
 */
class DeliveryOrderDpdValidatorFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryOrderDpdValidator|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $deliveryDpdManager = $container->get(DeliveryDpdManager::class);

        return new DeliveryOrderDpdValidator(
            $deliveryDpdManager
        );
    }
}