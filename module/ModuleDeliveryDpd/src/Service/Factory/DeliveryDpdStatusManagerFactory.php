<?php
namespace ModuleDeliveryDpd\Service\Factory;

use Interop\Container\ContainerInterface;
use ModuleDeliveryDpd\Service\DeliveryDpdStatusManager;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class DeliveryDpdStatusManagerFactory
 * @package ModuleDeliveryDpd\Service\Factory
 */
class DeliveryDpdStatusManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return DeliveryDpdStatusManager|object
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');

        return new DeliveryDpdStatusManager(
            $config
        );
    }
}