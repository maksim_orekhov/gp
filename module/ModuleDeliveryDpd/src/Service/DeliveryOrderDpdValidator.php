<?php
namespace ModuleDeliveryDpd\Service;

use ModuleDelivery\Service\DeliveryOrderValidatorInterface;
use ModuleDeliveryDpd\Entity\DpdCity;
use ModuleDeliveryDpd\Entity\DpdParcelShopLimits;
use ModuleDeliveryDpd\Entity\DpdPoint;
use ModuleDeliveryDpd\Form\DeliveryOrderDpdAddressForm;
use ModuleDeliveryDpd\Form\DeliveryOrderDpdForm;
use ModuleDeliveryDpd\Form\DeliveryOrderDpdParcelForm;

/**
 * Class DeliveryOrderDpdValidator
 * @package ModuleDeliveryDpd\Service
 */
class DeliveryOrderDpdValidator implements DeliveryOrderValidatorInterface
{
    /**
     * @var DeliveryDpdManager
     */
    private $deliveryDpdManager;

    /**
     * DeliveryOrderDpdValidator constructor.
     * @param DeliveryDpdManager $deliveryDpdManager
     */
    public function __construct(DeliveryDpdManager $deliveryDpdManager)
    {
        $this->deliveryDpdManager = $deliveryDpdManager;
    }

    /**
     * @inheritdoc
     */
    public function validate(array $data)
    {
        // Валидируем данные для DeliveryOrderDpd
        $deliveryOrderDpdForm = new DeliveryOrderDpdForm();
        $deliveryOrderDpdForm->setData($data);
        if (!$deliveryOrderDpdForm->isValid()) {
            // Если данные не проходят валицию формой, сразу возвращаем сообщения об ошибке
            return $deliveryOrderDpdForm->getMessages();
        }

        $address_validation_messages = null;

        // Валидируем данные для DpdDeliveryAddress
        if (isset($data['sender_address'])) {
            $address_data = $data['sender_address'];
            $address_data['csrf'] = $data['csrf'];
            // Валидируем данные адреса
            $address_validation_messages = $this->validateAddress($address_data, 'sender');
        }
        if (isset($data['receiver_address'])) {
            $address_data = $data['receiver_address'];
            $address_data['csrf'] = $data['csrf'];
            // Валидируем данные адреса
            $address_validation_messages = $this->validateAddress($address_data, 'receiver');
        }

        if (null !== $address_validation_messages) {

            return $address_validation_messages;
        }

        // Валидируем данные посылок, если есть
        if (isset($data['parcels']) && \count($data['parcels']) > 0) {

            $parcels_validation_messages = $this->validateParcels($data['parcels'], $data['csrf']);
            if (null !== $parcels_validation_messages) {

                return $parcels_validation_messages;
            }

            $point_validation_messages = $this->validatePointRelatedData($data);
            if (null !== $point_validation_messages) {

                return $point_validation_messages;
            }

        }

        return null;
    }

    /**
     * @param array $data
     * @return array|null
     */
    private function validatePointRelatedData(array $data)
    {
        if (isset($data['sender_address']['terminal_code'])) {
            // Получаем пункт по указанному пользователем коду. Если нет такого, то - возвращаем messages
            /** @var DpdPoint $dpdPoint */
            $dpdPoint = $this->deliveryDpdManager->getPointByCode($data['sender_address']['terminal_code']);

            if (null === $dpdPoint) {

                return ['dpd_point_data' => ['invalidDataSet' => 'DPD point with specified code not found']];
            }

            // Проверяем не превышает ли указанный вес посылки(ок) лимит для выбранного пункта
            $total_weight = 0;
            foreach ($data['parcels'] as $parcel) {
                if (isset($parcel['weight'])) {
                    $total_weight += $parcel['weight'];
                }
            }
            /** @var DpdParcelShopLimits $limits */
            $limits = $dpdPoint->getDpdParcelShopLimits();
            if (null !== $limits && $total_weight > $limits->getWeight()) {

                return [
                    'dpd_parcels_data' => [
                        'invalidDataSet' => 'Shipping weight exceeds the limit for chosen DPD point',
                        'max_weight' => $limits->getWeight()
                    ]
                ];
            }
        }

        return null;
    }

    /**
     * @param array $parcels_data
     * @param string $csrf
     * @return array|null|\Traversable
     */
    private function validateParcels(array $parcels_data, string $csrf)
    {
         foreach ($parcels_data as $data) {
            $data['csrf'] = $csrf;
            // Валидируем данные формой
            $deliveryOrderDpdParcelForm = new DeliveryOrderDpdParcelForm();
            $deliveryOrderDpdParcelForm->setData($data);
            if (!$deliveryOrderDpdParcelForm->isValid()) {
                // Если данные не проходят валицию формой, сразу возвращаем сообщения об ошибке
                return $deliveryOrderDpdParcelForm->getMessages();
            }
        }

        return null;
    }

    /**
     * @param array $data
     * @param string|null $type
     * @return array|null|\Traversable
     */
    private function validateAddress(array $data, string $type = null)
    {
        $data['country_name']   = null;
        $data['region']         = null;

        if (isset($data['city_id'])) {
            /** @var DpdCity $dpdCity */
            $dpdCity = $this->deliveryDpdManager->getCityByCityId((int)$data['city_id']);

            $data['country_name']   = $dpdCity->getCountryName();
            $data['city']           = $dpdCity->getCityName();
            $data['region']         = $dpdCity->getRegionName();
        }

        // Валидируем данные формой
        $deliveryOrderDpdAddressForm = new DeliveryOrderDpdAddressForm();
        $deliveryOrderDpdAddressForm->setData($data);
        if (!$deliveryOrderDpdAddressForm->isValid()) {
            // Если данные не проходят валицию формой, сразу возвращаем сообщения об ошибке
            return $deliveryOrderDpdAddressForm->getMessages();
        }

        // В объекте Address обязательно должнен быть указан либо terminalCode либо расписан адрес для доставки Д("до двери")
        // Данное поле является обязательным для вариантов перевозки «ДТ», «ТД» или «ТТ».
        // Если указывается данное поле, то адрес указывать не требуется.
        $private_address_validation_messages = $this->validatePrivateAddressData($data);
        if (null !== $private_address_validation_messages && !isset($data['terminal_code'])) {
            if (null === $private_address_validation_messages) {

                return [$type.'_address_data' => ['invalidDataSet' => 'Dpd terminal code or private address data must be provided']];
            }

            return $private_address_validation_messages;
        }

        // Проверяем есть ли терминал с указанным кодом в БД
        if (isset($data['terminal_code'])) {

            $dpdPoint = $this->deliveryDpdManager->getPointByCode($data['terminal_code']);

            if (null === $dpdPoint) {

                return [$type.'_terminal_code' => ['notInDB' => 'The input was not found in the DB']];
            }
        }

        return null;
    }

    /**
     * @param array $data
     * @return array|null
     */
    private function validatePrivateAddressData(array $data)
    {
        $validation_massages = [];

        if (!isset($data['country_name'])) {
            $validation_massages[] = ['address_country_name' => ['isEmpty' => 'Country name is required']];
        }

        if (!isset($data['city'])) {
            $validation_massages[] = ['address_city' => ['isEmpty' => 'City is required']];
        }

        if (!isset($data['street'])) {
            $validation_massages[] = ['address_street' => ['isEmpty' => 'Street is required']];
        }

        if (empty($validation_massages)) {

            return null;
        }

        return $validation_massages;
    }
}