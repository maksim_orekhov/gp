<?php
namespace ModuleDeliveryDpd\Service;

use ModuleDeliveryDpd\Entity\DpdCity;
use ModuleDeliveryDpd\Entity\DpdCityCandidate;
use ModuleDeliveryDpd\Entity\DpdCityPrevious;
use ModuleDeliveryDpd\Entity\DpdParcelShopLimitsCandidate;
use ModuleDeliveryDpd\Entity\DpdParcelShopState;
use ModuleDeliveryDpd\Entity\DpdParcelShopType;
use ModuleDeliveryDpd\Entity\DpdPoint;
use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;
use ModuleDeliveryDpd\Entity\DpdPointCandidate;
use ModuleDeliveryDpd\Entity\DpdPointPrevious;
use ModuleDeliveryDpd\Entity\DpdScheduleCandidate;
use ModuleDeliveryDpd\Entity\DpdTimetableCandidate;
use ModuleDeliveryDpd\Entity\DpdUpdateInfo;
use SoapClient;

/**
 * Class DeliveryDpdCatalogManager
 * @package ModuleDeliveryDpd\Service
 */
class DeliveryDpdCatalogManager
{
    use \Core\Provider\HttpRequestTrait;

    const REGION_NAMES_RU = [
        '1' => 'Республика Адыгея',
        '2' => 'Республика Башкортостан',
        '3' => 'Республика Бурятия',
        '4' => 'Республика Алтай',
        '5' => 'Республика Дагестан',
        '6' => 'Республика Ингушетия',
        '7' => 'Республика Кабардино-Балкария',
        '8' => 'Республика Калмыкия',
        '9' => 'Республика Карачаево-Черкесская',
        '10' => 'Республика Карелия',
        '11' => 'Республика Коми',
        '12' => 'Республика Марий Эл',
        '13' => 'Республика Мордовия',
        '14' =>	'Республика Саха (Якутия)',
        '15' => 'Республика Северная Осетия — Алания',
        '16' => 'Республика Татарстан',
        '17' => 'Республика Тыва',
        '18' => 'Республика Удмуртия',
        '19' => 'Республика Хакасия',
        '20' => 'Республика Чеченская',
        '21' => 'Республика Чувашская-Чувашия',
        '22' => 'край Алтайский',
        '23' => 'край Краснодарский',
        '24' => 'край Красноярский',
        '25' => 'край Приморский',
        '26' => 'край Ставропольский',
        '27' => 'край Хабаровский',
        '28' => 'обл. Амурская',
        '29' => 'обл. Архангельская',
        '30' => 'обл. Астраханская',
        '31' => 'обл. Белгородская',
        '32' => 'обл. Брянская',
        '33' => 'обл. Владимирская',
        '34' => 'обл. Волгоградская',
        '35' => 'обл. Вологодская',
        '36' => 'обл. Воронежская',
        '37' => 'обл. Ивановская',
        '38' => 'обл. Иркутская',
        '39' => 'обл. Калининградская',
        '40' => 'обл. Калужская',
        '41' => 'край Камчатский ',
        '42' => 'обл. Кемеровская',
        '43' => 'обл. Кировская',
        '44' => 'обл. Костромская',
        '45' => 'обл. Курганская',
        '46' => 'обл. Курская',
        '47' => 'обл. Ленинградская',
        '48' =>	'обл. Липецкая',
        '49' => 'обл. Магаданская',
        '50' => 'обл. Московская',
        '51' => 'обл. Мурманская',
        '52' => 'обл. Нижегородская',
        '53' => 'обл. Новгородская',
        '54' => 'обл. Новосибирская',
        '55' => 'обл. Омская',
        '56' => 'обл. Оренбургская',
        '57' => 'обл. Орловская',
        '58' => 'обл. Пензенская',
        '59' => 'край Пермский',
        '60' =>	'обл. Псковская',
        '61' => 'обл. Ростовская',
        '62' => 'обл. Рязанская',
        '63' => 'обл. Самарская',
        '64' => 'обл. Саратовская',
        '65' => 'обл. Сахалинская',
        '66' => 'обл. Свердловская',
        '67' => 'обл. Смоленская',
        '68' => 'обл. Тамбовская',
        '69' => 'обл. Тверская',
        '70' => 'обл. Томская',
        '71' => 'обл. Тульская',
        '72' => 'обл. Тюменская',
        '73' => 'обл. Ульяновская',
        '74' => 'обл. Челябинская',
        '75' => 'край Забайкальский',
        '76' => 'обл. Ярославская',
        '77' => 'г. Москва',
        '78' => 'г. Санкт-Петербург',
        '79' => 'Аобл Еврейская',
        '83' => 'АО Ненецкий',
        '86' => 'АО Ханты-Мансийский Автономный округ - Югра',
        '87' => 'АО Чукотский',
        '89' => 'АО Ямало-Ненецкий',
        '91' => 'Республика Крым',
        '92' => 'г. Севастополь',
    ];

    // Метод возвращает не полный список городов доставки, а лишь города с возможностью доставки с наложенным платежом.
    const REQUEST_METHOD_FOR_DPD_CITIES = 'getCitiesCashPay';

    // Метод получения всех доступных пунктов приема/выдачи посылок, имеющих ограничения по габаритам и весу,
    // с указанием режима работы пункта и доступностью выполнения самопривоза/самовывоза.
    // Пункт приема и выдачи может быть в городе, в котором нет доставки наложенным платежом курьером.
    // При работе с  методом  необходимо проводить получение информации по списку подразделений ежедневно.
    const REQUEST_METHOD_FOR_DPD_PARCEL_SHOPS = 'getParcelShops';

    // Метод получения списка подразделений DPD, не имеющих ограничений по габаритам и весу посылок приема/выдачи
    const REQUEST_METHOD_FOR_DPD_TERMINALS_SELF_DELIVERY = 'getTerminalsSelfDelivery2';

    const DPD_UPDATE_INFO_NAME = 'dpd_update_dates';

    const DPD_FTP_SERVER = 'ftp.dpd.ru';
    const DPD_FTP_LOGIN = 'integration';
    const DPD_FTP_PASS = 'xYUX~7W98';
    const DPD_FTP_PATH = '/integration/';
    const DPD_GEOGRAPHY_REGEX = '#(GeographyDPD_([\d]+)\.csv)#';
    const CITY_CANDIDATE_FILE = 'sql_city_candidate_';

    /**
     * @var string
     */
    private $account;

    /**
     * @var string
     */
    private $secret_key;

    /**
     * @var string
     */
    private $gateway_host;

    /**
     * @var string
     */
    private $url_for_data_requests;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var string
     */
    private $dpd_upload_folder;

    /**
     * DeliveryDpdManager constructor.
     * @param EntityManager $entityManager
     * @param array $config
     */
    public function __construct(EntityManager $entityManager,
                                array $config)
    {
        $this->entityManager = $entityManager;

        $this->account      = $config['delivery_dpd_settings']['account'];
        $this->secret_key   = $config['delivery_dpd_settings']['secret_key'];
        $this->gateway_host = $config['delivery_dpd_settings']['gateway_host'];

        $this->url_for_data_requests    = $this->gateway_host.'geography2?wsdl';
        $this->dpd_upload_folder = './'.$config['file-management']['main_upload_folder'].'/dpd-geography/';
    }

    /**
     * @param DpdUpdateInfo $dpdUpdateInfo
     * @param array $number_of_cities
     * @param array $number_of_points
     * @return array
     */
    public function getOperationsAvailability(DpdUpdateInfo $dpdUpdateInfo, array $number_of_cities, array $number_of_points)
    {
        $operations_availability = [];

        //// City
        $operations_availability['update_city_candidate'] =
            $this->isUpdateCityCandidateAvailable($dpdUpdateInfo);
        $operations_availability['update_city_production'] =
            $this->isUpdateCityProductionAvailable($dpdUpdateInfo, $number_of_cities['candidate']);

        //// Point
        $operations_availability['update_point_candidate'] =
            $this->isUpdatePointCandidateAvailable($dpdUpdateInfo);
        $operations_availability['update_point_production'] =
            $this->isUpdatePointProductionAvailable($dpdUpdateInfo, $number_of_points['candidate']);

        return $operations_availability;
    }

    /**
     * @param DpdUpdateInfo $dpdUpdateInfo
     * @param int $number_of_candidate_point
     * @return bool
     */
    public function isUpdatePointProductionAvailable(DpdUpdateInfo $dpdUpdateInfo, int $number_of_candidate_point): bool
    {
        $update_point_production = true;
        /** @var \DateTime $dateOfPointCandidateUpdate */
        $dateOfPointCandidateUpdate = $dpdUpdateInfo->getLastDpdPointCandidateUpdate();
        /** @var \DateTime $dateOfPointProductionUpdate */
        $dateOfPointProductionUpdate = $dpdUpdateInfo->getLastDpdPointProductionUpdate();

        // Если дата обновления Продакш больше или равна дате обновления Кандидата или в Кандидате пусто
        if ($dateOfPointProductionUpdate >= $dateOfPointCandidateUpdate || $number_of_candidate_point === 0) {
            $update_point_production = false;
        }

        return $update_point_production;
    }

    /**
     * @param DpdUpdateInfo $dpdUpdateInfo
     * @return bool
     */
    public function isUpdatePointCandidateAvailable(DpdUpdateInfo $dpdUpdateInfo): bool
    {
        $update_point_candidate = true;
        /** @var \DateTime $dateOfPointCandidateUpdate */
        $dateOfPointCandidateUpdate = $dpdUpdateInfo->getLastDpdPointCandidateUpdate();

        if (null !== $dateOfPointCandidateUpdate) {
            // Если прошло меньше часа с последнего обновления Кандитата
            $timeFromLastPointCandidateUpdate = date_diff($dateOfPointCandidateUpdate, new \DateTime());
            if ($timeFromLastPointCandidateUpdate->days === 0 && $timeFromLastPointCandidateUpdate->h < 1) {
                $update_point_candidate = false;
            }
        }

        return $update_point_candidate;
    }


    /**
     * @param DpdUpdateInfo $dpdUpdateInfo
     * @param int $number_of_candidate_cities
     * @return bool
     */
    public function isUpdateCityProductionAvailable(DpdUpdateInfo $dpdUpdateInfo, int $number_of_candidate_cities): bool
    {
        $update_city_production = true;
        /** @var \DateTime $dateOfCityCandidateUpdate */
        $dateOfCityCandidateUpdate = $dpdUpdateInfo->getLastDpdCityCandidateUpdate();
        /** @var \DateTime $dateOfCityProductionUpdate */
        $dateOfCityProductionUpdate = $dpdUpdateInfo->getLastDpdCityProductionUpdate();

        // Если дата обновления Продакш больше или равна дате обновления Кандидата или в Кандидате пусто
        if ($dateOfCityProductionUpdate >= $dateOfCityCandidateUpdate || $number_of_candidate_cities === 0) {
            $update_city_production = false;
        }

        return $update_city_production;
    }

    /**
     * @param DpdUpdateInfo $dpdUpdateInfo
     * @return bool
     */
    public function isUpdateCityCandidateAvailable(DpdUpdateInfo $dpdUpdateInfo): bool
    {
        $update_city_candidate = true;
        $dateOfCityCandidateUpdate = $dpdUpdateInfo->getLastDpdCityCandidateUpdate();

        if (null !== $dateOfCityCandidateUpdate) {
            // Если прошло меньше часа с последнего обновления Кандитата
            $timeFromLastCityCandidateUpdate = date_diff($dateOfCityCandidateUpdate, new \DateTime());
            if ($timeFromLastCityCandidateUpdate->days === 0 && $timeFromLastCityCandidateUpdate->h < 1) {
                $update_city_candidate = false;
            }
        }

        return $update_city_candidate;
    }

    /**
     * @return array|DpdUpdateInfo
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getOrCreateDpdUpdateInfo()
    {
        $dpdUpdate = $this->entityManager->getRepository(DpdUpdateInfo::class)
            ->findOneBy(['name' => self::DPD_UPDATE_INFO_NAME]);

        if (null === $dpdUpdate) {
            $dpdUpdate = new DpdUpdateInfo();
            $dpdUpdate->setName(self::DPD_UPDATE_INFO_NAME);
            $dpdUpdate->setLastDpdCityCandidateUpdate(null);
            $dpdUpdate->setLastDpdCityProductionUpdate(null);
            $dpdUpdate->setLastDpdCityPreviousUpdate(null);
            $dpdUpdate->setLastDpdPointCandidateUpdate(null);
            $dpdUpdate->setLastDpdPointProductionUpdate(null);
            $dpdUpdate->setLastDpdPointPreviousUpdate(null);

            $this->entityManager->persist($dpdUpdate);

            $this->entityManager->flush();
        }

        return $dpdUpdate;
    }

    /**
     * @return mixed
     */
    public function getNumbersOfDpdCities()
    {
        $production = $this->entityManager
            ->getRepository(DpdCity::class)->countAll();

        $candidate = $this->entityManager
            ->getRepository(DpdCityCandidate::class)->countAll();

        $previous = $this->entityManager
            ->getRepository(DpdCityPrevious::class)->countAll();

        return ['production' => $production, 'candidate' => $candidate, 'previous' => $previous];
    }

    /**
     * @return mixed
     */
    public function getNumbersOfDpdPoints()
    {
        $production =  $this->entityManager
            ->getRepository(DpdPoint::class)->countAll();

        $candidate =  $this->entityManager
            ->getRepository(DpdPointCandidate::class)->countAll();

        $previous =  $this->entityManager
            ->getRepository(DpdPointPrevious::class)->countAll();

        return ['production' => $production, 'candidate' => $candidate, 'previous' => $previous];
    }

    /**
     * Upload geography csv from ftp
     *
     * @return null|string
     */
    public function uploadCandidateCsvFile()
    {
        $conn_id = \ftp_connect(self::DPD_FTP_SERVER);
        if (!\ftp_login($conn_id, self::DPD_FTP_LOGIN, self::DPD_FTP_PASS)) {

            throw new \RuntimeException("Фтп соединение не установленно\n");
        }
        //включаем пассивный режим
        ftp_pasv($conn_id, true);
        // получение списка файлов директории /integration
        $buff = ftp_rawlist($conn_id, self::DPD_FTP_PATH);
        $file_name = null;
        foreach ($buff as $file) {
            if (preg_match(self::DPD_GEOGRAPHY_REGEX, $file, $matches) && isset($matches['1'])) {
                $file_name = $matches['1'];
                break;
            }
        }
        unset($buff);
        /**
         * если такой файл уже скачен то не скачиваем повторно,
         * просто отдаем локальный файл
         */
        if ($file_name && file_exists($this->dpd_upload_folder.$file_name)) {
            \ftp_close($conn_id);

            return $this->dpd_upload_folder.$file_name;
        }
        // скачиваем файл
        if ($file_name && ftp_get($conn_id, $this->dpd_upload_folder.$file_name, self::DPD_FTP_PATH.$file_name, FTP_BINARY)) {
            \ftp_close($conn_id);

            return $this->dpd_upload_folder.$file_name;
        }

        if (!file_exists($this->dpd_upload_folder . $file_name) && \function_exists('logMessage')) {
            logMessage('Файл '.$this->dpd_upload_folder.$file_name.' не закачался', 'dpd-errors');
        }
        \ftp_close($conn_id);

        return null;
    }

    /**
     * @param $file_path
     * @return null|string|string[]
     */
    public function getCsvContentsInUtf8($file_path)
    {
        return mb_convert_encoding(file_get_contents($file_path), 'utf-8', 'CP1251');
    }

    /**
     * @param $abbreviation
     * @param $city_name
     * @param $region_name
     * @param $country_name
     * @return string
     */
    public function buildFullCityName($abbreviation, $city_name, $region_name, $country_name): string
    {
        return trim($abbreviation.'. '.$city_name.', '.$region_name.', '.$country_name);
    }

    /**
     * @param $file_content
     * @return array
     */
    public function getCityArrayInCsvContents($file_content): array
    {
        if (empty($file_content)) {

            return [];
        }

        $data = explode("\r", $file_content);
        $cityCandidate = [];
        // собираем массив для сохранения в csv
        foreach ($data as $item) {
            if ($item) {
                $result = explode(';', $item);
                preg_match('/([\D]{2,3})(([\d]{2,2})[\d]+)/', $result[1], $codes);

                $cityCandidate[$result[0]] = [
                    'city_id' => $result[0],
                    'country_code' => $codes[1],
                    'country_name' => $result[5],
                    'region_code' => $codes[3],
                    'region_name' => $result[4],
                    'city_code' => $codes[2],
                    'city_name' => $result[3],
                    'abbreviation' => $result[2],
                    'index_min' => '',
                    'index_max ' => '',
                    'payment_on_delivery ' => 0,
                    //полное название города для полнотекстового поиска
                    'full_city_name ' => $this->buildFullCityName($result[2], $result[3], $result[4], $result[5]),
                ];
            }
        }
        unset($data);

        return $cityCandidate;
    }

    /**
     * @param array $cityCandidate
     * @param array $cities
     * @return array
     */
    public function extendCityCandidate(array $cityCandidate, array $cities): array
    {
        if ($cities) {
            foreach ($cities as $city) {
                $csv_region_name = null;
                if (isset($cityCandidate[$city->cityId])) {
                    $csv_region_name = $cityCandidate[$city->cityId]['region_name'];
                    unset($cityCandidate[$city->cityId]);
                }
                $country_code = $city->countryCode;
                if (!$csv_region_name && $country_code === 'RU' && isset(self::REGION_NAMES_RU[$country_code])) {
                    $csv_region_name = self::REGION_NAMES_RU[$country_code];
                }
                $abbreviation = $city->abbreviation;
                $city_name = $city->cityName;
                $region_name = $csv_region_name ?? $city->regionName;
                $country_name = $city->countryName;

                $cityCandidate[$city->cityId] = [
                    'city_id' => $city->cityId,
                    'country_code' => $city->countryCode,
                    'country_name' => $country_name,
                    'region_code' => $city->regionCode,
                    'region_name' => $region_name,
                    'city_code' => $city->cityCode,
                    'city_name' => $city_name,
                    'abbreviation' => $abbreviation,
                    'index_min' => $city->indexMin ?? '',
                    'index_max ' => $city->indexMax ?? '',
                    'payment_on_delivery ' => 1,
                    //полное название города для полнотекстового поиска
                    'full_city_name ' => $this->buildFullCityName($abbreviation, $city_name, $region_name, $country_name),
                ];
            }
        }

        return $cityCandidate;
    }

    /**
     * @param array $cityCandidate
     * @param string $curr_date
     * @return bool
     */
    public function makeSqlCityCandidateFile(array $cityCandidate, string $curr_date): bool
    {
        if ($cityCandidate) {
            $fp = fopen($this->dpd_upload_folder . self::CITY_CANDIDATE_FILE . $curr_date . '.csv', 'w+');

            foreach ($cityCandidate as $fields) {
                fputcsv($fp, $fields, ';', '"');
            }
            fclose($fp);
        }

        return true;
    }

    /**
     * чистит папку от старых файлов
     *
     * @return bool
     */
    public function clearOldFilesFromDpdDirectory(): bool
    {
        $curr_date = new \DateTime();
        $curr_date = $curr_date->format('Ymd');
        $path = $this->dpd_upload_folder.'*.csv';

        foreach (glob($path) as $file){
            if (! preg_match('/(?=\w+_'.$curr_date.'\.csv$).+/', $file)) {
                @unlink($file);
            }
        }
        return true;
    }

    /**
     * Update Dpd City Candidate
     *
     * @throws \Exception
     * @throws \Throwable
     */
    public function updateDpdCityCandidateTable(): array
    {
        ini_set('memory_limit','-1');

        if (!file_exists($this->dpd_upload_folder) && !mkdir($this->dpd_upload_folder, 0777, true) && !is_dir($this->dpd_upload_folder)) {
            if (\function_exists('logMessage')) {
                logMessage('Failed to create directory...', 'dpd-errors');
            }

            throw new \RuntimeException('Failed to create directory...');
        }

        header('X-Progress-Max: 100', true, 200);
        $this->obIgnore('0', true);

        // чистим папку от старых csv
        $this->clearOldFilesFromDpdDirectory();

        $curr_date = new \DateTime();
        $curr_date = $curr_date->format('Ymd');
        $sql_file_path = $this->dpd_upload_folder . self::CITY_CANDIDATE_FILE . $curr_date . '.csv';

        if (! file_exists($sql_file_path)) {
            // Получаем данные / Если один из методов возвращает null, то Exception на уровень выше
            $cities = $this->requestDpdCities();

            // *** progress bar ***
            $this->obIgnore(' 40', true);

            // Скачиваем csv файл городов с DPD ftp
            $file_path = $this->uploadCandidateCsvFile();

            // *** progress bar ***
            $this->obIgnore(' 70', true);

            $cityCandidate = [];
            // если есть скаченный файл то обрабатываем
            if ($file_path) {
                // конвертим csv из ''CP1251' в utf-8' и получаем контент файла
                $file_content = $this->getCsvContentsInUtf8($file_path);

                // разбираем csv файл на массив
                $cityCandidate = $this->getCityArrayInCsvContents($file_content);
                unset($file_content);

                // *** progress bar ***
                $this->obIgnore(' 80', true);
            }
            // дополняем csv города городами полученными запросом
            $cityCandidate = $this->extendCityCandidate($cityCandidate, $cities);

            unset($cities);

            $this->makeSqlCityCandidateFile($cityCandidate, $curr_date);
            unset($cityCandidate);
        }
        // *** progress bar ***
        $this->obIgnore(' 90', true);

        $result = [];
        $result['message'] = [];

        if (file_exists($sql_file_path)) {
            // обнуляем и сразу сетим новые данные
            $this->entityManager
                ->getRepository(DpdCityCandidate::class)->loadDataInFile($sql_file_path);
            $result['message'][] = 'DpdCityCandidate data has cleared';
            $result['message'][] = 'DpdCityCandidate has filled by new data';

            // *** progress bar ***
            $this->obIgnore(' 95', true);

            // обнуляем иначе могут остаться неактуальные связи
            $this->removeDpdPointCandidateData();
            $result['message'][] = 'DpdPointCandidate (with limits) data  has cleared';

            // *** progress bar ***
            $this->obIgnore(' 99', true);

            // Меняем дату обновления CityCandidate
            $this->lastDpdCityCandidateDateUpdate();
            $result['message'][] = 'DpdUpdateInfo updated';
        } else {

            throw new LogicException(null, LogicException::DELIVERY_DPD_SQL_FILE_CITIES_NOT_FOUND);
        }

        $this->obIgnore(' end', true);

        $result['status'] = 'SUCCESS';

        return $result;
    }

    /**
     * update Dpd Point Candidate
     *
     * @throws LogicException
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Throwable
     */
    public function updateDpdPointCandidateTable(): array
    {
        // Получаем данные / Если один из методов возвращает null, то Exception на уровень выше
        $parcel_shops = $this->requestParcelShops();
        $terminals_self_delivery = $this->requestTerminalsSelfDelivery();

        // Если до сих пор не выбросило Exception, начинаем обновление
        $result = [];
        $result['message'] = [];

        $connection = $this->entityManager->getConnection();
        $connection->beginTransaction();
        try {
            // Обнуляем Dpd данные в БД
            $this->removeDpdPointCandidateData();
            $result['message'][] = 'DpdPointCandidate data has cleared';

            // Заполняем таблицу пунктов DpdPointCandidate и связанных лимитов
            $this->fillDpdPoint($parcel_shops, $terminals_self_delivery);
            $result['message'][] = 'DpdPointCandidate has filled by new data';

            // Меняем дату обновления PointCandidate
            $this->lastDpdPointCandidateDateUpdate();
            $result['message'][] = 'DpdUpdateInfo updated';

            $connection->commit();
        }
        catch (\Throwable $t) {
            $connection->rollBack();
            // На всякий случай возвращаем FOREIGN_KEY_CHECKS в true
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            throw $t;
        }

        $result['status'] = 'SUCCESS';
        return $result;
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function replaceDpdCityTable(): array
    {
        $result = [];
        $result['message'] = [];
        try {
            // Меняем таблицы DpdCity и DpdCityCandidate
            $this->entityManager->getRepository(DpdCity::class)->renameTable();
            $result['message'][] = 'DpdCity replaced by DpdCityCandidate';

            // Меняем даты изменения
            $this->replaceDpdCityUpdateDates();
        }
        catch (\Throwable $t) {
            $connection = $this->entityManager->getConnection();
            // На всякий случай возвращаем FOREIGN_KEY_CHECKS в true
            $connection->query('SET FOREIGN_KEY_CHECKS=1');

            throw new \LogicException($t->getMessage());
        }

        $result['status'] = 'SUCCESS';
        return $result;
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function replaceDpdPointTables(): array
    {
        $result = [];
        $result['message'] = [];
        try {
            // Меняем таблицы DpdPoint и DpdPointCandidate и таблицы лимитов
            $result = $this->entityManager->getRepository(DpdPoint::class)->renameTables($result);

            // Меняем даты изменения
            $this->replaceDpdPointUpdateDates();
        }
        catch (\Throwable $t) {
            $connection = $this->entityManager->getConnection();
            // На всякий случай возвращаем FOREIGN_KEY_CHECKS в true
            $connection->query('SET FOREIGN_KEY_CHECKS=1');

            throw new \LogicException($t->getMessage());
        }

        $result['status'] = 'SUCCESS';
        return $result;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @return void
     */
    private function replaceDpdCityUpdateDates()
    {
        $dpdUpdate = $this->getOrCreateDpdUpdateInfo();
        // В CityPreviousUpdate пишем дату CityProductionUpdate
        $dpdUpdate->setLastDpdCityPreviousUpdate($dpdUpdate->getLastDpdCityProductionUpdate());
        // В CityProductionUpdate пишем дату CityCandidateUpdate
        $dpdUpdate->setLastDpdCityProductionUpdate($dpdUpdate->getLastDpdCityCandidateUpdate());

        $this->entityManager->persist($dpdUpdate);
        $this->entityManager->flush();
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @return void
     */
    private function replaceDpdPointUpdateDates()
    {
        $dpdUpdate = $this->getOrCreateDpdUpdateInfo();

        // В PointPreviousUpdate пишем дату PointProductionUpdate
        $dpdUpdate->setLastDpdPointPreviousUpdate($dpdUpdate->getLastDpdPointProductionUpdate());
        // В PointProductionUpdate пишем дату PointCandidateUpdate
        $dpdUpdate->setLastDpdPointProductionUpdate($dpdUpdate->getLastDpdPointCandidateUpdate());

        $this->entityManager->persist($dpdUpdate);
        $this->entityManager->flush();
    }

    /**
     * @param DpdUpdateInfo $dpdUpdateInfo
     * @return array
     */
    public function getDpdUpdateInfoOutput(DpdUpdateInfo $dpdUpdateInfo): array
    {
        $dpdUpdateInfoOutput = [];
        $dpdUpdateInfoOutput['name'] = $dpdUpdateInfo->getName();
        $lastCityCandidateUpdate = $dpdUpdateInfo->getLastDpdCityCandidateUpdate();
        $dpdUpdateInfoOutput['last_city_candidate_update'] = $lastCityCandidateUpdate ? $lastCityCandidateUpdate->format('d.m.Y H:i:s') : null;
        $lastPointCandidateUpdate = $dpdUpdateInfo->getLastDpdPointCandidateUpdate();
        $dpdUpdateInfoOutput['last_point_candidate_update'] = $lastPointCandidateUpdate ? $lastPointCandidateUpdate->format('d.m.Y H:i:s') : null;
        $lastCityProductionUpdate = $dpdUpdateInfo->getLastDpdCityProductionUpdate();
        $dpdUpdateInfoOutput['last_city_production_update'] = $lastCityProductionUpdate ? $lastCityProductionUpdate->format('d.m.Y H:i:s') : null;
        $lastPointProductionUpdate = $dpdUpdateInfo->getLastDpdPointProductionUpdate();
        $dpdUpdateInfoOutput['last_point_production_update'] = $lastPointProductionUpdate ? $lastPointProductionUpdate->format('d.m.Y H:i:s') : null;
        $lastCityPreviousUpdate = $dpdUpdateInfo->getLastDpdCityPreviousUpdate();
        $dpdUpdateInfoOutput['last_city_previous_update'] = $lastCityPreviousUpdate ? $lastCityPreviousUpdate->format('d.m.Y H:i:s') : null;
        $lastPointPreviousUpdate = $dpdUpdateInfo->getLastDpdPointPreviousUpdate();
        $dpdUpdateInfoOutput['last_point_previous_update'] = $lastPointPreviousUpdate ? $lastPointPreviousUpdate->format('d.m.Y H:i:s') : null;

        return $dpdUpdateInfoOutput;
    }

    /**
     * Обнуляем таблицу городов DpdCity
     *
     * @throws LogicException
     */
    private function removeDpdCityCandidateData()
    {
        // Обнуляем таблицу городов DpdCity
        $this->entityManager->getRepository(DpdCityCandidate::class)->truncateTable();
        // Проверяем, что таблица пуста
        $dpdCities = $this->entityManager->getRepository(DpdCityCandidate::class)->findAll();
        if (\count($dpdCities) > 0) {

            throw new LogicException(null, LogicException::DELIVERY_DPD_UPDATE_ERROR_TRUNCATE_FAILED);
        }
    }

    /**
     * Обнуляем таблицу городов DpdPoint и связанной с ней DpdParcelShopLimits
     *
     * @throws LogicException
     */
    private function removeDpdPointCandidateData()
    {
        // Обнуляем таблицу городов DpdPoint и связанной с ней DpdParcelShopLimits
        $this->entityManager->getRepository(DpdPointCandidate::class)->truncateTables();
        // Проверяем, что таблица пуста
        $dpdPoints = $this->entityManager->getRepository(DpdPointCandidate::class)->findAll();
        if (\count($dpdPoints) > 0) {

            throw new LogicException(null, LogicException::DELIVERY_DPD_UPDATE_ERROR_TRUNCATE_FAILED);
        }

        // Проверяем, что таблица пуста
        $dpdParcelShopLimits = $this->entityManager->getRepository(DpdParcelShopLimitsCandidate::class)->findAll();
        if (\count($dpdParcelShopLimits) > 0) {

            throw new LogicException(null, LogicException::DELIVERY_DPD_UPDATE_ERROR_TRUNCATE_FAILED);
        }
    }

    /**
     * @param array $cities
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function fillDpdCityCandidate(array $cities)
    {
        $quantity_to_flash = 100;
        $count = \count($cities);

        header("X-Progress-Max: $count", true, 200);
        #header("X-Progress-Padding: 20");
        #$this->obIgnore(str_repeat(' ', 20), true);
        $this->obIgnore('0', true);

        $j = 0;
        foreach ($cities as $city) {
            $dpdCity = $this->createDpdCity($city, true);
            $this->entityManager->persist($dpdCity);
            $j++;
            if ($count === $j || ($j/$quantity_to_flash >= 1 && 0 === $j%$quantity_to_flash)) {
                $this->entityManager->flush();
                // Отправляем пробел на вывод (progress bar)
                $this->obIgnore(' '.$j, true);
            }
        }

        $this->obIgnore(' end', true);
    }

    /**
     * @param array $parcel_shops
     * @param array $terminals_self_delivery
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function fillDpdPoint(array $parcel_shops, array $terminals_self_delivery)
    {
        /** @var DpdParcelShopType $dpdParcelShopType */
        $dpdParcelShopType = $this->entityManager
            ->getRepository(DpdParcelShopType::class)->findOneBy(['code' => 'PARCELSHOP']);
        /** @var DpdParcelShopState $dpdParcelShopStateFull */
        $dpdParcelShopStateFull = $this->entityManager
            ->getRepository(DpdParcelShopState::class)->findOneBy(['code' => 'full']);
        /** @var DpdParcelShopState $dpdParcelShopStateOpen */
        $dpdParcelShopStateOpen = $this->entityManager
            ->getRepository(DpdParcelShopState::class)->findOneBy(['code' => 'open']);

        $quantity_to_flash = 500;
        $parcel_shops_count = \count($parcel_shops);
        $terminals_count = \count($terminals_self_delivery);

        $total_points_count = $parcel_shops_count + $terminals_count;

        header("X-Progress-Max: $total_points_count", true, 200);
        $this->obIgnore('0', true);

        $j = 0;
        foreach ($parcel_shops as $parcelShop) {

            $dpdPoint = $this->createDpdParcelShopsPoint($parcelShop, $dpdParcelShopType, $dpdParcelShopStateFull, $dpdParcelShopStateOpen);
            if (null !== $dpdPoint) {
                $this->entityManager->persist($dpdPoint);
            }

            $j++;
            if ($parcel_shops_count === $j || ($j/$quantity_to_flash >= 1 && 0 === $j%$quantity_to_flash)) {
                $this->entityManager->flush();
                // Отправляем пробел на вывод (progress bar)
                $this->obIgnore(' '.$j, true);
            }
        }

        /** @var DpdParcelShopType $dpdParcelShopType */
        $dpdParcelShopType = $this->entityManager
            ->getRepository(DpdParcelShopType::class)->findOneBy(['code' => 'TERMINAL']);

        foreach ($terminals_self_delivery as $terminal) {
            $dpdPoint = $this->createDpdTerminalsSelfDeliveryPoint($dpdParcelShopType, $terminal);
            if (null !== $dpdPoint) {
                $this->entityManager->persist($dpdPoint);
            }
        }

        $this->entityManager->flush();
        // Отправляем пробел на вывод (progress bar)
        $this->obIgnore(' '.$total_points_count, true);
        $this->obIgnore(' end', true);
    }

    /**
     * @param $data
     * @param bool $flush
     */
    private function obIgnore($data, $flush = false)
    {
        $ob = array();
        while (ob_get_level())
        {
            array_unshift($ob, ob_get_contents());
            ob_end_clean();
        }

        echo $data;
        if ($flush) {
            flush();
        }
    }

    /**
     * @param DpdParcelShopType $dpdParcelShopType
     * @param \stdClass $terminal
     * @return DpdPointCandidate
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createDpdTerminalsSelfDeliveryPoint(DpdParcelShopType $dpdParcelShopType, \stdClass $terminal)
    {
        /** @var DpdCity $dpdCity */
        $dpdCity = $this->entityManager
            ->getRepository(DpdCityCandidate::class)->findOneBy(['cityId' => $terminal->address->cityId]);

        if (null === $dpdCity) {
            // Создание города, в который нет доставки наложенным платежом курьеру
            $dpdCity = $this->createDpdCityFromAddress($terminal->address, false);
            $this->entityManager->persist($dpdCity);
            $this->entityManager->flush($dpdCity);
            // Меняем дату обновления кандидата городов
            $this->lastDpdCityCandidateDateUpdate();
        }

        $dpdPoint = new DpdPointCandidate();
        $dpdPoint->setCity($dpdCity);
        $dpdPoint->setDpdParcelShopType($dpdParcelShopType);
        $dpdPoint->setCode($terminal->terminalCode);
        if (isset($terminal->terminalName)) {
            $dpdPoint->setName($terminal->terminalName);
        }
        #$dpdPoint->setCashpay();
        $dpdPoint->setStreet($terminal->address->street);
        if (isset($terminal->address->streetAbbr)) {
            $dpdPoint->setStreetAbr($terminal->address->streetAbbr);
        }
        if (isset($terminal->address->houseNo)) {
            $dpdPoint->setHouse($terminal->address->houseNo);
        }
        if (isset($terminal->address->structure)) {
            $dpdPoint->setStructure($terminal->address->structure);
        }
        if (isset($terminal->address->ownership)) {
            $dpdPoint->setOwnership($terminal->address->ownership);
        }
        $dpdPoint->setLatitude($terminal->geoCoordinates->latitude);
        $dpdPoint->setLongitude($terminal->geoCoordinates->longitude);

        return $dpdPoint;
    }

    /**
     * @param \stdClass $parcelShop
     * @param DpdParcelShopType $dpdParcelShopType
     * @param $dpdParcelShopStateFull
     * @param $dpdParcelShopStateOpen
     * @return DpdPointCandidate
     * @throws \Exception
     */
    private function createDpdParcelShopsPoint(
        \stdClass $parcelShop,
        DpdParcelShopType $dpdParcelShopType,
        $dpdParcelShopStateFull,
        $dpdParcelShopStateOpen): DpdPointCandidate
    {
        /** @var DpdCity $dpdCity */
        $dpdCity = $this->entityManager
            ->getRepository(DpdCityCandidate::class)->findOneBy(['cityId' => $parcelShop->address->cityId]);

        if (null === $dpdCity) {
            // Создание города, в который нет доставки наложенным платежом курьеру
            $dpdCity = $this->createDpdCityFromAddress($parcelShop->address, false);
            $this->entityManager->persist($dpdCity);
            $this->entityManager->flush($dpdCity);
            // Меняем дату обновления кандидата городов
            $this->lastDpdCityCandidateDateUpdate();
        }

        $dpdPoint = new DpdPointCandidate();
        $dpdPoint->setCity($dpdCity);
        $dpdPoint->setDpdParcelShopType($dpdParcelShopType);
        if ($dpdParcelShopStateFull && strtolower($parcelShop->state) === 'full') {
            $dpdPoint->setDpdParcelShopState($dpdParcelShopStateFull);
        }
        if ($dpdParcelShopStateOpen && strtolower($parcelShop->state) === 'open') {
            $dpdPoint->setDpdParcelShopState($dpdParcelShopStateOpen);
        }

        $dpdPoint->setCode($parcelShop->code);
        if (isset($parcelShop->brand)) {
            $dpdPoint->setName($parcelShop->brand);
        }
        $dpdPoint->setStreet($parcelShop->address->street);
        if (isset($parcelShop->address->streetAbbr)) {
            $dpdPoint->setStreetAbr($parcelShop->address->streetAbbr);
        }
        if (isset($parcelShop->address->houseNo)) {
            $dpdPoint->setHouse($parcelShop->address->houseNo);
        }
        if (isset($parcelShop->address->structure)) {
            $dpdPoint->setStructure($parcelShop->address->structure);
        }
        if (isset($parcelShop->address->ownership)) {
            $dpdPoint->setOwnership($parcelShop->address->ownership);
        }
        if (isset($parcelShop->address->descript)) {
            if (\strlen($parcelShop->address->descript) > 5000) {
                logMessage($parcelShop->code. ' description more than 5000 - ' . \strlen($parcelShop->address->descript), 'dpd');
            }
            $dpdPoint->setDescription($parcelShop->address->descript);
        }
        $dpdPoint->setLatitude($parcelShop->geoCoordinates->latitude);
        $dpdPoint->setLongitude($parcelShop->geoCoordinates->longitude);

        // dpdParcelShopLimits
        if (isset($parcelShop->limits)) {
            $dpdParcelShopLimits = $this->createDpdParcelShopLimits($parcelShop->limits);
            $this->entityManager->persist($dpdParcelShopLimits);

            $dpdPoint->setDpdParcelShopLimits($dpdParcelShopLimits);
        }
        // dpdSchedules
        if (isset($parcelShop->schedule) && \count($parcelShop->schedule) > 0) {
            $this->createSchedulesForParcelShop($dpdPoint, $parcelShop->schedule);
        }

        return $dpdPoint;
    }

    /**
     * @param DpdPointCandidate $dpdPoint
     * @param $schedule
     * @return array
     */
    private function createSchedulesForParcelShop(DpdPointCandidate $dpdPoint, $schedule): array
    {
        $schedules = [];
        if ($schedule instanceof \stdClass) {
            $schedules[] = $schedule;
        } elseif (\is_array($schedule)) {
            $schedules = $schedule;
        }

        $dpdSchedules = [];
        foreach ($schedules as $sch) {
            $dpdSchedule = new DpdScheduleCandidate();
            $dpdSchedule->setDpdPoint($dpdPoint);
            $dpdSchedule->setType($sch->operation);
            if (isset($sch->timetable) && \count($sch->timetable) > 0) {
                $this->createTimetablesForSchedule($dpdSchedule, $sch->timetable);
            }
            $dpdSchedules[] = $dpdSchedule;

            $this->entityManager->persist($dpdSchedule);
        }

        return $dpdSchedules;
    }

    /**
     * @param DpdScheduleCandidate $dpdSchedule
     * @param $timetable
     * @return array
     */
    private function createTimetablesForSchedule(DpdScheduleCandidate $dpdSchedule, $timetable)
    {
        $timetables = [];
        if ($timetable instanceof \stdClass) {
            $timetables[] = $timetable;
        } elseif (\is_array($timetable)) {
            $timetables = $timetable;
        }

        $dpdTimetables = [];
        foreach ($timetables as $tt) {
            $dpdTimetable = new DpdTimetableCandidate();
            $dpdTimetable->setSchedule($dpdSchedule);
            $dpdTimetable->setWeekDays($tt->weekDays);
            $dpdTimetable->setWorkTime($tt->workTime);
            $dpdTimetables[] = $dpdTimetable;

            $this->entityManager->persist($dpdTimetable);
        }

        return $dpdTimetables;
    }

    /**
     * @param \stdClass $limits
     * @return DpdParcelShopLimitsCandidate
     *
     * 'limits' =>
     *   object(stdClass)
     *   public 'maxShipmentWeight' => string '400' (length=3)
     *   public 'maxWeight' => string '30' (length=2)
     *   public 'maxLength' => string '200' (length=3)
     *   public 'maxWidth' => string '200' (length=3)
     *   public 'maxHeight' => string '200' (length=3)
     *   public 'dimensionSum' => string '200' (length=3)
     */
    private function createDpdParcelShopLimits(\stdClass $limits): DpdParcelShopLimitsCandidate
    {
        $dpdParcelShopLimits = new DpdParcelShopLimitsCandidate();
        #$dpdParcelShopLimits->setCode(); // XS, S, M, L, XL, XLL где взять?
        $dpdParcelShopLimits->setWeight($limits->maxWeight);
        $dpdParcelShopLimits->setLength($limits->maxLength);
        $dpdParcelShopLimits->setWidth($limits->maxWidth);
        $dpdParcelShopLimits->setHeight($limits->maxHeight);
        if (isset($limits->dimensionSum)) {
            $dpdParcelShopLimits->setDimensionSum($limits->dimensionSum);
        }

        return $dpdParcelShopLimits;
    }

    /**
     * @param \stdClass $city
     * @param bool $payment_on_delivery
     * @return DpdCityCandidate
     */
    private function createDpdCity(\stdClass $city, bool $payment_on_delivery = true): DpdCityCandidate
    {
        $dpdCity = new DpdCityCandidate;
        $dpdCity->setCityId($city->cityId);
        $dpdCity->setCountryCode($city->countryCode);
        $dpdCity->setCountryName($city->countryName);
        $dpdCity->setRegionCode($city->regionCode);
        $dpdCity->setRegionName($city->regionName);
        $dpdCity->setCityCode($city->cityCode);
        $dpdCity->setCityName($city->cityName);
        $dpdCity->setAbbreviation($city->abbreviation);
        if (isset($city->indexMin)) {
            $dpdCity->setIndexMin($city->indexMin);
        }
        if (isset($city->indexMax)) {
            $dpdCity->setIndexMax($city->indexMax);
        }
        $dpdCity->setPaymentOnDelivery($payment_on_delivery);

        return $dpdCity;
    }

    /**
     * @param \stdClass $address
     * @param bool $payment_on_delivery
     * @return DpdCityCandidate
     */
    private function createDpdCityFromAddress(\stdClass $address, bool $payment_on_delivery = false): DpdCityCandidate
    {
        $dpdCity = new DpdCityCandidate;
        $dpdCity->setCityId($address->cityId);
        $dpdCity->setCountryCode($address->countryCode);
        #$dpdCity->setCountryName($city->countryName); // нет в address
        $dpdCity->setRegionCode($address->regionCode);
        $dpdCity->setRegionName($address->regionName);
        $dpdCity->setCityCode($address->cityCode);
        $dpdCity->setCityName($address->cityName);
        #$dpdCity->setAbbreviation($city->abbreviation); // нет в address
        $dpdCity->setPaymentOnDelivery($payment_on_delivery);
        $dpdCity->setCityName(trim($address->cityName.', '.$address->regionName));

        return $dpdCity;
    }

    /**
     * @param \DateTime|null $date
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function lastDpdCityCandidateDateUpdate(\DateTime $date = null)
    {
        $date = $date ?? new \DateTime();
        $dpdUpdate = $this->getOrCreateDpdUpdateInfo();
        $dpdUpdate->setLastDpdCityCandidateUpdate($date);

        $this->entityManager->persist($dpdUpdate);
        $this->entityManager->flush($dpdUpdate);
    }

    /**
     * @param \DateTime|null $date
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function lastDpdPointCandidateDateUpdate(\DateTime $date = null)
    {
        $date = $date ?? new \DateTime();
        $dpdUpdate = $this->getOrCreateDpdUpdateInfo();
        $dpdUpdate->setLastDpdPointCandidateUpdate($date);

        $this->entityManager->persist($dpdUpdate);
        $this->entityManager->flush($dpdUpdate);
    }

    /**
     * Получаем все города из Dpd
     * @return mixed
     * @throws LogicException
     */
    public function requestDpdCities()
    {
        $wsdl_url = $this->url_for_data_requests;
        $method = self::REQUEST_METHOD_FOR_DPD_CITIES;

        $arguments = [];
        $arguments['request'] = [];
        $arguments['request']['auth'] = $this->getAuthData();
        #$arguments['request']['countryCode'] = 'RU';

        /*
         * Параметры на вход:
         * Внешний тэг: request
         * auth [
         *      clientNumber    // клиентский номер в системе DPD / (номер вашего договора с DPD) / число / 1000000000
         *      clientKey       // уникальный ключ для авторизации, полученный у сотрудника DPD / строка / 1FD890C3556
         * ],
         * countryCode          // код страны / строка / RU / необязательный
         *
         * Поля countryCode задаются для фильтрации списка.
         * Если поле не задано, возвращается полный список населенных пунктов зоны RU.
         */
        $result = $this->doSoapRequest($wsdl_url, $method, $arguments);
        /*
         * Ответ:
         * Массив городов
         * city [
         *      cityId          // Идентификатор города / Число / 195644235
         *      countryCode     // Код страны / строка / RU
         *      countryName     // Страна / строка / Россия
         *      regionCode      // Код региона / строка / 50
         *      regionName      // Регион / строка / Московская
         *      cityCode        // Код населенного пункта / строка / 50017001000
         *      cityName        // Населенный пункт / строка / Люберцы (буквенные обозначения аббревиатур и других знаков)
         *      abbreviation    // Аббревиатура / строка / г
         *      indexMin        // Минимальный индекс / строка / 140000 / не обязательный
         *      indexMax        // Максимальный индекс / строка / 143818 / не обязательный
         * ]
         */

        if (null === $result || null === $result->return || empty($result->return)) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_REQUEST_NO_CITIES_RETURNED);
        }

        return $result->return;
    }

    /**
     * @param string|null $region_code
     * @param string|null $city_code
     * @return mixed
     * @throws LogicException
     */
    public function requestParcelShops(string $region_code = null, string $city_code = null)
    {
        $wsdl_url = $this->url_for_data_requests;
        $method = self::REQUEST_METHOD_FOR_DPD_PARCEL_SHOPS;

        $arguments = [];
        $arguments['request'] = [];
        $arguments['request']['auth'] = $this->getAuthData();
        #$arguments['request']['countryCode'] = 'RU';
        if (null !== $region_code) {
            $arguments['request']['regionCode'] = trim($region_code);
        }
        if (null !== $city_code) {
            $arguments['request']['cityCode'] = trim($city_code);
        }

        /*
         * Параметры на вход:
         * Внешний тэг: request
         * auth [
         *      clientNumber    // клиентский номер в системе DPD / (номер вашего договора с DPD) / число / 1000000000
         *      clientKey       // уникальный ключ для авторизации, полученный у сотрудника DPD / строка / 1FD890C3556
         * ],
         * countryCode          // код страны / строка / RU / необязательный
         * regionCode           // Код региона / строка / 77 / необязательный
         * cityCode             // Код  города / строка / 77011000011 / необязательный
         * cityName             // Наименование города / строка / Москва (буквенные обозначения аббревиатур и других знаков) / необязательный
         */
        $result = $this->doSoapRequest($wsdl_url, $method, $arguments);
        /*
         * Ответ:
         * Массив ПВП и пунктов
         * parcelShop [
         *      code            // Код подразделения DPD / строка / LED
         *      parcelShopType  // Тип подразделения / строка / *см. Возможные типы подразделения ниже
         *      state           // Состояние подразделения / строка / *см. Возможные типы подразделения ниже
         *      address         // Адрес пункта / address / *см. Описание объекта типа address
         *      geoCoordinates  // Географические координаты по карте Яндекс / geoCoordinates / *см. Описание объекта типа geoCoordinates
         *      limits          // Ограничения параметров посылки / limits / *см. Описание объекта типа limits
         *      schedule        // Массив операций производственного подразделения / schedule / *см. Описание объекта schedule ниже
         *      extraService    // Массив опций / extraService /
         *      services        // Массив услуг / services / *см. Описание объекта типа service
         *      brand
         * ]
         */

        if (null === $result || null === $result->return || null === $result->return->parcelShop || empty($result->return->parcelShop)) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_DB_UPDATE_NO_PARCEL_SHOPS_RETURNED);
        }

        return $result->return->parcelShop;
    }

    /**
     * @return mixed
     * @throws LogicException
     */
    public function requestTerminalsSelfDelivery()
    {
        $wsdl_url = $this->url_for_data_requests;
        $method = self::REQUEST_METHOD_FOR_DPD_TERMINALS_SELF_DELIVERY;

        $arguments = [];
        $arguments['auth'] = $this->getAuthData();

        /*
         * Параметры на вход (внешнего тэга нет!):
         * auth [
         *      clientNumber    // клиентский номер в системе DPD / (номер вашего договора с DPD) / число / 1000000000
         *      clientKey       // уникальный ключ для авторизации, полученный у сотрудника DPD / строка / 1FD890C3556
         * ]
         */
        $result = $this->doSoapRequest($wsdl_url, $method, $arguments);
        /*
         * Ответ:
         * Массив терминалов DPD
         * terminal [
         *      terminalCode    // Код терминала DPD / строка / M13
         *      terminalName    // Название терминала DPD / строка / Москва
         *      address         // Адрес пункта / address / *см. Описание объекта типа address
         *      geoCoordinates  // Географические координаты по карте Яндекс / geoCoordinates / *см. Описание объекта типа geoCoordinates
         *      schedule        // Массив операций производственного подразделения / schedule / *см. Описание объекта schedule ниже
         *      extraService    // Массив опций / extraService /
         *      services        // Массив услуг / services / *см. Описание объекта типа service
         * ]
         */

        if (null === $result || null === $result->return || null === $result->return->terminal || empty($result->return->terminal)) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_DB_UPDATE_NO_TERMINALS_SELF_DELIVERY_RETURNED);
        }

        return $result->return->terminal;
    }

    /**
     * @return array
     */
    private function getAuthData(): array
    {
        return [
            'clientNumber' => $this->account,
            'clientKey' => $this->secret_key
        ];
    }

    /**
     * @param string $wsdl_url
     * @param string $method
     * @param array $arguments
     * @return mixed
     */
    private function doSoapRequest(string $wsdl_url, string $method, array $arguments)
    {
        $client = new SoapClient($wsdl_url);

        return $client->$method($arguments);
    }

    /**
     * @param $count
     * @param $quantity_to_flash
     * @return float|int
     */
    private function countIterationsNumber($count, $quantity_to_flash)
    {
        $iterations = $count/$quantity_to_flash;

        if ((int)$iterations !== $iterations) {
            $iterations = (int)$iterations + 1;
        }

        return $iterations;
    }

    /*
     * Описание объекта типа address:
     *
     * cityId       // Идентификатор города отправлени / число / 195868771
     * countryCode  // Код страны / строка / RU
     * regionCode   // Код региона / строка / 77
     * regionName   // Регион / строка / Московская обл. (формат ФИАС)
     * cityCode     // Код города / строка / 77000000000
     * cityName     // Город / строка / Москва (без буквенных обозначений аббревиатур и других знаков)
     * street       // Наименование улицы / строка / Земляной Вал
     * streetAbbr   // Аббревиатура улицы / строка / ул
     * houseNo      // Номер дома / строка / 7
     * building     // Корпус / строка /
     * structure    // Строение / строка /
     * ownership    // Владение / строка /
     * descript     // Описание проезда / строка
     */

    /*
     * Описание объекта типа geoCoordinates
     *
     * latitude     // Широта / число / 53.322927
     * longitude    // Долгота / число / 83.638803
     */

    /*
     * Описание объекта типа limits
     *
     * maxShipmentWeight    // Макс. вес отправки в кг / число / 30.5
     * maxWeight            // Макс. Вес посылки  в кг / число / 5.5
     * maxLength            // Макс. длина в см / число / 70
     * maxWidth             // Макс. ширина в см / число / 70
     * maxHeight            // Макс. высота в см / число / 50
     * dimensionSum         // Сумма габаритов в см / число / 200
     */

    /*
     * Описание объекта типа timetable
     *
     * weekDays // Список дней недели / строка / пн,вт,ср,чт,пт
     * workTime // Список времени работы / строка / 09:00–20:00
     */

    /*
     * Описание объекта типа service
     *
     * serviceCode // Код услуги / строка / PCL
     * days // Количество дней / число / 3
     */

    /*
     * Описание объекта типа extraService
     *
     * esCode // Код опции / строка / НПП / *см Опции ниже
     */

    /*
     * Описание объекта schedule
     *
     * operation // Наименование операции / Варианты операций / строка / *см. Возможные варианты операций ниже
     * timetable // Массив расписания для операции / timetable / *см. Описание объекта типа timetable ниже
     */

    /*
     * Возможные типы подразделения:
     * П    – постамат
     * ПВП  – пункт приема/выдачи посылок
     */

    /*
     * Возможные состояния пункта:
     * open - Открыт
     * full - Переполнен
     */

    /*
     * Возможные варианты операций:
     * SelfPickup           – прием посылок
     * SelfDelivery         – выдача посылок
     * Payment              - оплата наличными за доставку
     * PaymentByBankCard    - оплата банковской картой
     */

    /*
     * Опции:
     * ОЖД // Ожидание на адресе / reason_delay / причина ожидания на адресе из Справочника причин ожидания на адресе:
     *                                            ПРИМ С примеркой
     *                                            ПРОС Простая
     *                                            РАБТ С проверкой работоспособности
     * НПП // Наложенный платеж / sum_npp / Максимальная допустимая сумма НПП за заказ (пример: 15000)
     * ТРМ // Температурный режим
     */
}