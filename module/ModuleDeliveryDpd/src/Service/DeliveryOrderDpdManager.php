<?php
namespace ModuleDeliveryDpd\Service;

use Core\Exception\LogicException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ModuleDeliveryDpd\Entity\DpdDeliveryOrder;
use ModuleDeliveryDpd\Entity\DpdDeliveryAddress;
use ModuleDeliveryDpd\Entity\DpdDeliveryParcel;
use ModuleDeliveryDpd\Entity\DpdCity;
use Doctrine\ORM\EntityManager;
use ModuleDeliveryDpd\Entity\DpdPoint;
use ModuleDeliveryDpd\Entity\DpdSchedule;
use ModuleDeliveryDpd\Entity\DpdTimetable;

/**
 * Class DeliveryOrderDpdManager
 * @package ModuleDeliveryDpd\Service
 */
class DeliveryOrderDpdManager
{
    const MAX_RECURSION_LEVEL = 20;

    // Варианты доставки
    const DELIVERY_OPTIONS = [
        'ДД' => 'от двери отправителя до двери получателя',
        'ДТ' => 'от двери отправителя до терминала DPD',
        'ТД' => 'от терминала DPD до двери получателя',
        'ТТ' => 'от терминала DPD до терминала DPD',
    ];

    // Интервалы времени приёма
    const RECEIPT_TIME_INTERVALS = [
        [
            'name' => '9-18',
            'description' => 'в любое время с 09:00 до 18:00',
            'additional' => 'вариант по умолчанию'
        ],
        [
            'name' => '9-13',
            'description' => 'с 09:00 до 13:00',
            'additional' => null
        ],
        [
            'name' => '13-18',
            'description' => 'с 13:00 до 18:00',
            'additional' => null
        ],
    ];

    // Интервалы времени доставки
    const DELIVERY_TIME_INTERVALS = [
        [
            'name' => '9-18',
            'description' => 'в любое время с 09:00 до 18:00',
            'additional' => 'вариант по умолчанию'
        ],
        [
            'name' => '9-14',
            'description' => 'с 09:00 до 14:00',
            'additional' => null
        ],
        [
            'name' => '13-18',
            'description' => 'с 13:00 до 18:00',
            'additional' => null
        ],
        [
            'name' => '18-22',
            'description' => 'с 18:00 до 22:00',
            'additional' => 'внимание, данный интервал оплачивается дополнительно!'
        ],
        [
            'name' => '9-22',
            'description' => 'с 09:00 до 22:00',
            'additional' => 'внимание, данный интервал работает только по услугам DPD Online Express, DPD Online Classic, DPD Classic international'
        ],
    ];

    // Варианты формы оплаты
    const PAYMENT_METHOD_OPTIONS = [
        'ОУП' => 'оплата у получателя наличными',
        'ОУО' => 'оплата у отправителя наличными',
    ];
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var DeliveryDpdManager
     */
    private $deliveryDpdManager;

    /**
     * DeliveryOrderDpdManager constructor.
     * @param EntityManager $entityManager
     * @param DeliveryDpdManager $deliveryDpdManager
     */
    public function __construct(EntityManager $entityManager,
                                DeliveryDpdManager $deliveryDpdManager)
    {
        $this->entityManager        = $entityManager;
        $this->deliveryDpdManager   = $deliveryDpdManager;
    }

    /**
     * @param int $id
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getDeliveryOrderDpd(int $id)
    {
        return $this->entityManager->find(DpdDeliveryOrder::class, $id);
    }

    /**
     * @return array|null
     */
    public function getPendingDpdDeliveryOrder()
    {
        $pendingDpdDeliveryOrders = $this->entityManager->getRepository(DpdDeliveryOrder::class)
            ->findBy(['dpdOrderCreationStatus' => DeliveryDpdManager::DPD_PENDING_CREATION_STATUS]);

        return $pendingDpdDeliveryOrders;
    }

    /**
     * @param array $data
     * @return DpdDeliveryOrder
     * @throws \Exception
     */
    public function createDeliveryOrderDpd(array $data): DpdDeliveryOrder
    {
        // Если в data нет parcels или parcels - пустой массив, то создаем один элемент parcels
        // Он нам нужен, чтобы получить id посылки для формирования номера посылки для обращения к API DPD
        if (!isset($data['parcels']) || \count($data['parcels']) === 0) {
            $data['parcels'] = [
                ['weight' => null, 'length' => null, 'width' => null, 'height' => null]
            ];
        }
        $deliveryOrderDpd = new DpdDeliveryOrder();
        // Наполнение данными
        $deliveryOrderDpd = $this->fillDeliveryOrderDpd($deliveryOrderDpd, $data);

        $this->entityManager->persist($deliveryOrderDpd);
        $this->entityManager->flush();

        return $deliveryOrderDpd;
    }

    /**
     * @param DpdDeliveryOrder $dpdDeliveryOrder
     * @throws \Exception
     */
    public function removeDeliveryOrderDpd(DpdDeliveryOrder $dpdDeliveryOrder)
    {
        $dpdDeliveryOrder->setDeliveryOrder(null);
        $parcels = $dpdDeliveryOrder->getParcels();

        $dpdDeliveryOrder->setParcels(new ArrayCollection());
        $this->entityManager->persist($dpdDeliveryOrder);
        $this->entityManager->flush($dpdDeliveryOrder);

        /** @var DpdDeliveryParcel $parcel */
        foreach ($parcels as $parcel) {
            $parcel->setDpdDeliveryOrder(null);
            $this->entityManager->persist($parcel);
            $this->entityManager->flush($parcel);

            $this->entityManager->remove($parcel);
        }

        $this->entityManager->remove($dpdDeliveryOrder);
        $this->entityManager->flush();
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @param array $data
     * @return DpdDeliveryOrder
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateDeliveryOrderDpd(DpdDeliveryOrder $deliveryOrderDpd, array $data): DpdDeliveryOrder
    {
        $deliveryOrderDpd = $this->fillDeliveryOrderDpd($deliveryOrderDpd, $data);

        $this->entityManager->persist($deliveryOrderDpd);
        $this->entityManager->flush();

        return $deliveryOrderDpd;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @return array
     */
    public function getDeliveryOrderDpdOutput(DpdDeliveryOrder $deliveryOrderDpd): array
    {
        $deliveryOrderDpdOutput = [];
        $deliveryOrderDpdOutput['id'] = $deliveryOrderDpd->getId();
        $deliveryOrderDpdOutput['order_number'] = $deliveryOrderDpd->getOrderNumber();
        $deliveryOrderDpdOutput['pickup_time_period'] = $deliveryOrderDpd->getPickupTimePeriod();
        $deliveryOrderDpdOutput['delivery_time_period'] = $deliveryOrderDpd->getDeliveryTimePeriod();
        $deliveryOrderDpdOutput['service_code'] = $deliveryOrderDpd->getServiceCode();
        $deliveryOrderDpdOutput['declared_value'] = $deliveryOrderDpd->getDeclaredValue();
        $deliveryOrderDpdOutput['cargo_category'] = $deliveryOrderDpd->getCargoCategory();
        $deliveryOrderDpdOutput['payment_type'] = $deliveryOrderDpd->getPaymentType();
        $deliveryOrderDpdOutput['dpd_order_creation_status'] = $deliveryOrderDpd->getDpdOrderCreationStatus();
        $deliveryOrderDpdOutput['dpd_order_creation_error'] = $deliveryOrderDpd->getDpdOrderCreationError();
        /** @var \DateTime $datePickup */
        $datePickup = $deliveryOrderDpd->getDatePickup();
        $deliveryOrderDpdOutput['date_pickup'] = null === $datePickup ? null : $datePickup->format('Y-m-d');
        $deliveryOrderDpdOutput['sender_address'] = null;
        $deliveryOrderDpdOutput['receiver_address'] = null;
        if (null !== $deliveryOrderDpd->getSenderAddress()) {
            $is_sender = true;
            $deliveryOrderDpdOutput['sender_address'] = $this->getAddressOutput($deliveryOrderDpd->getSenderAddress(), $is_sender);
        }
        if (null !== $deliveryOrderDpd->getReceiverAddress()) {
            $is_sender = false;
            $deliveryOrderDpdOutput['receiver_address'] = $this->getAddressOutput($deliveryOrderDpd->getReceiverAddress(), $is_sender);
        }
        $deliveryOrderDpdOutput['parcels'] = $this->getParcelCollectionForOutput($deliveryOrderDpd->getParcels());

        return $deliveryOrderDpdOutput;
    }

    /**
     * @param DpdDeliveryAddress $address
     * @param bool $is_sender
     * @return array
     */
    public function getAddressOutput(DpdDeliveryAddress $address, $is_sender = true): array
    {
        $addressOutput = [];
        $addressOutput['id'] = $address->getId();
        $addressOutput['name'] = $address->getName();
        $addressOutput['terminal_code'] = $address->getTerminalCode();
        $addressOutput['terminal'] = null;
        if (!empty($addressOutput['terminal_code'])) {
            $addressOutput['terminal'] = $this->getTerminalOutputByCode($addressOutput['terminal_code'], $is_sender);
        }
        $addressOutput['index'] = $address->getIndex();
        $addressOutput['city_id'] = $address->getCityId();
        $addressOutput['street'] = $address->getStreet();
        $addressOutput['street_abr'] = $address->getStreetAbr();
        $addressOutput['house'] = $address->getHouse();
        $addressOutput['house_korpus'] = $address->getHouseKorpus();
        $addressOutput['str'] = $address->getStr();
        $addressOutput['vlad'] = $address->getVlad();
        $addressOutput['extra_info'] = $address->getExtraInfo();
        $addressOutput['office'] = $address->getOffice();
        $addressOutput['flat'] = $address->getFlat();
        $addressOutput['work_time_from'] = $address->getWorkTimeFrom();
        $addressOutput['dinner_time_from'] = $address->getDinnerTimeFrom();
        $addressOutput['dinner_time_to'] = $address->getDinnerTimeTo();
        $addressOutput['contact_fio'] = $address->getContactFio();
        $addressOutput['contact_email'] = $address->getContactEmail();
        $addressOutput['contact_phone'] = $address->getContactPhone();
        $addressOutput['instructions'] = $address->getInstructions();
        $addressOutput['need_pass'] = $address->isNeedPass();

        $addressCityOutput = $this->getAddressCityOutput($address);
        $addressOutput = array_merge_recursive($addressOutput, $addressCityOutput);

        return $addressOutput;
    }

    public function getTerminalOutputByCode($code, $is_sender = true): array
    {
        /** @var DpdPoint $point */
        $point = $this->deliveryDpdManager->getPointByCode($code);
        if (! $point) {
            return null;
        }
        $city = $point->getCity();
        $schedules = $point->getDpdSchedules();
        $type = $is_sender ? 'SelfPickup' : 'SelfDelivery';
        /** @var DpdSchedule $schedule */
        $schedule = $schedules->filter(function(DpdSchedule $entity) use ($type){
            return $entity->getType() === $type;
        })->first();
        $timetables = $schedule ? $schedule->getDpdTimetables() : [];

        $terminalOutput = [];
        $terminalOutput['code'] = $point->getCode();
        $terminalOutput['name'] = $point->getName();
        $terminalOutput['street'] = $point->getStreet();
        $terminalOutput['street_abr'] = $point->getStreetAbr();
        $terminalOutput['ownership'] = $point->getOwnership();
        $terminalOutput['house'] = $point->getHouse();
        $terminalOutput['structure'] = $point->getStructure();
        $terminalOutput['description'] = $point->getDescription();
        $terminalOutput['latitude'] = $point->getLatitude();
        $terminalOutput['longitude'] = $point->getLongitude();
        $terminalOutput['city_name'] = $city->getCityName();
        $terminalOutput['region_name'] = $city->getRegionName();
        $terminalOutput['country_name'] = $city->getCountryName();
        $terminalOutput['full_city_name'] = $city->getFullCityName();
        $terminalOutput['city_abr'] = $city->getAbbreviation();
        $terminalOutput['times'] = $this->deliveryDpdManager->getTimetableCollectionForOutput($timetables);
        $terminalOutput['limits'] = $this->deliveryDpdManager->getLimitsForOutput($point->getDpdParcelShopLimits());

        return $terminalOutput;
    }

    /**
     * @param DpdDeliveryAddress $address
     * @return array
     */
    public function getAddressCityOutput(DpdDeliveryAddress $address): array
    {
        $addressCityOutput = [];

        $city_id = $address->getCityId();
        /** @var DpdCity $city */
        $city = $this->deliveryDpdManager->getCityByCityId($city_id);
        if ($city) {
            $addressCityOutput['full_city_name'] = $city->getFullCityName();
        }
        return $addressCityOutput;
    }

    /**
     * @param $parcels
     * @return array
     */
    public function getParcelCollectionForOutput($parcels): array
    {
        $parcelsOutput = [];
        foreach ($parcels as $parcel) {
            $parcelsOutput[] = $this->getParcelOutput($parcel);
        }

        return $parcelsOutput;
    }

    /**
     * @param DpdDeliveryParcel $parcel
     * @return array
     */
    public function getParcelOutput(DpdDeliveryParcel $parcel): array
    {
        $parcelOutput = [];
        $parcelOutput['id'] = $parcel->getId();
        $parcelOutput['weight'] = (int) $parcel->getWeight();
        $parcelOutput['length'] = $parcel->getLength();
        $parcelOutput['width'] = $parcel->getWidth();
        $parcelOutput['height'] = $parcel->getHeight();

        return $parcelOutput;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @param array $data
     * @return DpdDeliveryOrder
     * @throws LogicException
     * @throws \Exception
     */
    private function fillDeliveryOrderDpd(DpdDeliveryOrder $deliveryOrderDpd, array $data): DpdDeliveryOrder
    {
        $deliveryOrderDpd->setOrderNumber($data['order_number'] ?? null);
        $deliveryOrderDpd->setPickupTimePeriod($data['pickup_time_period'] ?? null);
        $deliveryOrderDpd->setDeliveryTimePeriod($data['delivery_time_period'] ?? null);
        $deliveryOrderDpd->setServiceCode($data['service_code'] ?? null);
        if(isset($data['declared_value'])) {
            $deliveryOrderDpd->setDeclaredValue($data['declared_value']);
        }
        $deliveryOrderDpd->setCargoCategory($data['cargo_category'] ?? null);
        $deliveryOrderDpd->setPaymentType($data['payment_type'] ?? null);
        // Все грузы ценные
        $deliveryOrderDpd->setIsCargoValuable(1);

        $date_pickup = isset($data['date_pickup']) ? \DateTime::createFromFormat('Y-m-d', $data['date_pickup']) : null;
        $deliveryOrderDpd->setDatePickup($date_pickup);

        if (isset($data['sender_address']) || isset($data['receiver_address'])) {
            $deliveryOrderDpd = $this->manageAddresses($deliveryOrderDpd, $data);
        }

        if (isset($data['parcels'])) {
            $deliveryOrderDpd = $this->manageParcels($deliveryOrderDpd, $data['parcels']);
        }

        return $deliveryOrderDpd;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @param array $parcels_data
     * @return DpdDeliveryOrder
     * @throws LogicException
     * @throws \Exception
     */
    private function manageParcels(DpdDeliveryOrder $deliveryOrderDpd, array $parcels_data): DpdDeliveryOrder
    {
        /** @var DpdDeliveryParcel $parcel */
        foreach ($parcels_data as $parcel_data) {
            if (isset($parcel_data['id'])) {
                $this->updateDeliveryOrderDpdParcel($deliveryOrderDpd, $parcel_data);
            } else {
                /** @var DpdDeliveryParcel $deliveryOrderDpdParcel */
                $deliveryOrderDpdParcel = $this->createDeliveryOrderDpdParcel($parcel_data, $deliveryOrderDpd);
                $deliveryOrderDpd->addParcel($deliveryOrderDpdParcel);
            }
        }

        return $deliveryOrderDpd;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @param array $data
     * @return DpdDeliveryOrder
     * @throws \Exception
     */
    private function manageAddresses(DpdDeliveryOrder $deliveryOrderDpd, array $data): DpdDeliveryOrder
    {
        if (isset($data['sender_address'])) {
            $sender_city_id = $data['sender_address']['city_id'];
            /** @var DpdCity $dpdCity */
            $dpdCity = $this->deliveryDpdManager->getCityByCityId($sender_city_id);

            $data['sender_address']['country_name'] = $dpdCity->getCountryName();
            $data['sender_address']['city'] = $dpdCity->getCityName();
            $data['sender_address']['region'] = $dpdCity->getRegionName();

            /** @var DpdDeliveryAddress $senderAddress */
            $senderAddress = $deliveryOrderDpd->getSenderAddress();
            if (null === $senderAddress) {
                /** @var DpdDeliveryAddress $dpdDeliveryAddress */
                $dpdDeliveryAddress = $this->createDeliveryOrderDpdAddress($data['sender_address']);
                $deliveryOrderDpd->setSenderAddress($dpdDeliveryAddress);
            } else {
                $this->updateDeliveryOrderDpdAddress($senderAddress, $data['sender_address']);
            }
        }

        if (isset($data['receiver_address'])) {
            $receiver_city_id = $data['receiver_address']['city_id'];
            /** @var DpdCity $dpdCity */
            $dpdCity = $this->deliveryDpdManager->getCityByCityId($receiver_city_id);

            $data['receiver_address']['country_name'] = $dpdCity->getCountryName();
            $data['receiver_address']['city'] = $dpdCity->getCityName();
            $data['receiver_address']['region'] = $dpdCity->getRegionName();

            /** @var DpdDeliveryAddress $receiverAddress */
            $receiverAddress = $deliveryOrderDpd->getReceiverAddress();
            if (null === $receiverAddress) {
                /** @var DpdDeliveryAddress $deliveryOrderDpdAddress */
                $dpdDeliveryAddress = $this->createDeliveryOrderDpdAddress($data['receiver_address']);
                $deliveryOrderDpd->setReceiverAddress($dpdDeliveryAddress);
            } else {
                $this->updateDeliveryOrderDpdAddress($receiverAddress, $data['receiver_address']);
            }
        }

        return $deliveryOrderDpd;
    }

    /**
     * @param array $data
     * @return DpdDeliveryAddress
     * @throws \Exception
     */
    private function createDeliveryOrderDpdAddress(array $data): DpdDeliveryAddress
    {
        $address = new DpdDeliveryAddress();
        // Наполнение данными
        $address = $this->fillDeliveryOrderDpdAddress($address, $data);

        $this->entityManager->persist($address);
        $this->entityManager->flush($address);

        return $address;
    }

    /**
     * @param DpdDeliveryAddress $address
     * @param array $data
     * @return DpdDeliveryAddress
     * @throws \Exception
     */
    private function updateDeliveryOrderDpdAddress(DpdDeliveryAddress $address, array $data): DpdDeliveryAddress
    {
        $address = $this->fillDeliveryOrderDpdAddress($address, $data);

        $this->entityManager->persist($address);
        $this->entityManager->flush($address);

        return $address;
    }

    /**
     * @param DpdDeliveryAddress $address
     * @param array $data
     * @return DpdDeliveryAddress
     */
    private function fillDeliveryOrderDpdAddress(DpdDeliveryAddress $address, array $data): DpdDeliveryAddress
    {
        $name = $data['name'] ?? null;
        if (!$name) {
            $name = $data['contact_fio'] ?? null;
        }
        $address->setName($name);
        $address->setTerminalCode($data['terminal_code'] ?? null);
        $address->setIndex($data['index'] ?? null);
        $address->setCityId($data['city_id']);
        $address->setStreet($data['street']);
        $address->setStreetAbr($data['street_abr'] ?? null);
        $address->setHouse($data['house'] ?? null);
        $address->setHouseKorpus($data['house_korpus'] ?? null);
        $address->setStr($data['str'] ?? null);
        $address->setVlad($data['vlad'] ?? null);
        $address->setExtraInfo($data['extra_info'] ?? null);
        $address->setOffice($data['office'] ?? null);
        $address->setFlat($data['flat'] ?? null);
        $address->setWorkTimeFrom($data['work_time_from'] ?? null);
        $address->setWorkTimeTo($data['work_time_to'] ?? null);
        $address->setDinnerTimeFrom($data['dinner_time_from'] ?? null);
        $address->setDinnerTimeTo($data['dinner_time_to'] ?? null);
        $address->setContactFio($data['contact_fio'] ?? null);
        $address->setContactPhone($data['contact_phone'] ?? null);
        $address->setContactEmail($data['contact_email'] ?? null);
        $address->setInstructions($data['instructions'] ?? null);
        $address->setNeedPass($data['need_pass'] ?? null);

        return $address;
    }

    /**
     * @param array $data
     * @param DpdDeliveryOrder $dpdDeliveryOrder
     * @return DpdDeliveryParcel
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createDeliveryOrderDpdParcel(array $data, DpdDeliveryOrder  $dpdDeliveryOrder): DpdDeliveryParcel
    {
        $dpdDeliveryParcel = new DpdDeliveryParcel();
        // Наполнение данными
        $dpdDeliveryParcel = $this->fillDeliveryOrderDpdParcel($dpdDeliveryParcel, $data);
        $dpdDeliveryParcel->setDpdDeliveryOrder($dpdDeliveryOrder);

        $this->entityManager->persist($dpdDeliveryParcel);
        $this->entityManager->flush($dpdDeliveryParcel);

        return $dpdDeliveryParcel;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @param array $data
     * @return DpdDeliveryParcel
     * @throws LogicException
     * @throws \Exception
     */
    private function updateDeliveryOrderDpdParcel(DpdDeliveryOrder $deliveryOrderDpd, array $data): DpdDeliveryParcel
    {
        /** @var DpdDeliveryParcel $deliveryOrderDpdParcel */
        $deliveryOrderDpdParcel = $this->getDpdParcelByIdAndDeliveryOrder($data['id'], $deliveryOrderDpd);

        if (null === $deliveryOrderDpdParcel) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_PARCEL_DOES_NOT_BELONGS_TO_SPECIFIED_DELIVERY_ORDER);
        }

        $dpdDeliveryParcel = $this->fillDeliveryOrderDpdParcel($deliveryOrderDpdParcel, $data);

        $this->entityManager->persist($dpdDeliveryParcel);
        $this->entityManager->flush($dpdDeliveryParcel);

        return $dpdDeliveryParcel;
    }

    /**
     * @param DpdDeliveryParcel $parcel
     * @param array $data
     * @return DpdDeliveryParcel
     */
    private function fillDeliveryOrderDpdParcel(DpdDeliveryParcel $parcel, array $data): DpdDeliveryParcel
    {
        $weight = isset($data['weight']) && !empty($data['weight']) ? (float) $data['weight'] : null;
        $length = isset($data['length']) && !empty($data['length']) ? (float) $data['length'] : null;
        $width = isset($data['width']) && !empty($data['width']) ? (float) $data['width'] : null;
        $height = isset($data['height']) && !empty($data['height']) ? (float) $data['height'] : null;

        $parcel->setWeight($weight);
        $parcel->setLength($length);
        $parcel->setWidth($width);
        $parcel->setHeight($height);

        return $parcel;
    }

    /**
     * @param int $id
     * @return null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    private function getDeliveryOrderDpdParcel(int $id)
    {
        return $this->entityManager->find(DpdDeliveryParcel::class, $id);
    }

    /**
     * @param int $id
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @return null|object
     */
    private function getDpdParcelByIdAndDeliveryOrder(int $id, DpdDeliveryOrder $deliveryOrderDpd)
    {
        return $this->entityManager->getRepository(DpdDeliveryParcel::class)
            ->findOneBy(['id' => $id, 'dpdDeliveryOrder' => $deliveryOrderDpd]);
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @return bool
     */
    public function isAllowedCreateDpdDeliveryOrder(DpdDeliveryOrder $deliveryOrderDpd): bool
    {
        return $deliveryOrderDpd &&
            (
                !$deliveryOrderDpd->getDpdOrderCreationStatus() ||
                $deliveryOrderDpd->getDpdOrderCreationStatus() === DpdDeliveryOrder::ERROR
            );
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @param \DateTime $datePickup
     * @return DpdDeliveryOrder
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setDatePickupToDpdDeliveryOrder(DpdDeliveryOrder $deliveryOrderDpd, \DateTime $datePickup): DpdDeliveryOrder
    {
        #$date_pickup = isset($data['date_pickup']) ? \DateTime::createFromFormat('Y-m-d', $data['date_pickup']) : null;
        $deliveryOrderDpd->setDatePickup($datePickup);

        $this->entityManager->persist($deliveryOrderDpd);
        $this->entityManager->flush($deliveryOrderDpd);

        return $deliveryOrderDpd;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @param \DateTime $selectedDate
     * @param string $schedule_type
     * @return array|null
     */
    public function pointScheduleChecking(DpdDeliveryOrder $deliveryOrderDpd, \DateTime $selectedDate, string $schedule_type)
    {
        // от 0 (воскресенье) до 6 (суббота)
        $selectedDate_week_day_number = date('w', $selectedDate->getTimestamp());

        /** @var DpdDeliveryAddress $senderAddress */
        $senderAddress = $deliveryOrderDpd->getSenderAddress();

        $point_code = $senderAddress->getTerminalCode();
        if (null === $point_code) {

            return ['dpd_address_data' => [
                'requiredData' => 'Point code not specified.'
            ]];
        }

        /** @var DpdPoint $dpdPoint */
        $dpdPoint = $this->deliveryDpdManager->getPointByCode($point_code);
        if (null === $dpdPoint) {

            return ['dpd_point_data' => [
                'requiredData' => 'There is no point in the system with the specified code.'
            ]];
        }

        $dpdSchedules = $dpdPoint->getDpdSchedules();
        $checkingSchedule = null;
        /** @var DpdSchedule $dpdSchedule */
        foreach ($dpdSchedules as $dpdSchedule) {
            if ($dpdSchedule->getType() === $schedule_type) {
                $checkingSchedule = $dpdSchedule;
            }
        }
        if (null === $checkingSchedule) {

            return ['dpd_schedule_data' => [
                'requiredData' => 'Selected point does not provide '. $schedule_type .' service.'
            ]];
        }

        /** @var Collection $dpdTimetables */
        $dpdTimetables = $checkingSchedule->getDpdTimetables();
        if ($dpdTimetables->count() > 0 && $this->isSelectedDateDayOff($dpdTimetables, $selectedDate_week_day_number)) {

            return ['dpd_date' => [
                'wrongDate' => 'On the selected date '. $schedule_type .' service can not be provided. Selected point is not working on this day.'
            ]];
        }

        return null;
    }

    /**
     * @param Collection $dpdTimetables
     * @param int $selectedDate_week_day_number
     * @return bool
     */
    private function isSelectedDateDayOff(Collection $dpdTimetables, int $selectedDate_week_day_number)
    {
        /** @var DpdTimetable $dpdTimetable */
        foreach ($dpdTimetables as $dpdTimetable) {
            if ($dpdTimetable->getWorkTime() === $this->deliveryDpdManager::DAY_OFF
                && $this->pointWeekDaysContainsSelectedDay($dpdTimetable->getWeekDays(), DeliveryDpdManager::WEEK_DAYS[$selectedDate_week_day_number])) {

                return true;
            }
        }

        return false;
    }

    /**
     * @param string $point_week_days
     * @param string $selected_day
     * @return bool
     */
    private function pointWeekDaysContainsSelectedDay(string $point_week_days, string $selected_day)
    {
        return strpos($point_week_days, $selected_day) !== false;
    }



    /*
     * Описание типа address:
     *
     * code // строка / необязательный / 1234
     *      Код адреса в информационных системах заказчика и DPD. Адрес с кодом должен быть передан в DPD отдельно
     * name // строка / обязательный / Иванов Сергей Петрович
     *      Название отправителя/получателя.
     *      В случае, когда адрес приёма/доставки – это магазин, филиал компании, дилерский центр и т.п., в эту строку пишется его название.
     *      Если доставка осуществляется физическому лицу, то пишется Ф.И.О получателя.
     * terminalCode // Код терминала. / строка / обязательный / M13
     *      Данное поле является обязательным для вариантов перевозки «ДТ», «ТД» или «ТТ» в методе createOrder.
     *      Если указывается данное поле, то адрес указывать не требуется.
     *      При использовании метода createAddress или updateAddress, данного параметра нет.
     * addressString // строка / необязательный / 16, Grafton Street, Dublin, Ireland
     *      Строка адреса для международных отправок (не для России и стран ТС)
     * countryName // Название страны / строка / обязательный / Россия
     * index // Индекс / строка / необязательный / 140012
     * region // Регион / строка / необязательный / Московская обл.
     * city // Город / строка / обязательный / Люберцы
     * street // Улица (формат ФИАС) / строка / обязательный / Авиаторов
     * streetAbbr // Сокращения типа улицы (ул, пр-т, б-р и т.д.) / строка / необязательный / ул
     * house // Дом / строка / необязательный / 1
     * houseKorpus // Корпус / строка / необязательный / А
     * str // Строение / строка / необязательный / 1
     * vlad // Владение / строка / необязательный / 1
     * extraInfo // Доп. Информация / строка / необязательный / Пав. 1 (max 90 символов)
     * office // Офис / строка / необязательный / 12 (только цифры или цифры с буквами 12Б)
     * flat // Квартира / с трока / необязательный / 144 (только цифры или цифры с буквами 144А)
     * workTimeFrom // Время работы от / строка / необязательный / 9:00
     * workTimeTo // Время работы до / строка / необязательный / 21:00
     * dinnerTimeFrom // Время обеда от / строка / необязательный / 14:00
     * dinnerTimeTo // Время обеда до / строка / необязательный / 15:00
     * contactFio // Контактное лицо / строка / обязательный / Смирнов Игорь Николаевич
     * contactPhone // Контактный телефон / строка / обязательный / 89165555555
     * contactEmail // Контактный e-mail / строка / необязательный / smirnov@megashop.ru
     * instructions // Инструкции для курьера / строка / необязательный / Подъезд со стороны ул. Кирова
     * needPass // Требуется пропуск / строка / необязательный / 1
     */

    /*
     * Описание типа parcel:
     *
     * number // Номер посылки в информационной системе клиента (номер штрих-кода посылки) / строка / обязательный / 1001000998
     * box_needed* // Требуется обрешётка / целое число / необязательное / 1                - пока не используется нами
     *       1 - требуется обрешётка данной посылки 0 – не требуется.
     *       При этом необходимо передать отдельно опцию ОБР
     * weight // Вес посылки, кг / число / необязательное / 5
     * length // Длина посылки, см / число / необязательное / 50
     * width // Ширина посылки,см / число / необязательное / 20
     * height // Высота посылки, см / число / необязательное / 20
     * number_for_print // Клиентский ШК для печати / число / необязательное / 1000         - пока не используется нами
     * insuranceCost // Страховая стоимость посылки / число / необязательное / 120          - пока не используется нами
     * insuranceCostVat // НДС страх. стоимости / число / необязательное / 10               - пока не используется нами
     * codAmount // Сумма к получению от покупателя / число / необязательное / 100          - пока не используется нами
     */

    /*
     * Опции - см. параграф 3.12. документации по DPD                                       - пока не используется нами
     */
}