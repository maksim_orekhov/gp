<?php
namespace ModuleDeliveryDpd\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ModuleDelivery\Entity\DeliveryOrder;
use ModuleDeliveryDpd\Entity\DpdDeliveryOrder;
use ModuleDeliveryDpd\Entity\DpdDeliveryAddress;
use ModuleDeliveryDpd\Entity\DpdDeliveryParcel;
use ModuleDeliveryDpd\Entity\DpdCity;
use Doctrine\ORM\EntityManager;
use ModuleDeliveryDpd\Entity\DpdParcelShopLimits;
use ModuleDeliveryDpd\Entity\DpdParcelShopState;
use ModuleDeliveryDpd\Entity\DpdParcelShopType;
use ModuleDeliveryDpd\Entity\DpdPoint;
use ModuleDeliveryDpd\Entity\DpdSchedule;
use ModuleDeliveryDpd\Entity\DpdTimetable;
use ModuleDeliveryDpd\Form\DeliveryOrderDpdCostForm;
use SoapClient;
use Core\Exception\LogicException;

/**
 * Class DeliveryDpdManager
 * @package ModuleDeliveryDpd\Service
 */
class DeliveryDpdManager
{
    const REGION_NAMES_RU = [
        '1' => 'Адыгея',
        '2' => 'Башкортостан',
        '3' => 'Бурятия',
        '4' => 'Алтай',
        '5' => 'Дагестан',
        '6' => 'Ингушетия',
        '7' => 'Кабардино-Балкария',
        '8' => 'Калмыкия',
        '9' => 'Карачаево-Черкессия',
        '10' => 'Карелия',
        '11' => 'Коми',
        '12' => 'Марий Эл',
        '13' => 'Мордовия',
        '14' =>	'Саха (Якутия)',
        '15' => 'Северная Осетия — Алания',
        '16' => 'Татарстан',
        '17' => 'Тыва',
        '18' => 'Удмуртия',
        '19' => 'Хакасия',
        '20' => 'Чеченская Республика',
        '21' => 'Чувашия',
        '22' => 'Алтайский край',
        '23' => 'Краснодарский край',
        '24' => 'Красноярский край',
        '25' => 'Приморский край',
        '26' => 'Ставропольский край',
        '27' => 'Хабаровский край',
        '28' => 'Амурская обл.',
        '29' => 'Архангельская обл.',
        '30' => 'Астраханская обл.',
        '31' => 'Белгородская обл.',
        '32' => 'Брянская обл.',
        '33' => 'Владимирская обл.',
        '34' => 'Волгоградская обл.',
        '35' => 'Вологодская обл.',
        '36' => 'Воронежская обл.',
        '37' => 'Ивановская обл.',
        '38' => 'Иркутская обл.',
        '39' => 'Калининградская обл.',
        '40' => 'Калужская обл.',
        '41' => 'Камчатский край',
        '42' => 'Кемеровская обл.',
        '43' => 'Кировская обл.',
        '44' => 'Костромская обл.',
        '45' => 'Курганская обл.',
        '46' => 'Курская обл.',
        '47' => 'Ленинградская обл.',
        '48' =>	'Липецкая обл.',
        '49' => 'Магаданская обл.',
        '50' => 'Московская обл.',
        '51' => 'Мурманская обл.',
        '52' => 'Нижегородская обл.',
        '53' => 'Новгородская обл.',
        '54' => 'Новосибирская обл.',
        '55' => 'Омская обл.',
        '56' => 'Оренбургская обл.',
        '57' => 'Орловская обл.',
        '58' => 'Пензенская обл.',
        '59' => 'Пермский край',
        '60' =>	'Псковская обл.',
        '61' => 'Ростовская обл.',
        '62' => 'Рязанская обл.',
        '63' => 'Самарская обл.',
        '64' => 'Саратовская обл.',
        '65' => 'Сахалинская обл.',
        '66' => 'Свердловская обл.',
        '67' => 'Смоленская обл.',
        '68' => 'Тамбовская обл.',
        '69' => 'Тверская обл.',
        '70' => 'Томская обл.',
        '71' => 'Тульская обл.',
        '72' => 'Тюменская обл.',
        '73' => 'Ульяновская обл.',
        '74' => 'Челябинская обл.',
        '75' => 'Забайкальский край',
        '76' => 'Ярославская обл.',
        '77' => 'г. Москва',
        '78' => 'г. Санкт-Петербург',
        '79' => 'Еврейская автономная обл.',
        '83' => 'Ненецкий автономный округ',
        '86' => 'Ханты-Мансийский автономный округ — Югра',
        '87' => 'Чукотский автономный округ',
        '89' => 'Ямало-Ненецкий автономный округ',
        '91' => 'Крым',
        '92' => 'г. Севастополь',
    ];

    // Возращает общую стоимость доставки по России
    const REQUEST_METHOD_FOR_DPD_SERVICE_COST = 'getServiceCost2';
    // Возращает стоимость доставки по параметрам посылок
    const REQUEST_METHOD_FOR_DPD_SERVICE_COST_BY_PARCELS = 'getServiceCostByParcels2';
    // Создание заказа на доставку
    const REQUEST_METHOD_FOR_DPD_ORDER_CREATION = 'createOrder';
    // Создать адрес с кодом
    const REQUEST_METHOD_FOR_DPD_ADDRESS_CREATION = 'createAddress';
    // Изменить адреса с кодом
    const REQUEST_METHOD_FOR_DPD_ADDRESS_UPDATING = 'updateAddress';
    // Статус заказа
    const REQUEST_METHOD_FOR_DPD_ORDER_STATUS = 'getOrderStatus';

    // Услуги DPD
    const SERVICE_CODES = [
        'BZP' => 'DPD 18:00',
        'ECN' => 'DPD ECONOMY',
        'CUR' => 'DPD CLASSIC domestic',
        'NDY' => 'DPD EXPRESS',
        'CSM' => 'DPD Online Express',
        'PCL' => 'DPD Online Classic',
        'DPI' => 'DPD CLASSIC international IMPORT',
        'DPE' => 'DPD CLASSIC international EXPORT',
        'MAX' => 'DPD MAX domestic',
        'MXO' => 'DPD Online Max',
        'ECU' => 'DPD ECONOMY CU',
        'PUP' => 'DPD Shop to Shop',    // нет в документации, но есть в ответах DPD
        'IND' => 'DPD EXPRESS 13:00'    // нет в документации, но есть в ответах DPD
    ];

    const DPD_PENDING_CREATION_STATUS = 'OrderPending';

    const DPD_ORDER_CREATION_STATUSES = [
        DpdDeliveryOrder::OK => 'Заказ на доставку успешно создан',
        DpdDeliveryOrder::PENDING => 'Заказ на доставку принят, но нуждается в ручной доработке сотрудником DPD',
        DpdDeliveryOrder::DUPLICATE => 'Заказ на доставку не может быть принят',
        DpdDeliveryOrder::ERROR => 'Заказ на доставку не может быть создан',
    ];

    const DEFAULT_CITY_DATA = [
        'city_name' => 'Москва',
        'region_code' => 77
    ];

    const WEEK_DAYS = [0 => 'Вс', 1 => 'Пн', 2 => 'Вт', 3 => 'Ср', 4 => 'Чт', 5 => 'Пт', 6 => 'Сб'];

    // Обозначение выходного дня в DpdTimetable
    const DAY_OFF = 'выходной';


    /**
     * @var string
     */
    private $account;

    /**
     * @var string
     */
    private $secret_key;

    /**
     * @var string
     */
    private $gateway_host;

    /**
     * @var string
     */
    private $trace_page_url;

    /**
     * @var string
     */
    private $url_for_cost_calculation;

    /**
     * @var string
     */
    private $url_for_order;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * DeliveryDpdManager constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, array $config)
    {
        $this->entityManager = $entityManager;

        $this->account          = $config['delivery_dpd_settings']['account'];
        $this->secret_key       = $config['delivery_dpd_settings']['secret_key'];
        $this->gateway_host     = $config['delivery_dpd_settings']['gateway_host'];
        $this->trace_page_url   = $config['delivery_dpd_settings']['trace_page_url'];

        $this->url_for_cost_calculation = $this->gateway_host.'calculator2?wsdl';
        $this->url_for_order = $this->gateway_host.'order2?wsdl';
    }

    /**
     * @return array
     */
    public static function getRussiaRegions(): array
    {
        $regions = [];
        foreach (self::REGION_NAMES_RU as $key=>$value) {
            $regions[$key] = $value;
        }

        return $regions;
    }

    /**
     * @return string
     */
    public function getServiceTracePageUrl()
    {
        return $this->trace_page_url;
    }

    /**
     * @param $id
     * @return DpdCity|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function getCity(int $id)
    {
        /** @var DpdCity $city */ // Get City by Id
        $city = $this->entityManager->find(DpdCity::class, $id);

        return $city;
    }

    /**
     * @param string $name
     * @return DpdCity|null
     */
    public function getCityByName(string $name)
    {
        /** @var DpdCity $city */
        $city = $this->entityManager->getRepository(DpdCity::class)
            ->findOneBy(['cityName' => $name]);

        return $city;
    }

    /**
     * @return DpdCity|null
     */
    public function getCityMoscow()
    {
        /** @var DpdCity $city */
        $city = $this->entityManager->getRepository(DpdCity::class)
                ->findOneBy(['cityName' => 'Москва', 'abbreviation' => 'г', 'regionCode' => '77']);

        return $city;
    }

    /**
     * @param int $city_id
     * @return DpdCity|null
     */
    public function getCityByCityId(int $city_id)
    {
        /** @var DpdCity $city */
        $city = $this->entityManager->getRepository(DpdCity::class)
            ->findOneBy(['cityId' => $city_id]);

        return $city;
    }

    /**
     * @param DpdCity $city
     * @param string $schedule_type
     * @return mixed
     */
    public function getCityDpdPointsWithSpecificType(DpdCity $city, string $schedule_type)
    {
        return $this->entityManager->getRepository(DpdPoint::class)
            ->getPointsForCity($city, $schedule_type);
    }

    /**
     * @param string $code
     * @return DpdPoint|null
     */
    public function getPointByCode(string $code)
    {
        /** @var DpdPoint $point */
        $point = $this->entityManager->getRepository(DpdPoint::class)
            ->findOneBy(['code' => $code]);

        return $point;
    }

    /**
     * @return array
     */
    public function getAllCities(): array
    {
        return $this->entityManager
            ->getRepository(DpdCity::class)->findAll();
    }

    /**
     * @param int $limit
     * @return mixed
     * @throws \Exception
     */
    public function getBiggestCities($limit = 20)
    {
        // Выбираем самые крупные города, оринетируясь по количеству пунктов в них
        $cities = $this->entityManager->getRepository(DpdCity::class)
            ->getBiggestCities($limit);

        return $cities;
    }

    /**
     * @param null $search
     * @param int $limit
     * @return mixed
     */
    public function getAutoCompleteCities($search = null, $limit = 20)
    {
        $cities = $this->entityManager->getRepository(DpdCity::class)
            ->getAutoCompleteCities($search, $limit);

        return $cities;
    }

    /**
     * @param $search
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getCitiesBySearchRequest($search)
    {
        $cities = $this->entityManager->getRepository(DpdCity::class)
            ->gitCities($search);

        return $cities;
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getNumberOfDpdCities()
    {
        return $this->entityManager
            ->getRepository(DpdCity::class)->countAll();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function getNumberOfDpdPoints()
    {
        return $this->entityManager
            ->getRepository(DpdPoint::class)->countAll();
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @param array $postData
     * @return mixed
     * @throws LogicException
     */
    public function getServiceCost(DpdDeliveryOrder $deliveryOrderDpd, array $postData)
    {
        $preparedData = $this->getDataForCostQuery($deliveryOrderDpd);
        $preparedData['csrf'] = $postData['csrf'];

        /** @var DeliveryOrderDpdCostForm $orderDpdCostForm */
        $orderDpdCostForm = new DeliveryOrderDpdCostForm();

        $orderDpdCostForm->setData($preparedData);

        if (!$orderDpdCostForm->isValid()) {

            throw new LogicException($orderDpdCostForm->getMessages());
        }

        $data = $orderDpdCostForm->getData();

        return $this->getDeliveryDpdCost($data, $preparedData['parcels']);
    }

    /**
     * Метод используем для предварительной оценки стоимости,
     * а также для выясниния досутпна ли доставки(забор) посылки в указанный населенный пункт
     *
     * @param array $data
     * @return mixed
     */
    public function getServiceCostByCityData(array $data)
    {
        $costData['self_pickup'] = $data['self_pickup'] ?? 0;
        $costData['self_delivery'] = 0;
        $costData['weight'] = $data['weight'] ?? 1.00;
        $costData['pickup_city_id'] = $data['sender_city_id'];
        $costData['delivery_city_id'] = $data['receiver_city_id'];
        $costData['service_code'] = 'PCL';

        try {
            return $this->getDeliveryDpdCost($costData);
        }
        catch (\Throwable $t) {
            return [];
        }
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @return array
     */
    public function getDataForCostQuery(DpdDeliveryOrder $deliveryOrderDpd): array
    {
        $data = [];
        /** @var DpdDeliveryAddress $senderAddress */
        $senderAddress = $deliveryOrderDpd->getSenderAddress();
        /** @var DpdDeliveryAddress $receiverAddress */
        $receiverAddress = $deliveryOrderDpd->getReceiverAddress();

        // pickup
        $data['pickup_city_id'] = $senderAddress->getCityId();
        $data['pickup_index']   = null;
        $data['pickup_city_name'] = null;
        $data['pickup_region_code'] = null;
        $data['pickup_country_code'] = null;
        // delivery
        $data['delivery_city_id'] = $receiverAddress->getCityId();
        $data['delivery_index'] = null;
        $data['delivery_city_name'] = null;
        $data['delivery_region_code'] = null;
        $data['delivery_country_code'] = null;

        $data['self_pickup']     = null === $senderAddress->getTerminalCode() ? 1 : 0;
        $data['self_delivery']   = null === $receiverAddress->getTerminalCode() ? 1 : 0;

        $data['service_code']    = $deliveryOrderDpd->getServiceCode();
        $data['pickup_date']     = null;
        if ($deliveryOrderDpd->getDatePickup() instanceof \DateTime) {
            $data['pickup_date'] = $deliveryOrderDpd->getDatePickup()->format('Y-m-d');
        }
        $data['declared_value']  = $deliveryOrderDpd->getDeclaredValue();

        $data['weight']  = null;
        $data['parcel'] = null;
        // parcel
        /** @var ArrayCollection $parcels */
        $parcels = $deliveryOrderDpd->getParcels();
        if ($parcels->count() > 0) {
            $data['weight']  = 0;
            $data['parcels'] = [];
            /** @var DpdDeliveryParcel $parcel */
            foreach ($parcels as $parcel) {
                // Формируем массив посылки и добавляем в общий массив мпасылок
                $parcel_array = [];
                $parcel_array['weight'] = $parcel->getWeight();
                $parcel_array['length'] = $parcel->getLength();
                $parcel_array['width'] = $parcel->getWidth();
                $parcel_array['height'] = $parcel->getHeight();
                $parcel_array['quantity'] = 1;
                $data['parcels'][] = $parcel_array;
                // Считаем общий вес отправки
                $data['weight'] += $parcel->getWeight();
            }
        }

        return $data;
    }

    /**
     * @param array $data
     * @param array|null $parcels
     * @return mixed
     * @throws LogicException
     */
    public function getDeliveryDpdCost(array $data, array $parcels = null)
    {
        #logMessage($data);

        $wsdl_url = $this->url_for_cost_calculation;

        $arguments = [];
        $arguments['request'] = [];
        $arguments['request']['auth'] = $this->getAuthData();

        // pickup
        $pickup = [];
        $pickup['cityId']      = $data['pickup_city_id'] ?? null;
        $pickup['index']       = $data['pickup_index'] ?? null;
        $pickup['cityName']    = $data['pickup_city_name'] ?? null;
        $pickup['regionCode']  = $data['pickup_region_code'] ?? null;
        $pickup['countryCode'] = $data['pickup_country_code'] ?? null;
        $arguments['request']['pickup'] = $pickup;
        // delivery
        $delivery = [];
        $delivery['cityId']        = $data['delivery_city_id'] ?? null;
        $delivery['index']         = $data['delivery_index'] ?? null;
        $delivery['cityName']      = $data['delivery_city_name'] ?? null;
        $delivery['regionCode']    = $data['delivery_region_code'] ?? null;
        $delivery['countryCode']   = $data['delivery_country_code'] ?? null;
        $arguments['request']['delivery'] = $delivery;

        $arguments['request']['selfPickup']    = $data['self_pickup'] ?? null;
        $arguments['request']['selfDelivery']  = $data['self_delivery'] ?? null;
        $arguments['request']['weight']        = $data['weight'] ?? null;
        $arguments['request']['volume']        = $data['volume'] ?? null;
        $arguments['request']['serviceCode']   = $data['service_code'] ?? null;
        $arguments['request']['pickupDate']    = $data['pickup_date'] ?? null;
        $arguments['request']['maxDays']       = $data['max_days'] ?? null;
        $arguments['request']['maxCost']       = $data['max_cost'] ?? null;
        $arguments['request']['declaredValue'] = $data['declared_value'] ?? null;

        if (null !== $parcels) {
            $arguments['request']['parcel'] = $parcels;
        }

        if (isset($arguments['request']['parcels'])) {
            $method = self::REQUEST_METHOD_FOR_DPD_SERVICE_COST_BY_PARCELS;
        } else {
            $method = self::REQUEST_METHOD_FOR_DPD_SERVICE_COST;
        }

        /*
         * Параметры на вход:
         * Внешний тэг: request
         * auth [
         *      clientNumber    // клиентский номер в системе DPD / (номер вашего договора с DPD) / число / 1000000000
         *      clientKey       // уникальный ключ для авторизации, полученный у сотрудника DPD / строка / 1FD890C3556
         * ],
         * pickup [cityId, index, cityName, regionCode, countryCode],
         * delivery [cityId, index, cityName, regionCode, countryCode],
         * selfPickup,      - обязательный
         * selfDelivery,    - обязательный
         * weight,          - обязательный
         * volume,
         * serviceCode,
         * pickupDate,
         * maxDays,
         * maxCost,
         * declaredValue
         */
        $result = $this->doSoapRequest($wsdl_url, $method, $arguments);
        /*
         * Ответ при успешном запросе:
         * serviceСode // Код услуги DPD / строка / ECN
         * serviceName // Название услуги / строка / DPD ECONOMY
         * cost // Стоимость услуги / число / 2,651.46
         * days // Срок доставки, дней / целое число / 2
         * --- weight* // Вес отправки, кг / число / 5  // только для метода getServiceCostInternational
         * --- volume* // Объём, м3 / число / 0.05 // только для метода getServiceCostInternational
         */

        if (null === $result || null === $result->return || empty($result->return)) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_REQUEST_NO_SERVICE_COST_RETURNED);
        }

        return $result->return;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @return null
     * @throws \Throwable
     */
    public function createServiceOrder(DpdDeliveryOrder $deliveryOrderDpd)
    {
        try {
            $creationStatus = $this->createOrderInDpd($deliveryOrderDpd);

            if (null !== $creationStatus && isset($creationStatus->status)) {

                $result = $this->updateDpdOrderCreationStatus($deliveryOrderDpd, $creationStatus);
                if (null !== $result['order_number']) {
                    $this->setDeliveryDpdOrderNumber($deliveryOrderDpd, $result['order_number']);
                }

                return $result;
            }
        }
        catch (\Throwable $t) {

            throw $t;
        }

        return null;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @return null|string
     * @throws LogicException
     */
    public function getIdentifierForTrackingNumber(DpdDeliveryOrder $deliveryOrderDpd)
    {
        $dpd_order_number = $deliveryOrderDpd->getOrderNumber();

        if (null !== $dpd_order_number) {

            return $dpd_order_number;
        }

        throw new LogicException(null, LogicException::DELIVERY_DPD_ORDER_NUMBER_RETURNS_NULL);
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @param string $order_number
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function setDeliveryDpdOrderNumber(DpdDeliveryOrder $deliveryOrderDpd, string $order_number)
    {
        $deliveryOrderDpd->setOrderNumber($order_number);

        $this->entityManager->persist($deliveryOrderDpd);
        $this->entityManager->flush($deliveryOrderDpd);
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @return mixed
     * @throws LogicException
     */
    private function createOrderInDpd(DpdDeliveryOrder $deliveryOrderDpd)
    {
        $wsdl_url = $this->url_for_order;
        $method = self::REQUEST_METHOD_FOR_DPD_ORDER_CREATION;

        $prepared_data = $this->prepareDataForOrderCreation($deliveryOrderDpd);

        $arguments = [];
        $arguments['orders'] = [];
        $arguments['orders']['auth'] = $this->getAuthData();
        // header
        $arguments['orders']['header'] = $prepared_data['header'];
        // order
        $arguments['orders']['order'] = $prepared_data['order'];

        /*
         * Параметры на вход:
         * Внешний тэг: orders
         * auth [
         *      clientNumber    // клиентский номер в системе DPD / (номер вашего договора с DPD) / число / 1000000000
         *      clientKey       // уникальный ключ для авторизации, полученный у сотрудника DPD / строка / 1FD890C3556
         * ],
         * header [
         *      datePickup(обяз.),
         *      payer,
         *      senderAddress(обяз.),
         *      pickupTimePeriod(обяз.),
         *      regularNum
         * ],
         * order [
         *      orderNumberInternal(обяз.),
         *      serviceCode(обяз.),
         *      serviceVariant(обяз.),
         *      cargoNumPack(обяз.),
         *      cargoWeight(обяз.),
         *      cargoVolume,
         *      cargoRegistered(обяз.),
         *      cargoValue,
         *      cargoCategory(обяз.),
         *      deliveryTimePeriod,
         *      paymentType,
         *      extraParam,
         *      dataInt,
         *      receiverAddress(обяз.),
         *      extraService,
         *      parcel,
         *      unitLoad
         * ]
         */
        $result = $this->doSoapRequest($wsdl_url, $method, $arguments);
        /*
         * Параметры ответного сообщения:
         * orderNumberInternal // Номер заказа в информационной системе клиента / строка / 123456 - обязательный
         * orderNum // Номер заказа DPD. Возвращается в ответном сообщении. / строка / 01010001MOW - необязательный
         * status // Статус создания заказа. Возвращается в ответном сообщении. / строка / OK - обязательный (Возможные Статусы создания заказасм. в ниже)
         * errorMessage // Текст ошибки / строка / Не заполнен параметр «Улица» - необязательный
         */

        if (null === $result || null === $result->return || empty($result->return)) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_REQUEST_NO_ORDER_STATUS_RETURNED);
        }

        return $result->return;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @return array
     */
    public function getCurrentDpdOrderCreationStatus(DpdDeliveryOrder $deliveryOrderDpd):array
    {
        return [
            'status' => $deliveryOrderDpd->getDpdOrderCreationStatus(),
            'status_description' => self::DPD_ORDER_CREATION_STATUSES[$deliveryOrderDpd->getDpdOrderCreationStatus()],
            'error_message' => $deliveryOrderDpd->getDpdOrderCreationError()
        ];
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @param \stdClass|null $creationStatus
     * @return mixed
     * @throws LogicException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateDpdOrderCreationStatus(DpdDeliveryOrder $deliveryOrderDpd, \stdClass $creationStatus = null)
    {
        if (null === $creationStatus) {
            $data = [
                'order_number_internal' => $deliveryOrderDpd->getDeliveryOrder()->getOrderNumber()
            ];
            $creationStatus = $this->getDpdOrderCreationStatus($data);
        }

        //@TODO параметры для отладки
        /*$creationStatus->orderNumberInternal = 'GP-5bcdd99cd93ec';
        $creationStatus->orderNum = 'RU123456789';
        $creationStatus->status = 'OK';
        $creationStatus->errorMessage = null;*/

        $deliveryOrderDpd->setDpdOrderCreationStatus($creationStatus->status);
        $order_number = null;
        if (isset($creationStatus->orderNum)) {
            $order_number = $creationStatus->orderNum;
            $deliveryOrderDpd->setOrderNumber($creationStatus->orderNum);
        }

        $error_message = $creationStatus->errorMessage ?? null;
        $deliveryOrderDpd->setDpdOrderCreationError($error_message);

        #$this->entityManager->persist($deliveryOrderDpd);
        $this->entityManager->flush($deliveryOrderDpd);

        return [
            'status' => $creationStatus->status,
            'status_description' => self::DPD_ORDER_CREATION_STATUSES[$creationStatus->status],
            'error_message' => $error_message,
            'order_number' => $order_number,
        ];
    }

    /**
     * @param \stdClass $address
     * @return mixed
     * @throws LogicException
     */
    public function createAddress(\stdClass $address)
    {
        $wsdl_url = $this->url_for_order;
        $method = self::REQUEST_METHOD_FOR_DPD_ADDRESS_CREATION;

        $arguments = [];
        $arguments['address'] = [];
        $arguments['address']['auth'] = $this->getAuthData();
        $arguments['address']['clientAddress'] = $address;

        /*
         * Параметры на вход:
         * Внешний тэг: address
         * auth [
         *      clientNumber    // клиентский номер в системе DPD / (номер вашего договора с DPD) / число / 1000000000
         *      clientKey       // уникальный ключ для авторизации, полученный у сотрудника DPD / строка / 1FD890C3556
         * ],
         * clientAddress (stdClass $address)
         */
        $result = $this->doSoapRequest($wsdl_url, $method, $arguments);
        /*
         * Параметры ответного сообщения:
         * code // Код адреса / строка / обязательный / X333
         * status // Статус / строка / обязательный / Возможные статусы см. ниже в "Статусы создания и изменения адреса"
         * address-error // errorMessage / Описание ошибки / строка / необязательный / Не указана улица
         */

        if (null === $result || null === $result->return || empty($result->return)) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_REQUEST_NO_ADDRESS_CREATION_RESULT_RETURNED);
        }

        return $result->return;
    }

    /**
     * @param \stdClass $address
     * @return mixed
     * @throws LogicException
     */
    public function updateAddress(\stdClass $address)
    {
        $wsdl_url = $this->url_for_order;
        $method = self::REQUEST_METHOD_FOR_DPD_ADDRESS_UPDATING;

        $arguments = [];
        $arguments['address'] = [];
        $arguments['address']['auth'] = $this->getAuthData();
        $arguments['address']['clientAddress'] = $address;

        /*
         * Параметры на вход:
         * Внешний тэг: address
         * auth [
         *      clientNumber    // клиентский номер в системе DPD / (номер вашего договора с DPD) / число / 1000000000
         *      clientKey       // уникальный ключ для авторизации, полученный у сотрудника DPD / строка / 1FD890C3556
         * ],
         * clientAddress (stdClass $address)
         */
        $result = $this->doSoapRequest($wsdl_url, $method, $arguments);
        /*
         * Параметры ответного сообщения:
         * code // Код адреса / строка / обязательный / X333
         * status // Статус / строка / обязательный / Возможные статусы см. ниже в "Статусы создания и изменения адреса"
         * address-error // errorMessage / Описание ошибки / строка / необязательный / Не указана улица
         */

        if (null === $result || null === $result->return || empty($result->return)) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_REQUEST_NO_ADDRESS_UPDATING_RESULT_RETURNED);
        }

        return $result->return;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws LogicException
     */
    public function getDpdOrderCreationStatus(array $data)
    {
        $wsdl_url = $this->url_for_order;
        $method = self::REQUEST_METHOD_FOR_DPD_ORDER_STATUS;

        $arguments = [];
        $arguments['orderStatus'] = [];
        $arguments['orderStatus']['auth'] = $this->getAuthData();
        // order
        $order = [];
        $order['orderNumberInternal'] = $data['order_number_internal'];
        if (isset($data['date_pickup'])) {
            $order['datePickup'] = $data['date_pickup'];
        }
        $arguments['orderStatus']['order'] = $order;

        /*
         * Параметры на вход:
         * Внешний тэг: orderStatus
         * auth [
         *      clientNumber    // клиентский номер в системе DPD / (номер вашего договора с DPD) / число / 1000000000
         *      clientKey       // уникальный ключ для авторизации, полученный у сотрудника DPD / строка / 1FD890C3556
         * ],
         * order [
         *  orderNumberInternal - Номер заказа в информационной системе клиента // Строка / обязательный
         *  datePickup - Дата приёма груза (на тот случай, если номер в вашей информационной системе не является уникальным) // Дата / необязательный
         * ],
         */
        $result = $this->doSoapRequest($wsdl_url, $method, $arguments);
        /*
         * Параметры ответного сообщения:
         * orderNumberInternal // Номер заказа в информационной системе клиента / строка / 123456 - обязательный
         * orderNum // Номер заказа DPD. Возвращается в ответном сообщении. / строка / 01010001MOW - необязательный
         * status // Статус создания заказа. Возвращается в ответном сообщении. / строка / OK - обязательный (Возможные Статусы создания заказасм. в ниже)
         * errorMessage // Текст ошибки / строка / Не заполнен параметр «Улица» - необязательный
         */

        if (null === $result || null === $result->return || empty($result->return)) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_REQUEST_NO_ORDER_STATUS_RETURNED);
        }

        if (null === $result || null === $result->return || empty($result->return)) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_REQUEST_NO_ORDER_STATUS_RETURNED);
        }

        return $result->return;
    }

    /// Outputs and private

    /**
     * @param array $cities
     * @return array
     */
    public function getCityCollectionForOutput(array $cities): array
    {
        $citiesOutput = [];
        foreach ($cities as $city) {
            $citiesOutput[$city->getCityId()] = $this->getCityForOutput($city);
        }

        return $citiesOutput;
    }

    /**
     * @param DpdCity $city
     * @return array
     */
    public function getCityForOutput(DpdCity $city): array
    {
        $cityOutput = [];
        $cityOutput['id'] = $city->getId();
        $cityOutput['city_id'] = $city->getCityId();
        $cityOutput['country_code'] = $city->getCountryCode();
        $cityOutput['country_name'] = $city->getCountryName();
        $cityOutput['region_code'] = $city->getRegionCode();
        $cityOutput['region_name'] = $this->getRegionName($city);
        $cityOutput['city_code'] = $city->getCityCode();
        $cityOutput['city_name'] = $city->getCityName();
        $cityOutput['abbreviation'] = $city->getAbbreviation();
        $cityOutput['index_min'] = $city->getIndexMin();
        $cityOutput['index_max'] = $city->getIndexMax();
        $cityOutput['payment_on_delivery'] = $city->isPaymentOnDelivery();
        $cityOutput['full_city_name'] = $city->getFullCityName();

        return $cityOutput;
    }

    /**
     * @param DpdCity $city
     * @return mixed|string
     */
    public function getRegionName(DpdCity $city)
    {
        if ($city->getCountryCode() === 'RU' && isset(self::REGION_NAMES_RU[$city->getRegionCode()])) {
            $region_name = self::REGION_NAMES_RU[$city->getRegionCode()];
        } else {
            $region_name = $city->getRegionName();
        }

        return $region_name;
    }

    /**
     * @param array $points
     * @return array
     */
    public function getPointCollectionOutput($points)
    {
        $pointsOutput = [];
        foreach ($points as $point) {
            $pointsOutput[$point->getCode()] = $this->getPointForOutput($point);
        }

        return $pointsOutput;
    }

    /**
     * @param DpdPoint $point
     * @return array
     */
    public function getPointForOutput(DpdPoint $point): array
    {
        $pointOutput = [];
        $pointOutput['id'] = $point->getId();
        $pointOutput['name'] = $point->getName();
        $pointOutput['code'] = $point->getCode();
        $pointOutput['cashpay'] = $point->isCashpay();
        $pointOutput['street'] = $point->getStreet();
        $pointOutput['streetAbr'] = $point->getStreetAbr();
        $pointOutput['house'] = $point->getHouse();
        $pointOutput['structure'] = $point->getStructure();
        $pointOutput['ownership'] = $point->getOwnership();
        $pointOutput['description'] = $point->getDescription();
        $pointOutput['latitude'] = $point->getLatitude();
        $pointOutput['longitude'] = $point->getLongitude();
        /** @var DpdParcelShopType $shopType */
        $shopType = $point->getDpdParcelShopType();
        $pointOutput['type'] = $shopType->getCode();
        /** @var DpdParcelShopState $shopState */
        $shopState = $point->getDpdParcelShopState();
        $pointOutput['state'] = null;
        if (null !== $shopState) {
            $pointOutput['state'] = $shopState->getCode();
        }
        /** @var DpdParcelShopLimits $limits */
        $limits = $point->getDpdParcelShopLimits();
        $pointOutput['limits'] = null;
        if (null !== $limits) {
            $pointOutput['limits'] = $this->getLimitsForOutput($limits);
        }
        // Schedules
        $pointOutput['schedules'] = $this->getScheduleCollectionForOutput($point->getDpdSchedules());
        if (\count($pointOutput['schedules']) === 0) {
            $pointOutput['schedules'] = null;
        }

        return $pointOutput;
    }

    /**
     * @param DpdParcelShopLimits $limits
     * @return array
     */
    public function getLimitsForOutput(DpdParcelShopLimits $limits): array
    {
        $limitsOutput = [];
        $limitsOutput['weight'] = $limits->getWeight();
        $limitsOutput['width'] = $limits->getWidth();
        $limitsOutput['length'] = $limits->getLength();
        $limitsOutput['height'] = $limits->getHeight();
        $limitsOutput['dimension_sum'] = $limits->getDimensionSum();

        return $limitsOutput;
    }

    /**
     * @param $schedules
     * @return array
     */
    public function getScheduleCollectionForOutput($schedules): array
    {
        $schedulesOutput = [];
        foreach ($schedules as $schedule) {
            $schedulesOutput[] = $this->getScheduleForOutput($schedule);
        }

        return $schedulesOutput;
    }

    /**
     * @param DpdSchedule $schedule
     * @return array
     */
    public function getScheduleForOutput(DpdSchedule $schedule): array
    {
        $scheduleOutput = [];
        $scheduleOutput['type'] = $schedule->getType();
        $scheduleOutput['timetables'] = $this->getTimetableCollectionForOutput($schedule->getDpdTimetables());

        return $scheduleOutput;
    }

    /**
     * @param $timetables
     * @return array
     */
    public function getTimetableCollectionForOutput($timetables): array
    {
        $timetablesOutput = [];
        /** @var DpdTimetable $timetable */
        foreach ($timetables as $timetable) {
            $week_days = explode( ',', $timetable->getWeekDays());
            foreach ($week_days as $week_day) {
                $timetableOutput = [];
                $timetableOutput['week_day'] = $week_day;
                $timetableOutput['work_time'] = $timetable->getWorkTime();

                $timetablesOutput[] = $timetableOutput;
            }
        }

        return $timetablesOutput;
    }

    /**
     * @param DpdTimetable $timetable
     * @return array
     *
     * @TODO Пока не используется
     */
    public function getTimetableForOutput(DpdTimetable $timetable): array
    {
        $timetableOutput = [];
        $timetableOutput['week_days'] = $timetable->getWeekDays();
        $timetableOutput['work_time'] = $timetable->getWorkTime();

        return $timetableOutput;
    }

    /**
     * @param Collection $dpdParcels
     * @param string $order_number
     * @return array
     */
    private function createArrayOfStdParcelClasses(Collection $dpdParcels, string $order_number): array
    {
        $parcels = [];
        /** @var DpdDeliveryParcel $dpdParcel */
        foreach ($dpdParcels as $dpdParcel) {
            $parcel = [];
            // Номер посылки в информационной системе клиента (номер штрих-кода посылки)
            $parcel['number']   = $order_number . '-' . $dpdParcel->getId();
            // $parcel['box_needed'] = null; // пока не используем
            $parcel['weight']   = $dpdParcel->getWeight();
            $parcel['length']   = $dpdParcel->getLength();
            $parcel['width']    = $dpdParcel->getWidth();
            $parcel['height']   = $dpdParcel->getHeight();
            // $parcel['number_for_print'] = null; // пока не используем
            // $parcel['insurancceCost'] = null; // пока не используем
            // $parcel['codAmount = null']; // пока не используем

            $parcels[] = $parcel;
        }

        return $parcels;
    }

    /**
     * @param DpdDeliveryAddress $dpdDeliveryAddress
     * @param string|null $code
     * @return array
     */
    private function createStdAddressClass(DpdDeliveryAddress $dpdDeliveryAddress, string $code = null): array
    {
        /** @var DpdCity $dpdCity */
        $dpdCity = $this->entityManager
            ->getRepository(DpdCity::class)->findOneBy([
                'cityId'=> $dpdDeliveryAddress->getCityId()
            ]);


        $address = [];
        if (null !== $code) {
            $address['code'] = $code;
        }
        $address['countryName']     = $dpdCity ? $dpdCity->getCountryName() : null;
        $address['city']            = $dpdCity ? $dpdCity->getCityName() : null;
        $address['name']            = $dpdDeliveryAddress->getName();
        $address['terminalCode']    = $dpdDeliveryAddress->getTerminalCode();
        $address['index']           = $dpdDeliveryAddress->getIndex();
        $address['street']          = $dpdDeliveryAddress->getStreet();
        $address['streetAbbr']      = $dpdDeliveryAddress->getStreetAbr();
        $address['house']           = $dpdDeliveryAddress->getHouse();
        $address['houseKorpus']     = $dpdDeliveryAddress->getHouseKorpus();
        $address['str']             = $dpdDeliveryAddress->getStr();
        $address['vlad']            = $dpdDeliveryAddress->getVlad();
        $address['extraInfo']       = $dpdDeliveryAddress->getExtraInfo();
        $address['office']          = $dpdDeliveryAddress->getOffice();
        $address['flat']            = $dpdDeliveryAddress->getFlat();
        $address['workTimeFrom']    = $dpdDeliveryAddress->getWorkTimeFrom();
        $address['workTimeTo']      = $dpdDeliveryAddress->getWorkTimeTo();
        $address['dinnerTimeFrom']  = $dpdDeliveryAddress->getDinnerTimeFrom();
        $address['dinnerTimeTo']    = $dpdDeliveryAddress->getDinnerTimeTo();
        $address['contactFio']      = $dpdDeliveryAddress->getContactFio();
        $address['contactPhone']    = $dpdDeliveryAddress->getContactPhone();
        $address['contactEmail']    = $dpdDeliveryAddress->getContactEmail();
        $address['instructions']    = $dpdDeliveryAddress->getInstructions();
        $address['needPass']        = $dpdDeliveryAddress->isNeedPass();

        return $address;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @return string
     */
    private function getServiceVariant(DpdDeliveryOrder $deliveryOrderDpd): string
    {
        /** @var DpdDeliveryAddress $senderAddress */
        $senderAddress = $deliveryOrderDpd->getSenderAddress();
        /** @var DpdDeliveryAddress $receiverAddress */
        $receiverAddress = $deliveryOrderDpd->getReceiverAddress();

        // от двери отправителя
        $sender_variant = 'Д';
        // до двери получателя
        $receiver_variant = 'Д';

        if ($senderAddress->getTerminalCode()) {
            // от терминала DPD
            $sender_variant = 'Т';
        }

        // Пока только до двери получателя
//        if ($receiverAddress->getTerminalCode()) {
//            // до терминала DPD
//            $receiver_variant = 'Т';
//        }

        return $sender_variant.$receiver_variant;
    }

    /**
     * @param DpdDeliveryOrder $deliveryOrderDpd
     * @return array
     * @throws LogicException
     */
    private function prepareDataForOrderCreation(DpdDeliveryOrder $deliveryOrderDpd): array
    {
        $order_number = $deliveryOrderDpd->getDeliveryOrder()->getOrderNumber();

        $data = [];
        $data['header'] = [];
        $data['header']['datePickup'] = $deliveryOrderDpd->getDatePickup()->format('Y-m-d');
        // Клиентский номер плательщика в системе DPD (номер договора с DPD).
        // Если этот параметр не заполнен, то плательщиком будет считаться заказчик (номер из параметра auth).
        //$data['header']['payer'] = null;
        $data['header']['senderAddress'] = $this->createStdAddressClass($deliveryOrderDpd->getSenderAddress());
        $data['header']['pickupTimePeriod'] = $deliveryOrderDpd->getPickupTimePeriod();
        // Номер регулярного заказа DPD.
        // Если вы используете доставку на регулярной основе, уточните этот номер у своего менеджера.
        // $data['header']['regularNum'] = null;
        $data['order'] = [];
        /** @var DeliveryOrder $deliveryOrder */
        $deliveryOrder = $deliveryOrderDpd->getDeliveryOrder();
        $data['order']['orderNumberInternal'] = $deliveryOrder->getOrderNumber();
        $data['order']['serviceCode'] = $deliveryOrderDpd->getServiceCode();
        $data['order']['serviceVariant'] = $this->getServiceVariant($deliveryOrderDpd);
        $data['order']['cargoNumPack'] = $deliveryOrderDpd->getParcels()->count();
        $data['order']['cargoWeight'] = $this->getCargoWeight($deliveryOrderDpd->getParcels());
        $data['order']['cargoVolume'] = $this->getCargoVolume($deliveryOrderDpd->getParcels());
        $data['order']['cargoRegistered'] = $deliveryOrderDpd->isCargoValuable();
        $data['order']['cargoValue'] = $deliveryOrderDpd->getDeclaredValue();
        $data['order']['cargoCategory'] = $deliveryOrderDpd->getCargoCategory();
        $data['order']['deliveryTimePeriod'] = $deliveryOrderDpd->getDeliveryTimePeriod();
        // Если данный параметр не указывать, то счёт будет выставлен автоматически на отправителя по безналичному расчёту.
        $data['order']['paymentType'] = $deliveryOrderDpd->getPaymentType();
        // Зарезервированный параметр для ввода новых параметров без изменения схемы сервиса
        // $data['order']['extraParam'] = null; // пока не используем
        // Данные для международных отправок
        // $data['order']['dataInt'] = null; // пока не используем
        $data['order']['receiverAddress'] = $this->createStdAddressClass($deliveryOrderDpd->getReceiverAddress());
        // $data['order']['extraService'] = null; // пока не используем
        $data['order']['parcel'] = $this->createArrayOfStdParcelClasses($deliveryOrderDpd->getParcels(), $order_number);
        // Массив вложений в посылке для 54ФЗ(только для доставки по России)
        // $data['order']['unitLoad'] = null;  // пока не используем

        return $data;
    }

    /**
     * @param $address
     * @return bool
     */
    public function isNotFillDpdAddress($address):bool
    {
        if (! $address instanceof DpdDeliveryAddress) {

            return true;
        }
        $city_id = $address->getCityId();
        $street = $address->getStreet();
        $house = $address->getHouse();
        $contact_email = $address->getContactEmail();
        $contact_phone = $address->getContactPhone();
        $contact_fio = $address->getContactFio();

        if (!$city_id || !$street || !$house || !$contact_email || !$contact_phone || !$contact_fio) {

            return true;
        }

        return false;
    }

    /**
     * @param $parcels
     * @return bool
     */
    public function isNotFillDpdParcel($parcels):bool
    {
        if (! $parcels || empty($parcels)) {

            return true;
        }
        /** @var DpdDeliveryParcel $parcel */
        foreach ($parcels as $parcel) {
            if (! $parcel instanceof DpdDeliveryParcel || !$parcel->getWeight()) {

                return true;
            }
        }

        return false;
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getDpdDeliveryOrderByAddressId($id)
    {
        $dpdDeliveryAddress = $this->entityManager
            ->getRepository(DpdDeliveryAddress::class)->findOneBy([
                'id'=> (int) $id
            ]);

        if (!$dpdDeliveryAddress) {

            return null;
        }

        $dpdDeliveryOrder = $this->entityManager
            ->getRepository(DpdDeliveryOrder::class)->findOneBy([
                'receiverAddress'=> $dpdDeliveryAddress
            ]);
        if ($dpdDeliveryOrder) {

            return $dpdDeliveryOrder;
        }

        $dpdDeliveryOrder = $this->entityManager
            ->getRepository(DpdDeliveryOrder::class)->findOneBy([
                'senderAddress'=> $dpdDeliveryAddress
            ]);
        if ($dpdDeliveryOrder) {

            return $dpdDeliveryOrder;
        }

        return null;
    }

    /**
     * @param DpdDeliveryOrder $dpdDeliveryOrder
     */
    public function remove(DpdDeliveryOrder $dpdDeliveryOrder)
    {
        $this->entityManager->remove($dpdDeliveryOrder);
    }

    /**
     * @param Collection $dpdParcels
     * @return float|null
     */
    private function getCargoWeight(Collection $dpdParcels)
    {
        $weight = 0;
        /** @var DpdDeliveryParcel $dpdParcel */
        foreach ($dpdParcels as $dpdParcel) {
            $weight += $dpdParcel->getWeight();
        }

        return 0 === $weight ? null : $weight;
    }

    /**
     * @param Collection $dpdParcels
     * @return float|int|null
     * @throws LogicException
     */
    private function getCargoVolume(Collection $dpdParcels)
    {
        $total_volume = 0;
        /** @var DpdDeliveryParcel $dpdParcel */
        foreach ($dpdParcels as $dpdParcel) {
            $height = $dpdParcel->getHeight();
            $length = $dpdParcel->getLength();
            $width  = $dpdParcel->getWidth();
            if (null === $height || null === $length || null === $width) {

                #throw new LogicException(null, LogicException::DELIVERY_DPD_PARCEL_DIMENSION_NOR_SPECIFIED);
                return null;
            }

            $parcel_volume = $dpdParcel->getHeight() * $dpdParcel->getLength() * $dpdParcel->getWidth();
            $total_volume += $parcel_volume;
        }

        return 0 === $total_volume ? null : $total_volume;
    }

    /**
     * @return array
     */
    private function getAuthData(): array
    {
        return [
            'clientNumber' => $this->account,
            'clientKey' => $this->secret_key
        ];
    }

    /**
     * @param string $wsdl_url
     * @param string $method
     * @param array $arguments
     * @return mixed
     */
    private function doSoapRequest(string $wsdl_url, string $method, array $arguments)
    {
        $client = new SoapClient($wsdl_url);

        return $client->$method($arguments);
    }

    /*
     * Статусы создания заказа.
     * В ответном сообщении о создании заказа может быть возвращен один из следующих статусов:
     *
     * OK – заказ на доставку успешно создан с номером, указанным в поле orderNum.
     * OrderPending – заказ на доставку принят, но нуждается в ручной доработке сотрудником DPD,
     *               (например, по причине того, что адрес доставки не распознан автоматически).
     *                Номер заказа будет присвоен ему, когда это доработка будет произведена.
     * OrderDuplicate – заказ на доставку не может быть принять по причине, указанной в поле errorMessage.
     * OrderError – заказ на доставку не может быть создан по причине, указанной в поле errorMessage.
     */

    /*
     * Статусы создания и изменения адреса.
     * В ответном сообщении о результатах создания адреса может быть возвращен один из следующих статусов:
     *
     * ОК – адрес с кодом создан.
     * code-already-exists – адрес с указанным кодом уже существует.
     * address-error – адрес не может быть создан. Причина при этом указывается в поле message.
     * code-not-found- адрес с указанным кодом не найден.
     */
}