<?php
namespace ModuleDeliveryDpd\Service;

use Core\Exception\LogicException;
use Doctrine\ORM\EntityManager;
use SoapClient;

/**
 * Class DeliveryDpdStatusManager
 * @package ModuleDeliveryDpd\Service
 */
class DeliveryDpdStatusManager
{
    // Получить все состояния посылок клиента, изменившиеся с момента последнего вызова данного метода
    const REQUEST_METHOD_FOR_DPD_ORDER_STATUS_BY_CLIENT = 'getStatesByClient';

    const SUCCESS_DELIVERY_STATUS = 'Delivered';
    const FAILED_DELIVERY_STATUSES = [
        'NewOrderByClient', // оформлен новый заказ по инициативе клиента
        'NotDone',          // заказ отменен
        'Lost',             // посылка утеряна
        'NewOrderByDPD'     // оформлен новый заказ по инициативе DPD
    ];

    /**
     * @var string
     */
    private $account;

    /**
     * @var string
     */
    private $secret_key;

    /**
     * @var string
     */
    private $gateway_host;

    /**
     * @var string
     */
    private $trace_page_url;

    /**
     * @var string
     */
    private $url_for_status;

    /**
     * DeliveryDpdStatusManager constructor.
     * @param EntityManager $entityManager
     * @param DeliveryDpdManager $deliveryDpdManager
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->account          = $config['delivery_dpd_settings']['account'];
        $this->secret_key       = $config['delivery_dpd_settings']['secret_key'];
        $this->gateway_host     = $config['delivery_dpd_settings']['gateway_host'];
        $this->trace_page_url   = $config['delivery_dpd_settings']['trace_page_url'];

        $this->url_for_status = $this->gateway_host.'tracing?wsdl';
    }

    /**
     * @return string
     */
    public function getServiceTracePageUrl()
    {
        return $this->trace_page_url;
    }

    /**
     * @return mixed
     * @throws LogicException
     */
    public function getDataOfParcelsWithChangedStatus()
    {
        $wsdl_url = $this->url_for_status;
        $method = self::REQUEST_METHOD_FOR_DPD_ORDER_STATUS_BY_CLIENT;

        $arguments = [];
        $arguments['request'] = [];
        $arguments['request']['auth'] = $this->getAuthData();

        /*
         * Параметры на вход:
         * Внешний тэг: request
         * auth [
         *      clientNumber    // клиентский номер в системе DPD / (номер вашего договора с DPD) / число / 1000000000
         *      clientKey       // уникальный ключ для авторизации, полученный у сотрудника DPD / строка / 1FD890C3556
         * ]
         */
        $result = $this->doSoapRequest($wsdl_url, $method, $arguments);
        /*
         * Параметры ответного сообщения:
         * docId // Идентификатор документа. Данный идентификатор используется для подтверждения получения статусов / число / 12346897
         * docDate // Дата формирования документа / дата / 2014-02-28
         * clientNumber // Ваш клиентский номер в системе DPD / число / 1000000000
         * resultComplete // boolean / true
         *      Показывает, выбраны ли в текущем запросе все новые состояния по клиенту (значение true),
         *      или был достигнут лимит записей в одном запросе и для продолжения необходим ещё один запрос (значение false).
         *      Пояснение:
         *          Если возвращается false, то значит есть ещё статусы и можно повторно вызвать метод, чтобы их получить. В этом случае нет ограничения на повторный вызов.
         *          Если же в ответе вернулось true – то значит больше статусов нет и повторный вызов возможен только через 5мин.
         * states / Массив состояний посылок
         *      clientOrderNr // Номер заказа в информационной системе клиента / строка / 12346DPD
         *      clientParcelNr // Номер посылки в информационной системе клиента / строка / 12346897
         *      dpdOrderNr // Номер заказа в информационной системе DPD / строка / 04040001MOW
         *      dpdParcelNr // Номер посылки в информационной системе DPD / строка / 12346897
         *      pickupDate // Дата приёма груза / дата / 2014-02-28
         *      dpdOrderReNr // Номер повторного заказа в системе DPD / строка / 04040002MOW
         *          (заполняется в том случае, если по одному и тому же клиентскому номеру посылки в системе DPD
         *           существует два заказа – например, при заказе на возврат посылки)
         *      dpdParcelReNr // Номер посылки при повторном заказе в системе DPD / строка / 12346899
         *          (заполняется в том случае, если по одному и тому же клиентскому номеру посылки в системе DPD
         *           существует два заказа – например, при заказе на возврат посылки)
         *      isReturn1 // признак «Возвратная посылка» / логический / true/false
         *          Если значение true, то это возвратный заказ, если false, то переадресация.
         *      planDeliveryDate // Планируемая дата доставки посылки / дата / 2014-03-01
         *      newState // Состояние посылки после перехода / строка / Delivering
         *          См. список возможных состояний в разделе «Состояния» пункт  «Посылка».
         *      transitionTime // Время перехода состояния / дата/время / 2012-04-04T17:10:15
         *      terminalCode // Код терминала DPD, на котором произошел переход состояния / строка / LED
         *      terminalCity // Город терминала DPD, на котором произошел переход состояния / строка / LED
         *      incidentCode // Код инцидента, произошедшего при переходе состояния / строка / 90
         *          Список возможных кодов инцидентов и их расшифровок вы можете получить у своего менеджера.
         *      incidentName // Наименование инцидента, произошедшего при переходе состояния / строка / Возвращено отправителю
         *      consignee // Фактический получатель посылки (передается только со статусом Delivered) / строка / Иванов И.И.
         */

        if (null === $result || null === $result->return || empty($result->return)) {

            throw new LogicException(null,
                LogicException::DELIVERY_DPD_REQUEST_NO_STATUS_RETURNED);
        }

        return $result->return;
    }

    /**
     * @param \stdClass $data
     * @return array|null
     */
    public function getDataWithFinalStatuses(\stdClass $data)
    {
        $delivered = [];
        $failed = [];

        $states = $data->states ?? null;
        if (null === $states) {

            return null;
        }

        if (\is_array($states)) {
            foreach ($states as $state) {
                if ($state['newState'] === self::SUCCESS_DELIVERY_STATUS) {
                    $delivered[] = [
                        'order_number' => $state['clientOrderNr'], // Номер заказа в информационной системе клиента
                        'dpd_order_number' => $state['dpdOrderNr'], // Номер заказа в информационной системе DPD
                        'delivery_time' => $state['transitionTime'], // Время перехода состояния / 2012-04-04T17:10:15
                    ];
                } elseif (\in_array($state['newState'], self::FAILED_DELIVERY_STATUSES, true)) {
                    $failed[] = [
                        'order_number' => $state['clientOrderNr'], // Номер заказа в информационной системе клиента
                        'dpd_order_number' => $state['dpdOrderNr'], // Номер заказа в информационной системе DPD
                        'delivery_fail_time' => $state['transitionTime'], // Время перехода состояния / 2012-04-04T17:10:15
                    ];
                }
            }
        }

        return ['delivered' => $delivered, 'failed' => $failed];
    }

    /**
     * @return array
     */
    private function getAuthData(): array
    {
        return [
            'clientNumber' => $this->account,
            'clientKey' => $this->secret_key
        ];
    }

    /**
     * @param string $wsdl_url
     * @param string $method
     * @param array $arguments
     * @return mixed
     */
    private function doSoapRequest(string $wsdl_url, string $method, array $arguments)
    {
        $client = new SoapClient($wsdl_url);

        return $client->$method($arguments);
    }

    /**
     * Состояния посылки (Статусы доставки)
     * Посылка может принимать одно из следующих состояний:
     * NewOrderByClient – оформлен новый заказ по инициативе клиента
     * NotDone – заказ отменен
     * OnTerminalPickup – посылка находится на терминале приема отправления
     * OnRoad – посылка находится в пути (внутренняя перевозка DPD)
     * OnTerminal – посылка находится на транзитном терминале
     * OnTerminalDelivery – посылка находится на терминале доставки
     * Delivering – посылка выведена на доставку
     * Delivered – посылка доставлена получателю
     * Lost – посылка утеряна
     * Problem – с посылкой возникла проблемная ситуация
     * ReturnedFromDelivery – посылка возвращена с доставки
     * NewOrderByDPD – оформлен новый заказ по инициативе DPD
     */
}