<?php

namespace ModuleCodeTest\Provider;

use PHPUnit\Framework\TestCase;

class SmsCodeGeneratorTraitTest extends TestCase
{
    /**
     * @var object
     */
    private $codeGenerator;

    /**
     * This method is called before a test is executed.
     *
     * @return void
     */
    public function setUp()
    {
        $this->codeGenerator = new SmsCodeGeneratorTraitImpl();
    }


    public function testGenerateCodeFormat()
    {
        $expected = '/^[0-9]{5,10}$/';
        $code = $this->codeGenerator->codeGenerate();
        $this->assertRegExp($expected, $code);
    }

    public function testCodeGenerateUnequal()
    {
        // Code generate
        $code_1 = $this->codeGenerator->codeGenerate(\ModuleCode\Api\CodeController::SMS_CODE_LENGTH);
        $code_2 = $this->codeGenerator->codeGenerate(\ModuleCode\Api\CodeController::SMS_CODE_LENGTH);

        $this->assertNotSame($code_1, $code_2);
    }

    public function testCodeGenerateRequiredLength()
    {
        $code = $this->codeGenerator->codeGenerate(6);
        $this->assertEquals(6, strlen($code));

        $code = $this->codeGenerator->codeGenerate(8);
        $this->assertEquals(8, strlen($code));

        $code = $this->codeGenerator->codeGenerate(10);
        $this->assertEquals(10, strlen($code));
    }
}