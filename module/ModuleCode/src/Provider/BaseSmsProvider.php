<?php

namespace ModuleCode\Provider;

/**
 * Class BaseSmsProvider
 * @package ModuleCode\Provider
 */
abstract class BaseSmsProvider implements SmsProviderInterface
{
    use \Core\Provider\HttpRequestTrait;

    /**
     * @var string $gateway_host
     */
    protected $gateway_host;

    /**
     * @var string $phone
     */
    protected $phone;

    /**
     * @var float|null
     */
    protected $provider_balance = null;


    /**
     * {@inheritdoc}
     */
    abstract function smsSend($phone, $message);

    /**
     * {@inheritdoc}
     */
    abstract function smsStatusCheck();

    /**
     * {@inheritdoc}
     */
    abstract function isBalanceEnough();

    /**
     * {@inheritdoc}
     */
    abstract function getName();

    abstract function getBalanceLimit();

    /**
     * @return string
     */
    public function getGatewayHost()
    {
        return $this->gateway_host;
    }

    /**
     * @param string $gateway_host
     */
    public function setGatewayHost($gateway_host)
    {
        $this->gateway_host = $gateway_host;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getProviderBalance()
    {
        return $this->provider_balance;
    }

    /**
     * @param mixed $provider_balance
     */
    public function setProviderBalance($provider_balance)
    {
        $this->provider_balance = $provider_balance;
    }
}