<?php

namespace ModuleCode\Provider;

use Core\Exception\LogicException;

/**
 * Class ProstorSmsProvider
 * @package ModuleCode\Provider
 * @TODO Обработка всех ответов провайдера (ALL_PROVIDER_ANSWERS)
 */
class ProstorSmsProvider extends BaseSmsProvider
{
    // Provider name
    const PROVIDER_NAME = "Prostor-SMS";
    // Provider balance limit
    const BALANCE_LIMIT = 5;
    // All ProstorSmsProvider answers
    const SUCCESS_PROVIDER_ANSWER       = 'accepted';                       // Сообщение принято сервисом (accepted)
    const INVALID_MOBILE_PHONE          = 'invalid mobile phone';           // Неверно задан номер тефона (формат +71234567890)
    const TEXT_IS_EMPTY                 = 'text is empty';                  // Отсутствует текст
    const SENDER_ADDRESS_INVALID        = 'sender address invalid';         // Неверная (незарегистрированная) подпись отправителя
    const WAPURL_INVALID                = 'wapurl invalid';                 // Неправильный формат wap-push ссылки
    const INVALID_SCHEDULE_TIME_FORMAT  = 'invalid schedule time format';   // Неверный формат даты отложенной отправки сообщения
    const INVALID_STATUS_QUEUE_NAME     = 'invalid status queue name';      // Неверное название очереди статусов сообщений
    const NOT_ENOUGH_BALANCE            = 'not enough balance';             // Баланс пуст (проверьте баланс)

    /**
     * For sockets
     * @var int
     */
    private $port = null; // For sockets (пока не используем)

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string|null
     */
    private $sender;

    /**
     * @var string|null
     */
    private $wapurl;


    /**
     * ProstorSmsProvider constructor.
     * @param $config_sms_provider
     */
    public function __construct($config_sms_provider)
    {
        $this->gateway_host = $config_sms_provider['gateway_host'];
        $this->login        = $config_sms_provider['login'];
        $this->password     = $config_sms_provider['pass'];
        $this->sender       = $config_sms_provider['sender'];
        $this->wapurl       = $config_sms_provider['wapurl'];
        //$this->port         = $config_sms_provider['port']; // For sockets (пока не используем)
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::PROVIDER_NAME;
    }

    /**
     * {@inheritdoc}
     */
    public function smsSend($phone, $message)
    {
        if (!preg_match("/^(\+)/", $phone)) {
            $phone = '+'.$phone;
        }
        $url = $this->gateway_host."/messages/v2/send/?phone=".$phone."&text=".$message."&login=".$this->login."&password=".$this->password;
        $result = $this->smsSendHttpRequest($url);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function smsStatusCheck()
    {
        // TODO: Implement smsStatusCheck() method.
    }

    /**
     * {@inheritdoc}
     */
    public function isBalanceEnough()
    {
        $provider_balance = $this->providerBalanceRequest();

        return ($provider_balance > self::BALANCE_LIMIT) ? true : false;
    }

    /**
     * @return int
     */
    public function getBalanceLimit()
    {
        return self::BALANCE_LIMIT;
    }

    /**
     * @return float|null
     */
    public function providerBalanceRequest()
    {
        $url = $this->gateway_host."/messages/v2/balance/?login=".$this->login."&password=".$this->password;

        $provider_balance = $this->providerBalanceHttpRequest($url);
        // Set $provider_balance to $this->provider_balance property
        $this->setProviderBalance($provider_balance);

        return $provider_balance;
    }

    /**
     * Sends SMS message by provider gateway
     *
     * @param $url
     * @return bool
     * @throws \Exception
     */
    private function smsSendHttpRequest($url)
    {
        // Do request to provider's API
        $result = $this->doHttpRequest($url);

        if( !$result ) {

            throw new \Exception('Can not send SMS message by provider '.$this->getName());
        }

        // Split response (e.g. "accepted;2397708808") to pieces
        $resultPieces = explode(";", $result);
        //@TODO временное логирование
        logMessage($url."\n********\n".$result, 'sms');

        // If keys $resultPieces[0] and $resultPieces[1] exist as expected and $resultPieces[0] = 'accepted'
        if (array_key_exists(0, $resultPieces) && $resultPieces[0] == self::SUCCESS_PROVIDER_ANSWER
            && array_key_exists(1, $resultPieces)) {

            // Return message id (e.g. "2397708808") from API to save it in DB
            return $resultPieces[1];
        }
        // Prostor-SMS вернул ошибку (SMS не отправлено)
        // @TODO Т.к. SMS не отправлено, нужно удалить сгенерированный код.

        // Error message
        $error_message = isset($resultPieces[1]) ? " (" . $resultPieces[1] . ")" : null;

        throw new LogicException($this->getName(). " returned answer: ". $resultPieces[0] . $error_message,
            LogicException::SMS_PROVIDER_ERROR);
    }

    /**
     * @param $url
     * @return mixed
     * @throws \Exception
     */
    public function doHttpRequest($url)
    {
        try {
            // Http Client selection
            // Zend Http Client (passed $url provides SMS sending)
            $result = $this->zendHttpRequest($url);
            // Guzzle Http Client (passed $url provides SMS sending)
            #$result = $this->guzzleHttpRequest($url, 'json');
        }
        catch (\Throwable $t) {

            throw new \Exception($t->getMessage().'. '.$this->getName());
        }

        return $result;
    }

    /**
     * Get provider balance
     *
     * @param $url
     * @return float|null
     */
    private function providerBalanceHttpRequest($url)
    {
        // Zend Http Client
        $result = $this->zendHttpRequest($url);
        // Guzzle Http Client
        #$result = $this->guzzleHttpRequest($url);

        if( !$result ) return null;

        // Split response (e.g. "RUB;3008.6;0.0") to pieces
        $pieces = explode(";", $result);

        return (float)$pieces[1];
    }
}