<?php

namespace ModuleCode\Fixture;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineDataFixtureModule\ContainerAwareInterface;
use DoctrineDataFixtureModule\ContainerAwareTrait;
use ModuleCode\Entity\CodeOperationType;
use ModuleCode\Entity\Code;
use Application\Entity\User;

/**
 * Class CodeFixtureLoader
 * @package ModuleCode\Fixture
 */
class CodeFixtureLoader implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $entityManager = $this->container->get('doctrine.entitymanager.orm_default');
        // Relations
        $user = $entityManager->getRepository(User::class)
            ->findOneBy(['login' => 'test']);
        $codeOperationType = $entityManager->getRepository(CodeOperationType::class)
            ->findOneBy(['name' => 'PHONE_CONFIRMATION']);

        $confirmationCode = new Code();
        $confirmationCode->setValue('112233');
        $confirmationCode->setUser($user);
        $confirmationCode->setCodeOperationType($codeOperationType);
        $confirmationCode->setCreated(new \DateTime(date('Y-m-d H:i:s')));

        $entityManager->persist($confirmationCode);
        $entityManager->flush();
    }

    public function getOrder()
    {
        return 80;
    }
}