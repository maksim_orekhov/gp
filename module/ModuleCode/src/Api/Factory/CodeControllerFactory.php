<?php
namespace ModuleCode\Api\Factory;

use Interop\Container\ContainerInterface;
use Core\Service\SessionContainerManager;
use ModuleCode\Api\CodeController;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleCode\Service\CodeManager;
use Application\Service\UserManager;

/**
 * This is the factory for ConfirmationCode
 */
class CodeControllerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return CodeController|object
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager    = $container->get('doctrine.entitymanager.orm_default');
        $userManager      = $container->get(UserManager::class);
        $codeManager      = $container->get(CodeManager::class);
        $userSmsInspector = $container->get(\ModuleCode\Service\UserSmsInspector::class);
        $sessionContainerManager = $container->get(SessionContainerManager::class);

        $config = $container->get('Config');
        // Get contents of 'sms_providers' config key
        if (isset($config['sms_providers']))
            $config_sms_providers = $config['sms_providers'];
        else
            $config_sms_providers = [];
        // Get contents of 'email_providers' config key
        if (isset($config['email_providers']))
            $config_email_providers = $config['email_providers'];
        else
            $config_email_providers = [];

        return new CodeController(
            $entityManager,
            $userManager,
            $codeManager,
            $userSmsInspector,
            $sessionContainerManager,
            $config_sms_providers,
            $config_email_providers
        );
    }
}