<?php
namespace ModuleCode\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use ModuleCode\Entity\CodeOperationType;
use Application\Entity\User;

/**
 * Class CodeRepository
 * @package ModuleCode\Entity\Repository
 */
class CodeRepository extends EntityRepository
{
    /**
     * @param User                  $user
     * @param CodeOperationType     $codeOperationType
     * @return array
     */
    public function selectAllUserCodesByOperationType(User $user, CodeOperationType $codeOperationType)
    {
        $qb = $this->createQueryBuilder('c')
            ->addSelect('c')
            ->andWhere('c.user = :user')
            ->andWhere('c.codeOperationType = :codeOperationType')
            ->setParameter('user', $user)
            ->setParameter('codeOperationType', $codeOperationType);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param CodeOperationType $codeOperationType
     * @return mixed
     */
    public function countUserCodesByOperationType(User $user, CodeOperationType $codeOperationType)
    {
        $qb = $this->createQueryBuilder('c')
            ->Select('COUNT(c)')
            ->andWhere('c.user = :user')
            ->andWhere('c.codeOperationType = :codeOperationType')
            ->setParameter('user', $user)
            ->setParameter('codeOperationType', $codeOperationType);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param $code_value
     * @param $code_operation_type_name
     * @return array
     */
    public function selectAllCodesByValueAndType($code_value, $code_operation_type_name)
    {
        $qb = $this->createQueryBuilder('c')
            ->addSelect('t')
            ->leftJoin('c.codeOperationType', 't')
            ->andWhere('c.value = :code_value')
            ->andWhere('t.name = :code_operation_type_name')
            ->setParameter('code_value', $code_value)
            ->setParameter('code_operation_type_name', $code_operation_type_name);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return array
     */
    public function selectAllUserCodes(User $user){
        $qb = $this->createQueryBuilder('c')
            ->addSelect('c')
            ->andWhere('c.user = :user')
            ->setParameter('user', $user);

        return $qb->getQuery()->getResult();
    }

    /**
     * Удаляет все коды пользователя, созданные раньше указанной даты ($date)
     *
     * @param User $user
     * @param \DateTime $date
     * @return array
     */
    public function deleteOldUserCodes(User $user, \DateTime $date)
    {
        $qd = $this->createQueryBuilder('c')
            ->delete()
            ->andWhere('c.user = :user')
            ->andWhere('c.created < :date')
            ->setParameter('user', $user)
            ->setParameter('date', $date);

        return $qd->getQuery()->getResult();
    }
}