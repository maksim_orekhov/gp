<?php
namespace ModuleCode;

use Zend\Mvc\MvcEvent;
use Doctrine\ORM\Events;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * This method is called once the MVC bootstrapping is complete and allows
     * to register event listeners.
     *
     * @param MvcEvent $event
     */
    public function onBootstrap(MvcEvent $event)
    {

    }
}
