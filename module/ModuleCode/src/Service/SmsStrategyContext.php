<?php
namespace ModuleCode\Service;

use Application\Entity\Phone;
use ModuleCode\Provider\SmsProviderInterface;
use ModuleCode\Provider\ProstorSmsProvider;

/**
 * Class SmsStrategyContext
 * @package ModuleCode\Service
 */
class SmsStrategyContext
{
    /*
     * var \ModuleCode\Provider\SmsProviderInterface
     */
    private $smsProvider = null;

    /**
     * @var string
     */
    private $countryCode = null;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var array
     */
    private $config_sms_providers;

    /**
     * @var Phone
     */
    private $userPhone;


    public function __construct(Phone $userPhone, $config_sms_providers)
    {
        if($userPhone->getCountry()->getPhoneCode()) {
            $this->countryCode = $userPhone->getCountry()->getPhoneCode();
        }

        $this->phoneNumber = $userPhone->getPhoneNumber();
        $this->userPhone = $userPhone;
        $this->config_sms_providers = $config_sms_providers;
    }

    /**
     * Choice of sms provider by some criteria
     *
     * @return SmsProviderInterface|null
     * @throws \Exception
     * @TODO Доделать!
     */
    public function getSmsProvider()
    {
        // Select provider by Country Code
        $smsProvider = $this->getSmsProviderByCountryCode();

        // If not simulation, check balance
        if( false === $this->config_sms_providers['message_send_simulation'] && !$smsProvider->isBalanceEnough() ) {

            throw new \Exception('Provider is not suitable due to insufficient balance.');
        }

        // @TODO Реализовать альтернативный поиск провайдера.

        return $smsProvider;
    }

    /**
     * Select SMS Provider by country code
     *
     * @return SmsProviderInterface|null
     *
     * @TODO Применить PhoneNumberUtilAdapter!
     */
    private function getSmsProviderByCountryCode()
    {
        // Use some criteria

        // Additional zone check
        // @TODO Что делать с номерами, которые начинаются с нуля (0)?
        $prefix = (int)$this->phoneNumberParser($this->phoneNumber);

        // Choose SmsProvider by regionCode and
        switch ($this->countryCode) {
            case "7": // Россия, Казахстан(600—799), Абхазия, Южная Осетия
                // @TODO Вынести в отдельный метод этого же класса
                if($this->countryCode == "7" && ($prefix >= 600 && $prefix < 800)) {
                    // this->smsProvider = // Казахстан
                    break;
                }
                $this->smsProvider = new ProstorSmsProvider($this->config_sms_providers['prostor-sms']);
                break;
            case "375": // Беларусь
                // this->smsProvider =
                break;
            case "380": // Украина
                // this->smsProvider =
                break;
            default: // Default provider
                $this->smsProvider = new ProstorSmsProvider($this->config_sms_providers['prostor-sms']);
        }

        return $this->smsProvider;
    }


    private function getSmsProviderByBalance()
    {
        // @TODO Implement getSmsProviderByBalance() method.
    }

    /**
     * @param $phoneNumber
     * @param int $quantity
     * @return bool|string
     * @TODO Убрать!
     */
    private function phoneNumberParser($phoneNumber, $quantity=3)
    {
        $prefix = substr($phoneNumber, 1, $quantity);

        return $prefix;
    }

    /**
     * @return bool
     */
    public function isAllowedSmsNotify()
    {
        $smsAllowedNotificationPolitic = new SmsAllowedNotificationPolitic();

        return $smsAllowedNotificationPolitic->isAllowed($this->userPhone);
    }
}