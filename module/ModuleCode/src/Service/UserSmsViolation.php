<?php

namespace ModuleCode\Service;

/**
 * Abstract class UserSmsViolation
 * @package ModuleCode\Service
 */
abstract class UserSmsViolation implements UserSmsViolationInterface
{
    /**
     * @var \Application\Service\UserManager
     */
    protected $userManager;

    /**
     * @var array
     */
    protected $config_tokens;


    public function __construct($config_tokens, $userManager=null)
    {
        $this->config_tokens    = $config_tokens;
        $this->userManager      = $userManager;
    }
}