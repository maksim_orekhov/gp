<?php
namespace ModuleCode\Service;

use Application\Entity\User;
use ModuleCode\Entity\CodeOperationType;

/**
 * User verification to execute sms operation
 *
 * Class UserSmsInspector
 * @package ModuleCode\Service
 */
class UserSmsInspector
{
    /**
     * @var $userSmsInspectorStrategy
     */
    private $userSmsInspectorStrategy;

    public function __construct(UserSmsInspectorStrategy $userSmsInspectorStrategy)
    {
        $this->userSmsInspectorStrategy = $userSmsInspectorStrategy;
    }

    /**
     * @param User $user
     * @param CodeOperationType $codeOperationType
     * @return array|bool
     */
    public function isUserAllowedToRequestCode(User $user, CodeOperationType $codeOperationType)
    {
        // Check user's violations
        /** @var array $violation */
        $violation = $this->userSmsInspectorStrategy->isUserViolate($user, $codeOperationType);

        if ($violation) {

            $violation['allowed'] = false;

            return $violation;
        }

        return ['allowed' => true];
    }

    /**
     * @TODO Deprecated
     */
    private function isUserViolateSmsStrategy(User $user, CodeOperationType $codeOperationType)
    {
        // User limitations check
        // $userSmsInspectorStrategy = new UserSmsInspectorStrategy($user, $codeType);

        if($this->userSmsInspectorStrategy->isUserViolate($user, $codeOperationType)) {

            return true;
        }

        return false;
    }
}

