<?php
namespace ModuleCode\Service;

use Application\Entity\User;
use ModuleCode\Entity\CodeOperationType;

/**
 * User verification to execute sms operation
 *
 * Class UserSmsInspectorStrategy
 * @package ModuleCode\Service
 */
class UserSmsInspectorStrategy
{
    /**
     * @var \Application\Service\UserManager
     */
    private $userManager;

    /**
     * @var array
     */
    private $config_tokens;

    /**
     * UserSmsInspectorStrategy constructor.
     * @param \Application\Service\UserManager   $userManager
     * @param array         $config_tokens
     */
    public function __construct(\Application\Service\UserManager $userManager, $config_tokens)
    {
        $this->userManager      = $userManager;
        $this->config_tokens    = $config_tokens;
    }

    /**
     * @param User              $user
     * @param CodeOperationType $codeOperationType
     * @return bool
     */
    public function isUserViolate(User $user, CodeOperationType $codeOperationType)
    {
        return $this->userVerificationByStrategy(
            array(
                new UserSmsLegitimacyViolation(null, null),
                new UserSmsLimitByCodeTypeViolation($this->config_tokens, $this->userManager),
                new UserSmsMinIntervalViolation($this->config_tokens, $this->userManager),
            ),
            $user,
            $codeOperationType
        );
    }

    /**
     * @param array             $strategyCollection
     * @param User              $user
     * @param CodeOperationType $codeOperationType
     * @return bool
     */
    private function userVerificationByStrategy($strategyCollection, User $user, CodeOperationType $codeOperationType)
    {
        foreach($strategyCollection as $strategy) {
            $violation = $strategy->hasViolation($user, $codeOperationType);
            if ($strategy instanceof UserSmsViolationInterface && $violation) {

                return $violation;
            }
        }

        return false;
    }
}