<?php

namespace ModuleCode\Service;

use Application\Entity\User;
use ModuleCode\Entity\Code;
use ModuleCode\Entity\CodeOperationType;

/**
 * Class UserSmsMinIntervalVioletion
 * @package ModuleCode\Service
 */
class UserSmsMinIntervalViolation extends UserSmsViolation
{
    /**
     * {@inheritdoc}
     */
    public function hasViolation(User $user, CodeOperationType $codeOperationType=null)
    {
        if(!$codeOperationType) return true; // Violation detected!

        // Select the last user's Code with specific type of operation code
        /** @var Code $lastCode */
        $lastCode = $this->userManager->getLastUserCodeByOperationType($user, $codeOperationType);

        // If some Code exists
        if($lastCode) {
            /** @var \DateTime $currentDate */// Current date
            $currentDate = new \DateTime();
            /** @var \DateTime $createdDate */
            $createdDate = clone $lastCode->getCreated();

            // If code creation date not null and enough time of the last request has not passed
            $request_period = $this->config_tokens['confirmation_code_request_period'];

            if($createdDate->modify('+'.$request_period.' seconds') > $currentDate) {

                return [ // Violation detected!
                    'violation_type'        => 'min_interval',
                    'violation_message'     => 'Not enough time has passed since the last code request',
                    'request_period'        => $request_period,
                    'last_code_created_at'  => $lastCode->getCreated()->getTimestamp()
                ];
            }
        }

        return false; // No violations found
    }
}