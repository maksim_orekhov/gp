<?php

namespace ModuleCode\Service;

use Application\Entity\User;
use ModuleCode\Entity\CodeOperationType;

/**
 * Class UserSmsLegitimacyViolation
 * @package ModuleCode\Service
 */
class UserSmsLegitimacyViolation extends UserSmsViolation
{
    /**
     * {@inheritdoc}
     */
    public function hasViolation(User $user, CodeOperationType $codeOperationType=null)
    {
        if ($user->getIsBanned()) {

            return true; // Violation detected!
        }

        //
        // Other tests for check legitimacy
        //

        return false; // No violations found
    }
}