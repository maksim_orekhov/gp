<?php
namespace ModuleCode\Service;

use Application\Entity\User;
use ModuleCode\Entity\Code;
use ModuleCode\Entity\CodeOperationType;

/**
 * Class CodeManager
 * @package ModuleCode\Service
 */
class CodeManager
{
    /**
     * Doctrine entity manager.
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Contents of the 'tokens' config key.
     * @var array
     */
    private $config_tokens;

    /**
     * CodeManager constructor.
     * @param \Doctrine\ORM\EntityManager $entityManager
     * @param array $config_tokens
     */
    public function __construct(\Doctrine\ORM\EntityManager $entityManager, array $config_tokens)
    {
        $this->entityManager    = $entityManager;
        $this->config_tokens    = $config_tokens;
    }


    /**
     * @param User $user
     * @param $code_operation_type
     * @param $confirmation_code
     * @return bool
     * @throws \Exception
     */
    public function saveCode(User $user, $code_operation_type, $confirmation_code)
    {
        // Create and fill new Code object
        $code = new Code();
        $code->setUser($user);
        $code->setValue($confirmation_code);
        $code->setCodeOperationType($code_operation_type);
        $code->setCreated(new \DateTime(date('Y-m-d H:i:s')));

        // Persist and save
        $this->entityManager->persist($code);
        $this->entityManager->flush();

        // Check if Code saved successfully
        if(null === $code->getId()) {

            throw new \Exception('Can not save code');
        }

        return true;
    }

    /**
     * @param User              $user
     * @param string            $code_value
     * @param CodeOperationType $codeOperationType
     * @return bool
     */
    public function checkUserCodeExists(User $user, CodeOperationType $codeOperationType, $code_value)
    {
        // Get last users's code with codeOperationType
        $lastCode = $this->getLastUserCodeByOperationType($user, $codeOperationType);

        // If no such code exists or user provided code value does not match lastCode value
        if( !$lastCode || !$lastCode instanceof Code || $code_value != $lastCode->getValue() ) {

            return false;
        }

        return true;
    }

    /**
     * @param User              $user
     * @param CodeOperationType $codeOperationType
     * @return mixed
     * @see custom CodeRepository
     */
    public function getLastUserCodeByOperationType(User $user, CodeOperationType $codeOperationType)
    {
        $code = $this->entityManager->getRepository(Code::class)
            ->findOneBy(array('user' => $user, 'codeOperationType' => $codeOperationType),
                array('id' => 'DESC'));

        return $code;
    }

    /**
     * @param $code_value
     * @param $code_type_name
     * @return null|Code
     */
    public function getCodeCollectionByValueAndType($code_value, $code_type_name)
    {
        $codes = $this->entityManager->getRepository(Code::class)
            ->selectAllCodesByValueAndType($code_value, $code_type_name);

        return $codes;
    }

    /**
     * @param User $user
     * @param CodeOperationType $codeOperationType
     * @return void
     */
    public function removeAllUserCodesByCodeType(User $user, CodeOperationType $codeOperationType)
    {
        $codes = $this->entityManager->getRepository(Code::class)
            ->selectAllUserCodesByOperationType($user, $codeOperationType);

        // Remove all selected codes
        if($codes) {
            foreach ($codes as $code) {
                $this->entityManager->remove($code);
            }
            $this->entityManager->flush();
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    public function removeOldUserCodes(User $user)
    {
        $currentDate = new \DateTime();
        $code_expiration_time = $this->config_tokens['confirmation_code_expiration_time'];

        $this->entityManager->getRepository(Code::class)
            ->deleteOldUserCodes($user, $currentDate->modify('-' . $code_expiration_time . ' seconds'));

        return true;
    }

    /**
     * @param $operation_type_name
     * @return null|object
     * @throws \Exception
     */
    public function getCodeOperationTypeByName($operation_type_name)
    {
        $operationType = $this->entityManager->getRepository(CodeOperationType::class)
            ->findOneBy(array('name' => $operation_type_name));

        if(!$operationType || !$operationType instanceof CodeOperationType) {

            throw new \Exception('CodeOperationType with such name not found');
        }

        return $operationType;
    }
}