<?php
namespace ModuleCode\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleCode\Service\UserSmsInspectorStrategy;
use ModuleCode\Service\UserSmsInspector;

/**
 * Class UserSmsInspectorFactory
 * @package ModuleCode\Service\Factory
 */
class UserSmsInspectorFactory implements FactoryInterface
{
    /**
     * This method creates the UserSmsInspectorFactory service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $userSmsInspectorStrategy = $container->get(UserSmsInspectorStrategy::class);

        return new UserSmsInspector($userSmsInspectorStrategy);
    }
}