<?php
namespace ModuleCode\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleCode\Service\CodeManager;

/**
 * This is the factory class for CodeManager service
 */
class CodeManagerFactory implements FactoryInterface
{
    /**
     * This method creates the CodeManager service and returns its instance.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $config = $container->get('Config');
        if (isset($config['tokens']))
            $config_tokens = $config['tokens'];
        else
            $config_tokens = [];

        return new CodeManager($entityManager, $config_tokens);
    }
}