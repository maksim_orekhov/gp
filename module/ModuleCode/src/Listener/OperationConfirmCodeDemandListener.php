<?php
namespace ModuleCode\Listener;

/**
 * Class OperationConfirmCodeDemand
 * @package ModuleCode\Listener
 */
class OperationConfirmCodeDemandListener
{
    /**
     * @var \ModuleCode\Api\CodeController
     */
    private $confirmationCode;

    /**
     * @var \Zend\EventManager\EventManager
     */
    private $events;


    public function __construct($confirmationCode, $events)
    {
        $this->confirmationCode = $confirmationCode;
        $this->events           = $events;
    }

    /**
     * Subscription on Event "onOperationConfirmationCodeDemand"
     * Событие "Требование кода подтверждения пользователем"
     */
    public function subscribe()
    {
        $this->events->attach('onOperationConfirmationCodeDemand', function ($e) {
            // Generate, send and save confirmation code
            $res = $this->confirmationCode->sendCode($e->getParams());

            return $res->getVariables();
        });
    }

    /**
     * Trigger "onOperationConfirmationCodeDemand" event
     *
     * @param $user_login
     * @param $code_operation_type_name
     * @param $emailSender
     * @return mixed
     */
    public function trigger($user_login, $code_operation_type_name, $emailSender=null)
    {
        // Generate, send and save confirmation code
        $result = $this->events->trigger('onOperationConfirmationCodeDemand', null,
            [   // Event $params
                'user_login'                => $user_login,
                'code_operation_type_name'  => $code_operation_type_name,
                'emailSender'               => $emailSender
            ])->last();

        return $result;
    }
}