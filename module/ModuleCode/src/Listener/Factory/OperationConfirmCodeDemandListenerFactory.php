<?php
namespace ModuleCode\Listener\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use ModuleCode\Listener\OperationConfirmCodeDemandListener;
use Zend\EventManager\EventManager;
use ModuleCode\Api\CodeController;

/**
 * Class OperationConfirmCodeDemandListenerFactory
 * @package ModuleCode\Listener\Factory
 */
class OperationConfirmCodeDemandListenerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $confirmationCode = $container->get(CodeController::class);
        $events = new EventManager();

        return new OperationConfirmCodeDemandListener($confirmationCode, $events);
    }
}