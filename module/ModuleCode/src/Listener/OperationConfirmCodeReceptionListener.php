<?php
namespace ModuleCode\Listener;

/**
 * Class OperationConfirmCodeReceptionListener
 * @package ModuleCode\Listener
 */
class OperationConfirmCodeReceptionListener
{
    /**
     * @var \ModuleCode\Api\CodeController
     */
    private $confirmationCode;

    /**
     * @var \Zend\EventManager\EventManager
     */
    private $events;


    public function __construct($confirmationCode, $events)
    {
        $this->confirmationCode = $confirmationCode;
        $this->events           = $events;
    }

    /**
     * Subscription on Event "onOperationConfirmationCodeReception"
     * Событие "Получение кода от пользователя"
     */
    public function subscribe()
    {
        $this->events->attach('onOperationConfirmationCodeReception', function ($e) {
            // Check user submitted code
            $res = $this->confirmationCode->checkCode($e->getParams());

            return $res->getVariables();
        });
    }

    /**
     * Trigger "onOperationConfirmationCodeReception" event
     *
     * @param string $user_login
     * @param string $code_value
     * @param string $code_operation_type_name
     * @return mixed
     */
    public function trigger($user_login, $code_value, $code_operation_type_name)
    {
        // Check user submitted code
        $result = $this->events->trigger('onOperationConfirmationCodeReception', null,
            [   // Event $params
                'user_login'                => $user_login,
                'code_value'                => $code_value,
                'code_operation_type_name'  => $code_operation_type_name
            ])->last();

        return $result;
    }
}