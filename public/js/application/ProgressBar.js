class ProgressBar {
    constructor(options = {}) {
        this.init(options);

        this.init_class = 'init';
        this.completed_class = 'completed';
    }

    /**
     * @param {Object} options - опции для инициализации
     * @namespace
     * @property {function} render - функция вставки элемента в DOM
     * @property {number} progress - процент прогресса число без знака процентов (99 будет означать 99%).
     * @property {string} - text
     */
    init(options) {
        const { render, progress = 0, text = this.getPercentageFormat(progress) } = options;

        this.$progress_bar = $('<div class="progress-bar"></div>');

        this.$text = $(`<p class="progress-bar__text">${text}</p>`);
        this.$text.appendTo(this.$progress_bar);

        this.$progress = $(`<span style="width: ${progress}"></span>`);
        this.$progress.appendTo(this.$progress_bar);

        render(this.$progress_bar);
    }

    getPercentageFormat(progress) {
        if (progress) {
            return `${progress}%`
        }
        return '0%';
    }

    /**
     * @param {number} progress - процент заполнености прогресс бара, передается в процентах без знака % (99 будет означать 99%)
     */
    setProgress(progress = 0) {
        this.$progress.css('width', this.getPercentageFormat(progress));
    }

    /**
     * @param {string} text - текст внутри прогресс бара
     */
    setText(text) {
        this.$text.text(text);
    }

    /**
     * Можно запустить двумя спосабами:
     * 1. передав параметр в виде числа процентов, тогда процент для заполнения шкалы и текст будут сгенированы из одного числа
     * 2. передав объект с нужными параметрами, в случае текст должен отличаться от числа процентов
     * Список параметров для второго случая:
     * @property {number} progress - процент заполнения прогресс бара без знака % (55 будет означать 55%)
     * @property {string} text - текст внутри прогресс бара
     */

    update(options = 0) {
        if (options || options === 0) {
            let text;
            let progress;

            if (typeof options === 'object') {
                progress = options.progress;
                // Если текст передан то ставим его, если нет, то смотрим есть прогресс и строим текст основываясь на нем, если его нет то ставим default
                text = options.text || (options.progress && this.getPercentageFormat(progress)) || 'Выполнение...';
            } else {
                progress = options;
                text = this.getPercentageFormat(options);
            }

            this.setProgress(progress);
            this.setText(text);
        } else {
            console.error('Метод update у ProgressBar должен содержать параметры.')
        }
    }

    reset() {
        this.update();

        if (this.$progress_bar.hasClass(this.init_class) || this.$progress_bar.hasClass(this.completed_class)) {
            this.$progress_bar.removeClass(this.init_class);
            this.$progress_bar.removeClass(this.completed_class);
        }
    }

    setInitState(text = 'Получаем данные') {
        this.update({
            progress: 100,
            text: text
        });
        this.$progress_bar.addClass(this.init_class);
    }

    setReadyState(text = 'Готово') {
        this.update({
            progress: 100,
            text: text
        });
        this.$progress_bar.addClass(this.completed_class);
    }

    remove() {
        this.$progress_bar.remove();
    }
}

export default ProgressBar;