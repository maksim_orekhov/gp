import orderBy from 'lodash/orderBy';

/**
 * Fetch запрос с дефолтными настройками
 * @param {string} url - адрес запроса
 * @param {Object} options - опции fetch запроса, аналогично обычному fetch запросу
 * @returns {Promise} - обработанный ответ от сервера по статусу
 * @example
 * Пример GET запроса
 * customFetch(URLS.CSRF.GET)
 *  .then(data => {
 *      if (data.status === 'SUCCESS') {
 *          const { csrf } = data.data;
 *          this.setState({
 *              csrf
 *          });
 *      } else if (data.status === 'ERROR') {
 *          return Promise.reject('Ошибка при получении инициализации формы. Попробуйте обновить страницу.');
 *      }
 *  })
 *  .catch(error => this.updateFormError(error));
 *
 * Пример POST запроса с параметрами
 * customFetch('home', {
 *      method: 'POST',
 *      body: JSON.stringify({
 *           csrf
 *       })
 *  })
 *    .then(data => {
 *       if (data.status === 'SUCCESS') {
 *           handleOnSubmit && handleOnSubmit();
 *       } else if (data.status === 'ERROR') {
 *           return Promise.reject(data.message);
 *       }
 *    })
 *    .catch(err => {
 *       this.updateFormError(err);
 *    });
 */
export const customFetch = (url, options = {}) => {
    return fetch(url, {
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        },
        credentials: 'include',
        ...options
    })
        .then(response => {
            const { status } = response;
            if (status === 200) {
                return response.json();
            } else {
                return Promise.reject(response);
            }
        })
};

export const goToLink = (url) => {
   window.location = url;
};

export const getIdsFromString = (start_string) => {
    // Удаляем адрес хоста
    const pattern_host = /^(https?:\/\/)?(([\w\.]+)\.([a-z]{2,6}\.?)|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/;
    const string = start_string.replace(pattern_host, '');

    const pattern_id = /\/([1-9]\d*)\/?/g;
    const ids = string.match(pattern_id);

    if (ids) {
        return ids.map(id => id.replace(/\D/g, ''));
    }

    return ids;
};

export const getIdFromUrl = (url = window.location.href) => {
    const ids = getIdsFromString(url);

    if (ids && ids.length) {
        return ids[ids.length - 1]; // Возвращаем последний найденный id
    }

    return ids;
};

/**
 * @param {string} url
 * @returns {string} - возвращает последнюю часть url'a, т.е. часть url'a после последней / (например, /deal/create/ вернет create)
 */
export const getLastUrlPart = (url = window.location.href) => {
    if (url) {
        return url.match(/([^\/]*)\/*$/)[1];
    }
    return '';
};

// Функция сортировки, которая использует lodash/orderBy
export const sorting = (array, sortName, sortDirection) => {
   return orderBy(array, [sortName], [sortDirection]);
};

// Функция, которая меняет направление сортировки
export const changeSortDirection = (name, sortName, sortDirection) => {
   let newSortDirection;
   if (name !== sortName) { // если кликаем по новому столбцу, то ставим сортировку по возрастанию у этого столбца, иначе просто меняем направление у текущего
      newSortDirection = 'asc';
   } else {
      switch (sortDirection) {
         case 'asc':
            newSortDirection = 'desc';
            break;
         case 'desc':
            newSortDirection = 'asc';
         default:
            break;
      }
   }
   return newSortDirection;
};

// Универсальная функция для фильтрации данных в массиве для случая плоских данных. Фильтрация производится по всем переданным параметрам одновременно
export const filterArrayMultiple = (filterNames, filterValue) => {
    const matches = new RegExp(filterValue, "\i");

    return arrayItem => {
        let match_value = false;
        filterNames.forEach(filterName => {
            match_value |= matches.test(arrayItem[filterName]);
        });
        return !filterValue || match_value;
    }
};

// Универсальная функция для фильтрации данных в массиве, в случае плоской структуры данных. Если необходимо фильтровать по вложенным данным, например, arrayItem.deal_status.status, то необходимо писать кастомный метод для фильтрации, т.к. arrayItem[filterName] работать уже не будет.
export const filterArray = (filterName, filterValue) => {
   const matches = new RegExp(filterValue, "\i");
   return arrayItem => !filterValue || matches.test(arrayItem[filterName]);
};

//кастомная функци для фильтрации вложенных данных в массиве, например, arrayItem.deal_status.status
export const filterCustomArray = (filterFirstArgument, filterSecondArgument, filterValue) => {
    const matches = new RegExp(filterValue, "\i");
    return arrayItem => !filterValue || matches.test(arrayItem[filterFirstArgument][filterSecondArgument]);
};

export const filterArrayStrict = (filterName, filterValue) => {
    const matches = new RegExp(`^${filterValue}$`, "\i");
    return arrayItem => !filterValue || matches.test(arrayItem[filterName]);
};

// Фильтрация по ФИО
export const filterFullName = (fullName) => {
   const matches = new RegExp(fullName, "\i");
   return arrayItem =>
      !fullName
      || matches.test(`${arrayItem.last_name} ${arrayItem.first_name} ${arrayItem.secondary_name}`)
      || matches.test(`${arrayItem.first_name} ${arrayItem.last_name} ${arrayItem.secondary_name}`)
      || matches.test(`${arrayItem.first_name} ${arrayItem.secondary_name} ${arrayItem.last_name}`)
};

// Фильтрация по дате создания. Дата ОТ
export const filterCreatedFrom = (filterName, filterValue) => {
   return arrayItem => !filterValue || arrayItem[filterName] >= filterValue;
};

// Фильтрация по дате создания.  Дата ДО
export const filterCreatedTill = (filterName, filterValue) => {
   return arrayItem => !filterValue || arrayItem[filterName] <= filterValue;
};

// Фильтрация по точной дате.
export const filterCreated = (filterName, filterValue) => {
    return arrayItem => !filterValue || (arrayItem[filterName] >= filterValue && arrayItem[filterName] <= filterValue + 84600);
};

// Фильтрация по дате создания сделки в TableDealsTrouble.
export const filterCreatedDate = (filterName, filterValue) => {
    return arrayItem => !filterValue || (arrayItem.deal[filterName] >= filterValue && arrayItem.deal[filterName] <= filterValue + 84600);
};

/**
 * Добавляем дополнительное разбиение ролей в сделке в завимости от типа, либо товар, либо услуга.
 * @param {Array} deals - изначальный массив сделок полученный с бэка
 * @returns {Array} - новый массив сделок включающий разбиение различных полей ориентируясь на тип сделки(услуга, товар)
 * @description Возвращается массив объектов сделок
 * @typedef {Object} deal
 * @property {string} user_role_including_deal_type - теперь вместо 2 ролей будет 4 (покупатель, продавец, исполнитель, заказчик)
 * @property {string} status_including_deal_type - вместо статуса 'delivered' будут 'delivered_u' 'delivered_t'
 * @property {string} status_name_including_deal_type - имя статуса на русском вместо статуса 'Товар доставлен' будет товар 'Выполнено' и 'Получено'
 */
export const getDealsWithFieldsSplittedByDealTypes = (deals) => {
    return deals.map(deal => {
        let user_role_including_deal_type;
        const { user_role, type, deal_status } = deal;

        /** @property {string} user_role_including_deal_type - теперь вместо 2 ролей будет 4 (покупатель, продавец, исполнитель, заказчик) */
        if (type === 'Услуга') {
             if (user_role === 'customer') {
                 user_role_including_deal_type = 'customer';
             } else {
                 user_role_including_deal_type = 'contractor';
             }
        } else {
             if (user_role === 'customer') {
                 user_role_including_deal_type = 'buyer';
             } else {
                 user_role_including_deal_type = 'seller';
             }
        }
        deal['user_role_including_deal_type'] = user_role_including_deal_type;

        /**
         * @property {string} status_including_deal_type - вместо статуса 'delivered' будут 'delivered_u' 'delivered_t'
         * @property {string} status_name_including_deal_type - имя статуса на русском вместо статуса 'Товар доставлен' будет товар 'Выполнено' и 'Получено'
         */
        let status_including_deal_type = deal_status.status;
        let status_name_including_deal_type = deal_status.name;

        if (deal_status.status === 'delivered') {
            if (type === 'Услуга') {
                status_including_deal_type = 'delivered_u';
                status_name_including_deal_type = 'Услуга выполнена';
            } else {
                status_including_deal_type = 'delivered_t';
                status_name_including_deal_type = 'Товар доставлен';
            }
        }
        deal.deal_status['status_including_deal_type'] = status_including_deal_type;
        deal.deal_status['status_name_including_deal_type'] = status_name_including_deal_type;

        return deal;
    });
};

/**
 * Разбиваем статусы сделки с учетом типа сделки - товар или услуга.
 * @param {Array} deal_statuses - массив статусов
 * @returns {Array} - новый массив статусов с двумя новыми статусами
 * @description Возвращается массив объектов статусов сделок
 * @typedef {Object} deal_status
 * @property {string} status_including_deal_type - статус с учетом типа сделки
 * @property {string} status_name_including_deal_type - название статуса на русском с учетом типа сделки
 */
export const getDealStatusesSplittedByDealTypes = (deal_statuses) => {
    // Добавляем в каждый объект deal_status новые поля который будут содержать информацию о статусе с учетом deal_type
    const deal_statuses_splitted_by_deal_types = deal_statuses.map(deal_status => {
        deal_status.status_including_deal_type = deal_status.status;
        deal_status.status_name_including_deal_type = deal_status.name;
        return deal_status;
    });

    // Ищем индекс элемента массива со статусом delivered
    const indexElementWithStatusDelivered = deal_statuses_splitted_by_deal_types.findIndex(deal_status => deal_status.status === 'delivered');

    // Разбиваем массив на статусов на два, тем самым исключая статус delivered, чтобы на его место вставить два новых статуса
    const arr1 = deal_statuses_splitted_by_deal_types.slice(0, indexElementWithStatusDelivered);
    const arr2 = deal_statuses_splitted_by_deal_types.slice(indexElementWithStatusDelivered + 1);

    // Из статуса delivered делаем два новых для двух типов сделки - "Получено" - "Выполнено"
    const status_delivered = deal_statuses_splitted_by_deal_types[indexElementWithStatusDelivered];

    const status_delivered_u = {
        ...status_delivered
    };
    const status_delivered_t = {
        ...status_delivered
    };

    status_delivered_t.status_including_deal_type = 'delivered_t';
    status_delivered_t.status_name_including_deal_type = 'Товар доставлен';

    status_delivered_u.status_including_deal_type = 'delivered_u';
    status_delivered_u.status_name_including_deal_type = 'Услуга выполнена';

    // Собираем во едино все 3 массива
    return [...arr1, status_delivered_t, status_delivered_u, ...arr2];
};

/**
 * Разбиваем статусы спора с учетом типа сделки - товар или услуга.
 * @param {Array} dispute_statuses - массив статусов
 * @returns {Array} - новый массив статусов с двумя новыми статусами
 * @description Возвращается массив объектов статусов сделок
 * @typedef {Object} dispute_status
 * @property {string} status_including_deal_type - статус с учетом типа сделки
 * @property {string} status_name_including_deal_type - название статуса на русском с учетом типа сделки
 */
export const getDisputeStatusesSplittedByDealTypes = (dispute_statuses) => {
    // Добавляем в каждый объект dispute_status новые поля который будут содержать информацию о статусе с учетом deal_type
    const dispute_statuses_splitted_by_deal_types = dispute_statuses.map(dispute_status => {
        dispute_status.status_name_including_deal_type = dispute_status.name;
        return dispute_status;
    });

    // Ищем индекс элемента массива со статусом refund_request
    const indexElementWithStatusRefundRequest = dispute_statuses_splitted_by_deal_types.findIndex(dispute_status => dispute_status.status === 'refund_request');

    // Разбиваем массив статусов на два, тем самым исключая статусы refund_request, refund_request_rejected, refund_request_accepted, чтобы на его место вставить два новых статуса
    const arr1 = dispute_statuses_splitted_by_deal_types.slice(0, indexElementWithStatusRefundRequest);
    const arr2 = dispute_statuses_splitted_by_deal_types.slice(indexElementWithStatusRefundRequest + 1);
    const arr3 = [...arr1, ...arr2];

    const indexElementWithStatusRefundRequestRejected = arr3.findIndex(dispute_status => dispute_status.status === 'refund_request_rejected');

    const arr4 = arr3.slice(0, indexElementWithStatusRefundRequestRejected);
    const arr5 = arr3.slice(indexElementWithStatusRefundRequestRejected + 1);
    const arr6 = [...arr4, ...arr5];

    const indexElementWithStatusRefundRequestAccepted = arr6.findIndex(dispute_status => dispute_status.status === 'refund_request_accepted');

    const arr7 = arr6.slice(0, indexElementWithStatusRefundRequestAccepted);
    const arr8 = arr6.slice(indexElementWithStatusRefundRequestAccepted + 1);

    // Из статусов refund_request, refund_request_rejected, refund_request_accepted делаем два новых для двух типов сделки
    const status_refund_request = dispute_statuses_splitted_by_deal_types[indexElementWithStatusRefundRequest];
    const status_refund_request_rejected = arr3[indexElementWithStatusRefundRequestRejected];
    const status_refund_request_accepted = arr6[indexElementWithStatusRefundRequestAccepted];

    const status_refund_request_u = {
        ...status_refund_request
    };
    const status_refund_request_t = {
        ...status_refund_request
    };

    const status_refund_request_rejected_u = {
        ...status_refund_request_rejected
    };
    const status_refund_request_rejected_t = {
        ...status_refund_request_rejected
    };

    const status_refund_request_accepted_u = {
        ...status_refund_request_accepted
    };
    const status_refund_request_accepted_t = {
        ...status_refund_request_accepted
    };

    status_refund_request_t.status_including_deal_type = 'status_refund_request_t';
    status_refund_request_t.status_name_including_deal_type = 'Предложено решение: Возврат';

    status_refund_request_u.status_including_deal_type = 'status_refund_request_u';
    status_refund_request_u.status_name_including_deal_type = 'Предложено решение: Отказ от услуги';

    status_refund_request_rejected_t.status_including_deal_type = 'status_refund_request_rejected_t';
    status_refund_request_rejected_t.status_name_including_deal_type = 'Предложение возврата отклонено';

    status_refund_request_rejected_u.status_including_deal_type = 'status_refund_request_rejected_u';
    status_refund_request_rejected_u.status_name_including_deal_type = 'Предложение отказа от услуги отклонено';

    status_refund_request_accepted_t.status_including_deal_type = 'status_refund_request_accepted_t';
    status_refund_request_accepted_t.status_name_including_deal_type = 'Предложение возврата принято';

    status_refund_request_accepted_u.status_including_deal_type = 'status_refund_request_accepted_u';
    status_refund_request_accepted_u.status_name_including_deal_type = 'Предложение отказа от услуги принято';

    // Собираем воедино все массивы
    return [...arr7, status_refund_request_t, status_refund_request_u, status_refund_request_rejected_t, status_refund_request_rejected_u, status_refund_request_accepted_t, status_refund_request_accepted_u, ...arr8];
};

/**
 * Добавляем дополнительное разбиение ролей в объекты dispute в завимости от типа сделки, либо товар, либо услуга.
 * @param {Array} disputes - изначальный массив сделок полученный с бэка
 * @returns {Array} - новый споров включающий разбиение различных полей ориентируясь на тип сделки(услуга, товар)
 * @description Возвращается массив объектов споров
 * @typedef {Object} dispute
 * @property {string} status_including_deal_type - вместо статуса 'delivered' будут 'delivered_u' 'delivered_t'
 * @property {string} status_name_including_deal_type - имя статуса на русском вместо статуса 'Товар доставлен' будет товар 'Выполнено' и 'Получено'
 */
export const getDisputesWithFieldsSplittedByDealTypes = (disputes) => {
    return disputes.map(dispute => {
        const { deal_status, deal_type_ident } = dispute;

        /**
         * @property {string} status_including_deal_type - вместо статуса 'delivered' будут 'delivered_u' 'delivered_t'
         * @property {string} status_name_including_deal_type - имя статуса на русском вместо статуса 'Товар доставлен' будет товар 'Выполнено' и 'Получено'
         * @property {string} deal_closed_milliseconds - дата закрытия сделки в секундах
         */
        let status_including_deal_type = deal_status.status;
        let status_name_including_deal_type = deal_status.name;
        // перевод даты окончания сделки в секунды для работы фильтра
        let deal_closed_date_by_contract = dispute.deal_closed_date_by_contract;

        let deal_closed_milliseconds = setDateToSeconds(deal_closed_date_by_contract);

        if (deal_status.status === 'delivered') {
            if (deal_type_ident === 'U') {
                status_including_deal_type = 'delivered_u';
                status_name_including_deal_type = 'Услуга выполнена';
            } else {
                status_including_deal_type = 'delivered_t';
                status_name_including_deal_type = 'Товар доставлен';
            }
        }
        dispute.deal_status['status_including_deal_type'] = status_including_deal_type;
        dispute.deal_status['status_name_including_deal_type'] = status_name_including_deal_type;
        dispute['deal_closed_milliseconds'] = deal_closed_milliseconds;

        return dispute;
    });
};

/**
 * Добавляем дату создания платежки в секундах для работы фильтра даты в таблице проблемных платежек у оператора.
 * @param {Array} payments - изначальный массив проблемных платежек, полученный с бэка
 * @property {string} created_in_milliseconds - значение даты создания платежки в секундах
 */
export const getPaymentOrdersWithCreatedInMillisecondsField = (payments) => {
    return payments.map(payment => {
        const { created } = payment;

        let created_milliseconds = setDateToSeconds(created);

        payment['created_milliseconds'] = created_milliseconds;

        return payment;
    });
};

/**
 * Добавляем дату рождения в секундах для работы фильтра даты в таблице индивидуальных предпринимателей.
 * @param {Array} soleProprietors - изначальный массив ИП, полученный с бэка
 * @property {string} birth_date_in_milliseconds - значение даты рождения в секундах
 */
export const getSoleProprietorsWithBirthDateInMillisecondsField = (soleProprietors) => {
    return soleProprietors.map(proprietor => {
        const { birth_date } = proprietor;

        let birth_date_milliseconds = setDateToSeconds(birth_date);

        proprietor['birth_date_milliseconds'] = birth_date_milliseconds;

        return proprietor;
    });
};

/**
 * Добавляем дату создания сделки и срока выполнения в секундах для работы фильтра даты в таблице сделок с истекающим сроком у оператора.
 * @param {Array} deadlineDeals - изначальный массив сделок с истекающим сроком, полученный с бэка
 * @property {string} created_in_milliseconds - значение даты создания в секундах
 * @property {string} date_of_deadline_in_milliseconds - значение срока выполнения в секундах
 */
export const getDeadlineDealsWithCreatedInMillisecondsField = (deadlineDeals) => {
    return deadlineDeals.map(deadlineDeal => {
        const { created, date_of_deadline } = deadlineDeal;
        const created_date = created.split(' ')[0].split('-').join('.');
        const deadline = date_of_deadline.split(' ')[0].split('-').join('.');

        let created_milliseconds = setDateToSeconds(created_date);
        let date_of_deadline_in_milliseconds = setDateToSeconds(deadline);

        deadlineDeal['created_milliseconds'] = created_milliseconds;
        deadlineDeal['date_of_deadline_in_milliseconds'] = date_of_deadline_in_milliseconds;

        return deadlineDeal;
    });
};

/**
 * Добавляем дату создания сделки и срока выполнения в секундах для работы фильтра даты в таблице сделок с истекающим сроком у оператора.
 * @param {Array} deadlineDeals - изначальный массив сделок с истекающим сроком, полученный с бэка
 * @property {string} created_in_milliseconds - значение даты создания в секундах
 * @property {string} date_of_deadline_in_milliseconds - значение срока выполнения в секундах
 */
export const getDeadlineDealsWithDateOfDeadlineInMillisecondsField = (deadlineDeals) => {
    return deadlineDeals.map(deadlineDeal => {
        const { date_of_deadline } = deadlineDeal;
        const deadline = date_of_deadline.split(' ')[0].split('-').join('.');

        let date_of_deadline_in_milliseconds = setDateToSeconds(deadline);

        deadlineDeal['date_of_deadline_in_milliseconds'] = date_of_deadline_in_milliseconds;

        return deadlineDeal;
    });
};

/**
 * Функция перевода даты в секунды
 */
const setDateToSeconds = (date) => {
    let new_date = new Date(date.split(".").reverse().join(","));
    let dd = new_date.getDate();
    let mm = new_date.getMonth() + 1;
    let yy = new_date.getFullYear();
    let newDate = yy + ',' + mm + ',' + dd;
    return Date.parse(newDate) / 1000;
};

/**
 * Добавляет дополнительное полю элементу
 * @param {Array} array - массив состоящий из объектов, каждому из которых нужно добавить доп.поле
 * @param {string} fieldName - название поля
 * @param fieldValue - значение нового поля
 * @returns {Array}
 */
export const addKeyToObjectsInArray = (array = [], fieldName, fieldValue) => {
    /**
     * @param {Object} element
     */
    Array.isArray(array) && array.forEach(element => {
        element[fieldName] = fieldValue
    });

    return array;
};

/**
 * @param {Object} request_types
 * @returns {Array}
 */
export const addTypeToDisputeRequests = (request_types) => {
    const array = [];
    for (let [type, requests] of Object.entries(request_types)) {
        const new_requests = addKeyToObjectsInArray(requests, 'request_type', type) || [];
        array.push(new_requests);
    }
    return array;
};

export const checkControlErrors = (obj) => {
    for (let key in obj) {
        if (obj[key] === true) return key;                      //key == true
        else if (obj[key] === null) return null;
    }
    return false;
};

export const checkControlHints = (obj) => {
    for (let key in obj) {
        if (obj[key] === true) return key;                      //key == true
        else if (obj[key] === null) return null;
    }
    return false;
};

/**
 * @param {string} string
 * @returns {string} - строка проведенная к формату Первой загловоной буквы и из которой вырезаны все символы кроме букв
 */
export const replaceToTextCapitalize = (string) => (string.charAt(0).toUpperCase() + string.slice(1)).replace(/[\d`~!@#$%^&*()_|+=?;:'",.<>\{\}\[\]\\\/№§]/g, '').replace(/[Ё]/g, 'Е').replace(/[ё]/g, 'е');

/**
 * замена буквы ё на е
 * @param {string} string
 * @returns {string} - строка, в которой все буквы Ё(ё) заменены на Е(е)
 */
export const replaceRusLetterE = (string) => (string.replace(/[Ё]/g, 'Е').replace(/[ё]/g, 'е'));

/**
 * Вырезает все символы из строки кроме цифр
 * @param {string} string
 * @returns {string} - строка не содержащая любые символы кроме цифр
 */
export const textOnlyNumbers = (string) => string.replace(/\D/g, '');

/**
 * Разбивает строку по словам и возвращает их количество
 * @param {string} string
 * @returns {number} - количество слов в строке
 */
export const wordsCountInString = (string) => {
    string = string.replace(/(^\s*)|(\s*$)/gi, ''); // exclude  start and end white-space
    string = string.replace(/[ ]{2,}/gi, ' '); // 2 or more space to 1
    string = string.replace(/\n /, '\n'); // exclude newline with a start spacing
    return string.split(' ').length;
};

// Добавляет пробелы между разрядами числа
export const addSpacesBetweenThousands = (value) => {
    return String(value).replace(/[^\d.]+|(\.\d{2})\d*$/g, '$1').replace(/\d(?=(?:\d{3})+(?!\d))/g, "$& ");
};

// Форматирование БИК - удаляет всё кроме 04 из начала значения
export const bikTextFormat = (value, prevValue) => {
    let first_symbol = value[0] || '0';
    let second_symbol = value[1] || '4';

    first_symbol = first_symbol === '0';
    second_symbol = second_symbol ==='4';
    if (first_symbol && second_symbol) {
        return textOnlyNumbers(value);
    } else {
        return textOnlyNumbers(prevValue);
    }
};

/**
 * @return {string} - возвращает сегодняшнюю дату в формате dd.mm.yyyy
 */
export const getDateNow = () => {
    const today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    const yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10){
        mm = '0' + mm;
    }
    return `${dd}.${mm}.${yyyy}`;
};

/**
 * @param {string} date - необязательный параметр даты в формате dd.mm.yyyy, если он не передан берем дату сейчас
 * @return {string} - возвращает дату сейчас в формате 21 Июня 2018
 */
export const getDateNowInPrettyFormat = (date = getDateNow()) => {
    const [day, month, year] = date.split('.');

    const months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];

    // Получаем по ключу имя месяца, для этого вычитаем из него 1, т.к. первый индекс массива 0
    return `${day} ${months[Number(month) - 1]} ${year}`;
};

/**
 * Функция для поддержания единого стиля вывода имени физического лица
 * @param {string} first_name
 * @param {string} last_name
 * @param {string} secondary_name
 * @return {string}
 */
export const getNaturalPersonName = (first_name, last_name, secondary_name) => {
    return `${last_name} ${first_name} ${secondary_name}`
};

/**
 * Функция для получения параметра из текущего URL по имени параметра
 * @param {string} url_params
 * @param {string} name
 * @return {string}
 */
export const getUrlParameter = (url_params, name) => {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    let regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    let results = regex.exec(url_params);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

/**
 * Функция для установления в заданном URL параметра с заданным именем в заданное значение
 * @param {string} url
 * @param {string} name
 * @param {string} value
 * @return {string}
 */
export const setUrlParameter = (url, name, value) => {
    let regex = new RegExp("([?&])" + name + "=.*?(&|$)", "i");
    let separator = url.indexOf('?') !== -1 ? "&" : "?";
    if (url.match(regex)) {
        return url.replace(regex, '$1' + name + "=" + value + '$2');
    }
    else {
        return url + separator + name + "=" + value;
    }
};

export const getCookie = (name) => {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

export const readAndDeleteCookie = (name) => {
    const cookie = getCookie(name);
    if (cookie) {
        deleteCookie(name);
        return cookie;
    }
    return null;
};

export const setCookie = (name, value, options) => {
    options = options || {};

    let expires = options.expires;

    if (typeof expires == "number" && expires) {
        let d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    let updatedCookie = name + "=" + value;

    for (let propName in options) {
        updatedCookie += "; " + propName;
        let propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
};

export const deleteCookie = (name) => {
    setCookie(name, "", {
        expires: -1
    })
};

/**
 * @param {Object} point - объект с данными о пункте dpd
 * @returns {string} - название пункта
 */
export const getDpdPointAddress = (point) => {
    if (point) {
        let name = '';
        const street_abr = point.streetAbr;
        const street = point.street;
        const house = point.house;
        const structure = point.structure;

        if (street) {
            if (street_abr) {
                name += street_abr + '. '
            }
            name += street;
        }
        if (house) {
            name += ', д. ' + house;
            if (structure) {
                name += '/' + structure;
            }
        }

        return name;
    }
    return '';
};

export const filterDpdPointsByDealRole = (point, deal_role) => {
    let result;
    if (point) {
        if (point.type === 'TERMINAL') {
            result = point;
        } else if (point['schedules']) {
            point['schedules'].forEach((schedule) => {
                if (deal_role === 'customer') {
                    if (schedule.type === 'SelfDelivery') {
                        result = point;
                    }
                } else if (deal_role === 'contractor') {
                    if (schedule.type === 'SelfPickup') {
                        result = point;
                    }
                }
            });
        }
    }


    return result;
};

/**
 * Возвращает массив расписаний работы точки приёма\отправки
 * @param pickPoint
 * @param dealRole
 * @returns {Array}
 */
export const getPickPointShedule = (pickPoint, dealRole) => {
    let schedule_array = [];

    if (pickPoint['schedules']) {
        for (let schedule of pickPoint['schedules']) {
            if (dealRole === 'customer') {
                if (schedule.type === 'SelfDelivery') {
                    if (schedule['timetables']) {
                        schedule_array = schedule['timetables'];
                    }
                }
            } else if (dealRole === 'contractor') {
                if (schedule.type === 'SelfPickup') {
                    if (schedule['timetables']) {
                        schedule_array = schedule['timetables'];
                    }
                }
            }
        }
    }

    return schedule_array;
};

/**
 *
 * @param dealRole
 * @returns {string}
 */
export const getPickPointNameByDealRole = (dealRole) => {
    return dealRole.toLowerCase() === 'contractor' ? 'приема' : 'самовывоза'
};

export const getLimitName = limit => {
    let limitName = '';

    const translate = {
        weight: 'Вес',
        width: 'Ширина',
        length: 'Длина',
        height: 'Высота',
        dimension_sum: 'Сумма измерений'
    };

    const units = {
        weight: 'кг',
        width: 'см',
        length: 'см',
        height: 'см',
        dimension_sum: ''
    };

    const limit_name = translate[limit[0]];
    const limit_value = limit[1];

    if (limit && limit_value && limit_name) {
        limitName = `${limit_name}: ${limit_value} ${units[limit[0]]}`;
    }

    return limitName;
};

export const cyrillicToLatin = (value, separator) => {
    const arrru = ['Я','я','Ю','ю','Ч','ч','Ш','ш','Щ','щ','Ж','ж','А','а','Б','б','В','в','Г','г','Д','д','Е','е','Ё','ё','З','з','И','и','Й','й','К','к','Л','л','М','м','Н','н', 'О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ы','ы','Ь','ь','Ъ','ъ','Э','э'];
    const arren = ['Ya','ya','Yu','yu','Ch','ch','Sh','sh','Sh','sh','Zh','zh','A','a','B','b','V','v','G','g','D','d','E','e','E','e','Z','z','I','i','J','j','K','k','L','l','M','m','N','n', 'O','o','P','p','R','r','S','s','T','t','U','u','F','f','KH','kh','C','c','Y','y','`','`','\'','\'','E', 'e'];

    for(let i=0; i<arrru.length; i++){
        let reg = new RegExp(arrru[i], "g");
        value = value.replace(reg, arren[i]);
    }

    value = value.replace(/[^A-Za-z0-9_]+/g, separator).replace(/(_+$)/g, '').toLowerCase().trim();

    return value;
};

export const getPhoneInMaskFormat = (phone) => {
    if (!phone || typeof phone !== 'string') return '';

    if (phone.includes('+')) return phone;
    return `+${phone}`;
};
