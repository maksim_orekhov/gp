import React from 'react';
import { checkControlErrors } from './Helpers';

const ShowError = (props) => {
        if (!checkControlErrors(props.existing_errors)) {            //если в объекте props.existing_errors только ключи false или null - не рендерить
            return null;
        }
        const rules = props.messages[checkControlErrors(props.existing_errors)];   //вычислить какие правила именно нарушены и присвоить в переменную

        return (
            <p className="note-red">
                {rules}
            </p>
        );
};

export default ShowError;