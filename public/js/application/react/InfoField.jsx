import React from 'react';

export default function InfoField({ label, value }) {
   return (
      <div className="InfoField">
         <div className="InfoField-Label">{label}</div>
         <div className="InfoField-Text">{value}</div>
      </div>
   );
}
