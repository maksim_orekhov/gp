import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

class DropDownField extends Component {
    static propTypes = {
        placeholder: PropTypes.string.isRequired,
        value: PropTypes.string,
        name: PropTypes.string,
        handleChange: PropTypes.func,
        handleOpen: PropTypes.func,
        handleClose: PropTypes.func,
        needToCloseOnOutsideClick: PropTypes.bool,
        handleClickOutside: PropTypes.func,
        items: PropTypes.arrayOf(PropTypes.shape({
            title: PropTypes.string.isRequired,
            value: PropTypes.string.isRequired
        }).isRequired),
        children: PropTypes.element,
    };

    static defaultProps = {
        value: '',
        items: [],
        needToCloseOnOutsideClick: true
    };

    state = {
        dropListIsOpen: false,
    };

    componentDidUpdate(prevProps, prevState){
        if(prevState.dropListIsOpen !== this.state.dropListIsOpen){
            this.state.dropListIsOpen && this.props.handleOpen && this.props.handleOpen();
            !this.state.dropListIsOpen && this.props.handleClose && this.props.handleClose();
        }
    }

    componentDidMount(){
        window.addEventListener('click', this.handleClickOutside);
    }

    componentWillUnmount(){
        window.removeEventListener('click', this.handleClickOutside);
    }

    toggleDropList = () => {
        this.setState({ dropListIsOpen: !this.state.dropListIsOpen });
    };

    closeDropList = () => {
        this.setState({ dropListIsOpen: false });
    };

    handleItemClick = (item) => {
        this.props.handleChange && this.props.handleChange(item);
        this.closeDropList();
    };

    getTitleForValue = (value) => {
        const activeItem = _.find(this.props.items, item => item.value === value);
        return activeItem ? activeItem.title : null;
    };

    handleClickOutside = event => {
        if(!this.state.dropListIsOpen){
            return true;
        }

        if(this.props.needToCloseOnOutsideClick && !this.dropDownFieldRef.contains(event.target)){
            this.closeDropList();
        }
        this.props.handleClickOutside && this.props.handleClickOutside(event, this.dropDownFieldRef, this.closeDropList);
    };

    setDropDownFieldRef = element => this.dropDownFieldRef = element;

    render(){
        const { value, placeholder, children, items } = this.props;
        if(!children && !items.length){
            return null;
        }
        return(
            <div className="dropdown-field" ref={this.setDropDownFieldRef}>
                <div className="dropdown-field__selected-value" onClick={this.toggleDropList}>
                    { items.length && value && value.length ? this.getTitleForValue(value) : <span className="dropdown-field__placeholder">{placeholder}</span> }
                </div>
                <div className={`dropdown-field__items-list ${this.state.dropListIsOpen ? 'is-open' : ''}`}>
                    { children ? children : items.map((item, index) => <div key={item.value} className="dropdown-field__item" onClick={() => this.handleItemClick(item)}>{item.title}</div>) }
                </div>
            </div>
        );
    }
}

export default DropDownField;