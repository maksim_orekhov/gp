import React from 'react';
import PropTypes from 'prop-types';

export default class Popup extends React.Component {
    constructor(props) {
        super(props);

        this.handlePopupContentClick = this.handlePopupContentClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    static propTypes = {
        popupClassName: PropTypes.string,
        close: PropTypes.func.isRequired
    };

    static defaultProps = {
        popupClassName: ''
    };

    handlePopupContentClick(e) {
        e.stopPropagation();
    }

    componentDidMount() {
        if (this.props.not_open_on_mount) return;
        this.handleOpen();
    }

    componentWillUnmount() {
        const body = document.getElementsByTagName('body');
        const html = document.getElementsByTagName('html');
        if($(body).hasClass('overflow-y-hidden')) {
            $(body).removeClass('overflow-y-hidden');
            $(html).removeClass('overflow-y-hidden');
        }
    }

    handleClose() {
        const { close } = this.props;
        const body = document.getElementsByTagName('body');
        const html = document.getElementsByTagName('html');
        if($(body).hasClass('overflow-y-hidden')) {
            $(body).removeClass('overflow-y-hidden');
            $(html).removeClass('overflow-y-hidden');
        }
        close && close();
    }

    handleOpen() {
        const popup = document.getElementsByClassName('Popup-Container');
        if($(popup).hasClass('open')) {
            const body = document.getElementsByTagName('body');
            $(body).addClass('overflow-y-hidden');
            const html = document.getElementsByTagName('html');
            $(html).addClass('overflow-y-hidden');
        }
    }

    render() {
        const { children, popupClassName } = this.props;
        return (
            <div className="Popup-Container open" onClick={this.handleClose}>
                    <div className={`Popup ${popupClassName}`} onClick={this.handlePopupContentClick}>
                        <div className="Popup-Content">
                            { React.Children.map(children, child => {
                                return React.cloneElement(child, {
                                    closePopup: this.handleClose
                                });
                            }) }
                        </div>

                        <div className="button-popup-close" onClick={this.handleClose}>
                            <svg width='36' height='36' viewBox='0 0 36 36' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                <circle cx='18' cy='18' r='17.5' fill='#fff' stroke='#C5D0DE' />
                                <path fillRule='evenodd' clipRule='evenodd' d='M8.61707 11.4113L6.00499 8.80093L3.38293 11.4193C2.61008 12.1936 1.35397 12.1936 0.581128 11.4193C-0.193709 10.6469 -0.193709 9.39365 0.581128 8.61932L3.2012 5.999L0.581128 3.38068C-0.193709 2.60635 -0.193709 1.35307 0.581128 0.580742C1.35397 -0.193581 2.61008 -0.193581 3.38293 0.580742L6.00499 3.19907L8.61707 0.588724C9.38992 -0.185598 10.644 -0.185598 11.4189 0.588724C12.1917 1.36305 12.1917 2.61633 11.4189 3.38866L8.80679 6.001L11.4189 8.61134C12.1937 9.38367 12.1937 10.6389 11.4189 11.4113C10.646 12.1836 9.38992 12.1836 8.61707 11.4113Z'
                                      transform='translate(12 12)' fill='#C5D0DE' />
                            </svg>
                        </div>
                    </div>
            </div>
        )
    }
}
