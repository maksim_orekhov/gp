import React from 'react';
import DisputeRequest from './DisputeRequest';
import { getIdFromUrl, addTypeToDisputeRequests } from './Helpers';
import URLS from './constants/urls';
import MESSAGES from './constants/messages';
import { customFetch } from './Helpers';

export default class DisputeRequestList extends React.Component {
    constructor(props) {
        super(props);

        /**
         * @param {Array} requests - список всех реквестов
         * @param {string} deal_type - тип сделки либо 'U' - услуга, либо 'T' - товар
         * @param {boolean} is_operator - является ли пользователь оператором
         */
        this.state = {
            requests: [],
            deal_type: '',
            is_operator: false,
            is_allowed_arbitrage_confirm: false,
            is_allowed_tribunal_confirm: false,
            is_allowed_discount_confirm: false,
            is_allowed_refund_confirm: false,
            is_customer: null
        };

        this.handleOnSubmit = this.handleOnSubmit.bind(this);
    }

    componentWillMount() {
        this.init();
    }

    /**
     * Если is_need_update_request_list_collection изменился то значит это сигнал от ControllerDisputeRequests что нужно обновить коллекцию
     */
    componentWillReceiveProps(nextProps) {
        const { is_need_update_request_list_collection } = this.props;
        if (nextProps.is_need_update_request_list_collection !== is_need_update_request_list_collection) {
            this.init();
        }
    }

    /**
     * Получаем данные с сервера
     */
    init() {
        const idDeal = getIdFromUrl();

        customFetch(`${URLS.DEAL.FORM_INIT}?idDeal=${idDeal}`)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    console.log('DisputeRequestList', data);
                    let { arbitrage_requests, discount_requests, tribunal_requests, refund_requests, is_allowed_arbitrage_confirm, is_allowed_tribunal_confirm, is_allowed_discount_confirm, is_allowed_refund_confirm} = data.data.deal.dispute;
                    const {
                        deal: {
                            type_ident,
                            is_customer
                        },
                        user: {
                            is_operator
                        }
                    } = data.data;

                    // Добавляем дополнительное поле - request_type к каждому реквесту чтобы в дальнейшем можно было их различать
                    const request_types = {
                        arbitrage: arbitrage_requests,
                        discount: discount_requests,
                        tribunal: tribunal_requests,
                        refund: refund_requests
                    };

                    [arbitrage_requests, discount_requests, tribunal_requests, refund_requests] = addTypeToDisputeRequests(request_types);

                    //Собираем в один массив
                    const requests = [...arbitrage_requests, ...discount_requests, ...tribunal_requests, ...refund_requests];

                    this.setState({
                        requests,
                        deal_type: type_ident,
                        is_operator,
                        is_allowed_arbitrage_confirm,
                        is_allowed_tribunal_confirm,
                        is_allowed_discount_confirm,
                        is_allowed_refund_confirm,
                        is_customer
                    });
                } else if (data.status === 'ERROR') {
                    return Promise.reject('Ошибка при инициализации. Попробуйте обновить страницу.');
                }
            })
            .catch(error => console.log(error));
    }

    handleOnSubmit() {
        const { handleOnSubmit } = this.props;
        this.init();
        handleOnSubmit && handleOnSubmit();
    }

    isAllowedConfirm(request_type) {
        const { is_allowed_arbitrage_confirm, is_allowed_tribunal_confirm, is_allowed_discount_confirm, is_allowed_refund_confirm } = this.state;
        const confirms = {
            discount: is_allowed_discount_confirm,
            refund: is_allowed_refund_confirm,
            arbitrage: is_allowed_arbitrage_confirm,
            tribunal: is_allowed_tribunal_confirm
        };

        return confirms[request_type];
    }

    render() {
        const { requests, deal_type, is_operator, is_customer } = this.state;
        const { is_arbitrage_after_three_discount_rejects, is_arbitrage_after_refund_reject, is_arbitrage_after_tribunal_reject } = this.props;
        console.log(requests);
        return (
            <div>
                {
                    requests.map((request, i) => {
                        const { request_type } = request;

                        return (
                            <DisputeRequest
                                request={request}
                                deal_type={deal_type}
                                key={i}
                                handleOnSubmit={this.handleOnSubmit}
                                is_operator={is_operator}
                                is_allowed_confirm={this.isAllowedConfirm(request_type)}
                                is_customer={is_customer}
                                is_arbitrage_after_three_discount_rejects={is_arbitrage_after_three_discount_rejects}
                                is_arbitrage_after_refund_reject={is_arbitrage_after_refund_reject}
                                is_arbitrage_after_tribunal_reject={is_arbitrage_after_tribunal_reject}
                            />
                        )
                    })
                }
            </div>
        )
    }
};