import React from 'react';
import NAMES from './constants/names';

const DisputeRequestInfo = (props) => {
    const { confirm, request_type, deal_type, request_amount, is_author, is_customer,
        is_arbitrage_after_three_discount_rejects, is_arbitrage_after_refund_reject, is_arbitrage_after_tribunal_reject} = props;
    const info_field_text = deal_type === 'U' ? 'Отказ от услуги' : 'Возврат товара';

    /**
     * В зависимости от того был ли ответ по реквесту и какой ответ был меняем название лэйбла и цвет
     */
    let offer_title = 'Ваше предложение отклонено';
    if (!is_author) {
        offer_title = 'Предложение покупателя отклонено'
    }
    let offer_className = 'SubTitle text-red';
    if (confirm === null) {
        offer_className = 'SubTitle text-accent';
        //если решение о согласительной комиссии было создано без ведома участников сделки
        if (is_arbitrage_after_three_discount_rejects || is_arbitrage_after_refund_reject || is_arbitrage_after_tribunal_reject) {
            offer_title = 'Решение Guarant Pay';
        } else {
            if (is_author) {
                offer_title = 'Ваше предложение отправлено';
            } else {
                if (is_customer) {
                    // Т.к. он не автор и покупатель, то продавец отправил запрос
                    offer_title = deal_type === 'U' ? 'Исполнитель отправил предложение' : 'Продавец отправил предложение';
                } else {
                    // Т.к. он не автор и продавец, то покупатель отправил запрос
                    offer_title = deal_type === 'U' ? 'Заказчик отправил предложение' : 'Покупатель отправил предложение';
                }
            }
        }
    } else if (confirm.status === true) {
        if (is_arbitrage_after_three_discount_rejects || is_arbitrage_after_refund_reject || is_arbitrage_after_tribunal_reject) {
            offer_title = 'Предложение согласовано';
        } else {
            offer_title = 'Ваше предложение согласовано';
            if (!is_author) {
                offer_title = 'Предложение покупателя согласовано'
            }
        }
        offer_className = 'SubTitle text-green';
    }

    return (
        <div className="SolutionRequests-Status">
            <h2 className={offer_className}>{offer_title}</h2>
            <div className="InfoField">
                {
                    request_type !== 'refund' &&
                    <div className="InfoField-Text">{NAMES.DISPUTE_RESOLVE_LABELS[request_type]}</div>
                }
                {
                    request_type === 'refund' &&
                    <div className="InfoField-Text">{info_field_text}</div>
                }
            </div>
            {{
                tribunal: (
                    <p>После присвоения спору статуса Суд, в течение 10 дней Вам будет необходимо подтвердить данное решение, направив администрации копию обращения за судебной защитой.</p>
                ),
                refund: (
                    deal_type === 'T' && is_customer === true && <p>В срок не более 3-х рабочих дней Вам будет необходимо организовать доставку товара продавцу.</p>
                ),
                discount: (
                    <div>
                        <h2 className="SubTitle">Размер скидки, рублей:</h2>
                        <div className="InfoField">
                            <div className="InfoField-Text">{ request_amount }</div>
                        </div>
                    </div>
                )
            }[request_type]}
        </div>
    )
};

export default DisputeRequestInfo;

