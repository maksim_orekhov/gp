import React from 'react';

const DisputeResolveRequestsSwitcher = (props) => {
    const { activeTab, elementRules, list_items, isDropDownOpen, toggleDropDownOpen } = props;

    return (
        <div className="SelectImitation">
            <h2 className="SubTitle">Ваш вариант решения спора</h2>
            <div className="SelectImitation-Container">
                <label className="SelectImitation-Text" htmlFor="dropdown-input-solutions-request" onClick={toggleDropDownOpen}>
                    {elementRules[activeTab].label}
                </label>
                <input id="dropdown-input-solutions-request"
                       className="SelectImitation-Checkbox" type="checkbox" checked={isDropDownOpen} />
                <ul className="SelectImitation-Menu">{list_items}</ul>
            </div>
        </div>
    )
};

export default DisputeResolveRequestsSwitcher;
