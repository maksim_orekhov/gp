import React from 'react';
import FormLegalEntity from './forms/FormLegalEntity';
import Popup from './Popup';
import InfoField from './InfoField';
import { customFetch } from './Helpers';
import URLS from './constants/urls';

export default class InfoLegalEntity extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         legalEntity: '',
         isOpenedFormEdit: false,
      };
      this.getInfo = this.getInfo.bind(this);
      this.afterDelete = this.afterDelete.bind(this);
   }

   componentDidMount() {
      !this.props.isNested && this.getInfo();
   }

   getInfo() {
      customFetch(window.location)
          .then(data => {
              console.log(data);
              this.setState({
                  isOpenedFormEdit: false,
                  legalEntity: data.data.legalEntity,
              });
          })
          .catch(error => console.log(error));
   }

    afterDelete() {
        window.location = URLS.PROFILE;
    }

   componentWillReceiveProps(nextProps) {
      this.setState({
          legalEntity: nextProps.data
      });
   }

   toggleFormEdit(collectionItem) {
      this.setState({
         collectionItemEdit: collectionItem,
         isOpenedFormEdit: !this.state.isOpenedFormEdit
      });
   }

   render() {
      const { isOpenedFormEdit, legalEntity, collectionItemEdit } = this.state;
      const { name, legal_address, inn, kpp } = legalEntity;
      return (
          <div className="container">
              <div className="row nested-row">
                  <div className="col-xl-12">
                      <div className="CivilLawSubjectPage-Header">
                          <div className="TitleWithEditIcon">
                              <h1 className="TitleBlue">Юридическое лицо</h1>
                              <a className="IconEdit" onClick={() => this.toggleFormEdit(legalEntity)} />
                          </div>
                          <div className="CivilLawSubjectDataOverview">
                              <div className="CivilLawSubjectDataOverview-Column">
                                  <InfoField label="Наименование:" value={name} />
                                  <InfoField label="Юридический адрес:" value={legal_address} />
                              </div>
                              <div className="CivilLawSubjectDataOverview-Column">
                                  <InfoField label="ИНН:" value={inn} />
                                  <InfoField label="КПП:" value={kpp} />
                              </div>
                          </div>
                          {
                              isOpenedFormEdit &&
                              <Popup close={() => this.toggleFormEdit({})}>
                                  <FormLegalEntity
                                      data={legalEntity}
                                      editMode={true}
                                      afterSubmit={this.getInfo}
                                      afterDelete={this.afterDelete}
                                      item_id={collectionItemEdit}
                                  />
                              </Popup>
                          }
                      </div>
                  </div>
              </div>
          </div>
      );
   }
}
