import React from 'react';
import InfoLegalEntity from './InfoLegalEntity';
import TableDocuments from './tables/TableDocuments';
import TablePaymentMethods from './tables/TablePaymentMethods';
import { customFetch } from './Helpers';

export default class CardLegalEntity extends React.Component {

   constructor(props) {
      super(props);
      this.state = {
         civil_law_subject_id: '',
         legalEntity: '',
         documents: '',
         paymentMethods: '',
         page_isLoading: false,
      };

      this.getData = this.getData.bind(this);
   }

   componentDidMount() {
      this.getData();
      document.getElementById('CardLegalEntity').style.visibility = 'visible';
   }

    toggleIsLoading() {
        this.setState({
            page_isLoading: !this.state.page_isLoading
        });
    }

   getData() {
      this.toggleIsLoading();
      customFetch(window.location)
          .then(data => {
              console.log(data);
              const { civil_law_subject_id, legalEntity, documents, paymentMethods } = data.data;
              this.setState({
                  civil_law_subject_id,
                  legalEntity,
                  documents,
                  paymentMethods
              }, () => this.toggleIsLoading());
          })
          .catch(error => console.log(error));
   }

   render() {
      const { civil_law_subject_id, legalEntity, documents, paymentMethods, page_isLoading } = this.state;
      return (
         <div className={page_isLoading ? 'Preloader Preloader_solid Preloader_large' : null}>
            <InfoLegalEntity
               isNested={true}
               data={legalEntity}
            />
            <div className="CivilLawSubjectPage-Section">
               <TableDocuments
                  id={legalEntity.id}
                  isNested={true}
                  collection={documents}
               />
            </div>
            <div className="CivilLawSubjectPage-Section">
               <TablePaymentMethods
                  civil_law_subject_id={civil_law_subject_id}
                  isNested={true}
                  collection={paymentMethods}
               />
            </div>
         </div>
      );
   }
}
