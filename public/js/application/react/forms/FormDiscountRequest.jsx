import React from 'react';
import FormBase from './FormBase';
import ShowError from '../ShowError';
import DisputeDiscount from '../controls/ControlDisputeDiscount';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import { customFetch } from '../Helpers';

const classNames = require('classnames');

export default class FormDiscountRequest extends FormBase {
    constructor(props) {
        super(props);
        this.state = {
            /**
             * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
             * @example
             *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
             **/
            controls_server_errors: {
                csrf: null,
                amount_discount: null
            },
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            form: {
                csrf: props.csrf || '',
                amount_discount: ''
            },
            form_isValid: false,
            validation: {
                amount_discount_is_valid: false
            },
            form_isLoading: false
        };

        this.validation_props = [
            'amount_discount_is_valid'
        ];

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state
        this.form_name = 'form_discount_request'; // название формы для хранения данных в local_storage

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.getCsrf();
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    componentWillMount() {
        this.getFormFromLocalStorage();
    }

    getFormFromLocalStorage() {
        return new Promise((resolve, reject) => {
            if (localStorage && this.form_name && localStorage.hasOwnProperty(this.form_name)) {
                const form = JSON.parse(localStorage.getItem(this.form_name));

                this.setState({
                    form: {
                        ...this.state.form,
                        ...form
                    }
                }, () => {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    handleSubmit() {
        const { form_isValid } = this.state;
        const { csrf, amount_discount } = this.state.form;
        const { deal_id, handleOnSubmit } = this.props;

        if (form_isValid) {
            this.toggleIsLoading();
            customFetch('/discount/request', {
                method: 'POST',
                body: JSON.stringify({
                    csrf,
                    amount_discount,
                    type_form: 'discount-request-form',
                    deal: deal_id
                })
            })
                .then(data => {
                    this.toggleIsLoading();

                    if (data.status === 'SUCCESS') {
                        handleOnSubmit && handleOnSubmit(true); // true значит что нужно обновить реквест лист
                        // Если есть событие handleOnSubmit то не прячем прелоадер чтобы успела обновиться коллекция
                        !handleOnSubmit && this.toggleIsLoading();
                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(response => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                })
        }
    }

    render() {
        const { max_discount_amount } = this.props;
        const { form_isValid, form_isLoading } = this.state;
        const { amount_discount } = this.state.form;

        const SubmitButtonClassName = classNames({
            'ButtonApply': true,
            'Preloader Preloader_light': form_isLoading
        });

        return (
            <div className="FormLabelTop">
                <DisputeDiscount
                    label="Сделать скидку"
                    name="amount_discount"
                    placeholder="Размер скидки, рублей:"
                    value_prop={amount_discount}
                    max_discount_amount={max_discount_amount}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.amount_discount}
                />
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                <div className="ButtonsRow">
                    <button className={SubmitButtonClassName} onClick={this.handleSubmit} disabled={!form_isValid || form_isLoading} data-ripple-button="">
                        <span className="ripple-text">Отправить</span>
                    </button>
                </div>
            </div>
        )
    }
}