import React from 'react';
import Timer from '../Timer';
import Phone from '../../../landing/react/auth/forms/RegisterForm/controls/phone-input';
import ShowError from '../../../application/react/ShowError';
import URLS from '../constants/urls';
import { customFetch } from "../Helpers";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../constants/server_errors";
import FormCodeTimeout from "./FormCodeTimeout";

const classNames = require('classnames');

/**
 * Универсальная форма для добавления или смены номера телефона
 */
export default class FormPhoneManagement extends FormCodeTimeout {
    constructor(props) {
        super(props);

        /**
         * @param {string} csrf
         * @param {boolean} form_isLoading - флаг что идет запрос на сервер, запускающий прелоадер
         * @param {boolean} form_isValid
         * @param {string} formError - ошибка формы, полученная с бэка при отправке данных
         * @param {string} current_phone - текущей номер пользователя
         * @param {string} new_phone - новый телефон пользователя, получаемый из инпута формы
         * @param {number} request_period - минимальный интверал между двумя отправленными кодами, если в этот промежуток отправить еще один код то будет ошибка
         * @param {number} last_code_created_at - дата в секундах последнего отправленного кода
         * @param {number} timer_time - время через которое разблокируется отправка следующего кода
         * @param {boolean} is_allow_to_send_code - доступна ли кнопка изменения телефона
         * @param {boolean} is_limit_phone_code_change - говорит о том что был достигнут лимит на коды подтверждения
         */
        this.state = {
            ...this.state,
            formError: '',
            form_isLoading: false,
            form_isValid: false,
            is_limit_phone_code_change: false,
            form_preloader: false,
            // Validation props
            validation: {
                phone_is_valid: false
            },
            // -----------------------------
            form: {
                current_phone: '',
                phone: '',
                csrf: '',
            },
            controls_server_errors: {
                csrf: null,
                phone: null
            },
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS
        };

        // Поля контролов для автоматической валидации формы
        this.validation_props =
            [
                'phone_is_valid'
            ];

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.toggleTimer = this.toggleTimer.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        this.formInit();
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    formInit() {
        const { form_type } = this.props;
        customFetch(URLS.PROFILE)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const {
                        current_phone,
                        user_allowed_request_phone_change_code_status,
                        csrf
                    } = data.data;

                    let form = {};

                    // Так как вложенная структура стейта используем rest оператор как аналог immutable библиотеке
                    if (form_type === 'add') {
                        form = {
                            ...this.state.form,
                            csrf
                        };

                        this.setState({ form });
                    } else if (form_type === 'edit') {
                        form = {
                            ...this.state.form,
                            current_phone: `+${current_phone}`, // Добавляем вручную плюс к номеру телефона, иначе бэк выдает ошибку
                            csrf
                        };

                        this.setState({
                                form,
                                user_allowed_request_phone_change_code_status,
                            },
                            // В этом методе анализируем user_allowed_request_phone_change_code_status и ставим ограничения на отправку если необходимо
                            () => this.checkUserAllowedRequest()
                        );
                    }
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => this.setFormError('form_init_fail', true));
    }

    /**
     * Анализируем user_allowed_request_phone_change_code_status, с помощью него смотрим доступны ли нам операции на смену телефона.
     */
    checkUserAllowedRequest() {
        const { allowed, violation_type, request_period, last_code_created_at } = this.state.user_allowed_request_phone_change_code_status;
        if (!allowed) {
            /**
             * min_interval - Пришло предупреждение с бэка что следующий отправленный код вызовет ошибку
             * и нужно подождать определенное время, т.е. запустить таймер
             */
            if (violation_type === 'min_interval') {
                this.setState({
                    last_code_created_at: last_code_created_at, // Переводим в милисекунду
                    request_period: request_period
                }, () => this.checkAllowingToSendCodeTimeOut());

            } else if (violation_type === 'limit_by_code_type') { // Пришло сообщение что достингут лимит отправки кодов подтверждения
                this.setState({
                    is_limit_phone_code_change: true
                })
            }
        }
    }

    handleSubmit() {
        const { phone, csrf, current_phone } = this.state.form;
        const { url, form_type } = this.props;

        if (this.state.form_isValid) {
            let request_body = '';

            if (form_type === 'add') {
                request_body = JSON.stringify({
                    csrf,
                    phone: phone
                });
            } else if (form_type === 'edit') {
                request_body = JSON.stringify({
                    csrf,
                    current_phone,
                    new_phone: phone
                });
            }
            this.toggleIsLoading();
            customFetch(url, {
                method: 'POST',
                body: request_body
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        const { handleNextStep } = this.props;

                        handleNextStep && handleNextStep(form_type+'_phone');
                        handleNextStep && this.toggleFormPreloader();
                        this.toggleIsLoading();

                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    toggleFormPreloader() {
        this.setState({
            form_preloader: !this.state.form_preloader
        });
    }

    render() {
        const { form_isLoading, form_isValid, is_allow_to_send_code, timer_time, is_limit_phone_code_change, form_preloader } = this.state;
        const { button_label, form_label } = this.props;

        const SubmitButtonClassName = classNames({
            'FormVerification-Button ButtonApply': true,
            'Preloader Preloader_light': form_isLoading
        });

        return (
            <div className={"FormLabelTop" + (form_preloader ? ' Preloader Preloader_solid' : '')}>
                <button className="ButtonClose js-verification-close" />
                <h2 className="FormVerification-Title">{form_label}</h2>
                {
                    !is_limit_phone_code_change ?
                        <div>
                            <div className='Input-Wrapper'>
                                <Phone
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleComponentValid}
                                    validateOnChange={true}
                                    name="phone"
                                    form_control_server_errors={this.state.controls_server_errors.phone}
                                    componentValue={this.handleChange}
                                    register={false}
                                />
                            </div>
                            {
                                is_allow_to_send_code
                                    ?
                                    <button
                                        className={SubmitButtonClassName}
                                        disabled={!form_isValid || form_isLoading}
                                        onClick={this.handleSubmit}>
                                        <span className="ripple-text">{button_label}</span>
                                    </button>
                                    :
                                    <p>
                                        Вы сможете изменить номер телефона через <span className="TextAccent"> {/* span на этой строке чтобы сохранить пробел*/}
                                        <Timer
                                            start={timer_time}
                                            expired={this.toggleTimer}
                                            isShowTime={true}
                                        />
                                        </span> секунд.
                                    </p>
                            }
                            <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                        </div>
                        :
                        <p className="TextImportant">Следующий запрос на смену телефона можно будет сделать через 15 минут.</p>
                }
            </div>
        )
    }
}