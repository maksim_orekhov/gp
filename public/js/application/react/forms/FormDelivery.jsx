import React from 'react';
import {customFetch, getDateNowInPrettyFormat, getIdFromUrl, getPhoneInMaskFormat} from "../Helpers";
import URLS from '../constants/urls';
import ShowError from '../ShowError';
import FormDeal from './formsDeal/FormDeal';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import {DELIVERY_TYPES_NAMES} from "../constants/names";

export default class FormDelivery extends FormDeal {

    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {
                delivery: null
            },
            disabled_controls: {
                delivery: false
            },
            // -----------------------------
            form: {
                csrf: '',
                delivery: {
                    service_type: '',
                    service: {},
                    service_dpd: {
                        payment_type: 'ОУО',
                        service_code: 'PCL',
                        parcel_quantity: 1,
                        delivery_time_period: '9-18',
                        pickup_time_period: '9-18',
                        cargo_category: 'Товары народного потребления (без ГСМ и АКБ)',
                    },
                    terms_of_delivery: ''
                },
            },
            validation: {
                delivery_is_valid: false
            },
            user_email: '',
            user_phone: '',
            form_isValid: false,
            form_isPreloader: false,
            delivery_service_types: {},
            deal_types: {},
            update_collection_civil_law_subjects: 0, // Флаг того что нужно обновить коллекцию в компоненте, для активации повышаем на 1
        };
        const idDeal = getIdFromUrl();
        this.form_name = `form_delivery_${idDeal}`; // название формы для хранения данных в local_storage

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        if (!this.props.is_nested) {
            this.toggleIsLoading('form_isPreloader');
            this.formInit();
        } else {
            this.setState({
                form: {
                    ...this.state.form,
                    csrf: this.props.csrf,
                    delivery: this.props.delivery
                },
                delivery_service_types: this.props.delivery_service_types,
                deal_role: this.props.deal_role
            });
        }
        this.getFormFromLocalStorage();
    }

    clearLocalStorage() {
        this.clearFormLocalStorage();
    }

    getFormFromLocalStorage() {
        return new Promise((resolve, reject) => {
            if (localStorage && this.form_name && localStorage.hasOwnProperty(this.form_name)) {
                const form = JSON.parse(localStorage.getItem(this.form_name));
                this.setState({
                    formHasDataAtLocalStorage: true,
                    form: {
                        ...this.state.form,
                        ...form
                    }
                }, () => {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    getFormInitUrl() {
        const idDeal = getIdFromUrl();
        return `${URLS.DEAL.FORM_INIT}?idDeal=${idDeal}`;
    }

    handleChange(name, value) {
        this.props.handleComponentChange && this.props.handleComponentChange(value);
        this.setState({
            form: {
                ...this.state.form,
                [name]: value
            }
        }, () => this.saveFormToLocalStorage());
    }

    successAjaxFormInit(data) {
        console.log('formInit', data);
        const {
            csrf,
            delivery_service_types,
            user = {},
            deal_form,
            deal
        } = data.data;
        const { deal_role, is_owner } = deal_form;
        const form = {...this.state.form};
        const { update_collection_civil_law_subjects } = this.state;

        form['csrf'] = csrf;

        let data_to_state = {
            delivery_service_types: delivery_service_types || {},
            update_collection_civil_law_subjects: update_collection_civil_law_subjects + 1,
            form: {
                ...form
            },
            deal_role,
            is_owner,
            user_email: user.email || '',
            user_phone: getPhoneInMaskFormat(user.phone)
        };
        // Данные о текущей сделке
        // - доставка
        let delivery;
        if (deal_form.delivery) {
            delivery = {...deal_form.delivery};
            delete deal_form.delivery;

            delivery = this.getInitDeliveryData(delivery, delivery_service_types);
        } else {
            delete deal_form.delivery;
        }

        if (delivery && delivery.hasOwnProperty('service_type') &&
            this.getDeliveryServiceTypeName(delivery.service_type, delivery_service_types) === DELIVERY_TYPES_NAMES.DPD &&
            deal && typeof deal === 'object'
            && deal.hasOwnProperty('delivery') && deal.delivery && typeof deal.delivery === 'object'
            && deal.delivery.hasOwnProperty('service') && deal.delivery.service && typeof deal.delivery.service === 'object'
        ) {
            this.setDpdDeliveryExtendedData(deal.delivery.service, delivery, deal_form.deal_role);
        }

        if (delivery) {
            data_to_state.form.delivery = {
                ...this.state.form.delivery,
                ...delivery,
                service: {
                    ...this.state.form.delivery.service,
                    ...delivery.service,
                },
                service_dpd: {
                    ...this.state.form.delivery.service_dpd,
                    ...delivery.service_dpd
                }
            };
        }

        this.oldState = {...this.state.form, ...data_to_state.form};

        this.setState(data_to_state, () => {
            // this.getFormFromLocalStorage();
            // this.props.setDataToParentState && this.props.setDataToParentState(data_to_state);
            this.toggleIsLoading('form_isPreloader');
        });
    }

    getSubmitUrl() {
        const idDeal = getIdFromUrl();
        return `/deal/${idDeal}/delivery`;
    }

    getDataFromState() {
        const data = {...this.state.form};

        this.setDeliverySubmitData(data);

        return this.createFormData(data);
    }

    checkAllControlsAreValid() {
        const { delivery_is_valid } = this.state.validation;

        const form_isValid = (
            delivery_is_valid
        );

        if (this.state.form_isValid === form_isValid) return;

        this.setState({
            form_isValid
        });

        this.props.handleComponentValidation && this.props.handleComponentValidation('delivery_is_valid', form_isValid);
    }

    render() {
        const { deal_role, is_owner, form_isSubmitting, form_isValid, form_isPreloader } = this.state;
        const counteragent_role = deal_role === 'customer' ? 'продавец' : 'покупатель';

        return (
            <div className={`FormLabelTop ${form_isPreloader ? 'Preloader Preloader_solid' : ''}`}>
                {
                    !is_owner &&
                    <p className="TextMarked TextMarked_info">
                        При создании сделки {`${counteragent_role}`} указал доставку
                        «DPD курьерская». Если вы согласны, заполните форму ниже, иначе измените тип доставки.
                    </p>
                }
                {
                    this.getDeliveryControl({
                        can_added_counteragent_data: false,
                        deal_role
                    })
                }
                {
                    !this.props.is_nested
                    && <div className="row nested-row ButtonsRow">
                        <div className="col-xl-6 col-sm-4">
                            <button
                                onClick={this.handleSubmit}
                                className={`ButtonApply ${form_isSubmitting ? 'Preloader Preloader_light' : ''}`}
                                disabled={!form_isValid || form_isSubmitting}
                                data-ripple-button=""
                            >
                                <span className="ripple-text">Далее</span>
                            </button>
                        </div>
                    </div>
                }
            </div>
        );
    }
};
