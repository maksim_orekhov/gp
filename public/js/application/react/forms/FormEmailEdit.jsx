import React from 'react';
import Timer from '../Timer';
import Email from '../../../landing/react/auth/forms/RegisterForm/controls/email-input.jsx';
import ShowError from '../../../application/react/ShowError';
import URLS from '../constants/urls';
import { customFetch } from "../Helpers";
import MESSAGES from "../constants/messages";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../constants/server_errors";
import FormCodeTimeout from "./FormCodeTimeout";

const classNames = require('classnames');

/**
 * Форма смены email принимающая новый email
 */
export default class FormEmailEdit extends FormCodeTimeout {
    constructor(props) {
        super(props);

        this.state = {
            ...this.state,
            formError: '',
            form_isLoading: false,
            form_isValid: false,
            form_preloader: false,
            form: {
                current_email: '',
                email: '',
                csrf: '',
            },
            controls_server_errors: {
                csrf: null,
                email: null
            },
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                email_is_valid: false
            }
            // -----------------------------
        };

        // Поля контролов для автоматической валидации формы
        this.validation_props =
            [
                'email_is_valid'
            ];

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.toggleTimer = this.toggleTimer.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        this.formInit();
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    formInit() {
        customFetch(URLS.PROFILE)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const {
                        current_email,
                        email_confirmation_token_request_period,
                        token_created_date,
                        csrf
                    } = data.data;

                    // Так как вложенная структура стейта используем rest оператор как аналог immutable библиотеке
                    const form = {
                        ...this.state.form,
                        current_email,
                        csrf
                    };


                    this.setState({
                            form,
                            request_period: email_confirmation_token_request_period,
                            last_code_created_at: token_created_date
                        },
                        // В этом методе анализируем user_allowed_request_phone_change_code_status и ставим ограничения на отправку если необходимо
                        () => this.checkAllowingToSendCodeTimeOut()
                    );
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => this.setFormError('form_init_fail', true));
    }

    handleSubmit() {
        const { email, csrf, current_email } = this.state.form;

        if (this.state.form_isValid) {
            this.toggleIsLoading();
            customFetch(URLS.EMAIL.EDIT, {
                method: 'POST',
                body: JSON.stringify({
                    new_email: email,
                    csrf,
                    current_email
                })
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        const { handleNextStep } = this.props;

                        handleNextStep && handleNextStep('edit_email');
                        handleNextStep && this.toggleFormPreloader();
                        this.toggleIsLoading();

                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data);
                        this.toggleIsLoading();
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    toggleFormPreloader() {
        this.setState({
            form_preloader: !this.state.form_preloader
        });
    }

    render() {
        const { form_isLoading, form_isValid, is_allow_to_send_code, timer_time, form_preloader, email } = this.state;

        const SubmitButtonClassName = classNames({
            'FormVerification-Button ButtonApply': true,
            'Preloader Preloader_light': form_isLoading
        });

        return (
            <div className={"FormLabelTop" + (form_preloader ? ' Preloader Preloader_solid' : '')}>
                <button className="ButtonClose js-verification-close"/>
                <h2 className="FormVerification-Title">Изменить эл.почту</h2>
                <div>
                    <div className='Input-Wrapper'>
                        <Email
                            register={false}
                            handleComponentChange={this.handleChange}
                            email={email}
                            name="email"
                            form_control_server_errors={this.state.controls_server_errors.email}
                            handleComponentValidation={this.handleComponentValid}
                            componentValue={this.handleChange}
                        />
                    </div>
                    {
                        is_allow_to_send_code
                            ?
                            <button
                                className={SubmitButtonClassName}
                                disabled={!form_isValid || form_isLoading}
                                onClick={this.handleSubmit}
                                data-ripple-button="">
                                <span className="ripple-text">Изменить</span>
                            </button>
                            :
                            <p>
                                Вы сможете изменить адрес электронной почты через <span
                                className="TextAccent"> {/* span на этой строке чтобы сохранить пробел*/}
                                <Timer
                                    start={timer_time}
                                    expired={this.toggleTimer}
                                    isShowTime={true}
                                />
                                </span> секунд.
                            </p>
                    }
                    <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                </div>
            </div>
        )
    }
}