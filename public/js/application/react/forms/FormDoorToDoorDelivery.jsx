import React from 'react';
import Street from '../controls/ControlStreet';
import House from '../controls/ControlHouse';
import Apartment from '../controls/ControlApartment';
import CitySearch from '../controls/ControlDpdCitySearch';
import { customFetch } from "../Helpers";
import URLS from '../constants/urls';
import ShowError from '../ShowError';
import FormBase from './FormBase';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

export default class FormDoorToDoorDelivery extends FormBase {

    constructor(props) {
        super(props);

        this.state = {
            form_isValid: false,
            validation: {
                city_id_is_valid: false,
                street_is_valid: false,
                house_is_valid: false,
                flat_is_valid: true
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    handleComponentValid(name, value) {
        if (this.state.validation[name] === value) {
            this.checkAllControlsAreValid();
        } else {
            this.setState((prevState) => {
                return {
                    validation: {
                        ...prevState.validation,
                        [name]: value
                    }
                }
            }, () => {
                this.checkAllControlsAreValid();
            });
        }
    }

    render() {
        const { handleComponentChange, handleChangeCity, address, csrf, city_search_label } = this.props;
        const { street = '', house = '', flat = '', city_id = '', selected_city = {} } = address;

        return (
            <div className="row nested-row">
                <div className="row" style={{width: '100%'}}>
                    <CitySearch
                        csrf={csrf}
                        value_prop={city_id}
                        selected_city={selected_city}
                        name="city_id"
                        label={city_search_label}
                        handleComponentValidation={this.handleComponentValid}
                        handleCity={handleChangeCity}
                    />
                </div>
                <div className="row">
                    <Street
                        name='street'
                        label='Улица:'
                        placeholder='Введите улицу'
                        handleComponentChange={handleComponentChange}
                        handleComponentValidation={this.handleComponentValid}
                        value_prop={street}
                    />
                    <House
                        name='house'
                        label='Дом:'
                        placeholder='Введите номер'
                        handleComponentChange={handleComponentChange}
                        handleComponentValidation={this.handleComponentValid}
                        value_prop={house}
                    />
                    <Apartment
                        name='flat'
                        label='Квартира:'
                        placeholder='Введите номер'
                        handleComponentChange={handleComponentChange}
                        handleComponentValidation={this.handleComponentValid}
                        value_prop={flat}
                    />
                </div>
            </div>
        );
    }
};
