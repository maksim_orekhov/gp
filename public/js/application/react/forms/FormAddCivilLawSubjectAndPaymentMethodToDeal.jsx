import React from 'react';
import ControlCivilLawSubjectAndPaymentMethod from '../controllers/ControllerCivilLawSubjectAndPaymentMethod';
import ShowError from '../../../application/react/ShowError';
import FormBase from './FormBase';
import FormDeal from './formsDeal/FormDeal';
import Delivery from './FormDelivery';
import URLS from '../constants/urls';
import {getIdFromUrl, customFetch, getPhoneInMaskFormat} from '../Helpers';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import REQUEST_CODES from '../constants/request_codes';


export default class FormAddCivilLawSubjectAndPaymentMethodToDeal extends FormDeal {
    constructor(props) {
        super(props);
        this.state = {
            controls_server_errors: {
                civil_law_subject: null,
                civil_law_subject_form: null,
                payment_method_form: null,
                payment_method: null
            },
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            form: {
                csrf: '',
                civil_law_subject: '',
                payment_method: '',
                delivery: {
                    service_type: '',
                    service: {},
                    service_dpd: {
                        payment_type: 'ОУО',
                        service_code: 'PCL',
                        parcel_quantity: 1,
                        delivery_time_period: '9-18',
                        pickup_time_period: '9-18',
                        cargo_category: 'Товары народного потребления (без ГСМ и АКБ)',
                    },
                    terms_of_delivery: ''
                }
            },
            validation: {
                civil_law_subject_is_valid: false,
                civil_law_subject_form_is_valid: false,
                delivery_is_valid: false,
                payment_method_form_is_valid: false,
                payment_method_is_valid: false
            },
            is_need_to_fill_delivery: false,
            is_need_to_fill_civil_law_subject: false,
            payment_method_id: '',
            form_isSubmitting: false,
            form_isValid: false,
            form_isPreloader: false,
            natural_persons: [],
            legal_entities: [],
            sole_proprietors: [],
            civil_law_subject_form_data: {},
            payment_method_form_data: {},
            update_collection_civil_law_subjects: 0, // Флаг того что нужно обновить коллекцию в компоненте, для активации повышаем на 1
            clear_local_storage: 0,
            delivery_service_types: {}
        };

        this.form_name = 'form_add_cls_and_payment_to_deal';

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleChangeCivilLawSubject = this.handleChangeCivilLawSubject.bind(this);
        this.handleChangeDelivery = this.handleChangeDelivery.bind(this);
        this.handleCivilLawSubjectValid = this.handleCivilLawSubjectValid.bind(this);
        this.handleStateChange = this.handleStateChange.bind(this);
        this.formInit = this.formInit.bind(this);
    }

    componentWillMount() {
        this.getFormFromLocalStorage();
        this.toggleIsLoading('form_isPreloader');
        this.formInit();
    }

    getFormFromLocalStorage() {
        return new Promise((resolve, reject) => {
            if (localStorage && this.form_name && localStorage.hasOwnProperty(this.form_name)) {
                const form = JSON.parse(localStorage.getItem(this.form_name));
                this.setState({
                    formHasDataAtLocalStorage: true,
                    form: {
                        ...this.state.form,
                        ...form
                    }
                }, () => {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    getFormInitUrl() {
        const idDeal = getIdFromUrl();
        return `${URLS.DEAL.FORM_INIT}?idDeal=${idDeal}`;
    }

    successAjaxFormInit(data) {
        const {
            naturalPersons,
            legalEntities,
            soleProprietors,
            csrf,
            deal_form,
            delivery_service_types
        } = data.data;

        const { is_need_to_fill_delivery = false, is_need_to_fill_civil_law_subject = false } = data.data.deal;
        const { deal_role = false } = deal_form;
        let delivery = {};
        if (deal_form.delivery) {
            delivery = {...deal_form.delivery};
            delete deal_form.delivery;

            delivery = this.getInitDeliveryData(delivery, delivery_service_types);
        } else {
            delete deal_form.delivery;
        }

        if (delivery) {
            delivery = {
                ...this.state.form.delivery,
                ...delivery,
                service: {
                    ...this.state.form.delivery.service,
                    ...delivery.service,
                },
                service_dpd: {
                    ...this.state.form.delivery.service_dpd,
                    ...delivery.service_dpd
                }
            };
        }

        this.setState({
            natural_persons: naturalPersons || [],
            legal_entities: legalEntities || [],
            sole_proprietors: soleProprietors || [],
            form: {
                ...this.state.form,
                delivery,
                csrf
            },
            is_need_to_fill_delivery,
            is_need_to_fill_civil_law_subject,
            delivery_service_types,
            deal_role,
            update_collection_civil_law_subjects: this.state.update_collection_civil_law_subjects + 1
        }, () => {
            this.toggleIsLoading('form_isPreloader');
        })
    }

    getSubmitUrl() {
        const deal_id = getIdFromUrl();
        const { is_need_to_fill_civil_law_subject, is_need_to_fill_delivery } = this.state;

        if ((is_need_to_fill_civil_law_subject && is_need_to_fill_delivery) || is_need_to_fill_civil_law_subject) {
            return `${URLS.DEAL.SINGLE}/${deal_id}/civil-law-subject_payment-method`;
        } else if (!is_need_to_fill_civil_law_subject) {
            return `/deal/${deal_id}/delivery`;
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.form_isValid) {
            this.toggleIsLoading();

            customFetch(this.getSubmitUrl(), {
                method: 'POST',
                body: JSON.stringify(this.getDataFromState())
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        if (this.delivery_form) this.delivery_form.clearLocalStorage();
                        this.clearFormLocalStorage();
                        this.setState({
                            clear_local_storage: this.state.clear_local_storage + 1
                        });
                        const deal_id = getIdFromUrl();
                        const redirectUrl = data.data.redirectUrl || `${URLS.DEAL.SINGLE}/show/${deal_id}`;
                        window.location = redirectUrl;
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                        if (data.code === REQUEST_CODES.DEAL_WITH_SUCH_ID_NOT_FOUND) {
                            window.location = URLS.DEAL.SINGLE;
                        }
                    }
                })
                .catch((err) => {
                    console.log(err);
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    handleChangeDelivery(data_to_state_form) {
        this.setState({
            form: {
                ...this.state.form,
                delivery: {
                    ...data_to_state_form
                }
            },
        });
    }

    getDeliverySubmitData(data) {
        const new_delivery = {
            ...data,
            service: {
                ...data.service,
                ...data.service_dpd
            }
        };
        delete new_delivery['service_dpd'];

        return new_delivery;
    }

    getDataFromState() {
        const { civil_law_subject, payment_method } = this.state.form;
        const delivery = this.getDeliverySubmitData(this.state.form.delivery);
        const form_data = {
            ...this.state.form,
            civil_law_subject,
            payment_method,
            delivery
        };

        if (!civil_law_subject) {
            form_data['civil_law_subject'] = this.state.civil_law_subject_form_data;
        }
        if (!payment_method) {
            form_data['payment_method'] = this.state.payment_method_form_data;
        }
        return form_data;
    }

    checkAllControlsAreValid() {
        const {
            payment_method_form_is_valid,
            payment_method_is_valid,
            civil_law_subject_form_is_valid,
            civil_law_subject_is_valid,
            delivery_is_valid
        } = this.state.validation;
        const { is_need_to_fill_delivery } = this.state;

        const form_isValid =
            (payment_method_form_is_valid || payment_method_is_valid)
            && (civil_law_subject_form_is_valid || civil_law_subject_is_valid)
            && (!is_need_to_fill_delivery || delivery_is_valid);

        if (this.state.form_isValid === form_isValid) return;

        this.setState({
            form_isValid
        });
    }

    TakeApartInvalidFormData(errors) {
        this.setFormError('invalid_form_data', true);

        if (errors) {
            const controls_server_errors = {...this.state.controls_server_errors};

            Object.keys(errors).forEach(control => {
                switch (control) {
                    case 'csrf':
                        this.setFormError('csrf', true);
                        break;
                    case 'civil_law_subject':
                        if (errors[control].hasOwnProperty('civil_law_subject_type')) {
                            controls_server_errors['civil_law_subject'] = errors[control]['civil_law_subject_type'];
                        } else {
                            controls_server_errors['civil_law_subject_form'] = errors[control];
                        }
                        break;
                    case 'payment_method':
                        controls_server_errors['payment_method_form'] = errors[control];
                        break;
                    default:
                        controls_server_errors[control] = errors[control]
                }
            });

            this.setState({
                controls_server_errors
            });
        }
    }

    render() {
        const { form_isSubmitting,
            form_isValid,
            natural_persons,
            legal_entities,
            sole_proprietors,
            update_collection_civil_law_subjects,
            form_isPreloader,
            clear_local_storage,
            is_need_to_fill_delivery,
            is_need_to_fill_civil_law_subject,
            delivery_service_types,
            deal_role
        } = this.state;
        const { civil_law_subject, payment_method } = this.state.form;

        return (
            <div className={`FormLabelTop ${form_isPreloader ? 'Preloader Preloader_solid' : ''}`}>
                {
                    is_need_to_fill_civil_law_subject &&
                    <ControlCivilLawSubjectAndPaymentMethod
                        civil_law_subject_label="Вы будете участвовать в сделке как:"
                        payment_method_label="Метод получения оплаты:"
                        civil_law_subject_name="civil_law_subject"
                        civil_law_subject_id={civil_law_subject}
                        payment_method_name="payment_method"
                        payment_method_id={payment_method}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        handleCivilLawSubject={this.handleChangeCivilLawSubject}
                        handleCivilLawSubjectValid={this.handleCivilLawSubjectValid}
                        form_civil_law_subject_validate_name='civil_law_subject_form_is_valid'
                        form_civil_law_subject_data_name='civil_law_subject_form_data'
                        form_payment_method_validate_name='payment_method_form_is_valid'
                        form_payment_method_data_name='payment_method_form_data'
                        sendUpData={this.handleStateChange}
                        natural_persons={natural_persons}
                        legal_entities={legal_entities}
                        sole_proprietors={sole_proprietors}
                        update_collection={update_collection_civil_law_subjects}
                        is_nested={true}
                        is_cls_form={true}
                        is_hide_bank_transfer_name={true}
                        form_control_server_errors_civil_law_subject={this.state.controls_server_errors.civil_law_subject}
                        form_control_server_errors_civil_law_subject_form={this.state.controls_server_errors.civil_law_subject_form}
                        form_control_server_errors_payment_method={this.state.controls_server_errors.payment_method}
                        form_control_server_errors_payment_method_form={this.state.controls_server_errors.payment_method_form}
                        clear_local_storage={clear_local_storage}
                    />
                }
                {
                    is_need_to_fill_delivery &&
                    <Delivery
                        is_nested={true}
                        delivery={this.state.form.delivery}
                        csrf={this.state.form.csrf}
                        delivery_service_types={delivery_service_types}
                        deal_role={deal_role}
                        handleComponentChange={this.handleChangeDelivery}
                        handleComponentValidation={this.handleComponentValid}
                        ref={ref => this.delivery_form = ref}
                    />
                }
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                <div className="ButtonsRow row nested-row">
                    <div className="col-xl-6 col-md-6 col-sm-4">
                        <button
                            className={form_isSubmitting ? 'ButtonApply Preloader Preloader_light' : 'ButtonApply'}
                            disabled={form_isSubmitting || !form_isValid}
                            onClick={this.handleSubmit}
                        >
                            Далее
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}