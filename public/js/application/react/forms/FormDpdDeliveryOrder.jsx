import React from 'react';
import Street from '../controls/ControlStreet';
import House from '../controls/ControlHouse';
import Apartment from '../controls/ControlApartment';
import ClsType from '../controls/ControlDeliveryOrderCivilLawSubjectType';
import CustomerName from '../controls/ControlDeliveryOrderName';
import Phone from '../../../landing/react/auth/forms/RegisterForm/controls/phone-input';
import Email from '../../../landing/react/auth/forms/RegisterForm/controls/email-input.jsx';
import DeliveryOrderDeliveryInterval from '../controls/ControlDeliveryOrderDeliveryInterval';
import Date from '../controls/ControlDatePicker';
import CustomerCompanyName from '../controls/ControlDeliveryOrderCompanyName';
import Address from '../controllers/ControllerDeliveryAddress';
import ProductDimensions from '../controls/ControlDpdParcelsData';
import CargoCategory from '../controls/ControlCargoCategory';
import {customFetch, getIdFromUrl} from "../Helpers";
import URLS from '../constants/urls';
import ShowError from '../ShowError';
import FormBase from './FormBase';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import ShowHint from "../ShowHint";
import MESSAGES from "../constants/messages";

export default class FormDpdDeliveryOrderCustomer extends FormBase {

    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {
                contact_fio: '',
                phone: '',
                contact_email: '',
                delivery_interval: ''
            },
            validation: {

            },
            form: {
                csrf: '',
                contact_fio: '',
                phone: '',
                contact_email: '',
                contact_company_name: '',
                service: {
                    cargo_category: 1,
                    pickup_time_period: 0,
                    date_pickup: '',
                    sender_address: {},
                    receiver_address: {}
                }
            },
            disabled_controls: {
                delivery_interval: false
            },
            delivery_interval_options: [],
            hints: {
                base_hint: true
            },
            hint_messages: {
                base_hint: MESSAGES.HINTS.delivery_order
            },
            delivery_order_cls_type_option: 'natural_person',
            cargo_category_types: {
                1: {
                    id: 1,
                    name: 'Товары народного потребления (без ГСМ и АКБ)'
                },
                2: {
                    id: 2,
                    name: 'Автозапчасти'
                },
                3: {
                    id: 3,
                    name: 'Мобильные телефоны, устройства с аккумуляторами'
                },
                4: {
                    id: 4,
                    name: 'Опасный груз, жидкости'
                },
                5: {
                    id: 5,
                    name: 'Документы и печатная продукция'
                },
                6: {
                    id: 6,
                    name: 'Алкоголь'
                },
                7: {
                    id: 7,
                    name: 'Косметика и парфюмерия'
                }
            },
            form_isValid: true,
            form_isSubmitting: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeService = this.handleChangeService.bind(this);
        this.handleChangeClsType = this.handleChangeClsType.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleComponentValid() {

    }

    componentWillMount() {
        this.formInit();
    }

    handleSubmit(e) {
        e.preventDefault();
        this.getDataFromState();
        // const { form_isValid } = this.state;
        //
        // if (form_isValid && !this.props.is_nested) {
        //     // this.toggleIsLoading();
        //
        //     const { id } = this.state.form;
        //     const { editMode } = this.props;
        //
        //     const url = editMode ? `${URLS.PAYMENT_METHOD.SINGLE}/${id}` : URLS.PAYMENT_METHOD.SINGLE;
        //
        //     customFetch(url, {
        //         method: 'POST',
        //         body: JSON.stringify(this.getDataFromState())
        //     })
        //         .then(data => {
        //             // this.toggleIsLoading();
        //             // if (data.status === 'SUCCESS') {
        //             //     this.clearFormLocalStorage();
        //             //     const { id } = data.data.paymentMethod;
        //             //     typeof this.props.afterSubmit === 'function' && this.props.afterSubmit(id);
        //             // } else if (data.status === 'ERROR') {
        //             //     this.TakeApartErrorFromServer(data);
        //             // }
        //         })
        //         .catch(() => {
        //             this.setFormError('critical_error', true);
        //             // this.toggleIsLoading();
        //         });
        // }
    }

    getDataFromState() {
        const { is_customer, is_contractor, delivery_order_cls_type_option } = this.state;
        const { service, contact_company_name, contact_email, contact_fio, phone } = this.state.form;
        const data = {...this.state.form};

        const role = is_customer ? 'receiver' : 'sender' ;

        data['service']['cargo_category'] = this.state.cargo_category_types[service['cargo_category']].name;
        data['service']['pickup_time_period'] = this.state.delivery_interval_options[service['pickup_time_period']].name;
        // data['service']['delivery_time_period'] = this.state.delivery_interval_options[service['pickup_time_period']].name;

        if (delivery_order_cls_type_option === 'legal_entity') {
            data['service'][`${role}_address`]['name'] = contact_company_name;
        }

        data['service'][`${role}_address`]['contact_email'] = contact_email;
        data['service'][`${role}_address`]['contact_fio'] = contact_fio;
        data['service'][`${role}_address`]['contact_phone'] = phone;
        console.log(data);


        return this.createFormData(data);
    }

    createFormData(data) {
        const form_data = new FormData();

        Object.entries(data).forEach(([key, value]) => {
            if (typeof value === 'object' && value) {
                Object.entries(value).forEach(([k, v]) => {
                    if (typeof v === 'object' && v) {
                        Object.entries(v).forEach(([k_2, v_2]) => {
                            if (typeof v_2 === 'object' && v_2) {
                                Object.entries(v_2).forEach(([k_3, v_3]) => {
                                    if (typeof v_3 === 'object' && v_3) {
                                        Object.entries(v_3).forEach(([k_4, v_4]) => {
                                            form_data.append(`${key}[${k}][${k_2}][${k_3}][${k_4}]`, v_4);
                                        });
                                    } else {
                                        form_data.append(`${key}[${k}][${k_2}][${k_3}]`, v_3);
                                    }
                                });
                            } else {
                                form_data.append(`${key}[${k}][${k_2}]`, v_2);
                            }
                        });
                    } else {
                        form_data.append(`${key}[${k}]`, v);
                    }
                });
            } else {
                form_data.append(key, value);
            }
        });

        return form_data;
    }

    getFormInitUrl() {
        const idDeal = getIdFromUrl();
        return `${URLS.DEAL.FORM_INIT}?idDeal=${idDeal}`;
    }

    formInit() {
        const url = this.getFormInitUrl();

        customFetch(url)
            .then(data => {
                if (data.status === "SUCCESS") {
                    this.successAjaxFormInit(data);
                } else {
                    return Promise.reject(data);
                }
            })
            .catch(error => {
                console.log(error);
                this.setFormError('form_init_fail', true);
                this.toggleIsLoading('form_isPreloader');
            });
    }

    successAjaxFormInit(data) {
        console.log('formInit', data.data);
        const { csrf, service_dpd_delivery_time_period } = data.data;
        const { is_customer, is_contractor, customer, contractor, delivery } = data.data.deal;
        const { id, service_code, payment_type, parcels, sender_address, receiver_address } = delivery.delivery_service_dpd;
        const form = {...this.state.form};

        let delivery_order_cls_type_option = (customer.is_legal_entity || contractor.is_legal_entity) ? 'legal_entity' : 'natural_person';
        this.setState({
            delivery_order_cls_type_option
        }, () => this.saveFormToLocalStorage());


        console.log('data: ', id, service_code, payment_type, parcels, sender_address, receiver_address);

        form['id'] = id;
        form['service_type'] = 3;
        form['csrf'] = csrf;
        form['service']['service_code'] = service_code;
        form['service']['payment_type'] = payment_type;
        form['service']['parcel_quantity'] = parcels.length;
        form['service']['parcels'] = parcels;

        if (is_customer) {
            form['contact_email'] = customer.email || '';
            form['service']['receiver_address']['contact_phone'] = customer.phone || '';
            form['service']['receiver_address']['contact_fio'] = customer.fio;
            if (customer.is_legal_entity) {
                form['service']['receiver_address']['name'] = customer.name;
            }
        } else if (is_contractor) {
            form['service']['sender_address']['contact_phone'] = contractor.phone || '';
            form['contact_email'] = contractor.email;
            form['service']['sender_address']['contact_fio'] = contractor.fio || '';
            if (contractor.is_legal_entity) {
                form['contact_company_name'] = contractor.name;
            }
        }

        let data_to_state = {
            delivery_interval_options: service_dpd_delivery_time_period || [],
            is_customer,
            customer,
            is_contractor,
            contractor,
            parcels,
            sender_address,
            form: {
                ...form
            }
        };

        // data_to_state = this.setFormDefaultValues(data_to_state);

        this.oldState = {...this.state.form, ...data_to_state.form};

        this.setState(data_to_state, () => {
            this.getFormFromLocalStorage();
            this.toggleIsLoading('form_isPreloader');
        });
    }

    handleChangeService(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                service: {
                    ...this.state.form.service,
                    [name]: value
                }
            }
        }, () => this.saveFormToLocalStorage());
    }

    handleChangeClsType(name, value) {
        console.log(name, value);
        this.setState({
            [name]: value
        }, () => this.saveFormToLocalStorage());
    }

    render() {
        const { contact_fio, contact_email, contact_company_name,
            csrf, service } = this.state.form;
        const { delivery_interval_options, hint_messages, hints, validation_errors,
            form_isSubmitting, form_isValid, is_customer, is_contractor, parcels, sender_address, cargo_category_types,
            delivery_order_cls_type_option} = this.state;
        const interval_label = "Интервал " + (is_customer ? "доставки" : (is_contractor ? "забора" : ""));

        return (
            <div className="PageForm-Container FormLabelTop dpd-delivery-order-form">
                <p className="TextMarked TextMarked_info dpd-delivery-order-info">Для формирования заказа на доставку, пожалуйста, заполните данные отправителя.</p>
                <form>
                        <div className="row">
                            <div className="col-xl-12 cls-type">
                                <ClsType
                                    value_prop={delivery_order_cls_type_option}
                                    name="delivery_order_cls_type_option"
                                    handleComponentChange={this.handleChangeClsType}
                                    handleComponentValidation={this.handleValidate}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xl-6">
                                <CustomerName
                                    label="Контактное лицо:"
                                    placeholder="Введите ФИО"
                                    name="contact_fio"
                                    value_prop={contact_fio}
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleComponentValid}
                                    form_control_server_errors={this.state.controls_server_errors.contact_fio}
                                />
                            </div>
                        </div>
                        {
                            delivery_order_cls_type_option === 'legal_entity' &&
                            <div className="row">
                                <div className="col-xl-6">
                                    <CustomerCompanyName
                                        label="Название организации:"
                                        placeholder="Введите название"
                                        name="contact_company_name"
                                        value_prop={contact_company_name}
                                        handleComponentChange={this.handleChange}
                                        handleComponentValidation={this.handleComponentValid}
                                        form_control_server_errors={this.state.controls_server_errors.contact_company_name}
                                    />
                                </div>
                            </div>
                        }
                        <div className="row">
                            <div className="col-xl-6">
                                <div className="Input-Wrapper">
                                    <Phone
                                        handleComponentChange={this.handleChange}
                                        handleComponentValidation={this.handleComponentValid}
                                        validateOnChange={true}
                                        name="contact_phone"
                                        label="Контактный телефон:"
                                        form_control_server_errors={this.state.controls_server_errors.contact_phone}
                                        componentValue={this.handleChange}
                                        register={false}
                                    />
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <div className="Input-Wrapper">
                                    <Email
                                        register={false}
                                        not_check_unique={true}
                                        handleComponentChange={this.handleChange}
                                        email={contact_email}
                                        name="contact_email"
                                        label="Контактный email:"
                                        form_control_server_errors={this.state.controls_server_errors.contact_email}
                                        handleComponentValidation={this.handleComponentValid}
                                        componentValue={this.handleChange}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xl-3">
                                <ProductDimensions
                                    parcels={ parcels || []}
                                    name="parcels"
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleValidate}
                                />
                            </div>
                            <div className="col-xl-9">
                                <CargoCategory
                                    label="Содержимое заказа:"
                                    name="cargo_category"
                                    cargo_category_types={cargo_category_types}
                                    value_prop={service['cargo_category']}
                                    handleComponentChange={this.handleChangeService}
                                    handleComponentValidation={this.handleComponentValid}
                                    form_control_server_errors={this.state.controls_server_errors.cargo_category}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xl-12 cls-type">
                                <Address
                                    csrf={csrf}
                                    deal_role={'contractor'}
                                    name="sender_address"
                                    address={ sender_address || {} }
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleValidate}
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xl-6">
                                <DeliveryOrderDeliveryInterval
                                    label={interval_label}
                                    name="pickup_time_period"
                                    delivery_interval_options={delivery_interval_options}
                                    value_prop={service['pickup_time_period']}
                                    handleComponentChange={this.handleChangeService}
                                    handleComponentValidation={this.handleComponentValid}
                                    form_control_server_errors={this.state.controls_server_errors.delivery_interval}
                                    is_disable={this.state.disabled_controls.delivery_interval}
                                    {...this.props}
                                />
                            </div>
                            <div className="col-xl-6">
                                <Date
                                    name="date_pickup"
                                    label="Дата забора:"
                                    handleComponentChange={this.handleChangeService}
                                />
                            </div>
                        </div>
                        {
                            is_contractor
                        }
                        <div className="row">
                            <div className="ButtonsRow col-xl-6">
                                <button
                                    onClick={this.handleSubmit}
                                    className={`ButtonApply ${form_isSubmitting ? 'Preloader Preloader_light' : ''}`}
                                    disabled={!form_isValid || form_isSubmitting}
                                    data-ripple-button=""
                                >
                                    <span className="ripple-text">Подтвердить</span>
                                </button>
                            </div>
                        </div>
                        <ShowHint messages={hint_messages} existing_hints={hints} />
                </form>
            </div>
        );
    }
};
