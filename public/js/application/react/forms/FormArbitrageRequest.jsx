import React from 'react';
import FormBase from './FormBase';
import URLS from '../constants/urls';
import ShowError from '../ShowError';
import { customFetch } from '../Helpers';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

const classNames = require('classnames');

export default class FormArbitrageRequest extends FormBase {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                csrf: props.csrf || '',
            },
            form_isLoading: false,
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS
        };

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        !this.props.csrf && this.getCsrf()
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    handleSubmit() {
        const { csrf } = this.state.form;
        const { deal_id, handleOnSubmit } = this.props;

        this.toggleIsLoading();
        customFetch(`${URLS.DEAL.SINGLE}/${deal_id}/arbitrage/request`, {
            method: 'POST',
            body: JSON.stringify({
                csrf,
                type_form: 'arbitrage-request-form',
                deal: deal_id
            })
        })
            .then(data => {
                this.toggleIsLoading();

                if (data.status === 'SUCCESS') {
                    handleOnSubmit && handleOnSubmit(true); // true значит что нужно обновить реквест лист
                    // Если есть событие handleOnSubmit то не прячем прелоадер чтобы успела обновиться коллекция
                    !handleOnSubmit && this.toggleIsLoading();
                    // поставил принудительную перезагрузку страницы, согласно задаче #GP-1619
                    window.location.reload();
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(response => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading();
            })
    }

    render() {
        const { server_errors_messages, server_errors, form_isLoading } = this.state;

        const SubmitButtonClassName = classNames({
            'ButtonApply': true,
            'Preloader Preloader_light': form_isLoading
        });

        return (
            <div className="FormLabelTop">
                <div className="Input-Wrapper">
                    <label>
                        Если Вы не знаете, какой вариант решения спора Вам подходит.
                    </label>
                </div>
                <div className="ButtonsRow">
                    <button className={SubmitButtonClassName} disabled={form_isLoading} onClick={this.handleSubmit} data-ripple-button>
                        <span className="ripple-text">Отправить</span>
                    </button>
                </div>
                <ShowError messages={server_errors_messages} existing_errors={server_errors} />
            </div>
        )
    }
}