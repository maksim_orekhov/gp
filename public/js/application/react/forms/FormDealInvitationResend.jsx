import React from 'react';
import FormBase from './FormBase';
import Timer from '../Timer';
import ShowError from '../../../application/react/ShowError';
import { customFetch } from "../Helpers";
import URLS from '../constants/urls';
import MESSAGES from "../constants/messages";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../constants/server_errors";
import { getIdFromUrl } from './../Helpers';

const classNames = require('classnames');

export default class FormDealInvitationResend extends FormBase {
    constructor(props) {
        super(props);
        this.state = {
            code: '',
            csrf: '',
            form_isLoading: true,
            confirm_isLoading: false,
            resend_isLoading: false,
            formError: '',
            form_isValid: false,

            is_allow_to_send_invitation: true,
            request_period: null,
            last_code_created_at: null,
            is_code_expired: false,
            timer_time: null,
            form_preloader: false,

            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleTimer = this.toggleTimer.bind(this);
    }

    componentWillMount() {
        this.formInit();
    }

    formInit() {
        const deal_id = getIdFromUrl();
        const url = deal_id ? `${URLS.DEAL.FORM_INIT}?idDeal=${deal_id}` : URLS.DEAL.FORM_INIT;

        customFetch(url)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const {
                        csrf
                    } = data.data;
                    const invitation = data.data.deal.customer.invitation || data.data.deal.contractor.invitation;

                    const form = {
                        ...this.state.form,
                        last_invitation_sent_at: invitation.timestamp,
                        request_period: invitation.request_period,
                        csrf
                    };

                    this.setState({
                        form: {
                            ...form,
                            csrf
                        },
                        last_invitation_sent_at_formatted: invitation.date,
                    }, () => this.checkAllowingToSendInvitationTimeOut());
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => this.setFormError('form_init_fail', true));
    }

    checkAllowingToSendInvitationTimeOut() {
        const { request_period, last_invitation_sent_at } = this.state.form;
        const now = Date.now() / 1000; // Время сейчас в секундах
        const timer_time = last_invitation_sent_at + request_period - now;

        if (timer_time > 0) {
            this.setState({
                is_allow_to_send_invitation: false,
                timer_time: Math.round(timer_time),
                resend_isLoading: false
            });
        }
    }

    toggleTimer(isTimer) {
        this.setState({
            is_allow_to_send_invitation: isTimer
        });
    }

    handleSubmit() {
        const { csrf } = this.state.form;
        const url = `/deal/${getIdFromUrl()}/invitation/resend`;

        this.toggleIsLoading('resend_isLoading');
        customFetch(url, {
            method: 'POST',
            body: JSON.stringify({
                csrf
            })
        })
            .then(data => {
                if (data.status === 'SUCCESS') {
                    this.formInit();
                    this.toggleIsLoading('resend_isLoading');

                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                    this.toggleIsLoading('resend_isLoading');
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading('resend_isLoading');
            });
    }

    render() {
        const { last_invitation_sent_at_formatted, resend_isLoading, is_allow_to_send_invitation, timer_time } = this.state;

        const ResendButtonClassName = classNames({
            'ButtonApply FormVerification-Button FormInvitation-Resend': true,
            'Preloader Preloader_light disabled': resend_isLoading
        });

        return (
            <div className={"FormLabelTop"}>
                {
                    last_invitation_sent_at_formatted ?
                        <p className="note-tiny">Приглашение было отправлено {last_invitation_sent_at_formatted}.</p>
                        :
                        <p className="note-tiny">Приглашение не было отправлено</p>

                }
                {
                    is_allow_to_send_invitation ?
                        <button
                            className={ResendButtonClassName}
                            disabled={false}
                            onClick={this.handleSubmit}
                        >
                            Пригласить повторно
                        </button>
                        :
                        <p className="mt-20 note-tiny">
                            Вы сможете отправить приглашение повторно через <span className="TextAccent"> {/* span на этой строке чтобы сохранить пробел*/}
                            <Timer
                                start={timer_time}
                                expired={this.toggleTimer}
                                isShowTime={true}
                            />
                        </span> секунд.
                        </p>
                }
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
            </div>
        )
    }
};