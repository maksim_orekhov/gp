import React from 'react';
import FormBase from './FormBase';
import ShowError from '../../../application/react/ShowError';
import NAMES from '../constants/names';
import { customFetch } from '../Helpers';
import MESSAGES from "../constants/messages";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../constants/server_errors";

const classNames = require('classnames');

export default class FormRequestConfirm extends FormBase {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                csrf: props.csrf || '',
            },
            formError: '',
            form_isLoading: false,
            form_isRefusing: false,
            form_isSubmitting: false,
            button_isLoading: false,
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS
        };

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleConfirm = this.handleConfirm.bind(this);
        this.handleRefuse = this.handleRefuse.bind(this);
    }

    componentDidMount() {
        !this.props.csrf && this.getCsrf()
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    toggleButtonIsLoading(name = 'form_isSubmitting') {
        this.setState({
            button_isLoading: !this.state.button_isLoading,
            [name]: !this.state[name]
        });
    }

    toggleIsLoading() {
        this.setState({
            form_isLoading: !this.state.form_isLoading
        });
    }

    handleConfirm() {
        const { csrf } = this.state.form;
        const { request_id, handleOnSubmit, request_type } = this.props;

        this.toggleIsLoading();
        // this.toggleButtonIsLoading('form_isSubmitting');

        customFetch(`/deal/${request_type}/request/${request_id}/confirm`, {
            method: 'POST',
            body: JSON.stringify({
                csrf,
                type_form: 'request-confirm-form'
            })
        })
            .then(data => {
                if (data.status === 'SUCCESS') {
                    handleOnSubmit && handleOnSubmit();
                    // Если есть событие handleOnSubmit то не прячем прелоадер чтобы успела обновиться коллекция
                    !handleOnSubmit && this.toggleIsLoading();
                    // this.toggleButtonIsLoading('form_isSubmitting');
                    this.hideConfirmationMarkedText();
                    window.location.reload();
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                    this.toggleIsLoading();
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading();
                // this.toggleButtonIsLoading('form_isSubmitting');
            });
    }

    handleRefuse() {
        const { csrf } = this.state.form;
        const { request_type, request_id, handleOnSubmit } = this.props;

        this.toggleIsLoading();
        // this.toggleButtonIsLoading('form_isRefusing');

        customFetch(`/deal/${request_type}/request/${request_id}/refuse`, {
            method: 'POST',
            body: JSON.stringify({
                csrf,
                type_form: 'request-refuse-form'
            })
        })
            .then(data => {
                if (data.status === 'SUCCESS') {
                    handleOnSubmit && handleOnSubmit();
                    // Если есть событие handleOnSubmit то не прячем прелоадер чтобы успела обновиться коллекция
                    !handleOnSubmit && this.toggleIsLoading();
                    // this.toggleButtonIsLoading('form_isRefusing');
                    this.hideConfirmationMarkedText();
                    window.location.reload();
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                    this.toggleIsLoading();
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading();
                // this.toggleButtonIsLoading('form_isRefusing');
            });
    }

    // метод для скрытия текста 'Подтвердите или отклоните' в описании спора после принятия решения по спору
    hideConfirmationMarkedText() {
        if (document.getElementById('text-marked-confirm')) {
            document.getElementById('text-marked-confirm').style.visibility = 'hidden';
        }
    }

    render() {
        const { form_isLoading, form_isSubmitting, form_isRefusing, button_isLoading } = this.state;
        const { request_type, request_amount, deal_type, is_customer } = this.props;
        const form_refund_label = is_customer ?
                deal_type === 'U' ?
            'В срок не более 1-го рабочего дня Вам будет необходимо сообщить исполнителю об отказе от услуги.'
            : 'В срок не более 3-х рабочих дней Вам будет необходимо организовать доставку товара продавцу.'
            : deal_type === 'U' ?
                'В срок не более 1-го рабочего дня заказчику будет необходимо сообщить Вам об отказе от услуги.'
                : 'В срок не более 3-х рабочих дней покупателю будет необходимо вернуть Вам товар.'
            ;

        const info_field_text = deal_type === 'U' && 'Отказ от услуги' || (deal_type ===  'T' || !deal_type) && 'Возврат товара';

        const SubmitButtonClassName = classNames({
            'ButtonApply': true,
            'Preloader Preloader_light': form_isSubmitting
        });

        const RefuseButtonClassName = classNames({
            'ButtonRefuse': true,
            'Preloader Preloader_light': form_isRefusing
        });

        return (
            <div className={form_isLoading ? 'Preloader Preloader_solid' : 'SolutionRequests-Status'}>
                <h2 className="SubTitle">Предложение</h2>
                <div className="FormLabelTop">
                    <div className="InfoField">
                        {
                            request_type !== 'refund' &&
                            <div className="InfoField-Text">{NAMES.DISPUTE_RESOLVE_LABELS[request_type]}</div>
                        }
                        {
                            request_type === 'refund' &&
                            <div className="InfoField-Text">{info_field_text}</div>
                        }
                    </div>
                    {{
                        tribunal: (
                            <p className="SolutionRequests-StatusInfo">После присвоения спору статуса Суд, в течение 10 дней Вам будет необходимо подтвердить данное решение, направив администрации копию обращения за судебной защитой.</p>
                        ),
                        refund: (
                            <p className="SolutionRequests-StatusInfo">{form_refund_label}</p>
                        ),
                        discount: (
                            <div>
                                <h2 className="SubTitle">Размер скидки, рублей:</h2>
                                <div className="InfoField">
                                    <div className="InfoField-Text">{ request_amount }</div>
                                </div>
                            </div>
                        )
                    }[request_type]}
                    {
                        <div className="row nested-row ButtonsRow">
                            <div className="col-xl-6 col-sm-4">
                                <button className={SubmitButtonClassName} disabled={button_isLoading} onClick={this.handleConfirm} data-ripple-button>
                                    <span className="ripple-button">Принять</span>
                                </button>
                            </div>
                            <div className="col-xl-6 col-sm-4">
                                <button className={RefuseButtonClassName} disabled={button_isLoading} onClick={this.handleRefuse} data-ripple-button>
                                    <span className="ripple-button">Отклонить</span>
                                </button>
                            </div>
                        </div>
                    }
                    <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                </div>
            </div>
        )
    }
}