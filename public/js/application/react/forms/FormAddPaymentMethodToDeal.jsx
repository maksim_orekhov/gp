import React from 'react';
import PaymentMethod from '../controls/ControlPaymentMethod';
import Delivery from './FormDelivery';
import FormBase from './FormBase';
import FormDeal from './formsDeal/FormDeal';
import ShowError from '../../../application/react/ShowError';
import URLS from '../constants/urls';
import { getIdFromUrl, customFetch } from '../Helpers';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import REQUEST_CODES from '../constants/request_codes';

export default class FormAddPaymentMethodToDeal extends FormDeal {
    constructor(props) {
        super(props);
        this.state = {
            controls_server_errors: {
                payment_method: null,
                payment_method_form: null
            },
            form: {
                payment_method: '',
                csrf: '',
                delivery: {
                    service_type: '',
                    service: {},
                    service_dpd: {
                        payment_type: 'ОУО',
                        service_code: 'PCL',
                        parcel_quantity: 1,
                        delivery_time_period: '9-18',
                        pickup_time_period: '9-18',
                        cargo_category: 'Товары народного потребления (без ГСМ и АКБ)',
                    },
                    terms_of_delivery: ''
                }
            },
            is_need_to_fill_delivery: false,
            is_need_to_fill_civil_law_subject: false,
            is_need_to_fill_payment_method: false,
            validation: {
                payment_method_is_valid: false,
                payment_method_form_is_valid: false,
                delivery_is_valid: false
            },
            civil_law_subject_id: '',
            payment_method_form_data: '',
            natural_persons: [],
            legal_entities: [],
            form_isValid: false,
            form_isSubmitting: false,
            form_isPreloader: false,
            update_collection: 0,
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            clear_local_storage: 0,
            delivery_service_types: {}
        };

        this.form_name = null;

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleStateChange = this.handleStateChange.bind(this);
        this.handleChangeDelivery = this.handleChangeDelivery.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        this.formInit();
    }

    formInit() {
        const deal_id = getIdFromUrl();
        // Проверяем если у сделки в state ее id, если есть то делаем запрос с учетом id сделки.
        const url = deal_id ? `${URLS.DEAL.FORM_INIT}?idDeal=${deal_id}` : URLS.DEAL.FORM_INIT;

        this.toggleIsLoading('form_isPreloader');
        customFetch(url)
            .then(data => {
                console.log('formInit', data);
                this.toggleIsLoading('form_isPreloader');

                if (data.status === 'SUCCESS') {
                    const { naturalPersons, legalEntities, csrf, deal_form, delivery_service_types, soleProprietors } = data.data;
                    const {
                        is_customer,
                        customer,
                        contractor,
                        is_need_to_fill_delivery = false,
                        is_need_to_fill_payment_method = false
                    } = data.data.deal;
                    const { deal_role = false } = deal_form;

                    let delivery = {};
                    if (deal_form.delivery) {
                        delivery = {...deal_form.delivery};
                        delete deal_form.delivery;

                        delivery = this.getInitDeliveryData(delivery, delivery_service_types);
                    } else {
                        delete deal_form.delivery
                    }

                    if (delivery) {
                        delivery = {
                            ...this.state.form.delivery,
                            ...delivery,
                            service: {
                                ...this.state.form.delivery.service,
                                ...delivery.service,
                            },
                            service_dpd: {
                                ...this.state.form.delivery.service_dpd,
                                ...delivery.service_dpd
                            }
                        };
                    }

                    let civil_law_subject_id = contractor.civil_law_subject_id;

                    if (is_customer) {
                        civil_law_subject_id = customer.civil_law_subject_id;
                    }

                    this.setState({
                        natural_persons: naturalPersons || [],
                        legal_entities: legalEntities || [],
                        sole_proprietors: soleProprietors || [],
                        update_collection: this.state.update_collection + 1,
                        civil_law_subject_id,
                        form: {
                            ...this.state.form,
                            delivery,
                            csrf
                        },
                        is_need_to_fill_delivery,
                        is_need_to_fill_payment_method,
                        delivery_service_types,
                        deal_role
                    })
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => {
                this.setFormError('form_init_fail', true);
                this.toggleIsLoading('form_isPreloader');
            });
    }

    checkAllControlsAreValid() {
        const { payment_method_is_valid, payment_method_form_is_valid, delivery_is_valid } = this.state.validation;
        const is_need_to_fill_delivery = this.state.is_need_to_fill_delivery;

        const form_isValid = (
            (payment_method_form_is_valid || payment_method_is_valid)
            && (!is_need_to_fill_delivery || delivery_is_valid)
        );

        if (this.state.form_isValid === form_isValid) return;

        this.setState({
            form_isValid
        });
    }

    getSubmitUrl() {
        const deal_id = getIdFromUrl();

        return `${URLS.DEAL.SINGLE}/${deal_id}/payment-method`;
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.form_isValid) {
            const deal_id = getIdFromUrl();

            this.toggleIsLoading();
            customFetch(this.getSubmitUrl(), {
                method: 'POST',
                body: JSON.stringify(this.getDataFromState())
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.setState({
                            clear_local_storage: this.state.clear_local_storage + 1
                        });
                        const redirectUrl = data.data.redirectUrl || `${URLS.DEAL.SINGLE}/show/${deal_id}`;
                        window.location = redirectUrl;
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                        if (data.code === REQUEST_CODES.DEAL_WITH_SUCH_ID_NOT_FOUND) {
                            window.location = URLS.DEAL.SINGLE;
                        }
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    handleChangeDelivery(data_to_state_form) {
        this.setState({
            form: {
                ...this.state.form,
                delivery: {
                    ...data_to_state_form
                }
            },
        });
    }

    getDeliverySubmitData(data) {
        const new_delivery = {
            ...data,
            service: {
                ...data.service,
                ...data.service_dpd
            }
        };
        delete new_delivery['service_dpd'];

        return new_delivery;
    }

    getDataFromState() {
        const { payment_method, csrf } = this.state.form;
        const delivery = this.getDeliverySubmitData(this.state.form.delivery);
        if (payment_method) {
            return {
                ...this.state.form,
                delivery
            };
        } else {
            return {
                payment_method: {
                    ...this.state.payment_method_form_data,
                },
                delivery,
                csrf
            };
        }
    }

    TakeApartInvalidFormData(errors) {
        this.setFormError('invalid_form_data', true);

        if (errors) {
            const controls_server_errors = {...this.state.controls_server_errors};

            Object.keys(errors).forEach(control => {
                switch (control) {
                    case 'csrf':
                        this.setFormError('csrf', true);
                        break;
                    case 'payment_method':
                        if (errors[control].hasOwnProperty('payment_method_id')) {
                            controls_server_errors['payment_method'] = errors[control]['payment_method_id'];
                        } else {
                            controls_server_errors['payment_method_form'] = errors[control];
                        }
                        break;
                    default:
                        controls_server_errors[control] = errors[control]
                }
            });

            this.setState({
                controls_server_errors
            });
        }
    }

    render() {
        const {
            form_isSubmitting,
            form_isValid,
            natural_persons,
            legal_entities,
            sole_proprietors,
            update_collection,
            civil_law_subject_id,
            form_isPreloader,
            clear_local_storage,
            is_need_to_fill_delivery,
            delivery_service_types,
            deal_role
        } = this.state;

        const { payment_method } = this.state.form;

        return (
            <div className={`FormLabelTop ${form_isPreloader ? 'Preloader Preloader_solid' : ''}`}>
                <PaymentMethod
                    label="Способ получения оплаты:"
                    name="payment_method"
                    value_prop={payment_method}
                    civil_law_subject_id={civil_law_subject_id}
                    natural_persons={natural_persons}
                    legal_entities={legal_entities}
                    sole_proprietors={sole_proprietors}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_validate_name="payment_method_form_is_valid"
                    form_data_name="payment_method_form_data"
                    sendUpData={this.handleStateChange}
                    is_nested={true}
                    is_hide_name={true}
                    update_collection={update_collection}
                    form_control_server_errors={this.state.controls_server_errors.payment_method}
                    form_server_errors={this.state.controls_server_errors.payment_method_form}
                    clear_local_storage={clear_local_storage}
                />
                {
                    is_need_to_fill_delivery &&
                    <Delivery
                        is_nested={true}
                        delivery={this.state.form.delivery}
                        csrf={this.state.form.csrf}
                        delivery_service_types={delivery_service_types}
                        deal_role={deal_role}
                        handleComponentChange={this.handleChangeDelivery}
                        handleComponentValidation={this.handleComponentValid}
                    />
                }
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                <div className="ButtonsRow row nested-row">
                    <div className="col-xl-6 col-md-6 col-sm-4">
                        <button
                            className={form_isSubmitting ? 'ButtonApply Preloader Preloader_light' : 'ButtonApply'}
                            disabled={form_isSubmitting || !form_isValid}
                            onClick={this.handleSubmit}
                        >
                            Далее
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}