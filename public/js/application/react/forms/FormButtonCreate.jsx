import React from 'react';
import FormWrapper from '../components/FormDealWrapper';
import DealType from '../controls/ControlDealType';
import DealRole from '../controls/ControlDealRole';
import ButtonAuthorRole from '../controls/ControlButtonAuthorRole';
import DealName from '../controls/ControlDealName';
import FeePayerOption from '../controls/ControlFeePayerOption';
import AddonTerms from '../controls/ControlDealAddonTerms';
import DeliveryPeriod from '../controls/ControlDeliveryPeriod';
import Amount from '../controls/ControlAmount';
import DealConfirm from '../controls/ControlDealConfirm';
import CivilLawSubject from '../controls/ControlCivilLawSubject';
import CivilLawSubjectAndPaymentMethod from '../controllers/ControllerCivilLawSubjectAndPaymentMethod';
import PaymentMethod from '../controls/ControlPaymentMethod';
import File from '../controls/ControlFileList';
import FormDeal from './formsDeal/FormDeal';
import DisplayNone from '../components/DisplayNone';
import ShowError from '../ShowError';
import Popup from '../Popup';
//import EmailInnCompanyName from '../controls/ControlEmailInnCompanyName';
import ButtonName from '../controls/ControlInvitationButtonName';
import SlugName from '../controls/ControlInvitationSlugName';
import IntegrationRequestCreate from '../forms/FormIntegrationRequestCreate';

//Email контрагента при создании от имени рефера
import EmailCounterAgent from '../controls/ControlEmailCounterAgent';


import URLS from '../constants/urls';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import { customFetch, getDateNowInPrettyFormat, cyrillicToLatin } from '../Helpers';

const FormButtonCreateErrors = {
    ...SERVER_ERRORS_MESSAGES,
    not_auth: <span>Пожалуйста, <a href={`${URLS.LOGIN}?redirectUrl=${URLS.BUTTON.CREATE}`}>авторизируйтесь</a>.</span>,
};

export default class FormButtonCreate extends FormDeal {
    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {
                /**
                 * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
                 * @example
                 *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
                 **/
                deal_type: null,
                deal_role: null,
                deal_description: null,
                amount: null,
                name: null,
                delivery_period: null,
                fee_payer_option: null,
                counteragent_civil_law_subject: null,
                beneficiar_civil_law_subject: null,
                counteragent_payment_method: null,
                counteragent_civil_law_subject_form: null, // Ожидается объект со вложеннами ошибками формы
                beneficiar_civil_law_subject_form: null,
                payment_method_form: null, // Ожидается объект со вложеннами ошибками формы
                beneficiar_payment_method_form: null,
                button_create_confirm: false
            },
            formHasDataAtLocalStorage: false,
            is_integration_request_form_opened: false,
            // SERVER_ERRORS
            server_errors_messages: FormButtonCreateErrors,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                deal_type_is_valid: false,
                deal_role_is_valid: false,
                name_is_valid: false,
                deal_name_is_valid: false,
                button_author_role_is_valid: false,
                deal_description_is_valid: false,
                amount_is_valid: false,
                fee_payer_option_is_valid: false,
                delivery_period_is_valid: false,
                counteragent_civil_law_subject_is_valid: false,
                beneficiar_civil_law_subject_is_valid: false,
                counteragent_payment_method_is_valid: false,
                beneficiar_payment_method_is_valid: false,
                button_create_confirm_is_valid: false,
                civil_law_subject_form_is_valid: false,
                beneficiar_civil_law_subject_form_is_valid: false,
                payment_method_form_is_valid: false,
                counteragent_payment_method_form_is_valid: false,
                beneficiar_payment_method_form_is_valid: false,
                counteragent_email_is_valid: false
            },
            // -----------------------------
            form: {
                csrf: '',
                deal_type: '',
                referer_deal_type: '2',
                name: '',
                button_author_role: 'participant',
                deal_role: 'customer',
                invited_user_role: 'customer',
                invited_user_email: '',
                deal_name: '',
                deal_description: '',
                amount: '',
                fee_payer_option: '',
                delivery_period: '14',
                // beneficiar_user: '',
                counteragent_civil_law_subject: '',
                beneficiar_civil_law_subject: '',
                counteragent_payment_method: '',
                beneficiar_payment_method: '',
                counteragent_email: '',
                files: '',
                slug: '',
                slug_name: '',
                is_active: true,
            },
            // Флаги на случай если нужно сделать контролы disabled
            disabled_controls: {
                deal_type: false,
                deal_role: false,
                referer_deal_type: true,
                invited_user_role: true,
                deal_name: true,
                deal_description: true,
                files: true,
                amount: true,
                fee_payer_option: true,
                delivery_period: true,
                counteragent_civil_law_subject: false,
                beneficiar_civil_law_subject: false,
                counteragent_payment_method: false,
                beneficiar_payment_method: false
            },
            deal_id: '',
            form_isValid: false,
            form_isPreloader: false,
            form_isSubmitting: false,
            deal_types: {},
            fee_payer_options: [],
            deal_number: '',
            civil_law_subject_form_data: {},
            beneficiar_civil_law_subject_form_data: {},
            payment_method_form_data: {},
            beneficiar_payment_method_form_data: {},
            natural_persons: [],
            legal_entities: [],
            sole_proprietors: [],
            update_collection_civil_law_subjects: 0, // Флаг того что нужно обновить коллекцию в компоненте, для активации повышаем на 1
            date_of_create: getDateNowInPrettyFormat(),
            clear_local_storage: 0,
            form_template: 'base_form',
            max_deal_amount: null, // Максимальная сумма сделки
            was_slug_focused: false,
            is_slug_not_unique: false
        };

        this.form_name = 'form_button_create'; // название формы для хранения данных в local_storage

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeDealRole = this.handleChangeDealRole.bind(this);
        this.handleChangeCivilLawSubject = this.handleChangeCivilLawSubject.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleIsLoading = this.toggleIsLoading.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleStateChange = this.handleStateChange.bind(this);
        this.handleCivilLawSubjectValid = this.handleCivilLawSubjectValid.bind(this);
        this.setControlDisabled = this.setControlDisabled.bind(this);
        this.checkIsSlugNameUnique = this.checkIsSlugNameUnique.bind(this);
        this.handleChangeButtonName = this.handleChangeButtonName.bind(this);
        this.turnOnSlugFocus = this.turnOnSlugFocus.bind(this);
        this.getIntegrationRequestLink = this.getIntegrationRequestLink.bind(this);
        this.toggleFormIntegrationRequest = this.toggleFormIntegrationRequest.bind(this);
    }
    componentWillMount() {
        this.getCsrf();

        const formHTML = document.getElementById('CreateDealToken');
        if (formHTML) {
            formHTML.style.visibility = 'visible';
        }
        this.formInit();
    }

    getFormInitUrl() {
        return URLS.BUTTON.FORM_INIT;
    }

    resetForm = () => {
        console.log('---', this.oldState);
        if(this.oldState) {
            this.setState({form: {...this.state.form, ...this.oldState}, formHasDataAtLocalStorage: false});
            this.clearFormLocalStorage();
        }
    };

    formInit() {
        const url = this.getFormInitUrl();

        this.toggleIsLoading('form_isPreloader');
        customFetch(url)
            .then(data => {
                if (data.status === "SUCCESS") {
                    this.successAjaxFormInit(data);
                } else {
                    return Promise.reject(data);
                }
            })
            .catch(error => {
                console.log(error);
                this.setFormError('form_init_fail', true);
                this.toggleIsLoading('form_isPreloader');
            });
    }

    successAjaxFormInit(data) {
        const { deal_types = {}, fee_payer_options = [], naturalPersons, legalEntities, soleProprietors, max_deal_amount } = data.data;
        const form = {...this.state.form};
        const { update_collection_civil_law_subjects } = this.state;
        const { form_template, form_type } = this.props;

        let data_to_state = {
            deal_types,
            fee_payer_options,
            natural_persons: naturalPersons || [],
            legal_entities: legalEntities || [],
            sole_proprietors: soleProprietors || [],
            update_collection_civil_law_subjects: update_collection_civil_law_subjects + 1,
            form_template: form_template || this.state.form_template,
            max_deal_amount,
            form: {
                ...form
            }
        };

        data_to_state = this.setFormDefaultValues(data_to_state);

        if (form_type) {
            data_to_state = this.setCustomDataToState(data_to_state);
        }

        this.oldState = {...this.state.form, ...data_to_state.form};

        this.setState(data_to_state, () => {
            this.getFormFromLocalStorage().then(() => {
                const disabledControls = {};

                Object.keys(this.state.form).map((fieldName) => {
                    if (this.state.form[fieldName] && this.state.form[fieldName].length > 0) {
                        disabledControls[fieldName] = false;
                    }
                });

                this.setState({
                    disabled_controls: {
                        ...this.state.disabled_controls,
                        ...disabledControls,
                    }
                });
            });
            this.toggleIsLoading('form_isPreloader');
        });
    }

    // Метод заменяющий аналогичный в formDeal, НЕ УДАЛЯТЬ!
    setDeliveryDefaultData(data_to_state = {}) {
        return data_to_state;
    }

    setControlDisabled(name, enabled) {
        this.setState({
            disabled_controls: {
                ...this.state.disabled_controls,
                [name]: !enabled
            }
        }, () => {
            this.checkAllControlsAreValid()
        });
    }

    handleChangeDealRole(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value,
                payment_method: ''
            },
            disabled_controls: {
                ...this.state.disabled_controls,
                deal_role: value.toLowerCase() === 'referer',
            }
        }, () => {
            this.checkAllControlsAreValid();
            this.saveFormToLocalStorage();
        });
    }

    handleCivilLawSubjectValid(name, value) {
        this.handleComponentValid(name, value);
        this.handleComponentValid('counteragent_payment_method_is_valid', false);
        this.handleComponentValid('beneficiar_payment_method_is_valid', false);
    }

    handleChangeCivilLawSubject(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                counteragent_payment_method: '',
                beneficiar_payment_method: '',
                [name]: value
            }
        });
    }

    checkAllControlsAreValid() {
        const button_author_role = this.state.form.button_author_role;
        if(button_author_role === 'referer') {
            this.checkValidationForReferer();
        } else {
            this.checkValidationForParticipant();
        }
    }


    checkValidationForParticipant =() => {
        const {
            deal_type_is_valid,
            deal_role_is_valid,
            button_author_role_is_valid,
            deal_name_is_valid,
            name_is_valid,
            deal_description_is_valid,
            amount_is_valid,
            fee_payer_option_is_valid,
            delivery_period_is_valid,
            counteragent_civil_law_subject_is_valid,
            counteragent_payment_method_is_valid,
            button_create_confirm_is_valid,
            civil_law_subject_form_is_valid,
            counteragent_email_is_valid,
            payment_method_form_is_valid,
            counteragent_payment_method_form_is_valid,
            beneficiar_payment_method_form_is_valid,
        } = this.state.validation;

        if (deal_type_is_valid
            && deal_role_is_valid
            && name_is_valid
            && (this.state.disabled_controls.deal_name || deal_name_is_valid)
            && (this.state.disabled_controls.deal_description || deal_description_is_valid)
            && (this.state.disabled_controls.amount || amount_is_valid)
            && button_author_role_is_valid
            && fee_payer_option_is_valid
            && delivery_period_is_valid
            && button_create_confirm_is_valid
            && (
                (this.state.form.button_author_role === 'referer' && counteragent_email_is_valid)
                || (this.state.form.button_author_role === 'participant')
            )
            && (counteragent_civil_law_subject_is_valid || civil_law_subject_form_is_valid)
            && (
                (
                    this.state.form.deal_role === 'contractor'
                    && ( counteragent_payment_method_is_valid || counteragent_payment_method_form_is_valid )
                )
                || (this.state.form.deal_role === 'customer')
            )
        ) {
            this.setState({ form_isValid: true });
        } else {
            this.setState({ form_isValid: false });
        }
    };

    checkValidationForReferer =() => {
        const {
            deal_type_is_valid,
            deal_role_is_valid,
            button_author_role_is_valid,
            deal_name_is_valid,
            name_is_valid,
            deal_description_is_valid,
            amount_is_valid,
            delivery_period_is_valid,
            fee_payer_option_is_valid,
            button_create_confirm_is_valid,
            counteragent_email_is_valid,
            beneficiar_payment_method_is_valid,
            beneficiar_payment_method_form_is_valid,
            beneficiar_civil_law_subject_form_is_valid,
            beneficiar_civil_law_subject_is_valid,
        } = this.state.validation;

        const {deal_role, deal_type, deal_name, deal_description, amount, delivery_period, fee_payer_option } = this.state.disabled_controls;
        let form_isValid = true;
        const validation = {
            '(deal_role || !deal_role && deal_role_is_valid)': (deal_role || !deal_role && deal_role_is_valid),
            '(beneficiar_civil_law_subject_form_is_valid || beneficiar_civil_law_subject_is_valid)': (beneficiar_civil_law_subject_form_is_valid || beneficiar_civil_law_subject_is_valid),
            '(name_is_valid)': (name_is_valid),
            '(!this.state.is_slug_not_unique)': (!this.state.is_slug_not_unique),
            '(button_author_role_is_valid)': (button_author_role_is_valid),

            '(beneficiar_payment_method_form_is_valid || beneficiar_payment_method_is_valid)': (beneficiar_payment_method_form_is_valid || beneficiar_payment_method_is_valid),

            '(counteragent_email_is_valid)': (counteragent_email_is_valid),
            '(deal_type || !deal_type && deal_type_is_valid)': (deal_type || !deal_type && deal_type_is_valid),
            '(deal_name || !deal_name && deal_name_is_valid)': (deal_name || !deal_name && deal_name_is_valid),
            '(deal_description || !deal_description && deal_description_is_valid)': (deal_description || !deal_description && deal_description_is_valid),
            '(amount || !amount && amount_is_valid)': (amount || !amount && amount_is_valid),
            '(delivery_period || !delivery_period && delivery_period_is_valid)': (delivery_period || !delivery_period && delivery_period_is_valid),
            '(fee_payer_option || !fee_payer_option && fee_payer_option_is_valid)': (fee_payer_option || !fee_payer_option && fee_payer_option_is_valid),
            '(button_create_confirm_is_valid);': (button_create_confirm_is_valid)
        };
        Object.values(validation).map(validationCondition => {
            if (!validationCondition) {
                form_isValid = false;
            }
        });

        this.setState({  form_isValid });
    };

    getDataFromState() {
        const {
            form : {
                counteragent_civil_law_subject,
                beneficiar_civil_law_subject,
                deal_role,
                counteragent_payment_method,
                beneficiar_payment_method,
                deal_name,
                button_author_role,
                counteragent_email
            },
            disabled_controls
        } = this.state;
        const data = {...this.state.form};

        if (data['deal_role']) {
            data['deal_role'] = data['deal_role'].toLowerCase() === 'customer' ? 'contractor' : 'customer';
        }

        delete data.files;

        if (button_author_role === 'referer') {
            delete data.counteragent_civil_law_subject;
            delete data.counteragent_payment_method;
        } else {
            delete data.beneficiar_civil_law_subject;
            delete data.beneficiar_payment_method;
            delete data.counteragent_email;
        }

        if (disabled_controls.amount) {
            delete data.amount;
        }
        if (disabled_controls.deal_type) {
            delete data.deal_type;
        }
        if (disabled_controls.deal_description) {
            delete data.deal_description;
        }

        if (disabled_controls.fee_payer_option) {
            delete data.fee_payer_option;
        }

        if (disabled_controls.delivery_period) {
            delete data.delivery_period;
        }

        if (disabled_controls.deal_role) {
            delete data.deal_role;
        }

        const files = this.state.form.files;

        if (!counteragent_civil_law_subject && button_author_role !== 'referer') {
            data['counteragent_civil_law_subject'] = this.state.civil_law_subject_form_data;
        }
        if (deal_role === 'contractor' && !payment_method) {
            data['counteragent_payment_method'] = this.state.payment_method_form_data;
        }

        if (deal_role === 'customer') {
           delete data['counteragent_payment_method'];
        }

        data['is_active'] = data['is_active'] ? 1 : 0;
        return this.createFormData(data, files);
    }

    getSubmitUrl() {
        return '/profile/partner-button'
    }

    successAjaxSubmit(data) {
        this.clearFormLocalStorage();

        this.setState({
            deal_id: this.state.deal_id || data.data.id,
            clear_local_storage: this.state.clear_local_storage + 1
        }, () => {
            // Делаем активной вкладку с кнопками
            window.sessionStorage.setItem('activeTabCivilLawSubject', 'table_buttons');
            // Запоминаем id добавленой кнопки
            window.sessionStorage.setItem('addedButtonId', this.state.deal_id);
            this.toggleIsLoading('form_isPreloader');
            this.afterSubmitFormDeal();
        });

    }

    handleCancel() {
        window.location = `${URLS.PROFILE}`;
    }

    afterSubmitFormDeal() {
        window.location = `${URLS.PROFILE}`;
    }

    TakeApartInvalidFormData(errors) {
        this.setFormError('invalid_form_data', true);

        if (errors) {
            const controls_server_errors = {...this.state.controls_server_errors};

            Object.keys(errors).forEach(control => {
                switch (control) {
                    case 'csrf':
                        this.setFormError('csrf', true);
                        break;
                    case 'counteragent_civil_law_subject':
                        if (errors[control].hasOwnProperty('civil_law_subject_id')) {
                            controls_server_errors['counteragent_civil_law_subject'] = errors[control]['civil_law_subject_id'];
                        } else {
                            controls_server_errors['counteragent_civil_law_subject_form'] = errors[control];
                        }
                        break;
                    case 'counteragent_payment_method':
                        if (errors[control].hasOwnProperty('payment_method_id')) {
                            controls_server_errors['counteragent_payment_method'] = errors[control]['payment_method_id'];
                        } else {
                            controls_server_errors['counteragent_payment_method_form'] = errors[control];
                        }
                        break;
                    default:
                        controls_server_errors[control] = errors[control]
                }
            });

            this.setState({
                controls_server_errors
            });
        }
    }

    getFormWithRoleAndTypeTitle() {
        const ident = this.getCurrentDealTypeIdent();
        const deal_role = this.state.form.deal_role;

        return {
            U: {
                customer: 'Заказ услуги',
                contractor: 'Оказание услуги'
            },
            T: {
                customer: 'Покупка товара',
                contractor: 'Продажа товара'
            }
        }[ident][deal_role]
    }

    getLabels() {
        const ident = this.state.form.button_author_role === 'participant' ?
            this.getCurrentDealTypeIdent()
            :
            this.getCurrentRefererDealTypeIdent();
        const deal_name_label = ident === 'U' ? 'услуги' : 'товара';
        const deal_term_label = ident === 'U' ? 'исполнения' : 'доставки';


        return {
            deal_name_label,
            deal_term_label
        }
    }

    getCurrentDealTypeIdent() {
        const deal_types = this.state.deal_types;
        const deal_type = this.state.form.deal_type;

        if (deal_types[deal_type]) {
            return deal_types[deal_type].ident;
        } else {
            return 'T'
        }
    }

    getCurrentRefererDealTypeIdent() {
        const deal_types = this.state.deal_types;
        const deal_type = this.state.form.referer_deal_type;

        if (deal_types[deal_type]) {
            return deal_types[deal_type].ident;
        } else {
            return 'T'
        }
    }

    handleName(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value,
                slug: cyrillicToLatin(value, '_')
            }
        }, () => this.saveFormToLocalStorage());
    }

    handleChangeButtonName(name, value) {
        const { was_slug_focused } = this.state;

        if (was_slug_focused) {
            this.handleChange(name, value);
        } else {
            this.handleName(name, value);
        }
    }

    turnOnSlugFocus() {
        this.setState({
            was_slug_focused: true
        })
    }

    checkIsSlugNameUnique() {
        const url = '/profile/partner-button/slug';
        const { slug, csrf } = this.state.form;
        customFetch(url, {
            method: 'POST',
            body: JSON.stringify({
                slug,
                csrf
            })
        })
            .then(data => {
                if (data.status === 'SUCCESS') {
                    this.setState({
                        is_slug_not_unique: !!data.data
                    })

                } else if (data.status === 'ERROR') {
                    console.log('ERROR')
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
            });
    }

    toggleFormIntegrationRequest() {
        this.setState({
            is_integration_request_form_opened: !this.state.is_integration_request_form_opened
        });
    }

    getButtonNameControl(props) {
        const name = this.state.form.name;
        return (
            <ButtonName
                name="name"
                label="Название кнопки:"
                placeholder="Введите название"
                value_prop={name}
                handleComponentChange={this.handleChangeButtonName}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.name}
                is_disable={this.state.disabled_controls.name}
                formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                handleBlur={this.checkIsSlugNameUnique}
                {...props}
            />
        );
    }

    getSlugNameControl(props) {
        const slug = this.state.form.slug;
        const { was_slug_focused, is_slug_not_unique } = this.state;
        return (
            <div className="slug-input">
                <SlugName
                    name="slug"
                    label="URL-адрес кнопки:"
                    value_prop={cyrillicToLatin(slug, '_')}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.slug}
                    is_disable={this.state.disabled_controls.name}
                    formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                    was_focused={was_slug_focused}
                    toggleFocus={this.turnOnSlugFocus}
                    is_slug_not_unique={is_slug_not_unique}
                    {...props}
                />
            </div>
        );
    }

    getIntegrationRequestLink(props) {
        return (
            <div className='row'>
                <div className="col-x1-12 integration_request_link">
                    <span className="integration_icon"></span><a onClick={this.toggleFormIntegrationRequest}>Заявка на интеграцию</a>
                </div>
            </div>
        );
    }

    getIntegrationRequestPopUp(props) {
        return (
            <Popup close={this.toggleFormIntegrationRequest} popupClassName="integration_request_popup">
                <IntegrationRequestCreate close={this.toggleFormIntegrationRequest} />
            </Popup>
        );
    }

    getStringLabelControl(text) {
        const { deal_role } = this.state.form;
        const counter_agent_role_dp = deal_role === 'customer' ? 'продавцу' : 'покупателю';
        return (
            <div className='row'>
                <div className="col-x1-12 form_string_label">
                    Вы можете выбрать условия, которые будут добавлены в код кнопки создания сделки. Остальные обязательные условия необходимо будет заполнить {counter_agent_role_dp}.
                </div>
            </div>
        );
    }


    getButtonAuthorRoleControl(props) {
        const { button_author_role } = this.state.form;

        return (
            <ButtonAuthorRole
                label="Вы будете размещать кнопку как:"
                name="button_author_role"
                value_prop={button_author_role}
                deal_type={this.getCurrentDealTypeIdent()}
                handleComponentChange={this.handleChangeDealRole}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.button_author_role}
                is_disable={this.state.disabled_controls.button_author_role}
                {...props}
            />
        );
    }

    getDealRoleControl(props) {
        const { deal_role } = this.state.form;

        return (
            <DealRole
                label="Ваша роль в сделке:"
                name="deal_role"
                value_prop={deal_role}
                deal_type={this.getCurrentDealTypeIdent()}
                handleComponentChange={this.handleChangeDealRole}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.deal_role}
                is_disable={this.state.disabled_controls.deal_role}
                {...props}
            />
        );
    }

    getDealTypeControl(props) {
        const { deal_types } = this.state;
        const deal_type = this.state.form.deal_type;

        return (
            <div className="Input-Wrapper">
                <DealType
                    label="Тип сделки:"
                    name="deal_type"
                    deal_types={deal_types}
                    value_prop={deal_type}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.deal_type}
                    is_disable={this.state.disabled_controls.deal_type}
                    {...props}
                />
            </div>
        );
    }

    //Т.к. поля для рефера не обязательны к заполнению, отдельные контролы на тип сделки и роль приглашенного пользователя в сделке
    getDealTypeEnabableControl(props) {
        const { deal_types, form: { deal_type } } = this.state;
        const referer_deal_type = this.state.form.referer_deal_type;

        return (
            <div className="Input-Wrapper">
                <DealType
                    label="Тип сделки:"
                    name="deal_type"
                    deal_types={deal_types}
                    value_prop={deal_type}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.deal_type}
                    is_disable={this.state.disabled_controls.deal_type}
                    is_disabable={true}
                    id_checkbox="deal_type_control"
                    setControlDisabled={this.setControlDisabled}
                    {...props}
                />
            </div>
        )
    }

    getInvitedUserRoleEnabableControl() {
        const deal_role = this.state.form.deal_role;
        return (
            <DealRole
                label="Роль пользователя в сделке:"
                name="deal_role"
                value_prop={deal_role}
                deal_type={this.getCurrentRefererDealTypeIdent()}
                handleComponentChange={this.handleChangeDealRole}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.deal_role}
                is_disable={this.state.disabled_controls.deal_role}
                is_disabable={true}
                id_checkbox="deal_role_control"
                setControlDisabled={this.setControlDisabled}
            />
        )
    }

    getDealNameControl(props) {
        const name = this.state.form.deal_name;
        return (
            <div className="deal-name-input">
                <DealName
                    name="deal_name"
                    value_prop={name}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                    form_control_server_errors={this.state.controls_server_errors.deal_name}
                    is_disable={this.state.disabled_controls.deal_name}
                    is_disabable={true}
                    id_checkbox="deal_name_control"
                    setControlDisabled={this.setControlDisabled}
                    {...props}
                />
            </div>
        );
    }

    getAddonTermsControl(props) {
        const deal_description = this.state.form.deal_description;
        return (
            <AddonTerms
                name="deal_description"
                value_prop={deal_description}
                handleComponentChange={this.handleChange}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.deal_description}
                is_disable={this.state.disabled_controls.deal_description}
                is_disabable={true}
                id_checkbox="deal_description_control"
                setControlDisabled={this.setControlDisabled}
                formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                {...props}
            />
        );
    }

    getFileControl(props) {
        return (
            <File
                name="files"
                label="Прикрепить файл:"
                handleComponentChange={this.handleChange}
                is_disable={this.state.disabled_controls.files}
                is_disabable={true}
                id_checkbox="files_control"
                setControlDisabled={this.setControlDisabled}
                {...props}
            />
        );
    }

    getAmountControl(props) {
        const amount = this.state.form.amount;
        const max_deal_amount = this.state.max_deal_amount;
        return (
            <Amount
                name='amount'
                value_prop={amount}
                max_amount={max_deal_amount}
                handleComponentChange={this.handleChange}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.amount}
                is_disable={this.state.disabled_controls.amount}
                is_disabable={true}
                id_checkbox="amount_control"
                setControlDisabled={this.setControlDisabled}
                formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                {...props}
            />
        );
    }

    getDeliveryPeriodControl(props) {
        const delivery_period = this.state.form.delivery_period;
        return (
            <div className="partner-button-form__delivery-period">
                <DeliveryPeriod
                    name='delivery_period'
                    value_prop={delivery_period}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.delivery_period}
                    is_disable={this.state.disabled_controls.delivery_period}
                    is_disabable={true}
                    id_checkbox="delivery_period_control"
                    setControlDisabled={this.setControlDisabled}
                    formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                    {...props}
                />
            </div>
        );
    }

    getFeePayerOptionsControl(props) {
        const { fee_payer_options } = this.state;
        const { fee_payer_option } = this.state.form;

        return (
            <div className="Input-Group-Wrapper">
                <FeePayerOption
                    label="Кто оплачивает комиссию:"
                    name="fee_payer_option"
                    fee_payer_options={fee_payer_options}
                    deal_type={this.getCurrentDealTypeIdent()}
                    value_prop={fee_payer_option}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.fee_payer_option}
                    is_disable={this.state.disabled_controls.fee_payer_option}
                    is_disabable={true}
                    id_checkbox="fee_payer_option_control"
                    setControlDisabled={this.setControlDisabled}
                    formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                    {...props}
                />
            </div>
        );
    }

    getCivilLawSubjectControl(props) {
        const counteragent_civil_law_subject = this.state.form.counteragent_civil_law_subject;
        const beneficiar_civil_law_subject = this.state.form.beneficiar_civil_law_subject;
        const { natural_persons, legal_entities, sole_proprietors, update_collection_civil_law_subjects, clear_local_storage, form: {button_author_role} } = this.state;
        return (
            <div className="row">
                <div className="col-xl-12">
                    <CivilLawSubject
                        label={button_author_role === 'referer' ? "Владелец кнопки:" : "Вы будете участвовать в сделке как:" }
                        name={button_author_role === 'referer' ? "beneficiar_civil_law_subject" : "counteragent_civil_law_subject" }
                        value_prop={button_author_role === 'referer' ? beneficiar_civil_law_subject : counteragent_civil_law_subject }
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        sendUpData={this.handleStateChange}
                        is_nested={true}
                        form_validate_name={button_author_role === 'referer' ? 'beneficiar_civil_law_subject_form_is_valid' : 'civil_law_subject_form_is_valid' }
                        form_data_name={button_author_role === 'referer' ? 'beneficiar_civil_law_subject_form_data' : 'civil_law_subject_form_data' }
                        natural_persons={natural_persons}
                        legal_entities={legal_entities}
                        sole_proprietors={sole_proprietors}
                        update_collection={update_collection_civil_law_subjects}
                        form_control_server_errors={button_author_role === 'referer' ? this.state.controls_server_errors.beneficiar_civil_law_subject : this.state.controls_server_errors.counteragent_civil_law_subject }
                        form_server_errors={button_author_role === 'referer' ? this.state.controls_server_errors.beneficiar_civil_law_subject_form : this.state.controls_server_errors.counteragent_civil_law_subject_form }
                        clear_local_storage={clear_local_storage}
                        {...props}
                    />
                </div>
            </div>
        );
    }

    getCivilLawSubjectAndPaymentMethodControl(props) {
        const { counteragent_civil_law_subject, counteragent_payment_method, button_author_role, beneficiar_civil_law_subject, beneficiar_payment_method } = this.state.form;
        const { natural_persons, legal_entities, sole_proprietors, update_collection_civil_law_subjects, clear_local_storage } = this.state;
        return (
            <div className="row row-civil-law-subject-payment-method">
                <div className="col-xl-12">
                    <CivilLawSubjectAndPaymentMethod
                        civil_law_subject_label={button_author_role === 'referer' ? 'Владелец кнопки:' : 'Вы будете участвовать в сделке как:'}

                        payment_method_label={button_author_role === 'referer' ? 'Способ получения комиссионных:' : 'Способ получения оплаты:'}

                        civil_law_subject_name={button_author_role === 'referer' ? 'beneficiar_civil_law_subject' : 'counteragent_civil_law_subject'}

                        payment_method_name={button_author_role === 'referer' ? 'beneficiar_payment_method' : 'counteragent_payment_method'}

                        civil_law_subject_id={button_author_role === 'referer' ? beneficiar_civil_law_subject : counteragent_civil_law_subject}

                        payment_method_id={button_author_role === 'referer' ? beneficiar_payment_method : counteragent_payment_method}

                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        handleCivilLawSubject={this.handleChangeCivilLawSubject}
                        handleCivilLawSubjectValid={this.handleCivilLawSubjectValid}
                        form_civil_law_subject_validate_name={button_author_role === 'referer' ? 'beneficiar_civil_law_subject_form_is_valid' : 'civil_law_subject_form_is_valid'}
                        form_civil_law_subject_data_name={button_author_role === 'referer' ? 'beneficiar_civil_law_subject_form_data' : 'civil_law_subject_form_data'}

                        form_payment_method_validate_name={button_author_role === 'referer' ? 'beneficiar_payment_method_form_is_valid' : 'counteragent_payment_method_form_is_valid'}

                        form_payment_method_data_name={button_author_role === 'referer' ? 'beneficiar_payment_method_form_data': 'payment_method_form_data'}


                        sendUpData={this.handleStateChange}
                        natural_persons={natural_persons}
                        legal_entities={legal_entities}
                        sole_proprietors={sole_proprietors}
                        civil_law_subject_update_collection={update_collection_civil_law_subjects}
                        is_nested={true}
                        is_hide_bank_transfer_name={true}
                        form_control_server_errors_civil_law_subject={ button_author_role === 'referer' ? this.state.controls_server_errors.beneficiar_civil_law_subject : this.state.controls_server_errors.counteragent_civil_law_subject}
                        form_control_server_errors_civil_law_subject_form={button_author_role === 'referer' ? this.state.controls_server_errors.beneficiar_civil_law_subject_form : this.state.controls_server_errors.counteragent_civil_law_subject_form}

                        form_control_server_errors_payment_method={button_author_role === 'referer' ? this.state.controls_server_errors.beneficiar_payment_method : this.state.controls_server_errors.counteragent_payment_method}
                        form_control_server_errors_payment_method_form={button_author_role === 'referer' ? this.state.controls_server_errors.beneficiar_payment_method_form : this.state.controls_server_errors.counteragent_payment_method_form}
                        clear_local_storage={clear_local_storage}
                        {...props}
                    />
                </div>
            </div>
        );
    }


    getPaymentMethodControl(props) {
        const { counteragent_civil_law_subject, counteragent_payment_method, beneficiar_civil_law_subject, beneficiar_payment_method, button_author_role } = this.state.form;
        const { natural_persons, legal_entities, sole_proprietors, clear_local_storage } = this.state;

        return (
            <div className="row row-payment-method">
                <div className="col-xl-12">
                    <PaymentMethod
                        label="Способ получения комиссионных:"
                        name={button_author_role !== 'referer' ? 'counteragent_payment_method' : 'beneficiar_payment_method' }
                        value_prop={button_author_role !== 'referer' ? counteragent_payment_method : beneficiar_payment_method}
                        civil_law_subject_id={counteragent_civil_law_subject}
                        natural_persons={natural_persons}
                        legal_entities={legal_entities}
                        sole_proprietors={sole_proprietors}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        form_validate_name={button_author_role !== 'referer' ? 'counteragent_payment_method_form_is_valid' : 'beneficiar_payment_method_form_is_valid'}
                        form_data_name={button_author_role !== 'referer' ? 'counteragent_payment_method_form_data' : 'beneficiar_payment_method_form_data'}
                        sendUpData={this.handleStateChange}
                        is_nested={true}
                        is_hide_name={true}
                        form_control_server_errors={button_author_role !== 'referer' ? this.state.controls_server_errors.counteragent_payment_method : this.state.controls_server_errors.beneficiar_payment_method}
                        form_server_errors={button_author_role !== 'referer' ? this.state.controls_server_errors.counteragent_payment_method_form :  this.state.controls_server_errors.beneficiar_payment_method_form}
                        clear_local_storage={clear_local_storage}
                        {...props}
                    />
                </div>
            </div>
        );
    }

    //При заполнении от имени реферера
    getCounterAgentEmailControl(props) {
        const { deal_role, counteragent_email} = this.state.form;

        return (
            <div className="Input-Group-Wrapper">
                <EmailCounterAgent
                    label='Кто предлагает оферту:'
                    placeholder='Email пользователя'
                    deal_type={this.getCurrentDealTypeIdent()}
                    deal_role={deal_role}
                    value_prop={counteragent_email}
                    handleComponentValidation={this.handleComponentValid}
                    handleComponentChange={this.handleChange}
                    form_control_server_errors={this.state.controls_server_errors.counteragent_email}
                    formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                    {...props}
                />
            </div>
        );
    }

    getButtonCreateConfirmControl() {
        const button_create_confirm = this.state.form.button_create_confirm;
        return (
            <div className="row rules-agree">
                <div className="col-xl-12">
                    <DealConfirm
                        value_prop={button_create_confirm}
                        name='button_create_confirm'
                        label_text={<span>Нажимая на кнопку “Сформировать кнопку”, вы соглашаетесь с <a href={URLS.TERMS_OF_USE} target="_blank" className="info-link">регламентом пользования сервиса.</a></span>}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                </div>
            </div>
        );
    }

    getFormErrorsControl() {
        return (
            <div className="row messages">
                <div className="col-xl-12">
                    <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                </div>
            </div>
        );
    }

    getButtonsControl(props) {
        const { form_isValid, form_isSubmitting } = this.state;
        return (
            <div className="ButtonsRow ButtonsRow_form_deal">
                <button
                    onClick={this.handleSubmit}
                    className={`ButtonApply ${form_isSubmitting ? 'Preloader Preloader_light' : ''}`}
                    disabled={!form_isValid || form_isSubmitting}
                    data-ripple-button=""
                >
                    <span className="ripple-text">Сформировать кнопку</span>
                </button>
            </div>
        );
    }


    getFormTitleControl(props) {
        const { title } = props;
        return (
            <div className="row">
                <div className="col-xl-12">
                    <h3 className="TitleBlue">{title}</h3>
                </div>
            </div>
        );
    }

    getFormLogoControl() {
        return (
            <div className="Logo">
                <div className="Logo-Img">
                    <img src="../../../../img/logo.svg" alt="Логотип"/>
                </div>
                <p className="Logo-Text" />
            </div>
        );
    }

    getControls() {
        const { deal_name_label, deal_term_label } = this.getLabels();

        return {
            form_logo: this.getFormLogoControl(),
            form_title_control: (props) => this.getFormTitleControl({
                title: 'Создание кнопки-приглашения',
                ...props
            }),
            integration_request_link: (props) => this.getIntegrationRequestLink(props),
            name_control: (props) => this.getButtonNameControl(props),
            slug_control: (props) => this.getSlugNameControl(props),
            button_author_role_control: (props) => this.getButtonAuthorRoleControl(props),
            deal_role_control: (props) => this.getDealRoleControl(props),
            deal_type_control: (props) => this.getDealTypeControl(props),
            deal_name_control: (props) => this.getDealNameControl({
                placeholder: `Введите название ${deal_name_label}`,
                label: `Название ${deal_name_label}:`,
                ...props
            }),
            deal_description_control: (props) => this.getAddonTermsControl({
                label: `Описание ${deal_name_label}`,
                placeholder: `Введите описание ${deal_name_label}`,
                ...props
            }),
            file_control: (props) => this.getFileControl(props),
            amount_control: (props) => this.getAmountControl({
                label: `Цена ${deal_name_label}:`,
                placeholder: `Введите цену`,
                ...props
            }),
            delivery_period_control: (props) => this.getDeliveryPeriodControl({
                label: `Срок ${deal_term_label} и проверки (дней):`,
                placeholder: `Введите срок ${deal_term_label} и проверки ${deal_name_label}`
            }),
            fee_payer_options_control: (props) => this.getFeePayerOptionsControl(props),
            civil_law_subject_control: (props) => this.getCivilLawSubjectControl(props),
            civil_law_subject_and_payment_method_control: (props) => this.getCivilLawSubjectAndPaymentMethodControl(props),
            payment_method_control: (props) => this.getPaymentMethodControl(props),
            counteragent_email_control: (props) => this.getCounterAgentEmailControl(props),
            deal_type_enabable_control: (props) => this.getDealTypeEnabableControl(props),
            invited_user_role_enabable_control: (props) => this.getInvitedUserRoleEnabableControl(props),
            string_label_control: (props) => this.getStringLabelControl(props),
            button_create_confirm_control: this.getButtonCreateConfirmControl(),
            form_errors_control: this.getFormErrorsControl(),
            buttons_control: (props) => this.getButtonsControl(props),
            agent_email_control: (props) => this.getAgentEmailControl(props), // Контрол для других форм наследуемых от формы FormDeal
            integration_request_popup: (props) => this.getIntegrationRequestPopUp(props)
        }
    }

    getTemplates() {
        const { form_isPreloader, date_of_create, formHasDataAtLocalStorage, is_integration_request_form_opened } = this.state;
        const { deal_role, button_author_role } = this.state.form;

        const controls = this.getControls();
        const label = 'Владелец кнопки:';
        return (
            {
                base_form:
                    <FormWrapper is_loading={form_isPreloader} date_of_create={date_of_create} logo={controls.form_logo} title="Создание кнопки-приглашения" mobile_title="Создание кнопки-приглашения к сделке">
                        { controls.integration_request_link() }
                        { controls.name_control() }
                        { controls.slug_control() }
                        { controls.button_author_role_control() }
                        {
                            button_author_role === 'referer' ?
                                controls.civil_law_subject_and_payment_method_control()
                                :
                                void(0)
                        }
                        {
                            button_author_role === 'referer' ?
                                controls.counteragent_email_control()
                                :
                                void(0)
                        }
                        {
                            button_author_role === 'participant' ?
                                controls.deal_type_control()
                                :
                                void(0)
                        }
                        {
                            button_author_role === 'participant' ?
                                controls.deal_role_control()
                                :
                                void(0)
                        }
                        {
                            button_author_role === 'participant' ?
                                (
                                    deal_role === 'contractor' ?
                                        controls.civil_law_subject_and_payment_method_control()
                                        :
                                        controls.civil_law_subject_control()
                                )
                                :
                                void(0)
                        }
                        { controls.string_label_control() }

                        {
                            button_author_role === 'referer' ?
                                controls.deal_type_enabable_control()
                                :
                                void(0)
                        }
                        {
                            button_author_role === 'referer' ?
                                controls.invited_user_role_enabable_control()
                                :
                                void(0)
                        }
                        { controls.deal_name_control() }
                        { controls.deal_description_control() }
                        {/*{ controls.file_control() }*/}
                        <div className="row">
                            <div className="col-xl-6 col-sm-4">
                                { controls.amount_control() }
                            </div>
                            <div className="col-xl-6 col-sm-4">
                                { controls.delivery_period_control() }
                            </div>
                        </div>
                        { controls.fee_payer_options_control() }
                        { controls.button_create_confirm_control }
                        { controls.form_errors_control }
                        { controls.buttons_control() }
                        {
                            is_integration_request_form_opened &&
                            controls.integration_request_popup()
                        }
                    </FormWrapper>,
                form_with_role_and_type:
                    <FormWrapper
                        is_loading={form_isPreloader}
                        date_of_create={date_of_create}
                        logo={controls.form_logo}
                        mobile_title={this.getFormWithRoleAndTypeTitle()}
                    >
                        <DisplayNone>
                            { controls.deal_type_control() }
                            { controls.deal_role_control() }
                        </DisplayNone>
                        {
                            controls.form_title_control({
                                title: this.getFormWithRoleAndTypeTitle()
                            })
                        }
                        { controls.deal_name_control() }
                        { controls.deal_description_control() }
                        {/*{ controls.file_control() }*/}
                        <div className="row">
                            <div className="col-xl-6 col-sm-4">
                                { controls.amount_control() }
                            </div>
                            <div className="col-xl-6 col-sm-4">
                                { controls.delivery_period_control() }
                            </div>
                        </div>
                        { controls.fee_payer_options_control() }
                        {
                            deal_role === 'contractor' ?
                                controls.civil_law_subject_and_payment_method_control()
                                :
                                controls.civil_law_subject_control()
                        }
                        { controls.button_create_confirm_control }
                        { controls.form_errors_control }
                        { controls.buttons_control() }
                        {
                            is_integration_request_form_opened &&
                            controls.integration_request_popup()
                        }
                    </FormWrapper>
            }
        )
    }

    render() {
        const { form_template } = this.state;

        const templates = this.getTemplates();

        return (
            templates[form_template]
        )
    }
}