import React from 'react';
import FormBase from './FormBase';
import URLS from '../constants/urls';
import { customFetch, getIdFromUrl } from "../Helpers";


export default class FormPayMandarin extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            form_isValid: false,
            amount_without_fee: '',
            customer_fee: '',
            customer_amount: '',
            deal_status: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.formInit = this.formInit.bind(this);
    }

    componentDidMount() {
        this.toggleIsLoading();

        const mandarinScript = document.createElement("script");

        mandarinScript.src = URLS.MANDARIN_PAY;
        mandarinScript.async = true;
        mandarinScript.onload = this.formInit;

        document.getElementById("ControllerPayMandarin").appendChild(mandarinScript);
        $ && this.formPay && $('html, body').animate({
            scrollTop: $(this.formPay).offset().top - $('.Header').outerHeight()
        }, 400);
    }

    formInit() {
        const deal_id = getIdFromUrl();
        const url = deal_id ? `${URLS.DEAL.FORM_INIT}?idDeal=${deal_id}` : URLS.DEAL.FORM_INIT;

        customFetch(url)
            .then(response => {
                const { amount_without_fee, customer_fee, customer_amount, available_payment_method_types, status } = response.data.deal;

                if (status.status !== 'confirmed') {
                    window.location.reload(true);
                } else {
                    this.setState({
                        amount_without_fee,
                        customer_fee,
                        customer_amount,
                        available_payment_method_types
                    });
                }
        });

        customFetch(URLS.MANDARIN.OPERATION_ID, {
            method: 'POST',
            body: JSON.stringify({
                id: getIdFromUrl()
            })
        })
            .then(response => {
                this.toggleIsLoading();
                if (typeof mandarinpay.hosted.setup === 'function') {
                    mandarinpay.hosted.setup("#form-hosted-pay", {
                        operationId: response.data.jsOperationId,
                        onsuccess: function(data) {
                            window.location = data.__private.returnUrl;
                            //alert("Success, id: " + data.transaction.id);
                        },
                        onerror: function(data) {
                            alert("Error: " + data.error);
                        },
                        fields:
                            {
                                "card-number": {
                                    settings: {
                                        placeholder: "Номер карты",
                                        styles: {
                                            "font-size": "14px",
                                            "font-family": "Roboto",
                                            "color": "#354052",
                                        },
                                        placeholderStyles: {
                                            "font-size": "14px",
                                            "color": "#C5D0DE",
                                            "font-weight": "300",
                                            "font-style": "italic"
                                        }
                                    }
                                },
                                "card-expiration": {
                                    settings: {
                                        placeholder: "01/18",
                                        styles: {
                                            "font-size": "14px",
                                            "font-family": "Roboto",
                                            "color": "#354052"
                                        },
                                        placeholderStyles: {
                                            "font-size": "14px",
                                            "color": "#C5D0DE",
                                            "font-weight": "300",
                                            "font-style": "italic"
                                        }
                                    }
                                },
                                "card-holder": {
                                    settings: {
                                        placeholder: "Card holder",
                                        styles: {
                                            "font-size": "14px",
                                            "font-family": "Roboto",
                                            "color": "#354052"
                                        },
                                        placeholderStyles: {
                                            "font-size": "14px",
                                            "color": "#C5D0DE",
                                            "font-weight": "300",
                                            "font-style": "italic"
                                        }
                                    }
                                },
                                "card-cvv": {
                                    settings: {
                                        placeholder: "CVC/CVV",
                                        styles: {
                                            "font-size": "14px",
                                            "font-family": "Roboto",
                                            "color": "#354052"
                                        },
                                        placeholderStyles: {
                                            "font-size": "14px",
                                            "color": "#C5D0DE",
                                            "font-weight": "300",
                                            "font-style": "italic"
                                        }
                                    }
                                }
                            }
                    });
                }
            })
            .catch(error => {
                this.toggleIsLoading();
                console.log(error.message);
            });
    }

    handleSubmit(e) {
        e.preventDefault();
        const deal_id = getIdFromUrl();
        const url = deal_id ? `${URLS.DEAL.FORM_INIT}?idDeal=${deal_id}` : URLS.DEAL.FORM_INIT;

        customFetch(url)
            .then(response => {
                const { status } = response.data.deal;
                this.setState({
                    deal_status: status.status
                }, () => {
                    const { deal_status } = this.state;
                    if (deal_status === 'confirmed') {
                        return mandarinpay.hosted.process('#form-hosted-pay');
                    } else {
                        window.location.reload(true);
                    }
                });
            });

    }

    render() {
        const { amount_without_fee, customer_fee, customer_amount, form_isLoading } = this.state;
        const { toggleVisibility } = this.props;

        return (
            <div className={"row nested-row form-pay-mandarin " + (form_isLoading && "Preloader")} ref={node => this.formPay = node}>
                <h3 className="col-xl-12 TitleBlue">Введите реквизиты банковской карты</h3>
                <div className="col-xl-12 title-block">
                    <div className="title">
                        <p className="payment-subtitle">
                            Информация о карте передается в процессинговый центр в зашифрованном виде по защищенному протоколу HTTPS/SSL. Сервис «Гарант Пей» не имеет доступа к вводимой информации.
                        </p>
                    </div>
                    <div className="ssl-logo icon-ssl-logo" tabIndex="-1" />
                </div>
                <div className="col-xl-12">
                    <form id="form-hosted-pay">
                        <div className="card-container">
                            <div className="card card-back-side">
                                <div className="magnet-stripe"/>
                                <div className="back-card-line">
                                    <span className="field-label cvv-label">CVV2/CVC2</span>
                                    <div className="mandarinpay-field-card-cvv hosted-field" tabIndex="-1"/>
                                    <span className="cvv-tip">Последние 3 цифры на обратной стороне</span>
                                </div>
                            </div>
                            <div className="card card-front-side">
                                <div className="payment-systems-logo icon-payment-systems" tabIndex="-1">
                                    <div className="icon-payment-mir"/>
                                    <div className="icon-payment-mastercard"/>
                                    <div className="icon-payment-visa"/>
                                </div>
                                <div className="card-line">
                                    <span className="field-label">Номер карты</span>
                                    <div className="mandarinpay-field-card-number hosted-field" tabIndex="-1"/>
                                </div>
                                <div className="card-line">
                                    <span className="field-label expiration-date-label">Дата окончания действия карты</span>
                                    <span className="field-label expiration-date-label-adaptive">Действительна до</span>
                                    <div className="mandarinpay-field-card-expiration hosted-field" tabIndex="-1"/>
                                </div>
                                <span className="field-label">Имя владельца карты</span>
                                <div className="mandarinpay-field-card-holder hosted-field" tabIndex="-1"/>
                            </div>
                        </div>
                        <div className="row nested-row mandarin-form-payment-info">
                            <div className="col-xl-6 col-md-6 payment-cancel">
                                <p><b>Внимание! </b>
                                    Если ваша карта поддерживает технологию обеспечения безопасности интернет платежей 3D Secure, Вы будете отправлены на страницу банка, выпустившего Вам карту.</p>
                                <button type="button" className="ButtonRefuse" onClick={toggleVisibility} data-ripple-button="">
                                    <span className="ripple-text">Отменить оплату картой</span>
                                </button>
                            </div>
                            <div className="col-xl-6 col-md-6 payment-submit">
                                <table className="payment-statistics">
                                    <tbody>
                                    <tr>
                                        <td>Сумма по сделке: </td>
                                        <td className="money">{amount_without_fee} Р.</td>
                                    </tr>
                                    <tr>
                                        <td>Комиссия сервиса: </td>
                                        <td className="money">{customer_fee} Р.</td>
                                    </tr>
                                    <tr className="sum">
                                        <td>Сумма к оплате:</td>
                                        <td className="money">{customer_amount} Р.</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <button className="ButtonApply" onClick={this.handleSubmit} data-ripple-button=""><span className="ripple-text">Оплатить</span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}