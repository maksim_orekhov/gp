import React from 'react';
import MESSAGES from '../constants/messages';
import URLS from '../constants/urls';
import REQUEST_CODES from '../constants/request_codes'
import { customFetch } from '../Helpers';

export default class FormBase extends React.Component {
    getCsrf() {
        return customFetch(URLS.CSRF.GET)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const { csrf = '' } = data.data;
                    this.setState({
                        form: {
                            ...this.state.form,
                            csrf
                        }
                    }, () => {
                        return Promise.resolve();
                    });
                } else if (data.status === 'ERROR') {
                    return Promise.reject(data.message);
                }
            })
            .catch(error => {
                this.setFormError('form_init_fail', true);
            });
    }

    handleChange(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value
            }
        }, () => this.saveFormToLocalStorage());
    }

    saveFormToLocalStorage() {
        if (localStorage && this.form_name) {
            const form = {...this.state.form};
            delete form.csrf; // Удаляем csrf

            localStorage.setItem(this.form_name, JSON.stringify(form));
        }
    }

    getFormFromLocalStorage() {
        return new Promise((resolve, reject) => {
            if (localStorage && this.form_name && localStorage.hasOwnProperty(this.form_name)) {
                const form = JSON.parse(localStorage.getItem(this.form_name));

                this.setState({
                    formHasDataAtLocalStorage: true,
                    form: {
                        ...this.state.form,
                        ...form
                    }
                }, () => {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    clearFormLocalStorage() {
        if (localStorage && this.form_name && localStorage.hasOwnProperty(this.form_name)) {
            localStorage.removeItem(this.form_name);
        }
    }

    makeValidateUpdateFunction(name, value) {
        return (() => {
            this.can_update_validation = false;

            if (this.state.validation[name] === value) {
                this.checkAllControlsAreValid();
                this.can_update_validation = true;
            } else {
                this.setState({
                    validation: {
                        ...this.state.validation,
                        [name]: value
                    }
                }, () => {
                    this.checkAllControlsAreValid();
                    this.can_update_validation = true;
                });
            }
        });
    }

    handleComponentValid(name, value) {
        this.updateValidationFunctions.push(this.makeValidateUpdateFunction(name, value));
    }

    checkAllControlsAreValid() {
        const validation_values = this.state.validation;

        let form_isValid = true;

        Object.keys(validation_values).forEach(value => {
            if (!validation_values[value]) {
                form_isValid = false
            }
        });
        if (this.props.editMode) {
            const current_state = JSON.stringify(this.state.form);
            if (current_state === this.prev_state) {
                form_isValid = false
            }
        }
        this.setState({
            form_isValid
        }, () => this.sendUpFormData());
    }

    sendUpFormData() {
        if (this.props.is_nested) {
            const { form_isValid } = this.state;
            const { sendUpData, handleComponentValidation, form_validate_name, form_data_name } = this.props;

            handleComponentValidation && handleComponentValidation(form_validate_name, form_isValid);
            if (form_isValid) {
                sendUpData && sendUpData(form_data_name, this.getDataFromState());
            }
        }
    }

    getDataFromState() {
        return this.state.form;
    }

    toggleIsLoading(name = 'form_isSubmitting') {
        this.setState({
            form_isLoading: !this.state.form_isLoading,
            [name]: !this.state[name]
        });
    }

    /**
     * При ответе бэка о невалидности формы распределяем ошибки по их контролам
     * @param {Object} errors - Объект с ошибками с бэка
     */
    TakeApartInvalidFormData(errors) {
        this.setFormError('invalid_form_data', true);

        if (errors) {
            const controls_server_errors = {...this.state.controls_server_errors};

            Object.keys(errors).forEach(control => {
                if (control === 'csrf') {
                    this.setFormError('csrf', true);
                }
                controls_server_errors[control] = errors[control]
            });

            this.setState({
                controls_server_errors
            });
        }
    }

    setUndefinedFormError(data) {
        this.setFormError('undefined_error', true);

        this.setState({
            server_errors_messages: {
                ...this.state.server_errors_messages,
                undefined_error: data.message
            }
        });
    }

    /**
     * Зажигаем флаг ошибки с бэка
     */
    setFormError(name, value) {
        const server_errors = {...this.state.server_errors}; // Очищаем флаги прошлого запроса
        Object.keys(server_errors).forEach(error => {
            server_errors[error] = false;
        });

        this.setState({
            server_errors: {
                ...server_errors,
                [name]: value
            }
        });
    };

    /**
     * Кастомная функция разбора ошибок с бэка и обновления нужного флага ошибки
     * @param {Object} data - объект ответа с бэка
     * @param {boolean} not_login_form - костыль, что это не формы авторизации
     */
    // !!!! not_login_form - чисто костыль для формы FormLoginEdit, потому что там прилетала ошибка сервера с data.data.login
    // !!! и выводилась ошибка this.setFormError('auth_failed', true); (что неправильно введен логин или пароль), а нам это не нужно в той форме
    TakeApartErrorFromServer(data, not_login_form = false) {
        switch (data.code) {
            case REQUEST_CODES.INVALID_FORM_DATA:
                if (data.data && data.data.captcha && !data.data.login) {
                    this.setFormError('invalid_captcha', true);
                } else if (!not_login_form && data.data && data.data.login) {
                    this.setFormError('auth_failed', true);
                } else {
                    this.TakeApartInvalidFormData(data.data);
                }
                break;
            case REQUEST_CODES.ACCESS_DENIED:
                this.setFormError('access_denied', true);
                break;
            case REQUEST_CODES.NOT_AUTHORIZED:
                this.setFormError('not_auth', true);
                break;
            case REQUEST_CODES.PHONE_INVALID_CODE:
                this.setFormError('invalid_code', true);
                break;
            case REQUEST_CODES.PHONE_COUNTRY_NOT_DEFINED:
                this.setFormError('country_not_defined', true);
                break;
            case REQUEST_CODES.ALREADY_AUTHORIZED:
                this.setFormError('already_auth', true);
                break;
            case REQUEST_CODES.AUTHENTICATION_FAILED:
                this.setFormError('auth_failed', true);
                break;
            case REQUEST_CODES.CLS_CONFIRMED_DEAL_REMOVAL:
                this.setFormError('cls_confirmed_deal_removal', true);
                break;
            case REQUEST_CODES.CLS_LINKED_WITH_TOKEN_REMOVAL:
                this.setFormError('cls_linked_with_token_removal', true);
                break;
            case REQUEST_CODES.DEAL_WITH_SUCH_ID_NOT_FOUND:
                this.setFormError('deal_with_such_id_not_found', true);
                break;
            case REQUEST_CODES.DEAL_INVITATION_NOT_SENT:
                this.setFormError('deal_invitation_not_sent', true);
                break;
            case REQUEST_CODES.CIVIL_LAW_SUBJECT_NOT_FOUND_TYPE:
                this.setFormError('cls_not_found_type', true);
                break;
            default:
                this.setUndefinedFormError(data);
        }
    };

    // Метод для контрола CivilLawSubjectAndPaymentMethod
    handleChangeCivilLawSubject(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                payment_method: '',
                [name]: value
            }
        });
    }

    // Метод для контрола CivilLawSubjectAndPaymentMethod
    handleCivilLawSubjectValid(name, value) {
        this.handleComponentValid(name, value);
        this.handleComponentValid('payment_method_is_valid', false);
    }

    handleStateChange(name, value) {
        this.setState({
            [name]: value
        })
    };

    // В этом методе разбираем ошибки полученные из компонента сверху, если форма вложена
    SetControlsErrorFromUpperForm(currentErrors, nextErrors) {
        if (JSON.stringify(currentErrors) !== JSON.stringify(nextErrors)) {
            if (nextErrors) {
                const controls_server_errors = {...this.state.controls_server_errors};

                Object.keys(nextErrors).forEach(control => {
                    controls_server_errors[control] = nextErrors[control]
                });

                this.setState({
                    controls_server_errors
                });
            }
        }
    }

    // Проверяем очередь для обновления флагов валидация контрола в state
    tryToRunValidationFunctions() {
        if (this.updateValidationFunctions.length && this.can_update_validation) {
            (this.updateValidationFunctions.shift())();
        }
    }

    rememberState() {
        this.prev_state = JSON.stringify(this.state.form);
        this.checkAllControlsAreValid();
    }
}