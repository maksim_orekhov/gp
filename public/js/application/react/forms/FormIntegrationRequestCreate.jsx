import React from 'react';
import Email from '../controls/ControlEmail';
import ContactName from '../controls/ControlDeliveryOrderName';
import TargetUrl from '../controls/ControlUrl';
import AddonTerms from '../controls/ControlDealAddonTerms';
import FormBase from './FormBase';
import ShowError from '../ShowError';
import { customFetch } from "../Helpers";
import URLS from '../constants/urls';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

const classNames = require('classnames');

export default class FormIntegrationRequestCreate extends FormBase {

    constructor(props) {
        super(props);

        this.state = {
            /**
             * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
             * @example
             *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
             **/
            controls_server_errors: {
                csrf: null,
                email: null,
                name: null,
                target_url: null,
                additional_info: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                email_is_valid: false,
                name_is_valid: false,
                target_url_is_valid: false,
                additional_info_is_valid: false
            },
            // --------------------------------
            form: {
                csrf: '',
                typeForm: 'Сообщение с формы создания запроса на интеграцию: ',
                email: '',
                name: '',
                target_url: '',
                additional_info: ''
            },
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false,
            is_request_sent: false
        };

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.form_name = 'form_integration_create'; // Название формы для хранения данных в local_storage

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCloseForm = this.handleCloseForm.bind(this);
        this.getDataFromState = this.getDataFromState.bind(this);
    }

    componentWillMount() {
        this.getFormFromLocalStorage();
    }

    componentDidMount() {
        if (!this.props.is_nested) {
            this.getCsrf()
                .then(() => this.rememberState());
        }
    }

    componentWillReceiveProps(nextProps) {
        const { form_server_errors, clear_local_storage } = nextProps;

        if (clear_local_storage !== this.props.clear_local_storage) {
            this.clearFormLocalStorage();
        }

        form_server_errors && this.SetControlsErrorFromUpperForm(this.props.form_server_errors, form_server_errors);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    getDataFromProps() {
        const { email, name, target_url, additional_info } = this.props.data;
        this.setState({
            form: {
                ...this.state.form,
                email,
                name,
                target_url,
                additional_info
            }
        }, () => this.rememberState());
    }

    getDataFromState() {
        const { form } = this.state;
        let form_to_send = {
            ...form
        };

        Object.keys(form).map(function(key) {
            let prefix = '';

            switch(key) {
                case 'additional_info':
                    prefix = 'Дополнительные сведения: ';
                    break;
                case 'email':
                    prefix = 'Контактный email: ';
                    break;
                case 'name':
                    prefix = 'Контактное лицо: ';
                    break;
                case 'target_url':
                    prefix = 'Адрес сайта для интеграции: ';
                    break;
                default:
                    prefix = '';
            }
            form_to_send[key] = prefix + form[key];
        });

        return form_to_send;
    }

    handleSubmit(e) {
        e.preventDefault();
        const { form_isValid } = this.state;

        if (form_isValid) {
            this.toggleIsLoading();
            customFetch('/integration-request-sending', {
                method: 'POST',
                body: JSON.stringify(this.getDataFromState())
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.clearFormLocalStorage();
                        this.setState({
                            is_request_sent: true
                        }, () => {
                            setTimeout(() => {this.handleCloseForm()}, 5000);
                        });
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                })
        }
    }

    handleCloseForm() {
        this.props.close && this.props.close();
        this.props.slideUp && this.props.slideUp();
    }

    render() {
        const { isClosable, is_nested } = this.props;
        const { form_isValid, form_isLoading, form_isSubmitting, is_request_sent } = this.state;
        const { email, name, target_url, additional_info } = this.state.form;

        const formTitle = "Заявка на интеграцию";
        const subtitleText =
            is_request_sent ?
                "Ваша заявка отправлена. В ближайшее время с вами свяжется наш специалист для обработки заявки."
                :
                "Оставьте заявку на интеграцию и получите полную информацию и индивидуальные условия сотрудничества.";
        const buttonText = "Отправить";

        const SubmitButtonClassName = classNames({
            'ButtonApply margin-right': true,
            'Preloader Preloader_light': form_isSubmitting
        });

        return (
            <div className="PageForm-Container">
                <div className="FormLabelTop form-default" id='form_integration_request'>
                    {
                        isClosable &&
                        <div className="ButtonClose" onClick={this.handleCloseForm} />
                    }
                    <div className="row nested-row">
                        <div className="col-xl-12 col-sm-4">
                            <h1 className="TitleBlue integration_request_form_title">{formTitle}</h1>
                        </div>
                    </div>
                    <div className="row nested-row">
                        <div className="col-xl-12 col-sm-4 integration_popup_subtitle">
                            <p className={`integration_popup_subtitle_text ${is_request_sent ? 'no_margin' : ''}`}>{subtitleText}</p>
                        </div>
                    </div>
                    {
                        !is_request_sent &&
                        <div>
                            <div className="row nested-row">
                                <div className="col-xl-12 col-sm-4">
                                    <TargetUrl
                                        label="Адрес сайта для интеграции:"
                                        placeholder=""
                                        name="target_url"
                                        value_prop={target_url}
                                        handleComponentChange={this.handleChange}
                                        handleComponentValidation={this.handleComponentValid}
                                        form_control_server_errors={this.state.controls_server_errors.target_url}
                                    />
                                </div>
                            </div>
                            <div className="row nested-row">
                                <div className="col-xl-12 col-sm-4">
                                    <ContactName
                                        label="Как вас зовут:"
                                        placeholder=""
                                        name="name"
                                        value_prop={name}
                                        handleComponentChange={this.handleChange}
                                        handleComponentValidation={this.handleComponentValid}
                                    />
                                </div>
                            </div>
                            <div className="row nested-row">
                                <div className="col-xl-12 col-sm-4">
                                    <div className="Input-Wrapper">
                                        <Email
                                            value_prop={email}
                                            name="email"
                                            label="Контактный email:"
                                            handleComponentChange={this.handleChange}
                                            handleComponentValidation={this.handleComponentValid}
                                            form_control_server_errors={this.state.controls_server_errors.email}
                                            not_check_unique={true}
                                            no_hint={true}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="row nested-row">
                                <div className="col-xl-12 col-sm-4" style={{padding: 0}}>
                                    <AddonTerms
                                        name="additional_info"
                                        label="Дополнительные сведения:"
                                        value_prop={additional_info}
                                        validate_on_mount={true}
                                        handleComponentChange={this.handleChange}
                                        handleComponentValidation={this.handleComponentValid}
                                        form_control_server_errors={this.state.controls_server_errors.additional_info}
                                        formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                                    />
                                </div>
                            </div>
                            <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                            {
                                !is_nested &&
                                <div className="ButtonsRow row nested-row">
                                    <div className="col-xl-12 integration_popup_button">
                                        <button onClick={this.handleSubmit} className={SubmitButtonClassName} disabled={!form_isValid || form_isLoading} data-ripple-button="">
                                            <span className="ripple-text">{buttonText}</span>
                                        </button>
                                    </div>
                                </div>
                            }
                        </div>
                    }
                </div>
            </div>
        );
    }
};
