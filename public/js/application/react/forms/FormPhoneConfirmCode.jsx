import React from 'react';
import FormBase from './FormBase';
import Timer from '../Timer';
import ShowError from '../../../application/react/ShowError';
import PhoneConfirmCode from '../controls/ControlPhoneConfirmCode';
import { customFetch } from "../Helpers";
import URLS from '../constants/urls';
import MESSAGES from "../constants/messages";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../constants/server_errors";

const classNames = require('classnames');

export default class FormPhoneConfirmCode extends FormBase {
    constructor(props) {
        super(props);
        this.state = {
            form: {
                code: ''
            },
            csrf: '',
            form_isLoading: false,
            confirm_isLoading: false,
            resend_isLoading: false,
            form_preloader: false,
            form_isValid: false,

            sms_last_code_created_at: null,
            sms_request_period: null,
            sms_timer_time: null,
            is_user_allowed_sms_request: true,
            is_limit_sms_code: false,

            // Validation props
            validation: {
                code_is_valid: false
            },
            // -----------------------------
            controls_server_errors: {
                csrf: null,
                code: null
            },
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS
        };

        // Поля контролов для автоматической валидации формы
        this.validation_props =
            [
                'code_is_valid'
            ];

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleResendCode = this.handleResendCode.bind(this);
        this.toggleSmsTimer = this.toggleSmsTimer.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        const { handleNextStep } = this.props;
        this.formInit();

        if (handleNextStep) {
            this.setState({
                form_isLoading: true,
                resend_isLoading: true
            })
        }
        /**
         * Проверяем есть ли на странице кнопка отправки кода, она активна если у пользователя нет кодов подтверждения телефона,
         * обычно это происходит если пользователь только зарегистрировался, если такого кода нет то твиг рендерит кнопку с id send-phone-confirm-code
         */
        const code_send_button = document.getElementById('send-phone-confirm-code');
        if (code_send_button) {
            code_send_button.addEventListener('click', (e) => {
                e.preventDefault();
                this.handleResendCode();
            }, {
                once: true // Указывает что метод будет вызван только 1 раз
            });
        }
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    formInit() {
        customFetch(URLS.PROFILE)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const {
                        user_allowed_request_phone_confirmation_code_status,
                        csrf
                    } = data.data;

                    this.setState({
                            csrf,
                            user_allowed_request_phone_confirmation_code_status
                        }, () => {
                            this.checkUserAllowedSmsRequest();
                        }
                    );
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => this.setFormError('form_init_fail', true));
    }

    checkUserAllowedSmsRequest() {
        const { allowed, violation_type, request_period, last_code_created_at } = this.state.user_allowed_request_phone_confirmation_code_status;
        if (!allowed) {
            /**
             * min_interval - Пришло предупреждение с бэка что следующий отправленный код вызовет ошибку
             * и нужно подождать определенное время, т.е. запустить таймер
             */
            if (violation_type === 'min_interval') {
                this.setState({
                    sms_last_code_created_at: last_code_created_at,
                    sms_request_period: request_period
                }, () => this.checkAllowingToSendSmsCodeTimeOut());

            } else if (violation_type === 'limit_by_code_type') { // Пришло сообщение что достингут лимит отправки кодов подтверждения
                this.setState({
                    is_limit_sms_code: true,
                    form_isLoading: false, // Отключаем прелоадеры
                    resend_isLoading: false
                })
            }
        } else {
            this.setState({
                form_isLoading: false,
                resend_isLoading: false // Отключаем прелоадеры
            })
        }
    }

    checkAllowingToSendSmsCodeTimeOut() {
        const { sms_request_period, sms_last_code_created_at } = this.state;
        const now = Date.now() / 1000; // Время сейчас в секундах
        const timer_time = sms_last_code_created_at + sms_request_period - now;

        if (timer_time > 0) {
            this.setState({
                is_user_allowed_sms_request: false,
                sms_timer_time: Math.round(timer_time),
                form_isLoading: false, // Отключаем прелоадеры
                resend_isLoading: false
            });
        }
    }

    toggleSmsTimer(isTimer) {
        this.setState({
            is_user_allowed_sms_request: isTimer
        });
    }

    handleSubmit() {
        const { csrf, form_isValid } = this.state;
        const { code } = this.state.form;

        if (form_isValid) {
            this.toggleIsLoading();
            customFetch(URLS.PHONE.CONFIRM_CODE, {
                method: 'POST',
                body: JSON.stringify({
                    csrf,
                    code
                })
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        console.log(data);
                        const { handleNextStep, is_reload_on_success } = this.props;

                        handleNextStep && handleNextStep('phone');
                        handleNextStep && this.toggleFormPreloader();

                        if (is_reload_on_success) {
                            this.toggleFormPreloader();
                            window.location.reload(false);
                        }

                        this.toggleIsLoading();

                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data);
                        this.toggleIsLoading();
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    handleResendCode() {
        this.toggleIsLoading('resend_isLoading');
        customFetch(URLS.PHONE.RESEND_CODE, {
            method: 'POST',
            body: JSON.stringify({
                csrf
            })
        })
            .then(data => {
                if (data.status === 'SUCCESS') {
                    console.log(data);

                    this.formInit();
                    // Прелоадер отключаем в таймере чтобы не было задержки
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                    this.toggleIsLoading('resend_isLoading');
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading('resend_isLoading');
            });
    }

    toggleIsLoading(name = 'confirm_isLoading') {
        this.setState({
            form_isLoading: !this.state.form_isLoading,
            [name]: !this.state[name]
        });
    }

    toggleFormPreloader() {
        this.setState({
            form_preloader: !this.state.form_preloader
        });
    }

    render() {
        const { form_isLoading, confirm_isLoading, resend_isLoading, is_user_allowed_sms_request, is_limit_sms_code, sms_timer_time, form_preloader, form_isValid } = this.state;
        const { is_closable } = this.props;

        const ConfirmButtonClassName = classNames({
            'ButtonApply': true,
            'Preloader Preloader_light': confirm_isLoading
        });

        const ResendButtonClassName = classNames({
            'ButtonApply': true,
            'Preloader Preloader_light': resend_isLoading
        });

        return (
            <div className={"FormLabelTop" + (form_preloader ? ' Preloader Preloader_solid' : '')}>
                {
                    is_closable && <button className="ButtonClose js-verification-close"/>
                }
                <h3 className="FormVerification-Title">
                    Подтверждение телефона
                </h3>
                <p className="text-green">
                    На указанный вами номер телефона был отправлен код подтверждения
                </p>
                <div className="Input-Wrapper">
                    <label htmlFor="code">Введите код подтверждения</label>
                    <PhoneConfirmCode
                        name="code"
                        placeholder="Код подтверждения"
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        form_control_server_errors={this.state.controls_server_errors.code}
                    />
                </div>
                <button className={ConfirmButtonClassName} disabled={!form_isValid || form_isLoading || confirm_isLoading} onClick={this.handleSubmit} data-ripple-button="">Подтвердить</button>
                {
                    !is_limit_sms_code ?
                        is_user_allowed_sms_request ?
                            <button
                                className={ResendButtonClassName}
                                disabled={form_isLoading || confirm_isLoading}
                                onClick={this.handleResendCode}
                                data-ripple-button=""
                            >
                                Получить код повторно
                            </button>
                            :
                            <p className="mt-20">
                                Вы сможете получить код повторно через <span className="TextAccent"> {/* span на этой строке чтобы сохранить пробел*/}
                                <Timer
                                    start={sms_timer_time}
                                    expired={this.toggleSmsTimer}
                                    isShowTime={true}
                                />
                                </span> секунд.
                            </p>
                        :
                        <p className="TextImportant">Вы достигли лимита получения sms кодов и теперь сможете получить код повторно только через 15 минут.</p>
                }
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
            </div>
        )
    }
};