import React from 'react';
import FormBase from './FormBase';
import Login from '../../../application/react/controls/ControlLoginEdit';
import ShowError from '../../../application/react/ShowError';
import URLS from '../constants/urls';
import { customFetch } from "../Helpers";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../constants/server_errors";

const classNames = require('classnames');

/**
 * Форма смены логина принимающая новый логин
 */
export default class FormLoginEdit extends FormBase {
    constructor(props) {
        super(props);

        /**
         * @param {string} csrf
         * @param {boolean} form_isLoading - флаг что идет запрос на сервер, запускающий прелоадер
         * @param {boolean} form_isValid
         * @param {string} formError - ошибка формы, полученная с бэка при отправке данных
         * @param {string} new_login - новый логин пользователя, получаемый из инпута формы
         */
        this.state = {
            formError: '',
            form_isLoading: false,
            form_isValid: false,
            form_preloader: false,
            login_label: 'Новый логин',
            // Validation props
            validation: {
                new_login_is_valid: false
            },
            // -----------------------------
            form: {
                new_login: '',
                csrf: '',
            },
            controls_server_errors: {
                csrf: null,
                login: null
            },
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS
        };

        // Поля контролов для автоматической валидации формы
        this.validation_props =
            [
                'new_login_is_valid'
            ];

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        this.formInit();
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    formInit() {
        customFetch(URLS.PROFILE)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const {
                        csrf
                    } = data.data;

                    // Так как вложенная структура стейта используем rest оператор как аналог immutable библиотеке
                    const form = {
                        ...this.state.form,
                        csrf
                    };

                    this.setState({ form });
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => this.setFormError('form_init_fail', true));
    }

    handleSubmit() {
        const { new_login, csrf } = this.state.form;

        if (this.state.form_isValid) {
            this.toggleIsLoading();
            customFetch(URLS.LOGIN_EDIT, {
                method: 'POST',
                body: JSON.stringify({
                    csrf,
                    login: new_login
                })
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        window.location.reload(false);

                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data, true);
                        this.toggleIsLoading();
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    toggleFormPreloader() {
        this.setState({
            form_preloader: !this.state.form_preloader
        });
    }

    render() {
        const { form_isLoading, form_isValid, form_preloader, login_label } = this.state;

        const SubmitButtonClassName = classNames({
            'FormVerification-Button ButtonApply': true,
            'Preloader Preloader_light': form_isLoading
        });

        return (
            <div className={"FormLabelTop" + (form_preloader ? ' Preloader Preloader_solid' : '')}>
                <button className="ButtonClose js-verification-close" />
                <h2 className="FormVerification-Title">Изменить логин</h2>
                    <div>
                        <div className='Input-Wrapper'>
                            <Login
                                name='new_login'
                                label={login_label}
                                form_control_server_errors={this.state.controls_server_errors.login}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                value_prop={this.state.form.new_login}
                            />
                        </div>
                            <button
                                className={SubmitButtonClassName}
                                disabled={!form_isValid || form_isLoading}
                                onClick={this.handleSubmit}
                                data-ripple-button=""
                            >
                                <span className="ripple-text">Изменить</span>
                            </button>
                        <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                    </div>
            </div>
        )
    }
}