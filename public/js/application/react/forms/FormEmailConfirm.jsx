import React from 'react';
import Timer from '../Timer';
import ShowError from '../../../application/react/ShowError';
import { customFetch } from "../Helpers";
import URLS from '../constants/urls';
import MESSAGES from "../constants/messages";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../constants/server_errors";
import FormCodeTimeout from "./FormCodeTimeout";

const classNames = require('classnames');

export default class FormEmailConfirm extends FormCodeTimeout {
    constructor(props) {
        super(props);
        this.state = {
            ...this.state,
            code: '',
            csrf: '',
            form_isLoading: false,
            confirm_isLoading: false,
            resend_isLoading: false,
            formError: '',
            form_isValid: false,

            is_code_expired: false,
            form_preloader: false,

            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS
        };

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleResendCode = this.handleResendCode.bind(this);
        this.toggleTimer = this.toggleTimer.bind(this);
        this.bindEmailConfirmationControlsClickHandler = this.bindEmailConfirmationControlsClickHandler.bind(this);
    }

    componentWillMount() {
        const { handleNextStep } = this.props;
        this.formInit();
        if (handleNextStep) {
            this.setState({
                form_isLoading: true,
                resend_isLoading: true
            })
        }
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    formInit() {
        this.toggleIsLoading('resend_isLoading');
        customFetch(URLS.PROFILE)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const {
                        current_email,
                        email_confirmation_token_request_period,
                        token_created_date,
                        is_email_token_expired,
                        csrf
                    } = data.data;

                    // Так как вложенная структура стейта используем rest оператор как аналог immutable библиотеке
                    const form = {
                        ...this.state.form,
                        current_email,
                        csrf
                    };


                    this.setState({
                            form,
                            request_period: email_confirmation_token_request_period,
                            last_code_created_at: token_created_date,
                            is_code_expired: is_email_token_expired
                        },
                        // В этом методе анализируем user_allowed_request_phone_change_code_status и ставим ограничения на отправку если необходимо
                        () => this.checkAllowingToSendCodeTimeOut()
                    );
                    this.toggleIsLoading('resend_isLoading');
                    this.bindEmailConfirmationControlsClickHandler();
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => this.setFormError('form_init_fail', true));
    }

    bindEmailConfirmationControlsClickHandler() {
        const email_resend_confirmation_buttons = document.querySelectorAll('.email-resend-confirmation-buttons');
        email_resend_confirmation_buttons.forEach((elem) => {
            if (elem) {
                elem.addEventListener('click', (e) => {
                    e.preventDefault();
                    if (this.state.is_allow_to_send_code && elem.classList.contains('unconfirmed-profile-field')) {
                        this.handleResendCode();
                    }
                });
            }
        });
    }

    handleChange(e) {
        const { value } = e.target;
        this.setState({
            code: value
        }, () => this.validation());
    }

    validation() {
        const { code } = this.state;
        if (code) {
            this.setState({
                form_isValid: true
            });
        } else {
            this.setState({
                form_isValid: false
            })
        }
    }

    handleSubmit() {
        const { csrf, code, form_isValid } = this.state;

        if (form_isValid) {
            this.toggleIsLoading();
            customFetch(URLS.EMAIL.CONFIRM, {
                method: 'POST',
                body: JSON.stringify({
                    csrf,
                    confirmation_token: code
                })
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        const { handleNextStep, is_reload_on_success } = this.props;

                        handleNextStep && handleNextStep('email_confirm');
                        handleNextStep && this.toggleFormPreloader();
                        this.toggleIsLoading();

                        if (is_reload_on_success) {
                            this.toggleFormPreloader();
                            window.location.reload(false);
                        }

                        this.toggleIsLoading();

                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data);
                        this.toggleIsLoading();
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    handleResendCode() {
        const { csrf } = this.state;
        this.toggleIsLoading('resend_isLoading');
        customFetch(URLS.EMAIL.RESEND, {
            method: 'POST',
            body: JSON.stringify({
                csrf
            })
        })
            .then(data => {
                if (data.status === 'SUCCESS') {
                    this.formInit();
                    this.toggleIsLoading('resend_isLoading');
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading('resend_isLoading');
            });
    }

    toggleIsLoading(name = 'confirm_isLoading') {
        this.setState({
            form_isLoading: !this.state.form_isLoading,
            [name]: !this.state[name]
        });
    }

    toggleFormPreloader() {
        this.setState({
            form_preloader: !this.state.form_preloader
        });
    }

    render() {
        const { form_isLoading, confirm_isLoading, resend_isLoading, is_allow_to_send_code, timer_time, is_code_expired, form_preloader, form_isValid } = this.state;
        const { is_closable } = this.props;

        const ConfirmButtonClassName = classNames({
            'ButtonApply FormVerification-Button': true,
            'Preloader Preloader_light': confirm_isLoading
        });

        const ResendButtonClassName = classNames({
            'ButtonApply FormVerification-Button': true,
            'Preloader Preloader_light': resend_isLoading
        });

        return (
            <div className={"FormLabelTop" + (form_preloader ? ' Preloader Preloader_solid' : '')}>
                {
                    is_closable && <button className="ButtonClose js-verification-close"/>
                }
                <h3 className="FormVerification-Title">
                    Подтверждение эл.почты
                </h3>
                {
                    !is_code_expired ?
                        <div>
                            <p className="text-green">
                                На указанный вами email было отправлено письмо. Следуйте указаниям в письме.
                            </p>
                            {/*<div className="Input-Wrapper">
                                <label htmlFor="code">Введите код подтверждения</label>
                                <input name="code" placeholder="Код подтверждения" id="confirmation_code" type="text" onChange={this.handleChange}/>
                            </div>
                            <button
                                className={ConfirmButtonClassName}
                                disabled={!form_isValid || form_isLoading || confirm_isLoading}
                                onClick={this.handleSubmit}
                            >
                                Подтвердить
                            </button>*/}
                        </div>
                        :
                        <p className="TextImportant no-mt">Код для подтверждения отсутствует. Нажмите получить повторно.</p>
                }
                {
                    is_allow_to_send_code ?
                    <button
                        className={ResendButtonClassName}
                        disabled={form_isLoading || confirm_isLoading}
                        onClick={this.handleResendCode}
                        data-ripple-button=""
                    >
                        Отправить повторно
                    </button>
                    :
                    <p className="mt-20">
                        Вы сможете получить письмо повторно через <span className="TextAccent"> {/* span на этой строке чтобы сохранить пробел*/}
                        <Timer
                            start={timer_time}
                            expired={this.toggleTimer}
                            isShowTime={true}
                        />
                        </span> секунд.
                    </p>
                }
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
            </div>
        )
    }
};