import React from 'react';
import LastName from '../controls/ControlLastName';
import FirstName from '../controls/ControlFirstName';
import SecondaryName from '../controls/ControlSecondaryName';
import BirthDate from '../controls/ControlBirthDate';
import SoleProprietorName from '../controls/ControlSoleProprietorName';
import Inn from '../controls/ControlInn';
import NdsType from '../controls/ControlNdsType';
import { customFetch } from "../Helpers";
import URLS from '../constants/urls';
import ShowError from '../ShowError';
import FormBase from './FormBase';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

const classNames = require('classnames');


export default class FormSoleProprietor extends FormBase {

    constructor(props) {
        super(props);

        this.state = {
            /**
             * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
             * @example
             *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
             **/
            controls_server_errors: {
                csrf: null,
                last_name: null,
                first_name: null,
                secondary_name: null,
                birth_date: null,
                name: null,
                inn: null,
                nds_type: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                last_name_is_valid: false,
                first_name_is_valid: false,
                secondary_name_is_valid: false,
                birth_date_is_valid: false,
                name_is_valid: false,
                inn_is_valid: false,
                nds_type_is_valid: false,
            },
            // ------------------------------
            form: {
                csrf: '',
                name: '',
                last_name: '',
                first_name: '',
                secondary_name: '',
                birth_date: '',
                inn: '',
                nds_type: '',
                civil_law_subject_type: 'sole_proprietor'
            },
            form_isLoading: false,
            form_isDeleting: false,
            form_isSubmitting: false,
            nds_types: [],
            form_isValid: false,
            was_ip_focused: false, // Флаг что был фокус по полю Название ИП
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.form_name = 'form_sole_proprietor'; // Название формы для хранения данных в local_storage
        if (props.editMode) {
            this.form_name = null; // В режиме редактирования не сохраняем данные
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleCloseForm = this.handleCloseForm.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleChangeLastName = this.handleChangeLastName.bind(this);
        this.turnOnIpFocus = this.turnOnIpFocus.bind(this);
    }

    componentWillMount() {
        (this.props.data && this.props.editMode) && this.getDataFromProps();
    }

    componentDidMount() {
        if (!this.props.is_nested) {
            this.getCsrf()
                .then(() => this.rememberState())
        }
        this.getNdsTypes();
        this.getFormFromLocalStorage();
    }

    componentWillReceiveProps(nextProps) {
        const { form_server_errors, clear_local_storage } = nextProps;
        if (clear_local_storage !== this.props.clear_local_storage) {
            this.clearFormLocalStorage();
        }

        form_server_errors && this.SetControlsErrorFromUpperForm(this.props.form_server_errors, form_server_errors);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    getDataFromProps() {
        const { id, name, inn, nds, last_name, first_name, secondary_name, birth_date } = this.props.data;
        this.setState({
            id: id,
            form: {
                ...this.state.form,
                inn,
                nds_type: nds.type,
                last_name,
                first_name,
                secondary_name,
                birth_date,
                name
            }
        }, () => this.rememberState());
    }

    getNdsTypes() {
        customFetch(URLS.LEGAL_ENTITY.FORM_INIT)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    this.setState({
                        nds_types: data.data.handbooks.ndsTypes
                    });
                } else if (data.status === 'ERROR') {
                    return Promise.reject(data.message);
                }
            })
            .catch(error => {
                this.setFormError('form_init_fail', true);
            });
    }

    handleSubmit(e) {
        e.preventDefault();
        const { form_isValid } = this.state;

        if (form_isValid && !this.props.is_nested) {
            this.toggleIsLoading();

            customFetch(this.props.editMode ? `${URLS.SOLE_PROPRIETOR.SINGLE}/${this.state.id}` : URLS.SOLE_PROPRIETOR.SINGLE, {
                method: 'POST',
                body: JSON.stringify(this.state.form)
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.clearFormLocalStorage();
                        const is_after_submit = typeof this.props.afterSubmit === 'function';
                        !is_after_submit && this.toggleIsLoading();
                        is_after_submit && this.props.afterSubmit();
                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data);
                        this.toggleIsLoading();
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    handleDelete() {
        const { id } = this.props.data;
        this.toggleIsLoading('form_isDeleting');
        customFetch(`/sole-proprietor/${id}`, {
            method: 'DELETE'
        })
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const is_after_delete = typeof this.props.afterDelete === 'function';
                    !is_after_delete && this.toggleIsLoading('form_isDeleting');
                    is_after_delete && this.props.afterDelete();
                } else if (data.status === 'ERROR') {
                    this.toggleIsLoading('form_isDeleting');
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading('form_isDeleting');
            });
    }

    handleCloseForm() {
        this.props.handleClose && this.props.handleClose();
        this.props.slideUp && this.props.slideUp();
    }

    handleName(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value,
                name: `ИП ${value}`
            }
        }, () => this.saveFormToLocalStorage());
    }

    handleChangeLastName(name, value) {
        const { was_ip_focused } = this.state;
        const { editMode } = this.props;

        if (was_ip_focused || editMode) {
            this.handleChange(name, value);
        } else {
            this.handleName(name, value);
        }
    }

    turnOnIpFocus() {
        this.setState({
            was_ip_focused: true
        })
    }

    render() {
        const { isClosable, editMode, is_nested } = this.props;
        const { form_isValid, form_isLoading, form_isSubmitting, form_isDeleting, nds_types, was_ip_focused } = this.state;
        const { last_name, first_name, secondary_name, birth_date, name, inn, nds_type } = this.state.form;

        const formTitle = `${editMode ? 'Редактирование' : 'Добавление'} ИП`;
        const buttonText = editMode ? 'Сохранить' : 'Добавить';

        const SubmitButtonClassName = classNames({
            'ButtonApply margin-right': true,
            'Preloader Preloader_light': form_isSubmitting
        });

        const DeleteButtonClassName = classNames({
            'ButtonDelete': true,
            'Preloader Preloader_light': form_isDeleting
        });


        return (
            <div className='FormLabelTop form-default FormLabelTopSoleProprietor' id='form_sole_proprietor'>
                {
                    isClosable &&
                    <div className="ButtonClose" onClick={this.handleCloseForm} />
                }
                <div className="row nested-row">
                    <div className="col-xl-12 col-sm-4">
                        <h3 className="TitleBlue">{formTitle}</h3>
                    </div>
                </div>
                <div className="row nested-row">
                    <div className="col-xl-6 col-sm-4">
                        <LastName
                            label="Фамилия:"
                            placeholder="Введите фамилию"
                            name="last_name"
                            value_prop={last_name}
                            handleComponentChange={this.handleChangeLastName}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.last_name}
                        />
                    </div>
                    <div className="col-xl-6 col-sm-4">
                        <FirstName
                            label="Имя:"
                            placeholder="Введите имя"
                            name="first_name"
                            value_prop={first_name}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.first_name}
                        />
                    </div>
                </div>
                <div className="row nested-row">
                    <div className="col-xl-6 col-sm-4">
                        <SecondaryName
                            label="Отчество:"
                            name="secondary_name"
                            placeholder="Введите отчество"
                            value_prop={secondary_name}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.secondary_name}
                        />
                    </div>
                    <div className="col-xl-6 col-sm-4">
                        <BirthDate
                            label="Дата рождения:"
                            name="birth_date"
                            placeholder="дд.мм.гггг"
                            handleComponentChange={this.handleChange}
                            value_prop={birth_date}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.birth_date}
                        />
                    </div>
                </div>
                <div className="row nested-row">
                    <div className="col-xl-6 col-sm-4">
                        <SoleProprietorName
                            label="Название ИП:"
                            placeholder="Введите название ИП"
                            name="name"
                            value_prop={name}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.name}
                            toggleFocus={this.turnOnIpFocus}
                            was_focused={was_ip_focused}
                        />
                    </div>
                    <div className="col-xl-6 col-sm-4">
                        <Inn
                            label="ИНН:"
                            placeholder="Введите ИНН"
                            name="inn"
                            value_prop={inn}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.inn}
                        />
                    </div>
                </div>
                <div className="row nested-row">
                    <div className="col-xl-6 col-sm-4">
                        <NdsType
                            label="НДС:"
                            placeholder="Выберите тип НДС"
                            name="nds_type"
                            value_prop={nds_type}
                            nds_types={nds_types}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.nds_type}
                        />
                    </div>
                </div>
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                {
                    !is_nested &&
                        <div className="ButtonsRow row nested-row">
                            <div className="col-xl-6 col-sm-4">
                                <button onClick={this.handleSubmit} className={SubmitButtonClassName} disabled={!form_isValid || form_isLoading} data-ripple-button=""><span className="ripple-text">{buttonText}</span></button>
                            </div>
                            <div className="col-xl-6 col-sm-4">
                                {
                                    editMode &&
                                    <button onClick={this.handleDelete} className={DeleteButtonClassName} disabled={form_isLoading} data-ripple-button="">
                                        <span className="ripple-text">Удалить</span>
                                    </button>
                                }
                            </div>
                        </div>
                }
            </div>
        );
    }
};
