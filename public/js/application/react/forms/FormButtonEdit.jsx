import React from 'react';
import FormButtonCreate from './FormButtonCreate';
import FormWrapper from '../components/FormDealWrapper';
import ButtonAuthorRole from '../controls/ControlButtonAuthorRole';

import URLS from '../constants/urls';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import { customFetch, getIdFromUrl, getDateNowInPrettyFormat } from '../Helpers';

export default class FormButtonEdit extends FormButtonCreate {
    constructor(props) {
        super(props);

        this.state = {
            ...this.state,
            button_id: '',
            form: {
                ...this.state.form,
                created: '',
            },
            // Validation props
            validation: {
                deal_type_is_valid: true,
                deal_role_is_valid: true,
                name_is_valid: true,
                button_author_role_is_valid: true,
                deal_description_is_valid: true,
                amount_is_valid: true,
                fee_payer_option_is_valid: true,
                delivery_period_is_valid: true,
                counteragent_civil_law_subject_is_valid: true,
                counteragent_payment_method_is_valid: true,
                button_create_confirm_is_valid: true,
                civil_law_subject_form_is_valid: true,
                payment_method_form_is_valid: true,
                email_inn_company_name_referer_is_valid: true
            }
        };

        this.form_name = null; // название формы для хранения данных в local_storage
    }

    componentWillMount() {
        const formHTML = document.getElementById('form-button-edit');
        if (formHTML) {
            formHTML.style.visibility = 'visible';
        }
        this.formInit();
    }

    componentDidMount() {
        if (!this.props.is_nested) {
            this.getCsrf()
                .then(() => this.rememberState());
        }
    }

    getFormInitUrl() {
        const button_id = getIdFromUrl();
        // Проверяем если у кнопки в state ее id, если есть то делаем запрос с учетом id кнопки.
        return `${URLS.BUTTON.FORM_INIT}`;
    }

    successAjaxFormInit(data) {
        const button_id = getIdFromUrl();
        const { deal_types = {}, fee_payer_options = {}, naturalPersons, legalEntities, soleProprietors, max_deal_amount, allowedTokens, beneficiar_user } = data.data;
        const { update_collection_civil_law_subjects } = this.state;

        let data_to_state = {
            button_id,
            deal_types,
            fee_payer_options,
            max_deal_amount,
            natural_persons: naturalPersons || [],
            legal_entities: legalEntities || [],
            sole_proprietors: soleProprietors || [],
            update_collection_civil_law_subjects: update_collection_civil_law_subjects + 1,
        };

        //         // Данные о текущей кнопке
        if (allowedTokens) {
            console.log(allowedTokens);

            //Если необязательные поля заполнены при создании кнопки, они должны быть активны
            const filledTokenFields = {};
            for(let key in allowedTokens[button_id]) {
                if (allowedTokens[button_id][key]){
                    filledTokenFields[key] = false;
                }
            }
            console.log('filledTokenFields---', filledTokenFields);
            data_to_state = {
                ...data_to_state,
                form: {
                    id: allowedTokens[button_id].id,
                    name: allowedTokens[button_id].name,
                    amount: allowedTokens[button_id].amount ? allowedTokens[button_id].amount : 0,
                    counteragent_civil_law_subject: allowedTokens[button_id].counteragent_civil_law_subject ? allowedTokens[button_id].counteragent_civil_law_subject.id : null,
                    counteragent_payment_method: allowedTokens[button_id].counteragent_payment_method ? allowedTokens[button_id].counteragent_payment_method['bank_transfer']['id'] : null,
                    beneficiar_civil_law_subject: allowedTokens[button_id].beneficiar_civil_law_subject ? allowedTokens[button_id].beneficiar_civil_law_subject.id : null,
                    beneficiar_payment_method: allowedTokens[button_id].beneficiar_payment_method ? allowedTokens[button_id].beneficiar_payment_method['bank_transfer']['id'] : null,
                    deal_role: allowedTokens[button_id].deal_role != null ? (allowedTokens[button_id].deal_role.toLowerCase() === 'customer' ? 'contractor' : 'customer')  : 'customer',
                    deal_description: allowedTokens[button_id].deal_description ? allowedTokens[button_id].deal_description : '',
                    counteragent_email: allowedTokens[button_id].counteragent_email,
                    created: allowedTokens[button_id].created,
                    deal_name: allowedTokens[button_id].deal_name,
                    deal_type: allowedTokens[button_id].deal_type === 'T' ? '2' : '1',
                    delivery_period: allowedTokens[button_id].delivery_period,
                    fee_payer_option: allowedTokens[button_id].fee_payer_option ? allowedTokens[button_id].fee_payer_option : '3',
                    is_active: allowedTokens[button_id].is_active,
                    beneficiar_user: allowedTokens[button_id].beneficiar_user,
                    button_author_role: allowedTokens[button_id].beneficiar_user ? 'referer' : 'participant',
                },
                disabled_controls: {
                    ...this.state.disabled_controls,
                    ...filledTokenFields,
                    //fee_payer_option: !allowedTokens[button_id].fee_payer_option
                    deal_role: allowedTokens[button_id].deal_role == null ,
                    button_author_role: true,
                }
            };
        }

        this.setState(data_to_state, () => {
            this.getFormFromLocalStorage();
            this.toggleIsLoading('form_isPreloader');
        });
    }
    checkAllControlsAreValid() {
        const {
            deal_type_is_valid,
            deal_role_is_valid,
            button_author_role_is_valid,
            deal_name_is_valid,
            name_is_valid,
            deal_description_is_valid,
            amount_is_valid,
            fee_payer_option_is_valid,
            delivery_period_is_valid,
            counteragent_civil_law_subject_is_valid,
            counteragent_payment_method_is_valid,
            civil_law_subject_form_is_valid,
            email_inn_company_name_referer_is_valid,
            payment_method_form_is_valid,
        } = this.state.validation;

        if (deal_type_is_valid
            && deal_role_is_valid
            && name_is_valid
            && (this.state.disabled_controls.deal_name || deal_name_is_valid)
            && deal_description_is_valid
            && (this.state.disabled_controls.amount || amount_is_valid)
            && button_author_role_is_valid
            && fee_payer_option_is_valid
            && delivery_period_is_valid
            && (counteragent_civil_law_subject_is_valid || civil_law_subject_form_is_valid)
        ) {
            this.setState({ form_isValid: true });
        } else {
            this.setState({ form_isValid: false });
        }
    }
    getButtonsControl(props) {
        const { form_isValid, form_isSubmitting } = this.state;
        return (
            <div className="ButtonsRow row">
                <div className="col-xl-6 col-sm-2 col-xs-4">
                    <button
                        onClick={this.handleSubmit}
                        className={`ButtonApply ${form_isSubmitting ? 'Preloader Preloader_light' : ''}`}
                        disabled={!form_isValid || form_isSubmitting}
                        data-ripple-button=""
                    >
                        <span className="ripple-text">Изменить</span>
                    </button>
                </div>
                <div className="col-xl-6 col-sm-2 col-xs-4">
                    <button onClick={this.handleCancel} className="ButtonRefuse" data-ripple-button=""><span className="ripple-text">Отмена</span></button>
                </div>
            </div>
        );
    }

    getButtonAuthorRoleControl(props) {
        const { beneficiar_user } = this.state.form;

        const button_author_role = beneficiar_user ? 'referer' : 'participant';

        return (
            <ButtonAuthorRole
                label="Вы будете размещать кнопку как:"
                name="button_author_role"
                value_prop={button_author_role}
                deal_type={this.getCurrentDealTypeIdent()}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.button_author_role}
                is_disable={this.state.disabled_controls.button_author_role}
                {...props}
            />
        );
    }



    getSubmitUrl() {
        const { form: { id } } = this.state;
        return `/profile/partner-button/${id}`;
    }

    render() {
        const { form_isPreloader} = this.state;
        const {id, created, beneficiar_user, deal_role, role} = this.state.form;
        const button_author_role = beneficiar_user ? 'referer' : 'participant';

        const role_handled = role === 'Реферер' ? 'реферер' : 'участник сделки';

        let controls = this.getControls();
        controls.button_author_role_control = (props) => this.getButtonAuthorRoleControl(props);

        return(
            <FormWrapper is_loading={form_isPreloader}
                         date_of_create={created}
                         logo={controls.form_logo}
                         mobile_title={`Редактирование кнопки-приглашения № ${id}`}
                         title={`Редактирование кнопки-приглашения № ${id}`}>

                { controls.name_control() }
                { controls.button_author_role_control() }
                {
                    button_author_role === 'referer' ?
                        controls.civil_law_subject_and_payment_method_control()
                        :
                        void(0)
                }
                {
                    button_author_role === 'referer' ?
                        controls.counteragent_email_control()
                        :
                        void(0)
                }
                {
                    button_author_role === 'participant' ?
                        controls.deal_type_control()
                        :
                        void(0)
                }
                {
                    button_author_role === 'participant' ?
                        controls.deal_role_control()
                        :
                        void(0)
                }
                {
                    button_author_role === 'participant' ?
                        (
                            deal_role === 'contractor' ?
                                controls.civil_law_subject_and_payment_method_control()
                                :
                                controls.civil_law_subject_control()
                        )
                        :
                        void(0)
                }
                { controls.string_label_control() }
                {
                    button_author_role === 'referer' ?
                        controls.deal_type_enabable_control()
                        :
                        void(0)
                }
                {
                    button_author_role === 'referer' ?
                        controls.invited_user_role_enabable_control()
                        :
                        void(0)
                }
                { controls.deal_name_control() }
                { controls.deal_description_control() }
                <div className="row">
                    <div className="col-xl-6 col-sm-4">
                        { controls.amount_control() }
                    </div>
                    <div className="col-xl-6 col-sm-4">
                        { controls.delivery_period_control() }
                    </div>
                </div>
                { controls.fee_payer_options_control() }
                { controls.form_errors_control }
                { controls.buttons_control() }
            </FormWrapper>
        )
    }

}