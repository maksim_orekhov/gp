import React from 'react';
import {
    customFetch, getDpdPointAddress, getIdFromUrl, getPickPointNameByDealRole,
    getPickPointShedule
} from '../Helpers';
import DatePicker from './../controls/ControlDatePicker';
import FormDeal from './formsDeal/FormDeal';
import URLS from '../constants/urls';

export default class FormPickupDate extends FormDeal {

    constructor(props) {
        super(props);

        this.state = {
            // -----------------------------
            form: {
                csrf: '',
                date_pickup: ''
            },
            validation: {
                date_pickup_is_valid: false
            },
            disabledDays: null,
            schedule: null,
            point_address: '',
            point_name: '',
            point_street: '',
            point_streetAbr: '',
            point_house: '',
            point_structure: '',
            form_isValid: false,
            form_isPreloader: true // Важно, что true
        };

        this.form_name = null;

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentDidMount() {
        document.getElementById("form-pickup-date").style.visibility = 'visible';
    }

    getFormInitUrl() {
        const deal_id = getIdFromUrl();
        // Проверяем если у сделки в state ее id, если есть то делаем запрос с учетом id сделки.
        return `${URLS.DEAL.FORM_INIT}?idDeal=${deal_id}`;
    }

    successAjaxFormInit(data) {
        console.log('formInit', data);
        const {
            deal,
            csrf,
        } = data.data;

        const form = {...this.state.form};

        form['csrf'] = csrf;

        let data_to_state = {
            form: {
                ...form,
            }
        };

        if (deal) {
            if (deal.hasOwnProperty('delivery') && deal.delivery && typeof deal.delivery === 'object') {
                const delivery = deal.delivery;

                if (delivery.hasOwnProperty('service') && delivery.service && typeof delivery.service === 'object') {
                    const service = delivery.service;

                    if (service.hasOwnProperty('sender_address') && service.sender_address && typeof service.sender_address === 'object') {
                        const sender_address = service.sender_address;

                        if (sender_address.hasOwnProperty('terminal') && sender_address.terminal && typeof sender_address.terminal === 'object') {
                            const terminal = sender_address.terminal;

                            if (terminal.hasOwnProperty('times') && terminal.times && typeof terminal.times === 'object') {
                                const schedule = terminal.times;
                                data_to_state['schedule'] = schedule;
                                data_to_state['disabledDays'] = this.getDisabledDays(schedule);
                            }
                            if (terminal.hasOwnProperty('name') && terminal.name) {
                                data_to_state['point_name'] = terminal.name;
                            }
                            if (terminal.hasOwnProperty('street') && terminal.street) {
                                data_to_state['point_street'] = terminal.street;
                            }
                            if (terminal.hasOwnProperty('house') && terminal.house) {
                                data_to_state['point_house'] = terminal.house;
                            }
                            if (terminal.hasOwnProperty('street_abr') && terminal.street_abr) {
                                data_to_state['point_streetAbr'] = terminal.street_abr;
                            }
                            if (terminal.hasOwnProperty('structure') && terminal.structure) {
                                data_to_state['point_structure'] = terminal.structure;
                            }
                        }
                    }
                }
            }
        }

        this.setState(data_to_state, () => {
            this.toggleIsLoading('form_isPreloader');
        });
    }

    getWeekDayIndex(week_day_string) {
        const indexes = {
            'пн': 0,
            'вт': 1,
            'ср': 2,
            'чт': 3,
            'пт': 4,
            'сб': 5,
            'вс': 6
        };

        if (indexes[week_day_string.toLowerCase()]) {
            return indexes[week_day_string.toLowerCase()];
        }
    }

    getDisabledDays(schedule) {
        if (!schedule || !Array.isArray(schedule) || !schedule.length) return null;

        const disabledDays = [];
        schedule.forEach((day) => {
            if (!day.hasOwnProperty('work_time')) return;

            if (day['work_time'].toLowerCase() === 'выходной') {
                disabledDays.push(this.getWeekDayIndex(day['week_day']));
            }
        });

        return disabledDays;
    }

    handleChange(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                date_pickup: value
            }
        }, () => {
            this.saveFormToLocalStorage();
            this.checkAllControlsAreValid();
        });
    }

    checkAllControlsAreValid() {
        const { date_pickup } = this.state.form;

        this.setState({ form_isValid: date_pickup && date_pickup.length > 0 });
    }

    getSubmitUrl() {
        const idDeal = getIdFromUrl();
        return `/deal/${idDeal}/create-dpd-order`;
    }

    handleSubmit() {
        if (this.state.form_isValid) {
            this.toggleIsLoading();

            customFetch(this.getSubmitUrl(), {
                method: 'POST',
                body: this.getDataFromState(),
                processData: false,
                contentType: false
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        window.location.reload();
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(response => {
                    console.log(response);
                    this.toggleIsLoading();
                    this.setFormError('critical_error', true);
                })
        }
    }

    getDataFromState() {
        const data = {
            csrf: this.state.form.csrf,
            date_pickup: this.state.form.date_pickup
        };

        return this.createFormData(data);
    }

    render() {
        const {
            form_isSubmitting,
            form_isValid,
            disabledDays,
            point_name,
            schedule,
            point_street,
            point_streetAbr,
            point_house,
            point_structure,
            form_isPreloader
        } = this.state;

        return (
            <div className={form_isPreloader ? 'Preloader Preloader_solid' : 'FormLabelTop'}>
                <div className="col-xl-6 col-lt-6 date-picker-col">
                    <div className="Input-Wrapper">
                        <div className="delivery-point">
                            <div className="delivery-point__head">
                                <label>Пункт приема:</label>
                            </div>
                            <div className="search__item current delivery-point__body">
                                <h3 className="delivery-point__name">{point_name}</h3>
                                {
                                    point_street &&
                                    <div className="delivery-point__address">
                                        {
                                            getDpdPointAddress({
                                                street: point_street,
                                                street_abr: point_streetAbr,
                                                house: point_house,
                                                structure: point_structure
                                            })
                                        }
                                    </div>
                                }
                                {
                                    schedule && schedule.length !== 0 &&
                                    <div className="delivery-point__schedule">
                                        {
                                            schedule.map((day, index) => {
                                                return <p key={index}>{day['week_day']}: {day['work_time']}</p>
                                            })
                                        }
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                    <DatePicker
                        name="date_pickup"
                        label="Дата забора:"
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        disabledDays={disabledDays}
                    />
                    <div className="ButtonsRow">
                        <button
                            onClick={this.handleSubmit}
                            className={`ButtonApply ${form_isSubmitting ? 'Preloader Preloader_light' : ''}`}
                            disabled={!form_isValid || form_isSubmitting}
                            data-ripple-button=""
                        >
                            <span className="ripple-text">Подтвердить</span>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
};