import React from 'react';
import FormBase from "./FormBase";

export default class FormCodeTimeout extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            request_period: null,
            last_code_created_at: null,
            timer_time: null,
            is_allow_to_send_code: true
        };
    }

    toggleTimer(isTimer) {
        this.setState({
            is_allow_to_send_code: isTimer
        });
    }

    checkAllowingToSendCodeTimeOut() {
        const { request_period, last_code_created_at } = this.state;
        const now = Date.now() / 1000; // Время сейчас в секундах
        const timer_time = last_code_created_at + request_period - now;

        if (timer_time > 0) {
            this.setState({
                is_allow_to_send_code: false,
                timer_time: Math.round(timer_time)
            });
        }
    }
};