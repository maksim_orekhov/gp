import React from 'react';
import FormBase from './FormBase';
import ShowError from '../ShowError';
import { customFetch, addSpacesBetweenThousands } from "../Helpers";
import URLS from '../constants/urls';
import DEAL_ROLES from '../constants/deal_roles';
import DEAL_TYPES from '../constants/dealTypes';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import SelectImitation from "../controls/ControlSelectImitation";

const classNames = require('classnames');

export default class FormButton extends FormBase {

    constructor(props) {
        super(props);

        let defaultStyles = {
            button: 0,
            text: 0,
        };
        const buttonStyles = window.localStorage.getItem('buttonStyles');
        if (buttonStyles && JSON.parse(buttonStyles)[props.data.id]) {
            defaultStyles = JSON.parse(buttonStyles)[props.data.id];
        }

        this.state = {
            /**
             * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
             * @example
             *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
             **/
            controls_server_errors: {
                csrf: null,
                last_name: null,
                first_name: null,
                secondary_name: null,
                birth_date: null
            },
            colors: {
                button: [
                    {
                        id: 0,
                        name: 'Синий',
                        value: 'linear-gradient(#2F80ED, #0066B2)',
                    },
                    {
                        id: 1,
                        name: 'Фиолетовый',
                        value: 'linear-gradient(#a044ff, #6a3093)'
                    }
                ],
                text: [
                    {
                        id: 0,
                        name: 'Белый',
                        value: '#FFFFFF'
                    },
                    {
                        id: 1,
                        name: 'Чёрный',
                        value: '#000000'
                    }
                ]
            },
            current_colors: defaultStyles,
            styles: {
                display: 'flex',
                align_items: 'center',
                justify_content: 'center',
                max_width: '288px',
                height: '60px',
                text_align: 'center',
                text_decoration: 'none',
                font_family: 'Verdana',
                font_weight: '400',
                font_size: '18px',
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                last_name_is_valid: false,
                first_name_is_valid: false,
                secondary_name_is_valid: false,
                birth_date_is_valid: false
            },
            is_settings_opened: false,
            id: '',
            name: '',
            created: '',
            role: '',
            deals: [],
            amount: '',
            deal_type: '',
            name_counteragent_civil_law_subject: '',
            bank_name_counteragent_payment_method: '',
            fee_payer_options: [],
            // --------------------------------
            form: {
                csrf: '',
                last_name: '',
                first_name: '',
                secondary_name: '',
                birth_date: '',
                civil_law_subject_type: 'natural_person'
            },
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false,
            form_isDeleting: false,
            successCopyMessageIsVisible: false,
        };

        this.prev_state = null; // Первоначальное состояние формы для блокировки кнопки сохранить в режиме изменения, если данные не изменились от первоначальных
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.form_name = 'form_natural_person'; // Название формы для хранения данных в local_storage
        if (props.editMode) {
            this.form_name = null; // В режиме редактирования не сохраняем данные
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleCloseForm = this.handleCloseForm.bind(this);
        this.toggleButtonSettings = this.toggleButtonSettings.bind(this);
        this.setElementColor = this.setElementColor.bind(this);
    }

    componentWillMount() {
        this.getFormFromLocalStorage();
        (this.props.data && this.props.editMode) && this.getDataFromProps();
    }

    componentDidMount() {
        if (!this.props.is_nested) {
            this.getCsrf()
                .then(() => this.rememberState());
        }
        document.addEventListener('click', this.handleClickOutsideSettings);

    }

    componentWillReceiveProps(nextProps) {
        const { form_server_errors, clear_local_storage } = nextProps;

        if (clear_local_storage !== this.props.clear_local_storage) {
            this.clearFormLocalStorage();
        }

        form_server_errors && this.SetControlsErrorFromUpperForm(this.props.form_server_errors, form_server_errors);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    componentWillUnMount() {
        document.removeEventListener('click', this.handleClickOutsideSettings);
    }

    getDataFromProps() {
        const { id, name, created, beneficiar_user, deals, amount, deal_type, counteragent_civil_law_subject, counteragent_payment_method, token_key, fee_payer_options, fee_payer_option, beneficiar_civil_law_subject, beneficiar_payment_method } = this.props.data;
        console.log('getDataFromProps', this.props.data);

        const name_counteragent_civil_law_subject = counteragent_civil_law_subject ? counteragent_civil_law_subject.name : beneficiar_civil_law_subject.name;

        const bank_name_counteragent_payment_method = counteragent_payment_method
            ? counteragent_payment_method.bank_transfer.name
            : beneficiar_payment_method ? beneficiar_payment_method.bank_transfer.name : '';

        this.setState({
            id,
            name,
            created,
            beneficiar_user,
            deals,
            amount,
            deal_type,
            name_counteragent_civil_law_subject,
            bank_name_counteragent_payment_method,
            token_key,
            fee_payer_options,
            fee_payer_option,
        }, () => this.rememberState());
    }

    handleSubmit(e) {
        e.preventDefault();
        const { form_isValid } = this.state;

        if (form_isValid && !this.props.is_nested) {
            this.toggleIsLoading();
            customFetch(this.props.editMode ? `${URLS.NATURAL_PERSON.SINGLE}/${this.state.id}` : URLS.NATURAL_PERSON.SINGLE, {
                method: 'POST',
                body: JSON.stringify(this.state.form)
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.clearFormLocalStorage();

                        const is_after_submit = typeof this.props.afterSubmit === 'function';
                        !is_after_submit && this.toggleIsLoading();
                        is_after_submit && this.props.afterSubmit();
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                })
        }
    }

    handleDelete() {
        const { id } = this.props.data;
        this.toggleIsLoading('form_isDeleting');
        customFetch(`/partner-button/${id}`, {
            method: 'DELETE'
        })
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const is_after_delete = typeof this.props.afterDelete === 'function';
                    !is_after_delete && this.toggleIsLoading('form_isDeleting');
                    is_after_delete && this.props.afterDelete();
                } else if (data.status === 'ERROR') {
                    this.toggleIsLoading('form_isDeleting');
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading('form_isDeleting');
            });
    }

    handleEdit() {
        const { id } = this.props.data;
        window.location = `${URLS.BUTTON.EDIT+'/'+id+'/edit'}`;
    }

    handleCloseForm() {
        this.props.handleClose && this.props.handleClose();
        this.props.slideUp && this.props.slideUp();
    }

    copyCodeToClipboard = () => {
        if (window.getSelection) {
            const selection = window.getSelection();
            const selection_range = document.createRange();

            selection_range.selectNode(document.getElementById('button-src'));
            selection.removeAllRanges();
            selection.addRange(selection_range);
            document.execCommand("copy");
            window.getSelection().empty();
            this.showSuccessCopyMessage();
        }
    };

    showSuccessCopyMessage = () => {
        clearTimeout(this.copyMessageTimeoutId);
        this.setState({ successCopyMessageIsVisible: true }, () => {
            this.copyMessageTimeoutId = setTimeout(() => {
                this.setState({ successCopyMessageIsVisible: false });
            }, 2000);
        })
    };

    toggleButtonSettings() {
        this.setState({
            is_settings_opened: !this.state.is_settings_opened
        });
    }

    handleClickOutsideSettings = (e) => {
        if (this.state.is_settings_opened && this.settings_ref && !this.settings_ref.contains(e.target)) {
            this.setState({
                is_settings_opened: false
            });
        }
    };


    setElementColor(e) {
        const name = e.target.attributes.name.value;
        const value = e.target.dataset.value;

        this.setState({
            current_colors: {
                ...this.state.current_colors,
                [name]: value
            }
        }, () => {
            this.saveStyleOptions();
        });
    }

    saveStyleOptions = () => {
        const { current_colors } = this.state;
        const { data: { id } } = this.props;
        const savedColors = window.localStorage.getItem('buttonStyles') ? JSON.parse(window.localStorage.getItem('buttonStyles')) : {};

        savedColors[id] = current_colors;
        window.localStorage.setItem('buttonStyles', JSON.stringify(savedColors));
    };

    getColorOptions(element_name) {
        const { colors = {} } = this.state;

        return Object.values(colors[element_name]).map(colors_item => {
            return (
                <li
                    key={colors_item.id}
                    name={element_name}
                    data-value={colors_item.id}
                    onClick={this.setElementColor}
                >
                    {<div className="current-button-color" style={{background: colors_item.value}} />}
                </li>
            );
        }).reverse();
    }

    getActiveTab(select_name) {
        const { current_colors, colors } = this.state;
        const value = current_colors[select_name];
        if (colors[select_name] && colors[select_name][value]) {
            return colors[select_name][value].name
        }
        return ''
    }

    render() {
        const { isClosable, editMode, is_nested, fee_payer_options, changeActivityHandler, data: { id, is_active, deals_count, deals_sum } } = this.props;
        const { form_isValid, form_isLoading, form_isSubmitting, form_isDeleting, is_settings_opened, successCopyMessageIsVisible,
            name, beneficiar_user, amount, deals, deal_type, name_counteragent_civil_law_subject, bank_name_counteragent_payment_method, token_key, fee_payer_option } = this.state;

        const amount_numeric = amount === null ? 0 : amount;

        const button_author_role = beneficiar_user !== null ? 'реферер' : 'участник сделки';

        const buttonText = 'Изменить данные';

        const SubmitButtonClassName = classNames({
            'ButtonApply margin-right': true,
            'Preloader Preloader_light': form_isSubmitting
        });

        const DeleteButtonClassName = classNames({
            'ButtonDelete': true,
            'ButtonDeleteButton': true,
            'Preloader Preloader_light': form_isDeleting
        });

        const current_button_bg = this.state.colors['button'][this.state.current_colors['button']].value;
        const current_button_bg_class_name = this.state.colors['button'][this.state.current_colors['button']].name;
        const current_button_bg_class_value = this.state.colors['button'][this.state.current_colors['button']].value;
        const current_button_text_color = this.state.colors['text'][this.state.current_colors['text']].value;

        //Дефолтные параметры стилей для кнопки
        const default_button_display = this.state.styles['display'];
        const default_button_align_items = this.state.styles['align_items'];
        const default_button_justify_content = this.state.styles['justify_content'];
        const default_button_width = this.state.styles['max_width'];
        const default_button_height = this.state.styles['height'];
        const default_button_text_align = this.state.styles['text_align'];
        const default_button_text_decoration = this.state.styles['text_decoration'];
        const default_button_font_family = this.state.styles['font_family'];
        const default_button_font_weight = this.state.styles['font_weight'];
        const default_button_font_size = this.state.styles['font_size'];


        let feePayerName = '';

        if (fee_payer_option && deal_type) {
            feePayerName = fee_payer_options.find((option) =>  option.id === fee_payer_option).name.toLowerCase();
            const role = DEAL_ROLES[deal_type][feePayerName];
            if (role) {
                feePayerName = role['ip'];
            }
        }
        let dealTypeName = ''
        Object.keys(DEAL_TYPES).map(key => {
            if(DEAL_TYPES[key]['ident'] == deal_type){
                dealTypeName = DEAL_TYPES[key]['name'];
                return true
            }
            return false
        });


        return (
            <div id="button-details-form" className="PageForm-Container">
                <div className="FormLabelTop form-default" id='form_button'>
                    {
                        isClosable &&
                        <div className="ButtonClose" onClick={this.handleCloseForm} />
                    }
                    <div className="form-frame">
                        <h1 className="button-title">Кнопка «{name}»</h1>
                        <div className="popup-row button-info">
                            <div className="button-view button-column" style={{
                                background: current_button_bg,
                                color: current_button_text_color
                            }}>
                                <div className="icon-shield-gp" />
                                БЕЗОПАСНАЯ СДЕЛКА
                            </div>
                            <div className="button-statistics button-column">
                                <div>
                                    <b>Проведено сделок: </b> {deals_count || 0}
                                </div>
                                <div>
                                    <b>На сумму: </b>  {deals_sum ? addSpacesBetweenThousands(deals_sum) : 0} руб.
                                </div>
                            </div>
                        </div>
                        <div className="popup-row">
                            <div className="button-settings-block button-column">
                                Так будет выглядеть кнопка на сайте
                                <div className="button-settings-icon" ref={(element) => this.settings_ref = element }>
                                    <div id="icon-settings-gear" className="icon-settings-gear" onClick={this.toggleButtonSettings} />
                                    {
                                        is_settings_opened &&
                                        <div id="button-settings" className="button_settings">
                                            <div className="button-color">
                                                <p>Цвет кнопки</p>
                                                <SelectImitation
                                                    id="button_color_select"
                                                    default_option="Выберите цвет"
                                                    active_tab={this.getActiveTab('button')}
                                                    options={this.getColorOptions('button')}
                                                    is_color_picker={true}
                                                    color_block={<div className="current-button-color" style={{background: current_button_bg}} />}
                                                />
                                            </div>
                                            <div className="text-color">
                                                <p>Цвет текста</p>
                                                <SelectImitation
                                                    id="text_color_select"
                                                    default_option="Выберите цвет"
                                                    active_tab={this.getActiveTab('text')}
                                                    options={this.getColorOptions('text')}
                                                    is_color_picker={true}
                                                    color_block={<div className="current-button-color" style={{background: current_button_text_color}} />}
                                                />
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                            <div className="button-column" />
                        </div>
                        <div className="popup-row copy-link-container">
                            Установите кнопку прямо сейчас к себе на сайт, просто {
                            <span className="copy-link-wrapper">
                                <span className="copy-link" onClick={this.copyCodeToClipboard}>
                                    скопировав
                                </span>
                                {successCopyMessageIsVisible && <span className="copy-success">Скопировано в буфер обмена.</span>}
                            </span>
                            } готовый код.
                        </div>
                        <div className="popup-row button-code">
                            <div className="string-numbers">
                                <ul>
                                    <li>1</li>
                                    <li>2</li>
                                    <li>3</li>
                                    <li>4</li>
                                    <li>5</li>
                                    <li>6</li>
                                    <li>7</li>
                                    <li>8</li>
                                    <li>9</li>
                                    <li>10</li>
                                    <li>11</li>
                                    <li>12</li>
                                    <li>13</li>
                                    <li>14</li>
                                    <li>15</li>
                                    <li>16</li>
                                    <li>17</li>
                                    <li>18</li>
                                    <li>19</li>
                                    <li>20</li>
                                    <li>21</li>
                                </ul>
                            </div>
                            <div id="button-src" className="code">
                                <code>
                                    <p>{"<a style="}
                                       {`"background: ${current_button_bg_class_value}; `}
                                       {`color: ${current_button_text_color}; `}
                                       {`display: ${default_button_display}; `}
                                       {`align-items: ${default_button_align_items}; `}
                                       {`justify-content: ${default_button_justify_content}; `}
                                       {`max-width: ${default_button_width}; `}
                                       {`height: ${default_button_height}; `}
                                       {`text-align: ${default_button_text_align}; `}
                                       {`text-decoration: ${default_button_text_decoration}; `}
                                       {`font-family: ${default_button_font_family}; `}
                                       {`font-weight: ${default_button_font_weight}; `}
                                       {`font-size: ${default_button_font_size};"`}
                                       {`target="_blank" `}
                                       {`href="https://guarantpay.com/deal/create/partner-form/${token_key}">`}
                                    </p>
                                    <p>
                                        {`<img src="//${window.location.hostname}/img/icon-shield-gp.svg" alt="guarantpay">`}<br/>
                                        {`<span style="margin-left: 9px;">БЕЗОПАСНАЯ СДЕЛКА</span>`}
                                    </p>
                                    <p></p>
                                    <p>{"</a>\n"}</p>
                                </code>
                            </div>
                        </div>
                        <div className="grey-field button-active">
                            {is_active
                                ? `Кнопка активна. Для временной деактивации сдвиньте переключатель влево.`
                                : `Кнопка неактивна. Для активации сдвиньте переключатель вправо.`
                            }
                            <label className="toggled-checkbox-field toggled-checkbox-field__type_garant-pay">
                                <input type="checkbox" onChange={changeActivityHandler(id)} checked={is_active}/>
                                    <div className="toggled-checkbox-field__track">
                                        <div className="toggled-checkbox-field__button" />
                                    </div>
                            </label>
                        </div>
                        <div className="popup-row button-data">
                            <h2>Код кнопки содержит следующие данные:</h2>
                            <div>
                                <b> URL - адрес кнопки:</b> {token_key}
                            </div>
                            <div>
                                <b>Вы будете размещать кнопку как: </b> {button_author_role}
                            </div>

                            { beneficiar_user == null && deal_type &&
                                <div>
                                    <b>Тип сделки: </b> {dealTypeName.toLowerCase()}
                                </div>
                            }

                            {
                                beneficiar_user == null &&
                                <div>
                                    <b>Ваша роль в сделке: </b> продавец
                                </div>
                            }
                            {
                                <div>
                                    { beneficiar_user == null ? <b>Вы участвуете в сделке как: </b> : <b>Владелец кнопки: </b> } {name_counteragent_civil_law_subject}
                                </div>
                            }


                            { fee_payer_option &&
                                <div>
                                    <b>Кто платит комиссию: </b> { feePayerName }
                                </div>
                            }
                            {
                                bank_name_counteragent_payment_method &&
                                <div>
                                    <b>{beneficiar_user == null ? 'Способ получения оплаты:' : 'Способ получения комиссионных:' } </b> {bank_name_counteragent_payment_method}
                                </div>
                            }

                        </div>
                    <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                    {
                        !is_nested &&
                        <div className="ButtonsRow row nested-row">
                            <div className="col-xl-6 col-sm-4">
                                <button onClick={this.handleEdit} className="ButtonEdit" data-ripple-button="">
                                <span className="ripple-text">{buttonText}</span>
                            </button>
                            </div>
                            <div className="col-xl-6 col-sm-4">
                                {
                                    editMode &&
                                    <button onClick={this.handleDelete} className={DeleteButtonClassName} disabled={form_isLoading} data-ripple-button="">
                                        <span className="ripple-text">Удалить кнопку</span>
                                    </button>
                                }
                            </div>
                        </div>
                    }
                    </div>
                </div>
            </div>
        );
    }
};
