import React from 'react';

const LinkSort = ({ sortName, label, isActiveSortName, handleChangeSort, direction, arrowSort }) => {

   const handleClick = () => {
      handleChangeSort(sortName);
   };

   return (
       arrowSort ?
           <a className="LinkSort" onClick={handleClick}>
               <i className={`LinkSort-Icon ${direction === 'asc' ? 'LinkSortWhite-Icon_up' : 'LinkSortWhite-Icon_down'} ${isActiveSortName === sortName ? 'active' : ''}`}></i>
               {label}
           </a>
           :
           <a className="LinkSort" onClick={handleClick}>
              <i className={`LinkSort-Icon ${direction === 'asc' ? 'LinkSort-Icon_up' : 'LinkSort-Icon_down'} ${isActiveSortName === sortName ? 'active' : ''}`}></i>
              {label}
           </a>
   );
};

export default LinkSort;
