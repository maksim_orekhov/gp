// import './Input.scss';
import React from 'react';
import PropTypes from 'prop-types';
import { Transition } from 'react-transition-group';
import textMask, { conformToMask } from 'react-text-mask';
import createAutoCorrectedDatePipe from 'text-mask-addons/dist/createAutoCorrectedDatePipe.js';
const classNames = require('classnames');

export default class Datepicker extends React.Component {

   constructor(props) {
      super(props);
      this.state = {
         value: '',
         isFocused: false,
         isValid: null,
         isError: false,
         errorMsg: ''
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleOnBlur = this.handleOnBlur.bind(this);
      this.handleClick = this.handleClick.bind(this);
   }
   
   componentDidMount() {
      this.initDatepicker();
   }

   componentWillReceiveProps(nextProps) {
      const { name, handleBlur, valueProps } = nextProps;

      if (valueProps) {
         this.setState({
            value: valueProps,
            errorMsg: '',
            isValid: true,
            isError: false
         });
      }
   }

   initDatepicker() {
      $(this.input).datepicker({
         onSelect: (formattedDate) => {
            console.log(formattedDate);
            this.setState({
               value: formattedDate
            });
         },
         autoClose: true
      });
   }

   handleChange(e) {
      const { value } = e.target;
      const dateMask = [ /\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/];
      const formattedValue = conformToMask(value, dateMask, { guide: false, keepCharPositions: false }).conformedValue;

      this.setState({
         value: formattedValue
      });
   }

   onFocusHandler() {
      this.setState({
         isFocused: true
      });
   }

   onBlurHandler() {
      this.state.value ?
         this.setState({
            isFocused: true
         }) :
         this.setState({
            isFocused: false
         });
   }

   showError(errorMsg) {
      const newErrorMsg = errorMsg ? errorMsg : '';
      this.setState({
         errorMsg: newErrorMsg,
         isValid: false,
         isError: true
      });
   }

   validateIsRequired() {
      if (this.props.validation.isRequired) {
         if (this.state.value) {
            return true;
         } else {
            this.showError('Поле не может быть пустым');
            return false;
         }
      }
   }

   validateCharsLength() {
      const { minChars, maxChars } = this.props.validation;
      const { value } = this.state;

      if (minChars || maxChars) {
         if (value.length < minChars) {
            this.showError(`Минимальное количество символов - ${minChars}`);
            return false;
         } else if (value.length > maxChars) {
            this.showError(`Максимальное количество символов ${maxChars} символов`);
            return false;
         } else {
            return true;
         }
      } else {
         return true;
      }
   }

   validateWordsLength() {
      const { minWords, maxWords } = this.props.validation;
      const { value } = this.state;
      // const wordsCount = value.split(' ').length;

      const wordsCount = (s) => {
         s = s.replace(/(^\s*)|(\s*$)/gi, ''); // exclude  start and end white-space
         s = s.replace(/[ ]{2,}/gi, ' '); // 2 or more space to 1
         s = s.replace(/\n /, '\n'); // exclude newline with a start spacing
         return s.split(' ').length;
      };

      if (minWords && wordsCount(value) < minWords) {
         this.showError(`Минимальное количество слов ${minWords}`);
         return false;
      } else if (maxWords && wordsCount(value) > maxWords) {
         this.showError(`Максимальное количество слов ${maxWords}`);
         return false;
      } else {
         return true;
      }
   }

   validateAge() {
      const { minAge } = this.props.validation;
      const { value } = this.state;
      // const wordsCount = value.split(' ').length;

      if (minAge) {
         const dateToMilliseconds = (date) => {
            const dateObj = new Date(date.replace(/\./g, '-'));
            return dateObj.getTime();
         };

         // const diffMs = Date.now() - dateToMilliseconds(value);
         if (dateToMilliseconds(value) > Date.now() - minAge * 365 * 24 * 60 * 60 * 1000) {
            this.showError(`Вам должно быть больше ${minAge} лет`);
            return false;
         } else {
            return true;
         }
      } else {
         return true;
      }
   }

   checkAllValidations() {
      return (
         this.validateIsRequired()
            && this.validateCharsLength()
            && this.validateWordsLength()
            && this.validateAge()
            ? true : false
      );
   }

   handleOnBlur(e) {
      const { name, handleBlur } = this.props;
      const { value } = this.state;

      if (this.checkAllValidations()) {
         this.setState({
            errorMsg: '',
            isValid: true,
            isError: false
         }, () => handleBlur(name, value));
      } else {
         handleBlur(name, '');
      }
   }

   handleClick() {
      const { errorMsg } = this.state;
      this.setState({
         errorMsg: '',
         isError: false
      });
      this.input.focus();
   }

   render() {
      const { name, label, type, placeholder } = this.props;
      const { value, isFocused, isError, isValid, errorMsg } = this.state;

      const InputClassName = classNames({
         'Input-Wrapper': true,
         'hasHint': isError,
         'valid': isValid,
      });

      return (
         <div className={InputClassName} onClick={this.handleClick} >
            <label htmlFor={name}>{`${label} ${errorMsg !== '' ? '- ' + errorMsg.toLowerCase() : ''}`}</label>
            <input
               ref={input => this.input = input}
               name={name}
               tabIndex={0}
               type={type}
               // step="1"
               onChange={this.handleChange}
               onBlur={this.handleOnBlur}
               placeholder={placeholder}
               value={value}
               /* onFocus={this.onFocusHandler}
               onBlur={this.onBlurHandler} */ />
         </div>
      );
   }
}
