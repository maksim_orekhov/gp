/* 

<-- PROPS -->

Мы прокидываем через props следующие свойства компонента Control.
type: string; Тип input'a. Значения: text, number, email, password и т.д. Обязательное свойство!

name: string; Название input'a. Используется для привязки label и для прокидывания данных в родительский компонент при помощи метода this.props.handleBlur(name, value). Обязательное свойство!

format?: string; Формат ввода текста. Применяет маску и запрещает ввод тех символов, которые не пройдут валидацию. Это свойство можно улучшить и дополнить новыми форматами. Необязательное свойство.

label?: string; Название для label. Необязательное свойство.

placeholder?: string; Название для placeholder. Необязательное свойство.

valueProps?: any; Значение поля ввода, которое мы можем прокинуть через props компонента. Используется для подстановки значенией извне, например, автоподстановка данных о банке после ввода БИК. Или если мы открываем форму "Редактирование физического/юридического лица" и аналогичных форм. Необязательное свойство.


<-- ВАЛИДАЦИЯ -->

Все свойства являются необязательными.
validation?: {
   isRequired?: boolean; Поле обязательно для ввода или нет.
   minChars?: number; Минимальное кол-во символов
   maxChars?: number; Максимальное кол-во символов
   minWords?: number; Минимальное кол-во слов
   maxWords?: number; Максимальное кол-во слов
   customValidation?: { Кастомная валидация поля ввода
      validationFunction: validateDeliveryPeriod, Функция для кастомной валидации
      validationParams: { Объект с аргументами функции для кастомной валидации
         minDays: 1
      }
   }
};

this.props.handleBlur?: Function; Метод, который отправляет данные из input'a наверх в форму, в которой находится этот input, при событии onBlur. Необязательно свойство.


<-- STATE -->

value: any; Значение поля ввода
isValid?: boolean; Валидно ли значение value или нет.
isError?: boolean; В случае невалидного значение value вешается класс на Input-Wrapper
errorMsg?: string; Текст ошибки в случае невалидного value

*/

import React from 'react';
import PropTypes from 'prop-types';
import { TweenLite, Expo, Linear, TimelineLite } from 'gsap';
import { Transition } from 'react-transition-group';
import textMask, { conformToMask } from 'react-text-mask';
// import emailMask from 'text-mask-addons/dist/emailMask';
// import createAutoCorrectedDatePipe from 'text-mask-addons/dist/createAutoCorrectedDatePipe.js';
import Inputmask from "inputmask";
const classNames = require('classnames');

export default class Control extends React.Component {

   constructor(props) {
      super(props);
      this.state = {
         value: '',
         isValid: null,
         isError: false,
         errorMsg: ''
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleOnBlur = this.handleOnBlur.bind(this);
      this.handleClick = this.handleClick.bind(this);
      this.validateCustom = this.validateCustom.bind(this);
   }


   // componentWillReceiveProps. В случае, если мы прокидываем значение value через props (свойство valueProps), то необходимо выставить в state нужные состояния
   componentWillReceiveProps(nextProps) { 
      const { name, handleBlur, valueProps, formError } = nextProps;

      if (valueProps) {
         this.setState({
            value: valueProps,
            errorMsg: '',
            isValid: true,
            isError: false
         });
      }
      if (this.props.formError !== formError) {
         this.setState({
             isValid: false,
             isError: true,
             errorMsg: formError
         })
      }
   }

   // Метод, который сетит в state значение поля value, после прохождения через маску
   handleChange(e) {
      const { value } = e.target;
      let newValue;

      // Маски, которые используются для react-text-mask при помощи метода conformToMask. Их необходимо улучшить.
      const phoneMask = ['+', '7', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
      const dateMask = [/\d/, /\d/, '.', /\d/, /\d/, '.', /\d/, /\d/, /\d/, /\d/];

      if (this.props.format) {
         switch (this.props.format) {
            case 'number':
               newValue = value.replace(/\D/g, '');
               break;
            case 'text':
               newValue = value.replace(/\d/g, '');
               break;
            case 'money':
               newValue = value.replace(/[^0-9.,]/g, '');
               // RegExp on back-end '(^-?\d*(\.\d+)?$)'
               break;
            case 'textCapital':
               newValue = (value.charAt(0).toUpperCase() + value.slice(1)).replace(/[\d`~!@#$%^&*()_|+=?;:'",.<>\{\}\[\]\\\/№§]/g, '');
               break;
            case 'phone':
               newValue = conformToMask(value, phoneMask, { guide: false, keepCharPositions: false }).conformedValue;
               break;
            case 'date':
               // const autoCorrectedDatePipe = createAutoCorrectedDatePipe('dd.mm.yyyy');
               newValue = conformToMask(value, dateMask, { guide: false, keepCharPositions: false }).conformedValue;
               // newValue.autoCorrectedDatePipe;
               break;
            default:
               newValue = value;
               break;
         }
      } else {
         newValue = value;
      }

      this.setState({
         value: newValue
      }, () => this.handleOnBlur());
   }

   showError(errorMsg) {
      const newErrorMsg = errorMsg ? errorMsg : '';
      this.setState({
         errorMsg: newErrorMsg,
         isValid: false,
         isError: true
      });
   }

   // Валидация, которая проверяет является ли поле обязательным к заполнению
   validateIsRequired() {
      if (this.props.validation.isRequired) {
         if (this.state.value) {
            return true;
         } else {
            this.showError('поле не может быть пустым');
            return false;
         }
      } else {
         return true;
      }
   }
   
   // Валидация, которая проверяет кол-во символов в поле ввода
   validateCharsLength() {
      const { minChars, maxChars } = this.props.validation;
      const { value } = this.state;

      if (minChars || maxChars) {
         if (value.length < minChars) {
            this.showError(`минимальное количество символов - ${minChars}`);
            return false;
         } else if (value.length > maxChars) {
            this.showError(`максимальное количество символов - ${maxChars}`);
            return false;
         } else {
            return true;
         }
      } else {
         return true;
      }
   }
   
   // Валидация, которая проверяет кол-во слов в поле ввода
   validateWordsLength() {
      const { minWords, maxWords } = this.props.validation;
      const { value } = this.state;
      // const wordsCount = value.split(' ').length;

      if (minWords || maxWords) {
         const wordsCount = (s) => {
            s = s.replace(/(^\s*)|(\s*$)/gi, ''); // exclude  start and end white-space
            s = s.replace(/[ ]{2,}/gi, ' '); // 2 or more space to 1
            s = s.replace(/\n /, '\n'); // exclude newline with a start spacing
            return s.split(' ').length;
         };

         if (minWords && wordsCount(value) < minWords) {
            this.showError(`минимальное количество слов - ${minWords}`);
            return false;
         } else if (maxWords && wordsCount(value) > maxWords) {
            this.showError(`максимальное количество слов - ${maxWords}`);
            return false;
         } else {
            return true;
         }
      } else {
         return true;
      }
   }

   // Метод для обработки кастомной валидации
   validateCustom() {
      const { customValidation } = this.props.validation;

      if (customValidation && typeof customValidation === 'object') {
         const { validationFunction, validationParams } = customValidation;
         if (typeof validationFunction === 'function') {
            const validation = validationFunction(this.state.value, validationParams);
            const isValid = validation.isValid;
            const message = validation.message;
            return isValid ? true : this.showError(message);
         }
      } else {
         return true;
      }
   }

   // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
   checkAllValidations() {
      return (
         this.validateIsRequired()
            && this.validateCharsLength()
            && this.validateWordsLength()
            && this.validateCustom()
            ? true : false
      );
   }

   handleOnBlur() {
      const { name, handleBlur, isValid = () => {} } = this.props;
      const { value } = this.state;

      if (this.checkAllValidations()) {
         this.setState({
            errorMsg: '',
            isValid: true,
            isError: false
         }, () => {
            handleBlur(name, value);
            isValid(true);
         }); // Пробрасываем валидные данные наверх в форму, в которой находится Control
      } else {
         isValid(false);
         handleBlur(name, ''); // Или пробрасываем пустое значение, если данные невалидные
      }
   }


   // Метод, который срабатывает на клике по Input-Wrapper, чтобы сбросить ошибку и установить курсор непосредственно в поле ввода
   handleClick() {
      this.setState({
         errorMsg: '',
         isError: false
      });
      this.input.focus();
   }

   render() {
      const { name, label, type, placeholder, maxLength } = this.props;
      const { value, isError, isValid, errorMsg } = this.state;

      // classNames удобная библиотека для работы с классами, чтобы не городить огромную строку в className
      const InputClassName = classNames({
         'Input-Wrapper': true,
         'hasHint': isError,
         'valid': isValid,
      });

      return (
          <div className="grid-row">
                <div className={InputClassName} onClick={this.handleClick}>
                    {/* Показываем сообщение об ошибке сразу внутри label */}
                   <label htmlFor={name}>{`${label} ${errorMsg !== '' ? '- ' + errorMsg.toLowerCase() : ''}`}</label>
                   <input
                       ref={input => this.input = input} // используется в методе handleClick() - this.input.focus()
                       name={name}
                       tabIndex="0"
                       type={type}
                       onChange={this.handleChange}
                       onBlur={this.handleOnBlur}
                       placeholder={placeholder}
                       value={value}
                       maxLength={maxLength}
                   />
                </div>
          </div>
      );
   }
}

Control.propTypes = {
   name: PropTypes.string.isRequired,
   type: PropTypes.string.isRequired,
   label: PropTypes.string,
   placeholder: PropTypes.string,
   format: PropTypes.string,
   valueProps: PropTypes.string,
   validation: PropTypes.shape({
      isRequired: PropTypes.bool,
      minChars: PropTypes.number,
      maxChars: PropTypes.number,
      minWords: PropTypes.number,
      maxWords: PropTypes.number,
      customValidation: PropTypes.shape({
         validationFunction: PropTypes.func,
         validationParams: PropTypes.object,
      }),
   }),
   handleBlur: PropTypes.func,
};