import React from 'react';
import BankName from '../controls/ControlBankName';
import CorrAccountNumber from '../controls/ControlCorrAccountNumber';
import AccountNumber from '../controls/ControlAccountNumber';
import Bik from '../controls/ControlBik';
import FormBase from './FormBase';
import ShowError from '../ShowError';
import URLS from '../constants/urls';
import {customFetch} from "../Helpers";
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
// данная форма применяется в карточке спора для заполнения реквизитов на возврат денег
export default class FormRefundPaymentMethod extends FormBase {
    constructor(props) {
        super(props);
        this.state = {
            isEdit: false,
            /**
             * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
             * @example
             *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
             **/
            controls_server_errors: {
                csrf: null,
                name: null,
                bik: null,
                bank_name: null,
                account_number: null,
                corr_account_number: null
            },
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation_props
            validation: {
                name_is_valid: props.is_hide_name || false,
                bik_is_valid: false,
                bank_name_is_valid: false,
                account_number_is_valid: false,
                corr_account_number_is_valid: false
            },
            // -----------------------------------------
            form: {
                bik: '',
                bank_name: '',
                account_number: '',
                corr_account_number: '',
                type_form: 'discount_form'
            },
            // Обьект данных о самых популярных банках России
            popularBanks: {},
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false,
            form_isDeleting: false,
            is_deal: false // Флажок что форма открыта из карточки сделки и нужно привязать метод оплаты к сделке
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleBik = this.handleBik.bind(this);
    };

    componentDidMount() {
        this.getFormInitData();
    }

    getFormInitData() {
        customFetch(URLS.PAYMENT_METHOD.FORM_INIT)
            .then(data => {
                console.log(data);
                if (data.status === 'SUCCESS') {
                    this.setState({
                        popularBanks: data.data.handbooks.popularBanks
                    });
                } else {
                    return Promise.reject(data.message);
                }
            })
            .catch(error => {
                this.setFormError('form_init_fail', true);
                console.log(error);
            });
    }

    handleBik(bankInfo) {
        this.setState({
            form: {
                corr_account_number: bankInfo['bank_ks'],
                bank_name: bankInfo['bank_name'],
                bik: bankInfo['bank_bik']
            }
        }, () => this.checkAllControlsAreValid());
    }

    render() {
        const { popularBanks } = this.state;
        const { bik, bank_name, corr_account_number } = this.state.form;

        return (
            <div className="PageForm-Container PageForm-Container_large">
                <div className="FormLabelTop">
                    <div className="flex-start">
                        <div className="grid-col-2">
                            <Bik
                                name="bik"
                                label="БИК"
                                placeholder="Введите БИК"
                                value_prop={bik}
                                popularBanks={popularBanks}
                                handleBik={this.handleBik} // Метод принимающие данные для банка и делающий автозамену
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.bik}
                            />
                        </div>
                        <div className="grid-col-2">
                            <BankName
                                label="Наименование банка"
                                placeholder="Введите наименование банка"
                                name="bank_name"
                                value_prop={bank_name}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.bank_name}
                            />
                        </div>
                    </div>
                    <div className="flex-start">
                        <div className="grid-col-2">
                            <CorrAccountNumber
                                name="corr_account_number"
                                label="Номер корр. счета"
                                placeholder="Введите номер корр. счета"
                                value_prop={corr_account_number}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.corr_account_number}
                            />
                        </div>
                        <div className="grid-col-2">
                            <AccountNumber
                                name="account_number"
                                label="Номер лицевого счета"
                                placeholder="Введите номер лицевого счета"
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.account_number}
                            />
                        </div>
                    </div>
                    <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                </div>
            </div>
        );
    }
}