import React from 'react';
import LegalEntityName from '../controls/ControlLegalEntityName';
import Address from '../controls/ControlAddress';
import Inn from '../controls/ControlInn';
import Kpp from '../controls/ControlKpp';
import NdsType from '../controls/ControlNdsType';
import { customFetch } from "../Helpers";
import URLS from '../constants/urls';
import ShowError from '../ShowError';
import FormBase from './FormBase';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

const classNames = require('classnames');


export default class FormLegalEntity extends FormBase {

    constructor(props) {
        super(props);

        this.state = {
            /**
             * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
             * @example
             *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
             **/
            controls_server_errors: {
                csrf: null,
                name: null,
                legal_address: null,
                inn: null,
                kpp: null,
                nds_type: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                name_is_valid: false,
                legal_address_is_valid: false,
                inn_is_valid: false,
                kpp_is_valid: false,
                nds_type_is_valid: false
            },
            // ------------------------------
            form: {
                csrf: '',
                name: '',
                legal_address: '',
                inn: '',
                kpp: '',
                nds_type: '',
                civil_law_subject_type: 'legal_entity'
            },
            form_isLoading: false,
            form_isDeleting: false,
            form_isSubmitting: false,
            nds_types: [],
            form_isValid: false
        };

        this.prev_state = null; // Первоначальное состояние формы для блокировки кнопки сохранить в режиме изменения, если данные не изменились от первоначальных
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.form_name = 'form_legal_entity'; // Название формы для хранения данных в local_storage
        if (props.editMode) {
            this.form_name = null; // В режиме редактирования не сохраняем данные
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleCloseForm = this.handleCloseForm.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        (this.props.data && this.props.editMode) && this.getDataFromProps();
    }

    componentDidMount() {
        if (!this.props.is_nested) {
            this.getCsrf()
                .then(() => this.rememberState());
        }
        this.getNdsTypes();
        this.getFormFromLocalStorage();
    }

    componentWillReceiveProps(nextProps) {
        const { form_server_errors, clear_local_storage } = nextProps;

        if (clear_local_storage !== this.props.clear_local_storage) {
            this.clearFormLocalStorage();
        }

        form_server_errors && this.SetControlsErrorFromUpperForm(this.props.form_server_errors, form_server_errors);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    getDataFromProps() {
        const { id, name, legal_address, inn, kpp, nds } = this.props.data;

        this.setState({
            id,
            form: {
                ...this.state.form,
                name,
                legal_address,
                inn,
                kpp,
                nds_type: nds.type
            }
        }, () => this.rememberState());
    }

    getNdsTypes() {
        customFetch(URLS.LEGAL_ENTITY.FORM_INIT)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    this.setState({
                        nds_types: data.data.handbooks.ndsTypes
                    });
                } else if (data.status === 'ERROR') {
                    return Promise.reject(data.message);
                }
            })
            .catch(error => {
                this.setFormError('form_init_fail', true);
            });
    }

    handleSubmit(e) {
        e.preventDefault();
        const { form_isValid } = this.state;

        if (form_isValid && !this.props.is_nested) {
            this.toggleIsLoading();

            customFetch(this.props.editMode ? `${URLS.LEGAL_ENTITY.SINGLE}/${this.state.id}` : URLS.LEGAL_ENTITY.SINGLE, {
                method: 'POST',
                body: JSON.stringify(this.state.form)
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.clearFormLocalStorage();
                        const is_after_submit = typeof this.props.afterSubmit === 'function';
                        !is_after_submit && this.toggleIsLoading();
                        is_after_submit && this.props.afterSubmit();
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                })
        }
    }

    handleDelete() {
        const { id } = this.props.data;
        this.toggleIsLoading('form_isDeleting');
        customFetch(`/legal-entity/${id}`, {
            method: 'DELETE'
        })
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const is_after_delete = typeof this.props.afterDelete === 'function';
                    !is_after_delete && this.toggleIsLoading('form_isDeleting');
                    is_after_delete && this.props.afterDelete();
                } else if (data.status === 'ERROR') {
                    this.toggleIsLoading('form_isDeleting');
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading('form_isDeleting');
            });
    }

    handleCloseForm() {
        this.props.handleClose && this.props.handleClose();
        this.props.slideUp && this.props.slideUp();
    }

    render() {
        const { isClosable, editMode, is_nested } = this.props;
        const { form_isValid, form_isLoading, form_isSubmitting, form_isDeleting, nds_types } = this.state;
        const { name, legal_address, inn, kpp, nds_type } = this.state.form;

        const formTitle = `${editMode ? 'Редактирование' : 'Добавление'} юридического лица`;
        const buttonText = editMode ? 'Сохранить' : 'Добавить';

        const SubmitButtonClassName = classNames({
            'ButtonApply margin-right': true,
            'Preloader Preloader_light': form_isSubmitting
        });

        const DeleteButtonClassName = classNames({
            'ButtonDelete': true,
            'Preloader Preloader_light': form_isDeleting
        });


        return (
            <div className='FormLabelTop form-default' id='form_legal_entity'>
                {
                    isClosable &&
                    <div className="ButtonClose" onClick={this.handleCloseForm} />
                }
                <div className="row nested-row">
                    <div className="col-xl-12 col-sm-4">
                        <h3 className="TitleBlue">{formTitle}</h3>
                    </div>
                </div>
                <div className="row nested-row">
                    <div className="col-xl-6 col-sm-4">
                        <LegalEntityName
                            label="Наименование:"
                            placeholder="Введите наименование"
                            name="name"
                            value_prop={name}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.name}
                        />
                    </div>
                    <div className="col-xl-6 col-sm-4">
                        <Address
                            label="Юридический адрес:"
                            placeholder="Введите юридический адрес"
                            name="legal_address"
                            value_prop={legal_address}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.legal_address}
                        />
                    </div>
                </div>
                <div className="row nested-row">
                    <div className="col-xl-6 col-sm-4">
                        <Inn
                            label="ИНН:"
                            placeholder="Введите ИНН"
                            name="inn"
                            value_prop={inn}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.inn}
                        />
                    </div>
                    <div className="col-xl-6 col-sm-4">
                        <Kpp
                            label="КПП:"
                            placeholder="Введите КПП"
                            name="kpp"
                            value_prop={kpp}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.kpp}
                        />
                    </div>
                </div>
                <div className="row nested-row">
                    <div className="col-xl-6 col-sm-4">
                        <NdsType
                            label="НДС:"
                            placeholder="Выберите тип НДС"
                            name="nds_type"
                            value_prop={nds_type}
                            nds_types={nds_types}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.nds_type}
                        />
                    </div>
                </div>
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                {
                    !is_nested &&
                        <div className="ButtonsRow row nested-row">
                            <div className="col-xl-6 col-sm-4">
                                <button onClick={this.handleSubmit} className={SubmitButtonClassName} disabled={!form_isValid || form_isLoading} data-ripple-button=""><span className="ripple-text">{buttonText}</span></button>
                            </div>
                            <div className="col-xl-6 col-sm-4">
                                {
                                    editMode &&
                                    <button onClick={this.handleDelete} className={DeleteButtonClassName} disabled={form_isLoading} data-ripple-button=""><span className="ripple-text">Удалить</span></button>
                                }
                            </div>
                        </div>
                }
            </div>
        );
    }
};
