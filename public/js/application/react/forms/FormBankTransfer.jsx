import React from 'react';
import BankName from '../controls/ControlBankName';
import BankTransferName from '../controls/ControlBankTransferName';
import CorrAccountNumber from '../controls/ControlCorrAccountNumber';
import AccountNumber from '../controls/ControlAccountNumber';
import Bik from '../controls/ControlBik';
import FormBase from './FormBase';
import ShowError from '../ShowError';
import URLS from '../constants/urls';
import {customFetch, getIdFromUrl} from "../Helpers";
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

const classNames = require('classnames');

export default class FormBankTransfer extends FormBase {
    constructor(props) {
        super(props);
        this.state = {
            isEdit: false,
            /**
             * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
             * @example
             *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
             **/
            controls_server_errors: {
                csrf: null,
                name: null,
                bik: null,
                bank_name: null,
                account_number: null,
                corr_account_number: null
            },
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation_props
            validation: {
                name_is_valid: props.is_hide_name || false,
                bik_is_valid: false,
                bank_name_is_valid: false,
                account_number_is_valid: false,
                corr_account_number_is_valid: false
            },
            // -----------------------------------------
            form: {
                csrf: '',
                name: '',
                bik: '',
                bank_name: '',
                account_number: '',
                corr_account_number: '',
                civil_law_subject_id: props.civil_law_subject_id || '',
                payment_method_type: 'bank_transfer',
                id: props.id || ''
            },
            // Обьект данных о самых популярных банках России
            popularBanks: {},
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false,
            form_isDeleting: false,
            is_deal: false // Флажок что форма открыта из карточки сделки и нужно привязать метод оплаты к сделке
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.form_name = 'form_bank_transfer'; // Название формы для хранения данных в local_storage
        if (props.editMode) {
            this.form_name = null; // В режиме редактирования не сохраняем данные
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBik = this.handleBik.bind(this);
        this.handleCloseForm = this.handleCloseForm.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    };

    componentWillMount() {
        (this.props.data && this.props.editMode) && this.getDataFromProps();
    }

    componentDidMount() {
        if (!this.props.is_nested) {
            this.getCsrf()
                .then(() => this.rememberState())
        }

        this.getFormInitData();
        this.getFormFromLocalStorage();
    }

    componentWillReceiveProps(nextProps) {
        const { form_server_errors, civil_law_subject_id, clear_local_storage } = nextProps;

        if (clear_local_storage !== this.props.clear_local_storage) {
            this.clearFormLocalStorage();
        }

        civil_law_subject_id && this.setState({
            form: {
                ...this.state.form,
                civil_law_subject_id
            }
        });

        form_server_errors && this.SetControlsErrorFromUpperForm(this.props.form_server_errors, form_server_errors);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    getDataFromProps() {
        const { name, bik, bank_name, corr_account_number, account_number, id } = this.props.data.bank_transfer;

        this.setState({
            form: {
                ...this.state.form,
                name,
                bik,
                bank_name,
                corr_account_number,
                account_number,
                id
            }
        }, () => this.rememberState())
    }

    getFormInitData() {
        customFetch(URLS.PAYMENT_METHOD.FORM_INIT)
            .then(data => {
                console.log(data);
                if (data.status === 'SUCCESS') {
                    this.setState({
                        popularBanks: data.data.handbooks.popularBanks
                    });
                } else {
                    return Promise.reject(data.message);
                }
            })
            .catch(error => {
                this.setFormError('form_init_fail', true);
                console.log(error);
            });
    }

    handleSubmit() {
        const { form_isValid } = this.state;

        if (form_isValid && !this.props.is_nested) {
            this.toggleIsLoading();

            const { id } = this.state.form;
            const { editMode } = this.props;

            const url = editMode ? `${URLS.PAYMENT_METHOD.SINGLE}/${id}` : URLS.PAYMENT_METHOD.SINGLE;

            customFetch(url, {
                method: 'POST',
                body: JSON.stringify(this.getDataFromState())
            })
                .then(data => {
                    this.toggleIsLoading();
                    if (data.status === 'SUCCESS') {
                        this.clearFormLocalStorage();
                        const { id } = data.data.paymentMethod;
                        typeof this.props.afterSubmit === 'function' && this.props.afterSubmit(id);
                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
            }
    }

    handleDelete() {
        const { id } = this.state.form;

        this.toggleIsLoading('form_isDeleting');

        customFetch(`${URLS.PAYMENT_METHOD.SINGLE}/${id}`, {
            method: 'DELETE',
            body: JSON.stringify({
                id
            })
        })
            .then(data => {
                this.toggleIsLoading('form_isDeleting');
                if (data.status === 'SUCCESS') {
                    typeof this.props.afterSubmit === 'function' && this.props.afterSubmit();
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading('form_isDeleting');
            });
    }

    handleBik(bankInfo) {
        this.setState({
            form: {
                ...this.state.form,
                corr_account_number: bankInfo['bank_ks'],
                bank_name: bankInfo['bank_name'],
                bik: bankInfo['bank_bik']
            }
        }, () => {
            this.checkAllControlsAreValid();
            this.saveFormToLocalStorage();
        });
    }

    handleCloseForm() {
        this.props.handleClose && this.props.handleClose();
    }

    render() {
        const { editMode, is_hide_name, is_nested } = this.props;
        const { form_isValid, form_isLoading, popularBanks, form_isSubmitting, form_isDeleting } = this.state;
        const { name, bik, bank_name, corr_account_number, account_number } = this.state.form;

        const formTitle = `${editMode ? 'Редактирование' : 'Добавление'} метода оплаты типа "Банковский перевод"`;

        const SubmitButtonClassName = classNames({
            'ButtonApply margin-right': true,
            'Preloader Preloader_light': form_isSubmitting
        });

        const DeleteButtonClassName = classNames({
            'ButtonDelete': true,
            'Preloader Preloader_light': form_isDeleting
        });

        return (
            <div className="PageForm-Container PageForm-Container_large" id="form_bank_transfer">
                <div className="FormLabelTop form-default form-bank-transfer">
                    {
                        !is_nested &&
                        <div className="row nested-row">
                            <div className="col-xl-12">
                                <h2 className="TitleBlue">{formTitle}</h2>
                            </div>
                        </div>
                    }
                    {
                        !is_hide_name &&
                        <div className="row nested-row">
                            <div className="col-xl-6 col-sm-4">
                                <BankTransferName
                                    label="Название шаблона платежа:"
                                    placeholder="Введите название шаблона платежа"
                                    name="name"
                                    value_prop={name}
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleComponentValid}
                                    form_control_server_errors={this.state.controls_server_errors.name}
                                />
                            </div>
                        </div>
                    }
                    <div className="row nested-row">
                        <div className="col-xl-6 col-sm-4">
                            <Bik
                                name="bik"
                                label="БИК:"
                                placeholder="Введите БИК"
                                value_prop={bik}
                                popularBanks={popularBanks}
                                handleBik={this.handleBik} // Метод принимающие данные для банка и делающий автозамену
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.bik}
                            />
                        </div>
                        <div className="col-xl-6 col-sm-4">
                            <BankName
                                label="Наименование банка:"
                                placeholder="Введите наименование банка"
                                name="bank_name"
                                value_prop={bank_name}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.bank_name}
                            />
                        </div>
                    </div>
                    <div className="row nested-row">
                        <div className="col-xl-6 col-sm-4">
                            <CorrAccountNumber
                                name="corr_account_number"
                                label="Номер корр. счёта:"
                                placeholder="Введите корр.счёт"
                                value_prop={corr_account_number}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.corr_account_number}
                            />
                        </div>
                        <div className="col-xl-6 col-sm-4 account-number">
                            <AccountNumber
                                name="account_number"
                                label="Номер лицевого счёта:"
                                placeholder="Введите номер лицевого счёта"
                                value_prop={account_number}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.account_number}
                            />
                        </div>
                    </div>
                    <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                    {
                        !is_nested &&
                        (editMode ?
                                <div className="row nested-row ButtonsRow">
                                    <div className="col-xl-6 col-sm-2 col-xs-4">
                                        <button className={SubmitButtonClassName} disabled={!form_isValid || form_isLoading}  data-ripple-button=""
                                                onClick={this.handleSubmit}><span className="ripple-text">Сохранить</span></button>
                                    </div>
                                    <div className="col-xl-6 col-sm-2 col-xs-4">
                                        <button onClick={this.handleDelete} disabled={form_isLoading} className={DeleteButtonClassName} data-ripple-button=""><span className="ripple-text">Удалить</span></button>
                                    </div>
                                </div>
                                :
                                <div className="row nested-row ButtonsRow">
                                    <div className="col-xl-6 col-sm-2 col-xs-4">
                                        <button className={SubmitButtonClassName} disabled={!form_isValid || form_isLoading}  data-ripple-button=""
                                                onClick={this.handleSubmit}>
                                            <span className="ripple-text">Добавить</span>
                                        </button>
                                    </div>
                                </div>
                        )
                    }
                </div>
            </div>
        );
    }
}