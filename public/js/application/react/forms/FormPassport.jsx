import React from 'react';
import Control from './base/Control';
import Datepicker from './base/Datepicker';

class FormPassport extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         form_isValid: false,
         form_isLoading: false,
         passport_serial_number: '',
         date_issued: '',
         passport_issue_organisation: '',
         date_expired: '',
         file: '',
      }
      this.handleChange = this.handleChange.bind(this);
      this.toggleIsLoading = this.toggleIsLoading.bind(this);
   }

   handleChange(name, value) {
      this.setState({
         [name]: value
      }, () => this.checkAllInputsAreValid());
   }

   checkAllInputsAreValid() {
      const { isNested, sendUpValidation, sendUpData } = this.props;
      const { passport_serial_number, date_issued, passport_issue_organisation, date_expired, file } = this.state;

      if (passport_serial_number && date_issued && passport_issue_organisation && date_expired && file) {
         this.setState({
            form_isValid: true
         }, () => isNested && sendUpValidation(true));
      } else {
         this.setState({
            form_isValid: false
         }, () => isNested && sendUpValidation(false));
      }

      isNested && sendUpData(this.getDataFromState());
   }

   render() {
      return (
         <div className="FormLabelTop">
            <h3 className="TitleBlue">Добавление паспорта</h3>
            <Control
               name="passport_serial_number"
               type="text"
               label="Серия и номер"
               placeholder="Введите серию и номер"
               format="number"
               validation={{
                  isRequired: true,
                  minChars: 8,
                  maxChars: 10,
               }}
               handleBlur={this.handleChange}
            />
            <Datepicker
               name="date_issued"
               type="text"
               label="Дата выдачи"
               placeholder="Введите дату выдачи"
               validation={{
                  isRequired: true,
                  minChars: 8,
                  maxChars: 10,
               }}
               handleBlur={this.handleChange}
            />
            <Control
               name="passport_issue_organisation"
               type="text"
               label="Кем выдан"
               placeholder="Кем выдан"
               validation={{
                  isRequired: true,
                  minChars: 8,
                  maxChars: 10,
               }}
               handleBlur={this.handleChange}
            />
            <Datepicker
               name="date_expired"
               type="text"
               label="Дата истечения"
               placeholder="Выберите дату истечения"
               validation={{
                  isRequired: true,
                  minChars: 8,
                  maxChars: 10,
               }}
               handleBlur={this.handleChange}
            />
            <Control
               name="file"
               type="text"
               label="Скан паспорта"
               placeholder="Выберите файл для загрузки"
               format="number"
               validation={{
                  isRequired: true,
                  minChars: 8,
                  maxChars: 10,
               }}
               handleBlur={this.handleChange}
            />
         </div>
      );
   }
}

export default FormPassport;