import React from 'react';
import LastName from '../controls/ControlLastName';
import FirstName from '../controls/ControlFirstName';
import SecondaryName from '../controls/ControlSecondaryName';
import BirthDate from '../controls/ControlBirthDate';
import FormBase from './FormBase';
import ShowError from '../ShowError';
import { customFetch } from "../Helpers";
import URLS from '../constants/urls';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';

const classNames = require('classnames');

export default class FormNaturalPerson extends FormBase {

    constructor(props) {
        super(props);

        this.state = {
            /**
             * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
             * @example
             *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
             **/
            controls_server_errors: {
                csrf: null,
                last_name: null,
                first_name: null,
                secondary_name: null,
                birth_date: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validate_props
            validation: {
                last_name_is_valid: false,
                first_name_is_valid: false,
                secondary_name_is_valid: false,
                birth_date_is_valid: false
            },
            // --------------------------------
            form: {
                csrf: '',
                last_name: '',
                first_name: '',
                secondary_name: '',
                birth_date: '',
                civil_law_subject_type: 'natural_person'
            },
            form_isValid: false,
            form_isLoading: false,
            form_isSubmitting: false,
            form_isDeleting: false
        };

        this.prev_state = null; // Первоначальное состояние формы для блокировки кнопки сохранить в режиме изменения, если данные не изменились от первоначальных
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.form_name = 'form_natural_person'; // Название формы для хранения данных в local_storage
        if (props.editMode) {
            this.form_name = null; // В режиме редактирования не сохраняем данные
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleCloseForm = this.handleCloseForm.bind(this);
    }

    componentWillMount() {
        this.getFormFromLocalStorage();
        (this.props.data && this.props.editMode) && this.getDataFromProps();
    }

    componentDidMount() {
        if (!this.props.is_nested) {
            this.getCsrf()
                .then(() => this.rememberState());
        }
    }

    componentWillReceiveProps(nextProps) {
        const { form_server_errors, clear_local_storage } = nextProps;

        if (clear_local_storage !== this.props.clear_local_storage) {
            this.clearFormLocalStorage();
        }

        form_server_errors && this.SetControlsErrorFromUpperForm(this.props.form_server_errors, form_server_errors);
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    getDataFromProps() {
        const { id, last_name, first_name, secondary_name, birth_date } = this.props.data;
        this.setState({
            id,
            form: {
                ...this.state.form,
                last_name,
                first_name,
                secondary_name,
                birth_date
            }
        }, () => this.rememberState());
    }

    handleSubmit(e) {
        e.preventDefault();
        const { form_isValid } = this.state;

        if (form_isValid && !this.props.is_nested) {
            this.toggleIsLoading();
            customFetch(this.props.editMode ? `${URLS.NATURAL_PERSON.SINGLE}/${this.state.id}` : URLS.NATURAL_PERSON.SINGLE, {
                method: 'POST',
                body: JSON.stringify(this.state.form)
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.clearFormLocalStorage();

                        const is_after_submit = typeof this.props.afterSubmit === 'function';
                        !is_after_submit && this.toggleIsLoading();
                        is_after_submit && this.props.afterSubmit();
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                })
        }
    }

    handleDelete() {
        const { id } = this.props.data;
        this.toggleIsLoading('form_isDeleting');
        customFetch(`/natural-person/${id}`, {
            method: 'DELETE'
        })
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const is_after_delete = typeof this.props.afterDelete === 'function';
                    !is_after_delete && this.toggleIsLoading('form_isDeleting');
                    is_after_delete && this.props.afterDelete();
                } else if (data.status === 'ERROR') {
                    this.toggleIsLoading('form_isDeleting');
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.toggleIsLoading('form_isDeleting');
            });
    }

    handleCloseForm() {
        this.props.handleClose && this.props.handleClose();
        this.props.slideUp && this.props.slideUp();
    }

    render() {
        const { isClosable, editMode, is_nested } = this.props;
        const { form_isValid, form_isLoading, form_isSubmitting, form_isDeleting } = this.state;
        const { last_name, first_name, secondary_name, birth_date } = this.state.form;

        const formTitle = `${editMode ? 'Редактирование' : 'Добавление'} физического лица`;
        const buttonText = editMode ? 'Сохранить' : 'Добавить';

        const SubmitButtonClassName = classNames({
            'ButtonApply margin-right': true,
            'Preloader Preloader_light': form_isSubmitting
        });

        const DeleteButtonClassName = classNames({
            'ButtonDelete': true,
            'Preloader Preloader_light': form_isDeleting
        });

        return (
            <div className="PageForm-Container">
                <div className="FormLabelTop form-default" id='form_natural_person'>
                    {
                        isClosable &&
                        <div className="ButtonClose" onClick={this.handleCloseForm} />
                    }
                    <div className="row nested-row">
                        <div className="col-xl-12 col-sm-4">
                            <h3 className="TitleBlue">{formTitle}</h3>
                        </div>
                    </div>
                    <div className="row nested-row">
                        <div className="col-xl-6 col-sm-4">
                            <LastName
                                label="Фамилия:"
                                placeholder="Введите фамилию"
                                name="last_name"
                                value_prop={last_name}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.last_name}
                            />
                        </div>
                        <div className="col-xl-6 col-sm-4">
                            <FirstName
                                label="Имя:"
                                placeholder="Введите имя"
                                name="first_name"
                                value_prop={first_name}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.first_name}
                            />
                        </div>
                    </div>
                    <div className="row nested-row">
                        <div className="col-xl-6 col-sm-4">
                            <SecondaryName
                                label="Отчество:"
                                name="secondary_name"
                                placeholder="Введите отчество"
                                value_prop={secondary_name}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.secondary_name}
                            />
                        </div>
                        <div className="col-xl-6 col-sm-4">
                            <BirthDate
                                label="Дата рождения:"
                                name="birth_date"
                                placeholder="дд.мм.гггг"
                                handleComponentChange={this.handleChange}
                                value_prop={birth_date}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.birth_date}
                            />
                        </div>
                    </div>
                    <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                    {
                        !is_nested &&
                        <div className="ButtonsRow row nested-row">
                            <div className="col-xl-6 col-sm-4">
                                <button onClick={this.handleSubmit} className={SubmitButtonClassName} disabled={!form_isValid || form_isLoading} data-ripple-button="">
                                    <span className="ripple-text">{buttonText}</span>
                                </button>
                            </div>
                            <div className="col-xl-6 col-sm-4">
                                {
                                    editMode &&
                                    <button onClick={this.handleDelete} className={DeleteButtonClassName} disabled={form_isLoading} data-ripple-button="">
                                        <span className="ripple-text">Удалить</span>
                                    </button>
                                }
                            </div>
                        </div>
                    }
                </div>
            </div>
        );
    }
};
