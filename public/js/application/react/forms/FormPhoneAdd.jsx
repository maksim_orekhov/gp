import React from 'react';
import FormBase from './FormBase';
import Timer from '../Timer';
import Phone from '../../../landing/react/auth/forms/RegisterForm/controls/phone-input';
import ShowError from '../../../application/react/ShowError';
import URLS from '../constants/urls';
import { customFetch } from "../Helpers";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../constants/server_errors";

const classNames = require('classnames');

/**
 * Форма добавления телефона принимающая новый номер телефона
 */
export default class FormPhoneAdd extends FormBase {
    constructor(props) {
        super(props);

        /**
         * @param {string} csrf
         * @param {boolean} form_isLoading - флаг что идет запрос на сервер, запускающий прелоадер
         * @param {boolean} form_isValid
         * @param {string} formError - ошибка формы, полученная с бэка при отправке данных
         * @param {string} phone - телефон пользователя, получаемый из инпута формы
         * @param {number} request_period - минимальный интверал между двумя отправленными кодами, если в этот промежуток отправить еще один код то будет ошибка
         * @param {number} last_code_created_at - дата в секундах последнего отправленного кода
         * @param {number} timer_time - время через которое разблокируется отправка следующего кода
         * @param {boolean} is_allow_to_send_code - доступна ли кнопка изменения телефона
         * @param {boolean} is_limit_phone_code_change - говорит о том что был достигнут лимит на коды подтверждения
         */
        this.state = {
            formError: '',
            form_isLoading: false,
            form_isValid: false,
            request_period: null,
            last_code_created_at: null,
            timer_time: null,
            is_allow_to_send_code: true,
            is_limit_phone_code_change: false,
            form_preloader: false,
            // Validation props
            validation: {
                phone_is_valid: false
            },
            // -----------------------------
            form: {
                phone: '',
                csrf: '',
            },
            controls_server_errors: {
                csrf: null,
                phone: null
            },
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS
        };

        // Поля контролов для автоматической валидации формы
        this.validation_props =
            [
                'phone_is_valid'
            ];

        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.toggleTimer = this.toggleTimer.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    }

    componentWillMount() {
        this.formInit();
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    formInit() {
        customFetch(URLS.PROFILE)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const {
                        csrf
                    } = data.data;

                    // Так как вложенная структура стейта используем rest оператор как аналог immutable библиотеке
                    const form = {
                        ...this.state.form,
                        csrf
                    };


                    this.setState({ form});
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                }
            })
            .catch(() => this.setFormError('form_init_fail', true));
    }

    /**
     * Проверяет сначала не достигнут ли лимит кодов и если необходимое время не прошло с момента отправки последнего кода и если не прошло то запускает таймер обратного отсчета
     */
    checkAllowingToSendCodeTimeOut() {
        const { request_period, last_code_created_at } = this.state;
        const now = Date.now() / 1000; // Время сейчас в секундах
        const timer_time = last_code_created_at + request_period - now;

        if (timer_time > 0) {
            this.setState({
                is_allow_to_send_code: false,
                timer_time: Math.round(timer_time)
            });
        }
    }

    handleSubmit() {
        const { phone, csrf } = this.state.form;

        if (this.state.form_isValid) {
            this.toggleIsLoading();
            customFetch(URLS.PHONE.CREATE, {
                method: 'POST',
                body: JSON.stringify({
                    csrf,
                    phone: phone
                })
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        const { handleNextStep } = this.props;

                        handleNextStep && handleNextStep('add_phone');
                        handleNextStep && this.toggleFormPreloader();
                        this.toggleIsLoading();

                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data);
                        this.toggleIsLoading();
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    toggleTimer(isTimer) {
        this.setState({
            is_allow_to_send_code: isTimer
        });
    }

    toggleFormPreloader() {
        this.setState({
            form_preloader: !this.state.form_preloader
        });
    }

    render() {
        const { formError, form_isLoading, form_isValid, is_allow_to_send_code, timer_time, is_limit_phone_code_change, form_preloader } = this.state;

        const SubmitButtonClassName = classNames({
            'FormVerification-Button ButtonApply': true,
            'Preloader Preloader_light': form_isLoading
        });

        return (
            <div className={"FormLabelTop" + (form_preloader ? ' Preloader Preloader_solid' : '')}>
                <button className="ButtonClose js-verification-close" />
                <h2 className="FormVerification-Title">Добавить номер телефона</h2>
                {
                    !is_limit_phone_code_change ?
                        <div>
                            <div className='Input-Wrapper'>
                                <Phone
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleComponentValid}
                                    validateOnChange={true}
                                    name="phone"
                                    form_control_server_errors={this.state.controls_server_errors.phone}
                                    componentValue={this.handleChange}
                                    register={false}
                                />
                            </div>
                            {
                                is_allow_to_send_code
                                    ?
                                    <button
                                        className={SubmitButtonClassName}
                                        disabled={!form_isValid || form_isLoading}
                                        onClick={this.handleSubmit}
                                        data-ripple-button="">
                                        <span className="ripple-text">Добавить</span>
                                    </button>
                                    :
                                    <p>
                                        Вы сможете изменить номер телефона через <span className="TextAccent"> {/* span на этой строке чтобы сохранить пробел*/}
                                        <Timer
                                            start={timer_time}
                                            expired={this.toggleTimer}
                                            isShowTime={true}
                                        />
                                        </span> секунд.
                                    </p>
                            }
                            <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                            {/*{*/}
                            {/*formError.length !== 0 &&*/}
                            {/*<div className="FormErrors">*/}
                            {/*<p>{formError}</p>*/}
                            {/*</div>*/}
                            {/*}*/}
                        </div>
                        :
                        <p className="TextImportant">Следующий запрос на смену телефона можно будет сделать через 15 минут.</p>
                }
            </div>
        )
    }
}