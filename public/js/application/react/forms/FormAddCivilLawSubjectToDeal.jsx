import React from 'react';
import ControlCivilLawSubject from '../controls/ControlCivilLawSubject';
import ShowError from '../../../application/react/ShowError';
import FormBase from './FormBase';
import FormDeal from './formsDeal/FormDeal';
import Delivery from './FormDelivery';
import URLS from '../constants/urls';
import { getIdFromUrl, customFetch } from '../Helpers';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../constants/server_errors';
import REQUEST_CODES from '../constants/request_codes';

export default class FormAddCivilLawSubjectToDeal extends FormDeal {
    constructor(props) {
        super(props);
        this.state = {
            controls_server_errors: {
                civil_law_subject: null,
                civil_law_subject_form: null
            },
            // SERVER_ERRORS
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,

            form: {
                civil_law_subject: '',
                csrf: '',
                delivery: {
                    service_type: '',
                    service: {},
                    service_dpd: {
                        payment_type: 'ОУО',
                        service_code: 'PCL',
                        parcel_quantity: 1,
                        delivery_time_period: '9-18',
                        pickup_time_period: '9-18',
                        cargo_category: 'Товары народного потребления (без ГСМ и АКБ)',
                    },
                    terms_of_delivery: ''
                }
            },
            is_need_to_fill_delivery: false,
            is_need_to_fill_civil_law_subject: false,
            validation: {
                civil_law_subject_is_valid: false,
                civil_law_subject_form_is_valid: false,
                delivery_is_valid: false
            },
            civil_law_subject_form_data: {},
            form_isValid: false,
            form_isSubmitting: false,
            form_isPreloader: false,
            update_collection_civil_law_subjects: 0,
            natural_persons: [],
            legal_entities: [],
            sole_proprietors: [],
            clear_local_storage: 0,
            delivery_service_types: {}
        };

        this.form_name = null;

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleStateChange = this.handleStateChange.bind(this);
        this.handleChangeDelivery = this.handleChangeDelivery.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        this.formInit();
    }

    formInit() {
        const idDeal = getIdFromUrl();
        this.toggleIsLoading('form_isPreloader');
        customFetch(`${URLS.DEAL.FORM_INIT}?idDeal=${idDeal}`)
            .then(data => {
                console.log('formInit', data);
                if (data.status === 'SUCCESS') {
                    const { naturalPersons, legalEntities, soleProprietors, csrf, deal_form, delivery_service_types } = data.data;
                    const { is_need_to_fill_delivery = false, is_need_to_fill_civil_law_subject = false } = data.data.deal;
                    const { deal_role = false } = deal_form;
                    let delivery = {};
                    if (deal_form.delivery) {
                        delivery = {...deal_form.delivery};
                        delete deal_form.delivery;

                        delivery = this.getInitDeliveryData(delivery, delivery_service_types);
                    } else {
                        delete deal_form.delivery;
                    }

                    if (delivery) {
                        delivery = {
                            ...this.state.form.delivery,
                            ...delivery,
                            service: {
                                ...this.state.form.delivery.service,
                                ...delivery.service,
                            },
                            service_dpd: {
                                ...this.state.form.delivery.service_dpd,
                                ...delivery.service_dpd
                            }
                        };
                    }

                    this.setState({
                        natural_persons: naturalPersons || [],
                        legal_entities: legalEntities || [],
                        sole_proprietors: soleProprietors || [],
                        form: {
                            ...this.state.form,
                            delivery,
                            csrf
                        },
                        is_need_to_fill_delivery,
                        is_need_to_fill_civil_law_subject,
                        delivery_service_types,
                        deal_role,
                        update_collection_civil_law_subjects: this.state.update_collection_civil_law_subjects + 1
                    }, () => {
                        this.toggleIsLoading('form_isPreloader');
                    });
                } else if (data.status === 'ERROR') {
                    this.TakeApartErrorFromServer(data);
                    this.toggleIsLoading('form_isPreloader');
                }
            })
            .catch(() => {
                this.setFormError('form_init_fail', true);
                this.toggleIsLoading('form_isPreloader');
            });
    }

    handleChangeDelivery(data_to_state_form) {
        this.setState({
            form: {
                ...this.state.form,
                delivery: {
                    ...data_to_state_form
                }
            },
        });
    }

    checkAllControlsAreValid() {
        const { civil_law_subject_form_is_valid, civil_law_subject_is_valid, delivery_is_valid } = this.state.validation;
        const { is_need_to_fill_delivery } = this.state;

        const form_isValid = (
            (civil_law_subject_form_is_valid || civil_law_subject_is_valid)
            && (!is_need_to_fill_delivery || delivery_is_valid)
        );

        if (this.state.form_isValid === form_isValid) return;

        this.setState({
            form_isValid
        });
    }

    getSubmitUrl() {
        const deal_id = getIdFromUrl();

        return `${URLS.DEAL.SINGLE}/${deal_id}/civil-law-subject`;
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.form_isValid) {
            this.toggleIsLoading();

            customFetch(this.getSubmitUrl(), {
                method: 'POST',
                body: JSON.stringify(this.getDataFromState())
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        this.setState({
                            clear_local_storage: this.state.clear_local_storage + 1
                        });
                        window.location.reload();
                    } else if (data.status === 'ERROR') {
                        this.toggleIsLoading();
                        this.TakeApartErrorFromServer(data);
                        if (data.code === REQUEST_CODES.DEAL_WITH_SUCH_ID_NOT_FOUND) {
                            window.location = URLS.DEAL.SINGLE;
                        }
                    }
                })
                .catch(() => {
                    this.setFormError('critical_error', true);
                    this.toggleIsLoading();
                });
        }
    }

    getDeliverySubmitData(data) {
        const new_delivery = {
            ...data,
            service: {
                ...data.service,
                ...data.service_dpd
            }
        };
        delete new_delivery['service_dpd'];

        return new_delivery;
    }

    getDataFromState() {
        const { civil_law_subject, csrf } = this.state.form;
        const delivery = this.getDeliverySubmitData(this.state.form.delivery);
        if (civil_law_subject) {
            return {
                ...this.state.form,
                delivery
            };
        } else {
            return {
                civil_law_subject: {
                    ...this.state.civil_law_subject_form_data,
                },
                delivery,
                csrf
            };
        }
    }

    TakeApartInvalidFormData(errors) {
        this.setFormError('invalid_form_data', true);

        if (errors) {
            const controls_server_errors = {...this.state.controls_server_errors};

            Object.keys(errors).forEach(control => {
                switch (control) {
                    case 'csrf':
                        this.setFormError('csrf', true);
                        break;
                    case 'deal_civil_law_subject':
                        if (errors[control].hasOwnProperty('civil_law_subject_type')) {
                            controls_server_errors['civil_law_subject'] = errors[control]['civil_law_subject_type'];
                        } else {
                            controls_server_errors['civil_law_subject_form'] = errors[control];
                        }
                        break;
                    default:
                        controls_server_errors[control] = errors[control]
                }
            });

            this.setState({
                controls_server_errors
            });
        }
    }

    render() {
        const {
            form_isSubmitting,
            form_isValid,
            natural_persons,
            legal_entities,
            sole_proprietors,
            update_collection_civil_law_subjects,
            form_isPreloader,
            clear_local_storage,
            is_need_to_fill_delivery,
            is_need_to_fill_civil_law_subject,
            delivery_service_types,
            deal_role
        } = this.state;
        const { civil_law_subject } = this.state.form;
        return (
            <div className={`FormLabelTop ${form_isPreloader ? 'Preloader Preloader_solid' : ''}`}>
                {
                    is_need_to_fill_civil_law_subject &&
                    <ControlCivilLawSubject
                        label="Вы будете участвовать в сделке как:"
                        name="civil_law_subject"
                        value_prop={civil_law_subject}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        sendUpData={this.handleStateChange}
                        is_nested={true}
                        form_validate_name='civil_law_subject_form_is_valid'
                        form_data_name='civil_law_subject_form_data'
                        natural_persons={natural_persons}
                        legal_entities={legal_entities}
                        sole_proprietors={sole_proprietors}
                        update_collection={update_collection_civil_law_subjects}
                        form_control_server_errors={this.state.controls_server_errors.civil_law_subject}
                        form_server_errors={this.state.controls_server_errors.civil_law_subject_form}
                        clear_local_storage={clear_local_storage}
                    />
                }
                {
                    is_need_to_fill_delivery &&
                    <Delivery
                        is_nested={true}
                        delivery={this.state.form.delivery}
                        csrf={this.state.form.csrf}
                        delivery_service_types={delivery_service_types}
                        deal_role={deal_role}
                        handleComponentChange={this.handleChangeDelivery}
                        handleComponentValidation={this.handleComponentValid}
                    />
                }
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                <div className="row nested-row ButtonsRow">
                    <div className="col-xl-6 col-sm-4">
                        <button
                            className={form_isSubmitting ? 'ButtonApply Preloader Preloader_light' : 'ButtonApply'}
                            disabled={form_isSubmitting || !form_isValid}
                            onClick={this.handleSubmit}
                        >
                            Далее
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}