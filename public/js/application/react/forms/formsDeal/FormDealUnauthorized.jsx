import React from 'react';
import FormDeal from "./FormDeal";
import FormWrapper from '../../components/FormDealWrapper';
import DisplayNone from '../../components/DisplayNone';

import URLS from '../../constants/urls';
import { getReCaptchaSiteKey, getReCaptchaSrc } from "../../../../landing/react/Helpers";
import {customFetch} from "../../Helpers";
import ArchiveFormMessage from "../../components/ArchiveFormMessage";

import { DELIVERY_TYPES_NAMES } from '../../constants/names';

class FormDealUnauthorized extends FormDeal {
    constructor(props) {
        super(props);

        this.state = {
            ...this.state,
            controls_server_errors: {
                /**
                 * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
                 * @example
                 *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
                 **/
                deal_type: null,
                deal_role: null,
                addon_terms: null,
                amount: null,
                counteragent_email: null,
                agent_email: null,
                name: null,
                delivery_period: null,
                fee_payer_option: null,
                deal_confirm: false
            },
            form: {
                ...this.state.form,
                agent_email: ''
            },
            captcha_sitekey: '',
            validation: {
                agent_email_is_valid: false,
                deal_type_is_valid: false,
                deal_role_is_valid: false,
                name_is_valid: false,
                addon_terms_is_valid: true,
                amount_is_valid: false,
                fee_payer_option_is_valid: false,
                delivery_period_is_valid: false,
                counteragent_email_is_valid: false,
                deal_confirm_is_valid: false,
                delivery_is_valid: false
            },
        };

        this.form_name = 'form_deal_unauthorized';
    }

    componentWillMount() {
        const formHTML = document.getElementById('FormDealUnauthorized');
        if (formHTML) {
            formHTML.style.visibility = 'visible';
        }
        this.formInit();
    }

    componentDidMount() {
        this.renderReCaptcha();
    }

    renderReCaptcha() {
        this.setState({
            captcha_sitekey: getReCaptchaSiteKey()
        }, () => {
            const captcha_script = document.createElement("script");
            const element_before_script = document.getElementById("captcha_unregistered_deal");

            captcha_script.src = getReCaptchaSrc();
            element_before_script.parentNode.insertBefore(captcha_script, element_before_script.nextSibling);
        });
    }


    successAjaxFormInit(data) {
        console.log('formInit', data);
        const { deal_types = {}, fee_payer_options = [], csrf, max_deal_amount, delivery_service_types } = data.data;
        const form = {...this.state.form};
        const { form_template, form_type } = this.props;

        form['csrf'] = csrf;

        let data_to_state = {
            deal_types,
            fee_payer_options,
            delivery_service_types: delivery_service_types || {},
            form_template: form_template || this.state.form_template,
            max_deal_amount,
            form: {
                ...form
            }
        };

        const current_url = window.location.hash;
        if (current_url) {
            data_to_state['form']['deal_role'] = this.getCustomDealRole(current_url);
        }

        data_to_state = this.setFormDefaultValues(data_to_state);

        if (form_type) {
            data_to_state = this.setCustomDataToState(data_to_state);
        }

        this.oldState = {...this.state.form, ...data_to_state.form};

        this.setState(data_to_state, () => {
            this.getFormFromLocalStorage();
            this.toggleIsLoading('form_isPreloader');
        });
    }

    handleSubmit() {
        if (this.state.form_isValid) {
            this.toggleIsLoading();

            customFetch(this.getSubmitUrl(), {
                method: 'POST',
                body: this.getDataFromState(),
                processData: false, //Это важный параметр, без него не сработает
                contentType: false
            })
                .then(data => {
                    this.toggleIsLoading();
                    if (data.status === 'SUCCESS') {
                        this.successAjaxSubmit(data);
                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data);
                        if (grecaptcha) {
                            grecaptcha.reset();
                        }
                    }
                })
                .catch(response => {
                    console.log(response);
                    this.toggleIsLoading();
                    this.setFormError('critical_error', true);
                    if (grecaptcha) {
                        grecaptcha.reset();
                    }
                })
        }
    }

    getDataFromState() {
        const {
            csrf,
            deal_type,
            deal_role,
            name,
            addon_terms,
            amount,
            fee_payer_option,
            delivery_period,
            counteragent_email,
            agent_email,
            files,
            delivery
        } = this.state.form;

        const captcha = grecaptcha.getResponse();

        const data = {
            csrf,
            deal_type,
            deal_role,
            name,
            addon_terms,
            amount,
            fee_payer_option,
            delivery_period,
            counteragent_email,
            agent_email,
            captcha,
            delivery
        };

        this.setDeliverySubmitData(data);

        return this.createFormData(data, files);
    }

    getSubmitUrl() {
        return URLS.DEAL.CREATE_UNREGISTERED;
    }

    afterSubmitFormDeal() {
        window.location = '/deal/successful-created';
    }

    checkAllControlsAreValid() {
        const {
            deal_type_is_valid,
            deal_role_is_valid,
            name_is_valid,
            addon_terms_is_valid,
            amount_is_valid,
            fee_payer_option_is_valid,
            delivery_period_is_valid,
            counteragent_email_is_valid,
            deal_confirm_is_valid,
            agent_email_is_valid,
            delivery_is_valid
        } = this.state.validation;

        const deal_type_ident = this.getCurrentDealTypeIdent();

        const form_isValid = (
            deal_type_is_valid
            && deal_role_is_valid
            && name_is_valid
            && addon_terms_is_valid
            && amount_is_valid
            && fee_payer_option_is_valid
            && (deal_type_ident === 'U' || delivery_is_valid)
            && delivery_period_is_valid
            && agent_email_is_valid
            && counteragent_email_is_valid
            && deal_confirm_is_valid
        );

        if (this.state.form_isValid === form_isValid) return;

        this.setState({
            form_isValid
        });
    }

    getTemplates() {
        const { form_isPreloader, date_of_create, captcha_sitekey, formHasDataAtLocalStorage } = this.state;

        const controls = this.getControls();
        const delivery_type_name = this.getDeliveryServiceTypeName();
        const deal_type_ident = this.getCurrentDealTypeIdent();

        return (
            {
                base_form:
                    <FormWrapper is_loading={form_isPreloader} date_of_create={date_of_create} logo={controls.form_logo} mobile_title="Создание сделки" title="Создание сделки">
                        { formHasDataAtLocalStorage && <ArchiveFormMessage handleResetForm={this.resetForm} /> }
                        { controls.agent_email_control() }
                        { controls.deal_type_control() }
                        { controls.deal_role_control() }
                        { controls.deal_name_control() }
                        { controls.addon_terms_control() }
                        { controls.file_control() }
                        <div className="row">
                            <div className="col-xl-6 col-sm-4">
                                { controls.amount_control() }
                            </div>
                            { controls.fee_amount_control() }
                        </div>
                        <div className="Input-Group-Wrapper">
                            { controls.fee_payer_options_control() }
                            { controls.payment_hint_control() }
                        </div>
                        {
                            deal_type_ident === 'T' &&
                            controls.delivery_control()
                        }
                        {
                            delivery_type_name !== DELIVERY_TYPES_NAMES.DPD &&
                            <div className="row">
                                <div className="col-xl-6 col-sm-4">
                                    { controls.delivery_period_control() }
                                </div>
                            </div>
                        }
                        { controls.counteragent_email_control() }
                        { controls.deal_confirm_control }
                        <div className="captcha-unregistered-wrapper">
                            <div className="captcha-wrapper">
                                <div id="captcha_unregistered_deal" className="g-recaptcha" data-sitekey={captcha_sitekey}/>
                                <div className="captcha-overlay captcha-overlay_position-top"/>
                                <div className="captcha-overlay captcha-overlay_position-right"/>
                                <div className="captcha-overlay captcha-overlay_position-bottom"/>
                                <div className="captcha-overlay captcha-overlay_position-left"/>
                                <div className="captcha-image"/>
                                <div className="captcha-links">
                                    <a href="https://www.google.com/intl/ru/policies/privacy/"target="_blank">Конфиденциальность</a>
                                    <span aria-hidden="true" role="presentation"> - </span>
                                    <a href="https://www.google.com/intl/ru/policies/terms/" target="_blank">
                                        Условия использования
                                    </a>
                                </div>
                            </div>
                        </div>
                        { controls.form_errors_control }
                        { controls.buttons_control() }
                    </FormWrapper>,
                form_with_role_and_type:
                    <FormWrapper
                        is_loading={form_isPreloader}
                        date_of_create={date_of_create}
                        logo={controls.form_logo}
                        mobile_title={this.getFormWithRoleAndTypeTitle()}
                    >
                        <DisplayNone>
                            { controls.deal_type_control() }
                            { controls.deal_role_control() }
                        </DisplayNone>
                        {
                            controls.form_title_control({
                                title: this.getFormWithRoleAndTypeTitle()
                            })
                        }
                        { controls.agent_email_control() }
                        { controls.deal_name_control() }
                        { controls.addon_terms_control() }
                        { controls.file_control() }
                        <div className="row">
                            <div className="col-xl-6 col-sm-4">
                                { controls.amount_control() }
                            </div>
                            { controls.fee_amount_control() }
                        </div>
                        <div className="Input-Group-Wrapper">
                            { controls.fee_payer_options_control() }
                            { controls.payment_hint_control() }
                        </div>
                        {
                            deal_type_ident === 'T' &&
                            controls.delivery_control()
                        }
                        {
                            delivery_type_name !== DELIVERY_TYPES_NAMES.DPD &&
                            <div className="row">
                                <div className="col-xl-6 col-sm-4">
                                    { controls.delivery_period_control() }
                                </div>
                            </div>
                        }
                        { controls.counteragent_email_control() }
                        { controls.deal_confirm_control }
                        <div className="captcha-unregistered-wrapper">
                            <div className="captcha-wrapper">
                                <div id="captcha_unregistered_deal" className="g-recaptcha" data-sitekey={captcha_sitekey}/>
                                <div className="captcha-overlay captcha-overlay_position-top"/>
                                <div className="captcha-overlay captcha-overlay_position-right"/>
                                <div className="captcha-overlay captcha-overlay_position-bottom"/>
                                <div className="captcha-overlay captcha-overlay_position-left"/>
                            </div>
                        </div>
                        { controls.form_errors_control }
                        { controls.buttons_control() }
                    </FormWrapper>
            }
        )
    }

    render() {
        const { form_template } = this.state;

        const templates = this.getTemplates();

        return (
            <div className="PageForm-Container_new-design">
                { templates[form_template] }
            </div>
        );
    }
}

export default FormDealUnauthorized;