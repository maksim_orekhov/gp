import React from 'react';
import FormDeal from './FormDeal';
import FormWrapper from '../../components/FormDealWrapper';
import DisplayNone from '../../components/DisplayNone';
import Files from "../../controls/ControlDealFiles";

import URLS from '../../constants/urls';
import { getLastUrlPart, customFetch, getPhoneInMaskFormat } from '../../Helpers';
import { DELIVERY_TYPES_NAMES } from '../../constants/names';


export default class FormPartnerDeal extends FormDeal {
    constructor(props) {
        super(props);

        this.state = {
            ...this.state,
            controls_server_errors: {
                ...this.state.controls_server_errors,
                agent_email: null
            },
            // Validation props
            validation: {
                ...this.state.validation,
                agent_email_is_valid: false
            },
            // -----------------------------
            form: {
                ...this.state.form,
                agent_email: ''
            },
            // Контролы заполненные из токена и следовательные их изменять нельзя
            disabled_controls: {
                ...this.state.disabled_controls,
                agent_email: false
            },
            is_user_authorized: false,
            counteragent_name: '', // Имя партнера который приглашает к сделке
            partner_ident: 'base_form', // Тип тэмплейта для отображения формы
        };

        this.form_name = null;

        this.getDataFromState = this.getDataFromState.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount() {
        const formHTML = document.getElementById('FormPartnerDeal');
        if (formHTML) {
            formHTML.style.visibility = 'visible';
        }
        this.formInit();
    }

    getFormInitUrl() {
        const counteragent_token = getLastUrlPart();

        return `${URLS.DEAL.FORM_INIT}?token_key=${counteragent_token}`;
    }

    successAjaxFormInit(data) {
        console.log('formInit', data);
        const {
            deal_types = {},
            fee_payer_options = [],
            delivery_service_types = {},
            csrf,
            naturalPersons,
            legalEntities,
            soleProprietors,
            counteragent_token_data,
            is_user_authorized,
            max_deal_amount
        } = data.data;

        const user = data.data.user || {};

        const form = {...this.state.form};

        form['csrf'] = csrf;

        let data_to_state = {
            deal_types,
            fee_payer_options,
            delivery_service_types: delivery_service_types || {},
            natural_persons: naturalPersons || [],
            legal_entities: legalEntities || [],
            sole_proprietors: soleProprietors || [],
            update_collection_civil_law_subjects: this.state.update_collection_civil_law_subjects + 1,
            is_user_authorized,
            max_deal_amount,
            form: {
                ...form
            },
            user_email: user.email || '',
            user_phone: getPhoneInMaskFormat(user.phone)
        };

        // Ставим дефолтные значения формы
        data_to_state = this.setFormDefaultValues(data_to_state);

        // Заполняем данные сделки из токена контрагента
        if (counteragent_token_data) {
            data_to_state = this.formTakeApartTokenData(counteragent_token_data, data_to_state);
        }

        this.setState(data_to_state, () => {
            this.toggleIsLoading('form_isPreloader');
        });
    }

    handleSubmit() {
        if (this.state.form_isValid) {
            this.toggleIsLoading();

            customFetch(this.getSubmitUrl(), {
                method: 'POST',
                body: this.getDataFromState(),
                processData: false, //Это важный параметр, без него не сработает
                contentType: false
            })
                .then(data => {
                    this.toggleIsLoading();
                    if (data.status === 'SUCCESS') {
                        this.successAjaxSubmit(data);
                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(response => {
                    console.log(response);
                    this.toggleIsLoading();
                    this.setFormError('critical_error', true);
                })
        }
    }

    formTakeApartTokenData(counteragent_token_data, data_to_state) {
        const state_form = {...this.state.form}; // для сверки полей токена и формы
        const form_data = {};
        const disabled_controls = {};

        const { counteragent_name = '', partner_ident } = counteragent_token_data;

        for (let key in counteragent_token_data) {
            if (!counteragent_token_data[key] || !counteragent_token_data.hasOwnProperty(key)) continue;

            if (state_form.hasOwnProperty(key)) {
                form_data[key] = counteragent_token_data[key];
                disabled_controls[key] = true;
            } else if (key === 'deal_name') {
                //Из-за различий наименований полей на бэке
                form_data['name'] = counteragent_token_data[key];
                disabled_controls['name'] = true;
            } else if (key === 'deal_description') {
                //Из-за различий наименований полей на бэке
                form_data['addon_terms'] = counteragent_token_data[key];
                disabled_controls['addon_terms'] = true;
            }
        }

        return {
            ...data_to_state,
            disabled_controls: {
                ...this.state.disabled_controls,
                ...disabled_controls
            },
            counteragent_name,
            partner_ident: partner_ident || this.state.partner_ident,
            form: {
                ...data_to_state.form,
                ...form_data
            }
        };
    }

    checkAllControlsAreValid() {
        const {
            deal_type_is_valid,
            deal_role_is_valid,
            name_is_valid,
            addon_terms_is_valid,
            amount_is_valid,
            fee_payer_option_is_valid,
            delivery_period_is_valid,
            deal_civil_law_subject_is_valid,
            payment_method_is_valid,
            deal_confirm_is_valid,
            civil_law_subject_form_is_valid,
            payment_method_form_is_valid,
            delivery_is_valid,
            agent_email_is_valid,
            files_is_valid
        } = this.state.validation;
        const { is_user_authorized, partner_ident } = this.state;

        const deal_type_ident = this.getCurrentDealTypeIdent();

        const form_isValid = (
            deal_type_is_valid
            && (is_user_authorized || agent_email_is_valid) // Если авторизован то валиден
            && deal_role_is_valid
            && name_is_valid
            && addon_terms_is_valid
            && amount_is_valid
            && fee_payer_option_is_valid
            && delivery_period_is_valid
            && deal_confirm_is_valid
            && (deal_type_ident === 'U' || delivery_is_valid)
            && (deal_civil_law_subject_is_valid || civil_law_subject_form_is_valid) // Если не авторизован то валидно
            && (partner_ident !== 'lst_arenda' || files_is_valid) // Для лст_аренда поле файлов обязательно, для всех остальных нет
            /*&& (!is_user_authorized || deal_civil_law_subject_is_valid || civil_law_subject_form_is_valid) // Если не авторизован то валидно
            && (
                !is_user_authorized || // Если не авторизован то валидно
                (
                    this.state.form.deal_role === 'contractor'
                    && ( payment_method_is_valid || payment_method_form_is_valid )
                )
                || (this.state.form.deal_role === 'customer')
            )*/
        );

        if (this.state.form_isValid === form_isValid) return;

        this.setState({
            form_isValid
        });
    }

    getDataFromState() {
        const { deal_civil_law_subject, deal_role, payment_method } = this.state.form;
        const data = {...this.state.form};
        delete data.files;

        const files = this.state.form.files;


        const { is_user_authorized } = this.state;

        /*if (is_user_authorized) {
            if (!deal_civil_law_subject) {
                form_data['deal_civil_law_subject'] = this.state.civil_law_subject_form_data;
            }
            if (deal_role === 'contractor' && !payment_method) {
                form_data['payment_method'] = this.state.payment_method_form_data;
            }
        }*/

        if (!deal_civil_law_subject) {
            data['deal_civil_law_subject'] = this.state.civil_law_subject_form_data;
        }

        this.setDeliverySubmitData(data);

        return this.createFormData(data, files);
    }

    getInvitationTitleControl() {
        return (
            <div className="col-xl-12">
                <p className="invitation-title">
                    Вы участвуете в сделке с <span className="invitation-title__name">{this.state.counteragent_name || 'приглашением'}</span>
                </p>
            </div>
        );
    }

    getLstLogo() {
        return (
            <div className="Logo">
                <div className="Logo__main">
                    <div className="Logo-Img">
                        <img src="../../../../img/application/lst-arenda-logo.svg" alt="ЛСТ Аренда"/>
                    </div>
                    <div className="Logo__company-name">Аренда спецтехники</div>
                </div>
                <div className="Logo__company-requisites">
                    <p>ООО «ЛСТ-АРЕНДА»,  ОГРН 1157847232223, ИНН  784 003 6118, КПП  784001001</p>
                    <p>191002, Санкт-Петербург, ул. Достоевского, д. 24/9, ЛИТ. А, ПОМ. 3Н</p>
                </div>
            </div>
        )
    }

    getLTSFilesControl(props) {
        return (
            <Files
                name_first="file_contract"
                name_second="file_request"
                placeholder_first="Загрузить договор"
                placeholder_second="Загрузить заявку"
                download_link_text_first="Скачать типовой договор"
                download_link_text_second="Скачать бланк заявки"
                label_first="Для создания сделки загрузите заполненную заявку и договор."
                name="files"
                handleComponentChange={this.handleChange}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.files}
                {...props}
            />
        )
    }

    // Функция над getControls для того чтобы добавить дополнительные обертки над ними
    getCustomControls() {
        const controls = this.getControls();

        return {
            ...controls,
            invitation_title_control: this.getInvitationTitleControl(),
            lst_logo: this.getLstLogo(),
            lts_files_control: (props) => this.getLTSFilesControl(props)
        }
    }


    getTemplates() {
        const { form_isPreloader, date_of_create, is_user_authorized } = this.state;
        const controls = this.getCustomControls();
        const deal_type_ident = this.getCurrentDealTypeIdent();
        const delivery_type_name = this.getDeliveryServiceTypeName();

        return (
            {
                base_form:
                    <FormWrapper
                        is_loading={form_isPreloader}
                        date_of_create={date_of_create}
                        logo={controls.form_logo}
                        mobile_title="Создание безопасной сделки"
                        title="Создание безопасной сделки"
                    >
                        { controls.invitation_title_control }
                        {
                            !is_user_authorized &&
                            controls.agent_email_control()
                        }
                        { controls.deal_type_control() }
                        { controls.deal_role_control() }
                        { controls.deal_name_control() }
                        { controls.addon_terms_control() }
                        { controls.file_control() }
                        <div className="row">
                            <div className="col-xl-6 col-sm-4">
                                { controls.amount_control() }
                            </div>
                            { controls.fee_amount_control() }
                        </div>
                        <div className="Input-Group-Wrapper">
                            { controls.fee_payer_options_control() }
                            { controls.payment_hint_control() }
                        </div>
                        {
                            deal_type_ident === 'T' &&
                            controls.delivery_control()
                        }
                        {
                            delivery_type_name !== DELIVERY_TYPES_NAMES.DPD &&
                            <div className="row">
                                <div className="col-xl-6 col-sm-4">
                                    { controls.delivery_period_control() }
                                </div>
                            </div>
                        }
                        { controls.civil_law_subject_control() }
                        { controls.deal_confirm_control }
                        { controls.form_errors_control }
                        { controls.buttons_control() }
                    </FormWrapper>,
                lst_arenda:
                    <FormWrapper
                        is_loading={form_isPreloader}
                        date_of_create={date_of_create}
                        logo={controls.lst_logo}
                        is_partner_form={true}
                        mobile_title="Создание безопасной сделки"
                        title="Создание безопасной сделки"
                    >
                        <DisplayNone>
                            { controls.deal_type_control() }
                            { controls.addon_terms_control() }
                            { controls.fee_payer_options_control() }
                            { controls.deal_role_control() }
                        </DisplayNone>

                        {
                            !is_user_authorized &&
                            controls.agent_email_control()
                        }
                        {
                            controls.deal_name_control({
                                label: 'Название сделки:',
                                placeholder: 'Введите название'
                            })
                        }
                        { controls.lts_files_control() }
                        <div className="row">
                            <div className="col-xl-6 col-sm-4">
                                {
                                    controls.amount_control({
                                        label: 'Цена аренды (из заявки):',
                                        placeholder: 'Введите цену'
                                    })
                                }
                            </div>
                            <div className="col-xl-6 col-sm-4">
                                {
                                    controls.delivery_period_control({
                                        label: 'Срок исполнения услуги (дней):'
                                    })
                                }
                            </div>
                        </div>
                        { controls.civil_law_subject_control() }
                        { controls.deal_confirm_control }
                        { controls.form_errors_control }
                        { controls.buttons_control() }
                    </FormWrapper>
            }
        );
    }

    render() {
        const templates = this.getTemplates();
        const partner_ident = this.state.partner_ident;

        return (
            <div className="PageForm-Container_new-design">
                { templates[partner_ident] || templates['base_form'] }
            </div>
        );
    }
}