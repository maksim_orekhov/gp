import React from 'react';
import FormDeal from './FormDeal';
import FormWrapper from '../../components/FormDealWrapper';

import URLS from '../../constants/urls';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../../constants/server_errors';
import { DELIVERY_TYPES_NAMES } from '../../constants/names';
import { getIdFromUrl, getDateNowInPrettyFormat } from '../../Helpers';


export default class FormDealEdit extends FormDeal {
    constructor(props) {
        super(props);

        this.state = {
            ...this.state,
            is_owner: true, // Для того чтобы при редактировании сделки скрывать некоторые поля если ты не владелец сделки
            date_of_create: '',
        };

        this.form_name = null; // название формы для хранения данных в local_storage
    }

    componentWillMount() {
        const formHTML = document.getElementById('FormDealEdit');
        if (formHTML) {
            formHTML.style.visibility = 'visible';
        }
        this.formInit();
    }

    getFormInitUrl() {
        const deal_id = getIdFromUrl();
        // Проверяем если у сделки в state ее id, если есть то делаем запрос с учетом id сделки.
        return `${URLS.DEAL.FORM_INIT}?idDeal=${deal_id}`;
    }

    successAjaxFormInit(data) {
        console.log('formInit', data);
        const {
            deal_types = {},
            fee_payer_options = [],
            deal_form,
            csrf,
            naturalPersons,
            legalEntities,
            soleProprietors,
            delivery_service_types,
            max_deal_amount,
            deal
        } = data.data;
        const { fee_amount } = data.data.deal;
        const form = {...this.state.form};
        const { update_collection_civil_law_subjects } = this.state;

        const deal_id = getIdFromUrl();

        form['csrf'] = csrf;
        form['fee_amount'] = fee_amount;

        let data_to_state = {
            deal_id,
            deal_types,
            fee_payer_options,
            natural_persons: naturalPersons || [],
            legal_entities: legalEntities || [],
            sole_proprietors: soleProprietors || [],
            delivery_service_types: delivery_service_types || {},
            update_collection_civil_law_subjects: update_collection_civil_law_subjects + 1,
            max_deal_amount
        };

        // Данные о текущей сделки
        // - доставка
        let delivery;
        if (deal_form.delivery) {
            delivery = {...deal_form.delivery};
            delete deal_form.delivery;

            delivery = this.getInitDeliveryData(delivery, delivery_service_types);
        } else {
            delete deal_form.delivery; // Костыль для старых сделок в которых нет delivery
        }


        if (delivery && delivery.hasOwnProperty('service_type') &&
            this.getDeliveryServiceTypeName(delivery.service_type, delivery_service_types) === DELIVERY_TYPES_NAMES.DPD &&
            deal && typeof deal === 'object'
            && deal.hasOwnProperty('delivery') && deal.delivery && typeof deal.delivery === 'object'
            && deal.delivery.hasOwnProperty('service') && deal.delivery.service && typeof deal.delivery.service === 'object'
        ) {
            this.setDpdDeliveryExtendedData(deal.delivery.service, delivery, deal_form.deal_role);
        }

        if (deal_form) {
            data_to_state = {
                ...data_to_state,
                date_of_create: getDateNowInPrettyFormat(deal_form.created),
                is_owner: deal_form.is_owner,
                form: {
                    ...form,
                    ...deal_form
                }
            };

            if (delivery) {
                data_to_state.form.delivery = {
                    ...this.state.form.delivery,
                    ...delivery,
                    service: {
                        ...this.state.form.delivery.service,
                        ...delivery.service,
                    },
                    service_dpd: {
                        ...this.state.form.delivery.service_dpd,
                        ...delivery.service_dpd
                    }
                };
            }
        }

        this.setState(data_to_state, () => {
            this.toggleIsLoading('form_isPreloader');
        });
    }

    checkAllControlsAreValid() {
        const {
            deal_type_is_valid,
            deal_role_is_valid,
            name_is_valid,
            addon_terms_is_valid,
            amount_is_valid,
            fee_payer_option_is_valid,
            delivery_period_is_valid,
            deal_civil_law_subject_is_valid,
            counteragent_email_is_valid,
            payment_method_is_valid,
            civil_law_subject_form_is_valid,
            payment_method_form_is_valid,
            delivery_is_valid
        } = this.state.validation;
        const { is_owner } = this.state;

        const deal_type_ident = this.getCurrentDealTypeIdent();

        const form_isValid = (
            deal_type_is_valid
            && (deal_role_is_valid || !is_owner) // Если не владелец то поле скрыто и следовательно всегда валидно
            && name_is_valid
            && addon_terms_is_valid
            && amount_is_valid
            && fee_payer_option_is_valid
            && delivery_period_is_valid
            && (counteragent_email_is_valid || !is_owner) // Если не владелец то поле скрыто и следовательно всегда валидно
            && (deal_civil_law_subject_is_valid || civil_law_subject_form_is_valid)
            && (deal_type_ident === 'U' || delivery_is_valid)
            && (
                (
                    this.state.form.deal_role === 'contractor'
                    && ( payment_method_is_valid || payment_method_form_is_valid )
                )
                || (this.state.form.deal_role === 'customer')
            )
        );

        if (this.state.form_isValid === form_isValid) return;

        this.setState({
            form_isValid
        });
    }

    getButtonsControl(props) {
        const { form_isValid, form_isSubmitting } = this.state;
        return (
            <div className="ButtonsRow row">
                <div className="col-xl-6 col-sm-2 col-xs-4">
                    <button
                        onClick={this.handleSubmit}
                        className={`ButtonApply margin-right ${form_isSubmitting ? 'Preloader Preloader_light' : ''}`}
                        disabled={!form_isValid || form_isSubmitting}
                        data-ripple-button=""
                    >
                        <span className="ripple-text">Сохранить</span>
                    </button>
                </div>
                <div className="col-xl-6 col-sm-2 col-xs-4">
                    <button onClick={this.handleCancel} className="ButtonRefuse" data-ripple-button=""><span className="ripple-text">Отмена</span></button>
                </div>
            </div>
        );
    }

    render() {
        const { form_isPreloader, is_owner, date_of_create, fee_amount } = this.state;
        const { deal_role, deal_number } = this.state.form;

        const controls = this.getControls();
        const deal_type_ident = this.getCurrentDealTypeIdent();
        const delivery_type_name = this.getDeliveryServiceTypeName();

        return (
            <FormWrapper
                is_loading={form_isPreloader}
                date_of_create={date_of_create}
                logo={controls.form_logo}
                mobile_title={`Редактирование сделки ${deal_number}`}
                title={`Редактирование сделки ${deal_number}`}
            >
                { controls.deal_type_control() }
                {
                    is_owner &&
                    controls.deal_role_control()
                }
                { controls.deal_name_control() }
                { controls.addon_terms_control() }
                <div className="row">
                    <div className="col-xl-6 col-sm-4">
                        { controls.amount_control() }
                    </div>
                    { controls.fee_amount_control({fee_amount}) }
                </div>
                <div className="Input-Group-Wrapper">
                    { controls.fee_payer_options_control() }
                    { controls.payment_hint_control() }
                </div>
                {
                    deal_type_ident === 'T' &&
                    controls.delivery_control({
                        can_added_counteragent_data: false
                    })
                }
                {
                    delivery_type_name !== DELIVERY_TYPES_NAMES.DPD &&
                    <div className="row">
                        <div className="col-xl-6 col-sm-4">
                            { controls.delivery_period_control() }
                        </div>
                    </div>
                }
                {
                    deal_role === 'contractor' ?
                        controls.civil_law_subject_and_payment_method_control()
                        :
                        controls.civil_law_subject_control()
                }
                {
                    is_owner &&
                    controls.counteragent_email_control()
                }
                { controls.form_errors_control }
                { controls.buttons_control() }
            </FormWrapper>
        );

    }
}