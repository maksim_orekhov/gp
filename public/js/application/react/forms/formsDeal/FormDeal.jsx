import React from 'react';
import FormWrapper from '../../components/FormDealWrapper';
import DealType from '../../controls/ControlDealType';
import DealRole from '../../controls/ControlDealRole';
import DealName from '../../controls/ControlDealName';
import FeePayerOption from '../../controls/ControlFeePayerOption';
import Delivery from '../../controllers/ControllerDelivery';
import AddonTerms from '../../controls/ControlDealAddonTerms';
import DeliveryPeriod from '../../controls/ControlDeliveryPeriod';
import EmailCounterAgent from '../../controls/ControlEmailCounterAgent';
import Amount from '../../controls/ControlAmount';
import FeeAmount from '../../controls/ControlFeeAmount';
import DealConfirm from '../../controls/ControlDealConfirm';
import CivilLawSubject from '../../controls/ControlCivilLawSubject';
import CivilLawSubjectAndPaymentMethod from '../../controllers/ControllerCivilLawSubjectAndPaymentMethod';
import File from '../../controls/ControlFileList';
import FormBase from '../FormBase';
import AgentEmail from '../../controls/ControlAgentEmail';
import PaymentHint from '../../controls/ControlPaymentHint';
import DisplayNone from '../../components/DisplayNone';
import ShowError from '../../ShowError';
import ArchiveFormMessage from '../../components/ArchiveFormMessage';

import URLS from '../../constants/urls';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from '../../constants/server_errors';
import { DELIVERY_TYPES_NAMES } from '../../constants/names';
import FEE_AMOUNT from '../../constants/fee_amount';
import { customFetch, getDateNowInPrettyFormat, getPhoneInMaskFormat } from '../../Helpers';

const FormDealErrors = {
    ...SERVER_ERRORS_MESSAGES,
    not_auth: <span>Пожалуйста, <a href={`${URLS.LOGIN}?redirectUrl=${URLS.DEAL.CREATE}`}>авторизируйтесь</a>.</span>,
};

export default class FormDeal extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            controls_server_errors: {
                /**
                 * Ожидается объект вида {"ключ ошибки": "текст ошибки"}
                 * @example
                 *  amount: {
                 *    isEmpty: "Value is required and can't be empty",
                 *    regExp: "Invalid regexp"
                 *  }
                 **/
                deal_type: null,
                deal_role: null,
                addon_terms: null,
                amount: null,
                counteragent_email: null,
                name: null,
                delivery_period: null,
                fee_payer_option: null,
                delivery_type: null,
                deal_civil_law_subject: null,
                payment_method: null,
                deal_civil_law_subject_form: null, // Ожидается объект со вложеннами ошибками формы
                payment_method_form: null, // Ожидается объект со вложеннами ошибками формы
                deal_confirm: false
            },
            // SERVER_ERRORS
            server_errors_messages: FormDealErrors,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                deal_type_is_valid: false,
                deal_role_is_valid: false,
                name_is_valid: false,
                addon_terms_is_valid: true,
                amount_is_valid: false,
                fee_payer_option_is_valid: false,
                delivery_is_valid: true,
                delivery_period_is_valid: false,
                counteragent_email_is_valid: false,
                deal_civil_law_subject_is_valid: false,
                payment_method_is_valid: false,
                deal_confirm_is_valid: false,
                civil_law_subject_form_is_valid: false,
                payment_method_form_is_valid: false,
            },
            // -----------------------------
            form: {
                csrf: '',
                deal_type: '',
                deal_role: 'customer',
                name: '',
                addon_terms: '',
                amount: '',
                fee_amount: '',
                fee_payer_option: '',
                delivery: {
                    service_type: '',
                    service: {},
                    service_dpd: {
                        payment_type: 'ОУО',
                        service_code: 'PCL',
                        parcel_quantity: 1,
                        delivery_time_period: '9-18',
                        pickup_time_period: '9-18',
                        cargo_category: 'Товары народного потребления (без ГСМ и АКБ)',
                    },
                    terms_of_delivery: ''
                },
                delivery_period: '14',
                counteragent_email: '',
                deal_civil_law_subject: '',
                payment_method: '',
                files: ''
            },
            // Флаги на случай если нужно сделать контролы disabled
            disabled_controls: {
                deal_type: false,
                deal_role: false,
                name: false,
                addon_terms: false,
                amount: false,
                fee_payer_option: false,
                delivery: false,
                delivery_period: false,
                deal_civil_law_subject: false,
                payment_method: false
            },
            deal_id: '',
            form_isValid: false,
            form_isPreloader: true, // Включаем прелоадер по дефолту
            form_isSubmitting: false,
            deal_types: {},
            fee_payer_options: [],
            deal_number: '',
            civil_law_subject_form_data: {},
            payment_method_form_data: {},
            natural_persons: [],
            legal_entities: [],
            sole_proprietors: [],
            delivery_service_types: {},
            update_collection_civil_law_subjects: 0, // Флаг того что нужно обновить коллекцию в компоненте, для активации повышаем на 1
            date_of_create: getDateNowInPrettyFormat(),
            clear_local_storage: 0,
            form_template: 'base_form',
            max_deal_amount: null, // Максимальная сумма сделки
        };

        this.form_name = 'form_deal'; // название формы для хранения данных в local_storage

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeDealRole = this.handleChangeDealRole.bind(this);
        this.handleChangeCivilLawSubject = this.handleChangeCivilLawSubject.bind(this);
        this.handleChangeAmount = this.handleChangeAmount.bind(this);
        this.handleChangeFeeAmount = this.handleChangeFeeAmount.bind(this);
        this.handleChangeFeePayerOption = this.handleChangeFeePayerOption.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggleIsLoading = this.toggleIsLoading.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleStateChange = this.handleStateChange.bind(this);
        this.handleCivilLawSubjectValid = this.handleCivilLawSubjectValid.bind(this);
    }

    componentWillMount() {
        const formHTML = document.getElementById('FormDeal');
        if (formHTML) {
            formHTML.style.visibility = 'visible';
        }
        this.formInit();
    }

    getCustomDealRole = (current_url) => {
        if (current_url) {
            switch (current_url) {
                case '#prodavczam':
                    return 'contractor';
                    break;
                case '#pokupatelyam':
                    return 'customer';
                    break;
                default:
                    return;
            }
        }
    };

    resetForm = () => {
        if(this.oldState) {
            this.setState({form: {...this.state.form, ...this.oldState}, formHasDataAtLocalStorage: false});
            this.clearFormLocalStorage();
        }
    };

    getFormFromLocalStorage() {
        return new Promise((resolve, reject) => {
            if (localStorage && this.form_name && localStorage.hasOwnProperty(this.form_name)) {
                const form = JSON.parse(localStorage.getItem(this.form_name));
                const { deal_types, fee_payer_options } = this.state;
                
                if (form.hasOwnProperty('deal_type')) {
                    if (!deal_types.hasOwnProperty(form.deal_type)) {
                        delete form.deal_type;
                    }
                }
                if (form.hasOwnProperty('fee_payer_option')) {
                    if (!fee_payer_options.hasOwnProperty(form.fee_payer_option)) {
                        delete form.fee_payer_option;
                    }
                }

                if (form.hasOwnProperty('files')) delete form.files;


                if (form.hasOwnProperty('delivery')) delete form.delivery;

                this.setState({
                    formHasDataAtLocalStorage: true,
                    form: {
                        ...this.state.form,
                        ...form
                    }
                }, () => {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    getFormInitUrl() {
        return URLS.DEAL.FORM_INIT;
    }

    formInit() {
        const url = this.getFormInitUrl();

        customFetch(url)
            .then(data => {
                if (data.status === "SUCCESS") {
                    this.successAjaxFormInit(data);
                } else {
                    return Promise.reject(data);
                }
            })
            .catch(error => {
                console.log(error);
                this.setFormError('form_init_fail', true);
                this.toggleIsLoading('form_isPreloader');
            });
    }

    successAjaxFormInit(data) {
        console.log('formInit', data);
        const {
            deal_types = {},
            fee_payer_options = [],
            csrf,
            naturalPersons,
            legalEntities,
            soleProprietors,
            max_deal_amount,
            delivery_service_types
        } = data.data;

        const user = data.data.user || {};

        const form = {...this.state.form};
        const { update_collection_civil_law_subjects } = this.state;
        const { form_template, form_type } = this.props;

        form['csrf'] = csrf;

        let data_to_state = {
            deal_types,
            fee_payer_options,
            natural_persons: naturalPersons || [],
            legal_entities: legalEntities || [],
            sole_proprietors: soleProprietors || [],
            delivery_service_types: delivery_service_types || {},
            update_collection_civil_law_subjects: update_collection_civil_law_subjects + 1,
            form_template: form_template || this.state.form_template,
            max_deal_amount,
            form: {
                ...form
            },
            user_email: user.email || '',
            user_phone: getPhoneInMaskFormat(user.phone)
        };

        data_to_state = this.setFormDefaultValues(data_to_state);

        if (form_type) {
            data_to_state = this.setCustomDataToState(data_to_state);
        }

        this.oldState = {...this.state.form, ...data_to_state.form};

        const current_url = window.location.hash;
        if (current_url) {
            data_to_state['form']['deal_role'] = this.getCustomDealRole(current_url);
        }
        this.setState(data_to_state, () => {
            this.getFormFromLocalStorage();
            this.toggleIsLoading('form_isPreloader');
        });
    }

    findDealTypeIdByIdent(deal_types = {}, deal_type_ident) {
        if (!deal_types || !deal_type_ident) return;
        const deal_type = Object.values(deal_types).find(type => type.ident === deal_type_ident);

        if (!deal_type) return;

        return deal_type.id;
    }

    findFeePayerOptionIdByName(fee_payer_options = {}, fee_payer_option_name) {
        if (!fee_payer_options || !fee_payer_option_name) return;
        const fee_payer_option = Object.values(fee_payer_options).find(option => option.name === fee_payer_option_name);

        if (!fee_payer_option) return;

        return fee_payer_option.id;
    }

    setDefaultDealType(data_to_state = {}) {
        const { deal_types } = data_to_state;
        if (!deal_types || !Object.values(deal_types).length) return data_to_state;

        // Находим id типа сделки Товар и ставим его в форму по дефолту
        const deal_type = this.findDealTypeIdByIdent(deal_types, 'T');

        if (!deal_type) return data_to_state;

        data_to_state.form.deal_type = deal_type;

        return data_to_state;
    }

    setDefaultFeePayerOption(data_to_state = {}) {
        const { fee_payer_options } = data_to_state;
        if (!fee_payer_options || !Object.values(fee_payer_options).length) return data_to_state;

        const fee_payer_option = this.findFeePayerOptionIdByName(fee_payer_options, 'CUSTOMER');

        if (!fee_payer_option) return data_to_state;

        data_to_state.form.fee_payer_option = fee_payer_option;

        return data_to_state;
    }

    setDefaultDeliveryType(data_to_state = {}, ) {
        const { delivery_service_types } = data_to_state;

        if (!delivery_service_types || !data_to_state.form || !data_to_state.form.delivery) return data_to_state;

        const service_type_no_service = Object.values(delivery_service_types).find((service => service.name === 'NO_SERVICE'));
        if (!service_type_no_service) return data_to_state;

        const service_type = service_type_no_service.id;
        if (!service_type) return data_to_state;

        data_to_state.form.delivery.service_type = service_type;
        return data_to_state;
    }

    setDpdDeliveryExtendedData(original_data, delivery_data_to_state, deal_role) {
        if (!delivery_data_to_state) return;

        if (deal_role === 'contractor') {
            if (delivery_data_to_state.service_dpd.hasOwnProperty('sender_address') && delivery_data_to_state.service_dpd.sender_address) {
                const sender_address = delivery_data_to_state.service_dpd.sender_address;

                if (sender_address.hasOwnProperty('city_id') && sender_address.city_id) {
                    if (original_data.hasOwnProperty('sender_address') && original_data.sender_address) {
                        if (original_data.sender_address.city_id && original_data.sender_address.full_city_name) {
                            sender_address.selected_city = {
                                full_city_name: original_data.sender_address.full_city_name,
                                city_id: original_data.sender_address.city_id
                            }
                        }
                    }
                }

                if (sender_address.hasOwnProperty('terminal_code') && sender_address.terminal_code) {
                    if (original_data.hasOwnProperty('sender_address') && original_data.sender_address) {
                        if (original_data.sender_address.hasOwnProperty('terminal') && original_data.sender_address.terminal) {
                            if (original_data.sender_address.terminal.code === sender_address.terminal_code) {
                                sender_address.selected_point = {
                                    ...original_data.sender_address.terminal
                                };
                                if (original_data.sender_address.terminal.hasOwnProperty('times') && original_data.sender_address.terminal.times) {
                                    sender_address.selected_point.schedules = [[...original_data.sender_address.terminal.times]];
                                }
                                if (original_data.sender_address.terminal.hasOwnProperty('street_abr')
                                    && original_data.sender_address.terminal.street_abr)
                                {
                                    sender_address.selected_point.streetAbr = original_data.sender_address.terminal.street_abr
                                }
                            }
                        }
                    }
                }
            }
        } else if (deal_role === 'customer') {
            if (delivery_data_to_state.service_dpd.hasOwnProperty('receiver_address') && delivery_data_to_state.service_dpd.receiver_address) {
                const receiver_address = delivery_data_to_state.service_dpd.receiver_address;

                if (receiver_address.hasOwnProperty('city_id') && receiver_address.city_id) {
                    if (original_data.hasOwnProperty('receiver_address') && original_data.receiver_address) {
                        if (original_data.receiver_address.city_id && original_data.receiver_address.full_city_name) {
                            receiver_address.selected_city = {
                                full_city_name: original_data.receiver_address.full_city_name,
                                city_id: original_data.receiver_address.city_id
                            }
                        }
                    }
                }
            }
        }
    }

    setDpdPhoneAndEmail(address = {}, user_phone, user_email) {
        if (!address.contact_phone) {
            if (user_phone) {
                address.contact_phone = user_phone;
            }
        }

        if (!address.contact_email) {
            if (user_email) {
                address.contact_email = user_email;
            }
        }
    }

    setDefaultDpdUserPhoneAndEmail(data_to_state = {}) {
        const { user_email, user_phone, form } = data_to_state;

        if (!form || !form.delivery || !form.delivery.service_dpd) return data_to_state;
        const { deal_role, delivery } = form;

        if (deal_role === 'customer') {
            // Если такого поля нет, делаем пустой объект
            if (!delivery.service_dpd.receiver_address) delivery.service_dpd.receiver_address = {};

            this.setDpdPhoneAndEmail(delivery.service_dpd.receiver_address, user_phone, user_email);

        } else if (deal_role === 'contractor') {
            // Если такого поля нет, делаем пустой объект
            if (!delivery.service_dpd.sender_address) delivery.service_dpd.sender_address = {};

            this.setDpdPhoneAndEmail(delivery.service_dpd.sender_address, user_phone, user_email);
        }

        return data_to_state;
    }

    setDeliveryDefaultData(data_to_state = {}) {
        let data = {...data_to_state};
        data = this.setDefaultDeliveryType(data);
        // data = this.setDefaultDpdUserPhoneAndEmail(data);

        return data;
    }

    setFormDefaultValues(data_to_state = {}) {
        let data = {...data_to_state};

        data = this.setDefaultDealType(data);
        data = this.setDefaultFeePayerOption(data);
        data = this.setDeliveryDefaultData(data);

        return data;
    }

    setCustomDataToState(data_to_state) {
        const { deal_types } = data_to_state;
        const { form_type } = this.props;

        if (!form_type) return data_to_state;

        let deal_type;
        let deal_role;

        switch (form_type) {
            case 'service_contractor':
                deal_type = this.findDealTypeIdByIdent(deal_types, 'U');
                deal_role = 'contractor';
                break;
            case 'service_customer':
                deal_type = this.findDealTypeIdByIdent(deal_types, 'U');
                deal_role = 'customer';
                break;
            case 'product_contractor':
                deal_type = this.findDealTypeIdByIdent(deal_types, 'T');
                deal_role = 'contractor';
                break;
            case 'product_customer':
                deal_type = this.findDealTypeIdByIdent(deal_types, 'T');
                deal_role = 'customer';
                break;
            default:
                return data_to_state;
        }

        if (this.form_name) {
            this.form_name = `${this.form_name}-${form_type}`;
        }

        return {
            ...data_to_state,
            form: {
                ...data_to_state.form,
                deal_type,
                deal_role
            }
        };
    }

    switchDpdDeliveryAddresses() {
        if (!this.state.form.delivery || typeof this.state.form.delivery !== 'object') return;

        const service_type_name = this.getDeliveryServiceTypeName();
        if (service_type_name !== DELIVERY_TYPES_NAMES.DPD) return;

        const delivery = {...this.state.form.delivery};
        if (!delivery.service_dpd || typeof delivery.service_dpd !== 'object') return;

        const service_dpd = {...delivery.service_dpd}; // делаем копию
        delivery.service_dpd = service_dpd; // присваеваем копию

        // Если создание сделки и роль пользователя меняется на покупателя, но он уже заполнил данные контрагента то, очищаем данные контрагента,
        // так как только продавец может заполнять данные контрагента
        if (this.form_name === 'form_deal') {
            if (this.state.form.deal_role === 'customer') {
                if (service_dpd.receiver_address) {
                    delete service_dpd.receiver_address;
                }
            }
        }

        let current_sender_address;
        let current_receiver_address;

        let new_sender_address;
        let new_receiver_address;

        const is_receiver_address_exist = service_dpd.receiver_address && Object.values(service_dpd.receiver_address).length;
        const is_sender_address_exist = service_dpd.sender_address && Object.values(service_dpd.sender_address).length;

        // Делаем копии объектов адресов
        if (is_receiver_address_exist) {
            current_receiver_address = {...service_dpd.receiver_address};
            service_dpd.receiver_address = current_receiver_address;
        }

        if (is_sender_address_exist) {
            current_sender_address = {...service_dpd.sender_address};
            service_dpd.sender_address = current_sender_address;
        }
        // ------------------

        if (is_receiver_address_exist) {
            new_sender_address = {...current_receiver_address};
        }
        if (is_sender_address_exist) {
            new_receiver_address = {...current_sender_address};
        }

        if (new_sender_address) {
            service_dpd.sender_address = new_sender_address;
            // !!! Так как у отправителя теперь всегда пункт приема то очищаем улицу, дом, квартиру
            // !!! (Эти адресса теперь никогда не могут совпасть, т.к. разный тип доставки - у отправителя только курьерская, у получателя только пункт);
            new_sender_address.street = '';
            new_sender_address.house = '';
            new_sender_address.flat = '';
            // -------------------------
            if (!new_receiver_address) { // Стираем за собой данные если контрадреса нет
                delete service_dpd.receiver_address;
            }
        }
        if (new_receiver_address) {
            if (new_receiver_address.terminal_code) {
                // Проверка если terminal_code в адресе получателя, то его надо удалять, т.к. для покупателя только курьерская доставка
                delete new_receiver_address.terminal_code;
                delete new_receiver_address.name;
                delete new_receiver_address.selected_point;
                new_receiver_address.street = '';
                new_receiver_address.house = '';
                new_receiver_address.flat = '';
            }

            service_dpd.receiver_address = new_receiver_address;

            delete service_dpd.parcels; // Очищаем данные о параметрах посылки

            if (!new_sender_address) { // Стираем за собой данные если контрадреса нет
                delete service_dpd.sender_address;
            }
        }

        this.setState({
           form: {
               ...this.state.form,
               delivery: {
                   ...this.state.form.delivery,
                   service_dpd
               }
           }
        });
    }

    handleChangeDealRole(name, value) {
        const form = {
            ...this.state.form,
            [name]: value,
            payment_method: '',
        };

        this.setState({
            form
        }, () => {
            this.switchDpdDeliveryAddresses();
            this.checkAllControlsAreValid();
            this.saveFormToLocalStorage();
        });
    }

    handleChangeAmount(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value
            }
        }, () => {
            this.handleChangeFeeAmount();
            this.saveFormToLocalStorage();
        });
    }

    handleChangeFeeAmount() {
        const { amount } = this.state.form;
        const new_fee_amount = (amount * FEE_AMOUNT).toFixed(2);

        this.setState({
            form: {
                ...this.state.form,
                fee_amount: new_fee_amount
            }
        }, () => this.saveFormToLocalStorage());
    }

    handleChangeFeePayerOption(name, value) {
        this.setState({
            form: {
                ...this.state.form,
                [name]: value
            }
        }, () => {
            this.handleChangeFeeAmount();
            this.saveFormToLocalStorage();
        });
    }

    handleComponentValid(name, value) {
        if (this.state.validation[name] === value) {
            this.checkAllControlsAreValid();
        } else {
            this.setState((prevState) => {
                return {
                    validation: {
                        ...prevState.validation,
                        [name]: value
                    }
                }
            }, () => {
                this.checkAllControlsAreValid();
            });
        }
    }

    checkAllControlsAreValid() {
        const {
            deal_type_is_valid,
            deal_role_is_valid,
            name_is_valid,
            addon_terms_is_valid,
            amount_is_valid,
            fee_payer_option_is_valid,
            delivery_period_is_valid,
            deal_civil_law_subject_is_valid,
            counteragent_email_is_valid,
            payment_method_is_valid,
            deal_confirm_is_valid,
            civil_law_subject_form_is_valid,
            delivery_is_valid,
            payment_method_form_is_valid,
        } = this.state.validation;

        const deal_type_ident = this.getCurrentDealTypeIdent();

        const form_isValid = (
            deal_type_is_valid
            && deal_role_is_valid
            && name_is_valid
            && addon_terms_is_valid
            && amount_is_valid
            && fee_payer_option_is_valid
            && (deal_type_ident === 'U' || delivery_is_valid)
            && delivery_period_is_valid
            && counteragent_email_is_valid
            && deal_confirm_is_valid
            && (deal_civil_law_subject_is_valid || civil_law_subject_form_is_valid)
            && (
                (
                    this.state.form.deal_role === 'contractor'
                    && ( payment_method_is_valid || payment_method_form_is_valid )
                )
                || (this.state.form.deal_role === 'customer')
            )
        );

        if (this.state.form_isValid === form_isValid) return;

        this.setState({
            form_isValid
        });
    }

    setDeliverySubmitData(data) {
        const delivery_original_data = data.delivery;
        if (!delivery_original_data || typeof delivery_original_data !== 'object') return; // Проверка что что delivery существует

        const delivery = {...delivery_original_data}; // Делаем копию объект чтобы не изменялся state
        data.delivery = {}; // Очищаем доставку в данных для отправки чтобы заново ее сформировать
        data.delivery = delivery; // Кладем в данные для отправки копию объекта доставки

        const service_type_name = this.getDeliveryServiceTypeName();

        const delivery_service_dpd = delivery.service_dpd;
        if (service_type_name === DELIVERY_TYPES_NAMES.DPD && delivery_service_dpd && typeof delivery_service_dpd === 'object') {
            delivery.terms_of_delivery = ''; // Обнуляем описание доставки

            if (delivery.service) delivery.service = {}; // Удаляем текущее поле service чтобы положить туда service_dpd

            delivery.service = {...delivery_service_dpd}; // Записываем в service - service_dpd

            if ( // Удаляем вспомогательное поле
                delivery.service.hasOwnProperty('receiver_address')
                && delivery.service.receiver_address
                && delivery.service.receiver_address.hasOwnProperty('selected_city')
            ) delete delivery.service.receiver_address.selected_city;

            if ( // Удаляем вспомогательное поле
                delivery.service.hasOwnProperty('sender_address')
                && delivery.service.sender_address
                && delivery.service.sender_address.hasOwnProperty('selected_city')
            ) delete delivery.service.sender_address.selected_city;

            if ( // Удаляем вспомогательное поле
                delivery.service.hasOwnProperty('sender_address')
                && delivery.service.sender_address
                && delivery.service.sender_address.hasOwnProperty('selected_point')
            ) delete delivery.service.sender_address.selected_point;

            delivery.service_dpd = {}; // Удаляем service_dpd
            delete delivery.service_dpd;
        } else {
            if (delivery_service_dpd) {
                delivery.service_dpd = {};
                delete delivery.service_dpd;
            }
        }
    }

    getDataFromState() {
        const { deal_civil_law_subject, deal_role, payment_method } = this.state.form;
        const data = {...this.state.form};
        delete data.files;

        const files = this.state.form.files;

        if (!deal_civil_law_subject) {
            data['deal_civil_law_subject'] = this.state.civil_law_subject_form_data;
        }

        if (deal_role === 'contractor' && !payment_method) {
            data['payment_method'] = this.state.payment_method_form_data;
        }

        this.setDeliverySubmitData(data);

        return this.createFormData(data, files);
    }

    /**
     * @param form_data - объект new FormData()
     * @param data - данные для добавления в form_data
     * @param path - если при присутсвует path, значит это вложенный ключ и тут будет находится его путь
     */
    addFormData(form_data, data, path = '') {
        Object.entries(data).forEach(([key, value]) => {
            let key_path = key; // Ключ куда сохраняем данные
            if (path) { // Если path существует, то значит это вложенный ключ
                key_path = `${path}[${key}]`;
            }

            if (typeof value === 'object' && value) {
                this.addFormData(form_data, value, key_path); // Для добавления в form_data вложенного массива запускаем рекурсию
            } else {
                form_data.append(key_path, value);
            }
        });
    }

    createFormData(data, files = []) {
        const form_data = new FormData();

        this.addFormData(form_data, data);

        let file_index = 0;

        if (files.length) {
            Object.values(files).forEach((file) => {
                if (file) {
                    form_data.append(`files[${file_index}]`, file);
                    file_index++;
                }
            });
        }

        return form_data;
    }

    getSubmitUrl() {
        return ''
    }

    handleSubmit() {
        if (this.state.form_isValid) {
            this.toggleIsLoading();

            customFetch(this.getSubmitUrl(), {
                method: 'POST',
                body: this.getDataFromState(),
                processData: false, //Это важный параметр, без него не сработает
                contentType: false
            })
                .then(data => {
                    this.toggleIsLoading();
                    if (data.status === 'SUCCESS') {
                        this.successAjaxSubmit(data);
                    } else if (data.status === 'ERROR') {
                        this.TakeApartErrorFromServer(data);
                    }
                })
                .catch(response => {
                    console.log(response);
                    this.toggleIsLoading();
                    this.setFormError('critical_error', true);
                })
        }
    }

    successAjaxSubmit(data) {
        this.clearFormLocalStorage();

        this.setState({
            deal_id: this.state.deal_id || data.data.deal.id,
            clear_local_storage: this.state.clear_local_storage + 1
        }, () => {
            this.toggleIsLoading('form_isPreloader');
            this.afterSubmitFormDeal();
        });
    }

    handleCancel() {
        window.location = `${URLS.DEAL.SHOW}/${this.state.deal_id}`;
    }

    afterSubmitFormDeal() {
         window.location = `${URLS.DEAL.SHOW}/${this.state.deal_id}`;
    }

    TakeApartInvalidFormData(errors) {
        this.setFormError('invalid_form_data', true);

        if (errors) {
            const controls_server_errors = {...this.state.controls_server_errors};

            Object.keys(errors).forEach(control => {
                switch (control) {
                    case 'csrf':
                        this.setFormError('csrf', true);
                        break;
                    case 'deal_civil_law_subject':
                        if (errors[control].hasOwnProperty('civil_law_subject_id')) {
                            controls_server_errors['deal_civil_law_subject'] = errors[control]['civil_law_subject_id'];
                        } else {
                            controls_server_errors['deal_civil_law_subject_form'] = errors[control];
                        }
                        break;
                    case 'payment_method':
                        if (errors[control].hasOwnProperty('payment_method_id')) {
                            controls_server_errors['payment_method'] = errors[control]['payment_method_id'];
                        } else {
                            controls_server_errors['payment_method_form'] = errors[control];
                        }
                        break;
                    default:
                        controls_server_errors[control] = errors[control]
                }
            });

            this.setState({
                controls_server_errors
            });
        }
    }

    getFormWithRoleAndTypeTitle() {
        const ident = this.getCurrentDealTypeIdent();
        const deal_role = this.state.form.deal_role;

        return {
            U: {
                customer: 'Заказ услуги',
                contractor: 'Оказание услуги'
            },
            T: {
                customer: 'Покупка товара',
                contractor: 'Продажа товара'
            }
        }[ident][deal_role];
    }

    getLabels() {
        const ident = this.getCurrentDealTypeIdent();

        const deal_name_label = ident === 'U' ? 'услуги' : 'товара';
        const deal_term_label = ident === 'U' ? 'исполнения' : 'доставки';


        return {
            deal_name_label,
            deal_term_label
        }
    }

    getCurrentDealTypeIdent() {
        const deal_types = this.state.deal_types;
        const deal_type = this.state.form.deal_type;

        if (deal_types[deal_type]) {
            return deal_types[deal_type].ident;
        } else {
            return 'T'
        }
    }

    getDeliveryServiceTypeName (
        service_type = this.state.form.delivery ? this.state.form.delivery.service_type : undefined, // На случай если delivery - null
        service_types = this.state.delivery_service_types
    ) {
        if (service_types[service_type] && service_types[service_type].name) {
            return service_types[service_type].name;
        } else {
            return DELIVERY_TYPES_NAMES.NO_SERVICE;
        }
    };

    // -------------- Методы которые используются в потомках
    getInitDeliveryData(delivery, service_types) {
        const { service_type, service, terms_of_delivery } = delivery;
        const service_type_name = this.getDeliveryServiceTypeName(service_type, service_types);

        const delivery_init_data = {};
        delivery_init_data['service_type'] = service_type;
        delivery_init_data['terms_of_delivery'] = terms_of_delivery;

        let service_init_data = {};
        if (service_type_name === DELIVERY_TYPES_NAMES.DPD) {
            service_init_data = this.getDpdInitData(service);
            delivery_init_data['service_dpd'] = service_init_data;
        }

        return delivery_init_data;
    }

    addToObjectIfValueExist(object, key, value) {
        if (value) {
            object[key] = value;
        }
    }

    addDataToObject(data, object) {
        Object.entries(data).forEach(([key, value]) => {
            this.addToObjectIfValueExist(object, key, value);
        });
    }

    addAddressInitData(address, address_key, parent_object) {
        if (address) {
            const { city_id, contact_email, contact_fio, contact_phone, house, street, flat, terminal_code } = address;
            const address_values = { city_id, contact_email, contact_fio, contact_phone, house, street, flat, terminal_code };

            parent_object[address_key] = {};
            const address_init_data = parent_object[address_key];

            this.addDataToObject(address_values, address_init_data);
        }
    }

    getDpdInitData(service) {
        const { cargo_category, delivery_time_period, pickup_time_period, payment_type, service_code, receiver_address, sender_address, parcels } = service;
        const service_init_data = {};

        // Простые значение которые мы хотим сохранить в форму
        // Объекты и массивы могут иметь излишние данные, которые мы не хотим сохранять, поэтому их обработаем чуть ниже
        // Т.е. receiver_address, sender_address, parcels не включаем в simple_values_to_state,
        // потому что они имеют сложную стуктуру из которой мы хотим взять не все данные
        const simple_values_to_state = { cargo_category, delivery_time_period, pickup_time_period, payment_type, service_code };

        this.addDataToObject(simple_values_to_state, service_init_data);

        // Добавляем адреса
        this.addAddressInitData(receiver_address, 'receiver_address', service_init_data);
        this.addAddressInitData(sender_address, 'sender_address', service_init_data);

        if (parcels) {
            service_init_data['parcels'] = [];
            const parcels_init_data = service_init_data['parcels'];
            if (parcels[0]) {
                const { weight, height, width, length, id } = parcels[0];
                const parcel_values = { weight, height, width, length, id };

                parcels_init_data[0] = {};
                const parcel_init_data = parcels_init_data[0];

                this.addDataToObject(parcel_values, parcel_init_data);
            }
        }

        return service_init_data;
    }
    // ----------------

    getDealRoleControl(props) {
        const { deal_role } = this.state.form;

        return (
            <DealRole
                label="Ваша роль в сделке:"
                name="deal_role"
                value_prop={deal_role}
                deal_type={this.getCurrentDealTypeIdent()}
                handleComponentChange={this.handleChangeDealRole}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.deal_role}
                is_disable={this.state.disabled_controls.deal_role}
                {...props}
            />
        );
    }

    getDealTypeControl(props) {
        const { deal_types } = this.state;
        const deal_type = this.state.form.deal_type;

        return (
            <DealType
                label="Тип сделки:"
                name="deal_type"
                deal_types={deal_types}
                value_prop={deal_type}
                handleComponentChange={this.handleChange}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.deal_type}
                is_disable={this.state.disabled_controls.deal_type}
                {...props}
            />
        );
    }

    getDealNameControl(props) {
        const name = this.state.form.name;
        return (
            <DealName
                name="name"
                value_prop={name}
                handleComponentChange={this.handleChange}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.name}
                is_disable={this.state.disabled_controls.name}
                formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                {...props}
            />
        );
    }

    getAddonTermsControl(props) {
        const addon_terms = this.state.form.addon_terms;
        return (
            <AddonTerms
                name="addon_terms"
                value_prop={addon_terms}
                handleComponentChange={this.handleChange}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.addon_terms}
                is_disable={this.state.disabled_controls.addon_terms}
                formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                {...props}
            />
        );
    }

    getFileControl(props) {
        return (
            <File
                name="files"
                label="Прикрепить файл:"
                handleComponentChange={this.handleChange}
                {...props}
            />
        );
    }

    getAmountControl(props) {
        const amount = this.state.form.amount;
        const max_deal_amount = this.state.max_deal_amount;
        return (
            <Amount
                name='amount'
                value_prop={amount}
                max_amount={max_deal_amount}
                handleComponentChange={this.handleChangeAmount}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.amount}
                is_disable={this.state.disabled_controls.amount}
                formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                {...props}
            />
        );
    }

    getFeeAmountControl(props) {
        const { fee_amount } = this.state.form;
        const fee_amount_string = parseFloat(fee_amount) > 0 ? fee_amount : '';
        return (
            <FeeAmount
                name='fee_amount'
                label='Комиссия сервиса: '
                value_prop={String(fee_amount_string)}
                {...props}
            />
        );
    }

    getDeliveryPeriodControl(props) {
        const delivery_period = this.state.form.delivery_period;
        return (
            <div className="Input-Group-Wrapper">
                <DeliveryPeriod
                    name='delivery_period'
                    value_prop={delivery_period}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValid}
                    form_control_server_errors={this.state.controls_server_errors.delivery_period}
                    is_disable={this.state.disabled_controls.delivery_period}
                    formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                    {...props}
                />
            </div>
        );
    }

    getFeePayerOptionsControl(props) {
        const { fee_payer_options } = this.state;
        const { fee_payer_option } = this.state.form;

        return (
            <FeePayerOption
                label="Кто оплачивает комиссию:"
                name="fee_payer_option"
                fee_payer_options={fee_payer_options}
                deal_type={this.getCurrentDealTypeIdent()}
                value_prop={fee_payer_option}
                handleComponentChange={this.handleChangeFeePayerOption}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.fee_payer_option}
                is_disable={this.state.disabled_controls.fee_payer_option}
                {...props}
            />
        );
    }

    getDeliveryControl(props) {
        const { delivery = {}, deal_role, csrf } = this.state.form;
        // Данные для рассчета стоимости доставки
        const { service_dpd = {} } = delivery;
        const { sender_address = {}, receiver_address = {}, parcels = [] } = service_dpd;
        // Адреса
        const { city_id: sender_city_id, terminal_code: self_pickup } = sender_address;
        const { city_id:  receiver_city_id, terminal_code:  self_delivery } = receiver_address;
        // Вес
        const parcel = parcels[0] || {};
        const weight = parcel['weight'] || '';
        // ---------------------------------------

        return (
            <Delivery
                label="Доставка:"
                name="delivery"
                service_type_name="service_type"
                service_control_name="service"
                service_dpd_control_name="service_dpd"
                current_service_type_name={this.getDeliveryServiceTypeName()}
                deal_role={deal_role}
                value_prop={delivery}
                service_types={this.state.delivery_service_types}
                handleComponentChange={this.handleChange}
                handleComponentValidation={this.handleComponentValid}
                form_control_server_errors={this.state.controls_server_errors.delivery}
                is_disable={this.state.disabled_controls.delivery}
                can_added_counteragent_data={true}
                csrf={csrf}
                // -------------- Поля для калькулятора
                is_show_calc={true}
                calc_sender_city_id={sender_city_id}
                calc_receiver_city_id={receiver_city_id}
                calc_self_delivery={self_delivery ? '1' : '0'}
                calc_self_pickup={self_pickup ? '1' : '0'}
                calc_weight={weight}
                {...props}
            />
        );
    }

    getCounterAgentEmailControl(props) {
        const { deal_role, counteragent_email } = this.state.form;

        return (
            <div className="Input-Group-Wrapper">
                <EmailCounterAgent
                    deal_type={this.getCurrentDealTypeIdent()}
                    deal_role={deal_role}
                    value_prop={counteragent_email}
                    handleComponentValidation={this.handleComponentValid}
                    handleComponentChange={this.handleChange}
                    form_control_server_errors={this.state.controls_server_errors.counteragent_email}
                    formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                    {...props}
                />
            </div>
        );
    }

    getCivilLawSubjectControl(props) {
        const deal_civil_law_subject = this.state.form.deal_civil_law_subject;
        const { natural_persons, legal_entities, sole_proprietors, update_collection_civil_law_subjects, clear_local_storage } = this.state;
        return (
            <div className="row">
                <div className="col-xl-12">
                    <CivilLawSubject
                        label="Вы будете участвовать в сделке как:"
                        name="deal_civil_law_subject"
                        value_prop={deal_civil_law_subject}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        sendUpData={this.handleStateChange}
                        is_nested={true}
                        form_validate_name='civil_law_subject_form_is_valid'
                        form_data_name='civil_law_subject_form_data'
                        natural_persons={natural_persons}
                        legal_entities={legal_entities}
                        sole_proprietors={sole_proprietors}
                        update_collection={update_collection_civil_law_subjects}
                        form_control_server_errors={this.state.controls_server_errors.deal_civil_law_subject}
                        form_server_errors={this.state.controls_server_errors.deal_civil_law_subject_form}
                        clear_local_storage={clear_local_storage}
                        {...props}
                    />
                </div>
            </div>
        );
    }

    getCivilLawSubjectAndPaymentMethodControl(props) {
        const { deal_civil_law_subject, payment_method } = this.state.form;
        const { natural_persons, legal_entities, sole_proprietors, update_collection_civil_law_subjects, clear_local_storage } = this.state;

        return (
            <div className="row row-civil-law-subject-payment-method">
                <div className="col-xl-12">
                    <CivilLawSubjectAndPaymentMethod
                        civil_law_subject_label="Вы будете участвовать в сделке как:"
                        payment_method_label="Способ получения оплаты:"
                        civil_law_subject_name="deal_civil_law_subject"
                        payment_method_name="payment_method"
                        civil_law_subject_id={deal_civil_law_subject}
                        payment_method_id={payment_method}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                        handleCivilLawSubject={this.handleChangeCivilLawSubject}
                        handleCivilLawSubjectValid={this.handleCivilLawSubjectValid}
                        form_civil_law_subject_validate_name='civil_law_subject_form_is_valid'
                        form_civil_law_subject_data_name='civil_law_subject_form_data'
                        form_payment_method_validate_name='payment_method_form_is_valid'
                        form_payment_method_data_name='payment_method_form_data'
                        sendUpData={this.handleStateChange}
                        natural_persons={natural_persons}
                        legal_entities={legal_entities}
                        sole_proprietors={sole_proprietors}
                        civil_law_subject_update_collection={update_collection_civil_law_subjects}
                        is_nested={true}
                        is_cls_control={true}
                        is_hide_bank_transfer_name={true}
                        form_control_server_errors_civil_law_subject={this.state.controls_server_errors.deal_civil_law_subject}
                        form_control_server_errors_civil_law_subject_form={this.state.controls_server_errors.deal_civil_law_subject_form}
                        form_control_server_errors_payment_method={this.state.controls_server_errors.payment_method}
                        form_control_server_errors_payment_method_form={this.state.controls_server_errors.payment_method_form}
                        clear_local_storage={clear_local_storage}
                        {...props}
                    />
                </div>
            </div>
        );
    }

    getDealConfirmControl() {
        const deal_confirm = this.state.form.deal_confirm;
        return (
            <div className="row rules-agree">
                <div className="col-xl-12">
                    <DealConfirm
                        value_prop={deal_confirm}
                        name='deal_confirm'
                        label_text={<span>Нажимая на кнопку “Создать сделку”, вы соглашаетесь с <a href={URLS.TERMS_OF_USE} target="_blank" className="info-link">регламентом пользования сервиса.</a></span>}
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleComponentValid}
                    />
                </div>
            </div>
        );
    }

    getFormErrorsControl() {
        return (
            <div className="row messages">
                <div className="col-xl-12">
                    <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
                </div>
            </div>
        );
    }

    getButtonsControl(props) {
        const { form_isValid, form_isSubmitting } = this.state;
        return (
            <div className="ButtonsRow ButtonsRow_form_deal">
                <button
                    onClick={this.handleSubmit}
                    className={`ButtonApply ${form_isSubmitting ? 'Preloader Preloader_light' : ''}`}
                    disabled={!form_isValid || form_isSubmitting}
                    data-ripple-button=""
                >
                    <span className="ripple-text">Создать сделку</span>
                </button>
            </div>
        );
    }

    getFormTitleControl(props) {
        const { title } = props;
        return (
            <div className="row">
                <div className="col-xl-12">
                    <h3 className="TitleBlue">{title}</h3>
                </div>
            </div>
        );
    }

    getFormLogoControl() {
        return (
            <div className="Logo">
                <div className="Logo-Img">
                    <img src="../../../../img/logo.svg" alt="Логотип"/>
                </div>
                <p className="Logo-Text" />
            </div>
        );
    }

    getAgentEmailControl(props) {
        return (
            <div className="row agent-email">
                <div className="col-xl-12 col-email">
                    <div className="Input-Wrapper">
                        <AgentEmail
                            name="agent_email"
                            label='Ваш Email:'
                            register={false}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleComponentValid}
                            form_control_server_errors={this.state.controls_server_errors.counteragent_email}
                            value_prop={this.state.form.agent_email}
                            is_agent_email={true}
                            formHasDataAtLocalStorage={this.state.formHasDataAtLocalStorage}
                            {...props}
                        />
                    </div>
                </div>
            </div>
        );
    }

    getPaymentHintControl(props) {
        const { amount, fee_amount, deal_role, fee_payer_option } = this.state.form;
        const { fee_payer_options } = this.state;
        return (
            <PaymentHint
                amount={amount}
                fee_amount={fee_amount}
                deal_role={deal_role}
                fee_payer_options={fee_payer_options}
                fee_payer_option={fee_payer_option}
                {...props}
            />
        );
    }

    getControls() {
        const { deal_name_label, deal_term_label } = this.getLabels();
        const delivery_period_control_label = this.state.form.delivery ?
            (this.state.form.delivery.service_type === "1") ?
                "Срок проверки " : `Срок ${deal_term_label} и проверки `
            : `Срок ${deal_term_label} и проверки `;

        return {
            form_logo: this.getFormLogoControl(),
            form_title_control: (props) => this.getFormTitleControl({
                title: 'Создание сделки',
                ...props
            }),
            deal_role_control: (props) => this.getDealRoleControl(props),
            deal_type_control: (props) => this.getDealTypeControl(props),
            deal_name_control: (props) => this.getDealNameControl({
                placeholder: `Введите название ${deal_name_label}`,
                label: `Название ${deal_name_label}:`,
                ...props
            }),
            addon_terms_control: (props) => this.getAddonTermsControl({
                label: `Описание ${deal_name_label}`,
                placeholder: `Введите описание ${deal_name_label}`,
                ...props
            }),
            file_control: (props) => this.getFileControl(props),
            amount_control: (props) => this.getAmountControl({
                label: `Цена ${deal_name_label} (в рублях):`,
                placeholder: `Введите стоимость ${deal_name_label}`,
                ...props
            }),
            fee_amount_control: (props) => this.getFeeAmountControl(props),
            delivery_period_control: (props) => this.getDeliveryPeriodControl({
                label: `${delivery_period_control_label} (дней):`,
                placeholder: `Введите ${delivery_period_control_label.toLowerCase()}${deal_name_label}`
            }),
            fee_payer_options_control: (props) => this.getFeePayerOptionsControl(props),
            delivery_control: (props) => this.getDeliveryControl(props),
            civil_law_subject_control: (props) => this.getCivilLawSubjectControl(props),
            civil_law_subject_and_payment_method_control: (props) => this.getCivilLawSubjectAndPaymentMethodControl(props),
            counteragent_email_control: (props) => this.getCounterAgentEmailControl(props),
            deal_confirm_control: this.getDealConfirmControl(),
            form_errors_control: this.getFormErrorsControl(),
            buttons_control: (props) => this.getButtonsControl(props),
            agent_email_control: (props) => this.getAgentEmailControl(props), // Контрол для других форм наследуемых от формы FormDeal
            payment_hint_control: (props) => this.getPaymentHintControl(props),
        }
    }

    getTemplates() {
        const { form_isPreloader, date_of_create, formHasDataAtLocalStorage } = this.state;
        const { deal_role } = this.state.form;
        const deal_type_ident = this.getCurrentDealTypeIdent();
        const delivery_type_name = this.getDeliveryServiceTypeName();

        const controls = this.getControls();

        return (
            {
                base_form:
                    <FormWrapper is_loading={form_isPreloader} date_of_create={date_of_create} title='Создание сделки'>
                        { formHasDataAtLocalStorage && <ArchiveFormMessage handleResetForm={this.resetForm} /> }
                        { controls.deal_type_control() }
                        { controls.deal_role_control() }
                        { controls.deal_name_control() }
                        { controls.addon_terms_control() }
                        { controls.file_control() }
                        <div className="row">
                            <div className="col-xl-6 col-sm-4">
                                { controls.amount_control() }
                            </div>
                            { controls.fee_amount_control() }
                        </div>
                        <div className="Input-Group-Wrapper">
                            { controls.fee_payer_options_control() }
                            { controls.payment_hint_control() }
                        </div>
                        {
                            deal_type_ident === 'T' &&
                            controls.delivery_control()
                        }
                        {
                            delivery_type_name !== DELIVERY_TYPES_NAMES.DPD &&
                            <div className="row">
                                <div className="col-xl-6 col-sm-4">
                                    { controls.delivery_period_control() }
                                </div>
                            </div>
                        }
                        {
                            deal_role === 'contractor' ?
                                controls.civil_law_subject_and_payment_method_control()
                                :
                                controls.civil_law_subject_control()
                        }
                        { controls.counteragent_email_control() }
                        { controls.deal_confirm_control }
                        { controls.form_errors_control }
                        { controls.buttons_control() }
                    </FormWrapper>,
                form_with_role_and_type:
                    <FormWrapper
                        is_loading={form_isPreloader}
                        date_of_create={date_of_create}
                        logo={controls.form_logo}
                        mobile_title={this.getFormWithRoleAndTypeTitle()}
                    >
                        <DisplayNone>
                            { controls.deal_type_control() }
                            { controls.deal_role_control() }
                        </DisplayNone>
                        {
                            controls.form_title_control({
                                title: this.getFormWithRoleAndTypeTitle()
                            })
                        }
                        { controls.deal_name_control() }
                        { controls.addon_terms_control() }
                        { controls.file_control() }
                        <div className="row">
                            <div className="col-xl-6 col-sm-4">
                                { controls.amount_control() }
                            </div>
                            { controls.fee_amount_control() }
                        </div>
                        <div className="Input-Group-Wrapper">
                            { controls.fee_payer_options_control() }
                            { controls.payment_hint_control() }
                        </div>
                        {
                            deal_type_ident === 'T' &&
                            controls.delivery_control()
                        }
                        {
                            delivery_type_name !== DELIVERY_TYPES_NAMES.DPD &&
                            <div className="row">
                                <div className="col-xl-6 col-sm-4">
                                    { controls.delivery_period_control() }
                                </div>
                            </div>
                        }
                        {
                            deal_role === 'contractor' ?
                                controls.civil_law_subject_and_payment_method_control()
                                :
                                controls.civil_law_subject_control()
                        }
                        { controls.counteragent_email_control() }
                        { controls.deal_confirm_control }
                        { controls.form_errors_control }
                        { controls.buttons_control() }
                    </FormWrapper>
            }
        )
    }

    render() {
        const { form_template} = this.state;

        const templates = this.getTemplates();

        return (
            templates[form_template]
        )
    }
}