import React from 'react';
import Datepicker from '../forms/base/Datepicker';
import LinkSort from '../forms/base/LinkSort';
import {
    sorting, goToLink, filterArray, filterCreatedFrom, filterCreatedTill, getDisputesWithFieldsSplittedByDealTypes,
    getDealStatusesSplittedByDealTypes, getDisputeStatusesSplittedByDealTypes, customFetch,
    getPaymentOrdersWithCreatedInMillisecondsField
} from '../Helpers';
import Paginator from '../Paginator';
import TableBase from './TableBase';
import URLS from "../constants/urls";
import MESSAGES from "../constants/messages";
import withWindowDimensions from '../hocs/withWindowDimensions';

class TableDealsDispute extends TableBase {
    constructor(props) {
        super(props);
        this.state = {
            collection: [],
            collectionFiltered: [],
            isFiltering: false,
            deal_number: '',
            deal_name: '',
            name: '',
            number: '',
            author: '',
            created_from: '',
            created_till_in_milliseconds: '',
            created_from_in_milliseconds: '',
            created_till: '',
            status: '',
            deal_status: '',
            dispute_status: '',
            is_close: '',
            sortName: '',
            sortDirection: '',
            table_isLoading: false,
            paginator: {},
            page: 1,
            per_page: 20,
            csrf: '',
            debit: '',
            deal_statuses: [],
            dispute_statuses: []
        };

        this.handleChangeSort = this.handleChangeSort.bind(this);
        this.handleChangeFilter = this.handleChangeFilter.bind(this);
        this.filterIsClosed = this.filterIsClosed.bind(this);
        this.filterStatus = this.filterStatus.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handlePageSwitch = this.handlePageSwitch.bind(this);
        this.handlePerPageChange = this.handlePerPageChange.bind(this);
        this.getCollection = this.getCollection.bind(this);
    }

    getCollection(isNeedToRunHandleFilter) {
        const { page, csrf, per_page } = this.state;
        const filter = this.setFilterDataToSend();

        this.toggleIsLoading();
        $.ajax({
            dataType: "json",
            type: 'GET',
            data: {
                page,
                per_page,
                csrf,
                filter
            },
            url: URLS.DISPUTE.SINGLE,
            success: (data) => {
                const { csrf, disputes, paginator } = data.data;

                const disputeSplittedByDealTypes = getDisputesWithFieldsSplittedByDealTypes(disputes);
                console.log(data);
                this.setState({
                    collection: disputeSplittedByDealTypes,
                    paginator,
                    csrf
                }, () => {
                    this.toggleIsLoading();
                    isNeedToRunHandleFilter && this.handleFilter();
                    if (this.state.collection.length === 0) {
                        this.setState({
                            isFiltering: true
                        })
                    }
                });
            }
        }).fail(() => this.toggleIsLoading());
    }

    tableInit() {
        this.getStatuses();
        document.getElementById('TableDealsDispute').style.visibility = 'visible';
    }

    getStatuses() {
        fetch(URLS.DEAL.FORM_INIT, {
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status === 200) {
                    return response.json();
                } else {
                    return Promise.reject(MESSAGES.SERVER_ERRORS.CRITICAL_ERROR.text);
                }
            })
            .then((data) => {
                if (data.status === 'SUCCESS') {
                    console.log(data);
                    const deal_statuses = getDealStatusesSplittedByDealTypes(Object.values(data.data.deal_statuses));
                    const dispute_statuses = getDisputeStatusesSplittedByDealTypes(Object.values(data.data.dispute_statuses));

                    this.setState({
                        deal_statuses,
                        dispute_statuses
                    });
                } else if (data.status === 'ERROR') {
                    console.log('Ошибка при получении статусов');
                    return Promise.reject('Ошибка при получении статусов.');
                }
            })
            .catch(error => this.setState({formError: error}));
    }

    setFilterDataToSend() {
        const { deal_name, created_from, deal_status, deal_number, number, debit, created_till, dispute_status } = this.state;
        const filter = {
            deal_id: deal_number,
            deal_name,
            dispute_created_from: created_from,
            dispute_created_till: created_till,
            deal_status,
            dispute_status
        };

        return filter;
    }

    filterIsClosed(is_close) {
        return arrayItem => is_close === '' || arrayItem.is_close.toString() === is_close
    }

    handleFilter() {
        const { collection, deal_number, number, deal_name, author, created_from_in_milliseconds, created_till_in_milliseconds, created_till, deal_status, is_close, sortName, sortDirection, debit, dispute_status } = this.state;
        const isFiltering =
            number !== ''
            || deal_number !== ''
            || deal_name !== ''
            || author !== ''
            || created_from_in_milliseconds !== ''
            || created_till_in_milliseconds !== ''
            || created_till !== ''
            || deal_status !== ''
            || is_close !== ''
            || debit !== ''
            || sortName !== ''
            || dispute_status !== ''
        ;
        const getFilteredResults = (array) => {

            return array
                .filter(filterArray('number', number))
                .filter(filterArray('deal_number', deal_number))
                .filter(filterArray('deal_name', deal_name))
                .filter(filterArray('author', author))
                .filter(filterCreatedFrom('created_milliseconds', created_from_in_milliseconds))
                .filter(filterCreatedTill('created_milliseconds', created_till_in_milliseconds))
                .filter(this.filterStatus(deal_status))
                .filter(this.filterDisputeStatus(dispute_status))
                .filter(this.filterIsClosed(is_close))
        };

        this.setState({
            collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
            isFiltering
        });
    }

    render() {
        const { collection, collectionFiltered, isFiltering, sortName, sortDirection, paginator, per_page, table_isLoading, deal_statuses, dispute_statuses } = this.state;
        const collectionList = isFiltering ? collectionFiltered : collection;
        const { isMobile, isTablet } = this.props;

        return (
            <div className="TableWithPagination">
                <div className="TableWrapper">
                    <table className="TableDefault TableDealsDispute js-table-rows-are-link">
                        <tbody>
                        <tr className="table-head">
                            <td className="head-of-element">
                                <h3>Сделки со спором</h3>
                            </td>
                        </tr>
                        {/*<tr className="FilterRowButtonMobile">*/}
                            {/*<td>*/}
                                {/*<span className="TableDeals-OperatorCol"><input type="text" name="number" placeholder="ID" onChange={this.handleChangeFilter} /></span>*/}
                            {/*</td>*/}
                            {/*<td>*/}
                                {/*<div className="FilterColButton">*/}
                                    {/*<label className="ButtonEdit FilterButton SortButton js-button-sort-table-mobile" title="Сортировка">Сортировка</label>*/}
                                    {/*<div className="wrap">*/}
                                        {/*<label className="ButtonEdit FilterButton button-with-clear js-button-filter-table-mobile" title="Фильтр для таблицы">Фильтр*/}
                                        {/*</label>*/}
                                        {/*{*/}
                                            {/*filter ?*/}
                                                {/*<button id="filter-clear" className="js-clear"></button>*/}
                                                {/*:*/}
                                                {/*void(0)*/}
                                        {/*}*/}
                                    {/*</div>*/}
                                {/*</div>*/}
                            {/*</td>*/}
                        {/*</tr>*/}
                        <tr className="FilterTableRow">
                            <td className="col-number">
                               <span className="TableDisputes-OperatorCol">
                                  <input type="text" name="deal_number" placeholder="ID" className="InputSearch" onChange={this.handleChangeFilter} />
                               </span>
                            </td>
                            <td className="col-name">
                                <input type="text" name="deal_name" value={this.state.deal_name} placeholder="Поиск по названию" className="InputSearch" onChange={this.handleChangeFilter} />
                            </td>
                            <td>
                                <input type="text" name="author" placeholder="Поиск по инициатору" className="InputSearch" onChange={this.handleChangeFilter} />
                            </td>
                            <td className="DoubleInputDate">
                                <input ref={input => this.datepickerFrom = input} type="text" name="created_from" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                                <input ref={input => this.datepickerTill = input} type="text" name="created_till" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                            </td>
                            <td>
                                <select name="deal_status" onChange={this.handleChangeFilter}>
                                    <option value="">Статус сделки</option>
                                    {
                                        deal_statuses.map(status =>
                                            status.code > 5 &&
                                            <option
                                                key={status.status_name_including_deal_type}
                                                value={status.status_including_deal_type}
                                            >
                                                {status.status_name_including_deal_type}
                                            </option>
                                        )
                                    }
                                </select>
                            </td>
                            <td>
                                <select name="dispute_status" onChange={this.handleChangeFilter}>
                                    <option value="">Статус спора</option>
                                    {
                                        dispute_statuses.map((status, i) =>
                                            <option
                                                key={i}
                                                value={status.status}
                                            >
                                                {status.status_name_including_deal_type}
                                            </option>
                                        )
                                    }
                                </select>
                            </td>
                            <td className="button-search-wrap">
                                <button className="ButtonSearch" onClick={this.getCollection} type="submit"><span>&nbsp;</span></button>
                            </td>
                        </tr>
                        <tr className="Head">
                            <td className="col col-number">
                                <LinkSort sortName="deal_number"
                                          label="ID"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                />
                            </td>
                            <td className="col col-name">
                                <LinkSort sortName="deal_name"
                                          label="Название"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                />
                            </td>
                            <td className="col">
                                <LinkSort sortName="author"
                                          label="Инициатор спора"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                />
                            </td>
                            <td className="col col-date">
                                <LinkSort sortName="created"
                                          label="Дата открытия спора"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                />
                            </td>
                            <td className="col col-status no-sort">Статус сделки</td>
                            <td className="col no-sort">Статус спора</td>
                            <td className="col-empty"></td>
                        </tr>
                        {
                            collectionList.length !== 0 &&
                            !table_isLoading &&
                            collectionList.map(collectionItem => {
                                const { deal_id, deal_number, deal_name, author, created, deal_status, is_close, status } = collectionItem;
                                const { status_name_including_deal_type } = deal_status;
                                const url = `/deal/show/${deal_id}`;

                                return (
                                    <tr className="active" key={deal_number} onClick={() => !isMobile && goToLink(url)}>
                                        <td className="col col-number">
                                            <div className="col_mobile-title">ID</div>
                                            <div className="col_mobile-content">{deal_number}</div>
                                            </td>
                                        <td className="col col-name">
                                            <div className="col_mobile-title">Название сделки</div>
                                            <div className="col_mobile-content" title={deal_name}>{deal_name}</div>
                                            </td>
                                        <td className="col col-role">
                                            <div className="col_mobile-title">Инициатор спора</div>
                                            <div className="col_mobile-content" title={author}>{author}</div>
                                        </td>
                                        <td className="col">
                                            <div className="col_mobile-title">Дата открытия спора</div>
                                            <div className="col_mobile-content">{created}</div>
                                        </td>
                                        <td className="col">
                                            <div className="col_mobile-title">Статус сделки</div>
                                            <div className="col_mobile-content">{status_name_including_deal_type}</div>
                                        </td>
                                        <td className="col">
                                            <div className="col_mobile-title">Статус спора</div>
                                            <div className="col_mobile-content">{status.name}</div>
                                        </td>
                                        {!isMobile && !isTablet ?
                                            <td className="col-empty"/>
                                            :
                                            <td className="to-deal-link-cell">
                                                <a href={url} className="ButtonApply" data-ripple-button="">
                                                    <span className="ripple-text">Перейти к сделке</span>
                                                </a>
                                            </td>
                                        }
                                    </tr>
                                );
                            })
                        }
                        {
                            (collectionList.length === 0 && collection.length !== 0 && !table_isLoading) &&
                            <tr className="EmptyRow">
                                <td colSpan="7">Сделки со спором не найдены.</td>
                            </tr>
                        }
                        {
                            collection.length === 0 &&
                            !table_isLoading &&
                            <tr className="EmptyRow">
                                <td colSpan="7">В системе нет сделок со спором.</td>
                            </tr>
                        }
                        {
                            table_isLoading &&
                            <tr className="EmptyRow">
                                <td colSpan="7" className='Preloader Preloader_solid'></td>
                            </tr>
                        }
                        </tbody>
                    </table>
                </div>
                <Paginator
                    paginator={paginator}
                    handlePageSwitch={this.handlePageSwitch}
                    per_page={per_page}
                    handlePerPageChange={this.handlePerPageChange}
                />
            </div>
        );
    }
}

export default withWindowDimensions(TableDealsDispute);