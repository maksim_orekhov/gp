import React from 'react';
import Datepicker from '../forms/base/Datepicker';
import LinkSort from '../forms/base/LinkSort';
import {
    sorting, goToLink, filterArray, filterCustomArray, filterCreatedDate, filterCreated,
    customFetch, getDeadlineDealsWithDateOfDeadlineInMillisecondsField
} from '../Helpers';
import Paginator from '../Paginator';
import TableBase from './TableBase';
import URLS from "../constants/urls";
import withWindowDimensions from '../hocs/withWindowDimensions';

class TableDealsExpired extends TableBase {
    constructor(props) {
        super(props);
        this.state = {
            collection: [],
            collectionFiltered: [],
            isFiltering: false,
            id: '',
            name: '',
            amount_without_fee: '',
            owner: '',
            created: '',
            sortName: '',
            sortDirection: '',
            paginator: {},
            page: 1,
            per_page: 20,
            table_isLoading: false
        };

        this.getCollection = this.getCollection.bind(this);
        this.handleChangeSort = this.handleChangeSort.bind(this);
        this.handleChangeFilter = this.handleChangeFilter.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handlePageSwitch = this.handlePageSwitch.bind(this);
        this.handlePerPageChange = this.handlePerPageChange.bind(this);
    }

    tableInit() {
        document.getElementById('TableDealsExpired').style.visibility = 'visible';
    }

    getCollection(isNeedToRunHandleFilter) {
        this.toggleIsLoading();
        const { page, per_page } = this.state;
        const params = { page, per_page };
        const url_params = this.setUrlParams(params);

        customFetch(`${URLS.DEAL.EXPIRED}?${url_params}`)
            .then(data => {
                const { status } = data;
                if (status === 'ERROR') {
                    this.toggleIsLoading();
                }
                if (status === 'SUCCESS') {
                    const { payments, paginator = {} } = data.data;
                    const paymentsWithDeadlineInMilliseconds = getDeadlineDealsWithDateOfDeadlineInMillisecondsField(payments);
                    console.log(data);
                    this.setState({
                        collection: paymentsWithDeadlineInMilliseconds,
                        paginator
                    }, () => {
                        this.toggleIsLoading();
                        isNeedToRunHandleFilter && this.handleFilter();
                    });
                }
            })
            .catch(() => {
                this.toggleIsLoading();
            });
    }

    handleFilter() {
        const { collection, id, name, amount_without_fee, owner, created_from_in_milliseconds, date_of_deadline_in_milliseconds, sortName, sortDirection } = this.state;
        const isFiltering =
            id !== ''
            || name !== ''
            || amount_without_fee !== ''
            || owner !== ''
            || created_from_in_milliseconds !== ''
            || date_of_deadline_in_milliseconds !== ''
            || sortName !== ''
        ;

        const getFilteredResults = (array) => {
            return array
                .filter(filterArray('id', id))
                .filter(filterCustomArray('deal', 'name', name))
                .filter(filterArray('amount_without_fee', amount_without_fee))
                .filter(filterCustomArray('deal', 'owner', owner))
                .filter(filterCreatedDate('created_milliseconds', created_from_in_milliseconds))
                .filter(filterCreated('date_of_deadline_in_milliseconds', date_of_deadline_in_milliseconds))
        };

        this.setState({
            collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
            isFiltering
        });
    }

    render() {
        const { collection, collectionFiltered, isFiltering, sortName, sortDirection, paginator, table_isLoading, per_page } = this.state;
        const collectionList = isFiltering ? collectionFiltered : collection;
        const { isMobile } = this.props;
        return (
            <div className="TableWithPagination">
                <div className="TableWrapper">
                    <div className="head-of-element">
                        <h3>К оплате по сроку</h3>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="col-xl-12">
                                <div className="row">
                                    <div className="col-xl-12 col-action-all">
                                        <div className="ButtonsRow">
                                            <a className="ButtonActionAll ButtonApply" href="/deal/expired/all">Обработать все</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table className="TableDefault TableDealsExpired js-table-rows-are-link">
                        <tbody>
                        <tr className="FilterTableRow">
                            <td>
                                <input type="text" name="id" placeholder="№" onChange={this.handleChangeFilter} />
                            </td>
                            <td>
                                <input type="text" name="name" value={this.state.name} placeholder="Поиск по названию" className="InputSearch" onChange={this.handleChangeFilter} />
                            </td>
                            <td>
                                <input type="text" name="amount_without_fee" placeholder="Поиск по сумме" className="InputSearch" onChange={this.handleChangeFilter} />
                            </td>
                            <td>
                                <input type="text" name="owner" placeholder="Поиск по владельцу" className="InputSearch" onChange={this.handleChangeFilter} />
                            </td>
                            <td className="InputDate">
                                <input ref={input => this.datepickerFrom = input} type="text" name="created" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                            </td>
                            <td className="InputDate">
                                <input ref={input => this.datepickerDeFactoFrom = input} type="text" name="date_of_deadline" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                            </td>
                            {/*<td>*/}
                            {/*<select name="deal_status" onChange={this.handleChangeFilter}>*/}
                            {/*<option value="">Статус</option>*/}
                            {/*<option value="negotiation">Ожидает согласования</option>*/}
                            {/*<option value="confirmed">Согласовано</option>*/}
                            {/*<option value="debit_matched">Оплачено</option>*/}
                            {/*<option value="dispute">Спор</option>*/}
                            {/*<option value="delivered">Товар доставлен</option>*/}
                            {/*<option value="pending_credit_payment">Ожидает выплаты</option>*/}
                            {/*<option value="closed">Закрыто</option>*/}
                            {/*<option value="refund">Возврат</option>*/}
                            {/*</select>*/}
                            {/*</td>*/}
                            <td/>
                        </tr>
                        <tr className="Head">
                            <td className="col col-number">
                                <LinkSort
                                    sortName="id"
                                    label="№"
                                    handleChangeSort={this.handleChangeSort}
                                    isActiveSortName={sortName}
                                    direction={sortDirection}
                                />
                            </td>
                            <td>
                                <LinkSort
                                    sortName="deal.name"
                                    label="Название"
                                    handleChangeSort={this.handleChangeSort}
                                    isActiveSortName={sortName}
                                    direction={sortDirection}
                                />
                            </td>
                            <td>
                                <LinkSort
                                    sortName="amount_without_fee"
                                    label="Сумма"
                                    handleChangeSort={this.handleChangeSort}
                                    isActiveSortName={sortName}
                                    direction={sortDirection}
                                />
                            </td>
                            <td>
                                <LinkSort
                                    sortName="owner"
                                    label="Владелец"
                                    handleChangeSort={this.handleChangeSort}
                                    isActiveSortName={sortName}
                                    direction={sortDirection}
                                />
                            </td>
                            <td>
                                <LinkSort
                                    sortName="deal.created_milliseconds"
                                    label="Дата создания"
                                    handleChangeSort={this.handleChangeSort}
                                    isActiveSortName={sortName}
                                    direction={sortDirection}
                                />
                            </td>
                            <td>
                                <LinkSort
                                    sortName="date_of_deadline_milliseconds"
                                    label="Срок выполнения"
                                    handleChangeSort={this.handleChangeSort}
                                    isActiveSortName={sortName}
                                    direction={sortDirection}
                                />
                            </td>
                            <td>
                                <LinkSort
                                    sortName="status"
                                    label="Состояние"
                                    handleChangeSort={this.handleChangeSort}
                                    isActiveSortName={sortName}
                                    direction={sortDirection}
                                />
                            </td>
                            <td/>
                        </tr>
                        {
                            collectionList.length !== 0 &&
                            collectionList.map(collectionItem => {
                                const { id, deal, amount_without_fee, date_of_delivery, date_of_deadline } = collectionItem;
                                const { name, owner, created } = deal;
                                const url = `/deal/show/${id}`;

                                return (
                                    <tr className="active" key={id} onClick={() => !isMobile && goToLink(url)}>
                                        <td className="col col-number" data-label="ID:">
                                            <div className="col_mobile-title">ID</div>
                                            <div className="col_mobile-content">{id}</div>
                                        </td>
                                        <td className="col">
                                            <div className="col_mobile-title">Название сделки</div>
                                            <div className="col_mobile-content" title={name}>{name}</div>
                                        </td>
                                        <td className="col" data-label="Сумма:">
                                            <div className="col_mobile-title">Сумма</div>
                                            <div className="col_mobile-content">{amount_without_fee}</div>
                                        </td>
                                        <td className="col" data-label="Владелец:">
                                            <div className="col_mobile-title">Владелец</div>
                                            <div className="col_mobile-content" title={owner}>{owner}</div>
                                        </td>
                                        <td className="col" data-label="Дата создания:">
                                            <div className="col_mobile-title">Дата создания</div>
                                            <div className="col_mobile-content">{created}</div>
                                        </td>
                                        <td className="col" data-label="Срок выполнения:">
                                            <div className="col_mobile-title">Срок выполнения</div>
                                            <div className="col_mobile-content">{date_of_deadline}</div>
                                        </td>
                                        <td className="col" data-label="Обработать:">
                                            <a href={`/deal/expired/${id}`}>обработать</a>
                                        </td>
                                        {!isMobile ?
                                            <td/>
                                            :
                                            <td className="to-deal-link-cell">
                                                <a href={url} className="ButtonApply" data-ripple-button="">
                                                    <span className="ripple-text">Перейти к сделке</span>
                                                </a>
                                            </td>
                                        }
                                    </tr>
                                );
                            })
                        }
                        {
                            (collectionList.length === 0 && collection.length !== 0 && !table_isLoading) &&
                            <tr className="EmptyRow">
                                <td colSpan="7">Сделки не найдены.</td>
                            </tr>
                        }
                        {
                            collection.length === 0 &&
                            !table_isLoading &&
                            <tr className="EmptyRow">
                                <td colSpan="7">В системе нет сделок к оплате по сроку.</td>
                            </tr>
                        }
                        {
                            table_isLoading &&
                            <tr className="EmptyRow">
                                <td colSpan="7" className='Preloader Preloader_solid' />
                            </tr>
                        }
                        </tbody>
                    </table>
                </div>
                <Paginator
                    paginator={paginator}
                    handlePageSwitch={this.handlePageSwitch}
                    handlePerPageChange={this.handlePerPageChange}
                    per_page={per_page}
                />
            </div>
        );
    }
}

export default withWindowDimensions(TableDealsExpired);