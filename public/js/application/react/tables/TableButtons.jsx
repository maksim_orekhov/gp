import React from 'react';
import FormButton from '../forms/FormButton';
import LinkSort from '../forms/base/LinkSort';
import Popup from '../Popup';
import { sorting, changeSortDirection, addSpacesBetweenThousands, filterArrayMultiple, replaceRusLetterE, customFetch } from '../Helpers';
import URLS from "../constants/urls";
import PropTypes from 'prop-types';

export default class TableButtons extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collection: [],
            collectionFiltered: [],
            collectionItemEdit: {},
            isOpenedFormCreate: false,
            isOpenedFormEdit: false,
            isNewItem: false,
            table_isLoading: false,
            // filters
            isFiltering: false,
            filter_value: '',
            // sort
            sortName: '',
            sortDirection: ''
        };

        this.updateCollection = this.updateCollection.bind(this);
        this.updateCollectionAfterDelete = this.updateCollectionAfterDelete.bind(this);
        this.handleChangeFilter = this.handleChangeFilter.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.clearFilters = this.clearFilters.bind(this);
        this.handleChangeSort = this.handleChangeSort.bind(this);
        this.toggleFormCreate = this.toggleFormCreate.bind(this);
        this.toggleFormEdit = this.toggleFormEdit.bind(this);
        this.handleClickFilter = this.handleClickFilter.bind(this);
    }

    componentDidMount() {
        this.initDatepicker();
        this.setCollection(this.props.tokens);
    }

    componentWillReceiveProps(nextProps) {
        this.setCollection(nextProps.tokens);
        const addedButtonId = window.sessionStorage.getItem('addedButtonId');
        if (nextProps.tokens && nextProps.tokens.length && addedButtonId) {
            const addedButton = nextProps.tokens.find(token => token.id === Number(addedButtonId));
            if (addedButton) {
                const newState = {
                    collectionItemEdit: addedButton,
                    isOpenedFormEdit: true
                };
                window.sessionStorage.removeItem('addedButtonId');
                this.setState(newState);
            }
        }
    }

    setCollection(tokens) {
        const collection = tokens.map((token) => {
            let role;
            switch (token.deal_role) {
                case 'customer':
                    role = 'Заказчик';
                    break;
                case 'buyer':
                    role = 'Покупатель';
                    break;
                case 'contractor':
                    role = 'Исполнитель';
                    break;
                case 'seller':
                    role = 'Продавец';
                    break;
                default:
                    role = '';
            }
            return {
                ...token,
                role
            }
        });
        this.setState({ collection });
    }

    updateCollection() {
        if (this.props.is_nested) {
            this.setState({
                isOpenedFormEdit: false
            }, () => {
                this.clearFilters();
            });
            this.props.updateCollection(false, true);
        } else {
            customFetch(URLS.NATURAL_PERSON.SINGLE)
                .then(data => {
                    console.log(data);
                    this.setState({
                        collection: [
                            {
                                id: 1,
                                name: 'Продажа часов на Луне',
                                created: '17.08.2018',
                                is_active: true,
                                deal_type: 'Услуга',
                                deal_name: 'Сделка с субъектом пользователя test',
                                deal_description: null,
                                amount: null,
                                delivery_period: null,
                                fee_payer_option_id: null,
                                token_key: 'arenda_specztechniki',
                                beneficiar_user: null,
                                beneficiar_civil_law_subject: null,
                                counteragent_user: {
                                    id: 1,
                                    login: 'test',
                                },
                                counteragent_civil_law_subject: {
                                    type: 'sole_proprietor',
                                    id: 5,
                                    name: 'ИП Месси Леонель Аргентинович'
                                },
                                deals: []
                            }
                        ],
                    }, () => {
                        this.clearFilters();
                        this.highlightNewItem();
                    });
                });
        }
    }

    updateCollectionAfterDelete() {
        if (this.props.is_nested) {
            this.setState({
                isOpenedFormEdit: false
            }, () => {
                this.clearFilters();
            });
            this.props.updateCollection(false, false);
        } else {
            customFetch(URLS.BUTTON.FORM_INIT)
                .then(data => {
                    console.log(data);
                    this.setState({
                        isOpenedFormEdit: false,
                        collection: [
                            {
                                id: 1,
                                name: 'Продажа часов на Луне',
                                created: '17.08.2018',
                                is_active: true,
                                deal_type: 'Услуга',
                                deal_name: 'Сделка с субъектом пользователя test',
                                deal_description: null,
                                amount: null,
                                delivery_period: null,
                                fee_payer_option_id: null,
                                token_key: 'arenda_specztechniki',
                                beneficiar_user: null,
                                beneficiar_civil_law_subject: null,
                                counteragent_user: {
                                    id: 1,
                                    login: 'test',
                                },
                                counteragent_civil_law_subject: {
                                    type: 'sole_proprietor',
                                    id: 5,
                                    name: 'ИП Месси Леонель Аргентинович'
                                },
                                deals: []
                            }
                        ]
                    }, () => {
                        this.clearFilters();
                    });
                });
        }
    }

    highlightNewItem() {
        this.setState({ isNewItem: true });
        setTimeout(() => {
            this.setState({ isNewItem: false });
        }, 3000);
    }

    initDatepicker() {
        $(this.datepicker).datepicker({
            onSelect: formattedDate => {
                this.setState({
                    birthDate: formattedDate
                }, () => this.handleFilter());
            }
        });
    }

    handleChangeFilter(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: replaceRusLetterE(value.trim())
        }, () => this.handleFilter());
    }

    handleFilter() {
        const { collection, filter_value, sortName, sortDirection } = this.state;
        const isFiltering =
            filter_value !== ''
            || sortName !== ''
        ;

        const getFilteredResults = (array) => {
            return Object.values(array).filter(filterArrayMultiple([
                'id',
                'name',
                'created',
                'created',
            ], filter_value));
        };

        this.setState({
            isNewItem: false,
            collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
            isFiltering
        });
    }

    clearFilters() {
        this.setState({
            collectionFiltered: [],
            isFiltering: false,
            isOpenedFormCreate: false,
            filter_value: '',
            sortName: '',
            sortDirection: ''
        });
    }

    handleChangeSort(name) {
        const { sortName, sortDirection } = this.state;
        this.setState({
            sortName: name,
            sortDirection: changeSortDirection(name, sortName, sortDirection)
        }, () => this.handleFilter());
    }

    toggleFormCreate() {
        this.setState({
            isOpenedFormCreate: !this.state.isOpenedFormCreate
        });
    }

    toggleFormEdit(collectionItem) {
        this.setState({
            collectionItemEdit: collectionItem,
            isOpenedFormEdit: !this.state.isOpenedFormEdit
        });
    }

    toggleIsLoading() {
        this.setState({
            table_isLoading: !this.state.table_isLoading
        });
    }

    handleClickFilter() {
        window.location = '/profile'
    }

    handleChangeTokenActivity = (tokenId) => () => {
        const { collection, collectionItemEdit } = this.state;
        const { csrf } = this.props;
        const tokenIndex = collection.findIndex((collectionItem) => collectionItem.id === tokenId);
        if (tokenIndex < 0) {
            return false;
        }
        const nextCollection = [...collection];
        nextCollection[tokenIndex] = {
            ...nextCollection[tokenIndex],
            is_active: !nextCollection[tokenIndex]['is_active'],
        };
        this.setState({
            collection: nextCollection,
            collectionItemEdit: {
                ...collectionItemEdit,
                is_active: nextCollection[tokenIndex]['is_active'],
            }
        }, () => {
            customFetch(URLS.BUTTON.SWITCH_ACTIVE, {
                body: JSON.stringify({ csrf, idToken: nextCollection[tokenIndex]['id'], is_active: nextCollection[tokenIndex]['is_active'] }),
                method: 'POST'
            }).then((response) => {
                if (response.status.toLowerCase() === 'error') {
                    this.setState({ collection, collectionItemEdit });
                }
            }).catch((error) => {
                this.setState({ collection, collectionItemEdit });
            });
        });
    };

    render() {
        const { isOperator, is_nested, fee_payer_options } = this.props;
        const { collection, collectionFiltered, isFiltering, sortName, sortDirection, isOpenedFormEdit, collectionItemEdit } = this.state;
        const collectionList = isFiltering ? collectionFiltered : collection;

        return (
            <div>
                {
                    !isOperator &&
                    <div className="TableWrapper">
                        <table className="TableDefault TableButtons js-table-rows-are-link">
                            <tbody>
                            <tr className="FilterRowButtonMobile">
                                <td>
                                    <a className="ButtonApply FilterButton"
                                       href={URLS.BUTTON.CREATE}
                                       title="Добавить кнопку"
                                       data-ripple-button="">
                                        Создать
                                    </a>
                                </td>
                                <td/>
                                <td />
                                <td />
                            </tr>
                            <tr className="FilterTableRow js-filter-table-mobile">
                                <td colSpan="2">
                                    <a className="ButtonApply" href="profile/partner-button/create" title="Добавить кнопку" data-ripple-button="">
                                        Создать
                                    </a>
                                </td>
                                <td className="head-of-element">
                                    <h3>Фильтр</h3>
                                </td>
                                <td className="col-name">
                                    <input className="InputSearch" type="text" name="filter_value" value={this.state.filter_value} placeholder="Поиск" onChange={this.handleChangeFilter} />
                                </td>
                                <td />
                                <td />
                            </tr>
                            <tr className="Head">
                                <td>
                                    <LinkSort sortName="id"
                                              label="ID"
                                              handleChangeSort={this.handleChangeSort}
                                              isActiveSortName={sortName}
                                              direction={sortDirection}
                                    />
                                </td>


                                <td className="col">
                                    <LinkSort sortName="is_active"
                                              label="Кнопка активна"
                                              handleChangeSort={this.handleChangeSort}
                                              isActiveSortName={sortName}
                                              direction={sortDirection}
                                    />
                                </td>


                                <td className="col col-name">
                                    <LinkSort sortName="name"
                                              label="Название кнопки"
                                              handleChangeSort={this.handleChangeSort}
                                              isActiveSortName={sortName}
                                              direction={sortDirection}
                                    />
                                </td>
                                <td>
                                    <LinkSort sortName="role"
                                              label="Роль в сделке"
                                              handleChangeSort={this.handleChangeSort}
                                              isActiveSortName={sortName}
                                              direction={sortDirection}
                                    />
                                </td>
                                <td className="col col-date">
                                    <LinkSort sortName="date_of_creation"
                                              label="Дата создания"
                                              handleChangeSort={this.handleChangeSort}
                                              isActiveSortName={sortName}
                                              direction={sortDirection}
                                    />
                                </td>
                                <td>
                                    <LinkSort sortName="deals_count"
                                              label="Проведено сделок"
                                              handleChangeSort={this.handleChangeSort}
                                              isActiveSortName={sortName}
                                              direction={sortDirection}
                                    />
                                </td>
                                <td>
                                    <LinkSort sortName="deals_amount"
                                              label="На сумму, руб."
                                              handleChangeSort={this.handleChangeSort}
                                              isActiveSortName={sortName}
                                              direction={sortDirection}
                                    />
                                </td>
                            </tr>
                            {
                                collectionList.length !== 0 &&
                                Object.values(collectionList).map((collectionItem, index) => {
                                    const { id, created, deals = [], name, beneficiar_user, is_active, deals_sum, deals_count, role } = collectionItem;

                                    return (
                                        <tr key={id} onClick={() => this.toggleFormEdit(collectionItem)} className={`active ${is_nested ?
                                            this.props.isNewItem && index === collectionList.length - 1 ? 'highlighted' : ''
                                            :
                                            this.state.isNewItem && index === 0 ? 'highlighted' : '' }`}>
                                            <td className="col col-number">
                                                <div className="col_mobile-content">{id}</div>
                                            </td>
                                            <td className={`col col-button-check-active ${is_active ? 'is-active' : 'not-active'}`}>
                                                <div className="col_mobile-title">Кнопка активна</div>
                                                <div className="col_mobile-content">{is_active ? 'Да' : 'Нет'}</div>
                                            </td>
                                            <td className="col col-name">
                                                <div className="col_mobile-title">Название</div>
                                                <div className="col_mobile-content">{name}</div>
                                            </td>
                                            <td className="col">
                                                <div className="col_mobile-title">Роль</div>
                                                <div className="col_mobile-content">
                                                    {
                                                        beneficiar_user !== null ?
                                                            'Реферер'
                                                            :
                                                            role
                                                    }
                                                </div>
                                            </td>
                                            <td className="col col-date">
                                                <div className="col_mobile-title">Дата создания</div>
                                                <div className="col_mobile-content">{created}</div>
                                            </td>
                                            <td className="col">
                                                <div className="col_mobile-title">Проведено сделок</div>
                                                <div className="col_mobile-content">{deals_count || 0}</div>
                                            </td>
                                            <td className="col">
                                                <div className="col_mobile-title">На сумму, руб.</div>
                                                <div className="col_mobile-content">{deals_sum ? addSpacesBetweenThousands(deals_sum) : 0}</div>
                                            </td>
                                        </tr>
                                    );
                                })
                            }
                            {
                                (isFiltering && collectionList.length === 0 && collection.length !== 0) &&
                                <tr className="EmptyRow">
                                    <td colSpan="7">Кнопки не найдены.</td>
                                </tr>
                            }
                            {/*{*/}
                                {/*collection.length === 0 &&*/}
                                {/*<tr className="EmptyRow">*/}
                                    {/*<td colSpan="6" className={!is_nested && this.state.table_isLoading ? 'Preloader Preloader_solid' : null}>Вы еще не добавили в свой профиль ни одной кнопки.</td>*/}
                                {/*</tr>*/}
                            {/*}*/}
                            {
                                collection.length === 0 &&
                                <tr className="EmptyRow">
                                    <td colSpan="7" className={is_nested ? this.props.tableIsLoading ? 'Preloader Preloader_solid' : null
                                        : this.state.table_isLoading ? 'Preloader Preloader_solid' : null}>Вы еще не добавили в свой профиль ни одной кнопки.</td>
                                </tr>
                            }
                            </tbody>
                        </table>

                    </div>
                }

                {
                    isOpenedFormEdit &&
                    <Popup close={() => this.toggleFormEdit({})} popupClassName='button-details'>
                        <FormButton
                            data={collectionItemEdit}
                            editMode={true}
                            afterSubmit={this.updateCollectionAfterDelete}
                            afterDelete={this.updateCollectionAfterDelete}
                            item_id={collectionItemEdit}
                            fee_payer_options={fee_payer_options}
                            changeActivityHandler={this.handleChangeTokenActivity}
                        />
                    </Popup>
                }
            </div>
        );
    }
}

TableButtons.propTypes = {
    tableIsLoading: PropTypes.bool,
    isOperator: PropTypes.bool,
    isNewItem: PropTypes.bool,
    naturalPersons: PropTypes.array,
    is_nested: PropTypes.bool
};
