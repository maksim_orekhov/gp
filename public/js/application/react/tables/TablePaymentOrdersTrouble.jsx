import React from 'react';
import Datepicker from '../forms/base/Datepicker';
import LinkSort from '../forms/base/LinkSort';
import {
    sorting, goToLink, filterCreatedFrom, filterArray, getPaymentOrdersWithCreatedInMillisecondsField, filterCreatedTill, filterCreatedDate, customFetch,
} from '../Helpers';
import Paginator from '../Paginator';
import TableBase from './TableBase';
import URLS from '../constants/urls';

export default class TablePaymentOrdersTrouble extends TableBase {
   constructor(props) {
      super(props);
      this.state = {
         collection: [],
         collectionFiltered: [],
         abnormal_types: [],
         isFiltering: false,
         abnormal_type: '',
         sortName: '',
         sortDirection: '',
         paginator: {},
         page: 1,
         per_page: 20,
         name: '',
         created_from: '',
         id: '',
         de_facto_date_from: '',
         planned_date: '',
         created_from_in_milliseconds: '',
         de_facto_date_from_in_milliseconds: '',
         planned_date_in_milliseconds: '',
         created_till: '',
         customer: '',
         contractor: '',
         table_isLoading: false
      };

      this.getCollection = this.getCollection.bind(this);
      this.handleChangeFilter = this.handleChangeFilter.bind(this);
      this.handleChangeSort = this.handleChangeSort.bind(this);
      this.handleFilter = this.handleFilter.bind(this);
      this.handlePageSwitch = this.handlePageSwitch.bind(this);
      this.handlePerPageChange = this.handlePerPageChange.bind(this);
   }

    tableInit() {
        document.getElementById('TablePaymentOrdersTrouble').style.visibility = 'visible';
    }

   getCollection(isNeedToRunHandleFilter) {
       this.toggleIsLoading();
       customFetch(URLS.BANK_CLIENT.PAYMENT_ORDERS_TROUBLE, {
           method: 'GET'
       })
           .then(data => {
               const { status } = data;
               if (status === 'ERROR') {
                   this.toggleIsLoading();
               }
               if (status === 'SUCCESS') {
                   const { payments, paginator = {} } = data.data;
                   const payments_with_created_value = getPaymentOrdersWithCreatedInMillisecondsField(payments);
                   console.log(data);
                   this.setState({
                       collection: payments_with_created_value,
                       isFiltering: false,
                       paginator
                   }, () => {
                       this.toggleIsLoading();
                       isNeedToRunHandleFilter && this.handleFilter();
                       if (this.state.collection.length === 0) {
                           this.setState({
                               isFiltering: true
                           })
                       }
                   });
               }
           })
           .catch(() => {
               this.toggleIsLoading();
           });
   }

   handleFilter() {
       const { collection, id, amount, sortName, sortDirection, payment_purpose, created_from_in_milliseconds, created_till_in_milliseconds, recipient_account, payer_inn, payer_account } = this.state;
       const isFiltering =
           id !== ''
           || amount !== ''
           || sortName !== ''
           || payment_purpose !== ''
           || created_from_in_milliseconds !== ''
           || created_till_in_milliseconds !== ''
           || recipient_account !== ''
           || payer_inn !== ''
           || payer_account !== ''
       ;
       const getFilteredResults = (array) => {

           return array
               .filter(filterArray('id', id))
               .filter(filterArray('payment_purpose', payment_purpose))
               .filter(filterArray('amount', amount))
               .filter(filterArray('recipient_account', recipient_account))
               .filter(filterArray('payer_inn', payer_inn))
               .filter(filterArray('payer_account', payer_account))
               .filter(filterCreatedFrom('created_milliseconds', created_from_in_milliseconds))
               .filter(filterCreatedTill('created_milliseconds', created_till_in_milliseconds ))
       };

       this.setState({
           collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
           isFiltering
       });
   }

   render() {
      const { collection, collectionFiltered, isFiltering, sortName, sortDirection, paginator, table_isLoading } = this.state;
      const collectionList = isFiltering ? collectionFiltered : collection;

      return (
         <div className='TableWithPagination'>
            <div className='TableWrapper'>
            <table className='TableDefault TablePaymentOrdersTrouble js-table-rows-are-link'>
               <tbody>
               <tr className="table-head">
                   <td className="head-of-element">
                       <h3>Платежки проблемные</h3>
                   </td>
               </tr>
                  <tr className='FilterTableRow'>
                      <td>
                         <span className='TableDisputes-OperatorCol'>
                            <input type='text' name='id' placeholder='Номер' className='InputSearch' onChange={this.handleChangeFilter} />
                         </span>
                      </td>
                      <td>
                          <input type='text' name='payment_purpose' placeholder='Назначение платежа' className='InputSearch' onChange={this.handleChangeFilter} />
                      </td>
                      <td>
                          <input type='text' name='amount' placeholder='Сумма' className='InputSearch' onChange={this.handleChangeFilter} />
                      </td>
                      <td>
                          <div className="DoubleInputDate">
                              <input ref={input => this.datepickerFrom = input} type='text' name='created_from' className='InputDate2 datepicker-here' onChange={this.handleChangeFilter} data-auto-close='true' />
                              <input ref={input => this.datepickerTillUsers = input} type='text' name='created_till' className='InputDate2 datepicker-here' onChange={this.handleChangeFilter} data-auto-close='true' />
                          </div>
                      </td>
                      <td>
                          <input type='text' name='recipient_account' placeholder='Счет получателя' className='InputSearch' onChange={this.handleChangeFilter} />
                      </td>
                      <td>
                          <input type='text' name='payer_inn' placeholder='Плательщик' className='InputSearch' onChange={this.handleChangeFilter} />
                      </td>
                      <td>
                          <input type='text' name='payer_account' placeholder='Счет плательщика' className='InputSearch' onChange={this.handleChangeFilter} />
                      </td>
                     {/*<td>*/}
                        {/*<select name='trouble_reasons' id='trouble_reasons' className='form-control' onChange={this.handleChangeFilter}>*/}
                           {/*<option value=''>Причина</option>*/}
                           {/*<option>Нет привязки к файлу</option>*/}
                           {/*<option>Не вошла ни в одну коллекцию</option>*/}
                        {/*</select>*/}
                     {/*</td>*/}
                      <td></td>
                     <td className="button-search-wrap">
                        <button className='ButtonSearch' type='submit'><span>&nbsp;</span></button>
                     </td>
                  </tr>
                  <tr className='Head'>
                     <td className="col col-number">
                         <LinkSort
                           sortName='id'
                           label='ID'
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                         />
                     </td>
                     <td>
                        <LinkSort
                           sortName='payment_purpose'
                           label='Назначение платежа'
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>
                        <LinkSort
                           sortName='amount'
                           label="Сумма"
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>
                        <LinkSort
                           sortName="created"
                           label="Создан"
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>
                        <LinkSort
                           sortName="recipient_account"
                           label="Счет получателя"
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>
                        <LinkSort
                           sortName="payer_inn"
                           label="Плательщик"
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>
                        <LinkSort
                           sortName="payer_account"
                           label="Счет плательщика"
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>Причина</td>
                     <td></td>
                  </tr>
                  {
                     collectionList.length !== 0 &&
                     collectionList.map(collectionItem => {
                        const { id, payment_purpose, amount, created, recipient_account, payer, payer_account, trouble_reasons } = collectionItem;
                        const url = `/deal/show/${id}`;

                        return (
                           <tr className='active' key={id} onClick={() => goToLink(url)}>
                              <td className="col col-number" data-label='ID:'>
                                  <div className="col_mobile-title">ID</div>
                                  <div className="col_mobile-content">{id}</div>
                              </td>
                              <td className="col">
                                  <div className="col_mobile-title">Назначение платежа</div>
                                  <div className="col_mobile-content">{payment_purpose}</div>
                              </td>
                              <td className="col" data-label='Сумма:'>
                                  <div className="col_mobile-title">Сумма</div>
                                  <div className="col_mobile-content">{amount}</div>
                              </td>
                              <td className="col" data-label='Создан:'>
                                  <div className="col_mobile-title">Создан</div>
                                  <div className="col_mobile-content">{created}</div>
                              </td>
                              <td className="col" data-label='Счет получателя:'>
                                  <div className="col_mobile-title">Счет получателя</div>
                                  <div className="col_mobile-content">{recipient_account}</div>
                              </td>
                              <td className="col" data-label='Плательщик:'>
                                  <div className="col_mobile-title">Плательщик</div>
                                  <div className="col_mobile-content">{payer}</div>
                              </td>
                              <td className="col" data-label='Счет плательщика:'>
                                  <div className="col_mobile-title">Счет плательщика</div>
                                  <div className="col_mobile-content">{payer_account}</div>
                              </td>
                              <td className="col" data-label='Причина:'>
                                  <div className="col_mobile-title">Причина</div>
                                  <div className="col_mobile-content">{trouble_reasons}</div>
                              </td>
                             <td></td>
                           </tr>
                        );
                     })
                  }
                  {
                     (collectionList.length === 0 && collection.length !== 0 && !table_isLoading) &&
                     <tr className="EmptyRow">
                        <td colSpan="9">Сделки с проблемами не найдены.</td>
                     </tr>
                  }
                  {
                     collection.length === 0 &&
                     !table_isLoading &&
                     <tr className="EmptyRow">
                        <td colSpan="9">В системе нет проблемных сделок.</td>
                     </tr>
                  }
                  {
                      table_isLoading &&
                      <tr className="EmptyRow">
                          <td colSpan="9" className='Preloader Preloader_solid' />
                      </tr>
                  }
               </tbody>
            </table>
         </div>
            <Paginator
                paginator={paginator}
                handlePageSwitch={this.handlePageSwitch}
                handlePerPageChange={this.handlePerPageChange}
            />
         </div>
      );
   }
}
