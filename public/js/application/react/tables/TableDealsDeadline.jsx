import React from 'react';
import Datepicker from '../forms/base/Datepicker';
import LinkSort from '../forms/base/LinkSort';
import {
    sorting, goToLink, filterArray, getDeadlineDealsWithCreatedInMillisecondsField, filterCreatedFrom, filterCreated,
    customFetch, getDisputesWithFieldsSplittedByDealTypes
} from '../Helpers';
import Paginator from '../Paginator';
import TableBase from "./TableBase";
import URLS from "../constants/urls";

export default class TableDealsDeadline extends TableBase {
   constructor(props) {
      super(props);
      this.state = {
         collection: [],
         collectionFiltered: [],
         isFiltering: false,
         id: '',
         name: '',
         amount_without_fee: '',
         owner: '',
         created: '',
         sortName: '',
         sortDirection: '',
         paginator: {},
         page: 1,
         per_page: 20,
         table_isLoading: false
      };

      this.getCollection = this.getCollection.bind(this);
      this.handleChangeSort = this.handleChangeSort.bind(this);
      this.handleChangeFilter = this.handleChangeFilter.bind(this);
      this.handleFilter = this.handleFilter.bind(this);
      this.handlePageSwitch = this.handlePageSwitch.bind(this);
      this.handlePerPageChange = this.handlePerPageChange.bind(this);
   }

    tableInit() {
        document.getElementById('TableDealsDeadline').style.visibility = 'visible';
    }

   getCollection(isNeedToRunHandleFilter) {
       const { page, csrf, per_page } = this.state;
       const filter = this.setFilterDataToSend();

       this.toggleIsLoading();
       $.ajax({
           dataType: "json",
           type: 'GET',
           data: {
               page,
               per_page,
               csrf,
               filter
           },
           url: URLS.DEAL.DEADLINE,
           success: (data) => {
               const { deadlineDeals, paginator = {} } = data.data;
               const deadline_deals = getDeadlineDealsWithCreatedInMillisecondsField(deadlineDeals);
               console.log(data);
               this.setState({
                   collection: deadline_deals,
                   paginator
               }, () => {
                   this.toggleIsLoading();
                   isNeedToRunHandleFilter && this.handleFilter();
               });
           }
       }).fail(() => this.toggleIsLoading());
   }

    setFilterDataToSend() {
        const { name, id, created_from, amount_without_fee, owner, created_till } = this.state;
        const filter = {
            deal_id: id,
            deal_name: name,
            deal_created_from: created_from,
            deal_created_till: created_till,
            amount_without_fee,
            owner
        };

        return filter;
    }

   handleFilter() {
      const { collection, id, name, amount_without_fee, owner, created_from_in_milliseconds, de_facto_date_from_in_milliseconds, sortName, sortDirection } = this.state;
      const isFiltering =
         id !== ''
         || name !== ''
         || amount_without_fee !== ''
         || owner !== ''
         || created_from_in_milliseconds !== ''
         || de_facto_date_from_in_milliseconds !== ''
         || sortName !== ''
         ;

      const getFilteredResults = (array) => {
         return array
            .filter(filterArray('id', id))
            .filter(filterArray('name', name))
            .filter(filterArray('amount_without_fee', amount_without_fee))
            .filter(filterArray('owner', owner))
            .filter(filterCreated('created_milliseconds', created_from_in_milliseconds))
            .filter(filterCreated('date_of_deadline_in_milliseconds', de_facto_date_from_in_milliseconds))
      };

      this.setState({
         collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
         isFiltering
      });
   }

   render() {
      const { collection, collectionFiltered, isFiltering, sortName, sortDirection, paginator, table_isLoading, per_page } = this.state;
      const collectionList = isFiltering ? collectionFiltered : collection;
      return (
          <div className="TableWithPagination">
              <div className="TableWrapper">
                  <table className="TableDefault TableDealsExpired js-table-rows-are-link">
                     <tbody>
                     <tr className="FilterTableRow">
                        <td>
                           <input type="text" name="id" placeholder="№" onChange={this.handleChangeFilter} />
                        </td>
                        <td>
                           <input type="text" name="name" value={this.state.name} placeholder="Поиск по названию" className="InputSearch" onChange={this.handleChangeFilter} />
                        </td>
                        <td>
                           <input type="text" name="amount_without_fee" placeholder="Поиск по сумме" className="InputSearch" onChange={this.handleChangeFilter} />
                        </td>
                        <td>
                           <input type="text" name="owner" placeholder="Поиск по владельцу" className="InputSearch" onChange={this.handleChangeFilter} />
                        </td>
                        <td className="InputDate">
                           <input ref={input => this.datepickerFrom = input} type="text" name="created" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                        </td>
                        <td className="InputDate">
                           <input ref={input => this.datepickerDeFactoFrom = input} type="text" name="date_of_deadline" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                        </td>
                     </tr>
                     <tr className="Head">
                        <td className="col col-number">
                           <LinkSort
                              sortName="id"
                              label="№"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort
                              sortName="name"
                              label="Название"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort
                              sortName="amount_without_fee"
                              label="Сумма"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort
                              sortName="owner"
                              label="Владелец"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort
                              sortName="created_milliseconds"
                              label="Дата создания"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort
                              sortName="date_of_deadline_in_milliseconds"
                              label="Срок выполнения"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort
                              sortName="deadline"
                              label="Оставшийся срок"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                     </tr>
                     {
                        collectionList.length !== 0 &&
                        collectionList.map(collectionItem => {
                           const { id, amount_without_fee, date_of_deadline, name, created, owner, deadline, date_of_deadline_params } = collectionItem;
                           const url = `/deal/show/${id}`;

                           return (
                              <tr className="active" key={id} onClick={() => goToLink(url)}>
                                 <td className="col col-number" data-label="ID:">
                                     <div className="col_mobile-title">ID</div>
                                     <div className="col_mobile-content">{id}</div>
                                 </td>
                                 <td className="col">
                                     <div className="col_mobile-title">Название</div>
                                     <div className="col_mobile-content" title={name}>{name}</div>
                                 </td>
                                 <td className="col" data-label="Сумма:">
                                     <div className="col_mobile-title">Сумма</div>
                                     <div className="col_mobile-content">{amount_without_fee}</div>
                                 </td>
                                 <td className="col" data-label="Владелец:">
                                     <div className="col_mobile-title">Владелец</div>
                                     <div className="col_mobile-content" title={owner}>{owner}</div>
                                 </td>
                                 <td className="col" data-label="Дата создания:">
                                     <div className="col_mobile-title">Дата создания</div>
                                     <div className="col_mobile-content">{created}</div>
                                 </td>
                                 <td className="col" data-label="Срок выполнения:">
                                     <div className="col_mobile-title">Срок выполнения</div>
                                     <div className="col_mobile-content">{date_of_deadline}</div>
                                 </td>
                                 <td className="col" data-label="Оставшийся срок:">
                                     <div className="col_mobile-title">Оставшийся срок</div>
                                     <div className="col_mobile-content">{`${date_of_deadline_params.days}д.${date_of_deadline_params.hours}ч.${date_of_deadline_params.i}м.`}</div>
                                 </td>
                              </tr>
                           );
                        })
                     }
                     {
                        (collectionList.length === 0 && collection.length !== 0 && !table_isLoading) &&
                        <tr className="EmptyRow">
                           <td colSpan="7">Сделки не найдены.</td>
                        </tr>
                     }
                     {
                        collection.length === 0 &&
                        !table_isLoading &&
                        <tr className="EmptyRow">
                           <td colSpan="7">В системе нет сделок к оплате по сроку.</td>
                        </tr>
                     }
                     {
                         table_isLoading &&
                         <tr className="EmptyRow">
                             <td colSpan="7" className='Preloader Preloader_solid' />
                         </tr>
                     }
                  </tbody>
                  </table>
              </div>
              <Paginator
                  paginator={paginator}
                  handlePageSwitch={this.handlePageSwitch}
                  handlePerPageChange={this.handlePerPageChange}
                  per_page={per_page}
              />
          </div>
      );
   }
}
