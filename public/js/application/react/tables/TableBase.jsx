import React from 'react';
import { changeSortDirection, replaceRusLetterE } from '../Helpers';
import MESSAGES from '../constants/messages';
import { customFetch } from '../Helpers';
import URLS from '../constants/urls';

export default class TableBase extends React.Component {
    componentDidMount() {
        this.getCsrf();
        this.getCollection();
        this.tableInit();
        this.initDatepickerFrom();
        this.initDatepickerTill();
        this.initDatepickerClosedFrom();
        this.initDatepickerClosedTill();
        this.initDatepickerDeFactoFrom();
        this.initDatepickerPlannedDateFrom();
        this.initDatepickerTillUsers();
    }

    getCsrf() {
        customFetch(URLS.CSRF.GET)
            .then((data) => {
                console.log(data);
                if (data.status === 'SUCCESS') {
                    const { csrf } = data.data;
                    this.setState({
                        csrf
                    });
                } else if (data.status === 'ERROR') {
                    console.log('Ошибка при получении csrf');
                    return Promise.reject('Ошибка инициализации. Попробуйте обновить страницу.');
                }
            })
            .catch(error => this.setState({formError: error}));
    }

    datepickerDateToString(date) {
        const days = ('0' + date.getDate()).slice(-2);
        const months = ('0' + (date.getMonth() + 1)).slice(-2);

        const str = `${days}.${months}.${date.getFullYear()}`;
        return str;
    }

    initDatepickerFrom() {
        this.$datepickerFrom = $(this.datepickerFrom).datepicker({
            onSelect: (formattedDate, date) => {
                let newDateInSeconds = '', strDate = '';
                if (date) {
                    const newDate = new Date(date);
                    newDateInSeconds = newDate.getTime() / 1000;

                    strDate = this.datepickerDateToString(date);
                }
                this.setState({
                    created_from_in_milliseconds: newDateInSeconds,
                    created_from: strDate
                }, () => this.handleFilter());
            }
        }).data('datepicker');
    }

    initDatepickerTill() {
        this.$datepickerTill = $(this.datepickerTill).datepicker({
            onSelect: (formattedDate, date = '') => {
                let newDateInSeconds = '', strDate = '';
                if (date) {
                    const newDate = new Date(date);
                    newDateInSeconds = newDate.getTime() / 1000 + 86400; // + 86400 секунд (сутки) для того, чтобы был запас по времени, иначе некоторые сделки исчезают, когда выбираем фильтр "ДО" равный дате создания сделки

                    strDate = this.datepickerDateToString(date);
                }
                this.setState({
                    created_till_in_milliseconds: newDateInSeconds,
                    created_till: strDate,
                }, () => this.handleFilter());
            }
        }).data('datepicker');
    }

    initDatepickerTillUsers() {
        this.$datepickerTillUsers = $(this.datepickerTillUsers).datepicker({
            onSelect: (formattedDate, date = '') => {
                let newDateInSeconds = '', strDate = '';
                if (date) {
                    const newDate = new Date(date);
                    newDateInSeconds = newDate.getTime() / 1000;

                    strDate = this.datepickerDateToString(date);
                }
                this.setState({
                    created_till_in_milliseconds: newDateInSeconds,
                    created_till: strDate,
                }, () => this.handleFilter());
            }
        }).data('datepicker');
    }

    initDatepickerClosedFrom() {
        $(this.datepickerClosedFrom).datepicker({
            onSelect: (formattedDate, date) => {
                let newDateInSeconds = '', strDate = '';
                if (date) {
                    const newDate = new Date(date);
                    newDateInSeconds = newDate.getTime() / 1000;

                    strDate = this.datepickerDateToString(date);
                }
                this.setState({
                    closed_from_in_milliseconds: newDateInSeconds,
                    closed_from: strDate,
                }, () => this.handleFilter());
            }
        });
    }

    initDatepickerClosedTill() {
        $(this.datepickerClosedTill).datepicker({
            onSelect: (formattedDate, date = '') => {
                let newDateInSeconds = '', strDate = '';
                if (date) {
                    const newDate = new Date(date);
                    newDateInSeconds = newDate.getTime() / 1000; 

                    strDate = this.datepickerDateToString(date);
                }
                this.setState({
                    closed_till_in_milliseconds: newDateInSeconds,
                    closed_till: strDate,
                }, () => this.handleFilter());
            }
        });
    }

    initDatepickerDeFactoFrom() {
        $(this.datepickerDeFactoFrom).datepicker({
            onSelect: (formattedDate, date) => {
                let newDateInSeconds = '', strDate = '';
                if (date) {
                    const newDate = new Date(date);
                    newDateInSeconds = newDate.getTime() / 1000;

                    strDate = this.datepickerDateToString(date);
                }
                this.setState({
                    de_facto_date_from_in_milliseconds: newDateInSeconds,
                    de_facto_date_from: strDate,
                    date_of_deadline_in_milliseconds: newDateInSeconds,
                    date_of_deadline: strDate
                }, () => this.handleFilter());
            }
        });
    }

    initDatepickerPlannedDateFrom() {
        $(this.datepickerPlannedDateFrom).datepicker({
            onSelect: (formattedDate, date) => {
                let newDateInSeconds = '', strDate = '';
                if (date) {
                    const newDate = new Date(date);
                    newDateInSeconds = newDate.getTime() / 1000;

                    strDate = this.datepickerDateToString(date);
                }
                this.setState({
                    planned_date_in_milliseconds: newDateInSeconds,
                    planned_date: strDate
                }, () => this.handleFilter());
            }
        });
    }

    toggleIsLoading() {
        this.setState({
            table_isLoading: !this.state.table_isLoading
        });
    }

    handleChangeSort(name) {
        const { sortName, sortDirection } = this.state;
        this.setState({
            sortName: name,
            sortDirection: changeSortDirection(name, sortName, sortDirection)
        }, () => this.handleFilter());
    }

    handleChangeFilter(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: replaceRusLetterE(value.trim()),
            filter: true
        }, () => this.handleFilter());
    }

    filterStatus(status) {
        return arrayItem => !status || arrayItem.deal_status.status_including_deal_type === status
    }

    filterDisputeStatus(status) {
        return arrayItem => !status || arrayItem.status.status === status
    }

    handlePageSwitch(page) {
        const { sortName } = this.state;
        this.setState({
            sortName,
            page
        }, () => {
            this.getCollection(true);
        });
    }

    handlePerPageChange(per_page) {
        const { sortName } = this.state;
        this.setState({
            sortName,
            per_page
        }, () => {
            this.getCollection(true);
        });
    }

    setUrlParams(params, filters) {
        let esc = encodeURIComponent;
        if (filters) {
            filters = Object.keys(filters).map(filter => esc(filter) + '=' + esc(filters[filter])).join('&')
        } else {
            filters = ''
        }
        return Object.keys(params).map(param => esc(param) + '=' + esc(params[param])).join('&') + filters
    }
}