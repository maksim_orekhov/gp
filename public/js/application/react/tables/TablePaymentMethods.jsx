import React from 'react';
import FormBankTransfer from '../forms/FormBankTransfer';
import Popup from '../Popup';
import LinkSort from '../forms/base/LinkSort';
import {sorting, changeSortDirection, goToLink, filterArray, customFetch, replaceRusLetterE} from '../Helpers';

export default class TablePaymentMethods extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
          civil_law_subject_id: '',
          collection: [],
          collectionFiltered: [],
          collectionItemEdit: {},
          isOpenedFormCreate: false,
          isOpenedFormEdit: false,
          isNewItem: false,
          // filters
          isFiltering: false,
          name: '',
          type: '',
          status: '',
          // sorting
          sortName: '',
          sortDirection: ''
      };

      this.getCollection = this.getCollection.bind(this);
      this.updateCollection = this.updateCollection.bind(this);
      this.handleChangeFilter = this.handleChangeFilter.bind(this);
      this.handleFilter = this.handleFilter.bind(this);
      this.clearFilters = this.clearFilters.bind(this);
      this.handleChangeSort = this.handleChangeSort.bind(this);
      this.toggleFormCreate = this.toggleFormCreate.bind(this);
      this.toggleFormEdit = this.toggleFormEdit.bind(this);
      this.updateCollectionAfterDelete = this.updateCollectionAfterDelete.bind(this);
   }

   componentDidMount() {
      !this.props.isNested && this.getCollection();
   }

   componentWillReceiveProps(nextProps) {
      this.setState({
         civil_law_subject_id: nextProps.civil_law_subject_id,
         collection: nextProps.collection
      });
   }

   toggleFormEdit(collectionItem) {
       this.setState({
           collectionItemEdit: collectionItem,
           isOpenedFormEdit: !this.state.isOpenedFormEdit
       });
   }

   getCollection() {
      customFetch(window.location)
          .then(data => {
            console.log(data);
            this.setState({
               civil_law_subject_id: data.data.civil_law_subject_id,
               collection: data.data.paymentMethods
            });
      });
   }

   updateCollection() {
      this.setState({
          isOpenedFormCreate: false
      });

      customFetch(window.location)
          .then(data => {
            console.log(data);
            this.setState({
               collection: data.data.paymentMethods
            }, () => {
               this.clearFilters();
               this.highlightNewItem();
            });
      });
   }

   highlightNewItem() {
      this.setState({ isNewItem: true });
      setTimeout(() => {
         this.setState({ isNewItem: false });
      }, 3000);
   }

   handleChangeFilter(e) {
      const { name, value } = e.target;
      this.setState({
         [name]: replaceRusLetterE(value.trim())
      }, () => this.handleFilter());
   }

   filterPaymentName(filterValue) {
      const matches = new RegExp(filterValue, "\i");
      return arrayItem => !filterValue || matches.test(arrayItem.bank_transfer.name);
   }

   filterType(filterValue) {
      const matches = new RegExp(filterValue, "\i");
      return arrayItem => !filterValue || matches.test(arrayItem.type.output_name);
   }

   filterStatus(filterValue) {
      const matches = new RegExp(filterValue, "\i");
      return arrayItem => !filterValue || matches.test(arrayItem.type.is_active);
   }

   handleFilter() {
      const { collection, name, type, status, sortName, sortDirection } = this.state;
      const isFiltering =
         name !== ''
         || type !== ''
         || status !== ''
         || sortName !== ''
         || sortDirection !== ''
         ;

      const getFilteredResults = (array) => {
         return array
            .filter(this.filterPaymentName(name))
            .filter(this.filterType(type))
            .filter(this.filterStatus(status))
            ;
      }

      this.setState({
         collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
         isFiltering
      });
   }

   clearFilters() {
      this.setState({
         collectionFiltered: [],
         isFiltering: false,
         isOpenedFormCreate: false,
         name: '',
         type: '',
         status: '',
         sortName: '',
         sortDirection: ''
      });
   }

   updateCollectionAfterDelete() {
        this.setState({
            isOpenedFormEdit: false
        });

        $.ajax({
            dataType: "json",
            type: 'GET',
            // url: '/profile/natural-person',
            url: window.location,
            success: (data) => {
                console.log(data);
                this.setState({
                    collection: data.data.paymentMethods
                }, () => this.clearFilters());
            }
        });
    }

   handleChangeSort(name) {
      const { sortName, sortDirection } = this.state;
      this.setState({
         sortName: name,
         sortDirection: changeSortDirection(name, sortName, sortDirection)
      }, () => this.handleFilter());
   }

   toggleFormCreate() {
      this.setState({
         isOpenedFormCreate: !this.state.isOpenedFormCreate
      });
   }

   render() {
      const { civil_law_subject_id, collection, collectionFiltered, isFiltering, sortName, sortDirection, isOpenedFormCreate, isNewItem, isOpenedFormEdit, collectionItemEdit } = this.state;
      const collectionList = isFiltering ? collectionFiltered : collection;

      return (
         <div>
             <div className="container">
                 <div className="row nested-row">
                     <div className="col-xl-12">
                         <h1 className="TitleBlue">Метод оплаты</h1>
                     </div>
                 </div>
             </div>
            <div className="TableWrapper">
               <table className="TableDefault TablePaymentMethods js-table-rows-are-link">
                  <tbody>
                     <tr className="FilterRowButtonMobile">
                        <td className="col-button">
                           <button className="ButtonApply" title="Добавить метод оплаты" onClick={this.toggleFormCreate} data-ripple-button="">
                               <span className="ripple-text">Добавить</span>
                           </button>
                        </td>
                     </tr>
                     <tr className="FilterTableRow">
                        <td className="col-number">
                           <button className="ButtonApply" title="Добавить метод оплаты" onClick={this.toggleFormCreate} data-ripple-button="">
                               <span className="ripple-text">Добавить</span>
                           </button>
                        </td>
                        <td className="col-name">
                           <input type="text" className="InputSearch" name="name" value={this.state.name} placeholder="Метод оплаты" onChange={this.handleChangeFilter} />
                        </td>
                        <td>
                           <input type="text" className="InputSearch" name="type" placeholder="Тип" onChange={this.handleChangeFilter} />
                        </td>
                        <td>
                           <select /* className="chosen-select" */ name="status" id="" data-placeholder="Статус" onChange={this.handleChangeFilter}>
                              <option value="">Статус</option>
                              <option value="true">Активен</option>
                              <option value="false">Не активен</option>
                           </select>
                        </td>
                        <td className="col-empty"></td>
                     </tr>
                     <tr className="Head">
                        <td className="col col-number">
                           <LinkSort sortName="index"
                              label="№"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td className="col col-name">
                           <LinkSort sortName="bank_transfer.name"
                              label="Метод оплаты"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td className="col">
                           <LinkSort sortName="type"
                              label="Тип"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td className="col">
                           <LinkSort sortName="status"
                              label="Статус"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td className="col-empty"></td>
                     </tr>
                     {
                        collectionList.length !== 0 &&
                        collectionList.map((collectionItem, index) => {
                           const { bank_transfer, type } = collectionItem;
                           const { output_name, is_active } = type;
                           const { id, name } = bank_transfer;
                           return (
                              <tr key={id} className={`${isNewItem && index === 0 ? 'highlighted' : ''}`}>
                                 <td className="col col-number">
                                    <div className="col_mobile-content">{index + 1}</div>
                                 </td>
                                 <td className="col col-name">
                                    <div className="col_mobile-title">Метод оплаты:</div>
                                    <div className="col_mobile-content">{name}</div>
                                     </td>
                                 <td  className="col">
                                    <div className="col_mobile-title">Тип:</div>
                                    <div className="col_mobile-content">{output_name}</div>
                                 </td>
                                 <td className="col">
                                    <div className="col_mobile-title">Статус:</div>
                                    <div className="col_mobile-content">{is_active ? 'Активен' : 'Не активен'}</div>
                                 </td>
                                 <td><span className="IconEdit" onClick={() => this.toggleFormEdit(collectionItem)}></span></td>
                              </tr>
                           );
                        })
                     }
                     {
                        (isFiltering && collectionList.length === 0 && collection.length !== 0) &&
                        <tr className="EmptyRow">
                           <td colSpan="6">Методы оплаты не найдены.</td>
                        </tr>
                     }
                     {
                        collection.length === 0 &&
                        <tr className="EmptyRow">
                           <td colSpan="5">Вы еще не добавили ни одного метода оплаты.</td>
                        </tr>
                     }
                  </tbody>
               </table>
            </div>
             {
                 isOpenedFormEdit &&
                 <Popup close={this.toggleFormEdit}>
                    <FormBankTransfer
                        data={collectionItemEdit}
                        editMode={true}
                        civil_law_subject_id={civil_law_subject_id}
                        afterSubmit={this.updateCollectionAfterDelete}
                    />
                 </Popup>
             }
             {
                isOpenedFormCreate &&
                <Popup close={this.toggleFormCreate}>
                   <FormBankTransfer
                       afterSubmit={this.updateCollection}
                       civil_law_subject_id={civil_law_subject_id}
                   />
                </Popup>
             }
         </div>
      );
   }
}
