import React from 'react';
import LinkSort from '../forms/base/LinkSort';
import { sorting, changeSortDirection, goToLink, filterArray } from '../Helpers';

export default class TableDocuments extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         civil_law_subject_id: '',
         id: '',
         collection: [],
         collectionFiltered: [],
         isOpenedFormCreate: false,
         isNewItem: false,
         // filters
         isFiltering: false,
         type_name: '',
         date_issued_from: '',
         date_issued_till: '',
         // sort
         sortName: '',
         sortDirection: ''
      };

      this.getCollection = this.getCollection.bind(this);
      this.updateCollection = this.updateCollection.bind(this);
      this.handleChangeFilter = this.handleChangeFilter.bind(this);
      this.handleFilter = this.handleFilter.bind(this);
      this.highlightNewItem = this.highlightNewItem.bind(this);
      this.clearFilters = this.clearFilters.bind(this);
      this.handleChangeSort = this.handleChangeSort.bind(this);
      this.toggleFormCreate = this.toggleFormCreate.bind(this);
   }

   componentDidMount() {
      !this.props.isNested && this.getCollection();
   }

   componentWillReceiveProps(nextProps) {
      this.setState({
         id: nextProps.id,
         collection: nextProps.collection
      });
   }

   getCollection() {
      $.ajax({
         dataType: "json",
         type: 'GET',
         // url: '/profile/natural-person',
         url: window.location,
         success: (data) => {
            console.log(data);
            this.setState({
               id: data.data.naturalPerson.id,
               civil_law_subject_id: data.data.civil_law_subject_id,
               collection: data.data.documents
            });
         }
      });
   }

   updateCollection() {
      $.ajax({
         dataType: "json",
         type: 'GET',
         url: window.location,
         success: (data) => {
            console.log(data);
            this.setState({
               collection: data.data.documents
            }, () => {
               this.clearFilters();
               this.highlightNewItem();
            });
         }
      });
   }

   handleChangeFilter(e) {
      const { name, value } = e.target;
      this.setState({
         [name]: value.trim()
      }, () => this.handleFilter());
   }

   handleFilter() {
      const { collection, name, type, status, sortName, sortDirection } = this.state;
      const isFiltering =
         name !== ''
         || type !== ''
         || status !== ''
         || sortName !== ''
         || sortDirection !== ''
         ;

      const getFilteredResults = (array) => {
         return array
         /* .filter(this.filterPaymentName(name))
         .filter(this.filterType(type))
         .filter(this.filterStatus(status))
         ; */
      }

      this.setState({
         collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
         isFiltering
      });
   }

   highlightNewItem() {
      this.setState({ isNewItem: true });
      setTimeout(() => {
         this.setState({ isNewItem: false });
      }, 3000);
   }

   clearFilters() {
      this.setState({
         collectionFiltered: [],
         isFiltering: false,
         isOpenedFormCreate: false,
         name: '',
         sortName: '',
         sortDirection: ''
      });
   }

   handleChangeSort(name) {
      const { sortName, sortDirection } = this.state;
      this.setState({
         sortName: name,
         sortDirection: changeSortDirection(name, sortName, sortDirection)
      }, () => this.handleFilter());
   }

   toggleFormCreate() {
      this.setState({
         isOpenedFormCreate: !this.state.isOpenedFormCreate
      });
   }

   render() {
      const { id, civil_law_subject_id, collection, collectionFiltered, isFiltering, sortName, sortDirection, isOpenedFormCreate } = this.state;
      const collectionList = isFiltering ? collectionFiltered : collection;
      const names = {
          passport: 'Паспорт',
          driver_license: 'Водительское удостоверение',
          tax_inspection: 'Свидетельство о постановке на учет в налоговый орган',
          bank_details: 'Банковские реквизиты'
      };
      const urls = {
          passport: '/profile/natural-person-passport/',
          driver_license: '/profile/natural-person-driver-license/',
          tax_inspection: '/profile/legal-entity-tax-inspection/',
          bank_details: '/profile/legal-entity-bank-detail/'
      };

      return (
         <div className="CivilLawSubjectPage-Section Document">
            <h1 className="TitleBlue">Документы</h1>
            <div className="TableWrapper">
               <table className="TableDefault TableDocuments js-table-rows-are-link">
                  <tbody>
                     <tr className="FilterTableRow">
                        <td>
                           {/* <a className="ButtonApply" title="Добавить документ" onClick={this.toggleFormCreate}>Добавить</a> */}
                           <button type="button" className="ButtonApply js-popup-open" data-popup="#add-document-popup">Добавить</button>
                        </td>
                        <td>
                           <input type="text" className="InputSearch" name="name" placeholder="Документ" onChange={this.handleChangeFilter} />
                        </td>
                        <td className="DoubleInputDate">
                           <input ref={input => this.datepickerFrom = input} type="text" name="date_issued_from" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                           <input ref={input => this.datepickerTill = input} type="text" name="date_issued_till" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                        </td>
                        <td>
                           <select name="status" data-placeholder="Файл" onChange={this.handleChangeFilter}>
                              <option value="">Файл</option>
                              <option value="true">Паспорт</option>
                              <option value="false">Водительское удостоверение</option>
                           </select>
                        </td>
                        <td>
                           <button className="ButtonSearch" type="submit"><span>&nbsp;</span></button>
                        </td>
                     </tr>
                     <tr className="Head">
                        <td>
                           <LinkSort sortName="index"
                              label="№"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort sortName="type"
                              label="Тип"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort sortName="date_issued"
                              label="Дата выдачи"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           Файлы
                        </td>
                        <td></td>
                     </tr>
                     {
                        collectionList.length !== 0 &&
                        collectionList.map((document, index) => {
                           const { id, date_issued, type_name, files } = document;
                           const url = `${urls[type_name]}${id}`;

                           return (
                              <tr key={index} className="active" onClick={() => goToLink(url)}>
                                 <td data-label="№:">{index + 1}</td>
                                 <td data-label="Тип:">{names[type_name]}</td>
                                 <td data-label="Дата выдачи:">{date_issued}</td>
                                 {/* <a href="/profile/file/121/1" target="_blank">
                                    <i className="IconJPG"></i>
                                 </a> */}
                                 <td></td>
                                 <td></td>
                              </tr>
                           );
                        })
                     }
                     {
                        (isFiltering && collectionList.length === 0 && collection.length !== 0) &&
                        <tr className="EmptyRow">
                           <td colSpan="5">Документы не найдены.</td>
                        </tr>
                     }
                     {
                        collection.length === 0 &&
                        <tr className="EmptyRow">
                           <td colSpan="5">Вы еще не добавили ни одного документа.</td>
                        </tr>
                     }
                  </tbody>
               </table>
            </div>
            <div className="Popup-Container js-popup" id="add-document-popup">
               <div className="Popup-Bg"></div>
               <div className="Popup">
                  <div className="ButtonClose js-popup-close"></div>
                  <div className="Popup-Content">
                     <div className="FormPopupRouter js-route-popup">
                        <h3 className="TitleBlue">Выберите тип документа</h3>
                        <p className="InfoField js-item" data-path={`${id}/passport/create`}>Паспорт</p>
                        <p className="InfoField js-item" data-path={`${id}/driver-license/create`}>Водительское удостоверение</p>
                        <div className="ButtonsRow">
                           <button className="ButtonAccept margin-right js-confirm" type="button">Подтвердить</button>
                           <button className="ButtonRefuse js-reset" type="button">Отменить</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            {/* {
               isOpenedFormCreate &&
               <div className="FormPopup">
                  <div className="FormPopup-Container">
                     <div className="ButtonClose" onClick={this.toggleFormCreate}></div>
                     <FormAddDocuments />
                  </div>
                  <div className="FormPopup-Bg"></div>
               </div>
            } */}
         </div>
      );
   }
}
