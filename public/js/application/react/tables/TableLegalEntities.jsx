import React from 'react';
import FormLegalEntity from '../forms/FormLegalEntity';
import Popup from '../Popup';
import LinkSort from '../forms/base/LinkSort';
import { sorting, changeSortDirection, filterArray, replaceRusLetterE, customFetch } from '../Helpers';
import URLS from "../constants/urls";
import PropTypes from "prop-types";

export default class TableLegalEntities extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         collection: [],
         collectionFiltered: [],
         collectionItemEdit: {},
         isOpenedFormCreate: false,
         isOpenedFormEdit: false,
         isNewItem: false,
         table_isLoading: false,
         // filters
         isFiltering: false,
         name: '',
         // sort
         sortName: '',
         sortDirection: ''
      };

      this.updateCollection = this.updateCollection.bind(this);
      this.updateCollectionAfterDelete = this.updateCollectionAfterDelete.bind(this);
      this.handleChangeFilter = this.handleChangeFilter.bind(this);
      this.handleFilter = this.handleFilter.bind(this);
      this.clearFilters = this.clearFilters.bind(this);
      this.handleChangeSort = this.handleChangeSort.bind(this);
      this.toggleFormCreate = this.toggleFormCreate.bind(this);
      this.toggleFormEdit = this.toggleFormEdit.bind(this);
   }

   componentDidMount() {
       if (this.props.is_nested) {
           const { legalEntities } = this.props;
           legalEntities && this.setState({
               collection: legalEntities
           });
       } else {
           this.getCollection();
       }
   }

    componentWillReceiveProps(nextProps) {
        const { legalEntities } = nextProps;
        legalEntities && this.setState({
            collection: legalEntities
        });
    }

    getCollection() {
        this.toggleIsLoading();
        customFetch(URLS.LEGAL_ENTITY.SINGLE)
            .then(data => {
                this.toggleIsLoading();
                if (data.status === 'SUCCESS') {
                    this.setState({
                        collection: data.data.legalEntities,
                        is_operator: data.data.is_operator
                    });
                }
            })
            .catch(() => {
                this.toggleIsLoading();
                this.setState({
                    is_operator: true
                });
            });
    }

   updateCollection() {
       if (this.props.is_nested) {
           this.setState({
               isOpenedFormEdit: false
           }, () => {
               this.clearFilters();
           });
           this.props.updateCollection(false, true);
       } else {
           customFetch(URLS.LEGAL_ENTITY.SINGLE)
               .then(data => {
                   console.log(data);
                   this.setState({
                       collection: data.data.legalEntities
                   }, () => {
                       this.clearFilters();
                       this.highlightNewItem();
                   });
               });
       }
   }

   updateCollectionAfterDelete() {
       if (this.props.is_nested) {
           this.setState({
               isOpenedFormEdit: false
           }, () => {
               this.clearFilters();
           });
           this.props.updateCollection(false, false);
       } else {
           customFetch(URLS.LEGAL_ENTITY.SINGLE)
               .then(data => {
                   console.log(data);
                   this.setState({
                       isOpenedFormEdit: false,
                       collection: data.data.legalEntities
                   }, () => {
                       this.clearFilters();
                   });
               });
       }
   }

    highlightNewItem() {
        this.setState({ isNewItem: true });
        setTimeout(() => {
            this.setState({ isNewItem: false });
        }, 3000);
    }

   handleChangeFilter(e) {
      const { name, value } = e.target;
      this.setState({
         [name]: replaceRusLetterE(value.trim())
      }, () => this.handleFilter());
   }

   handleFilter() {
      const { collection, name, sortName, sortDirection } = this.state;
      const isFiltering = name !== '' || sortName !== '';

      const getFilteredResults = (array) => {
         return array
            .filter(filterArray('name', name));
      };

      this.setState({
         isNewItem: false,
         collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
         isFiltering
      });
   }

   clearFilters() {
      this.setState({
         collectionFiltered: [],
         isFiltering: false,
         isOpenedFormCreate: false,
         name: '',
         sortName: '',
         sortDirection: ''
      });
   }

   handleChangeSort(name) {
      const { sortName, sortDirection } = this.state;
      this.setState({
         sortName: name,
         sortDirection: changeSortDirection(name, sortName, sortDirection)
      }, () => this.handleFilter());
   }

   toggleFormCreate() {
      this.setState({
         isOpenedFormCreate: !this.state.isOpenedFormCreate
      });
   }

   toggleFormEdit(collectionItem) {
      this.setState({
         collectionItemEdit: collectionItem,
         isOpenedFormEdit: !this.state.isOpenedFormEdit
      });
   }

    toggleIsLoading() {
        this.setState({
            table_isLoading: !this.state.table_isLoading
        });
    }

   render() {
       const { isOperator, is_nested } = this.props;
      const { collection, collectionItemEdit, collectionFiltered, isFiltering, sortName, sortDirection, isOpenedFormCreate, isOpenedFormEdit } = this.state;
      const collectionList = isFiltering ? collectionFiltered : collection;

      return (
         <div>
             {
                 !isOperator &&
            <div className="TableWrapper">
               <table className="TableDefault TableLegalEntities js-table-rows-are-link">
                  <tbody>

                  <tr className="FilterRowButtonMobile">
                     <td className="FilterColButton">
                        <a className="ButtonApply FilterButton" onClick={this.toggleFormCreate} title="Добавить юридическое лицо" data-ripple-button="">Добавить</a>
                     </td>
                     {/*<td><label htmlFor="filter-input-hidden-legal-entity" className="ButtonEdit FilterButton js-button-filter-table-mobile" title="Фильтр для таблицы">Фильтр</label></td>*/}
                     <td></td>
                     <td></td>
                  </tr>

                     <tr className="FilterTableRow js-filter-table-mobile">
                        <td className="col col-number">
                            <a className="ButtonApply" /* href="/profile/natural-person/create" */ title="Добавить юридическое лицо" onClick={this.toggleFormCreate} data-ripple-button="">
                                Добавить
                            </a>
                        </td>
                        <td className="head-of-element">
                           <h3>Фильтр</h3>
                        </td>
                        <td className="col col-name">
                           <input type="text" className="InputSearch" name="name" value={this.state.name} placeholder="Поиск по названию" onChange={this.handleChangeFilter} />
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                     <tr className="Head">
                        <td>
                           <LinkSort sortName="id"
                              label="№"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td className="col col-name">
                           <LinkSort sortName="name"
                              label="Наименование"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort sortName="legal_address"
                              label="Адрес"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        {/* <td>
                           <LinkSort sortName="phone"
                              label="Телефон"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td> */}
                        <td>
                           <LinkSort sortName="inn"
                              label="ИНН"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort sortName="kpp"
                              label="КПП"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        {/* <td>
                           <LinkSort sortName="ogrn"
                              label="ОГРН"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td> */}
                        <td></td>
                     </tr>
                     {
                        collectionList.length !== 0 &&
                        collectionList.map((collectionItem, index) => {
                           const { id, name, legal_address, /* phone, */ inn, kpp/* , ogrn */ } = collectionItem;
                           const url = `/profile/legal-entity/${id}`;
                           return (
                              <tr key={id} className={`active ${is_nested ?
                                  this.props.isNewItem && index === collectionList.length - 1 ? 'highlighted' : ''
                                  :
                                  this.state.isNewItem && index === 0 ? 'highlighted' : '' }`} /* onClick={() => goToLink(url)} */>
                                 <td className="col col-number">{index + 1}</td>
                                 <td className="col col-name">
                                    <div className="col_mobile-title">Наименование:</div>
                                    <div className="col_mobile-content"> <a href={url}>{name}</a></div>
                                 </td>
                                 <td className="col">
                                    <div className="col_mobile-title">Адрес:</div>
                                    <div className="col_mobile-content">{legal_address}</div>
                                 </td>
                                 {/* <td data-label="Телефон:">{phone}</td> */}
                                 <td className="col">
                                    <div className="col_mobile-title">ИНН:</div>
                                    <div className="col_mobile-content">{inn}</div>
                                     </td>
                                 <td className="col">
                                    <div className="col_mobile-title">КПП:</div>
                                    <div className="col_mobile-content">{kpp}</div>
                                 </td>
                                 {/* <td data-label="ОГРН:">{ogrn}</td> */}
                                 <td>
                                    <span className="IconEdit" onClick={() => this.toggleFormEdit(collectionItem)}></span>
                                 </td>
                              </tr>
                           );
                        })
                     }
                     {
                        (isFiltering && collectionList.length === 0 && collection.length !== 0) &&
                        <tr className="EmptyRow">
                           <td colSpan="6">Юридические лица не найдены.</td>
                        </tr>
                     }
                     {
                        collection.length === 0 &&
                        <tr className="EmptyRow">
                           <td colSpan="6" className={is_nested ? this.props.tableIsLoading ? 'Preloader Preloader_solid' : null
                               : this.state.table_isLoading ? 'Preloader Preloader_solid' : null}>Вы еще не добавили в свой профиль ни одного юридического лица.</td>
                        </tr>
                     }
                  </tbody>
               </table>
            </div>
             }
            {
               isOpenedFormCreate &&
               <Popup close={this.toggleFormCreate}>
                  <FormLegalEntity afterSubmit={this.updateCollection} />
               </Popup>
            }
            {
               isOpenedFormEdit &&
               <Popup close={() => this.toggleFormEdit({})}>
                  <FormLegalEntity
                      data={collectionItemEdit}
                      editMode={true}
                      afterSubmit={this.updateCollectionAfterDelete}
                      afterDelete={this.updateCollectionAfterDelete}
                      item_id={collectionItemEdit}
                  />
               </Popup>
            }
         </div>
      );
   }
}

TableLegalEntities.propTypes = {
    isOperator: PropTypes.bool,
    is_nested: PropTypes.bool
};
