import React from 'react';
import Datepicker from '../forms/base/Datepicker';
import LinkSort from '../forms/base/LinkSort';
import {
    sorting, goToLink, filterCustomArray, filterArray, filterCreated, filterCreatedDate, customFetch,
    getDisputesWithFieldsSplittedByDealTypes,
} from '../Helpers';
import Paginator from '../Paginator';
import TableBase from './TableBase';
import URLS from '../constants/urls';
import withWindowDimensions from '../hocs/withWindowDimensions';

class TableDealsTrouble extends TableBase {
   constructor(props) {
      super(props);
      this.state = {
         collection: [],
         collectionFiltered: [],
         abnormal_types: [],
         isFiltering: false,
         abnormal_type: '',
         sortName: '',
         sortDirection: '',
         paginator: {},
         page: 1,
         per_page: 20,
         name: '',
         created_from: '',
         id: '',
         de_facto_date_from: '',
         planned_date: '',
         created_from_in_milliseconds: '',
         de_facto_date_from_in_milliseconds: '',
         planned_date_in_milliseconds: '',
         created_till: '',
         customer: '',
         contractor: '',
         table_isLoading: false
      };

      this.getCollection = this.getCollection.bind(this);
      this.handleChangeFilter = this.handleChangeFilter.bind(this);
      this.handleChangeSort = this.handleChangeSort.bind(this);
      this.handleFilter = this.handleFilter.bind(this);
      this.handlePageSwitch = this.handlePageSwitch.bind(this);
      this.handlePerPageChange = this.handlePerPageChange.bind(this);
   }

    tableInit() {
        document.getElementById('TableDealsTrouble').style.visibility = 'visible';
    }

   getCollection(isNeedToRunHandleFilter) {
       const { page, csrf, per_page } = this.state;
       const filter = this.setFilterDataToSend();

       this.toggleIsLoading();
       $.ajax({
           dataType: "json",
           type: 'GET',
           data: {
               page,
               per_page,
               csrf,
               filter
           },
           url: URLS.DEAL.ABNORMAL,
           success: (data) => {
               const { payments, paginator = {}, payment_abnormal_types } = data.data;
               console.log(data);
               this.setState({
                   abnormal_types: payment_abnormal_types,
                   collection: payments,
                   paginator
               }, () => {
                   this.toggleIsLoading();
                   isNeedToRunHandleFilter && this.handleFilter();
                   if (this.state.collection.length === 0) {
                       this.setState({
                           isFiltering: true
                       })
                   }
               });
           }
       }).fail(() => this.toggleIsLoading());
   }

    setFilterDataToSend() {
        const { name, created_from, number, created_till, abnormal_type } = this.state;
        const filter = {
            deal_name: name,
            deal_created_from: created_from,
            deal_created_till: created_till,
            abnormal_type
        };

        filter.deal_number = number;

        return filter;
    }

   filterTrouble(abnormal_type) {
      return deal => abnormal_type === '' || deal.deal.abnormality_reasons.hasOwnProperty(abnormal_type);
   }

   handleFilter() {
       const { collection, id, name, author, created_from_in_milliseconds, de_facto_date_from_in_milliseconds, planned_date_in_milliseconds, sortName, sortDirection, abnormal_type, customer, contractor } = this.state;
       const isFiltering =
           id !== ''
           || name !== ''
           || author !== ''
           || de_facto_date_from_in_milliseconds !== ''
           || planned_date_in_milliseconds !== ''
           || sortName !== ''
           || abnormal_type !== ''
           || customer !== ''
           || contractor !== ''
       ;
       const getFilteredResults = (array) => {

           return array
               .filter(filterArray('id', id))
               .filter(filterCustomArray('deal', 'name', name))
               .filter(filterCustomArray('deal', 'customer', customer))
               .filter(filterCustomArray('deal', 'contractor', contractor))
               .filter(filterCreatedDate('created_milliseconds', created_from_in_milliseconds))
               .filter(filterCreated('de_facto_date_milliseconds', de_facto_date_from_in_milliseconds))
               .filter(filterCreated('planned_date_milliseconds', planned_date_in_milliseconds))
               .filter(this.filterTrouble(abnormal_type))
       };

       this.setState({
           collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
           isFiltering
       });
   }

   render() {
      const { collection, collectionFiltered, isFiltering, sortName, sortDirection, paginator, table_isLoading, abnormal_types } = this.state;
      const collectionList = isFiltering ? collectionFiltered : collection;
      const { isMobile, isTablet } = this.props;

      return (
         <div className='TableWithPagination'>
            <div className='TableWrapper'>
            <table className='TableDefault TableDealsUbnormal js-table-rows-are-link'>
               <tbody>
               <tr className="table-head">
                   <td className="head-of-element">
                       <h3>Сделки проблемные</h3>
                   </td>
               </tr>
                  <tr className='FilterTableRow'>
                      <td>
                         <span className='TableDisputes-OperatorCol'>
                            <input type='text' name='id' placeholder='Номер' className='InputSearch' onChange={this.handleChangeFilter} />
                         </span>
                      </td>
                      <td>
                          <input type='text' name='name' value={this.state.name} placeholder='Поиск по названию' className='InputSearch' onChange={this.handleChangeFilter} />
                      </td>
                      <td>
                          {/*<div className="DoubleInputDate">*/}
                              <input ref={input => this.datepickerFrom = input} type='text' name='created_from' placeholder='Дата создания' className='InputDate2 datepicker-here' onChange={this.handleChangeFilter} data-auto-close='true' />
                          {/*</div>*/}
                      </td>
                      <td>
                          {/*<div className="DoubleInputDate">*/}
                              <input ref={input => this.datepickerDeFactoFrom = input} type='text' name='de_facto_date_from' placeholder='Дата оплаты' className='InputDate2 datepicker-here' onChange={this.handleChangeFilter} data-auto-close='true' />
                          {/*</div>*/}
                      </td>
                      <td>
                          {/*<div className="DoubleInputDate">*/}
                              <input ref={input => this.datepickerPlannedDateFrom = input} type='text' name='planned_date' placeholder='Срок оплаты' className='InputDate2 datepicker-here' onChange={this.handleChangeFilter} data-auto-close='true' />
                          {/*</div>*/}
                      </td>
                      <td>
                          <input type='text' name='customer' placeholder='Покупатель/Заказчик' className='InputSearch' onChange={this.handleChangeFilter} />
                      </td>
                      <td>
                          <input type='text' name='contractor' placeholder='Продавец/Исполнитель' className='InputSearch' onChange={this.handleChangeFilter} />
                      </td>
                     <td>
                        <select name='abnormal_type' id='abnormal_type' className='form-control' onChange={this.handleChangeFilter}>
                           <option value=''>Проблема</option>
                            {
                                Object.entries(abnormal_types).map(type =>
                                    <option
                                        key={type[0]}
                                        value={type[0]}
                                    >
                                        {type[1]}
                                    </option>
                                )
                            }
                        </select>
                     </td>
                      <td/>
                     <td className="button-search-wrap">
                        <button className='ButtonSearch' onClick={this.getCollection} type='submit'><span>&nbsp;</span></button>
                     </td>
                  </tr>
                  <tr className='Head'>
                     <td className="col col-number">
                         <LinkSort
                           sortName='id'
                           label='№'
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                         />
                     </td>
                     <td>
                        <LinkSort
                           sortName='deal.name'
                           label='Название'
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>
                        <LinkSort
                           sortName='deal.created_milliseconds'
                           label="Дата создания"
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>
                        <LinkSort
                           sortName="de_facto_date_milliseconds"
                           label="Дата оплаты"
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>
                        <LinkSort
                           sortName="planned_date_milliseconds"
                           label="Срок оплаты"
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>
                        <LinkSort
                           sortName="deal.customer"
                           label="Покупатель/ Заказчик"
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>
                        <LinkSort
                           sortName="deal.contractor"
                           label="Продавец/ Исполнитель"
                           handleChangeSort={this.handleChangeSort}
                           isActiveSortName={sortName}
                           direction={sortDirection}
                        />
                     </td>
                     <td>Проблема</td>
                     <td/>
                     <td/>
                  </tr>
                  {
                     collectionList.length !== 0 &&
                     !table_isLoading &&
                     collectionList.map(collectionItem => {
                        const { deal, de_facto_date, planned_date } = collectionItem;
                        const { name, created, customer, contractor, abnormality_reasons, id } = deal;
                        const url = `/deal/show/${id}`;
                        const reason = Object.values(abnormality_reasons).join(', ');

                        return (
                           <tr className='active' key={id} onClick={() => !isMobile && goToLink(url)}>
                              <td className="col col-number" data-label='ID:'>
                                  <div className="col_mobile-title">Номер</div>
                                  <div className="col_mobile-content">{id}</div>
                              </td>
                              <td className="col">
                                  <div className="col_mobile-title">Название сделки</div>
                                  <div className="col_mobile-content" title={name}>{name}</div>
                              </td>
                              <td className="col" data-label='Дата создания:'>
                                  <div className="col_mobile-title">Дата создания</div>
                                  <div className="col_mobile-content">{created}</div>
                              </td>
                              <td className="col" data-label='Дата оплаты:'>
                                  <div className="col_mobile-title">Дата оплаты</div>
                                  <div className="col_mobile-content">{de_facto_date}</div>
                              </td>
                              <td className="col" data-label='Срок оплаты:'>
                                  <div className="col_mobile-title">Срок оплаты</div>
                                  <div className="col_mobile-content">{planned_date}</div>
                              </td>
                              <td className="col" data-label='Покупатель:'>
                                  <div className="col_mobile-title">Покупатель</div>
                                  <div className="col_mobile-content" title={customer}>{customer}</div>
                              </td>
                              <td className="col" data-label='Продавец:'>
                                  <div className="col_mobile-title">Продавец</div>
                                  <div className="col_mobile-content" title={contractor}>{contractor}</div>
                              </td>
                              <td className="col" data-label='Причина:'>
                                  <div className="col_mobile-title">Причина</div>
                                  <div className="col_mobile-content" title={reason}>{reason}</div>
                              </td>
                              <td className="col">
                                  {
                                      reason.match(/Переплата/) ?
                                          <a href={`/deal/overpay/` + id}>Возврат переплаты</a>
                                          :
                                          void(0)
                                  }
                              </td>
                               {
                                   !isMobile && !isTablet ?
                                       <td/>
                                       :
                                       <td className="to-deal-link-cell">
                                           <a href={url} className="ButtonApply" data-ripple-button="">
                                               <span className="ripple-text">Перейти к сделке</span>
                                           </a>
                                       </td>
                               }
                           </tr>
                        );
                     })
                  }
                  {
                     (collectionList.length === 0 && collection.length !== 0 && !table_isLoading) &&
                     <tr className="EmptyRow">
                        <td colSpan="9">Сделки с проблемами не найдены.</td>
                     </tr>
                  }
                  {
                     collection.length === 0 &&
                     !table_isLoading &&
                     <tr className="EmptyRow">
                        <td colSpan="9">В системе нет проблемных сделок.</td>
                     </tr>
                  }
                  {
                      table_isLoading &&
                      <tr className="EmptyRow">
                          <td colSpan="9" className='Preloader Preloader_solid' />
                      </tr>
                  }
               </tbody>
            </table>
         </div>
             <Paginator
                 paginator={paginator}
                 handlePageSwitch={this.handlePageSwitch}
                 handlePerPageChange={this.handlePerPageChange}
             />
         </div>
      );
   }
}

export default withWindowDimensions(TableDealsTrouble);