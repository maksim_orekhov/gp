import React from 'react';
import Datepicker from '../forms/base/Datepicker';
import LinkSort from '../forms/base/LinkSort';
import {
    sorting, goToLink, filterArray, filterCreatedFrom, filterCreatedTill, getDisputesWithFieldsSplittedByDealTypes,
    getDealStatusesSplittedByDealTypes, customFetch, getDisputeStatusesSplittedByDealTypes
} from '../Helpers';
import Paginator from '../Paginator';
import TableBase from './TableBase';
import URLS from "../constants/urls";
import MESSAGES from "../constants/messages";
import withWindowDimensions from '../hocs/withWindowDimensions';

class TableDealsDisputeOpen extends TableBase {
    constructor(props) {
        super(props);
        this.state = {
            collection: [],
            collectionFiltered: [],
            isFiltering: false,
            deal_number: '',
            deal_name: '',
            name: '',
            number: '',
            author: '',
            created_from: '',
            created_till_in_milliseconds: '',
            closed_till_in_milliseconds: '',
            created_from_in_milliseconds: '',
            closed_from_in_milliseconds: '',
            created_till: '',
            closed_from: '',
            closed_till: '',
            status: '',
            deal_status: '',
            dispute_status: '',
            sortName: '',
            sortDirection: '',
            table_isLoading: false,
            paginator: {},
            page: 1,
            per_page: 20,
            csrf: '',
            deal_statuses: [],
            dispute_statuses: []
        };

        this.handleChangeSort = this.handleChangeSort.bind(this);
        this.handleChangeFilter = this.handleChangeFilter.bind(this);
        this.getCollection = this.getCollection.bind(this);
        this.filterStatus = this.filterStatus.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handlePageSwitch = this.handlePageSwitch.bind(this);
        this.handlePerPageChange = this.handlePerPageChange.bind(this);
    }

    getCollection(isNeedToRunHandleFilter) {
        const { page, csrf, per_page } = this.state;
        const filter = this.setFilterDataToSend();

        this.toggleIsLoading();
        $.ajax({
            dataType: "json",
            type: 'GET',
            data: {
                page,
                per_page,
                csrf,
                filter
            },
            url: URLS.DISPUTE.SINGLE,
            success: (data) => {
                const { csrf, disputes, paginator } = data.data;
                const disputeSplittedByDealTypes = getDisputesWithFieldsSplittedByDealTypes(disputes);
                console.log(data);
                this.setState({
                    collection: disputeSplittedByDealTypes,
                    paginator,
                    csrf
                }, () => {
                    this.toggleIsLoading();
                    isNeedToRunHandleFilter && this.handleFilter();
                    if (this.state.collection.length === 0) {
                        this.setState({
                            isFiltering: true
                        })
                    }
                });
            }
        }).fail(() => this.toggleIsLoading());
    }

    tableInit() {
        this.getStatuses();
        document.getElementById('TableDealsDisputeOpen').style.visibility = 'visible';
    }

    getStatuses() {
        fetch(URLS.DEAL.FORM_INIT, {
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status === 200) {
                    return response.json();
                } else {
                    return Promise.reject(MESSAGES.SERVER_ERRORS.CRITICAL_ERROR.text);
                }
            })
            .then((data) => {
                if (data.status === 'SUCCESS') {
                    console.log(data);
                    const deal_statuses = getDealStatusesSplittedByDealTypes(Object.values(data.data.deal_statuses));
                    const dispute_statuses = getDisputeStatusesSplittedByDealTypes(Object.values(data.data.dispute_statuses));

                    this.setState({
                        deal_statuses,
                        dispute_statuses
                    });
                } else if (data.status === 'ERROR') {
                    console.log('Ошибка при получении статусов');
                    return Promise.reject('Ошибка при получении статусов.');
                }
            })
            .catch(error => this.setState({formError: error}));
    }

    setFilterDataToSend() {
        const { deal_name, deal_number, created_from, number, created_till, dispute_status, closed_from, closed_till } = this.state;
        const filter = {
            deal_id: deal_number,
            deal_name,
            dispute_created_from: created_from,
            dispute_created_till: created_till,
            deal_closed_from: closed_from,
            deal_closed_till: closed_till,
            dispute_status
        };

        return filter;
    }

    handleFilter() {
        const { collection, deal_number, number, deal_name, author, created_from_in_milliseconds, created_till_in_milliseconds, created_till, sortName, sortDirection, dispute_status, closed_from_in_milliseconds, closed_till_in_milliseconds } = this.state;
        const isFiltering =
            number !== ''
            || deal_number !== ''
            || deal_name !== ''
            || author !== ''
            || created_from_in_milliseconds !== ''
            || created_till_in_milliseconds !== ''
            || closed_from_in_milliseconds !== ''
            || closed_till_in_milliseconds !== ''
            || created_till !== ''
            || sortName !== ''
            || dispute_status !== ''
        ;
        const getFilteredResults = (array) => {

            return array
                .filter(filterArray('number', number))
                .filter(filterArray('deal_number', deal_number))
                .filter(filterArray('deal_name', deal_name))
                .filter(filterCreatedFrom('created_milliseconds', created_from_in_milliseconds))
                .filter(filterCreatedTill('created_milliseconds', created_till_in_milliseconds))
                .filter(filterCreatedFrom('deal_closed_milliseconds', closed_from_in_milliseconds))
                .filter(filterCreatedTill('deal_closed_milliseconds', closed_till_in_milliseconds))
                .filter(this.filterDisputeStatus(dispute_status))
        };

        this.setState({
            collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
            isFiltering
        });
    }

    render() {
        const { collection, collectionFiltered, isFiltering, sortName, sortDirection, paginator, per_page, table_isLoading, dispute_statuses } = this.state;
        const collectionList = isFiltering ? collectionFiltered : collection;
        const { isMobile } = this.props;

        return (
            <div className="TableWithPagination">
                <div className="TableWrapper">
                    <table className="TableDefault TableDealsDispute js-table-rows-are-link">
                        <tbody>
                        <tr className="table-head">
                            <td className="head-of-element">
                                <h3>Сделки с открытым спором</h3>
                            </td>
                        </tr>
                        <tr className="FilterTableRow">
                            <td>
                               <span className="TableDisputes-OperatorCol">
                                  <input type="text" name="deal_number" placeholder="Номер" className="InputSearch" onChange={this.handleChangeFilter} />
                               </span>
                            </td>
                            <td>
                                <input type="text" name="deal_name" value={this.state.deal_name} placeholder="Поиск по названию" className="InputSearch" onChange={this.handleChangeFilter} />
                            </td>
                            <td>
                                <div className="DoubleInputDate">
                                    <input ref={input => this.datepickerFrom = input} type="text" name="created_from" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                                    <input ref={input => this.datepickerTill = input} type="text" name="created_till" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                                </div>
                            </td>
                            <td>
                                <div className="DoubleInputDate">
                                    <input ref={input => this.datepickerClosedFrom = input} type="text" name="closed_from" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                                    <input ref={input => this.datepickerClosedTill = input} type="text" name="closed_till" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                                </div>
                            </td>
                            <td>
                                <select name="dispute_status" onChange={this.handleChangeFilter}>
                                    <option value="">Статус спора</option>
                                    {
                                        dispute_statuses.map((status, i) =>
                                            <option
                                                key={i}
                                                value={status.status}
                                            >
                                                {status.status_name_including_deal_type}
                                            </option>
                                        )
                                    }
                                </select>
                            </td>
                            <td className="button-search-wrap">
                                <button className="ButtonSearch" onClick={this.getCollection} type="submit"><span>&nbsp;</span></button>
                            </td>
                        </tr>
                        <tr className="Head">
                            <td className="col col-number">
                                <LinkSort sortName="deal_number"
                                          label="Номер"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                />
                            </td>
                            <td>
                                <LinkSort sortName="deal_name"
                                          label="Название сделки"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                />
                            </td>
                            <td>
                                <LinkSort sortName="created"
                                          label="Дата открытия спора"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                />
                            </td>
                            <td>
                                <LinkSort sortName="deal_status"
                                          label="Дата окончания сделки"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                />
                            </td>
                            <td className="no-sort">Статус спора</td>
                            <td></td>
                        </tr>
                        {
                            collectionList.length !== 0 &&
                            !table_isLoading &&
                            collectionList.map(collectionItem => {
                                const { deal_id, deal_number, deal_name, created, status, deal_closed_date_by_contract } = collectionItem;
                                const url = `/deal/show/${deal_id}`;

                                return (
                                    <tr className="active" key={deal_number} onClick={() => !isMobile && goToLink(url)}>
                                        <td className="col col-number" data-label="Номер">
                                            <div className="col_mobile-title">Номер</div>
                                            <div className="col_mobile-content">{deal_number}</div>
                                        </td>
                                        <td className="col" data-label="Название сделки">
                                            <div className="col_mobile-title">Название сделки</div>
                                            <div className="col_mobile-content" title={deal_name}>{deal_name}</div>
                                        </td>
                                        <td className="col" data-label="Дата открытия спора">
                                            <div className="col_mobile-title">Дата открытия спора</div>
                                            <div className="col_mobile-content">{created}</div>
                                        </td>
                                        <td className="col" data-label="Дата окончания сделки">
                                            <div className="col_mobile-title">Дата окончания сделки</div>
                                            <div className="col_mobile-content">{deal_closed_date_by_contract}</div>
                                        </td>
                                        <td className="col" data-label="Статус спора">
                                            <div className="col_mobile-title">Статус спора</div>
                                            <div className="col_mobile-content" title={status.name}>{status.name}</div>
                                        </td>
                                        {!isMobile ?
                                            <td/>
                                            :
                                            <td className="to-deal-link-cell">
                                                <a href={url} className="ButtonApply" data-ripple-button="">
                                                    <span className="ripple-text">Перейти к сделке</span>
                                                </a>
                                            </td>
                                        }
                                    </tr>
                                );
                            })
                        }
                        {
                            (collectionList.length === 0 && collection.length !== 0 && !table_isLoading) &&
                            <tr className="EmptyRow">
                                <td colSpan="6">Сделки со спором не найдены.</td>
                            </tr>
                        }
                        {
                            collection.length === 0 &&
                            !table_isLoading &&
                            <tr className="EmptyRow">
                                <td colSpan="6">В системе нет сделок со спором.</td>
                            </tr>
                        }
                        {
                            table_isLoading &&
                            <tr className="EmptyRow">
                                <td colSpan="6" className='Preloader Preloader_solid'></td>
                            </tr>
                        }
                        </tbody>
                    </table>
                </div>
                <Paginator
                    paginator={paginator}
                    handlePageSwitch={this.handlePageSwitch}
                    per_page={per_page}
                    handlePerPageChange={this.handlePerPageChange}
                />
            </div>
        );
    }
}

export default withWindowDimensions(TableDealsDisputeOpen);