import React from 'react';
import ReactDOM from 'react-dom';
import Datepicker from '../forms/base/Datepicker';
import MESSAGES from '../constants/messages';
import URLS from '../constants/urls';
import LinkSort from '../forms/base/LinkSort';
import { sorting, goToLink, filterArray, filterArrayStrict, filterCreatedFrom, filterCreatedTill,
    getDealStatusesSplittedByDealTypes, getDealsWithFieldsSplittedByDealTypes, getUrlParameter, setUrlParameter,
} from '../Helpers';
import Paginator from '../Paginator';
import TableBase from './TableBase';
import Popup from '../Popup';
import withWindowDimensions from '../hocs/withWindowDimensions';

class TableDeals extends TableBase {
    constructor(props) {
        super(props);
        this.state = {
            collection: [],
            collectionFiltered: [],
            is_operator: null,
            // filters
            isFiltering: false,
            id: '',
            number: '',
            name: '',
            payment_amount: '',
            owner: '',
            created_from_in_milliseconds: '',
            created_from: '',
            created_till_in_milliseconds: '',
            created_till: '',
            deal_status: '',
            debit: '',
            // sort
            sortName: '',
            sortDirection: '',
            table_isLoading: false,
            paginator: {},
            page: 1,
            per_page: 20,
            unanswered: false,
            csrf: '',
            deal_statuses: [],
            filter: false,
            mobile_filter: false,
            mobile_sort: false,
            change_link_sort_icon: false, // флаг для изменения иконки сортировки (треугольника) в мобильной версии, меняется цвет треугольника с серого на белый при выборе сортировки
            is_verified: true,
            is_verified_notify_open: false
        };

        this.tableId = 'TableDeals';

        this.getCollection = this.getCollection.bind(this);
        this.handleChangeSort = this.handleChangeSort.bind(this);
        this.handleChangeFilter = this.handleChangeFilter.bind(this);
        this.filterStatus = this.filterStatus.bind(this);
        this.filterDebit = this.filterDebit.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handlePageSwitch = this.handlePageSwitch.bind(this);
        this.handlePerPageChange = this.handlePerPageChange.bind(this);
        this.handleResetForm = this.handleResetForm.bind(this);
        this.initDatepicker = this.initDatepicker.bind(this);
        this.showMobileFilter = this.showMobileFilter.bind(this);
        this.showMobileSort = this.showMobileSort.bind(this);
        this.filterSelected = this.filterSelected.bind(this);
        this.applyParametersFromUrl = this.applyParametersFromUrl.bind(this);
        this.handleOnPopState = this.handleOnPopState.bind(this);
        this.notifyPopupClose = this.notifyPopupClose.bind(this);
        this.notifyPopupOpen = this.notifyPopupOpen.bind(this);
        this.handleVerifiedClick = this.handleVerifiedClick.bind(this);
        this.handleDealClick = this.handleDealClick.bind(this);
    }

    componentWillMount() {
        this.applyParametersFromUrl();
        this.checkVerified();
    }

    componentDidMount() {
        super.componentDidMount();
        window.onpopstate = this.handleOnPopState;
    }

    componentWillUnmount() {
        window.onpopstate = () => {};
    }

    checkVerified() {
        const table = document.getElementById(this.tableId);
        if (table) {
            let is_verified = table.dataset.verified;
            if (is_verified !== undefined) {
                is_verified = is_verified ? true : false;

                this.setState({
                    is_verified,
                    is_verified_notify_open: !is_verified
                });
            }
        }
    }

    applyParametersFromUrl() {
        const url_params = window.location.search;

        const url_page_number = url_params.length ? +getUrlParameter(url_params, 'page') || this.state.page : 1;
        const url_per_page = url_params.length ? +getUrlParameter(url_params, 'per_page') || this.state.per_page : 20;
        const unanswered = url_params.length ? +getUrlParameter(url_params, 'unanswered') || this.state.unanswered : false;

        this.setState({
            page: url_page_number,
            per_page: url_per_page,
            unanswered: unanswered
        });
    }

    handleOnPopState() {
        this.applyParametersFromUrl();
        this.getCollection();
    }

    tableInit() {
        this.getStatuses();
        document.getElementById(this.tableId).style.visibility = 'visible';
    }

    getStatuses() {
        fetch(URLS.DEAL.FORM_INIT, {
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            },
            credentials: 'include'
        })
            .then((response) => {
                if (response.status === 200) {
                    return response.json();
                } else {
                    return Promise.reject(MESSAGES.SERVER_ERRORS.CRITICAL_ERROR.text);
                }
            })
            .then((data) => {
                if (data.status === 'SUCCESS') {
                    const deal_statuses = getDealStatusesSplittedByDealTypes(Object.values(data.data.deal_statuses));

                    this.setState({
                        deal_statuses
                    });
                } else if (data.status === 'ERROR') {
                    console.log('Ошибка при получении статусов');
                    return Promise.reject('Ошибка при получении статусов.');
                }
            })
            .catch(error => this.setState({formError: error}));
    }

    setFilterDataToSend() {
        const { name, payment_amount, created_from, created_till, deal_status, owner, is_operator, number, debit } = this.state;
        let { user_role } = this.state;
        const filter = {
            deal_name: name,
            deal_amount: payment_amount,
            deal_created_from: created_from,
            deal_created_till: created_till
        };

        if (is_operator) {
            filter.deal_owner = owner;
            filter.deal_number = number;
            if (debit) {
                filter.deal_debit_status = debit == 'underpayment' ? 1 : 2;
            }
        } else {
            // Так как фильтрация на бэке не заточена под дополнительные типы - заказчик исполнитель, смотрим что роль является подтипом customer либо contractor
            if (user_role === 'buyer') {
                user_role = 'customer';
            } else if (user_role === 'seller') {
                user_role = 'contractor';
            }
            filter.deal_role = user_role;
        }

        return filter;
    }

    getCollection(isNeedToRunHandleFilter) {
        const { page, csrf, per_page, unanswered } = this.state;
        const filter = this.setFilterDataToSend();

        this.toggleIsLoading();
        const data = unanswered ? {
            page,
            filter,
            csrf,
            per_page,
            unanswered
        } : {
            page,
            filter,
            csrf,
            per_page
        };

        $.ajax({
            type: 'GET',
            url: URLS.DEAL.SINGLE,
            data: data,
            success: (data) => {
                console.log(data);
                const { csrf, deals, is_operator, paginator } = data.data;

                // Разбиваем роли еще на два подтипа, взависимости от типа сделки - услуги, либо товара (покупатель, продавец, исполнитель, заказчик) и тд.
                const deals_with_fields_splitting_by_deal_type = getDealsWithFieldsSplittedByDealTypes(deals);

                this.setState({
                    collection: deals_with_fields_splitting_by_deal_type,
                    is_operator,
                    paginator,
                    csrf,
                    // collectionFiltered: []
                }, () => {
                    this.toggleIsLoading();
                    isNeedToRunHandleFilter && this.handleFilter();
                    if (this.state.collection.length === 0) {
                        this.setState({
                            isFiltering: true
                        })
                    }
                })
            }
        }).fail(() => this.toggleIsLoading());
    }

    filterDebit(debit) {
        return arrayItem =>
            !debit
            || debit === 'overpayment' && arrayItem.debit.overpayment > 0
            || debit === 'underpayment' && arrayItem.debit.underpayment > 0
            ;
    }

    handleFilter() {
        const { is_operator, collection, id, number, name, payment_amount, owner, user_role, created_from_in_milliseconds, created_till_in_milliseconds, deal_status, debit, sortName, sortDirection } = this.state;
        const isFiltering =
            number !== ''
            || name !== ''
            || payment_amount !== ''
            || owner !== ''
            || user_role !== ''
            || created_from_in_milliseconds !== ''
            || created_till_in_milliseconds !== ''
            || deal_status !== ''
            || debit !== ''
            || sortName !== ''
        ;

        const getFilteredResults = (array) => {
            return array
                .filter(filterArray('number', number))
                .filter(filterArray('name', name))
                .filter(filterArrayStrict('payment_amount_without_fee', payment_amount))
                .filter(is_operator ? filterArray('owner', owner) : filterArray('user_role_including_deal_type', user_role))
                .filter(filterCreatedFrom('created_milliseconds', created_from_in_milliseconds))
                .filter(filterCreatedTill('created_milliseconds', created_till_in_milliseconds))
                .filter(this.filterStatus(deal_status))
                .filter(this.filterDebit(debit))
        };

        this.setState({
            collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
            isFiltering
        });
    }

    // Метод для сброса данных из полей фильтра
    handleResetForm() {
        ReactDOM.findDOMNode(this.refs.name).value = '';
        ReactDOM.findDOMNode(this.refs.payment_amount).value = '';
        ReactDOM.findDOMNode(this.refs.user_role).value = '';
        ReactDOM.findDOMNode(this.refs.deal_status).value = '';
        this.datepickerFrom.value = '';
        this.datepickerTill.value = '';
        this.setState({
            filter: false,
            isFiltering: false,
            created_from: '',
            created_till: '',
            name: ''
        })
    }

    // Метод для работы датапикера в фильтре для мобилок
    initDatepicker() {
        this.initDatepickerFrom();
        this.initDatepickerTill();
    }

    showMobileFilter() {
        this.setState({
            mobile_filter: !this.state.mobile_filter,
        })
    }

    showMobileSort() {
        this.setState({
            mobile_sort: !this.state.mobile_sort
        })
    }

    filterSelected(e) {
        const cols = document.getElementsByClassName('col');
        for (let i = 0; i < cols.length; i++) {
            cols[i].classList.remove('col__clicked')
        }
        e.target.parentNode.classList.toggle('col__clicked');
        this.setState({
            change_link_sort_icon: true // флаг для изменения иконки сортировки (треугольника) в мобильной версии, меняется цвет треугольника с серого на белый при выборе сортировки
        })
    }

    handlePageSwitch(page) {
        const { sortName } = this.state;
        window.history.pushState({}, document.title, setUrlParameter(window.location.href, 'page', page));
        this.setState(() => {
            return {
                page: page,
                sortName
            }
        }, () => {
            this.applyParametersFromUrl();
            this.getCollection(true);
        });
    }

    handlePerPageChange(per_page) {
        const { sortName } = this.state;
        window.history.pushState({}, document.title, setUrlParameter(window.location.href, 'per_page', per_page));
        this.setState(() => {
            return {
                per_page: per_page,
                sortName
            }
        }, () => {
            this.applyParametersFromUrl();
            this.getCollection(true);
        });
    }

    notifyPopupClose() {
        this.setState({
            is_verified_notify_open: false
        });
    }

    notifyPopupOpen() {
        this.setState({
            is_verified_notify_open: true
        });
    }

    handleVerifiedClick(e) {
        const { is_verified } = this.state;

        if (!is_verified) {
            e.preventDefault();
            this.notifyPopupOpen();
        }
    }

    handleDealClick(url) {
        const { is_verified } = this.state;

        if (url && is_verified) {
            goToLink(url);
        } else if (!is_verified) {
            this.notifyPopupOpen();
        }
    }

    render() {
        const { collection, collectionFiltered, isFiltering, sortName, sortDirection, is_operator, table_isLoading, paginator, deal_statuses, debit, per_page, filter, created_from, created_till, mobile_filter, mobile_sort, change_link_sort_icon, is_verified, is_verified_notify_open } = this.state;
        const collectionList = isFiltering ? collectionFiltered : collection;
        const { isMobile, isTablet } = this.props;
        let createDealButtonOrSearchByIdInput;
        if (is_operator == null) {
            createDealButtonOrSearchByIdInput = (
                <span className="TableDeals-OperatorCol">
                    <input type="text" className="no-input" />
                </span>
            );
        } else if (is_operator === true) {
            createDealButtonOrSearchByIdInput = (
                <span className="TableDeals-OperatorCol">
                    <input type="text" name="number" placeholder="ID" onChange={this.handleChangeFilter}/>
                </span>
            );
        } else {
            createDealButtonOrSearchByIdInput = (
                < a
                    className = "ButtonApply"
                    href={URLS.DEAL.CREATE}
                    title="Добавить сделку"
                    onClick={this.handleVerifiedClick}
                    data-ripple-button=""
                >
                    <span className="ripple-text">Создать</span>
                </a>
            );
        }


        return (
            <div className="TableWithPagination">
                <div className="TableWrapper">

                    <table className="TableDefault TableDeals js-table-rows-are-link">
                        <tbody>
                        <tr className="table-head">
                            <td className="head-of-element">
                                <h3>Сделки</h3>
                            </td>
                        </tr>
                        <tr className="FilterRowButtonMobile">
                            <td>
                                { createDealButtonOrSearchByIdInput }
                            </td>
                            <td>
                                <div className="FilterColButton">
                                    <label
                                        className="ButtonEdit FilterButton SortButton js-button-sort-table-mobile"
                                        title="Сортировка"
                                        onClick={this.showMobileSort}
                                        data-ripple-button="">
                                        <span className="ripple-text">Сортировка</span>
                                    </label>
                                    <div className="wrap">
                                        <label
                                            htmlFor="filter-input-hidden"
                                            className="ButtonEdit FilterButton button-with-clear js-button-filter-table-mobile"
                                            title="Фильтр для таблицы"
                                            onClick={this.showMobileFilter}
                                            data-ripple-button=""
                                        >
                                            <span className="ripple-text">Фильтр</span>
                                        </label>
                                        {
                                            filter ?
                                                <button
                                                    id="filter-clear"
                                                    className="js-clear"
                                                    onClick={this.handleResetForm}
                                                />
                                                :
                                                void(0)
                                        }
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr className={mobile_filter ? "FilterTableRow js-filter-table-mobile active" : "FilterTableRow js-filter-table-mobile close"}>
                            <td>
                                { createDealButtonOrSearchByIdInput }
                            </td>
                            <td className="head-of-element">
                                <h3>Фильтр</h3>
                            </td>
                            <td className="buttons-wrap_filter-clear">
                                <div className="row">
                                    <div className="col-xl-12">
                                        <button onClick={this.handleResetForm} className="ButtonEdit button-for-filter" data-ripple-button="">
                                            <span className="ripple-text">Сбросить все</span>
                                        </button>
                                    </div>
                                </div>
                            </td>
                            <td className="col-name">
                                <input type="text" name="name" ref="name" value={this.state.name} placeholder="Название" className="InputSearch" onChange={this.handleChangeFilter} />
                            </td>
                            <td>
                                <input type="text" name="payment_amount" ref="payment_amount" placeholder="Сумма" className="InputSearch" onChange={this.handleChangeFilter} />
                            </td>
                            <td className="col-role">
                                {
                                    is_operator ?
                                        <input type="text" name="owner" placeholder="Владелец" className="InputSearch" onChange={this.handleChangeFilter} />
                                        :
                                        <select name="user_role" data-placeholder="Роль в сделке" ref='user_role' onChange={this.handleChangeFilter} >
                                            <option value="">Роль в сделке</option>
                                            <option value="customer">Заказчик</option>
                                            <option value="buyer">Покупатель</option>
                                            <option value="contractor">Исполнитель</option>
                                            <option value="seller">Продавец</option>
                                        </select>
                                }
                            </td>

                            {/*<td className="hide-for-merge">*/}
                            <td >
                                <label htmlFor="filter-tab-date" className="filter-tab" onClick={this.initDatepicker}> </label>
                                <div className="DoubleInputDate js-filter-date-open">
                                    <p className='Head'>{`Создана с ${created_from} - по ${created_till}`}</p>
                                    <input className="filter-tab-input" id="filter-tab-date" type="checkbox"/>
                                    <div className="DoubleInputDate-Wrap js-filter-date-block">
                                        <div className="head-of-element">
                                            <h3>Выбрать период</h3>
                                        </div>
                                        <div className="wrap-for-padding">
                                            <div className="row nested-row ButtonsRow">
                                                <div className="col-xl-6">
                                                    <button className="ButtonEdit button-for-filter js-filter-date-close" type="submit" data-ripple-button="">
                                                        <span className="ripple-text">Назад</span>
                                                    </button>
                                                </div>
                                                <div className="col-xl-6">
                                                    <button className="ButtonApply button-for-filter js-filter-date-close" type="submit" data-ripple-button="">
                                                        <span className="ripple-text">Применить</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <input ref={input => this.datepickerFrom = input} type="text" name="created_from" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                                            <input ref={input => this.datepickerTill = input} type="text" name="created_till" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                                        </div>
                                    </div>
                                </div>

                                <div className="filter-mobile-date-window js-tab-close">
                                </div>
                            </td>

                            <td>
                                <select name="deal_status" ref="deal_status" onChange={this.handleChangeFilter}>
                                    <option value="">Статус сделки</option>
                                    {
                                        deal_statuses.map(status =>
                                            <option
                                                key={status.status_name_including_deal_type}
                                                value={status.status_including_deal_type}
                                            >
                                                {status.status_name_including_deal_type}
                                            </option>
                                        )
                                    }
                                </select>
                            </td>
                            {
                                is_operator ?
                                    <td>
                                        <select name="debit" onChange={this.handleChangeFilter}>
                                            <option value="">Дебет</option>
                                            <option value="underpayment">Недоплаченные</option>
                                            <option value="overpayment">Переплаченные</option>
                                        </select>
                                    </td>
                                    :
                                    void(0)
                            }
                            <td className="ButtonsRow">
                                <div className="row">
                                    <div className="col-xl-2 col-md-3">
                                        <button className="ButtonEdit button-for-filter js-button-filter-close-2" type="submit" onClick={this.showMobileFilter} data-ripple-button="">
                                            <span className="ripple-text">Назад</span>
                                        </button>
                                    </div>
                                    <div className="col-xl-2 col-md-3">
                                        <button className="ButtonApply button-for-filter js-button-filter-close-2" type="submit" onClick={this.showMobileFilter} data-ripple-button="">
                                            <span className="ripple-text">Показать</span>
                                        </button>
                                    </div>
                                </div>
                                {/*<button className="ButtonSearch" onClick={this.getCollection}><span>&nbsp;</span></button>*/}
                            </td>
                            <td className="button-search-wrap">
                                <button className="ButtonSearch" onClick={this.getCollection} data-ripple-button=""><span>&nbsp;</span></button>
                            </td>
                            {/*<td>*/}
                            {/*<button className="ButtonSearch" onClick={this.handleResetForm}>X</button>*/}
                            {/*</td>*/}
                        </tr>
                        <tr className={mobile_sort ? "Head js-sort-table-mobile active" : "Head js-sort-table-mobile close"}>
                            <td className="head-of-element">
                                <h3>Сортировка</h3>
                            </td>
                            <td className="col col-number">
                                <LinkSort sortName="number"
                                          label="ID"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                          arrowSort={change_link_sort_icon}
                                />
                            </td>
                            <td className="col col-name" id='name' onClick={this.filterSelected}>
                                <LinkSort sortName="name"
                                          label="Название"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                          arrowSort={change_link_sort_icon}
                                />
                            </td>
                            <td className="col col-amount" onClick={this.filterSelected}>
                                <LinkSort sortName="payment_amount"
                                          label="Сумма"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                          arrowSort={change_link_sort_icon}
                                />
                            </td>
                            <td className="col col-role" onClick={this.filterSelected}>
                                {
                                    is_operator ?
                                        <LinkSort sortName="owner"
                                                  label="Владелец"
                                                  handleChangeSort={this.handleChangeSort}
                                                  isActiveSortName={sortName}
                                                  direction={sortDirection}
                                                  arrowSort={change_link_sort_icon}
                                        />
                                        :
                                        <LinkSort sortName="user_role"
                                                  label="Роль"
                                                  handleChangeSort={this.handleChangeSort}
                                                  isActiveSortName={sortName}
                                                  direction={sortDirection}
                                                  arrowSort={change_link_sort_icon}
                                        />
                                }
                            </td>
                            <td className="col col-date" onClick={this.filterSelected}>
                                <LinkSort sortName="created_milliseconds"
                                          label="Дата"
                                          handleChangeSort={this.handleChangeSort}
                                          isActiveSortName={sortName}
                                          direction={sortDirection}
                                          arrowSort={change_link_sort_icon}
                                />
                            </td>
                            <td className="col col-status no-sort">Статус</td>
                            {
                                is_operator ? <td className="col">Дебeт</td> : void(0)
                            }
                            <td className="ButtonsRow">
                                <button className="ButtonEdit button-for-filter js-close-sort" type="reset" onClick={this.showMobileSort} data-ripple-button>
                                    <span className="ripple-text">Назад</span>
                                </button>
                                <button className="ButtonApply button-for-filter js-close-sort" type="submit" onClick={this.showMobileSort} data-ripple-button>
                                    <span className="ripple-text">Применить</span>
                                </button>
                            </td>
                            <td className="col-empty">
                            </td>
                        </tr>
                        {
                            collectionList.length !== 0 &&
                            !table_isLoading &&
                            collectionList.map(collectionItem => {
                                const { id, number, name, payment_amount_without_fee, user_role, owner, created, deal_status, debit, type } = collectionItem;
                                const { matched, total_amount, overpayment, underpayment, number_of_payments } = debit;
                                const url = `/deal/show/${id}`;


                                let role;
                                switch (collectionItem.user_role_including_deal_type) {
                                    case 'customer':
                                        role = 'Заказчик';
                                        break;
                                    case 'buyer':
                                        role = 'Покупатель';
                                        break;
                                    case 'contractor':
                                        role = 'Исполнитель';
                                        break;
                                    case 'seller':
                                        role = 'Продавец';
                                        break;
                                    default:
                                        role = '';
                                }

                                const payments = [];
                                for (let i = 0; i < number_of_payments; i++) {
                                    payments.push(i);
                                }

                                return (
                                    <tr className="active" key={number} onClick={() => !isMobile && !isTablet && this.handleDealClick(url)}>
                                        <td className="col col-number">
                                            <div className="col_mobile-title">ID</div>
                                            <div className="col_mobile-content">{number}</div>
                                        </td>
                                        <td className="col col-name">
                                            <div className="col_mobile-title">Название</div>
                                            <div className="col_mobile-content" title={name}>{name}</div>
                                        </td>
                                        <td className="col col-amount">
                                            <div className="col_mobile-title">Сумма</div>
                                            <div className="col_mobile-content">{payment_amount_without_fee}</div>
                                        </td>
                                        {
                                            is_operator ?
                                                <td className="col col-role">
                                                    <div className="col_mobile-title">Владелец</div>
                                                    <div className="col_mobile-content" title={owner}> {owner}</div>
                                                </td>
                                                :
                                                <td className="col">
                                                    <div className="col_mobile-title">Вы</div>
                                                    <div className="col_mobile-content" title={role}>{role}</div>
                                                </td>
                                        }
                                        <td className="col">
                                            <div className="col_mobile-title">Создана</div>
                                            <div className="col_mobile-content">{created}</div>
                                        </td>
                                        <td className="col">
                                            <div className="col_mobile-title">Статус</div>
                                            <div className="col_mobile-content" title={deal_status.status_name_including_deal_type}>{deal_status.status_name_including_deal_type}</div>
                                        </td>
                                        {
                                            is_operator ?
                                                deal_status.code > 1 ?
                                                    <td className="col" style={{ color: `${matched ? 'green' : overpayment ? 'red' : 'blue'}` }}>
                                                        <div className="col_mobile-title">Оплата</div>
                                                        <div className="col_mobile-content"><strong>{total_amount}</strong></div>
                                                        <div>
                                              <span style={{ fontSize: '11px' }}>
                                                 {underpayment && `- ${underpayment}`}
                                                  {overpayment && `+ ${overpayment}`}
                                              </span>
                                                        </div>
                                                        <div style={{ width: '16px', height: '16px' }}>
                                                            {
                                                                payments.length > 0 &&
                                                                payments.map(index => {
                                                                    return <span key={index}>&#8381;</span>
                                                                })
                                                            }
                                                        </div>
                                                    </td>
                                                    : <td className="col-empty" />
                                                : void(0)
                                        }
                                        {!isMobile && !isTablet ?
                                            <td className="col-empty" />
                                            :
                                            <td className="to-deal-link-cell">
                                                <a href={url} className="ButtonApply" data-ripple-button="">
                                                    <span className="ripple-text">Перейти к сделке</span>
                                                </a>
                                            </td>
                                        }
                                        {/*<td className="col-empty"></td>*/}
                                    </tr>
                                );
                            })
                        }
                        {
                            (collectionList.length === 0 && collection.length !== 0 && !table_isLoading) &&
                            <tr className="EmptyRow">
                                {
                                    is_operator ?
                                        <td colSpan="8">Сделки не найдены.</td>
                                        :
                                        <td colSpan="7">Сделки не найдены.</td>
                                }
                            </tr>
                        }
                        {
                            collection.length === 0 &&
                            !table_isLoading &&
                            <tr className="EmptyRow">
                                <td colSpan="7">У вас еще нет сделок.</td>
                            </tr>
                        }
                        {
                            table_isLoading &&
                            <tr className="EmptyRow">
                                <td colSpan="7" className='Preloader Preloader_solid'></td>
                            </tr>
                        }
                        </tbody>
                    </table>
                </div>
                <Paginator
                    paginator={paginator}
                    handlePageSwitch={this.handlePageSwitch}
                    per_page={per_page}
                    handlePerPageChange={this.handlePerPageChange}
                />
                {
                    (!is_verified && is_verified_notify_open) &&
                    <Popup close={this.notifyPopupClose} popupClassName="verify-popup">
                        <div className="verify-message">
                            <h2 className="verify-message__title">Подтверждение аккаунта</h2>
                            <p className="verify-message__text">Для проведения безопасной сделки через сервис подтвердите E-mail и добавьте телефон.</p>
                            <a href="/profile" className="ButtonApply">перейти в Профиль</a>
                        </div>
                    </Popup>
                }
            </div>
        );
    }
}

export default withWindowDimensions(TableDeals);