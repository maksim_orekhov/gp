import React from 'react';
import FormLegalEntity from '../forms/FormLegalEntity';
import LinkSort from '../forms/base/LinkSort';
import {
    sorting, changeSortDirection, goToLink, filterArray, filterCreatedFrom, filterCreatedTill, customFetch,
    getPaymentOrdersWithCreatedInMillisecondsField, getDisputesWithFieldsSplittedByDealTypes
} from '../Helpers';
import Paginator from '../Paginator';
import TableBase from "./TableBase";
import URLS from "../constants/urls";

export default class TableUsers extends TableBase {
   constructor(props) {
       super(props);
       this.state = {
          collection: [],
          collectionFiltered: [],
          isOpenedFormCreate: false,
          table_isLoading: false,
          // filters
          isFiltering: false,
          id: '',
          login: '',
          created_from: '',
          created_till: '',
          created_till_in_milliseconds: '',
          created_from_in_milliseconds: '',
          csrf: '',
          // sort
          sortName: '',
          sortDirection: '',
          paginator: {},
          page: 1,
          per_page: 20
       };

       this.getCollection = this.getCollection.bind(this);
       this.handleChangeFilter = this.handleChangeFilter.bind(this);
       this.handleFilter = this.handleFilter.bind(this);
       this.handleChangeSort = this.handleChangeSort.bind(this);
       this.handlePageSwitch = this.handlePageSwitch.bind(this);
       this.handlePerPageChange = this.handlePerPageChange.bind(this);
   }

    tableInit() {
        document.getElementById('React-TableUsers').style.visibility = 'visible';
    }

   getCollection(isNeedToRunHandleFilter) {
       const { page, csrf, per_page } = this.state;
       const filter = this.setFilterDataToSend();

       this.toggleIsLoading();
       $.ajax({
           dataType: "json",
           type: 'GET',
           data: {
               page,
               per_page,
               csrf,
               filter
           },
           url: URLS.USER,
           success: (data) => {
               const { users, paginator } = data.data;

               const usersWithCreatedDate = getPaymentOrdersWithCreatedInMillisecondsField(users);
               console.log(data);
               this.setState({
                   collection: usersWithCreatedDate,
                   paginator
               }, () => {
                   this.toggleIsLoading();
                   isNeedToRunHandleFilter && this.handleFilter();
               });
           }
       }).fail(() => this.toggleIsLoading());
   }

   handleFilter() {
      const { collection, id, login, created_from_in_milliseconds, created_till_in_milliseconds, sortName, sortDirection } = this.state;
      const isFiltering =
         id !== ''
         || login !== ''
         || created_from_in_milliseconds !== ''
         || created_till_in_milliseconds !== ''
         || sortName !== ''
         ;

      const getFilteredResults = (array) => {
         return array
            .filter(filterArray('id', id))
            .filter(filterArray('login', login))
            .filter(filterCreatedFrom('created_milliseconds', created_from_in_milliseconds))
            .filter(filterCreatedTill('created_milliseconds', created_till_in_milliseconds))
            ;
      };

      this.setState({
         isNewItem: false,
         collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
         isFiltering
      });
   }

    setFilterDataToSend() {
        const { id, created_from, created_till, login, csrf } = this.state;
        const filter = {
            csrf,
            user_id: id,
            user_login: login,
            user_created_from: created_from,
            user_created_till: created_till
        };

        return filter;
    }

   render() {
      const { collection, collectionFiltered, isFiltering, sortName, sortDirection, isNewItem, paginator, per_page, table_isLoading } = this.state;
      const collectionList = isFiltering ? collectionFiltered : collection;

      return (
         <div className="TableWithPagination">
             <div className="TableWrapper">
               <table className="TableDefault TableUsers js-table-rows-are-link">
                  <tbody>
                  <tr className="table-head">
                      <td className="head-of-element">
                          <h3>Пользователи</h3>
                      </td>
                  </tr>
                     <tr className="FilterTableRow">
                        <td>
                           <input type="text" className="InputSearch" name="id" placeholder="ID" onChange={this.handleChangeFilter} />
                        </td>
                        <td>
                           <input type="text" className="InputSearch" name="login" placeholder="Поиск по логину" onChange={this.handleChangeFilter} />
                        </td>
                        <td>
                            <div className="DoubleInputDate">
                                <input ref={input => this.datepickerFrom = input} type="text" name="created_from" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                                <input ref={input => this.datepickerTillUsers = input} type="text" name="created_till" className="InputDate2 datepicker-here" onChange={this.handleChangeFilter} data-auto-close="true" />
                            </div>
                        </td>
                        <td/>
                        <td/>
                        <td/>
                        <td className="button-search-wrap">
                           <button className="ButtonSearch" type="submit" onClick={this.getCollection}><span>&nbsp;</span></button>
                        </td>
                     </tr>
                     <tr className="Head">
                        <td className="col col-number">
                           <LinkSort
                              sortName="id"
                              label="ID"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort
                              sortName="login"
                              label="Логин"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                         <td>
                             <LinkSort
                                 sortName="created"
                                 label="Дата регистрации"
                                 handleChangeSort={this.handleChangeSort}
                                 isActiveSortName={sortName}
                                 direction={sortDirection}
                             />
                         </td>
                        <td>
                           <LinkSort
                              sortName="email"
                              label="Email"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort
                              sortName="phone"
                              label="Телефон"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort
                              sortName="roles"
                              label="Роли"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td></td>
                     </tr>
                     {
                        collectionList.length !== 0 &&
                        !table_isLoading &&
                        collectionList.map((collectionItem, index) => {
                           const { id, login, email, phone, roles, created } = collectionItem;
                           const url = `/user/${id}`;
                           return (
                              <tr key={id} className={`active ${isNewItem && index === 0 ? 'highlighted' : ''}`} onClick={() => goToLink(url)}>
                                  <td className="col col-number">
                                      <div className="col_mobile-title">
                                          ID
                                      </div>
                                      <div className="col_mobile-content">
                                          {id}
                                      </div>
                                  </td>
                                  <td className="col">
                                      <div className="col_mobile-title">
                                          Наименование
                                      </div>
                                      <div className="col_mobile-content">
                                          <a href={url}>{login}</a>
                                      </div>
                                  </td>
                                  <td className="col">
                                      <div className="col_mobile-title">
                                          КПП
                                      </div>
                                      <div className="col_mobile-content">
                                          {created}
                                      </div>
                                  </td>
                                  <td className="col">
                                      <div className="col_mobile-title">
                                          Адрес
                                      </div>
                                      <div className="col_mobile-content" title={email}>
                                          {email}
                                      </div>
                                  </td>
                                  <td className="col">
                                      <div className="col_mobile-title">
                                          Телефон
                                      </div>
                                      <div className="col_mobile-content">
                                          {phone}
                                      </div>
                                  </td>
                                  <td className="col">
                                      <div className="col_mobile-title">
                                          ИНН
                                      </div>
                                      <div className="col_mobile-content">
                                          {roles}
                                      </div>
                                  </td>
                                 <td/>
                              </tr>
                           );
                        })
                     }
                     {
                        (collectionList.length === 0 && collection.length !== 0 && !table_isLoading) &&
                        <tr className="EmptyRow">
                           <td colSpan="7">Пользователи не найдены.</td>
                        </tr>
                     }
                     {
                        collection.length === 0 &&
                        !table_isLoading &&
                        <tr className="EmptyRow">
                           <td colSpan="7">В системе еще нет ни одного пользователя.</td>
                        </tr>
                     }
                     {
                         table_isLoading &&
                         <tr className="EmptyRow">
                             <td colSpan="7" className='Preloader Preloader_solid' />
                         </tr>
                     }
                  </tbody>
               </table>
            </div>
             <Paginator
                 paginator={paginator}
                 handlePageSwitch={this.handlePageSwitch}
                 per_page={per_page}
                 handlePerPageChange={this.handlePerPageChange}
             />
         </div>
      );
   }
}
