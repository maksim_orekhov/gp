import React from 'react';
import FormNaturalPerson from '../forms/FormNaturalPerson';
import LinkSort from '../forms/base/LinkSort';
import Popup from '../Popup';
import { sorting, changeSortDirection, filterFullName, filterArray, replaceRusLetterE, customFetch } from '../Helpers';
import URLS from "../constants/urls";
import PropTypes from 'prop-types';

export default class TableNaturalPersons extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         collection: [],
         collectionFiltered: [],
         collectionItemEdit: {},
         isOpenedFormCreate: false,
         isOpenedFormEdit: false,
         isNewItem: false,
          table_isLoading: false,
         // filters
         isFiltering: false,
         fullName: '',
         birthDate: '',
         // sort
         sortName: '',
         sortDirection: ''
      };

      this.updateCollection = this.updateCollection.bind(this);
      this.updateCollectionAfterDelete = this.updateCollectionAfterDelete.bind(this);
      this.handleChangeFilter = this.handleChangeFilter.bind(this);
      this.handleFilter = this.handleFilter.bind(this);
      this.clearFilters = this.clearFilters.bind(this);
      this.handleChangeSort = this.handleChangeSort.bind(this);
      this.toggleFormCreate = this.toggleFormCreate.bind(this);
      this.toggleFormEdit = this.toggleFormEdit.bind(this);
      this.handleClickFilter = this.handleClickFilter.bind(this);
   }

   componentDidMount() {
       this.initDatepicker();
       if (this.props.is_nested) {
           const { naturalPersons } = this.props;
           naturalPersons && this.setState({
               collection: naturalPersons
           });
       } else {
           this.getCollection();
       }
   }

    componentWillReceiveProps(nextProps) {
        const { naturalPersons } = nextProps;
        naturalPersons && this.setState({
            collection: naturalPersons
        });
    }

    getCollection() {
        this.toggleIsLoading();
        customFetch(URLS.NATURAL_PERSON.SINGLE)
            .then(data => {
                console.log(data);
                this.toggleIsLoading();
                if (data.status === 'SUCCESS') {
                    this.setState({
                        collection: data.data.naturalPersons,
                        is_operator: data.data.is_operator
                    });
                }
            })
            .catch(() => {
                this.toggleIsLoading();
                this.setState({
                    is_operator: true
                });
            });
    }

   updateCollection() {
       if (this.props.is_nested) {
           this.setState({
               isOpenedFormEdit: false
           }, () => {
               this.clearFilters();
           });
           this.props.updateCollection(false, true);
       } else {
           customFetch(URLS.NATURAL_PERSON.SINGLE)
               .then(data => {
                   console.log(data);
                   this.setState({
                       collection: data.data.naturalPersons
                   }, () => {
                       this.clearFilters();
                       this.highlightNewItem();
                   });
               });
       }
   }

   updateCollectionAfterDelete() {
       if (this.props.is_nested) {
           this.setState({
               isOpenedFormEdit: false
           }, () => {
               this.clearFilters();
           });
           this.props.updateCollection(false, false);
       } else {
           customFetch(URLS.NATURAL_PERSON.SINGLE)
               .then(data => {
                   console.log(data);
                   this.setState({
                       isOpenedFormEdit: false,
                       collection: data.data.naturalPersons
                   }, () => {
                       this.clearFilters();
                   });
               });
       }
   }

    highlightNewItem() {
        this.setState({ isNewItem: true });
        setTimeout(() => {
            this.setState({ isNewItem: false });
        }, 3000);
    }

   initDatepicker() {
      $(this.datepicker).datepicker({
         onSelect: formattedDate => {
            this.setState({
               birthDate: formattedDate
            }, () => this.handleFilter());
         }
      });
   }

   handleChangeFilter(e) {
      const { name, value } = e.target;
      this.setState({
         [name]: replaceRusLetterE(value.trim())
      }, () => this.handleFilter());
   }

   handleFilter() {
      const { collection, fullName, birthDate, sortName, sortDirection } = this.state;
      const isFiltering =
         fullName !== ''
         || birthDate !== ''
         || sortName !== ''
         ;

      const getFilteredResults = (array) => {
         return array
            .filter(filterFullName(fullName))
            .filter(filterArray('birth_date', birthDate))
            ;
      };

      this.setState({
         isNewItem: false,
         collectionFiltered: sorting(getFilteredResults(collection), sortName, sortDirection),
         isFiltering
      });
   }

   clearFilters() {
      this.setState({
         collectionFiltered: [],
         isFiltering: false,
         isOpenedFormCreate: false,
         fullName: '',
         birthDate: '',
         sortName: '',
         sortDirection: ''
      });
   }

   handleChangeSort(name) {
      const { sortName, sortDirection } = this.state;
      this.setState({
         sortName: name,
         sortDirection: changeSortDirection(name, sortName, sortDirection)
      }, () => this.handleFilter());
   }

   toggleFormCreate() {
      this.setState({
         isOpenedFormCreate: !this.state.isOpenedFormCreate
      });
   }

   toggleFormEdit(collectionItem) {
      this.setState({
         collectionItemEdit: collectionItem,
         isOpenedFormEdit: !this.state.isOpenedFormEdit
      });
   }

    toggleIsLoading() {
        this.setState({
            table_isLoading: !this.state.table_isLoading
        });
    }

    handleClickFilter() {
        window.location = '/profile'
    }

   render() {
       const { isOperator, is_nested } = this.props;
      const { collection, collectionFiltered, isFiltering, sortName, sortDirection, isOpenedFormCreate, isOpenedFormEdit, collectionItemEdit } = this.state;
      const collectionList = isFiltering ? collectionFiltered : collection;

      return (
         <div>
             {
                 !isOperator &&
            <div className="TableWrapper">
               <table className="TableDefault TableNaturalPersons js-table-rows-are-link">
                  <tbody>
                     <tr className="FilterRowButtonMobile">
                        <td>
                           <a className="ButtonApply FilterButton" onClick={this.toggleFormCreate} title="Добавить физическое лицо" data-ripple-button="">
                               Добавить
                           </a>
                        </td>
                        <td>
                           {/*<div className="FilterColButton">*/}
                              {/*<div className="wrap">*/}
                                 {/*<label htmlFor="filter-input-hidden-natural-person" className="ButtonEdit FilterButton button-with-clear js-button-filter-table-mobile" title="Фильтр для таблицы">Фильтр*/}
                                 {/*</label>*/}
                              {/*</div>*/}
                           {/*</div>*/}
                        </td>
                        <td></td>
                        <td></td>
                     </tr>
                     <tr className="FilterTableRow js-filter-table-mobile">
                        <td>
                            <a className="ButtonApply" /* href="/profile/natural-person/create" */ title="Добавить физическое лицо" onClick={this.toggleFormCreate} data-ripple-button="">
                                Добавить
                            </a>
                        </td>
                        <td className="head-of-element">
                           <h3>Фильтр</h3>
                        </td>
                        <td className="col-name">
                           <input className="InputSearch" type="text" name="fullName" value={this.state.fullName} placeholder="Поиск по ФИО" onChange={this.handleChangeFilter} />
                        </td>
                        <td></td>
                        <td></td>
                     </tr>
                     <tr className="Head">
                        <td>
                           <LinkSort sortName="index"
                              label="№"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td className="col col-name">
                           <LinkSort sortName="last_name"
                              label="ФИО"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td>
                           <LinkSort sortName="birth_date_milliseconds"
                              label="Дата рождения"
                              handleChangeSort={this.handleChangeSort}
                              isActiveSortName={sortName}
                              direction={sortDirection}
                           />
                        </td>
                        <td></td>
                        <td></td>
                     </tr>
                     {
                        collectionList.lenght !== 0 &&
                        collectionList.map((collectionItem, index) => {
                           const { id, last_name, first_name, secondary_name, birth_date, documents } = collectionItem;
                           const url = `/profile/natural-person/${id}`;
                           return (
                              <tr key={id} className={`active ${is_nested ?
                                  this.props.isNewItem && index === collectionList.length - 1 ? 'highlighted' : ''
                                  :
                                  this.state.isNewItem && index === 0 ? 'highlighted' : '' }`} /* onClick={() => goToLink(url)} */>
                                 <td className="col col-number">
                                    <div className="col_mobile-content">{index + 1}</div>
                                 </td>
                                 <td className="col col-name ">
                                    <div className="col_mobile-title">Фамилия И.О.</div>
                                    <div className="col_mobile-content"> <a href={url}>{last_name} {first_name} {secondary_name}</a></div>
                                 </td>
                                 <td className="col">
                                    <div className="col_mobile-title">Дата рождения</div>
                                    <div className="col_mobile-content"> {birth_date}</div>
                                 </td>
                                 <td></td>
                                 <td>
                                    <span className="IconEdit" onClick={() => this.toggleFormEdit(collectionItem)}></span>
                                 </td>
                              </tr>
                           );
                        })
                     }
                     {
                        (isFiltering && collectionList.length === 0 && collection.length !== 0) &&
                        <tr className="EmptyRow">
                           <td colSpan="5">Физические лица не найдены.</td>
                        </tr>
                     }
                     {
                        collection.length === 0 &&
                        <tr className="EmptyRow">
                           <td colSpan="5" className={!is_nested && this.state.table_isLoading ? 'Preloader Preloader_solid' : null}>Вы еще не добавили в свой профиль ни одного физического лица.</td>
                        </tr>
                     }
                  </tbody>
               </table>

            </div>
             }

            {
               isOpenedFormCreate &&
               <Popup close={this.toggleFormCreate}>
                        <FormNaturalPerson afterSubmit={this.updateCollection} />
                         {/* <FormNaturalPerson afterSubmit={this.updateCollection} /> */}
               </Popup>
            }
            {
               isOpenedFormEdit &&
               <Popup close={() => this.toggleFormEdit({})}>
                  <FormNaturalPerson
                     data={collectionItemEdit}
                     editMode={true}
                     afterSubmit={this.updateCollectionAfterDelete}
                     afterDelete={this.updateCollectionAfterDelete}
                     item_id={collectionItemEdit}
                  />
               </Popup>
            }
         </div>
      );
   }
}

TableNaturalPersons.propTypes = {
    tableIsLoading: PropTypes.bool,
    isOperator: PropTypes.bool,
    isNewItem: PropTypes.bool,
    naturalPersons: PropTypes.array,
    is_nested: PropTypes.bool
};
