import React from 'react';
import PropTypes from 'prop-types';

/**
 * HOC, проверяющий есть ли в localStorage уже сохраненная форма
 * @param {string} formName - имя формы, под которым она хранится в localStorage
 * @returns {function(*): function(*): *}
 */
const withArchiveFormMessage = formName => Component => {
  const formArchiveExist = () => {
    return localStorage && formName && localStorage.hasOwnProperty(formName);
  };
  const WithArchiveFormMessage = props => {
    return <Component {...props} formArchiveIsExist={formArchiveExist()} />
  };

  const wrappedComponentName = Component.displayName || Component.name || 'Component';
  WithArchiveFormMessage.displayName = `withArchiveFormMessage(${wrappedComponentName})`;

  return WithArchiveFormMessage;
};

withArchiveFormMessage.propTypes = {
  formName: PropTypes.string.isRequired,
};

export default withArchiveFormMessage;
