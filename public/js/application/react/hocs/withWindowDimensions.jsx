import React from 'react';
import PropTypes from 'prop-types';
import { BREAKPOINT_MOBILE, BREAKPOINT_TABLET } from '../constants/breakpoints';

const withWindowDimensions = Component => {
    return class withWindowDimensions extends React.Component{
        state = {
            windowWidth: 0,
            windowHeight: 0,
        };

        componentDidMount(){
            this.updateDimensions();
            window.addEventListener('resize', this.updateDimensions);
        }

        componentWillUnmount(){
            window.removeEventListener('resize', this.updateDimensions);
        }

        updateDimensions = () => {
            const w = window,
                d = document,
                documentElement = d.documentElement,
                body = d.getElementsByTagName('body')[0],
                width = w.innerWidth || documentElement.clientWidth || body.clientWidth,
                height = w.innerHeight|| documentElement.clientHeight || body.clientHeight;

            this.setState({ windowWidth: width, windowHeight: height});
        };

        render(){
            return <Component {...this.props} {...this.state} isMobile={this.state.windowWidth < BREAKPOINT_MOBILE}
                              isTablet={this.state.windowWidth < BREAKPOINT_TABLET && this.state.windowWidth >= BREAKPOINT_MOBILE} />
        }
    }
};

withWindowDimensions.propTypes = {
    Component: PropTypes.element
};

export default withWindowDimensions;
