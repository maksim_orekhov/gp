import React from 'react';
import DisputeResolveRequestsSwitcher from './DisputeResolveRequestsSwitcher';
import FormArbitrageRequest from './forms/FormArbitrageRequest';
import FormTribunalRequest from './forms/FormTribunalRequest';
import FormRefundRequest from './forms/FormRefundRequest';
import FormDiscountRequest from './forms/FormDiscountRequest';
import URLS from './constants/urls';
import MESSAGES from './constants/messages';
import { customFetch } from './Helpers';

export default class DisputeRequestCreateForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            csrf: '',
            deal_id: null,
            activeTab: 'discount',
            isDropDownOpen: false,
            description: [],
            form_is_loading: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.toggleDropDownOpen = this.toggleDropDownOpen.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
    };

    componentWillMount() {
        this.getDealIdFromUrl();
        this.setCurrentTab();
    }

    componentDidMount() {
        this.getCsrf();
    }

    getDealIdFromUrl() {
        const url = window.location.href;
        const deal_id = url.substring(url.lastIndexOf('/') + 1);
        this.setState({
            deal_id
        });
    }

    getCsrf() {
        this.toggleIsLoading();
        customFetch(URLS.CSRF.GET)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    this.toggleIsLoading();
                    const { csrf } = data.data;
                    this.setState({
                        csrf
                    });
                } else if (data.status === 'ERROR') {
                    console.log('Ошибка при получении csrf');
                    return Promise.reject('Ошибка при получении инициализации формы. Попробуйте обновить страницу.');
                }
            })
            .catch(error => {
                this.setState({formError: error});
                this.toggleIsLoading();
            });
    }

    toggleIsLoading() {
        this.setState({
            form_is_loading: !this.state.form_is_loading
        })
    }

    handleOnSubmit(isNeedToUpdateRequestsList) {
        const { handleOnSubmit } = this.props;
        handleOnSubmit && handleOnSubmit(isNeedToUpdateRequestsList);
    }

    handleChange(e) {
        const activeTab = e.target.dataset.value;
        this.setState({
            activeTab,
            isDropDownOpen: false
        })
    }

    getResultRequestNames() {
        const rulesRequest = {
            is_allowed_discount_request: 'discount',
            is_allowed_refund_request: 'refund',
            is_allowed_arbitrage_request: 'arbitrage',
            is_allowed_tribunal_request: 'tribunal'
        };

        const { is_allowed_discount_request, is_allowed_refund_request, is_allowed_arbitrage_request, is_allowed_tribunal_request } = this.props;

        const requests = {
            is_allowed_discount_request,
            is_allowed_refund_request,
            is_allowed_arbitrage_request,
            is_allowed_tribunal_request
        };

        const resultRequest = [];

        Object.keys(requests).map(function(key) {
            if (requests[key] === true) {
                resultRequest.push(rulesRequest[key]);
            }
        });

       return resultRequest.filter(element => element !== null);
    }

    getElementRules() {
        const { deal_type, is_customer } = this.props;
        const elementRules = {
            discount: {
                label: 'Скидка',
                description: ''
            },
            refund: {
                label: (deal_type === 'T') ? 'Возврат товара' : 'Отказ от услуги',
                description: is_customer ?
                    (deal_type === 'T')
                        ? 'В срок не более 3-х рабочих дней Вам будет необходимо организовать доставку товара продавцу.'
                        : 'В срок не более 1-го рабочего дня Вам будет необходимо сообщить исполнителю об отказе от услуги.'
                    : (deal_type === 'T')
                        ? 'В срок не более 3-х рабочих дней покупателю будет необходимо вернуть Вам товар.'
                        : 'В срок не более 1-го рабочего дня заказчику будет необходимо сообщить Вам об отказе от услуги.'
            },
            arbitrage: {
                label: 'Согласительная комиссия',
                description: 'После присвоения спору статуса Суд, в течение 10 дней Вам будет необходимо подтвердить данное решение, направив администрации копию обращения за судебной защитой.'
            },
            tribunal: {
                label: 'Суд',
                description: 'После присвоения спору статуса Суд, в течение 10 дней Вам будет необходимо подтвердить данное решение, направив администрации копию обращения за судебной защитой.'
            }
        };
        return elementRules;
    }

    getElements() {
        const element_rules = this.getElementRules();
        const result_request_names = this.getResultRequestNames();

        const select_elements = [];
        for (let i = 0; i < result_request_names.length; i++) {
            select_elements[result_request_names[i]] = element_rules[result_request_names[i]];
        }

        return Object.entries(select_elements).map((element, index) => <li key={index} onClick={this.handleChange} data-value={element[0]}>{element[1].label}</li>)
    }

    getDescription() {
        const element_rules = this.getElementRules();
        const result_request_names = this.getResultRequestNames();

        const select_elements = [];
        for (let i = 0; i < result_request_names.length; i++) {
            select_elements[result_request_names[i]] = element_rules[result_request_names[i]];
        }

        const elements = Object.entries(select_elements).map(element => element[1]);

        return elements[1].description
    }

    toggleDropDownOpen() {
        this.setState({
            isDropDownOpen: !this.state.isDropDownOpen
        })
    }

    setCurrentTab() {
        const resultNames = this.getResultRequestNames();

        let currentTab = this.state.activeTab;
        if (resultNames.length) {
            currentTab = resultNames[0]
        }
        this.setState({
            activeTab: currentTab
        });
    }

    render() {
        const { activeTab, csrf, deal_id, isDropDownOpen, form_is_loading } = this.state;
        const {
            max_discount_amount,
            handleOnSubmit
        } = this.props;
        const description = this.getDescription();
        return (
            <div className={form_is_loading ? 'Preloader Preloader_solid' : ''}>
                <DisputeResolveRequestsSwitcher
                    activeTab={activeTab}
                    elementRules={this.getElementRules()}
                    list_items={this.getElements()}
                    toggleDropDownOpen={this.toggleDropDownOpen}
                    isDropDownOpen={isDropDownOpen}
                />
                {{
                    tribunal: (
                        <FormTribunalRequest handleOnSubmit={handleOnSubmit} csrf={csrf} deal_id={deal_id} />
                    ),
                    arbitrage: (
                        <FormArbitrageRequest handleOnSubmit={handleOnSubmit} csrf={csrf} deal_id={deal_id} />
                    ),
                    refund: (
                        <FormRefundRequest handleOnSubmit={handleOnSubmit} csrf={csrf} deal_id={deal_id} description={description}/>
                    ),
                    discount: (
                        <FormDiscountRequest handleOnSubmit={handleOnSubmit} csrf={csrf} deal_id={deal_id} max_discount_amount={max_discount_amount}/>
                    )
                }[activeTab]}
            </div>
        )
    }
}
