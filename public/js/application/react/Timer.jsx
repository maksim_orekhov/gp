import React from 'react';
import update from 'immutability-helper';

export default class Timer extends React.Component {
    constructor() {
        super();
        this.state =    {
            time_amount: 0
        }
    }

    componentDidMount() {
        this.setState({
            time_amount:  this.props.start
        });
        this.timer = setInterval(this.tick.bind(this), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    tick() {
        this.setState({
            time_amount: this.state.time_amount - 1
        });
        this.counterCheck();
    }

    counterCheck() {
        const { expired } = this.props;
        if (this.state.time_amount < 1) {
            expired && expired(true);
        }
    }

    render() {
        const { shutDown, isShowTime } = this.props;

        if (shutDown) {
            this.componentWillUnmount();
        }

        // Если есть props isShowTime то выводим обратный отсчет
        if (isShowTime) {
            return <span>{this.state.time_amount}</span>
        }

        return(
            <span />
        );
    };
}