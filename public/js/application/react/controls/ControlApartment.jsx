import React from 'react';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';
import {textOnlyNumbers} from "../Helpers";
import VALIDATION_RULES from "../constants/validation_rules";

export default class ControlApartment extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                min_chars: null,
                undefined_error: false
            },
            error_messages: {
                undefined_error: ''
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.APARTMENT.min_chars
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent(e) {
        const value = textOnlyNumbers(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                min_chars: !this.validateMinCharsLengthOrEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateMinCharsLengthOrEmpty(value)
        );
        // return true;
    }

    render() {
        const { name, label, placeholder, value_prop, is_disable } = this.props;
        const { error_messages, validation_errors } = this.state;
        return (
            <div className="col-xl-3 col-sm-2">
                <div className={`Input-Wrapper ${this.colourInputField()}`}>
                    <label htmlFor={name}>{label}</label>
                    <input
                        name={name}
                        tabIndex="0"
                        type="text"
                        onChange={this.handleChangeEvent}
                        placeholder={placeholder}
                        value={value_prop}
                        onBlur={this.handleChangeEvent}
                        disabled={is_disable}
                    />
                    <ShowError messages={error_messages} existing_errors={validation_errors} />
                </div>
            </div>
        );
    }
};