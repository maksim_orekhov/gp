import React from 'react';
import ShowError from '../../../application/react/ShowError.jsx';
import EmailBase from "../../../landing/react/auth/forms/RegisterForm/controls/EmailBase";
import MESSAGES from "../../../application/react/constants/messages";
import ShowHint from '../ShowHint';


export default class ControlEmail extends EmailBase {
  constructor() {
    super();
    this.state = {
      disable: false,
      show_hint: true,
      validation_errors:{
          reg_exp_invalid:      null,
          email_unavailable:    false,
          connection_is_lost:   false,
          email:                false,
          undefined_error:      false,
      },
      onFocus: ' ',
      hints:{
          email_rules: false
      },
      error_messages: {
        isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
        reg_exp_invalid: MESSAGES.VALIDATION_ERRORS.email_reg_exp_invalid,
        email_unavailable: MESSAGES.VALIDATION_ERRORS.email_unavailable,
        email: MESSAGES.VALIDATION_ERRORS.email_reg_exp_invalid,
        undefined_error: ''
      }
    };

      this.handleChangeEvent = this.handleChangeEvent.bind(this);
      // this.handleBlur = this.handleBlur.bind(this);
      this.handleFocus = this.handleFocus.bind(this);
  };

    componentWillMount() {
        this.checkValidateOnMount();
    }

  componentWillReceiveProps(nextProps, nextState){
      super.componentWillReceiveProps(nextProps, nextState);
      if(this.props.formHasDataAtLocalStorage && !nextProps.formHasDataAtLocalStorage){
          this.setState({
              validation_errors:{
                  reg_exp_invalid:      null,
                  email_unavailable:    false,
                  connection_is_lost:   false,
                  email:                false,
                  undefined_error:      false,
              },
          });
      }
  }


    render() {
        const { validation_errors, disable, onFocus, error_messages, show_hint } = this.state;
        const { value_prop, name, label, is_agent_email, placeholder, no_hint } = this.props;
        const hints = {base_hint: true};
        const hint_messages = is_agent_email ? {base_hint: MESSAGES.HINTS.agent_email} : {base_hint: MESSAGES.HINTS.email};
        return (
            <div className={`form-field ${onFocus}`}>
                <label  htmlFor="login-email">{label || 'Email'}</label>
                <div className="form-input">
                    <input
                       type="email"
                       className={this.colourInputField()}
                       id="login-email"
                       value={value_prop}
                       onFocus={this.handleFocus}
                       onBlur={this.handleChangeEvent}
                       onChange={this.handleChangeEvent}
                       disabled={disable}
                       name={name}
                       placeholder={placeholder}
                    />
                    <ShowError
                        messages={error_messages}
                        existing_errors={validation_errors}
                    />
                    {
                        show_hint && !no_hint &&
                        <ShowHint messages={hint_messages} existing_hints={hints} />
                    }
                </div>
            </div>
        );
    }
}
