import React from 'react';
import MESSAGES from '../constants/messages';
import ControlBase from './ControlBase';
import ShowError from '../ShowError';
import ShowHint from '../ShowHint';

export default class ControlDeliveryPayerOption extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    render() {
        const { deal_role, label, is_disable, name, value_prop } = this.props;
        const { error_messages, validation_errors } = this.state;
        const hints = {base_hint: true};
        const hint_messages = {base_hint: MESSAGES.HINTS.delivery_option};

        return (
            <div className="row nested-row">
                <div className="col-xl-12 role-in-deal">
                    <div className="Input-Wrapper">
                        <label className="label-double-indent" htmlFor="delivery_payer_option_customer">{label}</label>
                        <div className="InputRadio">
                            <div className="row nested-row">
                                <div className="col-xl-3 col-sm-2">
                                    <input
                                        type="radio"
                                        name={name}
                                        value="customer"
                                        id="delivery_payer_option_customer"
                                        checked={value_prop === 'customer'}
                                        onChange={this.handleChangeEvent}
                                        onBlur={this.handleChangeEvent}
                                        disabled={is_disable}
                                    />
                                    <label htmlFor="delivery_payer_option_customer">
                                        <span className="radiobutton" />
                                        Покупатель
                                    </label>
                                </div>
                                <div className="col-xl-3 col-sm-2">
                                    <input
                                        type="radio"
                                        name={name}
                                        value="contractor"
                                        id="delivery_payer_option_contractor"
                                        checked={value_prop === 'contractor'}
                                        onChange={this.handleChangeEvent}
                                        onBlur={this.handleChangeEvent}
                                        disabled={is_disable}
                                    />
                                    <label htmlFor="delivery_payer_option_contractor">
                                        <span className="radiobutton" />
                                        Продавец
                                    </label>
                                </div>
                            </div>
                        </div>
                        <ShowError messages={error_messages} existing_errors={validation_errors} />
                        <ShowHint messages={hint_messages} existing_hints={hints} />
                    </div>
                </div>
            </div>
        )
    }
};