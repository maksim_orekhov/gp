import React from 'react';

export default class ControlSelectImitation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isDropDownOpen: false
        };

        this.toggleDropDownOpen = this.toggleDropDownOpen.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    componentWillMount() {
        document.addEventListener('click', this.handleClickOutside, false);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside, false);
    }

    handleClickOutside(e) {
        if (!this.node.contains(e.target)) {
            if (this.state.isDropDownOpen) {
                this.setState({
                    isDropDownOpen: false
                });
            }
        }
    }

    toggleDropDownOpen() {
        this.setState({
            isDropDownOpen: !this.state.isDropDownOpen
        });
    }

    getActiveTab() {
        const { default_option, active_tab, custom_label } = this.props;
        
        if (custom_label) {
            return custom_label;
        }
        if (active_tab) {
            return active_tab;
        }
        return default_option;
    }

    render() {
        const { isDropDownOpen } = this.state;
        const { options, id, is_disable, is_color_picker, color_block } = this.props;
        const active_tab = this.getActiveTab();

        if (is_disable) {
            return (
                <div className="SelectImitation disabled" ref={node => this.node = node}>
                    <div className="SelectImitation-Container">
                        <span
                            className='SelectImitation-Text'
                            title={active_tab}
                        >
                            {active_tab}
                        </span>
                    </div>
                </div>
            );
        }

        return (
            <div className="SelectImitation" ref={node => this.node = node}>
                <div className="SelectImitation-Container">
                    <label
                        className={`SelectImitation-Text ${isDropDownOpen ? 'active': ''}`}
                        htmlFor={id}
                        onClick={this.toggleDropDownOpen}
                        title={active_tab}
                    >
                        {
                            is_color_picker ?
                                color_block
                                :
                                active_tab
                        }
                    </label>
                    <input
                        id={id}
                        className="SelectImitation-Checkbox"
                        type="checkbox"
                        checked={isDropDownOpen}
                    />
                    <ul
                        className="SelectImitation-Menu"
                        onClick={this.toggleDropDownOpen}
                    >

                        {options}
                    </ul>
                </div>
            </div>
        )
    }
};
