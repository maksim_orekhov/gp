import React from 'react';
import Weight from './ControlProductWeight';
import CargoCategory from './ControlCargoCategory';


export default class ControlDpdParcelsData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            validation: {
                weight_is_valid: false
            }
        };

        this.handleValidation = this.handleValidation.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(name, value) {
        const { handleComponentChange, name: name_of_upper_component, value_prop } = this.props;

        const parcel = {
            ...value_prop,
            [name]: value
        };

        handleComponentChange && handleComponentChange(name_of_upper_component, parcel);
    };

    setChildrenValidations(name, value) {
        return new Promise((resolve, reject) => {
            if (this.state.validation[name] === value) {
                resolve(name, value);
            } else {
                this.setState({
                    validation: {
                        ...this.state.validation,
                        [name]: value
                    }
                }, resolve(name, value));
            }
        });
    }

    handleValidation(name, value) {
        // Если значение валидации дочернего компонента не изменилось то не обновляем state,
        // но всё равно заставляем компонент сообщить наверх о своей валидации
        // !!! это нужно так как например меняется валидация дочернего delivery_type, он по-прежнему остается валидным,
        // !!! следовательно значение в state обновлять не нужно, но вот валидность самого компонента, в котором находится delivery_type
        // !!! перепроверить нужно, так как delivery_type изменился, и для нового типа доставки другое условие в валидации
        // !!! ТАКОЙ ПОДХОД СТОИТ ИСПОЛЬЗОВАТЬ ЕСЛИ КОМПОНЕНТ ИМЕЕТ СЛОЖНУЮ ЛОГИКУ ВАЛИДАЦИИ
        this.setChildrenValidations(name, value)
            .then(() => {
                this.componentValidate();
            });
    }

    componentValidate() {
        const { handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;
        const { weight_is_valid } = this.state.validation;

        const is_valid = (
            weight_is_valid
        );

        handleComponentValidation && handleComponentValidation(validate_prop, is_valid);
    }

    getLabel() {
        const { max_weight } = this.props;
        const base_label = 'Вес товара, кг';

        if (max_weight) {
            return `${base_label} (макс. вес ${max_weight} кг):`;
        }

        return `${base_label}:`;
    }

    render() {
        const { value_prop, handleChangeIfDataNotBelongsParcel, cargo_category, max_weight } = this.props;
        const { weight } = value_prop;

        return (
            <div className="row nested-row">
                <div className="col-xl-6 col-sm-4">
                    <Weight
                        value_prop={weight}
                        name='weight'
                        label={this.getLabel()}
                        placeholder="Вес"
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleValidation}
                        max_weight={max_weight}
                    />
                </div>
                {/*<div className="col-xl-6 col-sm-4">
                    <CargoCategory
                        label="Содержимое заказа:"
                        name="cargo_category"
                        value_prop={cargo_category}
                        handleComponentChange={handleChangeIfDataNotBelongsParcel}
                        handleComponentValidation={this.handleComponentValid}
                    />
                </div>*/}
            </div>
        );
    }
};