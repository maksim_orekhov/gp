import React from 'react';
import FormBankTransfer from '../forms/FormBankTransfer';
import ControlPaymentMethodSelect from './ControlPaymentMethodSelect';
import MESSAGES from '../constants/messages';

export default class ControlPaymentMethod extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenedFormPaymentMethod: false,
            payment_methods: [],
            civil_law_subject_id: ''
        };

        this.openFormPaymentMethod = this.openFormPaymentMethod.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.getCustomLabelWhenFormIsOpen = this.getCustomLabelWhenFormIsOpen.bind(this);
    }

    componentWillMount() {
        const { civil_law_subject_id } = this.props;
        if (civil_law_subject_id) {
            this.findPaymentMethods(civil_law_subject_id);
        } else {
            this.setState({
                payment_methods: []
            }, () => this.openFormPaymentMethod())
        }
    }

    componentWillReceiveProps(nextProps) {
        const { civil_law_subject_id, update_collection, natural_persons, legal_entities, sole_proprietors } = nextProps;
        this.setState({
            civil_law_subject_id
        });

        if ((civil_law_subject_id !== this.props.civil_law_subject_id) || (update_collection && update_collection !== this.props.update_collection)) {
            if (civil_law_subject_id) {
                this.findPaymentMethods(civil_law_subject_id, natural_persons, legal_entities, sole_proprietors);
            } else {
                this.setState({
                    payment_methods: [],
                    isOpenedFormPaymentMethod: true
                })
            }
        }
    }

    findPaymentMethods(civil_law_subject_id, natural_persons = this.props.natural_persons || [], legal_entities = this.props.legal_entities || [], sole_proprietors = this.props.sole_proprietors || []) {
        const civil_law_subjects = [...natural_persons, ...legal_entities, ...sole_proprietors];

        let payment_methods = [];

        const civil_law_subject = civil_law_subjects.find(civil_law_subject => {
           return civil_law_subject.civil_law_subject_id == civil_law_subject_id;
        });

        if (civil_law_subject) {
            payment_methods = civil_law_subject.payment_methods
        }

        this.setState({
            payment_methods,
            isOpenedFormPaymentMethod: !payment_methods.length
        });
    }

    openFormPaymentMethod() {
        const { handleComponentChange, handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;
        this.setState({ isOpenedFormPaymentMethod: true });

        handleComponentChange && handleComponentChange(name, '');

        handleComponentValidation && handleComponentValidation(validate_prop, false);
    }

    closeFormPaymentMethod() {
        this.setState({ isOpenedFormPaymentMethod: false });
    }

    handleChange(name, value) {
        const { handleComponentChange } = this.props;
        this.closeFormPaymentMethod();
        handleComponentChange && handleComponentChange(name, value);
    }

    getCustomLabelWhenFormIsOpen() {
        const { isOpenedFormPaymentMethod } = this.state;

        if (isOpenedFormPaymentMethod) {
            return 'Новый метод оплаты'
        }
        return '';
    }

    render() {
        const {
            is_nested,
            value_prop,
            label,
            name,
            handleComponentValidation,
            form_validate_name,
            form_data_name,
            sendUpData,
            is_hide_name,
            is_cls_control,
            form_control_server_errors,
            form_server_errors,
            clear_local_storage
        } = this.props;
        const { isOpenedFormPaymentMethod, payment_methods, civil_law_subject_id } = this.state;

        return (
            <div className="payment-method">
                <div className="row nested-row civil-law-subject">
                    <div className="col-xl-6 col-md-6 col-sm-4">
                        {
                            payment_methods.length ?
                                    <ControlPaymentMethodSelect
                                        label={label}
                                        name={name}
                                        value_prop={value_prop}
                                        payment_methods={payment_methods}
                                        handleComponentChange={this.handleChange}
                                        handleComponentValidation={handleComponentValidation}
                                        custom_label={this.getCustomLabelWhenFormIsOpen()}
                                        form_control_server_errors={form_control_server_errors}
                                    />
                                :
                                <label className="payment-method-label" htmlFor="">Укажите способ получения оплаты:</label>
                        }
                    </div>

                    <div className={`col-xl-6 col-md-6 col-sm-4 ${is_cls_control ? '' : 'button_no_label'}`}>
                        {
                            payment_methods.length ?
                                <div className="ButtonsRow">
                                    <button
                                        className="ButtonApply button-add-payment-method"
                                        onClick={this.openFormPaymentMethod}
                                        disabled={isOpenedFormPaymentMethod}
                                        data-ripple-button=""
                                    >
                                        <span className="ripple-text"><span className="text-with-icon">Создать метод оплаты</span></span>
                                    </button>
                                </div>
                                :
                                void(0)
                        }
                    </div>
                </div>
                {
                    isOpenedFormPaymentMethod &&
                    <div className="form-add-payment-method">
                        <FormBankTransfer
                            form_validate_name={form_validate_name}
                            form_data_name={form_data_name}
                            is_nested={is_nested}
                            is_hide_name={is_hide_name}
                            sendUpData={sendUpData}
                            handleComponentValidation={handleComponentValidation}
                            civil_law_subject_id={civil_law_subject_id}
                            form_server_errors={form_server_errors}
                            clear_local_storage={clear_local_storage}
                        />
                    </div>
                }
            </div>
        );
    }
}