import React from 'react';
import ControlBase from './ControlBase';
import ControlSearchHint from './ControlSearchHint';
import ShowError from '../ShowError';
import { customFetch, textOnlyNumbers, bikTextFormat } from '../Helpers';
import URLS from '../constants/urls';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';

export default class ControlBik extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                is_bik_valid: null,
                chars_count: null,
                is_bik_exist: false,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                is_bik_valid: MESSAGES.VALIDATION_ERRORS.invalid_bik,
                chars_count: `${MESSAGES.VALIDATION_ERRORS.chars_count} - ${VALIDATION_RULES.BIK.chars_count}`,
                is_bik_exist: MESSAGES.VALIDATION_ERRORS.is_bik_exist,
                undefined_error: ''
            },
            validation_rules: {
                chars_count: VALIDATION_RULES.BIK.chars_count
            },
            value: '',
            filtered_banks: [],
            isSearchHint: false,
            keyPressed: null,
            isLoading: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleOnBlur = this.handleOnBlur.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleAutoComplete = this.handleAutoComplete.bind(this);
    }

    componentWillMount() {
        const { value_prop } = this.props;
        value_prop && this.setValueFromProp(value_prop);
    }

    componentWillReceiveProps(nextProps) {
        const { value_prop, form_control_server_errors } = nextProps;

        value_prop && this.setValueFromProp(value_prop);

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);
        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                is_bik_valid: !this.validateBik(value),
                chars_count: !this.validateCharsCount(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateBik(value)
            && this.validateCharsCount(value)
        );
    }

    getBankInfo() {
        const { popularBanks = [], handleBik, handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;
        const { value } = this.state;

        if (popularBanks[value]) {
            this.setIsBikExistError(false);
            handleComponentValidation(validate_prop, true);
            handleBik(popularBanks[value]);
        } else {
            this.toggleIsLoading();
            customFetch(URLS.BANK.SEARCH, {
                method: 'POST',
                body: JSON.stringify({
                    bik: value
                })
            })
                .then(data => {
                    if (data.status === 'SUCCESS') {
                        handleBik(data.data);
                        handleComponentValidation(validate_prop, false);

                        this.setIsBikExistError(false);
                        this.toggleIsLoading();

                    } else if (data.status === 'ERROR') {
                        this.setIsBikExistError(true);
                        return Promise.reject(data.message);
                    }
                })
                .catch(error => {
                    console.log(error);
                    this.toggleIsLoading();
                    handleComponentValidation(validate_prop, false);
                });
        }
    }

    handleAutoComplete(value) {
        this.setState({
            value
        }, () => this.handleOnBlur());
    }

    handleKeyPress(e) {
        const keyPressed  = e.keyCode;

        if (keyPressed === 40 || keyPressed === 38 || keyPressed === 13) {
            e.preventDefault();
        }

        this.setState({
            keyPressed
        });
    }

    setIsBikExistError(isExist) {
        this.setState({
            validation_errors: {
                isEmpty: false,
                is_bik_valid: false,
                chars_count: false,
                is_bik_exist: isExist,
                undefined_error: false
            }
        });
    }

    handleBlur() {
        this.setState({
            isSearchHint: false
        }, () => this.checkAllValidations(this.state.value));
    }

    handleChange(e) {
        const value = bikTextFormat(e.target.value, this.state.value);
        const { popularBanks = {} } = this.props;

        const filtered_banks = Object.values(popularBanks)
            .filter(el => el['bank_bik'].includes(value));

        this.setState({
            value,  // Форматирует согласно правилам написания БИК
            isSearchHint: true,
            filtered_banks
        }, () => this.handleOnBlur());
    }

    handleOnBlur() {
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        const { value } = this.state;
        const is_bik_valid = this.checkAllValidations(value);

        handleComponentChange && handleComponentChange(name, value);

        if (is_bik_valid) {
            this.setState({
                isSearchHint: false
            });
            this.getBankInfo();
        } else {
            handleComponentValidation && handleComponentValidation(validate_prop, false);
        }
    }

    searchHintTitleFormat(bank) {
        return bank['bank_name'];
    }

    render() {
        const { value, isSearchHint, keyPressed, isLoading, error_messages, validation_errors, validation_rules, filtered_banks } = this.state;
        const { label, placeholder, name } = this.props;

        return (
            <div className="grid-row">
                <div className={`Input-Wrapper ${this.colourInputField()} ${isSearchHint ? 'hasSearchHint' : ''}`}>
                    <label htmlFor={name}>{label}</label>
                    <div className={isLoading ? 'Preloader' : null}>
                        <input
                            name={name}
                            type="text"
                            placeholder={placeholder}
                            maxLength={validation_rules.chars_count}
                            autoComplete="off"
                            value={value}
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            onKeyDown={this.handleKeyPress}
                        />
                    </div>
                    {
                        isSearchHint &&
                        <ControlSearchHint
                            data={filtered_banks}
                            hintTitleValue={this.searchHintTitleFormat}
                            hintValueKey='bank_bik'
                            keyPressed={keyPressed}
                            handleChose={this.handleAutoComplete}
                        />
                    }
                    <ShowError messages={error_messages} existing_errors={validation_errors} />
                </div>
            </div>
        );
    }
}