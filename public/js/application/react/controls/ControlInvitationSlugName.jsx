import React from 'react';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';
import ShowError from '../ShowError';
import ShowHint from "../ShowHint";

export default class ControlInvitationSlugName extends ControlBase {
    constructor(props) {
        super(props);
        this.requestTimeOut = null;
        this.state = {
            validation_errors: {
                isEmpty: null,
                min_chars: null,
                max_chars: null,
                is_slug_not_unique: false,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.DEAL_NAME.min_chars}`,
                max_chars: `${MESSAGES.VALIDATION_ERRORS.max_chars} - ${VALIDATION_RULES.DEAL_NAME.max_chars}`,
                is_slug_not_unique: MESSAGES.VALIDATION_ERRORS.slug_not_unique,
                undefined_error: ''
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.DEAL_NAME.min_chars,
                max_chars: VALIDATION_RULES.DEAL_NAME.max_chars
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.checkIsSlugNameUnique = this.checkIsSlugNameUnique.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop, is_slug_not_unique, validation_errors } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
        if (is_slug_not_unique !== this.props.is_slug_not_unique) {
            this.setState({
                validation_errors: {
                    ...validation_errors,
                    is_slug_not_unique
                }
            })
        }
    }

    handleChangeEvent(e) {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        clearTimeout(this.requestTimeOut);
        this.requestTimeOut = setTimeout(() => {
            this.checkIsSlugNameUnique(value);
        }, 500);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                min_chars: !this.validateMinCharsLength(value),
                max_chars: !this.validateMaxCharsLength(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateMinCharsLength(value)
            && this.validateMaxCharsLength(value)
        );
    }

    handleFocus() {
        const { was_focused, toggleFocus } = this.props;
        if (!was_focused) {
            toggleFocus && toggleFocus();
        }
    }

    checkIsSlugNameUnique(value) {
        console.log(value);
    }

    render() {
        const { name, label, placeholder, value_prop } = this.props;
        const { error_messages, validation_errors } = this.state;
        const hints = {base_hint: true};
        const hint_messages = {base_hint: MESSAGES.HINTS.slug_name};
        return (
            <div className={`Input-Wrapper col-x1-12 ${this.colourInputField()}`}>
                <label htmlFor={name}>{label}</label>
                <div className="row nested-row">
                    <div className="col-xl-9 col-lt-6">
                        <input
                            name={name}
                            tabIndex="0"
                            type="text"
                            onChange={this.handleChangeEvent}
                            onBlur={this.handleChangeEvent}
                            onFocus={this.handleFocus}
                            placeholder={placeholder}
                            value={value_prop}
                        />
                    </div>
                </div>
                <ShowError messages={error_messages} existing_errors={validation_errors} />
                <ShowHint messages={hint_messages} existing_hints={hints} />
            </div>
        );
    }
};