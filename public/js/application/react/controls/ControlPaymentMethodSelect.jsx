import React from 'react';
import ControlBase from './ControlBase';
import SelectImitation from './ControlSelectImitation';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';
import ShowHint from '../ShowHint';

export default class ControlPaymentMethodSelect extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            },
            hints: {
                base_hint: true,
            },
            hint_messages: {
                base_hint: MESSAGES.HINTS.deal_payment_method
            }
        };

        this.handleChangeEventDataSet = this.handleChangeEventDataSet.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    getOptions() {
        const { payment_methods } = this.props;

        if (Array.isArray(payment_methods) && payment_methods.length) {
            return payment_methods.map(payment_method => {
                const { id, name } = payment_method;
                return (
                    <li
                        key={id}
                        data-value={id}
                        onClick={this.handleChangeEventDataSet}
                    >
                        {name}
                    </li>
                );
            });
        }
    }

    getActiveTab() {
        const { payment_methods, value_prop } = this.props;

        if (Array.isArray(payment_methods) && payment_methods.length) {
            const active_payment_method =  payment_methods.find(payment_method => payment_method.id == value_prop);

            if (active_payment_method) {
                return active_payment_method.name;
            }
        }

        return null;
    }

    render() {
        const { label, custom_label } = this.props;
        const { error_messages, validation_errors, hint_messages, hints } = this.state;

        return (
            <div className={`Input-Wrapper ${this.colourInputField()}`}>
                <label htmlFor="payment_method">{label}</label>
                <SelectImitation
                    id="payment_method"
                    default_option="Выберите метод оплаты:"
                    active_tab={this.getActiveTab()}
                    options={this.getOptions()}
                    custom_label={custom_label}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
                {/*<ShowHint messages={hint_messages} existing_hints={hints} existing_errors={validation_errors} />*/}
            </div>
        );
    }
}