import React from 'react';
import MESSAGES from '../constants/messages';
import ControlBase from './ControlBase';
import ShowError from '../ShowError';
import ShowHint from '../ShowHint';


export default class ControlDpdDeliveryType extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    render() {
        const { label, is_disable, value_prop, name, self_delivery_label, door_to_door_label, unique_id_postfix } = this.props;
        const { error_messages, validation_errors } = this.state;
        return (
            // !!! Тэг form тут не с проста, он разъединяет радиобаттоны с одним именем в одной форме,
            // !!! когда компонент импортится 2 раза, иначе они срабатывают как одно целое, хотя компоненты не зависимые
            <form className="row nested-row">
                <div className="col-xl-12">
                    <div className="Input-Wrapper">
                        {
                            label &&
                            <label className="label-double-indent" htmlFor="delivery_dpd_type_door_to_door">{label}</label>
                        }
                        <div className="InputRadio delivery-type-radio">
                            <div className="delivery-type__item">
                                <input
                                    type="radio"
                                    name={name}
                                    value="door_to_door"
                                    id={`delivery_dpd_type_door_to_door${unique_id_postfix || ''}`}
                                    checked={value_prop === 'door_to_door'}
                                    onChange={this.handleChangeEvent}
                                    onBlur={this.handleChangeEvent}
                                    disabled={is_disable}
                                />
                                <label htmlFor={`delivery_dpd_type_door_to_door${unique_id_postfix || ''}`}>
                                    <span className="radiobutton" />
                                    {door_to_door_label}
                                </label>
                            </div>
                            <div className="delivery-type__item">
                                <input
                                    type="radio"
                                    name={name}
                                    value="self_delivery"
                                    id={`delivery_dpd_type_self_delivery${unique_id_postfix || ''}`}
                                    checked={value_prop === 'self_delivery'}
                                    onChange={this.handleChangeEvent}
                                    onBlur={this.handleChangeEvent}
                                />
                                <label htmlFor={`delivery_dpd_type_self_delivery${unique_id_postfix || ''}`}>
                                    <span className="radiobutton" />
                                    {self_delivery_label}
                                </label>
                            </div>
                        </div>
                        <ShowError messages={error_messages} existing_errors={validation_errors} />
                    </div>
                </div>
            </form>
        )
    }
};