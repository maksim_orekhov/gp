import React from 'react';
import ControlBase from './ControlBase';
import ControlSearchHint from './ControlSearchHint';
import { customFetch } from "../Helpers";
import ShowError from '../ShowError';
import MESSAGES from "../constants/messages";
import VALIDATION_RULES from "../constants/validation_rules";


export default class ControlCivilLawSubjectSearch extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            isSearchHint: false,
            keyPressed: null,
            value: '',
            civil_law_subject: null,
            isLoading: false,
            civil_law_subjects: [],
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            },
        };

        this.handleBlur = this.handleBlur.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleAutoComplete = this.handleAutoComplete.bind(this);
        this.checkAllValidations = this.checkAllValidations.bind(this);

        this.timeout = null;
    }

    handleBlur(e) {
        this.setState({
            isSearchHint: false
        });
        const { value } = e.target;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    handleAutoComplete(civil_law_subject) {
        const value = this.state.civil_law_subjects[civil_law_subject].full_name;

        this.setState({
            value,
            civil_law_subject,
            isSearchHint: false
        });
    }

    handleKeyPress(e) {
        this.setState({
            keyPressed: e.keyCode
        });
    }

    ajaxSearch(value) {
        clearTimeout(this.timeout);
        this.setState({
            civil_law_subjects: ''
        });
        this.timeout = setTimeout(() => {
            this.setState({
                isLoading: true
            });
            setTimeout((() => {
                customFetch('/civil-law-subject/search', {
                        method: 'POST',
                        body: JSON.stringify({
                        search: value
                    })
                })
                .then(data => {
                    // console.log('data', data.data);
                    let search_data = {};
                    if (data.data) {
                        search_data = data.data;
                    }

                    Object.values(search_data).map(item => {
                        item['full_name'] = `${item.id} - ${item.name}`;
                        return item;
                    });

                    this.setState({
                        isLoading: false,
                        civil_law_subjects: search_data
                    });
                })
            }), 500);
        }, 500);
    }

    handleChange(e) {
        const { value } = e.target;

        this.ajaxSearch(value);

        this.setState({
            value,
            isSearchHint: true
        });
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    searchHintTitleFormat(civil_law_subject) {
        return civil_law_subject['full_name'];
    }

    render() {
        const { isSearchHint, keyPressed, value, isLoading, civil_law_subjects, error_messages, validation_errors } = this.state;
        const { label, placeholder, name } = this.props;

        return (
            <div className="grid-row">
                <div className={`Input-Wrapper ${this.colourInputField()} ${isSearchHint ? 'hasSearchHint' : ''}`}>
                    <label htmlFor={name}>{label}</label>
                    <div className={isLoading ? 'Preloader' : ''}>
                        <input
                            name={name}
                            id={name}
                            type="text"
                            placeholder={placeholder}
                            autoComplete="off"
                            value={value}
                            onChange={this.handleChange}
                            onBlur={this.handleBlur}
                            onKeyDown={this.handleKeyPress}
                        />
                    </div>
                    {
                        isSearchHint &&
                        <ControlSearchHint
                            data={Object.values(civil_law_subjects)}
                            hintTitleValue={this.searchHintTitleFormat}
                            hintValueKey='id'
                            keyPressed={keyPressed}
                            handleChose={this.handleAutoComplete}
                        />
                    }
                    <ShowError messages={error_messages} existing_errors={validation_errors} />
                </div>
            </div>
      );
    }
};