import React from 'react';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';
import ShowError from '../ShowError';
import ShowHint from '../ShowHint';
import { textOnlyNumbers } from '../Helpers';

export default class ControlProductWeight extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                max_weight: false,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                max_weight: 'Вес товара превышает лимит, выберите другой пункт приема.',
                undefined_error: ''
            },
            hints: {
                base_hint: true
            },
            hint_messages: {
                base_hint: 'Вес указывается в кг'
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    validateControl(nextProps) {
        const { value_prop, handleComponentValidation, name, validate_prop = `${name}_is_valid`, max_weight } = nextProps;

        if ((value_prop && value_prop !== this.props.value_prop) || (max_weight && max_weight !== this.props.max_weight)) {
            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value_prop, max_weight));
        }
    }

    updateErrors(value, max_weight) {
        if (max_weight) {
            this.setState({
                validation_errors: {
                    ...this.state.validation_errors,
                    isEmpty: !this.validateIsEmpty(value),
                    max_weight: !this.validateIsLesserOrEqual(value, max_weight)
                }
            });
        } else {
            this.setState({
                validation_errors: {
                    ...this.state.validation_errors,
                    isEmpty: !this.validateIsEmpty(value),
                    max_weight: false
                }
            });
        }
    }

    checkAllValidations(value, max_weight = this.props.max_weight) {
        this.updateErrors(value, max_weight);

        return (
            this.validateIsEmpty(value)
            && (!max_weight || this.validateIsLesserOrEqual(value, max_weight))
        );
    }

    handleChangeEvent(e) {
        const value = textOnlyNumbers(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    render() {
        const { name, label, placeholder, value_prop, is_disable } = this.props;
        const { error_messages, validation_errors, hints, hint_messages } = this.state;
        return (
            <div className={`Input-Wrapper ${this.colourInputField()}`}>
                <label htmlFor={name}>{label}</label>
                <input
                    name={name}
                    tabIndex="0"
                    type="text"
                    onChange={this.handleChangeEvent}
                    placeholder={placeholder}
                    value={value_prop}
                    onBlur={this.handleChangeEvent}
                    disabled={is_disable}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
                {/*<ShowHint messages={hint_messages} existing_hints={hints} existing_errors={validation_errors} />*/}
            </div>
        );
    }
};