import React from 'react';
import ControlBase from './ControlBase';
import SelectImitation from './ControlSelectImitation';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';
import ShowHint from '../ShowHint';
import { getNaturalPersonName } from '../Helpers';

export default class ControlCivilLawSubjectSelect extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            },
            hints: {
                base_hint: true,
            },
            hint_messages: {
                base_hint: MESSAGES.HINTS.deal_civil_law_subject
            }
        };

        this.handleChangeEventDataSet = this.handleChangeEventDataSet.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    handleChangeEventDataSet(e) {
        const value = e.target.dataset.value;
        const { name, handleComponentChange, handleCivilLawSubject, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        handleCivilLawSubject && handleCivilLawSubject(value);
    }

    getOptions() {
        const { natural_persons, legal_entities, sole_proprietors } = this.props;
        const natural_persons_options = [];
        const legal_entities_options = [];
        const sole_proprietors_options = [];

        if (Array.isArray(natural_persons) && natural_persons.length) {
            natural_persons_options.push(
                <li key='natural_persons' className='SelectImitation-SubsectionTitle' onClick={e => e.stopPropagation()}>Физические лица</li>
            );

            natural_persons.forEach(natural_person => {
                const { civil_law_subject_id, last_name, first_name, secondary_name } = natural_person;

                natural_persons_options.push(
                    <li
                        className='SelectImitation-SubsectionItem'
                        key={civil_law_subject_id}
                        data-value={civil_law_subject_id}
                        onClick={this.handleChangeEventDataSet}
                    >
                        {getNaturalPersonName(first_name, last_name, secondary_name)}
                    </li>
                );
            })
        }

        if (Array.isArray(legal_entities) && legal_entities.length) {
            legal_entities_options.push(
                <li key='legal_entities' className='SelectImitation-SubsectionTitle' onClick={e => e.stopPropagation()}>Юридические лица</li>
            );

            legal_entities.forEach(legal_entity => {
                const { civil_law_subject_id, name } = legal_entity;

                legal_entities_options.push(
                    <li
                        className='SelectImitation-SubsectionItem'
                        key={civil_law_subject_id}
                        data-value={civil_law_subject_id}
                        onClick={this.handleChangeEventDataSet}
                    >
                        {name}
                    </li>);
            })
        }

        if (Array.isArray(sole_proprietors) && sole_proprietors.length) {
            sole_proprietors_options.push(
                <li key='sole_proprietors' className='SelectImitation-SubsectionTitle' onClick={e => e.stopPropagation()}>ИП</li>
            );

            sole_proprietors.forEach(sole_proprietor => {
                const { civil_law_subject_id, last_name, first_name, secondary_name } = sole_proprietor;

                sole_proprietors_options.push(
                    <li
                        className='SelectImitation-SubsectionItem'
                        key={civil_law_subject_id}
                        data-value={civil_law_subject_id}
                        onClick={this.handleChangeEventDataSet}
                    >
                        {getNaturalPersonName(first_name, last_name, secondary_name)}
                    </li>
                );
            })
        }

        return [...natural_persons_options, ...legal_entities_options, ...sole_proprietors_options];
    }

    getActiveTab() {

        const { natural_persons, legal_entities, sole_proprietors, value_prop } = this.props;

        if (Array.isArray(natural_persons) && natural_persons.length) {

            const active_natural_person = natural_persons.find(natural_person => natural_person.civil_law_subject_id == value_prop);

            if (active_natural_person) {
                const { last_name, first_name, secondary_name } = active_natural_person;
                return getNaturalPersonName(first_name, last_name, secondary_name);
            }
        }

        if (Array.isArray(legal_entities) && legal_entities.length) {
            const active_legal_entity = legal_entities.find(legal_entity => legal_entity.civil_law_subject_id == value_prop);

            if (active_legal_entity) {
                return active_legal_entity.name;
            }
        }

        if (Array.isArray(sole_proprietors) && sole_proprietors.length) {

            const active_sole_proprietor = sole_proprietors.find(sole_proprietor => sole_proprietor.civil_law_subject_id == value_prop);

            if (active_sole_proprietor) {
                const { last_name, first_name, secondary_name } = active_sole_proprietor;
                return getNaturalPersonName(first_name, last_name, secondary_name);
            }
        }
        // Сюда мы дойдем только если совпадений не найдено, тогда мы возвращаем null;
        return null;
    }

    render() {
        const { label, custom_label } = this.props;
        const { error_messages, validation_errors, hint_messages, hints } = this.state;

        return (
            <div className={`Input-Wrapper ${this.colourInputField()}`}>
                <label htmlFor="deal_civil_law_subject">{label}</label>
                <SelectImitation
                    id="deal_civil_law_subject"
                    default_option="Ранее зарегистрированные"
                    active_tab={this.getActiveTab()}
                    options={this.getOptions()}
                    custom_label={custom_label}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
                {/*<ShowHint messages={hint_messages} existing_hints={hints} existing_errors={validation_errors} />*/}
                {/*<p className="Note">Лицо, представляющее вас в сделке.</p>*/}
            </div>
        );
    }
}