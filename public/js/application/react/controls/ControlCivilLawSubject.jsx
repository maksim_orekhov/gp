import React from 'react';
import FormNaturalPerson from '../forms/FormNaturalPerson';
import FormLegalEntity from '../forms/FormLegalEntity';
import FormSoleProprietor from '../forms/FormSoleProprietor';
import ControlCivilLawSubjectSelect from './ControlCivilLawSubjectSelect';
import { customFetch } from '../Helpers';

import URLS from '../constants/urls'

export default class ControlCivilLawSubject extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            natural_persons: props.natural_persons || [],
            legal_entities: props.legal_entities || [],
            sole_proprietors: props.sole_proprietors || [],
            isOpenForm: false,
            current_form: 'natural_person'
        };

        this.openForm = this.openForm.bind(this);
        this.closeForms = this.closeForms.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.changeCurrentForm = this.changeCurrentForm.bind(this);
    }

    componentWillMount() {
        const { natural_persons, legal_entities, sole_proprietors } = this.props;
        this.formReInit(natural_persons, legal_entities, sole_proprietors);
    }

    componentWillReceiveProps(nextProps) {
        const { natural_persons, legal_entities, sole_proprietors, update_collection } = nextProps;

        if (update_collection !== this.props.update_collection) {
            this.formReInit(natural_persons, legal_entities, sole_proprietors);
        }
    }

    formReInit(natural_persons, legal_entities, sole_proprietors) {
        const { openPaymentMethod } = this.props;
        // let current_form = '';
        let isOpenForm = false;

        if (!(natural_persons.length || legal_entities.length || sole_proprietors.length)) { // Если все коллекции пустые открываем форму natural_person
            isOpenForm = true;
        }

        this.setState({
            natural_persons,
            legal_entities,
            sole_proprietors,
            isOpenForm
        }, () => {
            openPaymentMethod && openPaymentMethod(!(natural_persons.length || legal_entities.length || sole_proprietors.length));
        });
    }

    resetFormsIsValid() {
        const { handleComponentValidation, form_validate_name } = this.props;

        handleComponentValidation && handleComponentValidation(form_validate_name, false);
    }

    openForm(e) {
        e.preventDefault();

        const {
            handleComponentChange,
            handleComponentValidation,
            name,
            validate_prop = `${name}_is_valid`,
            openPaymentMethod
        } = this.props;

        handleComponentChange && handleComponentChange(name, '');

        handleComponentValidation(validate_prop, false);

        openPaymentMethod && openPaymentMethod(true);

        this.resetFormsIsValid();

        this.setState({
            isOpenForm: true
        });
    }

    closeForms() {
        this.resetFormsIsValid();

        this.setState({
            isOpenForm: false
        });
    }

    handleChange(name, value) {
        const { handleComponentChange } = this.props;
        this.closeForms();
        handleComponentChange && handleComponentChange(name, value);
    }

    getCustomLabelWhenFormIsOpen() {
        const { current_form, isOpenForm } = this.state;

        if (current_form && isOpenForm) {
            const labels = {
                natural_person: 'Новое физ.лицо',
                legal_entity: 'Новое юр.лицо',
                sole_proprietor: 'Новый ИП'
            };
            return labels[current_form];
        }
        return '';
    }

    changeCurrentForm(e) {
        const value = e.target.value;

        this.setState({
            current_form: value
        });
    }

    render() {
        const {
            is_nested = false,
            label, name,
            value_prop,
            sendUpData,
            handleComponentValidation,
            form_validate_name,
            form_data_name,
            form_control_server_errors,
            form_server_errors,
            clear_local_storage
        } = this.props;

        const { natural_persons, legal_entities, sole_proprietors, current_form, isOpenForm } = this.state;

        const has_civil_law_subjects = natural_persons.length || legal_entities.length || sole_proprietors.length;

        return (
            <div className="row nested-row civil-law-subject">
                <div className="col-xl-6 col-md-6 col-sm-4">
                    {
                        has_civil_law_subjects ?
                            <ControlCivilLawSubjectSelect
                                label={label}
                                name={name}
                                natural_persons={natural_persons}
                                legal_entities={legal_entities}
                                sole_proprietors={sole_proprietors}
                                value_prop={value_prop}
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={handleComponentValidation}
                                custom_label={this.getCustomLabelWhenFormIsOpen()}
                                form_control_server_errors={form_control_server_errors}
                            />
                            :
                            void(0)
                    }
                </div>
                {
                    has_civil_law_subjects ?
                        <div className="col-xl-6 col-md-6 col-sm-4 button-container">
                            <button
                                className='ButtonApply button-add-civil-law-subject'
                                onClick={this.openForm}
                                disabled={isOpenForm}
                                data-ripple-button=""
                            >
                                <span className="ripple-text"><span className="text-with-icon">Создать нового участника</span></span>
                            </button>
                        </div>
                        :
                        void(0)
                }
                {
                    isOpenForm &&
                    <div className="col-xl-12">
                        <div className="row nested-row row-new-civil-law-subject">
                            <div className="col-xl-12 Input-Wrapper">
                                <div className="row nested-row">
                                    <div className="col-xl-12">
                                        {
                                            has_civil_law_subjects ?
                                                <label>Тип нового участника:</label>
                                                :
                                                <label>{label}</label>
                                        }
                                    </div>
                                </div>
                                <div className="row nested-row InputRadio">
                                    <div className="col-xl-6 col-md-4">
                                        <div className="row nested-row">
                                            <div className="col-xl-6">
                                                <input
                                                    type="radio"
                                                    id="type_natural_person"
                                                    checked={current_form === 'natural_person'}
                                                    name="civil_law_subject_radio"
                                                    value="natural_person"
                                                    onChange={this.changeCurrentForm}
                                                />
                                                <label htmlFor="type_natural_person">
                                                    <span className="radiobutton" />
                                                    Физ. лицо
                                                </label>
                                            </div>
                                            <div className="col-xl-6">
                                                <input
                                                    type="radio"
                                                    id="type_legal_entity"
                                                    name="civil_law_subject_radio"
                                                    checked={current_form === 'legal_entity'}
                                                    value="legal_entity"
                                                    onChange={this.changeCurrentForm}
                                                />
                                                <label htmlFor="type_legal_entity">
                                                    <span className="radiobutton" />
                                                    Юр. лицо
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-md-2">
                                        <input
                                            type="radio"
                                            id="type_sole-proprietor"
                                            name="civil_law_subject_radio"
                                            checked={current_form === 'sole_proprietor'}
                                            value="sole_proprietor"
                                            onChange={this.changeCurrentForm}
                                        />
                                        <label htmlFor="type_sole-proprietor">
                                            <span className="radiobutton" />
                                            ИП
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {
                    isOpenForm &&
                    <div className="col-xl-12 col-sm-6 form-add-civil-law-subject">
                        {{
                            natural_person: (
                                <FormNaturalPerson
                                    form_validate_name={form_validate_name}
                                    form_data_name={form_data_name}
                                    is_nested={is_nested}
                                    sendUpData={sendUpData}
                                    handleComponentValidation={handleComponentValidation}
                                    form_server_errors={form_server_errors}
                                    clear_local_storage={clear_local_storage}
                                />
                            ),
                            legal_entity: (
                                <FormLegalEntity
                                    form_validate_name={form_validate_name}
                                    form_data_name={form_data_name}
                                    is_nested={is_nested}
                                    sendUpData={sendUpData}
                                    handleComponentValidation={handleComponentValidation}
                                    form_server_errors={form_server_errors}
                                    clear_local_storage={clear_local_storage}
                                />
                            ),
                            sole_proprietor: (
                                <FormSoleProprietor
                                    form_validate_name={form_validate_name}
                                    form_data_name={form_data_name}
                                    is_nested={is_nested}
                                    sendUpData={sendUpData}
                                    handleComponentValidation={handleComponentValidation}
                                    form_server_errors={form_server_errors}
                                    clear_local_storage={clear_local_storage}
                                />
                            )
                        }[current_form]}
                    </div>
                }
            </div>
        );
    }
}