import React from 'react';
import ControlBase from './ControlBase';
import ShowError from '../ShowError';
import ShowHint from '../ShowHint';
import MESSAGES from '../constants/messages';
import { textOnlyNumbers, addSpacesBetweenThousands } from '../Helpers';

export default class ControlAmount extends ControlBase {

    constructor(props) {
        super(props);
        this.state = {
            key_value: '',
            caret_pos: '',
            is_iOS: false,
            validation_errors: {
                isEmpty: null,
                min_value: null,
                max_value: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                min_value: MESSAGES.VALIDATION_ERRORS.amount_min_value,
                max_value: MESSAGES.VALIDATION_ERRORS.amount_max_value,
                undefined_error: ''
            },
            hints: {
                base_hint: true
            },
            hint_messages: {
                base_hint: MESSAGES.HINTS.amount
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleKeydown = this.handleKeydown.bind(this);
        this.handleDisableCheckboxChange = this.handleDisableCheckboxChange.bind(this);
    };

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentDidMount() {
        this.getMobileOperatingSystem();
    }

    // componentWillReceiveProps. В случае, если мы прокидываем значение value через props (свойство valueProps), то необходимо выставить в state нужные состояния
    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        if(this.props.formHasDataAtLocalStorage && !nextProps.formHasDataAtLocalStorage){
            this.setState({
                validation_errors: {
                    isEmpty: null,
                    max_value: null,
                    undefined_error: false
                },
            })
        }

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                min_value: !this.validateIsGreaterOrEqual(value, this.props.min_amount || 1),
                max_value: !this.validateIsLesserOrEqual(value, this.props.max_amount),
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateIsLesserOrEqual(value, this.props.max_amount)
            && this.validateIsGreaterOrEqual(value, this.props.min_amount || 1)
        );
    }

    handleChangeEvent(e) {
        const value = textOnlyNumbers(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        if (!this.state.is_iOS) {
            this.getCursorPosition(value);
        }
    }

    getCursorPosition(value) {
        let caret_pos = 0;
        let val = this.input.value;
        caret_pos = val.slice(0, this.input.selectionStart).length;

        this.setState({
            caret_pos: caret_pos
        }, () => {this.setCursorPosition(caret_pos, value)});
        return caret_pos;
    }

    setCursorPosition(caret_pos, value) {
        this.input.setSelectionRange(caret_pos, caret_pos);

        if (value.length % 5 && this.state.key_value !== 'Backspace') {
            this.input.setSelectionRange(caret_pos + 1, caret_pos + 1);
        }
    }

    handleKeydown(e) {
        this.setState({
            key_value: e.key
        });
    }

    getMobileOperatingSystem() {
        let userAgent = navigator.userAgent || navigator.vendor || window.opera;
        if (/iPad|iPhone|iPod/i.test(userAgent) || /Apple Computer/.test(userAgent) || /Mac OS/.test(userAgent)) {
            this.setState({
                is_iOS: true
            })
        }
    }


    render() {
        const { name, label, placeholder, value_prop, is_disable, is_disabable, id_checkbox } = this.props;
        const { error_messages, validation_errors, hints, hint_messages } = this.state;
        const input_label = is_disabable ? (
            <span className="control-enable-control">
                <input
                    id={id_checkbox}
                    type="checkbox"
                    tabIndex="0"
                    onChange={this.handleDisableCheckboxChange}
                    checked={!is_disable}
                />
                <label htmlFor={id_checkbox}>{label}</label>
            </span>
        ) : <label htmlFor={name}>{label}</label>;

        return(
            <div className={`Input-Wrapper ${this.colourInputField()}`}>
                { input_label }
                <input
                    name={name}
                    value={addSpacesBetweenThousands(value_prop)}
                    onChange={this.handleChangeEvent}
                    onBlur={this.handleChangeEvent}
                    placeholder={placeholder}
                    disabled={is_disable}
                    ref={input => this.input = input}
                    onKeyDown={this.handleKeydown}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
                {/*<ShowHint messages={hint_messages} existing_hints={hints} existing_errors={validation_errors} />*/}
            </div>
       );
    }
}


