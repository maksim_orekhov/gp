import React from 'react';
import ShowError from '../../../application/react/ShowError.jsx';
import EmailBase from "../../../landing/react/auth/forms/RegisterForm/controls/EmailBase";
import MESSAGES from "../../../application/react/constants/messages";
import {getUrlParameter} from "../Helpers";
import ShowHint from '../ShowHint';

export default class ControlAgentEmail extends EmailBase {
    constructor() {
        super();
        let partner_token = getUrlParameter(window.location.search, 'counteragent_token');
        this.url_login = '/login';
        if (partner_token !== '') {
            this.url_login = this.url_login+'?redirectUrl=/deal/create/partner-form%3Fcounteragent_token='+partner_token;
        }

        this.state = {
            disable: false,
            show_hint: true,
            validation_errors:{
                reg_exp_invalid:      null,
                email_unavailable:    false,
                connection_is_lost:   false,
                email:                false,
                undefined_error:      false,
            },
            onFocus: ' ',
            hints:{
                email_rules: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                reg_exp_invalid: MESSAGES.VALIDATION_ERRORS.email_reg_exp_invalid,
                email_unavailable: <span>Пользователь с таким e-mail уже зарегистрирован в системе. Если это вы, пожалуйста, <a href={`${this.url_login}`}>авторизуйтесь</a>.</span>,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
    };

    componentWillReceiveProps(nextProps, nextState){
        super.componentWillReceiveProps(nextProps, nextState);
        if(this.props.formHasDataAtLocalStorage && !nextProps.formHasDataAtLocalStorage){
            this.setState({
                validation_errors:{
                    reg_exp_invalid:      null,
                    email_unavailable:    false,
                    connection_is_lost:   false,
                    email:                false,
                    undefined_error:      false,
                },
            });
        }
    }

    handleChangeEvent(e) {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        this.checkValidationErrors();
    }

    fetchConnectionSuccess(data) {
        console.log(data);
        const { handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;

        if(data.status === 'SUCCESS'){
            this.setState({
                validation_errors: {
                    ...this.state.validation_errors,
                    email_unavailable: false
                }
            }, () => {
                handleComponentValidation && handleComponentValidation(validate_prop, true);
            });
        }
        else if(data.status === 'ERROR'){
            this.setState({
                validation_errors: {
                    ...this.state.validation_errors,
                    email_unavailable: true
                }
            }, () => {
                handleComponentValidation && handleComponentValidation(validate_prop, false);
            });
        }
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                reg_exp_invalid: !this.validateEmail(value)
            }
        });
    }

    render() {
        const { validation_errors, disable, onFocus, error_messages, show_hint } = this.state;
        const { value_prop, name, label } = this.props;
        const hints = {base_hint: true};
        const hint_messages = {base_hint: MESSAGES.HINTS.agent_email};

        return (
            <div className={`form-field ${onFocus}`}>
                <label  htmlFor="login-email">{label || 'Email'}</label>
                <div className="form-input">
                    <input
                        type="email"
                        className={this.colourInputField()}
                        id="login-email"
                        value={value_prop}
                        onBlur={this.handleBlur}
                        onChange={this.handleChangeEvent}
                        disabled={disable}
                        name={name}
                    />
                    <ShowError
                        messages={error_messages}
                        existing_errors={validation_errors}
                    />
                    {
                        show_hint &&
                        <ShowHint messages={hint_messages} existing_hints={hints} />
                    }
                </div>
            </div>
        );
    }
}
