import React from 'react';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';
import ShowError from '../ShowError';

export default class ControlLoginEdit extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                login_reg_exp: false,
                min_chars: false,
                max_chars: false,
                max_words: false,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                login_reg_exp: MESSAGES.VALIDATION_ERRORS.login_reg_exp_invalid,
                min_chars: MESSAGES.VALIDATION_ERRORS.min_chars + ' - 3',
                max_chars: MESSAGES.VALIDATION_ERRORS.max_chars + ' - 16',
                max_words: MESSAGES.VALIDATION_ERRORS.max_words + ' - 1',
                undefined_error: ''
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.LOGIN.min_chars,
                max_chars: VALIDATION_RULES.LOGIN.max_chars,
                max_words: VALIDATION_RULES.LOGIN.max_words
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.validateLoginRegExp = this.validateLoginRegExp.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    validateLoginRegExp(value) {
        // Проверяем по регулярке, если только длинна значения больше 3
        // чтобы сначала поле валидировалось на мин. длинну
        if(value.length <= 2) return true;
        const reg_exp = /^[a-zA-Z][a-zA-Z0-9-_\.]{2,}$/;
        return reg_exp.test(value);
    }

    handleChangeEvent(e) {
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        const value = e.target.value;
        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                login_reg_exp: !this.validateLoginRegExp(value),
                min_chars: !this.validateMinCharsLength(value),
                max_chars: !this.validateMaxCharsLength(value),
                max_words: !this.validateMaxWordsLength(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateMinCharsLength(value)
            && this.validateMaxCharsLength(value)
            && this.validateMaxWordsLength(value)
            && this.validateLoginRegExp(value)
        );
    }

    render() {
        const { name, label, placeholder, value_prop } = this.props;
        const { validation_errors, error_messages } = this.state;

        return (
            <div className={`Input-Wrapper ${this.colourInputField()}`}>
                <label htmlFor={name}>{label}</label>
                <input
                    name={name}
                    tabIndex="0"
                    type="text"
                    onChange={this.handleChangeEvent}
                    onBlur={this.handleChangeEvent}
                    placeholder={placeholder}
                    value={value_prop}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        );
    }
}