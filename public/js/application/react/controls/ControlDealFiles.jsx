import React from 'react';
import ControlBase from "./ControlBase";
import FileWithDownloadLink from "./ControlDealFileWithDownloadLink"
import ShowError from '../ShowError';
import MESSAGES from "../constants/messages";

export default class ControlDealFiles extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            files: {
                'file_contract': null,
                'file_request': null
            },
            file_contract_is_valid: false,
            file_request_is_valid: false,
            validation_errors: {
                undefined_error: false
            },
            error_messages: {
                undefined_error: ''
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValidation = this.handleComponentValidation.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);
    }


    handleChange(file_name, files) {
        const { handleComponentChange, name } = this.props;

        this.setState({
            files: {
                ...this.state.files,
                [file_name]: files[0] || null
            }
        }, () => {
            handleComponentChange && handleComponentChange(name, Object.values(this.state.files));
        });
    }

    handleComponentValidation(file_name, value) {
        const { handleComponentValidation, name, validate_prop = `${name}_is_valid`} = this.props;

        this.setState({
            [file_name]: value
        }, () => { handleComponentValidation && handleComponentValidation(validate_prop, this.state.file_contract_is_valid && this.state.file_request_is_valid) });
    }

    render() {
        const { error_messages, validation_errors } = this.state;
        const { name_first, label_first, placeholder_first, download_link_text_first,
                name_second, label_second, placeholder_second, download_link_text_second} = this.props;
        return (
            <div className="deal-files">
                <FileWithDownloadLink
                    name={name_first}
                    validate_prop="file_contract_is_valid"
                    label={label_first}
                    placeholder={placeholder_first}
                    download_link_text={download_link_text_first}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValidation}
                    href="/common-file/download/shablon_tipovogo_dogovora.docx?out_file_name=типовой_договор.docx"
                />
                <FileWithDownloadLink
                    name={name_second}
                    validate_prop="file_request_is_valid"
                    placeholder={placeholder_second}
                    download_link_text={download_link_text_second}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleComponentValidation}
                    href="/common-file/download/blank_zayavki.xls?out_file_name=бланк_заявки.xls"
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        )
    }
}