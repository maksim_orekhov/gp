import React from 'react';
import MESSAGES from '../constants/messages';
import ControlBase from './ControlBase';
import SelectImitation from './ControlSelectImitation';
import ShowError from '../ShowError';

export default class ControlDeliveryOrderDeliveryInterval extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEventDataSet = this.handleChangeEventDataSet.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    getOptions() {
        const { name, delivery_interval_options = [] } = this.props;

        return delivery_interval_options.map((delivery_interval_item, id) => {
            return (
                <li
                    key={id}
                    name={name}
                    data-value={id}
                    onClick={this.handleChangeEventDataSet}
                >
                    {delivery_interval_item.name}
                </li>
            );
        }).reverse();
    }

    getActiveTab() {
        const { value_prop, delivery_interval_options } = this.props;
        if (delivery_interval_options[value_prop]) {
            return delivery_interval_options[value_prop].name
        }
        return ''
    }

    render() {
        const { label, is_disable } = this.props;
        const { validation_errors, error_messages } = this.state;

        return(
            <div className={`Input-Wrapper ${this.colourInputField()}`}>
                <label htmlFor="delivery_interval">{label}</label>
                <SelectImitation
                    id="delivery_interval_select"
                    default_option="С 9:00 до 18:00"
                    active_tab={this.getActiveTab()}
                    options={this.getOptions()}
                    is_disable={is_disable}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        )
    }
}