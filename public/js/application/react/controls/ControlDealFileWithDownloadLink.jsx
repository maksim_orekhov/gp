import React from 'react';
import ControlDealFile from './ControlDealFile';
import ShowError from '../ShowError';
import MESSAGES from "../constants/messages";

export default class ControlDealFileWithDownloadLink extends ControlDealFile{
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
        );
    }

    render() {
        const { error_messages, validation_errors } = this.state;
        const { name, label, placeholder, download_link_text, href = '#' } = this.props;
        return (
            <div className="row">
                <div className="col-xl-12">
                    <div className="Input-Wrapper">
                        <label htmlFor="control-touch-file">{label}</label>
                        <div className={`Input-Wrapper ${this.colourInputField()}`}>
                            <div className="InputFile InputFile__template">
                                <div className="row nested-row">
                                    <div className="col-xl-6 col-md-6">
                                        <span className="InputFile-Container">
                                                <mark
                                                    data-text-default={placeholder}
                                                    className="notChange"
                                                    ref={label => this.label = label}
                                                >
                                                    {
                                                        placeholder
                                                    }
                                                </mark>
                                                <input
                                                    name={name}
                                                    id="control-touch-file"
                                                    type="file"
                                                    onChange={this.handleChangeEvent}
                                                    ref={input => this.input = input}
                                                />
                                                <span
                                                    className="InputFile-Delete"
                                                    ref={clear => this.clear = clear}
                                                    onClick={this.clearInput}
                                                />
                                        </span>
                                    </div>
                                    <div className="col-xl-6 col-md-6">
                                        <div className="LinkWithIcon">
                                            <div className="IconDownload LinkWithIcon-Icon"/>
                                            <a href={href}
                                               className="LinkWithIcon-Text AccentLink">{download_link_text}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ShowError messages={error_messages} existing_errors={validation_errors} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}