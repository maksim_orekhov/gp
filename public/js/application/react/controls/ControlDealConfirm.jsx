import React from 'react';
import ControlBase from './ControlBase';

export default class ControlDealConfirm extends ControlBase {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const { checked } = e.target;
        const { name, handleComponentValidation, handleComponentChange, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentValidation && handleComponentValidation(validate_prop, checked);
        handleComponentChange && handleComponentChange(name, checked);
    }

    render() {
        const { label_text } = this.props;

        return (
            <div className='Input-Wrapper'>
                <div className="flex">
                    <input
                        id="check-agree-rules"
                        type="checkbox"
                        tabIndex="0"
                        onChange={this.handleChange}
                    />
                    <label htmlFor="check-agree-rules">
                        {label_text}
                    </label>
                </div>
            </div>
        );
    }
};