import React from 'react';
import MESSAGES from '../constants/messages';
import ControlBase from './ControlBase';
import SelectImitation from './ControlSelectImitation';
import ShowError from '../ShowError';
import PropTypes from 'prop-types';


export default class ControlCargoCategory extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            cargo_categories: {
                1: {
                    id: 1,
                    name: 'Товары народного потребления (без ГСМ и АКБ)'
                },
                2: {
                    id: 2,
                    name: 'Автозапчасти'
                },
                3: {
                    id: 3,
                    name: 'Мобильные телефоны, устройства с аккумуляторами'
                },
                4: {
                    id: 4,
                    name: 'Опасный груз, жидкости'
                },
                5: {
                    id: 5,
                    name: 'Документы и печатная продукция'
                },
                6: {
                    id: 6,
                    name: 'Алкоголь'
                },
                7: {
                    id: 7,
                    name: 'Косметика и парфюмерия'
                }
            },
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEventDataSet = this.handleChangeEventDataSet.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    getOptions() {
        const { cargo_categories } = this.state;
        const { name_of_component }  = this.props;

        return Object.values(cargo_categories).map(cargo_category_type_item => {
            return (
                <li
                    key={cargo_category_type_item.name}
                    name={name_of_component}
                    data-value={cargo_category_type_item.name}
                    onClick={this.handleChangeEventDataSet}
                >
                    {cargo_category_type_item.name}
                </li>
            );
        }).reverse();
    }

    getActiveTab() {
        const { value_prop } = this.props;

        if (value_prop) {
            return value_prop;
        }
    }

    render() {
        const { label, is_disable } = this.props;
        const { validation_errors, error_messages } = this.state;

        return (
            <div className={`Input-Wrapper ${this.colourInputField()}`}>
                <label htmlFor="cargo_category">{label}</label>
                <SelectImitation
                    id="cargo_category_select"
                    default_option="Выберите тип содержимого"
                    active_tab={this.getActiveTab()}
                    options={this.getOptions()}
                    is_disable={is_disable}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        )
    }
}