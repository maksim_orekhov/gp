import React from 'react';
import PropTypes from 'prop-types';
import MESSAGES from '../constants/messages';
import ControlBase from './ControlBase';
import ShowError from '../ShowError';
import DatePicker from '../components/DatePicker';


export default class ControlDatePicker extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                isDateValid: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            },
            date_input_value: '',
            selectedDate: null,
            datePickerIsVisible: false,
        };

        this.date_picker = null;
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    validateMinDate(value) {
        return (new Date(value) >= new Date());
    }

    handleChange = (date) => {
        const { name, handleComponentChange, handleComponentValidation } = this.props;
        let value  = '';
        if (date !== null) {
            const dayNumber = ('0' + date.getDate()).slice(-2);
            const monthNumber = ('0' + (date.getMonth() + 1)).slice(-2);
            value = `${date.getFullYear()}-${monthNumber}-${dayNumber}`;
        }

        this.setState({
            date_input_value: value,
            selectedDate: date,
            datePickerIsVisible: date === null,
        }, () => {
            this.checkAllValidations(value);
            handleComponentChange && handleComponentChange(name, value);
            handleComponentValidation && handleComponentValidation(name, value);
        });
    };

    setControlRef = (node) => {
        this.controlRef = node;
    };

    openDatePicker = () => {
        const { datePickerIsVisible } = this.state;
        if (datePickerIsVisible) {
            return false;
        }
        this.setState({ datePickerIsVisible: true });
    };

    handleClickOutside = (ev) => {
        const { datePickerIsVisible } = this.state;
        if (datePickerIsVisible && !this.controlRef.contains(ev.target)) {
            this.setState({ datePickerIsVisible: false });
        }
    };

    render() {
        const { label, name, disabledDays } = this.props;
        const { validation_errors, error_messages, date_input_value, datePickerIsVisible, selectedDate } = this.state;
        return(
            <div className={`Input-Wrapper ${this.colourInputField()} date-picker`} ref={this.setControlRef}>
                <label htmlFor={name}>{label}</label>
                <label className="date_picker_before" />
                <input
                    type="text"
                    name={name}
                    className="date_picker"
                    value={date_input_value}
                    onClick={this.openDatePicker}
                    onFocus={this.openDatePicker}
                />
                {datePickerIsVisible &&
                    <DatePicker
                        minDate={new Date()}
                        onSelectDate={this.handleChange}
                        selectedDate={selectedDate}
                        disabledDays={disabledDays || []}
                    />
                }
                <label className="date_picker_after" />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        )
    }
}

ControlDatePicker.PropTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    handleComponentChange: PropTypes.func,
    handleComponentValidation: PropTypes.func
};