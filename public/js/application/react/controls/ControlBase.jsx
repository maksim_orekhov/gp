import React from 'react';
import URLS from '../constants/urls';
import REQUEST_CODES from '../constants/request_codes';
import { checkControlErrors, customFetch, wordsCountInString, replaceRusLetterE } from '../Helpers';
import { validateRusCharacters, validateIsGreaterOrEqual, validateIsLesserOrEqual, validateIsGreaterOrEqualOrEmpty, validateInn, validateBik, validateEmail, validatePhone, validatePassword, validateUrl } from '../Validations';
import $ from "jquery";

export default class ControlBase extends React.Component {

    /**
     * Сетит в state value получаемое от родителя через props
     * @param value
     */
    setValueFromProp(value) {
        const { handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;
        if (value && this.state.value !== value) {
            this.setState({
                value
            }, () => {
                handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations());
            });
        }
    }


    /**
     * Сравнивает изменился ли props ошибки, если изменился то устанавливает его как ошибку контрола
     * если старый props равен новому и server_errors не null сбрасывает ошибку в контроле
     * @param {Object} errorsNext - новый объект ошибок
     * @param {string } errorsCurrent - старый объект ошибок
     */
    setControlErrorsFromServer(errorsNext, errorsCurrent) {
        if (JSON.stringify(errorsNext) !== JSON.stringify(errorsCurrent)) {
            const validation_errors = {...this.state.validation_errors};
            const error_messages = {...this.state.error_messages};

            Object.keys(errorsNext).forEach(error_name => {
                if (validation_errors.hasOwnProperty(error_name)) {
                    customFetch(URLS.LOG_ERRORS, {
                        method: 'POST',
                        body: JSON.stringify({
                            code: REQUEST_CODES.INVALID_FORM_DATA,
                            message: `Контрол: ${this.props.name || 'Неопределен'} </br>
                                Флаг валидации: ${error_name} </br>
                                Текст с бэка: ${errorsNext[error_name]} </br>
                                Текст с фронта: ${error_messages[error_name]}.`
                        })
                    });
                    error_messages[error_name] = errorsNext[error_name];
                    validation_errors[error_name] = true;
                    
                } else {
                    error_messages['undefined_error'] += ` ${errorsNext[error_name]}`;
                    validation_errors['undefined_error'] = true;
                }
            });

            this.setState({
                error_messages,
                validation_errors
            });
        } else {
            // Так как ошибки с бэка не валидируются то чтобы снова сделать инпут валидным при вводе проверяем что флаг undefined_error активен и сбрасываем его
            if (this.state.validation_errors.undefined_error) {
                this.clearUndefinedErrorFromServer();
            }
        }
    }

    clearUndefinedErrorFromServer() {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                undefined_error: false
            },
            error_messages: {
                ...this.state.error_messages,
                undefined_error: ''
            }
        });
    }

    /**
     * Устанавливает флаги ошибок
     * @param {string} name - ключ флага
     * @param {boolean} value - значение флага true or false
     * !!!!! НУЖНО ВНИМАТЕЛЬНО ИСПОЛЬЗОВАТЬ ЭТОТ МЕТОД Т.К. ЕСЛИ ВЫЗЫВАТЬ ЕГО С НЕБОЛЬШИМ ПРОМЕЖУТКОМ ОШИБКИ ПЕРЕТИРАЮТ ДРУГ ДРУГА И СЕТЯТСЯ НЕПРАВИЛЬНО
     */
    setError(name, value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                [name]: value
            }
        });
    }

    /**
     * Проверяет флаги ошибок контрола, и вешает нужный класс стилей в зависимости от этого
     * returns {string}
     */
    colourInputField() {
        const { validation_errors } = this.state;
        if (checkControlErrors(validation_errors) === false ) {
            return 'success';
        } else if (checkControlErrors(validation_errors) === null) {
            return '';
        }
        return 'error';
    }

    /**
     * Валидация проверяющая что поле обязательное и не может быть пустым
     * @return {boolean}
     */
    validateIsEmpty(value = this.state.value) {
        return !!value;
    }

    // Валидация, которая проверяет минимальное кол-во символов в поле ввода
    validateMinCharsLength(value = this.state.value) {
        return validateIsGreaterOrEqual(value.length, this.state.validation_rules.min_chars);
    }

    // Валидация, которая проверяет минимальное кол-во символов в поле ввода или поле пустое
    validateMinCharsLengthOrEmpty(value = this.state.value) {
        return validateIsGreaterOrEqualOrEmpty(value.length, this.state.validation_rules.min_chars);
    }

    // Валидация, которая проверяет максимальное кол-во символов в поле ввода
    validateMaxCharsLength(value = this.state.value) {
        return validateIsLesserOrEqual(value.length, this.state.validation_rules.max_chars);
    }

    // Валидация, которая проверяет кол-во слов в поле ввода
    validateMinWordsLength(value = this.state.value) {
        const words_count = wordsCountInString(value);
        return validateIsGreaterOrEqual(words_count, this.state.validation_rules.min_words);
    }

    // Валидация, которая проверяет кол-во слов в поле ввода
    validateMaxWordsLength(value = this.state.value) {
        const words_count = wordsCountInString(value);
        return validateIsLesserOrEqual(words_count, this.state.validation_rules.max_words);
    }

    // Валидация, котоаря проверяет что значение меньше данного
    validateIsLesserOrEqual(value = this.state.value, max_value) {
        if (max_value) { // если параметра max_value не будет то валидация возвращает true
            return validateIsLesserOrEqual(+value, +max_value);
        }
        return true;
    }

    validateIsGreaterOrEqual(value = this.state.value, min_value) {
        if (min_value) {
            return validateIsGreaterOrEqual(+value, +min_value);
        }
        return true;
    }

    // Валидация для полей имеющих строгое количество символов
    validateCharsCount(value = this.state.value) {
        return value.length === this.state.validation_rules.chars_count;
    }

    validateRusCharacters(value = this.state.value) {
        return validateRusCharacters(value);
    }

    validateInn(value = this.state.value) {
        return validateInn(value);
    }

    validateBik(value = this.state.value) {
        return validateBik(value);
    }

    validateDeliveryPeriod(value = this.state.value) {
        return validateIsGreaterOrEqual(value, this.state.validation_rules.min_days);
    }

    validateEmail(value = this.state.value) {
        return validateEmail(value);
    }

    validatePassword(value = this.state.value) {
        return validatePassword(value);
    }

    validatePhone(value = this.state.value, register) {
        return validatePhone(value, register);
    }

    validateUrl(value = this.state.value) {
        return validateUrl(value);
    }

    /**
     * @param e - event события, этот метод используется если необходимо сохранить в state текущее value контрола
     */
    handleChange(e) {
        const { value } = e.target;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));

        this.setState({
            value
        });
    }

    validateControl(nextProps) {
        const { value_prop, handleComponentValidation, name, validate_prop = `${name}_is_valid` } = nextProps;

        if (value_prop && value_prop !== this.props.value_prop) {
            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value_prop));
        }
    }

    handleOnBlur() {
        const { name, handleBlur, handleComponentValidation, validate_prop } = this.props;
        const { value } = this.state;

        handleBlur && handleBlur(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations());
    }

    /**
     * @param e - event события, этот метод используется если не нужно сохранять в state текущее вэлью
     * а сразу передать его компоненту наверх
     */
    handleChangeEvent(e) {
        const value = replaceRusLetterE(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    /**
     * Метод для обработки значений из data-value аттрибута у элементов которые не могут иметь value
     */
    handleChangeEventDataSet(e) {
        const value = e.target.dataset.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    // Если контрол делает ajax запрос этот метод позволяет включать прелоадер
    toggleIsLoading() {
        this.setState({
            isLoading: !this.state.isLoading
        });
    }

    // Валидация e-mail
    testRegExp(value, regExpPattern){
        let regExp = new RegExp(regExpPattern);
        if ( regExp.test(value) ){
            return true;
        }
        return false;
    };

    validationRegExp(value, regExpPattern){
        const validation_errors = {...this.state.validation_errors};

        if (this.testRegExp(value, regExpPattern)) {

            validation_errors.reg_exp_invalid = false;

            this.setState({validation_errors});
            return true;
        }
        // убираем error если поле пустое
        // else if (value === '') {
        //     validation_errors.reg_exp_invalid = false;
        //
        //     this.setState({validation_errors});
        //     return false;
        // }
        else {
            validation_errors.reg_exp_invalid = true;
            this.setState({validation_errors});
            return false;
        }
    }

    /**
     *
     * @param value
     * @param options array
     * options: {
     *  url: "url",
     *  field: "field_name"
     * }
     * @returns {*}
     */
    fetchValidation(value, options){
        return customFetch(options.url, {
            method: 'POST',
            body: JSON.stringify(
                {
                    [options.field] : value
                }
            )
        });
    };

    // Проверка валидности полей при открытии заполненной формы
    checkValidateOnMount() {
        const { value_prop, handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;

        if (value_prop) {
            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value_prop));
        }
    }

    showHint(){
        if(!checkControlErrors(this.state.validation_errors)){
            this.setHint();
        }
    }

    passwordConfirmation(passwordIsConfirmed){
        const validation_errors = {...this.state.validation_errors};
        if(passwordIsConfirmed){
            validation_errors.not_confirmed = false;
            validation_errors.reg_exp_invalid = false;
        } else {
            validation_errors.not_confirmed = true;
        }
        this.setState({validation_errors});
    }

    handleDisableCheckboxChange(e) {
        const { checked } = e.target;
        const { setControlDisabled, name } = this.props;

        setControlDisabled && setControlDisabled(name, checked);
    }

}