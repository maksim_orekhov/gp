import React from 'react';
import MESSAGES from '../constants/messages';
import ControlBase from './ControlBase';
import SelectImitation from './ControlSelectImitation';
import ShowError from '../ShowError';

export default class ControlDealType extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEventDataSet = this.handleChangeEventDataSet.bind(this);
        this.handleDisableCheckboxChange = this.handleDisableCheckboxChange.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    getOptions() {
        const { name, deal_types = {} } = this.props;

        return Object.values(deal_types).map(deal_type_item => {
            return (
                <li
                    key={deal_type_item.id}
                    name={name}
                    data-value={deal_type_item.id}
                    onClick={this.handleChangeEventDataSet}
                >
                    {deal_type_item.name}
                </li>
            );
        }).reverse();
    }

    getActiveTab() {
        const { value_prop, deal_types } = this.props;
        if (deal_types[value_prop]) {
            return deal_types[value_prop].name
        }
        return ''
    }

    render() {
        const { label, is_disable, is_disabable, id_checkbox } = this.props;
        const { validation_errors, error_messages } = this.state;
        const input_label = is_disabable ? (
            <span className="control-enable-control">
                <input
                    id={id_checkbox}
                    type="checkbox"
                    tabIndex="0"
                    onChange={this.handleDisableCheckboxChange}
                    checked={!is_disable}
                />
                <label htmlFor={id_checkbox}>{label}</label>
            </span>
        ) : <label htmlFor="deal_type">{label}</label>;

        return(
            <div className="row">
                <div className="col-xl-6 col-sm-4">
                    <div className={is_disable ? `Input-Wrapper` : `Input-Wrapper ${this.colourInputField()}`}>
                        { input_label }
                        <SelectImitation
                            id="deal_type_select"
                            default_option="Выберите тип сделки"
                            active_tab={this.getActiveTab()}
                            options={this.getOptions()}
                            is_disable={is_disable}
                        />
                        <ShowError messages={error_messages} existing_errors={validation_errors} />
                    </div>
                </div>
            </div>
        )
    }
}