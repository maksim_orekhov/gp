import React from 'react';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import { DELIVERY_TYPES_NAMES } from '../constants/names';
import ShowError from '../ShowError';


export default class ControlServiceType extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    getLabels(service_name, deal_role) {
        const { NO_SERVICE, DPD, NOT_REQUIRED } = DELIVERY_TYPES_NAMES;
        const labels = {
            customer: {
                [NO_SERVICE]: 'Собственная доставка',
                [DPD]: 'DPD курьерская',
                [NOT_REQUIRED]: 'Не требуется'
            },
            contractor: {
                [NO_SERVICE]: 'Собственная доставка',
                [DPD]: 'DPD доставка',
                [NOT_REQUIRED]: 'Не требуется'
            }
        };

        return labels[deal_role][service_name];
    }

    renderOption(service_type) {
        const { is_disable, value_prop, name, deal_role } = this.props;
        const { id, name: service_name } = service_type;

        return (
            <div className="col-xl-4 col-sm-4" key={`delivery-option-${id}`}>
                <input
                    type="radio"
                    name={name}
                    id={`delivery-option-${id}`}
                    data-placeholder="Собственная доставка"
                    value={id}
                    checked={value_prop == id}
                    onChange={this.handleChangeEvent}
                    onBlur={this.handleChangeEvent}
                    disabled={is_disable}
                />
                <label
                    htmlFor={`delivery-option-${id}`}
                >
                    <span className="radiobutton">
                    </span>
                    {this.getLabels(service_name, deal_role)}
                </label>
            </div>
        );
    }

    render() {
        const { label, service_types } = this.props;
        const { error_messages, validation_errors } = this.state;
        const service_type_order = {
            NO_SERVICE: 1,
            DPD: 2,
            NOT_REQUIRED: 3
        };

        return (
            <div className="row nested-row">
                <div className="col-xl-12">
                    <div className="Input-Wrapper">
                        <label htmlFor="delivery_type" className="label-double-indent">{label}</label>
                        <div className="InputRadio">
                            <div className="row nested-row">
                                {
                                    Object.values(service_types)
                                        .sort((service_type_a, service_type_b) => +service_type_order[service_type_a.name] - +service_type_order[service_type_b.name])
                                        .map(service_type => this.renderOption(service_type)).sort()
                                }
                            </div>
                        </div>
                        <ShowError messages={error_messages} existing_errors={validation_errors} />
                    </div>
                </div>
            </div>
        )
    }
};