import React from 'react';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';
import ShowError from '../ShowError';
import ShowHint from '../ShowHint';
import { textOnlyNumbers } from '../Helpers';

export default class ControlDeliveryPeriod extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                delivery_period_is_valid: false,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                delivery_period_is_valid: MESSAGES.VALIDATION_ERRORS.delivery_period_is_valid,
                undefined_error: ''
            },
            validation_rules: {
                min_days: VALIDATION_RULES.DELIVERY_PERIOD.min_days
            },
            hints: {
                base_hint: true
            },
            hint_messages: {
                base_hint: MESSAGES.HINTS.delivery_period
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleDisableCheckboxChange = this.handleDisableCheckboxChange.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                delivery_period_is_valid: !this.validateDeliveryPeriod(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateDeliveryPeriod(value)
        );
    }

    handleChangeEvent(e) {
        const value = textOnlyNumbers(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    render() {
        const { name, label, placeholder, value_prop, is_disable, is_disabable, id_checkbox } = this.props;
        const { error_messages, validation_errors, hints, hint_messages } = this.state;
        const input_label = is_disabable ? (
            <span className="control-enable-control">
                <input
                    id={id_checkbox}
                    type="checkbox"
                    tabIndex="0"
                    onChange={this.handleDisableCheckboxChange}
                    checked={!is_disable}
                />
                <label htmlFor={id_checkbox}>{label}</label>
            </span>
        ) : <label htmlFor={name}>{label}</label>;
        return (
            <div className={is_disable ? `Input-Wrapper` : `Input-Wrapper ${this.colourInputField()}`}>
                { input_label }
                <input
                    name={name}
                    tabIndex="0"
                    type="text"
                    onChange={this.handleChangeEvent}
                    placeholder={placeholder}
                    value={value_prop}
                    onBlur={this.handleChangeEvent}
                    maxLength={4}
                    disabled={is_disable}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
                <ShowHint messages={hint_messages} existing_hints={hints} existing_errors={validation_errors} />
            </div>
        );
    }
};