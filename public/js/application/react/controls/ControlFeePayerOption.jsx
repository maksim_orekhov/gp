import React from 'react';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';


export default class ControlFeePayerOption extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleDisableCheckboxChange = this.handleDisableCheckboxChange.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    getRulesLabel() {
        return {
            U: {
                'CUSTOMER': 'Заказчик',
                'CONTRACTOR': 'Исполнитель',
                '50/50': '50/50'
            },
            T: {
                'CUSTOMER': 'Покупатель',
                'CONTRACTOR': 'Продавец',
                '50/50': '50/50'
            }
        };
    }

    getLabel(deal_type, fee_payer_options_key) {
        const labels = this.getRulesLabel();
        return labels[deal_type][fee_payer_options_key.name]
    }

    renderPayerOption(fee_payer_options_key) {
        const { value_prop, deal_type, name, is_disable } = this.props;

        return (
            <div className="col-xl-3 col-sm-2" key={`fee-payer-option-${fee_payer_options_key.id}`}>
                <input
                    type="radio"
                    name={name}
                    id={`fee-payer-option-${fee_payer_options_key.id}`}
                    data-placeholder="Кто оплачивает комиссию"
                    value={fee_payer_options_key.id}
                    checked={value_prop == fee_payer_options_key.id}
                    onChange={this.handleChangeEvent}
                    onBlur={this.handleChangeEvent}
                    disabled={is_disable}
                />
                <label
                    htmlFor={`fee-payer-option-${fee_payer_options_key.id}`}>
                 <span className="radiobutton">
                 </span>
                    {this.getLabel(deal_type, fee_payer_options_key)}
                </label>
            </div>
        );
    }

    render() {
        const { fee_payer_options, label, is_disabable, id_checkbox, is_disable } = this.props;
        const { error_messages, validation_errors } = this.state;
        const input_label = is_disabable ? (
            <span className="control-enable-control">
                <input
                    id={id_checkbox}
                    type="checkbox"
                    tabIndex="0"
                    onChange={this.handleDisableCheckboxChange}
                    checked={!is_disable}
                />
                <label htmlFor={id_checkbox}>{label}</label>
            </span>
        ) : <label htmlFor="fee_payer_option">{label}</label>;
        return (
            <div className="row row-fee-payer">
                <div className="col-xl-12">
                    <div className="Input-Wrapper">
                        { input_label }
                        <div className="InputRadio">
                            <div className="row nested-row">
                                {
                                    fee_payer_options.map(fee_payer_option => this.renderPayerOption(fee_payer_option))
                                }
                            </div>
                        </div>
                        <ShowError messages={error_messages} existing_errors={validation_errors} />
                    </div>
                </div>
            </div>
        )
    }
};