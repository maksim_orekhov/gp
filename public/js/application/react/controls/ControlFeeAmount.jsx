import React from 'react';
import propTypes from 'prop-types';

const ControlFeeAmount = (props) => {
    const { name, label, value_prop } = props;

    return (
        <div className="col-xl-3 col-sm-2">
            <div className="Input-Wrapper">
                <label htmlFor={name}>{label}</label>
                <input
                    name={name}
                    tabIndex="0"
                    type="text"
                    value={value_prop}
                    disabled={true}
                />
            </div>
        </div>
    );
};

ControlFeeAmount.propTypes = {
    name: propTypes.string.isRequired,
    label: propTypes.string.isRequired,
    value_prop: propTypes.string.isRequired
};

export default ControlFeeAmount;