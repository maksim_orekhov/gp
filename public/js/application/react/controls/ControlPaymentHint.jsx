import React from 'react';
import {addSpacesBetweenThousands} from "../Helpers";

export default function ControlPaymentHint ({ amount, fee_amount, fee_payer_options, fee_payer_option, deal_role }) {

    const getPaymentHint = () => {
        let label =  'Итого ';
        const summAmount = getSumm();
        if (summAmount !== Number(amount) && deal_role === 'contractor') {
            label += 'за вычетом комиссии ';
        } else if (summAmount !== Number(amount) && deal_role === 'customer') {
            label += 'с учетом комиссии ';
        }
        label += deal_role === 'contractor' ? 'Вы получите:' : 'Вы заплатите';
        return <div className='form-info-message'><b>{label}</b> {addSpacesBetweenThousands(summAmount)} р.</div>;
    };

    const getSumm = () => {
        const feePayerOption = fee_payer_options.find((option) => option.id === +fee_payer_option);
        if (!feePayerOption) {
            return '';
        }
        const feePayerValue = feePayerOption['name'].toLowerCase();
        switch (feePayerValue){
            case 'customer':
                return deal_role.toLowerCase() === 'customer' ? +amount + +fee_amount : +amount;
            case 'contractor':
                return  deal_role.toLowerCase() === 'customer' ? +amount : +amount - +fee_amount;
            default:
                return deal_role.toLowerCase() === 'customer' ? +amount + +fee_amount / 2 : +amount - +fee_amount / 2;
        }
    };

    if (!amount || !amount.length) {
        return null;
    }

    return (
        <div className="row">
            <div className='col-xl-12'>
                {getPaymentHint()}
            </div>
        </div>
    );
}
