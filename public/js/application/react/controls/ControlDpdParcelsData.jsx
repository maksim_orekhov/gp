import React from 'react';
import Parcel from './ControlDpdParcelData'

export default class ControlDpdParcelsData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            validation: {
                parcel_is_valid: false,
            }
        };

        this.handleValidation = this.handleValidation.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(name, value) {
        const { handleComponentChange, name: name_of_upper_component, parcels } = this.props;

        parcels[name] = value;

        handleComponentChange && handleComponentChange(name_of_upper_component, [...parcels]);
    };

    setChildrenValidations(name, value) {
        return new Promise((resolve, reject) => {
            if (this.state.validation[name] === value) {
                resolve(name, value);
            } else {
                this.setState({
                    validation: {
                        ...this.state.validation,
                        [name]: value
                    }
                }, resolve(name, value));
            }
        });
    }

    handleValidation(name, value) {
        // Если значение валидации дочернего компонента не изменилось то не обновляем state,
        // но всё равно заставляем компонент сообщить наверх о своей валидации
        // !!! это нужно так как например меняется валидация дочернего delivery_type, он по-прежнему остается валидным,
        // !!! следовательно значение в state обновлять не нужно, но вот валидность самого компонента, в котором находится delivery_type
        // !!! перепроверить нужно, так как delivery_type изменился, и для нового типа доставки другое условие в валидации
        // !!! ТАКОЙ ПОДХОД СТОИТ ИСПОЛЬЗОВАТЬ ЕСЛИ КОМПОНЕНТ ИМЕЕТ СЛОЖНУЮ ЛОГИКУ ВАЛИДАЦИИ
        this.setChildrenValidations(name, value)
            .then(() => {
                this.componentValidate();
            });
    }

    componentValidate() {
        const { handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;
        const { parcel_is_valid } = this.state.validation;

        const is_valid = (
            parcel_is_valid
        );

        handleComponentValidation && handleComponentValidation(validate_prop, is_valid);
    }

    render() {
        const { parcels, handleComponentChange, cargo_category, selected_point } = this.props;

        const limits = selected_point.limits || {};
        const { weight: max_weight } = limits;

        return (
            <div>
                <Parcel
                    value_prop={parcels[0] || {}}
                    validate_prop="parcel_is_valid"
                    name="0"
                    cargo_category={cargo_category}
                    handleChangeIfDataNotBelongsParcel={handleComponentChange}
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleValidation}
                    max_weight={max_weight}
                />
            </div>
        );
    }
};