import React from 'react';
import ControlSearchHint from '../controls/ControlSearchHint';
import ControlBase from '../controls/ControlBase';
import ShowError from '../ShowError';
import PropTypes from 'prop-types';
import { customFetch } from '../Helpers';
import URLS from '../constants/urls';


export default class ControlDpdCitySearch extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            input_value: '',
            isSearchHint: false,
            keyPressed: -1,
            cities: {},
            is_loading: false,
            validation_errors: {
                isEmpty: null,
                is_city_not_picked: false,
                undefined_error: false
            },
            error_messages: {
                isEmpty: 'Выберите город из списка',
                is_city_not_picked: 'Выберите город из списка',
                undefined_error: ''
            }
        };

        this.is_ajax_waiting = false; // флаг того что идет запрос на бэк и нужно ждать ответа
        this.input_value_when_ajax_sending = ''; // Значение инпута на момент отправки ajax, если оно не совпадает со state делает повторный запрос

        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleAutoComplete = this.handleAutoComplete.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.updateCitiesCollection = this.updateCitiesCollection.bind(this);
        this.checkAllValidations = this.checkAllValidations.bind(this);
    }

    componentWillMount() {
        this.props.value_prop && this.getCityInfo();
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { value_prop, selected_city } = nextProps;
        if (!this.state.input_value && value_prop !== this.props.value_prop) {
            this.getCityInfo(value_prop, selected_city);
        }

        value_prop && this.validateControl(nextProps);
    }

    validateControl(nextProps) {
        const { value_prop, handleComponentValidation, name, validate_prop = `${name}_is_valid`, selected_city } = nextProps;

        if (value_prop && value_prop !== this.props.value_prop) {
            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value_prop, selected_city));
        }
    }

    getCityInfo(city_id = this.props.value_prop, selected_city = this.props.selected_city) {
        if (selected_city && typeof selected_city.object && selected_city.hasOwnProperty('city_id') && selected_city.city_id == city_id) {
            this.setState({
                input_value: selected_city.full_city_name || '',
            }, () => {
                this.componentValidate(city_id, selected_city);
            });
        } else {
            const { handleComponentChange } = this.props;
            this.setPreloader(true);
            customFetch(`${URLS.DPD.CITY}/${city_id}`)
                .then((data) => {
                    if (data.status === 'SUCCESS') {
                        const { city = {} } = data.data;

                        this.setState({
                            input_value: city.full_city_name || ''
                        }, () => {
                            handleComponentChange && handleComponentChange('selected_city', selected_city);
                            this.setPreloader(false);
                            this.componentValidate(city_id, city);
                            return Promise.resolve();
                        });
                    } else {
                        return Promise.reject(data);
                    }
                })
                .catch(error => {
                    console.error(error);
                    this.setPreloader(false);
                });
        }
    }

    handleAutoComplete(city_id) {
        const selected_city = this.getCityById(city_id);

        this.setState({
            input_value: selected_city.full_city_name,
            isSearchHint: false
        }, () => this.sendUpData(city_id, selected_city));
    }

    handleKeyPress(e) {
        const keyPressed  = e.keyCode;

        if (keyPressed === 40 || keyPressed === 38) {
            e.preventDefault();
        }

        this.setState({
            keyPressed
        });
    }

    setPreloader(trueOrFalse) {
        this.setState({
            is_loading: trueOrFalse
        });
    }

    getCitiesAutoCompleteUrl(city_name) {
        let url = URLS.DPD.CITY_AUTOCOMPLETE;
        if (city_name) {
            url = url + `?search=${city_name}`;
            if (this.props.csrf) {
                url = `${url}&csrf=${this.props.csrf}`;
            }
        } else {
            if (this.props.csrf) {
                url = `${url}?csrf=${this.props.csrf}`;
            }
        }

        return url;
    }

    /***
     *
     * @param city_name
     * @param {boolean} check_if_request_is_required
     */
    updateCitiesCollection(city_name, check_if_request_is_required) {
        if (!this.is_ajax_waiting) {
            if (check_if_request_is_required && this.state.cities && this.state.cities.length > 0) {
                this.setPreloader(false);
                this.is_ajax_waiting = false;
                this.setState({
                    isSearchHint: true
                });
                return;
            }

            this.setPreloader(true);
            this.is_ajax_waiting = true; // Флаг что запрос идет и новые делать не нужно
            this.input_value_when_ajax_sending = city_name;

            customFetch(this.getCitiesAutoCompleteUrl(city_name))
                .then((data) => {
                    this.is_ajax_waiting = false;
                    this.setPreloader(false);
                    if (data.status === 'SUCCESS') {
                        let cities = data.data;
                        // Т.к. с бэка при не нахождении ответа приходит пустой массив а нам нужен объект,
                        // то делаем проверку что пришел пустой массив и ставим в state пустой объект а не массив
                        if (!(cities && Object.values(cities).length)) {
                            cities = {};
                        }
                        this.setState({
                            cities,
                            isSearchHint: true
                        });

                        // Если на конец запроса значение инпута изменилось то делаем повторный запрос
                        if (this.input_value_when_ajax_sending !== this.state.input_value) {
                            this.updateCitiesCollection(this.state.input_value);
                        }
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.setPreloader(false);
                    this.is_ajax_waiting = false;
                })
        }
    }

    handleChange(e) {
        const { value } = e.target;

        // if (value.length >= 3) {
        //     this.updateCitiesCollection(value);
        // }
        this.updateCitiesCollection(value);
        this.setState({
            input_value: value
        });
        const { handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentValidation && handleComponentValidation(validate_prop, false);
    }

    getCityById(city_id) {
        return this.state.cities.find(city => city.city_id == city_id);
    }

    sendUpData(city_id, selected_city) {
        const { handleCity, name } = this.props;

        this.setState({
            isSearchHint: false
        });

        handleCity && handleCity(name, city_id, selected_city);
        this.componentValidate(city_id, selected_city);
    }

    componentValidate(value, selected_city) {
        const { handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value, selected_city));
    }

    handleBlur(e) {
        const { value } = e.target;

        this.componentValidate(value);

        this.setState({
            isSearchHint: false
        });
    }

    searchHintTitleFormat(city) {
        const { full_city_name } = city;

        return full_city_name;
    }

    updateErrors(value, selected_city) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                is_city_not_picked: !this.checkCityPicked(selected_city)
            }
        });
    }

    checkAllValidations(value, selected_city) {
        this.updateErrors(value, selected_city);

        return (
            this.validateIsEmpty(value)
            && this.checkCityPicked(selected_city)
        )
    }

    checkCityPicked(selected_city = this.props.selected_city) {
        const { input_value } = this.state;

        return selected_city.full_city_name === input_value;
    }

    render() {
        const { input_value, isSearchHint, keyPressed, is_loading, error_messages, validation_errors } = this.state;

        const cities = Object.values(this.state.cities);

        const label = this.props.label || 'Ваш населенный пункт:';

        return (
            <div className="col-xl-12">
                <div className={`Input-Wrapper ${isSearchHint ? 'hasSearchHint' : ''} ${this.colourInputField()}`}>
                    <label htmlFor="search-your-city-dpd">{label}</label>
                    <div className={is_loading ? 'Preloader' : null}>
                        <input
                            id="search-your-city-dpd"
                            name="search-city"
                            type="text"
                            placeholder="Введите город (населенный пункт)"
                            autoComplete="off"
                            value={input_value}
                            onChange={this.handleChange}
                            onKeyDown={this.handleKeyPress}
                            onBlur={this.handleBlur}
                            onFocus={() => this.updateCitiesCollection(this.input_value, true)}
                        />
                    </div>
                    {
                        (!is_loading && isSearchHint) ?
                            cities.length ?
                                <ControlSearchHint
                                    data={Object.values(cities)}
                                    hintTitleValue={this.searchHintTitleFormat}
                                    hintsCount={10}
                                    hintValueKey='city_id'
                                    keyPressed={keyPressed}
                                    handleChose={this.handleAutoComplete}
                                />
                                :
                                <p className="note-red">Город не найден</p>
                            : void(0)
                    }
                    <ShowError messages={error_messages} existing_errors={validation_errors} />
                </div>
            </div>
        );
    }
}

ControlDpdCitySearch.propTypes = {
    handleCity: PropTypes.func.isRequired,
    csrf: PropTypes.string.isRequired
};