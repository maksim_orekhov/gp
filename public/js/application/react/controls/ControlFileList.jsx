import React from 'react';
import ControlBase from './ControlBase';

export default class ControlFileList extends ControlBase {
    constructor(props) {
        super(props);
        this.state = {
            files: []
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.deleteFile = this.deleteFile.bind(this);
        this.handleDisableCheckboxChange = this.handleDisableCheckboxChange.bind(this);
    }

    validateIsEmpty(value) {
        return !!value.length;
    }

    handleChangeEvent(e) {
        const file_api = !!(window.File && window.FileReader && window.FileList && window.Blob);
        if (file_api) {
            const input_files = e.target.files;

            if (input_files.length) {
                const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

                const files = [...this.state.files, ...input_files];

                handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(input_files));
                handleComponentChange && handleComponentChange(name, files);

                $(this.input).val('');

                this.setState({
                   files
                });
            }
        }
    }

    deleteFile(e) {
        const file_index = e.target.dataset.value;
        const files = [...this.state.files];

        files.splice(file_index, 1);

        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, files);
        // handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(files));

        this.setState({
            files
        });
    }

    render() {
        const { name, label, placeholder = 'Выберите файл...', is_disable, is_disabable, id_checkbox } = this.props;
        const { files } = this.state;
        const input_label = is_disabable ? (
            <span className="control-enable-control">
                <input
                    id={id_checkbox}
                    type="checkbox"
                    tabIndex="0"
                    onChange={this.handleDisableCheckboxChange}
                />
                <label htmlFor={id_checkbox}>{label}</label>
            </span>
        ) : <label htmlFor="control-touch-file">{label}</label>;
        return (
            <div className="row">
                <div className="col-xl-12">
                    <div className="Input-Group-Wrapper">
                        <div className="Input-Wrapper">
                            { input_label }
                            <div className="input-file-wrapper">
                                <div className="InputFile">
                                    <span className={"InputFile-Container " + (is_disable ? "InputFile-Container_disabled" : "")}>
                                            <mark
                                                data-text-default={placeholder}
                                                className={"notChange " + (is_disable ? "notChange_disabled" : "")}
                                            >
                                                {
                                                    placeholder
                                                }
                                            </mark>
                                            <input
                                                name={name}
                                                id="control-touch-file"
                                                type="file"
                                                onChange={this.handleChangeEvent}
                                                ref={input => this.input = input}
                                                multiple={true}
                                                disabled={is_disable}
                                            />
                                    </span>
                                </div>
                            </div>
                            {
                                files.length > 0 &&
                                    <div className="files-list">
                                        {
                                            files.map((file, i) => {
                                                return (
                                                    <div className="file-item" key={i}>
                                                        {file.name}
                                                        <span style={{float: 'right'}} onClick={this.deleteFile} data-value={i} className="trash-icon" />
                                                    </div>
                                                );
                                            })
                                        }
                                    </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}