import React from 'react';
import PropTypes from 'prop-types';

export default class ControlSearchHint extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           activeHint: -1
        };

        this.handleClick = this.handleClick.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.updateHints(nextProps);
    }

    handleClick(e) {
        this.props.handleChose(e.target.dataset.value, e.target.firstChild.data);
    }

    handleKeyPressFromParent(props) {
        let { activeHint } = this.state;

        const { keyPressed, handleChose, hintValueKey, data, hintsCount, handleParentValue } = props;
        let maxCount = data.length;

        if (data.length > hintsCount) {
            maxCount = hintsCount;
        }

        switch (keyPressed) {
            case 40: // key down
                // maxCount - 1, т.к. индексация элементов массива начинается с 0
                if (activeHint < maxCount - 1) activeHint++;
                break;
            case 38: // key up
                if (activeHint > -1) activeHint--;
                break;
            case 13: // key enter
                if (activeHint > -1 && activeHint < maxCount) {
                    handleChose(data[activeHint][hintValueKey]);
                } else if (activeHint === -1) {
                    // Если в компоненте не выбран ни один пункт и пользователь нажал enter то срабывает метод в родители, обычно для отправки данных еще выше
                    handleParentValue && handleParentValue();
                }
                break;
            default:
                activeHint = -1
        }

        this.setState({
            activeHint
        });
    }

    updateHints(props) {
        const { keyPressed } = props;
        if (!(keyPressed === this.props.keyPressed && keyPressed === 13)) { // Чтобы не было рекурсии когда передается enter и функция много раз посылает данные вверх
            this.handleKeyPressFromParent(props);
        }
    }

    render() {
        const { activeHint } = this.state;
        const { hintTitleValue, hintValueKey, data, hintsCount } = this.props;

        return (
            <nav className="SearchHints">
                {
                    data.length ?
                        <ul>
                            {
                                data.map((hint, i) => {
                                    return (
                                        <li
                                            className={i === activeHint ? 'active' : ''}
                                            data-value={hint[hintValueKey]}
                                            key={hint[hintValueKey]}
                                            onMouseDown={this.handleClick}
                                            tabIndex={1}
                                        >
                                            {hintTitleValue(hint)}
                                        </li>
                                    )
                                }).slice(0, hintsCount)
                            }
                        </ul>
                        :
                        void(0)
                }
            </nav>
        );
    }
}

ControlSearchHint.propTypes = {
    hintsCount: PropTypes.number.isRequired,
    data: PropTypes.array.isRequired,
    hintTitleValue: PropTypes.func.isRequired,
    hintValueKey: PropTypes.string.isRequired,
    keyPressed: PropTypes.number.isRequired,
    handleChose: PropTypes.func.isRequired,
    handleParentValue: PropTypes.func
};

// Определение значений по умолчанию для props:
ControlSearchHint.defaultProps = {
    hintsCount: 5,
    data: []
};
