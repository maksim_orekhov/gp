import React from 'react';
import MESSAGES from '../constants/messages';
import ControlBase from './ControlBase';
import ShowError from '../ShowError';

export default class ControlDealRole extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleDisableCheckboxChange = this.handleDisableCheckboxChange.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    render() {
        const { value_prop, deal_type, label, is_disable, is_disabable, id_checkbox } = this.props;
        const { error_messages, validation_errors } = this.state;

        const customer_label = deal_type === 'U' ? 'Заказчик' : 'Покупатель';
        const contractor_label = deal_type === 'U' ? 'Исполнитель' : 'Продавец';

        const input_label = is_disabable ? (
            <span className="control-enable-control">
                <input
                    id={id_checkbox}
                    type="checkbox"
                    tabIndex="0"
                    onChange={this.handleDisableCheckboxChange}
                    checked={!is_disable}
                />
                <label htmlFor={id_checkbox}>{label}</label>
            </span>
        ) : <label className="label-double-indent" htmlFor="deal_role">{label}</label>;

        return (
            <div className="row">
                <div className="col-xl-12 role-in-deal">
                    <div className="Input-Wrapper">
                        { input_label }
                        <div className="InputRadio">
                            <div className="row nested-row">
                                <div className="col-xl-3 col-sm-2">
                                    <input
                                        type="radio"
                                        name="deal_role"
                                        id="customer"
                                        placeholder="Ваша роль в сделке:"
                                        value="customer"
                                        checked={value_prop === 'customer'}
                                        onChange={this.handleChangeEvent}
                                        onBlur={this.handleChangeEvent}
                                        disabled={is_disable}
                                    />
                                    <label htmlFor="customer">
                                        <span className="radiobutton" />
                                        {customer_label}
                                    </label>
                                </div>
                                <div className="col-xl-3 col-sm-2">
                                    <input
                                        type="radio"
                                        name="deal_role"
                                        id="contractor"
                                        placeholder="Ваша роль в сделке"
                                        value="contractor"
                                        checked={value_prop === 'contractor'}
                                        onChange={this.handleChangeEvent}
                                        onBlur={this.handleChangeEvent}
                                        disabled={is_disable}
                                    />
                                    <label htmlFor="contractor">
                                        <span className="radiobutton" />
                                        {contractor_label}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <ShowError messages={error_messages} existing_errors={validation_errors} />
                    </div>
                </div>
            </div>
        )
    }
};