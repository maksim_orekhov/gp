import React from 'react';
import ControlBase from './ControlBase';
import SelectImitation from './ControlSelectImitation';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';

export default class ControlNdsType extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEventDataSet = this.handleChangeEventDataSet.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
        );
    }

    getOptions() {
        const { nds_types } = this.props;

        if (nds_types.length) {
            return nds_types.map(nds_type => {
                const { type, name } = nds_type;
                return (
                    <li
                        key={type}
                        data-value={type}
                        onClick={this.handleChangeEventDataSet}
                    >
                        {name}
                    </li>
                );
            })
        }
    }

    getActiveTab() {
        const { nds_types, value_prop } = this.props;

        if (Array.isArray(nds_types) && nds_types.length) {
            const active_nds_type =  nds_types.find(nds_type => nds_type.type == value_prop);

            if (active_nds_type) {
                return active_nds_type.name;
            }
        }
        return null;
    }

    render() {
        const { label, placeholder} = this.props;
        const { error_messages, validation_errors } = this.state;

        return (
            <div className={`Input-Wrapper ${this.colourInputField()}`}>
                <label htmlFor="nds_code">{label}</label>
                <SelectImitation
                    id="nds_code"
                    default_option={placeholder}
                    active_tab={this.getActiveTab()}
                    options={this.getOptions()}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        );
    }
};