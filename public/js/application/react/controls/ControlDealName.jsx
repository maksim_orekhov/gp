import React from 'react';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';
import VALIDATION_RULES from "../constants/validation_rules";

export default class ControlDealName extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false,
                min_chars: false,
                max_chars: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.DEAL_NAME.min_chars}`,
                max_chars: `${MESSAGES.VALIDATION_ERRORS.max_chars} - ${VALIDATION_RULES.DEAL_NAME.max_chars}`,
                undefined_error: ''
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.DEAL_NAME.min_chars,
                max_chars: VALIDATION_RULES.DEAL_NAME.max_chars,
            }

        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleDisableCheckboxChange = this.handleDisableCheckboxChange.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        if(this.props.formHasDataAtLocalStorage && !nextProps.formHasDataAtLocalStorage){
            this.setState({
                validation_errors: {
                    isEmpty: null,
                    undefined_error: false
                }
            })
        }

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                min_chars: !this.validateMinCharsLength(value),
                max_chars: !this.validateMaxCharsLength(value),
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value) && this.validateMinCharsLength(value) && this.validateMaxCharsLength(value);
    }

    render() {
        const { name, label, placeholder, value_prop, is_disable, is_disabable, id_checkbox } = this.props;
        const { error_messages, validation_errors } = this.state;
        const input_label = is_disabable ? (
            <span className="control-enable-control">
                <input
                    id={id_checkbox}
                    type="checkbox"
                    tabIndex="0"
                    onChange={this.handleDisableCheckboxChange}
                    checked={!is_disable}
                />
                <label htmlFor={id_checkbox}>{label}</label>
            </span>
        ) : <label htmlFor={name}>{label}</label>;

        return (
            <div className="row">
                <div className="col-xl-12">
                    <div className={`Input-Wrapper ${this.colourInputField()}`}>
                        { input_label }
                        <input
                            name={name}
                            tabIndex="0"
                            type="text"
                            onChange={this.handleChangeEvent}
                            placeholder={placeholder}
                            value={value_prop}
                            onBlur={this.handleChangeEvent}
                            disabled={is_disable}
                        />
                        <ShowError messages={error_messages} existing_errors={validation_errors} />
                    </div>
                </div>
            </div>
        );
    }
};