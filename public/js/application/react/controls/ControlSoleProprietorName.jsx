import React from 'react';
import ControlBase from './ControlBase';
import { replaceToTextCapitalize } from '../Helpers';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';
import ShowError from '../ShowError';


export default class ControlSoleProprietorName extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            value_prop: '',
            last_name: '',
            validation_errors: {
                isEmpty: null,
                cyrillic_only: null,
                max_chars: null,
                max_words: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                cyrillic_only: MESSAGES.VALIDATION_ERRORS.cyrillic_only,
                max_chars: `${MESSAGES.VALIDATION_ERRORS.max_chars} - ${VALIDATION_RULES.SOLE_PROPRIETOR_NAME.max_chars}`,
                max_words: `${MESSAGES.VALIDATION_ERRORS.max_words} - ${VALIDATION_RULES.SOLE_PROPRIETOR_NAME.max_words}`,
                undefined_error: ''
            },
            validation_rules: {
                max_chars: VALIDATION_RULES.SOLE_PROPRIETOR_NAME.max_chars,
                max_words: VALIDATION_RULES.SOLE_PROPRIETOR_NAME.max_words
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent(e) {
        const value = replaceToTextCapitalize(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                max_chars: !this.validateMaxCharsLength(value),
                max_words: !this.validateMaxWordsLength(value),
                cyrillic_only: !this.validateRusCharacters(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateMaxCharsLength(value)
            && this.validateMaxWordsLength(value)
            && this.validateRusCharacters(value)
        );
    }

    handleFocus() {
        const { was_focused, toggleFocus } = this.props;
        if (!was_focused) {
            toggleFocus && toggleFocus();
        }
    }

    render() {
        const { name, label, placeholder, value_prop } = this.props;
        const { validation_errors, error_messages  } = this.state;

        return (
            <div className={`Input-Wrapper ${this.colourInputField()}`}>
                <label htmlFor={name}>{label}</label>
                <input
                    name={name}
                    tabIndex="0"
                    type="text"
                    onChange={this.handleChangeEvent}
                    onBlur={this.handleChangeEvent}
                    onFocus={this.handleFocus}
                    placeholder={placeholder}
                    value={value_prop}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        );
    }
}