import React from 'react';
import ControlBase from './ControlBase';

export default class ControlDealFile extends ControlBase {
    constructor(props) {
        super(props);

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.clearInput = this.clearInput.bind(this);
    }

    validateIsEmpty(value) {
        return !!value.length;
    }

    handleChangeEvent(e) {
        const files = e.target.files;

        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, files);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(files));

        const file_api = !!(window.File && window.FileReader && window.FileList && window.Blob);
        if (file_api) {
            let file_name;
            const clear = $(this.clear);
            const label = $(this.label);

            if (files.length) {
                file_name = files[0].name;
                clear.show();
                label.removeClass('notChange');
                if (label.is(':visible')) {
                    label.text(file_name.replace('C:\\fakepath\\', ''));
                }
            } else {
                this.clearInput();
            }
        }
    }

    clearInput() {
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, '');
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations([]));

        $(this.input).val('');
        const label = $(this.label);
        let text = label.data('text-default');
        label.text(text);

        label.addClass('notChange');
        $(this.clear).hide();
    }

    render() {
        const { name, label, placeholder = 'Выберите файл...' } = this.props;
        return (
            <div className="row">
                <div className="col-xl-12">
                    <div className="Input-Wrapper">
                        <label htmlFor="control-touch-file">{label}</label>
                        <div className="Input-Wrapper">
                            <div className="InputFile">
                                <span className="InputFile-Container">
                                        <mark
                                            data-text-default={placeholder}
                                            className="notChange"
                                            ref={label => this.label = label}
                                        >
                                            {
                                                placeholder
                                            }
                                        </mark>
                                        <input
                                            name={name}
                                            id="control-touch-file"
                                            type="file"
                                            onChange={this.handleChangeEvent}
                                            ref={input => this.input = input}
                                        />
                                        <span
                                            className="InputFile-Delete"
                                            ref={clear => this.clear = clear}
                                            onClick={this.clearInput}
                                        />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}