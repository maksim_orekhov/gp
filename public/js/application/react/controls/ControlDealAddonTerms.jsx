import React from 'react';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';
import VALIDATION_RULES from "../constants/validation_rules";
import {replaceRusLetterE} from "../Helpers";

const lineHeight = 18;
export default class ControlDealAddonTerms extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            rows: 7,
            validation_errors: {
                min_chars: null,
                undefined_error: false
            },
            error_messages: {
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.ADDON_TERMS.min_chars}`,
                undefined_error: ''
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.ADDON_TERMS.min_chars
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.adjustHeight = this.adjustHeight.bind(this);
        this.handleDisableCheckboxChange = this.handleDisableCheckboxChange.bind(this);
    }

    componentDidMount() {
        this.setState({
            validation_errors: {
                min_chars: null
            }
        })
    }

    componentWillMount() {
        const { value_prop, handleComponentValidation, name, validate_prop = `${name}_is_valid`, validate_on_mount = false } = this.props;

        handleComponentValidation && (validate_on_mount ? true : value_prop) && handleComponentValidation(validate_prop, this.checkAllValidations(value_prop));
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        if(this.props.formHasDataAtLocalStorage && !nextProps.formHasDataAtLocalStorage){
            this.setState({
                validation_errors: {
                    min_chars: null,
                    undefined_error: false
                },
            })
        }

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent(e) {
        const value = replaceRusLetterE(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        this.adjustHeight(e);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                min_chars: !this.validateMinCharsLengthOrEmpty(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateMinCharsLengthOrEmpty(value)
        );
    }

    adjustHeight(e) {
        const oldRows = e.target.rows;
        e.target.rows = 7;
        const newRows = Math.floor(e.target.scrollHeight/lineHeight);

        if (newRows === oldRows) {
            e.target.rows = newRows;
        }

        this.setState({
            rows: newRows
        });
    }

    render() {
        const { name, label, placeholder, value_prop, is_disable, is_disabable, id_checkbox } = this.props;
        const { error_messages, validation_errors, rows } = this.state;
        const input_label = is_disabable ? (
            <span className="control-enable-control">
                <input
                    id={id_checkbox}
                    type="checkbox"
                    tabIndex="0"
                    onChange={this.handleDisableCheckboxChange}
                    checked={!is_disable}
                />
                <label htmlFor={id_checkbox}>{label}</label>
            </span>
        ) : <label htmlFor="addon_terms">{label}</label>;

        return (
            <div className="row">
                <div className="col-xl-12">
                    <div className={`Input-Wrapper ${this.colourInputField()}`}>
                        { input_label }
                        <textarea
                            name={name}
                            id="addon_terms"
                            value={value_prop}
                            placeholder={placeholder}
                            onChange={this.handleChangeEvent}
                            onBlur={this.handleChangeEvent}
                            disabled={is_disable}
                            style={{lineHeight: `${lineHeight}px`}}
                            rows={rows}
                        />
                        <ShowError messages={error_messages} existing_errors={validation_errors}/>
                    </div>
                </div>
            </div>
        );
    }
};
