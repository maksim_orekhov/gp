import React from 'react';
import ControlBase from './ControlBase';
import { replaceToTextCapitalize } from '../Helpers';
import MESSAGES from '../constants/messages';
import VALIDATION_RULES from '../constants/validation_rules';
import ShowError from '../ShowError';

export default class ControlSecondaryName extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                cyrillic_only: null,
                min_words: null,
                max_words: null,
                min_chars: false,
                undefined_error: false
            },
            error_messages: {
                cyrillic_only: MESSAGES.VALIDATION_ERRORS.cyrillic_only,
                min_words: `${MESSAGES.VALIDATION_ERRORS.min_words} - ${VALIDATION_RULES.SECONDARY_NAME.min_words}`,
                max_words: `${MESSAGES.VALIDATION_ERRORS.max_words} - ${VALIDATION_RULES.SECONDARY_NAME.max_words}`,
                min_chars: `${MESSAGES.VALIDATION_ERRORS.min_chars} - ${VALIDATION_RULES.SECONDARY_NAME.min_chars}`,
                undefined_error: ''
            },
            validation_rules: {
                min_words: VALIDATION_RULES.SECONDARY_NAME.min_words,
                max_words: VALIDATION_RULES.SECONDARY_NAME.max_words,
                min_chars: VALIDATION_RULES.SECONDARY_NAME.min_chars
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent(e) {
        const value = replaceToTextCapitalize(e.target.value);
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                min_chars: !this.validateMinCharsLengthOrEmpty(value),
                min_words: !this.validateMinWordsLength(value),
                max_words: !this.validateMaxWordsLength(value),
                cyrillic_only: !this.validateRusCharacters(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateMinWordsLength(value)
            && this.validateMinCharsLengthOrEmpty(value)
            && this.validateMaxWordsLength(value)
            && this.validateRusCharacters(value)
        );
    }

    render() {
        const { name, label, placeholder, value_prop } = this.props;
        const { validation_errors, error_messages } = this.state;

        return (
            <div className={`Input-Wrapper ${this.colourInputField()}`}>
                <label htmlFor={name}>{label}</label>
                <input
                    name={name}
                    tabIndex="0"
                    type="text"
                    onChange={this.handleChangeEvent}
                    onBlur={this.handleChangeEvent}
                    placeholder={placeholder}
                    value={value_prop}
                />
                <ShowError messages={error_messages} existing_errors={validation_errors} />
            </div>
        );
    }
}