import React from 'react';
import ControlBase from './ControlBase';
import MESSAGES from '../constants/messages';
import ShowError from '../ShowError';
import {textOnlyNumbers} from "../Helpers";

export default class ControlHouse extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    render() {
        const { name, label, placeholder, value_prop, is_disable } = this.props;
        const { error_messages, validation_errors } = this.state;
        return (
            <div className="col-xl-3 col-sm-2">
                <div className={`Input-Wrapper ${this.colourInputField()}`}>
                    <label htmlFor={name}>{label}</label>
                    <input
                        name={name}
                        tabIndex="0"
                        type="text"
                        onChange={this.handleChangeEvent}
                        placeholder={placeholder}
                        value={value_prop}
                        onBlur={this.handleChangeEvent}
                        disabled={is_disable}
                    />
                    <ShowError messages={error_messages} existing_errors={validation_errors} />
                </div>
            </div>
        );
    }
};