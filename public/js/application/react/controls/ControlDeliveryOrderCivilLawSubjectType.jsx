import React from 'react';
import MESSAGES from '../constants/messages';
import ControlBase from './ControlBase';
import ShowError from '../ShowError';
import ShowHint from '../ShowHint';

export default class ControlDeliveryOrderCivilLawSubjectType extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    render() {
        const { is_disable, name, value_prop } = this.props;
        const { error_messages, validation_errors } = this.state;
        const hints = {base_hint: true};
        const hint_messages = {base_hint: MESSAGES.HINTS.delivery_option};

        return (
            <div className="col-xl-12">
                <div className="Input-Wrapper">
                    <div className="InputRadio">
                        <div className="row nested-row">
                            <div className="col-xl-6 col-sm-2">
                                <input
                                    type="radio"
                                    name={name}
                                    value="natural_person"
                                    id="delivery_order_cls_type_natural_person"
                                    checked={value_prop === 'natural_person'}
                                    onChange={this.handleChangeEvent}
                                    onBlur={this.handleChangeEvent}
                                    disabled={is_disable}
                                />
                                <label htmlFor="delivery_order_cls_type_natural_person">
                                    <span className="radiobutton" />
                                    Физическое лицо
                                </label>
                            </div>
                            <div className="col-xl-6 col-sm-2">
                                <input
                                    type="radio"
                                    name={name}
                                    value="legal_entity"
                                    id="delivery_order_cls_type_legal_entity"
                                    checked={value_prop === 'legal_entity'}
                                    onChange={this.handleChangeEvent}
                                    onBlur={this.handleChangeEvent}
                                    disabled={is_disable}
                                />
                                <label htmlFor="delivery_order_cls_type_legal_entity">
                                    <span className="radiobutton" />
                                    Юридическое лицо
                                </label>
                            </div>
                        </div>
                    </div>
                    <ShowError messages={error_messages} existing_errors={validation_errors} />
                    {/*<ShowHint messages={hint_messages} existing_hints={hints} />*/}
                </div>
            </div>
        )
    }
};