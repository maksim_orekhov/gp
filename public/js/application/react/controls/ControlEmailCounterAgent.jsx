import React from 'react';
import Email from './ControlEmail.jsx';
// import Email from '../../../landing/react/auth/forms/RegisterForm/controls/email-input.jsx';
import ShowHint from '../ShowHint';
import { COUNTERAGENT_DEAL_ROLES } from '../constants/deal_roles';
import MESSAGES from "../constants/messages";

const ControlEmailCounterAgent = (props) => {
    const { deal_role, deal_type, handleComponentChange, handleComponentValidation, form_control_server_errors, value_prop, is_agent_email, formHasDataAtLocalStorage, label, placeholder } = props;

    const hints = {base_hint: true};
    const hint_messages = {base_hint: MESSAGES.HINTS.email};

    return (
        <div className="row contractor-email">
            <div className="col-xl-12 col-sm-4 col-email">
                <div className="Input-Wrapper">
                    <Email
                        name="counteragent_email"
                        label= {label ? label : `Email ${COUNTERAGENT_DEAL_ROLES[deal_type][deal_role].rp}:`}
                        handleComponentChange={handleComponentChange}
                        not_check_unique={true}
                        register={false}
                        handleComponentValidation={handleComponentValidation}
                        form_control_server_errors={form_control_server_errors}
                        value_prop={value_prop}
                        is_agent_email={is_agent_email}
                        formHasDataAtLocalStorage={formHasDataAtLocalStorage}
                        placeholder={placeholder}
                    />
                    {/*<ShowHint messages={hint_messages} existing_hints={hints} />*/}
                </div>
            </div>
        </div>
    );
};

export default ControlEmailCounterAgent;