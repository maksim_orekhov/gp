import React from 'react';
import MESSAGES from '../constants/messages';
import ControlBase from './ControlBase';
import ShowError from '../ShowError';

export default class ControlButtonAuthorRole extends ControlBase {
    constructor(props) {
        super(props);

        this.state = {
            validation_errors: {
                isEmpty: null,
                undefined_error: false
            },
            error_messages: {
                isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
                undefined_error: ''
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
    }

    componentWillMount() {
        this.checkValidateOnMount();
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, value_prop } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

        value_prop && this.validateControl(nextProps);
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value)
            }
        });
    }

    checkAllValidations(value) {
        this.updateErrors(value);
        return this.validateIsEmpty(value);
    }

    render() {
        const { value_prop, deal_type, label, is_disable } = this.props;
        const { error_messages, validation_errors } = this.state;

        const participant_label = 'Участник сделки';
        const referer_label = 'Реферер';

        return (
            <div className="row">
                <div className="col-xl-12 role-in-deal">
                    <div className="Input-Wrapper">
                        <label className="label-double-indent" htmlFor="deal_role">{label}</label>
                        <div className="InputRadio">
                            <div className="row nested-row">
                                <div className="col-xl-4 col-sm-2">
                                    <input
                                        type="radio"
                                        name="button_author_role"
                                        id="participant"
                                        placeholder="Вы будете размещать кнопку как"
                                        value="participant"
                                        checked={value_prop === 'participant'}
                                        onChange={this.handleChangeEvent}
                                        onBlur={this.handleChangeEvent}
                                        disabled={is_disable}
                                    />
                                    <label htmlFor="participant">
                                        <span className="radiobutton" />
                                        {participant_label}
                                    </label>
                                </div>
                                <div className="col-xl-4 col-sm-2">
                                    <input
                                        type="radio"
                                        name="button_author_role"
                                        id="referer"
                                        placeholder="Вы будете размещать кнопку как"
                                        value="referer"
                                        checked={value_prop === 'referer'}
                                        onChange={this.handleChangeEvent}
                                        onBlur={this.handleChangeEvent}
                                        disabled={is_disable}
                                    />
                                    <label htmlFor="referer">
                                        <span className="radiobutton" />
                                        {referer_label}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <ShowError messages={error_messages} existing_errors={validation_errors} />
                    </div>
                </div>
            </div>
        )
    }
};