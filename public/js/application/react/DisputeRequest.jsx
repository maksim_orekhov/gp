import React from 'react';
import DisputeRequestInfo from './DisputeRequestInfo';
import FormRequestConfirm from './forms/FormRequestConfirm';

const DisputeRequest = (props) => {
    const {
        request: {
            confirm, is_author, id, amount, request_type
        },
        deal_type, handleOnSubmit, is_operator, is_allowed_confirm, is_customer,
        is_arbitrage_after_three_discount_rejects, is_arbitrage_after_refund_reject, is_arbitrage_after_tribunal_reject
    } = props;

    if (is_allowed_confirm && confirm === null) {
        return (
            <FormRequestConfirm
                request_type={request_type}
                request_id={id}
                request_amount={amount}
                deal_type={deal_type}
                handleOnSubmit={handleOnSubmit}
                is_customer={is_customer}
            />
        )
    }
    if (confirm === null || confirm.status === true) {
        return (
            <DisputeRequestInfo
                request_type={request_type}
                confirm={confirm}
                request_amount={amount}
                deal_type={deal_type}
                is_author={is_author}
                is_customer={is_customer}
                is_arbitrage_after_three_discount_rejects={is_arbitrage_after_three_discount_rejects}
                is_arbitrage_after_refund_reject={is_arbitrage_after_refund_reject}
                is_arbitrage_after_tribunal_reject={is_arbitrage_after_tribunal_reject}
            />
        )
    } else {
        return (
            null
        )
    }
};

export default DisputeRequest;