const MANDARIN_PAYMENT_STATUSES = {
    SUCCESS: 'success',
    FAILED: 'failed',
    PROCESSING: 'processing'
};

export default MANDARIN_PAYMENT_STATUSES;