export const BREAKPOINT_TABLET = 1024;
export const BREAKPOINT_MOBILE = 768;
export const BREAKPOINT_SMALL  = 460;
