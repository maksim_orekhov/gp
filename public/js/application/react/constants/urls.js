const URLS = {
    CSRF: {
      GET: '/auth/csrf-token'
    },
    LOGIN: '/login',
    LOGIN_EDIT: '/login/edit',
    USER: '/user',
    DEAL: {
        CREATE: '/deal/create',
        CREATE_UNREGISTERED: '/deal/create/unregistered-form',
        FORM_INIT: '/deal/form-init',
        SINGLE: '/deal',
        SHOW: '/deal/show',
        ABNORMAL: '/deal/abnormal',
        EXPIRED: '/deal/expired',
        DEADLINE: '/deal-deadline/notify'
    },
    BUTTON: {
        FORM_INIT: '/profile/partner-button/form-init',
        CREATE: '/profile/partner-button/create',
        EDIT: '/profile/partner-button',
        SWITCH_ACTIVE: '/profile/partner-button/switch-active',
    },
    PAYMENT_METHOD: {
        FORM_INIT: '/profile/payment-method/form-init',
        SINGLE: '/profile/payment-method'
    },
    NATURAL_PERSON: {
        SINGLE: '/profile/natural-person'
    },
    LEGAL_ENTITY: {
        SINGLE: '/profile/legal-entity',
        FORM_INIT: '/profile/legal-entity/form-init'
    },
    SOLE_PROPRIETOR: {
        SINGLE: '/profile/sole-proprietor',
    },
    PHONE: {
        CREATE: '/phone/create',
        EDIT: '/phone/edit',
        CONFIRM_CODE: '/phone/confirm',
        RESEND_CODE: '/phone/confirm/code-resend',
        CONFIRM_TOKEN: '/phone/edit-confirm',
        RESEND_TOKEN: '/phone/confirm/edit-code-resend'
    },
    EMAIL: {
        EDIT: '/email/edit',
        CONFIRM: '/email/confirm',
        RESEND: '/email/resend'
    },
    PROFILE: '/profile',
    BANK: {
        SEARCH: '/bank/search',
    },
    BANK_CLIENT: {
        PAYMENT_ORDERS_TROUBLE: '/bank-client/payment-orders/trouble'
    },
    DISPUTE: {
        SINGLE: '/dispute',
    },
    TERMS_OF_USE: '/terms-of-use',
    LOG_ERRORS: '/error/send',
    MANDARIN_PAY: 'https://secure.mandarinpay.com/api/hosted.js',
    MANDARIN: {
        CHECK_STATUS: '/mandarin/check',
        STATUS_INIT: '/mandarin/order-id',
        OPERATION_ID: '/mandarin/hash-operation-id'
    },
    DPD: {
        FORM_INIT: '/delivery/dpd/city/form-init',
        CITY: '/delivery/dpd/city',
        CITY_AUTOCOMPLETE: '/dpd/city/autocomplete',
        DELIVERY_COST: '/delivery/dpd/city/cost'
    }
};

export default URLS;