const NAMES = {
    DISPUTE_RESOLVE_LABELS: {
        discount: 'Скидка',
        refund: 'Возврат товара',
        arbitrage: 'Согласительная комиссия',
        tribunal: 'Суд'
    }
};

export const DELIVERY_TYPES_NAMES = {
    NO_SERVICE: 'NO_SERVICE',
    DPD: 'DPD',
    NOT_REQUIRED: 'NOT_REQUIRED'
};

export default NAMES;