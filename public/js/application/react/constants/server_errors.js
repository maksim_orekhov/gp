import MESSAGES from '../constants/messages';

export const SERVER_ERRORS_MESSAGES = {
    critical_error: MESSAGES.SERVER_ERRORS.CRITICAL_ERROR.text,
    not_auth: MESSAGES.SERVER_ERRORS.NOT_AUTH.text,
    access_denied: MESSAGES.SERVER_ERRORS.ACCESS_DENIED.text,
    csrf: MESSAGES.SERVER_ERRORS.CSRF.text,
    invalid_form_data: MESSAGES.SERVER_ERRORS.FORM_IS_NOT_VALID.text,
    form_init_fail: MESSAGES.SERVER_ERRORS.FORM_INIT_FAIL.text,
    invalid_code: MESSAGES.SERVER_ERRORS.PHONE_INVALID_CODE.text,
    country_not_defined: MESSAGES.SERVER_ERRORS.PHONE_COUNTRY_NOT_DEFINED.text,
    already_auth: MESSAGES.SERVER_ERRORS.ALREADY_AUTHORIZED.text,
    auth_failed: MESSAGES.SERVER_ERRORS.AUTHENTICATION_FAILED.text || MESSAGES.SERVER_ERRORS.AUTHENTICATION_FAILED.text,
    cls_confirmed_deal_removal: MESSAGES.SERVER_ERRORS.CLS_CONFIRMED_DEAL_REMOVAL.text,
    cls_linked_with_token_removal: MESSAGES.SERVER_ERRORS.CLS_LINKED_WITH_TOKEN_REMOVAL.text,
    deal_with_such_id_not_found: MESSAGES.SERVER_ERRORS.DEAL_WITH_SUCH_ID_NOT_FOUND.text,
    invalid_captcha: MESSAGES.SERVER_ERRORS.INVALID_CAPTCHA.text,
    deal_invitation_not_sent: MESSAGES.SERVER_ERRORS.DEAL_INVITATION_NOT_SENT.text,
    cls_not_found_type: MESSAGES.SERVER_ERRORS.CIVIL_LAW_SUBJECT_NOT_FOUND_TYPE.text,
    undefined_error: ''
};

export const SERVER_ERRORS = {
    critical_error: null,
    not_auth: null,
    access_denied: null,
    csrf: null,
    invalid_form_data: null,
    form_init_fail: null,
    invalid_code: null,
    country_not_defined: null,
    already_auth: null,
    auth_failed: null,
    undefined_error: null,
    cls_confirmed_deal_removal: null,
    cls_linked_with_token_removal: null,
    deal_with_such_id_not_found: null,
    invalid_captcha: null,
    deal_invitation_not_sent: null
};