import React from 'react';
import { customFetch, getIdFromUrl } from "./Helpers";
import URLS from './constants/urls';
import FormBase from './forms/FormBase';
import ShowError from './ShowError';
import { SERVER_ERRORS, SERVER_ERRORS_MESSAGES } from './constants/server_errors';
import MANDARIN_PAYMENT_STATUSES from './constants/mandarin_payment_statuses';

export default class MandarinPaymentStatus extends FormBase {
    constructor(props) {
        super(props);

        this.state = {
            payment_status: MANDARIN_PAYMENT_STATUSES.PROCESSING,
            orderId: null,
            login: null,
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            customer_amount: '',
            created: ''
        };

        this.checkStatus = this.checkStatus.bind(this);
    }

    componentWillMount() {
        this.init();
        this.getMandarinPaymentData();
    }

    componentDidMount() {
        document.getElementById('MandarinPaymentStatusProcessing').style.visibility = 'visible';
    }

    init() {
        customFetch(URLS.MANDARIN.STATUS_INIT, {
            method: 'POST',
            body: JSON.stringify({
                id: getIdFromUrl()
            })
        })
            .then(data => {
                console.log(data);
                if (data.status === 'SUCCESS') {
                    const { userLogin, orderId } = data.data;
                    if (orderId) {
                        this.setState({
                            orderId,
                            login: userLogin
                        }, () => this.checkStatus())
                    } else {
                        return Promise.reject('Ошибка при получении ордер id')
                    }
                } else if (data.status === 'ERROR') {
                    return Promise.reject(data.message);
                }
            })
            .catch(err => {
                console.log(err);
                this.setFormError('form_init_fail', true);
            })
    }

    checkStatus() {
        customFetch(URLS.MANDARIN.CHECK_STATUS, {
            method: 'POST',
            body: JSON.stringify({
                orderId: this.state.orderId
            })
        })
            .then(data => {
                console.log(data);
                if (data.status === 'SUCCESS') {
                    const { status } = data.data;
                    if (status === MANDARIN_PAYMENT_STATUSES.SUCCESS || status === MANDARIN_PAYMENT_STATUSES.FAILED) {
                        window.location.reload(true);
                    } else if (status === MANDARIN_PAYMENT_STATUSES.PROCESSING) {
                        setTimeout(() => this.checkStatus(), 2000);
                    }
                } else if (data.status === 'ERROR') {
                    return Promise.reject(data.message);
                }
            })
            .catch(error => {
                console.log(error);
                this.setFormError('critical_error', true);
            });
    }

    getMandarinPaymentData() {
        const deal_id = getIdFromUrl();

        customFetch(`${URLS.DEAL.FORM_INIT}?idDeal=${deal_id}`)
            .then(data => {
                this.setState({
                        customer_amount: data.data.deal.customer_amount,
                        created: data.data.deal.mandarin_acquiring.created,
                        transaction: data.data.deal.mandarin_acquiring.transaction
                });
            });
    }

    render() {
        const { login, payment_status, customer_amount, created, transaction } = this.state;

        return (
            <div>
                {{
                    [MANDARIN_PAYMENT_STATUSES.PROCESSING]: (
                        <div className="payment-info">
                            <div className="payment-info-header">
                                <div className="Preloader" style={{width: '36px', marginRight: '12px', marginLeft: '-24px'}}/>
                                <p className="TextMarked payment-status">
                                    Уважаемый <strong>{login}</strong>! <br />
                                    Ваш платёж обрабатывается. Пожалуйста, подождите...</p>
                            </div>
                            <p className="Note">
                                В зависимости от нагрузки системы, время обработки Вашей транзакции может достигать
                                2-3х часов. <br />
                                В случае, если по прошествии этого времени статус по-прежнему в "обработке", просим
                                Вас обратиться в техническую поддержку и указать номер сделки.
                            </p>
                            <div className="payment-details">
                                <h4 className="payment-details-title">Детали платежа</h4>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Дата:</td>
                                            <td className="value">{created}</td>
                                        </tr>
                                        <tr>
                                            <td>N транзакции:</td>
                                            <td className="value">{transaction}</td>
                                        </tr>
                                        <tr className="sum">
                                            <td>Сумма:</td>
                                            <td className="value">{customer_amount} Р.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className='icon-info-blue' />
                            <span> Если платеж не будет обработан в течение часа, пожалуйста, обратитесь в службу поддержки "Гарант Пэй"</span>
                        </div>
                    ),
                    [MANDARIN_PAYMENT_STATUSES.FAILED]: (
                        <div className="payment-info">
                            <div className="payment-info-header">
                                <div className="icon-warning" />
                                <p className="TextMarked payment-status">
                                    Уважаемый <strong>{login}</strong>! <br />
                                    Оплата по карте не прошла. Не удалось выполнить транзакцию.
                                </p>
                            </div>
                            <div className="payment-details">
                                <h4 className="payment-details-title">Детали платежа</h4>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Дата:</td>
                                            <td className="value">{created}</td>
                                        </tr>
                                        <tr>
                                            <td>N транзакции:</td>
                                            <td className="value">{transaction}</td>
                                        </tr>
                                        <tr className="sum">
                                            <td>Сумма:</td>
                                            <td className="value">{customer_amount} Р.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    ),
                    [MANDARIN_PAYMENT_STATUSES.SUCCESS]: (
                        <div className="payment-info">
                            <div className="payment-info-header">
                                <div className="icon-ok-large" />
                                <p className="TextMarked payment-status">
                                    Уважаемый <strong>{login}</strong>! <br />
                                    Оплата по карте прошла успешно. Платеж по сделке зачислен на гарантийный счет.</p>
                            </div>
                            <div className="payment-details">
                                <h4 className="payment-details-title">Детали платежа</h4>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Дата:</td>
                                            <td className="value">{created}</td>
                                        </tr>
                                        <tr>
                                            <td>N транзакции:</td>
                                            <td className="value">{transaction}</td>
                                        </tr>
                                        <tr className="sum">
                                            <td>Сумма:</td>
                                            <td className="value">{customer_amount} Р.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    )
                }[payment_status]}
                <ShowError messages={this.state.server_errors_messages} existing_errors={this.state.server_errors} />
            </div>
        );
    }
};