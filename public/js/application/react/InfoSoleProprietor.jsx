import React from 'react';
import FormSoleProprietor from './forms/FormSoleProprietor';
import Popup from './Popup';
import InfoField from './InfoField';
import { customFetch } from './Helpers';
import URLS from './constants/urls';

export default class InfoSoleProprietor extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         soleProprietor: '',
         isOpenedFormEdit: false,
      };
      this.getInfo = this.getInfo.bind(this);
      this.afterDelete = this.afterDelete.bind(this);
   }

   componentDidMount() {
      !this.props.isNested && this.getInfo();
   }

   getInfo() {
      customFetch(window.location)
          .then(data => {
              console.log(data);
              this.setState({
                  isOpenedFormEdit: false,
                  soleProprietor: data.data.soleProprietor,
              });
          })
          .catch(error => console.log(error));
   }

    afterDelete() {
        window.location = URLS.PROFILE;
    }

   componentWillReceiveProps(nextProps) {
      this.setState({
          soleProprietor: nextProps.data
      });
   }

   toggleFormEdit(collectionItem) {
      this.setState({
         collectionItemEdit: collectionItem,
         isOpenedFormEdit: !this.state.isOpenedFormEdit
      });
   }

   render() {
      const { isOpenedFormEdit, soleProprietor, collectionItemEdit } = this.state;
      const { name, last_name, first_name, secondary_name, inn, birth_date } = soleProprietor;
      return (
          <div className="container">
              <div className="row nested-row">
                  <div className="col-xl-12">
                      <div className="CivilLawSubjectPage-Header">
                          <div className="TitleWithEditIcon">
                              <h1 className="TitleBlue">Индивидуальный предприниматель</h1>
                              <a className="IconEdit" onClick={() => this.toggleFormEdit(soleProprietor)} />
                          </div>
                          <div className="CivilLawSubjectDataOverview">
                              <div className="CivilLawSubjectDataOverview-Column">
                                  <InfoField label="Наименование:" value={name} />
                                  <InfoField label="ИНН:" value={inn} />
                                  <InfoField label="Дата рождения:" value={birth_date} />
                              </div>
                              <div className="CivilLawSubjectDataOverview-Column">
                                  <InfoField label="Фамилия:" value={last_name} />
                                  <InfoField label="Имя:" value={first_name} />
                                  <InfoField label="Отчество:" value={secondary_name} />
                              </div>
                          </div>
                          {
                              isOpenedFormEdit &&
                              <Popup close={() => this.toggleFormEdit({})}>
                                  <FormSoleProprietor
                                      data={soleProprietor}
                                      editMode={true}
                                      afterSubmit={this.getInfo}
                                      afterDelete={this.afterDelete}
                                      item_id={collectionItemEdit}
                                  />
                              </Popup>
                          }
                      </div>
                  </div>
              </div>
          </div>
      );
   }
}
