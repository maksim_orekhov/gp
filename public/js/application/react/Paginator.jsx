import React from 'react';

export default class Paginator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: ''
        };

        this.handleClick = this.handleClick.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.handleInputValue = this.handleInputValue.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleButtonClick = this.handleButtonClick.bind(this);
        this.changePerPageCount = this.changePerPageCount.bind(this);
    }

    sendUpNewPageNumber(value) {
        const { first, last } = this.props.paginator;

        if (value >= first && value <= last) {
            this.props.handlePageSwitch(value);
        }
    }

    initPaginationItems() {
        const { first_page_in_range, last_page_in_range, current } = this.props.paginator;
        const pages = [];
        let next, prev;

        if (current - 1 >= first_page_in_range) {
            prev = <a href=''
                      className='Pagination-Item Pagination-Prev'
                      key='prev'
                      onClick={this.handlePrev}
            >
                <img src='../../../img/pagination-arrow-icon.svg' alt='<' />
            </a>
        } else {
            prev = <span className='Pagination-Item Pagination-Prev disable' key='prev'>
                <img src='../../../img/pagination-arrow-icon-disable.svg' alt='<' />
            </span>
        }
        pages.push(prev);

        for (let i = first_page_in_range; i <= last_page_in_range; i++) {
            let paginatorItem;
            if (i !== current) {
                paginatorItem = <a
                    href=''
                    className='Pagination-Item'
                    key={i}
                    data-value={i}
                    onClick={this.handleClick}
                >
                    {i}
                </a>;
            } else {
                paginatorItem = <span
                    className='Pagination-Item active'
                    key={i}
                >
                    {i}
                </span>
            }
            pages.push(paginatorItem);
        }

        if (current + 1 <= last_page_in_range) {
            next = <a href=''
                      className='Pagination-Item Pagination-Next'
                      key='next'
                      onClick={this.handleNext}
            >
                <img src='../../../img/pagination-arrow-icon.svg' alt='>' />
            </a>
        } else {
            next = <span className='Pagination-Item Pagination-Next disable' key='next'>
                <img src='../../../img/pagination-arrow-icon-disable.svg' alt='>' />
            </span>
        }

        pages.push(next);

        return pages;
    }

    handleClick(e) {
        e.preventDefault();
        this.sendUpNewPageNumber(e.target.dataset.value);
    }

    handlePrev(e) {
        e.preventDefault();
        const { first, current } = this.props.paginator;

        if (current > first) {
            this.props.handlePageSwitch(current - 1);
        }
    }

    handleNext(e) {
        e.preventDefault();
        const { last, current } = this.props.paginator;

        if (current < last) {
            this.props.handlePageSwitch(current + 1);
        }
    }

    handleInputValue(e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            this.sendUpNewPageNumber(e.target.value);
        }
    }

    handleInputChange(e) {
        const { first, last } = this.props.paginator;
        const value = e.target.value.replace(/\D/g, '');
        let inputValue = value;

        if (value < first ) {
            inputValue = '';
        } else if (value > last) {
            inputValue = last;
        }

        this.setState({
           inputValue
        });
    }

    handleButtonClick(e) {
        e.preventDefault();
        this.sendUpNewPageNumber(this.state.inputValue);
    }

    changePerPageCount(e) {
        const { value } = e.target;
        this.props.handlePerPageChange(value);
    }

    render() {
        const { per_page } = this.props;
        const paginationItems = this.initPaginationItems();
        return (
                <div className="Pagination">
                    <div className="Pagination-Wrap">
                        {
                            paginationItems
                        }
                        <div className="Pagination-ControlPanel">
                            <button className="Pagination-Button" onClick={this.handleButtonClick}>Перейти</button>
                            <input
                                className="Pagination-Input"
                                type="text"
                                onKeyDown={this.handleInputValue}
                                onChange={this.handleInputChange}
                                value={this.state.inputValue}
                            />
                        </div>
                        <div className='Pagination-PerPager'>
                            <label htmlFor="per_page_select">Показывать по</label>
                            <select name="" id="per_page_select" onChange={this.changePerPageCount} value={per_page}>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                            </select>
                        </div>
                    </div>
                </div>
        );
    }
}
