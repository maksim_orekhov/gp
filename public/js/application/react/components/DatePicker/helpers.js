export const months = [
    'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
    'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь',
];

export const weekDays = [
    'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс',
];

export function getMonthNameByNumber(monthNumber) {
    return months[monthNumber] || '';
}

/**
 * Валидатор PropTypes масиива дат для DatePicker
 * Каждый элемент массива должен быть объектом даты или массивом из двух объектов даты
 * @param propValue
 * @param propName
 * @param componentName
 * @returns {*}
 * @constructor
 */
export function DATES_ARRAY_PROPTYPES(propValue, propName, componentName) {
    if (
        !Array.isArray(propValue)
        || !propValue.every((propItem) => {
            return propItem instanceof Date || (Array.isArray(propItem) && propItem.length === 2 && propItem.every(date => date instanceof Date))
        })
    ) {
        return new Error(`Invalid prop  supplied to ${componentName}. ${propName} needs to be an date instance or array of two dates. Validation failed.`);
    }

    return null;
}

/**
 * Проверяет, что дата дня не входит в список заблокированных дат и выше минимальной даты и ниже максимальной
 * @param date
 * @param disabledDates
 * @param minDate
 * @param maxDate
 * @returns {boolean | *}
 */
export function dateInDisabledDateList(date, disabledDates = [], minDate = null, maxDate = null) {
    const dateTimeStamp = +date;
    return disabledDates.some((disabledDate) => {
        return (Array.isArray(disabledDate) && dateTimeStamp >= +disabledDate[0] && dateTimeStamp <= +disabledDate[1])
            || dateTimeStamp === +disabledDate
    }) || minDate && +minDate >= dateTimeStamp || maxDate && +maxDate <= dateTimeStamp;
}

/**
 * Проверка, что месяц выше минимальной даты и ниже максимальной
 * @param monthNumber
 * @param visibleDate
 * @param minDate
 * @param maxDate
 * @returns {*|boolean}
 */
export function monthIsDisabled(monthNumber, visibleDate, minDate, maxDate) {
    return minDate && (minDate.getFullYear() > visibleDate.getFullYear() || minDate.getFullYear() === visibleDate.getFullYear() && minDate.getMonth() > monthNumber)
        || maxDate && (maxDate.getFullYear() < visibleDate.getFullYear() || maxDate.getFullYear() === visibleDate.getFullYear() && maxDate.getMonth() < monthNumber);
}

/**
 * Проверяет выбрана ли дата
 * @param {Date} date
 * @param {Array<Date> | Date | null} selectedDates
 * @returns {boolean}
 */
export function dateIsSelected(date, selectedDates = []) {
    return Array.isArray(selectedDates) && selectedDates.some((selectedDate) => selectedDate && date.toString() === selectedDate.toString())
        || selectedDates instanceof Date && date.toString() === selectedDates.toString();
}