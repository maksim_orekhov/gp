import React from 'react';
import PropTypes from 'prop-types';
import {DATES_ARRAY_PROPTYPES, weekDays, dateInDisabledDateList, dateIsSelected} from './helpers';

function DaysTable({ handleDayClick, visibleDate, selectedDate, currentDate, disabledDates, disabledDays, minDate, maxDate }) {

    const getVisibleDays = () => {
        const dateForRender = new Date(visibleDate.getFullYear(), visibleDate.getMonth(), 1);
        if (dateForRender.getDay() !== 1) {
            dateForRender.setDate(dateForRender.getDate() - dateForRender.getDay() + 1);
        }
        const days = [];
        while (
            (dateForRender.getMonth() <= visibleDate.getMonth() && dateForRender.getFullYear() === visibleDate.getFullYear())
            || (dateForRender.getMonth() > visibleDate.getMonth() && dateForRender.getFullYear() < visibleDate.getFullYear())
            || dateForRender.getDay() !== 1
        ) {
            days.push(new Date(dateForRender));
            dateForRender.setDate(dateForRender.getDate() + 1);
        }

        return days;
    };

    const getClassNamesForDayCell = (date, dayNumber) => {
        let dayClassNames = 'date-picker-cell date-picker-cell-day';
        if (visibleDate.getMonth() !== date.getMonth()) {
            dayClassNames += ' other-month';
        }
        if (dateIsSelected(date, selectedDate)) {
            dayClassNames +=  ' selected';
        }
        if (currentDate.toDateString() === date.toDateString()) {
            dayClassNames +=  ' current';
        }

        if (dateInDisabledDateList(date, disabledDates, minDate, maxDate) || disabledDays.includes(dayNumber)) {
            dayClassNames += ' disabled';
        }

        return dayClassNames;
    };

    return(
        <div className="date-picker-body">
            <div className="date-picker-days__names">
                {weekDays.map((dayName) => <div className="date-picker-day__name" key={dayName}>{dayName}</div>)}
            </div>
            <div className="date-picker-cells">
                {
                    getVisibleDays().map((dateOfDay, dayIndex) => {
                        const dayNumber = dayIndex === 0 ? dayIndex : dayIndex % 7;
                        return(
                            <div
                                className={getClassNamesForDayCell(dateOfDay, dayNumber)}
                                onClick={!dateInDisabledDateList(dateOfDay, disabledDates, minDate, maxDate) && !disabledDays.includes(dayNumber) ? handleDayClick(dateOfDay) : null}
                                key={dateOfDay.toDateString()}
                            >
                                {dateOfDay.getDate()}
                            </div>
                        );
                    })
                }
            </div>
        </div>
    );
}

DaysTable.propTypes = {
    visibleDate: PropTypes.instanceOf(Date).isRequired,
    currentDate: PropTypes.instanceOf(Date).isRequired,
    handleDayClick: PropTypes.func.isRequired,
    disabledDates: PropTypes.arrayOf(DATES_ARRAY_PROPTYPES),
    disabledDays: PropTypes.arrayOf(PropTypes.number),
    selectedDate: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.arrayOf(PropTypes.instanceOf(Date)),
    ]),
    minDate: PropTypes.instanceOf(Date),
    maxDate: PropTypes.instanceOf(Date),
};

DaysTable.defaultProps = {
    selectedDate: null,
    minDate: null,
    maxDate: null,
    disabledDates: [],
    disabledDays: [],
};

export default DaysTable;
