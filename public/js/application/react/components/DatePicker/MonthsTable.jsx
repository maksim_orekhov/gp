import React from 'react';
import PropTypes from 'prop-types';
import { months, monthIsDisabled } from './helpers';

function MonthsTable({ visibleDate, selectedDate, currentDate, handleMonthClick, minDate, maxDate }) {

    const getClassNamesForMonthCell = (monthNumber) => {
        let monthClassNames = 'date-picker-cell date-picker-cell-month';
        if (monthNumber === currentDate.getMonth() && currentDate.getFullYear() === visibleDate.getFullYear()) {
            monthClassNames += ' current';
        }
        if (selectedDate && (
            selectedDate instanceof Date && monthNumber === selectedDate.getMonth() && selectedDate.getFullYear() === visibleDate.getFullYear()
            || Array.isArray(selectedDate) && selectedDate.some(d => monthNumber === d.getMonth() && d.getFullYear() === visibleDate.getFullYear())
        )) {
            monthClassNames += ' selected';
        }
        if (monthIsDisabled(monthNumber, visibleDate, minDate, maxDate)) {
            monthClassNames += ' disabled';
        }

        return monthClassNames;
    };

    return(
        <div className="date-picker-body">
            <div className="date-picker-cells">
                {months.map((monthName, monthNumber) => {
                    return (
                        <div
                            className={getClassNamesForMonthCell(monthNumber)}
                            onClick={!monthIsDisabled(monthNumber, visibleDate, minDate, maxDate) ? handleMonthClick(monthNumber) : null}
                            key={monthNumber}
                        >
                            {monthName}
                        </div>
                    )
                })}
            </div>
        </div>
    );
}

MonthsTable.propTypes = {
    visibleDate: PropTypes.instanceOf(Date).isRequired,
    currentDate: PropTypes.instanceOf(Date).isRequired,
    handleMonthClick: PropTypes.func.isRequired,
    selectedDate: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.arrayOf(PropTypes.instanceOf(Date)),
    ]),
    minDate: PropTypes.instanceOf(Date),
    maxDate: PropTypes.instanceOf(Date),
};

MonthsTable.defaultProps = {
    selectedDate: null,
    minDate: null,
    maxDate: null,
};

export default MonthsTable;
