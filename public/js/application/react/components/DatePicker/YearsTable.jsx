import React from 'react';
import PropTypes from 'prop-types';

function YearsTable ({ visibleDate, currentDate, selectedDate, handleYearClick }) {

    const getVisibleYears = () => {
        const yearsForRender = [];
        const startDate = new Date(visibleDate);
        if (startDate.getFullYear() % 12 === 0) {
            startDate.setFullYear(startDate.getFullYear() + 1);
        } else {
            startDate.setFullYear(startDate.getFullYear() - startDate.getFullYear() % 12);
        }
        do {
            yearsForRender.push(new Date(startDate));
            startDate.setFullYear(startDate.getFullYear() + 1);
        } while(startDate.getFullYear() % 12 !== 0);

        return yearsForRender;
    };

    const getClassNamesForYearCell = (date) => {
        let yearClassNames = 'date-picker-cell date-picker-cell-year';
        if (selectedDate && (
            selectedDate instanceof Date && selectedDate.getFullYear() === date.getFullYear()
            || Array.isArray(selectedDate) && selectedDate.some(d => d.getFullYear() === date.getFullYear())
        )) {
            yearClassNames += ' selected';
        }
        if (currentDate.getFullYear() === date.getFullYear()) {
            yearClassNames += ' current';
        }

        return yearClassNames
    };

    return (
        <div className="date-picker-body">
            <div className="date-picker-cells">
                {getVisibleYears().map((dateOfYear) => {
                    return(
                        <div
                            className={getClassNamesForYearCell(dateOfYear)}
                            key={dateOfYear.getFullYear()}
                            onClick={handleYearClick(dateOfYear.getFullYear())}
                        >
                            {dateOfYear.getFullYear()}
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

YearsTable.propTypes = {
    visibleDate: PropTypes.instanceOf(Date).isRequired,
    currentDate: PropTypes.instanceOf(Date).isRequired,
    handleYearClick: PropTypes.func.isRequired,
    selectedDate: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.arrayOf(PropTypes.instanceOf(Date)),
    ]),
};

export default YearsTable;
