import React from 'react';
import PropTypes from 'prop-types';
import { getMonthNameByNumber } from "./helpers";

function DateNavigation({ visibleDate, handleNavigationClick, currentView, handleNavTitleClick }) {
    const getNavigationTitle = () => {
        switch(currentView) {
            case 'days':
                return(
                    <div>
                        {getMonthNameByNumber(visibleDate.getMonth())}, <i>{visibleDate.getFullYear()}</i>
                    </div>
                );
            case 'months':
                return (
                    <div>
                        {visibleDate.getFullYear()}
                    </div>
                );
            case 'years':
                const startYear = visibleDate.getFullYear() - visibleDate.getFullYear() % 12;
                const endYear = startYear + 11;
                return (
                    <div>
                        {startYear} - {endYear}
                    </div>
                );
        }
    };

    return(
        <nav className="date-picker-nav">
            <div className="date-picker-nav__action" onClick={handleNavigationClick('prev')}>
                <svg>
                    <path d="M 17,12 l -5,5 l 5,5" />
                </svg>
            </div>
            <div
                className={`date-picker-nav__title ${currentView === 'years' ? ' disabled' : ''}`}
                onClick={handleNavTitleClick}
            >
                {getNavigationTitle()}
            </div>
            <div className="date-picker-nav__action" onClick={handleNavigationClick('next')}>
                <svg>
                    <path d="M 14,12 l 5,5 l -5,5" />
                </svg>
            </div>
        </nav>
    );
}

DateNavigation.propTypes = {
    visibleDate: PropTypes.instanceOf(Date).isRequired,
    handleNavigationClick: PropTypes.func.isRequired,
    handleNavTitleClick: PropTypes.func.isRequired,
    currentView: PropTypes.string.isRequired,
};

export default DateNavigation;
