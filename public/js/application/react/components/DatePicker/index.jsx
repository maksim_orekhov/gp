import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DaysTable from './DaysTable';
import MonthsTable from './MonthsTable';
import DateNavigation from './DateNavigation';
import YearsTable from './YearsTable';
import { dateIsSelected, DATES_ARRAY_PROPTYPES } from './helpers';

class DatePicker extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentDate: new Date(),
            selectedDate: props.selectedDate,
            visibleDate: props.selectedDate ? props.selectedDate : new Date(),
            currentView: props.currentView,
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        const { selectedDate, visibleDate, currentView } = this.state;
        return nextProps != nextProps || +nextState.selectedDate !== +selectedDate || +nextState.visibleDate !== +visibleDate || nextState.currentView !== currentView;
    }

    componentDidUpdate(prevProps, prevState) {
        const { selectedDate, visibleDate, currentView } = this.state;
        const { onSelectDate, onChangeYear, onChangeMonth, onChangeView } = this.props;
        if (+prevState.selectedDate !== +selectedDate) {
            onSelectDate(selectedDate);
        }
        if (prevState.visibleDate.getFullYear() !== visibleDate.getFullYear()) {
            onChangeYear({ selectedDate, visibleDate, currentView });
        }
        if (prevState.visibleDate.getMonth() !== visibleDate.getMonth()) {
            onChangeMonth({ selectedDate, visibleDate, currentView });
        }
        if (prevState.currentView !== currentView) {
            onChangeView({ selectedDate, visibleDate, currentView });
        }
    }

    setComponentRef = (node) => {
        this.datePickerRef = node;
    };

    handleDayClick = (date) => (ev) => {
        ev.preventDefault();
        const { selectedDate } = this.state;
        const { multipleDates } = this.props;
        const nextVisibleDate = new Date(date.getFullYear(), date.getMonth(), 1);
        // При multipleDates = true выбранные даты хранятся в массиве
        let nextSelectedDate = multipleDates
                                ? Array.isArray(selectedDate) ? [...selectedDate] : []
                                : selectedDate;
        if (dateIsSelected(date, selectedDate)) {
            nextSelectedDate = multipleDates
                ? nextSelectedDate.filter((selectedDate) => selectedDate.toString() !== date.toString())
                : null;
        } else {
            if (multipleDates) {
                nextSelectedDate.push(date)
            } else {
                nextSelectedDate = date;
            }
        }
        setTimeout(() => {
            this.setState({
                selectedDate: nextSelectedDate,
                visibleDate: nextVisibleDate,
            });
        }, 0)
    };

    handleMonthClick = (monthNumber) => () => {
        const { visibleDate } = this.state;
        const nextVisibleDate = new Date(visibleDate);
        nextVisibleDate.setMonth(monthNumber);
        setTimeout(() => {
            this.setState({
                visibleDate: nextVisibleDate,
                currentView: 'days',
            });
        }, 0)
    };

    handleYearClick = (year) => () => {
        const { visibleDate } = this.state;
        const nextVisibleDate = new Date(visibleDate);
        nextVisibleDate.setFullYear(year);
        setTimeout(() => {
            this.setState({
                visibleDate: nextVisibleDate,
                currentView: 'months',
            });
        }, 0)
    };

    handleNavigationClick = (direction = 'next') => () => {
        const { currentView, visibleDate } = this.state;
        const nextVisibleDate = new Date(visibleDate);
        if (currentView === 'days') {
            nextVisibleDate.setMonth(direction === 'next' ? nextVisibleDate.getMonth() + 1 : nextVisibleDate.getMonth() - 1);
        }
        if (currentView === 'months') {
            nextVisibleDate.setFullYear(direction === 'next' ? nextVisibleDate.getFullYear() + 1 : nextVisibleDate.getFullYear() - 1);
        }
        if (currentView === 'years') {
            nextVisibleDate.setFullYear(direction === 'next' ? nextVisibleDate.getFullYear() + 11 : nextVisibleDate.getFullYear() - 11);
        }

        this.setState({ visibleDate: nextVisibleDate });
    };

    handleNavTitleClick = () => {
        const { currentView } = this.state;
        if (currentView === 'years') {
            return false;
        }
        if (currentView === 'days') {
            return this.setState({ currentView: 'months' });
        }
        this.setState({ currentView: 'years' });
    };

    renderCurrentTable = () => {
        const { currentDate, selectedDate, visibleDate, currentView } = this.state;
        const { disabledDates, disabledDays, minDate, maxDate } = this.props;
        return {
            days:
                <DaysTable
                    currentDate={currentDate}
                    visibleDate={visibleDate}
                    selectedDate={selectedDate}
                    handleDayClick={this.handleDayClick}
                    disabledDates={disabledDates}
                    disabledDays={disabledDays}
                    minDate={minDate}
                    maxDate={maxDate}
                />
            ,
            months:
                <MonthsTable
                    currentDate={currentDate}
                    visibleDate={visibleDate}
                    selectedDate={selectedDate}
                    handleMonthClick={this.handleMonthClick}
                    minDate={minDate}
                    maxDate={maxDate}
                />
            ,
            years:
                <YearsTable
                    currentDate={currentDate}
                    visibleDate={visibleDate}
                    selectedDate={selectedDate}
                    handleYearClick={this.handleYearClick}
                />
        }[currentView] || null;
    };

    render() {
        const { visibleDate, currentView } = this.state;
        return(
            <div className="date-picker-component" ref={this.setComponentRef}>
                <DateNavigation
                    visibleDate={visibleDate}
                    currentView={currentView}
                    handleNavigationClick={this.handleNavigationClick}
                    handleNavTitleClick={this.handleNavTitleClick}
                />
                <div className="date-picker-content">
                    {this.renderCurrentTable()}
                </div>
            </div>
        );
    }
}

DatePicker.propTypes = {
    onSelectDate: PropTypes.func,
    onChangeYear: PropTypes.func,
    onChangeMonth: PropTypes.func,
    onChangeView: PropTypes.func,
    multipleDates: PropTypes.bool,
    selectedDate: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.instanceOf(Date)),
        PropTypes.instanceOf(Date)
    ]),
    disabledDates: PropTypes.arrayOf(DATES_ARRAY_PROPTYPES),
    disabledDays: PropTypes.arrayOf(PropTypes.number),
    currentView: PropTypes.oneOf(['days', 'months', 'years']),
    minDate: PropTypes.instanceOf(Date),
    maxDate: PropTypes.instanceOf(Date),
};

DatePicker.defaultProps = {
    onSelectDate: () => {},
    onChangeYear: () => {},
    onChangeMonth: () => {},
    onChangeView: () => {},
    multipleDates: false,
    selectedDate: null,
    currentView: 'days',
    minDate: null,
    maxDate: null,
    disabledDates: [],
    disabledDays: [],
};


export default DatePicker;
