import React from 'react';
import SelfDelivery from './Dpd/DpdSelfDelivery';
import Popup from '../Popup';
import { getDpdPointAddress, getPickPointNameByDealRole } from '../Helpers';

export default class DpdSelfDeliveryPopup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            is_show_popup: false,
            is_preloader: false,
        };

        this.setPopup = this.setPopup.bind(this);
        this.setPreloader = this.setPreloader.bind(this);
        this.handleFinalPointChange = this.handleFinalPointChange.bind(this);
        this.handleFinalPoint = this.handleFinalPoint.bind(this);
    }

    componentDidMount() {
        const code = this.props.address.terminal_code;

        code && this.componentValidate(code);
    }

    componentWillReceiveProps(nextProps) {
        const next_address = nextProps.address || {};
        const current_address = this.props.address;

        if (next_address.terminal_code !== current_address.terminal_code) {
            this.componentValidate(next_address.terminal_code);
        }
    }

    setPopup(trueOrFalse) {
        this.setState({
           is_show_popup: trueOrFalse
        });
    }

    setPreloader(trueOrFalse) {
        this.setState({
            is_preloader: trueOrFalse
        });
    }

    handleFinalPoint(point, city_id) {
        const { handleChangePoint, handleComponentChange } = this.props;
        // const terminal_code = point.code;
        const { code, street, house } = point;
        const terminal_props = {
            terminal_code: code,
            street: street,
            house: house,
            city_id: city_id
        };

        this.setPopup(false);

        if (city_id) {
            handleChangePoint && handleChangePoint('terminal_props', terminal_props, point);
        } else {
            handleComponentChange && handleComponentChange('selected_point', point);
        }

        this.componentValidate(code);
    }

    handleFinalPointChange() {
        this.setPopup(true);
    }

    componentValidate(terminal_code = this.props.address.terminal_code) {
        const { handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;

        handleComponentValidation && handleComponentValidation(validate_prop, !!terminal_code);
    }

    renderFinalPoint() {
        const { deal_role, address } = this.props;
        const { terminal_code, selected_point } = address;

        if (terminal_code && Object.values(selected_point).length) {
            return (
                <div className="row nested-row">
                    <div className="col-xl-9 col-md-6">
                        <div className="Input-Wrapper">
                            <div className="delivery-point">
                                <div className="delivery-point__head">
                                    <label>Пункт {getPickPointNameByDealRole(deal_role)}:</label>
                                    <button className="delivery-point__change-btn" onClick={this.handleFinalPointChange}>Изменить</button>
                                </div>
                                <div className="search__item current delivery-point__body">
                                    <h3 className="delivery-point__name">{ selected_point.name }</h3>
                                    <div className="delivery-point__address">{ getDpdPointAddress(selected_point) }</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        return null;
    }

    render() {
        const { is_show_popup, is_preloader } = this.state;
        const { deal_role, csrf, address, handleComponentValidation, handleComponentChange, handleChangeCity } = this.props;
        const { terminal_code, selected_point = {} } = address;

        return (
            <div className="row nested-row">
                <div className={`col-xl-12 ${is_preloader ? 'Preloader_center Preloader Preloader_solid' : '' }`}>
                    {
                        terminal_code ?
                            Object.values(selected_point).length ?
                                this.renderFinalPoint()
                                :
                                undefined
                            :
                        <div className="Input-Wrapper">
                            <button className="ButtonApply" onClick={() => this.setPopup(true)}>Выбрать пункт</button>
                        </div>
                    }
                    <div style={{visibility: is_show_popup ? 'visible' : 'hidden'}} className="map-popup">
                        <Popup close={() => this.setPopup(false)} ref={popup => this.popup = popup} not_open_on_mount={true}>
                            <SelfDelivery
                                deal_role={deal_role}
                                csrf={csrf}
                                address={address}
                                handleFinalPoint={this.handleFinalPoint}
                                handleChangeCity={handleChangeCity}
                                handleComponentChange={handleComponentChange}
                                handleComponentValidation={handleComponentValidation}
                            />
                        </Popup>
                    </div>
                </div>
            </div>
        );
    }
};