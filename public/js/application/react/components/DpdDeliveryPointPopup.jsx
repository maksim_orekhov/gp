import React from 'react';
import DpdDeliveryPoint from './Dpd/DpdDeliveryPoint';
import Popup from '../Popup';
import {getDpdPointAddress, customFetch, getIdFromUrl} from '../Helpers';
import URLS from "../constants/urls";

export default class DpdDeliveryPointPopup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            is_show_popup: false
        };

        this.getCsrf = this.getCsrf.bind(this);
    }

    componentWillMount() {
        this.getCsrf();
    }

    getCsrf() {
        return customFetch(URLS.CSRF.GET)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const { csrf } = data.data;
                    this.setState({
                        csrf
                    });
                } else if (data.status === 'ERROR') {
                    return Promise.reject(data.message);
                }
            })
            .catch(error => console.log('---', error));
    }

    setPopup = (trueOrFalse) => () => {
        this.setState({
           is_show_popup: trueOrFalse
        });

    };

    render() {
        const { is_show_popup, terminal, csrf } = this.state;

        return (
            <div className="row nested-row">
                <div className="col-xl-12">
                    <span className="show-map" id="reception-point-map" onClick={this.setPopup(true)}>На карте</span>
                    <div style={{visibility: is_show_popup ? 'visible' : 'hidden'}} className="map-popup">
                        <Popup close={this.setPopup(false)} ref={popup => this.popup = popup} not_open_on_mount={true}>
                            <DpdDeliveryPoint
                                terminal={terminal}
                                csrf={csrf}
                            />
                        </Popup>
                    </div>
                </div>
            </div>
        );
    }
};