import React from 'react';

let can_init = true;

export default class YandexMap extends React.PureComponent {
    constructor(props) {
      super(props);

      this.initMap = this.initMap.bind(this);

      this.YMap = null;
    }

    componentWillMount() {
        if (can_init) {
            can_init = false;
            this.connectYandexMapScript();
        } else {
            this.initMap();
        }
    }

    // Подключаем карту
    connectYandexMapScript() {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
        script.onload = this.initMap; // Сработает как только скрипт загрузится

        document.getElementsByTagName('head')[0].appendChild(script);
    }

    // Инициализируем карту
    initMap() {
        const { handleReady } = this.props;
        const init = () => {
            // Создание карты.
            this.YMap = new ymaps.Map("map", {
                center: [55.753215, 37.622504],
                zoom: 10,
                controls: ['zoomControl'],
                ...this.props.init_options
            });
        };
        // Функция ymaps.ready() будет вызвана, когда
        // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
        ymaps.ready(init)
            .then(() => {
                handleReady && handleReady(); // Сообщаем наверх что яндекс карта загрузилась
            })
            .catch((error) => {
                console.error(error);
            });
    }

    /**
     * Прокручиваем карту к нужной точке
     * @param {Array} coordinates - координаты точки
     * @param {number} time - время прокрутки
     * @param {number} zoom - увелечение карты
     */
    mapMoveTo(coordinates, time = 2000, zoom) {
        this.YMap.panTo(coordinates, {duration: time})
            .then(() => {
                if (zoom) {
                    this.YMap.setZoom(zoom, {duration: 500});
                }
            });
    }

    mapMoveToObject(object, time = 1000) {
        const coordinates = this.getObjectCoordinates(object);
        coordinates.forEach((coordinate, index) => coordinates[index] = +coordinate); // делаем все координаты цифрами для корректного перехода карты
        this.mapMoveTo(coordinates, time, 13);
    }

    /**
     * https://tech.yandex.ru/maps/doc/jsapi/2.1/dg/concepts/geocoding-docpage/
     * @param value - находит геообъект на карте
     * @return {Promise}
     */
    findGeoObject(value) {
        if (value) {
            return new Promise((resolve, reject) => {
                const myGeocoder = ymaps.geocode(value);

                myGeocoder.then(
                    (res) => {
                        const nearest = res.geoObjects.get(0);
                        const coordinates = nearest.geometry.getCoordinates();
                        const geoObjectProps = {
                            coordinates
                        };

                        resolve(geoObjectProps);
                    },
                    (err) => {
                        reject(err);
                    });
            });
        }
    }

    addGeoObjectToMap(geoObject) {
        this.YMap.geoObjects.add(geoObject);
    }

    /**
     * Добавляет к карте коллекцию геообъектов и ставит карту так чтобы захватить все точки
     * @param {Object} collection - yandex map коллекция с точками
     */
    addCollectionToMap(collection) {
        this.addGeoObjectToMap(collection);
        this.catchCollectionInMap(collection);
    }

    // https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/GeoObjectCollection-docpage/
    createCollection() {
        return new ymaps.GeoObjectCollection();
    }

    clearCollection(collection) {
        collection.removeAll();
    }

    setObjectCoordinates(object, coordinates) {
        object.geometry.setCoordinates(coordinates);
    }

    getObjectCoordinates(object) {
        return object.geometry.getCoordinates();
    }

    /**
     * @param object - геообъект
     * @param {Object} options
     */
    setObjectOptions(object, options) {
        object.options.set(options);
    }

    /**
     * @param object - геообъект
     * @param {string} option_name
     */
    getObjectOptions(object, option_name) {
        return object.options.get(option_name);
    }

    /**
     * @param object - геообъект
     * @param {Object} properties
     */
    setObjectProperties(object, properties) {
        object.properties.set(properties);
    }

    /**
     * @param object - геообъект
     * @param {string} property_name
     */
    getObjectProperties(object, property_name) {
        return object.properties.get(property_name);
    }

    collectionEach(collection, searchFunction) {
        collection.each(searchFunction);
    }

    /**
     * https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/Placemark-docpage/
     * @param {Array} coordinates - координаты точки
     * @param {Object} properties - свойста метки, сюда можно записывать свои свойста и потом их читать
     * @param {Object} options - опции метки, читать YANDEX MAP API
     * @param {Function} clickFunction - Функция коллбэка, которая срабатывает при клике на метку
     * @return {Object} - возвращает объект метки yandex карт
     */
    createPlacemark(coordinates, properties = {}, options = {}, clickFunction = null) {
        const placemark = new ymaps.Placemark(coordinates, properties, options);

        if (typeof clickFunction === 'function') {
            placemark.events.add('click', clickFunction);
        }

        return placemark;
    }

    // Ставит отдалениее карты так чтобы захватить все точки
    catchCollectionInMap(collection) {
        this.YMap.setBounds(collection.getBounds());
    }

    removeObject(object) {
        this.YMap.geoObjects.remove(object);
    }

    render() {
        return (
            <div className="yandex-map">
                <div id="map" style={{width: '100%', height: '100%'}}/>
            </div>
        );
    }
};