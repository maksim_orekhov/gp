import React from 'react';
import { DELIVERY_TYPES_NAMES } from '../constants/names';

const renderHint = (deal_role, service_name) => {
    const { NO_SERVICE, DPD } = DELIVERY_TYPES_NAMES;

    const no_service_text =
        <div className="delivery-payer-hint">
            <p>Опишите ваши договоренности об условиях доставки: за счет кого осуществляется доставка, название транспортной компании, условия транспортировки (хрупкий товар и тд.).</p>
        </div>;

    const hints = {
        customer: {
            [NO_SERVICE]: no_service_text,
            [DPD]: <div className="delivery-payer-hint">
                <p><strong>Доставку оплачивает продавец при отправке товара.</strong></p>
                <p>Вам необходимо указать контактные данные и адрес доставки.</p>
            </div>
        },
        contractor: {
            [NO_SERVICE]: no_service_text,
            [DPD]: <div className="delivery-payer-hint">
                <p><strong>Вы оплачиваете доставку.</strong></p>
                <p>Вам необходимо указать пункт приема, контактные данные и вес груза.</p>
            </div>
        }
    };

    if (!hints[deal_role] || !hints[deal_role][service_name]) return null;

    return (
        <div className="InfoField">
            {hints[deal_role][service_name]}
        </div>
    );
};

const DeliveryServiceTypeHints = (props) => {
    const { deal_role, current_service_type_name } = props;

    return renderHint(deal_role, current_service_type_name);
};

export default DeliveryServiceTypeHints;