import React from 'react';
import PropTypes from 'prop-types';

const FormDealWrapper = (props) => {
    const { is_loading, date_of_create, children, mobile_title, is_partner_form, logo } = props;

    return (
        <div className={is_loading ? 'Preloader Preloader_solid' : 'FormLabelTop form-deal'}>
            <div className={`head-of-element ${is_partner_form ? 'head-of-element_partners' : ''}`}>
                {mobile_title}
            </div>
            <div className={`header ${is_partner_form ? 'header_partners' : ''}`}>
                <div className="row">
                    <div className="col-xl-9 col-sm-4">
                        {
                            is_partner_form ?
                                logo
                                :
                                <h3 className="TitleBlue form-header-title">{ props.title }</h3>
                        }
                    </div>
                    <div className="col-xl-3">
                        <div className="date-create">
                            <h3>Дата создания</h3>
                            <p>{date_of_create}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="form-content-wrap">
                {children}
            </div>
        </div>
    );
};

FormDealWrapper.propTypes = {
    is_loading: PropTypes.bool.isRequired,
    logo: PropTypes.element,
    date_of_create: PropTypes.string.isRequired,
    mobile_title: PropTypes.string,
    isPartnerForm: PropTypes.bool,
    title: PropTypes.string
};

FormDealWrapper.defaultProps = {
    mobile_title: "Создание сделки",
    isPartnerForm: false
};

export default FormDealWrapper;