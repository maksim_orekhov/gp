import React from 'react';

const DisplayNone = (props) => {
    return (
        <div style={{display: 'none'}}>
            {props.children}
        </div>
    );
};

export default DisplayNone;