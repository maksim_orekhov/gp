import React from 'react';
import { customFetch } from '../../Helpers';
import URLS from '../../constants/urls';


export default class DpdDeliveryCost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cost: 0,
            days: null,
            is_loading: false
        };

        this.is_ajax_waiting = false; // флаг того что идет запрос на бэк и нужно ждать ответа
        this.values_when_ajax_sending = {}; // Значение инпута на момент отправки ajax, если оно не совпадает со state делает повторный запрос
    }

    componentWillMount() {
        const { sender_city_id, receiver_city_id, self_delivery, self_pickup, weight } = this.props;

        if (sender_city_id
            && receiver_city_id
            && (self_delivery === '0' || self_delivery === '1')
            && (self_pickup === '0' || self_pickup === '1')
            && weight
        ) {
            this.getCost();
        }
    }

    componentDidUpdate(prevProps) {
        const { sender_city_id, receiver_city_id, self_delivery, self_pickup, weight } = this.props;

        if (sender_city_id
            && receiver_city_id
            && (self_delivery === '0' || self_delivery === '1')
            && (self_pickup === '0' || self_pickup === '1')
            && weight
            && (
                receiver_city_id !== prevProps.receiver_city_id
                || sender_city_id !== prevProps.sender_city_id
                || self_delivery !== prevProps.self_delivery
                || self_pickup !== prevProps.self_pickup
                || weight !== prevProps.weight
            )
        ) {
            this.getCost();
        }
    }

    getDataForAjax() {
        const { sender_city_id, receiver_city_id, self_delivery, self_pickup, weight, csrf } = this.props;

        return {
            sender_city_id,
            receiver_city_id,
            self_pickup,
            self_delivery,
            weight,
            csrf
        }
    }

    setPreloader(trueOrFalse) {
        this.setState({
            is_loading: trueOrFalse
        });
    }

    compareDataBeforeAndAfterRequest() {
        const { sender_city_id, receiver_city_id, self_delivery, self_pickup, weight } = this.props;

        return (
            sender_city_id
            && receiver_city_id
            && (self_delivery === '0' || self_delivery === '1')
            && (self_pickup === '0' || self_pickup === '1')
            && weight
            && (
                receiver_city_id !== this.values_when_ajax_sending.receiver_city_id
                || sender_city_id !== this.values_when_ajax_sending.sender_city_id
                || self_delivery !== this.values_when_ajax_sending.self_delivery
                || self_pickup !== this.values_when_ajax_sending.self_pickup
                || weight !== this.values_when_ajax_sending.weight
            )
        );
    }

    getCost() {

        if (this.is_ajax_waiting === true) return;

        this.setPreloader(true);

        const form_data = this.getDataForAjax();

        this.is_ajax_waiting = true; // Флаг что запрос идет и новые делать не нужно

        // Данные в момент запроса, чтобы по окончанию сравнить изменились ли данные для определения цены, во время запроса
        // Чтобы если они изменились сделать новый запрос
        this.values_when_ajax_sending = form_data;

        console.log('start', this.is_ajax_waiting, this.values_when_ajax_sending);
        customFetch(URLS.DPD.DELIVERY_COST ,{
            method: 'POST',
            body: JSON.stringify(form_data)
        })
            .then((data) => {
                this.setPreloader(false);
                this.is_ajax_waiting = false;
                if (data.status === 'SUCCESS') {
                    const { cost, days } = data.data;
                    console.log('finish', this.values_when_ajax_sending);
                    this.setState({
                        cost,
                        days
                    });
                    if (this.compareDataBeforeAndAfterRequest()) {
                        this.getCost();
                    }
                }
            })
            .catch((error) => {
                console.error(error);
                this.setPreloader(false);
                this.is_ajax_waiting = false;
            })
    }

    render() {
        const { days, cost, is_loading } = this.state;

        if (days || cost) {
            return (
                <div className={`delivery-payer-hint ${is_loading ? 'Preloader Preloader-delivery-cost' : ''}`}>
                    <p><strong>Предварительная стоимость доставки:</strong> {cost ? `${cost} руб.` : 'неизвестна'}</p>
                    <p><strong>Срок доставки:</strong> {days ? `${days} дней` : 'неизвестно'}</p>
                    <p>Стоимость доставки может быть изменена, если фактические габариты груза при измерении будут отличаться от указанных в заказе.</p>
                </div>
            )
        }
        return null;
        return (
            <div className={`${is_loading ? 'Preloader' : ''}`}>
                Рекомендуем вам заполнить данные о доставке, чтобы сразу расчитать предварительную стоимость и сроки доставки.
            </div>
        );
    }
}