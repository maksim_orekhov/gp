import React from 'react';
import { getDpdPointAddress, getPickPointShedule, getPickPointNameByDealRole } from '../../Helpers';
import PickPointDetails from '../../components/PickPointDetails';


export default class ControlDpdPointSelection extends React.Component {
    constructor(props) {
        super(props);

        this.handlePoint = this.handlePoint.bind(this);
        this.handleChoice = this.handleChoice.bind(this);
    }

    state = {
        pickPointInfoIsOpen: false,
        activePickPoint: null
    };

    scrollToActiveItem() {
        const active_item = $(this.current_item);
        if (active_item.length) {
            const container = $(this.list);
            const container_height = container.height();

            const scroll_pos_before_scroll = container.get(0).scrollTop; // Получаем текущий размер прокрутки

            container.scrollTop(0); // Обнуляем его

            const top_pos = Math.abs(active_item.position().top); // Смотрим позицию активного элемента

            container.scrollTop(scroll_pos_before_scroll); // Возвращаем позицию скрола

            container.animate({ // Анимируем скрол к след элементу
                scrollTop: top_pos - (container_height - 67) / 2 // @TODO: 67 сейчас магическое число, это высота активного блока, сделать ее автоматическое высчитывание
            }, 300);
        }
    }

    componentDidUpdate(prevProps) {
        const value_prop = this.props.value_prop;

        if (value_prop && value_prop !== prevProps.value_prop) {
            this.scrollToActiveItem();
            if (this.props.points[value_prop]) {
                this.setState({
                    pickPointInfoIsOpen: true,
                    activePickPoint: this.props.points[value_prop]
                });
            }
        }
    }

    handlePoint(e) {
        const { handlePoint } = this.props;
        const value = e.currentTarget.dataset.value;

        handlePoint && handlePoint(value);
    }

    handleChoice(e) {
        const { handleFinalChoice, closePopup } = this.props;
        const value = e.currentTarget.dataset.value;

        handleFinalChoice && handleFinalChoice(value);
        this.setState({ pickPointInfoIsOpen: false, activePickPoint: null });
        closePopup && closePopup();
    }

    static getScheduleString(day) {
        return day ? `${day['week_day']}: ${day['work_time']}` : '';
    }

    static getTodaySchedule(schedule) {
        let current_day_week = new Date().getDay() - 1;
        if (current_day_week < 0) {
            current_day_week = 6;
        }

        return ControlDpdPointSelection.getScheduleString(schedule[current_day_week]);
    }

    getSchedule(point) {
        const deal_role = this.props.deal_role;

        let schedule_array = getPickPointShedule(point, deal_role);

        if (point['schedules']) {

            if (schedule_array) {
                return (
                    <div>
                        <p>{ControlDpdPointSelection.getTodaySchedule(schedule_array)}</p>
                    </div>
                )
            }
            return null;
        }
    }

    closePickPointDetails = () => {
        this.setState({ pickPointInfoIsOpen: false, activePickPoint: null });
    };

    openPickPointDetails = pickPoint => {
        this.setState({
            pickPointInfoIsOpen: true ,
            activePickPoint: pickPoint
        });
    };

    render() {
        const { points, value_prop, deal_role } = this.props;
        const { activePickPoint, pickPointInfoIsOpen } = this.state;
        return (
          <div className={`point-search ${pickPointInfoIsOpen ? 'pick-point-is-open' : ''}`}>
              {
                  pickPointInfoIsOpen &&
                  <PickPointDetails
                      pickPoint={activePickPoint}
                      closeDetails={this.closePickPointDetails}
                      dealRole={deal_role}
                      choiceHandler={this.handleChoice}
                  />
              }
              <div className="point-search__head">
                  {
                      Object.values(points).length ?
                        <h2>Пункты {getPickPointNameByDealRole(deal_role)} ({Object.values(points).length}):</h2>
                        :
                        <h2>Пунктов приема/выдачи не найдено. Выберите ближайший пункт приема/выдачи в поиске или воспользуетесь доставкой до двери.</h2>
                  }

              </div>
              <div className="point-search__body" ref={list => this.list = list}>
                  {
                      Object.values(points).map(point => {
                          const code = point.code;

                          if (value_prop !== code) {
                              return (
                                  <div onClick={ev => {
                                      this.handlePoint(ev);
                                      this.openPickPointDetails(point)
                                  }} key={code} data-value={code} className="search__item">
                                      <h3>{ point.name }</h3>
                                      <h4>{ getDpdPointAddress(point) }</h4>
                                      { this.getSchedule(point) }
                                  </div>
                              )
                          } else {
                              return (
                                  <div key={code}
                                       className="search__item current"
                                       ref={current_item => this.current_item = current_item}
                                       onClick={() => this.openPickPointDetails(point)}
                                  >
                                      <h3>{ point.name }</h3>
                                      <h4>{ getDpdPointAddress(point) }</h4>
                                      { this.getSchedule(point) }
                                  </div>
                              )
                          }
                      })
                  }
              </div>
          </div>
        );
    }
}