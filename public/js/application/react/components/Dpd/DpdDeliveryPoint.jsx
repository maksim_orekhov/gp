import React from 'react';
import Map from '../YandexMap';
import DeliveryPointDetails from '../../components/DeliveryPointDetails';
import { customFetch, getDpdPointAddress, getIdFromUrl } from '../../Helpers';
import URLS from '../../constants/urls';


export default class DpdDeliveryPoint extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            is_preloader: false,
            terminal: {},
            latitude: '',
            longitude: ''
        };
        
        this.default_map_icon_style = 'islands#blueCircleDotIcon';

        this.init = this.init.bind(this);
    }

    // Вместо вызова инициализации в componentWillMount, метод this.init запускается после инициализации карты коллбэком, там же выключаем прелоадер
    componentWillMount() {
        this.setPreloader(true);
    }

    init() { // Вызывается коллбэком после загрузки карты
        const { region_name, city_name } = this.state.terminal;
        const deal_id = getIdFromUrl();
        let default_url = `${URLS.DEAL.FORM_INIT}?idDeal=${deal_id}`;
        customFetch(default_url)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    this.setPreloader(false);
                    const { terminal } = data.data.deal.delivery.service.sender_address;
                    this.setState({
                        terminal,
                        latitude: terminal.latitude,
                        longitude: terminal.longitude
                    })
                } else if (data.status === 'ERROR') {
                    return Promise.reject(data.message);
                }
            })
            .then(() => {
                this.findCityInMap(`${region_name}, ${city_name}`);
                this.createPointsMapCollection();
                this.setCurrentPointInMap();
            })
            .then(() => {
                this.setCurrentPointInMap();
            })
            .catch(error => console.log('---', error));

    }

    setPreloader(trueOrFalse) {
        this.setState({
            is_preloader: trueOrFalse
        });
    }

    setCurrentPointInMap() {
        const { latitude, longitude } = this.state;
        const coordinates = [+latitude, +longitude];
        this.map.mapMoveTo(coordinates, 1000, 13);
    }

    // Ставим на карту метки
    createPointsMapCollection(points = this.state.terminal) {
        if (Object.values(points).length) {
            this.map.points_collection = this.map.createCollection();
            const collection = this.map.points_collection; // Делаем присвоение чтобы не писать постоянно this.map.points_collection;

            const { code, latitude, longitude } = points;
            const point_name = getDpdPointAddress(points);
            const placemark = this.map.createPlacemark([latitude, longitude],
                {
                    code,
                    hintContent: point_name
                }
            );

            collection.add(placemark);

            this.map.setObjectOptions(collection, {
                preset: this.default_map_icon_style
            });

            this.map.addCollectionToMap(collection);
        }
    }

    findCityInMap(name, time = 0) {
        this.map.findGeoObject(name)
            .then(geoObject => {
                const { coordinates } = geoObject;
                this.map.mapMoveTo(coordinates, time);
                this.setPreloader(false);
            });
    }

    render() {
        const { is_preloader, terminal } = this.state;
        return(
            <div className="row">
                <div className={`dpd-postamats ${is_preloader ? 'Preloader Preloader_solid' : ''}`}>
                    <div className="map-selection">
                        <div className="map-control">
                            <div className="map-control__map">
                                <Map ref={map => this.map = map} handleReady={this.init}/>
                            </div>
                        </div>
                        <div className={`point-search`}>
                            <div className="point-search__head">
                                <h2>Пункт приема:</h2>
                            </div>
                            <DeliveryPointDetails
                                pickPoint={terminal}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};