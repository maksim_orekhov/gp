import React from 'react';
import Map from '../YandexMap';
import PointSelection from './ControlDpdPointSelection';
import CitySearch from '../../controls/ControlDpdCitySearch';
import { customFetch, filterDpdPointsByDealRole, getDpdPointAddress } from '../../Helpers';
import URLS from '../../constants/urls';


export default class DpdSelfDelivery extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selected_point: '',
            is_preloader: false,
            points: {},
            filtered_points: {},
            city_info: {},
        };
        
        this.default_map_icon_style = 'islands#blueCircleDotIcon';
        this.active_point_icon_style = 'islands#redCircleDotIcon';

        this.handlePoint = this.handlePoint.bind(this);
        this.init = this.init.bind(this);
        this.handlePointFinalChoice = this.handlePointFinalChoice.bind(this);
    }

    // Вместо вызова инициализации в componentWillMount, метод this.init запускается после инициализации карты коллбэком, там же выключаем прелоадер
    componentWillMount() {
        this.setPreloader(true);
    }

    componentWillReceiveProps(nextProps) {
        const { address } = nextProps;
        const { city_id } = address;

        if (this.props.address && city_id !== this.props.address.city_id) {
            this.updatePoints(city_id);

            this.setState({
                selected_point: ''
            });
        }
    }

    componentDidUpdate(prevProps) {
        const deal_role = prevProps.deal_role;

        if (deal_role !== this.props.deal_role) {
            this.filterPointsByDealRole()
                .then((is_available) => {
                    if (!is_available) return;

                    this.clearPrevCityData();
                    this.createPointsMapCollection();
                });

            this.setState({
                selected_point: ''
            });
        }
    }

    init() { // Вызывается коллбэком после загрузки карты
        const { city_id, terminal_code } = this.props.address;

        let default_url; // Если города нет то ставим москву по дефолту
        if (!city_id) {
            default_url = `${URLS.DPD.FORM_INIT}?csrf=${this.props.csrf}`;
        }

        this.updatePoints(city_id, default_url);
    }

    updatePoints(city_id, default_url) {
        this.findPointsAjax(city_id, default_url)
            .then(() => this.filterPointsByDealRole())
            .then((is_available) => {
                if (!is_available) return;
                this.clearPrevCityData();
                this.findCityInMap(`${this.state.city_info['region_name']}, ${this.state.city_info['city_name']}`);
                this.createPointsMapCollection();
            });
    }

    // Делаем запрос на бэк для поиска точек в текущем городе
    findPointsAjax(city_id = this.props.address.city_id, url = `${URLS.DPD.CITY}/${city_id}`) {
        this.setPreloader(true);
        return customFetch(url)
            .then((data) => {
                if (data.status === 'SUCCESS') {
                    const { dpd_points, city } = data.data;

                    this.setState({
                        city_info: city || {},
                        points: dpd_points || {}
                    }, () => {
                        this.setPreloader(false);
                        return Promise.resolve();
                    });
                } else {
                    return Promise.reject(data);
                }
            })
            .catch(error => {
                console.error(error);
                this.setPreloader(false);
            });
    }

    filterPointsByDealRole(points = this.state.points) {
        return new Promise((resolve) => {
            const deal_role = this.props.deal_role;
            const filtered_points = {};

            Object.values(points).forEach((point) => {
                if (filterDpdPointsByDealRole(point, deal_role)) {
                    filtered_points[point.code] = point;
                }
            });

            this.setState({
                filtered_points
            }, () => {
                const is_available = this.checkAvailabilitySelfDelivery();
                resolve(is_available);
            });
        });

    }

    checkAvailabilitySelfDelivery() {
        const filtered_points = this.state.filtered_points;
        return !!Object.values(filtered_points).length;
    }

    setPreloader(trueOrFalse) {
        this.setState({
            is_preloader: trueOrFalse
        });
    }

    handleSelectPointFromMap(code) {
        if (code !== this.state.selected_point) {
            this.setState({
                selected_point: code
            });
        }
    }

    // Функция срабатывающая при нажатии на метку
    handlePointClick(e) {
        const current_placemark = e.get('target');

        this.changePointColorWhenSelected(current_placemark);

        const selected_point_code = this.map.getObjectProperties(current_placemark, 'code');
        this.handleSelectPointFromMap(selected_point_code);

        this.map.mapMoveToObject(current_placemark);
    }

    setCurrentPointInMap() {
        const { selected_point } = this.state;
        const collection = this.map.points_collection;

        if (collection && collection.getLength() && selected_point) {
            let result;

            this.map.collectionEach(collection, (item) => {
                const code = this.map.getObjectProperties(item, 'code');

                if (code === selected_point) {
                    result = item;
                }
            });
            this.changePointColorWhenSelected(result);

            this.map.mapMoveToObject(result);
        }
    }

    changePointColorWhenSelected(placemark) {
        this.map.collectionEach(this.map.points_collection, (geoObject) => {
            const color = this.map.getObjectOptions(geoObject, 'preset');

            if (color === this.active_point_icon_style) {
                this.map.setObjectOptions(geoObject, {
                    preset: this.default_map_icon_style
                });
            }
        });

        this.map.setObjectOptions(placemark, {
            preset: this.active_point_icon_style
        });
    }

    // Ставим на карту метки
    createPointsMapCollection(points = this.state.filtered_points) {
        if (Object.values(points).length) {
            this.map.points_collection = this.map.createCollection();
            const collection = this.map.points_collection; // Делаем присвоение чтобы не писать постоянно this.map.points_collection;

            Object.values(points).forEach(point => {
                const { code, latitude, longitude } = point;
                const point_name = getDpdPointAddress(point);
                const placemark = this.map.createPlacemark([latitude, longitude],
                    {
                        code,
                        hintContent: point_name
                    }
                );

                collection.add(placemark);
            });

            this.map.setObjectOptions(collection, {
                preset: this.default_map_icon_style
            });

            collection.events.add('click', (e) => {
                this.handlePointClick(e);
            });

            this.map.addCollectionToMap(collection);
        }
    }

    findCityInMap(name, time = 0) {
        this.map.findGeoObject(name)
            .then(geoObject => {
                const { coordinates } = geoObject;
                this.map.mapMoveTo(coordinates, time);
                this.setPreloader(false);
            });
    }

    clearPrevCityData() {
        this.map.points_collection && this.map.clearCollection(this.map.points_collection); // Чистим коллекцию поинтов
    }

    handlePoint(value) {
        if (value !== this.state.selected_point) {
            this.setState({
                selected_point: value
            }, () => {
                this.setCurrentPointInMap();
            });
        }
    }

    handlePointFinalChoice(final_point_code) {
        const { handleFinalPoint } = this.props;

        handleFinalPoint && handleFinalPoint(this.state.points[final_point_code], this.state.city_info['city_id']);
    }

    render() {
        const { selected_point, is_preloader, filtered_points } = this.state;
        const { deal_role, closePopup, csrf, address, handleComponentChange, handleChangeCity } = this.props;
        return(
            <div className="row">
                <div className={`dpd-postamats ${is_preloader ? 'Preloader Preloader_solid' : ''}`}>
                    <div className="map-selection">
                        <div className="map-control">
                            <div className="row nested-row">
                                <CitySearch
                                    csrf={csrf}
                                    value_prop={address.city_id}
                                    name="city_id"
                                    handleComponentValidation={this.handleValidate}
                                    handleCity={handleChangeCity}
                                    selected_city={address.selected_city || {}}
                                />
                            </div>
                            <div className="map-control__map">
                                <Map ref={map => this.map = map} handleReady={this.init}/>
                            </div>
                        </div>
                        <PointSelection
                            points={filtered_points}
                            value_prop={selected_point}
                            handlePoint={this.handlePoint}
                            handleFinalChoice={this.handlePointFinalChoice}
                            deal_role={deal_role}
                            closePopup={closePopup}
                        />
                    </div>
                </div>
            </div>
        );
    }
};