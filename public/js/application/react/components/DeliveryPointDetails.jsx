import React from 'react';

const renderSchedule = times => {
    if(!times || !Object.keys(times).length){
        return null;
    }
    return times.map(time => {
        const week_days = time.week_days;
        const work_time = time.work_time;
        return (
            <div key={work_time}>
                {week_days}: {work_time}
            </div>
        );
    });
};

const DeliveryPointDetails = ({ pickPoint }) => {
    return(
        <div className="pick-point">
            <div className="pick-point__body">
                <div className="pick-point-details-group pick-point__main">
                    <div className="pick-point-details-group pick-point__description">
                        <h4 className="pick-point__name">{pickPoint.region_name}</h4>
                    </div>
                    <h4 className="pick-point__name">{pickPoint.name}</h4>
                    <div className="pick-point__work-info">
                        {pickPoint.street_abr} {pickPoint.street}, {pickPoint.house}
                    </div>
                </div>
                {
                    pickPoint.description &&
                    <div className="pick-point-details-group pick-point__description">
                        {pickPoint.description}
                    </div>
                }
                {
                    pickPoint.times &&
                    <div className="pick-point-details-group pick-point__about">
                        <h4>Время работы:</h4>
                        {renderSchedule(pickPoint.times)}
                    </div>
                }
            </div>
        </div>
    );
};

// PickPointDetails.propTypes = {
//     pickPoint: PropTypes.object.isRequired,
//     closeDetails: PropTypes.func.isRequired,
//     dealRole: PropTypes.string.isRequired,
//     choiceHandler: PropTypes.func.isRequired
// };

export default DeliveryPointDetails;
