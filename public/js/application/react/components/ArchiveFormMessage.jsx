import React from 'react';
import PropTypes from 'prop-types';

const ArchiveFormMessage = ({ handleResetForm }) => {
  const handleResetLinkClick = ev => {
    ev.preventDefault();
    handleResetForm && handleResetForm();
  };
  return(
    <div className="row">
      <div className="col-xl-12">
        <div className="form-info-message">
          Заполнение формы восстановлено из архива.
          <br />
          Вы можете <a href="#" onClick={handleResetLinkClick}>очистить ранее заполненное</a>.
        </div>
      </div>
    </div>
  );
};

ArchiveFormMessage.propTypes = {
  handleResetForm: PropTypes.func.isRequired
};

export default ArchiveFormMessage;
