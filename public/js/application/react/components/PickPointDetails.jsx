import React from 'react';
import PropTypes from 'prop-types';
import {getPickPointShedule, getLimitName, getDpdPointAddress} from '../Helpers';

const renderShedule = (pickPoint, dealRole) => {
    return getPickPointShedule(pickPoint, dealRole).map(sheduleItem => {
        if(!sheduleItem){
            return null;
        }
        return(
            <div key={sheduleItem.week_day}>
                {sheduleItem.week_day}: {sheduleItem.work_time};
            </div>
        )
    })
};

const renderLimits = limits => {
    if(!limits || !Object.keys(limits).length){
        return null;
    }
    return Object.entries(limits).map(limit => {
        const limitName = getLimitName(limit);
        if(!limitName.length) {
            return null;
        }
        return (
            <div key={limit[0]}>
                {limitName}
            </div>
        );
    });
};

const PickPointDetails = ({ pickPoint, closeDetails, dealRole, choiceHandler}) => {
    return(
        <div className="pick-point">
            <div className="pick-point__head">
                <button className="pick-point__back-btn" onClick={() => closeDetails()}>Вернуться к списку</button>
            </div>
            <div className="pick-point__body">
                <div className="pick-point-details-group pick-point__main">
                    <h4 className="pick-point__name">{pickPoint.name}</h4>
                    <div className="pick-point__work-info">
                        {getDpdPointAddress(pickPoint)}
                    </div>
                    <div className="pick-point__work-info">
                        {renderShedule(pickPoint, dealRole)}
                    </div>
                    <div className="pick-point__select-btn">
                        <button
                            className="ButtonApply"
                            data-ripple-button=""
                            onClick={choiceHandler}
                            data-value={pickPoint.code}
                        >
                            <span className="ripple-text">Выбрать этот пункт</span>
                        </button>
                    </div>
                </div>
                {
                    pickPoint.description &&
                    <div className="pick-point-details-group pick-point__description">
                        {pickPoint.description}
                    </div>
                }
                {
                    pickPoint.limits &&
                    <div className="pick-point-details-group pick-point__about">
                        <h4>Ограничения:</h4>
                        {renderLimits(pickPoint.limits)}
                    </div>
                }
            </div>
        </div>
    );
};

PickPointDetails.propTypes = {
    pickPoint: PropTypes.object.isRequired,
    closeDetails: PropTypes.func.isRequired,
    dealRole: PropTypes.string.isRequired,
    choiceHandler: PropTypes.func.isRequired
};

export default PickPointDetails;
