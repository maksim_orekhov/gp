import React from 'react';
import FormNaturalPerson from './forms/FormNaturalPerson';
import Popup from './Popup';
import InfoField from './InfoField';
import { customFetch } from './Helpers';
import URLS from "./constants/urls";
// import Fade from './animations/Fade';

export default class InfoNaturalPerson extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         naturalPerson: '',
         isOpenedFormEdit: false
      };
      this.getInfo = this.getInfo.bind(this);
      this.afterDelete = this.afterDelete.bind(this)
   }

   componentDidMount() {
      !this.props.isNested && this.getInfo();
   }

   getInfo() {
      customFetch(window.location)
          .then(data => {
              console.log(data);
              this.setState({
                  isOpenedFormEdit: false,
                  naturalPerson: data.data.naturalPerson
              });
          })
          .catch(error => console.log(error));
   }

    afterDelete() {
        window.location = URLS.PROFILE;
    }

   componentWillReceiveProps(nextProps) {
      this.setState({
         naturalPerson: nextProps.data
      });
   }

   toggleFormEdit(collectionItem) {
      this.setState({
         collectionItemEdit: collectionItem,
         isOpenedFormEdit: !this.state.isOpenedFormEdit
      });
   }

   render() {
      const { isOpenedFormEdit, naturalPerson, collectionItemEdit } = this.state;
      const { last_name, first_name, secondary_name, birth_date } = naturalPerson;
      return (
          <div className="container">
              <div className="row nested-row">
                  <div className="col-xl-12">
                      <div className="CivilLawSubjectPage-Header">
                          <div className="TitleWithEditIcon">
                              <h1 className="TitleBlue">Физическое лицо</h1>
                              <a className="IconEdit" onClick={() => this.toggleFormEdit(naturalPerson)} /* href="/profile/natural-person/166/edit" *//>
                          </div>
                          <div className="CivilLawSubjectDataOverview">
                              <div className="CivilLawSubjectDataOverview-Column">
                                  <InfoField label="Фамилия:" value={last_name} />
                                  <InfoField label="Имя:" value={first_name} />
                              </div>
                              <div className="CivilLawSubjectDataOverview-Column">
                                  <InfoField label="Отчество:" value={secondary_name} />
                                  <InfoField label="Дата рождения:" value={birth_date} />
                              </div>
                          </div>
                          {
                              isOpenedFormEdit &&
                              // <Fade>
                              <Popup close={() => this.toggleFormEdit({})}>
                                  <FormNaturalPerson
                                      data={naturalPerson}
                                      editMode={true}
                                      afterSubmit={this.getInfo}
                                      afterDelete={this.afterDelete}
                                      item_id={collectionItemEdit}
                                  />
                              </Popup>
                              // </Fade>
                          }
                      </div>
                  </div>
              </div>
          </div>
      );
   }
}
