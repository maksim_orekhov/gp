import React from 'react';
import FormEmailEdit from '../forms/FormEmailEdit';
import FormEmailConfirm from '../forms/FormEmailConfirm';

/**
 * Компонент отвечающий за пошаговое отображение форм при смене email
 */
export default class ControllerEmailEdit extends React.Component {
    constructor(props) {
        super(props);

        this.forms = ['edit_email', 'email_confirm'];

        this.state = {
            current_form: this.forms[0]
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(form) {
        const current_form_index = this.forms.indexOf(form);

        if (current_form_index + 1 === this.forms.length) {
            window.location.reload(false);
        } else {
            setTimeout(() => {
                this.setState({
                    current_form: this.forms[current_form_index + 1]
                });
            }, 1000);
        }
    }

    render() {
        const { current_form } = this.state;

        return (
            <div>
                {{
                    edit_email: (
                        <FormEmailEdit handleNextStep={this.handleSubmit}/>
                    ),
                    email_confirm: (
                        <FormEmailConfirm handleNextStep={this.handleSubmit}/>
                    ),
                }[current_form]}
            </div>
        )
    }
}