import React from 'react';

import DeliveryPayerOption from '../controls/ControlDeliveryPayerOption';
import DeliveryData from './ControllerDpdDeliveryData';
import DeliveryCost from '../components/Dpd/DpdDeliveryCost';


export default class ControllerDpd extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            delivery_fee_payer_option: 'customer',
            destination_address_defined: false,
            is_preloader: false,
            is_valid: false,
            validation: {
                agent_delivery_data_is_valid: false,
                counter_agent_delivery_data_is_valid: false
            }
        };

        this.handleAddedCounterAgentDeliveryData = this.handleAddedCounterAgentDeliveryData.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleValidate = this.handleValidate.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { deal_role } = nextProps;

        if (this.props.deal_role !== deal_role && this.state.destination_address_defined) {
            this.setState({
                destination_address_defined: false
            });
        }
    }

    handleChange(name, value) {
        const { handleComponentChange, name: name_of_upper_component, value_prop } = this.props;

        const delivery = {
            ...value_prop,
            [name]: value
        };

        handleComponentChange && handleComponentChange(name_of_upper_component, delivery);
    };

    handleAddedCounterAgentDeliveryData(e) {
        const { name, checked } = e.target;
        const { deal_role, value_prop, handleComponentChange, name: name_of_upper_component } = this.props;
        const delivery = {...value_prop};

        // Очищаем данные под галочкой
        if (checked === false) {
            if (deal_role === 'contractor') {
                if (delivery.receiver_address) {
                    delete delivery.receiver_address;
                }
            } else if (deal_role === 'customer') {
                if (delivery.sender_address) {
                    delete delivery.sender_address;
                }
            }
        }

        handleComponentChange && handleComponentChange(name_of_upper_component, delivery);

        this.setState({
            [name]: checked,
            validation: {
                ...this.state.validation,
                counter_agent_delivery_data_is_valid: false
            }
        }, () => this.componentValidate());
    }

    setPreloader(trueOrFalse) {
        this.setState({
            is_preloader: trueOrFalse
        });
    }

    setChildrenValidations(name, value) {
        return new Promise((resolve, reject) => {
            if (this.state.validation[name] === value) {
                resolve(name, value);
            } else {
                this.setState({
                    validation: {
                        ...this.state.validation,
                        [name]: value
                    }
                }, resolve(name, value));
            }
        });
    }

    handleValidate(name, value) {
        // Если значение валидации дочернего компонента не изменилось то не обновляем state,
        // но всё равно заставляем компонент сообщить наверх о своей валидации
        // !!! это нужно так как например меняется валидация дочернего delivery_type, он по-прежнему остается валидным,
        // !!! следовательно значение в state обновлять не нужно, но вот валидность самого компонента, в котором находится delivery_type
        // !!! перепроверить нужно, так как delivery_type изменился, и для нового типа доставки другое условие в валидации
        // !!! ТАКОЙ ПОДХОД СТОИТ ИСПОЛЬЗОВАТЬ ЕСЛИ КОМПОНЕНТ ИМЕЕТ СЛОЖНУЮ ЛОГИКУ ВАЛИДАЦИИ
        this.setChildrenValidations(name, value)
            .then(() => {
                this.componentValidate();
            });
    }

    componentValidate() {
        const { handleComponentValidation, can_added_counteragent_data, validate_prop, deal_role } = this.props;
        const { agent_delivery_data_is_valid, counter_agent_delivery_data_is_valid } = this.state.validation;
        const { destination_address_defined } = this.state;

        const is_valid = (
            (agent_delivery_data_is_valid)
            && (
                !can_added_counteragent_data
                || (
                    deal_role === 'customer'
                    || (
                        !destination_address_defined ||
                        counter_agent_delivery_data_is_valid
                    )
                )
            )
        );

        handleComponentValidation && handleComponentValidation(validate_prop, is_valid);
    }

    render() {
        const { is_preloader , destination_address_defined } = this.state;
        const {
            deal_role,
            value_prop,
            csrf,
            can_added_counteragent_data,
            calc_sender_city_id,
            calc_receiver_city_id,
            calc_self_delivery,
            calc_self_pickup,
            calc_weight,
            is_show_calc
        } = this.props;

        return (
            <div className={`dpd-delivery-type ${is_preloader ? "Preloader Preloader_solid" : ""}`}>
                <DeliveryData
                    csrf={csrf}
                    deal_role={deal_role}
                    address_control_name={deal_role === "contractor" ? "sender_address" : "receiver_address"}
                    address_label={deal_role === "contractor" ? "Откуда забрать товар:" : "Куда доставить товар:"}
                    fio_label={deal_role === "contractor" ? "ФИО отправителя:" : "ФИО получателя:"}
                    value_prop={value_prop}
                    validate_prop="agent_delivery_data_is_valid"
                    handleComponentChange={this.handleChange}
                    handleComponentValidation={this.handleValidate}
                />
                {
                    deal_role === 'contractor'
                        ?
                        can_added_counteragent_data &&
                        <div className="Input-Wrapper">
                            <div className="form-info-message">
                                Рекомендуем вам указать адрес получателя, тогда сразу будет расчитана стоимость и сроки
                                доставки. Для расчета вы можете использовать <a href="https://www.dpd.ru/ols/calc/calc.do2" target="_blank">калькулятор</a>.
                            </div>
                            <div className='row i-know-receiver-address Input-Wrapper'>
                                <div className="flex">
                                    <input
                                        id="check-i-know-receiver-address"
                                        name="destination_address_defined"
                                        type="checkbox"
                                        tabIndex="0"
                                        onChange={this.handleAddedCounterAgentDeliveryData}
                                    />
                                    <label htmlFor="check-i-know-receiver-address">
                                        Я знаю адрес получателя.
                                    </label>
                                </div>
                            </div>
                        </div>
                        : <p className="Note">
                            Претензии после подтверждения получения товара не принимаются.
                            В случае, когда вы получили товар несоответствующий условиям сделки, откажитесь от него в присутствии курьера.
                        </p>
                }
                {
                    can_added_counteragent_data &&
                    deal_role === 'contractor' &&
                    destination_address_defined &&
                    <DeliveryData
                        // у этого компонента данные берутся наоборот (так как роль противоположная роли юзера)
                        csrf={csrf}
                        deal_role={deal_role === "contractor" ? "customer" : "contractor"}
                        address_control_name={deal_role === "contractor" ? "receiver_address" : "sender_address"}
                        value_prop={value_prop}
                        validate_prop="counter_agent_delivery_data_is_valid"
                        handleComponentChange={this.handleChange}
                        handleComponentValidation={this.handleValidate}
                        address_label={deal_role === "contractor" ? "Куда доставить товар:" : "Откуда забрать товар:"}
                        fio_label={deal_role === "contractor" ? "ФИО получателя:" : "ФИО отправителя:"}
                        unique_id_postfix_of_dpd_delivery_type="-counteragent"
                    />
                }
                {
                    is_show_calc &&
                    deal_role === 'contractor' &&
                    <DeliveryCost
                        sender_city_id={calc_sender_city_id}
                        receiver_city_id={calc_receiver_city_id}
                        self_delivery={calc_self_delivery}
                        self_pickup={calc_self_pickup}
                        weight={calc_weight}
                        csrf={csrf}
                    />
                }
            </div>
        );
    }
}