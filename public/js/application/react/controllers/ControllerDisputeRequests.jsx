import React from 'react';
import DisputeRequestList from '../DisputeRequestList';
import { getIdFromUrl } from '../Helpers';
import URLS from '../constants/urls';
import DisputeRequestCreateForm from "../DisputeRequestCreateForm";
import { customFetch } from '../Helpers';

export default class ControllerDisputeRequests extends React.Component {
    constructor(props) {
        super(props);
        /**
         * @param {boolean} is_allowed_arbitrage_request - доступен ли текущем пользователю запрос на арбитраж
         * @param {boolean} is_allowed_discount_request - доступен ли текущем пользователю запрос на скидку
         * @param {boolean} is_allowed_refund_request - доступен ли текущем пользователю запрос на возврат
         * @param {boolean} is_allowed_tribunal_request - доступен ли текущем пользователю запрос на суд
         * @param {string} deal_type - индификатор типа сделки, либо U - услуга, либо T - товар
         * @param {number} max_discount_amount - максимальный доступный размер скидки
         * @param {number} is_need_update_request_list_collection - используется именно число потому что DisputeRequestList отлавливает изменение этого флага
         * в componentWillReceiveProps и если он изменился обновляет коллекцию, поэтому мне показлось логично использовать число, которое будет повышаться
         * на 1 как только нужно обновиться коллекцию в DisputeRequestList
         */
        this.state = {
            is_allowed_arbitrage_request: false,
            is_allowed_discount_request: false,
            is_allowed_refund_request: false,
            is_allowed_tribunal_request: false,
            deal_type: '',
            max_discount_amount: null,
            is_need_update_request_list_collection: 0,
            is_customer: null,
            form_loaded: false
        };

        this.handleOnSubmit = this.handleOnSubmit.bind(this);
    }

    componentWillMount() {
        this.init();
    }

    componentDidMount() {
        this.controllerInit();
    }

    controllerInit() {
        document.getElementById('DisputeRequests').style.visibility = 'visible';
    }

    /**
     * @param {boolean} isNeedToUpdateRequestsList - сообщает о том что произошел сабмит RequestCreateForm и нужно обновить данные в DisputeRequestList,
     * если isNeedToUpdateRequestsList будет пустой либо false тогда пришел сабмит из DisputeRequestList и следовательно его обновлять уже не нужно,
     * он обновится сам
     */
    handleOnSubmit(isNeedToUpdateRequestsList) {
        const { is_need_update_request_list_collection } = this.state;

        isNeedToUpdateRequestsList && this.setState({
            // Повышаем на 1 чтобы DisputeRequestList понял что нужно обновить коллекцию
            is_need_update_request_list_collection: is_need_update_request_list_collection + 1
        });
        this.init();
    }

    /**
     * Получаем данные по реквестам с сервера
     */
    init() {
        const idDeal = getIdFromUrl();

        customFetch(`${URLS.DEAL.FORM_INIT}?idDeal=${idDeal}`)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    console.log('ControllerDisputeRequests', data.data);
                    const {
                        deal: {
                            type_ident,
                            is_customer,
                            dispute: {
                                is_allowed_arbitrage_request,
                                is_allowed_discount_request,
                                is_allowed_refund_request,
                                is_allowed_tribunal_request,
                                max_discount_amount
                            }
                        },
                        dispute_history
                    } = data.data;

                    const is_arbitrage_after_three_discount_rejects = dispute_history[dispute_history.length-1].code.indexOf('ARBITRAGE_REQUEST_WITH_DISCOUNT_') > -1;
                    const is_arbitrage_after_refund_reject = dispute_history[dispute_history.length-1].code.indexOf('REFUND_REQUEST_REJECTED_') > -1;
                    const is_arbitrage_after_tribunal_reject = dispute_history[dispute_history.length-1].code.indexOf('TRIBUNAL_REQUEST_REJECTED_') > -1;

                    this.setState({
                        is_allowed_arbitrage_request,
                        is_allowed_discount_request,
                        is_allowed_refund_request,
                        is_allowed_tribunal_request,
                        deal_type: type_ident,
                        max_discount_amount,
                        is_customer,
                        form_loaded: true,
                        is_arbitrage_after_three_discount_rejects,
                        is_arbitrage_after_refund_reject,
                        is_arbitrage_after_tribunal_reject
                    });
                } else if (data.status === 'ERROR') {
                    return Promise.reject('Ошибка при инициализации. Попробуйте обновить страницу.');
                }
            })
            .catch(error => console.log(error));
    }

    render() {
        const {
            is_allowed_arbitrage_request,
            is_allowed_discount_request,
            is_allowed_refund_request,
            is_allowed_tribunal_request,
            deal_type,
            max_discount_amount,
            is_need_update_request_list_collection,
            is_customer,
            form_loaded,
            is_arbitrage_after_three_discount_rejects,
            is_arbitrage_after_refund_reject,
            is_arbitrage_after_tribunal_reject
        } = this.state;

        return (
            <div>
                {
                    (is_allowed_arbitrage_request || is_allowed_discount_request || is_allowed_refund_request || is_allowed_tribunal_request) &&
                    <DisputeRequestCreateForm
                        is_allowed_arbitrage_request={is_allowed_arbitrage_request}
                        is_allowed_discount_request={is_allowed_discount_request}
                        is_allowed_refund_request={is_allowed_refund_request}
                        is_allowed_tribunal_request={is_allowed_tribunal_request}
                        deal_type={deal_type}
                        max_discount_amount={max_discount_amount}
                        handleOnSubmit={this.handleOnSubmit}
                        is_customer={is_customer}
                    />
                }
                {
                    form_loaded &&
                    <DisputeRequestList
                        handleOnSubmit={this.handleOnSubmit}
                        is_need_update_request_list_collection={is_need_update_request_list_collection}
                        is_arbitrage_after_three_discount_rejects={is_arbitrage_after_three_discount_rejects}
                        is_arbitrage_after_refund_reject={is_arbitrage_after_three_discount_rejects}
                        is_arbitrage_after_tribunal_reject={is_arbitrage_after_three_discount_rejects}
                    />
                }

            </div>
        )
    }
}