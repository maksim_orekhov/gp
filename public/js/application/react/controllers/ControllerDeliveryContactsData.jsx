import React from 'react';
import ContactName from '../controls/ControlDeliveryOrderName';
import Phone from '../../../landing/react/auth/forms/RegisterForm/controls/phone-input';
import Email from '../../../landing/react/auth/forms/RegisterForm/controls/email-input.jsx';

export default class ControllerDeliveryContactsData extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            validation: {
                contact_phone_is_valid: false,
                contact_email_is_valid: false,
                contact_fio_is_valid: false
            }
        };

        this.handleValidate = this.handleValidate.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    setChildrenValidations(name, value) {
        return new Promise((resolve, reject) => {
            if (this.state.validation[name] === value) {
                resolve(name, value);
            } else {
                this.setState((prevState) => {
                    return {
                        validation: {
                            ...prevState.validation,
                            [name]: value
                        }
                    }
                }, resolve(name, value));
            }
        });
    }

    handleValidate(name, value) {
        // Если значение валидации дочернего компонента не изменилось то не обновляем state,
        // но всё равно заставляем компонент сообщить наверх о своей валидации
        // !!! это нужно так как например меняется валидация дочернего delivery_type, он по-прежнему остается валидным,
        // !!! следовательно значение в state обновлять не нужно, но вот валидность самого компонента, в котором находится delivery_type
        // !!! перепроверить нужно, так как delivery_type изменился, и для нового типа доставки другое условие в валидации
        // !!! ТАКОЙ ПОДХОД СТОИТ ИСПОЛЬЗОВАТЬ ЕСЛИ КОМПОНЕНТ ИМЕЕТ СЛОЖНУЮ ЛОГИКУ ВАЛИДАЦИИ
        this.setChildrenValidations(name, value)
            .then(() => {
                this.componentValidate();
            });
    }

    componentValidate() {
        const { handleComponentValidation, validate_prop } = this.props;
        const { contact_phone_is_valid, contact_email_is_valid, contact_fio_is_valid } = this.state.validation;

        const is_valid = (
            contact_phone_is_valid
            && contact_email_is_valid
            && contact_fio_is_valid
        );

        handleComponentValidation && handleComponentValidation(validate_prop, is_valid);
    }


    handleChange(name, value) {
        const { handleComponentChange, name: name_of_upper_component, address } = this.props;

        const dpd_delivery_address = {
            ...address,
            [name]: value
        };

        handleComponentChange && handleComponentChange(name_of_upper_component, dpd_delivery_address);
    };

    render() {
        const { address, fio_label = 'Контактное лицо:' } = this.props;
        const { contact_fio = '', contact_phone = '', contact_email = '' } = address;

        return (
            <div className="PageForm-Container FormLabelTop dpd-delivery-order-form">
                <div className="row">
                    <div className="col-xl-12" style={{paddingRight: 0}}>
                        <ContactName
                            label={fio_label}
                            placeholder="Введите ФИО"
                            name="contact_fio"
                            value_prop={contact_fio}
                            handleComponentChange={this.handleChange}
                            handleComponentValidation={this.handleValidate}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col-xl-6 col-sm-4">
                        <div className="Input-Wrapper">
                            <Phone
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleValidate}
                                validateOnChange={true}
                                name="contact_phone"
                                label="Контактный телефон:"
                                componentValue={this.handleChange}
                                autocomplete_value={contact_phone}
                                register={false}
                            />
                        </div>
                    </div>
                    <div className="col-xl-6 col-sm-4" style={{paddingRight: 0}}>
                        <div className="Input-Wrapper">
                            <Email
                                register={false}
                                not_check_unique={true}
                                handleComponentChange={this.handleChange}
                                email={contact_email}
                                name="contact_email"
                                label="Контактный email:"
                                handleComponentValidation={this.handleValidate}
                                componentValue={this.handleChange}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};


