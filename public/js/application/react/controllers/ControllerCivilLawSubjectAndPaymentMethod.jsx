import React from 'react';
import CivilLawSubject from '../controls/ControlCivilLawSubject';
import PaymentMethod from '../controls/ControlPaymentMethod';

export default class ControllerCivilLawSubjectAndPaymentMethod extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenPaymentMethod: false
        };

        this.handleCivilLawSubject = this.handleCivilLawSubject.bind(this);
        this.handleOpenPaymentMethod = this.handleOpenPaymentMethod.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { civil_law_subject_id } = nextProps;

        if (civil_law_subject_id) {
            this.handleOpenPaymentMethod(true);
        }
    }

    checkOpenFormPaymentMethod(civil_law_subject_id) {
        this.setState({
            isOpenPaymentMethod: !!civil_law_subject_id
        });
    }

    handleCivilLawSubject(name, value) {
        const { handleCivilLawSubject } = this.props;

        handleCivilLawSubject && handleCivilLawSubject(name, value);

        this.checkOpenFormPaymentMethod(value);
    }

    handleOpenPaymentMethod(trueOrFalse) {
        this.setState({
            isOpenPaymentMethod: trueOrFalse
        })
    }

    render() {
        const { isOpenPaymentMethod } = this.state;
        const {
            civil_law_subject_label,
            payment_method_label,
            civil_law_subject_name,
            payment_method_name,
            civil_law_subject_id,
            payment_method_id,
            is_nested,
            is_cls_control,
            handleComponentChange,
            handleComponentValidation,
            sendUpData,
            handleCivilLawSubjectValid,
            update_collection,
            form_civil_law_subject_validate_name,
            form_civil_law_subject_data_name,
            form_payment_method_validate_name,
            form_payment_method_data_name,
            is_hide_bank_transfer_name,
            form_control_server_errors_civil_law_subject,
            form_control_server_errors_civil_law_subject_form,
            form_control_server_errors_payment_method,
            form_control_server_errors_payment_method_form,
            natural_persons,
            legal_entities,
            sole_proprietors,
            clear_local_storage
        } = this.props;

        return (
            <div>
                <CivilLawSubject
                    label={civil_law_subject_label}
                    name={civil_law_subject_name}
                    value_prop={civil_law_subject_id}
                    handleComponentChange={this.handleCivilLawSubject}
                    handleComponentValidation={handleCivilLawSubjectValid}
                    sendUpData={sendUpData}
                    is_nested={is_nested}
                    natural_persons={natural_persons}
                    legal_entities={legal_entities}
                    sole_proprietors={sole_proprietors}
                    form_validate_name={form_civil_law_subject_validate_name}
                    form_data_name={form_civil_law_subject_data_name}
                    update_collection={update_collection}
                    openPaymentMethod={this.handleOpenPaymentMethod}
                    form_control_server_errors={form_control_server_errors_civil_law_subject}
                    form_server_errors={form_control_server_errors_civil_law_subject_form}
                    clear_local_storage={clear_local_storage}
                />
                {
                    isOpenPaymentMethod &&
                    <PaymentMethod
                        label={payment_method_label}
                        name={payment_method_name}
                        value_prop={payment_method_id}
                        civil_law_subject_id={civil_law_subject_id}
                        natural_persons={natural_persons}
                        legal_entities={legal_entities}
                        sole_proprietors={sole_proprietors}
                        handleComponentChange={handleComponentChange}
                        handleComponentValidation={handleComponentValidation}
                        form_validate_name={form_payment_method_validate_name}
                        form_data_name={form_payment_method_data_name}
                        sendUpData={sendUpData}
                        is_nested={is_nested}
                        is_cls_control={is_cls_control}
                        is_hide_name={is_hide_bank_transfer_name}
                        form_control_server_errors={form_control_server_errors_payment_method}
                        form_server_errors={form_control_server_errors_payment_method_form}
                        clear_local_storage={clear_local_storage}
                    />
                }
            </div>
        );
    }
}