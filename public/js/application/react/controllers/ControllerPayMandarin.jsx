import React from 'react';
import FormPayMandarin from '../forms/FormPayMandarin';
import {customFetch, getIdFromUrl} from "../Helpers";
import URLS from "../constants/urls";

/**
 * Компонент-контроллер для формы оплаты Мандарином
 */
export default class ControllerPayMandarin extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isFormHidden: true,
            deal_id: getIdFromUrl(),
            available_payment_method_types: {}
        };

        this.toggleFormVisibility = this.toggleFormVisibility.bind(this);
        this.init = this.init.bind(this);
    }

    componentDidMount() {
        this.init();
        document.getElementById('ControllerPayMandarin').classList.remove('Preloader');
    }

    init() {
        const { deal_id } = this.state;
        const url = deal_id ? `${URLS.DEAL.FORM_INIT}?idDeal=${deal_id}` : URLS.DEAL.FORM_INIT;

        customFetch(url)
            .then(response => {
                this.setState(() => {
                    return {
                        available_payment_method_types: response.data.deal.available_payment_method_types
                    }
                });
            });
    }

    toggleFormVisibility() {
        this.setState((prevState) => {
            return {
                isFormHidden: !prevState.isFormHidden
            }
        });
    }

    render() {
        const { deal_id, available_payment_method_types } = this.state;
        return (
            <div className="DisputeMethod">
                <div className="row nested-row pay-buttons">
                    {
                        available_payment_method_types.acquiring_mandarin === true &&
                        <div className="col-xl-6 col-md-6">
                            <button disabled={!this.state.isFormHidden} className="ButtonApply"
                                    onClick={this.toggleFormVisibility} data-ripple-button="">
                                <span className="ripple-text">
                                    <div className="icon-security-card"/>
                                    Оплатить картой
                                </span>
                            </button>
                        </div>
                    }
                    {
                        available_payment_method_types.bank_transfer === true &&
                        <div className="col-xl-6 col-md-6">
                            <div className="LinkWithIcon deal-pay-download-bill">
                                <div className="IconDownload LinkWithIcon-Icon"></div>
                                <a href={'/invoice/' + deal_id + '/download'}
                                   className="LinkWithIcon-Text AccentLink">Скачать счет на оплату через банк</a>
                            </div>
                        </div>
                    }
                </div>
                {
                    this.state.isFormHidden ? null
                        : <FormPayMandarin
                            toggleVisibility={this.toggleFormVisibility}

                        />
                }
            </div>
        )
    }
}