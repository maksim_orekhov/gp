import React from 'react';

import DeliveryType from '../controls/ControlDpdDeliveryType';
import DoorToDoor from '../forms/FormDoorToDoorDelivery';
import SelfDelivery from '../components/DpdSelfDeliveryPopup';
import { customFetch } from '../Helpers';
import URLS from '../constants/urls';


export default class ControllerDeliveryAddress extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            csrf: this.props.csrf || '',
            dpd_delivery_type: 'door_to_door',
            is_open_self_delivery_popup: false,
            validation: {
                door_to_door_is_valid: false,
                self_delivery_is_valid: false
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeCity = this.handleChangeCity.bind(this);
        this.handleChangePoint = this.handleChangePoint.bind(this);
        this.handleValidate = this.handleValidate.bind(this);
        this.handleChangeDeliveryType = this.handleChangeDeliveryType.bind(this);
        this.triggerSelfDeliveryPopup = this.triggerSelfDeliveryPopup.bind(this);
    }

    componentWillMount() {
        if (!this.props.csrf) {
            this.getCsrf();
        }

        this.checkCurrentDeliveryTypeByDealRole();

        // Закоментил так как у продавца теперь всегда только пункт приема, и эта проверка не нужна
        // this.checkSelfDelivery();
    }

    componentWillReceiveProps(nextProps) {
        this.checkCurrentDeliveryTypeByDealRole(nextProps);

        // Закоментил так как у продавца теперь всегда только пункт приема, и эта проверка не нужна
        // this.checkSelfDelivery(nextProps);
    }

    checkCurrentDeliveryTypeByDealRole(props = this.props) {
        if (props.deal_role === 'customer' && this.state.dpd_delivery_type !== 'door_to_door') {
            this.setState({
                dpd_delivery_type: 'door_to_door'
            });
        } else if (props.deal_role === 'contractor' && this.state.dpd_delivery_type !== 'self_delivery') {
            this.setState({
                dpd_delivery_type: 'self_delivery'
            });
        }
    }

    // Проеряем пришел ли terminal_code, если да то надо открыть self_delivery
    checkSelfDelivery(props = this.props) {
        const { address } = props;

        if (address.terminal_code) {
            if (this.state.dpd_delivery_type === 'self_delivery') return;

            this.setState({
                dpd_delivery_type: 'self_delivery'
            });
        }
    }

    /**
     * Если нужно открыть попап self_delivery (он вложен в этот компонент) передаем ему свойство(boolean),
     * которое он слушает, и когда это свойство true он открывается
     * !!! А уже по окончанию открытия он вызывает ЭТУ ФУНКЦИЮ, которая снова сбросит свойство в false
     */
    triggerSelfDeliveryPopup(trueOrFalse) {
        this.setState({
            is_open_self_delivery_popup: trueOrFalse
        });
    }

    getCsrf() {
        this.setPreloader(true);
        return customFetch(URLS.CSRF.GET)
            .then(data => {
                if (data.status === 'SUCCESS') {
                    const {csrf = ''} = data.data;
                    this.setState({
                        csrf
                    }, () => {
                        this.setPreloader(false);
                        return Promise.resolve();
                    });
                } else if (data.status === 'ERROR') {
                    return Promise.reject(data.message);
                }
            })
            .catch(error => {
                this.setPreloader(false);
                console.error(error);
            });
    }

    setChildrenValidations(name, value) {
        return new Promise((resolve, reject) => {
            if (this.state.validation[name] === value) {
                resolve(name, value);
            } else {
                this.setState({
                    validation: {
                        ...this.state.validation,
                        [name]: value
                    }
                }, resolve(name, value));
            }
        });
    }

    handleValidate(name, value) {
        // Если значение валидации дочернего компонента не изменилось то не обновляем state,
        // но всё равно заставляем компонент сообщить наверх о своей валидации
        // !!! это нужно так как например меняется валидация дочернего delivery_type, он по-прежнему остается валидным,
        // !!! следовательно значение в state обновлять не нужно, но вот валидность самого компонента, в котором находится delivery_type
        // !!! перепроверить нужно, так как delivery_type изменился, и для нового типа доставки другое условие в валидации
        // !!! ТАКОЙ ПОДХОД СТОИТ ИСПОЛЬЗОВАТЬ ЕСЛИ КОМПОНЕНТ ИМЕЕТ СЛОЖНУЮ ЛОГИКУ ВАЛИДАЦИИ
        this.setChildrenValidations(name, value)
            .then(() => {
                this.componentValidate();
            });
    }

    componentValidate() {
        const { handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;
        const dpd_delivery_type = this.state.dpd_delivery_type;

        const {
            door_to_door_is_valid,
            self_delivery_is_valid
        } = this.state.validation;

        const is_valid = (
            (dpd_delivery_type === 'door_to_door' && door_to_door_is_valid)
            || (dpd_delivery_type === 'self_delivery' && self_delivery_is_valid)
        );

        handleComponentValidation && handleComponentValidation(validate_prop, is_valid);
    }

    handleChange(name, value) {
        const { handleComponentChange, name: name_of_upper_component, address } = this.props;

        const dpd_delivery_address = {
            ...address,
            [name]: value
        };

        handleComponentChange && handleComponentChange(name_of_upper_component, dpd_delivery_address);
    };

    handleChangeCity(name, value, selected_city) {
        const { handleComponentChange, name: name_of_upper_component, address } = this.props;

        const dpd_delivery_address = {
            ...address,
            [name]: value,
            selected_city
        };

        handleComponentChange && handleComponentChange(name_of_upper_component, dpd_delivery_address);
    }

    handleChangePoint(name, value, selected_point) {
        const { handleComponentChange, name: name_of_upper_component, address } = this.props;

        const dpd_delivery_address = {
            ...address,
            ...value,
            selected_point
        };

        handleComponentChange && handleComponentChange(name_of_upper_component, dpd_delivery_address);
    }

    handleChangeDeliveryType(name, value) {
        this.clearDpdDeliveryAddress();

        const { handleComponentValidation, name: name_of_upper_component, validate_prop = `${name_of_upper_component}_is_valid` } = this.props;

        if (value === 'self_delivery') {
            this.setState({
                is_open_self_delivery_popup: true
            });
        }

        this.setState({
            [name]: value
        });

        handleComponentValidation && handleComponentValidation(validate_prop, false);
    }

    clearDpdDeliveryAddress() {
        const { address, name, handleComponentChange } = this.props;

        if (!address || !Object.values(address)) return;

        const dpd_delivery_address = {
            ...address
        };

        for (let key in dpd_delivery_address) {
            if (key === 'street' || key === 'house' || key === 'flat' || key === 'terminal_code') {
                delete dpd_delivery_address[key];
            }
        }

        handleComponentChange && handleComponentChange(name, dpd_delivery_address);
    };

    setPreloader(trueOrFalse) {
        this.setState({
            is_preloader: trueOrFalse
        });
    }

    render() {
        const { csrf, dpd_delivery_type, is_open_self_delivery_popup } = this.state;
        const { deal_role, address, label, unique_id_postfix_of_dpd_delivery_type } = this.props;
        return (
            <div>
                {/*{
                    deal_role === 'contractor' &&
                    <DeliveryType
                        value_prop={dpd_delivery_type}
                        name="dpd_delivery_type"
                        unique_id_postfix={unique_id_postfix_of_dpd_delivery_type}
                        handleComponentChange={this.handleChangeDeliveryType}
                        door_to_door_label={deal_role === 'contractor' ? 'Забор товара от двери' : 'Курьеская доставка'}
                        self_delivery_label={deal_role === 'contractor' ? 'Отнести на пункт приема DPD' : 'Самовывоз из пункта выдачи\\постамата'}
                    />
                }*/}
                {
                    {
                        door_to_door:
                            <DoorToDoor
                                form_validate_name="door_to_door_is_valid"
                                is_nested={true}
                                city_search_label={label}
                                address={address}
                                csrf={csrf}
                                handleComponentChange={this.handleChange}
                                handleChangeCity={this.handleChangeCity}
                                handleComponentValidation={this.handleValidate}
                            />,
                        self_delivery:
                            <SelfDelivery
                                address={address}
                                deal_role={deal_role}
                                csrf={csrf}
                                name="self_delivery"
                                handleComponentValidation={this.handleValidate}
                                handleComponentChange={this.handleChange}
                                handleChangeCity={this.handleChangeCity}
                                handleChangePoint={this.handleChangePoint}
                                is_trigger_popup_open={is_open_self_delivery_popup}
                                popupOpenCallback={() => this.triggerSelfDeliveryPopup(false)}
                            />
                    }[dpd_delivery_type]
                }
            </div>
        );
    }
}