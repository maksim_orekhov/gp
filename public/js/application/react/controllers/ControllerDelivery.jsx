import React from 'react';
import ServiceType from '../controls/ControlServiceType';
import ServiceTypeHints from '../components/DeliveryServiceTypeHints';
import DeliveryForms from './ControllerDeliveryForms';

import { DELIVERY_TYPES_NAMES } from '../constants/names';


export default class ControllerDelivery extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            validation: {
                dpd_is_valid: false,
                no_service_is_valid: false,
                service_type_is_valid: false
            }
        };

        this.handleComponentValidation = this.handleComponentValidation.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    setChildrenValidations(name, value) {
        return new Promise((resolve, reject) => {
            if (this.state.validation[name] === value) {
                resolve(name, value);
            } else {
                this.setState((prevState) => {
                    return {
                        validation: {
                        ...prevState.validation,
                            [name]: value
                        }
                    }
                }, resolve(name, value));
            }
        });
    }

    handleComponentValidation(name, value) {
        // Если значение валидации дочернего компонента не изменилось то не обновляем state,
        // но всё равно заставляем компонент сообщить наверх о своей валидации
        // !!! это нужно так как например меняется валидация дочернего delivery_type, он по-прежнему остается валидным,
        // !!! следовательно значение в state обновлять не нужно, но вот валидность самого компонента, в котором находится delivery_type
        // !!! перепроверить нужно, так как delivery_type изменился, и для нового типа доставки другое условие в валидации
        // !!! ТАКОЙ ПОДХОД СТОИТ ИСПОЛЬЗОВАТЬ ЕСЛИ КОМПОНЕНТ ИМЕЕТ СЛОЖНУЮ ЛОГИКУ ВАЛИДАЦИИ
        this.setChildrenValidations(name, value)
            .then(() => {
                this.componentValidate();
            });
    }

    componentValidate(service_type = this.props.value_prop.service_type) {
        const { handleComponentValidation, name, validate_prop = `${name}_is_valid`, service_types, current_service_type_name } = this.props;

        const { dpd_is_valid, no_service_is_valid, service_type_is_valid } = this.state.validation;

        const is_valid = (
            service_type_is_valid
            && (
                (current_service_type_name === DELIVERY_TYPES_NAMES.NO_SERVICE && no_service_is_valid)
                || (current_service_type_name === DELIVERY_TYPES_NAMES.DPD && dpd_is_valid)
                || current_service_type_name === DELIVERY_TYPES_NAMES.NOT_REQUIRED
            )
        );

        handleComponentValidation && handleComponentValidation(validate_prop, is_valid);
    }

    handleChange(name, value) {
        const { handleComponentChange, name: name_of_upper_component, value_prop } = this.props;

        const delivery = {
            ...value_prop,
            [name]: value
        };

        handleComponentChange && handleComponentChange(name_of_upper_component, delivery);
    };

    render() {
        const {
            label,
            deal_role,
            value_prop,
            is_disable,
            form_control_server_errors,
            csrf,
            service_types,
            service_type_name,
            service_control_name,
            current_service_type_name,
            can_added_counteragent_data,
            calc_sender_city_id,
            calc_receiver_city_id,
            calc_self_delivery,
            calc_self_pickup,
            calc_weight,
            is_show_calc,
            service_dpd_control_name
        } = this.props;

        const service_type = value_prop.service_type;

        return (
            <div className="row nested-row">
                <div className="col-xl-12 delivery-column">
                    <div className="Input-Group-Wrapper">
                        <div className="row">
                            <div className="col-xl-12">
                                <ServiceType
                                    label={label}
                                    name={service_type_name}
                                    value_prop={service_type}
                                    service_types={service_types}
                                    deal_role={deal_role}
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleComponentValidation}
                                    form_control_server_errors={form_control_server_errors}
                                    is_disable={is_disable}
                                />
                                <ServiceTypeHints
                                    current_service_type_name={current_service_type_name}
                                    deal_role={deal_role}
                                />
                            </div>
                            <div className="col-xl-12">
                                <DeliveryForms
                                    name={service_control_name}
                                    dpd_control_name={service_dpd_control_name}
                                    delivery={value_prop}
                                    current_service_type_name={current_service_type_name}
                                    deal_role={deal_role}
                                    handleComponentChange={this.handleChange}
                                    handleComponentValidation={this.handleComponentValidation}
                                    can_added_counteragent_data={can_added_counteragent_data}
                                    csrf={csrf}
                                    is_show_calc={is_show_calc}
                                    calc_sender_city_id={calc_sender_city_id}
                                    calc_receiver_city_id={calc_receiver_city_id}
                                    calc_self_delivery={calc_self_delivery}
                                    calc_self_pickup={calc_self_pickup}
                                    calc_weight={calc_weight}
                                />
                            </div>
                        </div>
                  </div>
                </div>
            </div>
        );
    }
}