import React from 'react';
import ParcelsData from '../controls/ControlDpdParcelsData.jsx';
import Address from './ControllerDeliveryAddress';
import ControllerDeliveryContactsData from './ControllerDeliveryContactsData';


export default class ControllerDpdDeliveryData extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            is_preloader: false,
            is_valid: false,
            validation: {
                address_is_valid: false,
                contacts_data_is_valid: false,
                parcels_is_valid: false
            }
        };

        this.handleValidate = this.handleValidate.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { deal_role } = nextProps;

        if (deal_role !== this.props.deal_role) {
            this.setState({
               validation: {
                   address_is_valid: false,
                   contacts_data_is_valid: false,
                   parcels_is_valid: false
               }
            });
        }
    }

    setPreloader(trueOrFalse) {
        this.setState({
            is_preloader: trueOrFalse
        });
    }

    setChildrenValidations(name, value) {
        return new Promise((resolve, reject) => {
            if (this.state.validation[name] === value) {
                resolve(name, value);
            } else {
                this.setState({
                    validation: {
                        ...this.state.validation,
                        [name]: value
                    }
                }, resolve(name, value));
            }
        });
    }

    handleValidate(name, value) {
        // Если значение валидации дочернего компонента не изменилось то не обновляем state,
        // но всё равно заставляем компонент сообщить наверх о своей валидации
        // !!! это нужно так как например меняется валидация дочернего delivery_type, он по-прежнему остается валидным,
        // !!! следовательно значение в state обновлять не нужно, но вот валидность самого компонента, в котором находится delivery_type
        // !!! перепроверить нужно, так как delivery_type изменился, и для нового типа доставки другое условие в валидации
        // !!! ТАКОЙ ПОДХОД СТОИТ ИСПОЛЬЗОВАТЬ ЕСЛИ КОМПОНЕНТ ИМЕЕТ СЛОЖНУЮ ЛОГИКУ ВАЛИДАЦИИ
        this.setChildrenValidations(name, value)
            .then(() => {
                this.componentValidate();
            });
    }

    componentValidate() {
        const { handleComponentValidation, deal_role, validate_prop } = this.props;
        const { address_is_valid, contacts_data_is_valid, parcels_is_valid } = this.state.validation;

        let is_valid = false;

        if (deal_role === 'contractor') {
            is_valid = (
                address_is_valid
                && contacts_data_is_valid
                && parcels_is_valid
            );
        } else if (deal_role === 'customer') {
            is_valid = (
                address_is_valid
                && contacts_data_is_valid
            );
        }

        handleComponentValidation && handleComponentValidation(validate_prop, is_valid);
    }

    render() {
        const {
            deal_role,
            value_prop,
            csrf,
            address_control_name,
            handleComponentChange,
            unique_id_postfix_of_dpd_delivery_type,
            address_label,
            fio_label
        } = this.props;

        const address = value_prop[address_control_name] || {};

        return (
            <div>
            {
                {
                    customer:
                        <div>
                            <Address
                                csrf={csrf}
                                deal_role={deal_role}
                                name={address_control_name}
                                address={address}
                                validate_prop="address_is_valid"
                                handleComponentChange={handleComponentChange}
                                handleComponentValidation={this.handleValidate}
                                unique_id_postfix_of_dpd_delivery_type={unique_id_postfix_of_dpd_delivery_type}
                                label={address_label}
                            />
                            <ControllerDeliveryContactsData
                                fio_label={fio_label}
                                name={address_control_name}
                                validate_prop="contacts_data_is_valid"
                                address={address}
                                handleComponentChange={handleComponentChange}
                                handleComponentValidation={this.handleValidate}
                            />
                        </div>,
                    contractor:
                        <div>
                            <Address
                                csrf={csrf}
                                deal_role={deal_role}
                                name={address_control_name}
                                validate_prop="address_is_valid"
                                address={address}
                                handleComponentChange={handleComponentChange}
                                handleComponentValidation={this.handleValidate}
                                unique_id_postfix_of_dpd_delivery_type={unique_id_postfix_of_dpd_delivery_type}
                                label={address_label}
                            />
                            <ControllerDeliveryContactsData
                                name={address_control_name}
                                fio_label={fio_label}
                                address={address}
                                validate_prop="contacts_data_is_valid"
                                handleComponentChange={handleComponentChange}
                                handleComponentValidation={this.handleValidate}
                            />
                            <ParcelsData
                                parcels={value_prop['parcels'] || []}
                                selected_point={address.selected_point || {}}
                                name="parcels"
                                cargo_category={value_prop['cargo_category']}
                                validate_prop="parcels_is_valid"
                                handleComponentChange={handleComponentChange}
                                handleComponentValidation={this.handleValidate}
                            />
                        </div>
                }[deal_role]
            }
            </div>
        );
    }
}