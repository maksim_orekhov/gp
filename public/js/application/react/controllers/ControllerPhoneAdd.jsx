import React from 'react';
import FormPhoneConfirmCode from '../forms/FormPhoneConfirmCode';
import FormPhoneManagement from '../forms/FormPhoneManagement';
import URLS from '../constants/urls';

/**
 * Компонент отвечающий за пошаговое отображение форм при добавлении телефона
 */
export default class ControllerPhoneAdd extends React.Component {
    constructor(props) {
        super(props);

        this.forms = ['add_phone', 'phone'];

        /**
         * @param {number} step - текущий этап добавления телефона
         */
        this.state = {
            current_form: this.forms[0]
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(form) {
        const current_form_index = this.forms.indexOf(form);

        if (current_form_index + 1 === this.forms.length) {
            window.location.reload(false);
        } else {
            setTimeout(() => {
                this.setState({
                    current_form: this.forms[current_form_index + 1]
                });
            }, 1000);
        }
    }

    render() {
        const { current_form } = this.state;

        return (
            <div>
                {{
                    add_phone: (
                        <FormPhoneManagement
                            form_type={'add'}
                            form_label={'Добавить номер телефона'}
                            button_label={'Добавить'}
                            url={URLS.PHONE.CREATE}
                            handleNextStep={this.handleSubmit}/>
                    ),
                    phone: (
                        <FormPhoneConfirmCode handleNextStep={this.handleSubmit}/>
                    )
                }[current_form]}
            </div>
        )
    }
}