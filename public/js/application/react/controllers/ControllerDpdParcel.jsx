import React from 'react';

export default class ControllerDpdParcel extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ProductDimensions
                weight={product_weight}
                name="product_dimensions"
                handleComponentChange={this.handleChange}
                handleComponentValidation={this.handleValidate}
            />
        )
    }
}