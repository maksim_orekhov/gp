import React from 'react';
import FormPhoneManagement from '../forms/FormPhoneManagement';
import FormPhoneConfirmToken from '../forms/FormPhoneConfirmToken';
import FormPhoneConfirmCode from '../forms/FormPhoneConfirmCode';
import URLS from "../constants/urls";

/**
 * Компонент отвечающий за пошаговое отображение форм при смене телефона
 */
export default class ControllerPhoneEdit extends React.Component {
    constructor(props) {
        super(props);

        this.forms = ['edit_phone', 'phone_code_confirm', 'phone'];

        /**
         * @param {number} step - текущий этап смены телефона
         */
        this.state = {
            current_form: this.forms[0]
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(form) {
        const current_form_index = this.forms.indexOf(form);

        if (current_form_index + 1 === this.forms.length) {
            window.location.reload(false);
        } else {
            setTimeout(() => {
                this.setState({
                    current_form: this.forms[current_form_index + 1]
                });
            }, 1000);
        }
    }

    render() {
        const { current_form } = this.state;

        return (
            <div>
                {{
                    edit_phone: (
                        <FormPhoneManagement
                            form_type={'edit'}
                            form_label={'Изменить номер телефона'}
                            button_label={'Изменить'}
                            url={URLS.PHONE.EDIT}
                            handleNextStep={this.handleSubmit}/>
                    ),
                    phone_code_confirm: (
                        <FormPhoneConfirmToken handleNextStep={this.handleSubmit}/>
                    ),
                    phone: (
                        <FormPhoneConfirmCode handleNextStep={this.handleSubmit}/>
                    )
                }[current_form]}
            </div>
        )
    }
}