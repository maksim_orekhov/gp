import React from 'react';
import PropTypes from 'prop-types';
import TableNaturalPersons from "../tables/TableNaturalPersons";
import TableLegalEntities from "../tables/TableLegalEntities";
import TableSoleProprietors from "../tables/TableSoleProprietors";
import TableButtons from "../tables/TableButtons";
import {customFetch, getSoleProprietorsWithBirthDateInMillisecondsField} from "../Helpers";
import URLS from "../constants/urls";

/**
 * Компонент, отвечающий за отображение таблиц правовых форм в профиле
 */
export default class ControllerCivilLawSubjectsTables extends React.Component {
    constructor(props) {
        super(props);
        const defaultTable = window.sessionStorage.getItem("activeTabCivilLawSubject");

        this.state = {
            current_table: defaultTable ? defaultTable : 'table_natural_persons',
            naturalPersons: [],
            legalEntities: [],
            soleProprietors: [],
            tokens: [],
            fee_payer_options: [],
            table_isLoading: false,
            is_operator: null,
            isNewItem: false,
            csrf: '',
        };

        this.getCollection = this.getCollection.bind(this);
    }

    componentDidMount() {
        if (document.getElementById('profile-page-content')) {
            document.getElementById('profile-page-content').style.visibility = 'visible';
            document.getElementById('profile-page-table').style.display = 'block';
        }
        this.getCollection();
    }

    getCollection(isNeedToUpdateCollection = true, isNeedToHighlightNewItem) {
        isNeedToUpdateCollection && this.toggleIsLoading();
        customFetch(URLS.PROFILE)
            .then(data => {
                    console.log('profile', data);

                    isNeedToHighlightNewItem && this.highlightNewItem();
                    isNeedToUpdateCollection && this.toggleIsLoading();
                    const soleProprietorsSplittedByBirthDate = getSoleProprietorsWithBirthDateInMillisecondsField(data.data.soleProprietors);
                    if (data.status === 'SUCCESS') {
                        this.setState({
                            naturalPersons: data.data.naturalPersons,
                            legalEntities: data.data.legalEntities,
                            soleProprietors: soleProprietorsSplittedByBirthDate,
                            is_operator: data.data.is_operator,
                            tokens: Object.values(data.data.tokens),
                            fee_payer_options: data.data.fee_payer_options,
                            csrf: data.data.csrf,
                        });
                    }
            })
            .catch(() => {
                this.toggleIsLoading();
                this.setState({
                    is_operator: true
                });
            });


    }

    toggleIsLoading() {
        this.setState({
            table_isLoading: !this.state.table_isLoading
        });
    }

    handleChangeTable = (value) => () => {
        this.setState({ current_table: value });
        window.sessionStorage.setItem("activeTabCivilLawSubject", value);
    };

    highlightNewItem() {
        this.setState({ isNewItem: true });
        setTimeout(() => {
            this.setState({ isNewItem: false });
        }, 3000);
    }

    render() {
        const { current_table, table_isLoading, is_operator, naturalPersons, legalEntities, soleProprietors, tokens, fee_payer_options, isNewItem, csrf } = this.state;
        return (
            <div className={`col-xl-10 col-lg-8 col-lt-6 col-sm-4 col-xl-offset-2 col-lg-offset-1 col-lt-offset-0 ${table_isLoading ? 'Preloader Preloader_solid' : null}`}>
                <div className='row'>
                   <div className='col-xl-12'>
                        <h2 className='TitleBlue'>Ваши сохранённые правовые формы</h2>
                   </div>
                </div>
                <label htmlFor="tab1" className={`tab-point js-tab-btn${current_table === 'table_natural_persons' ? ' active' : ''}`} title="Физические лица">
                    <span className="tab-point_text link-with-dotted-line">Физические лица ({naturalPersons.length})</span>
                </label>
                <label htmlFor="tab2" className={`tab-point js-tab-btn${current_table === 'table_legal_entities' ? ' active' : ''}`} title="Юридические лица">
                    <span className="tab-point_text link-with-dotted-line">Юридические лица ({legalEntities.length})</span>
                </label>

                <label htmlFor="tab3" className={`tab-point js-tab-btn${current_table === 'table_sole_proprietors' ? ' active' : ''}`} title="Индивидуальные предприниматели">
                    <span className="tab-point_text link-with-dotted-line">ИП ({soleProprietors.length})</span>
                </label>
                <label htmlFor="tab4" className={`tab-point js-tab-btn${current_table === 'table_buttons' ? ' active' : ''}`} title="Мои кнопки">
                    <span className="tab-point_text link-with-dotted-line">Мои кнопки ({tokens.length})</span>
                </label>
                <input className="tab-input js-tab-input js-civil-law-subject" id="tab1" type="radio" name="tabs" onChange={this.handleChangeTable('table_natural_persons')}/>
                <input className="tab-input js-tab-input js-civil-law-subject" id="tab2" type="radio" name="tabs" onChange={this.handleChangeTable('table_legal_entities')}/>
                <input className="tab-input js-tab-input js-civil-law-subject" id="tab3" type="radio" name="tabs" onChange={this.handleChangeTable('table_sole_proprietors')}/>
                <input className="tab-input js-tab-input js-civil-law-subject" id="tab4" type="radio" name="tabs" onChange={this.handleChangeTable('table_buttons')}/>
                <div className="ProfilePage-Section" id='profile-page-table'>
                    {{
                        table_natural_persons: (
                            <TableNaturalPersons
                                tableIsLoading={table_isLoading}
                                naturalPersons={naturalPersons}
                                isOperator={is_operator}
                                updateCollection={this.getCollection}
                                isNewItem={isNewItem}
                                is_nested={true}
                            />
                        ),
                        table_legal_entities: (
                            <TableLegalEntities
                                legalEntities={legalEntities}
                                isOperator={is_operator}
                                updateCollection={this.getCollection}
                                isNewItem={isNewItem}
                                is_nested={true}
                            />
                        ),
                        table_sole_proprietors: (
                            <TableSoleProprietors
                                soleProprietors={soleProprietors}
                                isOperator={is_operator}
                                updateCollection={this.getCollection}
                                isNewItem={isNewItem}
                                is_nested={true}
                            />
                        ),
                        table_buttons: (
                            <TableButtons
                                tokens={tokens}
                                //tableIsLoading={table_isLoading}
                                isOperator={is_operator}
                                updateCollection={this.getCollection}
                                isNewItem={isNewItem}
                                is_nested={true}
                                fee_payer_options={fee_payer_options}
                                csrf={csrf}
                            />
                        )
                    }[current_table]}
                </div>
            </div>
        );
    }
}

ControllerCivilLawSubjectsTables.propTypes = {
    TableNaturalPersons: PropTypes.element,
    TableLegalEntities: PropTypes.element,
    TableSoleProprietors: PropTypes.element,
    TableButtons: PropTypes.element
};