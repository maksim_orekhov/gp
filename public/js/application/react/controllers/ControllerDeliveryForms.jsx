import React from 'react';
import NoService from '../controls/ControlDeliveryAddonTerms';
import Dpd from '../controllers/ControllerDpd';

import { DELIVERY_TYPES_NAMES } from '../constants/names';


const ControllerDeliveryForms = (props) => {
    const {
        delivery,
        deal_role,
        handleComponentChange,
        handleComponentValidation,
        csrf,
        name,
        current_service_type_name,
        can_added_counteragent_data,
        calc_sender_city_id,
        calc_receiver_city_id,
        calc_self_delivery,
        calc_self_pickup,
        calc_weight,
        is_show_calc,
        dpd_control_name
    } = props;
    const { NO_SERVICE, DPD } = DELIVERY_TYPES_NAMES;

    return (
        <div>
            {
                {
                    [NO_SERVICE]: <div className="row nested-row">
                        <div className="col-xl-12" style={{padding: 0}}>
                            <NoService
                                name="terms_of_delivery"
                                label="Описание доставки:"
                                value_prop={delivery.terms_of_delivery}
                                validate_prop="no_service_is_valid"
                                placeholder='Введите описание условий доставки'
                                handleComponentChange={handleComponentChange}
                                handleComponentValidation={handleComponentValidation}
                            />
                        </div>
                    </div>,
                    [DPD]: <Dpd
                        value_prop={delivery[dpd_control_name] || {}}
                        name={dpd_control_name}
                        validate_prop="dpd_is_valid"
                        deal_role={deal_role}
                        csrf={csrf}
                        handleComponentChange={handleComponentChange}
                        handleComponentValidation={handleComponentValidation}
                        can_added_counteragent_data={can_added_counteragent_data}
                        is_show_calc={is_show_calc}
                        calc_sender_city_id={calc_sender_city_id}
                        calc_receiver_city_id={calc_receiver_city_id}
                        calc_self_delivery={calc_self_delivery}
                        calc_self_pickup={calc_self_pickup}
                        calc_weight={calc_weight}
                    />,
                }[current_service_type_name]
            }
        </div>
    );
};

export default ControllerDeliveryForms;