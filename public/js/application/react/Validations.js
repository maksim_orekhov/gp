export function validateAge(value, minAge) {
    if (minAge) {
        const minAgeMilliseconds = minAge;

        const dateToMilliseconds = (date) => {
            const dateObj = new Date(date.replace(/\./g, '-'));
            return dateObj.getTime();
        };

        if (dateToMilliseconds(value) > Date.now() - minAge * 365 * 24 * 60 * 60 * 1000) {
            return {
                isValid: false,
                message: `вам должно быть больше ${minAge} лет`
            };
        } else {
            return {
                isValid: true,
                message: ''
            };
        }
    } else {
        return {
            isValid: true,
            message: ''
        };
    }
}


export function validateInn(value) {
    if (!value) {
        return false;
    }
    if (value.match(/\D/)) {
        return true;
    }

    let isValid, message;

    const inn = value.match(/(\d)/g);

    if (inn.length === 10) {
        if (inn[9] === String(((
                2 * inn[0] + 4 * inn[1] + 10 * inn[2] +
                3 * inn[3] + 5 * inn[4] + 9 * inn[5] +
                4 * inn[6] + 6 * inn[7] + 8 * inn[8]
            ) % 11) % 10)) {
            return true;
        } else {
            return false;
        }
    }
    else if (inn.length === 12) {
        if (inn[10] === String(((
                7 * inn[0] + 2 * inn[1] + 4 * inn[2] +
                10 * inn[3] + 3 * inn[4] + 5 * inn[5] +
                9 * inn[6] + 4 * inn[7] + 6 * inn[8] +
                8 * inn[9]
            ) % 11) % 10) && inn[11] === String(((
                3 * inn[0] + 7 * inn[1] + 2 * inn[2] +
                4 * inn[3] + 10 * inn[4] + 3 * inn[5] +
                5 * inn[6] + 9 * inn[7] + 4 * inn[8] +
                6 * inn[9] + 8 * inn[10]
            ) % 11) % 10)) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

export function validateBik(value) {
    return /^04+/.test(value);
}

export function validateEmail(value) {
    // if (value.match(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i))
    if (value.match(/^[^#@А-Яа-я,"][\.0-9A-Za-z-_]{0,}\@(([0-9A-Za-z-_]{2,18}\.[A-Za-z-]{2,5})|([0-9A-Za-z-_]{2,18}\.[0-9A-Za-z-_]{2,18}\.[-A-Za-z]{2,5}))$/)) // https://www.w3.org/TR/html5/forms.html#valid-e-mail-address
    {
        return true
    } else {
        return false
    }
}

export function validateUrl(value) {
    if (value.match(/^(https?:\/\/)?([\w\.]+)\.([a-z]{2,6}\.?)(\/[\w\.]*)*\/?$/)) // https://habr.com/post/66931/
    {
        return true
    } else {
        return false
    }
}

export function validatePhone(value, register) {
    if (value.match(/[_]/g))
    {
        if (register) {
            return {
                isValid: false,
                message: 'недопустимое значение'
            };
        } else {
            return false
        }
    } else {
        if (register) {
            return {
                isValid: true,
                message: ''
            };
        } else {
            return true
        }
    }
}

export function validatePassword(value) {
    if (value.match(/^[a-zA-Z0-9]{6,64}$/g)) {
        return true
    } else {
        return false
    }
}

export function validateDeliveryPeriod(value, params) {
    const { minDays } = params;
    if (value >= minDays) {
        return {
            isValid: true,
            message: ''
        };
    } else {
        return {
            isValid: false,
            message: 'недопустимое значение'
        };
    }
}

export function validateIsRequired(value, isRequired) {
    if (isRequired) {
        if (value) {
            return true;
        } else {
            this.showError('Поле не может быть пустым');
            return false;
        }
    } else {
        return true;
    }
}

export function validateWordsLength(value, minWords, maxWords) {
    if (minWords || maxWords) {
        const wordsCount = (s) => {
            s = s.replace(/(^\s*)|(\s*$)/gi, ''); // exclude  start and end white-space
            s = s.replace(/[ ]{2,}/gi, ' '); // 2 or more space to 1
            s = s.replace(/\n /, '\n'); // exclude newline with a start spacing
            return s.split(' ').length;
        };

        if (minWords && wordsCount(value) < minWords) {
            this.showError(`Минимальное количество слов ${minWords}`);
            return false;
        } else if (maxWords && wordsCount(value) > maxWords) {
            this.showError(`Максимальное количество слов ${maxWords}`);
            return false;
        } else {
            return true;
        }
    } else {
        return true;
    }
}

export const validateRusCharacters = (string) => {
    string = string.replace(/\s/g, ''); // удаляем пробелы
    if (!string) {
        return true;
    }
    return /^[А-Яа-яё-]+$/.test(string);
};

export const validateIsGreaterOrEqual = (value, min) => {
    return value >= min;
};

export const validateIsLesserOrEqual = (value, max) => {
    return value <= max;
};

export const validateIsGreaterOrEqualOrEmpty = (value, min) => {
    return (value >= min && value !== 0 || value === 0)
};

