import React from 'react';
import { checkControlHints } from './Helpers';

const ShowHint = (props) => {
    const { existing_hints, existing_errors, messages } = props;
    if (!checkControlHints(existing_hints)) {
        return null;
    }
    // Если есть активная ошибка то прячем ошибку
    if (existing_errors && typeof existing_errors === 'object') {
        for (let error in existing_errors) {
            if (existing_errors[error]) {
                return null;
            }
        }
    }

    const rules = messages[checkControlHints(existing_hints)];

    return (
        <p className="Note">
            {rules}
        </p>
    );
};

export default ShowHint;