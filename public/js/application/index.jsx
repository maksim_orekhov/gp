import React from 'react';
import ReactDom from 'react-dom';
import CardNaturalPerson from './react/CardNaturalPerson';
import CardLegalEntity from './react/CardLegalEntity';
import CardSoleProprietor from './react/CardSoleProprietor';
import TableNaturalPersons from './react/tables/TableNaturalPersons';
import TableLegalEntities from './react/tables/TableLegalEntities';
import TableSoleProprietors from './react/tables/TableSoleProprietors';
import TablePaymentMethods from './react/tables/TablePaymentMethods';
import TableDeals from './react/tables/TableDeals';
import TableDealsExpired from './react/tables/TableDealsExpired';
import TableDealsDeadline from './react/tables/TableDealsDeadline';
import TableDealsTrouble from './react/tables/TableDealsTrouble';
import TablePaymentOrdersTrouble from './react/tables/TablePaymentOrdersTrouble';
import TableDealsDispute from './react/tables/TableDealsDispute';
import TableDealsDisputeOpen from './react/tables/TableDealsDisputeOpen';
import TableUsers from './react/tables/TableUsers';
import FormDeal from './react/forms/formsDeal/FormDeal';
import FormDealEdit from './react/forms/formsDeal/FormDealEdit';
import FormPartnerDeal from './react/forms/formsDeal/FormPartnerDeal';
import FormDealUnauthorized from './react/forms/formsDeal/FormDealUnauthorized';
import FormDiscountPaymentMethod from './react/forms/FormDiscountPaymentMethod';
import FormRefundPaymentMethod from './react/forms/FormRefundPaymentMethod';
import ControlPaymentMethod from './react/controls/ControlPaymentMethod';
import FormAddCivilLawSubjectToDeal from './react/forms/FormAddCivilLawSubjectToDeal';
import FormDelivery from './react/forms/FormDelivery';
import FormAddPaymentMethodToDeal from './react/forms/FormAddPaymentMethodToDeal';
import FormAddCivilLawSubjectAndPaymentMethodToDeal from './react/forms/FormAddCivilLawSubjectAndPaymentMethodToDeal';
import ControllerDisputeRequests from './react/controllers/ControllerDisputeRequests';
import FormLoginEdit from './react/forms/FormLoginEdit';
import ControllerEmailEdit from './react/controllers/ControllerEmailEdit';
import ControllerPhoneAdd from './react/controllers/ControllerPhoneAdd';
import ControllerPhoneEdit from './react/controllers/ControllerPhoneEdit';
import FormEmailConfirm from './react/forms/FormEmailConfirm';
import FormPhoneConfirmCode from './react/forms/FormPhoneConfirmCode';
import ControllerPayMandarin from './react/controllers/ControllerPayMandarin';
import ControllerCivilLawSubjectsTables from './react/controllers/ControllerCivilLawSubjectsTables';
import MandarinPaymentStatus from './react/MandarinPaymentStatus';
import FormDealInvitationResend from './react/forms/FormDealInvitationResend';
import FormPickupDate from './react/forms/FormPickupDate';
import FormButtonCreate from './react/forms/FormButtonCreate';
import FormButtonEdit from './react/forms/FormButtonEdit';
import DpdDeliveryPointPopup from './react/components/DpdDeliveryPointPopup';
import { BREAKPOINT_TABLET } from './react/constants/breakpoints';


//DPD second test
// Для того, чтобы вставить реактивный компонент в нужный блок на странице мы используем ID этого блока. Так как у нас пока не SPA (Single Page Application), то мы все компоненты добавляем на страницу именно таким образом. Название ID устанавливаем в соответствующем twig шаблоне. Если надо отключить реактивный компонент, то комментируем блок кода и в браузере будет рендериться twig шаблон.

//active tab of civil law subject after reloading the page
/* (() => {
    $('body').on('change', '.js-civil-law-subject', function () {
        window.sessionStorage.setItem("activeTabCivilLawSubject", $(this).attr("id"));
    });
    if (window.sessionStorage.getItem("activeTabCivilLawSubject")) {
        const activeTabId = window.sessionStorage.getItem("activeTabCivilLawSubject");
        const activeTab = $(`.js-civil-law-subject#${activeTabId}`);
        console.log('---', activeTab);
        if (activeTab.length) {
            activeTab.prop('checked', true).trigger('change');
            activeTab.siblings(`[for="${activeTabId}"]`).addClass('active').siblings('label').removeClass('active');
        }
    }
})(); */

// if (document.getElementById('natural-person')) {
//     const rootEl = document.getElementById('natural-person');
//     ReactDom.render(<TableNaturalPersons/>, rootEl);
// }

if (document.getElementById('profile-page-content')) {
    const rootEl = document.getElementById('profile-page-content');
    ReactDom.render(<ControllerCivilLawSubjectsTables/>, rootEl);
}

// if (document.getElementById('legal-entity')) {
//     const rootEl = document.getElementById('legal-entity');
//     ReactDom.render(<TableLegalEntities/>, rootEl);
// }
//
// if (document.getElementById('sole-proprietor')) {
//     const rootEl = document.getElementById('sole-proprietor');
//     ReactDom.render(<TableSoleProprietors/>, rootEl);
// }

if (document.getElementById('CardNaturalPerson')) {
    const rootEl = document.getElementById('CardNaturalPerson');
    ReactDom.render(<CardNaturalPerson/>, rootEl);
}

if (document.getElementById('CardLegalEntity')) {
    const rootEl = document.getElementById('CardLegalEntity');
    ReactDom.render(<CardLegalEntity/>, rootEl);
}

if (document.getElementById('CardSoleProprietor')) {
    const rootEl = document.getElementById('CardSoleProprietor');
    ReactDom.render(<CardSoleProprietor/>, rootEl);
}

if (document.getElementById('TablePaymentMethods')) {
    const rootEl = document.getElementById('TablePaymentMethods');
    ReactDom.render(<TablePaymentMethods/>, rootEl);
}

if (document.getElementById('TableDeals')) {
    const rootEl = document.getElementById('TableDeals');
    ReactDom.render(<TableDeals/>, rootEl);
}

if (document.getElementById('TableDealsTrouble')) {
    const rootEl = document.getElementById('TableDealsTrouble');
    ReactDom.render(<TableDealsTrouble/>, rootEl);
}

if (document.getElementById('TableDealsExpired')) {
    const rootEl = document.getElementById('TableDealsExpired');
    ReactDom.render(<TableDealsExpired/>, rootEl);
}

if (document.getElementById('TableDealsDeadline')) {
    const rootEl = document.getElementById('TableDealsDeadline');
    ReactDom.render(<TableDealsDeadline/>, rootEl);
}

if (document.getElementById('TableDealsDispute')) {
    const rootEl = document.getElementById('TableDealsDispute');
    ReactDom.render(<TableDealsDispute/>, rootEl);
}

if (document.getElementById('TableDealsDisputeOpen')) {
    const rootEl = document.getElementById('TableDealsDisputeOpen');
    ReactDom.render(<TableDealsDisputeOpen/>, rootEl);
}

if (document.getElementById('TablePaymentOrdersTrouble')) {
    const rootEl = document.getElementById('TablePaymentOrdersTrouble');
    ReactDom.render(<TablePaymentOrdersTrouble/>, rootEl);
}

if (document.getElementById('React-TableUsers')) {
    const rootEl = document.getElementById('React-TableUsers');
    ReactDom.render(<TableUsers/>, rootEl);
}

if (document.getElementById('FormDealEdit')) {
    const rootEl = document.getElementById('FormDealEdit');
    ReactDom.render(<FormDealEdit/>, rootEl);
}

if (document.getElementById('FormDeal')) {
    const rootEl = document.getElementById('FormDeal');
    ReactDom.render(<FormDeal/>, rootEl);
}

if (document.getElementById('form-deal-service-contractor')) {
    const rootEl = document.getElementById('form-deal-service-contractor');
    ReactDom.render(<FormDeal form_template='form_with_role_and_type' form_type='service_contractor' />, rootEl);
}

if (document.getElementById('form-deal-service-customer')) {
    const rootEl = document.getElementById('form-deal-service-customer');
    ReactDom.render(<FormDeal form_template='form_with_role_and_type' form_type='service_customer'/>, rootEl);
}

if (document.getElementById('form-deal-product-contractor')) {
    const rootEl = document.getElementById('form-deal-product-contractor');
    ReactDom.render(<FormDeal form_template='form_with_role_and_type' form_type='product_contractor'/>, rootEl);
}

if (document.getElementById('form-deal-product-customer')) {
    const rootEl = document.getElementById('form-deal-product-customer');
    ReactDom.render(<FormDeal form_template='form_with_role_and_type' form_type='product_customer'/>, rootEl);
}

if (document.getElementById('FormPartnerDeal')) {
    const rootEl = document.getElementById('FormPartnerDeal');
    ReactDom.render(<FormPartnerDeal/>, rootEl);
}

if (document.getElementById('FormDealUnauthorized')) {
    const rootEl = document.getElementById('FormDealUnauthorized');
    ReactDom.render(<FormDealUnauthorized/>, rootEl);
}

if (document.getElementById('form-unauth-deal-service-contractor')) {
    const rootEl = document.getElementById('form-unauth-deal-service-contractor');
    ReactDom.render(<FormDealUnauthorized form_template='form_with_role_and_type' form_type='service_contractor' />, rootEl);
}

if (document.getElementById('form-unauth-deal-service-customer')) {
    const rootEl = document.getElementById('form-unauth-deal-service-customer');
    ReactDom.render(<FormDealUnauthorized form_template='form_with_role_and_type' form_type='service_customer'/>, rootEl);
}

if (document.getElementById('form-unauth-deal-product-contractor')) {
    const rootEl = document.getElementById('form-unauth-deal-product-contractor');
    ReactDom.render(<FormDealUnauthorized form_template='form_with_role_and_type' form_type='product_contractor'/>, rootEl);
}

if (document.getElementById('form-unauth-deal-product-customer')) {
    const rootEl = document.getElementById('form-unauth-deal-product-customer');
    ReactDom.render(<FormDealUnauthorized form_template='form_with_role_and_type' form_type='product_customer'/>, rootEl);
}

if (document.getElementById('FormDiscountPaymentMethod')) {
    const rootEl = document.getElementById('FormDiscountPaymentMethod');
    ReactDom.render(<FormDiscountPaymentMethod/>, rootEl);
}

if (document.getElementById('FormRefundPaymentMethod')) {
    const rootEl = document.getElementById('FormRefundPaymentMethod');
    ReactDom.render(<FormRefundPaymentMethod/>, rootEl);
}

if (document.getElementById('ControlPaymentMethod')) {
    const rootEl = document.getElementById('ControlPaymentMethod');
    ReactDom.render(<ControlPaymentMethod/>, rootEl);
}

if (document.getElementById('DisputeRequests')) {
    const rootEl = document.getElementById('DisputeRequests');
    ReactDom.render(<ControllerDisputeRequests/>, rootEl);
}

if (document.getElementById('FormAddCivilLawSubjectToDeal')) {
    const rootEl = document.getElementById('FormAddCivilLawSubjectToDeal');
    ReactDom.render(<FormAddCivilLawSubjectToDeal/>, rootEl);
}

if (document.getElementById('FormAddPaymentMethodToDeal')) {
    const rootEl = document.getElementById('FormAddPaymentMethodToDeal');
    ReactDom.render(<FormAddPaymentMethodToDeal/>, rootEl);
}

if (document.getElementById('FormAddCivilLawSubjectAndPaymentMethodToDeal')) {
    const rootEl = document.getElementById('FormAddCivilLawSubjectAndPaymentMethodToDeal');
    ReactDom.render(<FormAddCivilLawSubjectAndPaymentMethodToDeal/>, rootEl);
}

if (document.getElementById('FormAddDeliveryToDeal')) {
    const rootEl = document.getElementById('FormAddDeliveryToDeal');
    ReactDom.render(<FormDelivery/>, rootEl);
}

if (document.getElementById('FormLoginEdit')) {
    const rootEl = document.getElementById('FormLoginEdit');
    ReactDom.render(<FormLoginEdit/>, rootEl);
}

if (document.getElementById('FormEmailEdit')) {
    const rootEl = document.getElementById('FormEmailEdit');
    ReactDom.render(<ControllerEmailEdit/>, rootEl);
}

if (document.getElementById('FormPhoneAdd')) {
    const rootEl = document.getElementById('FormPhoneAdd');
    ReactDom.render(<ControllerPhoneAdd/>, rootEl);
}

if (document.getElementById('FormPhoneEdit')) {
    const rootEl = document.getElementById('FormPhoneEdit');
    ReactDom.render(<ControllerPhoneEdit/>, rootEl);
}

if (document.getElementById('FormPhoneConfirm')) {
    const rootEl = document.getElementById('FormPhoneConfirm');
    ReactDom.render(<FormPhoneConfirmCode is_reload_on_success={true} is_closable={true} />, rootEl);
}

if (document.getElementById('FormEmailConfirm')) {
    const rootEl = document.getElementById('FormEmailConfirm');
    ReactDom.render(<FormEmailConfirm  is_reload_on_success={true} is_closable={true} />, rootEl);
}

if (document.getElementById('ControllerPayMandarin')) {
    const rootEl = document.getElementById('ControllerPayMandarin');
    ReactDom.render(<ControllerPayMandarin/>, rootEl);
}

if (document.getElementById('MandarinPaymentStatusProcessing')) {
    const rootEl = document.getElementById('MandarinPaymentStatusProcessing');
    ReactDom.render(<MandarinPaymentStatus/>, rootEl);
}

if (document.getElementById('deal-invitation-resend-form')) {
    const rootEl = document.getElementById('deal-invitation-resend-form');
    ReactDom.render(<FormDealInvitationResend/>, rootEl);
}

if (document.getElementById('form-pickup-date')) {
    const rootEl = document.getElementById('form-pickup-date');
    ReactDom.render(<FormPickupDate />, rootEl);
}

if (document.getElementById('CreateDealToken')) {
    const rootEl = document.getElementById('CreateDealToken');
    ReactDom.render(<FormButtonCreate/>, rootEl);
}

if (document.getElementById('form-button-edit')) {
    const rootEl = document.getElementById('form-button-edit');
    ReactDom.render(<FormButtonEdit/>, rootEl);
}

if (document.getElementById('reception-point-map')) {
    const rootEl = document.getElementById('reception-point-map');
    ReactDom.render(<DpdDeliveryPointPopup/>, rootEl);
}

import 'air-datepicker';
import '../landing/oldBrowserNotify';
import ProgressBar from './ProgressBar';
import {customFetch, getDeadlineDealsWithCreatedInMillisecondsField} from "./react/Helpers";
import URLS from "./react/constants/urls";

$(document).ready(function () {
    /**
     * Переключение табов (сейчас в профиле)
     */
    $('.js-tab__switcher').click(function () {
        const tabSwitcher = $(this);
        const tab = $(tabSwitcher.attr('data-tab'));

        if (tab.length > 0) {
            tab.closest('.js-tab-container').find('.js-tab.active').removeClass('active');
            tab.addClass('active');

            tabSwitcher.closest('.js-tab-panel').find('.js-tab__switcher.active').removeClass('active');
            tabSwitcher.addClass('active');
        }
    });

    /**
     * POPUP open
     */
    (() => {
        $('.js-popup-open').click(function () {
            const button = $(this);
            const popup = $(button.data('popup'));

            if (popup.length > 0) {
                popup.first().addClass('open');
            } else {
                const popup = $('.js-popup').toArray().find((el) => $(el).attr('id') === undefined);
                popup.classList.add('open');
            }

            // Ставим автовысоту попапа
            const currentPopup = $('.js-popup.open');
            const height = currentPopup.find('.Popup-Content').outerHeight();
            currentPopup.find('.Popup').height(`${height}px`);
        });

        // Клик вне popup-окна
        $('.Popup-Container').click(function (event) {
            if (!$(event.target).closest('.Popup').length) {
                $('.js-popup').removeClass('open');
            }
        });

        // Нажатие Esc
        $(window).on('keyup', function(event) {
            if (event.keyCode === 27) {
                $('.js-popup').removeClass('open');
            }
        });

        // Клик кнопки закрытия
        $('.js-popup-close').click(function () {
            const close = $(this);
            close.closest('.js-popup').removeClass('open');
        });
    })();

    (() => {
        $(".js-TimelineRectangle-Item").on('click', function () {
            let tooltip_message = $(this).find('.tooltip_message');

            if (!$(tooltip_message).length) {
                $(tooltip_message).toggle();
            } else {
                return false;
            }

        });
    })();

    const toggleTooltipDescription = function() {
        let tooltip_message = $(this).find('.tooltip_description');

        $(tooltip_message).toggle();
    };

    (() => {
        $(".js-TimelineRectangle-Item").hover(toggleTooltipDescription,toggleTooltipDescription);
    })();

    /**
     * Input file
     */
    (() => {
        const input = $('.js-input-file');

        const file_api = window.File && window.FileReader && window.FileList && window.Blob;

        input.change(function () {
            const input = $(this);
            if (file_api && input[0].files[0]) {
                const form = input.closest('.js-input-file__container');
                form.addClass('active');
            }
        });
    })();

    /**
     * Тоггл для изменения номера телефона, эл.почты и тд.
     */
    (() => {
        $('.js-data-changer').click(function (e) {
            e.preventDefault();

            const target = $($(this).data('target'));

            $('.js-verification').removeClass('appear');
            target.addClass('appear');

            $('html, body').animate({
                scrollTop: 0 // Временное решение т.к. формы в верху экрана
            }, 500);
        });
        $('.js-verification-close').click(function () {
            const container = $(this).closest('.js-verification');
            container.removeClass('appear');
        });
    })();

    (() => {
        $('.js-download-file').each(function () {
            let wrapper = $(this);
            let inp = wrapper.find('input');
            let lbl = wrapper.find('mark');

            // Crutches for the :focus style:

            inp.focus(function () {
                wrapper.addClass('focus');
            }).blur(function () {
                wrapper.removeClass('focus');
            });

            var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

            inp.change(function () {
                var file_name;

                if (file_api && inp[0].files[0]) {
                    file_name = inp[0].files[0].name;

                    $('.js-clear-download-file').show();
                    lbl.removeClass('notChange');
                } else {
                    file_name = inp.val().replace('C:\\fakepath\\', '');
                }

                if (!file_name.length) {
                    return;
                }

                if (lbl.is(':visible')) {
                    lbl.text(file_name);
                }
            }).change();
        });


        $('.js-clear-download-file').click(function () {
            let wrapper = $(this).parents('.js-download-file');
            let inp = wrapper.find('input');
            let lbl = wrapper.find('mark');
            let text = lbl.data('text-default');
            lbl.text(text);
            inp.val('');
            lbl.addClass('notChange');
            $('.js-clear-download-file').hide();
        });
    })();

    // При клике на строку таблицы имитируем поведение ссылки переходя на нужный адрес из атрибута data-path
    (() => {
        const tables = $('.js-table-rows-are-link');
        if (tables.length > 0) {
            tables.each(function () {
                const table = $(this);
                table.find('tr').each(function () {
                    const row = $(this);
                    const path = row.data('path');
                    const start = table.data('start');
                    const length = table.data('length');

                    if (path) {
                        if (start !== undefined && start > 0 && length !== undefined && length > 0) {
                            row.find('td').each(function (index) {
                                if (index + 1 >= start && index + 1 < length + start) {
                                    $(this).addClass('active');
                                    $(this).on('click', function () {
                                        window.location = path;
                                    });
                                }
                            });
                        } else {
                            row.addClass('active');
                            row.on('click', function () {
                                window.location = path;
                            });
                        }
                    }
                });
            });
        }
    })();

    //Разворачиваем текст левого меню из иконок на маленьком экране
    (() => {
        $('.js-LeftMenu-Toggle').click(function () {
            $('.js-left-menu').toggleClass('active');
        });

        var left_menu = $(('.js-left-menu'));
            $(document).mouseup(function (e) {
                if (left_menu.has(e.target).length === 0){
                    left_menu.removeClass('active');
                }
            });

        $(window).on('resize', function () {
            var win = $(this); //this = window
            if (win.width() >= 1200) {
                $('.js-left-menu').removeClass('active');
            }
        });
    })();

    // Тоггл для селектов метода решенияы спора
    (() => {
        const select = $('.js-SelectTabSwitcher');
        const container = select.closest('.js-TabContainer');
        if (select.length > 0) {
            select.on('change', function () {
                container.find('.js-tab.active').removeClass('active');
                $($(this).val()).addClass('active');
            });
        }
    })();

    //Загрузка аватара в профиле
    (() => {
        $('.js-input-file').change(function () {
            $(this).parents("form")[0].submit();
        });
    })();

    //Hide dropdown
    (() => {
        $(document).click(function (e) {
            if ($(e.target).closest(".js-dropdown-list").length == 0) {
                $('.js-dropdown-list-input').prop('checked', false);
            }
        });
    })();

    (() => {
        const input = $('.js-tab-input');
        if (input.length) {
            input.on('change', function () {
                const target = $(this).attr('id');
                $('.js-tab-btn.active').removeClass('active');
                $(`.js-tab-btn[for=${target}]`).addClass('active');
            });
        }
    })();

    // Таймеры для форм отправки
    (() => {
        const setMessage = ($form) => {
            const formId = $form.attr('id');
            switch (formId) {
                case 'js-change-mail':
                    return 'Вы сможете изменить адрес эл.почты через';
                case 'email-edit-form':
                    return 'Вы сможете изменить адрес эл.почты через';
                case 'js-change-number':
                    return 'Вы сможете изменить номер телефона через';
                default:
                    return 'Вы сможете повторно запросить код подтверждения через';
            }
        };

        function timer(start, period, $form) {
            const timeNow = Math.round(Date.now() / 1000);
            const diff = timeNow - start;
            const restTime = period - diff;

            const btn = $form.find('.js-form-timer-button');
            const message = $form.find('.js-form-timer-message');
            const hiddenElements = $form.find('.js-form-timer-hide');

            if (diff < period) {
                if (btn.length) btn.hide();
                if (hiddenElements.length) hiddenElements.hide();
                if (message.length) {
                    message.find('span').html(restTime);
                } else {
                    $form.append($(`<p class="js-form-timer-message">${setMessage($form)} <span class="TextAccent">${restTime}</span> секунд.</p>`));
                }
                setTimeout(function () {
                    timer(start, period, $form);
                }, 1000);
            } else {
                if (message.length) {
                    message.remove();
                }
                if (btn.length) btn.show();
                if (hiddenElements.length) hiddenElements.show();
            }
        }

        const forms = $('.js-form-timer');
        if (forms.length) {
            forms.each(function () {
                const start = $(this).data('start');
                const period = $(this).data('period');
                timer(start, period, $(this));
            });
        }
    })();

    // Выпадашка для Spoiler
    (() => {
        const spoilers = $('.js-spoiler');
        if (spoilers.length) {
            spoilers.each(function () {
                $(this).addClass('isJs');
                const toggle = $(this).find('.Spoiler-Toggle');

                toggle.on('click', function (e) {
                    const spoilerBody = $(this).closest('.js-spoiler').find('.Spoiler-Hidden');
                    spoilerBody.slideToggle();
                    const spoilerHistory = $(this).closest('.js-spoiler').find('.Spoiler-Toggle');
                    spoilerHistory.toggleClass('Spoiler-Toggle-active');
                    e.preventDefault();
                });
            });
        }
    })();

    //TimeLine scroll
    (() => {
        $('.js-timeline-wrap').css({'display': 'block'});
        const item = $(".js-timeline-item.active, .js-timeline-item.fail, .js-timeline-item.completed");
        if (item.length) {
            let wrap = item.parents('.js-timeline-wrap');
            if (wrap.length) {
                let offset_time_line_active = wrap.position().left;
                if (offset_time_line_active > 0) {
                    $('.js-timeline-container').scrollLeft(offset_time_line_active);
                }
            }
        }
    })();

    // Filter on the small screen
    (() => {
        //Закрываем поле фильтра даты
        $('.js-filter-date-close').click(function () {
            $(".filter-tab-input").prop('checked', false);
            // $('.js-filter-table-mobile').removeClass('active');
        });
    })();

    //filter datepicker
    (() => {
        const debounce = (func, wait, immediate) => {
            let timeout;
            return () => {
                const context = this, args = arguments;
                const later = function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                const callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        };

        //event after resize
        window.addEventListener('resize', debounce(initDatepicker ,100, false), false);
        //event after load pages
        initDatepicker();

        function initDatepicker() {
            if (window.innerWidth <= 992){
                setMobileDatepicker();
            } else {
                setDesctopDatepicker();
            }
        }

        function setMobileDatepicker() {
            let inputDatapicker = $('.InputDate2');
            inputDatapicker.each(function () {
                if (!$(this).hasClass('datapicker-mobile')){
                    $(this).data('datepicker').destroy();
                    $(this).datepicker({
                        inline: true
                    });
                    $(this).addClass('datapicker-mobile');
                }
            });
        }

        function setDesctopDatepicker() {
            let inputDatapicker = $('.InputDate2');
            inputDatapicker.each(function () {
                if ($(this).hasClass('datapicker-mobile')){
                    $(this).data('datepicker').destroy();
                    $(this).datepicker({
                        inline: false
                    });
                    $(this).removeClass('datapicker-mobile');
                }
            });
        }

        let testinputDatapicker = $('.InputDate2');
        testinputDatapicker.each(function () {
            let testdatepicker = $('.InputDate2').datepicker().data('datepicker');
        });

        let input = document.getElementsByClassName("InputDate2")[0];
        if (input) {
            let value = input.value;
        }

        let testChange = $('.InputDate2').on('keydown', function(e) {
            // $(this).val();
            e.preventDefault();
        });

    })();

    (() => {
        $('.DealShowPage-Description .Spoiler-Checkbox').on('change', function () {
            const $this = $(this);
            $this.closest('.DealShowPage-Description')
                .find('.Spoiler-Checkbox').not(this).prop('checked', false);
        })
    })();

    /**
     * Добавляет кнопкам эффект нажатия, как у кнопок из material design
     */
    document.addEventListener('mousedown', function (ev) {
        const target = ev.target;
        const rippleButton = target.closest('[data-ripple-button]');
        if(!rippleButton || rippleButton.classList.contains('is-disabled') || rippleButton.hasAttribute('disabled')){
            return false;
        }

        const rippleDiameter = Math.min(rippleButton.offsetHeight, rippleButton.offsetWidth, 100),
            buttonClientRect = rippleButton.getBoundingClientRect(),
            x = ev.pageX - buttonClientRect.x,
            y = ev.pageY - buttonClientRect.y - window.scrollY,
            rippleElement = document.createElement('div');

        rippleElement.className = 'ripple';
        rippleButton.appendChild(rippleElement);

        //При завершении анимации удаляем круг
        const handleRippleCircleAnimationEnd = function (ev) {
            ev.target.removeEventListener('animationend', handleRippleCircleAnimationEnd);
            rippleButton.removeChild(rippleElement);
        };

        const rippleCircle = document.createElement('div');
        rippleCircle.className = 'ripple_animated';
        //Берём цвет круга из значения дата-атрибута (по умолчанию #fff)
        rippleCircle.style.background = rippleButton.getAttribute('data-ripple-button');
        rippleCircle.style.width = `${rippleDiameter}px`;
        rippleCircle.style.height = `${rippleDiameter}px`;
        rippleCircle.style.left = `${x - rippleDiameter / 2}px`;
        rippleCircle.style.top = `${y - rippleDiameter / 2}px`;
        rippleCircle.addEventListener('animationend', handleRippleCircleAnimationEnd);

        rippleElement.appendChild(rippleCircle);
    });

    // DPD - обновление базы данных
    (() => {
        $('.js-start-dpd-update').on('click', function(e) {
            e.preventDefault();

            const $rootEl = $(this);

            let max = 0;
            let current = 0;

            const popup = $('<div class="Popup-Container open" style="background: rgba(0, 0, 0, .7);"><div class="Popup" style="position: relative;top: 50%;transform: translateY(-50%);"><div class="Popup-Content"></div></div></div>');

            const render = (progress_bar) => {
                progress_bar.appendTo(popup.find('.Popup-Content'));
                popup.insertAfter($rootEl);
            };

            const progress_bar = new ProgressBar({
                render
            });

            const form_type = $rootEl.data('type');
            const csrf = $rootEl.data('csrf');

            const body = JSON.stringify({
                form_type,
                csrf
            });

            let popupTitle = '';
            switch (form_type) {
              case 'point':
                  popupTitle = 'Обновление кандидата пунктов приема/доставки';
                  break;
              default:
                  popupTitle = 'Обновление кандидата городов';
            }
            popup.find('.Popup-Content').prepend(`<h2 style="font-size: 24px;text-align: center;">${popupTitle}</h2>`);

            const xhr = new XMLHttpRequest();
            xhr.open('POST', `/admin/delivery/dpd/update/${form_type}`, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

            xhr.onreadystatechange = function() {
                switch (this.readyState) {
                    case 2:
                        progress_bar.reset();
                        max = +this.getResponseHeader('x-progress-max');
                        break;
                    case 3:
                        if (this.responseText) {
                            const iterations = this.responseText.split(' ');

                            current = +iterations[iterations.length - 1];
                            if (typeof current !== 'number' || isNaN(current)) {
                                current = +iterations[iterations.length - 2];
                            }

                            if (current >= max) {
                                progress_bar.setReadyState();
                            } else {
                                const current_progress = Math.round((current / max) * 100);

                                progress_bar.update({
                                    progress: current_progress,
                                    text: `${current_progress} % (${current}/${max})`
                                });
                            }
                        }
                        break;
                    case 4:
                        const response_str = this.response.split('end').pop();
                        console.log(response_str);

                        const response = JSON.parse(response_str);

                        if (response.status === 'SUCCESS') {
                            window.location.reload();
                        } else {
                            popup.remove();
                            $(`<p style="color: red; margin-top: 20px">Ошибка сервера: ${response.message}</p>`).insertAfter($rootEl);
                            console.log(response);
                        }
                        break;
                    default:
                        console.log('default')
                }
            };

            xhr.send(body);
            progress_bar.setInitState();
        });
    })();

    const $formSideMenu = $('.form-side-menu');
    if($formSideMenu.length){
        $(window).on('scroll', () => {
            const $window = $(window);
            if($window.width() >= BREAKPOINT_TABLET){
                let stickyDiff = $window.scrollTop() - $formSideMenu.parent().offset().top + $('.Header').outerHeight();
                stickyDiff = stickyDiff < 0 ? 0 : stickyDiff;
                $formSideMenu.css('transform', `translateY(${stickyDiff}px)`);
            }
        });
    }

    (() => {
        if(document.getElementById('dpd-delivery-data')) {
            let element = document.getElementById('dpd-delivery-data'),
                sender_city_id = element.dataset.sender_city_id,
                receiver_city_id = element.dataset.receiver_city_id,
                weight = element.dataset.weight,
                terminal_code = element.dataset.terminal_code,
                self_pickup,
                self_delivery = 0,
                csrf = element.dataset.csrf,
                date_pickup = element.dataset.date_pickup.split("-").join(",");
            terminal_code ? self_pickup = 1 : self_pickup = 0;

            $.ajax({
                dataType: "json",
                type: 'POST',
                data: JSON.stringify({
                    sender_city_id,
                    receiver_city_id,
                    weight,
                    self_pickup,
                    self_delivery,
                    csrf
                }),
                url: '/delivery/dpd/city/cost',
                success: (data) => {
                    // document.getElementById('deal-conditions').classList.remove("Preloader");
                    if(document.getElementsByClassName('pre-shipping-cost') && data.data.cost) {
                        let shipping_cost = document.getElementsByClassName('pre-shipping-cost');
                        for (let i = 0; i < shipping_cost.length; i++) {
                            shipping_cost[i].innerHTML = data.data.cost + ' р.'
                        }
                    }
                    if(document.getElementsByClassName('delivery-time-period') && data.data.days) {
                        let delivery_days = document.getElementsByClassName('delivery-time-period');
                        for (let i = 0; i < delivery_days.length; i++) {
                            delivery_days[i].innerHTML = data.data.days + ' д.'
                        }
                    }
                    if(document.getElementsByClassName('date-delivery') && data.data.days) {
                        let date_delivery = document.getElementsByClassName('date-delivery');
                        let date = new Date(date_pickup);
                        let newDate = new Date(date);

                        newDate.setDate(newDate.getDate() + data.data.days);

                        let dd = newDate.getDate();
                        let mm = newDate.getMonth() + 1;
                        let y = newDate.getFullYear();

                        let formattedDate = dd + '.' + mm + '.' + y;
                        for (let i = 0; i < date_delivery.length; i++) {
                            date_delivery[i].innerHTML = 'Дата доставки: ' + formattedDate;
                        }
                    }
                }
            }).fail((error) => console.log(error));
        }
    })();
});