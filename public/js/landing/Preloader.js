/**
 * Прелоадер картинок
 * @param {array} images - массив с путями картинок
 * @param {function} callBack - Функция, вызываемая после загрузки всех картинок
 */
class Preloader {
    constructor(images = [], callBack = null){
        this._images = images.slice();
        this._callback = callBack;
    }

    load(){
        let imagesPromises = [];
        for(let i = 0; i < this._images.length; i++){
            imagesPromises.push(new Promise( resolve => {
                const img = new Image();
                img.src = this._images[i];
                if(!img.width){
                    resolve();
                    return;
                }
                img.onload = img.onerror = ev => resolve();
            }));
        }
        Promise.all(imagesPromises).then(() => {
            this._callback && this._callback();
        });
    }

}

export default Preloader;