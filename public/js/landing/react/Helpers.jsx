export const animateEndListener = element => new Promise((resolve, reject) => {
        element.classList.add('active');
        element.addEventListener("animationend", () => {
            resolve();
        }, false);
    });

export const clearTimeoutsAndIntervals = (component) => {
    if (typeof component.timeout !== 'undefined') {
        clearTimeout(component.timeout);
    }

    if (typeof component.interval !== 'undefined') {
        clearInterval(component.interval);
    }
};

export const getReCaptchaSiteKey = () => {
    const site_key_element = document.getElementById("recaptcha-site-key");
    if (site_key_element) {
        if (site_key_element.getAttribute("data-site-key")) {
            return site_key_element.getAttribute("data-site-key");
        }
    }
    console.log('reCaptcha site key is not defined');
    return null;
};

export const getReCaptchaSrc = () => {
    return 'https://www.google.com/recaptcha/api.js?hl=ru';
};