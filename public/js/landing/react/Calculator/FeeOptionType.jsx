import React from 'react';
import Ranger from './Ranger';

export default class FeeOptionType extends React.Component {
    constructor(props) {
        super(props);

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChangeEvent (e) {
        const value = e.target.value.replace(/\D/, '');
        this.props.vozvrat(this.props.name, Number(value));
    }

    handleChange (num) {
        this.props.vozvrat(this.props.name, Number(num));
    }

    render () {
        const { name, amount, id, title } = this.props;
        return (
            <div className='Field'>
                <label htmlFor={id}>{title}</label>
                <div className='select-icon'>
                    <select name={name} onChange={this.handleChangeEvent} value={amount} id={id}>
                        <option value='0'>Покупатель</option>
                        <option value='1'>50/50</option>
                        <option value='2'>Продавец</option>
                    </select>
                </div>
                {/*<Ranger vozvrat={this.handleChange} value={amount} range={2} step={1} />*/}
            </div>
        );
    }
}