import React from 'react';
import Control from './Control';
import FeeOptionType from './FeeOptionType';
import {
    DELAY_BETWEEN_ACTIONS, TYPING_SPEED_CALCULATOR, CHANCE_OF_DELAY_CALCULATOR, TYPING_WITH_DELAY_SPEED_CALCULATOR, CALCULATOR_DELAY_BETWEEN_AMOUNT,
    INPUT_CLEAR_SPEED_CALCULATOR
} from '../HowItWorks/config';
import { clearTimeoutsAndIntervals } from '../Helpers';


export default class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dealAmount: 30000,
            contractor: 30000,
            customer: 30600,
            feeType: 0,
            fee: 0.04
        };

        this.handleChange = this.handleChange.bind(this);

        this.amount_animation_value = `${this.state.dealAmount}`;
        this.amount_last_char_pos = this.amount_animation_value.length;
    }

    componentDidMount() {
        !this.props.is_paused && this.clearInputAnimation(1000);
    }

    componentWillReceiveProps(nextProps) {
        const { is_paused } = nextProps;

        if (is_paused !== this.props.is_paused) {
            if (is_paused) {
                clearTimeoutsAndIntervals(this);
            } else {
                this.clearInputAnimation(500);
            }
        }
    }

    componentWillUnmount() {
        clearTimeoutsAndIntervals(this);
    }

    getRandomAmount(min = 10, max = 100) {
        return `${Math.round(Math.random() * (max - min) + min) * 1000}`;
    }

    inputAutoFilling() {
        this.timeout = setTimeout(() => {
            let typing_speed = TYPING_SPEED_CALCULATOR;

            this.interval = setInterval(() => {
                // Делаем неравномерную скорость ввода
                const is_delay = Math.random() < CHANCE_OF_DELAY_CALCULATOR;

                // Если выпадает нужное рандомное число то делаем задержку ввода текста больше, такое поведение более естественно выглядит,
                // чем рандомная задержка на каждый символ ввода
                if (is_delay) {
                    typing_speed = TYPING_WITH_DELAY_SPEED_CALCULATOR;
                }

                if (this.amount_last_char_pos === this.amount_animation_value.length) { // Если поле полностью заполнено
                    clearTimeoutsAndIntervals(this);
                    this.clearInputAnimation();
                } else { // Если не заполнено до конца
                    this.amount_last_char_pos = this.amount_last_char_pos + 1;

                    this.switchCalculate('dealAmount', +this.amount_animation_value.slice(0, this.amount_last_char_pos));
                }
            }, typing_speed);
        }, DELAY_BETWEEN_ACTIONS);
    }

    // Анимация стирания текста инпута
    clearInputAnimation(custom_time = 0) {
        this.timeout = setTimeout(() => {
            this.interval = setInterval(() => {
                if (this.amount_last_char_pos === 0) { // Если поле очистилось
                    clearTimeoutsAndIntervals(this);
                    this.amount_animation_value = this.getRandomAmount();
                    this.inputAutoFilling();
                } else {
                    this.amount_last_char_pos = this.amount_last_char_pos - 1;

                    this.switchCalculate('dealAmount', +this.amount_animation_value.slice(0, this.amount_last_char_pos));
                }
            }, INPUT_CLEAR_SPEED_CALCULATOR);
        }, custom_time || CALCULATOR_DELAY_BETWEEN_AMOUNT);
    }

    getFeeAmount () {
        const { dealAmount, fee } = this.state;
        return Math.round(dealAmount * fee);
    }

    getCustomerFeeAmount () {
        const feeType = this.state.feeType;
        if (feeType === 0) {
            return this.getFeeAmount();
        } else if (feeType === 1) {
            return this.getFeeAmount() / 2;
        }
        return 0;
    }

    getContractorFeeAmount () {
        const feeType = this.state.feeType;
        if (feeType === 2) {
            return this.getFeeAmount();
        } else if (feeType === 1) {
            return this.getFeeAmount() / 2;
        }
        return 0;
    }

    setCustomerAmount () {
        this.setState({
            customer: this.state.dealAmount + this.getCustomerFeeAmount()
        });
    }

    setContractorAmount () {
        this.setState({
            contractor: this.state.dealAmount - this.getContractorFeeAmount()
        });
    }

    setDealAmount (name, value) {
        const { feeType, fee } = this.state;
        let dealAmount = value;

        if (name === 'contractor') {
            if (feeType === 2) {
                dealAmount = value / (1 - fee);
            } else if (feeType === 1) {
                dealAmount = value / (1 - fee / 2);
            }
        } else {
            if (feeType === 0) {
                dealAmount = value / (1 + fee);
            } else if (feeType === 1) {
                dealAmount = value / (1 + fee / 2);
            }
        }
        dealAmount = Math.round(dealAmount);

        this.setState({
            dealAmount
        }, () => {
            if (name === 'contractor') {
                this.setCustomerAmount();
            } else {
                this.setContractorAmount();
            }
        });
    }

    switchCalculate (name, value) {
        this.setState({
            [name]: value
        }, () => {
            if (name === 'dealAmount' || name === 'feeType') {
                this.setCustomerAmount();
                this.setContractorAmount();
            } else {
                this.setDealAmount(name, value);
            }
        });
    }

    handleChange(name, value) {
        this.switchCalculate(name, value);
    }

    render () {
        const { setPause } = this.props;
        const { dealAmount, contractor, customer, feeType, fee } = this.state;
        return (
            <div className='Calculation' onClick={setPause}>
                <h3>Калькулятор</h3>
                <div className="Calculation__container">
                    <div className="Item">
                        <Control
                            id='calculator-input'
                            title='Сумма сделки:'
                            name='dealAmount'
                            vozvrat={this.handleChange}
                            amount={dealAmount}
                            is_rubble={true}
                            inputRef={input => this.input = input}
                        />
                        <FeeOptionType
                            id='calculator-input-4'
                            title='Кто платит комиссию:'
                            name='feeType'
                            amount={feeType}
                            vozvrat={this.handleChange}
                        />
                        <div className='Field'>
                            <div className='Label'>
                                Комиссия сервиса
                            </div>
                            <div className='Text'>
                                {fee * 100}%
                            </div>
                        </div>
                    </div>
                    <div className="Item">
                        <Control
                            id='calculator-input-3'
                            title='Покупатель заплатит:'
                            name='customer'
                            vozvrat={this.handleChange}
                            amount={customer}
                            is_rubble={true}
                        />
                        <Control
                            id='calculator-input-2'
                            title='Продавец получит:'
                            name='contractor'
                            vozvrat={this.handleChange}
                            amount={contractor}
                            is_rubble={true}
                        />
                        <div className='Field'>
                            <div className='Label'>
                                Защита сделки
                            </div>
                            <div className='Text rubble-field'>
                                {Math.round(fee * dealAmount)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}