import React from 'react';

export default class Ranger extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            range: this.props.range || 100000,
            step: this.props.step || 500
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange (e) {
        const value = e.target.value.replace(/\D/, '');
        this.props.vozvrat(Number(value));
    }

    render () {
        const { value } = this.props;
        const { step, range } = this.state;
        return (
            <input className='range' type='range' step={step} value={value} min='0' max={range} onChange={this.handleChange} />
        );
    }
}