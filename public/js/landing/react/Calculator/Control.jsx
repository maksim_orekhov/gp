import React from 'react';
import Ranger from './Ranger';

export default class Control extends React.Component {
    constructor(props) {
        super(props);

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChangeEvent (e) {
        const value = e.target.value.replace(/\D/, '');
        this.props.vozvrat(this.props.name, Number(value));
    }

    handleChange (num) {
        this.props.vozvrat(this.props.name, Number(num));
    }

    render () {
        const { name, amount, step, range, id, title, is_rubble } = this.props;
        return (
            <div className='Field'>
                <label htmlFor={id}>{title}</label>
                <div className={is_rubble ? 'rubble-field' : ''}>
                    <input
                        name={name}
                        type='text'
                        onChange={this.handleChangeEvent}
                        value={amount}
                        id={id}
                        maxLength={15}
                    />
                </div>
                {/*<Ranger value={amount} vozvrat={this.handleChange} step={step} range={range} />*/}
            </div>
        )
    }
}