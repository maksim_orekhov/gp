import React from 'react';
import { STAGES_TIME, STAGES_TIME_MOBILE } from './config';


export default class timeline extends React.Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);

        this.radius = 23;
        this.circumference = 2 * 3.14 * this.radius;
        this.time = 0;
    }

    componentDidMount() {
        const { is_paused } = this.props;
        !is_paused && this.runTimer();
    }

    componentWillReceiveProps(nextProps) {
        const { stage, is_paused } = nextProps;

        if (stage !== this.props.stage) {
            clearInterval(this.interval);
            this.time = 0;

            !is_paused && this.runTimer(stage);
        }

        if (is_paused !== this.props.is_paused) {
            if (is_paused) {
                clearInterval(this.interval);
            } else {
                this.runTimer(stage, this.time);
            }
        }
    }

    handleClick(e) {
        const { handleStageChange } = this.props;
        const value = e.currentTarget.dataset.value;

        handleStageChange && handleStageChange(value);
    }

    getTimeline() {
        const { stage, steps, is_paused, togglePause }  = this.props;
        const rows = [];

        for (let i = 0; i < steps; i++) {
            // Неактивные пункты
            if (stage !== i) {
                rows.push(
                    <div
                        data-value={i}
                        onClick={this.handleClick}
                        key={i}
                        className='timeline__item'
                    >
                        <span className='timeline__text'>
                            <span className="timeline__stage">
                                { i + 1 }
                            </span>
                            <span className="timeline__textHidden">
                                <span className="icon-pause__container">
                                    {
                                        is_paused ?
                                            <span className="icon-pause" />
                                            :
                                            <span className="icon-play" />
                                    }
                                </span>
                            </span>
                        </span>
                    </div>
                );
            } else { // Активный пункт
                rows.push(
                    <div
                        data-value={i}
                        className='timeline__item active '
                        onClick={togglePause}
                        key={i}
                    >
                        {i + 1}
                        <svg
                            width="50"
                            height="50"
                        >
                            <circle
                                ref={svg => this.svg = svg}
                                stroke="#1863A4"
                                strokeWidth="3"
                                fill="transparent"
                                r={this.radius}
                                cx="25"
                                cy="25"
                                strokeDasharray={this.circumference}
                                strokeDashoffset={this.circumference}
                            />
                        </svg>
                        {
                            is_paused ? // Если пауза выводим значок паузы
                                <span className="icon-pause__container">
                                    <span className="icon-pause" />
                                </span>
                                :
                                <span className='timeline__text'>
                                    <span className="timeline__stage">
                                        { i + 1 }
                                    </span>
                                    <span className="timeline__textHidden">
                                        <span className="icon-pause__container">
                                            <span className="icon-pause" />
                                        </span>
                                    </span>
                                </span>
                        }
                    </div>
                );
            }
        }
        return rows;
    }

    runTimer(stage = this.props.stage, completed_time = 0) {
        const { is_mobile_version } = this.props;
        let stages_time = STAGES_TIME;

        if (is_mobile_version) {
            stages_time = STAGES_TIME_MOBILE
        }

        const full_time = stages_time[stage];
        const start = Date.now() - completed_time;

        if (full_time) {
            this.interval = setInterval(() => {
                this.time = Date.now() - start;
                let progress_procentage = this.time / full_time * 100;

                if (progress_procentage > 100) {
                    progress_procentage = 100;
                    clearInterval(this.interval);
                }

                $(this.svg).css('strokeDashoffset', this.circumference - progress_procentage / 100 * this.circumference);
            }, 20);
        } else {
            setTimeout(() => { // Асинхронность тут важна, поэтому 0 таймаут
                $(this.svg).css('strokeDashoffset', 0);
            }, 0);
        }
    }

    render() {
        return (
            <div className="timeline active">
                {
                    this.getTimeline()
                }
            </div>
        );
    }
};