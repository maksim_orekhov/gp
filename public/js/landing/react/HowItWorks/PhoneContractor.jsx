import React from 'react';
import Phone from './Phone';
import MessagesCreate from './screens/MessagesCreate';
import MessagesPayment from './screens/MessagesPayment';
import MessagesConfirm from './screens/MessagesConfirm';
import DealConditionsPrototype from './screens/DealConditionsPrototype';
import DealTrackNumberPrototype from './screens/DealTrackNumberPrototype';


export default class PhoneContractor extends Phone {
    constructor(props) {
        super(props);

        this.is_counteragent = true; // флажок, что телефон будет выезжать чуть позже второго.
    }

    render() {
        const { stage, triggerDependComponents, handleEndOfStage, animation_trigger, is_paused, is_message_screen_active, is_mobile_version } = this.props;

        return (
            <div className="phone" ref={phone => this.phone = phone}>
                <div className="phone__case">
                    <div className="phone__screen">
                        {
                            [
                                <MessagesCreate
                                    animation_trigger={animation_trigger}
                                    triggerNextStage={handleEndOfStage}
                                    is_paused={is_paused}
                                    is_message_screen_active={is_message_screen_active}
                                    is_mobile_version={is_mobile_version}
                                />,
                                <DealConditionsPrototype
                                    triggerDependComponents={triggerDependComponents}
                                    is_paused={is_paused}
                                />,
                                <MessagesPayment
                                    animation_trigger={animation_trigger}
                                    triggerNextStage={handleEndOfStage}
                                    is_paused={is_paused}
                                    is_message_screen_active={is_message_screen_active}
                                    is_mobile_version={is_mobile_version}
                                />,
                                <DealTrackNumberPrototype
                                    triggerDependComponents={triggerDependComponents}
                                    is_paused={is_paused}
                                />,
                                <MessagesConfirm
                                    animation_trigger={animation_trigger}
                                    triggerNextStage={handleEndOfStage}
                                    is_paused={is_paused}
                                    is_message_screen_active={is_message_screen_active}
                                    is_mobile_version={is_mobile_version}
                                />
                            ][stage]
                        }
                    </div>
                </div>
                <div className="phone__button-home"/>
                <div className="phone__loudspeaker"/>
                <div className="phone__button phone__button-block"/>
                <div className="phone__side-buttons">
                    <div className="phone__button phone__button-sound" />
                    <div className="phone__button phone__button-volume" />
                    <div className="phone__button phone__button-volume" />
                </div>
            </div>
        );
    }
};