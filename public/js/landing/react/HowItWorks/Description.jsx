import React from 'react';

export default class Description extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            content: [
                {
                    title: 'Создание',
                    text: 'Продавец или покупатель регистрируется на сайте сервиса «Гарант Пэй» и создает сделку. Другому участнику сделки будет отправлено приглашение присоединиться.'
                }, {
                    title: 'Согласование',
                    text: 'После регистрации обе стороны соглашаются с условиями сделки либо предлагают новые.'
                }, {
                    title: 'Оплата',
                    text: 'Покупатель переводит деньги на гарантийный счет сервиса. GuarantPay проверяет платеж и уведомляет продавца о том, что оплата за товар/услугу обеспечена.'
                }, {
                    title: 'Доставка',
                    text: 'После подтверждения оплаты продавец отправляет товар покупателю и прикрепляет трек-номер отправления.'
                }, {
                    title: 'Подтверждение',
                    text: 'Покупатель принимает товар/услугу. До истечения гарантийного срока он должен подтвердить выполнение сделки или открыть спор.'
                }, {
                    title: 'Способ оплаты',
                    text: 'Вы можете оплатить сделку с помощью банковской карты, или через расчетный счет в банке.'
                }
            ]
        };
    }

    componentDidMount() {
        this.description.classList.add('animate');

        setTimeout(() => {
            this.description.classList.add('active');
        }, 0);
    }

    componentWillReceiveProps(nextProps) {
        const { stage, prepare_to_next_stage } = nextProps;

        if (prepare_to_next_stage !== this.props.prepare_to_next_stage) {
            this.description.classList.add('animate');
            this.description.classList.remove('active');
        }

        if (stage !== this.props.stage) {
            this.description.classList.remove('animate');
            this.description.classList.remove('active');

            setTimeout(() => {
                this.description.classList.add('animate');
                this.description.classList.add('active');
            }, 0);
        }
    }

    render() {
        const { stage, handleCalculatorStage, calculator_stage } = this.props;
        const { content } = this.state;

        return (
            <div className="gp-demo__info">
                {
                    stage !== calculator_stage &&
                        <div className="gp-demo__info-content">
                            <div className='gp-demo__info-description' ref={description => this.description = description}>
                                <h3 className="gp-demo__stage-title"><span>{content[stage]['title']}</span></h3>
                                <p className="gp-demo__stage-text">{content[stage]['text']}</p>
                            </div>
                            <span onClick={handleCalculatorStage} className="gp-demo__calc-link">Рассчитать стоимость сделки</span>
                        </div>
                }
                {
                    stage === calculator_stage &&
                        <div>
                            <div className='gp-demo__info-description' ref={description => this.description = description}>
                                <h3 className="gp-demo__stage-title gp-demo__stage-title_stage_calculator">{content[stage]['title']}</h3>
                                <p className="gp-demo__stage-text">{content[stage]['text']}</p>
                            </div>
                            <div className="OurPartners">
                                <span className="PartnersIcon PartnersIcon_mastercard" />
                                <span className="PartnersIcon PartnersIcon_visa" />
                                <span className="PartnersIcon PartnersIcon_mir" />
                            </div>
                            <p className='gp-demo__payment-info'>
                                Информация о карте передается в процессинговый центр в зашифрованном виде по защищенному протоколу HTTPS/SSL.  Сервис «Гарант Пей» не имеет доступа к вводимой информации.
                            </p>
                        </div>
                }
            </div>
        )
    }
};