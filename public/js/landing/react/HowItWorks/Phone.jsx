import React from 'react';
import '../animations/Jquery-timing-functions';
import { PHONES_APPEAR_SPEED, PHONES_APPEAR_DELAY, COUNTERAGENT_PHONE_APPEAR_DELAY } from "./config";

export default class Phone extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let appear_delay = PHONES_APPEAR_DELAY;
        if (this.is_counteragent) { // Говорит о том, что это телефон контрагента и он должен выезжать с запозданием
            appear_delay = COUNTERAGENT_PHONE_APPEAR_DELAY;
        }
        this.phone.style.animationDelay = `${appear_delay}ms`;
        this.phone.style.animationDuration = `${PHONES_APPEAR_SPEED}ms`;
        this.animateAppear();
    }

    componentWillReceiveProps(nextProps) {
        const { stage, is_end_of_stage } = nextProps;

        this.checkNewStage(stage, this.props.stage); // Проверка что новый этап и нужно сделать анимацию проявления;
        this.checkEndOfStage(is_end_of_stage, this.props.is_end_of_stage); // Проверка что конец этапа и нужно спрятать телефоны
    }

    // Проверка что новый этап и нужно сделать анимацию проявления;
    checkNewStage(next_value, current_value) {
        if (next_value !== current_value) {
            clearTimeout(this.timeout);
            if (this.animation) {
                this.animation.stop(true);
            }
            this.animateAppear();
        }
    }

    // Проверка что конец этапа и нужно спрятать телефоны
    checkEndOfStage(next_value, current_value) {
        if (next_value !== current_value) {
            const { handleNextStage } = this.props;
            this.animateDisappear()
                .then(() => {
                    handleNextStage && handleNextStage();
                });
        }
    }

    animateAppear() {
        return new Promise((resolve, reject) => {
            this.phone.style.transform = `translateY(1000px)`;
            this.phone.classList.remove('phone-disappear');
            this.phone.classList.remove('phone-appear');
            setTimeout(() => {
                this.phone.classList.add('phone-appear');
            },0);
            const handleAnimationEnd = () => {
                this.phone.removeEventListener('animationend', handleAnimationEnd);
                resolve();
            };
            this.phone.addEventListener('animationend', handleAnimationEnd);
        });
    }

    animateDisappear() {
        return new Promise((resolve, reject) => {
            this.phone.style.transform = `translateY(0)`;
            this.phone.classList.remove('phone-disappear');
            this.phone.classList.remove('phone-appear');
            setTimeout(() => {
                this.phone.classList.add('phone-disappear');
            },0);

            const handleAnimationEnd = () => {
                this.phone.removeEventListener('animationend', handleAnimationEnd);
                resolve();
            };
            this.phone.addEventListener('animationend', handleAnimationEnd);
        });
    }
};