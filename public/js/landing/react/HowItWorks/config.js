import React from 'react';

export const STEPS = 6; // Кол-во пунктов в таймлайн
export const MOBILE_BREAK_POINT = 767; // Мобильное разрешение при котором
export const AREA_OF_TRIGGER_APPEAR = 60; // Область в пикселях в которой активируется появление

export const CUSTOMER_MESSAGES = [
    <span>Иван Иванов подтведил «GT AVALANCHE». Сделка заключена! От вас ожидается оплата товара.</span>,
    <span>RB454004001SG Почта России трек номер-отправления «GT AVALANCHE». После получения товара пожалуйста подтвердите доставку.</span>
];
export const CONTRACTOR_MESSAGES = [
    <span>Иван Иванов пригласил Вас в качестве продавца для проведения безопасной сделки «GT AVALANCHE»</span>,
    <span>Иван Иванов перевел оплату по сделке «GT AVALANCHE» на гарантийный счет. Вам необходимо выполнить обязательства по договору до 21.06.18</span>,
    <span>Сбербанк онлайн. Guarantpay перевел(а) вам 49 000 RUB.</span>
];

export const DELAY_BETWEEN_TWO_PHONES = 300; // Время между анимациями на двух телефонах
export const DELAY_BETWEEN_SLIDES = 3000; // Время перехода между двумя слайдами, т.е. этапа 1 к этапу 2 на таймлайн
export const DELAY_BETWEEN_ACTIONS = 300; // Время между действиями, например нажатия кнопки и появления смс

/**
 * Анимация выезжания телефона
 */
export const PHONES_APPEAR_SPEED = 1000; // Скорость выезжания телефона
export const PHONES_APPEAR_DELAY = 0; // Задержка перед выезжанием телефона
export const DELAY_SECOND_PHONE_RELATIVE_TO_FIRST = 200; // Задержка между появлением второго телефона относительно первого
export const COUNTERAGENT_PHONE_APPEAR_DELAY = PHONES_APPEAR_DELAY + DELAY_SECOND_PHONE_RELATIVE_TO_FIRST;


/**
 * Задержка перед началом анимаций вызванная выезжанием телефона
 */
export const DELAY_BEFORE_START_SLIDE_ANIMATIONS = COUNTERAGENT_PHONE_APPEAR_DELAY + PHONES_APPEAR_SPEED;
/**
 * Анимация автозаполнения полей
 */
export const TYPING_SPEED = 100; // Скорость набора текста
export const CHANCE_OF_DELAY = 0; // Вероятность того что буква будет введена с задержкой, эмуляция естественного ввода(поиска клавиши)
export const TYPING_WITH_DELAY_SPEED = TYPING_SPEED * 3; // Скорость срабатывания события ввода с задержкой


/**
 * Анимация кнопок
 */
// !!!ВАЖНО!!!
// Это число символическое, на самом деле скорость анимации кнопок выставляется в css phone-button-apply.less,
// здесь оно объявлено только ради высчитывания примерного времени анимации этапа
const BUTTON_ANIMATE_SPEED = 300;


/**
 * Анимация сообщений
 */
export const MESSAGE_MOVE_SPEED = 300; // Скорость движения сообщения вниз
export const MESSAGE_APPEAR_SPEED = 500; // Скорость проявления сообщения (opacity)
// Задержка анимаций на экранах смс в мобильной версиии, здесь 1000 это время анимации в css классе smooth компонента phones.less
export const MESSAGE_SCREEN_DELAY_MOBILE = 1000 + DELAY_BETWEEN_ACTIONS;

/**
 * Конфиг экранов
 */
// функция подсчета времени заполнения инпута
const getInputAutoFillingTime = (str, start_pos) => {
    const rest_of_symbols = str.length - start_pos; // Количество символов которое осталось ввести
    // Количество символов введенных с задержкой, основанная на вероятности задержки ввода CHANCE_OF_DELAY
    const count_of_typo_symbols = Math.floor(rest_of_symbols * CHANCE_OF_DELAY);
    // Общее время ввода инпута
    return (rest_of_symbols - count_of_typo_symbols) * TYPING_SPEED + count_of_typo_symbols * TYPING_WITH_DELAY_SPEED;
};
/**
 * DealCreatePrototype
 */
export const DEAL_CREATE_EMAIL = 'petrov.petr@mail.ru';
export const DEAL_CREATE_EMAIL_START_SYMBOL_POSITION = 0;
// Общее время ввода инпута
const DEAL_CREATE_EMAIL_TYPING_SPEED = getInputAutoFillingTime(DEAL_CREATE_EMAIL, DEAL_CREATE_EMAIL_START_SYMBOL_POSITION) + DELAY_BETWEEN_ACTIONS;
/**
 * DEAL_PAYMENT_PROTOTYPE
 */
export const DEAL_PAYMENT_OWNER = 'IVAN IVANOV';
export const DEAL_PAYMENT_OWNER_START_SYMBOL_POSITION = 0;
// Общее время ввода инпута
const DEAL_PAYMENT_OWNER_TYPING_SPEED = getInputAutoFillingTime(DEAL_PAYMENT_OWNER, DEAL_PAYMENT_OWNER_START_SYMBOL_POSITION) + DELAY_BETWEEN_ACTIONS;


/**
 * Таймлайн, время заполнения пунктов
 */
const BUTTON_FULL_TIME_ANIMATION = BUTTON_ANIMATE_SPEED + DELAY_BETWEEN_ACTIONS;
const MESSAGE_FULL_TIME_ANIMATION = MESSAGE_APPEAR_SPEED + DELAY_BETWEEN_ACTIONS;
// Базовое время для каждого этапа, это появление телефонов, анимация смс, время между этапами и т.д.
const BASE_DELAY = DELAY_BEFORE_START_SLIDE_ANIMATIONS + DELAY_BETWEEN_TWO_PHONES + DELAY_BETWEEN_SLIDES + MESSAGE_FULL_TIME_ANIMATION;

// Время каждого этапа
export const STAGES_TIME = [
    // DealCreate stage
    BASE_DELAY + DEAL_CREATE_EMAIL_TYPING_SPEED + BUTTON_FULL_TIME_ANIMATION,
    // DealConditions stage
    BASE_DELAY + BUTTON_FULL_TIME_ANIMATION,
    // DealPayment stage
    BASE_DELAY + DEAL_PAYMENT_OWNER_TYPING_SPEED + BUTTON_FULL_TIME_ANIMATION,
    // DealTrackingNumber stage
    BASE_DELAY + BUTTON_FULL_TIME_ANIMATION * 2,
    // DealConfirm stage
    BASE_DELAY + BUTTON_FULL_TIME_ANIMATION
];

// Таймлайн мобильный вариант
const MESSAGE_FULL_TIME_ANIMATION_MOBILE = MESSAGE_APPEAR_SPEED + MESSAGE_SCREEN_DELAY_MOBILE;
// Базовое время для каждого этапа, это появление телефонов, анимация смс, время между этапами и т.д.
const BASE_DELAY_MOBILE = DELAY_BEFORE_START_SLIDE_ANIMATIONS + DELAY_BETWEEN_TWO_PHONES + DELAY_BETWEEN_SLIDES + MESSAGE_FULL_TIME_ANIMATION_MOBILE;

// Время каждого этапа
export const STAGES_TIME_MOBILE = [
    // DealCreate stage
    BASE_DELAY_MOBILE + DEAL_CREATE_EMAIL_TYPING_SPEED + BUTTON_FULL_TIME_ANIMATION,
    // DealConditions stage
    BASE_DELAY_MOBILE + BUTTON_FULL_TIME_ANIMATION,
    // DealPayment stage
    BASE_DELAY_MOBILE + DEAL_PAYMENT_OWNER_TYPING_SPEED + BUTTON_FULL_TIME_ANIMATION,
    // DealTrackingNumber stage
    BASE_DELAY_MOBILE + BUTTON_FULL_TIME_ANIMATION * 2,
    // DealConfirm stage
    BASE_DELAY_MOBILE + BUTTON_FULL_TIME_ANIMATION
];

/**
 * Калькулятор
 */
export const TYPING_SPEED_CALCULATOR = 200;
export const INPUT_CLEAR_SPEED_CALCULATOR = 100;
export const CHANCE_OF_DELAY_CALCULATOR = 0.2; // Вероятность того что буква будет введена с задержкой, эмуляция естественного ввода(поиска клавиши)
export const TYPING_WITH_DELAY_SPEED_CALCULATOR = TYPING_SPEED_CALCULATOR * 2; // Скорость срабатывания события ввода с задержкой
export const CALCULATOR_DELAY_BETWEEN_AMOUNT = 5000; // Задержка между полностью введеной суммой и вводом новой
