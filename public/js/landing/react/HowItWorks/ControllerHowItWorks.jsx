import React from 'react';
import Timeline from './Timeline';
import Phones from './Phones';
import Calculator from './../Calculator/Calculator';
import Description from './Description';
import { STEPS, MOBILE_BREAK_POINT, AREA_OF_TRIGGER_APPEAR } from './config';

export default class ControllerHowItWorks extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            stage: 0,
            is_paused: true,
            is_appeared: false, // Флаг того что элемент в центре экрана и нужно запустить анимацию появления, срабатывает только 1 раз
            is_mobile_version: false, // Флаг что маленький экран и нужно использовать мобильную версию
            prepare_to_next_stage: 0 // Флаг что готовиться переход к новому этапу и нужно спрятать текст
        };

        this.steps = STEPS;
        this.calculator_stage = this.steps - 1; // номер этапа калькулятор

        // Переменная для того чтобы отрегулировать поведение блока взависимости от его положения во вьюпорте
        // Это нужно для того чтобы когда блок появился во вьюпорте поставить is_paused: true
        // и пока блок не выйдет из вьюпорта повторно не обновлять state в is_paused: true на каждый scroll
        this.is_component_in_viewport = false;

        this.setStageFromTimeline = this.setStageFromTimeline.bind(this);
        this.setCalculatorStage = this.setCalculatorStage.bind(this);
        this.triggerNextStage = this.triggerNextStage.bind(this);
        this.togglePause = this.togglePause.bind(this);
        this.setPause = this.setPause.bind(this);
        this.handlePrepareToNextStage = this.handlePrepareToNextStage.bind(this);
    }

    componentWillMount() {
        if ($(window).width() < MOBILE_BREAK_POINT) {
            this.setState({
                is_mobile_version: true
            });
        }
        $(window).on('resize', () => {
            const { is_mobile_version } = this.state;
            const screen_width = $(window).width();

            if (!is_mobile_version && screen_width < MOBILE_BREAK_POINT) {
                this.setState({
                    is_mobile_version: true
                });
            } else if (is_mobile_version && screen_width >= MOBILE_BREAK_POINT) {
                this.setState({
                    is_mobile_version: false
                })
            }
        });
    }

    componentDidMount() {
        this.checkComponentInViewPort();
        this.componentBehaviorOnScroll();
    }

    checkComponentInViewPort() {
        const { is_appeared } = this.state;

        const element = $(this.node);

        const elementPositionCenter = element.offset().top + element.outerHeight() / 2;
        const window_height = $(window).height();
        const viewportTop = $(window).scrollTop();
        const viewportCenter =  viewportTop + window_height / 2;
        const is_play_zone = Math.abs(viewportCenter - elementPositionCenter) < window_height / 2;

        if (!is_appeared && is_play_zone) {
            // Это условие отвечает за первый рендер компонента и срабатывает только один раз
            this.is_component_in_viewport = true;
            this.setState({
                is_paused: false,
                is_appeared: true
            });
        }
    }

    componentBehaviorOnScroll() {
        const element = $(this.node);

        $(window).on('scroll', () => {
            const { is_appeared } = this.state;

            const elementPositionCenter = element.offset().top + element.outerHeight() / 2;

            const window_height = $(window).height();
            const viewportTop = $(window).scrollTop();
            const viewportCenter =  viewportTop + window_height / 2;

            const is_play_zone = Math.abs(viewportCenter - elementPositionCenter) < window_height / 1.5;
            const appear_zone = Math.abs(viewportCenter - elementPositionCenter) < window_height / 1.5;

            if (!is_appeared && appear_zone) {
                // Это условие отвечает за первый рендер компонента и срабатывает только один раз
                this.is_component_in_viewport = true;
                this.setState({
                    is_paused: false,
                    is_appeared: true
                });
            } else if (is_appeared && is_play_zone && !this.is_component_in_viewport) {
                // Условие отвечает за срабатывания play когда элемент попадает в зону видимости
                this.is_component_in_viewport = true;
                this.setState({
                    is_paused: false
                });
            } else if (is_appeared && !is_play_zone && this.is_component_in_viewport) {
                // Условие отвечает за остановку play когда элемент попадает в зону видимости
                this.is_component_in_viewport = false;
                this.setState({
                    is_paused: true
                });
            }
        });
    }

    togglePause() {
        const is_paused = !this.state.is_paused;

        this.setState({
           is_paused
        });
    }

    setPause() {
        if (!this.state.is_paused) {
            this.setState({
                is_paused: true
            });
        }
    }

    handlePrepareToNextStage() {
        const prepare_to_next_stage = this.state.prepare_to_next_stage + 1;

        this.setState({
            prepare_to_next_stage
        });
    }

    setStageFromTimeline(stage) {
        this.setStage(stage);
    }

    setCalculatorStage() {
        this.setStage(this.calculator_stage);
    }

    // Переключает этап компонента, т.е. переход с этапа 1 на 2 и тд
    triggerNextStage() {
        let stage = this.state.stage + 1;

        if (stage > this.steps - 1) {
            stage = 0;
        }

        this.setStage(stage);
    }

    setStage(stage) {
        this.setState({
            stage: Number(stage)
        });
    }

    render() {
        const { stage, is_paused, is_appeared, is_mobile_version, prepare_to_next_stage } = this.state;
        return (
            <div className="gp-demo" ref={node => this.node = node}>
                {
                    is_appeared &&
                    <Description
                        stage={stage}
                        calculator_stage={this.calculator_stage}
                        handleCalculatorStage={this.setCalculatorStage}
                        prepare_to_next_stage={prepare_to_next_stage}
                    />
                }
                <div className="gp-demo__gallery">
                    {
                        stage !== this.calculator_stage && is_appeared &&
                            <div className="gp-demo__phones">
                                <Phones
                                    stage={stage}
                                    triggerNextStage={this.triggerNextStage}
                                    is_paused={is_paused}
                                    is_mobile_version={is_mobile_version}
                                    handlePrepareToNextStage={this.handlePrepareToNextStage}
                                />
                            </div>
                    }
                    {
                        stage === this.calculator_stage &&
                        <Calculator
                            is_paused={is_paused}
                            setPause={this.setPause}
                        />
                    }
                    {
                        is_appeared &&
                        <Timeline
                            stage={stage}
                            steps={this.steps}
                            calculator_stage={this.calculator_stage}
                            handleStageChange={this.setStageFromTimeline}
                            togglePause={this.togglePause}
                            is_paused={is_paused}
                            is_mobile_version={is_mobile_version}
                        />
                    }
                </div>
            </div>
        );
    }
}