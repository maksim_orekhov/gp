import React from 'react';
import Fade from '../../animations/Fade';
import CurrentTime from './components/CurrentTime';
import { DELAY_BETWEEN_ACTIONS, DELAY_BETWEEN_SLIDES, MESSAGE_APPEAR_SPEED, MESSAGE_MOVE_SPEED, MESSAGE_SCREEN_DELAY_MOBILE } from '../config';
import { clearTimeoutsAndIntervals } from '../../Helpers';


export default class MessagesScreen extends React.Component {
    constructor(props) {
        super(props);

        this.current_animation = 'message';
        this.animations = {
            message: this.runAnimations.bind(this),
            trigger_next_stage: this.runNextStage.bind(this)
        };
    }

    componentDidMount() {
        this.hideLastMessage();
    }

    componentWillReceiveProps(nextProps) {
        const { animation_trigger, is_paused, is_message_screen_active, is_mobile_version } = nextProps;

        let delay = DELAY_BETWEEN_ACTIONS;
        if (is_mobile_version) {
            delay = MESSAGE_SCREEN_DELAY_MOBILE;
        }

        if (animation_trigger !== this.props.animation_trigger && !is_paused) {
            this.timeout = setTimeout(() => {
                this.runAnimations();
            }, delay);
        }

        if (is_paused !== this.props.is_paused && is_message_screen_active) {
            if (is_paused) {
                clearTimeoutsAndIntervals(this);
            } else {
                this.animations[this.current_animation]();
            }
        }
    }

    componentWillUnmount() {
        clearTimeoutsAndIntervals(this);
    }

    hideLastMessage() {
        const messages_count = this.props.messages.length;
        this.last_message_height = $(this.node).height();

        this.last_message_margin_top = $(this.node).css('marginTop');

        $(this.node).css('height', 0);

        if (messages_count === 1) {
            $(this.node).css('marginTop', -40);
        } else {
            $(this.node).css('marginTop', 0);
        }
    }

    runAnimations() {
        this.current_animation = 'message';
        // this.interval = this.scrollToBottom(); // Прокручивание дива с сообщениями вниз, чтобы добить эффекта всплытия

        $(this.message).animate({
            opacity: 1
        }, MESSAGE_APPEAR_SPEED);

        this.timeout = setTimeout(() => {
            $(this.node).animate({
                height: `${this.last_message_height}px`,
                marginTop: this.last_message_margin_top

            }, MESSAGE_MOVE_SPEED, () => {
               /* const container = this.node.closest('.js-phone-screen');
                console.log(container.scrollTop, container.scrollHeight);
                container.scrollTop = container.scrollHeight;*/


                // clearInterval(this.interval); // После конца анимации перестаем скроллить контейнер с сообщениями
                this.current_animation = 'trigger_next_stage';
                this.runNextStage();
            });
        }, 0); // Нулевая задержка чтобы добавить небольшую асинхронность
    }

    runNextStage() {
        this.current_animation = 'trigger_next_stage';
        const { triggerNextStage } = this.props;

        this.timeout = setTimeout(() => {
            triggerNextStage && triggerNextStage(); // Запускаем следующий слайд
        }, DELAY_BETWEEN_SLIDES);
    }

    scrollToBottom() {
        const container = this.node.closest('.js-phone-screen');
        return setInterval(() => {
            container.scrollTop = container.scrollHeight;
        }, 0);
    }

    render() {
        const { messages } = this.props;

        return (
            <Fade>
                <div className="phone-message-app">
                    <div className="phone-header">
                        <div className="phone-network">
                            <div className="phone-network__gsm">
                                <span/>
                                <span/>
                                <span/>
                                <span/>
                                <span/>
                            </div>
                            <div className="phone-network__type">
                                GUARANTPAY
                            </div>
                        </div>
                        <div className="phone-battery">
                            <div className="phone-battery__value">42%</div>
                            <div className="phone-battery__volume"/>
                        </div>
                    </div>
                    <CurrentTime />
                    <div className="phone-message-app__container js-phone-screen">
                        {
                            messages.map((message, index) => {
                                // Если это сообщение НЕПОСЛЕДНЕЕ то рэндерим статичное сообщение
                                if (index + 1 !== messages.length) {
                                    return (
                                        <div className='phone-message-app__item' key={index}>
                                            <div className='phone-message'>
                                                <div className="phone-message-head">
                                                    <div className="phone-message-head__app-name">СООБЩЕНИЯ</div>
                                                    <div className="phone-message-head__when">сейчас</div>
                                                </div>
                                                <div className="phone-message__from">Guarant Pay</div>
                                                <div className="phone-message__text">
                                                    {message}
                                                </div>
                                            </div>
                                        </div>
                                    );
                                // Если это сообщение последнее
                                } else if (index + 1 === messages.length) {
                                    return (
                                        <div className='phone-message-app__item' ref={node => this.node = node} key={index}>
                                            <div
                                                className='phone-message hidden'
                                                ref={message => this.message = message}
                                            >
                                                <div className="phone-message-head">
                                                    <div className="phone-message-head__app-name">СООБЩЕНИЯ</div>
                                                    <div className="phone-message-head__when">сейчас</div>
                                                </div>
                                                <div className="phone-message__from">Guarant Pay</div>
                                                <div className="phone-message__text">
                                                    {message}
                                                </div>
                                            </div>
                                        </div>
                                    );
                                }
                            }).reverse()
                        }
                    </div>
                </div>
            </Fade>
        );
    }
};