import React from 'react';
import Slide from './Slide';
import { DELAY_BETWEEN_ACTIONS } from '../config';
import { animateEndListener } from '../../Helpers';
import Fade from "../../animations/Fade";

export default class DealTrackNumberPrototype extends Slide {
    constructor(props) {
        super(props);

        this.current_animation = 'add';
        this.animations = {
            add: this.buttonAddClicking.bind(this),
            send: this.buttonSend.bind(this),
            trigger_sms: this.runNextComponent.bind(this)
        };
    }

    buttonAddClicking() {
        return new Promise((resolve, reject) => {
           resolve(this.buttonClicking(this.button_add, 'send', 'add'));
        });
    }

    buttonSend() {
        return new Promise((resolve, reject) => {
            resolve(this.buttonClicking(this.button, 'trigger_sms', 'send'));
        });
    }

    render() {
        return (
            <Fade>
                <div className="Form deal-track-number">
                    <div className="phone-text-field">
                        <div className="phone-text-field__label">Транспортная служба</div>
                        <div className="phone-text-field__input is-valid">Почта России</div>
                    </div>
                    <div className="phone-text-field">
                        <div className="phone-text-field__label">Трек-номер</div>
                        <div className="phone-text-field__input is-valid">RB454004001SG</div>
                    </div>
                    <div className="phone-button-apply" ref={button_add => this.button_add = button_add}>Добавить трек-номер</div>
                    <div className="phone-file-field">
                        <div className="phone-text-field">
                            <div className="phone-text-field__label">Квитанция на доставку</div>
                            <div className="phone-text-field__input">Выберите файл...</div>
                        </div>
                    </div>
                    <div className="phone-button-apply" ref={button => this.button = button}>Прикрепить</div>
                </div>
            </Fade>
        )
    }
};