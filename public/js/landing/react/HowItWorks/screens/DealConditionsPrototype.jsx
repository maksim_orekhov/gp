import React from 'react';
import Slide from './Slide';
import Fade from '../../animations/Fade';

export default class DealConditionsPrototype extends Slide {
    constructor(props) {
        super(props);

        this.current_animation = '';
        this.animations = {
            button: this.buttonClicking.bind(this),
            trigger_sms: this.runNextComponent.bind(this)
        };
    }

    render() {
        return (
            <Fade>
                <div className="Form deal-conditions">
                    <div className="deal-conditions__header">
                        Условия сделки
                    </div>
                    <div className="deal-conditions-list">
                        <div className="phone-deal-condition">
                            <div className="phone-deal-condition__title">Время исполнения:</div>
                            <div className="phone-deal-condition__value">7 дней</div>
                        </div>
                        <div className="phone-deal-condition">
                            <div className="phone-deal-condition__title">Описание:</div>
                            <div className="phone-deal-condition__value">Велосипед Хардтейл Колёса - 29, Год - 2018</div>
                        </div>
                        <div className="phone-deal-condition">
                            <div className="phone-deal-condition__title">Цена:</div>
                            <div className="phone-deal-condition__value">50 000 ₽</div>
                        </div>
                        <div className="phone-deal-condition">
                            <div className="phone-deal-condition__title">Комиссия:</div>
                            <div className="phone-deal-condition__value">1 000 ₽</div>
                        </div>
                        <div className="deal-conditions__footer">
                            <div>Итого:</div>
                            <div>49 000 ₽</div>
                        </div>
                    </div>

                    <div className="phone-button-apply" ref={button => this.button = button}>Подтвердить</div>
                </div>
            </Fade>
        )
    }
};