import React from 'react';
import { animateEndListener, clearTimeoutsAndIntervals } from '../../Helpers';
import { 
    DELAY_BETWEEN_ACTIONS, 
    TYPING_SPEED, 
    DELAY_BEFORE_START_SLIDE_ANIMATIONS, 
    CHANCE_OF_DELAY, 
    TYPING_WITH_DELAY_SPEED,
    DELAY_BETWEEN_TWO_PHONES
} from '../config';


export default class Slide extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (!this.props.is_paused) {
            this.timeout = setTimeout(() => {
                this.runAnimations();
            }, DELAY_BEFORE_START_SLIDE_ANIMATIONS);
        }
    }

    componentWillReceiveProps(nextProps) {
        const { is_paused } = nextProps;

        if (is_paused !== this.props.is_paused) {
            this.handlePauseChange(is_paused);
        }
    }

    componentWillUnmount() {
        clearTimeoutsAndIntervals(this);
    }

    /**
     * Очередь функций анимации
     * @param {String} animation_name - имя текущей анимации
     */
    runAnimations(animation_name) {
        if (animation_name === 'done') {
            return;
        }
        return (async () => {
            const animations_names = Object.keys(this.animations);
            let start_animation_index = 0;
            if (animation_name) {
                const current_animation = animations_names.findIndex(animation => animation === animation_name);
                start_animation_index = current_animation > 0 ? current_animation : start_animation_index;
            }
            for (let i = start_animation_index; i < animations_names.length; i++) {
                await this.animations[animations_names[i]]();
            }
        })();
    }

    inputAutoFilling(current_animation = 'input') {
        const is_auto_focus_input =  window.outerWidth > 1400;

        this.input.classList.add('is-focused');

        is_auto_focus_input && $(this.input).focus();
        this.current_animation = current_animation;

        return new Promise((resolve, reject) => {
            this.timeout = setTimeout(() => {
                let typing_speed = TYPING_SPEED;

                this.interval = setInterval(() => {
                    // Делаем неравномерную скорость ввода
                    const is_delay = Math.random() < CHANCE_OF_DELAY;

                    // Если выпадает нужное рандомное число то делаем задержку ввода текста больше, такое поведение более естественно выглядит,
                    // чем рандомная задержка на каждый символ ввода
                    if (is_delay) {
                        typing_speed = TYPING_WITH_DELAY_SPEED;
                    }

                    if (this.value_last_char_pos === this.final_value.length) { // Если поле полностью заполнено
                        clearInterval(this.interval);

                        this.input.classList.remove('is-focused');
                        this.input.classList.add('is-valid');

                        is_auto_focus_input && $(this.input).blur();
                        resolve();

                    } else { // Если не заполнено до конца
                        is_auto_focus_input && $(this.input).focus();
                        this.value_last_char_pos = this.value_last_char_pos + 1;

                        this.setState({
                            value: this.final_value.slice(0, this.value_last_char_pos)
                        });
                    }
                }, typing_speed);
            }, DELAY_BETWEEN_ACTIONS);
        });
    }

    buttonClicking(element = this.button, next_animation = 'trigger_sms', current_animation = 'click') {
        this.current_animation = current_animation;
        return new Promise((resolve, reject) => {
            this.timeout = setTimeout(() => {
                this.current_animation = next_animation;
                resolve(animateEndListener(element));
            }, DELAY_BETWEEN_ACTIONS);
        });
    }

    handlePauseChange(is_paused) {
        if (is_paused) {
            clearTimeoutsAndIntervals(this);
        } else {
            this.runAnimations(this.current_animation);
        }
    }

    runNextComponent(current_animation = 'trigger_sms') {
        this.current_animation = current_animation;
        const { triggerDependComponents } = this.props;

        this.timeout = setTimeout(() => {
            this.current_animation = 'done';
            triggerDependComponents && triggerDependComponents();
        }, DELAY_BETWEEN_TWO_PHONES);
    }
};