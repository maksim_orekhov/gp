import React from 'react';
import MessagesScreen from './MessagesScreen';
import { CONTRACTOR_MESSAGES } from '../config';

const MessagesPayment = (props) => {
    return (
        <MessagesScreen
            messages={CONTRACTOR_MESSAGES.slice(0, 2)}
            animation_trigger={props.animation_trigger}
            triggerNextStage={props.triggerNextStage}
            is_paused={props.is_paused}
            is_message_screen_active={props.is_message_screen_active}
            is_mobile_version={props.is_mobile_version}
        />
    );
};

export default MessagesPayment;