import React from 'react';

const CurrentTime = (props) => {
    const current_date = new Date();

    const current_minutes = current_date.getMinutes();
    const current_hours = current_date.getHours();

    const minutes = current_minutes < 10 ? `0${current_minutes}` : current_minutes;
    const hours = current_hours < 10 ? `0${current_hours}` : current_hours;

    const time = `${hours}:${minutes}`;

    const months = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентрября', 'Октября', 'Ноября', 'Декабря'];
    const week_names = [ 'Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

    const date = `${week_names[current_date.getDay()]}, ${current_date.getDate()} ${months[current_date.getMonth()]}`;

    return (
        <div className="phone-message-app_time">
            <div className="fulltime">
                <div className="fulltime__time">
                    {time}
                </div>
                <div className="fulltime__date">
                    {date}
                </div>
            </div>
        </div>
    );
};

export default CurrentTime;