import React from 'react';
import Slide from './Slide';
import Fade from '../../animations/Fade';

export default class DealTrackNumberPrototype extends Slide {
    constructor(props) {
        super(props);
        this.current_animation = '';
        this.animations = {
            button: this.buttonClicking.bind(this),
            trigger_sms: this.runNextComponent.bind(this)
        };
    }

    render() {
        return (
            <Fade>
                <div className="Form deal-confirm">
                    <div className="phone-header">
                        <div className="phone-header__burger"/>
                        <div className="phone-header__logo"/>
                        <div className="phone-header__user-logo"/>
                    </div>
                    <div className="deal-confirm__body">
                        <div className="payment-success">
                            Оплата прошла успешно
                        </div>
                        <div className="track-numbers-list">
                            <div className="track-numbers-list__link">Список трек-номеров</div>
                            <div className="track-numbers-list__item">RB454004001SG</div>
                            <div className="track-numbers-list__provider">Почта России</div>
                        </div>
                        <div className="delivery-receipt delivery-receipt-file">
                            <span className="delivery-receipt__text">Квитанция на доставку</span>
                        </div>
                        <div className="phone-button-apply" ref={button => this.button = button}>Подтвердить</div>
                        <div className="phone-button-apply phone-button-apply_type_default">ОТКРЫТЬ СПОР</div>
                        <div className="delivery-receipt delivery-receipt-info">
                            <span className="delivery-receipt__text">Варианты решения спора</span>
                        </div>
                    </div>
                </div>
            </Fade>
        )
    }
};