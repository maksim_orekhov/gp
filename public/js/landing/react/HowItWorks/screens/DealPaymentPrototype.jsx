import React from 'react';
import Slide from './Slide';
import Fade from '../../animations/Fade';
import { DEAL_PAYMENT_OWNER, DEAL_PAYMENT_OWNER_START_SYMBOL_POSITION } from '../config';

export default class DealPaymentPrototype extends Slide {
    constructor(props) {
        super(props);

        this.final_value = DEAL_PAYMENT_OWNER;
        this.value_last_char_pos = DEAL_PAYMENT_OWNER_START_SYMBOL_POSITION;

        this.state = {
            value: this.final_value.slice(0, this.value_last_char_pos)
        };

        this.current_animation = '';
        this.animations = {
            input: this.inputAutoFilling.bind(this),
            button: this.buttonClicking.bind(this),
            trigger_sms: this.runNextComponent.bind(this)
        };
    }

    render() {
        return (
            <Fade>
                <div className="Form deal-payment">
                    <div className="phone-text-field">
                        <div className="phone-text-field__label">Номер карты</div>
                        <div className="phone-text-field__input is-valid">4929&nbsp;&nbsp;&nbsp;5099&nbsp;&nbsp;&nbsp;4710&nbsp;&nbsp;&nbsp;6878</div>
                    </div>
                    <div className="phone-text-field">
                        <div className="phone-text-field__label">Действительна до</div>
                        <div className="phone-text-fields-wrapper">
                            <div className="phone-text-field__input is-valid">08</div>
                            <div className="phone-text-fields-wrapper__divider">/</div>
                            <div className="phone-text-field__input is-valid">21</div>
                        </div>
                    </div>
                    <div className="phone-text-field">
                        <div className="phone-text-field__label">CVV2/CVC2 </div>
                        <div className="phone-text-fields-wrapper">
                            <div className="phone-text-field__input is-valid">08</div>
                            <div className="phone-text-fields-wrapper__divider">&nbsp;</div>
                            <div className="deal-payment__methods">
                                <div className="PartnersIcon_mastercard" />
                                <div className="PartnersIcon_visa" />
                            </div>
                        </div>
                    </div>
                    <div className="phone-text-field">
                        <div className="phone-text-field__label">Имя владельца карты</div>
                        <input
                            type="text"
                            value={this.state.value}
                            className="phone-text-field__input"
                            ref={input => this.input = input}
                            onChange={() => {}}
                        />
                    </div>

                    <div className="phone-button-apply" ref={button => this.button = button}>Оплатить</div>
                </div>
            </Fade>
        )
    }
};