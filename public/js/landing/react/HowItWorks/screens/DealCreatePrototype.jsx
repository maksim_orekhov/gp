import React from 'react';
import Slide from './Slide';
import Fade from '../../animations/Fade';
import { DEAL_CREATE_EMAIL, DEAL_CREATE_EMAIL_START_SYMBOL_POSITION } from '../config';


export default class DealCreatePrototype extends Slide {
    constructor(props) {
        super(props);

        this.final_value = DEAL_CREATE_EMAIL;
        this.value_last_char_pos = DEAL_CREATE_EMAIL_START_SYMBOL_POSITION;

        this.state = {
            value: this.final_value.slice(0, this.value_last_char_pos)
        };

        this.current_animation = '';
        this.animations = {
            input: this.inputAutoFilling.bind(this),
            button: this.buttonClicking.bind(this),
            trigger_sms: this.runNextComponent.bind(this)
        };
    }

    render() {
        return (
            <Fade>
                <div className="deal-create-prototype" ref={form => this.form = form}>
                    <div className="phone-header">
                        <div className="phone-header__logo"/>
                        <div className="phone-header__burger"/>
                    </div>
                    <div className="phone__body">
                        <div className="phone-radio-button">
                            <div className="phone-radio-button__input" />
                            <div className="phone-radio-button__label">Продавец</div>
                        </div>
                        <div className="phone-radio-button">
                            <div className="phone-radio-button__input phone-radio-button__input_is-selected" />
                            <div className="phone-radio-button__label">50/50</div>
                        </div>
                        <div className="phone-select-field">
                            <div className="phone-select-field__label">
                                Вы участвуете как:
                            </div>
                            <div className="phone-select-field__input is-valid">
                                Иванов Иван
                            </div>
                        </div>
                        <div className="phone-select-field">
                            <div className="phone-select-field__label">
                                Получение оплаты:
                            </div>
                            <div className="phone-select-field__input is-valid">
                                Банк «Открытие»
                            </div>
                        </div>
                        <div className="phone-text-field">
                            <div className="phone-text-field__label">
                                Email покупателя:
                            </div>
                            <input
                                type="text"
                                value={this.state.value}
                                className="phone-text-field__input"
                                ref={input => this.input = input}
                                onChange={() => {}}
                            />
                        </div>
                        <div className="phone-button-apply" ref={button => this.button = button}>Создать сделку</div>
                    </div>
                </div>
            </Fade>
        )
    }
};