import React from 'react';
import Phone from './Phone';
import DealCreatePrototype from './screens/DealCreatePrototype';
import MessagesConditions from './screens/MessagesConditions';
import MessagesTrackNumber from './screens/MessagesTrackNumber';
import DealPaymentPrototype from './screens/DealPaymentPrototype';
import DealConfirmPrototype from './screens/DealConfirmPrototype';


export default class PhoneCustomer extends Phone {
    constructor(props) {
        super(props);
    }

    render() {
        const { stage, triggerDependComponents, handleEndOfStage, animation_trigger, is_paused, is_message_screen_active, is_mobile_version } = this.props;

        return (
            <div className="phone phone--dark" ref={phone => this.phone = phone}>
                <div className="phone__case">
                    <div className="phone__screen">
                        {
                            [
                                <DealCreatePrototype
                                    triggerDependComponents={triggerDependComponents}
                                    is_paused={is_paused}
                                />,
                                <MessagesConditions
                                    animation_trigger={animation_trigger}
                                    triggerNextStage={handleEndOfStage}
                                    is_paused={is_paused}
                                    is_message_screen_active={is_message_screen_active}
                                    is_mobile_version={is_mobile_version}
                                />,
                                <DealPaymentPrototype
                                    triggerDependComponents={triggerDependComponents}
                                    is_paused={is_paused}
                                />,
                                <MessagesTrackNumber
                                    animation_trigger={animation_trigger}
                                    triggerNextStage={handleEndOfStage}
                                    is_paused={is_paused}
                                    is_message_screen_active={is_message_screen_active}
                                    is_mobile_version={is_mobile_version}
                                />,
                                <DealConfirmPrototype
                                    triggerDependComponents={triggerDependComponents}
                                    is_paused={is_paused}
                                />
                            ][stage]
                        }
                    </div>
                </div>
                <div className="phone__button-home"/>
                <div className="phone__loudspeaker"/>
                <div className="phone__button phone__button-block"/>
                <div className="phone__side-buttons">
                    <div className="phone__button phone__button-sound" />
                    <div className="phone__button phone__button-volume" />
                    <div className="phone__button phone__button-volume" />
                </div>
            </div>
        );
    }
};