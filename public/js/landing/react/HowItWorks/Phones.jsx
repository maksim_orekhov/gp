import React from 'react';
import PhoneContractor from './PhoneContractor';
import PhoneCustomer from './PhoneCustomer';

export default class Phones extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            is_end_of_stage: 0, // Флажок что этап закончился и телефоны должны запустить анимацию исчезновения
            is_trigger_next_component: 0,
            is_message_screen_active: false,
            class_name: '' // Имя класса для мобильных анимаций
        };

        this.handleNextStage = this.handleNextStage.bind(this);
        this.triggerNextComponent = this.triggerNextComponent.bind(this);
        this.handleEndOfStage = this.handleEndOfStage.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { stage, is_mobile_version } = nextProps;

        if (stage !== this.props.stage) {
            this.setState({
                is_message_screen_active: false
            }, () => {
                is_mobile_version && this.checkMobileAnimations(stage, false);
            });
        }
    }

    // Запускает анимацию в телефоне противоположного юзера
    triggerNextComponent() {
        const { is_mobile_version } = this.props;
        const is_trigger_next_component = this.state.is_trigger_next_component + 1;

        this.setState({
            is_trigger_next_component,
            is_message_screen_active: true
        }, () => {
            is_mobile_version && this.checkMobileAnimations();
        });

    }

    // Механизм запускания анимации прятанья телефона
    handleEndOfStage() {
        const { handlePrepareToNextStage } = this.props;
        const is_end_of_stage = this.state.is_end_of_stage + 1;

        handlePrepareToNextStage && handlePrepareToNextStage();

        this.setState({
            is_end_of_stage
        });
    }

    handleNextStage() {
        const { triggerNextStage } = this.props;

        triggerNextStage && triggerNextStage();
    }

    // is_smooth - означет что анимация перехода должна быть плавной, т.е. если этап меняется через клик по таймлайн,
    // то если анимация будет плавной то телефоны будут появляться по диагонали, а такой эффект нам не нужен,
    // а вот если анимация перехода от телефона к телефону на одном этапе то она должна быть плавной чтобы добиться эффект выезжания телефона сбоку
    checkMobileAnimations(stage = this.props.stage, is_smooth = true) {
        const { is_message_screen_active } = this.state;

        const toggle = stage % 2;
        let class_name = '';

        if (is_smooth) {
            class_name = 'smooth'
        }

        if (toggle) {
            if (!is_message_screen_active) {
                class_name =  `${class_name} active`;
            }
        } else {
            if (is_message_screen_active) {
                class_name = `${class_name} active`;
            }
        }

        this.setState({
            class_name
        });
    }

    render() {
        const { is_trigger_next_component, is_message_screen_active, is_end_of_stage, class_name } = this.state;
        const { stage, is_paused, is_mobile_version } = this.props;

        return (
            <div className={`phones ${is_mobile_version ? class_name : ''}`}>
                <PhoneCustomer
                    stage={stage}
                    triggerDependComponents={this.triggerNextComponent}
                    handleEndOfStage={this.handleEndOfStage}
                    is_end_of_stage={is_end_of_stage}
                    animation_trigger={is_trigger_next_component}
                    is_paused={is_paused}
                    is_message_screen_active={is_message_screen_active}
                    is_mobile_version={is_mobile_version}
                />
                <PhoneContractor
                    stage={stage}
                    triggerDependComponents={this.triggerNextComponent}
                    handleEndOfStage={this.handleEndOfStage}
                    handleNextStage={this.handleNextStage} // Т.к. этот телефон прячется последним то он запускает новый этап
                    is_end_of_stage={is_end_of_stage}
                    animation_trigger={is_trigger_next_component}
                    is_paused={is_paused}
                    is_message_screen_active={is_message_screen_active}
                    is_mobile_version={is_mobile_version}
                />
            </div>
        );
    }
}