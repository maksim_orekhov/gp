import ReactCSSTransitionGroup from 'react-addons-css-transition-group' ;
import React from 'react';

const Fade = (props) => {
        return (
            <ReactCSSTransitionGroup
                transitionName="fade"
                transitionAppear={true}
                transitionAppearTimeout={500}
                transitionEnterTimeout={500}
                transitionLeaveTimeout={300}
            >
                {props.children}
            </ReactCSSTransitionGroup>
        )
};

export default Fade;