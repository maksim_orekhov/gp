import React from 'react';

import ForgotPassword from './forms/ForgotPasswordForm/ForgotPassword.jsx';
import SuccessForm from './forms/SuccessForm.jsx';



export default class ForgotPasswordLinkController extends React.Component {
    constructor() {
        super();
        this.state = {
            current_form: 'forgot_password',
            user_login: null
        };

        this.handleNext = this.handleNext.bind(this);
    };

    handleNext(next_form){
        this.setState({
            current_form: next_form
        });
    }

    formRender(){
        let forms = {
            'forgot_password': <ForgotPassword nextStep={this.handleNext} />,
            'forgot_password_success_message':
                <SuccessForm
                    nextStep={this.handleNext}
                    nextStepName="finish_registration"
                />,
        };

        let current_form = forms[this.state.current_form];
        return current_form;
    }

    render() {
        return (
            <div>
                {this.formRender()}
            </div>
        );
    }

}

