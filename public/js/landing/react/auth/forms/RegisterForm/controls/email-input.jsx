import React from 'react';
import ShowError from '../../../../../../application/react/ShowError.jsx';
import ShowErrorRegistration from '../../messages/ShowError.jsx';
import ShowHint from '../../messages/ShowHint.jsx';
import update from 'immutability-helper';
import EmailBase from "./EmailBase";
import MESSAGES from "../../../../../../application/react/constants/messages";
import ControlBase from "../../../../../../application/react/controls/ControlBase";


export default class EmailInput extends ControlBase {
  constructor() {
    super();
    this.state = {
      disable: false,
      validation_errors:{
          reg_exp_invalid:      null,
          email_unavailable:    false,
          connection_is_lost:   false,
          email:                false,
          undefined_error:      false,
      },
      onFocus: ' ',
      hints:{
          email_rules: false
      },

        /**
         * объект messages используется для отображения ошибок в форме регистрации
         * объект error_messages используется для отображения ошибок в форме изменения email в профиле
        **/

      messages:{
        errors:{
            reg_exp_invalid: {
                header: "Неправильный формат E-mail.",
                text:   "Адрес не должен начинаться со знака '@' и содержать кириллицу."
            },
            email_unavailable:{
                header: "Уже используется",
                text:   "Эта почта уже есть в системе. Возможно вы регистрировались у нас ранее?"
            },
            connection_is_lost:{
                header: "Соединение с сервером потеряно",
                text:   "Проверьте ваше соединение с интернетом и попробуйте еще раз"
            },
            undefined_error: {
                header: "Ошибка обработки запроса",
                text:   "Произошла неизвестная ошибка обработки запроса. Попробуйте обновить страницу и повторить запрос заново"
            }
        },
        hints: {
            email_rules:{
                header: "Формат ввода E-mail:",
                text:   "mail@example.com"
            }
        }
      },
      error_messages: {
        isEmpty: MESSAGES.VALIDATION_ERRORS.isEmpty,
        reg_exp_invalid: MESSAGES.VALIDATION_ERRORS.email_reg_exp_invalid,
        email_unavailable: MESSAGES.VALIDATION_ERRORS.email_unavailable,
        undefined_error: ''
      }
    };

      this.handleChangeEvent = this.handleChangeEvent.bind(this);
      this.handleBlur = this.handleBlur.bind(this);
      this.handleFocus = this.handleFocus.bind(this);
  };

    componentWillMount() {
        let invitationEmail = this.props.invitationEmail;
        let formEmail = this.props.email;

        //invitation email
        if (invitationEmail && invitationEmail !== null && invitationEmail.length > 0){
            this.setState({
                disable: true
            });
        }
        if(formEmail && formEmail !== null && formEmail.length > 0){
            const { handleComponentValidation, name, validate_prop = `${name}_is_valid` } = this.props;

            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(formEmail));

            this.processValidation({
                target: {
                    value: this.props.email
                }
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors, email, register, hint_messages, hints } = nextProps;

        if (!register) {
            form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);

            email && this.validateControl(nextProps);
        }
    }

    validateControl(nextProps) {
        const { email, handleComponentValidation, name, validate_prop = `${name}_is_valid` } = nextProps;
        if (email && email !== this.props.email) {
            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(email));
            this.processValidation({
                target: {
                    value: email
                }
            });
        }
    }

    handleChangeEvent(e) {
        const value = e.target.value;
        // {props} register - флаг для определения находимся ли мы в форме регистрации или в форме редактирования
        const { name = 'email', handleComponentChange, handleComponentValidation, register, componentValue, validate_prop = `${name}_is_valid` } = this.props;

        if (!register) {
            handleComponentChange && handleComponentChange(name, value);
            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
            this.processValidation(e);
        }
        componentValue && componentValue(name, value);
    }

    emailIsValid(trueOrFalse) {
        const { register } = this.props;
        if (register) {
            this.props.isValid('emailIsValid',trueOrFalse);            // отправка состояния компонента глобальному компоненту
        }
    };

    setHint() {
        this.setState({
            hints: {
                email_rules: true
            }
        });
    };

    hideHint() {
        this.setState({
            hints: {
                email_rules: false
            }
        });
    };

    setEmailUnavailable(boolean) {
        const newData = update(this.state.validation_errors, {
            email_unavailable: {$set: boolean}
        });
        this.setState({
            validation_errors:newData
        });
    };

    fetchConnectionSuccess(data) {
        console.log(data);
        if(data.status === 'SUCCESS'){
            this.emailIsValid(true);
            this.setEmailUnavailable(false);
        }
        else if(data.status === 'ERROR'){
            this.emailIsValid(false);
            this.setEmailUnavailable(true);
        }
        else{
            this.phoneIsValid(false);
        }
        return data;
    };

    fetchConnectionFail(err) {
        this.setConnectionIsLost(true);
        this.emailIsValid(false);
        return err;
    };

    handleFocus() {
        this.setState({
            onFocus : 'has-focus'
        });
        this.showHint();
    }

    processValidation(e) {
        this.setState({
            onFocus :  ' '
        });
        let regExp = '^[^#@А-Яа-я,"][\\.0-9A-Za-z-_]{0,}\\@(([0-9A-Za-z-_]{2,18}\\.[A-Za-z-]{2,5})|([0-9A-Za-z-_]{2,18}\\.[0-9A-Za-z-_]{2,18}\\.[-A-Za-z]{2,5}))$';
        let ajaxCheckUniqueOptions ={
            "url": "/register/check/email",
            "field": "email"
        };

        this.hideHint();
        if(this.validationRegExp(e.target.value, regExp)){
            if (!this.props.not_check_unique) {
                this.fetchValidation(e.target.value, ajaxCheckUniqueOptions)
                    .then(
                        data => this.fetchConnectionSuccess(data),
                        error => this.fetchConnectionFail(error)
                    );
            } else {
                this.emailIsValid(true);
            }
        }
        else{
            this.emailIsValid(false);
        }
    };

    updateErrors(value) {
        const { register } = this.props;
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                email: !this.validateEmail(value, register)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        const { register } = this.props;
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validateEmail(value, register)
        );
    }

    handleBlur(e) {
        this.processValidation(e);
    }

    render() {
        const { validation_errors, hints, messages, disable, onFocus, error_messages } = this.state;
        const { register, email, name, label } = this.props;
        return (
            <div className={`form-field ${onFocus}`}>
                <label  htmlFor="login-email">{label || 'Email'}</label>
                <div className="form-input">
                    <input
                       type="email"
                       className={this.colourInputField()}
                       id="login-email"
                       value={email}
                       onFocus={this.handleFocus}
                       onBlur={this.handleBlur}
                       onChange={this.handleChangeEvent}
                       disabled={disable}
                       name={name}
                    />
                    {
                        // компонент ShowErrorRegistration используется для отображения ошибок в форме регистрации
                        // компонент ShowError используется для отображения ошибок при изменении email в профиле пользователя
                        register ?
                            <ShowErrorRegistration
                                existing_errors={validation_errors}
                                messages={messages.errors}
                            />
                            :
                            <ShowError
                                messages={error_messages}
                                existing_errors={validation_errors}
                            />
                    }
                    {
                        register ?
                            <ShowHint
                                hints={hints}
                                messages={messages.hints}
                            />
                            :
                            void(0)
                    }
                </div>
            </div>
        );
    }
}
