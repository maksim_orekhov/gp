import React from 'react';
import ShowError from '../../messages/ShowError.jsx';
import ShowHint from '../../messages/ShowHint.jsx';
import Mixins from '../../input-mixins/input-mixin';
import update from 'immutability-helper';

export default class Login extends Mixins {
    constructor() {
        super();
        this.state = {
            validation_errors: {
                reg_exp_invalid: null,
                login_unavailable: false,
                connection_is_lost: false,
                server_error: false
            },
            onFocus: '',
            hints: {
                login_rules: false
            },
            messages: {
                errors: {
                    reg_exp_invalid: {
                        header: "Недопустимый формат логина",
                        text:   "Допускается латиница и цифры. Длина 3-16 символов. Должен начинаться с буквы."
                    },
                    login_unavailable: {
                        header: "Логин занят",
                        text:   "Такой логин уже есть в системе. Придумайте другой!"
                    },
                    connection_is_lost: {
                        header: "Соединение с сервером потеряно",
                        text:   "Проверьте ваше соединение с интернетом и попробуйте еще раз"
                    },
                    server_error: {
                        header: "Ошибка обработки запроса",
                        text:   "Произошла неизвестная ошибка обработки запроса. Попробуйте обновить страницу и повторить запрос заново"
                    }
                },
                hints: {
                    login_rules: {
                        header: "Введите логин",
                        text:   "Допускается латиница, цифры. Длина 3-16 символов. Должен начинаться с буквы."
                    }
                }
            }
        };

        this.handleFocus = this.handleFocus.bind(this);
        this.processValidation = this.processValidation.bind(this);
        this.handleChange = this.handleChange.bind(this);
    };

    loginIsValid(trueOrFalse){
        this.props.isValid('loginIsValid',trueOrFalse);            // отправка состояния компонента глобальному компоненту
    }

    setHint(){
        const newData = update(this.state.hints, {
            login_rules: {$set: true}
        });
        this.setState({
            hints:newData
        });
    };

    hideHint(){
        const newData = update(this.state.hints, {
            login_rules: {$set: false}
        });
        this.setState({
            hints:newData
        });
    };

    setLoginUnavailable(boolean){
        const newData = update(this.state.validation_errors, {
            login_unavailable: {$set: boolean}
        });
        this.setState({
            validation_errors:newData
        });
    };

    ajaxConnectionSuccess(data){
        if(data.status === 'SUCCESS'){
            this.loginIsValid(true);
            this.setLoginUnavailable(false);
        }
        else if(data['server_error']){
            this.setValidationServerError(true);
        }
        else if(data.status === 'ERROR'){
            this.loginIsValid(false);
            this.setLoginUnavailable(true);
        }
        return data;
    };

    ajaxConnectionIsFail(err){
        this.setConnectionIsLost(true);
        this.loginIsValid(false);
        return err;
    };

    handleChange(e){
        this.props.componentValue('login', e.target.value);
    }

    handleFocus(){
        this.setState({
            onFocus : 'has-focus'
        });
        this.showHint();
    }

    processValidation(e){
        this.setState({
            onFocus : ' '
        });
        let regExp = '^[a-zA-Z][a-zA-Z0-9-_\.]{2,15}$';
        this.hideHint();

        let ajaxValidationOptions ={
            "url": "/register/check/login",
            "field": "login"
        };
        if(this.validationRegExp(e.target.value, regExp)){
            this.ajaxValidation(e.target.value, ajaxValidationOptions)
                .then(
                    data => this.ajaxConnectionSuccess(data),
                    error => this.ajaxConnectionIsFail(error)
                );
        }
        else{
            this.loginIsValid(false);
        }
    };

  render() {
    const { validation_errors, messages, hints, onFocus } = this.state;

    return (
        <div className={`form-field ${onFocus}`}>
            <label htmlFor="login">Логин</label>
                <div className="form-input">
                    <input
                        type="text"
                        className={this.colourInputField()}
                        maxLength="16"
                        onFocus={this.handleFocus}
                        onBlur={this.processValidation}
                        onChange={this.handleChange}
                        id="login"
                    />
                    <ShowError existing_errors={validation_errors} messages={messages.errors} />
                    <ShowHint hints={hints} messages={messages.hints} />
              </div>
          </div>
    );
  }
}
