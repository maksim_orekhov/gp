import React from 'react';
import ShowError from '../../../../../../application/react/ShowError.jsx';
import ShowErrorRegistration from '../../messages/ShowError.jsx';
import ShowHint from '../../messages/ShowHint.jsx';
import update from 'immutability-helper';
import EmailBase from "./EmailBase";
import MESSAGES from "../../../../../../application/react/constants/messages";


export default class ControlEmailRegister extends EmailBase {
  constructor() {
    super();
    this.state = {
      disable: false,
      is_blur: false,
      validation_errors:{
          reg_exp_invalid:      null,
          email_unavailable:    false,
          connection_is_lost:   false,
          email:                false,
          undefined_error:      false,
      },
      onFocus: ' ',
      hints:{
          email_rules: false
      },
      messages:{
        errors:{
            reg_exp_invalid: {
                header: "Неправильный формат E-mail.",
                text:   "Адрес не должен начинаться со знака '@' и содержать кириллицу."
            },
            email_unavailable:{
                header: "Уже используется",
                text:   "Эта почта уже есть в системе. Возможно вы регистрировались у нас ранее?"
            },
            connection_is_lost:{
                header: "Соединение с сервером потеряно",
                text:   "Проверьте ваше соединение с интернетом и попробуйте еще раз"
            },
            undefined_error: {
                header: "Ошибка обработки запроса",
                text:   "Произошла неизвестная ошибка обработки запроса. Попробуйте обновить страницу и повторить запрос заново"
            }
        },
        hints: {
            email_rules:{
                header: "Формат ввода E-mail:",
                text:   "mail@example.com"
            }
        }
      }
    };

      this.handleChangeEvent = this.handleChangeEvent.bind(this);
      this.handleBlur = this.handleBlur.bind(this);
      this.handleFocus = this.handleFocus.bind(this);
  };

    componentWillMount() {
        let invitationEmail = this.props.invitationEmail;
        let formEmail = this.props.value_prop;
        //invitation email
        if (invitationEmail && invitationEmail !== null && invitationEmail.length > 0){
            this.setState({
                disable: true,
                validation_errors: {
                    ...this.state.validation_errors,
                    reg_exp_invalid: false
                }
            });
        }
        if(formEmail && formEmail !== null && formEmail.length > 0){
            const { name, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
            handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(this.props.value_prop));
        }
    }

    handleBlur(e) {
        this.setState({ is_blur: true});
        this.hideHint();
        let ajaxCheckUniqueOptions ={
            "url": "/register/check/email",
            "field": "email"
        };

        if (!this.props.not_check_unique) {
            this.fetchValidation(e.target.value, ajaxCheckUniqueOptions)
                .then(
                    data => this.fetchConnectionSuccess(data)
                )
        }
    }

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors } = nextProps;
        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);
    }

    handleChangeEvent(e) {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
        this.hideHint();
    }

    render() {
        const { validation_errors, hints, messages, disable, onFocus, is_blur } = this.state;
        const { value_prop, name, label } = this.props;
        return (
            <div className={`form-field ${onFocus}`}>
                <label  htmlFor="login-email">{label || 'Email'}</label>
                <div className="form-input">
                    <input
                       className={this.colourInputField()}
                       id="login-email"
                       value={value_prop}
                       onFocus={this.handleFocus}
                       onBlur={this.handleBlur}
                       onChange={this.handleChangeEvent}
                       disabled={disable}
                       name={name}
                    />
                    {
                        is_blur &&
                        <ShowErrorRegistration
                            existing_errors={validation_errors}
                            messages={messages.errors}
                        />
                    }
                    <ShowHint
                        hints={hints}
                        messages={messages.hints}
                    />
                </div>
            </div>
        );
    }
}
