import React from 'react';
import Email from './controls/ControlEmailRegister.jsx';
import Password from './controls/password-input.jsx';
import Phone from './controls/phone-input.jsx';
import Submit from '../../buttons/SubmitButton.jsx';
import Login from './controls/login.jsx';
import DidMountMixin from '../form-mixins/did_mount.jsx';
import ShowError from '../LoginForm/ShowError.jsx';
// import ShowError from '../../../../../application/react/ShowError.jsx';
import FormAnimation from '../animations/form_appear_group.jsx';
import {customFetch, getUrlParameter} from "../../../../../application/react/Helpers";
import FormBase from "../../../../../application/react/forms/FormBase.jsx";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../../../../../application/react/constants/server_errors";
import URLS from "../../../../../application/react/constants/urls";
import PropTypes from 'prop-types';
import { getReCaptchaSiteKey, getReCaptchaSrc } from "../../../Helpers";

export default class RegisterForm extends FormBase{
    constructor(props) {
        super(props);
        let token = getUrlParameter(window.location.search, 'token');
        this.html_form_url = '/register';
        if (token !== '') {
            this.html_form_url = this.html_form_url+'?token='+token;
        }

        this.state = {
            onLoadClass:                '',
            loginIsValid:               false,
            emailIsValid:               false,
            phoneIsValid:               false,
            passwordIsValid:            false,
            passwordsAreEqual:          false,
            registration_is_finished:   false,
            form_isValid:               false,
            controls_server_errors: {
                login:      null,
                password:   null,
                email:      null,
                phone:      null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                connection_is_lost:     false,
                error_from_server:      false,
                email_is_valid:         false,
                password_is_valid:      false,
                email_unavailable:      false,
                undefined_error:        false
            },
            validation_errors:  {
                connection_is_lost:     false,
                error_from_server:      false,
                form_not_valid: false,
                undefined_error: false
            },
            form: {
                login:      null,
                phone:      null,
                email:      props.invitationEmail || '',
                password:   null,
                password_confirm: null,
                invitationToken:   props.invitationToken || null,
                captcha: ''
            },
            captcha_sitekey: ''
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSetLogin = this.handleSetLogin.bind(this);
        this.handleValid = this.handleValid.bind(this);
        this.handleValue = this.handleValue.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.renderReCaptcha = this.renderReCaptcha.bind(this);
    };

    handleValid(name,isTrue){
        this.setState({[name] : isTrue})
    };

    renderReCaptcha() {
        this.setState({
            captcha_sitekey: getReCaptchaSiteKey()
        }, () => {
            const captcha_script = document.createElement("script");
            const element_before_script = document.getElementById("captcha_register");

            captcha_script.src = getReCaptchaSrc();
            element_before_script.parentNode.insertBefore(captcha_script, element_before_script.nextSibling);
        });
    }

    componentDidMount() {
        this.getCsrf();
        this.renderReCaptcha();
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    handleSubmit(e){
        e.preventDefault();
        this.setState({
            form: {
                ...this.state.form,
                captcha: grecaptcha.getResponse()
            }
        }, () => {
            if (!this.isSubmitButtonDisabled()) {
                this.setState({onLoadClass : "Preloader"});
                customFetch(this.html_form_url, {
                    method: 'POST',
                    body: JSON.stringify(this.state.form)
                })
                    .then(data => {
                        console.log(data);
                        if (data.status === 'SUCCESS') {
                            this.toFinishRegistration(data.data);
                        } else if (data.status === 'ERROR') {
                            grecaptcha.reset();
                            this.setState({onLoadClass : ""});
                            this.TakeApartErrorFromServer(data);
                        }
                    })
                    .catch(() => {
                        grecaptcha.reset();
                        this.setFormError('critical_error', true);
                    })
            } else {
                this.setState({
                    messages: {
                        errors: {
                            ...this.state.errors,
                            form_not_valid: true
                        }
                    }
                });
            }
        });
    };

    toFinishRegistration(data){
        console.log('toFinishRegistration');

        this.setState({registration_is_finished:true});
        this.props.registerFormValues(this.state.form);

        console.log('redirect_url: '+data.redirect_url);

        if (data.redirect_url !== null && data.redirect_url !== '/login') {
            window.location = data.redirect_url;
        } else {
            this.props.nextStep('confirm_email');
        }
    };

    isSubmitButtonDisabled() {
        const { is_auth_v2 } = this.props;
        if(!(
                this.state.validation.email_is_valid &&
                this.state.validation.password_is_valid &&
                this.state.passwordsAreEqual &&
                (
                    is_auth_v2 || (
                        this.state.phoneIsValid &&
                        this.state.loginIsValid
                    )
                )
            )
        )
        {
            this.setState({onLoadClass : ''});
            return true;
        }
        return false;
    }

    handleSetLogin(e) {
        e.preventDefault();
        this.props.nextStep('login');
    }

    handleValue(name, value) {
        if (name === 'password') {
            this.setState({
                form: {
                    ...this.state.form,
                    confirm_password: value,
                    [name]: value
                }
            });
        } else {
            this.setState({
                form: {
                    ...this.state.form,
                    [name]: value
                }
            });
        }
    }

    render() {
        const { onLoadClass, server_errors, server_errors_messages, controls_server_errors, form, captcha_sitekey } = this.state;
        const { text, invitationMessage, invitationEmail, is_auth_v2 } = this.props;
        const { email } = this.state.form;
        if (this.props.isHidden) return null;

        // this.handleSetLogin.bind(this)
        return (
            <FormAnimation>
                <div className="form-auth">
                    <form id="register-form" onSubmit={this.handleSubmit}>
                        <div className="form-title-group">
                            <a href="#" className="form-title" onClick={this.handleSetLogin}>Вход</a>
                            <h1 className="form-title is-active">{text}</h1>
                        </div>
                        <div className={`form-auth__inner ${onLoadClass}`}>
                            <div className="register-invitation" dangerouslySetInnerHTML={{__html: invitationMessage}} />
                            {
                                !is_auth_v2 &&
                                    <div>
                                        <Login
                                            isValid={this.handleValid}
                                            componentValue={this.handleValue}
                                            form_control_server_errors={controls_server_errors.login}
                                        />
                                        <Phone
                                            isValid={this.handleValid}
                                            componentValue={this.handleValue}
                                            register={true}
                                            form_control_server_errors={controls_server_errors.phone}
                                        />
                                    </div>
                            }
                            <Email
                                value_prop={email}
                                invitationEmail={invitationEmail}
                                name="email"
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={this.state.controls_server_errors.email}
                                not_check_unique={false}
                            />
                            <Password
                                // isValid={this.handleValid}
                                name="password"
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                areEqual={this.handleValid}
                                componentValue={this.handleValue}
                                form_control_server_errors={controls_server_errors.password}
                                onSubmit={handleBlur => this.handleBlur = handleBlur }
                            />
                            {/*<div>*/}
                                {/*<label htmlFor="check-agree-policy">*/}
                                    {/*<input type="checkbox" id="check-agree-policy"/>*/}
                                    {/*Я соглашаюсь с условиями <a className="info-link" href="#">«Политики конфиденциальности персональных данных»</a></label>*/}
                            {/*</div>*/}
                            <div className="captcha-wrapper">
                                <div id="captcha_register" className="g-recaptcha" data-sitekey={captcha_sitekey} />
                                <div className="captcha-overlay captcha-overlay_position-top"/>
                                <div className="captcha-overlay captcha-overlay_position-right"/>
                                <div className="captcha-overlay captcha-overlay_position-bottom"/>
                                <div className="captcha-overlay captcha-overlay_position-left"/>
                                <div className="captcha-image"/>
                                <div className="captcha-links">
                                    <a href="https://www.google.com/intl/ru/policies/privacy/"target="_blank">Конфиденциальность</a>
                                    <span aria-hidden="true" role="presentation"> - </span>
                                    <a href="https://www.google.com/intl/ru/policies/terms/" target="_blank">
                                        Условия использования
                                    </a>
                                </div>
                            </div>
                            <div className="form-field">
                                <button
                                    className="button-action button-action_type-filed_color-blue button-action_type-filed"
                                    type="submit"
                                    name="submit"
                                    onClick={() => this.handleBlur()}
                                >
                                    ЗАРЕГИСТРироваться
                                </button>
                            </div>
                            {/*<ShowError*/}
                                {/*existing_errors={validation_errors}*/}
                                {/*messages={messages.errors}*/}
                            {/*/>*/}
                            <ShowError
                                existing_errors={server_errors}
                                messages={server_errors_messages}
                            />
                        </div>
                    </form>
                </div>
            </FormAnimation>
        );
    }
}

RegisterForm.propTypes = {
    Email: PropTypes.element,
    Password: PropTypes.element,
    ShowError: PropTypes.element
};
