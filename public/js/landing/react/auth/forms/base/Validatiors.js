export default class Validatiors {
    /**
     * Получение название валидации
     */
    static helperGetNameValidation(element) {
        let nameValidation = element;
        if (typeof element === 'object') {
            nameValidation = Object.keys(element)[0];
        }

        return nameValidation;
    }

    /**
     * Получение доп аргументов
     */
    static helperGetArguments(element) {
        let result = [];

        if (typeof element === 'object') {
            result = element[Object.keys(element)[0]];
        }

        return result
    }

    /**
     * ------------------ Messages ------------------
     */
    static prepareMessage(message, name_field, arg) {
        let arg_1 = '';
        let arg_2 = '';
        let arg_3 = '';
        let arg_4 = '';

        if (typeof arg === 'object') {
            arg_1 = arg[Object.keys(arg)[0]];
            arg_2 = arg[Object.keys(arg)[1]];
            arg_3 = arg[Object.keys(arg)[2]];
            arg_4 = arg[Object.keys(arg)[3]];
        } else {
            arg_1 = arg;
        }

        message.header = message.header.replace("%name_field%", name_field);
        message.text = message.text.replace("%name_field%", name_field);
        message.header = message.header.replace("%arg_1%", arg_1);
        message.text = message.text.replace("%arg_1%", arg_1);
        message.header = message.header.replace("%arg_2%", arg_2);
        message.text = message.text.replace("%arg_2%", arg_2);
        message.header = message.header.replace("%arg_3%", arg_3);
        message.text = message.text.replace("%arg_3%", arg_3);
        message.header = message.header.replace("%arg_4%", arg_4);
        message.text = message.text.replace("%arg_4%", arg_4);

        return message;
    }

    static getMessages() {
        return {
            required: {
                header: '%name_field%',
                text: "Обязательное поле для заполнения"
            },
            minLength: {
                header: '%name_field%',
                text: "Минимальная длина поля %arg_1%"
            },
            maxLength: {
                header: '%name_field%',
                text: "Максимальная длина поля %arg_1%"
            },
            stringLength: {
                header: '%name_field%',
                text: "Поле должно содержать от %arg_1% до %arg_2% символов"
            },
            digits: {
                header: '%name_field%',
                text: "Допускаются только цифры"
            }
        };
    }

    /**
     * ------------------ Validations ------------------
     */

    static required (value, arg) {
        return value.toString().trim().length !== 0;
    };

    static minLength(value, arg) {
        return value.toString().trim().length >= arg;
    };

    static maxLength(value, arg) {
        return value.toString().trim().length <= arg;
    };

    static stringLength(value, arg) {
        return Validatiors.minLength(value, arg.min) && Validatiors.maxLength(value, arg.max);
    }

    static digits(value, arg) {
        const reg = new RegExp('^\\d+$');
        value = value.replace(' ', '');

        return reg.test(value);
    };
};