import React from 'react';
import update from 'immutability-helper';
import Mixins from '../input-mixins/input-mixin';
import ShowError from '../messages/ShowError.jsx';
import ShowHint from '../messages/ShowHint.jsx';
import Validatiors from './Validatiors.js';

export default class Control extends Mixins {
    constructor(props) {
        super(props);

        this.state = this.getInitialState(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.processValidation = this.processValidation.bind(this);

        this.blurControl = this.blurControl.bind(this);
    }

    getInitialState (props) {
        return {
            value: props.value || '',
            hints: { main: false },
            validation_errors: {},
            messages: {
                errors: {},
                hints: {},
            },
            onFocus: '',
        };
    }

    getName() {
        return this.props.name;
    }

    getNameString() {
        return this.props.nameString;
    }

    getValidations() {
        return this.props.validations;
    }

    getValidatiors() {
        return Validatiors;
    }

    /**
     * Проверка есть ли в текущем контроле валидация с именем {name} или нет
     */
    existValidation(name) {
        let validations = this.getValidations();
        let find = validations.filter((element) => {
            let nameValidation = this.getValidatiors().helperGetNameValidation(element);

            return nameValidation === name;
        });

        return find.length !== 0;
    }

    /**
     * Выполнение правил валидации
     */
    executeValidation(value) {
        const validations = this.getValidations();
        const validatiors = this.getValidatiors();
        let errors = {};

        let fails = validations.filter((element) => {
            let nameValidation = validatiors.helperGetNameValidation(element);
            let arg = validatiors.helperGetArguments(element);
            let result = validatiors[nameValidation](value, arg);

            return result === false;
        });

        if (fails.length > 0) {
            let nameValidation = validatiors.helperGetNameValidation(fails[0]);
            let arg = validatiors.helperGetArguments(fails[0]);

            errors = update(errors, {
                [nameValidation]: {$set: true}
            });

            this.setNewMessageError(nameValidation, arg);
        }

        this.setState({
            validation_errors: errors
        });

        return fails.length > 0;
    }

    setNewMessageError(nameValidation, arg) {
        let messageError = {};
        if (nameValidation in this.getValidatiors().getMessages()) {
            messageError = this.getValidatiors().getMessages()[nameValidation];
        }

        if (nameValidation in this.state.messages.errors) {
            messageError = this.state.messages.errors[nameValidation];
        }

        messageError = this.getValidatiors().prepareMessage(messageError, this.getNameString(), arg);

        const messagesErrorsState = update(this.state.messages.errors, {
            [nameValidation]: {$set: messageError}
        });

        const messagesState = update(this.state.messages, {
            errors: {$set: messagesErrorsState}
        });

        this.setState({
            messages: messagesState
        });
    }

    isRequired() {
        return this.props.isRequired || this.existValidation('required');
    }

    liftingUpValue(e) {
        this.props.onValueChange(e);
    }

    liftingUpValid(e, bool) {
        this.props.onIsValid({
            event: e,
            isValid: bool
        });
    }

    handleChange(e) {
        this.setState({
            value: e.target.value
        });

        this.liftingUpValue(e);
    }

    handleFocus() {
        this.setState({
            onFocus : 'has-focus'
        });

        this.showHint();
    }

    beforeProcessValidation(e) {
        let hasErrors = false;
        this.setState({
            onFocus : ' '
        });

        this.hideHint();

        hasErrors = this.executeValidation(e.target.value);

        return hasErrors;
    }

    processValidation(e) {
        let hasErrors = this.beforeProcessValidation(e);

        this.liftingUpValid(e, !hasErrors);
    }

    setValidationErrors(name, bool) {
        const newData = update(this.state.validation_errors, {
            [name]: {$set: bool}
        });

        this.setState({
            validation_errors: newData
        });
    };

    setHint() {
        if (this.hasHintMessage()) {
            const newData = update(this.state.hints, {
                main: {$set: true}
            });

            this.setState({
                hints: newData
            });
        }
    };

    hideHint(){
        const newData = update(this.state.hints, {
            main: {$set: false}
        });

        this.setState({
            hints:newData
        });
    };

    hasHintMessage() {
        for (let key in this.state.messages.hints) {
            return true;
        }

        return false;
    }

    blurControl() {
        this.control.focus();
        this.control.blur();
    }

    render() {
        let fieldState = this.colourInputField();
        let value = this.props.value || '';

        return (
            <div className={fieldState + " " + this.state.onFocus + " form-group"}>
                <label htmlFor="login">{this.getNameString()}</label>
                <input type="text"
                       ref={(input) => { this.control = input; }}
                       name={this.getName()}
                       className="form-control"
                       onFocus={this.handleFocus}
                       onBlur={this.processValidation}
                       onChange={this.handleChange}
                       placeholder={this.getNameString()}
                       value={value}
                />
                <ShowError existing_errors={this.state.validation_errors} messages={this.state.messages.errors} />
                <ShowHint hints={this.state.hints} messages={this.state.messages.hints} />
            </div>
        );
    }
}
