import React from 'react';
import ShowError from '../messages/ShowError.jsx';
import ShowHint from '../messages/ShowHint.jsx';
import Mixins from './../input-mixins/input-mixin';
import update from 'immutability-helper';

export default class Code extends Mixins {
    constructor() {
        super();
        this.state = {
            validation_errors: {
                reg_exp_invalid: null,
                connection_is_lost: false,
                server_error: false
            },
            hints: {
                code_rules: false
            },
            messages: {
                errors: {
                    reg_exp_invalid: {
                        header: "Недопустимый код",
                        text:   "Проверьте правильность ввода кода."
                    }
                },
                hints: {
                    code_rules: {
                        header: "Введите код подтверждения",
                        text:   "Введите 6-ти значный цифровой код, который был выслан на указанный вами номер телефона."
                    }
                }
            }
        };
    };

    codeIsValid(trueOrFalse){
        this.props.isValid('codeIsValid',trueOrFalse);            // отправка состояния компонента глобальному компоненту
    }

    setHint(){
        const newData = update(this.state.hints, {
            code_rules: {$set: true}
        });
        this.setState({
            hints:newData
        });
    };

    hideHint(){
        const newData = update(this.state.hints, {
            code_rules: {$set: false}
        });
        this.setState({
            hints:newData
        });
    };

    handleChange(e) {
        this.props.componentValue('code', e.target.value);
        this.processValidation(e);
    }

    processValidation(e){
        let regExp = '^[0-9]{6}$';
        this.hideHint();

        if(this.validationRegExp(e.target.value, regExp)){
            this.codeIsValid(true);
        }
        else{
            this.codeIsValid(false);
        }
    };


  render() {
    let fieldState = this.colourInputField(),
        state = this.state;

    return (
          <div className={fieldState + " form-group"}>
              <label  htmlFor="inputLogin">код</label>
              <div className="input-group">
                  <div className="input-group-addon">
                      <span className="glyphicon glyphicon-check"></span>
                  </div>
                <input type="text" className="form-control"
                       onFocus={this.showHint.bind(this)}
                       onBlur={this.processValidation.bind(this)}
                       onChange={this.handleChange.bind(this)}
                       id="inputLogin" placeholder="Введите код" />

              </div>
              <ShowError existing_errors={state.validation_errors} messages={state.messages.errors} />
              <ShowHint hints={state.hints} messages={state.messages.hints} />
          </div>
          );
  }
}
