import React from 'react';
import Control from '../base/Control.jsx';

export default class Bik extends Control {
    processValidation(e) {
        let hasErrors = this.beforeProcessValidation(e);

        this.ajaxBikCsrf()
            .then(
                data =>
                    this.ajaxBikData(this.state.value, data.data.csrf)
                        .then(data => this.ajaxConnectionSuccess(data))
            );

        this.liftingUpValid(e, !hasErrors);
    }

    ajaxConnectionSuccess(data){
        data.data.bik = this.state.value;
        this.liftingUpValue(data.data);
    };

    ajaxBikCsrf() {
        return $.ajax({
            dataType: "json",
            type: 'GET',
            url: '/bank/search',
        });
    }

    ajaxBikData(value, csrf) {
        return $.ajax({
            dataType: "json",
            type: 'POST',
            url: '/bank/search',
            data: JSON.stringify(
                {
                    'bik' : value,
                    'csrf' : csrf,
                }
            )
        });
    }
}
