import React from 'react';
import update from 'immutability-helper';

export default class Timer extends React.Component {
    constructor(){
        super();
        this.state =    {
            time_amount: 0
        }
    };

    componentDidMount(){
        this.setState({
            time_amount:  this.props.start
        });
        this.timer = setInterval(this.tick.bind(this), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    tick(){
        this.setState({
            time_amount: this.state.time_amount-1
        });
        this.counterCheck();
    }

    counterCheck(){
        if(this.state.time_amount < 1){
            this.props.expired(true);
        }
    }

    render(){
        if(this.props.shutDown){
            this.componentWillUnmount();
        }
        return(
            <span>&nbsp;</span>
        );
    };
}