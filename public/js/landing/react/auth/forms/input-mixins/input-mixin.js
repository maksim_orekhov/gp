import React from 'react';
import update from 'immutability-helper';
import $ from "jquery";

export default class Mixins extends React.Component{
  constructor() {
    super();
  };

 hasErrors(obj){
    for(var key in obj){
      if(obj[key] === true)return key;                      //key == true
      else if(obj[key] === null) return null;
    }
    return false;
  }

  showHint(){
       if(!this.hasErrors(this.state.validation_errors)){
           this.setHint();
           return;
       };
   }

   setHint() {
        const newData = update(this.state, {
            hints: {password_rules: {$set: true}}
        });
        this.setState(newData);
    };



   testRegExp(value, regExpPattern){
     let regExp = new RegExp(regExpPattern);
     if( regExp.test(value) ){
       return true;
     }
     return false;
   };

   validationRegExp(value, regExpPattern){
       const validation_errors = {...this.state.validation_errors};

       if (this.testRegExp(value, regExpPattern)) {

           validation_errors.reg_exp_invalid = false;

           this.setState({validation_errors});
           return true;
       }
       else {
           validation_errors.reg_exp_invalid = true;
           this.setState({validation_errors});
           return false;
       }
   }


  passwordConfirmation(passwordIsConfirmed){
    const validation_errors = {...this.state.validation_errors};
    if(passwordIsConfirmed){
        validation_errors.not_confirmed = false;
        validation_errors.reg_exp_invalid = false;
    } else {
        validation_errors.not_confirmed = true;
    }
    this.setState({validation_errors});
  }

   increaseCounter(){
     this.setState({counter:1});
   }

   colourInputField(){
      if(this.hasErrors(this.state.validation_errors ) === false){
        return 'is-valid';
      }
      else if(this.hasErrors(this.state.validation_errors ) === null){
        return null;
      }
      return 'is-not-valid';
  }

    setValidationServerError(boolean){
        const newData = update(this.state.validation_errors, {
            server_error: {$set: boolean}
        });
        this.setState({
            validation_errors: newData
        });
    }

    /**
     *
     * @param value
     * @param options array
     * options: {
     *  url: "url",
     *  field: "field_name"
     * }
     * @returns {*}
     */

    ajaxValidation(value, options){
        return $.ajax({
            dataType: "json",
            type: 'POST',
            url: options.url,
            data: JSON.stringify(
                {
                    [options.field] : value
                }
            )
        });
    };

    setConnectionIsLost(boolean){
        const newData = update(this.state.validation_errors, {
            connection_is_lost: {$set: boolean}
        });
        this.setState({
            validation_errors:newData
        });
    }

    getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    readAndDeleteCookie(name) {
        const cookie = this.getCookie(name);
        if (cookie) {
            this.deleteCookie(name);
            return cookie;
        }
        return null;
    }

    setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }
    deleteCookie(name) {
        this.setCookie(name, "", {
            expires: -1
        })
    }

    takeApartServerError(nextErrors, currentErrors) {
        if (nextErrors) {
            if (JSON.stringify(nextErrors) !== JSON.stringify(currentErrors)) {
                const server_error = {...this.state.error_messages.server_error};

                let text = '';

                Object.values(nextErrors).forEach(value => {
                    text = text + value + '. ';
                });

                server_error.text = text;

                this.setState({
                    validation_errors: {
                        server_error: !!Object.keys(nextErrors).length
                    },
                    error_messages: {
                        server_error
                    }
                });
            }
        }
    }
}
