import React from 'react';
import Timer from '../forms/controls/timer.jsx';
import Submit from '../buttons/SubmitButton.jsx';

import FormAnimation from './animations/form_appear_group.jsx';

export default class SuccessForm extends React.Component {
    constructor() {
        super();
        this.state = {
            onLoadClass: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    };

    handleSubmit(e) {
        e.preventDefault();
        if (this.props.nextStepName == 'finish_registration') {
            this.setState({onLoadClass : "Preloader"});
            window.location = '/login';
        } else {
            this.setState({onLoadClass : "Preloader"});
            this.throwNext();
        }
    }

    throwNext() {
        this.props.nextStep(this.props.nextStepName);
    }

    render() {
        let { onLoadClass } = this.state;
        return (
            <FormAnimation>
                <div className="col-md-6 fadeInUp">
                    <div className="form-title-group">
                        <h2 className="form-title is-active">Восстановление пароля</h2>
                    </div>
                    <div className={`form-auth__inner ${onLoadClass}`}>
                        <div>Инструкция по восстановлению пароля отправлена Вам на почту.</div> <br />
                        <div className="form-field">
                            <button type="submit" className="button-action button-action_type-filed_color-blue button-action_type-filed" id="auth_register_button_send_data" onClick={this.handleSubmit}>Продолжить</button>
                        </div>
                    </div>
                </div>
            </FormAnimation>
        );
    }
}