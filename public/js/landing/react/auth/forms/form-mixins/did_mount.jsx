import React from 'react';
import update from 'immutability-helper';
import $ from "jquery";
import InputMixins from '../input-mixins/input-mixin';

export default class DidMountMixin extends InputMixins {
    constructor() {
        super();
    };

    componentDidMount() {
        let self = this;

        function setToken(token) {
            const CSRFToken = update(self.state.form, {
                'csrf': {$set: token}
            });

            self.setState({
                form: CSRFToken,
            })
        }

        this.getCSRFToken(setToken);
    }

    getCSRFToken(setToken) {
        console.log(this.html_form_url);

        $.ajax({
            dataType: "json",
            type: 'GET',
            url: '/auth/csrf-token',
            success: function (data) {
                console.log(data);
                setToken(data.data.csrf);
            }
        });
    }

    setFormError(error) {
        const validation_errors = {...this.state.validation_errors};
        validation_errors[error] = true;

        this.setState({
            validation_errors
        });
    }

    clearServerErrors() {
        const validation_errors = {...this.state.validation_errors};
        const controls_server_errors = {...this.state.controls_server_errors};

        Object.keys(validation_errors).forEach(error => {
            validation_errors[error] = false;
        });

        Object.keys(controls_server_errors).forEach(control => {
            controls_server_errors[control] = {}; // Сбрасываем все прошлые ошибки в ноль, пустой объект тут важен
        });

        this.setState({
            validation_errors,
            controls_server_errors
        });
    }

    takeApartInvalidFormData(errors) {
        if (errors) {
            const controls_server_errors = {...this.state.controls_server_errors};

            Object.keys(errors).forEach(control => {
                controls_server_errors[control] = errors[control];
            });

            this.setState({
                controls_server_errors
            });
        }
    }
}