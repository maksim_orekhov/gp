import React from 'react';
import Mixins from '../input-mixins/input-mixin';
import ShowMessageTemplate from './ShowMessageTemplate.jsx';


export default class showWarning extends Mixins {
  constructor() {
    super();
  };

  render(){
      if (!this.hasErrors(this.props.warnings)) {            //если в объекте props только ключи false или null - не рендерить
        return null;
      };
      let rules = this.props.messages[this.hasErrors(this.props.warnings)];
      return (
          <ShowMessageTemplate rules={rules} color_class='bs-callout-warning'/>
      );
  }
}
