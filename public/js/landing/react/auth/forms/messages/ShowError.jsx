import React from 'react';
import Mixins from '../input-mixins/input-mixin';
import ShowMessageTemplate from './ShowMessageTemplate.jsx';
import FormAnimation from '../animations/form_appear_group.jsx';


export default class showError extends Mixins {
  constructor() {
    super();
  };

  render(){
      if (!this.hasErrors(this.props.existing_errors)) {            //если в объекте props.existing_errors только ключи false или null - не рендерить
        return null;
      };
      let rules =this.props.messages[this.hasErrors(this.props.existing_errors)];   //вычислить какие правила именно нарушены и присвоить в переменную
      if (!rules || !Object.keys(rules).length) {
          return null;
      }

      return (
          <FormAnimation>
                <ShowMessageTemplate rules={rules} color_class='error-message' wrapper_class='error' />
          </FormAnimation>
      );
  }
}