import React from 'react';
import Mixins from '../input-mixins/input-mixin';
import ShowMessageTemplate from './ShowMessageTemplate.jsx';
import FormAnimation from '../animations/form_appear_group.jsx';

export default class showHint extends Mixins {
  constructor() {
    super();
  };

  render(){

      if (!this.hasErrors(this.props.hints)) {            //если в объекте props только ключи false или null - не рендерить
        return null;
      };
      let rules = this.props.messages[this.hasErrors(this.props.hints)];
      return (
          <FormAnimation>
            <ShowMessageTemplate rules={rules} color_class='hint-message' wrapper_class='hint'/>
          </FormAnimation>
      );
  }
}
