import React from 'react';
import { CSSTransitionGroup } from 'react-transition-group' ;



export default function showMessageTemplate(props) {
    return(
        <CSSTransitionGroup
            transitionName="message"
            transitionAppear={true}
            transitionAppearTimeout={500}
            transitionEnter={false}
            transitionLeave={true}
            transitionLeaveTimeout={5000}
        >
            <div className={`form-hint ${props.wrapper_class}`}>
                <div className="form-hint__inner">
                    <div className="form-hint__title">{props.rules.header}</div>
                    <div className="form-hint__body">{props.rules.text}</div>
                </div>
            </div>
        </CSSTransitionGroup>
    )
}