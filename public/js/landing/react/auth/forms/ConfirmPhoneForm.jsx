import React from 'react';
import Code from './controls/code-input.jsx';
import Submit from '../buttons/SubmitButton.jsx';
import DidMountMixin from './form-mixins/did_mount.jsx';
import update from 'immutability-helper';
import ShowError from './messages/ShowError.jsx';

import FormAnimation from './animations/form_appear_group.jsx';

export default class ConfirmForm extends DidMountMixin{
    constructor() {
        super();
        this.html_form_url = '/phone/confirm';
        this.state = {
            codeIsValid:               false,
            validation_errors:  {
                connection_is_lost:     false,
                error_from_server:      false
            },
            form: {
                code: null
            },
            messages:{
                errors: {
                    connection_is_lost: {
                        header:     "Соединение с сервером потеряно",
                        text:       "Проверьте ваше соединение с интернетом и попробуйте еще раз."
                    },
                    error_from_server:{
                        header:     "Сервер возвратил ошибку",
                        text:       "При обработке сервером что-то пошло не так."
                    },
                    too_much_requests: {
                        header:     "Слишком много запросов",
                        text:       "Вы привысили суточное разрешенное количество запросов. Обрабтитесь в техподдержку."
                    },
                    too_fast_request: {
                        header:     "Слишком частые запросы",
                        text:       "Вы запрашиваете код слишком часто. Подождите минуту и попробуйте снова."
                    }
                }
            }
        };
    };

    handleValid(name,isTrue){
        this.setState({[name] : isTrue});
    };

    ajaxFormSend(url){
        return $.ajax({
            dataType: "json",
            type: 'POST',
            url: url,
            data: JSON.stringify(
                    this.state.form
            )
        });
    };

    handleSubmit(e){
        e.preventDefault();
        if (this.state.codeIsValid) {
            this.ajaxFormSend('/phone/confirm?phone='+this.props.user_phone)
                .then(
                    data => this.ajaxConnectionSuccess(data),
                    error => this.ajaxConnectionIsFail(error)
                );
        }
    };

    setServerReturnedError(boolean){
        const newData = update(this.state.validation_errors, {
            error_from_server: {$set: boolean}
        });
        this.setState({
            validation_errors:newData
        });
    }

    ajaxConnectionSuccess(data){
        if(data.status === 'SUCCESS'){
            this.toFinishConfirmPhone();
        }
        else{
            this.setServerReturnedError(true)
        }
        return data;
    };

    toFinishConfirmPhone(){
        this.props.nextStep('confirm_phone_success_message');
    }

    ajaxConnectionIsFail(err){
        console.log(err);
        this.setConnectionIsLost(true);
    };

    isSubmitButtonDisabled(){
        return !this.state.codeIsValid;
    }
    handleValue(name, value){
        const newData = update(this.state.form, {
            [name]: {$set: value}
        });

        this.isSubmitButtonDisabled();

        this.setState({
            form: newData
        });
    }
    render() {
        let state = this.state;

        if(this.props.isHidden)return null;
        return (
            <FormAnimation>
            <div className="col-md-6 fadeInUp">
                <h1>Подтверждение телефона</h1>
                    <form
                        onSubmit={this.handleSubmit.bind(this)}
                        className="confirm-form">
                        <Code isValid={this.handleValid.bind(this)}
                               componentValue={this.handleValue.bind(this)} buttonStatus={this.isSubmitButtonDisabled()} />
                        <ShowError existing_errors={state.validation_errors} messages={state.messages.errors} />
                        <input type="hidden" value={this.props.userLogin} name="user_login" />
                        <Submit disabled={this.isSubmitButtonDisabled()}
                        />
                    </form>
            </div>
            </FormAnimation>
        );
    }
}