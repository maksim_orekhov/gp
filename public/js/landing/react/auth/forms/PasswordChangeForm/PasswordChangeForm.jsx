import React from 'react';
import Password from './controls/password-input.jsx';
import ShowError from '../LoginForm/ShowError.jsx';
import FormAnimation from '../animations/form_appear_group.jsx';
import {customFetch, getUrlParameter} from "../../../../../application/react/Helpers";
import FormBase from "../../../../../application/react/forms/FormBase.jsx";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../../../../../application/react/constants/server_errors";
import URLS from "../../../../../application/react/constants/urls";
import PropTypes from 'prop-types';
import { getReCaptchaSiteKey, getReCaptchaSrc } from "../../../Helpers";

export default class PasswordChangeForm extends FormBase{
    constructor(props) {
        super(props);

        this.state = {
            onLoadClass:                '',
            passwordsAreEqual:          false,
            form_isValid:               false,
            controls_server_errors: {
                confirm_new_password: null,
                new_password: null
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                connection_is_lost:     false,
                error_from_server:      false,
                new_password_is_valid:  false,
                undefined_error:        false
            },
            validation_errors:  {
                connection_is_lost:     false,
                error_from_server:      false,
                form_not_valid: false,
                undefined_error: false
            },
            form: {
                new_password:   null,
                confirm_new_password: null,
                csrf: null
            }
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSetPassword = this.handleSetPassword.bind(this);
        this.handleValid = this.handleValid.bind(this);
        this.handleValue = this.handleValue.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
    };

    handleValid(name,isTrue){
        this.setState({[name] : isTrue})
    };

    componentDidMount() {
        this.getCsrf();
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    handleSubmit(e){
        e.preventDefault();
        this.setState({
            form: {
                ...this.state.form
            }
        }, () => {
            if (!this.isSubmitButtonDisabled()) {
                this.setState({onLoadClass : "Preloader"});
                customFetch(window.location, {
                    method: 'POST',
                    body: JSON.stringify(this.state.form)
                })
                    .then(data => {
                        console.log(data);
                        if (data.status === 'SUCCESS') {
                            this.handleSetPassword();
                        } else if (data.status === 'ERROR') {
                            this.setState({onLoadClass : ""});
                            this.TakeApartErrorFromServer(data);
                        }
                    })
                    .catch(() => {
                        this.setState({onLoadClass : ""});
                        this.setFormError('critical_error', true);
                    })
            } else {
                this.setState({
                    messages: {
                        errors: {
                            ...this.state.errors,
                            form_not_valid: true
                        }
                    }
                });
            }
        });
    };

    // toFinishRegistration(data){
    //     console.log('toFinishRegistration');
    //
    //     this.setState({registration_is_finished:true});
    //     this.props.registerFormValues(this.state.form);
    //
    //     console.log('redirect_url: '+data.redirect_url);
    //
    //     if (data.redirect_url !== null && data.redirect_url !== '/login') {
    //         window.location = data.redirect_url;
    //     } else {
    //         this.props.nextStep('confirm_email');
    //     }
    // };

    isSubmitButtonDisabled() {
        if(!(
                this.state.validation.new_password_is_valid &&
                this.state.passwordsAreEqual
            )
        )
        {
            return true;
        }
        return false;
    }

    handleSetPassword() {
        this.props.nextStep('change_password_success_message');
    }

    handleValue(name, value) {
        if (name === 'new_password') {
            this.setState({
                form: {
                    ...this.state.form,
                    confirm_new_password: value,
                    [name]: value
                }
            });
        } else {
            this.setState({
                form: {
                    ...this.state.form,
                    [name]: value
                }
            });
        }
    }

    render() {
        const { onLoadClass, server_errors, server_errors_messages, controls_server_errors, form } = this.state;

        return (
            <FormAnimation>
                <div className="form-auth">
                    <form id="password-change-form" onSubmit={this.handleSubmit}>
                        <div className="form-title-group">
                            <h1 className="form-title is-active">Изменить пароль</h1>
                        </div>
                        <div className={`form-auth__inner ${onLoadClass}`}>
                            <Password
                                name="new_password"
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                areEqual={this.handleValid}
                                componentValue={this.handleValue}
                                form_control_server_errors={controls_server_errors.new_password}
                                form_control_server_errors_second_password={controls_server_errors.confirm_new_password}
                                onSubmit={handleBlur => this.handleBlur = handleBlur }
                            />
                            <div className="form-field">
                                <button
                                    className="button-action button-action_type-filed_color-blue button-action_type-filed"
                                    type="submit"
                                    name="submit"
                                    onClick={() => this.handleBlur()}
                                    // disabled={this.isSubmitButtonDisabled()}
                                >
                                    изменить пароль
                                </button>
                            </div>
                            <ShowError
                                existing_errors={server_errors}
                                messages={server_errors_messages}
                            />
                        </div>
                    </form>
                </div>
            </FormAnimation>
        );
    }
}
