import React from 'react';
import ShowError from '../../messages/ShowError.jsx';
import ShowHint from '../../messages/ShowHint.jsx';
import ShowWarning from '../../messages/ShowWarning.jsx';
// import MakeMaskedPassword from '../../../../lib/MaskedPassword';
import ControlBase from "../../../../../../application/react/controls/ControlBase";

export default class PasswordInput extends ControlBase {
    constructor() {
        super();
        this.state = {
            is_blur: false,
            validation_errors: {
                reg_exp_invalid: null,
                not_confirmed: null,
                undefined_error: false
            },
            onFocus: '',
            hints: {
                password_rules: false
            },
            warnings: {
                capslock: false,
                cyrillic: false
            },
            type_input: 'password',
            isShowPassword: false,
            counter: 0,
            password: '',
            second_password: '',
            messages: {
                errors: {
                    reg_exp_invalid: {
                        header: "Неправильный пароль",
                        text: "Пароль может содержать только латинские символы и цифры A-Za-z0-9, от 6 до 64 символов. Кириллица запрещена."
                    },
                    not_confirmed: {
                        header: "Пароли не совпадают",
                        text: "Проверьте корректность ввода паролей"
                    },
                    undefined_error: {
                        header: 'Неправильный пароль',
                        text: ''
                    }
                },
                hints: {
                    password_rules: {
                        header: "Правила введения пароля",
                        text: "не менее 6 символов и не более 64 (A-Z,a-z,0-9), без пробелов."
                    }
                },
                warnings: {
                    capslock: {
                        header: "Внимание!",
                        text: "Вы печатаете со включенным CapsLock!"
                    },
                    cyrillic: {
                        header: "Внимание!",
                        text: "Вы печатаете кириллицей!"
                    }
                }
            }
        };

        this.handleChangeFirstPassword = this.handleChangeFirstPassword.bind(this);
        this.handleBlurFirstPassword = this.handleBlurFirstPassword.bind(this);
        this.showHint = this.showHint.bind(this);
        this.handleBlurSecondPassword = this.handleBlurSecondPassword.bind(this);
        this.handleChangeSecondPassword = this.handleChangeSecondPassword.bind(this);
        this.showPassword = this.showPassword.bind(this);
        this.isPasswordsEqual = this.isPasswordsEqual.bind(this);
        this.capslockHandler = this.capslockHandler.bind(this);
    };

    componentDidMount(){
        this.props.onSubmit(this.isPasswordsEqual);
    };

    componentWillReceiveProps(nextProps) {
        const { form_control_server_errors } = nextProps;

        form_control_server_errors && this.setControlErrorsFromServer(form_control_server_errors, this.props.form_control_server_errors);
    }

    /**
     * Сравнивает изменился ли props ошибки, если изменился то устанавливает его как ошибку контрола
     * если старый props равен новому и server_errors не null сбрасывает ошибку в контроле
     * @param {Object} errorsNext - новый объект ошибок
     * @param {string } errorsCurrent - старый объект ошибок
     */
    setControlErrorsFromServer(errorsNext, errorsCurrent) {
        if (JSON.stringify(errorsNext) !== JSON.stringify(errorsCurrent)) {
            const validation_errors = {...this.state.validation_errors};
            const error_messages = {...this.state.messages.errors};

            Object.keys(errorsNext).forEach(error_name => {
                if (validation_errors.hasOwnProperty(error_name)) {
                    customFetch(URLS.LOG_ERRORS, {
                        method: 'POST',
                        body: JSON.stringify({
                            code: REQUEST_CODES.INVALID_FORM_DATA,
                            message: `Контрол: ${this.props.name || 'Неопределен'} </br>
                                Флаг валидации: ${error_name} </br>
                                Текст с бэка: ${errorsNext[error_name]} </br>
                                Текст с фронта: ${error_messages[error_name]}.`
                        })
                    });
                    error_messages[error_name].text = errorsNext[error_name];
                    validation_errors[error_name] = true;

                } else {
                    error_messages['undefined_error'].text += ` ${errorsNext[error_name]}`;
                    validation_errors['undefined_error'] = true;
                }
            });

            this.setState({
                messages: {
                    ...this.state.messages,
                    errors: error_messages
                },
                validation_errors
            });
        } else {
            // Так как ошибки с бэка не валидируются то чтобы снова сделать инпут валидным при вводе проверяем что флаг undefined_error активен и сбрасываем его
            if (this.state.validation_errors.undefined_error) {
                this.clearUndefinedErrorFromServer();
            }
        }
    }

    clearUndefinedErrorFromServer() {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                undefined_error: false
            },
            messages: {
                ...this.state.messages,
                errors: {
                    ...this.state.messages.errors,
                    undefined_error: {
                        header: 'Неправильный пароль',
                        text: ''
                    }
                },
                undefined_error: ''
            }
        });
    }

    setUpstatePasswordsAreEqual(trueOrFalse) {                             //setUpState - префикс передачи состояния родительскому компоненту
        this.props.areEqual('passwordsAreEqual', trueOrFalse);            // отправка состояния компонента глобальному компоненту
        this.props.componentValue('new_password', this.state.password);
    }

    handleChangeEvent(e) {
        const value = e.target.value;
        const { name, handleComponentChange } = this.props;

        handleComponentChange && handleComponentChange(name, value);
    }

    capslockHandler(e) {        //отслеживает индикацию капс-лока
        this.hideHint();
        let caps = e.getModifierState && e.getModifierState('CapsLock');
        if (caps) {
            this.setState({
                warnings: {
                    ...this.state.warnings,
                    capslock: true
                }
            });
        } else {
            // this.setHint();
            this.setState({
                warnings: {
                    ...this.state.warnings,
                    capslock: false
                }
            });
        }
    };

    cyrillicHandler(e) {                                  //отслеживает ввод кириллицы
        if (/[А-Яа-яё]/i.test(e.target.value)) {
            this.hideHint();
            this.setState({
                warnings: {
                    ...this.state.warnings,
                    cyrillic: true
                }
            });
        }
        else {
            this.setState({
                warnings: {
                    ...this.state.warnings,
                    cyrillic: false
                }
            });
        }
    };

    isPasswordsEqual() {

        if (this.state.counter) {
            if (this.testPasswordsAreEqual(this.state.second_password, this.state.password)) {
                this.setUpstatePasswordsAreEqual(true);
                this.passwordConfirmation(true);
            }
            else {
                this.setUpstatePasswordsAreEqual(false);
                this.passwordConfirmation(false);
            }
        }
    }

    testPasswordsAreEqual(value1, value2) {
        if (value1 === value2 && this.state.validation_errors.reg_exp_invalid === false) {
            this.setState({
                validation_errors: {
                    ...this.state.validation_errors,
                    not_confirmed: false
                }
            });
            return true
        } else {
            this.setState({
                validation_errors: {
                    ...this.state.validation_errors,
                    not_confirmed: true
                }
            });
            return false
        }
    }

    showPassword() {
        this.setState({
            isShowPassword: !this.state.isShowPassword,
            type_input: !this.state.isShowPassword ? 'text' : 'password'
        });
    }

    handleChangeFirstPassword(e) {
        this.handleChangeEvent(e);
        this.setState({
            password: e.target.value
        });
        // this.cyrillicHandler(e);
    }

    handleChangeSecondPassword(e) {
        this.handleChangeEvent(e);
        this.setState({
            second_password: e.target.value
        });
        // this.cyrillicHandler(e);
    }

    handleBlurFirstPassword(e) {
        this.hideHint();
        const value = e.target.value;
        const { name, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    handleBlurSecondPassword() {
        this.setState({
            is_blur: true
        });
        this.hideHint();
        this.isPasswordsEqual();
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                isEmpty: !this.validateIsEmpty(value),
                reg_exp_invalid: !this.validatePassword(value)
            }
        });
    }

    setHint() {
        this.setState({
            hints: {
                password_rules: true
            }
        });
    };

    hideHint() {
        this.setState({
            hints: {
                password_rules: false
            }
        });
    };

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.increaseCounter();
        this.updateErrors(value);
        return (
            this.validateIsEmpty(value)
            && this.validatePassword(value)
        );
    }

    increaseCounter(){
        this.setState({counter:1});
    }

    render() {
        const { onFocus, type_input, validation_errors, messages, hints, warnings, is_blur } = this.state;

        return (
            <div>
                <div className={`form-field ${onFocus}`}>
                    <label htmlFor="password_field_1">Новый пароль</label>
                    <div className="form-input">
                        <input
                            id='password_field_1'
                            type={type_input}
                            className={this.colourInputField()}
                            onChange={this.handleChangeFirstPassword}
                            onBlur={this.handleBlurFirstPassword}
                            onFocus={this.showHint}
                            onKeyDown={this.capslockHandler}
                            name="new_password"
                            // onKeyPress={this.cyrillicHandler.bind(this)}
                        />
                        <span
                            className="toggle-password-field-type-button"
                            onClick={this.showPassword}
                        />
                        {
                            (is_blur || this.state.validation_errors) &&
                            <ShowError
                                existing_errors={validation_errors}
                                messages={messages.errors}
                            />
                        }
                        {/*<ShowError existing_errors={validation_errors} messages={messages.errors}/>*/}
                        <ShowHint hints={hints} messages={messages.hints}/>
                        <ShowWarning warnings={warnings} messages={messages.warnings}/>
                    </div>
                </div>
                <div className={`form-field ${onFocus}`}>
                    <label htmlFor="password_field_2">Повторите новый пароль</label>
                    <div className="form-input">
                        <input
                            id='password_field_2'
                            name="confirm_new_password"
                            type={type_input}
                            className={this.colourInputField()}
                            onChange={this.handleChangeSecondPassword}
                            onBlur={this.handleBlurSecondPassword}
                            onKeyDown={this.capslockHandler}
                            // onFocus={this.showHint.bind(this)}
                        />
                        <span
                            className="toggle-password-field-type-button"
                            onClick={this.showPassword}
                        />
                    </div>
                </div>
            </div>
        );
    }
}



