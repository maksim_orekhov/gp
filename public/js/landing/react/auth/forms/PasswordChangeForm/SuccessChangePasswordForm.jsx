import React from 'react';
import Timer from '../../forms/controls/timer.jsx';
// import Submit from '../buttons/SubmitButton.jsx';

import FormAnimation from '../animations/form_appear_group.jsx';

export default class SuccessChangePasswordForm extends React.Component {
    constructor() {
        super();
        this.state = {
            onLoadClass: '',
            shut_timer: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleExpired = this.handleExpired.bind(this)
    };

    handleSubmit(e) {
        e.preventDefault();
        this.setState({onLoadClass : "Preloader"});
        this.throwNext();
    }

    handleExpired(bool_true){
        console.log(['handleExpired', bool_true]);
        this.setState({
            shut_timer: true
        });
        this.throwNext();
    }

    throwNext() {
        window.location = '/profile';
    }

    render() {
        let { onLoadClass } = this.state;
        return (
            <FormAnimation>
                <div className="col-md-6 fadeInUp">
                    <div className="form-title-group">
                        <h2 className="form-title is-active">Пароль изменен</h2>
                    </div>
                    <div className={`form-auth__inner ${onLoadClass}`}>
                        <div>Ваш пароль был успешно изменен.</div> <br />
                        <div className="form-field">
                            <button type="submit" className="button-action button-action_type-filed_color-blue button-action_type-filed" onClick={this.handleSubmit}>Перейти в профиль</button>
                        </div>
                    </div>
                    <Timer start={15} expired={this.handleExpired} shutDown={this.state.shut_timer} />
                </div>
            </FormAnimation>
        );
    }
}