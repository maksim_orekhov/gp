import React from 'react';
import ShowError from '../../messages/ShowError.jsx';
import ShowHint from '../../messages/ShowHint.jsx';
import EmailBase from "../../RegisterForm/controls/EmailBase";


export default class EmailInput extends EmailBase {
  constructor() {
    super();
    this.state = {
      is_blur: false,
      validation_errors:{
          reg_exp_invalid:      null,
          email_unavailable:    false,
          connection_is_lost:   false,
          server_error:         false
      },
      hints:{
          email_rules: false
      },
      messages:{
        errors:{
            reg_exp_invalid: {
                header: "Неправильный формат ввода адреса электронной почты.",
                text:   "Адрес не должен начинаться со знака '@', а также содержать системные символы"
            },
            email_unavailable:{
                header: "E-mail не найден",
                text:   "Проверьте правильность набора"
            },
            connection_is_lost:{
                header: "Соединение с сервером потеряно",
                text:   "Проверьте ваше соединение с интернетом и попробуйте еще раз"
            },
            server_error:{
                header: "Ошибка обработки запроса",
                text:   "Произошла неизвестная ошибка обработки запроса. Попробуйте обновить страницу и повторить запрос заново"
            }
        },
        hints: {
            email_rules:{
                header: "Формат ввода E-mail:",
                text:   "Пример:mail@example.com"
            }
        }
      }
    };

      this.handleChangeEvent = this.handleChangeEvent.bind(this);
      this.handleBlur = this.handleBlur.bind(this);
      this.handleFocus = this.handleFocus.bind(this);
  };

    handleBlur() {
        this.setState({
            is_blur: true
        })
    }

    render() {
        let fieldState = this.colourInputField(),
            { is_blur, validation_errors, messages, hints } = this.state;
        const { value_prop, name } = this.props;

        return (
            <fieldset>
                <div className={fieldState + " form-field"}>
                    <label htmlFor="inputEmail">Введите Ваш e-mail для восстановления пароля:</label>
                    <div className="form-input">
                        <input type="email"
                               className={this.colourInputField()}
                               id="inputEmail"
                               onFocus={this.handleFocus}
                               onBlur={this.handleBlur}
                               onChange={this.handleChangeEvent}
                               placeholder="Введите email"
                               value={value_prop}
                               name={name}
                        />
                        {
                            is_blur &&
                            <ShowError existing_errors={validation_errors} messages={messages.errors} />
                        }
                        <ShowHint hints={hints} messages={messages.hints} />
                    </div>
                </div>
            </fieldset>
        );
    }
}
