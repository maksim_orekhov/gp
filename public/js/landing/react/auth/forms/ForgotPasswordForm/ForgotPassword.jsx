import React from 'react';
import Email from './controls/email-input.jsx';

import Submit from '../../buttons/SubmitButton.jsx';
import DidMountMixin from '../form-mixins/did_mount.jsx';
import update from 'immutability-helper';
import ShowError from '../LoginForm/ShowError.jsx';
import FormAnimation from '../animations/form_appear_group.jsx';
import FormBase from "../../../../../application/react/forms/FormBase";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../../../../../application/react/constants/server_errors";
import {customFetch} from "../../../../../application/react/Helpers";

export default class ForgotPassword extends FormBase {
    constructor() {
        super();
        this.html_form_url = '/forgot/password';
        this.state = {
            onLoadClass: '',
            emailIsValid:               false,
            validation_errors:  {
                connection_is_lost:     false,
                error_from_server:      false
            },
            controls_server_errors: {
                email:      null
            },
            form: {
                email:      null,
                csrf: ''
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                connection_is_lost:     false,
                error_from_server:      false,
                email_is_valid:         false,
                undefined_error:        false
            }
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    };

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
    }

    componentDidMount() {
        this.getCsrf();
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({onLoadClass : "Preloader"});
        customFetch('/forgot/password/reset', {
            method: 'POST',
            body: JSON.stringify(this.state.form)
        })
            .then(data => {
            console.log(data);
            if (data.status === 'SUCCESS') {
                this.toFinish();
            } else if (data.status === 'ERROR') {
                this.setState({onLoadClass : ""});
                this.TakeApartErrorFromServer(data);
            }
        })
            .catch(() => {
                this.setFormError('critical_error', true);
                this.setState({onLoadClass : ""});
            })
    };

    toFinish() {
        this.props.nextStep('forgot_password_success_message');
    };

    isSubmitButtonDisabled() {
        if (!this.state.validation.email_is_valid) {
            return true;
        }
        return false;
    }

    render() {
        let { server_errors, server_errors_messages, email, onLoadClass } = this.state;

        return (
            <FormAnimation>
                <div className="form-title-group">
                    <h2 className="form-title is-active">Восстановить пароль</h2>
                </div>
                <div className={`form-auth__inner ${onLoadClass}`}>
                    <Email value_prop={email}
                           name="email"
                           handleComponentChange={this.handleChange}
                           handleComponentValidation={this.handleComponentValid}
                           form_control_server_errors={this.state.controls_server_errors.email}
                           not_check_unique={true}
                    />
                    <div className="form-field">
                        <button type="submit" className="button-action button-action_type-filed_color-blue button-action_type-filed" disabled={this.isSubmitButtonDisabled()} id="auth_register_button_send_data" onClick={this.handleSubmit}>Сбросить пароль</button>
                    </div>
                    <ShowError existing_errors={server_errors} messages={server_errors_messages} />
                </div>
            </FormAnimation>
        );
    }
}