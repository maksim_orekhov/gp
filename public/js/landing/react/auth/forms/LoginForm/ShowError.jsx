import React from 'react';
import Mixins from '../input-mixins/input-mixin';
import FormAnimation from '../animations/form_appear_group.jsx';
import ShowMessageTemplate from './ShowMessageTemplate.jsx';
import {checkControlErrors} from "../../../../../application/react/Helpers";


export default class showError extends Mixins {
  constructor() {
    super();
  };


  render(){
      if (!checkControlErrors(this.props.existing_errors)) {            //если в объекте props.existing_errors только ключи false или null - не рендерить
          return null;
      }
      const rules = this.props.messages[checkControlErrors(this.props.existing_errors)];
      // if (!this.hasErrors(this.props.existing_errors)) {            //если в объекте props.existing_errors только ключи false или null - не рендерить
      //   return null;
      // };
      // let rules =this.props.messages[this.hasErrors(this.props.existing_errors)];   //вычислить какие правила именно нарушены и присвоить в переменную

      return (
          <FormAnimation>
              <ShowMessageTemplate rules={rules} color_class='bs-callout-danger center-align'/>
          </FormAnimation>
      );
  }

}