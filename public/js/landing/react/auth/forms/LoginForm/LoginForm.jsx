import React from 'react';

import ShowError from './ShowError.jsx';
import Password from './controls/password-input.jsx';
import Login from './controls/login.jsx';
import FormAnimation from '../animations/form_appear_group.jsx';
import { getUrlParameter, getCookie, readAndDeleteCookie, setCookie, customFetch } from "../../../../../application/react/Helpers";
import FormBase from "../../../../../application/react/forms/FormBase";
import {SERVER_ERRORS, SERVER_ERRORS_MESSAGES} from "../../../../../application/react/constants/server_errors";
import PropTypes from 'prop-types';
import { getReCaptchaSiteKey, getReCaptchaSrc } from '../../../../../landing/react/Helpers';
import URLS from "../../../../../application/react/constants/urls";

export default class LoginForm extends FormBase {
    constructor() {
        super();
        this.html_form_url = '/login';
        this.state = {
            loginIsValid:               false,
            passwordIsValid:            false,
            form_isValid:               false,
            onLoadClass: '',
            controls_server_errors: {
                login: null,
                password: null
            },
            validation_errors:  {
                connection_is_lost:     false,
                error_from_server:      false,
                invalid_form_data: false,
                already_auth: false,
                undefined_error: false
            },
            // Server_errors
            server_errors_messages: SERVER_ERRORS_MESSAGES,
            server_errors: SERVER_ERRORS,
            // Validation props
            validation: {
                login_is_valid:         false,
                password_is_valid:      false,
                undefined_error:        false
            },
            form: {
                login:      '',
                password:   ''
            },
            captcha_sitekey: '',
            is_show_captcha: false
        };

        this.prev_state = null;
        this.updateValidationFunctions = []; // Массив из функций для обновления валидаций
        this.can_update_validation = true; // Флаг может ли форма изменить state

        this.handleSubmit = this.handleSubmit.bind(this);
        // this.resetFormErrors = this.resetFormErrors.bind(this);
        this.handleSetRegister = this.handleSetRegister.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleComponentValid = this.handleComponentValid.bind(this);
        this.renderReCaptcha = this.renderReCaptcha.bind(this);
    }

    componentDidMount() {
        this.getCsrf();
        // Здесь мы проверяем если ошибки в куки, это происходит если неправильно заполнена форма входа на лэндинге
        this.getFormErrorsFromCookie();
    }

    componentWillUpdate() {
        this.tryToRunValidationFunctions();
        const is_show_captcha = getCookie('login_captcha_enable');

        if (is_show_captcha && !this.state.is_show_captcha) {
            this.setState({
                is_show_captcha: is_show_captcha
            }, () => {
                this.renderReCaptcha();
            });
        }
    }

    renderReCaptcha() {
        this.setState({
            captcha_sitekey: getReCaptchaSiteKey()
        }, () => {
            const captcha_script = document.createElement("script");
            const element_before_script = document.getElementById("captcha_login");

            captcha_script.src = getReCaptchaSrc();
            element_before_script.parentNode.insertBefore(captcha_script, element_before_script.nextSibling);
        });
    }

    getFormDataFromCookie() {
        const cookie = readAndDeleteCookie('login-form-data');
        if (cookie) {
            const form_data = JSON.parse(decodeURIComponent(cookie));

            if (Object.keys(form_data).length) {
                const form = {...this.state.form};

                form.login = form_data.login || '';
                form.password = form_data.password || '';

                this.setState({
                    form
                });
            }
        }
    }

    getFormErrorsFromCookie() {
        const cookie = readAndDeleteCookie('login-form-message');
        if (cookie) {
            const errors = JSON.parse(decodeURIComponent(cookie));
            if (Object.keys(errors).length) {
                this.setFormError('auth_failed', true);
                this.getFormDataFromCookie();

                const controls_server_errors = {...this.state.controls_server_errors};

                Object.keys(errors).forEach(error => {
                    controls_server_errors[error] = errors[error];
                });

                this.setState({
                    controls_server_errors,
                    validation: {
                        login_is_valid:         true,
                        password_is_valid:      true
                    }
                });
            }
        }
    }

    // resetFormErrors() {
    //     const newData = update(this.state.validation_errors, {
    //         connection_is_lost: {$set: false},
    //         error_from_server: {$set: false},
    //     });
    //     this.setState({
    //         validation_errors:newData
    //     });
    // }

    processFormSubmit(grecaptcha = null) {
        let redirect_url_params = getUrlParameter(window.location.search, 'redirectUrl');
        let login_url = '/login';
        if (redirect_url_params !== '') {
            login_url = login_url+'?redirectUrl='+redirect_url_params;
        }

        this.setState({onLoadClass : "Preloader"});
        customFetch(login_url, {
            method: 'POST',
            body: JSON.stringify(this.state.form)
        })
            .then(data => {
                console.log(data);
                if (data.status === 'SUCCESS') {
                    this.toFinishAuth(data)
                } else if (data.status === 'ERROR') {
                    this.setState({onLoadClass : ""});
                    this.TakeApartErrorFromServer(data);
                    if (grecaptcha) {
                        grecaptcha.reset();
                    }
                }
            })
            .catch((error) => {
                console.log(error);
                if (grecaptcha) {
                    grecaptcha.reset();
                }
                this.setFormError('critical_error', true);
                this.setState({onLoadClass : ""});
            })
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.state.is_show_captcha) {
            this.setState({
                form: {
                    ...this.state.form,
                    captcha: grecaptcha.getResponse()
                }
            }, () => {
                this.processFormSubmit(grecaptcha);
            });
        } else {
            this.processFormSubmit();
        }
    };

    /*setServerReturnedError(boolean){
        const newData = update(this.state.validation_errors, {
            error_from_server: {$set: boolean}
        });
        this.setState({
            validation_errors:newData
        });
    }*/

    toFinishAuth(data) {
        let params = data['data'];
        console.log(params.redirect_url);
        setCookie('guarant-pay-login', params.login);
        if(params.redirect_url === undefined){
            window.location = '/profile';
        } else {
            window.location = params.redirect_url;
        }
    };

    isSubmitButtonDisabled() {
        if (!
                (
                    this.state.validation.login_is_valid &&
                    this.state.validation.password_is_valid
                )
        )
        {
            return true;
        }
        return false;
    }

    handleSetRegister(e) {
        e.preventDefault();
        this.props.nextStep('register');
    }

    render () {
        const { server_errors, server_errors_messages, onLoadClass, controls_server_errors, form, captcha_sitekey, is_show_captcha } = this.state;
        const { text, is_auth_v2 } = this.props;
        const { login } = this.state.form;

        return (
            <FormAnimation>
                <div className='form-auth'>
                    <form id="login-form" onSubmit={this.handleSubmit}>
                        <div className="form-title-group">
                            <h1 className="form-title is-active">{text}</h1>
                            <a href="#" className="form-title" onClick={this.handleSetRegister}>Регистрация</a>
                        </div>
                        <div className={`form-auth__inner ${onLoadClass}`}>
                            <Login
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                label={is_auth_v2 ? 'E-mail / Телефон' : 'Логин'}
                                form_control_server_errors={controls_server_errors.login}
                                value_prop={login}
                                name='login'
                            />
                            <Password
                                handleComponentChange={this.handleChange}
                                handleComponentValidation={this.handleComponentValid}
                                form_control_server_errors={controls_server_errors.password}
                                value_prop={form.password}
                                name='password'
                            />
                            <div className="form-field form-field_checkbox-field">
                                <div className="row">
                                    <div className="col-xl-7">
                                        <label className="form-checkbox">
                                            <input type="checkbox" name="remember_me" value="1" />
                                            <span className="form-checkbox__field" />
                                            <span className="form-checkbox__label">Запомнить пароль</span>
                                        </label>
                                    </div>
                                    <div className="col-xl-5">
                                        <div className="right-link-wrapper">
                                            <a href="/forgot/password/reset" className="forgot-password-link">Забыли пароль?</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            { is_show_captcha &&
                            <div className="captcha-wrapper">
                                <div id="captcha_login" className="g-recaptcha" data-sitekey={captcha_sitekey} />
                                <div className="captcha-overlay captcha-overlay_position-top"/>
                                <div className="captcha-overlay captcha-overlay_position-right"/>
                                <div className="captcha-overlay captcha-overlay_position-bottom"/>
                                <div className="captcha-overlay captcha-overlay_position-left"/>
                                <div className="captcha-image"/>
                                <div className="captcha-links">
                                    <a href="https://www.google.com/intl/ru/policies/privacy/"target="_blank">Конфиденциальность</a>
                                    <span aria-hidden="true" role="presentation"> - </span>
                                    <a href="https://www.google.com/intl/ru/policies/terms/" target="_blank">
                                        Условия использования
                                    </a>
                                </div>
                            </div>
                            }
                            <div className="form-field">
                                <button
                                    className="button-action button-action_type-filed_color-blue button-action_type-filed"
                                    type="submit"
                                    id="auth_register_button"
                                    disabled={this.isSubmitButtonDisabled()}
                                    name="submit"
                                >
                                    Войти
                                </button>
                            </div>
                            <ShowError
                                existing_errors={server_errors}
                                messages={server_errors_messages}
                            />
                        </div>
                    </form>
                </div>
            </FormAnimation>
        );
    }
}

LoginForm.propTypes = {
    Login: PropTypes.element,
    Password: PropTypes.element,
    ShowError: PropTypes.element
};


