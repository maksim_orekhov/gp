import React from 'react';
import PropTypes from 'prop-types';
import Mixins from '../../input-mixins/input-mixin';
import ShowError from '../../messages/ShowError.jsx';
import update from 'immutability-helper';
import VALIDATION_RULES from "../../../../../../application/react/constants/validation_rules";
import ControlBase from "../../../../../../application/react/controls/ControlBase";

export default class PasswordInput extends ControlBase {
  constructor() {
    super();
    this.state = {
        is_blur: false,
        validation_errors: {
            min_chars: null,
            server_error: false
        },
        validation_rules: {
            min_chars: VALIDATION_RULES.PASSWORD.min_chars
        },
        error_messages: {
            server_error: {
                header: 'Ошибка ввода',
                text: ''
            },
            min_chars: {
                header: 'Ошибка ввода',
                text: 'Пароль должен содержать от 6 до 64 символов'
            }
        },
        type_input: 'password',
        isShowPassword: false
    };

    this.showPassword = this.showPassword.bind(this);
    this.handleChangeEvent = this.handleChangeEvent.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  };

    componentWillReceiveProps(nextProps) {
        const { value_prop } = nextProps;

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent(e) {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                min_chars: !this.validateMinCharsLength(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateMinCharsLength(value)
        );
    }

    showPassword() {
        this.setState({
            isShowPassword: !this.state.isShowPassword,
            type_input: !this.state.isShowPassword ? 'text' : 'password'
        });
    }

    handleBlur() {
        this.setState({
            is_blur: true
        })
    }

  render() {
    const { validation_errors, error_messages, is_blur } = this.state;
    const { value_prop, name } = this.props;

    return (
            <div className="form-field">
                <label htmlFor="login-password">Пароль</label>
                <div className="form-input">
                    <input
                        type={this.state.type_input}
                        onChange={this.handleChangeEvent}
                        onBlur={this.handleBlur}
                        className={this.colourInputField()}
                        id="login-password"
                        value={value_prop}
                        name={name}
                    />
                    <span
                        className="toggle-password-field-type-button"
                        onClick={this.showPassword}
                    />
                    {
                        is_blur &&
                        <ShowError
                            existing_errors={validation_errors}
                            messages={error_messages}
                        />
                    }

                </div>
            </div>
        );
  }
}

PasswordInput.propTypes = {
    value_prop: PropTypes.string,
    name: PropTypes.string
};
