import React from 'react';
import PropTypes from 'prop-types';
import Mixins from '../../input-mixins/input-mixin';
import ShowError from '../../messages/ShowError.jsx';
import update from 'immutability-helper';
import ControlBase from "../../../../../../application/react/controls/ControlBase";
import VALIDATION_RULES from "../../../../../../application/react/constants/validation_rules";
import PasswordInput from "./password-input";

export default class Login extends ControlBase {
    constructor() {
        super();
        this.state = {
            is_blur: false,
            validation_errors: {
                server_error: false,
                min_chars: null,
                max_words: null,
            },
            validation_rules: {
                min_chars: VALIDATION_RULES.LOGIN.min_chars,
                max_words: VALIDATION_RULES.LOGIN.max_words
            },
            error_messages: {
                server_error: {
                    header: 'Ошибка ввода',
                    text: ''
                },
                min_chars: {
                    header: 'Ошибка ввода',
                    text: `Минимальная длина логина ${VALIDATION_RULES.LOGIN.min_chars} символа`
                },
                max_words: {
                    header: 'Ошибка ввода',
                    text: `Максимальное количество слов - ${VALIDATION_RULES.LOGIN.max_words}`
                }
            }
        };

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    };

    componentWillReceiveProps(nextProps) {
        const { value_prop } = nextProps;

        value_prop && this.validateControl(nextProps);
    }

    handleChangeEvent(e) {
        const value = e.target.value;
        const { name, handleComponentChange, handleComponentValidation, validate_prop = `${name}_is_valid` } = this.props;
        handleComponentChange && handleComponentChange(name, value);
        handleComponentValidation && handleComponentValidation(validate_prop, this.checkAllValidations(value));
    }

    updateErrors(value) {
        this.setState({
            validation_errors: {
                ...this.state.validation_errors,
                min_chars: !this.validateMinCharsLength(value),
                max_words: !this.validateMaxWordsLength(value)
            }
        });
    }

    // Метод проверяющий все валидации, который срабатывает при событии onBlur input'а
    checkAllValidations(value) {
        this.updateErrors(value);
        return (
            this.validateMinCharsLength(value)
            && this.validateMaxWordsLength(value)
        );
    }

    handleBlur() {
        this.setState({
            is_blur: true
        })
    }

    render() {
        const { label, value_prop, name } = this.props;
        const { validation_errors, error_messages, is_blur } = this.state;

        return (
            <div className="form-field">
                <label htmlFor="login">{label}</label>
                <div className="form-input">
                    <input
                        type="text"
                        className={this.colourInputField()}
                        onChange={this.handleChangeEvent}
                        onBlur={this.handleBlur}
                        value={value_prop}
                        id="login"
                        name={name}
                    />
                    {
                        is_blur &&
                        <ShowError
                            existing_errors={validation_errors}
                            messages={error_messages}
                        />
                    }
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    value_prop: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string
};
