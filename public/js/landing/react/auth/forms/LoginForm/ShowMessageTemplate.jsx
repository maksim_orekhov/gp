import React from 'react';




export default function showMessageTemplate(props) {
    return(

        <div className="form-field">
            <div className={"form-alert " + props.color_class}>
                {/*<div className="form-alert__header">{props.rules.header}</div>*/}
                {/*<div className="form-alert__text">{props.rules.text}</div>*/}
                <div className="form-alert__text">{props.rules}</div>
            </div>
        </div>

    )
}