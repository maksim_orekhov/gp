import { CSSTransitionGroup } from 'react-transition-group' ;
import React from 'react';

export default class FormAnimation extends React.Component {
    render() {
        return (
            <CSSTransitionGroup
                transitionName="form"
                transitionAppear={true}
                transitionAppearTimeout={500}
                transitionEnter={false}
                transitionLeave={true}
                transitionLeaveTimeout={5000}
            >
                {this.props.children}
            </CSSTransitionGroup>
        )
    }
}