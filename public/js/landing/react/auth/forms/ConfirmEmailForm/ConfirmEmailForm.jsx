import React from 'react';
import CodeEmail from './controls/email-input.jsx';
import Submit from '../../buttons/SubmitButton.jsx';

import Refresh from '../../buttons/RefreshButton.jsx';
import Mixins from '../input-mixins/input-mixin';
import DidMountMixin from '../form-mixins/did_mount.jsx';
import update from 'immutability-helper';
import ShowError from '../messages/ShowError.jsx';

import FormAnimation from '../animations/form_appear_group.jsx';

export default class ConfirmEmailForm extends DidMountMixin {
    constructor() {
        super();
        this.html_form_url = '/email/confirm';
        this.state = {
            codeIsValid:               false,
            validation_errors:  {
                connection_is_lost:     false,
                error_from_server:      false,
                too_fast_resend:        false,
            },
            form: {
                //code: null
            },
            messages:{
                errors: {
                    connection_is_lost: {
                        header:     "Соединение с сервером потеряно",
                        text:       "Проверьте ваше соединение с интернетом и попробуйте еще раз."
                    },
                    error_from_server:{
                        header:     "Сервер возвратил ошибку",
                        text:       "При обработке сервером что-то пошло не так."
                    },
                    too_much_requests: {
                        header:     "Слишком много запросов",
                        text:       "Вы привысили суточное разрешенное количество запросов. Обрабтитесь в техподдержку."
                    },
                    too_fast_request: {
                        header:     "Слишком частые запросы",
                        text:       "Вы запрашиваете код слишком часто. Подождите минуту и попробуйте снова."
                    },
                }
            }
        };
    };

    handleValid(name,isTrue){
        this.setState({[name] : isTrue})
    };

    ajaxFormSend(url){
        return $.ajax({
            dataType: "json",
            type: 'POST',
            url: url,
            data: JSON.stringify(
                    this.state.form
            )
        });
    };
    ajaxEmailResend(url){
        return $.ajax({
            dataType: "json",
            type: 'GET',
            url: url,
            data: ''
        });
    };

    toFinishRegistration(){
        this.props.nextStep('register_success_message');
    }

    handleSubmit(e){
        e.preventDefault();
        this.ajaxFormSend('/email/confirm')
            .then(
                data => this.ajaxConnectionSuccess(data),
                error => this.ajaxConnectionIsFail(error)
            );
    };

    handleRefresh(e){
        e.preventDefault();
        this.ajaxEmailResend('/email/resend')
            .then(
                data => this.ajaxEmailResendSuccess(data),
                error => this.ajaxConnectionIsFail(error)
            );
    };

    setErrorFastRequest(boolean){
        const newData = update(this.state.validation_errors, {
            too_fast_request: {$set: boolean}
        });
        this.setState({
            validation_errors:newData
        });
    }

    setServerReturnedError(boolean){
        const newData = update(this.state.validation_errors, {
            error_from_server: {$set: boolean}
        });
        this.setState({
            validation_errors:newData
        });
    }

    ajaxConnectionSuccess(data){
        if(data.status === 'SUCCESS'){
            this.toFinishegistration();
        }
        else{
            this.setServerReturnedError(true);
        }
        return data;
    };

    ajaxEmailResendSuccess(data){
        console.log(data);
        if (data.status === 'ERROR'){
            if(data.message === 'Not enough time has passed since the last time'){
                this.setErrorFastRequest(true)
            }else{
                this.setServerReturnedError(true)
            }
        }else{
            this.setErrorFastRequest(false);
            this.setServerReturnedError(false);
        }
        return data;
    };

    ajaxConnectionIsFail(err){
        console.log(err);
        this.setConnectionIsLost(true);
    };

    isSubmitButtonDisabled(){
        if(!
                (
                    this.state.codeIsValid
                )
        )
        {
            return true;
        }
        return false;
    }
    handleValue(name, value){
        const newData = update(this.state.form, {
            [name]: {$set: value}
        });
        
        this.setState({
            form: newData
        })
    }
    render() {
        let state = this.state;
        if(this.props.isHidden)return null;
        return (
            <FormAnimation>
                <div className="fadeInUp form-register">
                    <h1 className="form-title">Подтверждение Email</h1>
                    <div className="registration__small_text">
                        <p>Для полноценного использования системы просим вас подтвердить ваш адрес электронной почты. Все необходимые инструкции отправлены на указанный вами email.</p>
                        <p>На Ваш почтовый ящик отправлено сообщение, содержащее ссылку для подтверждения правильности e-mail адреса. Пожалуйста, перейдите по ссылке для завершения регистрации.</p>
                        <p>Если письмо не пришло в течение 15 минут, проверьте папку «Спам». Если же письма нет и в папке «Спам», попробуйте зарегистрироваться еще раз. Возможно, вы ошиблись при вводе адреса электронной почты.</p>
                    </div>
                        {/*<form
                            onSubmit={this.handleSubmit.bind(this)}
                            className="confirm-form"
                            id="confirm-form">
                            <ShowError existing_errors={state.validation_errors} messages={state.messages.errors} />
                            <CodeEmail isValid={this.handleValid.bind(this)}
                                   componentValue={this.handleValue.bind(this)} />
                            <input type="submit" className="btn btn-blue btn-confirm" value="Отправить" />
                            <button type="button" className="btn btn-grey glyphicon glyphicon-retweet btn-resend" onClick={this.handleRefresh.bind(this)}/>

                        </form>*/}
                </div>
            </FormAnimation>
        );
    }
}