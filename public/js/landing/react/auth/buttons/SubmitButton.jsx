import React from 'react';

export default class Submit extends React.Component {
    buttonText() {
        if(this.props.title){
            return this.props.title;
        }
        return "Отправить";
    }

    render() {
        return (
          <div className="form-field">
              <button type="submit" className="button-action button-action_type-filed_color-blue button-action_type-filed" disabled={this.props.disabled} id="auth_register_button_send_data">Сбросить пароль</button>
          </div>
        );
  }
}
