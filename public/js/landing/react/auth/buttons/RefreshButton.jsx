import React from 'react';

export default class Refresh extends React.Component {

    render() {
        return (
            <div className="form-actions form-group form-control-static">
                <div className="submit_container_right">
                    <button type="button" className="tokenrefresh-button glyphicon glyphicon-retweet" />
                </div>
            </div>
        );
    }
}