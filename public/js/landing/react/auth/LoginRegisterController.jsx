import React from 'react';
import Mixins from './forms/input-mixins/input-mixin';
import LoginForm from "./forms/LoginForm/LoginForm.jsx";
import RegisterLinkController from './RegisterLinkController.jsx';


export default class ChangeProfileInfo extends Mixins {


    switchOutput(){
        if(!this.props.is_login){
            return(
                <RegisterLinkController text="Регистрация" />
            );
        }else{
            return(
                <LoginForm text="Вход" />
            );
        }
    }
    render(){
        return (
            <div>
                {this.switchOutput()}
            </div>
        );

    }
}
