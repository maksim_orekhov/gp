import React from 'react';

import PasswordChangeForm from './forms/PasswordChangeForm/PasswordChangeForm.jsx';
import SuccessForm from "./forms/PasswordChangeForm/SuccessChangePasswordForm";



export default class ForgotPasswordLinkController extends React.Component {
    constructor() {
        super();
        this.state = {
            current_form: 'change_password',
            user_login: null
        };

        this.handleNext = this.handleNext.bind(this);
    };

    handleNext(next_form){
        this.setState({
            current_form: next_form
        });
    }

    formRender(){
        let forms = {
            'change_password': <PasswordChangeForm nextStep={this.handleNext} />,
            'change_password_success_message':
                <SuccessForm
                    nextStep={this.handleNext}
                    nextStepName="finish_registration"
                />,
        };

        let current_form = forms[this.state.current_form];
        return current_form;
    }

    render() {
        return (
            <div>
                {this.formRender()}
            </div>
        );
    }

}

