import React from 'react';

import RegisterForm from './forms/RegisterForm/RegisterForm.jsx';
import LoginForm from "./forms/LoginForm/LoginForm.jsx";
import SuccessForm from './forms/SuccessForm.jsx';
import ConfirmEmailForm from './forms/ConfirmEmailForm/ConfirmEmailForm.jsx';


export default class RegisterLinkController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            form_is_hidden: true,
            current_form: 'register',
            invitationMessage: props.invitationMessage,
            invitationEmail: props.invitationEmail,
            invitationToken: props.invitationToken,
            user: {
                login: '',
                phone: null,
                email: null,
                password: null
            }
        };

        this.handleNext = this.handleNext.bind(this);
        this.handleRegisterFormValues = this.handleRegisterFormValues.bind(this);
    };

    componentWillMount(){
        if(this.props.current_form){
            this.setState({
                current_form: this.props.current_form
            });
        }
    }
    handleNext(next_form){
        this.setState({
            current_form: next_form
        });
    }

    handleRegisterFormValues(form_array){
        this.setState({
            user: form_array
        });
    }

    formRender(){
        const { invitationMessage, invitationEmail, invitationToken, is_auth_v2 } = this.props;

        const forms = {
            login:
                <LoginForm
                    text="Вход"
                    nextStep={this.handleNext}
                    is_auth_v2={is_auth_v2}
                />,
            register:
                <RegisterForm
                    text="Регистрация"
                    invitationMessage={invitationMessage}
                    invitationEmail={invitationEmail}
                    invitationToken={invitationToken}
                    nextStep={this.handleNext}
                    registerFormValues={this.handleRegisterFormValues}
                    is_auth_v2={is_auth_v2}
                />,
            confirm_email:
                <ConfirmEmailForm
                    nextStep={this.handleNext}
                    user_email={this.state.user.email}
                />,
            // 'register_success_message':
            //                     <SuccessForm
            //                                     nextStep={this.handleNext.bind(this)}
            //                                     nextStepName="finish_registration"
            //                                     message={
            //                                         {
            //                                             'header': 'Создана учетная запись!',
            //                                             'message': 'Спасибо за регистрацию!'
            //                                         }
            //                                     }
            //                     />
            // 'confirm_phone_success_message':
            //                     <SuccessForm
            //                                     nextStep={this.handleNext.bind(this)}
            //                                     nextStepName="confirm_email"
            //                                     message={
            //                                         {
            //                                             'header': 'Телефон подтвержден!',
            //                                             'message': 'Для завершения регистрации пожалуйста подтвердите адрес электронной почты.'
            //                                         }
            //                                     }
            //                     />,
            // 'confirm_email_success_message':
            //                     <SuccessForm
            //                                     nextStep={this.handleNext.bind(this)}
            //                                     nextStepName="finish_registration"
            //                                     message={
            //                                         {
            //                                             'header': 'Email подтвержден!',
            //                                             'message': 'Спасибо за регистрацию!'
            //                                         }
            //                                     }
            //                     />,
        };

        const current_form = forms[this.state.current_form];
        return current_form;
    }


    registerFormRender(){
            return (
                    <div>
                        {this.formRender()}
                    </div>
            )
    }

    render() {
        return (
            <div>
                {this.registerFormRender()}
            </div>
        );
    }

}

