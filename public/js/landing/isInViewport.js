/**
 * Проверяет находится ли элемент во вьюпорте
 * @param {boolean} onCenter - находится ли элемент по центру экрана
 * @returns {boolean}
 */
$.fn.isInViewport = function(onCenter = false) {
    const elementTop = $(this).offset().top;
    const elementBottom = elementTop + $(this).outerHeight();

    const viewportTop = $(window).scrollTop();
    const windowHeight = $(window).height();
    const viewportBottom = viewportTop + windowHeight;
    if(!onCenter){
        return elementBottom > viewportTop && elementTop < viewportBottom;
    }
    return elementBottom > viewportTop + windowHeight / 2 && elementTop < viewportBottom - windowHeight / 2;
};