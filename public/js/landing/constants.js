export const BREAKPOINT = {
    TABLET: 1024,
    MOBILE: 767,
};

export const SLIDE_HASH = [
  '#pokupatelyam',
  '#prodavczam',
  '#biznesu',
  '#ecommerce',
  '#partneram',
  '#informaciya',
];