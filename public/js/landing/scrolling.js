const Scrolling = {
    enable: function () {
        $('html, body, #content-wrapper').removeClass('popup-is-open');
    },

    disable: function () {
        $('html, body, #content-wrapper').addClass('popup-is-open');
    },

    toggle: function () {
        $('html, body, #content-wrapper').toggleClass('popup-is-open');
    },
    /**
     * Скролит страницу к указанному элементу
     * @param {string} selector - css селектор элемента
     * @param {number} offset - смещение
     * @param {number} time - время анимации в мс
     * @returns {boolean}
     */
    scrollTo: function (selector, offset = 0, time = 500) {
        const $element = $(selector);
        if(!$element.length){
            return false;
        }
        $('html, body').stop(true).animate({
            scrollTop: `${$element.offset().top + offset}px`
        }, time);
    }

};

export default Scrolling;
