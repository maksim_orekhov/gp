import { BREAKPOINT, SLIDE_HASH } from './constants';
import Scrolling from './scrolling';

const Information = {
    _activeIndex: 0,
    $section: null,
    $smartLine: null,
    slideIsChanging: false,
    smartLineOffset: 15,
    autoPlay: false,
    autoPlayTime: 6000,
    _autoPlayInterval: 0,

    init: function () {
        this.$section = $('section.information');
        if(!this.$section.length) {
            return false;
        }

        this.$smartLine = $('.slide-controls__smartline');
        const $controlItems = $('.slider-control-item');
        const $screenSlides = this.$section.find('.screen-slide');

        this._setSlideHashes($controlItems);
        this._initActiveSlide($controlItems, $screenSlides);
        this.moveSmartLineTo($controlItems.eq(this._activeIndex));
        this.$smartLine.addClass('smart-line');
        this._setDataNumbersToItems($screenSlides);

        if(this.autoPlay){
            this._autoPlayInterval = setInterval(() => {
                let $nextItem = $controlItems.eq(this._activeIndex + 1);
                if(!$nextItem.length){
                    $nextItem = $controlItems.eq(0);
                }
                this.changeSlide($nextItem);
            }, this.autoPlayTime);
        }

        $controlItems.on('click', ev => {
            ev.preventDefault();
            const $this = $(ev.target);
            this.changeSlide($this);
            if(this.autoPlay){
                this.autoPlay = false;
                clearInterval(this._autoPlayInterval);
            }
        });

        this.$section.find('.slider-menu-header').on('click', ev => {
            this.closeMobileSliderMenu();
        });

        /**
         * При заврешении анимации активного слайда удаляем все клонированные элементы
         */
        this.$section.on('webkitTransitionEnd oTransitionEnd otransitionend transitionend', '.screen-slide.active', ev => {
            const $slide = $(ev.target);
            if(!$slide.hasClass('leave')){
                $slide.prevAll('.leave').remove();
            }
        });

        this.$section.find('.screen-slider').addClass('is-loaded');
    },

    /**
     * Устанавливает слайдам значения location.hash параметров
     * @param $items
     * @private
     */
    _setSlideHashes: function ($items) {
        $items.each((index, item) => {
            $(item).data('slide-name', SLIDE_HASH[index]);
        });
    },

    _setDataNumbersToItems: function($items) {
        $items.each(function (index) {
            $(this).attr('data-slide', index);
        });
        //Костыль для дублирования первого слайда
        const $firstItem = $items.eq(this._activeIndex).clone();
        $firstItem.removeClass('active');
        $firstItem.appendTo(this.$section.find('.screen-slider'));
    },

    changeSlide: function($item) {
        if($item.index() !== this._activeIndex){
            this.setActiveSlide($item.index());
            this.setLocationHash($item);
            this.moveSmartLineTo($item);
        }
        if(
            $(window).width() <= BREAKPOINT.MOBILE
            && this.$section.find('.about-category').eq($item.index()).children().length
        ){
            this.openMobileSliderMenu();
        }
    },

    /**
     * Устанавливает активный первый слайд по значению location.hash
     * @param $items
     * @private
     */
    _initActiveSlide: function($items) {
        const $activeItem = $items.filter(function () {
            return $(this).data('slide-name') === window.location.hash;
        });
        if($activeItem.length){
            this._activeIndex = $activeItem.eq(0).index();
        }

        this._setActiveSliderControl(this._activeIndex);
        this._setActiveCategory(this._activeIndex);
        this._setActiveScreenSlide(this._activeIndex);
    },

    setLocationHash: function($item) {
      window.location.hash = $item.data('slide-name');
    },

    /**
     * Анимирует смену слайдов через клонирование элементов
     * @param index
     * @private
     */
    _changeActiveScreen: function(index) {
        const $clonedItem = this.$section.find(`.screen-slide[data-slide="${index}"]`).eq(0).clone();
        $clonedItem.removeClass('active leave');
        $clonedItem.appendTo(this.$section.find('.screen-slider'));
        $clonedItem.siblings('.active').addClass('leave');
        setTimeout(() => {
            $clonedItem.addClass('active');
        }, 50);
    },

    setActiveSlide: function (index) {
        this._activeIndex = index;
        this._changeActiveScreen(index);
        this._setActiveSliderControl(index);
        this._setActiveCategory(index);
    },

    openMobileSliderMenu: function () {
        this.$section.find('.about-categories-wrapper').addClass('is-open');
        Scrolling.disable();
    },

    closeMobileSliderMenu: function () {
        this.$section.find('.about-categories-wrapper').removeClass('is-open');
        Scrolling.enable();
    },

    _setActiveSliderControl: function (index) {
        this.$section
            .find('.slider-control-item').eq(index).addClass('active')
            .siblings('.slider-control-item').removeClass('active');
    },

    _setActiveCategory: function(index) {
        this.$section
            .find('.about-category').eq(index).addClass('active')
            .siblings('.about-category').removeClass('active');
    },

    _setActiveScreenSlide: function(index) {
        this.$section
            .find('.screen-slide').eq(index).addClass('active')
            .siblings('.screen-slide').removeClass('active');
    },

    /**
     * Двигает линию к активному пункту меню слайдера
     * @param $item
     */
    moveSmartLineTo: function ($item) {
        const smartLineWidth = $item.outerWidth();
        const smartLineHeight = $item.outerHeight();
        const smartLineLeftPos = $item.position().left;
        const smartLineTopPos = $item.position().top;

        if ($(window).width() > BREAKPOINT.TABLET) {
            this.$smartLine.css({
                'left': `${smartLineLeftPos + this.smartLineOffset}px`,
                'width': `${smartLineWidth - this.smartLineOffset * 2}px`,
                'top': '',
                'height': ''
            });
        } else {
            this.$smartLine.css({
                'top': `${smartLineTopPos}px`,
                'height': `${smartLineHeight}px`,
                'left': '',
                'width': ''
            });
        }

    }

};

export default Information;