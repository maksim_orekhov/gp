;
/**
 * Плагин для смартлайна
 */
(function ($) {
    let $SmartLine = null;

    let pluginConfig = {
        activeItemSelector: '.active', //Селектор активного элемента
        lineOffset: 0, //Смещение для лини, если испоьзуются колонки,
        needMouseHandlers: false,
        needClickHandler: true,
    };

    const method = {
        $siblings: null,

        init: function ($smartLine) {
            $SmartLine = $smartLine;
            this.$siblings = $SmartLine.siblings();
            this.moveSmartLineTo(this.getActiveItem());
            $SmartLine.addClass('smart-line');
            this.initHandlers();
        },

        initHandlers: function () {
            if(pluginConfig.needMouseHandlers){
                this.$siblings.on('mouseover', this.handleMouseOver).on('mouseout', this.handleMouseOut);
            }
            if(pluginConfig.needClickHandler){
                this.$siblings.on('click', this.handleClick);
            }
        },

        handleMouseOver: function (ev) {
            method.moveSmartLineTo($(ev.target));
        },

        handleMouseOut: function (ev) {
            method.moveSmartLineTo(method.getActiveItem());
        },

        handleClick: function (ev) {
            method.moveSmartLineTo($(ev.target));
        },

        getActiveItem: function(){
            return this.$siblings.filter(pluginConfig.activeItemSelector);
        },

        moveSmartLineTo: function ($item) {
            const smartLineWidth = $item.width();
            const smartLineLeftPos = $item.position().left;
            $SmartLine.css({
                'left': `${smartLineLeftPos + pluginConfig.lineOffset}px`,
                'width': `${smartLineWidth - pluginConfig.lineOffset * 2}px`
            });
        },

        destroy: function () {
            if(pluginConfig.needMouseHandlers) {
                this.$siblings.off('mouseover', this.handleMouseOver).off('mouseout', this.handleMouseOut);
            }
            if(pluginConfig.needClickHandler){
                this.$siblings.off('click', this.handleClick);
            }
            $SmartLine.removeClass('smart-line');
        }

    };

    $.fn.smartLine = function (userConfig) {
        pluginConfig = $.extend({}, pluginConfig, userConfig);
        return this.each(function () {
            method.init($(this));
        });
    }

})(jQuery);
