import 'featherlight';

import React from 'react';
import ReactDOM from 'react-dom';

import ControllerHowItWorks from './react/HowItWorks/ControllerHowItWorks';
import LoginRegisterController from './react/auth/RegisterLinkController.jsx';
import ForgotPasswordLinkController from './react/auth/ForgotPasswordLinkController.jsx';
import ChangePasswordLinkController from './react/auth/ChangePasswordLinkController.jsx';

import Scrolling from './scrolling';
import Information from  './Information';
import './isInViewport';
import Preloader from './Preloader';
import './oldBrowserNotify';

$(function () {

    /*
    ** react
    */
    if (document.getElementById('how-it-works-react')) {
        const rootEl = document.getElementById('how-it-works-react');
        ReactDOM.render(<ControllerHowItWorks />, rootEl);
    }

    if (document.getElementById('password-reset-form')) {
        const rootEl = document.getElementById('password-reset-form');
        ReactDOM.render(<ForgotPasswordLinkController />, rootEl);
    }

    if (document.getElementById('change-password-controller')) {
        const rootEl = document.getElementById('change-password-controller');
        ReactDOM.render(<ChangePasswordLinkController />, rootEl);
    }

    /**
     * Auth
     */
    let invitationMessage, invitationEmail, invitationToken;
    let element = document.getElementById('register-block');
    let is_login =  'register';
    let is_auth_v2;

    if (element) {
        const invitationBlock = document.getElementById('register-invitation');
        is_auth_v2 = document.getElementById('register-block').getAttribute('data-auth');
        if (invitationBlock) {
            invitationMessage = invitationBlock.innerHTML.trim();
            invitationEmail = invitationBlock.getAttribute('data-invitation-email').trim();
            invitationToken = invitationBlock.getAttribute('data-invitation-token').trim();
        }
    } else {
        element = document.getElementById('login-block');
        if (element) {
            is_login =  'login';
            is_auth_v2 = document.getElementById('login-block').getAttribute('data-auth');
        }
    }

    if (element) {
        ReactDOM.render(
            <LoginRegisterController
                current_form={is_login}
                invitationMessage={invitationMessage || null}
                invitationEmail={invitationEmail || null}
                invitationToken={invitationToken || null}
                is_auth_v2={is_auth_v2 || null}
            />,
            element
        );
    }
    // --------------- END AUTH --------------------- //

    /**
     * Forgot password
     */


    $('.menu-burger').on('click', function (event) {
        $('body').toggleClass('menu-is-open');
        Scrolling.toggle();
    });
    /**
     * Скролит к указанному в дата-атрибуте блоку
     */
    $('body').on('click', '[data-scroll-to]', function (event) {
        event.preventDefault();
        const elementSelector = $(this).data('scroll-to');
        Scrolling.enable();
        $('body').removeClass('menu-is-open');
        Scrolling.scrollTo(elementSelector);
    });


    $(window).on('scroll', handleAnimationsByScroll);

    /**
     * Обработчик отправки форм с лендинга
     */
    $('.guarant-premier-form, .integration-form').on('submit', function (ev) {
        ev.preventDefault();
        const $this = $(this);
        const formData = {
            typeForm: $this.hasClass('guarant-premier-form') ? "сообщение с формы guarantPremierForm" : "сообщение с формы Интеграция",
            emailOrPhone: "Телефон или Email: " + $this.find('[name="emailOrPhone"]').val()
        };

        if($this.find('[name="emailOrPhone"]').val().length < 4 || $this[0]._isSending){
            return false;
        }

        $this[0]._isSending = true;
        $.ajax({
            type: $this.attr('method'),
            url: $this.attr('action'),
            data: JSON.stringify(formData),
            dataType: 'json',
            processData: false,
            success: function (response) {
                $this.one('animationend', () => {
                    $this.remove();
                });
                $this[0].reset();
                $this[0]._isSending = false;
                $this.addClass('animation-leave')
                    .closest('.form-wrapper')
                    .find('.form-message').show();
            }
        });
    });

    //Закрытие попапа с формой логина по клику мимо
    const $dropdownLoginForm = $('.nav-item_with-dropdown');
    const $dropDownCheckBox = $dropdownLoginForm.find('.dropdown-checkbox');
    $(window).on('click',function (ev) {
        if($dropDownCheckBox.is(':checked') && !$dropdownLoginForm.has(ev.target).length){
            $dropDownCheckBox.prop('checked', false);
        }
    });

    $('.footer-dropdown-checkbox').on('change', function (ev) {
        const $this = $(this);
        $this.closest('.footer-body').find('.footer-dropdown-checkbox').not(this).prop('checked', false);
    });

    /**
     * Смещает блок с картинкой на мобильном разрешении
     */
    $('.section-image-wrapper').on('click', function () {
        $(this).closest('.section-with-image').toggleClass('image-is-visible');
    });

    /**
     * Предзагрузка картинок на лендинге
     */
    const $preloader = $('.preloader');
    if($preloader.length){
        let images = [];
        $('.slide-image, .guarant-premier').each(function () {
            let bgImage = $(this).css('background-image');
            images.push(bgImage.substring(5, bgImage.length-2));
        });
        Scrolling.disable();
        const landingPreloader = new Preloader(images, () => {
            Scrolling.enable();
            $preloader.fadeOut(200, function () {
                $(this).remove();
            });
            //Скрол к секции из хэша для FF, а то из-за прелоадера он вверх скрол перекидывает
            const locationHash = window.location.hash;
            const $section = $(locationHash);
            if($section.length) {
                window.scrollTo(0, $section.offset().top);
            }
            Information.init();
        });
        landingPreloader.load();
    }

    //Открытие чата carrot quest
    $('body').on('click', '.open-chat-link', function (e) {
        e.preventDefault();
        window.carrotquest && window.carrotquest.open();
    });

});

/**
 * Обработчик скролла для запуска анимаций
 * @param ev
 */
function handleAnimationsByScroll(ev) {
    const $window = $(window);
    const $readyForAnimationBlock = $('.js-animate');
    if(!$readyForAnimationBlock.length){
        $window.off('scroll', handleAnimationsByScroll);
    }

    $readyForAnimationBlock.each(function () {
        const $block = $(this);
        if($block.isInViewport(true)){
            $block.addClass('js-animated').removeClass('js-animate');
        }
    })
}