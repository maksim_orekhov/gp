(() => {
    // возвращает cookie с именем name, если есть, если нет, то undefined
    const getCookie = (name) => {
        const matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    };

    /**
     * Проверяет поддерживается ли css свойство браузером
     * @param {string} property - название css свойства
     * @param {string} value - значение css свойста
     * @return {boolean} - поддерживается ли свойство или нет
     */
    const SupportsCSS = function (property, value) {
        // Создаём элемент
        const element = document.createElement('span');
        // Проверяем, поддерживает ли браузер данное свойство
        if (element.style[property] !== undefined) {
            element.style.cssText = property + ':' + value; // Вносим новое свойство в style элемента
        } else {
            return false; // Если браузер не поддерживает свойство, то возвращаем false
        }
        // Если браузер поддерживает данное значение для указанного свойства, то значения будут равны
        return element.style[property] === value;
    };

    const createCloseButton = () => {
        const button = document.createElement('span');

        button.innerHTML = 'Скрыть';

        button.style.cursor = 'pointer';
        button.style.position = 'absolute';
        button.style.top = '15px';
        button.style.right = '15px';
        button.style.color = '#2F80ED';
        button.style.fontWeight = 'bold';
        button.style.fontSize = '15px';
        button.style.lineHeight = '15px';

        button.addEventListener('click', () => {
            const notification = document.getElementById('js-old-browser-notification');
            notification.style.display = 'none';
            document.cookie = "js-old-browser-notification=true; path=/;"
        });

        return button;
    };

    const createNotificationText = () => {
        const text = document.createElement('span');

        text.innerHTML = 'Ваш браузер устарел, попробуйте обновить его до новой версии, либо использовать современный браузер.';
        text.style.fontSize = '20px';
        text.style.fontWeight = '400';
        text.style.color = '#000';

        return text;
    };

    const createNotification = () => {
        const notification = document.createElement('p');
        notification.id = 'js-old-browser-notification';

        notification.style.position = 'relative';
        notification.style.width = '100%';
        notification.style.background = '#ffcc66';
        notification.style.padding = '30px';
        notification.style.textAlign = 'center';
        notification.style.zIndex = '100000';

        return notification;
    };

    if (!SupportsCSS('display', 'flex')) {
        if (!getCookie('js-old-browser-notification')) {
            const notification = createNotification();
            const button = createCloseButton();
            const text = createNotificationText();

            notification.appendChild(text);
            notification.appendChild(button);

            document.body.insertBefore(notification, document.body.firstChild);
        }
    }
})();