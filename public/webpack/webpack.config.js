const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractCss = new ExtractTextPlugin({
    filename: '[name].css',
    allChunks: true
});

module.exports = {
    module: {
        loaders: [
            {
                test: /\.js(x*)?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['react', 'es2015', 'stage-0']
                }
            }, {
                test: /\.less$/,
                use: extractCss.extract({
                    use: [{
                        loader: 'css-loader',
                        options: {
                            url: false
                        }
                    }, {
                        loader: 'postcss-loader',
                        options: {
                            config: {
                                path: path.resolve(__dirname, 'postcss.config.js')
                            }
                        }
                    }, {
                        loader: 'less-loader'
                    }],
                    fallback: 'style-loader'
                })
            }
        ]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        extractCss
    ],
    devtool: 'cheap-eval-source-map',
    stats: { children: false },
    resolve: {
        extensions: ['.js', '.jsx', '.css', '.less']
    },
    entry: {
        application: ['babel-polyfill', 'whatwg-fetch', path.resolve(__dirname, '../js/application/index.jsx'), path.resolve(__dirname, '../css/Application.less')],
        landing: ['babel-polyfill', path.resolve(__dirname, '../js/landing/index.js'), path.resolve(__dirname, '../css/Landing.less')],
        rules: [path.resolve(path.resolve(__dirname, '../css/Rules.less'))],
        'deal-button': [path.resolve(__dirname, '../css/DealButton.less')]
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, '../build'),
        sourceMapFilename: '[name].js.map'
    }
};

