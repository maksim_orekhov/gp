import React from 'react';
import FormSoleProprietor from '../../../js/application/react/forms/FormSoleProprietor';
import * as SetControlsErrorFromUpperForm from './Helpers/SetControlsErrorFromUpperForm';
import * as Csrf from './Helpers/Csrf';
import * as CheckAllControlsAreValid from './Helpers/CheckAllControlsAreValid';
import * as SendUpFormData from './Helpers/SendUpFormData';
import * as ButtonSubmit from './Helpers/ButtonSubmit';
import * as ButtonDelete from './Helpers/ButtonDelete';
import * as HandleSubmit from './Helpers/HandleSubmit';
import * as HandleDelete from './Helpers/HandleDelete';
import * as ButtonClose from './Helpers/ButtonClose';
import formBaseTests from './baseTests/formBaseTests';

formBaseTests(<FormSoleProprietor />);

describe('csrf', () => {
    it('should get csrf when mount', () => {
        Csrf.getCsrfOnMountTest(<FormSoleProprietor />);
    });

    it('should not get csrf when nested', () => {
        Csrf.notGetCsrfWhenNestedTest(
            <FormSoleProprietor
                is_nested={true}
            />
        );
    });

    it('should set correct csrf to state', async () => {
        await Csrf.setCsrfToStateTest(<FormSoleProprietor />);
    });

    it('should set form init error on fail request', async () => {
        await Csrf.setFormInitErrorTest(<FormSoleProprietor />);
    });
});

describe('getNds', () => {
    it('should run on mount', () => {
        const getNdsTypes = jest.fn(() => Promise.resolve());

        const component = shallow(<FormSoleProprietor />, {
            disableLifecycleMethods: true
        });

        component.instance().getNdsTypes = getNdsTypes;

        component.instance().componentDidMount();

        expect(getNdsTypes).toHaveBeenCalledTimes(1);
    });

    it('should set nds types to state', () => {
        const data = {
            status: 'SUCCESS',
            data: {
                handbooks: {
                    ndsTypes: 'nds types'
                }
            }
        };

        const customFetch = jest.fn(() => Promise.resolve(data));

        Helpers.customFetch = customFetch;

        const component = shallow(<FormSoleProprietor />, {
            disableLifecycleMethods: true
        });

        component.instance().getCsrf = jest.fn(() => Promise.resolve());

        component.instance().componentDidMount();

        return Promise.resolve()
            .then(() => {
                expect(component.state().nds_types).toBe(data.data.handbooks.ndsTypes);
            })
    });

    it('should set form init error if fail request', () => {
        const data = {
            status: 'ERROR',
        };

        const customFetch = jest.fn(() => Promise.resolve(data));

        Helpers.customFetch = customFetch;

        const component = shallow(<FormSoleProprietor />, {
            disableLifecycleMethods: true
        });

        component.instance().getCsrf = jest.fn(() => Promise.resolve());

        const setFormError = jest.fn();
        component.instance().setFormError = setFormError;

        component.instance().componentDidMount();

        // Цепочки промисов добавляют асинхронность
        return Promise.resolve()
            .then(() => Promise.resolve())
            .then(() => {
                expect(setFormError).lastCalledWith('form_init_fail', true);
            });
    });
});

describe('checkAllControlsAreValid', () => {
    it('form is not valid, if one flag is false', () => {
        CheckAllControlsAreValid.formIsInvalidTest(<FormSoleProprietor />);
    });

    it('form is valid if all flags are true', () => {
        CheckAllControlsAreValid.formIsValidTest(<FormSoleProprietor />);
    });

    it('form is not valid if editMode and data is equal to original', () => {
        CheckAllControlsAreValid.formInvalidOriginalDataTest(
            <FormSoleProprietor
                editMode={true}
            />
        );
    });

    it('form is valid if editMode and data is different from original', () => {
        CheckAllControlsAreValid.formValidOriginalDataTest(
            <FormSoleProprietor
                editMode={true}
            />
        );
    });
});

describe('sendUpFormData', () => {
    it('should not trigger if form is not nested', () => {
        SendUpFormData.notRunIfNotNestedTest(<FormSoleProprietor/>);
    });

    it('when form is not valid should only send up validation', () => {
        SendUpFormData.sendUpOnlyValidationTest(
            <FormSoleProprietor
                is_nested={true}
            />
        );
    });

    it('when form is valid send up data and validation', () => {
        SendUpFormData.sendUpDataAndValidationTest(
            <FormSoleProprietor
                is_nested={true}
            />
        );
    });
});

describe('button apply', () => {
    it('button is disable if form is invalid', () => {
        ButtonSubmit.disabledWhenFormIsInvalidTest(<FormSoleProprietor />);
    });

    it('button preloader', () => {
        ButtonSubmit.preloaderTest(<FormSoleProprietor />);
    });

    it('button is active when form is valid', () => {
        ButtonSubmit.activeWhenFormIsValidTest(<FormSoleProprietor />);
    });

    it('button is disable when form is loading', () => {
        ButtonSubmit.disabledWhenFormIsLoadingTest(<FormSoleProprietor />);
    });

    it('button should not render when form is nested', () => {
        ButtonSubmit.buttonDoesNotRenderTest(
            <FormSoleProprietor
                is_nested={true}
            />
        );
    });

    it('should handle submit on click', () => {
        ButtonSubmit.runHandleSubmitTest(<FormSoleProprietor />);
    });
});

describe('button delete', () => {
    it('button should not render when form is not editMode', () => {
        ButtonDelete.buttonDoesNotRenderTest(<FormSoleProprietor />);
    });

    it('button should render only when form is editMode', () => {
        ButtonDelete.buttonShouldRenderTest(
            <FormSoleProprietor
                editMode={true}
            />
        );
    });

    it('button is active default', () => {
        ButtonDelete.buttonIsActiveTest(
            <FormSoleProprietor
                editMode={true}
            />
        );
    });

    it('button preloader', () => {
        ButtonDelete.preloaderTest(
            <FormSoleProprietor
                editMode={true}
            />
        );
    });

    it('button is disable when form is loading', () => {
        ButtonDelete.disabledWhenFormIsLoadingTest(
            <FormSoleProprietor
                editMode={true}
            />
        );
    });

    it('button should not render when form is nested', () => {
        ButtonDelete.buttonDoesNotRenderTest(
            <FormSoleProprietor
                is_nested={true}
            />
        );
    });

    it('should handleDelete on click', () => {
        ButtonDelete.runHandleDeleteTest(
            <FormSoleProprietor
                editMode={true}
            />
        );
    });
});

describe('handleSubmit', () => {
    it('should not make request when form is nested', () => {
        HandleSubmit.shouldNotMakeRequestEvenIfValidTest(
            <FormSoleProprietor
                is_nested={true}
            />
        );
    });

    it('should not make request when form is not valid', () => {
        HandleSubmit.notMakeRequestWhenFormIsInvalidTest(<FormSoleProprietor/>);
    });

    it('should turn on preloader', () => {
        HandleSubmit.turnOnPreloaderTest(<FormSoleProprietor/>);
    });

    it('should turn off preloader when form submit is success', async () => {
        await HandleSubmit.turnOffPreloaderOnSuccessTest(<FormSoleProprietor/>);
    });

    it('should turn off preloader when form submit is fail', async () => {
        await HandleSubmit.turnOffPreloaderOnErrorTest(<FormSoleProprietor/>);
    });

    it('should run afterSubmit function when form submit is success', async () => {
        await HandleSubmit.runAfterSubmitFunctionOnSuccessTest(<FormSoleProprietor/>)
    });

    it('should not turn off preloader if form has afterSubmit function and submit is success', async () => {
        await HandleSubmit.shouldNotTurnOffPreloaderIfAfterSubmitFunctionTest(
            <FormSoleProprietor
                afterSubmit={() => {}}
            />
        );
    });

    it('should take apart errors when form submit is fail', async () => {
        await HandleSubmit.takeApartErrorsOnErrorTest(<FormSoleProprietor/>);
    });

    it('should create at correct url', async () => {
        await HandleSubmit.correctRequestUrlTest(<FormSoleProprietor/>, URLS.SOLE_PROPRIETOR.SINGLE);
    });

    it('should update at correct url', async () => {
        await HandleSubmit.correctRequestUrlTest(
            <FormSoleProprietor
                editMode={true}
            />,
            `${URLS.SOLE_PROPRIETOR.SINGLE}/undefined`
        );
    });
});

describe('handleDelete', () => {
    it('should turn on preloader', () => {
        HandleDelete.turnOnPreloaderTest(
            <FormSoleProprietor
                data={1}
            />
        );
    });

    it('should turn off preloader when delete is success', async () => {
        await HandleDelete.turnOffPreloaderOnSuccessTest(
            <FormSoleProprietor
                data={1}
            />
        );
    });

    it('should turn off preloader when form delete is fail', async () => {
        await HandleDelete.turnOffPreloaderOnErrorTest(
            <FormSoleProprietor
                data={1}
            />
        );
    });

    it('should run afterDelete function when form delete is success', async () => {
        await HandleDelete.runAfterDeleteFunctionOnSuccessTest(
            <FormSoleProprietor
                data={1}
            />
        );
    });

    it('should not turn off preloader if form has afterDelete function and delete is success', async () => {
        await HandleDelete.shouldNotTurnOffPreloaderIfAfterDeleteFunctionTest(
            <FormSoleProprietor
                data={1}
                afterDelete={() => {}}
            />
        );
    });

    it('should take apart errors when form delete is fail', async () => {
        await HandleDelete.takeApartErrorsOnErrorTest(
            <FormSoleProprietor
                data={1}
            />
        );
    });

    it('should delete at correct url', async () => {
        await HandleDelete.correctRequestUrlTest(
            <FormSoleProprietor
                data={{id: 1}}
            />,
            '/sole-proprietor/1'
        );
    });
});

describe('setControlsErrorFromUpperForm', () => {
    const form_server_errors = {
        last_name: 'last_name error',
        first_name: 'first_name error'
    };

    it('should run function when get props', () => {
        SetControlsErrorFromUpperForm.willReceivePropsTest(<FormSoleProprietor />);
    });

    it('should not setState when get the same props', () => {
        SetControlsErrorFromUpperForm.receiveSamePropsTest(
            <FormSoleProprietor
                form_server_errors={form_server_errors}
            />,
            form_server_errors
        );
    });

    it('should correct set state', () => {
        SetControlsErrorFromUpperForm.setErrorsToStateTest(<FormSoleProprietor />, form_server_errors);
    });
});

describe('buttonClose', () => {
    it('should not render button close without prop closable', () => {
        ButtonClose.buttonDoesNotRenderTest(<FormSoleProprietor />);
    });

    it('should render button close', () => {
        ButtonClose.buttonRenderTest(
            <FormSoleProprietor
                isClosable={true}
            />
        );
    });

    it('should run close form function on click', () => {
        ButtonClose.buttonClickTest(
            <FormSoleProprietor
                isClosable={true}
            />
        );
    });
});

describe('handleChangeLastName', () => {
    it('should auto complete IP name when filling last name', () => {
        const component = shallow(<FormSoleProprietor/>);

        const handleName = jest.fn();
        const handleChange = jest.fn();
        component.instance().handleName = handleName;
        component.instance().handleChange = handleChange;

        component.instance().handleChangeLastName('last_name', 'Петров');

        expect(handleName).toHaveBeenCalledTimes(1);
        expect(handleChange).toHaveBeenCalledTimes(0);
    });

    it('should call often handleChange if editMode', () => {
        const component = shallow(
            <FormSoleProprietor
                editMode={true}
            />
        );

        const handleName = jest.fn();
        const handleChange = jest.fn();
        component.instance().handleName = handleName;
        component.instance().handleChange = handleChange;

        component.instance().handleChangeLastName('last_name', 'Петров');

        expect(handleName).toHaveBeenCalledTimes(0);
        expect(handleChange).toHaveBeenCalledTimes(1);
    });

    it('should call often handleChange if IP name input was focused', () => {
        const component = shallow(<FormSoleProprietor/>);

        const handleName = jest.fn();
        const handleChange = jest.fn();
        component.instance().handleName = handleName;
        component.instance().handleChange = handleChange;

        component.setState({
            was_ip_focused: true
        });

        component.instance().handleChangeLastName('last_name', 'Петров');

        expect(handleName).toHaveBeenCalledTimes(0);
        expect(handleChange).toHaveBeenCalledTimes(1);
    });
});