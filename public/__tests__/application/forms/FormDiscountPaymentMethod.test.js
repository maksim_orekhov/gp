import React from 'react';
import FormDiscountPaymentMethod from '../../../js/application/react/forms/FormDiscountPaymentMethod';
import * as SetControlsErrorFromUpperForm from './Helpers/SetControlsErrorFromUpperForm';
import * as CheckAllControlsAreValid from './Helpers/CheckAllControlsAreValid';
import * as SendUpFormData from './Helpers/SendUpFormData';
import formBaseTests from './baseTests/formBaseTests';
import URLS from "../../../js/application/react/constants/urls";

formBaseTests(<FormDiscountPaymentMethod />);

describe('checkAllControlsAreValid', () => {
    it('form is not valid, if one flag is false', () => {
        CheckAllControlsAreValid.formIsInvalidTest(<FormDiscountPaymentMethod />);
    });

    it('form is valid if all flags are true', () => {
        CheckAllControlsAreValid.formIsValidTest(<FormDiscountPaymentMethod />);
    });

    it('form is valid if data is different from original', () => {
        CheckAllControlsAreValid.formValidOriginalDataTest(<FormDiscountPaymentMethod />);
    });
});

