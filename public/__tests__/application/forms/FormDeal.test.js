import React from 'react';
import FormDeal from '../../../js/application/react/forms/formsDeal/FormDeal';
import * as SetControlsErrorFromUpperForm from './Helpers/SetControlsErrorFromUpperForm';
import * as FormInit from './Helpers/FormInit';
import * as SuccessAjaxFormInit from './Helpers/SuccessAjaxFormInit';
import * as setCustomDataToState from './Helpers/setCustomDataToState';
import * as getDataFromState from './Helpers/getDataFromState';
import * as Csrf from './Helpers/Csrf';
import * as CheckAllControlsAreValid from './Helpers/CheckAllControlsAreValid';
import * as SendUpFormData from './Helpers/SendUpFormData';
import * as ButtonSubmit from './Helpers/ButtonSubmit';
import * as ButtonDelete from './Helpers/ButtonDelete';
import * as HandleSubmit from './Helpers/HandleSubmit';
import * as HandleDelete from './Helpers/HandleDelete';
import * as ButtonClose from './Helpers/ButtonClose';
import formBaseTests from './baseTests/formBaseTests';

formBaseTests(<FormDeal/>);

describe('form-init', () => {
    it('should run form init when will mount', () => {
        FormInit.runFormInit(<FormDeal/>);
    });

    it('should turn on preloader on run form-init', () => {
        FormDeal.prototype.componentWillMount = jest.fn();
        FormInit.shouldTurnOnPreloader(<FormDeal/>);
    });

    it('should get form init url', () => {
        FormInit.getFormInitUrl(<FormDeal/>);
    });

    it('should run successAjaxFormInit on success', async () => {
       await FormInit.shouldRunSuccessAjaxFormInitOnSuccess(<FormDeal/>);
    });

    it('should turn off preloader on fail', async () => {
        await FormInit.shouldTurnOffPreloaderOnFail(<FormDeal/>)
    });

    it('should set form init error on fail form-init', async () => {
        await FormInit.setFormInitErrorTest(<FormDeal/>);
    });
});

describe('successAjaxFormInit', () => {
    it('should increase update_collection_civil_law_subjects', () => {
        SuccessAjaxFormInit.increaseCivilLawSubjects(<FormDeal/>);
    });

    it('should run setFormDefaultValues', () => {
        SuccessAjaxFormInit.runSetFormDefaultValues(<FormDeal/>);
    });

    it('should turn off preloader', () => {
        SuccessAjaxFormInit.turnOffPreloader(<FormDeal/>);
    });

    it('should get form data from LocalStorage', () => {
        SuccessAjaxFormInit.getFormFromLocalStorage(<FormDeal/>);
    });

    it('should set correct csrf to state', () => {
        SuccessAjaxFormInit.setCsrfToStateTest(<FormDeal />);
    });
});

describe('setCustomDataToState', () => {
    it('should set correct custom data to state', () => {
        setCustomDataToState.setCustomData(
            <FormDeal
                form_type='service_contractor'
            />
        );
    })
});

describe('checkAllControlsAreValid', () => {
    it('form is not valid, if one flag is false', () => {
        CheckAllControlsAreValid.formIsInvalidTest(<FormDeal />);
    });

    it('form is valid if all flags are true', () => {
        CheckAllControlsAreValid.formIsValidTest(<FormDeal />);
    });
});

describe('getDataFromState', () => {
    it('should run createFormData', () => {
        getDataFromState.runCreateFormData(<FormDeal />)
    });
});

describe('button apply', () => {
    it('button is disable if form is invalid', () => {
        ButtonSubmit.disabledWhenFormIsInvalidTest(<FormDeal />);
    });

    it('button preloader', () => {
        ButtonSubmit.preloaderTest(<FormDeal />);
    });

    it('button is active when form is valid', () => {
        ButtonSubmit.activeWhenFormIsValidTest(<FormDeal />);
    });

    it('button is disable when form is loading', () => {
        ButtonSubmit.disabledWhenFormIsLoadingTest(<FormDeal />);
    });

    it('should handle submit on click', () => {
        ButtonSubmit.runHandleSubmitTest(<FormDeal />);
    });
});

describe('handleSubmit', () => {
    it('should not make request when form is not valid', () => {
        HandleSubmit.notMakeRequestWhenFormIsInvalidTest(<FormDeal/>);
    });

    it('should turn on preloader', () => {
        HandleSubmit.turnOnPreloaderTest(<FormDeal/>);
    });

    it('should turn off preloader when form submit is success', async () => {
        await HandleSubmit.turnOffPreloaderOnSuccessTest(<FormDeal/>);
    });

    it('should turn off preloader when form submit is fail', async () => {
        await HandleSubmit.turnOffPreloaderOnErrorTest(<FormDeal/>);
    });

    it('should take apart errors when form submit is fail', async () => {
        await HandleSubmit.takeApartErrorsOnErrorTest(<FormDeal/>);
    });

    it('should run successAjaxSubmit method after success submit', () => {
        HandleSubmit.runSuccessAjaxSubmit(<FormDeal/>);
    });

    // it('should run clear form local storage', () => {
    //
    // });
});

/*describe('csrf', () => {
    it('should get csrf when mount', () => {
        Csrf.getCsrfOnMountTest(<FormLegalEntity />);
    });

    it('should not get csrf when nested', () => {
        Csrf.notGetCsrfWhenNestedTest(
            <FormLegalEntity
                is_nested={true}
            />
        );
    });

    it('should set correct csrf to state', async () => {
        await Csrf.setCsrfToStateTest(<FormLegalEntity />);
    });

    it('should set form init error on fail request', async () => {
        await Csrf.setFormInitErrorTest(<FormLegalEntity />);
    });
});

describe('getNds', () => {
    it('should run on mount', () => {
        const getNdsTypes = jest.fn(() => Promise.resolve());

        const component = shallow(<FormLegalEntity />, {
            disableLifecycleMethods: true
        });

        component.instance().getNdsTypes = getNdsTypes;

        component.instance().componentDidMount();

        expect(getNdsTypes).toHaveBeenCalledTimes(1);
    });

    it('should set nds types to state', () => {
        const data = {
            status: 'SUCCESS',
            data: {
                handbooks: {
                    ndsTypes: 'nds types'
                }
            }
        };

        const customFetch = jest.fn(() => Promise.resolve(data));

        Helpers.customFetch = customFetch;

        const component = shallow(<FormLegalEntity />, {
            disableLifecycleMethods: true
        });

        component.instance().getCsrf = jest.fn(() => Promise.resolve());

        component.instance().componentDidMount();

        return Promise.resolve()
            .then(() => {
                expect(component.state().nds_types).toBe(data.data.handbooks.ndsTypes);
            })
    });

    it('should set form init error if fail request', () => {
        const data = {
            status: 'ERROR',
        };

        const customFetch = jest.fn(() => Promise.resolve(data));

        Helpers.customFetch = customFetch;

        const component = shallow(<FormLegalEntity />, {
            disableLifecycleMethods: true
        });

        component.instance().getCsrf = jest.fn(() => Promise.resolve());

        const setFormError = jest.fn();
        component.instance().setFormError = setFormError;

        component.instance().componentDidMount();

        // Цепочки промисов добавляют асинхронность
        return Promise.resolve()
            .then(() => Promise.resolve())
            .then(() => {
                expect(setFormError).lastCalledWith('form_init_fail', true);
            });
    });
});

describe('checkAllControlsAreValid', () => {
    it('form is not valid, if one flag is false', () => {
        CheckAllControlsAreValid.formIsInvalidTest(<FormLegalEntity />);
    });

    it('form is valid if all flags are true', () => {
        CheckAllControlsAreValid.formIsValidTest(<FormLegalEntity />);
    });

    it('form is not valid if editMode and data is equal to original', () => {
        CheckAllControlsAreValid.formInvalidOriginalDataTest(
            <FormLegalEntity
                editMode={true}
            />
        );
    });

    it('form is valid if editMode and data is different from original', () => {
        CheckAllControlsAreValid.formValidOriginalDataTest(
            <FormLegalEntity
                editMode={true}
            />
        );
    });
});

describe('sendUpFormData', () => {
    it('should not trigger if form is not nested', () => {
        SendUpFormData.notRunIfNotNestedTest(<FormLegalEntity/>);
    });

    it('when form is not valid should only send up validation', () => {
        SendUpFormData.sendUpOnlyValidationTest(
            <FormLegalEntity
                is_nested={true}
            />
        );
    });

    it('when form is valid send up data and validation', () => {
        SendUpFormData.sendUpDataAndValidationTest(
            <FormLegalEntity
                is_nested={true}
            />
        );
    });
});

describe('button apply', () => {
    it('button is disable if form is invalid', () => {
        ButtonSubmit.disabledWhenFormIsInvalidTest(<FormLegalEntity />);
    });

    it('button preloader', () => {
        ButtonSubmit.preloaderTest(<FormLegalEntity />);
    });

    it('button is active when form is valid', () => {
        ButtonSubmit.activeWhenFormIsValidTest(<FormLegalEntity />);
    });

    it('button is disable when form is loading', () => {
        ButtonSubmit.disabledWhenFormIsLoadingTest(<FormLegalEntity />);
    });

    it('button should not render when form is nested', () => {
        ButtonSubmit.buttonDoesNotRenderTest(
            <FormLegalEntity
                is_nested={true}
            />
        );
    });

    it('should handle submit on click', () => {
        ButtonSubmit.runHandleSubmitTest(<FormLegalEntity />);
    });
});

describe('button delete', () => {
    it('button should not render when form is not editMode', () => {
        ButtonDelete.buttonDoesNotRenderTest(<FormLegalEntity />);
    });

    it('button should render only when form is editMode', () => {
        ButtonDelete.buttonShouldRenderTest(
            <FormLegalEntity
                editMode={true}
            />
        );
    });

    it('button is active default', () => {
        ButtonDelete.buttonIsActiveTest(
            <FormLegalEntity
                editMode={true}
            />
        );
    });

    it('button preloader', () => {
        ButtonDelete.preloaderTest(
            <FormLegalEntity
                editMode={true}
            />
        );
    });

    it('button is disable when form is loading', () => {
        ButtonDelete.disabledWhenFormIsLoadingTest(
            <FormLegalEntity
                editMode={true}
            />
        );
    });

    it('button should not render when form is nested', () => {
        ButtonDelete.buttonDoesNotRenderTest(
            <FormLegalEntity
                is_nested={true}
            />
        );
    });

    it('should handleDelete on click', () => {
        ButtonDelete.runHandleDeleteTest(
            <FormLegalEntity
                editMode={true}
            />
        );
    });
});

describe('handleSubmit', () => {
    it('should not make request when form is nested', () => {
        HandleSubmit.shouldNotMakeRequestEvenIfValidTest(
            <FormLegalEntity
                is_nested={true}
            />
        );
    });

    it('should not make request when form is not valid', () => {
        HandleSubmit.notMakeRequestWhenFormIsInvalidTest(<FormLegalEntity/>);
    });

    it('should turn on preloader', () => {
        HandleSubmit.turnOnPreloaderTest(<FormLegalEntity/>);
    });

    it('should turn off preloader when form submit is success', async () => {
        await HandleSubmit.turnOffPreloaderOnSuccessTest(<FormLegalEntity/>);
    });

    it('should turn off preloader when form submit is fail', async () => {
        await HandleSubmit.turnOffPreloaderOnErrorTest(<FormLegalEntity/>);
    });

    it('should run afterSubmit function when form submit is success', async () => {
        await HandleSubmit.runAfterSubmitFunctionOnSuccessTest(<FormLegalEntity/>)
    });

    it('should not turn off preloader if form has afterSubmit function and submit is success', async () => {
        await HandleSubmit.shouldNotTurnOffPreloaderIfAfterSubmitFunctionTest(
            <FormLegalEntity
                afterSubmit={() => {}}
            />
        );
    });

    it('should take apart errors when form submit is fail', async () => {
        await HandleSubmit.takeApartErrorsOnErrorTest(<FormLegalEntity/>);
    });

    it('should create at correct url', async () => {
        await HandleSubmit.correctRequestUrlTest(<FormLegalEntity/>, URLS.LEGAL_ENTITY.SINGLE);
    });

    it('should update at correct url', async () => {
        await HandleSubmit.correctRequestUrlTest(
            <FormLegalEntity
                editMode={true}
            />,
            `${URLS.LEGAL_ENTITY.SINGLE}/undefined`
        );
    });
});

describe('handleDelete', () => {
    it('should turn on preloader', () => {
        HandleDelete.turnOnPreloaderTest(
            <FormLegalEntity
                data={1}
            />
        );
    });

    it('should turn off preloader when delete is success', async () => {
        await HandleDelete.turnOffPreloaderOnSuccessTest(
            <FormLegalEntity
                data={1}
            />
        );
    });

    it('should turn off preloader when form delete is fail', async () => {
        await HandleDelete.turnOffPreloaderOnErrorTest(
            <FormLegalEntity
                data={1}
            />
        );
    });

    it('should run afterDelete function when form delete is success', async () => {
        await HandleDelete.runAfterDeleteFunctionOnSuccessTest(
            <FormLegalEntity
                data={1}
            />
        );
    });

    it('should not turn off preloader if form has afterDelete function and delete is success', async () => {
        await HandleDelete.shouldNotTurnOffPreloaderIfAfterDeleteFunctionTest(
            <FormLegalEntity
                data={1}
                afterDelete={() => {}}
            />
        );
    });

    it('should take apart errors when form delete is fail', async () => {
        await HandleDelete.takeApartErrorsOnErrorTest(
            <FormLegalEntity
                data={1}
            />
        );
    });

    it('should delete at correct url', async () => {
        await HandleDelete.correctRequestUrlTest(
            <FormLegalEntity
                data={{id: 1}}
            />,
            '/legal-entity/1'
        );
    });
});

describe('setControlsErrorFromUpperForm', () => {
    const form_server_errors = {
        name: 'name error',
        legal_address: 'legal_address error'
    };

    it('should run function when get props', () => {
        SetControlsErrorFromUpperForm.willReceivePropsTest(<FormLegalEntity />);
    });

    it('should not setState when get the same props', () => {
        SetControlsErrorFromUpperForm.receiveSamePropsTest(
            <FormLegalEntity
                form_server_errors={form_server_errors}
            />,
            form_server_errors
        );
    });

    it('should correct set state', () => {
        SetControlsErrorFromUpperForm.setErrorsToStateTest(<FormLegalEntity />, form_server_errors);
    });
});

describe('buttonClose', () => {
    it('should not render button close without prop closable', () => {
        ButtonClose.buttonDoesNotRenderTest(<FormLegalEntity />);
    });

    it('should render button close', () => {
        ButtonClose.buttonRenderTest(
            <FormLegalEntity
                isClosable={true}
            />
        );
    });

    it('should run close form function on click', () => {
        ButtonClose.buttonClickTest(
            <FormLegalEntity
                isClosable={true}
            />
        );
    });
});*/