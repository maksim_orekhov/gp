import * as SetUndefinedFormError from '../Helpers/SetUndefinedFormError';

const setUndefinedFormErrorTests = (component) => {
    describe('setUndefinedFormError', () => {
        it('should set form error', () => {
            SetUndefinedFormError.setFormErrorTest(component);
        });

        it('should set error message to state', () => {
            SetUndefinedFormError.setMessageToStateTest(component);
        });
    });
};

export default setUndefinedFormErrorTests;