import * as SetFormError from '../Helpers/SetFormError';

const setFormErrorTests = (component) => {
    describe('setFormError', () => {
        it('should set error flag to state', () => {
            SetFormError.setFormErrorTest(component);
        });

        it('should reset past error flags', () => {
            SetFormError.resetPastErrorTest(component);
        });
    });
};

export default setFormErrorTests;