import tryToRunValidationFunctionsTests from './tryToRunValidationFunctionsTests';
import takeApartErrorFromServerTests from './takeApartErrorFromServerTests';
import setUndefinedFormErrorTests from './setUndefinedFormErrorTests';
import takeApartInvalidFormDataTests from './takeApartInvalidFormDataTests';
import setFormErrorTests from './setFormErrorTests';
import handleComponentValidTests from './handleComponentValidTests';
import handleChangeTests from './handleChangeTests';
import * as Common from '../Helpers/Common';

const formBaseTests = (component) => {

    describe('render', () => {
        it('should be correct snapshot', () => {
            Common.snapShotTest(component);
        });
    });

    tryToRunValidationFunctionsTests(component);
    takeApartErrorFromServerTests(component);
    setUndefinedFormErrorTests(component);
    takeApartInvalidFormDataTests(component);
    setFormErrorTests(component);
    handleComponentValidTests(component);
    handleChangeTests(component);
};

export default formBaseTests;