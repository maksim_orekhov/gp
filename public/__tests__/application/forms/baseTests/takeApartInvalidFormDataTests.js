import * as TakeApartInvalidFormData from '../Helpers/TakeApartInvalidFormData';

const takeApartInvalidFormDataTests = (component) => {
    it('should set form error', () => {
        TakeApartInvalidFormData.setFormErrorTest(component);
    });

    it('should set csrf error if csrf incorrect', () => {
        TakeApartInvalidFormData.setCsrfErrorTest(component);
    });

    it('should set correct errors to state', () => {
        const errors = {
            test_field_one: 'test_field_one error',
            test_field_two: 'test_field_two error'
        };

        TakeApartInvalidFormData.setErrorsToStateTest(component, errors);
    });
};

export default takeApartInvalidFormDataTests;