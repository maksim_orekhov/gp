import * as HandleComponentValid from '../Helpers/HandleComponentValid';

const handleComponentValidTests = (component) => {
    describe('handleComponentValid', () => {
        it('should add validate function in deck', () => {
            HandleComponentValid.addingToDeckTest(component);
        });

        it('should add only one function in deck', () => {
            HandleComponentValid.addedOnlyOneFunctionTest(component);
        });
    });
};

export default handleComponentValidTests;