import * as TryToRunValidationFunctions from '../Helpers/TryToRunValidationFunctions';

const tryToRunValidationFunctionsTests = (component) => {
    describe('tryToRunValidationFunctions', () => {
        it('should run function', () => {
            TryToRunValidationFunctions.functionRunTest(component);
        });

        it('should run only first function', () => {
            TryToRunValidationFunctions.runOnlyFirstFunctionTest(component);
        });

        it('should not run if another function in deck', () => {
            TryToRunValidationFunctions.shouldNotRunIfAnotherFunctionInDeckTest(component);
        });

        it('should shift function after run', () => {
            TryToRunValidationFunctions.shiftFunctionAfterRunTest(component);
        });

        it('should not shift function after run if another function in deck', () => {
            TryToRunValidationFunctions.notShiftFunctionIfAnotherInDeckTest(component);
        });
    });
};

export default tryToRunValidationFunctionsTests;