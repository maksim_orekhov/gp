import * as TakeApartErrorFromServer from '../Helpers/TakeApartErrorFromServer'

const takeApartErrorFromServerTests = (component) => {
    describe('takeApartErrorFromServer', () => {
        it('code invalid form data', () => {
            TakeApartErrorFromServer.invalidFormDataTest(component);
        });

        it('another codes', () => {
            TakeApartErrorFromServer.anotherCodesTest(component);
        });

        it('undefined form error', () => {
            TakeApartErrorFromServer.undefinedFormErrorTest(component);
        });
    });
};

export default takeApartErrorFromServerTests;