export const increaseCivilLawSubjects = (testing_component) => {
    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    const data = {
        data: {
            deal_types: {
                1: {id: 1, ident: "T", name: "Товар"}
            },
            fee_payer_options: [
                {
                    id: 1,
                    name:
                        "CUSTOMER"
                }
            ]
        }
    };
    component.instance().successAjaxFormInit(data);

    expect(component.state().update_collection_civil_law_subjects).toBe(1);
};

export const runSetFormDefaultValues = (testing_component) => {
    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    const setFormDefaultValues = jest.fn();
    component.instance().setFormDefaultValues = setFormDefaultValues;

    component.instance().successAjaxFormInit({data: {}});

    expect(setFormDefaultValues).toHaveBeenCalledTimes(1);
};

export const turnOffPreloader = (testing_component) => {
    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    component.instance().setFormDefaultValues = jest.fn();

    const toggleIsLoading = jest.fn();
    component.instance().toggleIsLoading = toggleIsLoading;

    component.instance().successAjaxFormInit({data: {}});

    expect(toggleIsLoading).toHaveBeenCalledTimes(1);
};

export const getFormFromLocalStorage = (testing_component) => {
    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    component.instance().setFormDefaultValues = jest.fn();

    const getFormFromLocalStorage = jest.fn();
    component.instance().getFormFromLocalStorage = getFormFromLocalStorage;

    component.instance().successAjaxFormInit({data: {}});

    expect(getFormFromLocalStorage).toHaveBeenCalledTimes(1);
};

export const setCsrfToStateTest = (testing_component) => {
    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });
    const data = {
        data: {
            deal_types: {
                1: {id: 1, ident: "T", name: "Товар"}
            },
            fee_payer_options: [
                {
                    id: 1,
                    name:
                        "CUSTOMER"
                }
            ],
            csrf: '1234'
        }
    };
    component.instance().successAjaxFormInit(data);
    expect(component.state().form.csrf).toBe(data.data.csrf);
};
