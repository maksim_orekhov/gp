export const formIsInvalidTest = (testing_component) => {
    const component = shallow(testing_component);

    const validation = {...component.state().validation};

    const first_flag = Object.keys(validation)[0];

    component.setState({
        validation: {
            ...validation,
            [first_flag]: false
        }
    });

    component.instance().checkAllControlsAreValid();
    expect(component.state().form_isValid).toBe(false);
};

export const formIsValidTest = (testing_component) => {
    const component = shallow(testing_component);

    const validation = {...component.state().validation};

    Object.keys(validation).forEach(flag => {
       validation[flag] = true
    });

    component.setState({
        validation
    });

    component.instance().checkAllControlsAreValid();
    expect(component.state().form_isValid).toBe(true);
};

export const formInvalidOriginalDataTest = (testing_component) => {
    const component = shallow(testing_component);

    // Делаем форму валидной
    const validation = {...component.state().validation};

    Object.keys(validation).forEach(flag => {
        validation[flag] = true
    });

    component.setState({
        validation
    });

    // Запоминаем состояние
    component.instance().prev_state = JSON.stringify(component.state().form);

    component.instance().checkAllControlsAreValid();
    expect(component.state().form_isValid).toBe(false);
};

export const formValidOriginalDataTest = (testing_component) => {
    const component = shallow(testing_component);

    // Делаем форму валидной
    const validation = {...component.state().validation};

    Object.keys(validation).forEach(flag => {
        validation[flag] = true
    });

    component.setState({
        validation
    });

    // Меняем данные чтобы они отличались от изначальных
    const first_field = Object.keys(component.state().form)[0];

    component.setState({
        form: {
            ...component.state().form,
            [first_field]: 'Петров'
        }
    });

    component.instance().checkAllControlsAreValid();
    expect(component.state().form_isValid).toBe(true);
};