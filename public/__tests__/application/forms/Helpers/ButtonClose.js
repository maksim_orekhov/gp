const buttonClose = '.ButtonClose';

export const buttonDoesNotRenderTest = (testing_component, button = buttonClose) => {
    const component = shallow(testing_component);

    expect(component.find(button).length).toBe(0);
};

export const buttonRenderTest = (testing_component, button = buttonClose) => {
    const component = shallow(testing_component);

    expect(component.find(button).length).toBe(1);
};

export const buttonClickTest = (testing_component, button = buttonClose) => {
    const component = shallow(testing_component);

    const handleCloseForm = jest.fn();

    component.instance().handleCloseForm = handleCloseForm;

    component.setState({}); // Без этой строчки тест почему-то не работает

    component.find(button).simulate('click');

    expect(handleCloseForm).toHaveBeenCalledTimes(1);
};