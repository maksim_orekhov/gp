export const setStateTest = (testing_component, name, value) => {
    const component = shallow(
        testing_component
    );

    component.instance().saveFormToLocalStorage = jest.fn;

    component.instance().handleChange(name, value);
    expect(component.state().form[name]).toBe(value);
};