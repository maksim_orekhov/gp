export const setCustomData = (testing_component) => {
    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });
    const deal_types = {
        1: {
            id: '1',
            ident: 'U'
        },
        2: {
            id: '2',
            ident: 'T'
        }
    };
    const data_to_state = {
        form: {}
    };
    const form = {'deal_role': 'contractor', 'deal_type': '1'};
    const result = component.instance().setCustomDataToState(deal_types, data_to_state);
    expect(result).toEqual(expect.objectContaining({form: form}));
};