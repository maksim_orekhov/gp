export const setFormErrorTest = (testing_component) => {
    const component = shallow(testing_component);

    component.instance().setFormError('access_denied', true);

    expect(component.instance().state.server_errors.access_denied).toBe(true);
};

export const resetPastErrorTest = (testing_component) => {
    const component = shallow(testing_component);

    component.instance().setFormError('csrf', true);

    component.instance().setFormError('access_denied', true);

    expect(component.instance().state.server_errors.csrf).toBe(false);
};