export const runFormInit = (testing_component) => {
    const formInit = jest.fn(() => Promise.resolve());

    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    component.instance().formInit = formInit;

    component.instance().componentWillMount();

    expect(formInit).toHaveBeenCalledTimes(1);
};

export const setFormInitErrorTest = (testing_component) => {
    const data = {
        status: 'ERROR',
    };

    Helpers.customFetch = jest.fn(() => Promise.resolve(data));

    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    const setFormError = jest.fn();
    component.instance().setFormError = setFormError;

    component.instance().formInit();

    // Цепочки промисов добавляют асинхронность
    return Promise.resolve()
        .then(() => Promise.resolve())
        .then(() => {
            expect(setFormError).lastCalledWith('form_init_fail', true);
        });
};

export const shouldTurnOnPreloader = (testing_component) => {
    const customFetch = jest.fn(() => Promise.resolve());

    Helpers.customFetch = customFetch;

    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    component.instance().formInit();

    expect(component.state().form_isPreloader).toBe(true);
};

export const shouldRunSuccessAjaxFormInitOnSuccess = (testing_component) => {
    const customFetch = jest.fn(() => Promise.resolve({
        status: 'SUCCESS'
    }));

    Helpers.customFetch = customFetch;

    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    const successAjaxFormInit = jest.fn();

    component.instance().successAjaxFormInit = successAjaxFormInit;
    component.instance().formInit();

    return Promise.resolve()
        .then(() => {
            expect(successAjaxFormInit).toHaveBeenCalledTimes(1);
        });
};

export const shouldTurnOffPreloaderOnFail = (testing_component) => {
    const customFetch = jest.fn(() => Promise.resolve({
        status: 'ERROR'
    }));

    Helpers.customFetch = customFetch;

    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    component.instance().formInit();

    return Promise.resolve()
        .then(() => Promise.resolve())
        .then(() => {
            expect(component.state().form_isPreloader).toBe(false);
        });
};

export const getFormInitUrl = (testing_component) => {
    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    const getFormInitUrl = jest.fn();
    component.instance().getFormInitUrl = getFormInitUrl;

    component.instance().formInit();

    expect(getFormInitUrl).toHaveBeenCalledTimes(1);
};