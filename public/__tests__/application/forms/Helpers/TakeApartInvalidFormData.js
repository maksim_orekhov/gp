export const setFormErrorTest = (testing_component) => {
    const component = shallow(testing_component);

    const setFormError = jest.fn();
    component.instance().setFormError = setFormError;

    component.instance().TakeApartInvalidFormData({});

    expect(setFormError).toHaveBeenCalledTimes(1);
};

export const setCsrfErrorTest = (testing_component) => {
    const component = shallow(testing_component);

    const errors = {
        csrf: 'undefined error'
    };

    const setFormError = jest.fn();
    component.instance().setFormError = setFormError;

    component.instance().TakeApartInvalidFormData(errors);

    expect(setFormError).toHaveBeenCalledTimes(2); // 1 раз invalid form data. 2 раз csrf
};

export const setErrorsToStateTest = (testing_component, errors) => {
    const component = shallow(testing_component);

    component.instance().TakeApartInvalidFormData(errors);

    Object.keys(errors).forEach(error => {
        expect(component.state().controls_server_errors[error]).toBe(errors[error]);
    });
};