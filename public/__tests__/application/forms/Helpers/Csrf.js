export const getCsrfOnMountTest = (testing_component) => {
    const getCsrf = jest.fn(() => Promise.resolve());

    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    component.instance().getCsrf = getCsrf;

    component.instance().componentDidMount();

    expect(getCsrf).toHaveBeenCalledTimes(1);
};

export const notGetCsrfWhenNestedTest = (testing_component) => {
    const getCsrf = jest.fn(() => Promise.resolve());

    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    component.instance().getCsrf = getCsrf;
    component.instance().componentDidMount();

    expect(getCsrf).toHaveBeenCalledTimes(0);
};

export const setCsrfToStateTest = (testing_component) => {
    const data = {
        status: 'SUCCESS',
        data: {
            csrf: '123qwerty'
        }
    };

    Helpers.customFetch = jest.fn(() => Promise.resolve(data));

    const component = shallow(testing_component);

    return Promise.resolve()
        .then(() => {
            expect(component.state().form.csrf).toBe(data.data.csrf)
        });
};

export const setFormInitErrorTest = (testing_component) => {
    const data = {
        status: 'ERROR',
    };

    Helpers.customFetch = jest.fn(() => Promise.resolve(data));

    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    const setFormError = jest.fn();
    component.instance().setFormError = setFormError;

    component.instance().componentDidMount();

    // Цепочки промисов добавляют асинхронность
    return Promise.resolve()
        .then(() => Promise.resolve())
        .then(() => {
            expect(setFormError).lastCalledWith('form_init_fail', true);
        });
};