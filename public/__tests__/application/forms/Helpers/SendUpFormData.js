export const notRunIfNotNestedTest = (testing_component) => {
    const handleComponentValidation = jest.fn();
    const sendUpData = jest.fn();

    const component = shallow(testing_component);

    component.setProps({handleComponentValidation, sendUpData});

    component.instance().sendUpFormData();

    expect(handleComponentValidation).toHaveBeenCalledTimes(0);
    expect(sendUpData).toHaveBeenCalledTimes(0);
};

export const sendUpOnlyValidationTest = (testing_component) => {
    const handleComponentValidation = jest.fn();
    const sendUpData = jest.fn();

    const component = shallow(testing_component);

    component.setProps({handleComponentValidation, sendUpData});

    // Делаем форму невалидной
    component.setState({
        form_isValid: false
    });

    component.instance().sendUpFormData();

    expect(handleComponentValidation).toHaveBeenCalledTimes(1);
    expect(sendUpData).toHaveBeenCalledTimes(0);
};

export const sendUpDataAndValidationTest = (testing_component) => {
    const handleComponentValidation = jest.fn();
    const sendUpData = jest.fn();

    const component = shallow(testing_component);

    component.setProps({handleComponentValidation, sendUpData});

    // Делаем форму валидной
    component.setState({
        form_isValid: true
    });

    component.instance().sendUpFormData();

    expect(handleComponentValidation).toHaveBeenCalledTimes(1);
    expect(sendUpData).toHaveBeenCalledTimes(1);
};