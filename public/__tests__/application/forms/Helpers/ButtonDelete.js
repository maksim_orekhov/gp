const buttonDelete = 'button.ButtonDelete';

export const preloaderTest = (testing_component, button = buttonDelete) => {
    const component = shallow(testing_component);

    component.setState({
        form_isDeleting: true
    });

    component.update();
    expect(component.find(`${button}.Preloader`).length).toBe(1);
};

export const disabledWhenFormIsLoadingTest = (testing_component, button = buttonDelete) => {
    const component = shallow(testing_component);

    component.setState({
        form_isLoading: true
    });

    component.update();
    expect(component.find(button).prop('disabled')).toBe(true);
};

export const buttonDoesNotRenderTest = (testing_component, button = buttonDelete) => {
    const component = shallow(testing_component);

    expect(component.find(button).length).toBe(0);
};

export const runHandleDeleteTest = (testing_component, button = buttonDelete) => {
    const component = shallow(testing_component);

    const handleDelete = jest.fn();
    component.instance().handleDelete = handleDelete;

    component.setState({}); // Без этой строчки тест почему-то не работает

    component.find(button).simulate('click');

    expect(handleDelete).toHaveBeenCalledTimes(1);
};

export const buttonShouldRenderTest = (testing_component, button = buttonDelete) => {
    const component = shallow(testing_component);

    expect(component.find(button).length).toBe(1);
};

export const buttonIsActiveTest = (testing_component, button = buttonDelete) => {
    const component = shallow(testing_component);

    // Кнопка удаления всегда активна
    expect(component.find(button).prop('disabled')).toBe(false);
};