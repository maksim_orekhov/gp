export const addingToDeckTest = (testing_component) => {
    const component = shallow(testing_component);

    component.instance().handleComponentValid('test_field', true);
    // Проверяем что функция добавилась в массив функций валидации
    expect(typeof component.instance().updateValidationFunctions[0]).toBe('function');
};

export const addedOnlyOneFunctionTest = (testing_component) => {
    const component = shallow(testing_component);

    component.instance().handleComponentValid('test_field', true);
    // Проверяем что функция добавилась в массив функций валидации
    expect(component.instance().updateValidationFunctions.length).toBe(1);
};