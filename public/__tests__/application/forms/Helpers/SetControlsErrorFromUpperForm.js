export const willReceivePropsTest = (testing_component) => {
    const component = shallow(testing_component);

    const SetControlsErrorFromUpperForm = jest.fn();
    component.instance().SetControlsErrorFromUpperForm = SetControlsErrorFromUpperForm;

    component.setProps({
        form_server_errors: 'error'
    });

    expect(SetControlsErrorFromUpperForm).toHaveBeenCalledTimes(1);
};

export const receiveSamePropsTest = (testing_component, form_server_errors) => {
    const component = shallow(testing_component);

    const setState = jest.fn();
    component.instance().setState = setState;

    component.setProps({
        form_server_errors
    });

    expect(setState).toHaveBeenCalledTimes(0);
};

export const setErrorsToStateTest = (testing_component, form_server_errors) => {
    const component = shallow(
        testing_component
    );

    component.setProps({
        form_server_errors
    });

    Object.keys(form_server_errors).forEach(error => {
        expect(component.instance().state.controls_server_errors[error]).toBe(form_server_errors[error]);
    });
};