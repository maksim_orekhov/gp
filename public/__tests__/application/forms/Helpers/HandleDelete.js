export const turnOnPreloaderTest = (testing_component) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'SUCCESS'
    }));

    Helpers.customFetch = customFetch;

    component.instance().handleDelete();

    expect(component.state().form_isDeleting).toBe(true);
};

export const turnOffPreloaderOnSuccessTest = (testing_component) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'SUCCESS'
    }));

    Helpers.customFetch = customFetch;

    component.instance().handleDelete();

    return Promise.resolve()
        .then(() => {
            expect(component.state().form_isDeleting).toBe(false);
        });
};

export const turnOffPreloaderOnErrorTest = (testing_component) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'ERROR'
    }));

    Helpers.customFetch = customFetch;

    component.instance().handleDelete();

    return Promise.resolve()
        .then(() => {
            expect(component.state().form_isDeleting).toBe(false);
        });
};

export const runAfterDeleteFunctionOnSuccessTest = (testing_component) => {
    const afterDelete = jest.fn();
    const component = shallow(testing_component);

    component.setProps({
        afterDelete
    });

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'SUCCESS'
    }));

    Helpers.customFetch = customFetch;

    component.instance().handleDelete();

    return Promise.resolve()
        .then(() => {
            expect(afterDelete).toHaveBeenCalledTimes(1);
        });
};

export const shouldNotTurnOffPreloaderIfAfterDeleteFunctionTest = (testing_component) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'SUCCESS'
    }));

    Helpers.customFetch = customFetch;

    component.instance().handleDelete();

    return Promise.resolve()
        .then(() => {
            expect(component.state().form_isDeleting).toBe(true);
        });
};

export const takeApartErrorsOnErrorTest = (testing_component) => {
    const component = shallow(testing_component);

    const TakeApartErrorFromServer = jest.fn();
    component.instance().TakeApartErrorFromServer = TakeApartErrorFromServer;

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'ERROR'
    }));

    Helpers.customFetch = customFetch;

    component.instance().handleDelete();

    return Promise.resolve()
        .then(() => {
            expect(TakeApartErrorFromServer).toHaveBeenCalledTimes(1);
        });
};

export const correctRequestUrlTest = (testing_component, url) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve());

    Helpers.customFetch = customFetch;

    component.instance().handleDelete();

    return Promise.resolve()
        .then(() => {
            expect(customFetch).lastCalledWith(url, expect.anything());
        });
};