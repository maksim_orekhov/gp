const buttonApply = 'button.ButtonApply';

export const disabledWhenFormIsInvalidTest = (testing_component, button = buttonApply) => {
    const component = shallow(testing_component);

    component.setState({
        form_isValid: false
    });

    expect(component.find(button).prop('disabled')).toBe(true);
};

export const preloaderTest = (testing_component, button = buttonApply) => {
    const component = shallow(testing_component);

    component.setState({
        form_isSubmitting: true
    });

    component.update();
    expect(component.find(`${button}.Preloader`).length).toBe(1);
};

export const activeWhenFormIsValidTest = (testing_component, button = buttonApply) => {
    const component = shallow(testing_component);

    // Проверяем что кнопка активна
    component.setState({
        form_isValid: true
    });

    component.update();
    expect(component.find(button).prop('disabled')).toBe(false);
};

export const disabledWhenFormIsLoadingTest = (testing_component, button = buttonApply) => {
    const component = shallow(testing_component);

    component.setState({
        form_isLoading: true
    });

    component.update();
    expect(component.find(button).prop('disabled')).toBe(true);
};

export const buttonDoesNotRenderTest = (testing_component, button = buttonApply) => {
    const component = shallow(testing_component);

    expect(component.find(button).length).toBe(0);
};

export const runHandleSubmitTest = (testing_component, button = buttonApply) => {
    const component = shallow(testing_component);

    const handleSubmit = jest.fn();
    component.instance().handleSubmit = handleSubmit;

    component.setState({
        form_isValid: true
    });
    component.update();

    component.find(button).simulate('click');

    expect(handleSubmit).toHaveBeenCalledTimes(1);
};