export const setFormErrorTest = (testing_component) => {
    const component = shallow(testing_component);

    const setFormError = jest.fn();
    component.instance().setFormError = setFormError;

    component.instance().setUndefinedFormError({});

    expect(setFormError).toHaveBeenCalledTimes(1);
};

export const setMessageToStateTest = (testing_component) => {
    const component = shallow(testing_component);

    const data = {
        message: 'undefined error'
    };

    component.instance().setUndefinedFormError(data);

    expect(component.instance().state.server_errors_messages.undefined_error).toBe(data.message);
};