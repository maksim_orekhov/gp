export const snapShotTest = (testing_component) => {
    const component = shallow(testing_component);
    expect(shallowToJson(component)).toMatchSnapshot();
};