export const invalidFormDataTest = (testing_component) => {
    const component = shallow(testing_component);

    const data = {
        code: REQUEST_CODES.INVALID_FORM_DATA
    };

    const TakeApartInvalidFormData = jest.fn();
    component.instance().TakeApartInvalidFormData = TakeApartInvalidFormData;

    component.instance().TakeApartErrorFromServer(data);

    expect(TakeApartInvalidFormData).toHaveBeenCalledTimes(1);
};

export const anotherCodesTest = (testing_component) => {
    const component = shallow(testing_component);

    const data = {
        code: REQUEST_CODES.ACCESS_DENIED
    };

    const setFormError = jest.fn();
    component.instance().setFormError = setFormError;

    component.instance().TakeApartErrorFromServer(data);

    expect(setFormError).toHaveBeenCalledTimes(1);
};

export const undefinedFormErrorTest = (testing_component) => {
    const component = shallow(testing_component);

    const data = {
        code: undefined
    };

    const setUndefinedFormError = jest.fn();
    component.instance().setUndefinedFormError = setUndefinedFormError;

    component.instance().TakeApartErrorFromServer(data);

    expect(setUndefinedFormError).toHaveBeenCalledTimes(1);
};