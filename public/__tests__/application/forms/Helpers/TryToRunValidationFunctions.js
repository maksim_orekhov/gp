export const functionRunTest = (testing_component) => {
    const component = shallow(testing_component);

    const firstFunction = jest.fn();

    component.instance().updateValidationFunctions.push(firstFunction);

    component.instance().tryToRunValidationFunctions();

    expect(firstFunction).toHaveBeenCalledTimes(1);
};

export const runOnlyFirstFunctionTest = (testing_component) => {
    const component = shallow(testing_component);

    const firstFunction = jest.fn();
    const secondFunction = jest.fn();

    component.instance().updateValidationFunctions.push(firstFunction);
    component.instance().updateValidationFunctions.push(secondFunction);

    component.instance().tryToRunValidationFunctions();

    expect(secondFunction).toHaveBeenCalledTimes(0);
};

export const shouldNotRunIfAnotherFunctionInDeckTest = (testing_component) => {
    const component = shallow(testing_component);

    const firstFunction = jest.fn();

    component.instance().updateValidationFunctions.push(firstFunction);
    component.instance().can_update_validation = false;

    component.instance().tryToRunValidationFunctions();

    expect(firstFunction).toHaveBeenCalledTimes(0);
};

export const shiftFunctionAfterRunTest = (testing_component) => {
    const component = shallow(testing_component);

    const firstFunction = jest.fn();

    component.instance().updateValidationFunctions.push(firstFunction);

    component.instance().tryToRunValidationFunctions();

    expect(component.instance().updateValidationFunctions.length).toBe(0);
};

export const notShiftFunctionIfAnotherInDeckTest = (testing_component) => {
    const component = shallow(testing_component);

    const firstFunction = jest.fn();

    component.instance().updateValidationFunctions.push(firstFunction);
    component.instance().can_update_validation = false;

    component.instance().tryToRunValidationFunctions();

    expect(component.instance().updateValidationFunctions.length).toBe(1);
};
