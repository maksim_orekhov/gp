const e = {
    preventDefault: () => {}
};

export const shouldNotMakeRequestEvenIfValidTest = (testing_component) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve());

    Helpers.customFetch = customFetch;

    component.setState({
        form_isValid: true
    });

    component.instance().handleSubmit(e);

    expect(customFetch).toHaveBeenCalledTimes(0);
};

export const notMakeRequestWhenFormIsInvalidTest = (testing_component) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve());

    Helpers.customFetch = customFetch;

    component.setState({
        form_isValid: false
    });

    component.instance().handleSubmit(e);

    expect(customFetch).toHaveBeenCalledTimes(0);
};

export const turnOnPreloaderTest = (testing_component) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'SUCCESS'
    }));

    Helpers.customFetch = customFetch;

    component.setState({
        form_isValid: true
    });

    component.instance().handleSubmit(e);

    expect(component.state().form_isSubmitting).toBe(true);
};

export const turnOffPreloaderOnSuccessTest = (testing_component) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'SUCCESS'
    }));

    Helpers.customFetch = customFetch;

    component.setState({
        form_isValid: true
    });

    component.instance().clearFormLocalStorage = jest.fn();
    component.instance().handleSubmit(e);

    return Promise.resolve()
        .then(() => {
            expect(component.state().form_isSubmitting).toBe(false);
        });
};

export const turnOffPreloaderOnErrorTest = (testing_component) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'ERROR'
    }));

    Helpers.customFetch = customFetch;

    component.setState({
        form_isValid: true
    });

    component.instance().handleSubmit(e);

    return Promise.resolve()
        .then(() => {
            expect(component.state().form_isSubmitting).toBe(false);
        });
};

export const runAfterSubmitFunctionOnSuccessTest = (testing_component) => {
    const afterSubmit = jest.fn();
    const component = shallow(testing_component);

    component.setProps({
        afterSubmit
    });

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'SUCCESS'
    }));

    Helpers.customFetch = customFetch;

    component.setState({
        form_isValid: true
    });

    component.instance().clearFormLocalStorage = jest.fn();
    component.instance().handleSubmit(e);

    return Promise.resolve()
        .then(() => {
            expect(afterSubmit).toHaveBeenCalledTimes(1);
        });
};

export const shouldNotTurnOffPreloaderIfAfterSubmitFunctionTest = (testing_component) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'SUCCESS'
    }));

    Helpers.customFetch = customFetch;

    component.setState({
        form_isValid: true
    });

    component.instance().handleSubmit(e);

    return Promise.resolve()
        .then(() => {
            expect(component.state().form_isSubmitting).toBe(true);
        });
};

export const takeApartErrorsOnErrorTest = (testing_component) => {
    const component = shallow(testing_component);

    const TakeApartErrorFromServer = jest.fn();
    component.instance().TakeApartErrorFromServer = TakeApartErrorFromServer;

    const customFetch = jest.fn(() => Promise.resolve({
        status: 'ERROR'
    }));

    Helpers.customFetch = customFetch;

    component.setState({
        form_isValid: true
    });

    component.instance().handleSubmit(e);

    return Promise.resolve()
        .then(() => {
            expect(TakeApartErrorFromServer).toHaveBeenCalledTimes(1);
        });
};

export const correctRequestUrlTest = (testing_component, url) => {
    const component = shallow(testing_component);

    const customFetch = jest.fn(() => Promise.resolve());

    Helpers.customFetch = customFetch;

    component.setState({
        form_isValid: true
    });

    component.instance().handleSubmit(e);

    return Promise.resolve()
        .then(() => {
            expect(customFetch).lastCalledWith(url, expect.anything());
        });
};

export const runSuccessAjaxSubmit = (testing_component) => {
    const component = shallow(testing_component);
    const customFetch = jest.fn(() => Promise.resolve({
        status: 'SUCCESS'
    }));

    Helpers.customFetch = customFetch;

    component.setState({
        form_isValid: true
    });
    const successAjaxSubmit = jest.fn();
    component.instance().handleSubmit();
    component.instance.successAjaxSubmit = successAjaxSubmit;
    return Promise.resolve()
        .then(() => {
            expect(successAjaxSubmit).toHaveBeenCalledTimes(1);
        });
};

// export const runClearFormLocalStorage = (testing_component) => {
//     const component = shallow(testing_component);
// };