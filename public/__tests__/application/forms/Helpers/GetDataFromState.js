export const runCreateFormData = (testing_component) => {
    const component = shallow(testing_component, {
        disableLifecycleMethods: true
    });

    const createFormData = jest.fn();
    component.instance().createFormData = createFormData;
    component.instance().getDataFromState();
    expect(createFormData).toHaveBeenCalledTimes(1);
};
