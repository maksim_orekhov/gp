import React from 'react';
import FormNaturalPerson from '../../../js/application/react/forms/FormNaturalPerson';
import * as SetControlsErrorFromUpperForm from './Helpers/SetControlsErrorFromUpperForm';
import * as Csrf from './Helpers/Csrf';
import * as CheckAllControlsAreValid from './Helpers/CheckAllControlsAreValid';
import * as SendUpFormData from './Helpers/SendUpFormData';
import * as ButtonSubmit from './Helpers/ButtonSubmit';
import * as ButtonDelete from './Helpers/ButtonDelete';
import * as HandleSubmit from './Helpers/HandleSubmit';
import * as HandleDelete from './Helpers/HandleDelete';
import * as ButtonClose from './Helpers/ButtonClose';
import formBaseTests from './baseTests/formBaseTests';

formBaseTests(<FormNaturalPerson/>);

describe('csrf', () => {
    it('should get csrf when mount', () => {
        Csrf.getCsrfOnMountTest(<FormNaturalPerson />);
    });

    it('should not get csrf when nested', () => {
        Csrf.notGetCsrfWhenNestedTest(
            <FormNaturalPerson
                is_nested={true}
            />
        );
    });

    it('should set correct csrf to state', async () => {
        await Csrf.setCsrfToStateTest(<FormNaturalPerson />);
    });

    it('should set form init error on fail request', async () => {
        await Csrf.setFormInitErrorTest(<FormNaturalPerson />);
    });
});

describe('checkAllControlsAreValid', () => {
    it('form is not valid, if one flag is false', () => {
        CheckAllControlsAreValid.formIsInvalidTest(<FormNaturalPerson />);
    });

    it('form is valid if all flags are true', () => {
        CheckAllControlsAreValid.formIsValidTest(<FormNaturalPerson />);
    });

    it('form is not valid if editMode and data is equal to original', () => {
        CheckAllControlsAreValid.formInvalidOriginalDataTest(
            <FormNaturalPerson
                editMode={true}
            />
        );
    });

    it('form is valid if editMode and data is different from original', () => {
        CheckAllControlsAreValid.formValidOriginalDataTest(
            <FormNaturalPerson
                editMode={true}
            />
        );
    });
});

describe('sendUpFormData', () => {
    it('should not trigger if form is not nested', () => {
        SendUpFormData.notRunIfNotNestedTest(<FormNaturalPerson/>);
    });

    it('when form is not valid should only send up validation', () => {
        SendUpFormData.sendUpOnlyValidationTest(
            <FormNaturalPerson
                is_nested={true}
            />
        );
    });

    it('when form is valid send up data and validation', () => {
        SendUpFormData.sendUpDataAndValidationTest(
            <FormNaturalPerson
                is_nested={true}
            />
        );
    });
});

describe('button apply', () => {
    it('button is disable if form is invalid', () => {
        ButtonSubmit.disabledWhenFormIsInvalidTest(<FormNaturalPerson />);
    });

    it('button preloader', () => {
        ButtonSubmit.preloaderTest(<FormNaturalPerson />);
    });

    it('button is active when form is valid', () => {
        ButtonSubmit.activeWhenFormIsValidTest(<FormNaturalPerson />);
    });

    it('button is disable when form is loading', () => {
        ButtonSubmit.disabledWhenFormIsLoadingTest(<FormNaturalPerson />);
    });

    it('button should not render when form is nested', () => {
        ButtonSubmit.buttonDoesNotRenderTest(
            <FormNaturalPerson
                is_nested={true}
            />
        );
    });

    it('should handle submit on click', () => {
        ButtonSubmit.runHandleSubmitTest(<FormNaturalPerson />);
    });
});

describe('button delete', () => {
    it('button should not render when form is not editMode', () => {
        ButtonDelete.buttonDoesNotRenderTest(<FormNaturalPerson />);
    });

    it('button should render only when form is editMode', () => {
        ButtonDelete.buttonShouldRenderTest(
            <FormNaturalPerson
                editMode={true}
            />
        );
    });

    it('button is active default', () => {
        ButtonDelete.buttonIsActiveTest(
            <FormNaturalPerson
                editMode={true}
            />
        );
    });

    it('button preloader', () => {
        ButtonDelete.preloaderTest(
            <FormNaturalPerson
                editMode={true}
            />
        );
    });

    it('button is disable when form is loading', () => {
        ButtonDelete.disabledWhenFormIsLoadingTest(
            <FormNaturalPerson
                editMode={true}
            />
        );
    });

    it('button should not render when form is nested', () => {
        ButtonDelete.buttonDoesNotRenderTest(
            <FormNaturalPerson
                is_nested={true}
            />
        );
    });

    it('should handleDelete on click', () => {
        ButtonDelete.runHandleDeleteTest(
            <FormNaturalPerson
                editMode={true}
            />
        );
    });
});

describe('handleSubmit', () => {
    it('should not make request when form is nested', () => {
        HandleSubmit.shouldNotMakeRequestEvenIfValidTest(
            <FormNaturalPerson
                is_nested={true}
            />
        );
    });

    it('should not make request when form is not valid', () => {
        HandleSubmit.notMakeRequestWhenFormIsInvalidTest(<FormNaturalPerson/>);
    });

    it('should turn on preloader', () => {
        HandleSubmit.turnOnPreloaderTest(<FormNaturalPerson/>);
    });

    it('should turn off preloader when form submit is success', async () => {
        await HandleSubmit.turnOffPreloaderOnSuccessTest(<FormNaturalPerson/>);
    });

    it('should turn off preloader when form submit is fail', async () => {
        await HandleSubmit.turnOffPreloaderOnErrorTest(<FormNaturalPerson/>);
    });

    it('should run afterSubmit function when form submit is success', async () => {
        await HandleSubmit.runAfterSubmitFunctionOnSuccessTest(<FormNaturalPerson/>)
    });

    it('should not turn off preloader if form has afterSubmit function and submit is success', async () => {
        await HandleSubmit.shouldNotTurnOffPreloaderIfAfterSubmitFunctionTest(
            <FormNaturalPerson
                afterSubmit={() => {}}
            />
        );
    });

    it('should take apart errors when form submit is fail', async () => {
        await HandleSubmit.takeApartErrorsOnErrorTest(<FormNaturalPerson/>);
    });

    it('should create at correct url', async () => {
        await HandleSubmit.correctRequestUrlTest(<FormNaturalPerson/>, URLS.NATURAL_PERSON.SINGLE);
    });

    it('should update at correct url', async () => {
        await HandleSubmit.correctRequestUrlTest(
            <FormNaturalPerson
                editMode={true}
            />,
            `${URLS.NATURAL_PERSON.SINGLE}/undefined`
        );
    });
});

describe('handleDelete', () => {
    it('should turn on preloader', () => {
        HandleDelete.turnOnPreloaderTest(
            <FormNaturalPerson
                data={1}
            />
        );
    });

    it('should turn off preloader when delete is success', async () => {
        await HandleDelete.turnOffPreloaderOnSuccessTest(
            <FormNaturalPerson
                data={1}
            />
        );
    });

    it('should turn off preloader when form delete is fail', async () => {
        await HandleDelete.turnOffPreloaderOnErrorTest(
            <FormNaturalPerson
                data={1}
            />
        );
    });

    it('should run afterDelete function when form delete is success', async () => {
        await HandleDelete.runAfterDeleteFunctionOnSuccessTest(
            <FormNaturalPerson
                data={1}
            />
        );
    });

    it('should not turn off preloader if form has afterDelete function and delete is success', async () => {
        await HandleDelete.shouldNotTurnOffPreloaderIfAfterDeleteFunctionTest(
            <FormNaturalPerson
                data={1}
                afterDelete={() => {}}
            />
        );
    });

    it('should take apart errors when form delete is fail', async () => {
        await HandleDelete.takeApartErrorsOnErrorTest(
            <FormNaturalPerson
                data={1}
            />
        );
    });

    it('should delete at correct url', async () => {
        await HandleDelete.correctRequestUrlTest(
            <FormNaturalPerson
                data={{id: 1}}
            />,
            '/natural-person/1'
        );
    });
});

describe('setControlsErrorFromUpperForm', () => {
    const form_server_errors = {
        last_name: 'last_name error',
        first_name: 'first_name error'
    };

    it('should run function when get props', () => {
        SetControlsErrorFromUpperForm.willReceivePropsTest(<FormNaturalPerson />);
    });

    it('should not setState when get the same props', () => {
        SetControlsErrorFromUpperForm.receiveSamePropsTest(
            <FormNaturalPerson
                form_server_errors={form_server_errors}
            />,
            form_server_errors
        );
    });

    it('should correct set state', () => {
        SetControlsErrorFromUpperForm.setErrorsToStateTest(<FormNaturalPerson />, form_server_errors);
    });
});

describe('buttonClose', () => {
    it('should not render button close without prop closable', () => {
        ButtonClose.buttonDoesNotRenderTest(<FormNaturalPerson />);
    });

    it('should render button close', () => {
        ButtonClose.buttonRenderTest(
            <FormNaturalPerson
                isClosable={true}
            />
        );
    });

    it('should run close form function on click', () => {
       ButtonClose.buttonClickTest(
           <FormNaturalPerson
               isClosable={true}
           />
       );
    });
});