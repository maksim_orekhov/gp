import React from 'react';
import FormBankTransfer from '../../../js/application/react/forms/FormBankTransfer';
import * as SetControlsErrorFromUpperForm from './Helpers/SetControlsErrorFromUpperForm';
import * as Csrf from './Helpers/Csrf';
import * as CheckAllControlsAreValid from './Helpers/CheckAllControlsAreValid';
import * as SendUpFormData from './Helpers/SendUpFormData';
import * as ButtonSubmit from './Helpers/ButtonSubmit';
import * as ButtonDelete from './Helpers/ButtonDelete';
import * as HandleSubmit from './Helpers/HandleSubmit';
import * as HandleDelete from './Helpers/HandleDelete';
import * as ButtonClose from './Helpers/ButtonClose';
import formBaseTests from './baseTests/formBaseTests';
import URLS from "../../../js/application/react/constants/urls";

formBaseTests(<FormBankTransfer />);

describe('csrf', () => {
    it('should get csrf when mount', () => {
        Csrf.getCsrfOnMountTest(<FormBankTransfer />);
    });

    it('should not get csrf when nested', () => {
        Csrf.notGetCsrfWhenNestedTest(
            <FormBankTransfer
                is_nested={true}
            />
        );
    });

    it('should set correct csrf to state', async () => {
        await Csrf.setCsrfToStateTest(<FormBankTransfer />);
    });

    it('should set form init error on fail request', async () => {
        await Csrf.setFormInitErrorTest(<FormBankTransfer />);
    });
});

describe('checkAllControlsAreValid', () => {
    it('form is not valid, if one flag is false', () => {
        CheckAllControlsAreValid.formIsInvalidTest(<FormBankTransfer />);
    });

    it('form is valid if all flags are true', () => {
        CheckAllControlsAreValid.formIsValidTest(<FormBankTransfer />);
    });

    it('form is not valid if editMode and data is equal to original', () => {
        CheckAllControlsAreValid.formInvalidOriginalDataTest(
            <FormBankTransfer
                editMode={true}
            />
        );
    });

    it('form is valid if editMode and data is different from original', () => {
        CheckAllControlsAreValid.formValidOriginalDataTest(
            <FormBankTransfer
                editMode={true}
            />
        );
    });
});

describe('sendUpFormData', () => {
    it('should not trigger if form is not nested', () => {
        SendUpFormData.notRunIfNotNestedTest(<FormBankTransfer/>);
    });

    it('when form is not valid should only send up validation', () => {
        SendUpFormData.sendUpOnlyValidationTest(
            <FormBankTransfer
                is_nested={true}
            />
        );
    });

    it('when form is valid send up data and validation', () => {
        SendUpFormData.sendUpDataAndValidationTest(
            <FormBankTransfer
                is_nested={true}
            />
        );
    });
});

describe('button submit', () => {
    it('button is disabled when form is not valid', () => {
        ButtonSubmit.disabledWhenFormIsInvalidTest(<FormBankTransfer/>)
    });

    it('button preloader', () => {
        ButtonSubmit.preloaderTest(<FormBankTransfer/>)
    });

    it('button is active when form is valid', () => {
        ButtonSubmit.activeWhenFormIsValidTest(<FormBankTransfer/>)
    });

    it('button is disabled when form is loading', () => {
        ButtonSubmit.disabledWhenFormIsLoadingTest(<FormBankTransfer/>)
    });

    it('button should not render when form is nested', () => {
        ButtonSubmit.buttonDoesNotRenderTest(
            <FormBankTransfer
                is_nested={true}
            />
        );
    });

    it('should handle submit on click', () => {
        ButtonSubmit.runHandleSubmitTest(<FormBankTransfer />);
    });
});

describe('button delete', () => {
    it('button preloader', () => {
        ButtonDelete.preloaderTest(<FormBankTransfer editMode={true}/>)
    });

    it('button disable when form is loading', () => {
        ButtonDelete.disabledWhenFormIsLoadingTest(<FormBankTransfer editMode={true}/>)
    });

    it('button should not render when form is not in editMode', () => {
        ButtonDelete.buttonDoesNotRenderTest(<FormBankTransfer />)
    });

    it('should call handleDelete method on click', () => {
        ButtonDelete.runHandleDeleteTest(<FormBankTransfer editMode={true}/>)
    });

    it('button should render when form is in editMode', () => {
        ButtonDelete.buttonShouldRenderTest(<FormBankTransfer editMode={true}/>)
    });

    it('button is active', () => {
        ButtonDelete.buttonIsActiveTest(<FormBankTransfer editMode={true}/>)
    });
});

describe('handle Submit', () => {
    it('should not make request when form is nested', () => {
        HandleSubmit.shouldNotMakeRequestEvenIfValidTest(
            <FormBankTransfer
                is_nested={true}
            />
        )
    });

    it('should not send request when form is invalid', () => {
        HandleSubmit.notMakeRequestWhenFormIsInvalidTest(<FormBankTransfer />)
    });

    it('should turn on preloader', () => {
        HandleSubmit.turnOnPreloaderTest(<FormBankTransfer />)
    });

    it('should turn off preloader when submit is success', async () => {
        await HandleSubmit.turnOffPreloaderOnSuccessTest(<FormBankTransfer />)
    });

    it('should turn off preloader when submit is error', async () => {
        await HandleSubmit.turnOffPreloaderOnErrorTest(<FormBankTransfer />)
    });

    it('should call afterSubmit method when submit is success', () => {
        HandleSubmit.runAfterSubmitFunctionOnSuccessTest(<FormBankTransfer />)
    });

    it('should call TakeApartErrorsFromServer method when submit failed', async () => {
        await HandleSubmit.takeApartErrorsOnErrorTest(<FormBankTransfer />)
    });

    it('should create at correct url', async () => {
        await HandleSubmit.correctRequestUrlTest(<FormBankTransfer/>, URLS.PAYMENT_METHOD.SINGLE);
    });

    it('should update at correct url', () => {
        HandleSubmit.correctRequestUrlTest(
            <FormBankTransfer
                editMode={true}
            />,
            `${URLS.PAYMENT_METHOD.SINGLE}/undefined`
        );
    });
});

describe('handle Delete', () => {
    it('should turn on preloader', () => {
        HandleDelete.turnOnPreloaderTest(<FormBankTransfer />)
    });

    it('should turn off preloader when delete is success', () => {
        HandleDelete.turnOffPreloaderOnSuccessTest(<FormBankTransfer />)
    });

    it('should turn off preloader when delete is error', async () => {
        await HandleDelete.turnOffPreloaderOnErrorTest(<FormBankTransfer />)
    });

    it('should not turn off preloader if form has afterDelete function and delete is success', () => {
       HandleDelete.shouldNotTurnOffPreloaderIfAfterDeleteFunctionTest(<FormBankTransfer />)
    });

    it('should call TakeApartErrorsFromServer method when submit failed', async () => {
        await HandleDelete.takeApartErrorsOnErrorTest(<FormBankTransfer />)
    });

    it('should delete at correct url', async () => {
        await HandleDelete.correctRequestUrlTest(<FormBankTransfer editMode={true}/>, `${URLS.PAYMENT_METHOD.SINGLE}/`);
    });
});

describe('setControlsErrorFromUpperForm', () => {
    const form_server_errors = {
        name: 'name error',
        bank_name: 'bank_name error'
    };

    it('should run function when get props', () => {
        SetControlsErrorFromUpperForm.willReceivePropsTest(<FormBankTransfer />);
    });

    it('should not setState when get the same props', () => {
        SetControlsErrorFromUpperForm.receiveSamePropsTest(
            <FormBankTransfer
                form_server_errors={form_server_errors}
            />,
            form_server_errors
        );
    });

    it('should correct set state', () => {
        SetControlsErrorFromUpperForm.setErrorsToStateTest(<FormBankTransfer />, form_server_errors);
    });
});


