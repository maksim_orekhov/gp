import { getUrlParameter, wordsCountInString, getIdFromUrl, getIdsFromString
} from '../../js/application/react/Helpers';
import {
    validateIsGreaterOrEqual, validateIsLesserOrEqual, validateRusCharacters
} from '../../js/application/react/Validations';


describe('Helpers', () => {
    it('test of getUrlParameter', () => {
        const test_url_params = '?page=2&per_page=10';

        expect(getUrlParameter(test_url_params, 'page')).toBe('2');
        expect(getUrlParameter(test_url_params, 'per_page')).toBe('10');
        expect(getUrlParameter(test_url_params, 'del_id')).toBe('');
    });
});

describe('Minimum characters length', () => {
    it('test of minCharsLength', () => {

        expect(validateIsGreaterOrEqual(4, 2)).toBe(true);
        expect(validateIsGreaterOrEqual(2, 2)).toBe(true);
        expect(validateIsGreaterOrEqual(1, 2)).toBe(false);
    });
});

describe('Maximum characters length', () => {
    it('test of maxCharsLength', () => {

        expect(validateIsLesserOrEqual(4, 2)).toBe(false);
        expect(validateIsLesserOrEqual(2, 2)).toBe(true);
        expect(validateIsLesserOrEqual(1, 2)).toBe(true);
    });
});

describe('Minimum words in control', () => {
    it('test of minWords', () => {

        expect(validateIsGreaterOrEqual(wordsCountInString('Один два три'), 2)).toBe(true);
        expect(validateIsGreaterOrEqual(wordsCountInString('Один два три'), 3)).toBe(true);
        expect(validateIsGreaterOrEqual(wordsCountInString('Один два три'), 4)).toBe(false);
    });
});

describe('Maximum words in control', () => {
    it('test of maxWords', () => {

        expect(validateIsLesserOrEqual(wordsCountInString('Один два три'), 2)).toBe(false);
        expect(validateIsLesserOrEqual(wordsCountInString('Один два три'), 3)).toBe(true);
        expect(validateIsLesserOrEqual(wordsCountInString('Один два три'), 4)).toBe(true);
    });
});

describe('Cyrillic letters', () => {
    it('test of rusCharacters', () => {
        const value = 'No cyrillic letters here';

        expect(validateRusCharacters(value)).toBe(false);
    });
});

describe('getIdFromUrl', () => {
    it('should return id 12', () => {
        expect(getIdFromUrl('http://192.168.0.99/deal/12/')).toBe('12');
    });

    it('should return null', () => {
        expect(getIdFromUrl('http://192.168.0.99/deal/')).toBe(null);
    });

    it('should return id 10', () => {
        expect(getIdFromUrl('http://192.168.0.99/deal/123/comment/10')).toBe('10');
    });
});

describe('getIdsFromString', () => {
    it('should return [12]', () => {
        expect(getIdsFromString('http://192.168.0.99/deal/12/')).toEqual(['12']);
    });

    it('should return null', () => {
        expect(getIdsFromString('http://192.168.0.99/deal/')).toBe(null);
    });

    it('should return [123, 10]', () => {
        expect(getIdsFromString('http://192.168.0.99/deal/123/comment/10')).toEqual(['123', '10']);
    });
});