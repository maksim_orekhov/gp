import React from 'react';
import ControllerCivilLawSubjectsTables from '../../../js/application/react/controllers/ControllerCivilLawSubjectsTables';

describe('ControllerCivilLawSubjectsTables', () => {
    it('should correct snapshot render', () => {
        const component = shallow(<ControllerCivilLawSubjectsTables />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should run getCollection function when mounted', () => {
        const getCollection = jest.fn(() => Promise.resolve());

        const component = shallow(<ControllerCivilLawSubjectsTables />);
        component.instance().getCollection = getCollection;
        component.instance().componentDidMount();
        expect(getCollection).toHaveBeenCalledTimes(1);
    });

    it('should turn on preloader when mounted', () => {
        const toggleIsLoading = jest.fn();

        const component = shallow(<ControllerCivilLawSubjectsTables />);
        component.instance().toggleIsLoading = toggleIsLoading;
        component.instance().componentDidMount();
        // expect(toggleIsLoading).toHaveBeenCalledTimes(1);
        expect(component.state().table_isLoading).toBe(true);
    });

    it('should set correct collection to state', () => {
        const data = {
            status: 'SUCCESS',
            data: {
                naturalPersons: []
            }
        };

        Helpers.customFetch = jest.fn(() => Promise.resolve(data));

        const component = shallow(<ControllerCivilLawSubjectsTables />);

        return Promise.resolve()
            .then(() => {
                expect(component.state().naturalPersons).toEqual(data.data.naturalPersons)
            });
    });

    it('should set is_operator to state', () => {
        const component = shallow(<ControllerCivilLawSubjectsTables />);
        Helpers.customFetch = jest.fn(() => Promise.resolve());

        return Promise.resolve()
            .catch(() => {
                expect(component.state().is_operator).toBe(true)
            });
    });

    it('should correctly change tables', () => {
        const component = shallow(<ControllerCivilLawSubjectsTables />);
        const table = 'table_legal_entities';
        const event = {
            preventDefault() {},
            target: { dataset: {value: 'table_legal_entities'} }
        };
        component.instance().handleChangeTable(event);
        expect(component.state().current_table).toEqual(table)
    });

    it('should highlight new item', () => {
        jest.useFakeTimers();
        const component = shallow(<ControllerCivilLawSubjectsTables />);

        component.instance().highlightNewItem();
        expect(component.state().isNewItem).toBe(true);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 3000);
    });
});