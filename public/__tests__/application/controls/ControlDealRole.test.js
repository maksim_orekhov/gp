import React from 'react';
import ControlDealRole from '../../../js/application/react/controls/ControlDealRole';
import { sendUpValidationOnReceivePropsTest } from './Helpers/SendUpValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlDealRole', () => {
    const props = {
        label: 'Ваша роль в сделке:',
        name: 'deal_role',
        value_prop: 'customer',
        deal_type: {ident: 'U'}
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlDealRole {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlDealRole {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlDealRole {...props} />, flags);
    });
});