import React from 'react';
import ControlDealAddonTerms from '../../../js/application/react/controls/ControlDealAddonTerms';
import { sendUpValidationOnReceivePropsTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations } from './Helpers/CheckAllValidation';

describe('ControlDealAddonTerms', () => {
    const props = {
        label: 'Описание товара',
        name: 'addon_terms',
        placeholder: 'Введите описание товара',
        value_prop: 'Некое описание товара'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlDealAddonTerms {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlDealAddonTerms {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckAllValidations(<ControlDealAddonTerms {...props} />, props);
    });
});