import React from 'react';
import ControlBik from '../../../js/application/react/controls/ControlBik';
import { sendUpValidationOnReceivePropsTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlBik', () => {
    const props = {
        label: 'БИК',
        name: 'bik',
        placeholder: 'Введите БИК',
        value_prop: `04${'1'.repeat(VALIDATION_RULES.BIK.chars_count - 2)}`
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlBik {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlBik {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        didComponentChangeWhenInputChangeTest(<ControlBik {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlBik {...props} />);
        shouldCheckAllValidations(<ControlBik {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            is_bik_valid: true,
            chars_count: true,
            is_bik_exist: false,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlBik {...props} />, flags);
    });

    it('should set biks hint', () => {
        const component = shallow(<ControlBik {...props} />);
        component.find('input').simulate('change', {target: {value: '04'}});
        expect(component.state().isSearchHint).toBe(true);
    });
});