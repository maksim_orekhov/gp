import React from 'react';
import ControlSoleProprietorName from '../../../js/application/react/controls/ControlSoleProprietorName';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlSoleProprietorName', () => {
    const props = {
        label: 'Название ИП',
        name: 'name',
        placeholder: 'Введите название ИП',
        value_prop: 'ИП Иванов'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlSoleProprietorName {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlSoleProprietorName {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlSoleProprietorName {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlSoleProprietorName {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlSoleProprietorName {...props} />);
        shouldCheckAllValidations(<ControlSoleProprietorName {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            cyrillic_only: false,
            max_chars: false,
            max_words: false,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlSoleProprietorName {...props} />, flags);
    });
});