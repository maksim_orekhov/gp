import React from 'react';
import ControlDeliveryPeriod from '../../../js/application/react/controls/ControlDeliveryPeriod';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlDeliveryPeriod', () => {
    const props = {
        label: 'Срок доставки и проверки (дней):',
        name: 'delivery_period',
        placeholder: 'Введите срок доставки и проверки товара товара',
        value_prop: '14'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlDeliveryPeriod {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlDeliveryPeriod {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlDeliveryPeriod {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlDeliveryPeriod {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlDeliveryPeriod {...props} />);
        shouldCheckAllValidations(<ControlDeliveryPeriod {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            delivery_period_is_valid: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlDeliveryPeriod {...props} />, flags);
    });
});