import React from 'react';
import ControlAmount from '../../../js/application/react/controls/ControlAmount';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlAmount', () => {
    const props = {
        label: 'Цена товара',
        name: 'amount',
        placeholder: 'Введите стоимость товара',
        value_prop: '1000'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlAmount {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlAmount {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlAmount {...props} />, props, true);
        didComponentChangeWhenInputChangeTest(<ControlAmount {...props} />, props, true);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlAmount {...props} />);
        shouldCheckAllValidations(<ControlAmount {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlAmount {...props} />, flags);
    });
});