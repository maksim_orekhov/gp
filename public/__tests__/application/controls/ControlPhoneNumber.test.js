import React from 'react';
import PhoneNumber from '../../../js/landing/react/auth/forms/RegisterForm/controls/phone-input';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('PhoneNumber', () => {
    const props = {
        componentValue: '+79102341212',
        register: true,
        name: 'phone'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<PhoneNumber {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {

        const component = shallow(
            <PhoneNumber
                {...props}
            />
        );
        expect(component.instance().checkAllValidations('')).toBe(false); // пустое значение
        expect(component.instance().checkAllValidations(props.componentValue)).toEqual({"isValid": true, "message": ""}); // валидный вариант
    });

    it('should sends up value and validation when input change value', () => {
        const handleComponentValidation = jest.fn();
        const handleComponentChange = jest.fn();

        const component = shallow(
            <PhoneNumber
                {...props}
                componentValue=''
                handleComponentChange={handleComponentChange}
                handleComponentValidation={handleComponentValidation}
                register={false}
                name='phone'
            />
        );
        // Симулируем изменение инпута
        component.find('InputElement').simulate('change', {target: {value: props.componentValue}});
        // Ожидаем что сработают функции у компонента родителя
        expect(handleComponentValidation).lastCalledWith(`${props.name}_is_valid`, true);
        expect(handleComponentChange).lastCalledWith(props.name, props.componentValue);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            phone: false,
            reg_exp_invalid:    null,
            phone_unavailable:  false,
            connection_is_lost: false,
            undefined_error:    false
        };
        setCorrectErrorFlags(<PhoneNumber {...props} />, flags);
    });
});