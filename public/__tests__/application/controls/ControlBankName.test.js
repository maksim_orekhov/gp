import React from 'react';
import ControlBankName from '../../../js/application/react/controls/ControlBankName';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlBankName', () => {
    const props = {
        label: 'Наименование банка',
        name: 'bank_name',
        placeholder: 'Введите наименование банка',
        value_prop: 'Сбербанк'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlBankName {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlBankName {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlBankName {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlBankName {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlBankName {...props} />);
        shouldCheckAllValidations(<ControlBankName {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            min_chars: true,
            max_chars: false,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlBankName {...props} />, flags);
    });
});