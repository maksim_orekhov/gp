import React from 'react';
import ControlDealName from '../../../js/application/react/controls/ControlDealName';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlDealName', () => {
    const props = {
        label: 'Название сделки',
        name: 'deal_name',
        placeholder: 'Введите название сделки',
        value_prop: 'Тестовая сделка'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlDealName {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlDealName {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlDealName {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlDealName {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlDealName {...props} />);
        shouldCheckAllValidations(<ControlDealName {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlDealName {...props} />, flags);
    });
});