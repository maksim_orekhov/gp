import React from 'react';
import ControlNdsType from '../../../js/application/react/controls/ControlNdsType';
import { sendUpValidationOnReceivePropsTest } from './Helpers/SendUpValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';
import {shouldCheckAllValidations, shouldCheckEmptyValue} from "./Helpers/CheckAllValidation";
import {getActiveTabNds} from "./Helpers/getActiveTab";

describe('ControlNdsType', () => {
    const props = {
        label: 'Тип НДС',
        name: 'nds_type',
        placeholder: 'Выберите тип НДС',
        value_prop: 1,
        nds_types: [
           {
                name: 0,
                type: 1
           }
        ]
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlNdsType {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlNdsType {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlNdsType {...props} />);
        shouldCheckAllValidations(<ControlNdsType {...props} />, props);
    });

    it('should correct get active tab', () => {
        getActiveTabNds(<ControlNdsType {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlNdsType {...props} />, flags);
    });
});