import React from 'react';
import ControlFirstName from '../../../js/application/react/controls/ControlFirstName';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';
import VALIDATION_RULES from "../../../js/application/react/constants/validation_rules";

describe('ControlFirstName', () => {
    const props = {
        label: 'Имя',
        name: 'first_name',
        placeholder: 'Введите имя',
        value_prop: 'Иван'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlFirstName {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlFirstName {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlFirstName {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlFirstName {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlFirstName {...props} />);
        shouldCheckAllValidations(<ControlFirstName {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            cyrillic_only: false,
            min_chars: true,
            max_chars: false,
            min_words: false,
            max_words: false,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlFirstName {...props} />, flags);
    });
});