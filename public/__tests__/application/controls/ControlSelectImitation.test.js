import React from 'react';
import ControlSelectImitation from '../../../js/application/react/controls/ControlSelectImitation';
import { isDropdownOpen } from './Helpers/OpenForm';
import {getActiveTab} from "./Helpers/GetActiveTab";

describe('ControlSelectImitation', () => {
    const props = {
        default_option: 'Выберите метод оплаты',
        active_tab: 'Счет ВТБ'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlSelectImitation {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should dropdown open', () => {
        isDropdownOpen(<ControlSelectImitation {...props} />, props);
    });

    it('should correct get active tab', () => {
        getActiveTab(<ControlSelectImitation {...props} />, props);
    });
});