import React from 'react';
import ControlFeePayerOption from '../../../js/application/react/controls/ControlFeePayerOption';
import { sendUpValidationOnReceivePropsTest } from './Helpers/SendUpValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlFeePayerOption', () => {
    const props = {
        label: 'Кто оплачивает комиссию:',
        name: 'fee_payer_option',
        value_prop: '13',
        deal_type: {ident: 'U'},
        fee_payer_options: [
           {
                name: 'CUSTOMER',
                id: 13
           }
        ]
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlFeePayerOption {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlFeePayerOption {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlFeePayerOption {...props} />, flags);
    });
});