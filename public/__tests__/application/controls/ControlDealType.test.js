import React from 'react';
import ControlDealType from '../../../js/application/react/controls/ControlDealType';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlDealType', () => {
    const props = {
        label: 'Тип сделки:',
        name: 'deal_type',
        value_prop: 9,
        deal_type: {ident: 'U', name: 'Услуга'},
        deal_types: {9: {name: 'Услуга'}}
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlDealType {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    // it('should sends up validation when receive new value from parent component', () => {
    //     sendUpValidationOnReceivePropsTest(<ControlDealType {...props} />, props);
    // });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlDealType {...props} />);
        shouldCheckAllValidations(<ControlDealType {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlDealType {...props} />, flags);
    });
});