import React from 'react';
import ControlLastName from '../../../js/application/react/controls/ControlLastName';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlLastName', () => {
    const props = {
        label: 'Фамилия',
        name: 'last_name',
        placeholder: 'Введите фамилию',
        value_prop: 'Иванов'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlLastName {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlLastName {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlLastName {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlLastName {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlLastName {...props} />);
        shouldCheckAllValidations(<ControlLastName {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            cyrillic_only: false,
            min_chars: true,
            max_chars: false,
            min_words: false,
            max_words: false,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlLastName {...props} />, flags);
    });
});