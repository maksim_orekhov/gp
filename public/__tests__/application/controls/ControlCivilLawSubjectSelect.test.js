import React from 'react';
import ControlCivilLawSubjectSelect from '../../../js/application/react/controls/ControlCivilLawSubjectSelect';
import { sendUpValidationOnReceivePropsTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlCivilLawSubjectSelect', () => {
    const props = {
        label: 'Вы будете участвовать в сделке как:',
        name: 'deal_civil_law_subject',
        value_prop: '379'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlCivilLawSubjectSelect {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlCivilLawSubjectSelect {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlCivilLawSubjectSelect {...props} />);
        shouldCheckAllValidations(<ControlCivilLawSubjectSelect {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlCivilLawSubjectSelect {...props} />, flags);
    });
});