import React from 'react';
import ControlPhoneConfirmToken from '../../../js/application/react/controls/ControlPhoneConfirmToken';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlPhoneConfirmToken', () => {
    const props = {
        name: 'code',
        placeholder: 'Код подтверждения',
        value_prop: 1000
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlPhoneConfirmToken {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlPhoneConfirmToken {...props} />);
        shouldCheckAllValidations(<ControlPhoneConfirmToken {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlPhoneConfirmToken {...props} />, flags);
    });
});