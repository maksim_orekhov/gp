import React from 'react';
import ControlAddress from '../../../js/application/react/controls/ControlAddress';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlAddress', () => {
    const props = {
        label: 'Юридический адрес',
        name: 'legal_address',
        placeholder: 'Введите юридический адрес',
        value_prop: 'ул. Орловская, г.Брянск'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlAddress {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlAddress {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlAddress {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlAddress {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlAddress {...props} />);
        shouldCheckAllValidations(<ControlAddress {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            min_chars: true,
            max_chars: false,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlAddress {...props} />, flags);
    });
});