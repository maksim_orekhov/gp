import React from 'react';
import ControlDisputeDiscount from '../../../js/application/react/controls/ControlDisputeDiscount';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlDisputeDiscount', () => {
    const props = {
        label: 'Сделать скидку',
        name: 'amount_discount',
        placeholder: 'Размер скидки, рублей:',
        max_discount_amount: '100000',
        value_prop: '100'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlDisputeDiscount {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlDisputeDiscount {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlDisputeDiscount {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlDisputeDiscount {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlDisputeDiscount {...props} />);
        shouldCheckAllValidations(<ControlDisputeDiscount {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            max_discount_amount: false,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlDisputeDiscount {...props} />, flags);
    });
});