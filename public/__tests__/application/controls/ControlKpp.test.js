import React from 'react';
import ControlKpp from '../../../js/application/react/controls/ControlKpp';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlKpp', () => {
    const props = {
        label: 'КПП',
        name: 'kpp',
        placeholder: 'Введите КПП',
        value_prop: '366401001'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlKpp {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlKpp {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlKpp {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlKpp {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlKpp {...props} />);
        shouldCheckAllValidations(<ControlKpp {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            min_chars: true,
            max_chars: false,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlKpp {...props} />, flags);
    });
});