import React from 'react';
import ControlPaymentMethodSelect from '../../../js/application/react/controls/ControlPaymentMethodSelect';
import { sendUpValidationOnReceivePropsTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';
import {getActiveTabPaymentMethod} from "./Helpers/GetActiveTab";

describe('ControlPaymentMethodSelect', () => {
    const props = {
        label: 'Метод получения оплаты:',
        name: 'payment_method',
        value_prop: 355,
        payment_methods: [
            {
                id: 355,
                name: 'Счет ВТБ'
            }
        ]
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlPaymentMethodSelect {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlPaymentMethodSelect {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlPaymentMethodSelect {...props} />);
        shouldCheckAllValidations(<ControlPaymentMethodSelect {...props} />, props);
    });

    it('should correct get active tab', () => {
        getActiveTabPaymentMethod(<ControlPaymentMethodSelect {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlPaymentMethodSelect {...props} />, flags);
    });
});