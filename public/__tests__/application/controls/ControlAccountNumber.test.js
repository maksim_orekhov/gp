import React from 'react';
import ControlAccountNumber from '../../../js/application/react/controls/ControlAccountNumber';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlAccountNumber', () => {
    const props = {
        label: 'Номер лицевого счета',
        name: 'account_number',
        placeholder: 'Введите номер лицевого счета',
        value_prop: '1'.repeat(VALIDATION_RULES.ACCOUNT_NUMBER.chars_count)
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlAccountNumber {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlAccountNumber {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlAccountNumber {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlAccountNumber {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlAccountNumber {...props} />);
        shouldCheckAllValidations(<ControlAccountNumber {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            chars_count: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlAccountNumber {...props} />, flags);
    });
});