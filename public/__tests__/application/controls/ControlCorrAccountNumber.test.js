import React from 'react';
import ControlCorrAccountNumber from '../../../js/application/react/controls/ControlCorrAccountNumber';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlCorrAccountNumber', () => {
    const props = {
        label: 'Номер корреспондентского счета',
        name: 'corr_account_number',
        placeholder: 'Введите номер корреспондентского счета',
        value_prop: '1'.repeat(VALIDATION_RULES.CORR_ACCOUNT_NUMBER.chars_count)
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlCorrAccountNumber {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlCorrAccountNumber {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlCorrAccountNumber {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlCorrAccountNumber {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlCorrAccountNumber {...props} />);
        shouldCheckAllValidations(<ControlCorrAccountNumber {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            chars_count: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlCorrAccountNumber {...props} />, flags);
    });
});