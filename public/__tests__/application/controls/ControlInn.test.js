import React from 'react';
import ControlInn from '../../../js/application/react/controls/ControlInn';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlInn', () => {
    const props = {
        label: 'ИНН',
        name: 'inn',
        placeholder: 'Введите ИНН',
        value_prop: '3664069397'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlInn {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlInn {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlInn {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlInn {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlInn {...props} />);
        shouldCheckAllValidations(<ControlInn {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            min_chars: true,
            max_chars: false,
            is_inn_valid: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlInn {...props} />, flags);
    });
});