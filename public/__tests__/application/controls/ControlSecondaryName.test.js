import React from 'react';
import ControlSecondaryName from '../../../js/application/react/controls/ControlSecondaryName';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlSecondaryName', () => {
    const props = {
        label: 'Отчество',
        name: 'secondary_name',
        placeholder: 'Введите отчество',
        value_prop: 'Иванович'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlSecondaryName {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlSecondaryName {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlSecondaryName {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlSecondaryName {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckAllValidations(<ControlSecondaryName {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            cyrillic_only: false,
            min_words: false,
            max_words: false,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlSecondaryName {...props} />, flags);
    });
});