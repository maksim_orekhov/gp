import React from 'react';

export const sendUpValidationOnReceivePropsTest = (TestingComponent, props) => {
    const handleComponentValidation = jest.fn();

    const component = shallow(TestingComponent);

    component.setProps({value_prop: ''});
    component.setProps({handleComponentValidation});
    // Меняем значение value_prop
    component.setProps({value_prop: props.value_prop});
    // Ожидаем что сработает функция handleComponentValidation
    expect(handleComponentValidation).lastCalledWith(`${props.name}_is_valid`, true);
};

export const sendUpValidationWhenInputChangeTest = (TestingComponent, props, is_mount) => {
    const handleComponentValidation = jest.fn();

    let component;
    if (is_mount) {
        component = mount(TestingComponent);
    } else {
        component = shallow(TestingComponent);
    }

    component.setProps({handleComponentValidation});
    // Симулируем изменение инпута
    component.find('input').simulate('change', {target: {value: props.value_prop}});
    // Ожидаем что сработают функции у компонента родителя
    expect(handleComponentValidation).lastCalledWith(`${props.name}_is_valid`, true);
};

export const didComponentChangeWhenInputChangeTest = (TestingComponent, props, is_mount) => {
    const handleComponentChange = jest.fn();

    let component;
    if (is_mount) {
        component = mount(TestingComponent);
    } else {
        component = shallow(TestingComponent);
    }

    component.setProps({handleComponentChange});
    // Симулируем изменение инпута
    component.find('input').simulate('change', {target: {value: props.value_prop}});
    // Ожидаем что сработают функции у компонента родителя
    expect(handleComponentChange).lastCalledWith(props.name, props.value_prop);
};
