import React from 'react';

export const shouldCheckEmptyValue = (TestingComponent) => {
    const component = shallow(TestingComponent);

    expect(component.instance().checkAllValidations('')).toBe(false); // пустое значение
};

export const shouldCheckAllValidations = (TestingComponent, props) => {
    const component = shallow(TestingComponent);

    expect(component.instance().checkAllValidations(props.value_prop)).toBe(true); // валидный вариант
};
