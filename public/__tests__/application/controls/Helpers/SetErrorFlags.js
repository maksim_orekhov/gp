import React from 'react';

export const setCorrectErrorFlags = (TestingComponent, flags) => {
    const component = shallow(TestingComponent);

    // пустое значение
    component.instance().updateErrors('');
    expect(component.state().validation_errors).toMatchObject(flags);
};