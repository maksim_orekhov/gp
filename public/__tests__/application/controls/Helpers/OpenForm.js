import React from 'react';

export const isDropdownOpen = (TestingComponent) => {
    const component = shallow(TestingComponent);

    component.find('ul').simulate('click');

    expect(component.state().isDropDownOpen).toEqual(true);
};
