import React from 'react';

export const getActiveTab = (TestingComponent, props) => {
    const component = shallow(TestingComponent);
    component.setProps({default_option: props.default_option});
    component.setProps({active_tab: props.active_tab});

    expect(component.instance().getActiveTab(props.default_option, props.active_tab)).toBe('Счет ВТБ');
};

export const getActiveTabNds = (TestingComponent, props) => {
    const component = shallow(TestingComponent);
    component.setProps({nds_types: props.nds_types});
    component.setProps({value_prop: props.value_prop});

    expect(component.instance().getActiveTab(props.nds_types, props.value_prop)).toBe(0);
};

export const getActiveTabPaymentMethod = (TestingComponent, props) => {
    const component = shallow(TestingComponent);
    component.setProps({payment_methods: props.payment_methods});
    component.setProps({value_prop: props.value_prop});

    expect(component.instance().getActiveTab(props.nds_types, props.value_prop)).toBe('Счет ВТБ');
};
