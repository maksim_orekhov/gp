import React from 'react';
import ControlPhoneConfirmCode from '../../../js/application/react/controls/ControlPhoneConfirmCode';
import { sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlPhoneConfirmCode', () => {
    const props = {
        name: 'code',
        placeholder: 'Код подтверждения',
        value_prop: 1000
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlPhoneConfirmCode {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlPhoneConfirmCode {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlPhoneConfirmCode {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlPhoneConfirmCode {...props} />);
        shouldCheckAllValidations(<ControlPhoneConfirmCode {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlPhoneConfirmCode {...props} />, flags);
    });
});