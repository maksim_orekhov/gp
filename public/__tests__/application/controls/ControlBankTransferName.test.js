import React from 'react';
import ControlBankTransferName from '../../../js/application/react/controls/ControlBankTransferName';
import { sendUpValidationOnReceivePropsTest, sendUpValidationWhenInputChangeTest, didComponentChangeWhenInputChangeTest } from './Helpers/SendUpValidation';
import { shouldCheckAllValidations, shouldCheckEmptyValue } from './Helpers/CheckAllValidation';
import { setCorrectErrorFlags } from './Helpers/SetErrorFlags';

describe('ControlBankTransferName', () => {
    const props = {
        label: 'Название шаблона платежа',
        name: 'name',
        placeholder: 'Введите название шаблона платежа',
        value_prop: 'Основной метод оплаты'
    };

    it('should correct snapshot render', () => {
        const component = shallow(<ControlBankTransferName {...props} />);
        expect(shallowToJson(component)).toMatchSnapshot();
    });

    it('should sends up validation when receive new value from parent component', () => {
        sendUpValidationOnReceivePropsTest(<ControlBankTransferName {...props} />, props);
    });

    it('should sends up value and validation when input change value', () => {
        sendUpValidationWhenInputChangeTest(<ControlBankTransferName {...props} />, props);
        didComponentChangeWhenInputChangeTest(<ControlBankTransferName {...props} />, props);
    });

    it('should correct works component checkAllValidations', () => {
        shouldCheckEmptyValue(<ControlBankTransferName {...props} />);
        shouldCheckAllValidations(<ControlBankTransferName {...props} />, props);
    });

    it('should correct sets errors flags', () => {
        const flags = {
            isEmpty: true,
            min_chars: true,
            max_chars: false,
            undefined_error: false
        };
        setCorrectErrorFlags(<ControlBankTransferName {...props} />, flags);
    });
});