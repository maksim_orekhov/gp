import Enzyme, { shallow, render, mount } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-15';
import 'isomorphic-fetch';
import VALIDATION_RULES from '../js/application/react/constants/validation_rules';
import * as Helpers from '../js/application/react/Helpers';
import URLS from '../js/application/react/constants/urls';
import REQUEST_CODES from '../js/application/react/constants/request_codes';

// React 15 Enzyme adapter
Enzyme.configure({ adapter: new Adapter() });


// Заменяем ajax запросы фейками
const customFetch = () => new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve({
            status: 'SUCCESS',
            data: {}
        });
    }, 100);
});

Helpers.customFetch = customFetch;

global.Helpers = Helpers;
global.URLS = URLS;
global.REQUEST_CODES = REQUEST_CODES;

// Make Enzyme functions available in all test files without importing
global.shallow = shallow;
global.render = render;
global.mount = mount;
global.shallowToJson = shallowToJson;
global.VALIDATION_RULES = VALIDATION_RULES;