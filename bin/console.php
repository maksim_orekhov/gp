<?php
use Application\Service\DeliveryManager;
use Core\Service\TwigRenderer;
use ModuleDelivery\Service\DeliveryOrderManager;
use ModuleDeliveryDpd\Service\DeliveryDpdStatusManager;
use ModuleDeliveryDpd\Service\DeliveryOrderDpdManager;
use Zend\Console\Console;
use Zend\ServiceManager\ServiceManager;
use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\Glob;
use ZF\Console\Application;
use ZF\Console\Dispatcher;
use \ZendTwig\Extension\Extension;

require_once __DIR__ . '/../vendor/autoload.php'; // Composer autoloader

$configuration = [];
foreach (Glob::glob(__DIR__.'/../config/{{*}}{{,*.local}}.php', Glob::GLOB_BRACE) as $file) {
    $configuration = ArrayUtils::merge($configuration, include $file);
}
foreach (Glob::glob(__DIR__.'/../config/autoload/{{,*.}global,{,*.}local}.php', Glob::GLOB_BRACE) as $file) {
    $configuration = ArrayUtils::merge($configuration, include $file);
}
if (isset($configuration['module_listener_options'])) {
    $configuration['module_listener_options']['config_cache_enabled'] = false;
    $configuration['module_listener_options']['module_map_cache_enabled'] = false;
}

// Prepare the service manager
$smConfig = $configuration['service_manager'] ?? [];
$smConfig = new \Zend\Mvc\Service\ServiceManagerConfig($smConfig);

$serviceManager = new ServiceManager();
$smConfig->configureServiceManager($serviceManager);
$serviceManager->setService('ApplicationConfig', $configuration);
// Load modules
$serviceManager->get('ModuleManager')->loadModules();
$config = $serviceManager->get('config');

//init twig extension
$env = $serviceManager->get(\Twig_Environment::class);
$renderer = $serviceManager->get(TwigRenderer::class);
$extension = new Extension($serviceManager, $renderer);
$env->addExtension($extension);

$routes = [
    [
        // Отслеживание статусов доставки посылок, отправленных сервисом DPD
        'name' => 'delivery dpd statuses update', // вызывать не чаще, чем раз в 5 минут!
        'route' => 'delivery dpd statuses update',
        'description' => 'Receiving and processing the status of parcels changed since the previous request',
        'short_description' => 'Delivery DPD statuses updating',
//        'options_descriptions' => [
//            'foo'   => 'Lorem Ipsum',
//        ],
//        'defaults' => [
//            'foo'   => 'bar',
//        ],
        'handler' => function($route, $console) use ($serviceManager) {
            $deliveryDpdStatusManager = $serviceManager->get(DeliveryDpdStatusManager::class);
            $deliveryManager = $serviceManager->get(DeliveryManager::class);
            $handler = new \ModuleDeliveryDpd\Command\DeliveryDpdStatusesUpdateCommand(
                $deliveryDpdStatusManager,
                $deliveryManager
            );
            return $handler($route, $console);
        }
    ],
    [
        // Изменение статусов создания заказа на доставку в системе DPD (рекомендуется вызывать 1 раз в 15 минут)
        'name' => 'delivery dpd order creation statuses update', // или delivery order creation statuses update
        // @TODO
        // Код в методе updateDpdOrderCreationStatus (DeliveryDpdManager)
        // или для общего случая - updateServiceOrderCreationStatus (DeliveryOrderManager)
        'route' => 'delivery dpd order creation statuses update',
        'description' => 'Checking and updating dpd orders with pending statuses',
        'short_description' => 'Delivery DPD statuses updating',
        'handler' => function($route, $console) use ($serviceManager) {
            $deliveryOrderManager = $serviceManager->get(DeliveryOrderManager::class);
            $deliveryOrderDpdManager = $serviceManager->get(DeliveryOrderDpdManager::class);
            $deliveryManager = $serviceManager->get(DeliveryManager::class);
            $handler = new \ModuleDeliveryDpd\Command\DeliveryDpdOrderCreationStatusesUpdateCommand(
                $deliveryOrderManager,
                $deliveryOrderDpdManager,
                $deliveryManager
            );
            return $handler($route, $console);
        }

    ]
];

Console::overrideIsConsole(false);
$application = new Application(
    $config['app'],
    $config['version'],
    $routes,
    Console::getInstance(detectConsoleAdapter()),
    new Dispatcher()
);

$exit = $application->run();
exit($exit);

// Try to detect best instance for console
function detectConsoleAdapter()
{
    if (Console::isWindows()) {
        if (Console::isAnsicon()) {
            $className = 'WindowsAnsicon';
        } else {
            $className = 'Windows';
        }

        return $className;
    }

    // Default is a Posix console
    $className = 'Posix';

    return $className;
}