<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

use Doctrine\DBAL\Driver\PDOMySql\Driver as PDOMySqlDriver;
use Zend\Session;
use Zend\Cache\Storage\Adapter\Filesystem;

return [
    'recaptcha_v2' => [
        'site_key' => '6LfrLmoUAAAAAERnLjMKJOR3gUypG-ATg9ebo3nf',
        'secret_key' => '6LfrLmoUAAAAAP-WU-BNSnQOFgUQxXXKE1ReCx8U',
        'bypass' => false
    ],
    'recaptcha_v3' => [
        'site_key' => '6LdmxGkUAAAAAOv1ZLqIWDFQlFFhmkdE-6m9oWuZ',
        'secret_key' => '6LdmxGkUAAAAAPVNFB9b0TjrxLBAtcpZTrFiDgFa'
    ],
    // Main url
    'main_project_url' => 'http://guarantpay.ru/',
    // Main host
    'main_project_host' => 'guarantpay.ru',
    // $_SERVER['DOCUMENT_ROOT'] replacement for Unit Test
    'server_document_root' => '',
    // Admin email
    'admin_email' => 'test@simple-technology.ru',
    'dependencies' => [
        'factories' => [
            ModuleCode\Api\CodeController::class => ModuleCode\Api\Factory\CodeControllerFactory::class
        ],
    ],
    'service_manager' => [
        'abstract_factories' => [
            Zend\Navigation\Service\NavigationAbstractServiceFactory::class,
        ],
    ],
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => PDOMySqlDriver::class,
                'params' => [ //@TODO Прописать тут данные боевого сервера. Параметры для локального - в local.php
                    'host'      => '127.0.0.1',
                    'user'      => 'root',
                    'password'  => '',
                    'dbname'    => 'guarant_pay',
                    'charset'   => 'UTF8',
                    'driverOptions'	=> [
                        1002 => "SET NAMES 'UTF8' COLLATE 'utf8_unicode_ci'",
                        // \PDO::MYSQL_ATTR_LOCAL_INFILE
                        1001 => true
                    ]
                ],
            ],
        ],
        'configuration' => [
            'orm_default' => array(
                'datetime_functions' => [
                    'strtodate' => 'DoctrineExtensions\Query\Mysql\StrToDate',
                ]
            )
        ],
        // migrations configuration
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => 'data/Migrations',
                'name'      => 'Doctrine Database Migrations',
                'namespace' => 'Migrations',
                'table'     => 'migrations',
            ],
        ],
    ],
    // Sessions
    'session_config' => [
        'name' => 'guarant-pay',
        // Default cookie session lifetime (Срок действия cookie сессии истечет через 1 час)
        'cookie_lifetime' => 60*60,
        // Данные сессии будут храниться на сервере до 30 дней.
        'gc_maxlifetime'     => 60*60*24*30,
    ],
    // Настройка менеджера сессий.
    'session_manager' => [
        // Валидаторы сессии (используются для безопасности).
        'validators' => [
            Session\Validator\RemoteAddr::class,
            #Session\Validator\HttpUserAgent::class //@TODO закоментил согласно GP-1721
        ]
    ],
    'session_containers' => [
        'ContainerNamespace'
    ],
    // Настройка хранилища сессий.
    'session_storage' => [
        'type' => Session\Storage\SessionArrayStorage::class
    ],
    // Cookie session lifetime with "remember me"
    'cookie_lifetime_with_remember_me' => 60*60*24*30,

    // Tokens length and validity config // Время только в секундах!
    'tokens' => [ // 3600 = 60 минут
        'deal_invitation_request_period'            => 30, // 30 секунд - максимальная частота запроса кода
        // Phone confirmation code
        #'phone_confirmation_code_length'            => 6, // 6 символов - Прописан константой в CodeController (SMS_CODE_LENGTH = 6)
        'max_number_of_specific_code_for_user'      => 5, // Лимит кодов для одного типа операции
        'confirmation_code_expiration_time'         => 60*15, // 15 минут - время действия кода
        'confirmation_code_request_period'          => 60, // 60 секунд - максимальная частота запроса кода
        // Email confirmation
        'email_confirmation_token_length'           => 8, // 8 символов
        'email_confirmation_token_request_period'   => 60, // 60 секунд - максимальная частота смены email
        'email_confirmation_token_expiration_time'  => 3600*24, // 24 часа - время действия токена
        // Password resetting
        'password_resetting_token_length'           => 8, // 8 символов
        'password_resetting_token_request_period'   => 60, // 60 секунд - максимальная частота смены password
        'password_resetting_token_expiration_time'  => 3600*24, // 24 часа - время действия токена
        // Encryption Key
        'token_encryption_key'                      => '0yV8fvEaG43fA', // Key for JWToken encryption
        // List on controller's name, that do not use encoded token validation
        //@TODO Проверить нужен ли? Использовали в коде для токенов header
        'whitelist_of_controllers' => [
            'Application\Controller\IndexController',
        ],
        'guarant_pay_login_cookie_lifetime'         => 3600*24, // 24 часа
    ],
    'mail_options' => [
        'name'              => 'smtp.guarantpay.ru',
        'host'              => 'smtp.guarantpay.ru',
        'port'              => 25025, // (альтернативный 25)
        'connection_class'  => 'login',
        'connection_config' => [
            'username'      => 'noreply@guarantpay.ru',
            'password'      => 'j3A4y29We5',
        ],
        'from_email'    => 'noreply@guarantpay.ru',
        'from_name'     => 'Guarant Pay'
    ],
    // Cache config
    'caches' => [
        'FilesystemCache' => [
            'adapter' => [
                'name'    => Filesystem::class,
                'options' => [
                    // Store cached data in this directory.
                    'cache_dir' => './data/cache',
                    // Store cached data for 1 hour.
                    'ttl' => 60*60*1
                ],
            ],
            'plugins' => [
                [
                    'name' => 'serializer',
                    'options' => [
                    ],
                ],
            ],
        ],
    ],
    // File management
    'file-management' => [
        'main_upload_folder' => 'upload',
    ],
    // Pagination
    'default_pagination_preferences' => [
        'start'             => 0,       // начальная точка
        'number_per_page'   => 20,      // кол-во на странице
        'sorting_order'     => 'DESC'   // порядок
    ],
    // PDF
    'pdf' => [
        'file_folder' => './data/files/pdf',
        'main_logo' => './data/files/pdf/main_logo.png',
        'signature' => './data/files/pdf/signature.png',
        'seal' => './data/files/pdf/seal.png'
    ],
    // Various exceptions type hide
    'hide_exceptions' => [
        'session_validation_failed' => true, // 'Session validation failed' hide
    ],
    'deal_notification' => [
        //приглашение на новую сделку
        'invitation_to_deal' => [
            'email'=> true,
            'sms'=> false
        ],
        //изменения в сделке
        'changed_deal' => [
            'email'=> true,
            'sms'=> false
        ],
        //пользователь согласился со сделкой
        'confirmed_agent' => [
            'email'=> true,
            'sms'=> false
        ],
        //достигнуто соглашение посделке
        'confirmed_deal' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомление об оплате в полном объеме
        'payment_receipt_full' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомление об оплате в полном объеме через мандарин
        'mandarin_payment_receipt_full' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомление об оплате в частичном объеме
        'payment_receipt_part' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомление о добавлении трек номера
        'tracking_number' => [ // Возможно, уже не нужно
            'email'=> true,
            'sms'=> false
        ],
        //уведомление о подтверждении доставки
        'delivery_confirm' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомление об открытии спора
        'open_dispute' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомление о закрытии спора кастомером
        'customer_closed_dispute' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомление о закрытии спора и продление периода гарантии
        'closed_dispute_with_extension' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомление о закрытии спора без продление периода гарантии
        'closed_dispute_without_extension' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомление о предоставлении скидки
        'discount_provided' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомление о возврате средств
        'refund_provided' => [
            'email'=> true,
            'sms'=> false
        ],
        //Спор передан в третейский суд
        'referred_to_tribunal' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить о приближении дедлайна
        'near_deadline' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить о запросе скидки
        'discount_request' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить о запросе возврата
        'refund_request' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить о запросе арбитражной комиссии
        'arbitrage_request' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить о запросе суда
        'tribunal_request' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить об ответе на запрос скидки
        'discount_confirm' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить об ответе на запрос возврата
        'refund_confirm' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить об ответе на запрос арбитражной комиссии
        'arbitrage_confirm' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить об ответе на запрос суда
        'tribunal_confirm' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить о закрытии сделки
        'deal_closed' => [
            'email'=> true,
            'sms'=> false
        ],
        // @TODO Новые, созданные в рамках GP-1995 / Добавить на test и prod
        //уведомить о создании трекинг номера
        'delivery_tracking_number_creation' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить об успешной доставке
        'delivery_successful' => [
            'email'=> true,
            'sms'=> false
        ],
        //уведомить о неуспешной доставке
        'delivery_unsuccessful' => [
            'email'=> true,
            'sms'=> false
        ],
    ],
    // @TODO Новые, созданные в рамках GP-1995 / Добавить на test и prod
    'app' => 'guarant-pay project',
    'version' => '1.0.0'
];
