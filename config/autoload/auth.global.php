<?php

return [
    'auth_service' => [
        'provider' => 'ModuleAuthV2',
    ],
    'service_manager' => [
        'aliases' => [
            'ModuleAuth' => ModuleAuth\Provider\AuthServiceProvider::class,
            'ModuleAuthV2' => ModuleAuthV2\Provider\AuthServiceProvider::class,
        ],
    ],
];