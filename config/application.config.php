<?php
/**
 * If you need an environment-specific system or application configuration,
 * there is an example in the documentation
 * @see https://docs.zendframework.com/tutorials/advanced-config/#environment-specific-system-configuration
 * @see https://docs.zendframework.com/tutorials/advanced-config/#environment-specific-application-configuration
 */

#******************************************************#
// режим удаленного сервера (server_test/server_prod)
\defined('REMOTE_SERVER_MODE') or \define('REMOTE_SERVER_MODE', 'server_test'); // Переключать тут!
#******************************************************#

$environment = 'production';
if (file_exists('config/development.config.php')) {
    $environment = 'development';
} elseif (REMOTE_SERVER_MODE === 'server_test') {
    $environment = 'testing';
}

\defined('APP_ENV') or \define('APP_ENV', $environment);

\defined('APP_ENV_DEV') or \define('APP_ENV_DEV', APP_ENV === 'development');
\defined('APP_ENV_PROD') or \define('APP_ENV_PROD', APP_ENV === 'production');
\defined('APP_ENV_TEST') or \define('APP_ENV_TEST', APP_ENV === 'testing');
\defined('APP_ENV_UNIT') or \define('APP_ENV_UNIT', APP_ENV === 'unit');

putenv('APP_ENV='.(\getenv('APP_ENV') ?: APP_ENV));

return [
    'version' => '10102018',
    // Retrieve list of modules used in this application.
    'modules' => require __DIR__ . '/modules.config.php',

    // These are various options for the listeners attached to the ModuleManager
    'module_listener_options' => [
        // This should be an array of paths in which modules reside.
        // If a string key is provided, the listener will consider that a module
        // namespace, the value of that key the specific path to that module's
        // Module class.
        'module_paths' => [
            './module',
            './vendor',
        ],

        // An array of paths from which to glob configuration files after
        // modules are loaded. These effectively override configuration
        // provided by modules themselves. Paths may use GLOB_BRACE notation.
        'config_glob_paths' => [
            realpath(__DIR__) . '/autoload/{{,*.}global,{,*.}local}.php',
            realpath(__DIR__) . '/environment/' . (\getenv('APP_ENV') ?: 'production') . '.config.php',
        ],

        // Whether or not to enable a configuration cache.
        // If enabled, the merged configuration will be cached and used in
        // subsequent requests.
        'config_cache_enabled' => true,

        // The key used to create the configuration cache file name.
        'config_cache_key' => 'application.config.cache',

        // Whether or not to enable a module class map cache.
        // If enabled, creates a module class map cache which will be used
        // by in future requests, to reduce the autoloading process.
        'module_map_cache_enabled' => true,

        // The key used to create the class map cache file name.
        'module_map_cache_key' => 'application.module.cache',

        // The path in which to cache merged configuration.
        'cache_dir' => 'data/cache/',

        // Whether or not to enable modules dependency checking.
        // Enabled by default, prevents usage of modules that depend on other modules
        // that weren't loaded.
        // 'check_dependencies' => true,
    ],
    // Tests
    'tests' => [
        'user_login'    => 'test',
        'user_password' => 'qwerty',
        'user_email'    => 'test@guarantpay.com',
        'user_phone'    => '+79033463906'
    ]

    // Used to create an own service manager. May contain one or more child arrays.
    // 'service_listener_options' => [
    //     [
    //         'service_manager' => $stringServiceManagerName,
    //         'config_key'      => $stringConfigKey,
    //         'interface'       => $stringOptionalInterface,
    //         'method'          => $stringRequiredMethodName,
    //     ],
    // ],

    // Initial configuration with which to seed the ServiceManager.
    // Should be compatible with Zend\ServiceManager\Config.
    // 'service_manager' => [],
];
