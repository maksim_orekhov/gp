<?php
// Для боевого проекта в режиме production
return [
    'view_manager' => [
        'display_exceptions' => false,
    ],
    // Mail configurations
    'email_providers' => [
        'message_send_simulation' => false,
    ],
    'sms_providers' => [
        'message_send_simulation' => false,
    ],

    // Mandarin
    'mandarin' => [
        'merchant_id'   => '940',
        'secret_key'    => 'Cm0uUN6L0H',
    ],
    // Доставка DPD
    'delivery_dpd_settings' => [
        // !Important Боевой!
        'gateway_host' => 'http://ws.dpd.ru/services/', // обычный сервер
    ],
];
