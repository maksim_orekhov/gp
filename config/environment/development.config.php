<?php
// Для локальных проектов в режиме development
return [
    'view_manager' => [
        'display_exceptions' => true,
    ],
    'sms_providers' => [
        'message_send_simulation' => true, // Turns on the simulation mode in dev
    ],
    'email_providers' => [
        'message_send_simulation' => false, // Turns on the simulation mode in dev
    ],
    // Various exceptions type hide
    'hide_exceptions' => [
        'session_validation_failed' => true, // 'Session validation failed' show
    ],

    // Mandarin
    'mandarin' => [
        'merchant_id' => '216',
        'secret_key'  => '123321'
    ],
    // Доставка DPD
    'delivery_dpd_settings' => [
        'gateway_host' => 'http://wstest.dpd.ru/services/', // тестовый сервер
    ],
];