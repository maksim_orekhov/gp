<?php
// Для удаленных тестовых проектов в режиме production
return [
    'module_listener_options' => [
        'config_cache_enabled' => false,
        'module_map_cache_enabled' => false,
    ],
    'view_manager' => [
        'display_exceptions' => false,
    ],
    'sms_providers' => [
        'message_send_simulation' => false,
    ],
    'email_providers' => [
        'message_send_simulation' => false,
    ],
    'auth_service' => \getenv('AUTH_PROVIDER_ENV') ? ['provider' => \getenv('AUTH_PROVIDER_ENV')] : [],

    // Mandarin
    'mandarin' => [
        'merchant_id' => '216',
        'secret_key'  => '123321'
    ],
    // Доставка DPD
    'delivery_dpd_settings' => [
        'gateway_host' => 'http://wstest.dpd.ru/services/', // тестовый сервер
    ],
];