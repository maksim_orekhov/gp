# GuarantPay Alpha

## Introduction

Alpha-версия проекта guarantpay.com.


## Modules

- Core
- Application
- ModuleCode
- ModuleFileManager
- ModuleRbac


## Installation

Устанавливаем [Composer](https://getcomposer.org/).

В папке, где нужно клонировать проект, в composer запускаем команду клонирования
```bash
$ git clone ssh://git@jira.simple-tech.org:7999/guar/dev.git
```
Вручную создаем рабочие копии dist файлов (в папках конфигурации).
Некоторые копируются сами при переключении а Development mode (см. ниже).


## Database

С помощью [PhpMyAdmin](http://localhost/phpmyadmin/) создаем базу данных **guarant_pay**
(utf8_unicode_ci).

Подробнее о коммандах для работы с базой данных - в нашем [wiki](http://wiki.simple-tech.org/index.php/%D0%91%D0%B0%D0%B7%D0%B0_%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85).

#### Configuration
Важно! Убедиться, что в параметрах кофигурации доктрины (global.php, блок 'doctrine') присутсвует параметр:
```bash
'driverOptions'	=> [
    '1002' => "SET NAMES 'UTF8' COLLATE 'utf8_unicode_ci'"
]
```
Без него возможно проблемы с кодировкой при получении и записи данных в базу.


## Development mode

В режиме разработки включаем Development mode (если не включить до деплоя, не установятся dev-зависимости)
```bash
$ composer development-enable
```
Отключение Development mode (на production) и проверка
```bash
$ composer development-disable
$ composer development-status
```

## Deployment

### Развёртывание с помощью bash-скрипта

Предварительно проверить, есть ли в папке data папка **cache**. Если нет, создать eё.

В окне Composer запускаем команду (для Windows)
```bash
$ sh deploy.sh
```

Команда выполнит следующие действия:
- Запустит composer install, который загрузит зависимости бэка из composer.json
- Выполнит все миграции
- Установит фикстуры
- Запустит npm install, который установит зависимтости фронта из public/package.json
- Соберет фронт

### Развёртывание "вручную"

Если операции нужно выполнить отдельно, используем следующией комманды:

Установка зависисомтей из composer.json
```bash
$ composer install
```
Миграции
```bash
$ ./vendor/bin/doctrine-module migrations:migrate
```
Фикстуры
```bash
$ ./vendor/bin/doctrine-module orm:fixtures:load
```
Переходим в папку public
```bash
$ cd public
```
Устанавливаем зависимости из public/package.json
```bash
$ npm install
```
Собираем фронт
```bash
$ npm run start
```


Выход из режима пересборки фронта - Ctrl+C.

### System state rollback

Важно для тестирования работы Оператора с банк-клиентом

Если нужно откатить систему к первоначальному состоянию, запускаем команду:

```bash
$ sh rollback.sh
```

Команда с помощью фикстур вернет базу данных в исходное состояние и удалит все файлы из папок upload/bank-client (файлы платёжек) и upload/deal (контракты по сделкам).


## Users

В режиме разработки в системе присутствуют три предустановленных пользователя:
- **test** (роль Verified)
- **test2** (роль Verified)
- **operator** (роль Operator)

Пароль для всех один - **qwerty**


## RBAC - Role-Based Access Control

### Ограничители доступа

- ✱ (звездочка) - разрешается всем
- @ (коммерческое at) - разрешается авторизованным пользователям
- @identity (где identity - логин) - разрешается только конкретному авторизованному пользователю
- &#35;role - разрешается авторизованным пользователям с указанной ролью
- +permission.name - разрешаем авторизованным пользователям с привилегией "permission.name"

### Доступ к контроллерам

Параметры доступа к экшенам контроллеров прописываются в ключе **'access_filter'->'controllers'** конфигурационных файлов
**module.config.php** каждого модуля.

Например, для Controller\ProfileController:
```bash
# разрешить доступ с indexAction аутентифицированным пользователям с привилегией "profile.view"
['actions' => ['index'], 'allows' => ['+profile.view']]
```
или
```bash
# разрешить всем доступ к экшену(методу)"index"
['actions' => ['index'], 'allows' => ['*']]
```
или
```bash
# разрешить авторизованным пользователям доступ к экшену(методу) "index"
['actions' => ['index'], 'allows' => ['@']]
```

Параметры 'actions' и 'allows' обязательно должны быть массивами (не забываем ставить скобки [ ]
даже если там всего одно значение)!

В 'actions' может быть сколько угодно элементов
('actions' => ['index', 'show', 'edit', 'delete']).

В 'allows' тоже может быть сколько угодно элементов ('allows' => ['+profile.own.edit', '#Operator']).
За исключением случаев с '*' и '@'. Если эти значения есть в массиве 'allows',
то они должны быть единственными.

Более подробное описание RBAC (тонкая настройка, проверка прав из кода любого контроллера или шаблона и т.д.) -
в нашем [wiki](http://wiki.simple-tech.org/index.php/ACL_(%D0%B8%D0%BB%D0%B8_RBAC_-_Role-Based_Access_Control)).

## PDF

Для генерации PDF файлов (счетов на оплату, контрактов по сделкам и т.д.) требуются изображения (логотип, печати и т.д.).
Папка для таких файлов - ./data/files/pdf.
В конфиг (global.php, блок 'pdf') можно добавлять путь до конретного файла:
```bash
'pdf' => [
    'file_folder' => './data/files/pdf',
    'main_logo' => './data/files/pdf/main_logo.png'
]
```

Получение файла с использование конфига:
```bash
$logo = file_get_contents($this->config['pdf']['main_logo']);
```


## Unit Tests

Конфигурация - в файле phpunit.xml

Запускаем с помощь IDE PhpStorm. Подробно о настройке и использовании -
в тикете [GP-76](http://jira.simple-tech.org:8080/browse/GP-76)

Запуск тестов из консоли
```bash
$ ./vendor/bin/phpunit
```
Тесты конкретного класса (TestController)
```bash
$ ./vendor/bin/phpunit module/Application/test/Controller/TestControllerTest.php
```

Об экономии памяти при прогонке тестов - в нашем [wiki](http://wiki.simple-tech.org/index.php/Phpunit).


## Web server setup

### Apache setup

To setup apache, setup a virtual host to point to the public/ directory of the
project and you should be ready to go! It should look something like below:

```apache
<VirtualHost *:80>
    ServerName zfapp.localhost
    DocumentRoot /path/to/zfapp/public
    <Directory /path/to/zfapp/public>
        DirectoryIndex index.php
        AllowOverride All
        Order allow,deny
        Allow from all
        <IfModule mod_authz_core.c>
        Require all granted
        </IfModule>
    </Directory>
</VirtualHost>
```

### Nginx setup

To setup nginx, open your `/path/to/nginx/nginx.conf` and add an
[include directive](http://nginx.org/en/docs/ngx_core_module.html#include) below
into `http` block if it does not already exist:

```nginx
http {
    # ...
    include sites-enabled/*.conf;
}
```


Create a virtual host configuration file for your project under `/path/to/nginx/sites-enabled/zfapp.localhost.conf`
it should look something like below:

```nginx
server {
    listen       80;
    server_name  zfapp.localhost;
    root         /path/to/zfapp/public;

    location / {
        index index.php;
        try_files $uri $uri/ @php;
    }

    location @php {
        # Pass the PHP requests to FastCGI server (php-fpm) on 127.0.0.1:9000
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_param  SCRIPT_FILENAME /path/to/zfapp/public/index.php;
        include fastcgi_params;
    }
}
```

Restart the nginx, now you should be ready to go!

## QA Tools

The skeleton does not come with any QA tooling by default, but does ship with
configuration for each of:

- [phpcs](https://github.com/squizlabs/php_codesniffer)
- [phpunit](https://phpunit.de)

Additionally, it comes with some basic tests for the shipped
`Application\Controller\IndexController`.

If you want to add these QA tools, execute the following:

```bash
$ composer require --dev phpunit/phpunit squizlabs/php_codesniffer zendframework/zend-test
```

We provide aliases for each of these tools in the Composer configuration:

```bash
# Run CS checks:
$ composer cs-check
# Fix CS errors:
$ composer cs-fix
# Run PHPUnit tests:
$ composer test
```
