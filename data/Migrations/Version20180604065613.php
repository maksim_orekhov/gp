<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180604065613 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE dispute_cycle (id INT AUTO_INCREMENT NOT NULL, dispute_id INT DEFAULT NULL, created DATETIME DEFAULT NULL, closed DATETIME DEFAULT NULL, INDEX IDX_2590F7A7C7B47CB5 (dispute_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dispute_cycle ADD CONSTRAINT FK_2590F7A7C7B47CB5 FOREIGN KEY (dispute_id) REFERENCES dispute (id)');
        $this->addSql('ALTER TABLE arbitrage_request ADD dispute_cycle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE arbitrage_request ADD CONSTRAINT FK_810D557911846ACE FOREIGN KEY (dispute_cycle_id) REFERENCES dispute_cycle (id)');
        $this->addSql('CREATE INDEX IDX_810D557911846ACE ON arbitrage_request (dispute_cycle_id)');
        $this->addSql('ALTER TABLE discount ADD dispute_cycle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE discount ADD CONSTRAINT FK_E1E0B40E11846ACE FOREIGN KEY (dispute_cycle_id) REFERENCES dispute_cycle (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E1E0B40E11846ACE ON discount (dispute_cycle_id)');
        $this->addSql('ALTER TABLE discount_request ADD dispute_cycle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE discount_request ADD CONSTRAINT FK_8D9FE28E11846ACE FOREIGN KEY (dispute_cycle_id) REFERENCES dispute_cycle (id)');
        $this->addSql('CREATE INDEX IDX_8D9FE28E11846ACE ON discount_request (dispute_cycle_id)');
        $this->addSql('ALTER TABLE payment ADD repaidOverpay_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D868B0C5D FOREIGN KEY (repaidOverpay_id) REFERENCES repaid_overpay (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6D28840D868B0C5D ON payment (repaidOverpay_id)');
        $this->addSql('ALTER TABLE refund ADD dispute_cycle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE refund ADD CONSTRAINT FK_5B2C145811846ACE FOREIGN KEY (dispute_cycle_id) REFERENCES dispute_cycle (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5B2C145811846ACE ON refund (dispute_cycle_id)');
        $this->addSql('ALTER TABLE refund_request ADD dispute_cycle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE refund_request ADD CONSTRAINT FK_652005DB11846ACE FOREIGN KEY (dispute_cycle_id) REFERENCES dispute_cycle (id)');
        $this->addSql('CREATE INDEX IDX_652005DB11846ACE ON refund_request (dispute_cycle_id)');
        $this->addSql('ALTER TABLE tribunal ADD dispute_cycle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tribunal ADD CONSTRAINT FK_DC8C3AAF11846ACE FOREIGN KEY (dispute_cycle_id) REFERENCES dispute_cycle (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DC8C3AAF11846ACE ON tribunal (dispute_cycle_id)');
        $this->addSql('ALTER TABLE tribunal_request ADD dispute_cycle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tribunal_request ADD CONSTRAINT FK_15D6272511846ACE FOREIGN KEY (dispute_cycle_id) REFERENCES dispute_cycle (id)');
        $this->addSql('CREATE INDEX IDX_15D6272511846ACE ON tribunal_request (dispute_cycle_id)');
        $this->addSql('ALTER TABLE warranty_extension ADD dispute_cycle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE warranty_extension ADD CONSTRAINT FK_3B47AA3B11846ACE FOREIGN KEY (dispute_cycle_id) REFERENCES dispute_cycle (id)');
        $this->addSql('CREATE INDEX IDX_3B47AA3B11846ACE ON warranty_extension (dispute_cycle_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE arbitrage_request DROP FOREIGN KEY FK_810D557911846ACE');
        $this->addSql('ALTER TABLE discount DROP FOREIGN KEY FK_E1E0B40E11846ACE');
        $this->addSql('ALTER TABLE discount_request DROP FOREIGN KEY FK_8D9FE28E11846ACE');
        $this->addSql('ALTER TABLE refund DROP FOREIGN KEY FK_5B2C145811846ACE');
        $this->addSql('ALTER TABLE refund_request DROP FOREIGN KEY FK_652005DB11846ACE');
        $this->addSql('ALTER TABLE tribunal DROP FOREIGN KEY FK_DC8C3AAF11846ACE');
        $this->addSql('ALTER TABLE tribunal_request DROP FOREIGN KEY FK_15D6272511846ACE');
        $this->addSql('ALTER TABLE warranty_extension DROP FOREIGN KEY FK_3B47AA3B11846ACE');
        $this->addSql('DROP TABLE dispute_cycle');
        $this->addSql('DROP INDEX IDX_810D557911846ACE ON arbitrage_request');
        $this->addSql('ALTER TABLE arbitrage_request DROP dispute_cycle_id');
        $this->addSql('DROP INDEX UNIQ_E1E0B40E11846ACE ON discount');
        $this->addSql('ALTER TABLE discount DROP dispute_cycle_id');
        $this->addSql('DROP INDEX IDX_8D9FE28E11846ACE ON discount_request');
        $this->addSql('ALTER TABLE discount_request DROP dispute_cycle_id');
        $this->addSql('ALTER TABLE payment DROP FOREIGN KEY FK_6D28840D868B0C5D');
        $this->addSql('DROP INDEX UNIQ_6D28840D868B0C5D ON payment');
        $this->addSql('ALTER TABLE payment DROP repaidOverpay_id');
        $this->addSql('DROP INDEX UNIQ_5B2C145811846ACE ON refund');
        $this->addSql('ALTER TABLE refund DROP dispute_cycle_id');
        $this->addSql('DROP INDEX IDX_652005DB11846ACE ON refund_request');
        $this->addSql('ALTER TABLE refund_request DROP dispute_cycle_id');
        $this->addSql('DROP INDEX UNIQ_DC8C3AAF11846ACE ON tribunal');
        $this->addSql('ALTER TABLE tribunal DROP dispute_cycle_id');
        $this->addSql('DROP INDEX IDX_15D6272511846ACE ON tribunal_request');
        $this->addSql('ALTER TABLE tribunal_request DROP dispute_cycle_id');
        $this->addSql('DROP INDEX IDX_3B47AA3B11846ACE ON warranty_extension');
        $this->addSql('ALTER TABLE warranty_extension DROP dispute_cycle_id');
    }
}
