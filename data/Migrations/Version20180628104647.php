<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180628104647 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sole_proprietor (id INT AUTO_INCREMENT NOT NULL, civil_law_subject_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, first_name VARCHAR(45) DEFAULT NULL, secondary_name VARCHAR(45) DEFAULT NULL, last_name VARCHAR(45) DEFAULT NULL, birth_date DATETIME DEFAULT NULL, inn VARCHAR(12) NOT NULL, UNIQUE INDEX UNIQ_C95CCC7684EF4BF2 (civil_law_subject_id), INDEX fk_sole_proprietor_civil_law_subject1_idx (civil_law_subject_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sole_proprietor ADD CONSTRAINT FK_C95CCC7684EF4BF2 FOREIGN KEY (civil_law_subject_id) REFERENCES civil_law_subject (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE sole_proprietor');
    }
}
