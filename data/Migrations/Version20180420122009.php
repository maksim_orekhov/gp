<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180420122009 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tribunal_confirm (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_A1920BFEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tribunal_request (id INT AUTO_INCREMENT NOT NULL, dispute_id INT DEFAULT NULL, deal_agent_id INT DEFAULT NULL, confirm_id INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_15D62725C7B47CB5 (dispute_id), INDEX IDX_15D62725D7936091 (deal_agent_id), UNIQUE INDEX UNIQ_15D627258B228CE7 (confirm_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tribunal_confirm ADD CONSTRAINT FK_A1920BFEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tribunal_request ADD CONSTRAINT FK_15D62725C7B47CB5 FOREIGN KEY (dispute_id) REFERENCES dispute (id)');
        $this->addSql('ALTER TABLE tribunal_request ADD CONSTRAINT FK_15D62725D7936091 FOREIGN KEY (deal_agent_id) REFERENCES deal_agent (id)');
        $this->addSql('ALTER TABLE tribunal_request ADD CONSTRAINT FK_15D627258B228CE7 FOREIGN KEY (confirm_id) REFERENCES tribunal_confirm (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tribunal_request DROP FOREIGN KEY FK_15D627258B228CE7');
        $this->addSql('DROP TABLE tribunal_confirm');
        $this->addSql('DROP TABLE tribunal_request');
    }
}
