<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180616091650 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE payment DROP FOREIGN KEY FK_6D28840D868B0C5D');
        $this->addSql('DROP INDEX UNIQ_6D28840D868B0C5D ON payment');
        $this->addSql('ALTER TABLE payment DROP repaidOverpay_id');
        $this->addSql('ALTER TABLE mandarin_payment_order CHANGE order_id order_id TEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE mandarin_payment_order CHANGE order_id order_id TEXT NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE payment ADD repaidOverpay_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D868B0C5D FOREIGN KEY (repaidOverpay_id) REFERENCES repaid_overpay (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6D28840D868B0C5D ON payment (repaidOverpay_id)');
    }
}
