<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180419103721 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE refund_confirm (id INT AUTO_INCREMENT NOT NULL, deal_agent_id INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_D1642900D7936091 (deal_agent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE refund_request (id INT AUTO_INCREMENT NOT NULL, dispute_id INT DEFAULT NULL, deal_agent_id INT DEFAULT NULL, confirm_id INT DEFAULT NULL, created DATETIME NOT NULL, INDEX IDX_652005DBC7B47CB5 (dispute_id), INDEX IDX_652005DBD7936091 (deal_agent_id), UNIQUE INDEX UNIQ_652005DB8B228CE7 (confirm_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE refund_confirm ADD CONSTRAINT FK_D1642900D7936091 FOREIGN KEY (deal_agent_id) REFERENCES deal_agent (id)');
        $this->addSql('ALTER TABLE refund_request ADD CONSTRAINT FK_652005DBC7B47CB5 FOREIGN KEY (dispute_id) REFERENCES dispute (id)');
        $this->addSql('ALTER TABLE refund_request ADD CONSTRAINT FK_652005DBD7936091 FOREIGN KEY (deal_agent_id) REFERENCES deal_agent (id)');
        $this->addSql('ALTER TABLE refund_request ADD CONSTRAINT FK_652005DB8B228CE7 FOREIGN KEY (confirm_id) REFERENCES refund_confirm (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE refund_request DROP FOREIGN KEY FK_652005DB8B228CE7');
        $this->addSql('DROP TABLE refund_confirm');
        $this->addSql('DROP TABLE refund_request');
    }
}
