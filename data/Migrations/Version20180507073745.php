<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180507073745 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE repaid_overpay (id INT AUTO_INCREMENT NOT NULL, payment_method_id INT DEFAULT NULL, payment_id INT DEFAULT NULL, created DATETIME NOT NULL, amount NUMERIC(10, 2) NOT NULL, UNIQUE INDEX UNIQ_EDA208C25AA1164F (payment_method_id), UNIQUE INDEX UNIQ_EDA208C24C3A3BB (payment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE repaid_overpay_payment_order (repaid_overpay_id INT NOT NULL, payment_order_id INT NOT NULL, INDEX IDX_CFB2EF046BCF3F (repaid_overpay_id), UNIQUE INDEX UNIQ_CFB2EF0CD592F50 (payment_order_id), PRIMARY KEY(repaid_overpay_id, payment_order_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE repaid_overpay ADD CONSTRAINT FK_EDA208C25AA1164F FOREIGN KEY (payment_method_id) REFERENCES payment_method (id)');
        $this->addSql('ALTER TABLE repaid_overpay ADD CONSTRAINT FK_EDA208C24C3A3BB FOREIGN KEY (payment_id) REFERENCES payment (id)');
        $this->addSql('ALTER TABLE repaid_overpay_payment_order ADD CONSTRAINT FK_CFB2EF046BCF3F FOREIGN KEY (repaid_overpay_id) REFERENCES repaid_overpay (id)');
        $this->addSql('ALTER TABLE repaid_overpay_payment_order ADD CONSTRAINT FK_CFB2EF0CD592F50 FOREIGN KEY (payment_order_id) REFERENCES payment_order (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE repaid_overpay_payment_order DROP FOREIGN KEY FK_CFB2EF046BCF3F');
        $this->addSql('DROP TABLE repaid_overpay');
        $this->addSql('DROP TABLE repaid_overpay_payment_order');
    }
}
