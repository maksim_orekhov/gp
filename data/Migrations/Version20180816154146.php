<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180816154146 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bank_client_payment_order CHANGE amount amount NUMERIC(12, 2) NOT NULL');
        $this->addSql('ALTER TABLE mandarin_payment_order CHANGE amount amount NUMERIC(12, 2) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bank_client_payment_order CHANGE amount amount DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE mandarin_payment_order CHANGE amount amount NUMERIC(10, 2) NOT NULL');
    }
}
