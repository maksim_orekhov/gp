<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180615144537 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE mandarin_payment_order CHANGE order_id order_id TEXT DEFAULT NULL, CHANGE transaction transaction VARCHAR(64) DEFAULT NULL, CHANGE customer_email customer_email VARCHAR(64) DEFAULT NULL, CHANGE customer_phone customer_phone VARCHAR(64) DEFAULT NULL, CHANGE card_holder card_holder VARCHAR(64) DEFAULT NULL, CHANGE card_number card_number VARCHAR(16) DEFAULT NULL, CHANGE card_expiration_year card_expiration_year VARCHAR(2) DEFAULT NULL, CHANGE card_expiration_month card_expiration_month VARCHAR(2) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE mandarin_payment_order CHANGE order_id order_id TEXT NOT NULL COLLATE utf8_unicode_ci, CHANGE transaction transaction VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, CHANGE customer_email customer_email VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, CHANGE customer_phone customer_phone VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, CHANGE card_holder card_holder VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, CHANGE card_number card_number VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci, CHANGE card_expiration_year card_expiration_year VARCHAR(2) NOT NULL COLLATE utf8_unicode_ci, CHANGE card_expiration_month card_expiration_month VARCHAR(2) NOT NULL COLLATE utf8_unicode_ci');
    }
}
