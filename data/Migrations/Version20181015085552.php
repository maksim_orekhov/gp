<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181015085552 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE token (id INT AUTO_INCREMENT NOT NULL, beneficiar_user_id INT DEFAULT NULL, beneficiar_civil_law_subject_id INT DEFAULT NULL, beneficiar_payment_method_id INT DEFAULT NULL, counteragent_user_id INT DEFAULT NULL, counteragent_civil_law_subject_id INT DEFAULT NULL, counteragent_payment_method_id INT DEFAULT NULL, deal_type_id INT DEFAULT NULL, fee_payer_option_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, deal_role VARCHAR(10) DEFAULT NULL, partner_ident VARCHAR(60) DEFAULT NULL, created DATETIME NOT NULL, is_active TINYINT(1) DEFAULT NULL, is_deleted TINYINT(1) DEFAULT \'0\', counteragent_email VARCHAR(45) DEFAULT NULL, deal_name VARCHAR(255) DEFAULT NULL, deal_description TEXT DEFAULT NULL, amount DOUBLE PRECISION DEFAULT NULL, delivery_period SMALLINT DEFAULT NULL, token_key VARCHAR(60) NOT NULL, UNIQUE INDEX UNIQ_5F37A13B1772D4D7 (partner_ident), UNIQUE INDEX UNIQ_5F37A13B53B816F5 (token_key), INDEX IDX_5F37A13BB56D3CA8 (beneficiar_user_id), INDEX IDX_5F37A13B44026DEE (beneficiar_civil_law_subject_id), INDEX IDX_5F37A13B5CEDDBF9 (beneficiar_payment_method_id), INDEX IDX_5F37A13BB39EF43C (counteragent_user_id), INDEX IDX_5F37A13BDB415B71 (counteragent_civil_law_subject_id), INDEX IDX_5F37A13B858674A9 (counteragent_payment_method_id), INDEX IDX_5F37A13B2156041B (deal_type_id), INDEX IDX_5F37A13BDB13039A (fee_payer_option_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT FK_5F37A13BB56D3CA8 FOREIGN KEY (beneficiar_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT FK_5F37A13B44026DEE FOREIGN KEY (beneficiar_civil_law_subject_id) REFERENCES civil_law_subject (id)');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT FK_5F37A13B5CEDDBF9 FOREIGN KEY (beneficiar_payment_method_id) REFERENCES payment_method (id)');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT FK_5F37A13BB39EF43C FOREIGN KEY (counteragent_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT FK_5F37A13BDB415B71 FOREIGN KEY (counteragent_civil_law_subject_id) REFERENCES civil_law_subject (id)');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT FK_5F37A13B858674A9 FOREIGN KEY (counteragent_payment_method_id) REFERENCES payment_method (id)');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT FK_5F37A13B2156041B FOREIGN KEY (deal_type_id) REFERENCES deal_type (id)');
        $this->addSql('ALTER TABLE token ADD CONSTRAINT FK_5F37A13BDB13039A FOREIGN KEY (fee_payer_option_id) REFERENCES fee_payer_option (id)');
        $this->addSql('ALTER TABLE deal ADD deal_token_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE deal ADD CONSTRAINT FK_E3FEC116A259F623 FOREIGN KEY (deal_token_id) REFERENCES token (id)');
        $this->addSql('CREATE INDEX IDX_E3FEC116A259F623 ON deal (deal_token_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE deal DROP FOREIGN KEY FK_E3FEC116A259F623');
        $this->addSql('DROP TABLE token');
        $this->addSql('DROP INDEX IDX_E3FEC116A259F623 ON deal');
        $this->addSql('ALTER TABLE deal DROP deal_token_id');
    }
}
