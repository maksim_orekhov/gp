<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180607074835 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE mandarin_payment_order (id INT AUTO_INCREMENT NOT NULL, payment_order_id INT DEFAULT NULL, type VARCHAR(8) NOT NULL, order_id TEXT NOT NULL, amount NUMERIC(10, 2) NOT NULL, action VARCHAR(6) NOT NULL, transaction VARCHAR(64) NOT NULL, status VARCHAR(16) NOT NULL, customer_email VARCHAR(64) NOT NULL, customer_phone VARCHAR(64) NOT NULL, card_holder VARCHAR(64) NOT NULL, card_number VARCHAR(16) NOT NULL, card_expiration_year VARCHAR(2) NOT NULL, card_expiration_month VARCHAR(2) NOT NULL, error_code INT DEFAULT NULL, error_description TEXT DEFAULT NULL, payment_purpose VARCHAR(255) NOT NULL, created DATETIME NOT NULL, UNIQUE INDEX UNIQ_7F34743ECD592F50 (payment_order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mandarin_payment_order ADD CONSTRAINT FK_7F34743ECD592F50 FOREIGN KEY (payment_order_id) REFERENCES payment_order (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE mandarin_payment_order');
    }
}
