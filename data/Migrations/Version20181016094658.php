<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181016094658 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE delivery_order (id INT AUTO_INCREMENT NOT NULL, deal_id INT DEFAULT NULL, delivery_service_type_id INT DEFAULT NULL, order_number VARCHAR(45) NOT NULL, created DATETIME NOT NULL, terms_of_delivery TEXT DEFAULT NULL, UNIQUE INDEX UNIQ_E522750A551F0F81 (order_number), UNIQUE INDEX UNIQ_E522750AF60E2305 (deal_id), INDEX IDX_E522750ABC0EFE22 (delivery_service_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery_service_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(45) NOT NULL, output_name VARCHAR(45) NOT NULL, description TEXT DEFAULT NULL, priority INT DEFAULT NULL, is_active TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_DD1E1AC15E237E06 (name), UNIQUE INDEX UNIQ_DD1E1AC15213D7DC (output_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery_tracking_number (id INT AUTO_INCREMENT NOT NULL, delivery_order_id INT DEFAULT NULL, created DATETIME DEFAULT NULL, tracking_number VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_88DABC353E1C9C18 (tracking_number), INDEX IDX_88DABC35ECFE8C54 (delivery_order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_city (id INT AUTO_INCREMENT NOT NULL, city_id BIGINT UNSIGNED NOT NULL, country_code VARCHAR(2) NOT NULL, country_name VARCHAR(16) DEFAULT NULL, region_code VARCHAR(3) NOT NULL, region_name VARCHAR(128) NOT NULL, city_code VARCHAR(11) NOT NULL, city_name VARCHAR(64) NOT NULL, abbreviation VARCHAR(20) DEFAULT NULL, index_min INT DEFAULT NULL, index_max INT DEFAULT NULL, payment_on_delivery TINYINT(1) NOT NULL, full_city_name VARCHAR(192) DEFAULT NULL, UNIQUE INDEX UNIQ_F8D2697D8BAC62AF (city_id), FULLTEXT INDEX city_ft_index (city_name), FULLTEXT INDEX full_city_ft_index (full_city_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_city_candidate (id INT AUTO_INCREMENT NOT NULL, city_id BIGINT UNSIGNED NOT NULL, country_code VARCHAR(2) NOT NULL, country_name VARCHAR(16) DEFAULT NULL, region_code VARCHAR(3) NOT NULL, region_name VARCHAR(128) NOT NULL, city_code VARCHAR(11) NOT NULL, city_name VARCHAR(64) NOT NULL, abbreviation VARCHAR(20) DEFAULT NULL, index_min INT DEFAULT NULL, index_max INT DEFAULT NULL, payment_on_delivery TINYINT(1) NOT NULL, full_city_name VARCHAR(192) DEFAULT NULL, UNIQUE INDEX UNIQ_65F013518BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_city_previous (id INT AUTO_INCREMENT NOT NULL, city_id BIGINT UNSIGNED NOT NULL, country_code VARCHAR(2) NOT NULL, country_name VARCHAR(16) DEFAULT NULL, region_code VARCHAR(3) NOT NULL, region_name VARCHAR(128) NOT NULL, city_code VARCHAR(11) NOT NULL, city_name VARCHAR(64) NOT NULL, abbreviation VARCHAR(20) DEFAULT NULL, index_min INT DEFAULT NULL, index_max INT DEFAULT NULL, payment_on_delivery TINYINT(1) NOT NULL, full_city_name VARCHAR(192) DEFAULT NULL, UNIQUE INDEX UNIQ_27DACEB68BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_delivery_address (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, terminal_code VARCHAR(16) DEFAULT NULL, post_index VARCHAR(6) DEFAULT NULL, city_id BIGINT UNSIGNED NOT NULL, street VARCHAR(64) NOT NULL, street_abr VARCHAR(40) DEFAULT NULL, house VARCHAR(16) DEFAULT NULL, house_korpus VARCHAR(16) DEFAULT NULL, str VARCHAR(16) DEFAULT NULL, vlad VARCHAR(16) DEFAULT NULL, extra_info VARCHAR(90) DEFAULT NULL, office VARCHAR(10) DEFAULT NULL, flat VARCHAR(10) DEFAULT NULL, work_time_from VARCHAR(6) DEFAULT NULL, work_time_to VARCHAR(6) DEFAULT NULL, dinner_time_from VARCHAR(6) DEFAULT NULL, dinner_time_to VARCHAR(6) DEFAULT NULL, contact_fio VARCHAR(255) DEFAULT NULL, contact_phone VARCHAR(16) DEFAULT NULL, contact_email VARCHAR(60) DEFAULT NULL, instructions VARCHAR(255) DEFAULT NULL, need_pass TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_delivery_order (id INT AUTO_INCREMENT NOT NULL, delivery_order_id INT DEFAULT NULL, sender_address_id INT DEFAULT NULL, receiver_address_id INT DEFAULT NULL, order_number VARCHAR(45) DEFAULT NULL, date_pickup DATETIME DEFAULT NULL, pickup_time_period VARCHAR(6) DEFAULT NULL, delivery_time_period VARCHAR(6) DEFAULT NULL, service_code VARCHAR(60) NOT NULL, is_cargo_valuable TINYINT(1) DEFAULT NULL, declared_value NUMERIC(10, 2) DEFAULT NULL, cargo_category VARCHAR(128) DEFAULT NULL, payment_type VARCHAR(3) DEFAULT NULL, dpd_order_creation_status VARCHAR(20) DEFAULT NULL, dpd_order_creation_error TEXT DEFAULT NULL, UNIQUE INDEX UNIQ_EC34F40AECFE8C54 (delivery_order_id), UNIQUE INDEX UNIQ_EC34F40AA133F1B8 (sender_address_id), UNIQUE INDEX UNIQ_EC34F40AB613A9BC (receiver_address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_delivery_parcel (id INT AUTO_INCREMENT NOT NULL, dpd_delivery_order_id INT DEFAULT NULL, weight NUMERIC(6, 2) DEFAULT NULL, length NUMERIC(7, 3) DEFAULT NULL, width NUMERIC(7, 3) DEFAULT NULL, parcel_height NUMERIC(7, 3) DEFAULT NULL, INDEX IDX_D783B26F3AB29006 (dpd_delivery_order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_parcel_shop_limits (id INT AUTO_INCREMENT NOT NULL, weight DOUBLE PRECISION DEFAULT NULL, width INT DEFAULT NULL, length INT DEFAULT NULL, height INT DEFAULT NULL, dimension_sum INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_parcel_shop_limits_candidate (id INT AUTO_INCREMENT NOT NULL, weight DOUBLE PRECISION DEFAULT NULL, width INT DEFAULT NULL, length INT DEFAULT NULL, height INT DEFAULT NULL, dimension_sum INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_parcel_shop_limits_previous (id INT AUTO_INCREMENT NOT NULL, weight DOUBLE PRECISION DEFAULT NULL, width INT DEFAULT NULL, length INT DEFAULT NULL, height INT DEFAULT NULL, dimension_sum INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_parcel_shop_state (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(4) NOT NULL, UNIQUE INDEX UNIQ_E0957FBC77153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_parcel_shop_type (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(16) NOT NULL, icon VARCHAR(16) DEFAULT NULL, UNIQUE INDEX UNIQ_E39A7A4177153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_point (id INT AUTO_INCREMENT NOT NULL, city_id INT DEFAULT NULL, dpd_parcel_shop_type_id INT DEFAULT NULL, dpd_parcel_shop_state_id INT DEFAULT NULL, dpd_parcel_shop_limits_id INT DEFAULT NULL, code VARCHAR(16) NOT NULL, name VARCHAR(100) DEFAULT NULL, cashpay TINYINT(1) DEFAULT NULL, street VARCHAR(64) NOT NULL, street_abr VARCHAR(40) DEFAULT NULL, house VARCHAR(16) DEFAULT NULL, structure VARCHAR(16) DEFAULT NULL, ownership VARCHAR(16) DEFAULT NULL, latitude VARCHAR(10) NOT NULL, longitude VARCHAR(10) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_B870837B8BAC62AF (city_id), INDEX IDX_B870837BD213C902 (dpd_parcel_shop_type_id), INDEX IDX_B870837B82C7C056 (dpd_parcel_shop_state_id), INDEX IDX_B870837BD454E27D (dpd_parcel_shop_limits_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_point_candidate (id INT AUTO_INCREMENT NOT NULL, city_id INT DEFAULT NULL, dpd_parcel_shop_type_id INT DEFAULT NULL, dpd_parcel_shop_state_id INT DEFAULT NULL, dpd_parcel_shop_limits_id INT DEFAULT NULL, code VARCHAR(16) NOT NULL, name VARCHAR(100) DEFAULT NULL, cashpay TINYINT(1) DEFAULT NULL, street VARCHAR(64) NOT NULL, street_abr VARCHAR(40) DEFAULT NULL, house VARCHAR(16) DEFAULT NULL, structure VARCHAR(16) DEFAULT NULL, ownership VARCHAR(16) DEFAULT NULL, latitude VARCHAR(10) NOT NULL, longitude VARCHAR(10) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_6B95FDEB8BAC62AF (city_id), INDEX IDX_6B95FDEBD213C902 (dpd_parcel_shop_type_id), INDEX IDX_6B95FDEB82C7C056 (dpd_parcel_shop_state_id), INDEX IDX_6B95FDEBD454E27D (dpd_parcel_shop_limits_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_point_previous (id INT AUTO_INCREMENT NOT NULL, city_id INT DEFAULT NULL, dpd_parcel_shop_type_id INT DEFAULT NULL, dpd_parcel_shop_state_id INT DEFAULT NULL, dpd_parcel_shop_limits_id INT DEFAULT NULL, code VARCHAR(16) NOT NULL, name VARCHAR(100) DEFAULT NULL, cashpay TINYINT(1) DEFAULT NULL, street VARCHAR(64) NOT NULL, street_abr VARCHAR(40) DEFAULT NULL, house VARCHAR(16) DEFAULT NULL, structure VARCHAR(16) DEFAULT NULL, ownership VARCHAR(16) DEFAULT NULL, latitude VARCHAR(10) NOT NULL, longitude VARCHAR(10) NOT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_99BC46BE8BAC62AF (city_id), INDEX IDX_99BC46BED213C902 (dpd_parcel_shop_type_id), INDEX IDX_99BC46BE82C7C056 (dpd_parcel_shop_state_id), INDEX IDX_99BC46BED454E27D (dpd_parcel_shop_limits_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_schedule (id INT AUTO_INCREMENT NOT NULL, dpd_point_id INT DEFAULT NULL, type VARCHAR(20) NOT NULL, INDEX IDX_E45E27D47E4EF88D (dpd_point_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_schedule_candidate (id INT AUTO_INCREMENT NOT NULL, dpd_point_id INT DEFAULT NULL, type VARCHAR(20) NOT NULL, INDEX IDX_82078CF07E4EF88D (dpd_point_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_schedule_previous (id INT AUTO_INCREMENT NOT NULL, dpd_point_id INT DEFAULT NULL, type VARCHAR(20) NOT NULL, INDEX IDX_686868B87E4EF88D (dpd_point_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_timetable (id INT AUTO_INCREMENT NOT NULL, schedule_id INT DEFAULT NULL, week_days VARCHAR(20) NOT NULL, work_time VARCHAR(20) NOT NULL, INDEX IDX_ADDEAD1FA40BC2D5 (schedule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_timetable_candidate (id INT AUTO_INCREMENT NOT NULL, schedule_id INT DEFAULT NULL, week_days VARCHAR(20) NOT NULL, work_time VARCHAR(20) NOT NULL, INDEX IDX_738B4607A40BC2D5 (schedule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_timetable_previous (id INT AUTO_INCREMENT NOT NULL, schedule_id INT DEFAULT NULL, week_days VARCHAR(20) NOT NULL, work_time VARCHAR(20) NOT NULL, INDEX IDX_3040DD69A40BC2D5 (schedule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dpd_update_info (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(25) NOT NULL, last_dpd_city_candidate_update DATETIME DEFAULT NULL, last_dpd_point_candidate_update DATETIME DEFAULT NULL, last_dpd_city_production_update DATETIME DEFAULT NULL, last_dpd_point_production_update DATETIME DEFAULT NULL, last_dpd_city_previous_update DATETIME DEFAULT NULL, last_dpd_point_previous_update DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_13932B35E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE delivery_order ADD CONSTRAINT FK_E522750AF60E2305 FOREIGN KEY (deal_id) REFERENCES deal (id)');
        $this->addSql('ALTER TABLE delivery_order ADD CONSTRAINT FK_E522750ABC0EFE22 FOREIGN KEY (delivery_service_type_id) REFERENCES delivery_service_type (id)');
        $this->addSql('ALTER TABLE delivery_tracking_number ADD CONSTRAINT FK_88DABC35ECFE8C54 FOREIGN KEY (delivery_order_id) REFERENCES delivery_order (id)');
        $this->addSql('ALTER TABLE dpd_delivery_order ADD CONSTRAINT FK_EC34F40AECFE8C54 FOREIGN KEY (delivery_order_id) REFERENCES delivery_order (id)');
        $this->addSql('ALTER TABLE dpd_delivery_order ADD CONSTRAINT FK_EC34F40AA133F1B8 FOREIGN KEY (sender_address_id) REFERENCES dpd_delivery_address (id)');
        $this->addSql('ALTER TABLE dpd_delivery_order ADD CONSTRAINT FK_EC34F40AB613A9BC FOREIGN KEY (receiver_address_id) REFERENCES dpd_delivery_address (id)');
        $this->addSql('ALTER TABLE dpd_delivery_parcel ADD CONSTRAINT FK_D783B26F3AB29006 FOREIGN KEY (dpd_delivery_order_id) REFERENCES dpd_delivery_order (id)');
        $this->addSql('ALTER TABLE dpd_point ADD CONSTRAINT FK_B870837B8BAC62AF FOREIGN KEY (city_id) REFERENCES dpd_city (id)');
        $this->addSql('ALTER TABLE dpd_point ADD CONSTRAINT FK_B870837BD213C902 FOREIGN KEY (dpd_parcel_shop_type_id) REFERENCES dpd_parcel_shop_type (id)');
        $this->addSql('ALTER TABLE dpd_point ADD CONSTRAINT FK_B870837B82C7C056 FOREIGN KEY (dpd_parcel_shop_state_id) REFERENCES dpd_parcel_shop_state (id)');
        $this->addSql('ALTER TABLE dpd_point ADD CONSTRAINT FK_B870837BD454E27D FOREIGN KEY (dpd_parcel_shop_limits_id) REFERENCES dpd_parcel_shop_limits (id)');
        $this->addSql('ALTER TABLE dpd_point_candidate ADD CONSTRAINT FK_6B95FDEB8BAC62AF FOREIGN KEY (city_id) REFERENCES dpd_city_candidate (id)');
        $this->addSql('ALTER TABLE dpd_point_candidate ADD CONSTRAINT FK_6B95FDEBD213C902 FOREIGN KEY (dpd_parcel_shop_type_id) REFERENCES dpd_parcel_shop_type (id)');
        $this->addSql('ALTER TABLE dpd_point_candidate ADD CONSTRAINT FK_6B95FDEB82C7C056 FOREIGN KEY (dpd_parcel_shop_state_id) REFERENCES dpd_parcel_shop_state (id)');
        $this->addSql('ALTER TABLE dpd_point_candidate ADD CONSTRAINT FK_6B95FDEBD454E27D FOREIGN KEY (dpd_parcel_shop_limits_id) REFERENCES dpd_parcel_shop_limits_candidate (id)');
        $this->addSql('ALTER TABLE dpd_point_previous ADD CONSTRAINT FK_99BC46BE8BAC62AF FOREIGN KEY (city_id) REFERENCES dpd_city_previous (id)');
        $this->addSql('ALTER TABLE dpd_point_previous ADD CONSTRAINT FK_99BC46BED213C902 FOREIGN KEY (dpd_parcel_shop_type_id) REFERENCES dpd_parcel_shop_type (id)');
        $this->addSql('ALTER TABLE dpd_point_previous ADD CONSTRAINT FK_99BC46BE82C7C056 FOREIGN KEY (dpd_parcel_shop_state_id) REFERENCES dpd_parcel_shop_state (id)');
        $this->addSql('ALTER TABLE dpd_point_previous ADD CONSTRAINT FK_99BC46BED454E27D FOREIGN KEY (dpd_parcel_shop_limits_id) REFERENCES dpd_parcel_shop_limits_previous (id)');
        $this->addSql('ALTER TABLE dpd_schedule ADD CONSTRAINT FK_E45E27D47E4EF88D FOREIGN KEY (dpd_point_id) REFERENCES dpd_point (id)');
        $this->addSql('ALTER TABLE dpd_schedule_candidate ADD CONSTRAINT FK_82078CF07E4EF88D FOREIGN KEY (dpd_point_id) REFERENCES dpd_point_candidate (id)');
        $this->addSql('ALTER TABLE dpd_schedule_previous ADD CONSTRAINT FK_686868B87E4EF88D FOREIGN KEY (dpd_point_id) REFERENCES dpd_point_previous (id)');
        $this->addSql('ALTER TABLE dpd_timetable ADD CONSTRAINT FK_ADDEAD1FA40BC2D5 FOREIGN KEY (schedule_id) REFERENCES dpd_schedule (id)');
        $this->addSql('ALTER TABLE dpd_timetable_candidate ADD CONSTRAINT FK_738B4607A40BC2D5 FOREIGN KEY (schedule_id) REFERENCES dpd_schedule_candidate (id)');
        $this->addSql('ALTER TABLE dpd_timetable_previous ADD CONSTRAINT FK_3040DD69A40BC2D5 FOREIGN KEY (schedule_id) REFERENCES dpd_schedule_previous (id)');
        $this->addSql('ALTER TABLE delivery ADD delivery_order_id INT DEFAULT NULL, DROP tracking_number');
        $this->addSql('ALTER TABLE delivery ADD CONSTRAINT FK_3781EC10ECFE8C54 FOREIGN KEY (delivery_order_id) REFERENCES delivery_order (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3781EC10ECFE8C54 ON delivery (delivery_order_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE delivery DROP FOREIGN KEY FK_3781EC10ECFE8C54');
        $this->addSql('ALTER TABLE delivery_tracking_number DROP FOREIGN KEY FK_88DABC35ECFE8C54');
        $this->addSql('ALTER TABLE dpd_delivery_order DROP FOREIGN KEY FK_EC34F40AECFE8C54');
        $this->addSql('ALTER TABLE delivery_order DROP FOREIGN KEY FK_E522750ABC0EFE22');
        $this->addSql('ALTER TABLE dpd_point DROP FOREIGN KEY FK_B870837B8BAC62AF');
        $this->addSql('ALTER TABLE dpd_point_candidate DROP FOREIGN KEY FK_6B95FDEB8BAC62AF');
        $this->addSql('ALTER TABLE dpd_point_previous DROP FOREIGN KEY FK_99BC46BE8BAC62AF');
        $this->addSql('ALTER TABLE dpd_delivery_order DROP FOREIGN KEY FK_EC34F40AA133F1B8');
        $this->addSql('ALTER TABLE dpd_delivery_order DROP FOREIGN KEY FK_EC34F40AB613A9BC');
        $this->addSql('ALTER TABLE dpd_delivery_parcel DROP FOREIGN KEY FK_D783B26F3AB29006');
        $this->addSql('ALTER TABLE dpd_point DROP FOREIGN KEY FK_B870837BD454E27D');
        $this->addSql('ALTER TABLE dpd_point_candidate DROP FOREIGN KEY FK_6B95FDEBD454E27D');
        $this->addSql('ALTER TABLE dpd_point_previous DROP FOREIGN KEY FK_99BC46BED454E27D');
        $this->addSql('ALTER TABLE dpd_point DROP FOREIGN KEY FK_B870837B82C7C056');
        $this->addSql('ALTER TABLE dpd_point_candidate DROP FOREIGN KEY FK_6B95FDEB82C7C056');
        $this->addSql('ALTER TABLE dpd_point_previous DROP FOREIGN KEY FK_99BC46BE82C7C056');
        $this->addSql('ALTER TABLE dpd_point DROP FOREIGN KEY FK_B870837BD213C902');
        $this->addSql('ALTER TABLE dpd_point_candidate DROP FOREIGN KEY FK_6B95FDEBD213C902');
        $this->addSql('ALTER TABLE dpd_point_previous DROP FOREIGN KEY FK_99BC46BED213C902');
        $this->addSql('ALTER TABLE dpd_schedule DROP FOREIGN KEY FK_E45E27D47E4EF88D');
        $this->addSql('ALTER TABLE dpd_schedule_candidate DROP FOREIGN KEY FK_82078CF07E4EF88D');
        $this->addSql('ALTER TABLE dpd_schedule_previous DROP FOREIGN KEY FK_686868B87E4EF88D');
        $this->addSql('ALTER TABLE dpd_timetable DROP FOREIGN KEY FK_ADDEAD1FA40BC2D5');
        $this->addSql('ALTER TABLE dpd_timetable_candidate DROP FOREIGN KEY FK_738B4607A40BC2D5');
        $this->addSql('ALTER TABLE dpd_timetable_previous DROP FOREIGN KEY FK_3040DD69A40BC2D5');
        $this->addSql('DROP TABLE delivery_order');
        $this->addSql('DROP TABLE delivery_service_type');
        $this->addSql('DROP TABLE delivery_tracking_number');
        $this->addSql('DROP TABLE dpd_city');
        $this->addSql('DROP TABLE dpd_city_candidate');
        $this->addSql('DROP TABLE dpd_city_previous');
        $this->addSql('DROP TABLE dpd_delivery_address');
        $this->addSql('DROP TABLE dpd_delivery_order');
        $this->addSql('DROP TABLE dpd_delivery_parcel');
        $this->addSql('DROP TABLE dpd_parcel_shop_limits');
        $this->addSql('DROP TABLE dpd_parcel_shop_limits_candidate');
        $this->addSql('DROP TABLE dpd_parcel_shop_limits_previous');
        $this->addSql('DROP TABLE dpd_parcel_shop_state');
        $this->addSql('DROP TABLE dpd_parcel_shop_type');
        $this->addSql('DROP TABLE dpd_point');
        $this->addSql('DROP TABLE dpd_point_candidate');
        $this->addSql('DROP TABLE dpd_point_previous');
        $this->addSql('DROP TABLE dpd_schedule');
        $this->addSql('DROP TABLE dpd_schedule_candidate');
        $this->addSql('DROP TABLE dpd_schedule_previous');
        $this->addSql('DROP TABLE dpd_timetable');
        $this->addSql('DROP TABLE dpd_timetable_candidate');
        $this->addSql('DROP TABLE dpd_timetable_previous');
        $this->addSql('DROP TABLE dpd_update_info');
        $this->addSql('DROP INDEX UNIQ_3781EC10ECFE8C54 ON delivery');
        $this->addSql('ALTER TABLE delivery ADD tracking_number MEDIUMTEXT DEFAULT NULL COLLATE utf8_unicode_ci, DROP delivery_order_id');
    }
}
