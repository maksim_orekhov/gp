<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180622073743 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE civil_law_subject ADD nds_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE civil_law_subject ADD CONSTRAINT FK_CBAC6342AC21E54F FOREIGN KEY (nds_type_id) REFERENCES nds_type (id)');
        $this->addSql('CREATE INDEX IDX_CBAC6342AC21E54F ON civil_law_subject (nds_type_id)');
        $this->addSql('ALTER TABLE legal_entity DROP FOREIGN KEY FK_E21E9E13AC21E54F');
        $this->addSql('DROP INDEX IDX_E21E9E13AC21E54F ON legal_entity');
        $this->addSql('ALTER TABLE legal_entity DROP nds_type_id');
        $this->addSql('DROP INDEX UNIQ_45D5AA5677153098 ON nds_type');
        $this->addSql('ALTER TABLE nds_type ADD is_active TINYINT(1) DEFAULT NULL, CHANGE code type INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_45D5AA568CDE5729 ON nds_type (type)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE civil_law_subject DROP FOREIGN KEY FK_CBAC6342AC21E54F');
        $this->addSql('DROP INDEX IDX_CBAC6342AC21E54F ON civil_law_subject');
        $this->addSql('ALTER TABLE civil_law_subject DROP nds_type_id');
        $this->addSql('ALTER TABLE legal_entity ADD nds_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE legal_entity ADD CONSTRAINT FK_E21E9E13AC21E54F FOREIGN KEY (nds_type_id) REFERENCES nds_type (id)');
        $this->addSql('CREATE INDEX IDX_E21E9E13AC21E54F ON legal_entity (nds_type_id)');
        $this->addSql('DROP INDEX UNIQ_45D5AA568CDE5729 ON nds_type');
        $this->addSql('ALTER TABLE nds_type DROP is_active, CHANGE type code INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_45D5AA5677153098 ON nds_type (code)');
    }
}
